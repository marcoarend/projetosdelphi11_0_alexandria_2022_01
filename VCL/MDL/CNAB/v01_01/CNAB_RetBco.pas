unit CNAB_RetBco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCNAB_RetBco = class(TForm)
    QrBcoDir: TmySQLQuery;
    QrBcoArq: TmySQLQuery;
    QrBcoDirCodigo: TIntegerField;
    QrBcoDirNome: TWideStringField;
    QrBcoDirCNABDirMul: TWideStringField;
    QrBcoDirDirExiste: TWideStringField;
    QrBcoDirArquivos: TIntegerField;
    QrBcoDirAtivo: TSmallintField;
    DsBcoDir: TDataSource;
    DsBcoArq: TDataSource;
    QrTem: TmySQLQuery;
    QrTemItens: TLargeintField;
    QrBcoArqSEQ: TIntegerField;
    PnCarrega: TPanel;
    PnArquivos: TPanel;
    Panel1: TPanel;
    Splitter3: TSplitter;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    Grade1_: TStringGrid;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    Memo2: TMemo;
    QrLocByCed: TmySQLQuery;
    QrLocByCedCNPJ_CPF: TWideStringField;
    QrLocEnt2: TmySQLQuery;
    QrLocEnt2NOMEENT: TWideStringField;
    QrLocEnt2CLIENTE: TIntegerField;
    QrLocEnt2CliInt: TIntegerField;
    QrLocEnt1: TmySQLQuery;
    QrLocEnt1NOMEENT: TWideStringField;
    QrLocEnt1CLIENTE: TIntegerField;
    QrLocEnt1CliInt: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Vencimento: TDateField;
    QrPesq2PercMulta: TFloatField;
    QrPesq2PercJuros: TFloatField;
    QrPesq2Credito: TFloatField;
    QrBcoArqCodigo: TIntegerField;
    QrBcoArqControle: TIntegerField;
    QrBcoArqArquivo: TWideStringField;
    QrBcoArqEntidade: TIntegerField;
    QrBcoArqNO_Entid: TWideStringField;
    QrBcoArqCNPJ_CPF: TWideStringField;
    QrBcoArqLotes: TIntegerField;
    QrBcoArqItens: TIntegerField;
    QrBcoArqCreditos: TFloatField;
    QrBcoArqBcoArq: TIntegerField;
    QrBcoArqCNAB: TIntegerField;
    QrBcoArqAtivo: TSmallintField;
    DBGBcoDir: TDBGrid;
    DBGBcoArq: TDBGrid;
    QrBcoReg: TmySQLQuery;
    DsBcoReg: TDataSource;
    QrBcoRegCodigo: TIntegerField;
    QrBcoRegControle: TIntegerField;
    QrBcoRegConta: TAutoIncField;
    QrBcoRegNossoNum: TWideStringField;
    QrBcoRegOcorrCod: TIntegerField;
    QrBcoRegOcorrTxt: TWideStringField;
    QrBcoRegOcorrDta: TDateField;
    QrBcoRegSeuNumer: TFloatField;
    QrBcoRegNumDocum: TWideStringField;
    QrBcoRegValTitul: TFloatField;
    QrBcoRegValAbati: TFloatField;
    QrBcoRegValDesco: TFloatField;
    QrBcoRegValEPago: TFloatField;
    QrBcoRegValJuros: TFloatField;
    QrBcoRegValMulta: TFloatField;
    QrBcoRegValOutrC: TFloatField;
    QrBcoRegValJuMul: TFloatField;
    QrBcoRegValTarif: TFloatField;
    QrBcoRegValErro: TFloatField;
    QrBcoRegMotivos: TWideStringField;
    QrBcoRegMotivTxt: TWideStringField;
    QrBcoRegPagtoDta: TDateField;
    QrBcoRegDtaTarif: TDateField;
    QrBcoRegEntidade: TIntegerField;
    QrBcoRegID_Link: TWideStringField;
    QrBcoRegSeqDir: TIntegerField;
    QrBcoRegSeqArq: TIntegerField;
    QrBcoRegSeqReg: TIntegerField;
    QrBcoRegBanco: TIntegerField;
    QrBcoRegValCBrut: TFloatField;
    QrBcoRegValOutrD: TFloatField;
    QrBcoRegVenctDta: TDateField;
    QrBcoRegAtivo: TSmallintField;
    DBGrid1: TDBGrid;
    QrBcoRegSEQ: TIntegerField;
    QrBcoRegOcorrDta_TXT: TWideStringField;
    QrBcoRegPagtoDta_TXT: TWideStringField;
    QrBcoRegDtaTarif_TXT: TWideStringField;
    QrBcoRegVenctDta_TXT: TWideStringField;
    QrBcoRegCNPJ_CPF: TWideStringField;
    QrBcoRegEntCliente: TIntegerField;
    QrBcoRegNO_EntClie: TWideStringField;
    QrErros: TmySQLQuery;
    QrBcoRegCedente: TWideStringField;
    QrLocEnt3: TmySQLQuery;
    QrLocEnt3NOMEENT: TWideStringField;
    QrLocEnt3CLIENTE: TIntegerField;
    QrLocEnt3CliInt: TIntegerField;
    QrLocEnt3CNPJ_CPF: TWideStringField;
    QrLocCar1: TmySQLQuery;
    QrLocCar1Codigo: TIntegerField;
    QrLocCar1Nome: TWideStringField;
    QrBcoRegCartCod: TIntegerField;
    QrBcoRegCartTxt: TWideStringField;
    QrSemCart: TmySQLQuery;
    QrDupl: TmySQLQuery;
    QrDuplCodigo: TIntegerField;
    QrPesq3: TmySQLQuery;
    QrPesq3Credito: TFloatField;
    QrPesq3PercMulta: TFloatField;
    QrPesq3PercJuros: TFloatField;
    QrPesq3Vencimento: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtArquivos: TBitBtn;
    BtEdita: TBitBtn;
    BtCarrega: TBitBtn;
    CkReverter: TCheckBox;
    BtAbertos: TBitBtn;
    BtBuffer: TBitBtn;
    PB1: TProgressBar;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrBcoDirBeforeClose(DataSet: TDataSet);
    procedure QrBcoDirAfterScroll(DataSet: TDataSet);
    procedure DBGBcoDirDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrBcoDirAfterClose(DataSet: TDataSet);
    procedure QrBcoArqCalcFields(DataSet: TDataSet);
    procedure QrBcoArqAfterScroll(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure QrBcoArqBeforeClose(DataSet: TDataSet);
    procedure QrBcoRegCalcFields(DataSet: TDataSet);
    procedure BtEditaClick(Sender: TObject);
    procedure QrBcoRegBeforeClose(DataSet: TDataSet);
    procedure QrBcoRegAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FBcoDir, FBcoArq, FBcoReg: String;
    {}
    FLin1,
    FConta: Integer;
    {}
    function CarregaArquivo(const NomeLayout, Arquivo: String; const
             Controle: Integer; var Banco, CNAB, Entidade: Integer): Boolean;
    function CarregaItensRetorno(LinA, Banco, CNAB, Controle, Entidade:
             Integer; Arquivo: String): Boolean;
    function CarregaItensRetornoA(NomeLayout, Arquivo: String;
             SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
    function GravaItens(): Integer;

    procedure CarregaItensRetorno240(NomeLayout: String; Banco, Entidade, SeqDir,
              SeqArq: Integer; Arquivo: String);
    procedure CarregaItensRetorno400(NomeLayout: String; Banco, Entidade,
              SeqDir, SeqArq: Integer; Arquivo: String);
    procedure LeArquivos(Banco, Controle: Integer; Dir, Arq: String);
    procedure HabilitaBotoes();
    procedure ReopenBcoArq(Controle: Integer);
    procedure ReopenBcoDir(Filtra: Boolean; Codigo: Integer);
    procedure ReopenLocEnt1(CNPJ: String);
    //  Never Used
    //procedure ReopenLocEnt2(Banco: Integer; Agencia, Cedente: String);
    procedure ReopenLocEnt3(Banco: Integer; Agencia, Conta, DAC: String);
    procedure ReopenLocEnt4(Banco: Integer; Agencia, Conta,
              DVAgencia, DVConta: String);
    procedure ReopenLocCar1(Banco, EntCliente: Integer; CNPJ, Cedente: String);
    procedure ReopenQrPesq2(IDLink, Cliente: Integer);
    procedure ReopenSQL3(ID_Link, Entidade: Integer);
    procedure VerificaImplementacoes(Row: Integer);

  public
    { Public declarations }
    FLista: TStrings;
    FLengt: Integer;
    //
    procedure ReopenBcoReg(Conta: Integer);
    function ErroLinha(Entidade, Banco: Integer;
             ID_Link, OcorrCod, NossoNum: String; Avisa: Boolean;
              ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta,
              ValJuMul, ValTarif, ValErro, ValOutro, ValReceb: Double): Double;
  end;

  var
  FmCNAB_RetBco: TFmCNAB_RetBco;

implementation

uses UnMyObjects, Module, ModuleGeral, UnInternalConsts, UCreate, UMySQLModule,
UnBancos, MyDBCheck, CNAB_DuplCad, CNAB_RetBcoEdit;

{$R *.DFM}

procedure TFmCNAB_RetBco.BtArquivosClick(Sender: TObject);
var
  DirExiste: String;
  Diretorio, Nome: String;
  K, Arquivos: Integer;
  I, Codigo, Controle: Integer;
  ListBox: TListBox;
begin
  Screen.Cursor := crHourGlass;
  FConta := 0;
  ListBox := TListBox.Create(FmCNAB_RetBco);
  ListBox.ParentWindow := FmCNAB_RetBco.Handle;
  try
    FBcoDir := UCriar.RecriaTempTableNovo(ntrttCNABBcoDir, DmodG.QrUpdPID1, False);
    FBcoArq := UCriar.RecriaTempTableNovo(ntrttCNABBcoArq, DmodG.QrUpdPID1, False);
    FBcoReg := UCriar.RecriaTempTableNovo(ntrttCNABBcoReg, DmodG.QrUpdPID1, False);
    Controle := 0;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FBcoDir);
    DModG.QrUpdPID1.SQL.Add('SELECT Codigo, Nome, CNABDirMul, ');
    DModG.QrUpdPID1.SQL.Add('"N�o" DirExiste, 0 Arquivos, 0 Ativo');
    DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.bancos');
    DModG.QrUpdPID1.SQL.Add('WHERE CNABDirMul <> ""');
    DModG.QrUpdPID1.ExecSQL;
    ReopenBcoDir(False, 0);
    if QrBcoDir.RecordCount = 0 then
    begin
      Geral.MensagemBox(
      'Nenhum banco tem definido em seu cadastro o caminho' + sLineBreak +
      'dos arquivos de retorno de m�ltiplas empresas!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    QrBcoDir.First;
    while not QrBcoDir.Eof do
    begin
      Codigo := QrBcoDirCodigo.Value;
      DirExiste := 'N�o';
      Diretorio := QrBcoDirCNABDirMul.Value;
      K := Length(Diretorio);
      if Diretorio[K] = '\' then
        Diretorio := Copy(Diretorio, 1, K -1);
      Arquivos := 0;
      if DirectoryExists(QrBcoDirCNABDirMul.Value) then
      begin
        DirExiste := 'Sim';
        MyObjects.GetAllFiles(False, Diretorio + '\*.*', ListBox, True, LaAviso1, LaAviso2);
        if Arquivos > 0  then
        begin
          for I := 0 to Arquivos - 1 do
          begin
            Controle := Controle + 1;
            Nome := ExtractFileName(ListBox.Items[I]);
            //
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'cnabbcoarq', False, [
            'Codigo', 'Arquivo'], ['Controle'], [
            Codigo, Nome], [Controle], False) then
            begin
              try
                LeArquivos(Codigo, Controle, QrBcoDirCNABDirMul.Value, Nome);
              except
                ;
              end;
            end;
          end;
        end;
      end;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'cnabbcodir', False,
        ['DirExiste', 'Arquivos'], ['Codigo'],
        [DirExiste, Arquivos], [Codigo], False);
      //
      QrBcoDir.Next;
    end;
    ReopenBcoDir(True, 0);
  finally
    Screen.Cursor := crDefault;
    ListBox.Free;
  end;
end;

procedure TFmCNAB_RetBco.BtCarregaClick(Sender: TObject);
begin
  if GravaItens() > 0 then
  begin
    BtCarrega.Enabled := FLin1 > 0;
{  Parei aqui
    MostraAbertos();
}
  end;
end;

procedure TFmCNAB_RetBco.BtEditaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_RetBcoEdit, FmCNAB_RetBcoEdit, afmoNegarComAviso) then
  begin
    FmCNAB_RetBcoEdit.EdValJuros.ValueVariant := QrBcoRegValJuros.Value;
    FmCNAB_RetBcoEdit.EdValMulta.ValueVariant := QrBcoRegValMulta.Value;
    FmCNAB_RetBcoEdit.EdValJuMul.ValueVariant := QrBcoRegValJuMul.Value;
    FmCNAB_RetBcoEdit.EdValOutrC.ValueVariant := QrBcoRegValOutrC.Value;
    FmCNAB_RetBcoEdit.EdValTarif.ValueVariant := QrBcoRegValTarif.Value;
    FmCNAB_RetBcoEdit.ShowModal;
    FmCNAB_RetBcoEdit.Destroy;
  end;
end;

procedure TFmCNAB_RetBco.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmCNAB_RetBco.CarregaArquivo(const NomeLayout, Arquivo: String;
  const Controle: Integer; var Banco, CNAB, Entidade: Integer): Boolean;

  function ObtemDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String): String;
  begin
    case TamReg of
      240: UBancos.LocDado240(Banco, Campo, Linha, FLista, Mensagem, NomeLayout, Result);
      400: UBancos.LocDado400(Banco, Campo, Linha, FLista, Mensagem, NomeLayout, Result);
    end;
  end;

  function ConfereDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String; var Res: String): Boolean;
  var
    Padr: String;
  begin
    Res := ObtemDado(TamReg, Banco, Campo, Linha, Lista, Mensagem);
    Padr := UBancos.ValorPadrao(TamReg, Banco, Campo, NomeLayout);
    Result := Res = Padr;
    if not Result then
      Geral.MensagemBox(Mensagem, 'Aviso', MB_OK+MB_ICONWARNING);
  end;

var
  //p,
  Lin, IndexReg: Integer;
  //NomeArq, CeTxt,
  s, CNPJ, AgTxt, CCTxt, DTTxt: String;
  //
  Cedente, CodCedente,
  NO_Entid, CNPJ_CPF: String;
  BcoArq: Integer;
begin
  Result := False;
  CodCedente := '';
  //Controle := 0;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox(('O arquivo "'+Arquivo+'" n�o foi localizado.'+
    ' Ele pode ter sido exclu�do!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  FLengt := -1000;
  if not UBancos.TipoArqCNAB(Arquivo, FLista, FLengt) then Exit;

  // Verifica o padr�o do registro (240, 400)
  IndexReg := UBancos.TamanhoLinhaCNAB_Index(FLengt);
  if IndexReg = 0 then Exit;
  Banco := Geral.IMV(ObtemDado(FLengt, 0, 001, 0, FLista, ''));
  if Banco = 0 then
  begin
    Geral.MB_Erro('N�o foi poss�vel definir o banco do arquivo "' +
      Arquivo + '". Contate a dermatek caso esse arquivo for realmente de ' +
      'remessa/retorno banc�rio!' + sLineBreak + 'Arquivo de ' +
      Geral.FF0(FLengt) + ' posi��es!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //

  // Tipo de arquivo - Lote de Servico
  if not ConfereDado(FLengt, Banco, 003, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � lote de servi�o!', s) then Exit;

  // Arquivo de retorno
  if not ConfereDado(FLengt, Banco, 007, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � arquivo de retorno!', s) then Exit;


  if not UBancos.BancoImplemMultiEmp(Banco, FLengt, ecnabRetorno) then
    Exit;
  //////////////////////////////////////////////////////////////////////////////


  // Cliente interno
  // Somente para bancos que informam o CNPJ ou CPF
{
  if ((Banco = 001) and (FLengt = 400))
  or ((Banco = 399) and (FLengt = 400))
  or ((Banco = 409) and (FLengt = 400)) then
  begin
    CodCedente := ObtemDado(FLengt, Banco, 410, 0, FLista, '');
    QrLocByCed.Close;
    QrLocByCed.Params[00].AsString  := CodCedente;
    QrLocByCed.Params[01].AsInteger := Banco;
    UMyMod.AbreQuery(QrLocByCed, 'TFmCNAB_Ret2.CarregaArquivo(');
    case QrLocByCed.RecordCount of
      0:
      begin
        Geral.MensagemBox(('N�o foi localizado carteira para o arquivo "' +
        Arquivo+'" onde consta:' +sLineBreak + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + sLineBreak +
        'C�digo do cedente: "'+ CodCedente + '"' + sLineBreak +
        'Informe o c�digo retorno CNAB na carteira (de extrato) correspondente em:' +
        sLineBreak + 'Cadastros -> Financeiros -> Carteiras.'),
        'Aviso', MB_OK+MB_ICONWARNING);
        Screen.Cursor := crDefault;
        Exit;
      end;
      1: CNPJ := QrLocByCedCNPJ_CPF.Value;
      else
      begin
        Geral.MensagemBox(('Foram localizados '+
        IntToStr(QrLocEnt2.RecordCount)+' cadastros de carteiras para o arquivo "' +
        Arquivo+'" onde consta:' +sLineBreak + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + sLineBreak +
        'C�digo retorno CNAB: "'+ CodCedente + '"' +
        sLineBreak + '! Para evitar erros nenhum foi considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
  end else
}
  if Banco = 341 then
  begin
    CodCedente := ObtemDado(FLengt, Banco, 420, 0, FLista, '');
    AgTxt := Copy(CodCedente, 01, 04);
    CCTxt := Copy(CodCedente, 07, 05);
    DTTxt := Copy(CodCedente, 12, 01);
    ReopenLocEnt3(Banco, AgTxt, CCTxt, DTTxt);
    case QrLocEnt3.RecordCount of
      0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
      Arquivo+'" onde consta:' +sLineBreak + 'Banco: ' +
      FormatFloat('000', Banco) + sLineBreak + 'Ag�ncia: ' + AgTxt + sLineBreak +
      'Conta corrente: "' + CCTxt + '"' + sLineBreak + 'DV: '+ DTTxt),
      'Aviso', MB_OK+MB_ICONWARNING);
      else
      begin
        if QrLocEnt3.RecordCount > 1 then
        begin
          Geral.MensagemBox(('Foram localizados '+ IntToStr(
          QrLocEnt3.RecordCount)+' cadastros de clientes para o arquivo "' +
          Arquivo+'" onde consta:' +sLineBreak + 'Banco: ' +
          FormatFloat('000', Banco) + sLineBreak + 'Ag�ncia: ' + AgTxt + sLineBreak +
          'Conta corrente: "' + CCTxt + sLineBreak + 'DV: '+ DTTxt),
          'Aviso', MB_OK+MB_ICONWARNING);
          if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
          begin
            FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt3;
            FmCNAB_DuplCad.ShowModal;
            FmCNAB_DuplCad.Destroy;
          end;
        end;
        NO_Entid := QrLocEnt3NOMEENT.Value;
        Entidade := QrLocEnt3CLIENTE.Value;
        CNPJ     := QrLocEnt3CNPJ_CPF.Value;
        CNPJ_CPF := Geral.FormataCNPJ_TT(CNPJ);
        //Cedente  := CodCedente; // N�o pode! muda em cada linha!
        BcoArq   := Banco;
        CNAB     := FLengt;
        if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'cnabbcoarq', False, [
        'Entidade', 'NO_Entid', 'CNPJ_CPF', (*'Cedente',*) // N�o pode! muda em cada linha!
        //'Lotes', 'Itens', 'Creditos',
        'BcoArq', 'CNAB'], [
        'Controle'], [
        Entidade, NO_Entid, CNPJ_CPF, (*Cedente,*) // N�o pode! muda em cada linha!
        //Lotes, Itens, Creditos,
        BcoArq, CNAB], [
        Controle], False) then
          Result := True;
      end;
    end;
{
  end else
  if Banco = 756 then
  begin
    // Pela Ag�ncia + C�digo do cedente no nome do arquivo
    //
    NomeArq := ExtractFileName(Arquivo);
    p := pos('_', NomeArq);
    //ShowMessage(IntToStr(p));
    if p > 0 then
    begin
      AgTxt := Copy(NomeArq, p-4, 4);
      CeTxt := Copy(NomeArq, p+1, 7);
      ReopenLocEnt2(Banco, AgTxt, CeTxt);
      //
      case QrLocEnt2.RecordCount of
        0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
        Arquivo+'" onde consta:' +sLineBreak + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + sLineBreak +
        'Ag�ncia: "' + AgTxt + '"'  +
        sLineBreak + 'C�digo do cedente: "'+ CeTxt + '"'),
        'Aviso', MB_OK+MB_ICONWARNING);
        1:
        begin
          (*
          GradeA.Cells[02, linA] := QrLocEnt2NOMEENT.Value;
          GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
          GradeA.Cells[04, linA] := CodCedente;
          GradeA.Cells[09, linA] := IntToStr(QrLocEnt2CLIENTE.Value);
          GradeA.Cells[10, linA] := FormatFloat('000', Banco);
          GradeA.Cells[11, linA] := IntToStr(FLengt);
          *)
          NO_Entid := QrLocEnt2NOMEENT.Value;
          Entidade := QrLocEnt2CLIENTE.Value;
          CNPJ_CPF := Geral.FormataCNPJ_TT(CNPJ);
          //Cedente  := CodCedente; // N�o pode! muda em cada linha!
          BcoArq   := Banco;
          CNAB     := FLengt;
          if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'cnabbcoarq', False, [
          'Entidade', 'NO_Entid', 'CNPJ_CPF', (*'Cedente',*) // N�o pode! muda em cada linha!
          //'Lotes', 'Itens', 'Creditos',
          'BcoArq', 'CNAB'], [
          'Controle'], [
          Entidade, NO_Entid, CNPJ_CPF, (*Cedente,*) // N�o pode! muda em cada linha!
          //Lotes, Itens, Creditos,
          BcoArq, CNAB], [
          Controle], False) then
            Result := True;
        end;
        else Geral.MensagemBox(('Foram localizados '+
        IntToStr(QrLocEnt2.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta:' +sLineBreak + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + sLineBreak +
        'Ag�ncia: "' + AgTxt + '"'  +
        sLineBreak + 'C�digo do cedente: "'+ CeTxt + '"' +
        sLineBreak + '! Para evitar erros nenhum foi considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
      end;

    end else Geral.MensagemBox(('Foi detectado que o arquivo "'+
    NomeArq + '" pertence ao banco ' + FormatFloat('000', Banco) +
    ', mas o nome n�o est� no formato indicado pelo banco!'), 'Erro',
    MB_OK+MB_ICONERROR);
}
  end else
  begin
    Lin := -1;
    case FLengt of
      240: Lin := 0;
      400:
      begin
        if Banco = 748 then
          Lin := 0
        else
          Lin := 1;
      end;
    end;
    CNPJ := ObtemDado(FLengt, Banco, 401, Lin, FLista, '');
    if CodCedente = '' then
      CodCedente := ObtemDado(FLengt, Banco, 420, 0, FLista, '');
    if (CNPJ = '') and (CodCedente = '') then
    begin
      if not UBancos.BancoImplementado(Banco, FLengt, ecnabRetorno) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      Geral.MB_Aviso('N�o foi poss�vel localizar o CNPJ e/ou o ' +
        'c�digo do cedente no arquivo "' + Arquivo + '" onde consta:' + sLineBreak +
        'Banco: "' + Geral.FF0(Banco) + '"' + sLineBreak + 'Sem estes dados n�o � poss�vel '
        + 'definir o cliente interno!');
      Exit;
    end;
  end;
  if (Banco <> 756) then
  begin
    if CNPJ = '' then
    begin
      Geral.MB_Aviso('N�o foi poss�vel obter o CNPJ no arquivo "'
        + Arquivo + '" onde consta:' + sLineBreak +
        'Banco: "' + Geral.FF0(Banco) + '"' + sLineBreak + 'Sem o CNPJ n�o � poss�vel '
        + 'Definir o cliente interno!');
      Exit;
    end;
    ReopenLocEnt1(CNPJ);
    case QrLocEnt1.RecordCount of
      0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
      Arquivo+'" onde consta:' +sLineBreak + 'CNPJ/CPF: "' +
      Geral.FormataCNPJ_TT(CNPJ) + '"' + sLineBreak +
      //'C�digo do cedente: "'+ CodCedente + '"' + sLineBreak +
      'Banco: "' + IntToStr(Banco) + '"'), 'Aviso', MB_OK+MB_ICONWARNING);
      1:
      begin
        (*
        GradeA.Cells[02, linA] := QrLocEnt1NOMEENT.Value;
        GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
        GradeA.Cells[04, linA] := CodCedente;
        GradeA.Cells[09, linA] := IntToStr(QrLocEnt1CLIENTE.Value);
        GradeA.Cells[10, linA] := FormatFloat('000', Banco);
        GradeA.Cells[11, linA] := IntToStr(FLengt);
        *)
        NO_Entid := QrLocEnt1NOMEENT.Value;
        Entidade := QrLocEnt1CLIENTE.Value;
        CNPJ_CPF := Geral.FormataCNPJ_TT(CNPJ);
        Cedente  := CodCedente;  // N�o pode! muda em cada linha!
        BcoArq   := Banco;
        CNAB     := FLengt;
        if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'cnabbcoarq', False, [
        'Entidade', 'NO_Entid', 'CNPJ_CPF', 'Cedente',
        //'Lotes', 'Itens', 'Creditos', Fazer?
        'BcoArq', 'CNAB'], [
        'Controle'], [
        Entidade, NO_Entid, CNPJ_CPF, Cedente,
        //Lotes, Itens, Creditos, Fazer?
        BcoArq, CNAB], [
        Controle], False) then
          Result := True;
      end;
      else
      begin
        Geral.MensagemBox(('Foram localizados '+
        IntToStr(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ)+
        //' e  c�digo de cedente '+ CodCedente +
        '! Para evitar erros nenhum foi '+ ' considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        //
        if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
        begin
          FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt1;
          FmCNAB_DuplCad.ShowModal;
          FmCNAB_DuplCad.Destroy;
        end;
      end;
    end;
  end;
  //

  // Quantidade de lotes no arquivo
  //GradeA.Cells[05, linA] := MLAGeral.CNABRetQtdeLotes(Banco, FLengt, FLista, Arquivo);
  //
  (*
  case BcoCod of
    748: Result := CarregaItensRetornoSicredi(Arquivo,
                   QrCNAB_DirCodigo.Value,
                   Geral.IMV(GradeA.Cells[00, GradeA.RowCount-1]),
                   BcoCod, QrLocEntCLIENTE.Value);
    else
    begin
      Result := False;
    end;
  end;
  *)
end;

function TFmCNAB_RetBco.CarregaItensRetorno(LinA, Banco, CNAB, Controle,
Entidade: Integer; Arquivo: String): Boolean;
{
var
  Extensao, FileName: String;
  BcoCod, Posicoes: Integer;
}
begin
  Result := True;
{
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  if GradeA.Cells[1,1] = '' then
  begin
    Result := False;
    Exit;
  end;
  BcoCod   := Geral.IMV(GradeA.Cells[10, LinA]);
  Posicoes := Geral.IMV(GradeA.Cells[11, LinA]);
}
  //
  if Banco <> QrBcoDirCodigo.Value then
  begin
    Geral.MensagemBox('Banco n�o ' +
    'confere com o banco informado no arquivo CNAB!' + sLineBreak +
    'A pasta � do banco: ' + FormatFloat('000', QrBcoDirCodigo.Value) +
    sLineBreak + 'Banco informado no arquivo: ' + FormatFloat('000', Banco) +
    ' (na linha '+IntToStr(LinA) + ')' + sLineBreak + sLineBreak +
    'SER� CONSIDERADO O BANCO INFORMADO NO ARQUIVO: ' + FormatFloat('000', Banco),
    'Aviso de diverg�ncia!', MB_OK+MB_ICONWARNING);
    //Exit;
  end;
//  Arquivo   := Trim(ExtractFileName(GradeA.Cells[1, LinA]));
  if Arquivo <> '' then
  begin
{
    Extensao  := '';
    FileName := MLAGeral.CaminhoArquivo(QrBcoDirCNABDirMul.Value, Arquivo, Extensao);
}
    CarregaItensRetornoA('', Arquivo, QrBcoDirCodigo.Value,
      (*Geral.IMV(GradeA.Cells[00, LinA]),*) Controle,
      (*Geral.IMV(GradeA.Cells[09, LinA]),*) Entidade, CNAB, Banco);
    VerificaImplementacoes(LinA);
  end else Result := False;
end;

procedure TFmCNAB_RetBco.CarregaItensRetorno240(NomeLayout: String; Banco,
  Entidade, SeqDir, SeqArq: Integer; Arquivo: String);
var
  i: Integer;
  Registro, Segmento,
  SequeReg, ID_Link,  NossoNum, OcorrCod, OcorrDta, SeuNumer, PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutrC, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  OcorrTxt, NumDocum, TxtCBrut, TxtOutrD, VenctDta,
  Msg: String;
  //
  ValTitul, ValAbati, ValDesco, ValEPago, ValJuros, ValMulta, ValOutrC,
  ValJuMul, ValTarif, ValErro, ValCBrut, ValOutrD: Double;
  Motivos, MotivTxt, CNPJ_CPF: String;
  Codigo, Controle, SeqReg: Integer;
  EntCliente, CartCod: Integer;
  AgTxt, DATxt, CCTxt, DCTxt,
  NO_EntClie, CartTxt, Cedente: String;
  JaAvisou: String;
begin
  JaAvisou := '';
  for i := 0 to FLista.Count - 1 do
  begin
    ValErro := 0;
    //ShowMessage(FLista[i]);
    // Tipo de registro
    //LocDadoAll(670, i, '', Registro);
    Registro := Copy(FLista[i], 008, 001);
    //LocDadoAll(671, i, '', Segmento);
    Segmento := Copy(FLista[i], 014, 001);
    if (Registro = '3') and (Segmento = 'T') then
    begin
      inc(FLin1, 1);
      //.Grade1.RowCount := FLin1 + 1;
      //.Grade1.Cells[00, FLin1] := IntToStr(FLin1);

      // ID Link
      Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      IntToStr(i) + '!';
      if not UBancos.LocDado240(Banco, 1000, i, FLista, Msg, NomeLayout, ID_Link) then Exit;
      //.Grade1.Cells[21, FLin1] := ID_Link;

      // Empresa Cliente do cedente
      if (Banco = 008) or (Banco = 033) or (Banco = 353)  then
      begin
        {
        020: Result := 'C�digo da Ag�ncia da Empresa';
        021: Result := 'N�mero da Conta Corrente da Empresa';
        022: Result := 'D�gito verificador Ag�ncia Empresa';
        023: Result := 'D�gito verificador Conta Empresa';
        024: Result := 'D�gito verificador Ag�ncia e Conta Empresa';
        }

        UBancos.LocDado240(Banco, 040, i, FLista, '', NomeLayout, AgTxt);
        UBancos.LocDado240(Banco, 042, i, FLista, '', NomeLayout, DATxt);
        UBancos.LocDado240(Banco, 041, i, FLista, '', NomeLayout, CCTxt);
        UBancos.LocDado240(Banco, 043, i, FLista, '', NomeLayout, DCTxt);

        if (Banco = 008) or (Banco = 033) or (Banco = 353)  then
        begin
          // Tirar zeros insignificativos � esquerda
          CCTxt := Geral.FF0(Geral.IMV(CCTxt));
        end;

        ReopenLocEnt4(Banco, AgTxt, CCTxt, DATxt, DCTxt);
        case QrLocEnt3.RecordCount of
          0: Geral.MB_Aviso(('N�o foi localizado cliente para o arquivo "' +
          Arquivo + '" onde consta:' +sLineBreak + 'Banco: ' +
          Geral.FFN(Banco, 3) + sLineBreak + 'Ag�ncia: ' + AgTxt + sLineBreak +
          'D�gito verificador da ag�ncia: ' + DATxt + sLineBreak +
          'Conta corrente: "' + CCTxt  + '"' + sLineBreak +
          'Digito verificador da conta: ' + DCTxt));
          else
          begin
            if QrLocEnt3.RecordCount > 1 then
            begin
              Geral.MB_Aviso(('Foram localizados '+ IntToStr(
              QrLocEnt3.RecordCount)+' cadastros de clientes para o arquivo "' +
              Arquivo + '" onde consta:' +sLineBreak + 'Banco: ' +
              Geral.FFN(Banco, 3) + sLineBreak + 'Ag�ncia: ' + AgTxt + sLineBreak +
              'D�gito verificador da ag�ncia: ' + DATxt + sLineBreak +
              'Conta corrente: "' + CCTxt + sLineBreak +
              'Digito verificador da conta: ' + DCTxt));
              if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
              begin
                FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt3;
                FmCNAB_DuplCad.ShowModal;
                FmCNAB_DuplCad.Destroy;
              end;
            end;
            NO_EntClie := QrLocEnt3NOMEENT.Value;
            EntCliente := QrLocEnt3CLIENTE.Value;
            CNPJ_CPF   := Geral.FormataCNPJ_TT(QrLocEnt3CNPJ_CPF.Value);
            //Cedente  := CodCedente; // N�o pode! muda em cada linha!
          end;
        end;
      end else
      begin
        // At� agora o banco 341?
        UBancos.LocDado240(Banco, 401, i, FLista, '', NomeLayout, CNPJ_CPF);
        ReopenLocEnt1(CNPJ_CPF);
        EntCliente := 0;
        NO_EntClie := '';
        case QrLocEnt1.RecordCount of
          0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
          Arquivo+'" onde consta:' +sLineBreak + 'CNPJ/CPF: "' +
          Geral.FormataCNPJ_TT(CNPJ_CPF) + '"' + sLineBreak +
          'Banco: "' + IntToStr(Banco) + '"'), 'Aviso', MB_OK+MB_ICONWARNING);
          1:
          begin
            EntCliente := QrLocEnt1CLIENTE.Value;
            NO_EntClie := QrLocEnt1NOMEENT.Value;
          end;
          else
          begin
            Geral.MensagemBox(('Foram localizados '+
            IntToStr(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
            Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ_CPF)+
            '! Para evitar erros nenhum foi '+ ' considerado!'),
            'Aviso', MB_OK+MB_ICONWARNING);
            //
            if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
            begin
              FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt1;
              FmCNAB_DuplCad.ShowModal;
              FmCNAB_DuplCad.Destroy;
            end;
          end;
        end;
      end;

      // Cedente (para descobrir a carteira de cada condom�nio!)
      CartCod := 0;
      CartTxt := 'N�o definida!';
      if (Banco = 008) or (Banco = 033) or (Banco = 353) then
      begin
        //UBancos.LocDado400(Banco, 430, i, FLista, '', Cedente);
        UBancos.LocDado240(Banco, 040, i, FLista, '', NomeLayout, AgTxt);
        UBancos.LocDado240(Banco, 042, i, FLista, '', NomeLayout, DATxt);
        UBancos.LocDado240(Banco, 041, i, FLista, '', NomeLayout, CCTxt);
        UBancos.LocDado240(Banco, 043, i, FLista, '', NomeLayout, DCTxt);

        Cedente := AgTxt + DATxt + CCTxt + DCTxt;

        ReopenLocCar1(Banco, EntCliente, CNPJ_CPF, Cedente);

        case QrLocCar1.RecordCount of
          0:
          begin
            if JaAvisou <> CNPJ_CPF + Cedente then
            begin
              Geral.MensagemBox('N�o foi localizado carteira para: ' + sLineBreak +
              'C�digo da entidade: ' + FormatFloat('0', EntCliente) + sLineBreak +
              'CNPJ/CPF cadastrado na entidade: ' + Geral.FormataCNPJ_TT(CNPJ_CPF) + sLineBreak + sLineBreak +
              'Com os seguintes cadastros na carteira de conta corrente:' + sLineBreak +
              'C�digo do Banco: ' + FormatFloat('0', Banco) + sLineBreak +
              'C�digo do retorno CNAB: ' + Cedente, 'Aviso', MB_OK+MB_ICONWARNING);
              //
              JaAvisou := CNPJ_CPF + Cedente;
            end;
          end;
          1:
          begin
            CartCod := QrLocCar1Codigo.Value;
            CartTxt := QrLocCar1Nome.Value;
          end;
          else
          begin
            Geral.MensagemBox('Foram localizadas '+ IntToStr(
            QrLocCar1.RecordCount) + ' cadastros de carteiras para:' + sLineBreak +
            'Entidade: ' + FormatFloat('0', EntCliente) + sLineBreak +
            'CNPJ/CPF: ' + Geral.FormataCNPJ_TT(CNPJ_CPF) + sLineBreak + sLineBreak +
            'com os seguintes cadastros na carteira de conta corrente:' + sLineBreak +
            'Banco: ' + FormatFloat('0', Banco) + sLineBreak +
            'C�digo retorno CNAB: ' + Cedente, 'Aviso', MB_OK+MB_ICONWARNING);
          end;
        end;
      end;
      // Nosso n�mero -> ID do registro no Sicredi e no Syndic
      UBancos.LocDado240(Banco, 501, i, FLista, '', NomeLayout, NossoNum);
      //.Grade1.Cells[01, FLin1] := NossoNum;

      // Ocorr�ncia
      UBancos.LocDado240(Banco, 504, i, FLista, '', NomeLayout, OcorrCod);
      //.Grade1.Cells[02, FLin1] := OcorrCod;
      // Texto da ocorr�ncia
      OcorrTxt :=
        UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, Geral.IMV(OcorrCod), 240, False, NomeLayout);
      //.Grade1.Cells[03, FLin1] := OcorrTxt;


      // Data da ocorr�ncia
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', NomeLayout, OcorrDta);
      try
        if (OcorrDta = '000000') or (OcorrDta = '00000000') then
        begin
          Geral.MensagemBox(('A data da ocorr�ncia "' +
          OcorrTxt + '" n�o foi definida (' + OcorrDta +') para o bloqueto ' +
          ID_Link + ' !'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end else
        //.Grade1.Cells[04, FLin1] := OcorrDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data da ' +
        'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;

      // Seu Numero
      UBancos.LocDado240(Banco, 506, i, FLista, '', NomeLayout, SeuNumer);
      //.Grade1.Cells[05, FLin1] := SeuNumer;

      // Documento (Texto do n�mero do documento no t�tulo)
      UBancos.LocDado240(Banco, 502, i, FLista, '', NomeLayout, NumDocum);
      //.Grade1.Cells[05, FLin1] := NumDocum;

      // Valor do t�tulo
      UBancos.LocDado240(Banco, 550, i, FLista, '', NomeLayout, TxtTitul);
      //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
      //ValTitul := Geral.DMV(TxtTitul);
      //.Grade1.Cells[06, FLin1] := TxtTitul;

      // Abatimento concedido
      UBancos.LocDado240(Banco, 551, i+1, FLista, '', NomeLayout, TxtAbati);
      //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
      //ValAbati := Geral.DMV(TxtAbati);
      //.Grade1.Cells[07, FLin1] := TxtAbati;

      // Desconto concedido
      UBancos.LocDado240(Banco, 552, i+1, FLista, '', NomeLayout, TxtDesco);
      //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
      //ValDesco := Geral.DMV(TxtDesco);
      //.Grade1.Cells[08, FLin1] := TxtDesco;

      // Valor efetivamente pago
      //UBancos.LocDado240(Banco, 553, i, FLista, '', TxtEPago);
      // Valor Total pago
      UBancos.LocDado240(Banco, 578, i+1, FLista, '', NomeLayout, TxtEPago);
      //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtEPago);
      //.Grade1.Cells[09, FLin1] := TxtEPago;

      // Valor de cr�dito bruto
      UBancos.LocDado240(Banco, 579, i+1, FLista, '', NomeLayout, TxtCBrut);
      //TxtCBrut := dmkPF.XFT(TxtCBrut, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtCBrut);
      //.Grade1.Cells[27, FLin1] := TxtCBrut;

      // Valor de outros d�bitos
      UBancos.LocDado240(Banco, 585, i+1, FLista, '', NomeLayout, TxtOutrD);
      //TxtOutrD := dmkPF.XFT(TxtOutrD, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtOutrD);
      //.Grade1.Cells[28, FLin1] := TxtOutrD;

      //  N�O TEM
      // Juros de mora
      UBancos.LocDado240(Banco, 555, i, FLista, '', NomeLayout, TxtJuros);
      //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
      //ValJuros := Geral.DMV(TxtJuros);
      //.Grade1.Cells[10, FLin1] := TxtJuros;

      //  N�O TEM
      // Multa
      UBancos.LocDado240(Banco, 556, i, FLista, '', NomeLayout, TxtMulta);
      //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtMulta);
      //.Grade1.Cells[11, FLin1] := TxtMulta;

      //  N�O TEM
      // Outros cr�ditos
      UBancos.LocDado240(Banco, 554, i, FLista, '', NomeLayout, TxtOutrC);
      //TxtOutrC := dmkPF.XFT(TxtOutrC, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtOutrC);
      //.Grade1.Cells[12, FLin1] := TxtOutrC;

      // Juros de Mora e Multa (Somados)
      UBancos.LocDado240(Banco, 557, i+1, FLista, '', NomeLayout, TxtJuMul);
      //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
      //.Grade1.Cells[13, FLin1] := TxtJuMul;

      // Tarifa (Despesa) de cobran�a
      UBancos.LocDado240(Banco, 570, i, FLista, '', NomeLayout, TxtTarif);
      //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
      //.Grade1.Cells[14, FLin1] := TxtTarif;

      // ERRO Calculado no final
      //Grade1.Cells[15, FLin1] := ERRO;

      //Parei Aqui
      // Falta fazer (sem pressa)
      {
      // Motivo da ocorrencia
      Grade1.Cells[16, FLin1] := Copy(FLista[i], 319, 010);
      }
      Motivos := '';
      {
      // Texto do motivo da ocorrencia
      Grade1.Cells[17, FLin1] := MLAGeral.CNABMotivosDeTipoDeMovimento28(
        Banco, Copy(FLista[i], 319, 010));
      }
      MotivTxt := '';

      // Data de lancamento na conta corrente
      Msg := 'Data de lan�amento na conta corrente n�o definida!' +
      sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
      ID_Link + ' (Nosso n�mero = ' + NossoNum + ')';
      UBancos.LocDado240(Banco, 581, i+1, FLista, Msg, NomeLayout, PagtoDta);
      try
        // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
        // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
        if PagtoDta = '' then
          Geral.MensagemBox(
          PChar('Data de lan�amento na conta corrente n�o definida!' +
          sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
          ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
          MB_OK+MB_ICONWARNING);
        //.Grade1.Cells[18, FLin1] := PagtoDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
        'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // Data de d�bito da tarifa de cobranca
      //UBancos.LocDado240(Banco, 582, i+1, FLista, '', DtaTarif);
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', NomeLayout, DtaTarif);
      try
        //.Grade1.Cells[19, FLin1] := DtaTarif;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
        'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // C�digo Condom�nio
      //.Grade1.Cells[20, FLin1] := IntToStr(Entidade);

      // Sequencia do diretorio
      //.Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

      // Sequencia do arquivo
      //.Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

      // N�mero sequencial do registro
      UBancos.LocDado240(Banco, 999, i, FLista, '', NomeLayout, SequeReg);
      //.Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

      // Banco
      //.Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);

      // Vencimento
      UBancos.LocDado240(Banco, 580, i+1, FLista, '', NomeLayout, VenctDta);
      try
        //.Grade1.Cells[29, FLin1] := VenctDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data de ' +
        'vencimento. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;





      //Parei Aqui
      // Fazer rejei��o de t�tulos



      ValTitul := Geral.DMV(TxtTitul);
      ValAbati := Geral.DMV(TxtAbati);
      ValDesco := Geral.DMV(TxtDesco);
      ValEPago := Geral.DMV(TxtEPago);
      ValJuros := Geral.DMV(TxtJuros);
      ValMulta := Geral.DMV(TxtMulta);
      ValOutrC := Geral.DMV(TxtOutrC);
      ValJuMul := Geral.DMV(TxtJuMul);
      ValTarif := Geral.DMV(TxtTarif);
      ValCBrut := Geral.DMV(TxtCBrut);
      ValOutrD := Geral.DMV(TxtOutrD);
      ValErro  := ErroLinha(Entidade, Banco, ID_Link, OcorrCod, NossoNum, False,
                  ValTitul, ValAbati, ValDesco, ValEPago, ValJuros, ValMulta,
                  ValJuMul, ValTarif, ValErro, ValOutrC, ValCBrut);
      //
      Codigo   := SeqDir;
      Controle := SeqArq;
      SeqReg   := i;
      OcorrDta := dmkPF.CDS(OcorrDta, 2, 1);
      PagtoDta := dmkPF.CDS(PagtoDta, 2, 1);
      DtaTarif := dmkPF.CDS(DtaTarif, 2, 1);
      VenctDta := dmkPF.CDS(VenctDta, 2, 1);
      {
      FConta    := FConta + 1;
      Conta     := FConta;
      }
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'cnabbcoreg', True, [
      'Codigo', 'Controle', 'CNPJ_CPF',
      'EntCliente', 'NO_EntClie', 'Cedente',
      'CartCod', 'CartTxt', 'NossoNum',
      'OcorrCod', 'OcorrTxt', 'OcorrDta',
      'SeuNumer', 'NumDocum', 'ValTitul',
      'ValAbati', 'ValDesco', 'ValEPago',
      'ValJuros', 'ValMulta', 'ValOutrC',
      'ValJuMul', 'ValTarif', 'ValErro',
      'Motivos', 'MotivTxt', 'PagtoDta',
      'DtaTarif', 'Entidade', 'ID_Link',
      'SeqDir', 'SeqArq', 'SeqReg',
      'Banco', 'ValCBrut', 'ValOutrD',
      'VenctDta'], [
      (*'Conta'*)], [
      Codigo, Controle, CNPJ_CPF,
      EntCliente, NO_EntClie, Cedente,
      CartCod, CartTxt, NossoNum,
      OcorrCod, OcorrTxt, OcorrDta,
      SeuNumer, NumDocum, ValTitul,
      ValAbati, ValDesco, ValEPago,
      ValJuros, ValMulta, ValOutrC,
      ValJuMul, ValTarif, ValErro,
      Motivos, MotivTxt, PagtoDta,
      DtaTarif, Entidade, ID_Link,
      SeqDir, SeqArq, SeqReg,
      Banco, ValCBrut, ValOutrD,
      VenctDta], [
      (*Conta*)], False);
    end;
  end;
end;

procedure TFmCNAB_RetBco.CarregaItensRetorno400(NomeLayout: String; Banco,
  Entidade, SeqDir, SeqArq: Integer; Arquivo: String);
const
  NumDocum = '';
var
  i: Integer;
  SequeReg,
  ID_Link,  NossoNum, OcorrCod, OcorrTxt, OcorrDta, SeuNumer, {Mensagem,} PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutrC, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  VenctDta, Lin, Msg: String;
  //
  ValTitul, ValAbati, ValDesco, ValEPago, ValJuros, ValMulta, ValOutrC,
  ValJuMul, ValTarif, ValErro, ValCBrut, ValOutrD: Double;
  Motivos, MotivTxt, CNPJ_CPF: String;
  Codigo, Controle, Conta, SeqReg: Integer;
  CartCod, EntCliente: Integer;
  CartTxt, AgTxt, CCTxt, DTTxt, NO_EntClie, Cedente: String;
  JaAvisou: String;
begin
  JaAvisou := '';
  if FLista.Count - 2 = 0 then
  begin
    Geral.MensagemBox(PChar('N�o h� dados de transa��es no arquivo selecionado!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  for i := 1 to FLista.Count - 2 do
  begin
    ValErro := 0;
    inc(FLin1, 1);
    //.Grade1.RowCount := FLin1 + 1;
    //.Grade1.Cells[00, FLin1] := IntToStr(FLin1);
    Lin := FLista[i];

    // ID Link
    Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      IntToStr(i) + '!';
    if not UBancos.LocDado400(Banco, 1000, i, FLista, Msg, NomeLayout, ID_Link) then Exit;
    //.Grade1.Cells[21, FLin1] := ID_Link;

    // Empresa Cliente do cedente
    UBancos.LocDado400(Banco, 401, i, FLista, '', NomeLayout, CNPJ_CPF);
    ReopenLocEnt1(CNPJ_CPF);
    EntCliente := 0;
    NO_EntClie := '';
    case QrLocEnt1.RecordCount of
      0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
      Arquivo+'" onde consta:' +sLineBreak + 'CNPJ/CPF: "' +
      Geral.FormataCNPJ_TT(CNPJ_CPF) + '"' + sLineBreak +
      'Banco: "' + IntToStr(Banco) + '"'), 'Aviso', MB_OK+MB_ICONWARNING);
      1:
      begin
        EntCliente := QrLocEnt1CLIENTE.Value;
        NO_EntClie := QrLocEnt1NOMEENT.Value;
      end;
      else
      begin
        Geral.MensagemBox(('Foram localizados '+
        IntToStr(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ_CPF)+
        '! Para evitar erros nenhum foi '+ ' considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        //
        if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
        begin
          FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt1;
          FmCNAB_DuplCad.ShowModal;
          FmCNAB_DuplCad.Destroy;
        end;
      end;
    end;

    // Cedente (para descobrir a carteira de cada condom�nio!)
    CartCod := 0;
    CartTxt := 'N�o definida!';
    if Banco = 341 then
    begin
      UBancos.LocDado400(Banco, 430, i, FLista, '', NomeLayout, Cedente);
      AgTxt := Copy(Cedente, 01, 04);
      CCTxt := Copy(Cedente, 07, 05);
      DTTxt := Copy(Cedente, 12, 01);
      ReopenLocCar1(Banco, EntCliente, CNPJ_CPF, Cedente);
      case QrLocCar1.RecordCount of
        0:
        begin
          if JaAvisou <> CNPJ_CPF + Cedente then
          begin
            Geral.MensagemBox('N�o foi localizado carteira para: ' + sLineBreak +
            'Entidade: ' + FormatFloat('0', EntCliente) + sLineBreak +
            'CNPJ/CPF: ' + Geral.FormataCNPJ_TT(CNPJ_CPF) + sLineBreak + sLineBreak +
            'com os seguintes cadastros na carteira de conta corrente:' + sLineBreak +
            'Banco: ' + FormatFloat('0', Banco) + sLineBreak +
            'C�digo retorno CNAB: ' + Cedente, 'Aviso', MB_OK+MB_ICONWARNING);
            //
            JaAvisou := CNPJ_CPF + Cedente;
          end;
        end;
        1:
        begin
          CartCod := QrLocCar1Codigo.Value;
          CartTxt := QrLocCar1Nome.Value;
        end;
        else
        begin
          Geral.MensagemBox('Foram localizadas '+ IntToStr(
          QrLocCar1.RecordCount) + ' cadastros de carteiras para:' + sLineBreak +
          'Entidade: ' + FormatFloat('0', EntCliente) + sLineBreak +
          'CNPJ/CPF: ' + Geral.FormataCNPJ_TT(CNPJ_CPF) + sLineBreak + sLineBreak +
          'com os seguintes cadastros na carteira de conta corrente:' + sLineBreak +
          'Banco: ' + FormatFloat('0', Banco) + sLineBreak +
          'C�digo retorno CNAB: ' + Cedente, 'Aviso', MB_OK+MB_ICONWARNING);
          {
          if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
          begin
            FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocCar1;
            FmCNAB_DuplCad.ShowModal;
            FmCNAB_DuplCad.Destroy;
          end;
          }
        end;
      end;
    end;

    // Nosso n�mero -> ID do registro no Sicredi e no Syndic
    UBancos.LocDado400(Banco, 501, i, FLista, '', NomeLayout, NossoNum);
    //.Grade1.Cells[01, FLin1] := NossoNum;

    // Ocorr�ncia
    UBancos.LocDado400(Banco, 504, i, FLista, '', NomeLayout, OcorrCod);
    //.Grade1.Cells[02, FLin1] := OcorrCod;

    // Texto da ocorr�ncia
    OcorrTxt := UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno,
                  Geral.IMV(OcorrCod), 400, False, NomeLayout);
    //.Grade1.Cells[03, FLin1] := OcorrTxt;

    // Data da ocorr�ncia
    UBancos.LocDado400(Banco, 505, i, FLista, '', NomeLayout, OcorrDta);
    try
      //.Grade1.Cells[04, FLin1] := OcorrDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data da ' +
      'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
      'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
    end;

    // Seu Numero
    UBancos.LocDado400(Banco, 506, i, FLista, '', NomeLayout, SeuNumer);
    //.Grade1.Cells[05, FLin1] := SeuNumer;

    // Valor do t�tulo
    UBancos.LocDado400(Banco, 550, i, FLista, '', NomeLayout, TxtTitul);
    //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
    //ValTitul := Geral.DMV(TxtTitul);
    //.Grade1.Cells[06, FLin1] := TxtTitul;

    // Abatimento concedido
    UBancos.LocDado400(Banco, 551, i, FLista, '', NomeLayout, TxtAbati);
    //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
    //ValAbati := Geral.DMV(TxtAbati);
    //.Grade1.Cells[07, FLin1] := TxtAbati;

    // Desconto concedido
    UBancos.LocDado400(Banco, 552, i, FLista, '', NomeLayout, TxtDesco);
    //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
    //ValDesco := Geral.DMV(TxtDesco);
    //.Grade1.Cells[08, FLin1] := TxtDesco;

    // Valor efetivamente pago
    UBancos.LocDado400(Banco, 553, i, FLista, '', NomeLayout, TxtEPago);
    //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
    //ValEPago := Geral.DMV(TxtEPago);
    //.Grade1.Cells[09, FLin1] := TxtEPago;

    // Juros de mora
    UBancos.LocDado400(Banco, 555, i, FLista, '', NomeLayout, TxtJuros);
    //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
    //ValJuros := Geral.DMV(TxtJuros);
    //.Grade1.Cells[10, FLin1] := TxtJuros;

    // Multa
    UBancos.LocDado400(Banco, 556, i, FLista, '', NomeLayout, TxtMulta);
    //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtMulta);
    //.Grade1.Cells[11, FLin1] := TxtMulta;

    // Outros cr�ditos
    UBancos.LocDado400(Banco, 554, i, FLista, '', NomeLayout, TxtOutrC);
    //TxtOutrC := dmkPF.XFT(TxtOutrC, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtOutrC);
    //.Grade1.Cells[12, FLin1] := TxtOutrC;

    // Juros de Mora e Multa (Somados)
    UBancos.LocDado400(Banco, 557, i, FLista, '', NomeLayout, TxtJuMul);
    //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
    //.Grade1.Cells[13, FLin1] := TxtJuMul;

    // Tarifa (Despesa) de cobran�a
    UBancos.LocDado400(Banco, 570, i, FLista, '', NomeLayout, TxtTarif);
    //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
    //.Grade1.Cells[14, FLin1] := TxtTarif;

    // ERRO Calculado no final
    //Grade1.Cells[15, FLin1] := ERRO;

    //Parei Aqui
    // Falta fazer (sem pressa)
    {
    // Motivo da ocorrencia
    Grade1.Cells[16, FLin1] := Copy(i, FLista, 319, 010);
    // Texto do motivo da ocorrencia
    Grade1.Cells[17, FLin1] := MLAGeral.CNABMotivosDeTipoDeMovimento28(
      Banco, Copy(i, FLista, 319, 010));
    }

    // Motivos
    UBancos.LocDado400(Banco, 530, i, FLista, '', NomeLayout, Motivos);
    //.Grade1.Cells[16, FLin1] := Motivos;
    //
    MotivTxt := '';

    // Data de lancamento na conta corrente
    UBancos.LocDado400(Banco, 581, i, FLista, 'Data de lan�amento na conta corrente n�o definida!' +
    sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
    ID_Link + ' (Nosso n�mero = ' + NossoNum + ')', NomeLayout, PagtoDta);
    try
      // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
      // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
      if PagtoDta = '' then
        Geral.MensagemBox(
        PChar('Data de lan�amento na conta corrente n�o definida!' +
        sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
        ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
        MB_OK+MB_ICONWARNING);
      //.Grade1.Cells[18, FLin1] := PagtoDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
      'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // Data de d�bito da tarifa de cobranca
    UBancos.LocDado400(Banco, 582, i, FLista, '', NomeLayout, DtaTarif);
    try
      //.Grade1.Cells[19, FLin1] := DtaTarif;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
      'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // C�digo Condom�nio
    //.Grade1.Cells[20, FLin1] := IntToStr(Entidade);

    // Sequencia do diretorio
    //.Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

    // Sequencia do arquivo
    //.Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

    // N�mero sequencial do registro
    UBancos.LocDado400(Banco, 999, i, FLista, '', NomeLayout, SequeReg);
    //.Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

    // Banco
    //.Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);


    // Data de vencimento
    UBancos.LocDado400(Banco, 580, i, FLista, '', NomeLayout, VenctDta);
    try
      //.Grade1.Cells[29, FLin1] := VenctDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data de ' +
      'vencimento. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    //Parei Aqui
    // Fazer rejei��o de t�tulos


      ValTitul := Geral.DMV(TxtTitul);
      ValAbati := Geral.DMV(TxtAbati);
      ValDesco := Geral.DMV(TxtDesco);
      ValEPago := Geral.DMV(TxtEPago);
      ValJuros := Geral.DMV(TxtJuros);
      ValMulta := Geral.DMV(TxtMulta);
      ValOutrC := Geral.DMV(TxtOutrC);
      ValJuMul := Geral.DMV(TxtJuMul);
      ValTarif := Geral.DMV(TxtTarif);
      ValCBrut := 0; //Geral.DMV(TxtCBrut);
      ValOutrD := 0; //Geral.DMV(TxtOutrD);

      ValErro  := ErroLinha(Entidade, Banco, ID_Link, OcorrCod, NossoNum, False,
                  ValTitul, ValAbati, ValDesco, ValEPago, ValJuros, ValMulta,
                  ValJuMul, ValTarif, ValErro, ValOutrC, ValCBrut);
      //
      Codigo   := SeqDir;
      Controle := SeqArq;
      SeqReg   := i;
      OcorrDta := dmkPF.CDS(OcorrDta, 2, 1);
      PagtoDta := dmkPF.CDS(PagtoDta, 2, 1);
      DtaTarif := dmkPF.CDS(DtaTarif, 2, 1);
      VenctDta := dmkPF.CDS(VenctDta, 2, 1);
      {
      FConta    := FConta + 1;
      Conta     := FConta;
      }
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'cnabbcoreg', True, [
      'Codigo', 'Controle', 'CNPJ_CPF',
      'EntCliente', 'NO_EntClie', 'Cedente',
      'CartCod', 'CartTxt', 'NossoNum',
      'OcorrCod', 'OcorrTxt', 'OcorrDta',
      'SeuNumer', 'NumDocum', 'ValTitul',
      'ValAbati', 'ValDesco', 'ValEPago',
      'ValJuros', 'ValMulta', 'ValOutrC',
      'ValJuMul', 'ValTarif', 'ValErro',
      'Motivos', 'MotivTxt', 'PagtoDta',
      'DtaTarif', 'Entidade', 'ID_Link',
      'SeqDir', 'SeqArq', 'SeqReg',
      'Banco', 'ValCBrut', 'ValOutrD',
      'VenctDta'], [
      (*'Conta'*)], [
      Codigo, Controle, CNPJ_CPF,
      EntCliente, NO_EntClie, Cedente,
      CartCod, CartTxt, NossoNum,
      OcorrCod, OcorrTxt, OcorrDta,
      SeuNumer, NumDocum, ValTitul,
      ValAbati, ValDesco, ValEPago,
      ValJuros, ValMulta, ValOutrC,
      ValJuMul, ValTarif, ValErro,
      Motivos, MotivTxt, PagtoDta,
      DtaTarif, Entidade, ID_Link,
      SeqDir, SeqArq, SeqReg,
      Banco, ValCBrut, ValOutrD,
      VenctDta], [
      (*Conta*)], False);
  end;
end;

function TFmCNAB_RetBco.CarregaItensRetornoA(NomeLayout, Arquivo: String;
  SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  //
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  BtCarrega.Enabled := FLista.Count > 0;
  //.MyObjects.LimpaGrade(Grade1, 1, 1, True);
  //
  FLin1 := 0;
  //
  Memo2.Lines.Clear;
  Memo2.Lines.Add('Campo Linha Pos Tam Valor');
  //
  if not UBancos.BancoImplementado(Banco, TamReg, ecnabRetorno) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  {
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := BcoUse;
  UMyMod.AbreQuery(QrBanco);
  if TamReg = 400 then
  begin
    if (QrBancoID_400i.Value = 0) or (QrBancoID_400t.Value = 0) then
    begin
      Geral.MensagemBox(('N�o foi definido o identificador de ' +
      'cobran�a CNAB400 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end else begin
    if (QrBancoID_240i.Value = 0) or (QrBancoID_240t.Value = 0) then
    begin
      Geral.MensagemBox(('N�o foi definido o identificador de ' +
      'cobran�a CNAB240 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  //
  }
  {
  QrCampos.Close;
  QrCampos.Params[0].AsInteger := BcoUse;
  QrCampos.SQL[10] := 'AND T'+FormatFloat('0', TamReg)+'=1';
  UMyMod.AbreQuery(QrCampos)
  if QrCampos.RecordCount = 0 then
  begin
    Geral.MensagemBox(('N�o h� nenhum tipo de campo de registro '+
    'detalhe definido para o arquivo "'+ Arquivo + '" do banco '+
    FormatFloat('000', BcoUse) + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  }
  case TamReg of
    240: CarregaItensRetorno240(NomeLayout, Banco, Entidade, SeqDir, SeqArq, Arquivo);
    //240: CarregaArquivo240('?', 0);
    400: CarregaItensRetorno400(NomeLayout, Banco, Entidade, SeqDir, SeqArq, Arquivo);
    else Geral.MensagemBox(('Carregamento de itens de retorno n�o ' +
    'implementado para arquivo contendo ' + IntToStr(TamReg) + ' posi��es!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crDefault;
  {
  if nc > 0 then Geral.MensagemBox(('Existem '+IntToStr(nc)+
  ' registros detalhe que n�o s�o cobran�a sem registro e foram DESCONSIDERADOS '+
  'na leitura do arquivo "'+Arquivo+'"!'), 'Aviso', MB_OK+MB_ICONWARNING);
  }
end;

procedure TFmCNAB_RetBco.DBGBcoDirDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'DirExiste' then
  begin
    if (QrBcoDirDirExiste.Value = 'Sim') then
      Cor := clBlue
    else
      Cor := clRed;
    //
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGBcoDir), Rect,
      Cor, clWindow, Column.Alignment, Column.Field.DisplayText);
  end;
end;

function TFmCNAB_RetBco.ErroLinha(Entidade, Banco: Integer;
ID_Link, OcorrCod, NossoNum: String; Avisa: Boolean;
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta,
  ValJuMul, ValTarif, ValErro, ValOutro, ValReceb: Double): Double;
var
  //
  DefErro: Boolean;
begin
  DefErro := False;
  if Geral.IMV(OcorrCod) = 6 then // Baixa simples
  begin
    if ValReceb > 0 then
      ValErro := ValPago - ValTarif - ValReceb
    else
      ValErro := ValTitul - ValAbati - ValDesco + ValJuros + ValMulta
               + ValOutro + ValJuMul - ValTarif - ValPago;
    if Banco = 409 then ValErro := ValErro + ValTarif;
    //
    if (ValErro < 0.01) and (ValErro > -0.01) then
      ValErro := 0;
    //
    if Avisa then
    begin
      if DefErro then
      begin
        ReopenQrPesq2(Geral.IMV(ID_Link), Entidade);
        //
        if QrPesq2.RecordCount > 0 then
        begin
          Geral.MensagemBox(('O documento com "Nosso n�m." = ' +
          NossoNum + ' foi localizado, mas tem diverg�ncias nos ' +
          'dados informados!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          Geral.MensagemBox(('O documento com "Nosso n�m." = ' +
          NossoNum + ' tem diverg�ncias nos dados informados, e n�o ' +
          'foi localizado nos bloquetos emitidos!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    Result := ValErro;
  end else Result := 0;
end;

procedure TFmCNAB_RetBco.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_RetBco.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrBcoDir.Database := DModG.MyPID_DB;
  QrBcoArq.Database := DModG.MyPID_DB;
  QrBcoReg.Database := DModG.MyPID_DB;
  QrErros.Database  := DModG.MyPID_DB;
  //
  PageControl1.ActivePageIndex := 0;
  //PageControl2.ActivePageIndex := 0;
  //PageControl4.ActivePageIndex := 0;
  //
  FLista := TStringList.Create;
  FLengt := 0;
  //
  PnArquivos.Align := alClient;
{
  GradeA.ColWidths[00] := 032;
  GradeA.ColWidths[01] := 160;
  GradeA.ColWidths[02] := 272;
  GradeA.ColWidths[03] := 112;
  GradeA.ColWidths[04] := 104;
  GradeA.ColWidths[05] := 044;
  GradeA.ColWidths[06] := 036;
  GradeA.ColWidths[07] := 028;
  GradeA.ColWidths[08] := 064;
  GradeA.ColWidths[09] := 040;
  GradeA.ColWidths[10] := 028;
  GradeA.ColWidths[11] := 036;
  //
  GradeA.Cells[00,00] := 'Seq';
  GradeA.Cells[01,00] := 'Arquivo';
  GradeA.Cells[02,00] := 'Cliente interno';
  GradeA.Cells[03,00] := 'CNPJ / CPF';
  GradeA.Cells[04,00] := 'Cedente';
  GradeA.Cells[05,00] := 'Lotes';
  GradeA.Cells[06,00] := 'Itens';
  GradeA.Cells[07,00] := 'Dir';
  GradeA.Cells[08,00] := 'Cr�ditos';
  GradeA.Cells[09,00] := 'Cliente';
  GradeA.Cells[10,00] := 'Bco';
  GradeA.Cells[11,00] := 'CNAB';
  //
  Grade1.ColWidths[00] := 032;
  Grade1.ColWidths[01] := 108;
  Grade1.ColWidths[02] := 024;
  Grade1.ColWidths[03] := 180;
  Grade1.ColWidths[04] := 056;
  Grade1.ColWidths[05] := 072;
  Grade1.ColWidths[06] := 072;
  Grade1.ColWidths[07] := 064;
  Grade1.ColWidths[08] := 064;
  Grade1.ColWidths[09] := 072;
  Grade1.ColWidths[10] := 064;
  Grade1.ColWidths[11] := 064;
  Grade1.ColWidths[12] := 064;
  Grade1.ColWidths[13] := 064;
  Grade1.ColWidths[14] := 064;
  Grade1.ColWidths[15] := 064;
  Grade1.ColWidths[16] := 072;
  Grade1.ColWidths[17] := 120;
  Grade1.ColWidths[18] := 056;
  Grade1.ColWidths[19] := 056;
  Grade1.ColWidths[20] := 064;
  Grade1.ColWidths[21] := 072;
  Grade1.ColWidths[22] := 024;
  Grade1.ColWidths[23] := 024;
  Grade1.ColWidths[24] := 024;
  Grade1.ColWidths[25] := 024;
  Grade1.ColWidths[26] := 108;
  Grade1.ColWidths[27] := 064;
  Grade1.ColWidths[28] := 064;
  Grade1.ColWidths[29] := 056;
  /////////////
  Grade1.Cells[00, 00] := 'Seq';
  Grade1.Cells[01, 00] := 'Nosso n�m.';
  Grade1.Cells[02, 00] := 'Ocorr�ncia';
  Grade1.Cells[03, 00] := 'Descri��o da ocorr�ncia';
  Grade1.Cells[04, 00] := 'Data ocor.';
  Grade1.Cells[05, 00] := 'Seu n�mero';
  Grade1.Cells[06, 00] := 'Val. t�tulo';
  Grade1.Cells[07, 00] := 'Abatimento';
  Grade1.Cells[08, 00] := 'Desconto';
  Grade1.Cells[09, 00] := 'Val. receb.';
  Grade1.Cells[10, 00] := 'Juros mora';
  Grade1.Cells[11, 00] := 'Multa';
  Grade1.Cells[12, 00] := 'Outros +';
  Grade1.Cells[13, 00] := 'Jur + Mul.';
  Grade1.Cells[14, 00] := 'Tarifa';
  Grade1.Cells[15, 00] := 'ERRO';
  Grade1.Cells[16, 00] := 'M.O.';
  Grade1.Cells[17, 00] := 'Motivos ocorr�ncia';
  Grade1.Cells[18, 00] := 'Dt.lanc.c/c';
  Grade1.Cells[19, 00] := 'Dt.D�b.tarifa';
  Grade1.Cells[20, 00] := 'Entidade';
  Grade1.Cells[21, 00] := 'ID Link';
  Grade1.Cells[22, 00] := 'Dir';
  Grade1.Cells[23, 00] := 'Arq';
  Grade1.Cells[24, 00] := 'Item';
  Grade1.Cells[25, 00] := 'Bco';
  Grade1.Cells[26, 00] := 'Documento';
  Grade1.Cells[27, 00] := 'Val. Bruto';
  Grade1.Cells[28, 00] := 'Outros -';
  Grade1.Cells[29, 00] := 'Vencto';
}
end;

procedure TFmCNAB_RetBco.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmCNAB_RetBco.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmCNAB_RetBco.GravaItens(): Integer;
const
  NomeLayout = '';
var
  ID_Link, Codigo, Banco, SeuNum, OcorrCodi, Diretorio, ItemArq,
  i, n, a, IDNum, Entidade, Carteira, Dias, Erros, CliInt, TamReg: Integer;
  OcorrData, QuitaData, Motivo1, Motivo2, Motivo3, Motivo4, Motivo5, Arquivo,
  Extensao, FileName, DestName, NossoNum, DtaTarif: String;
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta, DevJuros, DevMulta,
  TotJuros, ValJuMul, ValTarif, ValErro, ValOutro, TxaMulta, TxaJuros, JurMul: Double;
  DataPagto: TDateTime;
  Exclui, Continua, InfoJM, SeparaJM: Boolean;
  Texto, CtrlTxt: String;
begin
  Result := 0;
  //tarifa de cobran�a -
  //fazer c�lculo de verifica��o de Val tit + jur + mul - tarifa - desco - abat = val pago
  QrSemCart.Close;
  QrSemcart.Database := DModG.MyPID_DB;
  QrSemCart.Params[0].AsInteger := QrBcoArqControle.Value;
  QrSemCart.Open;
  if QrSemCart.RecordCount > 0 then
  begin
    Geral.MensagemBox('Existem ' + IntToStr(QrSemCart.RecordCount) +
    ' registros sem carteira de conta corrente definida no arquivo "' +
    QrBcoArqArquivo.Value + '".' + sLineBreak + 'Carregamento cancelado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  //Erros := 0;
  QrErros.Close;
  QrErros.Database := DModG.MyPID_DB;
  QrErros.Params[0].AsInteger := QrBcoArqControle.Value;
  QrErros.Open;
  (*
  for i := 1 to Grade1.RowCount - 1 do
  begin
    Banco := Geral.IMV(Grade1.Cells[25, i]);
    if ErroLinha(Banco, i, True) then Inc(Erros, 1);
  end;
  if Erros > 0 then
  *)
  Erros := QrErros.RecordCount;
  if Erros > 0 then
  begin
    if Geral.MensagemBox(('Existem ' + IntToStr(Erros) +
    ' diverg�ncias no arquivo "' + QrBcoArqArquivo.Value + '".' + sLineBreak +
    'Como � calculado a diverg�ncia:' + sLineBreak +
    'Valor do t�tulo + Multa + Juros de mora + (Juros e multa somados) + ' +
    'Outros cr�ditos - Abatimentos - Descontos - Despesa de cobran�a (Tarifa) - ' +
    'Valor a creditar em conta corrente' + sLineBreak +
    'Deseja continuar assim mesmo?'), 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Result := 0;
      Exit;
    end;
  end;
  Exclui   := True;
  Codigo   := 0;
  Result   := 0;
  Dias     := 0;
  {
  DevJuros := 0;
  DevMulta := 0;
  }
{
  n        := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    //  pode vir com letra "P" no final no caso do Banco do Brasil
    Texto := Geral.SoNumero_TT(Trim(Grade1.Cells[1, i]));
    if Geral.DMV(Texto) >= 1 then
      n := n + 1;
  end;
  if n = 0 then
}
  if QrBcoReg.RecordCount = 0 then
  begin
    Geral.MensagemBox('N�o h� itens a serem gravados. O arquivo ser� ' +
    'movido para a pasta "Vazios"!', 'Aviso', MB_OK+MB_ICONWARNING);
{
    Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
    Extensao  := '';
}
    Arquivo   := QrBcoArqArquivo.Value;
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrBcoDirCNABDirMul.Value, Arquivo, Extensao);
    if FileExists(FileName) then
    begin
      DestName := dmkPF.CaminhoArquivo(QrBcoDirCNABDirMul.Value, 'Vazios', '');
      ForceDirectories(DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
    end;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible := True;
{
  PB1.Max := Grade1.RowCount - 1 + GradeA.RowCount - 1;
  TamReg := Geral.IMV(GradeA.Cells[11, Geral.IMV(Grade1.Cells[23, 1])]);//FActiveRowA]);
  for a := 1 to Grade1.RowCount - 1 do
}
  PB1.Max := QrBcoReg.RecordCount;
  TamReg := QrBcoArqCNAB.Value;
   QrBcoReg.First;
  while not QrBcoReg.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    //NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, a]);
    NossoNum  := QrBcoRegNossoNum.Value;
    //OcorrCodi := Geral.IMV(Grade1.Cells[02, a]);
    OcorrCodi := QrBcoRegOcorrCod.Value;
    // Texto da ocorrencia                [03
    //OcorrData := dmkPF.CDS(Grade1.Cells[04, a], 2, 1);
    OcorrData := Geral.FDT(QrBcoRegOcorrDta.Value, 1);
    //SeuNum    := Geral.IMV(Grade1.Cells[05, a]);
    SeuNum  := Trunc(QrBcoRegSeuNumer.Value);
    //ValTitul  := Geral.DMV(Grade1.Cells[06, a]);
    ValTitul := QrBcoRegValTitul.Value;
    //ValAbati  := Geral.DMV(Grade1.Cells[07, a]);
    ValAbati := QrBcoRegValAbati.Value;
    //ValDesco  := Geral.DMV(Grade1.Cells[08, a]);
    ValDesco := QrBcoRegValDesco.Value;
    //ValPago   := Geral.DMV(Grade1.Cells[09, a]);
    ValPago  := QrBcoRegValEPago.Value;
    //ValJuros  := Geral.DMV(Grade1.Cells[10, a]);
    ValJuros := QrBcoRegValJuros.Value;
    //ValMulta  := Geral.DMV(Grade1.Cells[11, a]);
    ValMulta := QrBcoRegValMulta.Value;
    //ValOutro  := Geral.DMV(Grade1.Cells[12, a]);
    ValOutro := QrBcoRegValOutrC.Value;
    //ValJuMul  := Geral.DMV(Grade1.Cells[13, a]);
    ValJuMul := QrBcoRegValJuMul.Value;
    //ValTarif  := Geral.DMV(Grade1.Cells[14, a]);
    ValTarif := QrBcoRegValTarif.Value;
    //Motivo1   := Copy(Grade1.Cells[16, a], 1, 2);
    Motivo1  := Copy(QrBcoRegMotivos.Value, 1, 2);
    //Motivo2   := Copy(Grade1.Cells[16, a], 3, 2);
    Motivo2  := Copy(QrBcoRegMotivos.Value, 3, 2);
    //Motivo3   := Copy(Grade1.Cells[16, a], 5, 2);
    Motivo3  := Copy(QrBcoRegMotivos.Value, 5, 2);
    //Motivo4   := Copy(Grade1.Cells[16, a], 7, 2);
    Motivo4  := Copy(QrBcoRegMotivos.Value, 7, 2);
    //Motivo5   := Copy(Grade1.Cells[16, a], 9, 2);
    Motivo5  := Copy(QrBcoRegMotivos.Value, 9, 2);
    // Texto dos motivos da ocorrencia    [13
    //QuitaData := dmkPF.CDS(Grade1.Cells[18, a], 2, 1);
    QuitaData := Geral.FDT(QrBcoRegPagtoDta.Value, 1);
    //DtaTarif  := dmkPF.CDS(Grade1.Cells[19, a], 2, 1);
    DtaTarif := Geral.FDT(QrBcoRegDtaTarif.Value, 1);
    //Diretorio := Geral.IMV(Grade1.Cells[22, a]);
    Diretorio := QrBcoRegSeqDir.Value;
    //Arquivo   := ExtractFileName(GradeA.Cells[1, StrToInt(Grade1.Cells[22, a])]);
    Arquivo := QrBcoArqArquivo.Value;
    //ItemArq   := Geral.IMV(Grade1.Cells[24, a]);
    ItemArq := QrBcoRegSeqReg.Value;
    //Banco     := Geral.IMV(Grade1.Cells[25, a]);
    Banco := QrBcoRegBanco.Value;
    //Entidade  := Geral.IMV(Grade1.Cells[20, a]);
    Entidade := QrBcoRegEntCliente.Value;
    //ID_Link   := Geral.IMV(Grade1.Cells[21, a]);
    ID_Link := Geral.IMV(QrBcoRegID_Link.Value);
    //Carteira  := QrCNAB_DirCarteira.Value;
    Carteira  := QrBcoRegCartCod.Value;
    //
    if not UBancos.SeparaJurosEMultaImplementado(Banco, NomeLayout, SeparaJM) then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    if not SeparaJM then
    begin
      // N�o separa Juros e multa no arquivo.
      // Fazer manual
      if (ValPago > ValTitul) and (ValJuros + ValMulta = 0) then
      begin
        if DModG.DadosRetDeEntidade(Entidade, CliInt) then
        begin
          ValMulta := Round(DModG.QrLocCIPercMulta.Value * ValTitul) / 100;
          JurMul := ValPago - ValTitul;
          if ValMulta > JurMul then
          begin
            ValMulta := JurMul;
            ValJuros := 0;
          end else begin
            ValJuros := JurMul - ValMulta;
            if ValJuros < 0 then
            begin
              // precau��o
              ValMulta := ValMulta + ValJuros;
              ValJuros := 0;
            end;
          end;
        end;
      end;
    end;
    Continua := UBancos.BancoTemEntidade(Banco);
    if not Continua then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, NomeLayout);
    if not Continua then
    begin
      Continua := OcorrCodi = UBancos.CodigoTarifa(Banco, NomeLayout);
      // Tentativa de eliminar a tabela BancosLei
      (*
      QrBcocor.Close;
      QrBcocor.Params[00].AsInteger := Banco;
      QrBcocor.Params[01].AsString  := Grade1.Cells[02, a];
      UMyMod.AbreQuery(QrBcocor)

      if QrBcocor.RecordCount = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end else Continua := QrBcocorCarrega.Value <> 0;
      *)
      if not Continua then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end;
    end;
    if Continua then
    begin
      IDNum := ID_Link;
      //
      if IDNum = 0 then
      begin
        Exclui := False;
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Banco ' +
          FormatFloat('000', Banco) + ' - n�o implementado) > (ID_Link = 0)');
        Application.ProcessMessages;
      end else begin
        QrDupl.Close;
        QrDupl.Params[00].AsInteger := Banco;
        QrDupl.Params[01].AsString  := NossoNum;
        QrDupl.Params[02].AsInteger := SeuNum;
        QrDupl.Params[03].AsInteger := OcorrCodi;
        QrDupl.Params[04].AsString  := Arquivo;
        QrDupl.Params[05].AsInteger := ItemArq;
        QrDupl.Params[06].AsFloat   := ValTitul;
        QrDupl.Params[07].AsFloat   := ValPago;
        QrDupl.Params[08].AsString  := QuitaData;
        UMyMod.AbreQuery(QrDupl, Dmod.MyDB, 'TFmCNAB_Ret2.GravaItens()');
        if QrDupl.RecordCount > 0 then
        begin
          if CkReverter.Checked then
          begin
            Dmod.QrUpd2.SQL.Clear;
            Dmod.QrUpd2.SQL.Add('UPDATE cnab_lei SET Step=0 WHERE Codigo=:P0');
            Dmod.QrUpd2.Params[0].AsInteger := QrDuplCodigo.Value;
            Dmod.QrUpd2.ExecSQL;
          end else begin
            PageControl1.ActivePageIndex := 1;
            Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
              ' do arquivo '+Arquivo+' j� foi registrado anteriormente!');
            Application.ProcessMessages;
          end;
        end else
        begin
          Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB,
            'Livres', 'Controle', 'CNAB_Lei', 'CNAB_Lei', 'Codigo');
          //
          DevJuros := 0;
          DevMulta := 0;
          Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, NomeLayout);
          if Continua then
          begin
            ReopenSQL3(ID_Link, Entidade);
            //DataPagto := Geral.ValidaDataSimples(Grade1.Cells[04, a], True);
            DataPagto := QrBcoRegOcorrDta.Value;
            if QrPesq3Vencimento.Value < DataPagto then
              Dias := UMyMod.DiasUteis(QrPesq3Vencimento.Value + 1, DataPagto);
            if Dias > 0 then
            begin
              TotJuros := dmkPF.CalculaJuroSimples(QrPesq3PercJuros.Value,
              DataPagto - QrPesq3Vencimento.Value);
              DevJuros := Round(TotJuros * QrPesq3Credito.Value) / 100;
              DevMulta := Round(QrPesq3PercMulta.Value * QrPesq3Credito.Value) / 100;
            end;
          end;
          //
          // Dve ser aqui para evitar erros
          if Continua then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('INSERT INTO cnab_lei SET ');
            Dmod.QrUpd.SQL.Add('Codigo=:P0, Banco=:P1, NossoNum=:P2, SeuNum=:P3, ');
            Dmod.QrUpd.SQL.Add('OcorrCodi=:P4, OcorrData=:P5, ValTitul=:P6, ');
            Dmod.QrUpd.SQL.Add('ValAbati=:P7, ValDesco=:P8, ValPago=:P9, ');
            Dmod.QrUpd.SQL.Add('ValJuros=:P10, ValMulta=:P11, Motivo1=:P12, ');
            Dmod.QrUpd.SQL.Add('Motivo2=:P13, Motivo3=:P14, Motivo4=:P15, ');
            Dmod.QrUpd.SQL.Add('Motivo5=:P16, QuitaData=:P17, Diretorio=:P18, ');
            Dmod.QrUpd.SQL.Add('Arquivo=:P19, ItemArq=:P20, IDNum=:P21, Entidade=:P22, ');
            Dmod.QrUpd.SQL.Add('Carteira=:P23, DevJuros=:P24, DevMulta=:P25, ');
            Dmod.QrUpd.SQL.Add('ID_Link=:P26, ValJuMul=:P27, ValTarif=:P28, ');
            Dmod.QrUpd.SQL.Add('ValOutro=:P29, DtaTarif=:P30, TamReg=:P31');
            //
            Dmod.QrUpd.Params[00].AsInteger := Codigo;
            Dmod.QrUpd.Params[01].AsInteger := Banco;
            Dmod.QrUpd.Params[02].AsString  := NossoNum;
            Dmod.QrUpd.Params[03].AsInteger := SeuNum;
            Dmod.QrUpd.Params[04].AsInteger := OcorrCodi;
            Dmod.QrUpd.Params[05].AsString  := OcorrData;
            Dmod.QrUpd.Params[06].AsFloat   := ValTitul;
            Dmod.QrUpd.Params[07].AsFloat   := ValAbati;
            Dmod.QrUpd.Params[08].AsFloat   := ValDesco;
            Dmod.QrUpd.Params[09].AsFloat   := ValPago;
            Dmod.QrUpd.Params[10].AsFloat   := ValJuros;
            Dmod.QrUpd.Params[11].AsFloat   := ValMulta;
            Dmod.QrUpd.Params[12].AsString  := Motivo1;
            Dmod.QrUpd.Params[13].AsString  := Motivo2;
            Dmod.QrUpd.Params[14].AsString  := Motivo3;
            Dmod.QrUpd.Params[15].AsString  := Motivo4;
            Dmod.QrUpd.Params[16].AsString  := Motivo5;
            Dmod.QrUpd.Params[17].AsString  := QuitaData;
            Dmod.QrUpd.Params[18].AsInteger := Diretorio;
            Dmod.QrUpd.Params[19].AsString  := Arquivo;
            Dmod.QrUpd.Params[20].AsInteger := ItemArq;
            Dmod.QrUpd.Params[21].AsInteger := IDNum;
            Dmod.QrUpd.Params[22].AsInteger := Entidade;
            Dmod.QrUpd.Params[23].AsInteger := Carteira;
            Dmod.QrUpd.Params[24].AsFloat   := DevJuros;
            Dmod.QrUpd.Params[25].AsFloat   := DevMulta;
            Dmod.QrUpd.Params[26].AsInteger := ID_Link;
            Dmod.QrUpd.Params[27].AsFloat   := ValJuMul;
            Dmod.QrUpd.Params[28].AsFloat   := ValTarif;
            Dmod.QrUpd.Params[29].AsFloat   := ValOutro;
            Dmod.QrUpd.Params[30].AsString  := DtaTarif;
            Dmod.QrUpd.Params[31].AsInteger := TamReg;
            Dmod.QrUpd.ExecSQL;
          end;
        end;
      end;
    end;
    QrBcoReg.Next;
  end;

  //

{
  Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
  Extensao  := '';
  FileName := MLAGeral.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
  if Exclui then
  begin
    if FileExists(FileName) then
    begin
      //ShowMessage('Arquivo: '+FileName);
      DestName := MLAGeral.CaminhoArquivo(QrCNAB_DirNome.Value, 'Lidos', '');
      ForceDirectories(DestName);
      //ShowMessage('Destino: '+DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
      //
    end;
  end;
}
  if Exclui then
  begin
    Arquivo   := QrBcoArqArquivo.Value;
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrBcoDirCNABDirMul.Value, Arquivo, Extensao);
    if FileExists(FileName) then
    begin
      DestName := dmkPF.CaminhoArquivo(QrBcoDirCNABDirMul.Value, 'Lidos', '');
      ForceDirectories(DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
      if not FileExists(FileName) then
      begin
        CtrlTxt := FormatFloat('0', QrBcoArqControle.Value);
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('DELETE FROM cnabbcoreg');
        DModG.QrUpdPID1.SQL.Add('WHERE Controle = ' + CtrlTxt);
        DModG.QrUpdPID1.ExecSQL;
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('DELETE FROM cnabbcoarq');
        DModG.QrUpdPID1.SQL.Add('WHERE Controle = ' + CtrlTxt);
        DModG.QrUpdPID1.ExecSQL;
        //
      end;
    end;
  end;
  //

  PB1.Visible := False;
  Screen.Cursor := crDefault;
  Result := Codigo;
  //
  ReopenBcoArq(0);
  if QrBcoArq.RecordCount = 0 then
  begin
    CtrlTxt := FormatFloat('0', QrBcoDirCodigo.Value);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('DELETE FROM cnabbcodir');
    DModG.QrUpdPID1.SQL.Add('WHERE Codigo = ' + CtrlTxt);
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenBcoDir(True, 0);
  end;
end;

procedure TFmCNAB_RetBco.HabilitaBotoes();
begin
  QrTem.Close;
  UMyMod.AbreQuery(QrTem, Dmod.MyDB, 'TFmCNAB_RetBco.HabilitaBotoes()');
  BtCarrega.Enabled := FLin1 > 0;
  BtAbertos.Enabled := QrTemItens.Value > 0;
end;

procedure TFmCNAB_RetBco.LeArquivos(Banco, Controle: Integer; Dir, Arq: String);
const
  NomeLayout = '';
var
  Bco, CNAB, Entidade: Integer;
  Arquivo: String;
begin
  try
    Arquivo := dmkPF.CaminhoArquivo(Dir, Arq, '');
    if FileExists(Arquivo) then
    begin
      if CarregaArquivo(NomeLayout, Arquivo, Controle, Bco, CNAB, Entidade) then
        CarregaItensRetorno(1, Bco, CNAB, Controle, Entidade, Arquivo);
    end;
  except
    Geral.MensagemBox('Erro ao ler arquivo!' + sLineBreak + Arquivo,
    'Erro', MB_OK+MB_ICONERROR);
    raise;
  end;
end;

procedure TFmCNAB_RetBco.QrBcoArqAfterScroll(DataSet: TDataSet);
begin
{
  Screen.Cursor := crHourGlass;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  FLin1 := 0;
  LeArquivos(QrBcoDirCodigo.Value);
  HabilitaBotoes();
  Screen.Cursor := crDefault;
}
  ReopenBcoReg(0);
end;

procedure TFmCNAB_RetBco.QrBcoArqBeforeClose(DataSet: TDataSet);
begin
  QrBcoReg.Close;
end;

procedure TFmCNAB_RetBco.QrBcoArqCalcFields(DataSet: TDataSet);
begin
  QrBcoArqSEQ.Value := QrBcoArq.RecNo;
end;

procedure TFmCNAB_RetBco.QrBcoDirAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes();
end;

procedure TFmCNAB_RetBco.QrBcoDirAfterScroll(DataSet: TDataSet);
begin
  ReopenBcoArq(0);
  HabilitaBotoes();
end;

procedure TFmCNAB_RetBco.QrBcoDirBeforeClose(DataSet: TDataSet);
begin
  QrBcoArq.Close;
end;

procedure TFmCNAB_RetBco.QrBcoRegAfterOpen(DataSet: TDataSet);
begin
  BtEdita.Enabled := QrBcoReg.RecordCount > 0;
end;

procedure TFmCNAB_RetBco.QrBcoRegBeforeClose(DataSet: TDataSet);
begin
  BtEdita.Enabled := False;
end;

procedure TFmCNAB_RetBco.QrBcoRegCalcFields(DataSet: TDataSet);
begin
  QrBcoRegSEQ.Value := QrBcoReg.RecNo;
  QrBcoRegOcorrDta_TXT.Value := Geral.FDT(QrBcoRegOcorrDta.Value, 3);
  QrBcoRegPagtoDta_TXT.Value := Geral.FDT(QrBcoRegPagtoDta.Value, 3);
  QrBcoRegDtaTarif_TXT.Value := Geral.FDT(QrBcoRegDtaTarif.Value, 3);
  QrBcoRegVenctDta_TXT.Value := Geral.FDT(QrBcoRegVenctDta.Value, 3);
end;

procedure TFmCNAB_RetBco.ReopenBcoArq(Controle: Integer);
begin
  QrBcoArq.Close;
  QrBcoArq.SQL.Clear;
  QrBcoArq.SQL.Add('SELECT * FROM cnabbcoarq');
  QrBcoArq.SQL.Add('WHERE Codigo=' + FormatFloat('0', QrBcoDirCodigo.Value));
  QrBcoArq.Open;
  //
  QrBcoArq.Locate('Controle', Controle, []);
end;

procedure TFmCNAB_RetBco.ReopenBcoDir(Filtra: Boolean; Codigo: Integer);
begin
  QrBcoDir.Close;
  QrBcoDir.SQL.Clear;
  QrBcoDir.SQL.Add('SELECT * FROM cnabbcodir');
  if Filtra then
    QrBcoDir.SQL.Add('WHERE Arquivos > 0 OR DirExiste = "N�o"');
  QrBcoDir.Open;
  //
  QrBcoDir.Locate('Codigo', Codigo, []);
end;

procedure TFmCNAB_RetBco.ReopenBcoReg(Conta: Integer);
begin
  QrBcoReg.Close;
  QrBcoReg.SQL.Clear;
  QrBcoReg.SQL.Add('SELECT * FROM cnabbcoreg');
  QrBcoReg.SQL.Add('WHERE Controle=' + FormatFloat('0', QrBcoArqControle.Value));
  QrBcoReg.Open;
  //
  QrBcoReg.Locate('Conta', Conta, []);
end;

procedure TFmCNAB_RetBco.ReopenLocCar1(Banco, EntCliente: Integer;
CNPJ, Cedente: String);
begin
  QrLocCar1.Close;
  QrLocCar1.SQL.Clear;
  QrLocCar1.SQL.Add('SELECT car.*');
  QrLocCar1.SQL.Add('FROM carteiras car');
  QrLocCar1.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI');
  QrLocCar1.SQL.Add('WHERE car.ForneceI=' + FormatFloat('0', EntCliente));
  QrLocCar1.SQL.Add('AND car.Banco1=' + FormatFloat('0', Banco));
  QrLocCar1.SQL.Add('AND car.CodCedente="' + Cedente + '"');
  QrLocCar1.SQL.Add('AND (IF (ent.Tipo=0, ent.CNPJ, ent.CPF))="' + Geral.SoNumero_TT(CNPJ) + '"');
  QrLocCar1.SQL.Add('ORDER BY car.Nome');
  QrLocCar1.Open;
  //dmkPF.LeMeuTexto(QrLocCar1.SQL.Text);
end;

procedure TFmCNAB_RetBco.ReopenLocEnt1(CNPJ: String);
begin
  QrLocEnt1.Close;
  QrLocEnt1.SQL.Clear;
  //
  QrLocEnt1.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  QrLocEnt1.SQL.Add('ent.Nome END NOMEENT, ent.Codigo CLIENTE, ent.CliInt');
  QrLocEnt1.SQL.Add('FROM entidades ent ');
  QrLocEnt1.SQL.Add('WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0');
  QrLocEnt1.SQL.Add('');
  QrLocEnt1.Params[00].AsString := CNPJ;
  UMyMod.AbreQuery(QrLocEnt1, Dmod.MyDB, 'TFmCNAB_Ret2.CarregaArquivo()');
end;

{ Never used
procedure TFmCNAB_RetBco.ReopenLocEnt2(Banco: Integer; Agencia,
  Cedente: String);
begin
  QrLocEnt2.Close;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrLocEnt2.SQL.Text :=
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ' +
    'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt ' +
    'FROM cond cnd ' +
    'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ' +
    'WHERE cnd.Banco=:P0 ' +
    'AND cnd.Agencia=:P1 ' +
    'AND cnd.CodCedente=:P2 ';
  end else
  begin
    QrLocEnt2.SQL.Text := '*** SQL N�o Definida ***'
    (*
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ' +
    'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt ' +
    'FROM cond cnd ' +
    'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ' +
    'WHERE cnd.Banco=:P0 ' +
    'AND cnd.Agencia=:P1 ' +
    'AND cnd.CodCedente=:P2 ';
    *)
  end;
  QrLocEnt2.Params[00].AsInteger := Banco;
  QrLocEnt2.Params[01].AsInteger := Geral.IMV(Agencia);
  QrLocEnt2.Params[02].AsString  := Cedente;
  UMyMod.AbreQuery(QrLocEnt2, 'TFmCNAB_Ret2.ReopenLocEnt2()');
end;
}

procedure TFmCNAB_RetBco.ReopenLocEnt3(Banco: Integer; Agencia, Conta,
  DAC: String);
begin
  QrLocEnt3.Close;
  QrLocEnt3.SQL.Clear;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrLocEnt3.SQL.Add('SELECT');
    QrLocEnt3.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
    QrLocEnt3.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,');
    QrLocEnt3.SQL.Add('ent.Codigo CLIENTE, cnd.Codigo CliInt');
    QrLocEnt3.SQL.Add('FROM cond cnd');
    QrLocEnt3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
    QrLocEnt3.SQL.Add('WHERE cnd.Banco=' + FormatFloat('000', Banco));
    QrLocEnt3.SQL.Add('AND cnd.Agencia=' + Agencia);
    QrLocEnt3.SQL.Add('AND cnd.Conta="' + Conta + '"');
    QrLocEnt3.SQL.Add('AND cnd.DVConta="' + DAC + '"');
    // Pegar primeiro a administradora!
    QrLocEnt3.SQL.Add('ORDER BY CliInt');
    //dmkPF.LeMeuTexto(QrLocEnt3.SQL.Text);
  end else
  begin
    QrLocEnt3.SQL.Text := '*** SQL N�o Definida ***'
  end;
  UMyMod.AbreQuery(QrLocEnt3, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLocEnt3()');
end;

procedure TFmCNAB_RetBco.ReopenLocEnt4(Banco: Integer; Agencia, Conta,
  DVAgencia, DVConta: String);
begin
  QrLocEnt3.Close;
  QrLocEnt3.SQL.Clear;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrLocEnt3.SQL.Add('SELECT');
    QrLocEnt3.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
    QrLocEnt3.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,');
    QrLocEnt3.SQL.Add('ent.Codigo CLIENTE, cnd.Codigo CliInt');
    QrLocEnt3.SQL.Add('FROM cond cnd');
    QrLocEnt3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
    QrLocEnt3.SQL.Add('WHERE cnd.Banco=' + FormatFloat('000', Banco));
    QrLocEnt3.SQL.Add('AND cnd.Agencia=' + Agencia);
    QrLocEnt3.SQL.Add('AND cnd.Conta="' + Conta + '"');
    QrLocEnt3.SQL.Add('AND cnd.DVAgencia="' + DVAgencia + '"');
    QrLocEnt3.SQL.Add('AND cnd.DVConta="' + DVConta + '"');
    // Pegar primeiro a administradora!
    QrLocEnt3.SQL.Add('ORDER BY CliInt');
    //dmkPF.LeMeuTexto(QrLocEnt3.SQL.Text);
  end else
  begin
    QrLocEnt3.SQL.Text := '*** SQL N�o Definida ***'
  end;
  UMyMod.AbreQuery(QrLocEnt3, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLocEnt3()');
end;

procedure TFmCNAB_RetBco.ReopenQrPesq2(IDLink, Cliente: Integer);
begin
  QrPesq2.Close;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrPesq2.SQL.Text :=
    'SELECT DISTINCT lan.Vencimento, lan.Credito, ' +
    'cnd.PercMulta, cnd.PercJuros ' +
    'FROM ' + VAR_LCT + ' lan ' +
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ' +
    'LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI ' +
    'WHERE lan.FatID in (600,601) ' +
    'AND lan.FatNum=:P0 ' +
    'AND car.Tipo=2 ' +
    'AND car.ForneceI=:P1 ';
  end else
  begin
    QrPesq2.SQL.Text := '*** SQL N�o Definida ***';
    {
    'SELECT DISTINCT lan.Vencimento, lan.Credito, ' +
    '0.0000 PercMulta, 0.0000 PercJuros ' +
    'FROM ' + VAR_LCT + ' lan ' +
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ' +
    'WHERE lan.FatID in (600,601) ' +
    'AND lan.FatNum=:P0 ' +
    'AND car.Tipo=2 ' +
    'AND car.ForneceI=:P1 ';
    }
  end;
  QrPesq2.Params[00].AsInteger := IDLink;
  QrPesq2.Params[01].AsInteger := Cliente;
  UMyMod.AbreQuery(QrPesq2, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenQrPesq2()');
end;

procedure TFmCNAB_RetBco.ReopenSQL3(ID_Link, Entidade: Integer);
begin
  QrPesq3.Close;
  QrPesq3.SQL.Clear;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrPesq3.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrPesq3.SQL.Add('cnd.PercMulta, cnd.PercJuros, lan.Vencimento');
    QrPesq3.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPesq3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq3.SQL.Add('LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI');
    QrPesq3.SQL.Add('WHERE lan.FatID in (600,601)');
    QrPesq3.SQL.Add('AND lan.FatNum=:P0');
    QrPesq3.SQL.Add('AND car.Tipo=2');
    QrPesq3.SQL.Add('AND car.ForneceI=:P1');
    QrPesq3.SQL.Add('GROUP BY lan.FatNum');
  end else begin
    QrPesq3.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrPesq3.SQL.Add('0.0000 PercMulta, 0.0000 PercJuros, lan.Vencimento');
    QrPesq3.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPesq3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq3.SQL.Add('WHERE lan.FatID in (600,601)');
    QrPesq3.SQL.Add('AND lan.FatNum=:P0');
    QrPesq3.SQL.Add('AND car.Tipo=2');
    QrPesq3.SQL.Add('AND car.ForneceI=:P1');
    QrPesq3.SQL.Add('GROUP BY lan.FatNum');
  end;
  QrPesq3.Params[00].AsInteger := ID_Link;
  QrPesq3.Params[01].AsInteger := Entidade;
  UMyMod.AbreQuery(QrPesq3, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenSQL3()');
end;

procedure TFmCNAB_RetBco.VerificaImplementacoes(Row: Integer);
{
var
  CodLiq, Banco, TamReg, Genero: Integer;
  VarBool: Boolean;
  Fator, Diferenca: Double;
}
begin
  {
  Banco  := Geral.IMV(GradeA.Cells[10, Row]);
  TamReg := Geral.IMV(GradeA.Cells[11, Row]);
  CodLiq := Geral.IMV(Grade1.Cells[02, 1]);
  //
  UBancos.SeparaJurosEMultaImplementado(Banco, VarBool);
  UBancos.EhCodigoLiquidacao(CodLiq, Banco, TamReg);
  UBancos.InformaTarifaDeCobrancaImplementado(Banco, VarBool);
  UBancos.ContaDaOcorrencia(Banco, TamReg, FormatFloat('00', CodLiq), Genero);
  UBancos.FatorDeRecebimento(Banco, 1, 0.1, 1, Fator);
  UBancos.DiferencaDeRecebimento(Banco, 1, 0.1, 1, Diferenca);
  UBancos.FatorMultaDeRecebimento(Banco, 1, 0.1, 1, Fator);
  }
end;

// Parei aqui! fazer cnpj no 240!
end.
