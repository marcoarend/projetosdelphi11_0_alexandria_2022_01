unit CNAB_Fld;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, Db,
  mySQLDbTables, frxClass, frxDBSet, dmkImage, dmkgeral, UnDmkEnums;

type
  TFmCNAB_Fld = class(TForm)
    Panel1: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Label27: TLabel;
    Label25: TLabel;
    EdFuncionalidade: TEdit;
    BitBtn5: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    StaticText1: TStaticText;
    frxFuncionalidade: TfrxReport;
    frxDsFuncionalidade: TfrxDBDataset;
    DsFuncionalidade: TDataSource;
    QrFuncionalidade: TmySQLQuery;
    QrFuncionalidadeCodigo: TIntegerField;
    QrFuncionalidadeNome: TWideStringField;
    BtOK: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure EdFuncionalidadeChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCNAB_Fld: TFmCNAB_Fld;

implementation

uses UnMyObjects, UnBancos;

{$R *.DFM}

procedure TFmCNAB_Fld.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Fld.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_Fld.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmCNAB_Fld.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Fld.BitBtn5Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxFuncionalidade, 'Lista de Funcionalidades');
end;

procedure TFmCNAB_Fld.EdFuncionalidadeChange(Sender: TObject);
begin
  QrFuncionalidade.Close;
  QrFuncionalidade.Params[0].AsString := '%' + EdFuncionalidade.Text + '%';
  QrFuncionalidade.Open;
end;

procedure TFmCNAB_Fld.BtOKClick(Sender: TObject);
begin
  UBancos.RecarregaMesuCodigosDeCamposCNAB();
end;

end.
