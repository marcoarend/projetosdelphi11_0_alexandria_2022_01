unit CNAB_Ret;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Db, mySQLDbTables,
  DBCtrls, Grids, DBGrids, math, ComCtrls, FileCtrl, Menus, 
    dmkDBGrid;

type
  TFmCNAB_Ret = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCNAB_Dir: TmySQLQuery;
    DsCNAB_Dir: TDataSource;
    QrCNAB_DirNOMEBANCO: TWideStringField;
    QrCNAB_DirCodigo: TIntegerField;
    QrCNAB_DirLk: TIntegerField;
    QrCNAB_DirDataCad: TDateField;
    QrCNAB_DirDataAlt: TDateField;
    QrCNAB_DirUserCad: TIntegerField;
    QrCNAB_DirUserAlt: TIntegerField;
    QrCNAB_DirNome: TWideStringField;
    QrCNAB_DirEnvio: TSmallintField;
    Timer1: TTimer;
    PnCarrega: TPanel;
    PainelConfirma: TPanel;
    BtCarrega: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Button1: TButton;
    PnArquivos: TPanel;
    ListBox1: TListBox;
    MemoTam: TMemo;
    QrDupl: TmySQLQuery;
    QrLotesIts_: TmySQLQuery;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrLotesIts_NOMECLI: TWideStringField;
    QrLotesIts_Codigo: TIntegerField;
    QrLotesIts_Duplicata: TWideStringField;
    QrLotesIts_CPF: TWideStringField;
    QrLotesIts_Emitente: TWideStringField;
    QrLotesIts_Bruto: TFloatField;
    QrLotesIts_Desco: TFloatField;
    QrLotesIts_Quitado: TIntegerField;
    QrLotesIts_Valor: TFloatField;
    QrLotesIts_Emissao: TDateField;
    QrLotesIts_DCompra: TDateField;
    QrLotesIts_Vencto: TDateField;
    QrLotesIts_DDeposito: TDateField;
    QrLotesIts_Dias: TIntegerField;
    QrLotesIts_Data3: TDateField;
    QrLotesIts_Cobranca: TIntegerField;
    QrLotesIts_Repassado: TSmallintField;
    QrLotesIts_Lote: TIntegerField;
    QrLotesIts_TIPOLOTE: TSmallintField;
    QrLotesIts_Cliente: TIntegerField;
    QrCNAB240_: TmySQLQuery;
    QrCNAB240_CNAB240L: TIntegerField;
    QrCNAB240_CNAB240I: TIntegerField;
    QrCNAB240_Controle: TIntegerField;
    QrCNAB240_Banco: TIntegerField;
    QrCNAB240_ConfigBB: TIntegerField;
    QrCNAB240_Lote: TIntegerField;
    QrCNAB240_Item: TIntegerField;
    QrCNAB240_DataOcor: TDateField;
    QrCNAB240_Envio: TSmallintField;
    QrCNAB240_Movimento: TSmallintField;
    QrCNAB240_Lk: TIntegerField;
    QrCNAB240_DataCad: TDateField;
    QrCNAB240_DataAlt: TDateField;
    QrCNAB240_UserCad: TIntegerField;
    QrCNAB240_UserAlt: TIntegerField;
    QrCNAB240_NOMEENVIO: TWideStringField;
    QrCNAB240_NOMEBANCO: TWideStringField;
    QrCNAB240_NOMEMOVIMENTO: TWideStringField;
    QrCNAB240_NOMECLI: TWideStringField;
    QrCNAB240_MeuLote: TIntegerField;
    QrCNAB240_Duplicata: TWideStringField;
    QrCNAB240_CPF: TWideStringField;
    QrCNAB240_Emitente: TWideStringField;
    QrCNAB240_Bruto: TFloatField;
    QrCNAB240_Desco: TFloatField;
    QrCNAB240_Quitado: TIntegerField;
    QrCNAB240_Valor: TFloatField;
    QrCNAB240_Emissao: TDateField;
    QrCNAB240_DCompra: TDateField;
    QrCNAB240_Vencto: TDateField;
    QrCNAB240_DDeposito: TDateField;
    QrCNAB240_Dias: TIntegerField;
    QrCNAB240_Data3: TDateField;
    QrCNAB240_Cobranca: TIntegerField;
    QrCNAB240_Repassado: TSmallintField;
    QrCNAB240_Bordero: TIntegerField;
    QrCNAB240_TIPOLOTE: TSmallintField;
    QrCNAB240_DATA3_TXT: TWideStringField;
    QrCNAB240_NOMESTATUS: TWideStringField;
    QrCNAB240_NOMETIPOLOTE: TWideStringField;
    QrCNAB240_CPF_TXT: TWideStringField;
    QrCNAB240_Custas: TFloatField;
    QrCNAB240_IRTCLB_TXT: TWideStringField;
    QrCNAB240_ValPago: TFloatField;
    QrCNAB240_ValCred: TFloatField;
    QrCNAB240_Cliente: TIntegerField;
    QrCNAB240_Acao: TIntegerField;
    QrCNAB240_NOMEACAO: TWideStringField;
    QrCNAB240_Sequencia: TIntegerField;
    QrCNAB240_Convenio: TWideStringField;
    QrCNAB240_IRTCLB: TWideStringField;
    DsCNAB240: TDataSource;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    PnMovimento: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Button2: TButton;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    DsOcorreu: TDataSource;
    QrOcorDupl: TmySQLQuery;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    QrOcorDuplValOcorBank: TFloatField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    QrOcorBankEnvio: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankFormaCNAB: TSmallintField;
    Timer2: TTimer;
    QrOB2: TmySQLQuery;
    QrOB2Nome: TWideStringField;
    PageControl1: TPageControl;
    QrLocEnt1: TmySQLQuery;
    QrLocEnt1NOMEENT: TWideStringField;
    QrLocEnt1CLIENTE: TIntegerField;
    QrLocEnt1COND: TIntegerField;
    Splitter1: TSplitter;
    TabSheet3: TTabSheet;
    Grade1: TStringGrid;
    QrLei: TmySQLQuery;
    DsLei: TDataSource;
    QrLeiCodigo: TIntegerField;
    QrLeiBanco: TIntegerField;
    QrLeiSeuNum: TIntegerField;
    QrLeiOcorrData: TDateField;
    QrLeiValTitul: TFloatField;
    QrLeiValAbati: TFloatField;
    QrLeiValDesco: TFloatField;
    QrLeiValPago: TFloatField;
    QrLeiValJuros: TFloatField;
    QrLeiValMulta: TFloatField;
    QrLeiMotivo1: TWideStringField;
    QrLeiMotivo2: TWideStringField;
    QrLeiMotivo3: TWideStringField;
    QrLeiMotivo4: TWideStringField;
    QrLeiMotivo5: TWideStringField;
    QrLeiQuitaData: TDateField;
    QrLeiDiretorio: TIntegerField;
    QrLeiArquivo: TWideStringField;
    QrLeiItemArq: TIntegerField;
    QrLeiStep: TSmallintField;
    QrLeiLk: TIntegerField;
    QrLeiDataCad: TDateField;
    QrLeiDataAlt: TDateField;
    QrLeiUserCad: TIntegerField;
    QrLeiUserAlt: TIntegerField;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    BtAbertos: TBitBtn;
    QrLeiIDNum: TIntegerField;
    PnCarregados: TPanel;
    Panel5: TPanel;
    PB1: TProgressBar;
    QrLeiEntidade: TIntegerField;
    QrLeiNOMEENT: TWideStringField;
    QrLeiItens: TmySQLQuery;
    DsLeiItens: TDataSource;
    QrLeiItensData: TDateField;
    QrLeiItensCarteira: TIntegerField;
    QrLeiItensGenero: TIntegerField;
    QrLeiItensDescricao: TWideStringField;
    QrLeiItensCredito: TFloatField;
    QrLeiItensCompensado: TDateField;
    QrLeiItensSit: TIntegerField;
    QrLeiItensVencimento: TDateField;
    QrLeiItensMez: TIntegerField;
    QrLeiItensFornecedor: TIntegerField;
    QrLeiVALBOLETO: TFloatField;
    QrBcocor: TmySQLQuery;
    QrBcocorCarrega: TSmallintField;
    StaticText1: TStaticText;
    QrLeiItensNOMEPROPRIET: TWideStringField;
    BtExclui: TBitBtn;
    PMExclui: TPopupMenu;
    ExcluiAtual1: TMenuItem;
    ExcluiSelecionados1: TMenuItem;
    ExcluiTodos1: TMenuItem;
    BtConcilia: TBitBtn;
    PMConcilia: TPopupMenu;
    ConciliaAtual1: TMenuItem;
    ConciliaSelecionados1: TMenuItem;
    ConciliaTodos1: TMenuItem;
    QrLeiOcorrCodi: TWideStringField;
    QrLocCta: TmySQLQuery;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    QrTem: TmySQLQuery;
    QrTemItens: TLargeintField;
    BitBtn1: TBitBtn;
    QrCNAB_DirNOMECARTEIRA: TWideStringField;
    QrCNAB_DirCliInt: TIntegerField;
    QrCNAB_DirCarteira: TIntegerField;
    QrCNAB_DirBanco1: TIntegerField;
    QrLeiCarteira: TIntegerField;
    QrLeiTipoCart: TIntegerField;
    QrLEB: TmySQLQuery;
    QrLEBEntidade: TIntegerField;
    QrLUH: TmySQLQuery;
    QrLUHApto: TIntegerField;
    QrLUHPropriet: TIntegerField;
    Splitter2: TSplitter;
    QrLeiItensControle: TIntegerField;
    QrLeiItensSub: TSmallintField;
    QrLeiItensTipoCart: TIntegerField;
    QrLeiItensDebito: TFloatField;
    QrLeiItensNotaFiscal: TIntegerField;
    QrLeiItensSerieCH: TWideStringField;
    QrLeiItensDocumento: TFloatField;
    QrLeiItensCliente: TIntegerField;
    QrLeiItensCliInt: TIntegerField;
    QrLeiItensForneceI: TIntegerField;
    QrLeiItensDataDoc: TDateField;
    QrLeiItensApto: TIntegerField;
    QrLeiItensUH: TWideStringField;
    QrLeiItensMez_TXT: TWideStringField;
    CkReverter: TCheckBox;
    QrDuplCodigo: TIntegerField;
    QrLeiDevJuros: TFloatField;
    QrLeiDevMulta: TFloatField;
    QrLeiDJM: TFloatField;
    QrPesq2: TmySQLQuery;
    QrPesq2Vencimento: TDateField;
    QrPesq2PercMulta: TFloatField;
    QrPesq2PercJuros: TFloatField;
    Label1: TLabel;
    QrBco: TmySQLQuery;
    QrBcoPadrIni: TIntegerField;
    QrBcoPadrTam: TIntegerField;
    QrBcoBcoOrig: TIntegerField;
    QrPesq1: TmySQLQuery;
    QrPesq1PadrIni: TIntegerField;
    QrPesq1PadrTam: TIntegerField;
    QrPesq1PadrVal: TWideStringField;
    QrPesq1SecoVal: TWideStringField;
    QrPesq1AlfaNum: TSmallintField;
    QrPesq1ID_REGISTRO: TWideStringField;
    QrCampos: TmySQLQuery;
    QrBanco: TmySQLQuery;
    QrCamposCampo: TIntegerField;
    QrCamposPadrIni: TIntegerField;
    QrCamposPadrTam: TIntegerField;
    QrCamposFormato: TWideStringField;
    QrCamposCasas: TSmallintField;
    QrBancoID_400i: TIntegerField;
    QrBancoID_400t: TIntegerField;
    QrLeiID_Link: TIntegerField;
    QrLeiValJuMul: TFloatField;
    QrLeiValOutro: TFloatField;
    QrLeiValTarif: TFloatField;
    QrLocCond: TmySQLQuery;
    QrLeiItensMulta: TFloatField;
    Label2: TLabel;
    Label3: TLabel;
    BtAgenda: TBitBtn;
    QrLeiItensCOND: TIntegerField;
    QrLocCondCodigo: TIntegerField;
    QrPesq2Credito: TFloatField;
    QrLeiNossoNum: TWideStringField;
    QrLeiDtaTarif: TDateField;
    QrLeiDTA_TARIF_TXT: TWideStringField;
    QrPesq3: TmySQLQuery;
    QrPesq3Credito: TFloatField;
    QrPesq3PercMulta: TFloatField;
    QrPesq3PercJuros: TFloatField;
    QrPesq3Vencimento: TDateField;
    QrLocCtaMensal: TWideStringField;
    QrLeiDescriCNR: TWideStringField;
    QrLocEnt2: TmySQLQuery;
    QrLocEnt2NOMEENT: TWideStringField;
    QrLocEnt2CLIENTE: TIntegerField;
    QrLocEnt2COND: TIntegerField;
    TabSheet2: TTabSheet;
    Memo2: TMemo;
    QrLocCondPercJuros: TFloatField;
    QrLocCondPercMulta: TFloatField;
    QrLocCondVTCBBNITAR: TFloatField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBGrid6: TDBGrid;
    dmkDBGrid1: TdmkDBGrid;
    N1: TMenuItem;
    ExcluiBloquetosnolocalizados1: TMenuItem;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    Alteravalordoitemdearrecadaoselecionado1: TMenuItem;
    Excluioitemdearrecadaoselecionado1: TMenuItem;
    Excluso1: TMenuItem;
    Ajustavaloresdobloquetoatual1: TMenuItem;
    QrLeiTIPO_BOL: TFloatField;
    QrLeiNOME_TIPO_BOL: TWideStringField;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet7: TTabSheet;
    QrLeiAgr: TmySQLQuery;
    DsLeiAgr: TDataSource;
    DBGrid3: TDBGrid;
    QrLeiAgrNOMEPROPRIET: TWideStringField;
    QrLeiAgrValor: TFloatField;
    QrLeiAgrData: TDateField;
    QrLeiAgrMez: TIntegerField;
    QrLeiAgrCliInt: TIntegerField;
    QrLeiAgrForneceI: TIntegerField;
    QrLeiAgrApto: TIntegerField;
    QrLeiAgrUH: TWideStringField;
    QrLeiAgrTIPO_BLOQ: TLargeintField;
    QrLeiAgrNOME_TIPO_BLOQ: TWideStringField;
    QrLeiAgrMez_TXT: TWideStringField;
    QrBancoID_240i: TIntegerField;
    QrBancoID_240t: TIntegerField;
    QrBancoID_240r: TSmallintField;
    QrBancoID_240s: TWideStringField;
    QrF240T: TmySQLQuery;
    Splitter3: TSplitter;
    GradeA: TStringGrid;
    QrLeiTamReg: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtCarregaClick(Sender: TObject);
    procedure QrCNAB240_CalcFields(DataSet: TDataSet);
    procedure QrCNAB240_AfterScroll(DataSet: TDataSet);
    procedure Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Grade1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtAbertosClick(Sender: TObject);
    procedure QrLeiAfterScroll(DataSet: TDataSet);
    procedure QrLeiBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluiAtual1Click(Sender: TObject);
    procedure ExcluiSelecionados1Click(Sender: TObject);
    procedure ExcluiTodos1Click(Sender: TObject);
    procedure QrLeiAfterOpen(DataSet: TDataSet);
    procedure BtConciliaClick(Sender: TObject);
    procedure ConciliaAtual1Click(Sender: TObject);
    procedure ConciliaSelecionados1Click(Sender: TObject);
    procedure ConciliaTodos1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCNAB_DirAfterScroll(DataSet: TDataSet);
    procedure QrLeiItensCalcFields(DataSet: TDataSet);
    procedure QrLeiCalcFields(DataSet: TDataSet);
    procedure DBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtAgendaClick(Sender: TObject);
    procedure QrLeiAfterClose(DataSet: TDataSet);
    procedure ExcluiBloquetosnolocalizados1Click(Sender: TObject);
    procedure Alteravalordoitemdearrecadaoselecionado1Click(
      Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Excluioitemdearrecadaoselecionado1Click(Sender: TObject);
    procedure Ajustavaloresdobloquetoatual1Click(Sender: TObject);
    procedure QrLeiAgrCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FLinB, FLinA, FLin1, FActiveRowA, FActiveRowB, FActiveRowC,
    FAbertosCli: Integer;
    procedure LeArquivos(Diretorio: String; CodDir, LinA: Integer);
    //procedure ReopenOcorreu(Codigo: Integer);
    //
    function GravaItens: Integer;
    function CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
    //
    function CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
    function CarregaItensRetornoA(Arquivo: String;
             SeqDir, SeqArq, Entidade, TamReg, BcoRet, BcoUse: Integer): Boolean;
    procedure CarregaItensRetorno240(BcoRet, BcoUse, Entidade, SeqDir, SeqArq: Integer);
    procedure CarregaItensRetorno400(Banco, Entidade, SeqDir, SeqArq: Integer);
    procedure MostraAbertos;
    procedure ReopenCNAB0Lei(Codigo: Integer);
    procedure ExcluirItens(Acao: TSelType);
    procedure ConciliaItens(Acao: TSelType);
    procedure HabilitaBotoes;
    function ErroLinha(Linha: Integer; Avisa: Boolean): Boolean;
    function CondDeEntidade(const Entidade: Integer;
             var Cond: Integer): Boolean;
    procedure ReopenLeiItens(Controle: Integer);
    procedure ReopenLeiAgr(Data: TDateTime;
              CliInt, ForneceI, Apto, Mez: Integer);
    function LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
    function LocDado240(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
  public
    { Public declarations }
    FLista: TStrings;
    FLengt: Integer;
  end;

  var
  FmCNAB_Ret: TFmCNAB_Ret;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnGOTOy, UnInternalConsts, Principal, SelCod,
  CondGerArreFut, ModuleCond, UnBancos, UnFinanceiro;


procedure TFmCNAB_Ret.QrCNAB240_CalcFields(DataSet: TDataSet);
begin
{
  case QrCNAB240TIPOLOTE.Value of
    0: QrCNAB240NOMETIPOLOTE.Value := 'Cheque';
    1: QrCNAB240NOMETIPOLOTE.Value := 'Duplicata';
    else QrCNAB240NOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrCNAB240DATA3_TXT.Value := Geral.FDT(QrCNAB240Data3.Value, 2);
  QrCNAB240CPF_TXT.Value := Geral.FormataCNPJ_TT(QrCNAB240CPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrCNAB240Quitado.Value,
    QrCNAB240DDeposito.Value, Date, QrCNAB240Data3.Value, QrCNAB240Repassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMEMOVIMENTO.Value := MLAGeral.CNABTipoDeMovimento(
    QrCNAB240Banco.Value, QrCNAB240Envio.Value,  QrCNAB240Movimento.Value, 0);
  QrCNAB240NOMEENVIO.Value := MLAGeral.CNAB240Envio(QrCNAB240Envio.Value);
  if QrCNAB240Acao.Value = 1 then QrCNAB240NOMEACAO.Value := 'Sim'
  else QrCNAB240NOMEACAO.Value := '';
}  
end;

procedure TFmCNAB_Ret.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmCNAB_Ret.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Ret.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if QrCNAB_Dir.State = dsInactive then Timer1.Enabled := True;
end;

procedure TFmCNAB_Ret.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB_Ret.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PnMovimento.Align  := alClient;
  PnArquivos.Align   := alClient;
  PnCarregados.Align := alClient;
  //
  FLengt := 0;
  FLista := TStringList.Create;
  FActiveRowA := 0;
  FActiveRowB := 0;
  FActiveRowC := 0;
  //
  GradeA.ColWidths[00] := 032;
  GradeA.ColWidths[01] := 160;
  GradeA.ColWidths[02] := 272;
  GradeA.ColWidths[03] := 112;
  GradeA.ColWidths[04] := 104;
  GradeA.ColWidths[05] := 044;
  GradeA.ColWidths[06] := 036;
  GradeA.ColWidths[07] := 028;
  GradeA.ColWidths[08] := 064;
  GradeA.ColWidths[09] := 040;
  GradeA.ColWidths[10] := 028;
  GradeA.ColWidths[11] := 036;
  //
  GradeA.Cells[00,00] := 'Seq';
  GradeA.Cells[01,00] := 'Arquivo';
  GradeA.Cells[02,00] := 'Cliente interno';
  GradeA.Cells[03,00] := 'CNPJ / CPF';
  GradeA.Cells[04,00] := 'Cedente';
  GradeA.Cells[05,00] := 'Lotes';
  GradeA.Cells[06,00] := 'Itens';
  GradeA.Cells[07,00] := 'Dir';
  GradeA.Cells[08,00] := 'Cr�ditos';
  GradeA.Cells[09,00] := 'Cliente';
  GradeA.Cells[10,00] := 'Bco';
  GradeA.Cells[11,00] := 'CNAB';
  //
  Grade1.ColWidths[00] := 032;
  Grade1.ColWidths[01] := 068;
  Grade1.ColWidths[02] := 024;
  Grade1.ColWidths[03] := 180;
  Grade1.ColWidths[04] := 056;
  Grade1.ColWidths[05] := 072;
  Grade1.ColWidths[06] := 072;
  Grade1.ColWidths[07] := 064;
  Grade1.ColWidths[08] := 064;
  Grade1.ColWidths[09] := 072;
  Grade1.ColWidths[10] := 064;
  Grade1.ColWidths[11] := 064;
  Grade1.ColWidths[12] := 064;
  Grade1.ColWidths[13] := 064;
  Grade1.ColWidths[14] := 064;
  Grade1.ColWidths[15] := 064;
  Grade1.ColWidths[16] := 072;
  Grade1.ColWidths[17] := 120;
  Grade1.ColWidths[18] := 056;
  Grade1.ColWidths[19] := 056;
  Grade1.ColWidths[20] := 064;
  Grade1.ColWidths[21] := 072;
  Grade1.ColWidths[22] := 024;
  Grade1.ColWidths[23] := 024;
  Grade1.ColWidths[24] := 024;
  Grade1.ColWidths[25] := 024;
  Grade1.ColWidths[26] := 108;
  Grade1.ColWidths[27] := 064;
  Grade1.ColWidths[28] := 064;
  /////////////
  Grade1.Cells[00, 00] := 'Seq';
  Grade1.Cells[01, 00] := 'Nosso n�m.';
  Grade1.Cells[02, 00] := 'Ocorr�ncia';
  Grade1.Cells[03, 00] := 'Descri��o da ocorr�ncia';
  Grade1.Cells[04, 00] := 'Data ocor.';
  Grade1.Cells[05, 00] := 'Seu n�mero';
  Grade1.Cells[06, 00] := 'Val. t�tulo';
  Grade1.Cells[07, 00] := 'Abatimento';
  Grade1.Cells[08, 00] := 'Desconto';
  Grade1.Cells[09, 00] := 'Val. receb.';
  Grade1.Cells[10, 00] := 'Juros mora';
  Grade1.Cells[11, 00] := 'Multa';
  Grade1.Cells[12, 00] := 'Outros +';
  Grade1.Cells[13, 00] := 'Jur + Mul.';
  Grade1.Cells[14, 00] := 'Tarifa';
  Grade1.Cells[15, 00] := 'ERRO';
  Grade1.Cells[16, 00] := 'M.O.';
  Grade1.Cells[17, 00] := 'Motivos ocorr�ncia';
  Grade1.Cells[18, 00] := 'Dt.lanc.c/c';
  Grade1.Cells[19, 00] := 'Dt.D�b.tarifa';
  Grade1.Cells[20, 00] := 'Entidade';
  Grade1.Cells[21, 00] := 'ID Link';
  Grade1.Cells[22, 00] := 'Dir';
  Grade1.Cells[23, 00] := 'Arq';
  Grade1.Cells[24, 00] := 'Item';
  Grade1.Cells[25, 00] := 'Bco';
  Grade1.Cells[26, 00] := 'Documento';
  Grade1.Cells[27, 00] := 'Val. Bruto';
  Grade1.Cells[28, 00] := 'Outros -';
  //
end;

procedure TFmCNAB_Ret.LeArquivos(Diretorio: String; CodDir, LinA: Integer);
var
  i, n: Integer;
begin
  try
    if Diretorio[Length(Diretorio)] <> '\' then Diretorio := Diretorio + '\';
    MyObjects.GetAllFiles(False, Diretorio + '*.*', ListBox1, True);
    n := LinA;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      if FileExists(ListBox1.Items[i]) then
      begin
        if Trim(GradeA.Cells[01, n]) <> '' then
          n := n + 1;
        GradeA.RowCount := n + 1;
        GradeA.Cells[00, n] := IntToStr(n);
        GradeA.Cells[01, n] := ExtractFileName(ListBox1.Items[i]);
        GradeA.Cells[07, n] := FormatFloat('000', CodDir);
        //
        if not CarregaArquivo(ListBox1.Items[i], n) then
          Exit;
      end;
    end;
    if Trim(GradeA.Cells[1,1]) <> '' then
    CarregaItensRetorno(1, 1);
    //
    {if GradeB.RowCount > 2 then
      if GradeB.Cells[00, GradeB.RowCount -1] = '' then
        GradeB.RowCount := GradeB.RowCount - 1;
    //
    if GradeC.RowCount > 2 then
      if GradeC.Cells[00, GradeC.RowCount -1] = '' then
        GradeC.RowCount := GradeC.RowCount - 1;
    }
  except
    Application.MessageBox('Erro ao ler arquivos!', 'Erro', MB_OK+MB_ICONERROR);
    raise;
  end;
end;

procedure TFmCNAB_Ret.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowA then
  begin
    FActiveRowA := ARow;
    CarregaItensRetorno(FActiveRowA, 2);
  end;
end;

procedure TFmCNAB_Ret.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  QrCNAB_Dir.Close;
  QrCNAB_Dir.Open;
  if QrCNAB_Dir.RecordCount = 0 then Application.MessageBox(PChar(
    'Nenhum diret�rio foi definido em Cadastros -> CNAB -> Diret�rios '+
    'CNAB. Defina ao menos um diret�rio!'), 'Aviso', MB_OK+MB_ICONWARNING);
  HabilitaBotoes;
end;

procedure TFmCNAB_Ret.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  MemoTam.Lines.Clear;
  for i := 0 to GradeA.ColCount -1 do
    MemoTam.Lines.Add('  GradeA.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeA.ColWidths[i])+';');
end;

procedure TFmCNAB_Ret.GradeADrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeA.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end
end;

{procedure TFmCNAB_Ret.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrCNAB240Controle.Value;
  QrOcorreu.Open;
end;
}

procedure TFmCNAB_Ret.QrCNAB240_AfterScroll(DataSet: TDataSet);
begin
  //ReopenOcorreu(0);
end;

function TFmCNAB_Ret.CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
  function DadoLinha(SQLCompl, Linha: String; Item, Banco: Integer): String;
  begin
    QrPesq1.Close;
    QrPesq1.SQL[5] := SQLCompl;
    QrPesq1.Params[00].AsInteger := Item;
    QrPesq1.Params[01].AsInteger := Banco;
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
      Result := Copy(Linha, QrPesq1PadrIni.Value, QrPesq1PadrTam.Value)
    else Result := '';
  end;
  function DadoLista(SQLCompl: String; Lista: TStrings; Item, Banco,
  TipoRegI, TipoRegT: Integer): String;
  var
    i: Integer;
  begin
    QrPesq1.Close;
    QrPesq1.SQL[5] := SQLCompl;
    QrPesq1.Params[00].AsInteger := Item;
    QrPesq1.Params[01].AsInteger := Banco;
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      for i := 0 to Lista.Count - 1 do
      begin
        if Trim(Copy(Lista[i], TipoRegI, TipoRegT)) = Trim(QrPesq1ID_REGISTRO.Value) then
        begin
          Result := Copy(Lista[i], QrPesq1PadrIni.Value, QrPesq1PadrTam.Value);
          Exit;
        end;
      end;
    end else Result := '';
  end;
  function ConfereDado(const SQLCompl, Linha: String; const
  Item, Banco: Integer; const Mensagem: String; var Resultado: String): Boolean;
  begin
    Resultado := Trim(DadoLinha(SQLCompl, Linha, Item, Banco));
    Result := Resultado = Trim(QrPesq1PadrVal.Value);
    if not Result then
    begin
      Result := Resultado = Trim(QrPesq1SecoVal.Value);
      if not Result then
      begin
        if QrPesq1AlfaNum.Value = 1 then
        begin
          Result := Geral.IMV(Resultado) = Geral.IMV(Trim(QrPesq1PadrVal.Value));
          if not Result then
          begin
            Result := Geral.IMV(Resultado) = Geral.IMV(Trim(QrPesq1SecoVal.Value));
          end;
        end;
      end;
    end;
    if not Result then
      Application.MessageBox(PChar('Comparativo de dados retornou negativo!'+
      '   -   Mensagem:'+sLineBreak + '"' + Mensagem + '"' + sLineBreak
      + 'Valor Procurado: ' + '"' + QrPesq1PadrVal.Value + '"' + sLineBreak
      + 'Valor Localizado: ' + '"' + Resultado), 'Erro',
      MB_OK+MB_ICONWARNING);
  end;
  function BancoDoRegistro(const TamReg: Integer; const Linha: String;
  var Banco: integer): Integer;
  var
    s: String;
  begin
    Result := 0;
    QrBco.Close;
    QrBco.SQL.Clear;
    QrBco.SQL.Add('SELECT cad.BcoOrig, cad.*');
    QrBco.SQL.Add('FROM cnab_cad cad');
    QrBco.SQL.Add('WHERE cad.Campo=1');
    QrBco.SQL.Add('AND T'+FormatFloat('0', TamReg)+'=1');
    //
    //MLAGeral.LeMeuSQLy(QrBco, '', nil, True, True);
    QrBco.Open;
    Banco := 0;
    if TamReg = 240 then
    begin
      s := Copy(Linha, QrBcoPadrIni.Value, QrBcoPadrTam.Value);
      Banco := Geral.IMV(s);
    end else begin
      while not QrBco.Eof do
      begin
        s := Copy(Linha, QrBcoPadrIni.Value, QrBcoPadrTam.Value);
        if Geral.IMV(s) = QrBcoBcoOrig.Value then
        begin
          s := DadoLinha('AND T'+FormatFloat('0', TamReg)+'=1',
               Linha, 2, QrBcoBcoOrig.Value);
          if (Uppercase(Trim(s)) = Uppercase(Trim(QrPesq1PadrVal.Value)))
          or (Uppercase(Trim(s)) = Uppercase(Trim(QrPesq1SecoVal.Value))) then
          begin
            Banco := QrBcoBcoOrig.Value;
            Exit;
          end;
        end;
        QrBco.Next;
      end;
    end;
  end;
var
  i, k, n, p, BcoRet, BcoUse, IndexReg, TipoRegI, TipoRegT: Integer;
  s, CodCedente, CNPJ, ServNom, SQLCompl, AgTxt, CeTxt, NomeArq: String;
begin

  FLengt := -1000;
  Result := False;
  BcoRet := 0;
  BcoUse := 0;
  //Controle := 0;
  if not FileExists(Arquivo) then
  begin
    Application.MessageBox(PChar('O arquivo "'+Arquivo+'" n�o foi localizado.'+
    ' Ele pode ter sido exclu�do!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  k := 0;
  for i := 0 to FLista.Count -1 do
  begin
    n := Length(FLista[i]);
    if (FLengt = -1000) then
      if ( (n=240) or (n=400) ) then
        FLengt := n;
    if (n=0) or (n<>FLengt) then
    begin
      if Application.MessageBox(PChar('O arquivo "'+Arquivo+'" possui '+IntToStr(n)+
      ' caracteres na linha '+IntToStr(i)+ ' quando o esperado era 240 ou 400.' +
      '. Deseja abortar?'), 'Erro', MB_YESNO+MB_ICONERROR) = ID_YES
      then Exit;
      k := k + 1;
    end;
  end;
  if k > 0 then
  begin
    Application.MessageBox(PChar('O arquivo "'+Arquivo+'" possui '+IntToStr(k)+
    ' linhas que possuem quantidade de caracteres diferente de '+IntToStr(FLengt)+
    '.'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  // Verifica o padr�o do registro (240, 400)
  IndexReg := MLAGeral.TamanhoLinhaCNAB_Index(FLengt);
  if IndexReg = 0 then Exit;
  BancoDoRegistro(FLengt, FLista[0], BcoRet);
  case FLengt of
    240: BcoUse := -1;
    400: BcoUse := BcoRet;
  end;

  if BcoRet = 0 then
  begin
    Application.MessageBox(PChar('N�o foi poss�vel definir o banco do arquivo "'+
    Arquivo+'". Contate a dermatek caso esse arquivo for realmente de ' +
    'remessa/retorno banc�rio!+ sLineBreak'Arquivo de ' +
    IntToStr(FLengt) + ' posi��es!'), 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //

  SQLCompl := 'AND T'+FormatFloat('0', FLengt)+'=1';
  QrPesq1.Close;
  QrPesq1.SQL[5] := SQLCompl;
  QrPesq1.Params[00].AsInteger := 10;
  QrPesq1.Params[01].AsInteger := BcoUse;
  QrPesq1.Open;
  //MLAGeral.LeMeuSQLy(QrPesq1, '', nil, True, True);
  if QrPesq1.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('N�o foi encontrado nenhum campo de ' +
      '"identifica��o do registro header de arquivo".+ sLineBreaksLineBreak+
      'C�digo Dermatek: 10+ sLineBreak +
      'Banco: ' + FormatFloat('000', BcoUse) + sLineBreak+
      'CNAB: ' + IntToStr(FLengt) + ' posi��es!+ sLineBreaksLineBreak +
      'Informe a DERMATEK'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;

  TipoRegI := QrPesq1PadrIni.Value;
  TipoRegT := QrPesq1PadrTam.Value;

  // Tipo de arquivo - Lote de Servico
  if not ConfereDado(SQLCompl, FLista[0], 3, BcoUse,
    'O arquivo "'+Arquivo+'" n�o � lote de servi�o!', s) then Exit;
  //ServCod := Geral.IMV(s);
  ServNom := DadoLinha(SQLCompl, FLista[0], 4, BcoUse);

  // Arquivo de retorno
  if not ConfereDado(SQLCompl, FLista[0], 7, BcoUse,
    'O arquivo "'+Arquivo+'" n�o � lote de servi�o!', s) then Exit;
  //x := Geral.IMV(s);
  s := DadoLinha(SQLCompl, FLista[0], 8, BcoUse);

  // Header de arquivo

  if not ConfereDado(SQLCompl, FLista[0], 10, BcoUse,
    'O registro header n�o foi localizado no arquivo "'+Arquivo+'"!', s) then Exit;

  //Quantidade de registros no arquivo
  if not ConfereDado(SQLCompl, FLista[0], 301, BcoUse,
    'Quantidade de registros detalhe n�o definido no arquivo "'+Arquivo+'"!', s) then Exit;
  //if not MLAGeral.CNABRetRegistrosOK(BcoCod, FLengt, FLista, Arquivo) then Exit;


  //////////////////////////////////////////////////////////////////////////////


  // Cliente interno
  // Somente para bancos que informam o CNPJ ou CPF
  if BcoUse = 756 then
  begin
    // Pela Ag�ncia + C�digo do cedente no nome do arquivo
    //
    NomeArq := ExtractFileName(Arquivo);
    p := pos('_', NomeArq);
    //ShowMessage(IntToStr(p));
    if p > 0 then
    begin
      AgTxt := Copy(NomeArq, p-4, 4);
      CeTxt := Copy(NomeArq, p+1, 7);
      //ShowMessage(AgTxt + '/' + CeTxt);
      // Parei Aqui
      QrLocEnt2.Close;
      QrLocEnt2.Params[00].AsInteger := BcoRet;
      QrLocEnt2.Params[01].AsInteger := Geral.IMV(AgTxt);
      QrLocEnt2.Params[02].AsString  := CeTxt;
      QrLocEnt2.Open;
      //
      case QrLocEnt2.RecordCount of
        0: Application.MessageBox(PChar('N�o foi localizado cliente para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', BcoRet) + '"' + Char(13) + Char(10) +
        'Ag�ncia: "' + AgTxt + '"'  +
        Char(13) + Char(10) + 'C�digo do cedente: "'+ CeTxt + '"'),
        'Aviso', MB_OK+MB_ICONWARNING);
        1:
        begin
          // Parei Aqui
          GradeA.Cells[02, linA] := QrLocEnt2NOMEENT.Value;
          GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
          GradeA.Cells[04, linA] := CodCedente;
          GradeA.Cells[09, linA] := IntToStr(QrLocEnt2CLIENTE.Value);
          GradeA.Cells[10, linA] := FormatFloat('000', BcoRet);
          GradeA.Cells[11, linA] := IntToStr(FLengt);
          Result := True;
        end;
        else Application.MessageBox(PChar('Foram localizados '+
        IntToStr(QrLocEnt2.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', BcoRet) + '"' + Char(13) + Char(10) +
        'Ag�ncia: "' + AgTxt + '"'  +
        Char(13) + Char(10) + 'C�digo do cedente: "'+ CeTxt + '"' +
        Char(13) + Char(10) + '! Para evitar erros nenhum foi '+ ' considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
      end;

    end else Application.MessageBox(PChar('Foi detectado que o arquivo "'+
    NomeArq + '" pertence ao banco ' = FormatFloat('000', BcoRet) +
    ', mas o nome n�o est� no formato indicado pelo banco!'), 'Erro',
    MB_OK+MB_ICONERROR);
  end else begin
    //CNPJ := MLAGeral.CNABRetCNPJ(BcoCod, FLengt, FLista, Arquivo);
    CNPJ := DadoLista(SQLCompl, FLista, 401, BcoUse, TipoRegI, TipoRegT);
    if CNPJ = '' then
    begin
      Application.MessageBox(PChar('N�o foi poss�vel obter o CNPJ no arquivo "'
      + Arquivo + '" onde consta:+ sLineBreak +
      'Banco: "' + IntToStr(BcoRet) + '"+ sLineBreak'Sem o CNPJ n�o � poss�vel '
      +'Definir o cliente interno!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //CodCedente := MLAGeral.CNABRetCodCedente(BcoCod, FLengt, FLista, Arquivo);
    CodCedente := DadoLinha(SQLCompl, FLista[0], 410, BcoUse);
    if (CNPJ = '') and (CodCedente = '') then
    begin
      Application.MessageBox(PChar('N�o foi poss�vel localizar o CNPJ e/ou o ' +
      'c�digo do cedente no arquivo "' + Arquivo+'" onde consta:+ sLineBreak +
      'Banco: "' + IntToStr(BcoRet) + '"+ sLineBreak'Sem estes dados n�o � poss�vel '
      +'definir o cliente interno!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    QrLocEnt1.Close;
    QrLocEnt1.Params[00].AsString := CNPJ;
    //QrLocEnt1.Params[01].AsString := CodCedente;
    QrLocEnt1.Open;
    case QrLocEnt1.RecordCount of
      0: Application.MessageBox(PChar('N�o foi localizado cliente para o arquivo "' +
      Arquivo+'" onde consta:' +Char(13) + Char(10) + 'CNPJ/CPF: "' +
      Geral.FormataCNPJ_TT(CNPJ) + '"' + Char(13) + Char(10) +
      //'C�digo do cedente: "'+ CodCedente + '"' + Char(13) + Char(10) +
      'Banco: "' + IntToStr(BcoRet) + '"'), 'Aviso', MB_OK+MB_ICONWARNING);
      1:
      begin
        GradeA.Cells[02, linA] := QrLocEnt1NOMEENT.Value;
        GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
        GradeA.Cells[04, linA] := CodCedente;
        GradeA.Cells[09, linA] := IntToStr(QrLocEnt1CLIENTE.Value);
        GradeA.Cells[10, linA] := FormatFloat('000', BcoRet);
        GradeA.Cells[11, linA] := IntToStr(FLengt);
        Result := True;
      end;
      else Application.MessageBox(PChar('Foram localizados '+
      IntToStr(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
      Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ)+
      //' e  c�digo de cedente '+ CodCedente +
      '! Para evitar erros nenhum foi '+ ' considerado!'),
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
  //








  Exit;



  // Quantidade de lotes no arquivo
  GradeA.Cells[05, linA] := MLAGeral.CNABRetQtdeLotes(BcoUse, FLengt, FLista, Arquivo);
  //
  (*
  case BcoCod of
    748: Result := CarregaItensRetornoSicredi(Arquivo,
                   QrCNAB_DirCodigo.Value,
                   Geral.IMV(GradeA.Cells[00, GradeA.RowCount-1]),
                   BcoCod, QrLocEntCLIENTE.Value);
    else
    begin
      Result := False;
    end;
  end;
  *)
end;

procedure TFmCNAB_Ret.Grade1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(Grade1.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 12 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 13 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 14 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 15 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 16 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 17 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 20 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 21 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 22 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 23 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 24 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 25 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 26 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 27 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 28 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end;
end;

procedure TFmCNAB_Ret.Grade1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  (*if ARow <> FActiveRowC then
    FActiveRowC := ARow;
  if ARow <> FActiveRowB then
    FActiveRowB := ARow;*)
  //ShowMessage(IntToStr(ACol));
  //
  if ACol in ([10,11,12,13,14]) then
    Grade1.Options := Grade1.Options + [goEditing]
  else
    Grade1.Options := Grade1.Options - [goEditing];
end;

procedure TFmCNAB_Ret.BtCarregaClick(Sender: TObject);
begin
  if GravaItens > 0 then
    MostraAbertos;
end;

function TFmCNAB_Ret.GravaItens: Integer;
var
  ID_Link, Codigo, Banco, SeuNum, OcorrCodi, Diretorio, ItemArq,
  i, n, a, IDNum, Entidade, Carteira, Dias, Erros, Cond, TamReg: Integer;
  OcorrData, QuitaData, Motivo1, Motivo2, Motivo3, Motivo4, Motivo5, Arquivo,
  Extensao, FileName, DestName, NossoNum, DtaTarif: String;
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta, DevJuros, DevMulta,
  TotJuros, ValJuMul, ValTarif, ValErro, ValOutro, TxaMulta, TxaJuros, JurMul: Double;
  DataPagto: TDateTime;
  Exclui, Continua, InfoJM: Boolean;
begin
  //tarifa de cobran�a -
  //fazer c�lculo de verifica��o de Val tit + jur + mul - tarifa - desco - abat = val pago
  Erros := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    if ErroLinha(i, True) then Inc(Erros, 1);
  end;
  if Erros > 0 then
  begin
    if Application.MessageBox(PChar('Existem ' + IntToStr(Erros) +
    ' diverg�ncias no arquivo "' + ExtractFileName(GradeA.Cells[1,
    Geral.IMV(Grade1.Cells[23, 1])]) + '".' + sLineBreak +
    'Como � calculado a diverg�ncia:' + sLineBreak +
    'Valor do t�tulo + Multa + Juros de mora + (Juros e multa somados) + ' +
    'Outros cr�ditos - Abatimentos - Descontos - Despesa de cobran�a (Tarifa) - ' +
    'Valor a creditar em conta corrente' + sLineBreak +
    'Deseja continuar assim mesmo?'), 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Result := 0;
      Exit;
    end;
  end;
  Exclui   := True;
  Codigo   := 0;
  Result   := 0;
  DevJuros := 0;
  DevMulta := 0;
  Dias     := 0;
  n        := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    if Geral.DMV(Grade1.Cells[1, i]) >= 1 then
      n := n + 1;
  end;
  if n = 0 then
  begin
    Application.MessageBox('N�o h� itens a serem gravados!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible := True;
  PB1.Max := Grade1.RowCount - 1 + GradeA.RowCount - 1;
  TamReg := Geral.IMV(GradeA.Cells[11, Geral.IMV(Grade1.Cells[23, 1])]);//FActiveRowA]);
  for a := 1 to Grade1.RowCount - 1 do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, a]);
    OcorrCodi := Geral.IMV(Grade1.Cells[02, a]);
    // Texto da ocorrencia                [03
    OcorrData := dmkPF.CDS(Grade1.Cells[04, a], 2, 1);
    SeuNum    := Geral.IMV(Grade1.Cells[05, a]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, a]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, a]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, a]);
    ValPago   := Geral.DMV(Grade1.Cells[09, a]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, a]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, a]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, a]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, a]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, a]);
    Motivo1   := Copy(Grade1.Cells[16, a], 1, 2);
    Motivo2   := Copy(Grade1.Cells[16, a], 3, 2);
    Motivo3   := Copy(Grade1.Cells[16, a], 5, 2);
    Motivo4   := Copy(Grade1.Cells[16, a], 7, 2);
    Motivo5   := Copy(Grade1.Cells[16, a], 9, 2);
    // Texto dos motivos da ocorrencia    [13
    QuitaData := dmkPF.CDS(Grade1.Cells[18, a], 2, 1);
    DtaTarif  := dmkPF.CDS(Grade1.Cells[19, a], 2, 1);
    Diretorio := Geral.IMV(Grade1.Cells[22, a]);
    Arquivo   := ExtractFileName(GradeA.Cells[1, StrToInt(Grade1.Cells[22, a])]);
    ItemArq   := Geral.IMV(Grade1.Cells[24, a]);
    Banco     := Geral.IMV(Grade1.Cells[25, a]);
    Entidade  := Geral.IMV(Grade1.Cells[20, a]);
    ID_Link   := Geral.IMV(Grade1.Cells[21, a]);
    Carteira  := QrCNAB_DirCarteira.Value;
    //
    if not UBancos.SeparaJurosEMulta(Banco) then
    begin
      // N�o separa Juros e multa no arquivo.
      // Fazer manual
      if (ValPago > ValTitul) and (ValJuros + ValMulta = 0) then
      begin
        if CondDeEntidade(Entidade, Cond) then
        begin
          ValMulta := Round(QrLocCondPercMulta.Value * ValTitul) / 100;
          JurMul := ValPago - ValTitul;
          if ValMulta > JurMul then
          begin
            ValMulta := JurMul;
            ValJuros := 0;
          end else begin
            ValJuros := JurMul - ValMulta;
            if ValJuros < 0 then
            begin
              // precau��o
              ValMulta := ValMulta + ValJuros;
              ValJuros := 0;
            end;
          end;
        end;
      end;
    end;
    Continua := UBancos.BancoTemEntidade(Banco);
    if not Continua then Exit;
    Continua := OcorrCodi = UBancos.CodigoLiquidacao(Banco, TamReg);
    if not Continua then
    begin
      Continua := OcorrCodi = UBancos.CodigoTarifa(Banco);
      // Tentativa de eliminar a tabela BancosLei
      {QrBcocor.Close;
      QrBcocor.Params[00].AsInteger := Banco;
      QrBcocor.Params[01].AsString  := Grade1.Cells[02, a];
      QrBcocor.Open;

      if QrBcocor.RecordCount = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end else Continua := QrBcocorCarrega.Value <> 0;}
      if not Continua then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end;
    end;
    if Continua then
    begin
      IDNum := ID_Link;
      //
      if IDNum = 0 then
      begin
        Exclui := False;
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Banco ' +
          FormatFloat('000', Banco) + ' - n�o implementado');
        Application.ProcessMessages;
      end else begin
        QrDupl.Close;
        QrDupl.Params[00].AsInteger := Banco;
        QrDupl.Params[01].AsString  := NossoNum;
        QrDupl.Params[02].AsInteger := SeuNum;
        QrDupl.Params[03].AsInteger := OcorrCodi;
        QrDupl.Params[04].AsString  := Arquivo;
        QrDupl.Params[05].AsInteger := ItemArq;
        QrDupl.Params[06].AsFloat   := ValTitul;
        QrDupl.Params[07].AsFloat   := ValPago;
        QrDupl.Params[08].AsString  := QuitaData;
        QrDupl.Open;
        if QrDupl.RecordCount > 0 then
        begin
          if CkReverter.Checked then
          begin
            Dmod.QrUpd2.SQL.Clear;
            Dmod.QrUpd2.SQL.Add('UPDATE cnab_lei SET Step=0 WHERE Codigo=:P0');
            Dmod.QrUpd2.Params[0].AsInteger := QrDuplCodigo.Value;
            Dmod.QrUpd2.ExecSQL;
          end else begin
            PageControl1.ActivePageIndex := 1;
            Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
              ' do arquivo '+Arquivo+' j� foi registrado anteriormente!');
            Application.ProcessMessages;
          end;
        end else
        begin
          Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB,
            'Livres', 'Controle', 'CNAB_Lei', 'CNAB_Lei', 'Codigo');
          //
          DevJuros := 0;
          DevMulta := 0;
          if OcorrCodi = UBancos.CodigoLiquidacao(Banco, TamReg) then
          begin
            QrPesq3.Close;
            QrPesq3.Params[00].AsInteger := ID_Link;
            QrPesq3.Params[01].AsInteger := Entidade;
            QrPesq3.Open;
            DataPagto := Geral.ValidaDataSimples(Grade1.Cells[04, a], True);
            if QrPesq3Vencimento.Value < DataPagto then
              Dias := UMyMod.DiasUteis(QrPesq3Vencimento.Value + 1, DataPagto);
            if Dias > 0 then
            begin
              TotJuros := dmkPF.CalculaJuroSimples(QrPesq3PercJuros.Value,
              DataPagto - QrPesq3Vencimento.Value);
              DevJuros := Round(TotJuros * QrPesq3Credito.Value) / 100;
              DevMulta := Round(QrPesq3PercMulta.Value * QrPesq3Credito.Value) / 100;
            end;
          end;
          //
          // Dve ser aqui para evitar erros
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO cnab_lei SET ');
          Dmod.QrUpd.SQL.Add('Codigo=:P0, Banco=:P1, NossoNum=:P2, SeuNum=:P3, ');
          Dmod.QrUpd.SQL.Add('OcorrCodi=:P4, OcorrData=:P5, ValTitul=:P6, ');
          Dmod.QrUpd.SQL.Add('ValAbati=:P7, ValDesco=:P8, ValPago=:P9, ');
          Dmod.QrUpd.SQL.Add('ValJuros=:P10, ValMulta=:P11, Motivo1=:P12, ');
          Dmod.QrUpd.SQL.Add('Motivo2=:P13, Motivo3=:P14, Motivo4=:P15, ');
          Dmod.QrUpd.SQL.Add('Motivo5=:P16, QuitaData=:P17, Diretorio=:P18, ');
          Dmod.QrUpd.SQL.Add('Arquivo=:P19, ItemArq=:P20, IDNum=:P21, Entidade=:P22, ');
          Dmod.QrUpd.SQL.Add('Carteira=:P23, DevJuros=:P24, DevMulta=:P25, ');
          Dmod.QrUpd.SQL.Add('ID_Link=:P26, ValJuMul=:P27, ValTarif=:P28, ');
          Dmod.QrUpd.SQL.Add('ValOutro=:P29, DtaTarif=:P30, TamReg=:P31');
          //
          Dmod.QrUpd.Params[00].AsInteger := Codigo;
          Dmod.QrUpd.Params[01].AsInteger := Banco;
          Dmod.QrUpd.Params[02].AsString  := NossoNum;
          Dmod.QrUpd.Params[03].AsInteger := SeuNum;
          Dmod.QrUpd.Params[04].AsInteger := OcorrCodi;
          Dmod.QrUpd.Params[05].AsString  := OcorrData;
          Dmod.QrUpd.Params[06].AsFloat   := ValTitul;
          Dmod.QrUpd.Params[07].AsFloat   := ValAbati;
          Dmod.QrUpd.Params[08].AsFloat   := ValDesco;
          Dmod.QrUpd.Params[09].AsFloat   := ValPago;
          Dmod.QrUpd.Params[10].AsFloat   := ValJuros;
          Dmod.QrUpd.Params[11].AsFloat   := ValMulta;
          Dmod.QrUpd.Params[12].AsString  := Motivo1;
          Dmod.QrUpd.Params[13].AsString  := Motivo2;
          Dmod.QrUpd.Params[14].AsString  := Motivo3;
          Dmod.QrUpd.Params[15].AsString  := Motivo4;
          Dmod.QrUpd.Params[16].AsString  := Motivo5;
          Dmod.QrUpd.Params[17].AsString  := QuitaData;
          Dmod.QrUpd.Params[18].AsInteger := Diretorio;
          Dmod.QrUpd.Params[19].AsString  := Arquivo;
          Dmod.QrUpd.Params[20].AsInteger := ItemArq;
          Dmod.QrUpd.Params[21].AsInteger := IDNum;
          Dmod.QrUpd.Params[22].AsInteger := Entidade;
          Dmod.QrUpd.Params[23].AsInteger := Carteira;
          Dmod.QrUpd.Params[24].AsFloat   := DevJuros;
          Dmod.QrUpd.Params[25].AsFloat   := DevMulta;
          Dmod.QrUpd.Params[26].AsInteger := ID_Link;
          Dmod.QrUpd.Params[27].AsFloat   := ValJuMul;
          Dmod.QrUpd.Params[28].AsFloat   := ValTarif;
          Dmod.QrUpd.Params[29].AsFloat   := ValOutro;
          Dmod.QrUpd.Params[30].AsString  := DtaTarif;
          Dmod.QrUpd.Params[31].AsInteger := TamReg;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
    end;
  end;

  //

  Arquivo   := ExtractFileName(GradeA.Cells[1, FActiveRowA]);
  Extensao  := '';
  FileName := MLAGeral.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
  if Exclui then
  begin
    if FileExists(FileName) then
    begin
      //ShowMessage('Arquivo: '+FileName);
      DestName := MLAGeral.CaminhoArquivo(QrCNAB_DirNome.Value, 'Lidos', '');
      ForceDirectories(DestName);
      //ShowMessage('Destino: '+DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
      //
    end;
  end;
  //

  PB1.Visible := False;
  Screen.Cursor := crDefault;
  Result := Codigo;

  //
  // Recarrega arquivos
  Timer1.Enabled := True;

end;

procedure TFmCNAB_Ret.BtAbertosClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraAbertos;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Ret.MostraAbertos;
begin
  FAbertosCli := 0;
  QrClientes.Close;
  QrClientes.Open;
  case QrClientes.RecordCount of
    0: Application.MessageBox('N�o h� itens abertos!', 'Aviso', MB_OK+MB_ICONWARNING);
    1: FAbertosCli := QrClientesCodigo.Value;
    else begin
      Application.CreateForm(TFmSelCod, FmSelCod);
      FmSelCod.Caption := 'Cliente Interno';
      FmSelCod.LaPrompt.Caption := 'Cliente Interno';
      (*FmSelCod.QrSel.Close;
      FmSelCod.QrSel.SQL.Clear;
      FmSelCod.QrSel.SQL.Add('SELECT CASE WHEN Tipo=0 THEN RazaoSocial');
      FmSelCod.QrSel.SQL.Add('ELSE Nome END Descricao, Codigo');
      FmSelCod.QrSel.SQL.Add('FROM entidades');
      FmSelCod.QrSel.SQL.Add('WHERE Fornece1="V" OR Fornece2="V"');
      FmSelCod.QrSel.SQL.Add('OR Fornece3="V" OR Fornece4="V"');
      FmSelCod.QrSel.SQL.Add('OR Fornece5="V" OR Fornece5="V"');
      FmSelCod.QrSel.SQL.Add('ORDER BY Descricao');
      FmSelCod.QrSel.Open;*)
      FmSelCod.CBSel.ListSource := nil;
      FmSelCod.CBSel.ListField := 'NOMEENT';
      FmSelCod.CBSel.ListSource := DsClientes;
      //
      FmSelCod.ShowModal;
      FmSelCod.Destroy;
      FAbertosCli := VAR_SELCOD;
    end;
  end;
  if FAbertosCli > 0 then
  begin
    Application.ProcessMessages;
    //
    PnMovimento.Visible := True;
    PnCarrega.Visible := False;
    Application.ProcessMessages;
    //
    ReopenCNAB0Lei(0);
  end;
end;

procedure TFmCNAB_Ret.ReopenCNAB0Lei(Codigo: Integer);
begin
  QrLei.Close;
  QrLei.Params[0].AsInteger := FAbertosCli;
  QrLei.Open;
  //
  QrLei.Locate('Codigo', Codigo, []);
end;

procedure TFmCNAB_Ret.QrLeiAfterScroll(DataSet: TDataSet);
begin
  ReopenLeiItens(0);
  ReopenLeiAgr(0, 0, 0, 0, 0);
end;

procedure TFmCNAB_Ret.ReopenLeiAgr(Data: TDateTime;
  CliInt, ForneceI, Apto, Mez: Integer);
begin
  QrLeiAgr.Close;
  QrLeiAgr.Params[00].AsInteger := QrLeiIDNum.Value;
  QrLeiAgr.Params[01].AsInteger := QrLeiEntidade.Value;
  QrLeiAgr.Open;
  //
  QrLeiAgr.Locate('Data;CliInt;ForneceI;Apto;Mez',
    VarArrayOf([Data, CliInt, ForneceI, Apto, Mez]), []);
end;

procedure TFmCNAB_Ret.ReopenLeiItens(Controle: Integer);
begin
  QrLeiItens.Close;
  // Liquida��o
  if (Geral.IMV(QrLeiOcorrCodi.Value) =
  UBancos.CodigoLiquidacao(QrLeiBanco.Value, QrLeiTamReg.Value))
  // vale para bancos 001 e 748; mais algum?

  //Todos?
  {and (
       (QrLeiBanco.Value = 1)
    or (QrLeiBanco.Value = 748)
      )}
  then
  begin
    QrLeiItens.Params[00].AsInteger := QrLeiIDNum.Value;
    QrLeiItens.Params[01].AsInteger := QrLeiEntidade.Value;
    QrLeiItens.Open;
    QrLeiItens.Locate('Controle', Controle, []);
    //
  end;
  BtAgenda.Enabled := QrLeiDJM.Value < 0;
end;

procedure TFmCNAB_Ret.QrLeiBeforeClose(DataSet: TDataSet);
begin
  QrLeiItens.Close;
  BtConcilia.Enabled := False;
end;

procedure TFmCNAB_Ret.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmCNAB_Ret.ExcluiAtual1Click(Sender: TObject);
begin
  ExcluirItens(istAtual);
end;

procedure TFmCNAB_Ret.ExcluiSelecionados1Click(Sender: TObject);
begin
  ExcluirItens(istSelecionados);
end;

procedure TFmCNAB_Ret.ExcluiTodos1Click(Sender: TObject);
begin
  ExcluirItens(istTodos);
end;

procedure TFmCNAB_Ret.ExcluiBloquetosnolocalizados1Click(Sender: TObject);
begin
  ExcluirItens(istExtra1);
end;

procedure TFmCNAB_Ret.ExcluirItens(Acao: TSelType);
  procedure ExcluiAtual;
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cnab_lei WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  if Acao = istSelecionados then
    if DBGrid6.SelectedRows.Count < 2 then Acao := istAtual;
  case Acao of
    istAtual:
    begin
      if Application.MessageBox(PChar('Confirma a exclus�o do item selecionado?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then ExcluiAtual;
    end;
    istSelecionados:
    begin
      if Application.MessageBox(PChar('Confirma a exclus�o dos ' + IntToStr(
      DBGrid6.SelectedRows.Count) + ' itens selecionados?'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with DBGrid6.DataSource.DataSet do
        for i:= 0 to DBGrid6.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGrid6.SelectedRows.Items[i]));
          ExcluiAtual;
        end;
      end;
    end;
    istTodos:
    begin
      if Application.MessageBox('Confirma a exclus�o de todos itens ?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
    istExtra1:
    begin
      if Application.MessageBox('Confirma a exclus�o de todos boletos n�o localizados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          if (QrLeiItens.State = dsInactive)
          or (QrLeiItens.RecordCount = 0) then
            ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
  end;
  //
  Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
  ReopenCNAB0Lei(Prox);
end;

procedure TFmCNAB_Ret.QrLeiAfterOpen(DataSet: TDataSet);
begin
    BtConcilia.Enabled := QrLei.RecordCount > 0;
end;

procedure TFmCNAB_Ret.BtConciliaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConcilia, BtConcilia);
end;

procedure TFmCNAB_Ret.ConciliaAtual1Click(Sender: TObject);
begin
  ConciliaItens(istAtual);
end;

procedure TFmCNAB_Ret.ConciliaSelecionados1Click(Sender: TObject);
begin
  ConciliaItens(istSelecionados);
end;

procedure TFmCNAB_Ret.ConciliaTodos1Click(Sender: TObject);
begin
  ConciliaItens(istTodos);
end;

procedure TFmCNAB_Ret.ConciliaItens(Acao: TSelType);
  function EntidadeDeBanco(var EntBanco: Integer; const Banco: Integer): Boolean;
  begin
    QrLEB.Close;
    QrLEB.Params[0].AsInteger := Banco;
    QrLEB.Open;
    EntBanco := QrLEBEntidade.Value;
    Result := EntBanco > 0;
    if not Result then Application.MessageBox(PChar('Entidade banc�ria n�o ' +
    'localizada para o banco: '+FormatFloat('000', Banco)+'!'), 'Erro',
    MB_OK+MB_ICONERROR);
    if not Result then Screen.Cursor := crDefault;
  end;
  function UnidadeHabitacional(var Dono: Integer; var Unidade: Integer;
    const IDNum: Integer; const TipoBol: Double): Boolean;
  begin
    if TipoBol < 610 then
    begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM arreits');
      QrLUH.SQL.Add('WHERE Boleto=' + IntToStr(IDNum));
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('UNION');
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM consits');
      QrLUH.SQL.Add('WHERE Boleto=' + IntToStr(IDNum));
      QrLUH.SQL.Add('');
      //QrLUH.Params[0].AsInteger := IDNum;
      //QrLUH.Params[1].AsInteger := IDNUm;
    end else begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT bp.CodigoEnt Propriet, bp.CodigoEsp Apto');
      QrLUH.SQL.Add('FROM bloqparcpar bpp');
      QrLUH.SQL.Add('LEFT JOIN bloqparc bp ON bp.Codigo=bpp.Codigo');
      QrLUH.SQL.Add('LEFT JOIN lanctos lan ON lan.FatNum=bpp.FatNum');
      QrLUH.SQL.Add('  AND lan.ForneceI=bp.CodigoEnt');
      QrLUH.SQL.Add('  AND lan.Depto=bp.CodigoEsp');
      QrLUH.SQL.Add('WHERE lan.Sit < 2');
      QrLUH.SQL.Add('AND lan.Tipo=2');
      QrLUH.SQL.Add('AND bpp.FatNum=' + IntToStr(IDNum));
    end;
    QrLUH.Open;
    Unidade := QrLUHApto.Value;
    Dono := QrLUHPropriet.Value;
    Result := (Unidade > 0) and (Dono <> 0);
    if not Result then
    begin
      Unidade := QrLeiItensApto.Value;
      Dono    := QrLeiItensForneceI.Value;
      Result := (Unidade > 0) and (Dono <> 0);
      if not Result then
      begin
        Unidade := QrLeiAgrApto.Value;
        Dono    := QrLeiAgrForneceI.Value;
        Result := (Unidade > 0) and (Dono <> 0);
      end;
    end;
    if not Result then Application.MessageBox(PChar('Unidade habitacional n�o ' +
    'localizada para o "IDNum": '+IntToStr(IDNum)+'!'), 'Erro',
    MB_OK+MB_ICONERROR);
    if not Result then Screen.Cursor := crDefault;
  end;
  procedure ConciliaAtual;
    {function ContaDaOcorrencia(const Banco: Integer;
      const Ocorrencia: String; var Genero: Integer): Boolean;
    begin
      Genero := 0;
      QrLocOcor.Close;
      QrLocOcor.Params[0].AsInteger := Banco;
      QrLocOcor.Params[1].AsInteger := Geral.IMV(Ocorrencia);
      QrLocOcor.Open;
      if QrLocOcor.RecordCount = 0 then Application.MessageBox(PChar(
      'N�o h� cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!'), 'Aviso',
      MB_OK+MB_ICONWARNING) else if QrLocOcorGenero.Value < 1 then
      Application.MessageBox(PChar('N�o foi definida nenhuma conta no ' +
      'cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!'), 'Aviso',
      MB_OK+MB_ICONWARNING) else Genero := QrLocOcorGenero.Value;
      Result := Genero > 0;
      if not Result then Screen.Cursor := crDefault;
      // N�o fechar QrLocOcor !!!!
      //QrLocOcor.Close;
    end;}
    function QuitaDocumento(Credito, MoraVal, MultaVal: Double): Integer;
    var
      Controle2: Integer;
      Compensado, PagoBanco: String;
    begin
      Result := 0;
      PagoBanco := Geral.FDT(QrLeiOcorrData.Value, 1);
      Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
      //
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', 'Lanctos', 'Lanctos', 'Controle');
      UFinanceiro.LancamentoDefaultVARS;
      //

      // CNAB_Sit = 2 -> Baixa pelo CANB
      FLAN_Tipo           := 1;
      FLAN_Sit            := 3;
      FLAN_CNAB_Sit       := 2;
      //
      FLAN_Data           := Compensado;
      FLAN_Controle       := Controle2;
      FLAN_Descricao      := QrLeiItensDescricao.Text;
      FLAN_NotaFiscal     := QrLeiItensNotaFiscal.Value;
      FLAN_Debito         := QrLeiItensDebito.Value;
      FLAN_Credito        := Credito;
      FLAN_Compensado     := PagoBanco; // antes de compensar (D+ do banco)
      FLAN_SerieCH        := QrLeiItensSerieCH.Value;
      FLAN_Documento      := Trunc(QrLeiItensDocumento.Value + 0.01);
      FLAN_Cliente        := QrLeiItensCliente.Value;
      FLAN_Fornecedor     := QrLeiItensFornecedor.Value;
      FLAN_ID_Pgto        := QrLeiItensControle.Value;
      FLAN_Sub            := QrLeiItensSub.Value;
      FLAN_DataCad        := FormatDateTime(VAR_FORMATDATE, Date);
      FLAN_UserCad        := VAR_USUARIO;
      FLAN_DataDoc        := FormatDateTime(VAR_FORMATDATE, QrLeiItensDataDoc.Value);
      FLAN_Vencimento     := FormatDateTime(VAR_FORMATDATE, QrLeiItensVencimento.Value);
      FLAN_Carteira       := QrLeiCarteira.Value;
      FLAN_CliInt         := QrLeiItensCliInt.Value;
      FLAN_ForneceI       := QrLeiItensForneceI.Value;
      FLAN_Depto          := QrLeiItensApto.Value;
      FLAN_Genero         := QrLeiItensGenero.Value;
      FLAN_MoraVal        := MoraVal;
      FLAN_MultaVal       := MultaVal;
      FLAN_Mez            := Geral.TFT_NULL(IntToStr(QrLeiItensMez.Value), 0, siNegativo);
      //
      {
      Dmod.QrUpdM.Params[00].AsString  := Compensado;
      Dmod.QrUpdM.Params[01].AsFloat   := Controle2;
      Dmod.QrUpdM.Params[02].AsString  := QrLeiItensDescricao.Text;
      Dmod.QrUpdM.Params[03].AsInteger := QrLeiItensNotaFiscal.Value;
      Dmod.QrUpdM.Params[04].AsFloat   := QrLeiItensDebito.Value;
      Dmod.QrUpdM.Params[05].AsFloat   := Credito;
      Dmod.QrUpdM.Params[06].AsString  := PagoBanco; // antes de compensar (D+ do banco)
      Dmod.QrUpdM.Params[07].AsString  := QrLeiItensSerieCH.Value;
      Dmod.QrUpdM.Params[08].AsFloat   := QrLeiItensDocumento.Value;
      Dmod.QrUpdM.Params[09].AsInteger := QrLeiItensCliente.Value;
      Dmod.QrUpdM.Params[10].AsInteger := QrLeiItensFornecedor.Value;
      Dmod.QrUpdM.Params[11].AsFloat   := QrLeiItensControle.Value;
      Dmod.QrUpdM.Params[12].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpdM.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[14].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[15].AsString  := FormatDateTime(VAR_FORMATDATE, QrLeiItensDataDoc.Value);
      Dmod.QrUpdM.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, QrLeiItensVencimento.Value);
      Dmod.QrUpdM.Params[17].AsInteger := QrLeiCarteira.Value;
      Dmod.QrUpdM.Params[18].AsInteger := QrLeiItensCliInt.Value;
      Dmod.QrUpdM.Params[19].AsInteger := QrLeiItensForneceI.Value;
      Dmod.QrUpdM.Params[20].AsInteger := QrLeiItensApto.Value;
      Dmod.QrUpdM.Params[21].AsInteger := QrLeiItensGenero.Value;
      Dmod.QrUpdM.Params[22].AsFloat   := MoraVal;
      Dmod.QrUpdM.Params[23].AsFloat   := MultaVal;
      Dmod.QrUpdM.Params[24].AsInteger := QrLeiItensMez.Value;
      //
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.InsereLancamento;
      Result := Result + 1;

      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lanctos SET AlterWeb=1, Sit=3, ');
      Dmod.QrUpdM.SQL.Add('Compensado=:P0, DataAlt=:P1, UserAlt=:P2');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Sub=:P4 AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := Compensado;
      Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[03].AsFloat   := QrLeiItensControle.Value;
      Dmod.QrUpdM.Params[04].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpdM.ExecSQL;
      Result := Result + 2;
      //
    end;
  //
  var
    Dono, Unidade, Ocorrencia, Genero, EntBanco, SomaQ, Cond: Integer;
    Fator, SomaVal, Valor, Multa, Juros, Diferenca, FatorM{, FatorJ}, ValTarif: Double;
    InfoTar: Boolean;
    OcorrTxt: String;
  begin
    if not EntidadeDeBanco(EntBanco, QrLeiBanco.Value) then Exit;
    if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value, QrLeiTIPO_BOL.Value) then Exit;

    //
    // Verifica antes de conciliar se a conta da ocorr�ncia
    // j� est� configurada
    InfoTar := UBancos.InformaTarifaDeCobranca(QrLeiBanco.Value);
    // Quando a tarifa vem na mesma linha do pagamento ...
    if (QrLeiValTarif.Value > 0)
    // ou quando o banco (756) n�o informa no arquivo mas � informado
    // o valor no cadastro do condom�nio (QrCond)
    or (InfoTar = False) then
    begin
      Genero := 0;
      OcorrTxt := QrLeiOcorrCodi.Value;//-1;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
      QrLeiTamReg.Value, OcorrTxt, Genero) then  Exit;
    end;
    //

    Screen.Cursor := crHourGlass;
    Ocorrencia := Geral.IMV(QrLeiOcorrCodi.Value);
    EntBanco := 0;
    if Ocorrencia =
    UBancos.CodigoLiquidacao(QrLeiBanco.Value, QrLeiTamReg.Value) then
    begin
      if QrLeiVALBOLETO.Value <= 0 then
      begin
        Application.MessageBox(PChar('Valor do boleto zerado para o IDNum = '+
        IntToStr(QrLeiIDNum.Value)+'!'), 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
      if (QrLeiVALBOLETO.Value <> QrLeiValTitul.Value)
      and (QrLeiValTitul.Value > 0) then
      begin
        Application.MessageBox(PChar('Valores n�o conferem para o IDNum = '+
        IntToStr(QrLeiIDNum.Value)+'!' + sLineBreak +
        'Se este bloqueto foi re-gerado pela internet com juros e multa, ' +
        'utilize a op��o "Ajusta valores do bloqueto atual" do bot�o ' +
        '"Bloqueto" antes de conciliar!'), 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
      Fator := UBancos.FatorDeRecebimento(QrLeiBanco.Value,
        QrLeiValPago.Value, QrLeiValTarif.Value, QrLeiVALBOLETO.Value);
      QrLeiItens.First;
      SomaVal := 0;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        SomaVal  := SomaVal + Valor;
        //
        QrLeiItens.Next;
      end;
      Diferenca := UBancos.DiferencaDeRecebimento(QrLeiBanco.Value,
        QrLeiValPago.Value, QrLeiValTarif.Value, SomaVal);
      {Application.MessageBox(PChar('Diferen�a: '+Geral.FFT(
        Diferenca, 4, siNegativo)), 'Mensagem',
        MB_OK+MB_ICONINFORMATION);}
      //
      if QrLeiValMulta.Value < 0.01 then FatorM := 0 else
        FatorM := UBancos.FatorMultaDeRecebimento(QrLeiBanco.Value,
          QrLeiValPago.Value, QrLeiValTarif.Value, QrLeiValMulta.Value);
      //
      SomaQ := 0;
      QrLeiItens.First;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        if QrLeiItens.RecNo = 1 then Valor := Valor + Diferenca;

        //
        Multa := 0;
        Juros := 0;
        // Quando juros e multa s�o juntos
        if QrLeiValJuMul.Value >= 0.01 then
        begin
          Multa := Round(QrLeiItensCredito.Value * QrLeiItensMulta.Value) / 100;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
          if Juros  < 0 then
          begin
            Juros := 0;
            Multa := Valor - QrLeiItensCredito.Value;
          end;
        end else
        if  (QrLeiValMulta.Value >= 0.01)
        and (QrLeiValJuros.Value >= 0.01) then
        begin
          Multa := Valor * FatorM;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
        end else if QrLeiValMulta.Value <> 0 then
          Multa := Valor - QrLeiItensCredito.Value
        else
          Juros := Valor - QrLeiItensCredito.Value;
        //
        SomaQ := SomaQ + QuitaDocumento(Valor, Juros, Multa);
        QrLeiItens.Next;
      end;
      //

      // INSERE TARIFA DE COBRAN�A BANC�RIA
      //InfoTar := UBancos.InformaTarifaDeCobranca(QrLeiBanco.Value);
      // Quando a tarifa vem na mesma linha do pagamento ...
      if (QrLeiValTarif.Value > 0)
      // ou quando o banco (756) n�o informa no arquivo mas � informado
      // o valor no cadastro do condom�nio (QrCond)
      or (InfoTar = False) then
      begin
        Genero := 0;
        OcorrTxt := QrLeiOcorrCodi.Value;//-1;
        Unidade := 0;
        Dono    := 0;
        if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
        QrLeiTamReg.Value, OcorrTxt, Genero) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        //fatid: como saber? pode ter varios!
        // J� feito acima
        {
        if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        QrLeiTIPO_BOL.Value) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        }
        QrLocCta.Close;
        QrLocCta.Params[0].AsInteger := Genero;
        QrLocCta.Open;
        if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
        FLAN_Mez :=
          IntToStr(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
        //
        if QrLeiDtaTarif.Value > 0 then
        begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end else begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end;
        if InfoTar then ValTarif := QrLeiValTarif.Value else
        begin
        //if
          CondDeEntidade(QrLeiEntidade.Value, Cond); //then
          ValTarif := QrLocCondVTCBBNITAR.Value;
        end;
        FLAN_Descricao  := QrLeiDescriCNR.Value;
        FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
        //FLAN_Duplicata  := '';
        FLAN_Doc2       := IntToStr(QrLeiIDNum.Value);
        //FLAN_Serie      := '';
        FLAN_Documento  := QrLeiIDNum.Value;
        FLAN_Tipo       := QrLeiTipoCart.Value;
        FLAN_Carteira   := QrLeiCarteira.Value;
        FLAN_Genero     := Genero;
        //FLAN_NotaFiscal := 0;
        FLAN_Sit        := 3;
        FLAN_Controle   := 0;
        //
        FLAN_Credito    := 0;
        FLAN_Cliente    := 0;
        //
        FLAN_Debito     := ValTarif;
        FLAN_Fornecedor := EntBanco;
        //
        FLAN_UserCad    := VAR_USUARIO;
        FLAN_CliInt     := QrLeiEntidade.Value;
        FLAN_Depto      := Unidade;
        FLAN_ForneceI   := Dono;
        FLAN_FatID      := 0;
        FLAN_FatID_Sub  := 0;
        FLAN_FatNum     := 0;
        FLAN_FatParcela := 0;
        //
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
        //
        if UFinanceiro.InsereLancamento then ;
      end;

      if SomaQ = QrLeiItens.RecordCount * 3 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    end else begin
      Genero := 0;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value, QrLeiTamReg.Value,
      QrLeiOcorrCodi.Value, Genero) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;
      Unidade := 0;
      Dono    := 0;
      if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        QrLeiTIPO_BOL.Value) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;







      case Ocorrencia of
        028:
        begin
          case QrLeiBanco.Value of
            748:
            begin
              QrLocCta.Close;
              QrLocCta.Params[0].AsInteger := Genero;
              QrLocCta.Open;
              //
              if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
              FLAN_Mez :=
                IntToStr(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
              //
              FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_DataCad    := Geral.FDT(QrLeiOcorrData.Value, 1);

              FLAN_Descricao  := QrLeiDescriCNR.Value;
              FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
              //FLAN_Duplicata  := '';
              FLAN_Doc2       := IntToStr(QrLeiIDNum.Value);
              //FLAN_Serie      := '';
              FLAN_Documento  := QrLeiIDNum.Value;
              FLAN_Tipo       := QrLeiTipoCart.Value;
              FLAN_Carteira   := QrLeiCarteira.Value;
              FLAN_Genero     := Genero;
              //FLAN_NotaFiscal := 0;
              FLAN_Sit        := 3;
              FLAN_Controle   := 0;
              //
              FLAN_Credito    := 0;
              FLAN_Cliente    := 0;
              //
              FLAN_Debito     := QrLeiValTitul.Value;
              FLAN_Fornecedor := EntBanco;
              //
              FLAN_UserCad    := VAR_USUARIO;
              FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
              FLAN_CliInt     := QrLeiEntidade.Value;
              FLAN_Depto      := Unidade;
              FLAN_ForneceI   := Dono;
              FLAN_FatID      := 0;
              FLAN_FatID_Sub  := 0;
              FLAN_FatNum     := 0;
              FLAN_FatParcela := 0;
              //
              FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
                'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
              //
              if UFinanceiro.InsereLancamento then
              begin
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
                Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
                Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
                Dmod.QrUpd.ExecSQL;
              end;
            end;
            else Application.MessageBox(PChar('Este banco n�o possui a��o ' +
            'definida no aplicativo para a ocorr�cia informada!'), 'Aviso',
            MB_OK+MB_ICONWARNING);
          end;
        end else Application.MessageBox('Banco sem a��es definidas!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      end;
    end;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  VAR_NaoReabrirLanctos := True;
  try
    if Acao = istSelecionados then
      if DBGrid6.SelectedRows.Count < 2 then Acao := istAtual;
    case Acao of
      istAtual:
      begin
        if Application.MessageBox(PChar('Confirma a concilia��o do item selecionado?'),
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then ConciliaAtual;
      end;
      istSelecionados:
      begin
        if Application.MessageBox(PChar('Confirma a concilia��o dos ' + IntToStr(
        DBGrid6.SelectedRows.Count) + ' itens selecionados?'), 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          with DBGrid6.DataSource.DataSet do
          for i:= 0 to DBGrid6.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGrid6.SelectedRows.Items[i]));
            ConciliaAtual;
          end;
        end;
      end;
      istTodos:
      begin
        if Application.MessageBox('Confirma a concilia��o de todos itens ?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          QrLei.First;
          while not QrLei.Eof do
          begin
            ConciliaAtual;
            QrLei.Next;
          end;
        end;
      end;
    end;
  finally
    VAR_NaoReabrirLanctos := False;
    UFinanceiro.RecalcSaldoCarteira(QrLeiCarteira.Value, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(QrLeiItensCarteira.Value, nil, False, False);
    Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
    ReopenCNAB0Lei(Prox);
  end;
end;

procedure TFmCNAB_Ret.HabilitaBotoes;
begin
  QrTem.Close;
  QrTem.Open;
  BtCarrega.Enabled := QrTemItens.Value = 0;
  BtAbertos.Enabled := QrTemItens.Value > 0;
end;

procedure TFmCNAB_Ret.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Timer1.Enabled := True;
  //
  PnCarrega.Visible := True;
  PnMovimento.Visible := False;
  Application.ProcessMessages;
  //
  Screen.Cursor := crDefault;
end;

function TFmCNAB_Ret.CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
var
  Extensao, Arquivo, FileName: String;
  BcoCod, BcoRet, BcoUse, Posicoes: Integer;
begin
  Result := True;
  MLAGeral.LimpaGrade(Grade1, 1, 1);
  BcoCod   := Geral.IMV(GradeA.Cells[10, LinA]);
  Posicoes := Geral.IMV(GradeA.Cells[11, LinA]);
  case Posicoes of
    240:
    begin
      BcoRet := BcoCod;
      BcoUse := -1;
    end;
    400:
    begin
      BcoRet := BcoCod;
      BcoUse := BcoCod;
    end;
    else begin
      BcoRet := 0;
      BcoUse := 0;
    end;
  end;
  if GradeA.Cells[1,1] = '' then
  begin
    Result := False;
    Exit;
  end;
  //
  if BcoCod <> QrCNAB_DirBanco1.Value then
  begin
    Application.MessageBox(PChar('Banco informado no cadastro da carteira n�o ' +
    'confere com o banco informado no arquivo CNAB!' + sLineBreak +
    'Banco da carteira: ' + FormatFloat('000', QrCNAB_DirBanco1.Value) +
    sLineBreak + 'Banco do arquivo: ' + FormatFloat('000', BcoCod) +
    '     (linha '+IntToStr(LinA) + ' - Proced�ncia ' + IntToStr(QuemChamou) + ')'),
    'Aviso de diverg�ncia!', MB_OK+MB_ICONWARNING);
    //Exit;
  end;
  Arquivo   := Trim(ExtractFileName(GradeA.Cells[1, LinA]));
  if Arquivo <> '' then
  begin
    Extensao  := '';
    FileName := MLAGeral.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
    CarregaItensRetornoA(FileName, QrCNAB_DirCodigo.Value,
    Geral.IMV(GradeA.Cells[00, LinA]),
    Geral.IMV(GradeA.Cells[09, LinA]), Posicoes, BcoRet, BcoUse);
    {case BcoCod of
      748: Result := CarregaItensRetornoSicredi(FileName,
                     QrCNAB_DirCodigo.Value,
                     Geral.IMV(GradeA.Cells[00, LinA]),
                     BcoCod, Geral.IMV(GradeA.Cells[09, LinA]));
      else
      begin
        Result := False;
      end;
    end;}
  end else Result := False;
end;

procedure TFmCNAB_Ret.QrCNAB_DirAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  MLAGeral.LimpaGrade(GradeA, 1, 1);
  MLAGeral.LimpaGrade(Grade1, 1, 1);
  //MLAGeral.LimpaGrade(GradeB, 1, 1);
  //MLAGeral.LimpaGrade(GradeC, 1, 1);
  FLinA := 0;
  FLinB := 1;
  FLin1 := 0;
  {while not QrCNAB_Dir.Eof do
  begin}
    LeArquivos(QrCNAB_DirNome.Value, QrCNAB_DirCodigo.Value, FLinA);
    FLinA := GradeA.RowCount - 1;
    {QrCNAB_Dir.Next;
  end;}
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret.QrLeiItensCalcFields(DataSet: TDataSet);
begin
  QrLeiItensMez_TXT.Value := dmkPF.MezToFDT(QrLeiItensMez.Value, 0, 104);
end;

procedure TFmCNAB_Ret.QrLeiCalcFields(DataSet: TDataSet);
var
  TipoBol: Integer;
begin
  if Geral.IMV(QrLeiOcorrCodi.Value) =
  UBancos.CodigoLiquidacao(QrLeiBanco.Value, QrLeiTamReg.Value) then
    QrLeiDJM.Value := -((QrLeiDevJuros.Value + QrLeiDevMulta.Value) -
    (QrLeiValJuros.Value + QrLeiValMulta.Value + QrLeiValJuMul.Value ))
  else QrLeiDJM.Value := 0;

  QrLeiDTA_TARIF_TXT.Value := Geral.FDT(QrLeiDtaTarif.Value, 3);

  {
  if Trunc(QrLeiTIPO_BOL.Value) = 610 then
    QrLeiNOME_TIPO_BOL.Value := 'Reparcelamento'
  else if QrLeiTIPO_BOL.Value = 610 then
  }

  TipoBol := Trunc(QrLeiTIPO_BOL.Value * 100);
  case TipoBol of
    //00000..59999: QrLeiTIPO_BOL.Value := 'Desconhecido'
    60000: QrLeiNOME_TIPO_BOL.Value := 'Arrecadac�es';
    60001..
    60099: QrLeiNOME_TIPO_BOL.Value := 'Arrecadac�es e leituras';
    60100: QrLeiNOME_TIPO_BOL.Value := 'Leituras';
    61000: QrLeiNOME_TIPO_BOL.Value := 'Reparcelamentos';
      else QrLeiNOME_TIPO_BOL.Value := 'Desconhecido';
  end;
end;

function TFmCNAB_Ret.LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Application.MessageBox(PChar(Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;

function TFmCNAB_Ret.LocDado240(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Application.MessageBox(PChar(Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;

function TFmCNAB_Ret.CarregaItensRetornoA(Arquivo: String;
  SeqDir, SeqArq, Entidade, TamReg, Bcoret, BcoUse: Integer): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  //
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  BtCarrega.Enabled := FLista.Count > 0;
  MLAGeral.LimpaGrade(Grade1, 1, 1);
  //
  FLin1 := 0;
  //
  Memo2.Lines.Clear;
  Memo2.Lines.Add('Campo Linha Pos Tam Valor');
  //
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := BcoUse;
  QrBanco.Open;
  if TamReg = 400 then
  begin
    if (QrBancoID_400i.Value = 0) or (QrBancoID_400t.Value = 0) then
    begin
      Application.MessageBox(PChar('N�o foi definido o identificador de ' +
      'cobran�a CNAB400 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end else begin
    if (QrBancoID_240i.Value = 0) or (QrBancoID_240t.Value = 0) then
    begin
      Application.MessageBox(PChar('N�o foi definido o identificador de ' +
      'cobran�a CNAB240 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  //
  QrCampos.Close;
  QrCampos.Params[0].AsInteger := BcoUse;
  QrCampos.SQL[10] := 'AND T'+FormatFloat('0', TamReg)+'=1';
  //MLAGeral.LeMeuSQLy(QrCampos, '', nil, True, True);
  QrCampos.Open;
  if QrCampos.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum tipo de campo de registro '+
    'detalhe definido para o arquivo "'+ Arquivo + '" do banco '+
    FormatFloat('000', BcoUse) + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  case TamReg of
    240: CarregaItensRetorno240(BcoRet, BcoUse, Entidade, SeqDir, SeqArq);
    //240: CarregaArquivo240('?', 0);
    400: CarregaItensRetorno400(BcoUse, Entidade, SeqDir, SeqArq);
    else Application.MessageBox(PChar('Carregamento de itens de retorno n�o ' +
    'implementado para arquivo contendo ' + IntToStr(TamReg) + ' posi��es!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crDefault;
  {
  if nc > 0 then Application.MessageBox(PChar('Existem '+IntToStr(nc)+
  ' registros detalhe que n�o s�o cobran�a sem registro e foram DESCONSIDERADOS '+
  'na leitura do arquivo "'+Arquivo+'"!'), 'Aviso', MB_OK+MB_ICONWARNING);
  }
end;

procedure TFmCNAB_Ret.CarregaItensRetorno240(
  BcoRet, BcoUse, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  Registro, Segmento,
  SequeReg, ID_Link,  NossoNum, OcorrCod, OcorrDta, SeuNumer, PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutrC, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  OcorrTxt, NumDocum, TxtCBrut, TxtOutrD: String;
begin
  for i := 0 to FLista.Count - 1 do
  begin
    //ShowMessage(FLista[i]);
    // Tipo de registro
    LocDadoAll(670, i, '', Registro);
    LocDadoAll(671, i, '', Segmento);
    if (Geral.IMV(Registro) = QrBancoID_240r.Value) and
       (Segmento = QrBancoID_240s.Value) then
    begin
      //Abrir aqui somente registros do T
      QrF240T.Close;
      QrF240T.Params[0].AsInteger := BcoUse;
      QrF240T.Open;
      //
      inc(FLin1, 1);
      Grade1.RowCount := FLin1 + 1;
      Grade1.Cells[00, FLin1] := IntToStr(FLin1);

      // ID Link
      (*Mensagem := 'O identificador de cobran�a CNAB foi definido (' +
        IntToStr(QrBancoID_CNAB.Value) +'), mas ele n�o foi ' +
        'localizado no cadastro do banco ' + FormatFloat('000', Banco) + '!';
      {if not }LocDado240(QrBancoID_CNAB.Value, i, Mensagem, ID_Link) {then Exit};*)
      ID_Link := Trim(Copy(FLista[i], QrBancoID_240i.Value, QrBancoID_240t.Value));
      Grade1.Cells[21, FLin1] := ID_Link;

      // Nosso n�mero -> ID do registro no Sicredi e no Syndic
      LocDado240(501, i, '', NossoNum);
      Grade1.Cells[01, FLin1] := NossoNum;

      // Ocorr�ncia
      LocDado240(504, i, '', OcorrCod);
      Grade1.Cells[02, FLin1] := OcorrCod;
      // Texto da ocorr�ncia
      OcorrTxt :=
        MLAGeral.CNABTipoDeMovimento(BcoUse, 2, Geral.IMV(OcorrCod), 240);
      Grade1.Cells[03, FLin1] := OcorrTxt;


      // Data da ocorr�ncia
      LocDado240(505, i+1, '', OcorrDta);
      try
        if (OcorrDta = '000000') or (OcorrDta = '00000000') then
        begin
          Application.MessageBox(PChar('A data da ocorr�ncia "' +
          OcorrTxt + '" n�o foi definida (' + OcorrDta +') para o bloqueto ' +
          ID_Link + ' !'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end else
        Grade1.Cells[04, FLin1] := dmkPF.CDS2(OcorrDta, QrCamposFormato.Value, 3);
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data da ' +
        'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;

      // Seu Numero
      LocDado240(506, i, '', SeuNumer);
      Grade1.Cells[05, FLin1] := SeuNumer;

      // Documento (Texto do n�mero do documento no t�tulo)
      LocDado240(502, i, '', NumDocum);
      Grade1.Cells[05, FLin1] := NumDocum;

      // Valor do t�tulo
      LocDado240(550, i, '', TxtTitul);
      TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
      //ValTitul := Geral.DMV(TxtTitul);
      Grade1.Cells[06, FLin1] := TxtTitul;

      // Abatimento concedido
      LocDado240(551, i+1, '', TxtAbati);
      TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
      //ValAbati := Geral.DMV(TxtAbati);
      Grade1.Cells[07, FLin1] := TxtAbati;

      // Desconto concedido
      LocDado240(552, i+1, '', TxtDesco);
      TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
      //ValDesco := Geral.DMV(TxtDesco);
      Grade1.Cells[08, FLin1] := TxtDesco;

      // Valor efetivamente pago
      //LocDado240(553, i, '', TxtEPago);
      // Valor Total pago
      LocDado240(578, i+1, '', TxtEPago);
      TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtEPago);
      Grade1.Cells[09, FLin1] := TxtEPago;

      // Valor de cr�dito bruto
      LocDado240(579, i+1, '', TxtCBrut);
      TxtCBrut := dmkPF.XFT(TxtCBrut, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtCBrut);
      Grade1.Cells[27, FLin1] := TxtCBrut;

      // Valor de outros d�bitos
      LocDado240(585, i+1, '', TxtOutrD);
      TxtOutrD := dmkPF.XFT(TxtOutrD, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtOutrD);
      Grade1.Cells[28, FLin1] := TxtOutrD;

      //  N�O TEM
      // Juros de mora
      LocDado240(555, i, '', TxtJuros);
      TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
      //ValJuros := Geral.DMV(TxtJuros);
      Grade1.Cells[10, FLin1] := TxtJuros;

      //  N�O TEM
      // Multa
      LocDado240(556, i, '', TxtMulta);
      TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtMulta);
      Grade1.Cells[11, FLin1] := TxtMulta;

      //  N�O TEM
      // Outros cr�ditos
      LocDado240(554, i, '', TxtOutrC);
      TxtOutrC := dmkPF.XFT(TxtOutrC, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtOutrC);
      Grade1.Cells[12, FLin1] := TxtOutrC;

      // Juros de Mora e Multa (Somados)
      LocDado240(557, i+1, '', TxtJuMul);
      TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[13, FLin1] := TxtJuMul;

      // Tarifa (Despesa) de cobran�a
      LocDado240(570, i, '', TxtTarif);
      TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[14, FLin1] := TxtTarif;

      // ERRO Calculado no final
      //Grade1.Cells[15, FLin1] := ERRO;

      //Parei Aqui
      // Falta fazer (sem pressa)
      {
      // Motivo da ocorrencia
      Grade1.Cells[16, FLin1] := Copy(FLista[i], 319, 010);
      // Texto do motivo da ocorrencia
      Grade1.Cells[17, FLin1] := MLAGeral.CNABMotivosDeTipoDeMovimento28(
        Banco, Copy(FLista[i], 319, 010));
      }

      // Data de lancamento na conta corrente
      LocDado240(581, i+1, 'Data de lan�amento na conta corrente n�o definida!' +
      sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
      ID_Link + ' (Nosso n�mero = ' + NossoNum + ')', PagtoDta);
      try
        // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
        // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
        if PagtoDta = '' then
          Application.MessageBox(
          PChar('Data de lan�amento na conta corrente n�o definida!' +
          sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
          ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
          MB_OK+MB_ICONWARNING);
        Grade1.Cells[18, FLin1] := dmkPF.CDS2(PagtoDta, QrCamposFormato.Value, 3);
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
        'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // Data de d�bito da tarifa de cobranca
      //LocDado240(582, i+1, '', DtaTarif);
      LocDado240(505, i+1, '', DtaTarif);
      try
        Grade1.Cells[19, FLin1] := dmkPF.CDS2(DtaTarif, QrCamposFormato.Value, 3);
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
        'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // C�digo Condom�nio
      Grade1.Cells[20, FLin1] := IntToStr(Entidade);

      // Sequencia do diretorio
      Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

      // Sequencia do arquivo
      Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

      // N�mero sequencial do registro
      LocDado240(999, i, '', SequeReg);
      Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

      // Banco
      Grade1.Cells[25, FLin1] := FormatFloat('000', BcoRet);


      ErroLinha(FLin1, False);

      //Parei Aqui
      // Fazer rejei��o de t�tulos
    end;
  end;
end;

procedure TFmCNAB_Ret.CarregaItensRetorno400(
  Banco, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  SequeReg,
  ID_Link,  NossoNum, OcorrCod, {OcorrTxt,} OcorrDta, SeuNumer, {Mensagem,} PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutro, TxtEPago, TxtJuMul, TxtTarif, DtaTarif: String;
begin
  for i := 1 to FLista.Count -2 do
  begin
    inc(FLin1, 1);
    Grade1.RowCount := FLin1 + 1;
    Grade1.Cells[00, FLin1] := IntToStr(FLin1);

    // ID Link
    (*Mensagem := 'O identificador de cobran�a CNAB foi definido (' +
      IntToStr(QrBancoID_CNAB.Value) +'), mas ele n�o foi ' +
      'localizado no cadastro do banco ' + FormatFloat('000', Banco) + '!';
    {if not }LocDadoAll(QrBancoID_CNAB.Value, i, Mensagem, ID_Link) {then Exit};*)
    ID_Link := Trim(Copy(FLista[i], QrBancoID_400i.Value, QrBancoID_400t.Value));
    Grade1.Cells[21, FLin1] := ID_Link;

    // Nosso n�mero -> ID do registro no Sicredi e no Syndic
    LocDadoAll(501, i, '', NossoNum);
    Grade1.Cells[01, FLin1] := NossoNum;

    // Ocorr�ncia
    LocDadoAll(504, i, '', OcorrCod);
    Grade1.Cells[02, FLin1] := OcorrCod;

    // Texto da ocorr�ncia
    Grade1.Cells[03, FLin1] :=
      MLAGeral.CNABTipoDeMovimento(Banco, 2, Geral.IMV(OcorrCod), 400);

    // Data da ocorr�ncia
    LocDadoAll(505, i, '', OcorrDta);
    try
      Grade1.Cells[04, FLin1] := dmkPF.CDS2(OcorrDta, QrCamposFormato.Value, 3);
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data da ' +
      'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
      'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
    end;

    // Seu Numero
    LocDadoAll(506, i, '', SeuNumer);
    Grade1.Cells[05, FLin1] := SeuNumer;

    // Valor do t�tulo
    LocDadoAll(550, i, '', TxtTitul);
    TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
    //ValTitul := Geral.DMV(TxtTitul);
    Grade1.Cells[06, FLin1] := TxtTitul;

    // Abatimento concedido
    LocDadoAll(551, i, '', TxtAbati);
    TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
    //ValAbati := Geral.DMV(TxtAbati);
    Grade1.Cells[07, FLin1] := TxtAbati;

    // Desconto concedido
    LocDadoAll(552, i, '', TxtDesco);
    TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
    //ValDesco := Geral.DMV(TxtDesco);
    Grade1.Cells[08, FLin1] := TxtDesco;

    // Valor efetivamente pago
    LocDadoAll(553, i, '', TxtEPago);
    TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
    //ValEPago := Geral.DMV(TxtEPago);
    Grade1.Cells[09, FLin1] := TxtEPago;

    // Juros de mora
    LocDadoAll(555, i, '', TxtJuros);
    TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
    //ValJuros := Geral.DMV(TxtJuros);
    Grade1.Cells[10, FLin1] := TxtJuros;

    // Multa
    LocDadoAll(556, i, '', TxtMulta);
    TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtMulta);
    Grade1.Cells[11, FLin1] := TxtMulta;

    // Outros cr�ditos
    LocDadoAll(554, i, '', TxtOutro);
    TxtOutro := dmkPF.XFT(TxtOutro, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtOutro);
    Grade1.Cells[12, FLin1] := TxtOutro;

    // Juros de Mora e Multa (Somados)
    LocDadoAll(557, i, '', TxtJuMul);
    TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[13, FLin1] := TxtJuMul;

    // Tarifa (Despesa) de cobran�a
    LocDadoAll(570, i, '', TxtTarif);
    TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[14, FLin1] := TxtTarif;

    // ERRO Calculado no final
    //Grade1.Cells[15, FLin1] := ERRO;

    //Parei Aqui
    // Falta fazer (sem pressa)
    {
    // Motivo da ocorrencia
    Grade1.Cells[16, FLin1] := Copy(FLista[i], 319, 010);
    // Texto do motivo da ocorrencia
    Grade1.Cells[17, FLin1] := MLAGeral.CNABMotivosDeTipoDeMovimento28(
      Banco, Copy(FLista[i], 319, 010));
    }

    // Data de lancamento na conta corrente
    LocDadoAll(581, i, 'Data de lan�amento na conta corrente n�o definida!' +
    sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
    ID_Link + ' (Nosso n�mero = ' + NossoNum + ')', PagtoDta);
    try
      // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
      // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
      if PagtoDta = '' then
        Application.MessageBox(
        PChar('Data de lan�amento na conta corrente n�o definida!' +
        sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
        ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
        MB_OK+MB_ICONWARNING);
      Grade1.Cells[18, FLin1] := dmkPF.CDS2(PagtoDta, QrCamposFormato.Value, 3);
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
      'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // Data de d�bito da tarifa de cobranca
    LocDadoAll(582, i, '', DtaTarif);
    try
      Grade1.Cells[19, FLin1] := dmkPF.CDS2(DtaTarif, QrCamposFormato.Value, 3);
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
      'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // C�digo Condom�nio
    Grade1.Cells[20, FLin1] := IntToStr(Entidade);

    // Sequencia do diretorio
    Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

    // Sequencia do arquivo
    Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

    // N�mero sequencial do registro
    LocDadoAll(999, i, '', SequeReg);
    Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

    // Banco
    Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);


    ErroLinha(FLin1, False);

    //Parei Aqui
    // Fazer rejei��o de t�tulos
  end;
end;

procedure TFmCNAB_Ret.DBGrid6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'DJM') then
  begin
    if      QrLeiDJM.Value > 0 then Cor := clBlue
    else if QrLeiDJM.Value < 0 then Cor := clRed
    else Cor := clBlack;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid6, Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end
  else if (Column.FieldName = 'NOME_TIPO_BOL') then
  begin
         if QrLeiTIPO_BOL.Value = 600 then Cor := clBlue
    else if QrLeiTIPO_BOL.Value < 601 then Cor := clGreen
    else if QrLeiTIPO_BOL.Value = 600 then Cor := clBlue
    else if QrLeiTIPO_BOL.Value = 610 then Cor := clRed
                                      else Cor := clFuchsia;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid6, Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmCNAB_Ret.QrLeiAfterClose(DataSet: TDataSet);
begin
  BtAgenda.Enabled := False;
end;

procedure TFmCNAB_Ret.BtAgendaClick(Sender: TObject);
var
  Ano, Mes, Dia: Word;
  i, Cond: Integer;
begin
  if not CondDeEntidade(QrLeiEntidade.Value, Cond) then Exit;
  Application.CreateForm(TFmCondGerArreFut, FmCondGerArreFut);
  FmCondGerArreFut.FCond := Cond;
  FmCondGerArreFut.LaTipo.Caption := CO_INCLUSAO;
  FmCondGerArreFut.QrAptos.Close;
  FmCondGerArreFut.QrAptos.Params[0].AsInteger := Cond;
  FmCondGerArreFut.QrAptos.Open;
  DecodeDate(Date, Ano, Mes, Dia);
  if QrLeiItens.RecordCount > 0 then
  begin
    with FmCondGerArreFut do
    begin
      EdApto.Text := IntToStr(QrLeiItensApto.Value);
      CBApto.KeyValue := QrLeiItensApto.Value;
      if Dmod.QrControleCNABCtaJur.Value > 0 then
      begin
        EdConta.Text := IntToStr(Dmod.QrControleCNABCtaJur.Value);
        CBConta.KeyValue := Dmod.QrControleCNABCtaJur.Value;
      end else ;
      EdValor.Text := Geral.FFT(QrLeiDJM.Value, 2, siPositivo);
      EdDescricao.Text := 'Diferen�a de juros ' + QrLeiItensMez_TXT.Value +
        ' (boleto ' + FormatFloat('00000', QrLeiIDNum.Value)+')';
      FmCondGerArreFut.CkContinuar.Visible := False;
      FmCondGerArreFut.CkContinuar.Checked := False;
      for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
      begin
        if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
          FmCondGerArreFut.CBAno.ItemIndex := i;
      end;
      FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
    end;
  end;
  FmCondGerArreFut.CkContinuar.Visible := True;
  FmCondGerArreFut.CkContinuar.Checked := True;
  //
  FmCondGerArreFut.ShowModal;
  FmCondGerArreFut.Destroy;
end;

function TFmCNAB_Ret.CondDeEntidade(const Entidade: Integer;
  var Cond: Integer): Boolean;
begin
  QrLocCond.Close;
  QrLocCond.Params[0].AsInteger := Entidade;
  QrLocCond.Open;
  if QrLocCond.RecordCount = 1 then
  begin
    Cond := QrLocCondCodigo.Value;
    Result := True;
  end else begin
    Result := False;
    Cond := 0;
    Application.MessageBox(PChar('ERRO! Foram encontrados ' + IntToStr(
    QrLocCond.RecordCount) + ' condom�nios para a entidade n� ' + IntToStr(
    Entidade) + '!'), 'Aviso de ERRO', MB_OK+MB_ICONERROR);
  end;
  // N�o Fechar por cousa de juros e multa
end;

function TFmCNAB_Ret.ErroLinha(Linha: Integer; Avisa: Boolean): Boolean;
var
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta,
  ValJuMul, ValTarif, ValErro, ValOutro, ValReceb: Double;
  IDLink, Cliente: Integer;
  NossoNum: String;
  //
  DefErro: Boolean;
begin
  if Geral.IMV(Grade1.Cells[02, Linha]) = 6 then // Baixa simples
  begin
    Cliente   := Geral.IMV(GradeA.Cells[09, GradeA.Row]);
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, Linha]);
    IDLink    := Geral.IMV(Grade1.Cells[21, Linha]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, Linha]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, Linha]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, Linha]);
    ValPago   := Geral.DMV(Grade1.Cells[09, Linha]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, Linha]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, Linha]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, Linha]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, Linha]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, Linha]);
    ValReceb  := Geral.DMV(Grade1.Cells[27, Linha]);
    //
    if ValReceb > 0 then
      ValErro := ValPago - ValTarif - ValReceb
    else
      ValErro := ValTitul - ValAbati - ValDesco + ValJuros + ValMulta
               + ValOutro + ValJuMul - ValTarif - ValPago;
    Grade1.Cells[15, Linha] := Geral.FFT(ValErro, 2, siNegativo);
    DefErro := (ValErro >= 0.01) or (ValErro <= -0.01);
    //
    if Avisa then
    begin
      if DefErro then
      begin
        QrPesq2.Close;
        QrPesq2.Params[00].AsInteger := IDLink;
        QrPesq2.Params[01].AsInteger := Cliente;
        QrPesq2.Open;
        //
        if QrPesq2.RecordCount > 0 then
        begin
          Application.MessageBox(PChar('O documento com "Nosso n�m." = ' +
          NossoNum + ' foi localizado, mas tem diverg�ncias nos ' +
          'dados informados!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          Application.MessageBox(PChar('O documento com "Nosso n�m." = ' +
          NossoNum + ' tem diverg�ncias nos dados informados, e n�o ' +
          'foi localizado nos bloquetos emitidos!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    Result := DefErro;
  end else Result := False;
end;

procedure TFmCNAB_Ret.Alteravalordoitemdearrecadaoselecionado1Click(
  Sender: TObject);
var
  Valor: String;
  Novo: Double;
begin
  //
  Valor := FormatFloat('#,###,##0.00', QrLeiItensCredito.Value);
  if InputQuery('Altera��o de valor', 'Informe o novo valor:', Valor) then
  begin
    Novo := Geral.DMV(Valor);
    if Novo >= 0.01 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lanctos SET AlterWeb=1, Credito=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      //
      Dmod.QrUpd.Params[00].AsFloat   := Novo;
      Dmod.QrUpd.Params[01].AsInteger := QrLeiItensControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCNAB0Lei(QrLeiCodigo.Value);
      ReopenLeiItens(QrLeiItensControle.Value);
    end else Application.MessageBox(PChar('O valor informado "' + Valor +
    '" � inv�lido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCNAB_Ret.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCNAB_Ret.Excluioitemdearrecadaoselecionado1Click(
  Sender: TObject);
begin
  if Application.MessageBox(PChar('Tem certeza que deseja excluir o item "' +
  QrLeiItensDescricao.Value + '"?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lanctos WHERE Controle=:P0 AND Sub=:P1');
    Dmod.QrUpd.Params[00].AsInteger := QrLeiItensControle.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrLeiItensSub.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenCNAB0Lei(QrLeiCodigo.Value);
  end;
end;

procedure TFmCNAB_Ret.Ajustavaloresdobloquetoatual1Click(Sender: TObject);
var
  Juros, Multa, Difer, Desco: Double;
begin
  Screen.Cursor := crHourGlass;
  Desco := 0;
  Multa := 0;
  Juros := 0;
  Difer := QrLeiValTitul.Value - QrLeiVALBOLETO.Value;
  if Difer > 0 then
  begin
    if QrLeiDevMulta.Value > Difer then Multa := Difer
    else begin
      Multa := QrLeiDevMulta.Value;
      Juros := Difer - QrLeiDevMulta.Value
    end;
  end else Desco := - Difer;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValJuros=:P0, ValMulta=:P1, ');
  Dmod.QrUpd.SQL.Add('ValDesco=:P2, ValTitul=:P3 WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := Juros;
  Dmod.QrUpd.Params[01].AsFloat   := Multa;
  Dmod.QrUpd.Params[02].AsFloat   := Desco;
  Dmod.QrUpd.Params[03].AsFloat   := QrLeiVALBOLETO.Value;
  //
  Dmod.QrUpd.Params[04].AsInteger := QrLeiCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenCNAB0Lei(QrLeiCodigo.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret.QrLeiAgrCalcFields(DataSet: TDataSet);
begin
  QrLeiAgrMez_TXT.Value := dmkPF.MezToFDT(QrLeiAgrMez.Value, 0, 104);
  case QrLeiAgrTIPO_BLOQ.Value of
    1: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Taxa condominial';
    2: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Reparcelamento';
    else QrLeiAgrNOME_TIPO_BLOQ.Value := 'Desconhecido';
  end;
end;

{
Parei aqui
tentar eliminar BancosLei:
dfm: 1908 e 1964
QrBcocor e QrLocOcor

}

end.


