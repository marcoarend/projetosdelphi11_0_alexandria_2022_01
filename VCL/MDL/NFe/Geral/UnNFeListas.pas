unit UnNFeListas;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkGeral, dmkEdit, Variants;

type
  TUnNFeListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ArrayEnquadramentoLegalIPI(): MyArrayLista;

  end;

var
  NFeListas: TUnNFeListas;

implementation

{ TUnNFeListas }

function TUnNFeListas.ArrayEnquadramentoLegalIPI: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo,
  Grupo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Grupo;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  AddLinha(Result, Linha, '001', 'Imunidade ', 'Livros, jornais, peri�dicos e o papel destinado � sua impress�o - Art. 18 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '002', 'Imunidade ', 'Produtos industrializados destinados ao exterior - Art. 18 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '003', 'Imunidade ', 'Ouro, definido em lei como ativo financeiro ou instrumento cambial - Art. 18 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '004', 'Imunidade ', 'Energia el�trica, derivados de petr�leo, combust�veis e minerais do Pa�s - Art. 18 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '005', 'Imunidade ', 'Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para empresa sediada no exterior - atividades de pesquisa ou lavra de jazidas de petr�leo e de g�s natural Art. 19 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '006', 'Imunidade ', 'Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para empresa sediada no exterior - incorporados a produto final exportado para o Brasil - Art. 19 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '007', 'Imunidade ', 'Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para �rg�o ou entidade de governo estrangeiro ou organismo internacional de que o Brasil seja membro, para ser entregue, no Pa�s, � ordem do comprador' + ' - Art. 19 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '101', 'Suspens�o ', '�leo de menta em bruto, produzido por lavradores - Art. 43 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '102', 'Suspens�o ', 'Produtos remetidos � exposi��o em feiras de amostras e promo��es semelhantes - Art. 43 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '103', 'Suspens�o ', 'Produtos remetidos a dep�sitos fechados ou armaz�ns-gerais, bem assim aqueles devolvidos ao remetente - Art. 43 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '104', 'Suspens�o ', 'Produtos industrializados, que com mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) importados submetidos a regime aduaneiro especial (drawback - suspens�o/isen��o), remetidos diretamente a empresa' + 's industriais exportadoras - Art. 43 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '105', 'Suspens�o ', 'Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para empresas comerciais exportadoras, com o fim espec�fico de exporta��o - Art. 43, Inciso V, al�nea "a" do Decreto 7.212/2010');
  AddLinha(Result, Linha, '106', 'Suspens�o ', 'Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para recintos alfandegados onde se processe o despacho aduaneiro de exporta��o - Art. 43, Inciso V, al�neas "b" do Decreto 7.212/2010');
  AddLinha(Result, Linha, '107', 'Suspens�o ', 'Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para outros locais onde se processe o despacho aduaneiro de exporta��o - Art. 43, Inciso V, al�neas "c" do Decreto 7.212/2010');
  AddLinha(Result, Linha, '108', 'Suspens�o ', 'Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) destinados ao executor de industrializa��o por encomenda - Art. 43 Inciso VI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '109', 'Suspens�o ', 'Produtos industrializados por encomenda remetidos ao estabelecimento de origem - Art. 43 Inciso VII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '110', 'Suspens�o ', 'Mat�rias-primas ou produtos intermedi�rios remetidos para emprego em opera��o industrial realizada pelo remetente fora do estabelecimento - Art. 43 Inciso VIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '111', 'Suspens�o ', 'Ve�culo, aeronave ou embarca��o destinados a emprego em provas de engenharia pelo fabricante - Art. 43 Inciso IX do Decreto 7.212/2010');
  AddLinha(Result, Linha, '112', 'Suspens�o ', 'Produtos remetidos, para industrializa��o ou com�rcio, de um para outro estabelecimento da mesma firma - Art. 43 Inciso X do Decreto 7.212/2010');
  AddLinha(Result, Linha, '113', 'Suspens�o ', 'Bens do ativo permanente remetidos a outro estabelecimento da mesma firma, para serem utilizados no processo industrial do recebedor - Art. 43 Inciso XI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '114', 'Suspens�o ', 'Bens do ativo permanente remetidos a outro estabelecimento, para serem utilizados no processo industrial de produtos encomendados pelo remetente - Art. 43 Inciso XII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '115', 'Suspens�o ', 'Partes e pe�as destinadas ao reparo de produtos com defeito de fabrica��o, quando a opera��o for executada gratuitamente, em virtude de garantia - Art. 43 Inciso XIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '116', 'Suspens�o ', 'Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) de fabrica��o nacional, vendidos a estabelecimento industrial, para industrializa��o de produtos destinados � exporta��o ou a estabelecimento comerc' + 'ial, para industrializa��o em outro estabelecimento da mesma firma ou de terceiro, de produto destinado � exporta��o - Art. 43 Inciso XIV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '117', 'Suspens�o ', 'Produtos para emprego ou consumo na industrializa��o ou elabora��o de produto a ser exportado, adquiridos no mercado interno ou importados - Art. 43 Inciso XV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '118', 'Suspens�o ', 'Bebidas alc�olicas e demais produtos de produ��o nacional acondicionados em recipientes de capacidade superior ao limite m�ximo permitido para venda a varejo - Art. 44 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '119', 'Suspens�o ', 'Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de estabelecimento industrial destinado a comercial equiparado a industrial - Art. 45 Inciso I do Decreto7.' + '212/2010');
  AddLinha(Result, Linha, '120', 'Suspens�o ', 'Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de estabelecimento comercial equiparado a industrial destinado a equiparado a industrial - Art. 45 Inciso I' + 'I do Decreto7.212/2010');
  AddLinha(Result, Linha, '121', 'Suspens�o ', 'Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de importador destinado a equiparado a industrial - Art. 45 Inciso III do Decreto7.212/2010');
  AddLinha(Result, Linha, '122', 'Suspens�o ', 'Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) destinados a estabelecimento que se dedique � elabora��o de produtos classificados nos c�digos previstos no art. 25 da Lei 10.684/2003 - Art. 46 Inc' + 'iso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '123', 'Suspens�o ', 'Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) adquiridos por estabelecimentos industriais fabricantes de partes e pe�as destinadas a estabelecimento industrial fabricante de produto classificado' + ' no Cap�tulo 88 da Tipi - Art. 46 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '124', 'Suspens�o ', 'Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) adquiridos por pessoas jur�dicas preponderantemente exportadoras - Art. 46 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '125', 'Suspens�o ', 'Materiais e equipamentos destinados a embarca��es pr�-registradas ou registradas no Registro Especial Brasileira - REB quando adquiridos por estaleiros navais brasileiros - Art. 46 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '126', 'Suspens�o ', 'Aquisi��o por benefici�rio de regime aduaneiro suspensivo do imposto, destinado a industrializa��o para exporta��o - Art. 47 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '127', 'Suspens�o ', 'Desembara�o de produtos de proced�ncia estrangeira importados por lojas francas - Art. 48 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '128', 'Suspens�o ', 'Desembara�o de maquinas, equipamentos, ve�culos, aparelhos e instrumentos sem similar nacional importados por empresas nacionais de engenharia, destinados � execu��o de obras no exterior - Art. 48 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '129', 'Suspens�o ', 'Desembara�o de produtos de proced�ncia estrangeira com sa�da de reparti��es aduaneiras com suspens�o do Imposto de Importa��o - Art. 48 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '130', 'Suspens�o ', 'Desembara�o de mat�rias-primas, produtos intermedi�rios e materiais de embalagem, importados diretamente por estabelecimento de que tratam os incisos I a III do caput do Decreto 7.212/2010 - Art. 48 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '131', 'Suspens�o ', 'Remessa de produtos para a ZFM destinados ao seu consumo interno, utiliza��o ou industrializa��o - Art. 84 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '132', 'Suspens�o ', 'Remessa de produtos para a ZFM destinados � exporta��o - Art. 85 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '133', 'Suspens�o ', 'Produtos que, antes de sua remessa � ZFM, forem enviados pelo seu fabricante a outro estabelecimento, para industrializa��o adicional, por conta e ordem do destinat�rio - Art. 85 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '134', 'Suspens�o ', 'Desembara�o de produtos de proced�ncia estrangeira importados pela ZFM quando ali consumidos ou utilizados, exceto armas, muni��es, fumo, bebidas alco�licas e autom�veis de passageiros. - Art. 86 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '135', 'Suspens�o ', 'Remessa de produtos para a Amaz�nia Ocidental destinados ao seu consumo interno ou utiliza��o - Art. 96 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '136', 'Suspens�o ', 'Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Tabatinga - ALCT destinados ao seu consumo interno ou utiliza��o - Art. 106 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '137', 'Suspens�o ', 'Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Guajar�-Mirim - ALCGM destinados ao seu consumo interno ou utiliza��o - Art. 109 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '138', 'Suspens�o ', 'Entrada de produtos estrangeiros nas �reas de Livre Com�rcio de Boa Vista - ALCBV e Bomfim - ALCB destinados a seu consumo interno ou utiliza��o - Art. 112 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '139', 'Suspens�o ', 'Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Macap� e Santana - ALCMS destinados a seu consumo interno ou utiliza��o - Art. 116 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '140', 'Suspens�o ', 'Entrada de produtos estrangeiros nas �reas de Livre Com�rcio de Brasil�ia - ALCB e de Cruzeiro do Sul - ALCCS destinados a seu consumo interno ou utiliza��o - Art. 119 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '141', 'Suspens�o ', 'Remessa para Zona de Processamento de Exporta��o - ZPE - Art. 121 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '142', 'Suspens�o ', 'Setor Automotivo - Desembara�o aduaneiro, chassis e outros - regime aduaneiro especial - industrializa��o 87.01 a 87.05 - Art. 136, I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '143', 'Suspens�o ', 'Setor Automotivo - Do estabelecimento industrial produtos 87.01 a 87.05 da TIPI - mercado interno - empresa comercial atacadista controlada por PJ encomendante do exterior. - Art. 136, II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '144', 'Suspens�o ', 'Setor Automotivo - Do estabelecimento industrial - chassis e outros classificados nas posi��es 84.29, 84.32, 84.33, 87.01 a 87.06 e 87.11 da TIPI. - Art. 136, III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '145', 'Suspens�o ', 'Setor Automotivo - Desembara�o aduaneiro, chassis e outros classificados nas posi��es 84.29, 84.32, 84.33, 87.01 a 87.06 e 87.11 da TIPI quando importados diretamente por estabelecimento industrial - Art. 136, IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '146', 'Suspens�o ', 'Setor Automotivo - do estabelecimento industrial mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, adquiridos por fabricantes, preponderantemente, de componentes, chassis e outros classificados nos C�digos' + ' 84.29, 8432.40.00, 8432.80.00, 8433.20, 8433.30.00, 8433.40.00, 8433.5 e 87.01 a 87.06 da TIPI - Art. 136, V do Decreto 7.212/2010');
  AddLinha(Result, Linha, '147', 'Suspens�o ', 'Setor Automotivo - Desembara�o aduaneiro, as mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, importados diretamente por fabricantes, preponderantemente, de componentes, chassis e outros classificados nos' + ' C�digos 84.29, 8432.40.00, 8432.80.00, 8433.20, 8433.30.00, 8433.40.00, 8433.5 e 87.01 a 87.06 da TIPI - Art. 136, VI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '148', 'Suspens�o ', 'Bens de Inform�tica e Automa��o - mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, quando adquiridos por estabelecimentos industriais fabricantes dos referidos bens. - Art. 148 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '149', 'Suspens�o ', 'Reporto - Sa�da de Estabelecimento de m�quinas e outros quando adquiridos por benefici�rios do REPORTO - Art. 166, I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '150', 'Suspens�o ', 'Reporto - Desembara�o aduaneiro de m�quinas e outros quando adquiridos por benefici�rios do REPORTO - Art. 166, II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '151', 'Suspens�o ', 'Repes - Desembara�o aduaneiro - bens sem similar nacional importados por benefici�rios do REPES - Art. 171 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '152', 'Suspens�o ', 'Recine - Sa�da para benefici�rio do regime - Art. 14, III da Lei 12.599/2012');
  AddLinha(Result, Linha, '153', 'Suspens�o ', 'Recine - Desembara�o aduaneiro por benefici�rio do regime - Art. 14, IV da Lei 12.599/2012');
  AddLinha(Result, Linha, '154', 'Suspens�o ', 'Reif - Sa�da para benefici�rio do regime - Lei 12.794/1013, art. 8, III');
  AddLinha(Result, Linha, '155', 'Suspens�o ', 'Reif - Desembara�o aduaneiro por benefici�rio do regime - Lei 12.794/1013, art. 8, IV');
  AddLinha(Result, Linha, '156', 'Suspens�o ', 'Repnbl-Redes - Sa�da para benefici�rio do regime - Lei n� 12.715/2012, art. 30, II');
  AddLinha(Result, Linha, '157', 'Suspens�o ', 'Recompe - Sa�da de mat�rias-primas e produtos intermedi�rios para benefici�rios do regime - Decreto n� 7.243/2010, art. 5�, I');
  AddLinha(Result, Linha, '158', 'Suspens�o ', 'Recompe - Sa�da de mat�rias-primas e produtos intermedi�rios destinados a industrializa��o de equipamentos - Programa Est�mulo Universidade-Empresa - Apoio � Inova��o - Decreto n� 7.243/2010, art. 5�, III');
  AddLinha(Result, Linha, '159', 'Suspens�o ', 'Rio 2016 - Produtos nacionais, dur�veis, uso e consumo dos eventos, adquiridos pelas pessoas jur�dicas mencionadas no � 2o do art. 4o da Lei n� 12.780/2013 - Lei n� 12.780/2013, Art. 13');
  AddLinha(Result, Linha, '301', 'Isen��o', 'Produtos industrializados por institui��es de educa��o ou de assist�ncia social, destinados a uso pr�prio ou a distribui��o gratuita a seus educandos ou assistidos - Art. 54 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '302', 'Isen��o', 'Produtos industrializados por estabelecimentos p�blicos e aut�rquicos da Uni�o, dos Estados, do Distrito Federal e dos Munic�pios, n�o destinados a com�rcio - Art. 54 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '303', 'Isen��o', 'Amostras de produtos para distribui��o gratuita, de diminuto ou nenhum valor comercial - Art. 54 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '304', 'Isen��o', 'Amostras de tecidos sem valor comercial - Art. 54 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '305', 'Isen��o', 'P�s isolados de cal�ados - Art. 54 Inciso V do Decreto 7.212/2010');
  AddLinha(Result, Linha, '306', 'Isen��o', 'Aeronaves de uso militar e suas partes e pe�as, vendidas � Uni�o - Art. 54 Inciso VI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '307', 'Isen��o', 'Caix�es funer�rios - Art. 54 Inciso VII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '308', 'Isen��o', 'Papel destinado � impress�o de m�sicas - Art. 54 Inciso VIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '309', 'Isen��o', 'Panelas e outros artefatos semelhantes, de uso dom�stico, de fabrica��o r�stica, de pedra ou barro bruto - Art. 54 Inciso IX do Decreto 7.212/2010');
  AddLinha(Result, Linha, '310', 'Isen��o', 'Chap�us, roupas e prote��o, de couro, pr�prios para tropeiros - Art. 54 Inciso X do Decreto 7.212/2010');
  AddLinha(Result, Linha, '311', 'Isen��o', 'Material b�lico, de uso privativo das For�as Armadas, vendido � Uni�o - Art. 54 Inciso XI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '312', 'Isen��o', 'Autom�vel adquirido diretamente a fabricante nacional, pelas miss�es diplom�ticas e reparti��es consulares de car�ter permanente, ou seus integrantes, bem assim pelas representa��es internacionais ou regionais de que o Brasil seja ' + 'membro, e seus funcion�rios, peritos, t�cnicos e consultores, de nacionalidade estrangeira, que exer�am fun��es de car�ter permanente - Art. 54 Inciso XII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '313', 'Isen��o', 'Ve�culo de fabrica��o nacional adquirido por funcion�rio das miss�es diplom�ticas acreditadas junto ao Governo Brasileiro - Art. 54 Inciso XIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '314', 'Isen��o', 'Produtos nacionais sa�dos diretamente para Lojas Francas - Art. 54 Inciso XIV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '315', 'Isen��o', 'Materiais e equipamentos destinados a Itaipu Binacional - Art. 54 Inciso XV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '316', 'Isen��o', 'Produtos Importados por miss�es diplom�ticas, consulados ou organismo internacional - Art. 54 Inciso XVI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '317', 'Isen��o', 'Bagagem de passageiros desembara�ada com isen��o do II. - Art. 54 Inciso XVII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '318', 'Isen��o', 'Bagagem de passageiros desembara�ada com pagamento do II. - Art. 54 Inciso XVIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '319', 'Isen��o', 'Remessas postais internacionais sujeitas a tributa��o simplificada. - Art. 54 Inciso XIX do Decreto 7.212/2010');
  AddLinha(Result, Linha, '320', 'Isen��o', 'M�quinas e outros destinados � pesquisa cient�fica e tecnol�gica - Art. 54 Inciso XX do Decreto 7.212/2010');
  AddLinha(Result, Linha, '321', 'Isen��o', 'Produtos de proced�ncia estrangeira, isentos do II conforme Lei n� 8032/1990. - Art. 54 Inciso XXI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '322', 'Isen��o', 'Produtos de proced�ncia estrangeira utilizados em eventos esportivos - Art. 54 Inciso XXII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '323', 'Isen��o', 'Ve�culos automotores, m�quinas, equipamentos, bem assim suas partes e pe�as separadas, destinadas � utiliza��o nas atividades dos Corpos de Bombeiros - Art. 54 Inciso XXIII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '324', 'Isen��o', 'Produtos importados para consumo em congressos, feiras e exposi��es - Art. 54 Inciso XXIV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '325', 'Isen��o', 'Bens de inform�tica, Mat�ria Prima, produtos intermedi�rios e embalagem destinados a Urnas eletr�nicas - TSE - Art. 54 Inciso XXV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '326', 'Isen��o', 'Materiais, equipamentos, m�quinas, aparelhos e instrumentos, bem assim os respectivos acess�rios, sobressalentes e ferramentas, que os acompanhem, destinados � constru��o do Gasoduto Brasil - Bol�via - Art. 54 Inciso XXVI do Decreto 7.212/2010');
  AddLinha(Result, Linha, '327', 'Isen��o', 'Partes, pe�as e componentes, adquiridos por estaleiros navais brasileiros, destinados ao emprego na conserva��o, moderniza��o, convers�o ou reparo de embarca��es registradas no Registro Especial Brasileiro - REB - Art. 54 Inciso XX' + 'VII do Decreto 7.212/2010');
  AddLinha(Result, Linha, '328', 'Isen��o', 'Aparelhos transmissores e receptores de radiotelefonia e radiotelegrafia; ve�culos para patrulhamento policial; armas e muni��es, destinados a �rg�os de seguran�a p�blica da Uni�o, dos Estados e do Distrito Federal - Art. 54 Inciso' + ' XXVIII do Decreto 7.212/2010 ');
  AddLinha(Result, Linha, '329', 'Isen��o', 'Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi adquiridos por motoristas profissionais - Art. 55 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '330', 'Isen��o', 'Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi por impedidos de exercer atividade por destrui��o, furto ou roubo do ve�culo adquiridos por motoristas profissionais. - Art. 55 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '331', 'Isen��o', 'Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi adquiridos por cooperativas de trabalho. - Art. 55 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '332', 'Isen��o', 'Autom�veis de passageiros de fabrica��o nacional, destinados a pessoas portadoras de defici�ncia f�sica, visual, mental severa ou profunda, ou autistas - Art. 55 Inciso IV do Decreto 7.212/2010');
  AddLinha(Result, Linha, '333', 'Isen��o', 'Produtos estrangeiros, recebidos em doa��o de representa��es diplom�ticas estrangeiras sediadas no Pa�s, vendidos em feiras, bazares e eventos semelhantes por entidades beneficentes - Art. 67 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '334', 'Isen��o', 'Produtos industrializados na Zona Franca de Manaus - ZFM, destinados ao seu consumo interno - Art. 81 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '335', 'Isen��o', 'Produtos industrializados na ZFM, por estabelecimentos com projetos aprovados pela SUFRAMA, destinados a comercializa��o em qualquer outro ponto do Territ�rio Nacional - Art. 81 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '336', 'Isen��o', 'Produtos nacionais destinados � entrada na ZFM, para seu consumo interno, utiliza��o ou industrializa��o, ou ainda, para serem remetidos, por interm�dio de seus entrepostos, � Amaz�nia Ocidental - Art. 81 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '337', 'Isen��o', 'Produtos industrializados por estabelecimentos com projetos aprovados pela SUFRAMA, consumidos ou utilizados na Amaz�nia Ocidental, ou adquiridos atrav�s da ZFM ou de seus entrepostos na referida regi�o - Art. 95 Inciso I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '338', 'Isen��o', 'Produtos de proced�ncia estrangeira, relacionados na legisla��o, oriundos da ZFM e que derem entrada na Amaz�nia Ocidental para ali serem consumidos ou utilizados: - Art. 95 Inciso II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '339', 'Isen��o', 'Produtos elaborados com mat�rias-primas agr�colas e extrativas vegetais de produ��o regional, por estabelecimentos industriais localizados na Amaz�nia Ocidental, com projetos aprovados pela SUFRAMA - Art. 95 Inciso III do Decreto 7.212/2010');
  AddLinha(Result, Linha, '340', 'Isen��o', 'Produtos industrializados em �rea de Livre Com�rcio - Art. 105 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '341', 'Isen��o', 'Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Tabatinga - ALCT - Art. 107 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '342', 'Isen��o', 'Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Guajar�-Mirim - ALCGM - Art. 110 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '343', 'Isen��o', 'Produtos nacionais ou nacionalizados, destinados � entrada nas �reas de Livre Com�rcio de Boa Vista - ALCBV e Bonfim - ALCB - Art. 113 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '344', 'Isen��o', 'Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Macap� e Santana - ALCMS - Art. 117 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '345', 'Isen��o', 'Produtos nacionais ou nacionalizados, destinados � entrada nas �reas de Livre Com�rcio de Brasil�ia - ALCB e de Cruzeiro do Sul - ALCCS - Art. 120 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '346', 'Isen��o', 'Recompe - equipamentos de inform�tica - de benefici�rio do regime para escolas das redes p�blicas de ensino federal, estadual, distrital, municipal ou nas escolas sem fins lucrativos de atendimento a pessoas com defici�ncia - Decreto n�' + ' 7.243/2010, art. 7�');
  AddLinha(Result, Linha, '347', 'Isen��o', 'Rio 2016 - Importa��o de materiais para os jogos (medalhas, trof�us, impressos, bens n�o dur�veis, etc.) - Lei n� 12.780/2013, Art. 4�, �1�, I');
  AddLinha(Result, Linha, '348', 'Isen��o', 'Rio 2016 - Suspens�o convertida em Isen��o - Lei n� 12.780/2013, Art. 6�, I');
  AddLinha(Result, Linha, '349', 'Isen��o', 'Rio 2016 - Empresas vinculadas ao CIO - Lei n� 12.780/2013, Art. 9�, I, d');
  AddLinha(Result, Linha, '350', 'Isen��o', 'Rio 2016 - Sa�da de produtos importados pelo RIO 2016 - Lei n� 12.780/2013, Art. 10, I, d');
  AddLinha(Result, Linha, '351', 'Isen��o', 'Rio 2016 - Produtos nacionais, n�o dur�veis, uso e consumo dos eventos, adquiridos pelas pessoas jur�dicas mencionadas no � 2o do art. 4o da Lei n� 12.780/2013 - Lei n� 12.780/2013, Art. 12');
  AddLinha(Result, Linha, '601', 'Redu��o', 'Equipamentos e outros destinados � pesquisa e ao desenvolvimento tecnol�gico - Art. 72 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '602', 'Redu��o', 'Equipamentos e outros destinados � empresas habilitadas no PDTI e PDTA utilizados em pesquisa e ao desenvolvimento tecnol�gico - Art. 73 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '603', 'Redu��o', 'Microcomputadores e outros de at� R$11.000,00, unidades de disco, circuitos, etc, destinados a bens de inform�tica ou automa��o. Centro-Oeste SUDAM SUDENE - Art. 142, I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '604', 'Redu��o', 'Microcomputadores e outros de at� R$11.000,00, unidades de disco, circuitos, etc, destinados a bens de inform�tica ou automa��o. - Art. 142, I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '605', 'Redu��o', 'Bens de inform�tica n�o inclu�dos no art. 142 do Decreto 7.212/2010 - Produzidos no Centro-Oeste, SUDAM, SUDENE - Art. 143, I do Decreto 7.212/2010');
  AddLinha(Result, Linha, '606', 'Redu��o', 'Bens de inform�tica n�o inclu�dos no art. 142 do Decreto 7.212/2010 - Art. 143, II do Decreto 7.212/2010');
  AddLinha(Result, Linha, '607', 'Redu��o', 'Padis - Art. 150 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '608', 'Redu��o', 'Patvd - Art. 158 do Decreto 7.212/2010');
  AddLinha(Result, Linha, '999', 'Outros', 'Tributa��o normal IPI; Outros;');
end;

end.
