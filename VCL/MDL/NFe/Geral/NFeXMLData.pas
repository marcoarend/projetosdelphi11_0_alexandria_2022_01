unit NFeXMLData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker;

type
  TFmNFeXMLData = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Edide_Serie: TdmkEdit;
    Label1: TLabel;
    Edide_nNF: TdmkEdit;
    EdChaveNFe: TdmkEdit;
    Label2: TLabel;
    EdEmit_TXT: TdmkEdit;
    EdEmit: TdmkEdit;
    Label3: TLabel;
    Panel6: TPanel;
    Label4: TLabel;
    TPDataFiscal: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPDataEntrada: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmNFeXMLData: TFmNFeXMLData;

implementation

uses UnMyObjects, ModuleGeral;

{$R *.DFM}

procedure TFmNFeXMLData.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeXMLData.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeXMLData.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeXMLData.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataEntrada.Date := DModG.ObtemAgora();
  TPDataFiscal.Date := DModG.ObtemAgora();
end;

procedure TFmNFeXMLData.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
