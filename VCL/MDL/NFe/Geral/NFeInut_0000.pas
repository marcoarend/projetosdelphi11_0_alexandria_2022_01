unit NFeInut_0000;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, ComCtrls, dmkEditDateTimePicker,
  OleCtrls, SHDocVw, dmkImage, UnDmkEnums;

type
  TFmNFeInut_0000 = class(TForm)
    PainelDados: TPanel;
    DsNFeInut: TDataSource;
    QrNFeInut: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrNFeInutCodigo: TIntegerField;
    QrNFeInutNome: TWideStringField;
    QrNFeInutEmpresa: TIntegerField;
    QrNFeInutversao: TFloatField;
    QrNFeInutId: TWideStringField;
    QrNFeInuttpAmb: TSmallintField;
    QrNFeInutcUF: TSmallintField;
    QrNFeInutano: TSmallintField;
    QrNFeInutCNPJ: TWideStringField;
    QrNFeInutmodelo: TSmallintField;
    QrNFeInutSerie: TIntegerField;
    QrNFeInutnNFIni: TIntegerField;
    QrNFeInutnNFFim: TIntegerField;
    QrNFeInutJustif: TIntegerField;
    QrNFeInutxJust: TWideStringField;
    QrNFeInutcStat: TIntegerField;
    QrNFeInutxMotivo: TWideStringField;
    QrNFeInutLk: TIntegerField;
    QrNFeInutDataCad: TDateField;
    QrNFeInutDataAlt: TDateField;
    QrNFeInutUserCad: TIntegerField;
    QrNFeInutUserAlt: TIntegerField;
    QrNFeInutAlterWeb: TSmallintField;
    QrNFeInutAtivo: TSmallintField;
    QrNFeInutCodUsu: TIntegerField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    EdSerie: TdmkEdit;
    EdnNFIni: TdmkEdit;
    EdnNFFim: TdmkEdit;
    dmkLabel3: TdmkLabel;
    dmkLabel4: TdmkLabel;
    dmkLabel5: TdmkLabel;
    QrNFeJust: TmySQLQuery;
    QrNFeJustCodigo: TIntegerField;
    QrNFeJustNome: TWideStringField;
    DsNFeJust: TDataSource;
    CBNFeJust: TdmkDBLookupComboBox;
    EdNFeJust: TdmkEditCB;
    EdModelo: TdmkEdit;
    dmkLabel6: TdmkLabel;
    dmkVUFilial: TdmkValUsu;
    dmkVUNFEJust: TdmkValUsu;
    dmkLabel7: TdmkLabel;
    dmkLabel9: TdmkLabel;
    dmkLabel10: TdmkLabel;
    dmkLabel11: TdmkLabel;
    dmkLabel12: TdmkLabel;
    DBEdit2: TDBEdit;
    QrNFeInutNO_Empresa: TWideStringField;
    QrNFeInutNO_Justif: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    DBEdit9: TDBEdit;
    QrNFeInutMsg: TmySQLQuery;
    DsNFeInutMsg: TDataSource;
    PMDados: TPopupMenu;
    Incluinovoprocesso1: TMenuItem;
    Alteradadosdoprocessoatual1: TMenuItem;
    TPDataC: TdmkEditDateTimePicker;
    Label4: TLabel;
    Label6: TLabel;
    QrNFeInutDataC: TDateField;
    DBEdit10: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrNFeInutMsgCodigo: TIntegerField;
    QrNFeInutMsgControle: TIntegerField;
    QrNFeInutMsgversao: TFloatField;
    QrNFeInutMsgId: TWideStringField;
    QrNFeInutMsgtpAmb: TSmallintField;
    QrNFeInutMsgverAplic: TWideStringField;
    QrNFeInutMsgcStat: TIntegerField;
    QrNFeInutMsgxMotivo: TWideStringField;
    QrNFeInutMsgcUF: TSmallintField;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    Label12: TLabel;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    DBEdit16: TDBEdit;
    Label16: TLabel;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    DBEdit18: TDBEdit;
    QrNFeInutNO_Ambiente: TWideStringField;
    DBEdit19: TDBEdit;
    Label18: TLabel;
    DBEdit20: TDBEdit;
    Label19: TLabel;
    DBEdit21: TDBEdit;
    Label20: TLabel;
    DBEdit22: TDBEdit;
    Label21: TLabel;
    DBEdit23: TDBEdit;
    QrNFeInutdhRecbto: TDateTimeField;
    QrNFeInutnProt: TWideStringField;
    Label22: TLabel;
    DBEdit24: TDBEdit;
    Label23: TLabel;
    DBEdit25: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeMsg: TDBGrid;
    TabSheet2: TTabSheet;
    QrNFeInutXML_Inu: TWideMemoField;
    DBMemo1: TDBMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel4: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtDados: TBitBtn;
    BtSolicita: TBitBtn;
    BtLeXML: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeInutAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeInutBeforeOpen(DataSet: TDataSet);
    procedure BtDadosClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeInutAfterScroll(DataSet: TDataSet);
    procedure Incluinovoprocesso1Click(Sender: TObject);
    procedure Alteradadosdoprocessoatual1Click(Sender: TObject);
    procedure BtSolicitaClick(Sender: TObject);
    procedure BtLeXMLClick(Sender: TObject);
    procedure QrNFeInutBeforeClose(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMDadosPopup(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FEmpresa, FLote, FnNFIni, FnNFFim, FSerie, FModelo, FAno, FJustif: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure DefineVarsDePreparacao();
  public
    { Public declarations }
    FFilial: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeInutMsg(Controle: Integer);
  end;

var
  FmNFeInut_0000: TFmNFeInut_0000;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleNFe_0000,
  //{$IFNDef semNFe _ v 0 2 0 0} NFeSteps _ 0 2 0 0 , {$EndIF}
  DmkDAC_PF, NFeJust, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeInut_0000.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeInut_0000.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeInutCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeInut_0000.DefParams;
begin
  VAR_GOTOTABELA := 'NFeInut';
  VAR_GOTOMYSQLTABLE := QrNFeInut;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial, ent.Nome) NO_Empresa,');
  VAR_SQLx.Add('jus.Nome NO_Justif, inu.*');
  VAR_SQLx.Add('FROM nfeinut inu');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=inu.Empresa');
  VAR_SQLx.Add('LEFT JOIN nfejust jus ON jus.Codigo=inu.Justif');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE inu.Codigo > 0');
  VAR_SQLx.Add('AND inu.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND inu.Codigo=:P0');
  //
  VAR_SQL2.Add('AND inu.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND inu.Nome Like :P0');
  //
end;

procedure TFmNFeInut_0000.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfeinut', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeInut_0000.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeInut_0000.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 2 then
    DmNFe_0000.LoadXML(TMemo(DBMemo1), WBResposta, PageControl1, -1);
end;

procedure TFmNFeInut_0000.PMDadosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrNFeInut.State <> dsInactive) and (QrNFeInut.RecordCount > 0);
  //
  Alteradadosdoprocessoatual1.Enabled := Enab;
end;

procedure TFmNFeInut_0000.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNFeInut.AlteraRegistro;
var
  NFeInut : Integer;
begin
  NFeInut := QrNFeInutCodigo.Value;
  if QrNFeInutCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NFeInut, Dmod.MyDB, 'NFeInut', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFeInut, Dmod.MyDB, 'NFeInut', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFeInut.IncluiRegistro;
var
  Cursor : TCursor;
  NFeInut : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NFeInut := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'NFeInut', 'NFeInut', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NFeInut))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, NFeInut);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNFeInut_0000.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeInut_0000.ReopenNFeInutMsg(Controle: Integer);
begin
  QrNFeInutMsg.Close;
  QrNFeInutMsg.Params[0].AsInteger := QrNFeInutCodigo.Value;
  //QrNFeInutMsg. Open;
  UnDmkDAC_PF.AbreQuery(QrNFeInutMsg, DMod.MyDB);
end;

procedure TFmNFeInut_0000.DefineONomeDoForm;
begin
end;

procedure TFmNFeInut_0000.DefineVarsDePreparacao;
begin
  FEmpresa := QrNFeInutEmpresa.Value;
  FLote    := QrNFeInutCodigo.Value;
  FnNFIni  := QrNFeInutnNFIni.Value;
  FnNFFim  := QrNFeInutnNFFim.Value;
  FSerie   := QrNFeInutSerie.Value;
  FModelo  := QrNFeInutmodelo.Value;
  FAno     := Geral.IMV(FormatDateTime('yy', QrNFeInutDataC.Value));
  FJustif  := QrNFeInutJustif.Value;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeInut_0000.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeInut_0000.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeInut_0000.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeInut_0000.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeInut_0000.SpeedButton5Click(Sender: TObject);
var
  NFeJust: Integer;
begin
  VAR_CADASTRO := 0;
  NFeJust      := EdNFeJust.ValueVariant;
  //
  if DBCheck.CriaFm(TFmNFeJust, FmNFeJust, afmoNegarComAviso) then
  begin
    if NFeJust <> 0 then
      FmNFeJust.LocCod(NFeJust, NFeJust);
    FmNFeJust.ShowModal;
    FmNFeJust.Destroy;
  end;
  //
  if VAR_CADASTRO > 0 then
  begin
    //Para n�o perder a SQL pois vaira no reopen de acordo com a aplica��o
    //QrNFeJust.Close;
    //QrNFeJust. Open;
    UnDmkDAC_PF.AbreQuery(QrNFeJust, DMod.MyDB);
    //
    EdNFeJust.ValueVariant := VAR_CADASTRO;
    CBNFeJust.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmNFeInut_0000.Alteradadosdoprocessoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeInut, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'nfeinut');
end;

procedure TFmNFeInut_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeInutCodigo.Value;
  Close;
end;

procedure TFmNFeInut_0000.BtSolicitaClick(Sender: TObject);
var
  Numeros: String;
begin
  if (QrNFeInut.State <> dsInactive) and (QrNFeInut.RecordCount > 0) then
  begin
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
    //
    DefineVarsDePreparacao();
    if (FnNFIni = FnNFFim) then
      Numeros := 'o n�mero ' + IntTostr(FnNFIni)
    else
      Numeros := 'os n�meros de ' + IntTostr(FnNFIni) + ' at� ' + IntTostr(FnNFFim);
    if Geral.MB_Pergunta('Deseja realmente inutilizar ' + Numeros +
    ' da s�rie ' + Geral.FF0(QrNFeInutSerie.Value) + ' do modelo ' + Geral.FF0(
    QrNFeInutmodelo.Value) + '?') = ID_YES then
    begin
      UnNFe_PF.MostraFormStepsNFe_Inutilizacao(FEmpresa, FLote, FAno, FModelo,
      FSerie, FnNFIni, FnNFFim, FJustif);
      LocCod(FLote, FLote);
    end;
  end;
end;

procedure TFmNFeInut_0000.BtConfirmaClick(Sender: TObject);
var
  Codigo, Serie, NFini, NFFim: Integer;
  Nome: String;
begin
  Nome  := EdNome.ValueVariant;
  Serie := EdSerie.ValueVariant;
  NFini := EdnNFIni.ValueVariant;
  NFFim := EdnNFFim.ValueVariant;
  //
  if MyObjects.FIC(EdCodUsu.ValueVariant = 0, EdCodUsu, 'Informe o C�digo!') then
    Exit;
  if MyObjects.FIC(EdNFeJust.ValueVariant = 0, EdNFeJust, 'Informe a justificativa!') then
    Exit;
  (* 2018-01-30 => Desmarcado pois o cliente pode utilizar a s�rie "0"
  if MyObjects.FIC(Serie = 0, EdSerie, 'Informe a S�rie NF!') then
    Exit;
  *)
  if MyObjects.FIC(NFini = 0, EdnNFIni, 'Informe a N� NF incial!') then
    Exit;
  if MyObjects.FIC(NFFim = 0, EdnNFFim, 'Informe a N� NF final!' + sLineBreak +
    'OBS.: Caso queira inutilizar apenas um n�mero repita o n�mero preenchido no campo "N� NF incial".') then
    Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinut', '', 0)
  else
    Codigo := QrNFeInutCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeInut_0000, PainelEdit,
    'NFeInut', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeInut_0000.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFeInut', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeInut', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeInut', 'Codigo');
end;

procedure TFmNFeInut_0000.BtLeXMLClick(Sender: TObject);
begin
  Geral.MB_Info('Deprecado! Solicite implementa��o � dermatek!');
(*
  {$IFNDef semNFe_v 0 2 0 0}
  if (QrNFeInut.State <> dsInactive) and (QrNFeInut.RecordCount > 0) then
  begin
    DefineVarsDePreparacao();
    if DBCheck.CriaFm(TFmNFeSteps_ 0 2 0 0, FmNFeSteps_ 0 2 0 0, afmoNegarComAviso) then
    begin
      //
      FmNFeSteps_ 0 2 0 0.PnLoteEnv.Visible := True;
      FmNFeSteps_ 0 2 0 0.Show;
      //
      FmNFeSteps_ 0 2 0 0.RGAcao.ItemIndex := 4; // Pedido de inutiliza��o
      FmNFeSteps_ 0 2 0 0.CkSoLer.Checked := True;
      FmNFeSteps_ 0 2 0 0.PreparaInutilizaNumerosNF(FEmpresa, FLote, FAno, FModelo,
        FSerie, FnNFIni, FnNFFim, FJustif);
      //
      //
      //FmNFeSteps_ 0 2 0 0.Destroy;
      //
    end;
  end;
  {$EndIF}
*)
end;

procedure TFmNFeInut_0000.BtDadosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDados, BtDados);
end;

procedure TFmNFeInut_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Screen.Cursor                := crHourGlass;
  PainelEdit.Align             := alClient;
  PageControl1.Align           := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  TPDataC.Date      := Date;
  FFilial           := 0;
  //
  CBFilial.ListSource := DModG.DsEmpresas;
  if (DModG.QrEmpresas.State = dsInactive) or (DModG.QrEmpresas.RecordCount = 0) then
    DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  //
  QrNFeJust.Close;
  QrNFeJust.Params[0].AsInteger := 2; // inutiliza��o
//  QrNFeJust. Open;
  UnDmkDAC_PF.AbreQuery(QrNFeJust, DMod.MyDB);
  //
  {
  QrClientes. Open;
  TPDataI.Date := Date - 31;
  TPDataF.Date := Date;
  //
  ReopenNFeCabA(0);
  }


  //{$IFNDef semNFe_v 0 2 0 0}
  //BtLeXML.Visible := True;
  //{$Else}
  BtLeXML.Visible := False;
  //{$EndIF}
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFeInut_0000.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeInutCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeInut_0000.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeInut_0000.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeInut_0000.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeInutCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeInut_0000.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeInut_0000.QrNFeInutAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeInut_0000.QrNFeInutAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrNFeInutcStat.Value <> 102;
  Alteradadosdoprocessoatual1.Enabled := Habilita;
  BtSolicita.Enabled := Habilita;
  ReopenNFeInutMsg(0);
  if PageControl1.ActivePageIndex = 2 then
    DmNFe_0000.LoadXML(TMemo(DBMemo1), WBResposta, PageControl1, -1);
end;

procedure TFmNFeInut_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeInut_0000.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeInutCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeInut', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeInut_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeInut_0000.FormShow(Sender: TObject);
begin
  EdFilial.ValueVariant := FFilial;
  CBFilial.KeyValue     := FFilial;
end;

procedure TFmNFeInut_0000.Incluinovoprocesso1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeInut, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'nfeinut');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfeinut', 'CodUsu', [], [], stIns,
    0, siPositivo, EdCodUsu);
  //
  EdModelo.ValueVariant := 55;
  TPDataC.Date          := Date;
end;

procedure TFmNFeInut_0000.QrNFeInutBeforeClose(DataSet: TDataSet);
begin
  Alteradadosdoprocessoatual1.Enabled := False;
  BtSolicita.Enabled := False;
  QrNFeInutMsg.Close;
end;

procedure TFmNFeInut_0000.QrNFeInutBeforeOpen(DataSet: TDataSet);
begin
  QrNFeInutCodigo.DisplayFormat := FFormatFloat;
end;

end.

