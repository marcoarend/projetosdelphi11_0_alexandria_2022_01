unit NFeExportaXML_2013_08_08;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker,
  dmkCheckGroup, DB, mySQLDbTables, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmNFeExportaXML = class(TForm)
    Panel1: TPanel;
    EdEmpresa: TdmkEditCB;
    Label4: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdDest: TdmkEditCB;
    CBDest: TdmkDBLookupComboBox;
    Label14: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    CGStatus: TdmkCheckGroup;
    GroupBox1: TGroupBox;
    CkProt: TCheckBox;
    CkUmArqPorNFe: TCheckBox;
    CkZipar: TCheckBox;
    Label1: TLabel;
    EdRaiz: TEdit;
    SbRaiz: TSpeedButton;
    Label5: TLabel;
    EdArq: TEdit;
    QrDest: TmySQLQuery;
    QrDestCodigo: TIntegerField;
    QrDestNO_ENT: TWideStringField;
    QrDestTipo: TSmallintField;
    QrDestCNPJ_CPF: TWideStringField;
    DsDest: TDataSource;
    QrA: TmySQLQuery;
    QrAId: TWideStringField;
    QrAIDCtrl: TIntegerField;
    QrAStatus: TIntegerField;
    QrAversao: TFloatField;
    DBGrid1: TDBGrid;
    DsA: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CGStatusClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbRaizClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeExportaXML: TFmNFeExportaXML;

implementation

uses UnMyObjects, ModuleNFe_0000, ModuleGeral, Module, UnInternalConsts, dmkGeral,
NFeXMLGerencia, ZForge;

{$R *.DFM}

procedure TFmNFeExportaXML.BtOKClick(Sender: TObject);
var
  CodDest: Integer;
  CNPJ_emit, CNPJ_dest, Situacoes, DirRaiz, DirCria, Dest_TXT, CamZip: String;
  XML_Distribuicao: String;
  Continua: Boolean;
begin
  //CodEmit := EdEmpresa.ValueVariant;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Defina o Emitente!') then Exit;
  CNPJ_emit := Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value);
  if MyObjects.FIC(CNPJ_emit = '', EdEmpresa,
    'O emitente selecionado n�o possui CNPJ/CPF em seu cadastro!') then Exit;
  CodDest  := EdDest.ValueVariant;
  Dest_TXT := FormatFloat('0', CodDest);
  if CodDest <> 0 then
  begin
    CNPJ_dest := Geral.SoNumero_TT(QrDestCNPJ_CPF.Value);
    if MyObjects.FIC(CNPJ_dest = '', EdDest,
      'O destinat�rio selecionado n�o possui CNPJ/CPF em seu cadastro!') then Exit;
  end;
  //
  Situacoes := '';
  if Geral.IntInConjunto(1, CGStatus.Value) then
   Geral.AddStrToStr_Separador(NFe_CodAutorizaTxt, ',', Situacoes);
  if Geral.IntInConjunto(2, CGStatus.Value) then
   Geral.AddStrToStr_Separador(NFe_CodCanceladTxt, ',', Situacoes);
  if Geral.IntInConjunto(4, CGStatus.Value) then
   Geral.AddStrToStr_Separador(NFe_CodInutilizTxt, ',', Situacoes);
  if Geral.IntInConjunto(8, CGStatus.Value) then
   Geral.AddStrToStr_Separador(NFe_CodDenegadoTxt, ',', Situacoes);
  //
  if MyObjects.FIC(Situacoes = '', CGStatus,
    'Nenhum status de NFe foi definido!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  //
  QrA.Close;
  QrA.SQL.Clear;
  QrA.SQL.Add('SELECT caba.*');
  QrA.SQL.Add('FROM nfecaba caba');
  QrA.SQL.Add('WHERE caba.ide_mod IN (' + NFe_AllModelos + ')');
  QrA.SQL.Add('AND caba.emit_CNPJ="' + CNPJ_emit + '"');
  QrA.SQL.Add('AND caba.Status IN (' + Situacoes + ')');
  if CodDest <> 0 then
    QrA.SQL.Add('AND caba.CodInfoDest = ' + Dest_TXT);
  //
  QrA.SQL.Add(dmkPF.SQL_Periodo(' AND caba.ide_dEmi ',
    TPIni.Date, TPFim.Date, True, True));
  //
  QrA.Open;
  if QrA.RecordCount = 0 then
  begin
    Screen.Cursor := crDefault;
    Geral.MensagemBox('Nenhuma NFe foi localizada!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  DirRaiz := EdRaiz.Text;
  if not Geral.VerificaDir(DirRaiz, '\',
  'Diret�rio raiz da exporta��o de XML de NFe', True) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  DirCria := DirRaiz + EdArq.Text;
  if DirectoryExists(DirCria) then
  begin
    if MLAGeral.GetAllFiles(True, DirCria + '\*.*', nil, False) > 0 then
    begin
      Geral.MensagemBox('A��o abortada! Pasta j� existe!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  Geral.VerificaDir(DirCria, '\', 'Diret�rio da exporta��o de XML de NFe', True);
  QrA.First;
  while not QrA.Eof do
  begin
    DmNFe_0000.QrArq.Close;
    DmNFe_0000.QrArq.Params[0].AsInteger := QrAIDCtrl.Value;
    DmNFe_0000.QrArq.Open;
    //
    Continua := False;
    if NFeXMLGeren.XML_DistribuiNFe(QrAId.Value, QrAStatus.Value,
    DmNFe_0000.QrArqXML_NFe.Value, DmNFe_0000.QrArqXML_Aut.Value,
    DmNFe_0000.QrArqXML_Can.Value, CkProt.Checked, XML_Distribuicao,
    QrAversao.Value, 'na base de dados') then
      //Geral.MensagemBox(XML_Distribuicao, 'XML da NFe protocolada', MB_OK+MB_ICONINFORMATION);
      Continua :=
        NFeXMLGeren.SalvaXML(DirCria + QrAId.Value + '.xml', XML_Distribuicao);
    if not Continua then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    // Eventos!

    //
    QrA.Next;
  end;
  //
  {
  //Zipar Precisa do WinZip
  if CkZipar.Checked then
  begin
    MLAGeral.CompactaArquivo(DirCria, '*.*', DirRaiz, EdArq.Text+'.zip', False);
    Geral.MensagemBox('Exporta��o finalizada!' + #13#10 + 'Arquivo: ' + #13#10 +
    dmkPF.CaminhoArquivo(DirRaiz, EdArq.Text, 'zip'), 'Aviso', MB_OK+MB_ICONWARNING);
  end else
    Geral.MensagemBox('Exporta��o finalizada!' + #13#10 + 'Diret�rio: ' + #13#10 +
    DirCria, 'Aviso', MB_OK+MB_ICONWARNING);
  //
  }
  //ZIPAR N�o usa o WinZip
  if CkZipar.Checked then
  begin
    Application.CreateForm(TFmZForge, FmZForge);
    FmZForge.Show;
    CamZip := FmZForge.ZipaArquivo(1, ExtractFileDir(DirRaiz), DirCria,
        EdArq.Text, '', False, False);
    FmZForge.Destroy;
    //
    if Length(CamZip) = 0 then
    begin
      Geral.MensagemBox('Falha ao zipar arquivo!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  Geral.MensagemBox('Exporta��o finalizada!' + #13#10 + 'Diret�rio: ' + #13#10 +
    DirCria, 'Aviso', MB_OK+MB_ICONWARNING);
  Screen.Cursor := crDefault;
  //Parei Aqui
end;

procedure TFmNFeExportaXML.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeExportaXML.CGStatusClick(Sender: TObject);
begin
  BtOK.Enabled := CGStatus.Value > 0;
end;

procedure TFmNFeExportaXML.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeExportaXML.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  TPIni.Date := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPFim.Date := Geral.PrimeiroDiaDoMes(Date) -1;
  //
  CGStatus.Value := 3;
  //
  EdArq.Text := FormatDateTime('YYYYMMDD"_"HHNNSS', Now());
  //
  QrDest.Open;
end;

procedure TFmNFeExportaXML.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeExportaXML.SbRaizClick(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Dir) then
    EdRaiz.Text := Dir;
end;

end.
