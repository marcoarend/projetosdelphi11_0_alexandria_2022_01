object FmNFeExportaXML: TFmNFeExportaXML
  Left = 339
  Top = 185
  Caption = 'XML-EXPOR-001 :: Exporta XML de NF-e'
  ClientHeight = 604
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 309
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Emitente:'
      end
      object Label14: TLabel
        Left = 8
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Destinat'#225'rio:'
      end
      object Label1: TLabel
        Left = 8
        Top = 224
        Width = 54
        Height = 13
        Caption = 'Pasta Raiz:'
      end
      object SbRaiz: TSpeedButton
        Left = 752
        Top = 240
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbRaizClick
      end
      object Label5: TLabel
        Left = 8
        Top = 264
        Width = 233
        Height = 13
        Caption = 'Nome da pasta (e arquivo zip) a ser(em) criado(s):'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 576
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDest: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBDest
        IgnoraDBLookupComboBox = False
      end
      object CBDest: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 576
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsDest
        TabOrder = 3
        dmkEditCB = EdDest
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object GroupBox2: TGroupBox
        Left = 644
        Top = 15
        Width = 129
        Height = 118
        Caption = ' Per'#237'odo: '
        TabOrder = 4
        object Label2: TLabel
          Left = 8
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label3: TLabel
          Left = 8
          Top = 64
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object TPIni: TdmkEditDateTimePicker
          Left = 8
          Top = 36
          Width = 112
          Height = 21
          Date = 40182.966222291670000000
          Time = 40182.966222291670000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPFim: TdmkEditDateTimePicker
          Left = 8
          Top = 80
          Width = 112
          Height = 21
          Date = 40182.966222291670000000
          Time = 40182.966222291670000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
      object CGStatus: TdmkCheckGroup
        Left = 8
        Top = 84
        Width = 632
        Height = 49
        Caption = ' Status da NFe: '
        Columns = 4
        ItemIndex = 2
        Items.Strings = (
          'Autorizadas'
          'Canceladas'
          'Inutilizadas'
          'Denegadas')
        TabOrder = 5
        OnClick = CGStatusClick
        UpdType = utYes
        Value = 4
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 136
        Width = 765
        Height = 85
        Caption = ' Outros filtros: '
        TabOrder = 6
        object CkProt: TCheckBox
          Left = 12
          Top = 20
          Width = 741
          Height = 17
          Caption = 'Incluir protocolo de autoriza'#231#227'o / cancelamento da NFe.'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 0
        end
        object CkUmArqPorNFe: TCheckBox
          Left = 12
          Top = 40
          Width = 741
          Height = 17
          Caption = 'Gerar um arquivo para cada NFe / inutiliza'#231#227'o.'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 1
        end
        object CkZipar: TCheckBox
          Left = 12
          Top = 60
          Width = 741
          Height = 17
          Caption = 'Zipar a pasta gerada.'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
      object EdRaiz: TEdit
        Left = 8
        Top = 240
        Width = 741
        Height = 21
        TabOrder = 7
        Text = 'C:\Dermatek\ExportaXML'
      end
      object EdArq: TEdit
        Left = 8
        Top = 280
        Width = 765
        Height = 21
        TabOrder = 8
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 309
      Width = 784
      Height = 139
      Align = alClient
      DataSource = DsA
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 259
        Height = 32
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 259
        Height = 32
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 259
        Height = 32
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 496
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 540
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDest: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'Tipo, IF(Tipo=0,CNPJ, CPF) CNPJ_CPF'
      'FROM Entidades'
      'WHERE ('
      '     (Tipo=0 AND CNPJ <> "")'
      '     OR'
      '     (Tipo=1 AND CPF <> "")'
      ')'
      'ORDER BY NO_ENT'
      '')
    Left = 120
    Top = 92
    object QrDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrDestTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDestCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsDest: TDataSource
    DataSet = QrDest
    Left = 148
    Top = 92
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE ide_mod=55'
      'AND emit_CNPJ="02595943000130"'
      'AND ide_dEmi BETWEEN "2009-09-01" AND "2009-09-30"')
    Left = 8
    Top = 4
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 36
    Top = 4
  end
end
