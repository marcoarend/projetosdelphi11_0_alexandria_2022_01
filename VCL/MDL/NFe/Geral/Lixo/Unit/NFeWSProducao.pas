unit NFeWSProducao;

interface

uses StdCtrls;

type
  TTipoConsumoWS = (tcwsRecepcaoEvento, tcwsNfeInutilizacao,
    tcwsNfeConsultaProtocolo, tcwsNfeStatusServico, tcwsNfeConsultaCadastro,
    tcwsNFeAutorizacao, tcwsNFeRetAutorizacao, tcwsNFeDistribuicaoDFe,
    tcwsNfeRecepcao, tcwsNfeRetRecepcao, tcwsNfeDownloadNF, tcwsNfeConsultaDest);
  TNFeWSProducao = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemWebServer_producao_100(UFServico: String;
              Acao: TTipoConsumoWS): String;
    function  ObtemWebServer_producao_200(UFServico: String;
              Acao: TTipoConsumoWS): String;
    function  ObtemWebServer_producao_310(UFServico: String;
              Acao: TTipoConsumoWS): String;
    function  ObtemWebServer_producao_400(UFServico: String;
              Acao: TTipoConsumoWS): String;
  end;

var
  UnNFeWSProducao: TNFeWSProducao;

implementation

uses dmkGeral;

{ TNFeWSProducao }

function TNFeWSProducao.ObtemWebServer_producao_100(UFServico: String;
  Acao: TTipoConsumoWS): String;
begin
  Result := '';
  //
  if UFServico = 'AM' then //Sefaz Amazonas - (AM)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/RecepcaoEvento';
    end;
  end else
  if UFServico = 'BA' then //Sefaz Bahia - (BA)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx';
    end;
  end else
  if UFServico = 'CE' then //Sefaz Cear� - (CE)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento?wsdl';
    end;
  end else
  if UFServico = 'GO' then //Sefaz Goias - (GO)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento?wsdl';
    end;
  end else
  if UFServico = 'MG' then //Sefaz Minas Gerais - (MG)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento';
    end;
  end else
  if UFServico = 'MS' then //Sefaz Mato Grosso do Sul - (MS)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/RecepcaoEvento';
    end;
  end else
  if UFServico = 'MT' then //Sefaz Mato Grosso - (MT)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento?wsdl';
    end;
  end else
  if UFServico = 'PE' then //Sefaz Pernambuco - (PE)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento';
    end;
  end else
  if UFServico = 'PR' then //Sefaz Paran� - (PR)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe2.fazenda.pr.gov.br/nfe-evento/NFeRecepcaoEvento?wsdl';
    end;
  end else
  if UFServico = 'RS' then //Sefaz Rio Grande do Sul - (RS)
  begin
    case Acao of
      tcwsNfeConsultaCadastro:
        Result := 'https://cad.sefazrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx';
    end;
  end else
  if UFServico = 'SP' then //Sefaz S�o Paulo - (SP)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx';
    end;
  end else
  if UFServico = 'SVAN' then //Sefaz Virtual Ambiente Nacional - (SVAN)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx';
    end;
  end else
  if UFServico = 'SVRS' then //Sefaz Virtual Rio Grande do Sul - (SVRS)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://cad.svrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx';
    end;
  end else
  if UFServico = 'SVC-AN' then //Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://www.svc.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx';
    end;
  end else
  if UFServico = 'SVC-RS' then //Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx';
    end;
  end else
  if UFServico = 'AN' then //Ambiente Nacional - (AN)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://www.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx';
      tcwsNFeDistribuicaoDFe:
        Result := 'https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx';
    end;
  end else
  begin
    Geral.MB_Erro('UFServico n�o implementada!' + sLineBreak +
      'UF: ' + UFServico + 'Fun��o: ' + 'TNFeWSProducao.ObtemWebServer_producao_100');
  end;
end;

function TNFeWSProducao.ObtemWebServer_producao_200(UFServico: String;
  Acao: TTipoConsumoWS): String;
begin
  Result := '';
  //
  if UFServico = 'AM' then //Sefaz Amazonas - (AM)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRecepcao2';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetRecepcao2';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeStatusServico2';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2';
    end;
  end else
  if UFServico = 'BA' then //Sefaz Bahia - (BA)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeRecepcao2.asmx';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeRetRecepcao2.asmx';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/nfeinutilizacao2.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/nfeconsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeStatusServico2.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx';
    end;
  end else
  if UFServico = 'CE' then //Sefaz Cear� - (CE)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRecepcao2?wsdl';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetRecepcao2?wsdl';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2?wsdl';
      tcwsNfeDownloadNF:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF?wsdl';
    end;
  end else
  if UFServico = 'GO' then //Sefaz Goias - (GO)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRecepcao2?wsdl';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetRecepcao2?wsdl';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2?wsdl';
    end;
  end else
  if UFServico = 'MG' then //Sefaz Minas Gerais - (MG)
  begin
    case Acao of
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/cadconsultacadastro2';
      tcwsNfeRecepcao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRecepcao2';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetRecepcao2';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeStatus2';
    end;
  end else
  if UFServico = 'MS' then //Sefaz Mato Grosso do Sul - (MS)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRecepcao2';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetRecepcao2';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/CadConsultaCadastro2';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeStatusServico2';
    end;
  end else
  if UFServico = 'MT' then //Sefaz Mato Grosso - (MT)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRecepcao2?wsdl';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetRecepcao2?wsdl';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2?wsdl';
    end;
  end else
  if UFServico = 'PE' then //Sefaz Pernambuco - (PE)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRecepcao2';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetRecepcao2';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/CadConsultaCadastro2';
    end;
  //Sefaz Paran� - (PR) => N�o tem vers�o 2.00
  //Sefaz Rio Grande do Sul - (RS) => N�o tem vers�o 2.00
  end else
  if UFServico = 'SP' then //Sefaz S�o Paulo - (SP)
  begin
    case Acao of
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/cadconsultacadastro2.asmx';
    end;
  //Sefaz Virtual Ambiente Nacional - (SVAN) => N�o tem vers�o 2.00
  //Sefaz Virtual Rio Grande do Sul - (SVRS) => N�o tem vers�o 2.00
  //Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN) => N�o tem vers�o 2.00
  //Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS) => N�o tem vers�o 2.00
  end else
  if UFServico = 'AN' then //Ambiente Nacional - (AN)
  begin
    case Acao of
      tcwsNfeConsultaDest:
        Result := 'https://www.nfe.fazenda.gov.br/NFeConsultaDest/NFeConsultaDest.asmx';
      tcwsNfeDownloadNF:
        Result := 'https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx';
    end;
  end else
  begin
    Geral.MB_Erro('UFServico n�o implementada!' + sLineBreak +
      'UF: ' + UFServico + 'Fun��o: ' + 'TNFeWSProducao.ObtemWebServer_producao_200');
  end;
end;

function TNFeWSProducao.ObtemWebServer_producao_310(UFServico: String;
  Acao: TTipoConsumoWS): String;
begin
  Result := '';
  //
  if UFServico = 'AM' then //Sefaz Amazonas - (AM)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeStatusServico2';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao';
    end;
  end else
  if UFServico = 'BA' then //Sefaz Bahia - (BA)
  begin
    case Acao of
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NfeInutilizacao/NfeInutilizacao.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NfeConsulta/NfeConsulta.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NfeStatusServico/NfeStatusServico.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'CE' then //Sefaz Cear� - (CE)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2?wsdl';
      tcwsNfeDownloadNF:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao?wsdl';
    end;
  end else
  if UFServico = 'GO' then //Sefaz Goias - (GO)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl';
    end;
  end else
  if UFServico = 'MG' then //Sefaz Minas Gerais - (MG)
  begin
    case Acao of
      tcwsNfeRecepcao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRecepcao2';
      tcwsNfeRetRecepcao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetRecepcao2';
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeStatus2';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao';
    end;
  end else
  if UFServico = 'MS' then //Sefaz Mato Grosso do Sul - (MS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeStatusServico2';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeAutorizacao';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao';
    end;
  end else
  if UFServico = 'MT' then //Sefaz Mato Grosso - (MT)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao2?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta2?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico2?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl';
    end;
  end else
  if UFServico = 'PE' then //Sefaz Pernambuco - (PE)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/CadConsultaCadastro2';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl';
    end;
  end else
  if UFServico = 'PR' then //Sefaz Paran� - (PR)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeInutilizacao3?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeConsulta3?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeStatusServico3?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/CadConsultaCadastro2?wsdl';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl';
    end;
  end else
  if UFServico = 'RS' then //Sefaz Rio Grande do Sul - (RS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'SP' then //Sefaz S�o Paulo - (SP)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeconsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfestatusservico2.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx';
    end;
  end else
  if UFServico = 'SVAN' then //Sefaz Virtual Ambiente Nacional - (SVAN)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx';
      tcwsNfeDownloadNF:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'SVRS' then //Sefaz Virtual Rio Grande do Sul - (SVRS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'SVC-AN' then //Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN)
  begin
    case Acao of
      tcwsNfeConsultaProtocolo:
        Result := 'https://www.svc.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://www.svc.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://www.svc.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://www.svc.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'SVC-RS' then //Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS)
  begin
    case Acao of
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx';
    end;
  end else
  if UFServico = 'AN' then //Ambiente Nacional - (AN)
  begin
    case Acao of
      tcwsNfeConsultaDest:
        Result := 'https://www.nfe.fazenda.gov.br/NFeConsultaDest/NFeConsultaDest.asmx';
      tcwsNfeDownloadNF:
        Result := 'https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx';
    end;
  end else
  begin
    Geral.MB_Erro('UFServico n�o implementada!' + sLineBreak +
      'UF: ' + UFServico + 'Fun��o: ' + 'TNFeWSProducao.ObtemWebServer_producao_310');
  end;
end;

function TNFeWSProducao.ObtemWebServer_producao_400(UFServico: String;
  Acao: TTipoConsumoWS): String;
begin
  Result := '';
  //
  if UFServico = 'AM' then //Sefaz Amazonas - (AM)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeInutilizacao4';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeConsulta4';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeStatusServico4';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/RecepcaoEvento4';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao4';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao4';
    end;
  end else
  if UFServico = 'BA' then //Sefaz Bahia - (BA)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeInutilizacao4/NFeInutilizacao4.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeConsultaProtocolo4/NFeConsultaProtocolo4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeStatusServico4/NFeStatusServico4.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/CadConsultaCadastro4/CadConsultaCadastro4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeAutorizacao4/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.ba.gov.br/webservices/NFeRetAutorizacao4/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'CE' then //Sefaz Cear� - (CE)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeInutilizacao4?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeConsultaProtocolo4?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeStatusServico4?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/CadConsultaCadastro4?wsdl';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeRecepcaoEvento4?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeAutorizacao4?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.ce.gov.br/nfe4/services/NFeRetAutorizacao4?wsdl';
    end;
  end else
  if UFServico = 'GO' then //Sefaz Goias - (GO)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeInutilizacao4?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeConsultaProtocolo4?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeStatusServico4?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/CadConsultaCadastro4?wsdl';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeRecepcaoEvento4?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeAutorizacao4?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.go.gov.br/nfe/services/NFeRetAutorizacao4?wsdl';
    end;
  end else
  if UFServico = 'MG' then //Sefaz Minas Gerais - (MG)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeInutilizacao4';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeConsultaProtocolo4';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeStatusServico4';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeRecepcaoEvento4';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeAutorizacao4';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NFeRetAutorizacao4';
    end;
  end else
  if UFServico = 'MS' then //Sefaz Mato Grosso do Sul - (MS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeInutilizacao4';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeConsultaProtocolo4';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeStatusServico4';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/CadConsultaCadastro4';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeRecepcaoEvento4';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeAutorizacao4';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.ms.gov.br/ws/NFeRetAutorizacao4';
    end;
  end else
  if UFServico = 'MT' then //Sefaz Mato Grosso - (MT)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao4?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta4?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico4?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro4?wsdl';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento4?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao4?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao4?wsdl';
    end;
  end else
  if UFServico = 'PE' then //Sefaz Pernambuco - (PE)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeInutilizacao4';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeConsultaProtocolo4';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeStatusServico4';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeRecepcaoEvento4';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeAutorizacao4';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NFeRetAutorizacao4';
    end;
  end else
  if UFServico = 'PR' then //Sefaz Paran� - (PR)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeInutilizacao4?wsdl';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeConsultaProtocolo4?wsdl';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeStatusServico4?wsdl';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/CadConsultaCadastro4?wsdl';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeRecepcaoEvento4?wsdl';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeAutorizacao4?wsdl';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefa.pr.gov.br/nfe/NFeRetAutorizacao4?wsdl';
    end;
  end else
  if UFServico = 'RS' then //Sefaz Rio Grande do Sul - (RS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao4.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeConsulta/NfeConsulta4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico4.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://cad.sefazrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.sefazrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'SP' then //Sefaz S�o Paulo - (SP)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeinutilizacao4.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeconsultaprotocolo4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfestatusservico4.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/cadconsultacadastro4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nferecepcaoevento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao4.asmx';
    end;
  end else
  if UFServico = 'SVAN' then //Sefaz Virtual Ambiente Nacional - (SVAN)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeInutilizacao4/NFeInutilizacao4.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeConsultaProtocolo4/NFeConsultaProtocolo4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeAutorizacao4/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://www.sefazvirtual.fazenda.gov.br/NFeRetAutorizacao4/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'SVRS' then //Sefaz Virtual Rio Grande do Sul - (SVRS)
  begin
    case Acao of
      tcwsNfeInutilizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao4.asmx';
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico4.asmx';
      tcwsNfeConsultaCadastro:
        Result := 'https://cad.svrs.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'SVC-AN' then //Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN)
  begin
    case Acao of
      tcwsNfeConsultaProtocolo:
        Result := 'https://www.svc.fazenda.gov.br/NFeConsultaProtocolo4/NFeConsultaProtocolo4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://www.svc.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://www.svc.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://www.svc.fazenda.gov.br/NFeAutorizacao4/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://www.svc.fazenda.gov.br/NFeRetAutorizacao4/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'SVC-RS' then //Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS)
  begin
    case Acao of
      tcwsNfeConsultaProtocolo:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta4.asmx';
      tcwsNfeStatusServico:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico4.asmx';
      tcwsRecepcaoEvento:
        Result := 'https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento4.asmx';
      tcwsNFeAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao4.asmx';
      tcwsNFeRetAutorizacao:
        Result := 'https://nfe.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao4.asmx';
    end;
  end else
  if UFServico = 'AN' then //Ambiente Nacional - (AN)
  begin
    case Acao of
      tcwsRecepcaoEvento:
        Result := 'https://www.nfe.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx';
    end;
  end else
  begin
    Geral.MB_Erro('UFServico n�o implementada!' + sLineBreak +
      'UF: ' + UFServico + 'Fun��o: ' + 'TNFeWSProducao.ObtemWebServer_producao_400');
  end;
end;

end.
