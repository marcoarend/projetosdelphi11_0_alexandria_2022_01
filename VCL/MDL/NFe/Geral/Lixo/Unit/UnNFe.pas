unit UnNFe;

interface

uses StdCtrls;

type
  TNfeWS = class
  private
    FUF: String;
    FServico: String;
    FVersao: Double;
    FUrl: String;
    FtpAmb: Integer;
  published
    constructor Create();
    destructor Destroy;
    property UF: String read FUF;
    property Servico: String read FServico;
    property Versao: Double read FVersao;
    property Url: String read FUrl;
    property tpAmb: Integer read FtpAmb default 0;
  end;
  TUnNFe = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ObtemVersaoServico(): Double;
  end;

var
  UNfe: TUnNFe;

implementation

uses dmkGeral;

{ TNfeWS }

constructor TNfeWS.Create();
begin
  FUF      := '';
  FServico := '';
  FVersao  := 0;
  FUrl     := '';
  FtpAmb   := 0;
end;

destructor TNfeWS.Destroy;
begin
  //
end;

{ TNFeWSHomologacao }

end.
