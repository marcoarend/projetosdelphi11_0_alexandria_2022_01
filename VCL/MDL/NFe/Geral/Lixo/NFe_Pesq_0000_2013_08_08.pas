unit NFe_Pesq_0000_2013_08_08;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  dmkCheckGroup;

type
  TFmNFe_Pesq_0000 = class(TForm)
    PnDados: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrNFeCabA_: TmySQLQuery;
    DsNFeCabA_: TDataSource;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    QrNFeCabA_NO_Cli: TWideStringField;
    QrNFeCabA_FatID: TIntegerField;
    QrNFeCabA_FatNum: TIntegerField;
    QrNFeCabA_Empresa: TIntegerField;
    QrNFeCabA_LoteEnv: TIntegerField;
    QrNFeCabA_Id: TWideStringField;
    QrNFeCabA_ide_natOp: TWideStringField;
    QrNFeCabA_ide_serie: TIntegerField;
    QrNFeCabA_ide_nNF: TIntegerField;
    QrNFeCabA_ide_dEmi: TDateField;
    QrNFeCabA_ide_tpNF: TSmallintField;
    QrNFeCabA_ICMSTot_vProd: TFloatField;
    QrNFeCabA_ICMSTot_vNF: TFloatField;
    QrNFeCabA_xMotivo: TWideStringField;
    QrNFeCabA_dhRecbto: TWideStringField;
    QrNFeCabA_IDCtrl: TIntegerField;
    PMLeArq: TPopupMenu;
    Cancelamento1: TMenuItem;
    QrNFeCabAMsg_: TmySQLQuery;
    DsNFeCabAMsg_: TDataSource;
    QrNFeCabAMsg_FatID: TIntegerField;
    QrNFeCabAMsg_FatNum: TIntegerField;
    QrNFeCabAMsg_Empresa: TIntegerField;
    QrNFeCabAMsg_Controle: TIntegerField;
    QrNFeCabAMsg_Solicit: TIntegerField;
    QrNFeCabAMsg_Id: TWideStringField;
    QrNFeCabAMsg_tpAmb: TSmallintField;
    QrNFeCabAMsg_verAplic: TWideStringField;
    QrNFeCabAMsg_dhRecbto: TDateTimeField;
    QrNFeCabAMsg_nProt: TWideStringField;
    QrNFeCabAMsg_digVal: TWideStringField;
    QrNFeCabAMsg_cStat: TIntegerField;
    QrNFeCabAMsg_xMotivo: TWideStringField;
    frxDsA_: TfrxDBDataset;
    QrA_: TmySQLQuery;
    QrA_FatID: TIntegerField;
    QrA_FatNum: TIntegerField;
    QrA_Empresa: TIntegerField;
    QrA_LoteEnv: TIntegerField;
    QrA_versao: TFloatField;
    QrA_Id: TWideStringField;
    QrA_ide_cUF: TSmallintField;
    QrA_ide_cNF: TIntegerField;
    QrA_ide_natOp: TWideStringField;
    QrA_ide_indPag: TSmallintField;
    QrA_ide_mod: TSmallintField;
    QrA_ide_serie: TIntegerField;
    QrA_ide_nNF: TIntegerField;
    QrA_ide_dEmi: TDateField;
    QrA_ide_dSaiEnt: TDateField;
    QrA_ide_tpNF: TSmallintField;
    QrA_ide_cMunFG: TIntegerField;
    QrA_ide_tpImp: TSmallintField;
    QrA_ide_tpEmis: TSmallintField;
    QrA_ide_cDV: TSmallintField;
    QrA_ide_tpAmb: TSmallintField;
    QrA_ide_finNFe: TSmallintField;
    QrA_ide_procEmi: TSmallintField;
    QrA_ide_verProc: TWideStringField;
    QrA_emit_CNPJ: TWideStringField;
    QrA_emit_CPF: TWideStringField;
    QrA_emit_xNome: TWideStringField;
    QrA_emit_xFant: TWideStringField;
    QrA_emit_xLgr: TWideStringField;
    QrA_emit_nro: TWideStringField;
    QrA_emit_xCpl: TWideStringField;
    QrA_emit_xBairro: TWideStringField;
    QrA_emit_cMun: TIntegerField;
    QrA_emit_xMun: TWideStringField;
    QrA_emit_UF: TWideStringField;
    QrA_emit_CEP: TIntegerField;
    QrA_emit_cPais: TIntegerField;
    QrA_emit_xPais: TWideStringField;
    QrA_emit_fone: TWideStringField;
    QrA_emit_IE: TWideStringField;
    QrA_emit_IEST: TWideStringField;
    QrA_emit_IM: TWideStringField;
    QrA_emit_CNAE: TWideStringField;
    QrA_dest_CNPJ: TWideStringField;
    QrA_dest_CPF: TWideStringField;
    QrA_dest_xNome: TWideStringField;
    QrA_dest_xLgr: TWideStringField;
    QrA_dest_nro: TWideStringField;
    QrA_dest_xCpl: TWideStringField;
    QrA_dest_xBairro: TWideStringField;
    QrA_dest_cMun: TIntegerField;
    QrA_dest_xMun: TWideStringField;
    QrA_dest_UF: TWideStringField;
    QrA_dest_CEP: TWideStringField;
    QrA_dest_cPais: TIntegerField;
    QrA_dest_xPais: TWideStringField;
    QrA_dest_fone: TWideStringField;
    QrA_dest_IE: TWideStringField;
    QrA_dest_ISUF: TWideStringField;
    QrA_ICMSTot_vBC: TFloatField;
    QrA_ICMSTot_vICMS: TFloatField;
    QrA_ICMSTot_vBCST: TFloatField;
    QrA_ICMSTot_vST: TFloatField;
    QrA_ICMSTot_vProd: TFloatField;
    QrA_ICMSTot_vFrete: TFloatField;
    QrA_ICMSTot_vSeg: TFloatField;
    QrA_ICMSTot_vDesc: TFloatField;
    QrA_ICMSTot_vII: TFloatField;
    QrA_ICMSTot_vIPI: TFloatField;
    QrA_ICMSTot_vPIS: TFloatField;
    QrA_ICMSTot_vCOFINS: TFloatField;
    QrA_ICMSTot_vOutro: TFloatField;
    QrA_ICMSTot_vNF: TFloatField;
    QrA_ISSQNtot_vServ: TFloatField;
    QrA_ISSQNtot_vBC: TFloatField;
    QrA_ISSQNtot_vISS: TFloatField;
    QrA_ISSQNtot_vPIS: TFloatField;
    QrA_ISSQNtot_vCOFINS: TFloatField;
    QrA_RetTrib_vRetPIS: TFloatField;
    QrA_RetTrib_vRetCOFINS: TFloatField;
    QrA_RetTrib_vRetCSLL: TFloatField;
    QrA_RetTrib_vBCIRRF: TFloatField;
    QrA_RetTrib_vIRRF: TFloatField;
    QrA_RetTrib_vBCRetPrev: TFloatField;
    QrA_RetTrib_vRetPrev: TFloatField;
    QrA_ModFrete: TSmallintField;
    QrA_Transporta_CNPJ: TWideStringField;
    QrA_Transporta_CPF: TWideStringField;
    QrA_Transporta_XNome: TWideStringField;
    QrA_Transporta_IE: TWideStringField;
    QrA_Transporta_XEnder: TWideStringField;
    QrA_Transporta_XMun: TWideStringField;
    QrA_Transporta_UF: TWideStringField;
    QrA_RetTransp_vServ: TFloatField;
    QrA_RetTransp_vBCRet: TFloatField;
    QrA_RetTransp_PICMSRet: TFloatField;
    QrA_RetTransp_vICMSRet: TFloatField;
    QrA_RetTransp_CFOP: TWideStringField;
    QrA_RetTransp_CMunFG: TWideStringField;
    QrA_VeicTransp_Placa: TWideStringField;
    QrA_VeicTransp_UF: TWideStringField;
    QrA_VeicTransp_RNTC: TWideStringField;
    QrA_Cobr_Fat_NFat: TWideStringField;
    QrA_Cobr_Fat_vOrig: TFloatField;
    QrA_Cobr_Fat_vDesc: TFloatField;
    QrA_Cobr_Fat_vLiq: TFloatField;
    QrA_InfAdic_InfAdFisco: TWideMemoField;
    QrA_InfAdic_InfCpl: TWideMemoField;
    QrA_Exporta_UFEmbarq: TWideStringField;
    QrA_Exporta_XLocEmbarq: TWideStringField;
    QrA_Compra_XNEmp: TWideStringField;
    QrA_Compra_XPed: TWideStringField;
    QrA_Compra_XCont: TWideStringField;
    QrA_Status: TIntegerField;
    QrA_infProt_Id: TWideStringField;
    QrA_infProt_tpAmb: TSmallintField;
    QrA_infProt_verAplic: TWideStringField;
    QrA_infProt_dhRecbto: TDateTimeField;
    QrA_infProt_nProt: TWideStringField;
    QrA_infProt_digVal: TWideStringField;
    QrA_infProt_cStat: TIntegerField;
    QrA_infProt_xMotivo: TWideStringField;
    QrA__Ativo_: TSmallintField;
    QrA_Lk: TIntegerField;
    QrA_DataCad: TDateField;
    QrA_DataAlt: TDateField;
    QrA_UserCad: TIntegerField;
    QrA_UserAlt: TIntegerField;
    QrA_AlterWeb: TSmallintField;
    QrA_Ativo: TSmallintField;
    QrA_IDCtrl: TIntegerField;
    QrA_infCanc_Id: TWideStringField;
    QrA_infCanc_tpAmb: TSmallintField;
    QrA_infCanc_verAplic: TWideStringField;
    QrA_infCanc_dhRecbto: TDateTimeField;
    QrA_infCanc_nProt: TWideStringField;
    QrA_infCanc_digVal: TWideStringField;
    QrA_infCanc_cStat: TIntegerField;
    QrA_infCanc_xMotivo: TWideStringField;
    QrA_infCanc_cJust: TIntegerField;
    QrA_infCanc_xJust: TWideStringField;
    QrA_ID_TXT: TWideStringField;
    QrA_EMIT_ENDERECO: TWideStringField;
    QrA_EMIT_FONE_TXT: TWideStringField;
    QrA_EMIT_IE_TXT: TWideStringField;
    QrA_EMIT_IEST_TXT: TWideStringField;
    QrA_EMIT_CNPJ_TXT: TWideStringField;
    QrA_DEST_CNPJ_CPF_TXT: TWideStringField;
    QrA_DEST_ENDERECO: TWideStringField;
    QrA_DEST_CEP_TXT: TWideStringField;
    QrA_DEST_FONE_TXT: TWideStringField;
    QrA_DEST_IE_TXT: TWideStringField;
    QrA_TRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrA_TRANSPORTA_IE_TXT: TWideStringField;
    QrI_: TmySQLQuery;
    QrI_prod_cProd: TWideStringField;
    QrI_prod_cEAN: TWideStringField;
    QrI_prod_xProd: TWideStringField;
    QrI_prod_NCM: TWideStringField;
    QrI_prod_EXTIPI: TWideStringField;
    QrI_prod_genero: TSmallintField;
    QrI_prod_CFOP: TIntegerField;
    QrI_prod_uCom: TWideStringField;
    QrI_prod_qCom: TFloatField;
    QrI_prod_vUnCom: TFloatField;
    QrI_prod_vProd: TFloatField;
    QrI_prod_cEANTrib: TWideStringField;
    QrI_prod_uTrib: TWideStringField;
    QrI_prod_qTrib: TFloatField;
    QrI_prod_vUnTrib: TFloatField;
    QrI_prod_vFrete: TFloatField;
    QrI_prod_vSeg: TFloatField;
    QrI_prod_vDesc: TFloatField;
    QrI_Tem_IPI: TSmallintField;
    frxDsI_: TfrxDBDataset;
    QrN_: TmySQLQuery;
    QrO_: TmySQLQuery;
    frxDsN_: TfrxDBDataset;
    frxDsO_: TfrxDBDataset;
    QrI_nItem: TIntegerField;
    QrN_nItem: TIntegerField;
    QrN_ICMS_Orig: TSmallintField;
    QrN_ICMS_CST: TSmallintField;
    QrN_ICMS_modBC: TSmallintField;
    QrN_ICMS_pRedBC: TFloatField;
    QrN_ICMS_vBC: TFloatField;
    QrN_ICMS_pICMS: TFloatField;
    QrN_ICMS_vICMS: TFloatField;
    QrN_ICMS_modBCST: TSmallintField;
    QrN_ICMS_pMVAST: TFloatField;
    QrN_ICMS_pRedBCST: TFloatField;
    QrN_ICMS_vBCST: TFloatField;
    QrN_ICMS_pICMSST: TFloatField;
    QrN_ICMS_vICMSST: TFloatField;
    QrO_nItem: TIntegerField;
    QrO_IPI_clEnq: TWideStringField;
    QrO_IPI_CNPJProd: TWideStringField;
    QrO_IPI_cSelo: TWideStringField;
    QrO_IPI_qSelo: TFloatField;
    QrO_IPI_cEnq: TWideStringField;
    QrO_IPI_CST: TWideStringField;
    QrO_IPI_vBC: TFloatField;
    QrO_IPI_qUnid: TFloatField;
    QrO_IPI_vUnid: TFloatField;
    QrO_IPI_pIPI: TFloatField;
    QrO_IPI_vIPI: TFloatField;
    QrA_DOC_SEM_VLR_JUR: TWideStringField;
    QrA_DOC_SEM_VLR_FIS: TWideStringField;
    PMImprime: TPopupMenu;
    CampospreenchidosnoXMLdaNFe1: TMenuItem;
    QrNFeXMLI_: TmySQLQuery;
    frxDsNFeXMLI_: TfrxDBDataset;
    frxDsNFeCabA_: TfrxDBDataset;
    QrNFeXMLI_Codigo: TWideStringField;
    QrNFeXMLI_ID: TWideStringField;
    QrNFeXMLI_Valor: TWideStringField;
    QrNFeXMLI_Pai: TWideStringField;
    QrNFeXMLI_Descricao: TWideStringField;
    QrNFeXMLI_Campo: TWideStringField;
    QrY_: TmySQLQuery;
    frxDsY_: TfrxDBDataset;
    QrY_nDup: TWideStringField;
    QrY_dVenc: TDateField;
    QrY_vDup: TFloatField;
    QrNFeYIts_: TmySQLQuery;
    QrNFeYIts_Seq1: TIntegerField;
    QrNFeYIts_nDup1: TWideStringField;
    QrNFeYIts_dVenc1: TDateField;
    QrNFeYIts_vDup1: TFloatField;
    QrNFeYIts_Seq2: TIntegerField;
    QrNFeYIts_nDup2: TWideStringField;
    QrNFeYIts_dVenc2: TDateField;
    QrNFeYIts_vDup2: TFloatField;
    QrNFeYIts_Seq3: TIntegerField;
    QrNFeYIts_nDup3: TWideStringField;
    QrNFeYIts_dVenc3: TDateField;
    QrNFeYIts_vDup3: TFloatField;
    QrNFeYIts_Seq4: TIntegerField;
    QrNFeYIts_nDup4: TWideStringField;
    QrNFeYIts_dVenc4: TDateField;
    QrNFeYIts_vDup4: TFloatField;
    QrNFeYIts_Seq5: TIntegerField;
    QrNFeYIts_nDup5: TWideStringField;
    QrNFeYIts_dVenc5: TDateField;
    QrNFeYIts_vDup5: TFloatField;
    QrNFeYIts_Linha: TIntegerField;
    frxDsNFeYIts_: TfrxDBDataset;
    QrNFeYIts_xVenc1: TWideStringField;
    QrNFeYIts_xVenc2: TWideStringField;
    QrNFeYIts_xVenc3: TWideStringField;
    QrNFeYIts_xVenc4: TWideStringField;
    QrNFeYIts_xVenc5: TWideStringField;
    QrXVol_: TmySQLQuery;
    frxDsXvol_: TfrxDBDataset;
    QrXVol_qVol: TFloatField;
    QrXVol_esp: TWideStringField;
    QrXVol_marca: TWideStringField;
    QrXVol_nVol: TWideStringField;
    QrXVol_pesoL: TFloatField;
    QrXVol_pesoB: TFloatField;
    QrV_: TmySQLQuery;
    frxDsV_: TfrxDBDataset;
    QrV_nItem: TIntegerField;
    QrV_InfAdProd: TWideMemoField;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabANO_Cli: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAnProt: TWideStringField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAdhRecbto: TWideStringField;
    QrNFeCabAIDCtrl: TIntegerField;
    DsNFeCabA: TDataSource;
    frxDsNFeCabA: TfrxDBDataset;
    QrNFeCabAMsg: TmySQLQuery;
    QrNFeCabAMsgFatID: TIntegerField;
    QrNFeCabAMsgFatNum: TIntegerField;
    QrNFeCabAMsgEmpresa: TIntegerField;
    QrNFeCabAMsgControle: TIntegerField;
    QrNFeCabAMsgSolicit: TIntegerField;
    QrNFeCabAMsgId: TWideStringField;
    QrNFeCabAMsgtpAmb: TSmallintField;
    QrNFeCabAMsgverAplic: TWideStringField;
    QrNFeCabAMsgdhRecbto: TDateTimeField;
    QrNFeCabAMsgnProt: TWideStringField;
    QrNFeCabAMsgdigVal: TWideStringField;
    QrNFeCabAMsgcStat: TIntegerField;
    QrNFeCabAMsgxMotivo: TWideStringField;
    DsNFeCabAMsg: TDataSource;
    frxDsA: TfrxDBDataset;
    QrA: TmySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_NFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAIDCtrl: TIntegerField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrAID_TXT: TWideStringField;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_IEST_TXT: TWideStringField;
    QrAEMIT_CNPJ_TXT: TWideStringField;
    QrADEST_CNPJ_CPF_TXT: TWideStringField;
    QrADEST_ENDERECO: TWideStringField;
    QrADEST_CEP_TXT: TWideStringField;
    QrADEST_FONE_TXT: TWideStringField;
    QrADEST_IE_TXT: TWideStringField;
    QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrATRANSPORTA_IE_TXT: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    QrI: TmySQLQuery;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrITem_IPI: TSmallintField;
    QrInItem: TIntegerField;
    frxDsI: TfrxDBDataset;
    frxDsN: TfrxDBDataset;
    QrN: TmySQLQuery;
    QrNnItem: TIntegerField;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrO: TmySQLQuery;
    QrOnItem: TIntegerField;
    QrOIPI_clEnq: TWideStringField;
    QrOIPI_CNPJProd: TWideStringField;
    QrOIPI_cSelo: TWideStringField;
    QrOIPI_qSelo: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrOIPI_vIPI: TFloatField;
    frxDsO: TfrxDBDataset;
    frxDsY: TfrxDBDataset;
    QrY: TmySQLQuery;
    QrYnDup: TWideStringField;
    QrYdVenc: TDateField;
    QrYvDup: TFloatField;
    QrNFeYIts: TmySQLQuery;
    QrNFeYItsSeq1: TIntegerField;
    QrNFeYItsnDup1: TWideStringField;
    QrNFeYItsdVenc1: TDateField;
    QrNFeYItsvDup1: TFloatField;
    QrNFeYItsSeq2: TIntegerField;
    QrNFeYItsnDup2: TWideStringField;
    QrNFeYItsdVenc2: TDateField;
    QrNFeYItsvDup2: TFloatField;
    QrNFeYItsSeq3: TIntegerField;
    QrNFeYItsnDup3: TWideStringField;
    QrNFeYItsdVenc3: TDateField;
    QrNFeYItsvDup3: TFloatField;
    QrNFeYItsSeq4: TIntegerField;
    QrNFeYItsnDup4: TWideStringField;
    QrNFeYItsdVenc4: TDateField;
    QrNFeYItsvDup4: TFloatField;
    QrNFeYItsSeq5: TIntegerField;
    QrNFeYItsnDup5: TWideStringField;
    QrNFeYItsdVenc5: TDateField;
    QrNFeYItsvDup5: TFloatField;
    QrNFeYItsLinha: TIntegerField;
    QrNFeYItsxVenc1: TWideStringField;
    QrNFeYItsxVenc2: TWideStringField;
    QrNFeYItsxVenc3: TWideStringField;
    QrNFeYItsxVenc4: TWideStringField;
    QrNFeYItsxVenc5: TWideStringField;
    frxDsNFeYIts: TfrxDBDataset;
    frxDsXvol: TfrxDBDataset;
    QrXVol: TmySQLQuery;
    QrXVolqVol: TFloatField;
    QrXVolesp: TWideStringField;
    QrXVolmarca: TWideStringField;
    QrXVolnVol: TWideStringField;
    QrXVolpesoL: TFloatField;
    QrXVolpesoB: TFloatField;
    QrV: TmySQLQuery;
    QrVnItem: TIntegerField;
    QrVInfAdProd: TWideMemoField;
    frxDsV: TfrxDBDataset;
    QrNFeXMLI: TmySQLQuery;
    QrNFeXMLICodigo: TWideStringField;
    QrNFeXMLIID: TWideStringField;
    QrNFeXMLIValor: TWideStringField;
    QrNFeXMLIPai: TWideStringField;
    QrNFeXMLIDescricao: TWideStringField;
    QrNFeXMLICampo: TWideStringField;
    frxDsNFeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_NFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    NFe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    Panel3: TPanel;
    QrNFeCabAdest_xNome: TWideStringField;
    QrAide_DSaiEnt_Txt: TWideStringField;
    QrNFeCabA_versao: TFloatField;
    QrNFeCabAversao: TFloatField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAide_hSaiEnt: TTimeField;
    QrAide_dhCont: TDateTimeField;
    QrAide_xJust: TWideStringField;
    QrAemit_CRT: TSmallintField;
    QrAdest_email: TWideStringField;
    QrAVagao: TWideStringField;
    QrABalsa: TWideStringField;
    QrAMODFRETE_TXT: TWideStringField;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaNFes: TfrxReport;
    N2: TMenuItem;
    Listadasnotaspesquisadas1: TMenuItem;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabANOME_tpEmis: TWideStringField;
    QrNFeCabANOME_tpNF: TWideStringField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFe_100: TmySQLQuery;
    frxDsNFe_100: TfrxDBDataset;
    QrNFe_XXX: TmySQLQuery;
    frxDsNFe_XXX: TfrxDBDataset;
    QrNFe_XXXOrdem: TIntegerField;
    QrNFe_XXXcStat: TIntegerField;
    QrNFe_XXXide_nNF: TIntegerField;
    QrNFe_XXXide_serie: TIntegerField;
    QrNFe_XXXide_AAMM_AA: TWideStringField;
    QrNFe_XXXide_AAMM_MM: TWideStringField;
    QrNFe_XXXide_dEmi: TDateField;
    QrNFe_XXXNOME_tpEmis: TWideStringField;
    QrNFe_XXXNOME_tpNF: TWideStringField;
    QrNFe_XXXStatus: TIntegerField;
    QrNFe_XXXMotivo: TWideStringField;
    QrNFe_100ide_nNF: TIntegerField;
    QrNFe_100ide_serie: TIntegerField;
    QrNFe_100ide_dEmi: TDateField;
    QrNFe_100ICMSTot_vProd: TFloatField;
    QrNFe_100ICMSTot_vST: TFloatField;
    QrNFe_100ICMSTot_vFrete: TFloatField;
    QrNFe_100ICMSTot_vSeg: TFloatField;
    QrNFe_100ICMSTot_vIPI: TFloatField;
    QrNFe_100ICMSTot_vOutro: TFloatField;
    QrNFe_100ICMSTot_vDesc: TFloatField;
    QrNFe_100ICMSTot_vNF: TFloatField;
    QrNFe_100ICMSTot_vBC: TFloatField;
    QrNFe_100ICMSTot_vICMS: TFloatField;
    QrNFe_100NOME_tpEmis: TWideStringField;
    QrNFe_100NOME_tpNF: TWideStringField;
    QrNFe_100Ordem: TIntegerField;
    QrNFe_XXXNOME_ORDEM: TWideStringField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFe_101: TmySQLQuery;
    frxDsNFe_101: TfrxDBDataset;
    QrNFe_101Ordem: TIntegerField;
    QrNFe_101ide_nNF: TIntegerField;
    QrNFe_101ide_serie: TIntegerField;
    QrNFe_101ide_AAMM_AA: TWideStringField;
    QrNFe_101ide_AAMM_MM: TWideStringField;
    QrNFe_101ide_dEmi: TDateField;
    QrNFe_101NOME_tpEmis: TWideStringField;
    QrNFe_101NOME_tpNF: TWideStringField;
    QrNFe_101infCanc_dhRecbto: TDateTimeField;
    QrNFe_101infCanc_nProt: TWideStringField;
    QrNFe_101Motivo: TWideStringField;
    QrNFe_101Id: TWideStringField;
    QrNFe_101Id_TXT: TWideStringField;
    PreviewdaNFe1: TMenuItem;
    QrADEST_XMUN_TXT: TWideStringField;
    QrNFe_100ICMSTot_vPIS: TFloatField;
    QrNFe_100ICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabA_cStat: TFloatField;
    QrNFeCabAcStat: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    BtEvento: TBitBtn;
    BtCancela: TBitBtn;
    BtInutiliza: TBitBtn;
    BtLeArq: TBitBtn;
    BtArq: TBitBtn;
    BtConsulta: TBitBtn;
    BitBtn1: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrNICMS_CSOSN: TIntegerField;
    frxA4A_002: TfrxReport;
    Splitter2: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGEventos: TDBGrid;
    TabSheet2: TTabSheet;
    DBGMensagens: TDBGrid;
    QrNFeEveRRet: TmySQLQuery;
    DsNFeEveRRet: TDataSource;
    QrNFeEveRRetControle: TIntegerField;
    QrNFeEveRRetSubCtrl: TIntegerField;
    QrNFeEveRRetret_versao: TFloatField;
    QrNFeEveRRetret_Id: TWideStringField;
    QrNFeEveRRetret_tpAmb: TSmallintField;
    QrNFeEveRRetret_verAplic: TWideStringField;
    QrNFeEveRRetret_cOrgao: TSmallintField;
    QrNFeEveRRetret_cStat: TIntegerField;
    QrNFeEveRRetret_xMotivo: TWideStringField;
    QrNFeEveRRetret_chNFe: TWideStringField;
    QrNFeEveRRetret_tpEvento: TIntegerField;
    QrNFeEveRRetret_xEvento: TWideStringField;
    QrNFeEveRRetret_nSeqEvento: TIntegerField;
    QrNFeEveRRetret_CNPJDest: TWideStringField;
    QrNFeEveRRetret_CPFDest: TWideStringField;
    QrNFeEveRRetret_emailDest: TWideStringField;
    QrNFeEveRRetret_dhRegEvento: TDateTimeField;
    QrNFeEveRRetret_TZD_UTC: TFloatField;
    QrNFeEveRRetret_nProt: TWideStringField;
    QrNFeEveRRetNO_EVENTO: TWideStringField;
    Panel5: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    BtReabre: TBitBtn;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Ck100e101: TCheckBox;
    Panel6: TPanel;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    CGcSitConf: TdmkCheckGroup;
    QrNFeCabAcSitNFe: TSmallintField;
    QrNFeCabAcSitConf: TSmallintField;
    QrNFeCabANO_cSitConf: TWideStringField;
    PMEventos: TPopupMenu;
    CorrigeManifestao1: TMenuItem;
    Panel7: TPanel;
    PMcSitConf: TPopupMenu;
    Manifestar1: TMenuItem;
    frxA4A_003: TfrxReport;
    QrANFeNT2013_003LTT: TSmallintField;
    QrM: TmySQLQuery;
    frxDsM: TfrxDBDataset;
    QrMFatID: TIntegerField;
    QrMFatNum: TIntegerField;
    QrMEmpresa: TIntegerField;
    QrMnItem: TIntegerField;
    QrMiTotTrib: TSmallintField;
    QrMvTotTrib: TFloatField;
    QrNFeCabANFeNT2013_003LTT: TSmallintField;
    mySQLQuery1: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    frxDBDataset1: TfrxDBDataset;
    QrAvTotTrib: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure Cancelamento1Click(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BtLeArqClick(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConsultaClick(Sender: TObject);
    procedure BtInutilizaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxA4A_000_GetValue(const VarName: string; var Value: Variant);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrIAfterScroll(DataSet: TDataSet);
    procedure QrYAfterOpen(DataSet: TDataSet);
    procedure QrNFeYItsCalcFields(DataSet: TDataSet);
    procedure QrXVolAfterOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtArqClick(Sender: TObject);
    procedure PMArqPopup(Sender: TObject);
    procedure NFe1Click(Sender: TObject);
    procedure Autorizao1Click(Sender: TObject);
    procedure Cancelamento2Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure DiretriodoarquivoXML1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure Listadasnotaspesquisadas1Click(Sender: TObject);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure QrNFe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaNFesGetValue(const VarName: string; var Value: Variant);
    procedure frxA4A_002GetValue(const VarName: string; var Value: Variant);
    procedure QrNFe_101CalcFields(DataSet: TDataSet);
    procedure PreviewdaNFe1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure QrNFeEveRRetCalcFields(DataSet: TDataSet);
    procedure DBGEventosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMEventosPopup(Sender: TObject);
    procedure CorrigeManifestao1Click(Sender: TObject);
    procedure CGcSitConfClick(Sender: TObject);
    procedure Manifestar1Click(Sender: TObject);
  private
    { Private declarations }
    FNFeYIts: String;
    FLote, FEmpresa: Integer;
    FChaveNFe, FProtocolo: String;
    // CabY
    FFaturas: array[0..14] of array[0..2] of String;
    // CabXVol
    Fesp, Fmarca, FnVol: String;
    FpesoL, FpesoB, FqVol: Double;
    // Impress�o de lista de NFes
    F_NFe_100, F_NFe_101, F_NFe_XXX: String;
    //
    FSo_O_ID: Boolean;
    //
    procedure DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
    procedure DefineVarsDePreparacao();
  public
    { Public declarations }
    procedure FechaNFeCabA();
    procedure ReopenNFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
    procedure ReopenNFeCabAMsg();
    procedure ReopenNFeEveRRet();
    //
    function  DefineQual_frxNFe(OQueFaz: TfrxImpComo): TfrxReport;
  end;

  var
  FmNFe_Pesq_0000: TFmNFe_Pesq_0000;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
NFeInut_0000, UMySQLModule, Module, NFeEmail, ModuleNFe_0000, Grade_Create,
NFeSteps_0200, NFeXMLGerencia, NFeEveRCab, MeuFrx, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFe_Pesq_0000.BtConsultaClick(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  case DModG.VersaoNFe(Geral.IMV(VAR_LIB_EMPRESAS)) of
    200:
    if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
    begin
      //
      FmNFeSteps_0200.PnLoteEnv.Visible := True;
      FmNFeSteps_0200.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpNFeVerConNFe.Value;
      FmNFeSteps_0200.Show;
      //
      FmNFeSteps_0200.RGAcao.ItemIndex := 5; // Consulta NFe
      DefineVarsDePreparacao();
      FmNFeSteps_0200.PreparaConsultaNFe(FEmpresa, QrNFeCabAIDCtrl.Value, FChaveNFe);
      //
      //
      //FmNFeSteps_0200.Destroy;
      //
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.BtImprimeClick(Sender: TObject);
begin
  {
  if QrNfeCabANFeNT2013_003LTT.Value = 3 then
  begin
    MyObjects.frxDefineDataSets(frxA4A_003, [
    frxDsA,
    frxDsI,
    frxDsM,
    frxDsN,
    frxDsNfeYIts,
    frxDsO,
    frxDsV,
    frxDsY
    ]);
    DefineFrx(frxA4A_003, ficMostra);
  end else
    DefineFrx(frxA4A_002, ficMostra);
  }
  DefineQual_frxNFe(ficMostra);  
end;

procedure TFmNFe_Pesq_0000.DBGEventosMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMEventos, DBGEventos, X,Y);
end;

procedure TFmNFe_Pesq_0000.DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
var
  I: Integer;
  //Page: TfrxReportPage;
begin
  Screen.Cursor := crHourGlass;
  //
  DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
  QrA.Close;
  QrA.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrA.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrA.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrA.Open;
  //
  QrI.Close;
  QrI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrI.Open;
  //
  QrY.Close;
  QrY.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrY.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrY.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrY.Open;
  //
  QrXVol.Close;
  QrXVol.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrXVol.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrXVol.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrXVol.Open;
  //
  case OQueFaz of
    ficMostra: //MyObjects.frxMostra(Report, 'NFe' + QrAId.Value);
    begin
      Report.PreviewOptions.ZoomMode := zmPageWidth;
      MyObjects.frxPrepara(Report, 'NFe' + QrAId.Value);
      for I := 0 to Report.PreviewPages.Count -1 do
      begin
        Report.PreviewPages.Page[I].PaperSize := DMPAPER_A4;
        Report.PreviewPages.ModifyPage(I, Report.PreviewPages.Page[I]);
      end;
      FmMeuFrx.ShowModal;
      FmMeuFrx.Destroy;
    end;
    {ficImprime:
    ficSalva}
    ficNone: ;// N�o faz nada!
    ficExporta: MyObjects.frxPrepara(Report, 'NFe' + QrAId.Value);
    else Geral.MensagemBox('A��o n�o definida para frx da DANFE!',
    'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.BtInutilizaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeInut_0000, FmNFeInut_0000, afmoNegarComAviso) then
  begin
    FmNFeInut_0000.ShowModal;
    FmNFeInut_0000.Destroy;
  end;
end;

procedure TFmNFe_Pesq_0000.Autorizao1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Aut.Value, 'XML Autoriza��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.BitBtn1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  BtImprimeClick(Self);
end;

procedure TFmNFe_Pesq_0000.BtReabreClick(Sender: TObject);
var
  IDCtrl: Integer;
begin
  IDCtrl := 0;
  if (QrNFeCabA.State <> dsInactive) then IDCtrl := QrNFeCabAIDCtrl.Value;
  ReopenNFeCabA(IDCtrl, False);
end;

procedure TFmNFe_Pesq_0000.BtEnviaClick(Sender: TObject);
var
  DirNFeXML, DirDANFEs, CNPJ_CPF: String;
begin
  // 2010-08-13
  DmNFe_0000.ReopenOpcoesNFe(DModG.QrEmpresasCodigo.Value, True);
  // Fim 2010-08-13
  if DBCheck.CriaFm(TFmNFeEmail, FmNFeEmail, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    DirNFeXML := DmNFe_0000.QrFilialDirNFeProt.Value;
    if DirNFeXML = '' then DirNFeXML := 'C:\Dermatek\NFe\Clientes';
    DirDANFEs := DmNFe_0000.QrFilialDirDANFEs.Value;
    if DirDANFEs = '' then DirDANFEs := 'C:\Dermatek\NFe\Clientes';
    if QrNFeCabAdest_CNPJ.Value <> '' then
      CNPJ_CPF := QrNFeCabAdest_CNPJ.Value
    else
      CNPJ_CPF := QrNFeCabAdest_CPF.Value;
    //
    Geral.VerificaDir(DirNFeXML, '\', '', True);
    DirNFeXML := DirNFeXML + CNPJ_CPF;
    Geral.VerificaDir(DirDANFEs, '\', '', True);
    DirDANFEs := DirDANFEs + CNPJ_CPF;
    //
    FmNFeEmail.EdCNPJ_CPF.ValueVariant   := CNPJ_CPF;
    FmNFeEmail.Eddest_xNome.Text         := QrNFeCabAdest_xNome.Value;

    FmNFeEmail.EdNFeId.ValueVariant      := QrNFeCabAId.Value;
    FmNFeEmail.EdNFeStatus.ValueVariant  := QrNFeCabAcStat.Value;
    FmNFeEmail.EdNFeStatus_TXT.Text      := QrNFeCabAxMotivo.Value;
    FmNFeEmail.EdNFeIDCtrl.ValueVariant  := QrNFeCabAIDCtrl.Value;
    FmNFeEmail.EdNFeXML.Text             := DirNFeXML;
    FmNFeEmail.EdDANFE.Text              := DirDANFEs;
    //FmNFeEmail.FFrx                      := frxA4A_002;
    FmNFeEmail.FFrx                      := DefineQual_frxNFe(ficNone);
    FmNFeEmail.EdVersaoNFe.ValueVariant  := QrNFeCabAversao.Value;
    //DefineFrx(frxA4A_002, ficExporta);
    DefineFrx(FmNFeEmail.FFrx, ficExporta);
    //
    Screen.Cursor := crDefault;
    FmNFeEmail.ShowModal;
    FmNFeEmail.Destroy;
  end;
end;

procedure TFmNFe_Pesq_0000.BtEventoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeEveRCab, FmNFeEveRCab, afmoNegarComAviso) then
  begin
    FmNFeEveRCab.ReopenNfeCabA(
      QrNFeCabAFatID.Value, QrNFeCabAFatNum.Value, QrNFeCabAEmpresa.Value);
    FmNFeEveRCab.ShowModal;
    FmNFeEveRCab.Destroy;
  end;
end;

procedure TFmNFe_Pesq_0000.BtExcluiClick(Sender: TObject);
var
  Status, FatID, FatNum, Empresa, ide_nNF: Integer;
begin
  Status  := Trunc(QrNFeCabAcStat.Value);
  FatID   := QrNFeCabAFatID.Value;
  FatNum  := QrNFeCabAFatNum.Value;
  Empresa := QrNFeCabAEmpresa.Value;
  //
  if Geral.MensagemBox('Deseja realmente excluir a NFe:' + #13#10 +
  'S�rie: ' + FormatFloat('000', QrNFeCabAide_serie.Value) + #13#10 +
  'N�: ' + FormatFloat('000', QrNFeCabAide_nNF.Value) + #13#10 + #13#10 +
  'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if DmNFe_0000.ExcluiNfe(Status, FatID, FatNum, Empresa, False) then
    begin
      QrNfeCabA.Next;
      ide_nNF := UMyMod.ProximoRegistro(QrNFeCabA, 'ide_nNF', QrNFeCabAide_nNF.Value);
      //
      BtReabreClick(Self);
      QrNfeCabA.Locate('ide_nNF', ide_nNF, []);
      Geral.MensagemBox('NFe exclu�da com sucesso!',
      'Informa��o', MB_OK+MB_ICONINFORMATION);
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.BtArqClick(Sender: TObject);
begin
  QrArq.Close;
  QrArq.Params[0].AsInteger := QrNFeCabAIDCtrl.Value;
  QrArq.Open;
  //
  MyObjects.MostraPopUpDeBotao(PMArq, BtArq);
end;

procedure TFmNFe_Pesq_0000.BtCancelaClick(Sender: TObject);
{
var
  ChaveNFe: String;
}
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if Application.MessageBox(PChar('Deseja realmente cancelar a NF-e ' +
  MLAGeral.FFD(QrNFeCabAide_nNF.Value, 9, siPositivo) + '?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    case DModG.VersaoNFe(Geral.IMV(VAR_LIB_EMPRESAS)) of
      200:
      if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
      begin
        //
        FmNFeSteps_0200.PnLoteEnv.Visible := True;
        FmNFeSteps_0200.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpNFeVerCanNFe.Value;
        FmNFeSteps_0200.Show;
        //
        FmNFeSteps_0200.RGAcao.ItemIndex := 3; // Pedido de cancelamento
        DefineVarsDePreparacao();
        FmNFeSteps_0200.PreparaCancelamentoDeNFe(FLote, FEmpresa, FChaveNFe, FProtocolo);
        //
        //
        //FmNFeSteps_0200.Destroy;
        //
      end;
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.BtLeArqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLeArq, BtLeArq);
end;

procedure TFmNFe_Pesq_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFe_Pesq_0000.CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
begin
  QrNFeXMLI.Close;
  QrNFeXMLI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeXMLI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeXMLI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeXMLI.Open;
  //
  MyObjects.frxMostra(frxCampos, 'Campos preenchidos no XML');
end;

procedure TFmNFe_Pesq_0000.Cancelamento1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  case DModG.VersaoNFe(Geral.IMV(VAR_LIB_EMPRESAS)) of
    200:
    if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
    begin
      FmNFeSteps_0200.PnLoteEnv.Visible := True;
      FmNFeSteps_0200.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpNFeVerCanNFe.Value;
      FmNFeSteps_0200.Show;
      //
      FmNFeSteps_0200.RGAcao.ItemIndex := 3; // Pedido de cancelamento
      FmNFeSteps_0200.CkSoLer.Checked  := True;
      DefineVarsDePreparacao();
      FmNFeSteps_0200.PreparaCancelamentoDeNFe(FLote, FEmpresa, FChaveNFe, FProtocolo);
      //
      //
      //FmNFeSteps_0200.Destroy;
      //
      //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.Cancelamento2Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Can.Value, 'XML Cancelamento', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.CGcSitConfClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.CkEmitClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.CorrigeManifestao1Click(Sender: TObject);
var
  IDCtrl, cSitConf: Integer;
begin
  case QrNFeEveRRetret_tpEvento.Value of
    NFe_CodEventoMDeConfirmacao, // = 210200;
    NFe_CodEventoMDeCiencia    , // = 210210;
    NFe_CodEventoMDeDesconhece , // = 210220;
    NFe_CodEventoMDeNaoRealizou: // = 210240;
    begin
      if (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfNaoConsultada)
      or (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfMDeSemManifestacao) then
      begin
        if QrNFeEveRRetret_cStat.Value = 135 then
        begin
          IDCtrl   := QrNFeCabAIDCtrl.Value;
          cSitConf := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(
            QrNFeEveRRetret_tpEvento.Value);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
          (*'cSitNFe',*) 'cSitConf'
          ], [
          'IDCtrl'
          ], [
          (*cSitNFe,*) cSitConf
          ], [
          IDCtrl], True) then
            ReopenNFeCabA(IDCtrl, FSo_O_ID);
        end
        else Geral.MB_Aviso('O evento selecionado n�o foi vinculado na sefaz!');
      end
      else Geral.MB_Aviso('O evento selecionado n�o permite altera��o!');
    end
    else Geral.MB_Aviso('N�o h� implementa��o para o evento selecionado!');
  end;
end;

procedure TFmNFe_Pesq_0000.DefineVarsDePreparacao;
begin
  FLote      := QrNFeCabALoteEnv.Value;
  FEmpresa   := QrNFeCabAEmpresa.Value;
  FChaveNFe  := QrNFeCabAId.Value;
  FProtocolo := QrNFeCabAnProt.Value;
end;

function TFmNFe_Pesq_0000.DefineQual_frxNFe(OQueFaz: TfrxImpComo): TfrxReport;
begin
  if QrNfeCabANFeNT2013_003LTT.Value = 3 then
  begin
    MyObjects.frxDefineDataSets(frxA4A_003, [
    frxDsA,
    frxDsI,
    frxDsM,
    frxDsN,
    frxDsNfeYIts,
    frxDsO,
    frxDsV,
    frxDsY
    ]);
    DefineFrx(frxA4A_003, OQueFaz);
    Result := frxA4A_003;
  end else
  begin
    DefineFrx(frxA4A_002, OQueFaz);
    Result := frxA4A_002;
  end;
end;

procedure TFmNFe_Pesq_0000.DiretriodoarquivoXML1Click(Sender: TObject);
var
  Dir, Arq, Ext, Arquivo: String;
begin
  if not DmNFe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value) then Exit;
  //
  Arq := QrNFeCabAId.Value;
  Ext := NFE_EXT_NFE_XML;
  //
  if DmNFe_0000.ObtemDirXML(Ext, Dir, True) then
  begin
    Arquivo := Dir + Arq + Ext;
    Geral.MensagemBox(Arquivo, 'Caminho do arquivo XML', MB_OK+MB_ICONWARNING);
  end; 
end;

procedure TFmNFe_Pesq_0000.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, Trunc(QrNFeCabAcStat.Value));
  //
  DmNFe_0000.ColoreSitConfNFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, QrNFeCabAcSitConf.Value);
end;

procedure TFmNFe_Pesq_0000.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdClienteExit(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdFilialChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdFilialExit(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFe_Pesq_0000.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ImgTipo.SQLType := stPsq;
  PageControl1.ActivePageIndex := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  QrClientes.Open;
  TPDataI.Date := Date - 10; // m�x permitido pelo fisco
  TPDataF.Date := Date;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  CBFilial.ListSource := DModG.DsEmpresas;
  RGAmbiente.ItemIndex := DModG.QrParamsEmpide_tpAmb.Value;
  //
  ReopenNFeCabA(0, False);
  //
  QrNFeYIts.Database := DModG.MyPID_DB;
  //
  CGcSitConf.SetMaxValue();
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFe_Pesq_0000.frxA4A_000_GetValue(const VarName: string; var Value: Variant);
{
var
  I, J: Integer;
}
begin

{
  if Copy(VarName, 1, 9) = 'VARF_FAT_' then
  begin
    I := StrToInt(Copy(VarName, 10, 2));
    J := StrToInt(Copy(VarName, 13, 2));
    //
    Value := FFaturas[I,J];
  end;
}
end;

procedure TFmNFe_Pesq_0000.frxA4A_002GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_XVOL_ESP' then Value := Fesp else
  if VarName = 'VARF_XVOL_MARCA' then Value := Fmarca else
  if VarName = 'VARF_XVOL_NVOL' then Value := FnVol else
  if VarName = 'VARF_XVOL_PESOL' then Value := FpesoL else
  if VarName = 'VARF_XVOL_PESOB' then Value := FpesoB else
  if VarName = 'VARF_XVOL_QVOL' then Value := FqVol else
  if VarName = 'LogoNFeExiste' then Value := FileExists(DmodG.QrParamsEmpPathLogoNF.Value) else
  if VarName = 'LogoNFePath' then Value := DmodG.QrParamsEmpPathLogoNF.Value else
  if VarName = 'NFeItsLin' then
  begin
    case DmodG.QrParamsEmpNFeItsLin.Value of
      1: Value := 12.47244;    // 1 linha
      2: Value := 21.54331;    // 2 linhas
      else Value := 32.12598;  // 3 linhas
    end;
  end;
  if VarName = 'NFeFTRazao' then Value := DmodG.QrParamsEmpNFeFTRazao.Value else
  if VarName = 'NFeFTEnder' then Value := DmodG.QrParamsEmpNFeFTEnder.Value else
  if VarName = 'NFeFTFones' then Value := DmodG.QrParamsEmpNFeFTFones.Value else
end;

procedure TFmNFe_Pesq_0000.frxListaNFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, Null)
  else
  if VarName = 'VARF_PERIODO' then
    Value := MLAGeral.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmNFe_Pesq_0000.Listadasnotaspesquisadas1Click(Sender: TObject);
var
  ide_nNF, ide_serie, cStat, Ordem, Status: Integer;
  ide_dEmi: String;
  ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
  ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS: Double;
  NOME_tpEmis, NOME_tpNF, Motivo,
  ide_AAMM_AA, ide_AAMM_MM, Id, infCanc_dhRecbto, infCanc_nProt: String;
begin
{ TODO : Incluir Notas inutilizadas }
  Screen.Cursor := crHourGlass;
  try
    F_NFe_100 := GradeCriar.RecriaTempTableNovo(ntrttNFe_100, DmodG.QrUpdPID1, False);
    F_NFe_101 := GradeCriar.RecriaTempTableNovo(ntrttNFe_101, DmodG.QrUpdPID1, False);
    F_NFe_XXX := GradeCriar.RecriaTempTableNovo(ntrttNFe_XXX, DmodG.QrUpdPID1, False);
    //
    QrNFeCabA.First;
    while not QrNFeCabA.Eof do
    begin
      // 0..999
      cStat            := Trunc(QrNFeCabAcStat.Value);
      ide_nNF          := QrNFeCabAide_nNF.Value;
      ide_serie        := QrNFeCabAide_serie.Value;
      ide_dEmi         := Geral.FDT(QrNFeCabAide_dEmi.Value, 1);
      // 100
      ICMSTot_vProd    := QrNFeCabAICMSTot_vProd.Value;
      ICMSTot_vST      := QrNFeCabAICMSTot_vST.Value;
      ICMSTot_vFrete   := QrNFeCabAICMSTot_vFrete.Value;
      ICMSTot_vSeg     := QrNFeCabAICMSTot_vSeg.Value;
      ICMSTot_vIPI     := QrNFeCabAICMSTot_vIPI.Value;
      ICMSTot_vOutro   := QrNFeCabAICMSTot_vOutro.Value;
      ICMSTot_vDesc    := QrNFeCabAICMSTot_vDesc.Value;
      ICMSTot_vNF      := QrNFeCabAICMSTot_vNF.Value;
      ICMSTot_vBC      := QrNFeCabAICMSTot_vBC.Value;
      ICMSTot_vICMS    := QrNFeCabAICMSTot_vICMS.Value;
      ICMSTot_vPIS     := QrNFeCabAICMSTot_vPIS.Value;
      ICMSTot_vCOFINS  := QrNFeCabAICMSTot_vCOFINS.Value;
      NOME_tpEmis      := QrNFeCabANOME_tpEmis.Value;
      NOME_tpNF        := QrNFeCabANOME_tpNF.Value;
      // 101
      ide_AAMM_AA      := Geral.FDT(QrNFeCabAide_dEmi.Value, 21);
      ide_AAMM_MM      := Geral.FDT(QrNFeCabAide_dEmi.Value, 22);
      Motivo           := QrNFeCabAinfCanc_xJust.Value;
      Id               := QrNFeCabAId.Value;
      infCanc_dhRecbto := Geral.FDT(QrNFeCabAinfCanc_dhRecbto.Value, 9);
      infCanc_nProt    := QrNFeCabAinfCanc_nProt.Value;
      // Outros
      Status         := QrNFeCabAStatus.Value;
      //
      case cStat of
        100:
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_100, True, [
          'Ordem',
          'ide_nNF', 'ide_serie', 'ide_dEmi',
          'ICMSTot_vProd', 'ICMSTot_vST', 'ICMSTot_vFrete',
          'ICMSTot_vSeg', 'ICMSTot_vIPI', 'ICMSTot_vOutro',
          'ICMSTot_vDesc', 'ICMSTot_vNF', 'ICMSTot_vBC',
          'ICMSTot_vICMS', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
          'NOME_tpEmis', 'NOME_tpNF'], [
          ], [
          0,
          ide_nNF, ide_serie, ide_dEmi,
          ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
          ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
          ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
          ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS,
          NOME_tpEmis, NOME_tpNF], [
          ], False);
        end;
        101:
        begin
          Ordem := 0;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_101, False, [
          'Ordem', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Id', 'infCanc_dhRecbto', 'infCanc_nProt', 'Motivo'], [
          ], [
          Ordem, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Id, infCanc_dhRecbto, infCanc_nProt, Motivo], [
          ], False);
        end;
        else begin
          if Status < 100 then
            Ordem := 2
          else
            Ordem := 1;
          Motivo := NFeXMLGeren.Texto_StatusNFe(Status, QrNFeCabAversao.Value);
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_XXX, False, [
          'Ordem', 'cStat', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Status', 'Motivo'], [
          ], [
          Ordem, cStat, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Status, Motivo], [
          ], False);
        end;
      end;
      //
      QrNFeCabA.Next;
    end;
    //
    QrNFe_100.Close;
    QrNFe_100.Database := DModG.MyPID_DB;
    QrNFe_100.Open;
    //
    QrNFe_101.Close;
    QrNFe_101.Database := DModG.MyPID_DB;
    QrNFe_101.Open;
    //
    QrNFe_XXX.Database := DModG.MyPID_DB;
    MyObjects.frxMostra(frxListaNFes, 'Lista de NF-e(s)');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFe_Pesq_0000.Manifestar1Click(Sender: TObject);
begin
  RGQuemEmit.ItemIndex := 1;
  CGcSitConf.Value := 3;
  BtReabreClick(Self);
end;

procedure TFmNFe_Pesq_0000.NFe1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_NFe.Value, 'XML NFe', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.PMArqPopup(Sender: TObject);
begin
  NFe1         .Enabled := QrArqXML_NFe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmNFe_Pesq_0000.PMEventosPopup(Sender: TObject);
begin
  CorrigeManifestao1.Enabled :=
    (QrNFeCabA.State <> DsInactive) and (QrNFeCabA.RecordCount > 0) and (
      (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfNaoConsultada) or
      (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfMDeSemManifestacao)
    );
end;

procedure TFmNFe_Pesq_0000.PreviewdaNFe1Click(Sender: TObject);
var
  I: Integer;
(*  MD_NIVEL1: TfrxMasterData;
*)
  //frxReportPage: TfrxReportPage;
  //Nomes, Tipos: array of String;
  Report: TfrxReport;
  Memo: TfrxMemoView;
begin
(*
  MD_NIVEL1 := frxErros.FindObject('MD_NIVEL1') as TfrxMasterData;
  MEMO1 := frxErros.FindObject('MEMO1') as TfrxMemoView;
*)

  //Report := frxA4A_002;
  Report := DefineQual_frxNFe(ficNone);
  try
    (*
    SetLength(Nomes, Report.ComponentCount);
    SetLength(Tipos, Report.ComponentCount);
    *)
    for I := 0 to Report.ComponentCount - 1 do
    begin
      (*
      Nomes[I] := TComponent(Report.Components[I]).Name;
      Tipos[I] := TComponent(Report.Components[I]).ClassName;
      *)
      //
      if Report.Components[I] is TfrxMemoView then
      begin
        Memo := TfrxMemoView(Report.Components[I]);
        //Memo.Frame.Typ := [];
        case Memo.Tag of
          0: Memo.Visible := False;
          1: ; // Deixa como est�
          2: Memo.Visible := True;
        end;
      end
      else if Report.Components[I] is TfrxLineView then
        TfrxLineView(Report.Components[I]).Visible := False
      else if Report.Components[I] is TfrxBarCodeView then
        TfrxBarCodeView(Report.Components[I]).Visible := False
    end;
    //Geral.MensagemArray(MB_OK+MB_ICONINFORMATION, 'Componentes', Nomes, Tipos, nil);
    DefineFrx(Report, ficMostra);
  finally
    //Report.Free;
  end;
end;

procedure TFmNFe_Pesq_0000.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := DmNFe_0000.FormataID_NFe(QrAId.Value);
{
  QrAId_TXT.Value := Copy(QrAId.Value, 01, 04) + ' ' +
                     Copy(QrAId.Value, 05, 04) + ' ' +
                     Copy(QrAId.Value, 09, 04) + ' ' +
                     Copy(QrAId.Value, 13, 04) + ' ' +
                     Copy(QrAId.Value, 17, 04) + ' ' +
                     Copy(QrAId.Value, 21, 04) + ' ' +
                     Copy(QrAId.Value, 25, 04) + ' ' +
                     Copy(QrAId.Value, 29, 04) + ' ' +
                     Copy(QrAId.Value, 33, 04) + ' ' +
                     Copy(QrAId.Value, 37, 04) + ' ' +
                     Copy(QrAId.Value, 41, 04);
}
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + #13#10 +
    QrAemit_xBairro.Value + #13#10 + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + #13#10 +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value)) + '  ' + QrAemit_xPais.Value;

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  QrAEMIT_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value);
  //
    //
    ////// DESTINAT�RIO
    //
  //
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrParamsEmpNFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + #13#10 +
      DModG.QrParamsEmpNFeShowURL.Value;
  if DModG.QrParamsEmpNFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
    //
    ////// TRANSPORTADORA - REBOQUE???
    //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  {
  QrATRANSPORTA_ENDERECO.Value := QrAtransporta_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAtransporta_nro.Value, True) + ' ' + QrAtransporta_xCpl.Value;
  //
  QrATRANSPORTA_CEP_TXT.Value := Geral.FormataCEP_TT(QrAtransporta_CEP.Value);
  QrATRANSPORTA_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAtransporta_fone.Value);
  //
  }
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'NF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
{ p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
}
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
end;

procedure TFmNFe_Pesq_0000.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabAMsg.Close;
  BtLeArq.Enabled     := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Enabled   := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
end;

procedure TFmNFe_Pesq_0000.QrIAfterScroll(DataSet: TDataSet);
begin
  QrM.Close;
  QrM.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrM.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrM.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrM.Params[03].AsInteger := QrInItem.Value;
  QrM.Open;
  //
  QrN.Close;
  QrN.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrN.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrN.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrN.Params[03].AsInteger := QrInItem.Value;
  QrN.Open;
  //
  QrO.Close;
  QrO.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrO.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrO.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrO.Params[03].AsInteger := QrInItem.Value;
  QrO.Open;
  //
  QrV.Close;
  QrV.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrV.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrV.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrV.Params[03].AsInteger := QrInItem.Value;
  QrV.Open;
  //
end;

procedure TFmNFe_Pesq_0000.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible    := True;
  BtConsulta.Enabled := QrNFeCabA.RecordCount > 0;
  SbImprime.Enabled  := QrNFeCabA.RecordCount > 0;
  SbNovo.Enabled     := QrNFeCabA.RecordCount > 0;
end;

procedure TFmNFe_Pesq_0000.QrNFeCabAAfterScroll(DataSet: TDataSet);
begin
  BtLeArq.Enabled     := True;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Enabled   := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
  case Trunc(QrNFeCabAcStat.Value) of
    0..99, 201..203, 205..999:  //No status 204 Rejei��o: Duplicidade de NF-e n�o deixar excluir
    begin
      BtExclui.Enabled := True;
    end;
    100:
    begin
      BtImprime.Enabled := True;
      BtEvento.Enabled  := True;
      BtCancela.Enabled := True;
      BtEnvia.Enabled   := True;
    end;
    101:
    begin
      BtEnvia.Enabled   := True;
      BtEvento.Enabled  := True;
    end;
    //100: BtImprime.Enabled := True;
  end;
  ReopenNFeEveRRet();
  ReopenNFeCabAMsg();
end;

procedure TFmNFe_Pesq_0000.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible    := False;
  BtConsulta.Enabled := False;
  SbImprime.Enabled  := False;
  SbNovo.Enabled     := False;
  QrNFeCabAMsg.Close;
  QrNFeEveRRet.Close;
end;

procedure TFmNFe_Pesq_0000.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  case QrNfeCabAide_tpEmis.Value of
    1: QrNfeCabANome_tpEmis.Value := 'Normal';
    2: QrNfeCabANome_tpEmis.Value := 'Conting�ncia FS';
    3: QrNfeCabANome_tpEmis.Value := 'Conting�ncia SCAN';
    4: QrNfeCabANome_tpEmis.Value := 'Conting�ncia DEPEC';
    5: QrNfeCabANome_tpEmis.Value := 'Conting�ncia FS-DA';
    else QrNfeCabANome_tpEmis.Value := '? ? ? ?';
  end;
  case QrNfeCabAide_tpNF.Value of
    0: QrNfeCabANome_tpNF.Value := 'E';
    1: QrNfeCabANome_tpNF.Value := 'S';
    else QrNfeCabANome_tpNF.Value := '?';
  end;
end;

procedure TFmNFe_Pesq_0000.QrNFeEveRRetCalcFields(DataSet: TDataSet);
begin
  if QrNFeEveRRetret_xEvento.Value <> '' then
    QrNFeEveRRetNO_EVENTO.Value := QrNFeEveRRetret_xEvento.Value
  else
    QrNFeEveRRetNO_EVENTO.Value :=
      NFeXMLGeren.Obtem_DeEvento_Nome(QrNFeEveRRetret_tpEvento.Value);
end;

procedure TFmNFe_Pesq_0000.QrNFeYItsCalcFields(DataSet: TDataSet);
begin
  QrNFeYItsxVenc1.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc1.Value, 2);
  QrNFeYItsxVenc2.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc2.Value, 2);
  QrNFeYItsxVenc3.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc3.Value, 2);
  QrNFeYItsxVenc4.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc4.Value, 2);
  QrNFeYItsxVenc5.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc5.Value, 2);
end;

procedure TFmNFe_Pesq_0000.QrNFe_101CalcFields(DataSet: TDataSet);
begin
  QrNFe_101Id_TXT.Value := DmNFe_0000.FormataID_NFe(QrNFe_101Id.Value);
end;

procedure TFmNFe_Pesq_0000.QrNFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrNFe_XXXOrdem.Value of
    0: QrNFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrNFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrNFe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrNFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmNFe_Pesq_0000.QrXVolAfterOpen(DataSet: TDataSet);
begin
  Fesp    := '';
  Fmarca  := '';
  FnVol   := '';
  //
  FpesoL  := 0;
  FpesoB  := 0;
  FqVol   := 0;
  //
  while not QrXVol.Eof do
  begin
    if (QrXVolesp.Value <> '') and (Fesp <> '') then Fesp := Fesp + '/';
    Fesp    := Fesp    + QrXVolesp.Value;
    if (QrXVolmarca.Value <> '') and (Fmarca <> '') then Fmarca := Fmarca + '/';
    Fmarca  := Fmarca  + QrXVolmarca.Value;
    if (QrXVolnVol.Value <> '') and (FnVol <> '') then FnVol := FnVol + '/';
    FnVol   := FnVol   + QrXVolnVol.Value;
    //         //
    FpesoL  := FpesoL  + QrXVolpesoL.Value;
    FpesoB  := FpesoB  + QrXVolpesoB.Value;
    FqVol   := FqVol   + QrXVolqVol.Value;
    //
    QrXVol.Next;
  end;
end;

procedure TFmNFe_Pesq_0000.QrYAfterOpen(DataSet: TDataSet);
  function ProximoSeq(var nSeq: Integer): String;
  begin
    nSeq := nSeq + 1;
    Result := FormatFloat('0', nSeq);
  end;
const
  Fields = 03; // (nDup, dVencv, Dup) + 1 Seq  = 4 no total
  //MaxRow = 02;
  MaxCol = 14; // 5 grupos de 3 campos > 0..14 = 15 itens
  //
var
  Seq, MaxRow, Linha, Col, Row, ColsInRow: Integer;
  Faturas: array of array of String;
  Seq1, nDup1, dVenc1, vDup1,
  Seq2, nDup2, dVenc2, vDup2,
  Seq3, nDup3, dVenc3, vDup3,
  Seq4, nDup4, dVenc4, vDup4,
  Seq5, nDup5, dVenc5, vDup5: String;
begin
  ColsInRow := (MaxCol + 1) div Fields;
  // Aumentar para arredondar para cima
  MaxRow := QrY.RecordCount + ColsInRow - 1;
  // Total de linhas
  MaxRow := (MaxRow div ColsInRow) - 1; // > 0..?

  //
  //  Limpar vari�veis
  SetLength(Faturas, MaxCol + 1, MaxRow + 1);
  for Col := 0 to MaxCol do
    for Row := 0 to MaxRow do
      FFaturas[Col,Row] := '';

//begin
  //

  Col := 0;
  Row := 0;
  QrY.First;
  while not QrY.Eof do
  begin
    Faturas[Col, Row] :=  QrYnDup.Value;
    Col := Col + 1;
    Faturas[Col, Row] :=  Geral.FDT(QrYdVenc.Value, 1);
    Col := Col + 1;
    Faturas[Col, Row] :=  dmkPF.FFP(QrYvDup.Value, 2);
    //
    if Col = MaxCol then
    begin
      Col := 0;
      Row := Row + 1;
    end else
      Col := Col + 1;
    //
    QrY.Next;
  end;

  //

  FNFeYIts := UCriar.RecriaTempTable('NFeYIts', DmodG.QrUpdPID1, False);

  Seq := 0;
  if MaxRow > -1 then
  begin
    for Row := 0 to MaxRow do
    begin
      Linha  := Row + 1;
      Seq1   := ProximoSeq(Seq);
      nDup1  := Faturas[00, Row];
      dVenc1 := Faturas[01, Row];
      vDup1  := Faturas[02, Row];

      Seq2   := ProximoSeq(Seq);
      nDup2  := Faturas[03, Row];
      dVenc2 := Faturas[04, Row];
      vDup2  := Faturas[05, Row];

      Seq3   := ProximoSeq(Seq);
      nDup3  := Faturas[06, Row];
      dVenc3 := Faturas[07, Row];
      vDup3  := Faturas[08, Row];

      Seq4   := ProximoSeq(Seq);
      nDup4  := Faturas[09, Row];
      dVenc4 := Faturas[10, Row];
      vDup4  := Faturas[11, Row];

      Seq5   := ProximoSeq(Seq);
      nDup5  := Faturas[12, Row];
      dVenc5 := Faturas[13, Row];
      vDup5  := Faturas[14, Row];
      //
      //for Col := 0 to MaxCol do
      //if
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
        'Seq1', 'nDup1', 'dVenc1', 'vDup1',
        'Seq2', 'nDup2', 'dVenc2', 'vDup2',
        'Seq3', 'nDup3', 'dVenc3', 'vDup3',
        'Seq4', 'nDup4', 'dVenc4', 'vDup4',
        'Seq5', 'nDup5', 'dVenc5', 'vDup5',
        'Linha'], [
      ], [
        Seq1, nDup1, dVenc1, vDup1,
        Seq2, nDup2, dVenc2, vDup2,
        Seq3, nDup3, dVenc3, vDup3,
        Seq4, nDup4, dVenc4, vDup4,
        Seq5, nDup5, dVenc5, vDup5,
        Linha], [
      ], False);
    end;
  end else begin
    Seq1    := '';
    nDup1   := '';
    dVenc1  := '';
    vDup1   := '';
    Seq2    := '';
    nDup2   := '';
    dVenc2  := '';
    vDup2   := '';
    Seq3    := '';
    nDup3   := '';
    dVenc3  := '';
    vDup3   := '';
    Seq4    := '';
    nDup4   := '';
    dVenc4  := '';
    vDup4   := '';
    Seq5    := '';
    nDup5   := '';
    dVenc5  := '';
    vDup5   := '';
    Linha   := 1;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
      'Seq1', 'nDup1', 'dVenc1', 'vDup1',
      'Seq2', 'nDup2', 'dVenc2', 'vDup2',
      'Seq3', 'nDup3', 'dVenc3', 'vDup3',
      'Seq4', 'nDup4', 'dVenc4', 'vDup4',
      'Seq5', 'nDup5', 'dVenc5', 'vDup5',
      'Linha'], [
    ], [
      Seq1, nDup1, dVenc1, vDup1,
      Seq2, nDup2, dVenc2, vDup2,
      Seq3, nDup3, dVenc3, vDup3,
      Seq4, nDup4, dVenc4, vDup4,
      Seq5, nDup5, dVenc5, vDup5,
      Linha], [
    ], False);
  end;
  QrNFeYIts.Close;
  QrNFeYIts.Open;
end;

procedure TFmNFe_Pesq_0000.FechaNFeCabA();
begin
  QrNFeCabA.Close;
end;

procedure TFmNFe_Pesq_0000.RGOrdem1Click(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.RGOrdem2Click(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
var
  Empresa, DOC1, Ord2, SitConfs: String;
  //Loc,Atual: Integer;
begin
  Screen.Cursor := crHourGlass;
  FSo_o_ID := So_O_ID;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  //

  (*
  Loc := IDCtrl;
  if Loc = 0 then
  begin
    if QrNFeCabA.State <> dsInactive then
      Loc := QrNFeCabAIDCtrl.Value;
  end;
  //
  *)
  QrNFeCabA.Close;
  if Empresa <> '' then
  begin
    QrNFeCabA.SQL.Clear;
    QrNFeCabA.SQL.Add('SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,');
    QrNFeCabA.SQL.Add('nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,');
    QrNFeCabA.SQL.Add('ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,');
    QrNFeCabA.SQL.Add('ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF, ');
    QrNFeCabA.SQL.Add('dest_xNome, ');
    QrNFeCabA.SQL.Add('IF(Importado>0, Status, ');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) + 0.000 cStat,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, ' +
    // 2012-08-31
    'DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ' +
    'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,');
    // Fim 2012-08-31
    QrNFeCabA.SQL.Add('IDCtrl, versao, ');
    // 2011-01-11
    QrNFeCabA.SQL.Add('ide_tpEmis, infCanc_xJust, Status, ');
    QrNFeCabA.SQL.Add('ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg, ');
    QrNFeCabA.SQL.Add('ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc, ');
    QrNFeCabA.SQL.Add('ICMSTot_vBC, ICMSTot_vICMS, ');
    // fim 2011-02-11
    // 2012-05-09
    QrNFeCabA.SQL.Add('ICMSTot_vPIS, ICMSTot_vCOFINS, ');
    // fim 2012-05-09
    // 2013-05-05
    QrNFeCabA.SQL.Add('cSitNFe, cSitConf,');
    QrNFeCabA.SQL.Add('ELT(cSitConf+2, "N�o consultada", "Sem manifesta��o", "Confirmada",');
    QrNFeCabA.SQL.Add('"Desconhecida", "N�o realizada", "Ci�ncia", "? ? ?") NO_cSitConf,');
    // fim 2013-05-05
    // 2011-08-20
    QrNFeCabA.SQL.Add('nfa.infCanc_dhRecbto, nfa.infCanc_nProt,');
    // fim 2011-08-20
    // 2013-05-10
    QrNFeCabA.SQL.Add('nfa.NFeNT2013_003LTT');
    // fim 2013-05-10
    QrNFeCabA.SQL.Add('FROM nfecaba nfa');
    QrNFeCabA.SQL.Add('LEFT JOIN entidades cli ON');
    //2010-04-08
    {
    QrNFeCabA.SQL.Add('  IF(nfa.dest_CNPJ<>"",nfa.dest_CNPJ=cli.CNPJ,');
    QrNFeCabA.SQL.Add('  nfa.dest_CPF=cli.CPF)');
    }
    // fim 2010-04-08
    QrNFeCabA.SQL.Add('cli.Codigo=nfa.CodInfoDest');
    QrNFeCabA.SQL.Add('');


    // 2011-08-24
    if FSo_O_ID then
    begin
      QrNFeCabA.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', IDCtrl));
      QrNFeCabA.Open;
    // Fim 2011-08-24
    end else begin
      QrNFeCabA.SQL.Add(dmkPF.SQL_Periodo('WHERE ide_dEmi ', TPDataI.Date, TPDataF.Date, True, True));
      QrNFeCabA.SQL.Add('AND nfa.Empresa = ' + Empresa);
      case RGQuemEmit.ItemIndex of
        0: QrNFeCabA.SQL.Add('AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"');
        1: QrNFeCabA.SQL.Add('AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)');
      end;
      if Ck100e101.Checked  then
        QrNFeCabA.SQL.Add('AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)');
      case RGAmbiente.ItemIndex of
        0: ; // Tudo
        1: QrNFeCabA.SQL.Add('AND nfa.ide_tpAmb=1');
        2: QrNFeCabA.SQL.Add('AND nfa.ide_tpAmb=2');
      end;
      if EdCliente.ValueVariant <> 0 then
      begin
        if QrClientesTipo.Value = 0 then
        begin
          Doc1 := Geral.TFD(QrClientesCNPJ.Value, 14, siPositivo);
          QrNFeCabA.SQL.Add('AND dest_CNPJ="' + Doc1 + '"');
        end else begin
          Doc1 := Geral.TFD(QrClientesCPF.Value, 11, siPositivo);
          QrNFeCabA.SQL.Add('AND dest_CPF="' + Doc1 + '"');
        end;
      end;
      SitConfs := CGcSitConf.GetIndexesChecked(True, -1);
      QrNFeCabA.SQL.Add('AND nfa.cSitConf IN (' + SitConfs + ')');
      //
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: QrNFeCabA.SQL.Add('ORDER BY nfa.ide_dEmi ' + Ord2 + ', nfa.ide_nNF ' + Ord2);
        1: QrNFeCabA.SQL.Add('ORDER BY nfa.ide_nNF ' + Ord2 + ', nfa.ide_dEmi ' + Ord2 );
        2: QrNFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_nNF ' + Ord2);
        3: QrNFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_dEmi ' + Ord2);
      end;
      QrNFeCabA.Open;
      QrNFeCabA.Locate('IDCtrl', IDCtrl, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.ReopenNFeCabAMsg;
begin
  QrNFeCabAMsg.Close;
  QrNFeCabAMsg.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeCabAMsg.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeCabAMsg.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeCabAMsg.Open;
  //
end;

procedure TFmNFe_Pesq_0000.ReopenNFeEveRRet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRRet, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeeverret',
  'WHERE ret_chNFe="' + QrNFeCabAId.Value + '" ',
  'ORDER BY ret_dhRegEvento DESC',
  '']);
end;

procedure TFmNFe_Pesq_0000.RGAmbienteClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFe_Pesq_0000.TPDataIChange(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.TPDataIClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

{
infAdProd na descri��o do produto
informa��es adicionais nas complementares
QrV - fazer Informa��es adicionais


No evento OnCalcFields da Query QrACalcFields: Modificar �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value;� para �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);�
No componentes frxA4A_002, frxReport1, frxA4A_000, frxA4A_001 nos labels que mostram o �Protocolo de autoriza��o de uso� mudar a fonte de 12px para 10px;
}

end.
