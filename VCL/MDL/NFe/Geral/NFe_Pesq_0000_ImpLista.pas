unit NFe_Pesq_0000_ImpLista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  frxDBSet, Data.DB, mySQLDbTables, DmkGeral, UnInternalConsts, UnDmkProcFunc,
  UnDmkEnums, dmkCheckGroup;

type
  TFmNFe_Pesq_0000_ImpLista = class(TForm)
    QrNFeCabA: TmySQLQuery;
    QrNFeCabANO_Cli: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAnProt: TWideStringField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAdhRecbto: TWideStringField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    QrNFeCabAdest_xNome: TWideStringField;
    QrNFeCabAversao: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabANOME_tpEmis: TWideStringField;
    QrNFeCabANOME_tpNF: TWideStringField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFeCabAcStat: TFloatField;
    QrNFeCabAcSitNFe: TSmallintField;
    QrNFeCabAcSitConf: TSmallintField;
    QrNFeCabANO_cSitConf: TWideStringField;
    QrNFeCabANFeNT2013_003LTT: TSmallintField;
    QrNFeCabANO_Emi: TWideStringField;
    QrNFeCabANO_Terceiro: TWideStringField;
    frxDsNFeCabA: TfrxDBDataset;
    frxListaNFes: TfrxReport;
    frxListaNFesB: TfrxReport;
    QrNFe_100: TmySQLQuery;
    QrNFe_100Ordem: TIntegerField;
    QrNFe_100ide_nNF: TIntegerField;
    QrNFe_100ide_serie: TIntegerField;
    QrNFe_100ide_dEmi: TDateField;
    QrNFe_100ICMSTot_vProd: TFloatField;
    QrNFe_100ICMSTot_vST: TFloatField;
    QrNFe_100ICMSTot_vFrete: TFloatField;
    QrNFe_100ICMSTot_vSeg: TFloatField;
    QrNFe_100ICMSTot_vIPI: TFloatField;
    QrNFe_100ICMSTot_vOutro: TFloatField;
    QrNFe_100ICMSTot_vDesc: TFloatField;
    QrNFe_100ICMSTot_vNF: TFloatField;
    QrNFe_100ICMSTot_vBC: TFloatField;
    QrNFe_100ICMSTot_vICMS: TFloatField;
    QrNFe_100ICMSTot_vPIS: TFloatField;
    QrNFe_100ICMSTot_vCOFINS: TFloatField;
    QrNFe_100NOME_tpEmis: TWideStringField;
    QrNFe_100NOME_tpNF: TWideStringField;
    QrNFe_100ide_NatOP: TWideStringField;
    QrNFe_100PesoB: TFloatField;
    QrNFe_100PesoL: TFloatField;
    QrNFe_100nVol: TWideStringField;
    QrNFe_100marca: TWideStringField;
    QrNFe_100esp: TWideStringField;
    QrNFe_100qVol: TFloatField;
    frxDsNFe_100: TfrxDBDataset;
    QrNFe_101: TmySQLQuery;
    QrNFe_101Ordem: TIntegerField;
    QrNFe_101ide_nNF: TIntegerField;
    QrNFe_101ide_serie: TIntegerField;
    QrNFe_101ide_AAMM_AA: TWideStringField;
    QrNFe_101ide_AAMM_MM: TWideStringField;
    QrNFe_101ide_dEmi: TDateField;
    QrNFe_101NOME_tpEmis: TWideStringField;
    QrNFe_101NOME_tpNF: TWideStringField;
    QrNFe_101Id: TWideStringField;
    QrNFe_101infCanc_dhRecbto: TDateTimeField;
    QrNFe_101infCanc_nProt: TWideStringField;
    QrNFe_101Motivo: TWideStringField;
    QrNFe_101Id_TXT: TWideStringField;
    frxDsNFe_101: TfrxDBDataset;
    QrNFe_XXX: TmySQLQuery;
    QrNFe_XXXOrdem: TIntegerField;
    QrNFe_XXXcStat: TIntegerField;
    QrNFe_XXXide_nNF: TIntegerField;
    QrNFe_XXXide_serie: TIntegerField;
    QrNFe_XXXide_AAMM_AA: TWideStringField;
    QrNFe_XXXide_AAMM_MM: TWideStringField;
    QrNFe_XXXide_dEmi: TDateField;
    QrNFe_XXXNOME_tpEmis: TWideStringField;
    QrNFe_XXXNOME_tpNF: TWideStringField;
    QrNFe_XXXStatus: TIntegerField;
    QrNFe_XXXMotivo: TWideStringField;
    QrNFe_XXXNOME_ORDEM: TWideStringField;
    frxDsNFe_XXX: TfrxDBDataset;
    frxNFe_PESQU_002_C: TfrxReport;
    QrNFe_CFOP: TmySQLQuery;
    frxDsNFe_CFOP: TfrxDBDataset;
    QrNFe_CFOPnItem: TIntegerField;
    QrNFe_CFOPprod_cProd: TWideStringField;
    QrNFe_CFOPprod_xProd: TWideStringField;
    QrNFe_CFOPprod_CFOP: TIntegerField;
    QrNFe_CFOPprod_uCom: TWideStringField;
    QrNFe_CFOPprod_vUnCom: TFloatField;
    QrNFe_CFOPprod_vProd: TFloatField;
    QrNFe_CFOPOrdem: TIntegerField;
    QrNFe_CFOPide_nNF: TIntegerField;
    QrNFe_CFOPide_serie: TIntegerField;
    QrNFe_CFOPide_dEmi: TDateField;
    QrNFe_CFOPICMSTot_vProd: TFloatField;
    QrNFe_CFOPICMSTot_vNF: TFloatField;
    QrNFe_CFOPNOME_tpEmis: TWideStringField;
    QrNFe_CFOPNOME_tpNF: TWideStringField;
    QrNFe_CFOPide_natOp: TWideStringField;
    QrNFe_CFOPqVol: TFloatField;
    QrNFe_CFOPesp: TWideStringField;
    QrNFe_CFOPmarca: TWideStringField;
    QrNFe_CFOPnVol: TWideStringField;
    QrNFe_CFOPPesoL: TFloatField;
    QrNFe_CFOPPesoB: TFloatField;
    QrNFe_CFOPprod_qCom: TFloatField;
    frxNFe_PESQU_002_D: TfrxReport;
    QrNFe_CFOPNO_CFOP: TWideStringField;
    QrNFe_CFOPprod_NCM: TWideStringField;
    ___frxNFe_PESQU_002_E___: TfrxReport;
    frxNFe_PESQU_002_F: TfrxReport;
    QrNFe_CFOPPrdGrupTip: TIntegerField;
    QrNFe_CFOPNO_PGT: TWideStringField;
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure frxListaNFesGetValue(const VarName: string; var Value: Variant);
    procedure QrNFe_101CalcFields(DataSet: TDataSet);
    procedure QrNFe_XXXCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    F_NFe_100, F_NFe_CFOP: String;
    //
    procedure ReopenNFe_100();
    (* Substituído pela função ReopenNFe_Its
    procedure ReopenNFe_CFOP();
    *)
    procedure ReopenNFe_Its(Grupo: TNFEAgruRelNFs);
  public
    { Public declarations }
    EdFilial_ValueVariant: Variant;
    TPDataI_Date, TPDataF_Date: TDateTime;
    RGQuemEmit_ItemIndex, RGAmbiente_ItemIndex, EdCliente_ValueVariant,
    QrClientesTipo_Value, RGOrdem1_ItemIndex, RGOrdem2_ItemIndex: Integer;
    Ck100e101_Checked: Boolean;
    QrClientesCNPJ_Value, QrClientesCPF_Value: String;
    CGcSitConf: TdmkCheckGroup;
    CkideNatOp_Checked: Boolean;
    CBFilial_Text, CBCliente_Text: String;
    FAgrupa1: Boolean;
    //
    procedure ImprimeListaNatOp();
    (* Substituído pela função ImprimeLista_Its
    procedure ImprimeListaCFOP();
    *)
    procedure ImprimeLista_Its(Grupo: TNFEAgruRelNFs);
    function  SQL_Filtro(): String;
  end;

var
  FmNFe_Pesq_0000_ImpLista: TFmNFe_Pesq_0000_ImpLista;

implementation

uses Module, ModuleGeral, DmkDAC_PF, ModuleNFe_0000, UnMyObjects, UnGrade_Create;

{$R *.dfm}

{ TFmNFe_Pesq_0000_ImpLista }

procedure TFmNFe_Pesq_0000_ImpLista.FormCreate(Sender: TObject);
begin
  FAgrupa1 := False;
end;

procedure TFmNFe_Pesq_0000_ImpLista.frxListaNFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial_Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente_Text, EdCliente_ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI_Date, TPDataF_Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101_Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit_ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente_ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODUÇÃO';
      2: Value := 'HOMOLOGAÇÃO';
    end;
  end
  else
  if VarName = 'VARF_AGRUPA1' then
    Value := FAgrupa1
  else
end;

procedure TFmNFe_Pesq_0000_ImpLista.ImprimeLista_Its(Grupo: TNFEAgruRelNFs);
var
  Report: TfrxReport;
  GH: TfrxGroupHeader;
  Me_112: TfrxMemoView;
begin
  F_NFe_CFOP := GradeCriar.RecriaTempTableNovo(ntrttNFe_CFOP, DmodG.QrUpdPID1, False);
  ReopenNFe_Its(Grupo);
  //
  GH     := frxNFe_PESQU_002_D.FindObject('GroupHeader4') as TfrxGroupHeader;
  Me_112 := frxNFe_PESQU_002_D.FindObject('Memo112') as TfrxMemoView;
  //
  case Grupo of
    nfearnCOFP:
    begin
      GH.Condition     := 'frxDsNFe_CFOP."prod_CFOP"';
      Me_112.Memo.Text := 'CFOP: [frxDsNFe_CFOP."prod_CFOP"]   -   [frxDsNFe_CFOP."NO_CFOP"]';
      Report           := frxNFe_PESQU_002_D;
    end;
    nfearnNCM:
    begin
      GH.Condition     := 'frxDsNFe_CFOP."prod_NCM"';
      Me_112.Memo.Text := 'NCM: [frxDsNFe_CFOP."prod_NCM"]';
      Report           := frxNFe_PESQU_002_D;
    end;
    nfearnProd:
    begin
      FAgrupa1         := False;
      Report           := frxNFe_PESQU_002_F;
    end;
    nfearnTipPrd:
    begin
      FAgrupa1         := True;
      Report           := frxNFe_PESQU_002_F;
    end;
  end;
  MyObjects.frxDefineDataSets(Report, [
    DModG.frxDsDono,
    frxDsNFe_CFOP
  ]);
  MyObjects.frxMostra(Report, 'Lista de itens de NF-e(s)');
end;

(*
procedure TFmNFe_Pesq_0000_ImpLista.ImprimeListaCFOP;
begin
  F_NFe_CFOP := GradeCriar.RecriaTempTableNovo(ntrttNFe_CFOP, DmodG.QrUpdPID1, False);
  ReopenNFe_CFOP();
  MyObjects.frxDefineDataSets(frxNFe_PESQU_002_D, [
    DModG.frxDsDono,
    frxDsNFe_CFOP
  ]);
  MyObjects.frxMostra(frxNFe_PESQU_002_D, 'Lista de itens de NF-e(s)')
end;
*)

procedure TFmNFe_Pesq_0000_ImpLista.ImprimeListaNatOp();
begin
  //ReopenPesquisa();
  F_NFe_100 := GradeCriar.RecriaTempTableNovo(ntrttNFe_100, DmodG.QrUpdPID1, False);
  ReopenNFe_100();
  MyObjects.frxDefineDataSets(frxNFe_PESQU_002_C, [
    DModG.frxDsDono,
    frxDsNFe_100
  ]);
  MyObjects.frxMostra(frxNFe_PESQU_002_C, 'Lista de NF-e(s)')
end;

procedure TFmNFe_Pesq_0000_ImpLista.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  case QrNfeCabAide_tpEmis.Value of
    1: QrNfeCabANome_tpEmis.Value := 'Normal';
    2: QrNfeCabANome_tpEmis.Value := 'Contingência FS';
    3: QrNfeCabANome_tpEmis.Value := 'Contingência SCAN';
    4: QrNfeCabANome_tpEmis.Value := 'Contingência EPEC';
    5: QrNfeCabANome_tpEmis.Value := 'Contingência FS-DA';
    else QrNfeCabANome_tpEmis.Value := '? ? ? ?';
  end;
  case QrNfeCabAide_tpNF.Value of
    0: QrNfeCabANome_tpNF.Value := 'E';
    1: QrNfeCabANome_tpNF.Value := 'S';
    else QrNfeCabANome_tpNF.Value := '?';
  end;
end;

procedure TFmNFe_Pesq_0000_ImpLista.QrNFe_101CalcFields(DataSet: TDataSet);
begin
  QrNFe_101Id_TXT.Value := DmNFe_0000.FormataID_NFe(QrNFe_101Id.Value);
end;

procedure TFmNFe_Pesq_0000_ImpLista.QrNFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrNFe_XXXOrdem.Value of
    0: QrNFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrNFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrNFe_XXXNOME_ORDEM.Value := 'NÃO ENVIADA';
    else QrNFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmNFe_Pesq_0000_ImpLista.ReopenNFe_100;
var
  Ordem100: String;
begin
(*
  if CkideNatOp_Checked then
    Ordem100 := 'ORDER BY ide_NatOp, ide_nNF'
  else
    Ordem100 := 'ORDER BY ide_nNF';
*)
  //
  Ordem100 := 'ORDER BY ide_NatOp, ide_nNF';
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_100, DModG.MyPID_DB, [
  'DELETE FROM ' + F_NFe_100 + '; ',
  'INSERT INTO ' + F_NFe_100 + ' ',
  'SELECT 0 Ordem, ',
  'nfa.ide_nNF, nfa.ide_serie, nfa.ide_dEmi, ',
  'nfa.ICMSTot_vProd, nfa.ICMSTot_vST, nfa.ICMSTot_vFrete, ',
  'nfa.ICMSTot_vSeg, nfa.ICMSTot_vIPI, nfa.ICMSTot_vOutro, ',
  'nfa.ICMSTot_vDesc, nfa.ICMSTot_vNF, nfa.ICMSTot_vBC, ',
  'nfa.ICMSTot_vICMS, nfa.ICMSTot_vPIS, nfa.ICMSTot_vCOFINS, ',
  'ELT(nfa.ide_tpEmis, "Normal", "Contingência FS",  ',
  '"Contingência SCAN", "Contingência DEPEC",  ',
  '"Contingência FS-DA", "? ? ? ?") NOME_tpEmis,  ',
  'ELT(nfa.ide_tpNF + 1, "E", "S", "?") NOME_tpNF,  ',
  'nfa.ide_natOp, ',
  'SUM(nfxv.qVol) qVol, nfxv.esp, nfxv.marca, nfxv.nVol,  ',
  'SUM(nfxv.PesoL) PesoL, SUM(nfxv.PesoB) PesoB ',
  ' ',
  'FROM ' + TMeuDB + '.nfecaba nfa ',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON ',
  '  cli.Codigo=nfa.CodInfoDest ',
  'LEFT JOIN ' + TMeuDB + '.entidades emi ON emi.Codigo=nfa.CodInfoEmit ',
  'LEFT JOIN ' + TMeuDB + '.nfecabxvol nfxv ON  ',
  '  nfxv.FatID=nfa.FatID AND ',
  '  nfxv.FatNum=nfa.FatNum AND ',
  '  nfxv.Empresa=nfa.Empresa ',
  SQL_Filtro(),
  'AND IF(nfa.infCanc_cStat > 0, nfa.infCanc_cStat, nfa.infProt_cStat) = 100 ',
  'GROUP BY nfa.FatID, nfa.FatNum, nfa.Empresa ',
  '; ',
  ' ',
  'SELECT * FROM ' + F_NFe_100 + ' ',
  Ordem100,
  '; ',
  '']);
  //Geral.MB_SQL(Self, QrNFe_100);
end;

(*
procedure TFmNFe_Pesq_0000_ImpLista.ReopenNFe_CFOP();
var
  Ordem100: String;
begin
  Ordem100 := 'ORDER BY prod_CFOP, ide_nNF, nItem';
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_CFOP, DModG.MyPID_DB, [
  'DELETE FROM ' + F_NFe_CFOP + '; ',
  'INSERT INTO ' + F_NFe_CFOP + ' ',
  'SELECT nfi.nItem, nfi.prod_cProd, nfi.prod_xProd, ',
  'nfi.prod_CFOP, nfi.prod_NCM, nfi.prod_uCom, nfi.prod_qCom, nfi.prod_vUnCom,  ',
  'nfi.prod_vProd, ',
  '0 Ordem,  ',
  'nfa.ide_nNF, nfa.ide_serie, nfa.ide_dEmi,  ',
  'IF(nfi.nItem > 1, 0, nfa.ICMSTot_vProd) ICMSTot_vProd, ',
  'IF(nfi.nItem > 1, 0, nfa.ICMSTot_vNF) ICMSTot_vNF, ',
  'ELT(nfa.ide_tpEmis, "Normal", "Contingência FS",   ',
  '"Contingência SCAN", "Contingência EPEC",   ',
  '"Contingência FS-DA", "? ? ? ?") NOME_tpEmis,   ',
  'ELT(nfa.ide_tpNF + 1, "E", "S", "?") NOME_tpNF,   ',
  'IF(nfi.nItem > 1, "", nfa.ide_natOp) ide_natOp,  ',
  'IF(nfi.nItem > 1, 0, nfxv.qVol) qVol, ',
  'IF(nfi.nItem > 1, "", nfxv.esp) esp, ',
  'IF(nfi.nItem > 1, "", nfxv.marca) marca, ',
  'IF(nfi.nItem > 1, "", nfxv.nVol) nVol,   ',
  'IF(nfi.nItem > 1, 0, nfxv.PesoL) PesoL, ',
  'IF(nfi.nItem > 1, 0, nfxv.PesoB) PesoB  ',
  '  ',
  'FROM ' + TMeuDB + '.nfecaba nfa  ',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON  ',
  '  cli.Codigo=nfa.CodInfoDest  ',
  'LEFT JOIN ' + TMeuDB + '.entidades emi ON emi.Codigo=nfa.CodInfoEmit  ',
  'LEFT JOIN ' + TMeuDB + '.nfecabxvol nfxv ON   ',
  '  nfxv.FatID=nfa.FatID AND  ',
  '  nfxv.FatNum=nfa.FatNum AND  ',
  '  nfxv.Empresa=nfa.Empresa  ',
  'LEFT JOIN ' + TMeuDB + '.nfeitsi nfi ON   ',
  '  nfi.FatID=nfa.FatID AND  ',
  '  nfi.FatNum=nfa.FatNum AND  ',
  '  nfi.Empresa=nfa.Empresa  ',
   SQL_Filtro(),
  'AND IF(nfa.infCanc_cStat > 0, nfa.infCanc_cStat, nfa.infProt_cStat) = 100 ',
  '; ',
  ' ',
  'SELECT cfp.Nome NO_CFOP, nfe.*  ',
  'FROM ' + F_NFe_CFOP + ' nfe ',
  'LEFT JOIN ' + TMeuDB + '.cfop2003 cfp ON REPLACE(cfp.Codigo, ".", "")=nfe.prod_CFOP  ',
  Ordem100,
  '; ',
  '']);
  //Geral.MB_SQL(Self, QrNFe_CFOP);
end;
*)

procedure TFmNFe_Pesq_0000_ImpLista.ReopenNFe_Its(Grupo: TNFEAgruRelNFs);
var
  Ordem100: String;
begin
  case Grupo of
    nfearnCOFP:
      Ordem100 := 'ORDER BY prod_CFOP, ide_nNF, nItem';
    nfearnNCM:
      Ordem100 := 'ORDER BY prod_NCM, ide_nNF, nItem';
    nfearnProd:
      Ordem100 := 'ORDER BY ordem, prod_CFOP, prod_xProd';
    nfearnTipPrd:
      Ordem100 := 'ORDER BY ordem, prod_CFOP, NO_PGT, prod_xProd';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_CFOP, DModG.MyPID_DB, [
    'DELETE FROM ' + F_NFe_CFOP + '; ',
    'INSERT INTO ' + F_NFe_CFOP + ' ',
    'SELECT nfi.nItem, nfi.prod_cProd, nfi.prod_xProd, ',
    'nfi.prod_CFOP, nfi.prod_NCM, nfi.prod_uCom, nfi.prod_qCom, nfi.prod_vUnCom,  ',
    'nfi.prod_vProd, ',
    '0 Ordem,  ',
    'nfa.ide_nNF, nfa.ide_serie, nfa.ide_dEmi,  ',
    'IF(nfi.nItem > 1, 0, nfa.ICMSTot_vProd) ICMSTot_vProd, ',
    'IF(nfi.nItem > 1, 0, nfa.ICMSTot_vNF) ICMSTot_vNF, ',
    'ELT(nfa.ide_tpEmis, "Normal", "Contingência FS",   ',
    '"Contingência SCAN", "Contingência EPEC",   ',
    '"Contingência FS-DA", "? ? ? ?") NOME_tpEmis,   ',
    'ELT(nfa.ide_tpNF + 1, "E", "S", "?") NOME_tpNF,   ',
    'IF(nfi.nItem > 1, "", nfa.ide_natOp) ide_natOp,  ',
    'IF(nfi.nItem > 1, 0, nfxv.qVol) qVol, ',
    'IF(nfi.nItem > 1, "", nfxv.esp) esp, ',
    'IF(nfi.nItem > 1, "", nfxv.marca) marca, ',
    'IF(nfi.nItem > 1, "", nfxv.nVol) nVol,   ',
    'IF(nfi.nItem > 1, 0, nfxv.PesoL) PesoL, ',
    'IF(nfi.nItem > 1, 0, nfxv.PesoB) PesoB  ',
    '  ',
    'FROM ' + TMeuDB + '.nfecaba nfa  ',
    'LEFT JOIN ' + TMeuDB + '.entidades cli ON  ',
    '  cli.Codigo=nfa.CodInfoDest  ',
    'LEFT JOIN ' + TMeuDB + '.entidades emi ON emi.Codigo=nfa.CodInfoEmit  ',
    'LEFT JOIN ' + TMeuDB + '.nfecabxvol nfxv ON   ',
    '  nfxv.FatID=nfa.FatID AND  ',
    '  nfxv.FatNum=nfa.FatNum AND  ',
    '  nfxv.Empresa=nfa.Empresa  ',
    'LEFT JOIN ' + TMeuDB + '.nfeitsi nfi ON   ',
    '  nfi.FatID=nfa.FatID AND  ',
    '  nfi.FatNum=nfa.FatNum AND  ',
    '  nfi.Empresa=nfa.Empresa  ',
     SQL_Filtro(),
    'AND IF(nfa.infCanc_cStat > 0, nfa.infCanc_cStat, nfa.infProt_cStat) = 100 ',
    '; ',
    ' ',
{   ini 2019-04-10
    'SELECT cfp.Nome NO_CFOP, nfe.*  ',
    'FROM ' + F_NFe_CFOP + ' nfe ',
    'LEFT JOIN ' + TMeuDB + '.cfop2003 cfp ON REPLACE(cfp.Codigo, ".", "")=nfe.prod_CFOP  ',
    ////
}
    'SELECT cfp.Nome NO_CFOP, nfe.*,  ',
    'gg1.PrdGrupTip, pgt.Nome NO_PGT',
    'FROM ' + F_NFe_CFOP + ' nfe ',
    'LEFT JOIN ' + TMeuDB + '.cfop2003 cfp ON REPLACE(cfp.Codigo, ".", "")=nfe.prod_CFOP  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=nfe.prod_cProd',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
    '',
    Ordem100,
    '; ',
    '']);
  //Geral.MB_SQL(Self, QrNFe_CFOP);
end;

function TFmNFe_Pesq_0000_ImpLista.SQL_Filtro(): String;
var
  Empresa, SQL_DOC, SQL_Doc1, SQL_1XX, SQL_Amb, SQL_SitConf: String;
begin
  if (EdFilial_ValueVariant <> Null) and (EdFilial_ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  case RGQuemEmit_ItemIndex of
    0: SQL_DOC := 'AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"';
    1: SQL_DOC := 'AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)';
    else SQL_DOC := '';
  end;
  if Ck100e101_Checked  then
    SQL_1XX := 'AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)'
  else
    SQL_1XX := '';
  //
  case RGAmbiente_ItemIndex of
    //0: ; // Tudo
    1: SQL_Amb := 'AND nfa.ide_tpAmb=1';
    2: SQL_Amb := 'AND nfa.ide_tpAmb=2';
    else SQL_Amb := '';
  end;
  SQL_Doc1 := '';
  if EdCliente_ValueVariant <> 0 then
  begin
    if QrClientesTipo_Value = 0 then
    begin
      SQL_Doc1 := Geral.TFD(QrClientesCNPJ_Value, 14, siPositivo);
      SQL_Doc1 := 'AND dest_CNPJ="' + SQL_Doc1 + '"';
    end else begin
      SQL_Doc1 := Geral.TFD(QrClientesCPF_Value, 11, siPositivo);
      SQL_Doc1 := 'AND dest_CPF="' + SQL_Doc1 + '"';
    end;
  end;
  //
  SQL_SitConf := CGcSitConf.GetIndexesChecked(True, -1);
  SQL_SitConf := 'AND nfa.cSitConf IN (' + SQL_SitConf + ')';
  //
  Result := Geral.ATS([
  dmkPF.SQL_Periodo('WHERE ide_dEmi ', TPDataI_Date, TPDataF_Date, True, True),
  'AND nfa.Empresa = ' + Empresa,
  SQL_DOC,
  SQL_1XX,
  SQL_Amb,
  SQL_Doc1,
  SQL_SitConf,
  '']);
end;

end.
