unit NFe_LocEPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCB, dmkDBLookupComboBox, mySQLDbTables, dmkDBGridZTO;

type
  TFmNFe_LocEPesq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPsq: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    QrEmitente: TmySQLQuery;
    QrEmitenteCodigo: TIntegerField;
    QrEmitenteNOMEENTIDADE: TWideStringField;
    DsEmitente: TDataSource;
    EdEmitente: TdmkEditCB;
    CBEmitente: TdmkDBLookupComboBox;
    CkEmitente: TCheckBox;
    EdNF: TdmkEdit;
    Label2: TLabel;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabANO_Cli: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAnProt: TWideStringField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAdhRecbto: TWideStringField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    QrNFeCabAdest_xNome: TWideStringField;
    QrNFeCabAversao: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabANOME_tpEmis: TWideStringField;
    QrNFeCabANOME_tpNF: TWideStringField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFeCabAcStat: TFloatField;
    QrNFeCabAcSitNFe: TSmallintField;
    QrNFeCabAcSitConf: TSmallintField;
    QrNFeCabANO_cSitConf: TWideStringField;
    QrNFeCabANFeNT2013_003LTT: TSmallintField;
    QrNFeCabANO_Emi: TWideStringField;
    QrNFeCabANO_Terceiro: TWideStringField;
    QrNFeCabAemit_CNPJ: TWideStringField;
    QrNFeCabANO_Cli2: TWideStringField;
    QrNFeCabAinfCCe_nSeqEvento: TIntegerField;
    DsNFeCabA: TDataSource;
    dmkDBGrid1: TdmkDBGridZTO;
    BtLoc: TBitBtn;
    QrNFeItsI: TmySQLQuery;
    QrNFeItsIFatID: TIntegerField;
    QrNFeItsIFatNum: TIntegerField;
    QrNFeItsIEmpresa: TIntegerField;
    QrNFeItsInItem: TIntegerField;
    QrNFeItsIprod_cProd: TWideStringField;
    QrNFeItsIprod_cEAN: TWideStringField;
    QrNFeItsIprod_xProd: TWideStringField;
    QrNFeItsIprod_NCM: TWideStringField;
    QrNFeItsIprod_EXTIPI: TWideStringField;
    QrNFeItsIprod_genero: TSmallintField;
    QrNFeItsIprod_CFOP: TIntegerField;
    QrNFeItsIprod_uCom: TWideStringField;
    QrNFeItsIprod_qCom: TFloatField;
    QrNFeItsIprod_vUnCom: TFloatField;
    QrNFeItsIprod_vProd: TFloatField;
    QrNFeItsIprod_cEANTrib: TWideStringField;
    QrNFeItsIprod_uTrib: TWideStringField;
    QrNFeItsIprod_qTrib: TFloatField;
    QrNFeItsIprod_vUnTrib: TFloatField;
    QrNFeItsIprod_vFrete: TFloatField;
    QrNFeItsIprod_vSeg: TFloatField;
    QrNFeItsIprod_vDesc: TFloatField;
    QrNFeItsIprod_vOutro: TFloatField;
    QrNFeItsITem_IPI: TSmallintField;
    QrNFeItsIInfAdCuztm: TIntegerField;
    QrNFeItsIEhServico: TIntegerField;
    QrNFeItsIEhServico_TXT: TWideStringField;
    QrNFeItsITem_II: TSmallintField;
    QrNFeItsIprod_indTot: TSmallintField;
    QrNFeItsIprod_CEST: TIntegerField;
    QrNFeItsIprod_xPed: TWideStringField;
    QrNFeItsIprod_nItemPed: TIntegerField;
    QrNFeItsIprod_nFCI: TWideStringField;
    QrNFeItsIStqMovValA: TIntegerField;
    DsNFeItsI: TDataSource;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPsqClick(Sender: TObject);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure BtLocClick(Sender: TObject);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }

  public
    { Public declarations }
    FEmitida: Boolean;
    //
    procedure Pesquisa();
  end;

  var
  FmNFe_LocEPesq: TFmNFe_LocEPesq;

implementation

uses UnMyObjects, Module, ModuleGeral, NFe_Pesq_0000, DmkDAC_PF, MyDBCheck;

{$R *.DFM}

procedure TFmNFe_LocEPesq.BtLocClick(Sender: TObject);
var
  IDCtrl: Integer;
begin
  IDCtrl := QrNFeCabAIDCtrl.Value;
  if IDCtrl <> 0 then
  begin
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      FmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl, True);
      FmNFe_Pesq_0000.ShowModal;
      FmNFe_Pesq_0000.Destroy;
    end;
  end;
end;

procedure TFmNFe_LocEPesq.BtPsqClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmNFe_LocEPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFe_LocEPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFe_LocEPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrEmitente, Dmod.MyDB);
end;

procedure TFmNFe_LocEPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFe_LocEPesq.Pesquisa();
var
  Empresa, CodInfoEmit, NF, IDCtrl: Integer;
  SQL_CodInfoEmit, SQL_Empresa, SQL_FatID: String;
begin
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  CodInfoEmit       := EdEmitente.ValueVariant;
  NF                := EdNF.ValueVariant;
  if Empresa = 0 then
    SQL_Empresa := 'AND nfa.Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL)
  else
    SQL_Empresa := 'AND nfa.Empresa=' + Geral.FF0(Empresa);
  if CodInfoEmit = 0 then
    SQL_CodInfoEmit := ''
  else
    SQL_CodInfoEmit := 'AND nfa.CodInfoEmit=' + Geral.FF0(CodInfoEmit);
  //
  if FEmitida then
    SQL_FatID := 'WHERE FatID=1 '
  else
    SQL_FatID := 'WHERE FatID<>1 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNfeCabA, Dmod.MyDB, [
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,  ',
  'IF(cli.Tipo=0,  ',
  'IF(cli.Fantasia <> "", cli.Fantasia, cli.RazaoSocial),  ',
  'IF(cli.Apelido <> "", cli.Apelido, cli.Nome)) NO_Cli2,   ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi,  ',
  'IF(nfa.FatID IN (51, 53),   ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome),  ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro,  ',
  'nfa.infCCe_nSeqEvento, nfa.FatID, nfa.FatNum,   ',
  'nfa.Empresa, nfa.LoteEnv, nfa.Id,  ',
  'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,  ',
  'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF,   ',
  'dest_xNome,   ',
  'IF(Importado>0, Status,   ',
  'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) + 0.000 cStat,  ',
  'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,  ',
  'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,  ',
  'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,  ',
  'IDCtrl, versao,   ',
  'ide_tpEmis, infCanc_xJust, Status,   ',
  'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg,   ',
  'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc,   ',
  'ICMSTot_vBC, ICMSTot_vICMS,   ',
  'ICMSTot_vPIS, ICMSTot_vCOFINS,   ',
  'cSitNFe, cSitConf,  ',
  'ELT(cSitConf+2, "N�o consultada", "Sem manifesta��o", "Confirmada",  ',
  '"Desconhecida", "N�o realizada", "Ci�ncia", "? ? ?") NO_cSitConf,  ',
  'nfa.infCanc_dhRecbto, nfa.infCanc_nProt,  ',
  'nfa.NFeNT2013_003LTT, nfa.emit_CNPJ   ',
  'FROM nfecaba nfa  ',
  'LEFT JOIN entidades cli ON  ',
  'cli.Codigo=nfa.CodInfoDest  ',
  'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmit  ',
  //'WHERE ide_dEmi  BETWEEN "2018-01-17" AND "2018-01-27 23:59:59"  ',
  SQL_FatID,
  SQL_Empresa,
  SQL_CodInfoEmit,
  'AND nfa.ide_nNF=' + Geral.FF0(NF),
  'ORDER BY ide_dEmi DESC',
  '']);
end;

procedure TFmNFe_LocEPesq.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  BtLoc.Enabled := QrNFeCabA.RecordCount > 0;
end;

procedure TFmNFe_LocEPesq.QrNFeCabAAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI, Dmod.MyDB, [
  'SELECT *   ',
  'FROM nfeitsi  ',
  'WHERE FatID=' + Geral.FF0(QrNFeCAbAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeCAbAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeCAbAEmpresa.Value),
  '']);
end;

procedure TFmNFe_LocEPesq.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  BtLoc.Enabled := False;
  QrNFeItsI.Close;
end;

end.
