unit NFeJust;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,  
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkCheckGroup,
  dmkImage, UnDmkEnums;

type
  TFmNFeJust = class(TForm)
    PainelDados: TPanel;
    DsNFeJust: TDataSource;
    QrNFeJust: TmySQLQuery;
    PainelEdita: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrNFeJustCodigo: TIntegerField;
    QrNFeJustCodUsu: TIntegerField;
    QrNFeJustNome: TWideStringField;
    CGAplicacao: TdmkCheckGroup;
    QrNFeJustAplicacao: TIntegerField;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeJustAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeJustBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmNFeJust: TFmNFeJust;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeJust.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeJust.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeJustCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeJust.DefParams;
begin
  VAR_GOTOTABELA := 'NFeJust';
  VAR_GOTOMYSQLTABLE := QrNFeJust;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Aplicacao');
  VAR_SQLx.Add('FROM nfejust');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmNFeJust.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfejust', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeJust.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeJust.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNFeJust.AlteraRegistro;
var
  NFeJust : Integer;
begin
  NFeJust := QrNFeJustCodigo.Value;
  if QrNFeJustCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NFeJust, Dmod.MyDB, 'NFeJust', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFeJust, Dmod.MyDB, 'NFeJust', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFeJust.IncluiRegistro;
var
  Cursor : TCursor;
  NFeJust : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NFeJust := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'NFeJust', 'NFeJust', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NFeJust))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, NFeJust);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNFeJust.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeJust.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeJust.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeJust.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeJust.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeJust.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeJust.BtAlteraClick(Sender: TObject);
begin
  if (QrNFeJust.State <> dsInactive) and (QrNFeJust.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeJust, [PainelDados],
      [PainelEdita], EdCodUsu, ImgTipo, 'nfejust');
  end;
end;

procedure TFmNFeJust.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeJustCodigo.Value;
  Close;
end;

procedure TFmNFeJust.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Length(Nome) < 15, EdNome,
  'Defina uma descri��o com no m�nimo 15 caracteres.') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfejust', '', 0)
  else
    Codigo := QrNFeJustCodigo.Value;
  //  
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeJust, PainelEdit,
    'NFeJust', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeJust.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFeJust', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeJust', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeJust', 'Codigo');
end;

procedure TFmNFeJust.BtExcluiClick(Sender: TObject);
begin
  Geral.MB_Aviso('Exclus�o n�o implementado para esta janela!');
end;

procedure TFmNFeJust.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeJust, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfejust');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfejust', 'CodUsu', [], [], stIns,
    0, siPositivo, EdCodUsu);
end;

procedure TFmNFeJust.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmNFeJust.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeJustCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeJust.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeJust.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeJust.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeJustCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeJust.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeJust.QrNFeJustAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeJust.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeJust.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeJustCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeJust', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeJust.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeJust.QrNFeJustBeforeOpen(DataSet: TDataSet);
begin
  QrNFeJustCodigo.DisplayFormat := FFormatFloat;
end;

end.


