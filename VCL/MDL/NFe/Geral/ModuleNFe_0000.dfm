object DmNFe_0000: TDmNFe_0000
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 634
  Width = 1092
  PixelsPerInch = 96
  object QrCFOP: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CFOP  '
      'FROM fisregcfop '
      'WHERE Codigo=:P0 '
      'AND Contribui=:P1 '
      'AND Interno=:P2 '
      'AND Proprio=:P3 '
      'AND Servico=:P4 '
      'AND SubsTrib=:P5 ')
    Left = 352
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
  end
  object QrOpcoesNFe: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesNFeversao: TFloatField
      FieldName = 'versao'
      Origin = 'paramsemp.versao'
    end
    object QrOpcoesNFeide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'paramsemp.ide_mod'
    end
    object QrOpcoesNFeide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'paramsemp.ide_tpImp'
    end
    object QrOpcoesNFeide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'paramsemp.ide_tpAmb'
    end
    object QrOpcoesNFeLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'paramsemp.Lk'
    end
    object QrOpcoesNFeAppCode: TSmallintField
      FieldName = 'AppCode'
      Origin = 'paramsemp.AppCode'
    end
    object QrOpcoesNFeEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'paramsemp.EntiTipCto'
    end
    object QrOpcoesNFeEntiTipCt1: TIntegerField
      FieldName = 'EntiTipCt1'
    end
    object QrOpcoesNFeNO_ETC_0: TWideStringField
      FieldName = 'NO_ETC_0'
      Size = 30
    end
    object QrOpcoesNFeNO_ETC_1: TWideStringField
      FieldName = 'NO_ETC_1'
      Size = 30
    end
    object QrOpcoesNFeMyEmailNFe: TWideStringField
      FieldName = 'MyEmailNFe'
      Origin = 'paramsemp.MyEmailNFe'
      Size = 255
    end
    object QrOpcoesNFeAssDigMode: TSmallintField
      FieldName = 'AssDigMode'
      Origin = 'paramsemp.AssDigMode'
    end
    object QrOpcoesNFeUF_MDeMDe: TWideStringField
      FieldName = 'UF_MDeMDe'
      Size = 4
    end
    object QrOpcoesNFeUF_MDeDes: TWideStringField
      FieldName = 'UF_MDeDes'
      Size = 4
    end
    object QrOpcoesNFeUF_MDeNFe: TWideStringField
      FieldName = 'UF_MDeNFe'
      Size = 4
    end
    object QrOpcoesNFeUF_DistDFeInt: TWideStringField
      DisplayWidth = 10
      FieldName = 'UF_DistDFeInt'
      Size = 10
    end
    object QrOpcoesNFeNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrOpcoesNFeNFeUF_EPEC: TWideStringField
      FieldName = 'NFeUF_EPEC'
      Size = 255
    end
  end
  object QrEmpresa: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial'
      ''
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 32
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmpresaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEmpresaCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrEmpresaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEmpresaFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEmpresaNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrEmpresaNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrEmpresaNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaDTB_UF: TWideStringField
      FieldName = 'DTB_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrEmpresaIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEmpresaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEmpresaCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
    object QrEmpresaCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
    object QrEmpresaUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrDest: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo,'
      'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, '
      'LUF, l2.Nome NO_LLOGRAD, LCodMunici,'
      'ml.Nome NO_LMunici, ul.Nome NO_LUF,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA'
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN dtb_munici ml ON ml.Codigo=en.LCodMunici'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'LEFT JOIN ufs ul ON ul.Codigo=en.LUF'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      '')
    Left = 32
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDestTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDestCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrDestCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrDestIE: TWideStringField
      FieldName = 'IE'
    end
    object QrDestRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrDestCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrDestFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrDestRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDestCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDestBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDestTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrDestNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrDestNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrDestNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrDestNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrDestSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrDestL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrDestL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object QrDestLLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object QrDestLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrDestLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrDestLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrDestLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrDestLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrDestLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrDestNO_LLOGRAD: TWideStringField
      FieldName = 'NO_LLOGRAD'
      Size = 10
    end
    object QrDestLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrDestNO_LMunici: TWideStringField
      FieldName = 'NO_LMunici'
      Size = 100
    end
    object QrDestNO_LUF: TWideStringField
      FieldName = 'NO_LUF'
      Required = True
      Size = 2
    end
    object QrDestNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrDestNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrDestCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrDestCodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object QrDestCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
    object QrDestUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrDestEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrDestEstrangTip: TSmallintField
      FieldName = 'EstrangTip'
    end
    object QrDestEstrangNum: TWideStringField
      FieldName = 'EstrangNum'
    end
    object QrDestindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object QrDestMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
  end
  object QrCFOP1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'WHERE Codigo=:P0'
      'ORDER BY Nome'
      '')
    Left = 28
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCFOP1Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCFOP1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrTotal1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(smva.Total) Total, SUM(smva.Qtde) Qtde,'
      'COUNT(DISTINCT OriCnta) + 0.000 Volumes'
      'FROM stqmovvala smva'
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P0'
      'AND smva.Empresa=:P1 ')
    Left = 664
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTotal1Total: TFloatField
      FieldName = 'Total'
    end
    object QrTotal1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTotal1Volumes: TFloatField
      FieldName = 'Volumes'
      Required = True
    end
  end
  object QrProds1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT med.Sigla NO_UNIDADE,                                    ' +
        '    '
      
        'smva.Empresa, smva.Preco, smva.GraGruX CU_PRODUTO,              ' +
        '    '
      
        'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,             ' +
        '    '
      
        'SUM(smva.Total) Total, SUM(smva.Qtde) Qtde, smva.CFOP,          ' +
        '    '
      
        'smva.CFOP_Contrib, smva.CFOP_MesmaUF, smva.CFOP_Proprio,        ' +
        '    '
      
        'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         ' +
        '    '
      
        'gg1.CST_A, gg1.CST_B,                                           ' +
        '    '
      
        'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              ' +
        '    '
      
        'gg1.NCM, ncm.Letras, gg1.IPI_CST, gg1.IPI_cEnq,                 ' +
        '    '
      
        'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              ' +
        '    '
      
        'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,              ' +
        '    '
      
        'gg1.InfAdProd, smva.MedidaC, smva.MedidaL, smva.MedidaA,        ' +
        '    '
      
        'smva.MedidaE, mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4' +
        ',   '
      
        'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4, gta.PrintTam,   ' +
        '    '
      
        'gg1.PIS_CST, gg1.PIS_AlqV, gg1.PISST_AlqV, gg1.COFINS_CST,      ' +
        '    '
      
        'gg1.COFINS_AlqP, gg1.COFINS_AlqV, gg1.COFINSST_AlqP,            ' +
        '    '
      
        'gg1.COFINSST_AlqV, gg1.ICMS_modBC, gg1.ICMS_modBCST,            ' +
        '    '
      
        'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST, gg1.ICMS_pMVAST,            ' +
        '    '
      
        'gg1.ICMS_pICMSST,  gg1.IPI_vUnid, gg1.IPI_TpTrib,               ' +
        '    '
      
        'gg1.IPI_pIPI, gg1.ICMS_Pauta, gg1.ICMS_MaxTab,                  ' +
        '    '
      
        'gg1.PIS_pRedBC, gg1.PISST_pRedBCST,                             ' +
        '    '
      
        'gg1.COFINS_pRedBC, gg1.COFINSST_pRedBCST,                       ' +
        '    '
      
        'gg1.cGTIN_EAN, gg1.EX_TIPI, gg1.PISST_AlqP, gg1.PIS_AlqP,       ' +
        '    '
      
        'smva.Servico, gcc.PrintCor, smva.prod_indTot,                   ' +
        '    '
      'smva.CSOSN, smva.pCredSN, iTotTrib, vTotTrib,'
      'gg1.prod_CEST, gg1.prod_indEscala'
      'FROM stqmovvala smva'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo'
      'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm'
      'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem'
      'LEFT JOIN gragrux ggx1 ON ggx1.Controle=smva.RefProd'
      'LEFT JOIN gragru1 gg11 ON gg11.Nivel1=ggx1.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE smva.Tipo = 1'
      'AND smva.OriCodi=3783'
      'AND smva.Empresa=-11'
      'GROUP BY smva.Preco, smva.GraGruX'
      '')
    Left = 664
    object QrProds1NO_UNIDADE: TWideStringField
      FieldName = 'NO_UNIDADE'
      Size = 6
    end
    object QrProds1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProds1Preco: TFloatField
      FieldName = 'Preco'
    end
    object QrProds1CU_PRODUTO: TIntegerField
      FieldName = 'CU_PRODUTO'
    end
    object QrProds1CONTROLE: TIntegerField
      FieldName = 'CONTROLE'
    end
    object QrProds1InfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrProds1PercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrProds1Total: TFloatField
      FieldName = 'Total'
    end
    object QrProds1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrProds1CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 6
    end
    object QrProds1CFOP_Contrib: TSmallintField
      FieldName = 'CFOP_Contrib'
    end
    object QrProds1CFOP_MesmaUF: TSmallintField
      FieldName = 'CFOP_MesmaUF'
    end
    object QrProds1CFOP_Proprio: TSmallintField
      FieldName = 'CFOP_Proprio'
    end
    object QrProds1CU_GRUPO: TIntegerField
      FieldName = 'CU_GRUPO'
    end
    object QrProds1NO_GRUPO: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_GRUPO'
      Size = 120
    end
    object QrProds1CST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrProds1CST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrProds1CST_T: TWideStringField
      FieldName = 'CST_T'
      Size = 8
    end
    object QrProds1NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProds1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrProds1IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrProds1GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrProds1CU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrProds1NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrProds1NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 10
    end
    object QrProds1CO_GRUPO: TIntegerField
      FieldName = 'CO_GRUPO'
    end
    object QrProds1Desc_Per: TLargeintField
      FieldName = 'Desc_Per'
      Required = True
    end
    object QrProds1InfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object QrProds1MedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrProds1MedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrProds1MedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrProds1MedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrProds1Medida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrProds1Medida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrProds1Medida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrProds1Medida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrProds1Sigla1: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object QrProds1Sigla2: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object QrProds1Sigla3: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object QrProds1Sigla4: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object QrProds1PrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrProds1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrProds1PIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object QrProds1PISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object QrProds1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrProds1COFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object QrProds1COFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object QrProds1COFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object QrProds1COFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object QrProds1ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrProds1ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrProds1ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrProds1ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrProds1ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrProds1ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrProds1IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrProds1IPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrProds1IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrProds1ICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
    end
    object QrProds1ICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
    end
    object QrProds1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProds1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProds1Servico: TIntegerField
      FieldName = 'Servico'
    end
    object QrProds1PrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrProds1PIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
    end
    object QrProds1PISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
    end
    object QrProds1COFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
    end
    object QrProds1COFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
    end
    object QrProds1PISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
    end
    object QrProds1PIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
    end
    object QrProds1prod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrProds1pCredSN: TFloatField
      FieldName = 'pCredSN'
    end
    object QrProds1iTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrProds1vTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrProds1pTotTrib: TFloatField
      FieldName = 'pTotTrib'
    end
    object QrProds1ICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
      DisplayFormat = '0.0000'
    end
    object QrProds1ICMS_pICMSDeson: TFloatField
      FieldName = 'ICMS_pICMSDeson'
      DisplayFormat = '0.0000'
    end
    object QrProds1ICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrProds1prod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrProds1ID: TIntegerField
      FieldName = 'ID'
    end
    object QrProds1prod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrProds1IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrProds1prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrProds1prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrProds1prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrProds1prod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrProds1CSOSN_Manual: TIntegerField
      FieldName = 'CSOSN_Manual'
    end
    object QrProds1CSOSN_GG1: TIntegerField
      FieldName = 'CSOSN_GG1'
    end
    object QrProds1UsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
    object QrProds1prod_cBarraGG1: TWideStringField
      FieldName = 'prod_cBarraGG1'
      Size = 30
    end
    object QrProds1prod_cBarraGGX: TWideStringField
      FieldName = 'prod_cBarraGGX'
      Size = 30
    end
    object QrProds1obsCont_xCampo: TWideStringField
      FieldName = 'obsCont_xCampo'
    end
    object QrProds1obsCont_xTexto: TWideStringField
      FieldName = 'obsCont_xTexto'
      Size = 60
    end
    object QrProds1obsFisco_xCampo: TWideStringField
      FieldName = 'obsFisco_xCampo'
    end
    object QrProds1obsFisco_xTexto: TWideStringField
      FieldName = 'obsFisco_xTexto'
      Size = 60
    end
  end
  object QrTransporta: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrTransportaCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.RG, en.NIRE, en.CNAE,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF'
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo=:P0'
      '')
    Left = 28
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransportaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTransportaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTransportaRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTransportaCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrTransportaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrTransportaFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrTransportaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrTransportaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrTransportaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrTransportaTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrTransportaNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrTransportaNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrTransportaNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrTransportaENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 60
      Calculated = True
    end
    object QrTransportaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrTransportaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrTransportaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrTransportaUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrTransportaCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
  end
  object QrFilial: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFilialSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
      Origin = 'paramsemp.SimplesFed'
    end
    object QrFilialUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Origin = 'paramsemp.UF_WebServ'
      Size = 2
    end
    object QrFilialDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Origin = 'paramsemp.DirNFeGer'
      Size = 255
    end
    object QrFilialDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Origin = 'paramsemp.DirNFeAss'
      Size = 255
    end
    object QrFilialDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Origin = 'paramsemp.DirEnvLot'
      Size = 255
    end
    object QrFilialDirRec: TWideStringField
      FieldName = 'DirRec'
      Origin = 'paramsemp.DirRec'
      Size = 255
    end
    object QrFilialDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Origin = 'paramsemp.DirPedRec'
      Size = 255
    end
    object QrFilialDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Origin = 'paramsemp.DirProRec'
      Size = 255
    end
    object QrFilialDirDen: TWideStringField
      FieldName = 'DirDen'
      Origin = 'paramsemp.DirDen'
      Size = 255
    end
    object QrFilialDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Origin = 'paramsemp.DirPedCan'
      Size = 255
    end
    object QrFilialDirCan: TWideStringField
      FieldName = 'DirCan'
      Origin = 'paramsemp.DirCan'
      Size = 255
    end
    object QrFilialDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Origin = 'paramsemp.DirPedInu'
      Size = 255
    end
    object QrFilialDirInu: TWideStringField
      FieldName = 'DirInu'
      Origin = 'paramsemp.DirInu'
      Size = 255
    end
    object QrFilialDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Origin = 'paramsemp.DirPedSit'
      Size = 255
    end
    object QrFilialDirSit: TWideStringField
      FieldName = 'DirSit'
      Origin = 'paramsemp.DirSit'
      Size = 255
    end
    object QrFilialDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Origin = 'paramsemp.DirPedSta'
      Size = 255
    end
    object QrFilialDirSta: TWideStringField
      FieldName = 'DirSta'
      Origin = 'paramsemp.DirSta'
      Size = 255
    end
    object QrFilialSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrFilialInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
    end
    object QrFilialUF_Servico: TWideStringField
      DisplayWidth = 10
      FieldName = 'UF_Servico'
      Size = 10
    end
    object QrFilialNFeSerNum: TWideStringField
      FieldName = 'NFeSerNum'
      Size = 255
    end
    object QrFilialSINTEGRA_Path: TWideStringField
      FieldName = 'SINTEGRA_Path'
      Size = 255
    end
    object QrFilialDirDANFEs: TWideStringField
      FieldName = 'DirDANFEs'
      Size = 255
    end
    object QrFilialDirNFeProt: TWideStringField
      FieldName = 'DirNFeProt'
      Size = 255
    end
    object QrFilialNFeSerVal: TDateField
      FieldName = 'NFeSerVal'
    end
    object QrFilialNFeSerAvi: TSmallintField
      FieldName = 'NFeSerAvi'
    end
    object QrFilialNFetpEmis: TSmallintField
      FieldName = 'NFetpEmis'
    end
    object QrFilialSCAN_Ser: TIntegerField
      FieldName = 'SCAN_Ser'
    end
    object QrFilialSCAN_nNF: TIntegerField
      FieldName = 'SCAN_nNF'
    end
    object QrFilialDirNFeRWeb: TWideStringField
      FieldName = 'DirNFeRWeb'
      Size = 255
    end
    object QrFilialDirEveEnvLot: TWideStringField
      FieldName = 'DirEveEnvLot'
      Size = 255
    end
    object QrFilialDirEveRetLot: TWideStringField
      FieldName = 'DirEveRetLot'
      Size = 255
    end
    object QrFilialDirEvePedCCe: TWideStringField
      FieldName = 'DirEvePedCCe'
      Size = 255
    end
    object QrFilialDirEvePedCan: TWideStringField
      FieldName = 'DirEvePedCan'
      Size = 255
    end
    object QrFilialDirEveRetCan: TWideStringField
      FieldName = 'DirEveRetCan'
      Size = 255
    end
    object QrFilialDirEveRetCCe: TWideStringField
      FieldName = 'DirEveRetCCe'
      Size = 255
    end
    object QrFilialDirEveProcCCe: TWideStringField
      FieldName = 'DirEveProcCCe'
      Size = 255
    end
    object QrFilialPreMailEveCCe: TIntegerField
      FieldName = 'PreMailEveCCe'
    end
    object QrFilialDirRetNfeDes: TWideStringField
      FieldName = 'DirRetNfeDes'
      Size = 255
    end
    object QrFilialDirEvePedMDe: TWideStringField
      FieldName = 'DirEvePedMDe'
      Size = 255
    end
    object QrFilialDirEveRetMDe: TWideStringField
      FieldName = 'DirEveRetMDe'
      Size = 255
    end
    object QrFilialDirDowNFeDes: TWideStringField
      FieldName = 'DirDowNFeDes'
      Size = 255
    end
    object QrFilialDirDowNFeNFe: TWideStringField
      FieldName = 'DirDowNFeNFe'
      Size = 255
    end
    object QrFilialNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrFilialDirDistDFeInt: TWideStringField
      FieldName = 'DirDistDFeInt'
      Size = 255
    end
    object QrFilialDirRetDistDFeInt: TWideStringField
      FieldName = 'DirRetDistDFeInt'
      Size = 255
    end
    object QrFilialDirDowNFeCnf: TWideStringField
      FieldName = 'DirDowNFeCnf'
      Size = 255
    end
    object QrFilialDirEvePedEPEC: TWideStringField
      FieldName = 'DirEvePedEPEC'
      Size = 255
    end
    object QrFilialDirEveRetEPEC: TWideStringField
      FieldName = 'DirEveRetEPEC'
      Size = 255
    end
  end
  object QrNFECabA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfecaba.FatID'
    end
    object QrNFECabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfecaba.FatNum'
    end
    object QrNFECabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfecaba.Empresa'
    end
    object QrNFECabAversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfecaba.versao'
    end
    object QrNFECabAId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfecaba.Id'
      Size = 44
    end
    object QrNFECabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
      Origin = 'nfecaba.ide_cUF'
    end
    object QrNFECabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
      Origin = 'nfecaba.ide_cNF'
    end
    object QrNFECabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'nfecaba.ide_natOp'
      Size = 60
    end
    object QrNFECabAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
      Origin = 'nfecaba.ide_indPag'
    end
    object QrNFECabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'nfecaba.ide_mod'
    end
    object QrNFECabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'nfecaba.ide_serie'
    end
    object QrNFECabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Origin = 'nfecaba.ide_nNF'
    end
    object QrNFECabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'nfecaba.ide_dEmi'
    end
    object QrNFECabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      Origin = 'nfecaba.ide_dSaiEnt'
    end
    object QrNFECabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrNFECabAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
      Origin = 'nfecaba.ide_cMunFG'
    end
    object QrNFECabAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'nfecaba.ide_tpImp'
    end
    object QrNFECabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'nfecaba.ide_tpEmis'
    end
    object QrNFECabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
      Origin = 'nfecaba.ide_cDV'
    end
    object QrNFECabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'nfecaba.ide_tpAmb'
    end
    object QrNFECabAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
      Origin = 'nfecaba.ide_finNFe'
    end
    object QrNFECabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
      Origin = 'nfecaba.ide_procEmi'
    end
    object QrNFECabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
      Origin = 'nfecaba.ide_verProc'
    end
    object QrNFECabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Origin = 'nfecaba.emit_CNPJ'
      Size = 14
    end
    object QrNFECabAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Origin = 'nfecaba.emit_CPF'
      Size = 11
    end
    object QrNFECabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Origin = 'nfecaba.emit_xNome'
      Size = 60
    end
    object QrNFECabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Origin = 'nfecaba.emit_xFant'
      Size = 60
    end
    object QrNFECabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Origin = 'nfecaba.emit_xLgr'
      Size = 60
    end
    object QrNFECabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Origin = 'nfecaba.emit_nro'
      Size = 60
    end
    object QrNFECabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Origin = 'nfecaba.emit_xCpl'
      Size = 60
    end
    object QrNFECabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Origin = 'nfecaba.emit_xBairro'
      Size = 60
    end
    object QrNFECabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
      Origin = 'nfecaba.emit_cMun'
    end
    object QrNFECabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Origin = 'nfecaba.emit_xMun'
      Size = 60
    end
    object QrNFECabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Origin = 'nfecaba.emit_UF'
      Size = 2
    end
    object QrNFECabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
      Origin = 'nfecaba.emit_CEP'
    end
    object QrNFECabAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
      Origin = 'nfecaba.emit_cPais'
    end
    object QrNFECabAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Origin = 'nfecaba.emit_xPais'
      Size = 60
    end
    object QrNFECabAemit_fone: TWideStringField
      DisplayWidth = 14
      FieldName = 'emit_fone'
      Origin = 'nfecaba.emit_fone'
      Size = 14
    end
    object QrNFECabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Origin = 'nfecaba.emit_IE'
      Size = 14
    end
    object QrNFECabAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Origin = 'nfecaba.emit_IEST'
      Size = 14
    end
    object QrNFECabAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Origin = 'nfecaba.emit_IM'
      Size = 15
    end
    object QrNFECabAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Origin = 'nfecaba.emit_CNAE'
      Size = 7
    end
    object QrNFECabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Origin = 'nfecaba.dest_CNPJ'
      Size = 14
    end
    object QrNFECabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Origin = 'nfecaba.dest_CPF'
      Size = 11
    end
    object QrNFECabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Origin = 'nfecaba.dest_xNome'
      Size = 60
    end
    object QrNFECabAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Origin = 'nfecaba.dest_xLgr'
      Size = 60
    end
    object QrNFECabAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Origin = 'nfecaba.dest_nro'
      Size = 60
    end
    object QrNFECabAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Origin = 'nfecaba.dest_xCpl'
      Size = 60
    end
    object QrNFECabAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Origin = 'nfecaba.dest_xBairro'
      Size = 60
    end
    object QrNFECabAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
      Origin = 'nfecaba.dest_cMun'
    end
    object QrNFECabAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Origin = 'nfecaba.dest_xMun'
      Size = 60
    end
    object QrNFECabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Origin = 'nfecaba.dest_UF'
      Size = 2
    end
    object QrNFECabAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Origin = 'nfecaba.dest_CEP'
      Size = 8
    end
    object QrNFECabAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
      Origin = 'nfecaba.dest_cPais'
    end
    object QrNFECabAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Origin = 'nfecaba.dest_xPais'
      Size = 60
    end
    object QrNFECabAdest_fone: TWideStringField
      DisplayWidth = 14
      FieldName = 'dest_fone'
      Origin = 'nfecaba.dest_fone'
      Size = 14
    end
    object QrNFECabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Origin = 'nfecaba.dest_IE'
      Size = 14
    end
    object QrNFECabAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Origin = 'nfecaba.dest_ISUF'
      Size = 9
    end
    object QrNFECabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Origin = 'nfecaba.ICMSTot_vBC'
    end
    object QrNFECabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Origin = 'nfecaba.ICMSTot_vICMS'
    end
    object QrNFECabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Origin = 'nfecaba.ICMSTot_vBCST'
    end
    object QrNFECabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Origin = 'nfecaba.ICMSTot_vST'
    end
    object QrNFECabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Origin = 'nfecaba.ICMSTot_vProd'
    end
    object QrNFECabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Origin = 'nfecaba.ICMSTot_vFrete'
    end
    object QrNFECabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Origin = 'nfecaba.ICMSTot_vSeg'
    end
    object QrNFECabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Origin = 'nfecaba.ICMSTot_vDesc'
    end
    object QrNFECabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Origin = 'nfecaba.ICMSTot_vII'
    end
    object QrNFECabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Origin = 'nfecaba.ICMSTot_vIPI'
    end
    object QrNFECabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Origin = 'nfecaba.ICMSTot_vPIS'
    end
    object QrNFECabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Origin = 'nfecaba.ICMSTot_vCOFINS'
    end
    object QrNFECabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Origin = 'nfecaba.ICMSTot_vOutro'
    end
    object QrNFECabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Origin = 'nfecaba.ICMSTot_vNF'
    end
    object QrNFECabAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
      Origin = 'nfecaba.ISSQNtot_vServ'
    end
    object QrNFECabAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
      Origin = 'nfecaba.ISSQNtot_vBC'
    end
    object QrNFECabAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
      Origin = 'nfecaba.ISSQNtot_vISS'
    end
    object QrNFECabAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
      Origin = 'nfecaba.ISSQNtot_vPIS'
    end
    object QrNFECabAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
      Origin = 'nfecaba.ISSQNtot_vCOFINS'
    end
    object QrNFECabARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
      Origin = 'nfecaba.RetTrib_vRetPIS'
    end
    object QrNFECabARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
      Origin = 'nfecaba.RetTrib_vRetCOFINS'
    end
    object QrNFECabARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
      Origin = 'nfecaba.RetTrib_vRetCSLL'
    end
    object QrNFECabARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
      Origin = 'nfecaba.RetTrib_vBCIRRF'
    end
    object QrNFECabARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
      Origin = 'nfecaba.RetTrib_vIRRF'
    end
    object QrNFECabARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
      Origin = 'nfecaba.RetTrib_vBCRetPrev'
    end
    object QrNFECabARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
      Origin = 'nfecaba.RetTrib_vRetPrev'
    end
    object QrNFECabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Origin = 'nfecaba.ModFrete'
    end
    object QrNFECabATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Origin = 'nfecaba.Transporta_CNPJ'
      Size = 14
    end
    object QrNFECabATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Origin = 'nfecaba.Transporta_CPF'
      Size = 11
    end
    object QrNFECabATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Origin = 'nfecaba.Transporta_XNome'
      Size = 60
    end
    object QrNFECabATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Origin = 'nfecaba.Transporta_IE'
      Size = 14
    end
    object QrNFECabATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Origin = 'nfecaba.Transporta_XEnder'
      Size = 60
    end
    object QrNFECabATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Origin = 'nfecaba.Transporta_XMun'
      Size = 60
    end
    object QrNFECabATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Origin = 'nfecaba.Transporta_UF'
      Size = 2
    end
    object QrNFECabARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
      Origin = 'nfecaba.RetTransp_vServ'
    end
    object QrNFECabARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
      Origin = 'nfecaba.RetTransp_vBCRet'
    end
    object QrNFECabARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
      Origin = 'nfecaba.RetTransp_PICMSRet'
    end
    object QrNFECabARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
      Origin = 'nfecaba.RetTransp_vICMSRet'
    end
    object QrNFECabARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Origin = 'nfecaba.RetTransp_CFOP'
      Size = 4
    end
    object QrNFECabARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Origin = 'nfecaba.RetTransp_CMunFG'
      Size = 7
    end
    object QrNFECabAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Origin = 'nfecaba.VeicTransp_Placa'
      Size = 8
    end
    object QrNFECabAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Origin = 'nfecaba.VeicTransp_UF'
      Size = 2
    end
    object QrNFECabAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
      Origin = 'nfecaba.VeicTransp_RNTC'
    end
    object QrNFECabACobr_Fat_NFat: TWideStringField
      FieldName = 'Cobr_Fat_NFat'
      Origin = 'nfecaba.Cobr_Fat_nFat'
      Size = 60
    end
    object QrNFECabACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
      Origin = 'nfecaba.Cobr_Fat_vOrig'
    end
    object QrNFECabACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
      Origin = 'nfecaba.Cobr_Fat_vDesc'
    end
    object QrNFECabACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
      Origin = 'nfecaba.Cobr_Fat_vLiq'
    end
    object QrNFECabAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      Origin = 'nfecaba.InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFECabAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Origin = 'nfecaba.Exporta_UFEmbarq'
      Size = 2
    end
    object QrNFECabAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Origin = 'nfecaba.Exporta_XLocEmbarq'
      Size = 60
    end
    object QrNFECabACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'nfecaba.Compra_XNEmp'
      Size = 17
    end
    object QrNFECabACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'nfecaba.Compra_XPed'
      Size = 60
    end
    object QrNFECabACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'nfecaba.Compra_XCont'
      Size = 60
    end
    object QrNFECabAStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrNFECabA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Origin = 'nfecaba._Ativo_'
    end
    object QrNFECabALk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfecaba.Lk'
    end
    object QrNFECabADataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfecaba.DataCad'
    end
    object QrNFECabADataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfecaba.DataAlt'
    end
    object QrNFECabAUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfecaba.UserCad'
    end
    object QrNFECabAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfecaba.UserAlt'
    end
    object QrNFECabAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfecaba.AlterWeb'
    end
    object QrNFECabAAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfecaba.Ativo'
    end
    object QrNFECabAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrNFECabASegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrNFECabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFECabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrNFECabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNFECabAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrNFECabAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrNFECabAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrNFECabAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrNFECabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrNFECabAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrNFECabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFECabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFECabAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrNFECabAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrNFECabAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrNFECabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFECabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFECabAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrNFECabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFECabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFECabAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrNFECabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrNFECabAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrNFECabACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrNFECabATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrNFECabACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrNFECabAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrNFECabAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrNFECabAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNFECabAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrNFECabAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrNFECabAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrNFECabAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNFECabAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrNFECabAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrNFECabAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrNFECabAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNFECabAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrNFECabACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrNFECabACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrNFECabACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNFECabACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrNFECabADataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrNFECabAprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrNFECabAretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
    end
    object QrNFECabASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrNFECabASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrNFECabASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrNFECabASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrNFECabASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrNFECabASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrNFECabASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrNFECabASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrNFECabASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrNFECabASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrNFECabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrNFECabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrNFECabAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNFECabAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNFECabAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrNFECabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNFECabAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNFECabAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNFECabABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrNFECabAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFECabANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrNFECabAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFECabAide_dhEmi: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhEmi'
      Calculated = True
    end
    object QrNFECabAide_dhSaiEnt: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhSaiEnt'
      Calculated = True
    end
    object QrNFECabAide_idDest: TSmallintField
      FieldName = 'ide_idDest'
    end
    object QrNFECabAide_indFinal: TSmallintField
      FieldName = 'ide_indFinal'
    end
    object QrNFECabAide_indPres: TSmallintField
      FieldName = 'ide_indPres'
    end
    object QrNFECabAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
    end
    object QrNFECabAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrNFECabAdest_indIEDest: TSmallintField
      FieldName = 'dest_indIEDest'
    end
    object QrNFECabAdest_IM: TWideStringField
      FieldName = 'dest_IM'
      Size = 15
    end
    object QrNFECabAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
    end
    object QrNFECabAEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrNFECabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrNFECabAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrNFECabAide_dhSaiEntTZD: TFloatField
      FieldName = 'ide_dhSaiEntTZD'
    end
    object QrNFECabAInfCpl_totTrib: TWideStringField
      FieldName = 'InfCpl_totTrib'
      Size = 255
    end
    object QrNFECabAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
    end
    object QrNFECabAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
    end
    object QrNFECabAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
    end
    object QrNFECabAExporta_XLocDespacho: TWideStringField
      FieldName = 'Exporta_XLocDespacho'
      Size = 60
    end
    object QrNFECabAICMSTot_vFCP: TFloatField
      FieldName = 'ICMSTot_vFCP'
    end
    object QrNFECabAICMSTot_vFCPST: TFloatField
      FieldName = 'ICMSTot_vFCPST'
    end
    object QrNFECabAICMSTot_vFCPSTRet: TFloatField
      FieldName = 'ICMSTot_vFCPSTRet'
    end
    object QrNFECabAICMSTot_vIPIDevol: TFloatField
      FieldName = 'ICMSTot_vIPIDevol'
    end
    object QrNFECabAURL_QrCode: TWideStringField
      FieldName = 'URL_QrCode'
      Size = 255
    end
    object QrNFECabAURL_Consulta: TWideStringField
      FieldName = 'URL_Consulta'
      Size = 255
    end
    object QrNFECabACNPJCPFAvulso: TWideStringField
      FieldName = 'CNPJCPFAvulso'
      Size = 14
    end
    object QrNFECabARazaoNomeAvulso: TWideStringField
      FieldName = 'RazaoNomeAvulso'
      Size = 60
    end
    object QrNFECabAide_indIntermed: TSmallintField
      FieldName = 'ide_indIntermed'
    end
    object QrNFECabAInfIntermed_CNPJ: TWideStringField
      FieldName = 'InfIntermed_CNPJ'
      Size = 14
    end
    object QrNFECabAInfIntermed_idCadIntTran: TWideStringField
      FieldName = 'InfIntermed_idCadIntTran'
      Size = 60
    end
    object QrNFECabAEmiteAvulso: TSmallintField
      FieldName = 'EmiteAvulso'
    end
  end
  object QrNFeLayI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT layc.Nome NO_Grupo, layi.* '
      'FROM nfelayi layi'
      'LEFT JOIN nfelayc layc ON layc.Grupo=layi.Grupo'
      'WHERE layi.Versao=:P0')
    Left = 28
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLayIGrupo: TWideStringField
      FieldName = 'Grupo'
      Size = 2
    end
    object QrNFeLayICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 11
    end
    object QrNFeLayIID: TWideStringField
      FieldName = 'ID'
      Size = 7
    end
    object QrNFeLayICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrNFeLayIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeLayIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrNFeLayIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeLayITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrNFeLayIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrNFeLayIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrNFeLayITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrNFeLayITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrNFeLayITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrNFeLayIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrNFeLayIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeLayILeftZeros: TSmallintField
      FieldName = 'LeftZeros'
    end
    object QrNFeLayIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrNFeLayINO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 100
    end
    object QrNFeLayIFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrNFeLayIDtIniProd: TDateField
      FieldName = 'DtIniProd'
    end
    object QrNFeLayIDtIniHomo: TDateField
      FieldName = 'DtIniHomo'
    end
  end
  object QrNFEItsN: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 108
    Top = 576
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsNFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsNFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsNEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFEItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNFEItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNFEItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNFEItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNFEItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNFEItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNFEItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNFEItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNFEItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNFEItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNFEItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNFEItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNFEItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrNFEItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrNFEItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrNFEItsNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrNFEItsNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
    end
    object QrNFEItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrNFEItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
    end
    object QrNFEItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
    end
    object QrNFEItsNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
    end
    object QrNFEItsNICMS_vICMSOp: TFloatField
      FieldName = 'ICMS_vICMSOp'
    end
    object QrNFEItsNICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
    end
    object QrNFEItsNICMS_vICMSDif: TFloatField
      FieldName = 'ICMS_vICMSDif'
    end
    object QrNFEItsNICMS_vBCFCP: TFloatField
      FieldName = 'ICMS_vBCFCP'
    end
    object QrNFEItsNICMS_pFCP: TFloatField
      FieldName = 'ICMS_pFCP'
    end
    object QrNFEItsNICMS_vFCP: TFloatField
      FieldName = 'ICMS_vFCP'
    end
    object QrNFEItsNICMS_vBCFCPST: TFloatField
      FieldName = 'ICMS_vBCFCPST'
    end
    object QrNFEItsNICMS_pFCPST: TFloatField
      FieldName = 'ICMS_pFCPST'
    end
    object QrNFEItsNICMS_vFCPST: TFloatField
      FieldName = 'ICMS_vFCPST'
    end
    object QrNFEItsNICMS_pST: TFloatField
      FieldName = 'ICMS_pST'
    end
    object QrNFEItsNICMS_vBCFCPSTRet: TFloatField
      FieldName = 'ICMS_vBCFCPSTRet'
    end
    object QrNFEItsNICMS_pFCPSTRet: TFloatField
      FieldName = 'ICMS_pFCPSTRet'
    end
    object QrNFEItsNICMS_vFCPSTRet: TFloatField
      FieldName = 'ICMS_vFCPSTRet'
    end
    object QrNFEItsNICMS_vICMSSubstituto: TFloatField
      FieldName = 'ICMS_vICMSSubstituto'
    end
    object QrNFEItsNICMS_pRedBCEfet: TFloatField
      FieldName = 'ICMS_pRedBCEfet'
    end
    object QrNFEItsNICMS_vBCEfet: TFloatField
      FieldName = 'ICMS_vBCEfet'
    end
    object QrNFEItsNICMS_pICMSEfet: TFloatField
      FieldName = 'ICMS_pICMSEfet'
    end
    object QrNFEItsNICMS_vICMSEfet: TFloatField
      FieldName = 'ICMS_vICMSEfet'
    end
    object QrNFEItsNICMS_vICMSSTDeson: TFloatField
      FieldName = 'ICMS_vICMSSTDeson'
    end
    object QrNFEItsNICMS_pFCPDif: TFloatField
      FieldName = 'ICMS_pFCPDif'
    end
    object QrNFEItsNICMS_vFCPDif: TFloatField
      FieldName = 'ICMS_vFCPDif'
    end
    object QrNFEItsNICMS_vFCPEfet: TFloatField
      FieldName = 'ICMS_vFCPEfet'
    end
    object QrNFEItsNICMS_motDesICMSST: TSmallintField
      FieldName = 'ICMS_motDesICMSST'
    end
  end
  object QrNFEItsO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsOFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsOFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsOEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrNFEItsOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrNFEItsOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrNFEItsOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrNFEItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrNFEItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrNFEItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrNFEItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrNFEItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrNFEItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrNFEItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrNFEItsO_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrNFEItsOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFEItsODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFEItsODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFEItsOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFEItsOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFEItsOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFEItsOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNFeTotI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Tem_IPI) Tem_IPI,'
      'SUM(IF(prod_indTot=1, prod_vProd, 0)) prod_vProd, '
      'SUM(prod_vFrete) prod_vFrete,'
      'SUM(prod_vSeg) prod_vSeg,'
      'SUM(prod_vDesc) prod_vDesc,'
      'SUM(prod_vOutro) prod_vOutro '
      'FROM nfeitsi'
      'WHERE EhServico=0 '
      'AND FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 584
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotITem_IPI: TFloatField
      FieldName = 'Tem_IPI'
    end
    object QrNFeTotIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrNFeTotIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrNFeTotIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrNFeTotIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrNFeTotIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
  end
  object QrNFeTotN: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(ICMS_vBC) ICMS_vBC,'
      'SUM(ICMS_vICMS) ICMS_vICMS,'
      'SUM(ICMS_vBCST) ICMS_vBCST,'
      'SUM(ICMS_vICMSST) ICMS_vICMSST,'
      'SUM(ICMSTot_vFCP) ICMSTot_vFCP'
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNFeTotNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNFeTotNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNFeTotNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNFeTotNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
    end
    object QrNFeTotNICMS_vFCP: TFloatField
      FieldName = 'ICMS_vFCP'
    end
  end
  object QrNFeTotO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(IPI_vIPI) IPI_vIPI'
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object QrNFeXMLi: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfexmli'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 348
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeXMLiFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeXMLiFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeXMLiEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeXMLiOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFeXMLiCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrNFeXMLiID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrNFeXMLiValor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
  end
  object QrNFeLEnC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Id, FatID, FatNum'
      'FROM nfecaba'
      'WHERE LoteEnv=:P0'
      '')
    Left = 28
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnCId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeLEnCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeLEnCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
  end
  object QrNFeInut: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM nfeinut'
      'WHERE Empresa=:P0'
      'AND cUF=:P1'
      'AND ano=:P2'
      'AND CNPJ=:P3'
      'AND modelo=:P4'
      'AND Serie=:P5'
      'AND nNFIni=:P6'
      'AND nNFFim=:P7')
    Left = 348
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end>
    object QrNFeInutCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrVolumes: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Quantidade qVol, Especie esp, Marca marca, '
      'Numero nVol, kgLiqui pesoL, kgBruto pesoB '
      'FROM stqmovnfsa'
      'WHERE Tipo=1'
      'AND OriCodi=21'
      'AND Empresa=-11')
    Left = 432
    Top = 484
    object QrVolumesqVol: TWideStringField
      FieldName = 'qVol'
      Size = 30
    end
    object QrVolumesesp: TWideStringField
      FieldName = 'esp'
      Size = 30
    end
    object QrVolumesmarca: TWideStringField
      FieldName = 'marca'
      Size = 30
    end
    object QrVolumesnVol: TWideStringField
      FieldName = 'nVol'
      Size = 30
    end
    object QrVolumespesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrVolumespesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object QrFatYIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatID, FatNum, Vencimento, Credito Valor, '
      'FatParcela, Duplicata, Controle, Sub'
      'FROM lanctos'
      'WHERE FatID = 1'
      'AND FatNum=21'
      'AND CliInt=-1')
    Left = 432
    Top = 532
    object QrFatYItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFatYItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrFatYItsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrFatYItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrFatYItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFatYItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrFatYItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFatYItsSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrFatYTot: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatID, FatNum, SUM(Credito) Valor'
      'FROM lanctos'
      'WHERE FatID = 1'
      'AND FatNum=21'
      'AND CliInt=-1'
      'GROUP BY CliInt')
    Left = 348
    Top = 432
    object QrFatYTotFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFatYTotFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrFatYTotValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrNFECabG: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabg'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 112
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabGentrega_CNPJ: TWideStringField
      FieldName = 'entrega_CNPJ'
      Size = 14
    end
    object QrNFECabGentrega_xLgr: TWideStringField
      FieldName = 'entrega_xLgr'
      Size = 60
    end
    object QrNFECabGentrega_nro: TWideStringField
      FieldName = 'entrega_nro'
      Size = 60
    end
    object QrNFECabGentrega_xCpl: TWideStringField
      FieldName = 'entrega_xCpl'
      Size = 60
    end
    object QrNFECabGentrega_xBairro: TWideStringField
      FieldName = 'entrega_xBairro'
      Size = 60
    end
    object QrNFECabGentrega_cMun: TIntegerField
      FieldName = 'entrega_cMun'
    end
    object QrNFECabGentrega_xMun: TWideStringField
      FieldName = 'entrega_xMun'
      Size = 60
    end
    object QrNFECabGentrega_UF: TWideStringField
      FieldName = 'entrega_UF'
      Size = 2
    end
    object QrNFECabGentrega_CPF: TWideStringField
      FieldName = 'entrega_CPF'
      Size = 11
    end
    object QrNFECabGentrega_xNome: TWideStringField
      FieldName = 'entrega_xNome'
      Size = 60
    end
    object QrNFECabGentrega_CEP: TIntegerField
      FieldName = 'entrega_CEP'
    end
    object QrNFECabGentrega_cPais: TIntegerField
      FieldName = 'entrega_cPais'
    end
    object QrNFECabGentrega_xPais: TWideStringField
      FieldName = 'entrega_xPais'
      Size = 60
    end
    object QrNFECabGentrega_fone: TWideStringField
      FieldName = 'entrega_fone'
      Size = 14
    end
    object QrNFECabGentrega_email: TWideStringField
      FieldName = 'entrega_email'
      Size = 60
    end
    object QrNFECabGentrega_IE: TWideStringField
      FieldName = 'entrega_IE'
      Size = 14
    end
  end
  object QrNFECabB: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 112
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabBQualNFref: TSmallintField
      FieldName = 'QualNFref'
    end
    object QrNFECabBrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrNFECabBrefNFeSig: TWideStringField
      FieldName = 'refNFeSig'
      Size = 44
    end
    object QrNFECabBrefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
    end
    object QrNFECabBrefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
    end
    object QrNFECabBrefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Size = 14
    end
    object QrNFECabBrefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
    end
    object QrNFECabBrefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
    end
    object QrNFECabBrefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
    end
    object QrNFECabBrefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
    end
    object QrNFECabBrefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
    end
    object QrNFECabBrefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Size = 14
    end
    object QrNFECabBrefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Size = 11
    end
    object QrNFECabBrefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Size = 14
    end
    object QrNFECabBrefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
    end
    object QrNFECabBrefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
    end
    object QrNFECabBrefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
    end
    object QrNFECabBrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrNFECabBrefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Size = 2
    end
    object QrNFECabBrefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
    end
    object QrNFECabBrefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
    end
  end
  object QrNFECabF: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabf'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 112
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabFretirada_CNPJ: TWideStringField
      FieldName = 'retirada_CNPJ'
      Size = 14
    end
    object QrNFECabFretirada_xLgr: TWideStringField
      FieldName = 'retirada_xLgr'
      Size = 60
    end
    object QrNFECabFretirada_nro: TWideStringField
      FieldName = 'retirada_nro'
      Size = 60
    end
    object QrNFECabFretirada_xCpl: TWideStringField
      FieldName = 'retirada_xCpl'
      Size = 60
    end
    object QrNFECabFretirada_xBairro: TWideStringField
      FieldName = 'retirada_xBairro'
      Size = 60
    end
    object QrNFECabFretirada_cMun: TIntegerField
      FieldName = 'retirada_cMun'
    end
    object QrNFECabFretirada_xMun: TWideStringField
      FieldName = 'retirada_xMun'
      Size = 60
    end
    object QrNFECabFretirada_UF: TWideStringField
      FieldName = 'retirada_UF'
      Size = 2
    end
    object QrNFECabFretirada_CPF: TWideStringField
      FieldName = 'retirada_CPF'
      Size = 11
    end
    object QrNFECabFretirada_xNome: TWideStringField
      FieldName = 'retirada_xNome'
      Size = 60
    end
    object QrNFECabFretirada_CEP: TIntegerField
      FieldName = 'retirada_CEP'
    end
    object QrNFECabFretirada_cPais: TIntegerField
      FieldName = 'retirada_cPais'
    end
    object QrNFECabFretirada_xPais: TWideStringField
      FieldName = 'retirada_xPais'
      Size = 60
    end
    object QrNFECabFretirada_fone: TWideStringField
      FieldName = 'retirada_fone'
      Size = 14
    end
    object QrNFECabFretirada_email: TWideStringField
      FieldName = 'retirada_email'
      Size = 60
    end
    object QrNFECabFretirada_IE: TWideStringField
      FieldName = 'retirada_IE'
      Size = 14
    end
  end
  object QrNFECabXReb: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxreb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabXRebplaca: TWideStringField
      FieldName = 'placa'
      Size = 8
    end
    object QrNFECabXRebUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNFECabXRebRNTC: TWideStringField
      FieldName = 'RNTC'
    end
  end
  object QrNFECabXVol: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrNFECabXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFECabXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFECabXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFECabXVolpesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrNFECabXVolpesoB: TFloatField
      FieldName = 'pesoB'
    end
    object QrNFECabXVolControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrNFECabXLac: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxlac'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3')
    Left = 276
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFECabXLacnLacre: TWideStringField
      FieldName = 'nLacre'
      Size = 60
    end
  end
  object QrNFECabY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabYnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrNFECabYdVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrNFECabYvDup: TFloatField
      FieldName = 'vDup'
    end
    object QrNFECabYFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrCustomiz_: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT gg1.SiglaCustm'
      'FROM pedivdacuz pvc'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pvc.Controle=30'
      'AND gg1.SiglaCustm <> '#39#39)
    Left = 348
    Top = 240
    object QrCustomiz_SiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
  end
  object QrNFEItsV: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 188
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrNFEInfCuz: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Nome '
      'FROM nfeinfcuz'
      'WHERE Codigo=:P0'
      '')
    Left = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFEInfCuzNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM FisRegCad'
      'WHERE Codigo=:P0')
    Left = 348
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFisRegCadICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
    end
    object QrFisRegCadIPI_Usa: TSmallintField
      FieldName = 'IPI_Usa'
    end
    object QrFisRegCadPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
    end
    object QrFisRegCadCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
    end
    object QrFisRegCadIR_Usa: TSmallintField
      FieldName = 'IR_Usa'
    end
    object QrFisRegCadIR_Alq: TFloatField
      FieldName = 'IR_Alq'
    end
    object QrFisRegCadCS_Usa: TSmallintField
      FieldName = 'CS_Usa'
    end
    object QrFisRegCadCS_Alq: TFloatField
      FieldName = 'CS_Alq'
    end
    object QrFisRegCadISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
    end
    object QrFisRegCadISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
    end
    object QrFisRegCadide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrFisRegCadII_Usa: TSmallintField
      FieldName = 'II_Usa'
    end
  end
  object QrAliq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, fru.* '
      'FROM fisregufs fru'
      'LEFT JOIN fisregcad frc ON frc.Codigo=fru.Codigo'
      'WHERE fru.Codigo=:P0'
      'AND fru.Interno=:P1'
      'AND fru.Contribui=:P2'
      'AND fru.Proprio=:P3'
      'AND fru.UFEmit=:P4'
      'AND fru.UFDest=:P5')
    Left = 352
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrAliqICMSAliq: TFloatField
      FieldName = 'ICMSAliq'
    end
    object QrAliqICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
    end
    object QrAliqPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
    end
    object QrAliqCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
    end
    object QrAliqCST_B: TWideStringField
      FieldName = 'CST_B'
      Size = 2
    end
    object QrAliqpRedBC: TFloatField
      FieldName = 'pRedBC'
    end
    object QrAliqmodBC: TSmallintField
      FieldName = 'modBC'
    end
    object QrAliqpBCUFDest: TFloatField
      FieldName = 'pBCUFDest'
    end
    object QrAliqpFCPUFDest: TFloatField
      FieldName = 'pFCPUFDest'
    end
    object QrAliqpICMSUFDest: TFloatField
      FieldName = 'pICMSUFDest'
    end
    object QrAliqpICMSInter: TFloatField
      FieldName = 'pICMSInter'
    end
    object QrAliqpICMSInterPart: TFloatField
      FieldName = 'pICMSInterPart'
    end
    object QrAliqUsaInterPartLei: TSmallintField
      FieldName = 'UsaInterPartLei'
    end
    object QrAliqcBenef: TWideStringField
      FieldName = 'cBenef'
      Size = 10
    end
    object QrAliqpDif: TFloatField
      FieldName = 'pDif'
    end
    object QrAliqCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 3
    end
  end
  object QrServico_: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM grasrv1'
      'WHERE Codigo=:P0'
      ''
      '')
    Left = 728
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrServico_infAdProd: TWideStringField
      FieldName = 'infAdProd'
      Size = 255
    end
    object QrServico_cListServ: TIntegerField
      FieldName = 'cListServ'
    end
    object QrServico_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrServico_CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrServico_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrServico_PrecoPc: TFloatField
      FieldName = 'PrecoPc'
    end
    object QrServico_PrecoM2: TFloatField
      FieldName = 'PrecoM2'
    end
    object QrServico_Precokg: TFloatField
      FieldName = 'Precokg'
    end
  end
  object QrNFeTotU: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(ISSQN_vBC) ISSQN_vBC, '
      'SUM(ISSQN_vISSQN) ISSQN_vISSQN'
      'FROM nfeitsu'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotUISSQN_vBC: TFloatField
      FieldName = 'ISSQN_vBC'
    end
    object QrNFeTotUISSQN_vISSQN: TFloatField
      FieldName = 'ISSQN_vISSQN'
    end
  end
  object QrNFEItsQ: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsQFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsQFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsQEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsQnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrNFEItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrNFEItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrNFEItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrNFEItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrNFEItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
  end
  object QrNFEItsS: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsSFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsSFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsSnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrNFEItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrNFEItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrNFEItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrNFEItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrNFEItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object QrNFEItsU: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsu'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsUFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsUFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsUEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsUnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsUISSQN_vBC: TFloatField
      FieldName = 'ISSQN_vBC'
    end
    object QrNFEItsUISSQN_vAliq: TFloatField
      FieldName = 'ISSQN_vAliq'
    end
    object QrNFEItsUISSQN_vISSQN: TFloatField
      FieldName = 'ISSQN_vISSQN'
    end
    object QrNFEItsUISSQN_cMunFG: TIntegerField
      FieldName = 'ISSQN_cMunFG'
    end
    object QrNFEItsUISSQN_cListServ: TWideStringField
      FieldName = 'ISSQN_cListServ'
      Size = 5
    end
    object QrNFEItsUISSQN_cSitTrib: TWideStringField
      FieldName = 'ISSQN_cSitTrib'
      Size = 1
    end
    object QrNFEItsUISSQN_vDeducao: TFloatField
      FieldName = 'ISSQN_vDeducao'
    end
    object QrNFEItsUISSQN_vOutro: TFloatField
      FieldName = 'ISSQN_vOutro'
    end
    object QrNFEItsUISSQN_vDescIncond: TFloatField
      FieldName = 'ISSQN_vDescIncond'
    end
    object QrNFEItsUISSQN_vDescCond: TFloatField
      FieldName = 'ISSQN_vDescCond'
    end
    object QrNFEItsUISSQN_vISSRet: TFloatField
      FieldName = 'ISSQN_vISSRet'
    end
    object QrNFEItsUISSQN_indISS: TSmallintField
      FieldName = 'ISSQN_indISS'
    end
    object QrNFEItsUISSQN_cServico: TWideStringField
      FieldName = 'ISSQN_cServico'
    end
    object QrNFEItsUISSQN_cMun: TIntegerField
      FieldName = 'ISSQN_cMun'
    end
    object QrNFEItsUISSQN_cPais: TIntegerField
      FieldName = 'ISSQN_cPais'
    end
    object QrNFEItsUISSQN_nProcesso: TWideStringField
      FieldName = 'ISSQN_nProcesso'
      Size = 30
    end
    object QrNFEItsUISSQN_indIncentivo: TSmallintField
      FieldName = 'ISSQN_indIncentivo'
    end
  end
  object QrDTB_Munici: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM dtb_munici'
      'ORDER BY Nome'
      '')
    Left = 428
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 508
  end
  object QrNFeTotP: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(II_vII) II_vII '
      'FROM nfeitsp'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotPII_vII: TFloatField
      FieldName = 'II_vII'
    end
  end
  object QrNFeTotQ: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(PIS_vPIS) PIS_vPIS'
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
  end
  object QrNFeTotS: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(COFINS_vCOFINS) COFINS_vCOFINS'
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 584
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object QrGraSrv1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM grasrv1'
      'ORDER BY Nome'
      '')
    Left = 432
    Top = 96
    object QrGraSrv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraSrv1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraSrv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraSrv1infAdProd: TWideStringField
      FieldName = 'infAdProd'
      Size = 255
    end
    object QrGraSrv1cListServ: TIntegerField
      FieldName = 'cListServ'
    end
  end
  object DsGraSrv1: TDataSource
    DataSet = QrGraSrv1
    Left = 508
    Top = 96
  end
  object QrGraGruX: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT  ggx.Controle GraGruX, gg1.Nome NO_GG1, '
      'IF(gcc.PrintCor=1,gcc.Nome,"") NO_COR, '
      'IF(gtc.PrintTam=1,gti.Nome,"") NO_TAM,'
      'CONCAT(gg1.Nome, '
      '  IF(gtc.PrintTam=1, CONCAT(" ", gti.Nome),""),'
      '  IF(gcc.PrintCor=1, CONCAT(" ", gcc.Nome),"")) NO_GGX '
      'FROM gragrux ggx'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruc'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY NO_GGX')
    Left = 432
    Top = 144
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 10
    end
    object QrGraGruXNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 67
    end
  end
  object DsGraGRuX: TDataSource
    DataSet = QrGraGruX
    Left = 508
    Top = 144
  end
  object QrLocFisRegCad: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM fisregcad'
      'WHERE Codigo=:P0'
      '')
    Left = 604
    Top = 488
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocFisRegCadICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
    end
    object QrLocFisRegCadICMS_Alq: TFloatField
      FieldName = 'ICMS_Alq'
    end
    object QrLocFisRegCadICMS_Frm: TWideMemoField
      FieldName = 'ICMS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadIPI_Usa: TSmallintField
      FieldName = 'IPI_Usa'
    end
    object QrLocFisRegCadIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrLocFisRegCadIPI_Frm: TWideMemoField
      FieldName = 'IPI_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
    end
    object QrLocFisRegCadPIS_Alq: TFloatField
      FieldName = 'PIS_Alq'
    end
    object QrLocFisRegCadPIS_Frm: TWideMemoField
      FieldName = 'PIS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
    end
    object QrLocFisRegCadCOFINS_Alq: TFloatField
      FieldName = 'COFINS_Alq'
    end
    object QrLocFisRegCadCOFINS_Frm: TWideMemoField
      FieldName = 'COFINS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadIR_Usa: TSmallintField
      FieldName = 'IR_Usa'
    end
    object QrLocFisRegCadIR_Alq: TFloatField
      FieldName = 'IR_Alq'
    end
    object QrLocFisRegCadIR_Frm: TWideMemoField
      FieldName = 'IR_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadCS_Usa: TSmallintField
      FieldName = 'CS_Usa'
    end
    object QrLocFisRegCadCS_Alq: TFloatField
      FieldName = 'CS_Alq'
    end
    object QrLocFisRegCadCS_Frm: TWideMemoField
      FieldName = 'CS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocFisRegCadISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
    end
    object QrLocFisRegCadISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
    end
    object QrLocFisRegCadISS_Frm: TWideMemoField
      FieldName = 'ISS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsDTB_UFs: TDataSource
    DataSet = QrDTB_UFs
    Left = 508
    Top = 192
  end
  object QrDTB_UFs: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DTB, Nome'
      'FROM ufs'
      'ORDER BY Nome')
    Left = 432
    Top = 192
    object QrDTB_UFsDTB: TWideStringField
      FieldName = 'DTB'
      Size = 2
    end
    object QrDTB_UFsNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
  end
  object QrDTBMunici1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM DTB_Munici')
    Left = 432
    Top = 240
    object QrDTBMunici1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTBMunici1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTBMunici1: TDataSource
    DataSet = QrDTBMunici1
    Left = 508
    Top = 240
  end
  object DsBACEN_Pais: TDataSource
    DataSet = QrBACEN_Pais
    Left = 508
    Top = 288
  end
  object QrBACEN_Pais: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY NOme')
    Left = 432
    Top = 288
    object QrBACEN_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBACEN_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrnNFDupl: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfecaba nfea'
      'WHERE Empresa=:P0'
      'AND ide_serie=:P1'
      'AND ide_nNF=:P2')
    Left = 348
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrnNFInut: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeinut'
      'WHERE Empresa=:P0 '
      'AND Serie=:P1'
      'AND :P2 BETWEEN nNFIni AND nNFFim'
      '')
    Left = 348
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrRefer1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT inn.DataE, inn.NFInn, inn.GraGruX, '
      'inn.TipoNF, inn.refNFe, inn.modNF, inn.Serie, '
      'its.OutQtdkg, "kg" No_Peso, its.OutQtdPc, '
      '"Pe'#231'as" NO_Pecas, its.OutQtdM2, "m2" NO_Area,'
      'OutQtdVal, Benefikg, BenefiPc, BenefiM2, '
      'inn.Cliente, IF(cli.Tipo=0, cli.CNPJ, cli.CPF)'
      'CNPJ_CPF, ufs.DTB DTB_UF'
      'FROM CMPToutits its'
      'LEFT JOIN cmptinn inn ON inn.Codigo=its.CMPTInn'
      'LEFT JOIN entidades cli ON cli.Codigo=inn.Cliente'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(cli.Tipo=0, EUF, PUF)'
      'WHERE its.Codigo>0')
    Left = 664
    Top = 96
    object QrRefer1DataE: TDateField
      FieldName = 'DataE'
    end
    object QrRefer1NFInn: TIntegerField
      FieldName = 'NFInn'
    end
    object QrRefer1GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrRefer1TipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrRefer1refNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrRefer1modNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrRefer1Serie: TIntegerField
      FieldName = 'Serie'
    end
    object QrRefer1OutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
    end
    object QrRefer1No_Peso: TWideStringField
      FieldName = 'No_Peso'
      Required = True
      Size = 2
    end
    object QrRefer1OutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
    end
    object QrRefer1NO_Pecas: TWideStringField
      FieldName = 'NO_Pecas'
      Required = True
      Size = 5
    end
    object QrRefer1OutQtdM2: TFloatField
      FieldName = 'OutQtdM2'
    end
    object QrRefer1NO_Area: TWideStringField
      FieldName = 'NO_Area'
      Required = True
      Size = 2
    end
    object QrRefer1OutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
    end
    object QrRefer1Benefikg: TFloatField
      FieldName = 'Benefikg'
    end
    object QrRefer1BenefiPc: TFloatField
      FieldName = 'BenefiPc'
    end
    object QrRefer1BenefiM2: TFloatField
      FieldName = 'BenefiM2'
    end
    object QrRefer1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrRefer1CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrRefer1DTB_UF: TWideStringField
      FieldName = 'DTB_UF'
      Required = True
      Size = 2
    end
  end
  object QrProds1_: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT med.Sigla NO_UNIDADE,                                    ' +
        '    '
      
        'smva.Empresa, smva.Preco, smva.GraGruX CU_PRODUTO,              ' +
        '    '
      
        'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,             ' +
        '    '
      
        'SUM(smva.Total) Total, SUM(smva.Qtde) Qtde, smva.CFOP,          ' +
        '    '
      
        'smva.CFOP_Contrib, smva.CFOP_MesmaUF, smva.CFOP_Proprio,        ' +
        '    '
      
        'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         ' +
        '    '
      
        'gg1.CST_A, gg1.CST_B,                                           ' +
        '    '
      
        'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              ' +
        '    '
      
        'gg1.NCM, ncm.Letras, gg1.IPI_CST, gg1.IPI_cEnq,                 ' +
        '    '
      
        'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              ' +
        '    '
      
        'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,              ' +
        '    '
      
        'gg1.InfAdProd, smva.MedidaC, smva.MedidaL, smva.MedidaA,        ' +
        '    '
      
        'smva.MedidaE, mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4' +
        ',   '
      
        'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4, gta.PrintTam,   ' +
        '    '
      
        'gg1.PIS_CST, gg1.PIS_AlqV, gg1.PISST_AlqV, gg1.COFINS_CST,      ' +
        '    '
      
        'gg1.COFINS_AlqP, gg1.COFINS_AlqV, gg1.COFINSST_AlqP,            ' +
        '    '
      
        'gg1.COFINSST_AlqV, gg1.ICMS_modBC, gg1.ICMS_modBCST,            ' +
        '    '
      
        'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST, gg1.ICMS_pMVAST,            ' +
        '    '
      
        'gg1.ICMS_pICMSST,  gg1.IPI_vUnid, gg1.IPI_TpTrib,               ' +
        '    '
      
        'gg1.IPI_pIPI, gg1.ICMS_Pauta, gg1.ICMS_MaxTab,                  ' +
        '    '
      
        'gg1.PIS_pRedBC, gg1.PISST_pRedBCST,                             ' +
        '    '
      
        'gg1.COFINS_pRedBC, gg1.COFINSST_pRedBCST,                       ' +
        '    '
      
        'gg1.cGTIN_EAN, gg1.EX_TIPI, gg1.PISST_AlqP, gg1.PIS_AlqP,       ' +
        '    '
      
        'smva.Servico, gcc.PrintCor, smva.prod_indTot,                   ' +
        '    '
      
        'smva.CSOSN, smva.pCredSN                                        ' +
        '    '
      
        'FROM stqmovvala smva                                            ' +
        '    '
      
        'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              ' +
        '    '
      
        'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               ' +
        '    '
      
        'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             ' +
        '    '
      
        'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             ' +
        '    '
      
        'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             ' +
        '    '
      
        'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo                ' +
        '    '
      
        'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           ' +
        '    '
      
        'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem              ' +
        '    '
      
        'LEFT JOIN gragrux ggx1 ON ggx1.Controle=smva.RefProd            ' +
        '    '
      
        'LEFT JOIN gragru1 gg11 ON gg11.Nivel1=ggx1.GraGru1              ' +
        '    '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'GROUP BY smva.Preco,  smva.GraGruX'
      '')
    Left = 728
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object StringField1: TWideStringField
      FieldName = 'NO_UNIDADE'
      Origin = 'unidmed.Sigla'
      Size = 3
    end
    object IntegerField1: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovvala.Empresa'
      Required = True
    end
    object FloatField1: TFloatField
      FieldName = 'Preco'
      Origin = 'stqmovvala.Preco'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'CU_PRODUTO'
      Origin = 'stqmovvala.GraGruX'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'CONTROLE'
      Origin = 'stqmovvala.ID'
      Required = True
    end
    object FloatField2: TFloatField
      FieldName = 'Total'
      Origin = 'Total'
    end
    object QrProds1ICMS_Val: TFloatField
      FieldName = 'ICMS_Val'
      Origin = 'ICMS_Val'
    end
    object QrProds1IPI_Val: TFloatField
      FieldName = 'IPI_Val'
      Origin = 'IPI_Val'
    end
    object FloatField3: TFloatField
      FieldName = 'Qtde'
      Origin = 'Qtde'
    end
    object IntegerField4: TIntegerField
      FieldName = 'CU_GRUPO'
      Origin = 'gragru1.CodUsu'
    end
    object StringField2: TWideStringField
      FieldName = 'NO_GRUPO'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object SmallintField1: TSmallintField
      FieldName = 'CST_A'
      Origin = 'gragru1.CST_A'
    end
    object SmallintField2: TSmallintField
      FieldName = 'CST_B'
      Origin = 'gragru1.CST_B'
    end
    object StringField3: TWideStringField
      FieldName = 'CST_T'
      Origin = 'CST_T'
      Size = 8
    end
    object StringField4: TWideStringField
      FieldName = 'NCM'
      Origin = 'gragru1.NCM'
      Size = 10
    end
    object StringField5: TWideStringField
      FieldName = 'Letras'
      Origin = 'ncms.Letras'
      Size = 5
    end
    object IntegerField5: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object IntegerField6: TIntegerField
      FieldName = 'CU_COR'
      Origin = 'gracorcad.CodUsu'
    end
    object StringField6: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object StringField7: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'gratamits.Nome'
      Size = 10
    end
    object IntegerField7: TIntegerField
      FieldName = 'CO_GRUPO'
      Origin = 'gragrux.GraGru1'
    end
    object LargeintField1: TLargeintField
      FieldName = 'Desc_Per'
      Required = True
    end
    object StringField8: TWideStringField
      FieldName = 'CFOP'
      Origin = 'stqmovvala.CFOP'
      Size = 6
    end
    object StringField9: TWideStringField
      FieldName = 'IPI_CST'
      Origin = 'gragru1.IPI_CST'
      Size = 2
    end
    object StringField10: TWideStringField
      FieldName = 'IPI_cEnq'
      Origin = 'gragru1.IPI_cEnq'
      Size = 3
    end
    object StringField11: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object IntegerField8: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object FloatField4: TFloatField
      FieldName = 'PercCustom'
    end
    object FloatField5: TFloatField
      FieldName = 'MedidaC'
    end
    object FloatField6: TFloatField
      FieldName = 'MedidaL'
    end
    object FloatField7: TFloatField
      FieldName = 'MedidaA'
    end
    object FloatField8: TFloatField
      FieldName = 'MedidaE'
    end
    object StringField12: TWideStringField
      FieldName = 'Medida1'
    end
    object StringField13: TWideStringField
      FieldName = 'Medida2'
    end
    object StringField14: TWideStringField
      FieldName = 'Medida3'
    end
    object StringField15: TWideStringField
      FieldName = 'Medida4'
    end
    object StringField16: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object StringField17: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object StringField18: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object StringField19: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object SmallintField3: TSmallintField
      FieldName = 'PrintTam'
    end
    object StringField20: TWideStringField
      FieldName = 'PIS_CST'
      Size = 2
    end
    object FloatField9: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object FloatField10: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object StringField21: TWideStringField
      FieldName = 'COFINS_CST'
      Size = 2
    end
    object FloatField11: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object FloatField12: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object FloatField13: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object FloatField14: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object SmallintField4: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object SmallintField5: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object FloatField15: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object FloatField16: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object FloatField17: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object FloatField18: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object SmallintField6: TSmallintField
      FieldName = 'CFOP_Contrib'
    end
    object SmallintField7: TSmallintField
      FieldName = 'CFOP_MesmaUF'
    end
    object SmallintField8: TSmallintField
      FieldName = 'CFOP_Proprio'
    end
    object FloatField19: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object SmallintField9: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object IntegerField9: TIntegerField
      FieldName = 'Servico'
    end
  end
  object QrListServ: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 432
    Top = 336
    object QrListServCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 4
    end
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrListServDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 508
    Top = 336
  end
  object QrParcela: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Max(FatParcela) FatParcela'
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 664
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrParcelaFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrServi1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT med.Sigla, gs1.Codigo, gs1.Nome, gs1.infAdProd, gs1.cList' +
        'Serv, gs1.UnidMed '
      'FROM grasrv1 gs1'
      'LEFT JOIN unidmed med ON med.Codigo=gs1.UnidMed '
      'WHERE gs1.Codigo=:P0')
    Left = 712
    Top = 416
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrServi1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrServi1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrServi1infAdProd: TWideStringField
      FieldName = 'infAdProd'
      Size = 255
    end
    object QrServi1cListServ: TIntegerField
      FieldName = 'cListServ'
    end
    object QrServi1Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrServi1UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrNFEItsP: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsp'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsPFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsPFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsPEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsPnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsPII_vBC: TFloatField
      FieldName = 'II_vBC'
    end
    object QrNFEItsPII_vDespAdu: TFloatField
      FieldName = 'II_vDespAdu'
    end
    object QrNFEItsPII_vII: TFloatField
      FieldName = 'II_vII'
    end
    object QrNFEItsPII_vIOF: TFloatField
      FieldName = 'II_vIOF'
    end
  end
  object QrSumRetFis: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(prod_vProd) prod_vProd, '
      ''
      'SUM(ICMSRec_vBC) ICMSRec_vBC,'
      
        'SUM(ICMSRec_pRedBC * ICMSRec_vBC) / SUM(ICMSRec_vBC) ICMSRec_pRe' +
        'dBC,'
      
        'SUM(ICMSRec_pAliq * ICMSRec_vBC) / SUM(ICMSRec_vBC) ICMSRec_pAli' +
        'q,'
      'SUM(ICMSRec_vICMS) ICMSRec_vICMS,'
      ''
      'SUM(IPIRec_vBC) IPIRec_vBC,'
      'SUM(IPIRec_pRedBC * IPIRec_vBC) / SUM(IPIRec_vBC) IPIRec_pRedBC,'
      'SUM(IPIRec_pAliq * IPIRec_vBC) / SUM(IPIRec_vBC) IPIRec_pAliq,'
      'SUM(IPIRec_vIPI) IPIRec_vIPI,'
      ''
      'SUM(PISRec_vBC) PISRec_vBC,'
      'SUM(PISRec_pRedBC * PISRec_vBC) / SUM(PISRec_vBC) PISRec_pRedBC,'
      'SUM(PISRec_pAliq * PISRec_vBC) / SUM(PISRec_vBC) PISRec_pAliq,'
      'SUM(PISRec_vPIS) PISRec_vPIS,'
      ''
      'SUM(COFINSRec_vBC) COFINSRec_vBC,'
      
        'SUM(COFINSRec_pRedBC * COFINSRec_vBC) / SUM(COFINSRec_vBC) COFIN' +
        'SRec_pRedBC,'
      
        'SUM(COFINSRec_pAliq * COFINSRec_vBC) / SUM(COFINSRec_vBC) COFINS' +
        'Rec_pAliq,'
      'SUM(COFINSRec_vCOFINS) COFINSRec_vCOFINS'
      ''
      'FROM nfeitsi'
      ''
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 728
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSumRetFisprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrSumRetFisICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrSumRetFisICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrSumRetFisICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrSumRetFisICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrSumRetFisIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrSumRetFisIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrSumRetFisIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrSumRetFisIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrSumRetFisPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrSumRetFisPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrSumRetFisPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrSumRetFisPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrSumRetFisCOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrSumRetFisCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrSumRetFisCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrSumRetFisCOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
  end
  object QrEnt_Doc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE CNPJ=:P0'
      'OR CPF=:P1')
    Left = 728
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEnt_DocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocNFe: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatNum, DataFiscal'
      'FROM nfecaba'
      'WHERE Id=:P0')
    Left = 728
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocNFeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocNFeDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrLocNFeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocNFeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrEmb: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.ICMSRec_pRedBC, gg1.IPIRec_pRedBC,'
      'gg1.PISRec_pRedBC, gg1.COFINSRec_pRedBC'
      'FROM gragrue gge '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gge.Nivel1'
      'WHERE gge.Fornece=:P0'
      'AND gge.cProd=:P1'
      ''
      '')
    Left = 664
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmbNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrEmbICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrEmbIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrEmbPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrEmbCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
  end
  object QrCod: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, ggx.Controle GraGruX,'
      
        'gg1.ICMSRec_pRedBC, gg1.IPIRec_pRedBC, gg1.PISRec_pRedBC, gg1.CO' +
        'FINSRec_pRedBC,'
      
        'gg1.ICMSRec_pAliq, gg1.IPIRec_pAliq, gg1.PISRec_pAliq, gg1.COFIN' +
        'SRec_pAliq,'
      
        'gg1.ICMSRec_tCalc, gg1.IPIRec_tCalc, gg1.PISRec_tCalc, gg1.COFIN' +
        'SRec_tCalc'
      'FROM gragrueits gge'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gge.Nivel1'
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1'
      'WHERE gge.Fornece=:P0'
      'AND gge.cProd=:P1'
      '')
    Left = 664
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCodNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrCodICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrCodIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrCodPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrCodCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrCodICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrCodIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrCodPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrCodCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrCodICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object QrCodIPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object QrCodPISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object QrCodCOFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object QrCodGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCodFatorQtd: TFloatField
      FieldName = 'FatorQtd'
    end
  end
  object QrA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 792
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_fone: TWideStringField
      DisplayWidth = 14
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrACobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrASegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrADataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 792
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
  end
  object QrN: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 792
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object QrO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 792
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
  end
  object QrQ: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 792
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
    object QrQPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrQPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrQPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrQPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrQPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
  end
  object QrS: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 792
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
    object QrSCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrSCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrSCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrSCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrSCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
  end
  object QrXVol: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '*********'
      ' deve-se montar a sql na consulta!'
      '**********'
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND pesoL=:P3')
    Left = 792
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrXVolControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrXVolpesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrXVolpesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object Qr_D_E: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = Qr_D_ECalcFields
    SQL.Strings = (
      'SELECT IDCtrl, FatID, FatNum, Empresa, '
      'CodInfoDest, CodInfoEmit,'
      'emit_CNPJ, emit_CPF, dest_CNPJ, dest_CPF'
      'FROM nfecaba'
      'WHERE CodInfoDest = 0 '
      'OR CodInfoEmit=0')
    Left = 792
    Top = 340
    object Qr_D_EIDCtrl: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'IDCtrl'
    end
    object Qr_D_EFatID: TIntegerField
      FieldName = 'FatID'
    end
    object Qr_D_EFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object Qr_D_EEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr_D_ECodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object Qr_D_ECodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object Qr_D_Eemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object Qr_D_Eemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object Qr_D_Edest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object Qr_D_Edest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object Qr_D_EFatID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FatID_TXT'
      Size = 100
      Calculated = True
    end
  end
  object QrEmit: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo,'
      'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, '
      'LUF, l2.Nome NO_LLOGRAD, LCodMunici,'
      'ml.Nome NO_LMunici, ul.Nome NO_LUF,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA, en.IEST'
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN dtb_munici ml ON ml.Codigo=en.LCodMunici'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'LEFT JOIN ufs ul ON ul.Codigo=en.LUF'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      '')
    Left = 32
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmitCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmitCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEmitIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmitRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEmitCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrEmitNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEmitFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrEmitRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmitCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmitBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmitTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEmitNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrEmitNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrEmitNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrEmitNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrEmitSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmitL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrEmitL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object QrEmitLLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object QrEmitLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrEmitLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrEmitLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrEmitLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrEmitLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrEmitLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrEmitNO_LLOGRAD: TWideStringField
      FieldName = 'NO_LLOGRAD'
      Size = 10
    end
    object QrEmitLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrEmitNO_LMunici: TWideStringField
      FieldName = 'NO_LMunici'
      Size = 100
    end
    object QrEmitNO_LUF: TWideStringField
      FieldName = 'NO_LUF'
      Required = True
      Size = 2
    end
    object QrEmitNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmitIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEmitUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrEmitNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEmitCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEmitCodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object QrEmitCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
  end
  object Ds_D_E: TDataSource
    DataSet = Qr_D_E
    Left = 792
    Top = 388
  end
  object QrAtoArq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT caba.IDCtrl'
      'FROM nfecaba caba'
      'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NULL'
      'AND Id <> ""'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAtoArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrNFeArq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, '
      'caba.Id, caba.FatID, caba.FatNum, caba.Empresa'
      'FROM nfecaba caba'
      'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE caba.IDCtrl=:P0')
    Left = 856
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeArqIDCtrl_CabA: TIntegerField
      FieldName = 'IDCtrl_CabA'
    end
    object QrNFeArqIDCtrl_Arq: TIntegerField
      FieldName = 'IDCtrl_Arq'
      Required = True
    end
    object QrNFeArqId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeArqFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeArqFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeArqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrNFeAut: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.LoteEnv, caba.Id'
      'FROM nfecaba caba'
      'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infProt_cStat=100'
      'AND arq.XML_Aut IS NULL'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeAutIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeAutLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNFeAutId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrNFeCan: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.Id'
      'FROM nfecaba caba'
      'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infCanc_cStat=101'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCanIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCanId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrNFeInu: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeinut'
      'WHERE XML_Inu IS NULL'
      'AND cStat=102')
    Left = 856
    Top = 192
    object QrNFeInuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeInuCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeInuNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNFeInuEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeInuDataC: TDateField
      FieldName = 'DataC'
    end
    object QrNFeInuversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeInuId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrNFeInutpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeInucUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeInuano: TSmallintField
      FieldName = 'ano'
    end
    object QrNFeInuCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFeInumodelo: TSmallintField
      FieldName = 'modelo'
    end
    object QrNFeInuSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrNFeInunNFIni: TIntegerField
      FieldName = 'nNFIni'
    end
    object QrNFeInunNFFim: TIntegerField
      FieldName = 'nNFFim'
    end
    object QrNFeInuJustif: TIntegerField
      FieldName = 'Justif'
    end
    object QrNFeInuxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrNFeInucStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeInuxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeInudhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeInunProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeInuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeInuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeInuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeInuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeInuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeInuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeInuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeInuXML_Inu: TWideMemoField
      FieldName = 'XML_Inu'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrArq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 856
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_NFe: TWideMemoField
      FieldName = 'XML_NFe'
      Origin = 'nfearq.XML_NFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object QrGraGruCST: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT ggt.CST_B '
      'FROM gragrucst ggt'
      'LEFT JOIN gragrux ggx ON ggx.GraGRu1=ggt.Nivel1'
      'WHERE ggx.Controle=:P0'
      'AND ggt.UF_Orig=:P1'
      'AND ggt.UF_Dest=:P2')
    Left = 728
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGruCSTCST_B: TSmallintField
      FieldName = 'CST_B'
    end
  end
  object QrNfeItsIDI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsidi nfedi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 112
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNfeItsIDIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfeitsidi.FatID'
    end
    object QrNfeItsIDIFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfeitsidi.FatNum'
    end
    object QrNfeItsIDIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeitsidi.Empresa'
    end
    object QrNfeItsIDInItem: TIntegerField
      FieldName = 'nItem'
      Origin = 'nfeitsidi.nItem'
    end
    object QrNfeItsIDIDI_nDI: TWideStringField
      DisplayWidth = 15
      FieldName = 'DI_nDI'
      Origin = 'nfeitsidi.DI_nDI'
      Size = 15
    end
    object QrNfeItsIDIDI_dDI: TDateField
      FieldName = 'DI_dDI'
      Origin = 'nfeitsidi.DI_dDI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNfeItsIDIDI_xLocDesemb: TWideStringField
      FieldName = 'DI_xLocDesemb'
      Origin = 'nfeitsidi.DI_xLocDesemb'
      Size = 60
    end
    object QrNfeItsIDIDI_UFDesemb: TWideStringField
      FieldName = 'DI_UFDesemb'
      Origin = 'nfeitsidi.DI_UFDesemb'
      Size = 2
    end
    object QrNfeItsIDIDI_dDesemb: TDateField
      FieldName = 'DI_dDesemb'
      Origin = 'nfeitsidi.DI_dDesemb'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNfeItsIDIDI_cExportador: TWideStringField
      FieldName = 'DI_cExportador'
      Origin = 'nfeitsidi.DI_cExportador'
      Size = 60
    end
    object QrNfeItsIDIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'nfeitsidi.Controle'
    end
    object QrNfeItsIDIDI_tpViaTransp: TSmallintField
      FieldName = 'DI_tpViaTransp'
    end
    object QrNfeItsIDIDI_vAFRMM: TFloatField
      FieldName = 'DI_vAFRMM'
    end
    object QrNfeItsIDIDI_tpIntermedio: TSmallintField
      FieldName = 'DI_tpIntermedio'
    end
    object QrNfeItsIDIDI_CNPJ: TWideStringField
      FieldName = 'DI_CNPJ'
      Size = 18
    end
    object QrNfeItsIDIDI_UFTerceiro: TWideStringField
      FieldName = 'DI_UFTerceiro'
      Size = 2
    end
  end
  object QrNfeItsIDIa: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT nfedia.*, nfedia.Adi_nDraw + 0.000 Adi_nDraw '
      'FROM nfeitsidia nfedia'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      'AND Controle=:P4'
      '')
    Left = 112
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrNfeItsIDIaAdi_nAdicao: TIntegerField
      FieldName = 'Adi_nAdicao'
      Origin = 'nfeitsidia.Adi_nAdicao'
    end
    object QrNfeItsIDIaAdi_nSeqAdic: TIntegerField
      FieldName = 'Adi_nSeqAdic'
      Origin = 'nfeitsidia.Adi_nSeqAdic'
    end
    object QrNfeItsIDIaAdi_cFabricante: TWideStringField
      FieldName = 'Adi_cFabricante'
      Origin = 'nfeitsidia.Adi_cFabricante'
      Size = 60
    end
    object QrNfeItsIDIaAdi_vDescDI: TFloatField
      FieldName = 'Adi_vDescDI'
      Origin = 'nfeitsidia.Adi_vDescDI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNfeItsIDIaConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'nfeitsidia.Conta'
    end
    object QrNfeItsIDIaAdi_nDraw: TFloatField
      FieldName = 'Adi_nDraw'
    end
  end
  object QrCNAE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CodTxt, CodAlf, Nome'
      'FROM cnae21cad')
    Left = 432
    Top = 384
    object QrCNAENome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAECodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAECodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
  end
  object DsCNAE: TDataSource
    DataSet = QrCNAE
    Left = 508
    Top = 384
  end
  object QrNFEItsR: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsr'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsRFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsRFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsREmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsRnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrNFEItsRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrNFEItsRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrNFEItsRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrNFEItsRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrNFEItsRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
    object QrNFEItsRPISST_indSomaPISST: TSmallintField
      FieldName = 'PISST_indSomaPISST'
    end
  end
  object QrNFEItsT: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitst'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsTFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsTEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsTnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrNFEItsTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrNFEItsTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrNFEItsTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrNFEItsTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrNFEItsTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
    object QrNFEItsTCOFINSST_indSomaCOFINSST: TSmallintField
      FieldName = 'COFINSST_indSomaCOFINSST'
    end
  end
  object QrExce: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT fru.Nivel,'
      'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN fisregufx fru ON fru.Nivel=1 AND fru.CodNiv=gg1.Nivel1'
      'WHERE fru.Nivel IS NOT NULL'
      'AND fru.Codigo=1'
      'AND fru.Interno=1'
      'AND fru.Contribui=1'
      'AND Proprio=1'
      'AND UFEmit="PR"'
      'AND UFDest="PR"'
      'AND ggx.Controle=369'
      ''
      'UNION'
      ''
      'SELECT fru.Nivel,'
      'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN fisregufx fru ON fru.Nivel=2 AND fru.CodNiv=gg1.Nivel2'
      'WHERE fru.Nivel IS NOT NULL'
      'AND fru.Codigo=1'
      'AND fru.Interno=1'
      'AND fru.Contribui=1'
      'AND Proprio=1'
      'AND UFEmit="PR"'
      'AND UFDest="PR"'
      'AND ggx.Controle=369'
      ''
      'UNION'
      ''
      'SELECT fru.Nivel,'
      'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN fisregufx fru ON fru.Nivel=3 AND fru.CodNiv=gg1.Nivel3'
      'WHERE fru.Nivel IS NOT NULL'
      'AND fru.Codigo=1'
      'AND fru.Interno=1'
      'AND fru.Contribui=1'
      'AND Proprio=1'
      'AND UFEmit="PR"'
      'AND UFDest="PR"'
      'AND ggx.Controle=369'
      ''
      'UNION'
      ''
      'SELECT fru.Nivel,'
      'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN fisregufx fru ON fru.Nivel=4 AND fru.CodNiv=gg1.PrdGru' +
        'pTip'
      'WHERE fru.Nivel IS NOT NULL'
      'AND fru.Codigo=1'
      'AND fru.Interno=1'
      'AND fru.Contribui=1'
      'AND Proprio=1'
      'AND UFEmit="PR"'
      'AND UFDest="PR"'
      'AND ggx.Controle=369'
      ''
      ''
      'ORDER BY Nivel')
    Left = 432
    Top = 432
    object QrExceNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrExceICMSAliq: TFloatField
      FieldName = 'ICMSAliq'
    end
    object QrExceCST_B: TWideStringField
      FieldName = 'CST_B'
      Size = 2
    end
    object QrExcepRedBC: TFloatField
      FieldName = 'pRedBC'
    end
    object QrExcemodBC: TSmallintField
      FieldName = 'modBC'
    end
    object QrExcecBenef: TWideStringField
      FieldName = 'cBenef'
      Size = 10
    end
    object QrExcepDif: TFloatField
      FieldName = 'pDif'
    end
    object QrExceCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 3
    end
  end
  object QrEveTp: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT nSeqEvento '
      'FROM nfeevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND tpEvento=:P3'
      'ORDER BY nSeqEvento DESC'
      ''
      '')
    Left = 928
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEveTpnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
  end
  object QrNFeEveRCab: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento,'
      'Status, ret_cStat, Id'
      'FROM nfeevercab'
      'WHERE EventoLote=:P0')
    Left = 28
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabchNFe_NF_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object QrNFeEveRCabchNFe_NF_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object QrNFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCabId: TWideStringField
      FieldName = 'Id'
      Size = 100
    end
  end
  object QrEveIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle'
      'FROM nfeevercab'
      'WHERE EventoLote=:P0'
      'AND tpEvento=:P1'
      'AND nSeqEvento=:P2')
    Left = 856
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEveItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEveSub: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SubCtrl'
      'FROM nfeeverret'
      'WHERE Controle=:P0'
      'AND ret_Id=:P1')
    Left = 856
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEveSubSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21cad
    Left = 508
    Top = 48
  end
  object QrCNAE21cad: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE LENGTH(CodTxt)=7'
      'ORDER BY Nome'
      '')
    Left = 432
    Top = 48
  end
  object QrLocCabY: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle'
      'FROM lanctos'
      'WHERE FatID=1'
      'AND FatNum=2908'
      'AND FatParcela=3'
      'AND CliInt=-11')
    Left = 728
    Top = 288
    object QrLocCabYControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrNFeItsM: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 108
    Top = 528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsMFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsMEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsMnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsMiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrNFeItsMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFeItsMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsMAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsMAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeItsMpTotTrib: TFloatField
      FieldName = 'pTotTrib'
    end
    object QrNFeItsMtabTotTrib: TSmallintField
      FieldName = 'tabTotTrib'
    end
    object QrNFeItsMverTotTrib: TWideStringField
      FieldName = 'verTotTrib'
      Required = True
      Size = 30
    end
    object QrNFeItsMtpAliqTotTrib: TSmallintField
      FieldName = 'tpAliqTotTrib'
    end
    object QrNFeItsMfontTotTrib: TSmallintField
      FieldName = 'fontTotTrib'
    end
  end
  object QrNFeTotM: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(vTotTrib) vTotTrib'
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFeTotMvBasTrib: TFloatField
      FieldName = 'vBasTrib'
    end
    object QrNFeTotMvTribFed: TFloatField
      FieldName = 'vTribFed'
    end
    object QrNFeTotMvTribEst: TFloatField
      FieldName = 'vTribEst'
    end
    object QrNFeTotMvTribMun: TFloatField
      FieldName = 'vTribMun'
    end
  end
  object QrLocEve: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(*) ITENS'
      'FROM nfeevercab'
      'WHERE chNFe="41130310783968000195550010000001531769037263"'
      'AND tpEvento=110110'
      'AND ret_cStat in (135,136)')
    Left = 28
    Top = 476
    object QrLocEveITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrLoc2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 829
    Top = 508
  end
  object QrLoc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 800
    Top = 508
  end
  object QrNFeCabGA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabga'
      'WHERE FatID=:P0'
      'AND FAtNum=:P1'
      'AND EMpresa=:P2')
    Left = 112
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabGAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabGAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabGAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabGAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabGAautXML_CNPJ: TWideStringField
      FieldName = 'autXML_CNPJ'
      Size = 14
    end
    object QrNFeCabGAautXML_CPF: TWideStringField
      FieldName = 'autXML_CPF'
      Size = 11
    end
    object QrNFeCabGALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeCabGADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeCabGADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeCabGAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeCabGAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeCabGAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeCabGAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeCabGAAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrNFeCabGAAddIDCad: TSmallintField
      FieldName = 'AddIDCad'
    end
    object QrNFeCabGATipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrNFeItsI03: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi03'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 112
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsI03FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsI03FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsI03Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsI03nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsI03Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeItsI03detExport_nDraw: TLargeintField
      FieldName = 'detExport_nDraw'
    end
    object QrNFeItsI03detExport_nRE: TLargeintField
      FieldName = 'detExport_nRE'
    end
    object QrNFeItsI03detExport_chNFe: TWideStringField
      FieldName = 'detExport_chNFe'
      Required = True
      Size = 44
    end
    object QrNFeItsI03detExport_qExport: TFloatField
      FieldName = 'detExport_qExport'
    end
    object QrNFeItsI03Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsI03DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsI03DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsI03UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsI03UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsI03AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsI03Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNFeItsINVE: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsinve'
      'WHERE FatID=:P0'
      'AND FAtNum=:P1'
      'AND EMpresa=:P2'
      'AND nItem=:P3')
    Left = 112
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsINVEFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsINVEFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsINVEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsINVEnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsINVEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeItsINVEImportacia: TIntegerField
      FieldName = 'Importacia'
    end
    object QrNFeItsINVENVE: TWideStringField
      FieldName = 'NVE'
      Size = 6
    end
    object QrNFeItsINVELk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsINVEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsINVEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsINVEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsINVEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsINVEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsINVEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrIBPTax: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM ibptax'
      'WHERE Tabela=0'
      'AND Codigo=5021019'
      'AND Ex=0')
    Left = 28
    Top = 524
    object QrIBPTaxCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrIBPTaxEx: TIntegerField
      FieldName = 'Ex'
    end
    object QrIBPTaxTabela: TIntegerField
      FieldName = 'Tabela'
    end
    object QrIBPTaxNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrIBPTaxDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrIBPTaxAliqNac: TFloatField
      FieldName = 'AliqNac'
    end
    object QrIBPTaxAliqImp: TFloatField
      FieldName = 'AliqImp'
    end
    object QrIBPTaxVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrIBPTaxLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIBPTaxDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIBPTaxDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIBPTaxUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIBPTaxUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIBPTaxAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIBPTaxFonteStr: TWideStringField
      FieldName = 'FonteStr'
      Size = 34
    end
    object QrIBPTaxChave: TWideStringField
      FieldName = 'Chave'
    end
    object QrIBPTaxVigenIni: TDateField
      FieldName = 'VigenIni'
    end
    object QrIBPTaxVigenFim: TDateField
      FieldName = 'VigenFim'
    end
    object QrIBPTaxAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIBPTaxEstadual: TFloatField
      FieldName = 'Estadual'
    end
    object QrIBPTaxMunicipal: TFloatField
      FieldName = 'Municipal'
    end
  end
  object QrNFeDFeINFe: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT cab.Empresa, nfe.* '
      'FROM nfedfeinfe nfe'
      'LEFT JOIN nfedfeicab cab ON cab.Codigo=nfe.Codigo')
    Left = 924
    Top = 4
    object QrNFeDFeINFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDFeINFeControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrNFeDFeINFeConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrNFeDFeINFeversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDFeINFetpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDFeINFechNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeDFeINFeCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeDFeINFeCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrNFeDFeINFeIE: TWideStringField
      FieldName = 'IE'
      Size = 18
    end
    object QrNFeDFeINFedhEmi: TDateTimeField
      FieldName = 'dhEmi'
    end
    object QrNFeDFeINFedhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
    end
    object QrNFeDFeINFetpNF: TSmallintField
      FieldName = 'tpNF'
    end
    object QrNFeDFeINFevNF: TFloatField
      FieldName = 'vNF'
    end
    object QrNFeDFeINFedigVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrNFeDFeINFedhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeDFeINFedhRecbtoTZD: TFloatField
      FieldName = 'dhRecbtoTZD'
    end
    object QrNFeDFeINFenProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeDFeINFecSitNFe: TSmallintField
      FieldName = 'cSitNFe'
    end
    object QrNFeDFeINFeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDFeINFeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDFeINFeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDFeINFeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDFeINFeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDFeINFeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDFeINFeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeDFeINFexNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrNFeDFeINFeNFa_IDCtrl: TIntegerField
      FieldName = 'NFa_IDCtrl'
    end
    object QrNFeDFeINFeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrDup: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 928
    Top = 52
  end
  object QrAux: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 928
    Top = 104
  end
  object QrNFEItsI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT iti.*, gg1.EX_TIPI'
      'FROM nfeitsi iti'
      'LEFT JOIN gragrux ggx ON ggx.Controle=iti.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE iti.FatID=:P0'
      'AND iti.FAtNum=:P1'
      'AND iti.EMpresa=:P2')
    Left = 112
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFEItsIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfeitsi.FatID'
    end
    object QrNFEItsIFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfeitsi.FatNum'
    end
    object QrNFEItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeitsi.Empresa'
    end
    object QrNFEItsInItem: TIntegerField
      FieldName = 'nItem'
      Origin = 'nfeitsi.nItem'
    end
    object QrNFEItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Origin = 'nfeitsi.prod_cProd'
      Size = 60
    end
    object QrNFEItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Origin = 'nfeitsi.prod_cEAN'
      Size = 14
    end
    object QrNFEItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Origin = 'nfeitsi.prod_xProd'
      Size = 120
    end
    object QrNFEItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Origin = 'nfeitsi.prod_NCM'
      Size = 8
    end
    object QrNFEItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Origin = 'nfeitsi.prod_EXTIPI'
      Size = 3
    end
    object QrNFEItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
      Origin = 'nfeitsi.prod_genero'
    end
    object QrNFEItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Origin = 'nfeitsi.prod_CFOP'
    end
    object QrNFEItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Origin = 'nfeitsi.prod_uCom'
      Size = 6
    end
    object QrNFEItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Origin = 'nfeitsi.prod_qCom'
    end
    object QrNFEItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Origin = 'nfeitsi.prod_vUnCom'
    end
    object QrNFEItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Origin = 'nfeitsi.prod_vProd'
    end
    object QrNFEItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Origin = 'nfeitsi.prod_cEANTrib'
      Size = 14
    end
    object QrNFEItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Origin = 'nfeitsi.prod_uTrib'
      Size = 6
    end
    object QrNFEItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Origin = 'nfeitsi.prod_qTrib'
    end
    object QrNFEItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Origin = 'nfeitsi.prod_vUnTrib'
    end
    object QrNFEItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Origin = 'nfeitsi.prod_vFrete'
    end
    object QrNFEItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Origin = 'nfeitsi.prod_vSeg'
    end
    object QrNFEItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Origin = 'nfeitsi.prod_vDesc'
    end
    object QrNFEItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Origin = 'nfeitsi._Ativo_'
    end
    object QrNFEItsILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfeitsi.Lk'
    end
    object QrNFEItsIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfeitsi.DataCad'
    end
    object QrNFEItsIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfeitsi.DataAlt'
    end
    object QrNFEItsIUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfeitsi.UserCad'
    end
    object QrNFEItsIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfeitsi.UserAlt'
    end
    object QrNFEItsIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfeitsi.AlterWeb'
    end
    object QrNFEItsIAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfeitsi.Ativo'
    end
    object QrNFEItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Origin = 'nfeitsi.Tem_IPI'
    end
    object QrNFEItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Origin = 'nfeitsi.InfAdCuztm'
    end
    object QrNFEItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrNFEItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFEItsITem_II: TSmallintField
      FieldName = 'Tem_II'
    end
    object QrNFEItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFEItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFEItsIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrNFEItsIEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrNFEItsIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrNFEItsIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrNFEItsIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrNFEItsIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrNFEItsIprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrNFEItsIprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
  end
  object QrNFeItsNA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsna'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsNAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsNAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsNAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsNAnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsNAICMSUFDest_vBCUFDest: TFloatField
      FieldName = 'ICMSUFDest_vBCUFDest'
    end
    object QrNFeItsNAICMSUFDest_pFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_pFCPUFDest'
    end
    object QrNFeItsNAICMSUFDest_pICMSUFDest: TFloatField
      FieldName = 'ICMSUFDest_pICMSUFDest'
    end
    object QrNFeItsNAICMSUFDest_pICMSInter: TFloatField
      FieldName = 'ICMSUFDest_pICMSInter'
    end
    object QrNFeItsNAICMSUFDest_pICMSInterPart: TFloatField
      FieldName = 'ICMSUFDest_pICMSInterPart'
    end
    object QrNFeItsNAICMSUFDest_vFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_vFCPUFDest'
    end
    object QrNFeItsNAICMSUFDest_vICMSUFDest: TFloatField
      FieldName = 'ICMSUFDest_vICMSUFDest'
    end
    object QrNFeItsNAICMSUFDest_vICMSUFRemet: TFloatField
      FieldName = 'ICMSUFDest_vICMSUFRemet'
    end
    object QrNFeItsNAICMSUFDest_vBCFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_vBCFCPUFDest'
    end
  end
  object QrNFeTotNA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'SUM(ICMS_vBC) ICMS_vBC,'
      'SUM(ICMS_vICMS) ICMS_vICMS,'
      'SUM(ICMS_vBCST) ICMS_vBCST,'
      'SUM(ICMS_vICMSST) ICMS_vICMSST'
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 584
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTotNAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
    end
    object QrNFeTotNAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
    end
    object QrNFeTotNAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
    end
  end
  object QrNFeCabZCon: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 272
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZConFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZConFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZConEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZConControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZConxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeCabZConxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object QrNFeCabZFis: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 272
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZFisFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZFisFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZFisEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZFisControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZFisxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeCabZFisxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object QrNFeCabZPro: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 272
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZProFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZProFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZProEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZProControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZPronProc: TWideStringField
      FieldName = 'nProc'
      Size = 60
    end
    object QrNFeCabZProindProc: TSmallintField
      FieldName = 'indProc'
    end
    object QrNFeCabZProtpAto: TSmallintField
      FieldName = 'tpAto'
    end
  end
  object QrNFECabYA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabya'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 272
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFECabYAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFECabYAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFECabYAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFECabYAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFECabYAtPag: TSmallintField
      FieldName = 'tPag'
    end
    object QrNFECabYAvPag: TFloatField
      FieldName = 'vPag'
    end
    object QrNFECabYAtpIntegra: TSmallintField
      FieldName = 'tpIntegra'
    end
    object QrNFECabYACNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFECabYAtBand: TSmallintField
      FieldName = 'tBand'
    end
    object QrNFECabYAcAut: TWideStringField
      FieldName = 'cAut'
    end
    object QrNFECabYAvTroco: TFloatField
      FieldName = 'vTroco'
    end
  end
  object QrRetiradaEnti: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 508
    Top = 432
    object QrRetiradaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRetiradaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrRetiradaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrRetiradaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrRetiradaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrRetiradaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrRetiradaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrRetiradaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrRetiradaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrRetiradaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrRetiradaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrRetiradaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrRetiradaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrRetiradaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrRetiradaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrRetiradaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrRetiradaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrRetiradaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrRetiradaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrRetiradaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrRetiradaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrRetiradaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrRetiradaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrRetiradaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrRetiradaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrRetiradaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrRetiradaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrRetiradaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrRetiradaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrRetiradaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrRetiradaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrRetiradaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrRetiradaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrRetiradaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrRetiradaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrRetiradaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrRetiradaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrRetiradaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrRetiradaEntiCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrRetiradaEntiNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrEntregaEnti: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 508
    Top = 484
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrEntregaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrEntregaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntregaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntregaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEntregaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEntregaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntregaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntregaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntregaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrEntregaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEntregaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrEntregaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntregaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrEntregaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEntregaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntregaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEntregaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEntregaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntregaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntregaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntregaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntregaEntiNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEntregaEntiCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object QrEntiEntrega: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT    '
      ' en.L_CNPJ, '
      ' en.L_CPF, '
      ' en.L_Nome, '
      ' en.LLograd, '
      ' en.LRua, '
      ' en.LNumero, '
      ' en.LCompl, '
      ' en.LBairro, '
      ' en.LCodMunici, '
      ' mun.Nome xMunicipio, '
      ' en.LUF, '
      ' en.LCEP, '
      ' en.LCodiPais, '
      ' pai.Nome xPais, '
      ' en.LTel, '
      ' en.LEmail, '
      ' en.L_IE,'
      ' en.L_Ativo  '
      'FROM entidades en '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'WHERE en.Codigo=:P0')
    Left = 508
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiEntregaL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrEntiEntregaL_CPF: TWideStringField
      FieldName = 'L_CPF'
      Size = 18
    end
    object QrEntiEntregaL_Nome: TWideStringField
      FieldName = 'L_Nome'
      Size = 60
    end
    object QrEntiEntregaLLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object QrEntiEntregaNO_LLograd: TWideStringField
      FieldName = 'NO_LLograd'
      Size = 15
    end
    object QrEntiEntregaLRua: TWideStringField
      FieldName = 'LRua'
      Size = 60
    end
    object QrEntiEntregaLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrEntiEntregaLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 60
    end
    object QrEntiEntregaLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 60
    end
    object QrEntiEntregaLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrEntiEntregaxMunicipio: TWideStringField
      FieldName = 'xMunicipio'
      Size = 100
    end
    object QrEntiEntregaLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrEntiEntregaNO_LUF: TWideStringField
      FieldName = 'NO_LUF'
      Size = 2
    end
    object QrEntiEntregaLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrEntiEntregaLCodiPais: TIntegerField
      FieldName = 'LCodiPais'
    end
    object QrEntiEntregaxPais: TWideStringField
      FieldName = 'xPais'
      Size = 100
    end
    object QrEntiEntregaLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrEntiEntregaLEmail: TWideStringField
      FieldName = 'LEmail'
      Size = 100
    end
    object QrEntiEntregaL_IE: TWideStringField
      FieldName = 'L_IE'
    end
    object QrEntiEntregaL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
  end
  object QrNFeItsLA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsla')
    Left = 112
    Top = 480
    object QrNFeItsLAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrNFeItsLAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFeItsLAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNFeItsLAnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrNFeItsLADetComb_cProdANP: TIntegerField
      FieldName = 'DetComb_cProdANP'
      Required = True
    end
    object QrNFeItsLADetComb_descANP: TWideStringField
      FieldName = 'DetComb_descANP'
      Required = True
      Size = 95
    end
    object QrNFeItsLADetComb_pGLP: TFloatField
      FieldName = 'DetComb_pGLP'
      Required = True
    end
    object QrNFeItsLADetComb_pGNn: TFloatField
      FieldName = 'DetComb_pGNn'
      Required = True
    end
    object QrNFeItsLADetComb_pGNi: TFloatField
      FieldName = 'DetComb_pGNi'
      Required = True
    end
    object QrNFeItsLADetComb_vPart: TFloatField
      FieldName = 'DetComb_vPart'
      Required = True
    end
    object QrNFeItsLADetComb_CODIF: TWideStringField
      FieldName = 'DetComb_CODIF'
      Size = 21
    end
    object QrNFeItsLADetComb_qTemp: TFloatField
      FieldName = 'DetComb_qTemp'
      Required = True
    end
    object QrNFeItsLADetComb_UFCons: TWideStringField
      FieldName = 'DetComb_UFCons'
      Required = True
      Size = 2
    end
    object QrNFeItsLADetComb_CIDE: TSmallintField
      FieldName = 'DetComb_CIDE'
      Required = True
    end
    object QrNFeItsLADetComb_CIDE_qBCProd: TFloatField
      FieldName = 'DetComb_CIDE_qBCProd'
      Required = True
    end
    object QrNFeItsLADetComb_CIDE_vAliqProd: TFloatField
      FieldName = 'DetComb_CIDE_vAliqProd'
      Required = True
    end
    object QrNFeItsLADetComb_CIDE_vCIDE: TFloatField
      FieldName = 'DetComb_CIDE_vCIDE'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante: TSmallintField
      FieldName = 'DetComb_encerrante'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante_nBico: TIntegerField
      FieldName = 'DetComb_encerrante_nBico'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante_nBomba: TIntegerField
      FieldName = 'DetComb_encerrante_nBomba'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante_nTanque: TIntegerField
      FieldName = 'DetComb_encerrante_nTanque'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante_vEncIni: TFloatField
      FieldName = 'DetComb_encerrante_vEncIni'
      Required = True
    end
    object QrNFeItsLADetComb_encerrante_vEncFin: TFloatField
      FieldName = 'DetComb_encerrante_vEncFin'
      Required = True
    end
    object QrNFeItsLALk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrNFeItsLADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsLADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsLAUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrNFeItsLAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrNFeItsLAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrNFeItsLAAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrNFeItsLAAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrNFeItsLAAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrQRCode_WS: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 272
    Top = 244
  end
  object QrLocCEST: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 928
    Top = 208
    object QrLocCESTCEST: TWideStringField
      DisplayWidth = 12
      FieldName = 'CEST'
      Size = 12
    end
    object QrLocCESTNCM: TWideStringField
      FieldName = 'NCM'
      Size = 11
    end
    object QrLocCESTNome: TWideStringField
      FieldName = 'Nome'
      Size = 1024
    end
  end
  object QrSMVA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT pgt.MadeBy, gg1.UsaSubsTrib, smv.ID'
      'FROM stqmovvala smv'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smv.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE smv.Tipo=1'
      'AND smv.Empresa=-11'
      'AND smv.OriCodi=64')
    Left = 928
    Top = 260
    object QrSMVAMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrSMVAUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
    object QrSMVAID: TIntegerField
      FieldName = 'ID'
    end
  end
  object QrNFEItsUA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsu'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 192
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsUAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsUAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsUAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsUAnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsUAimpostoDevol_pDevol: TFloatField
      FieldName = 'impostoDevol_pDevol'
    end
    object QrNFEItsUAimpostoDevol_vIPIDevol: TFloatField
      FieldName = 'impostoDevol_vIPIDevol'
    end
  end
  object QrInfIntermedEnti: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrInfIntermedEntiCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.RG, en.NIRE, en.CNAE,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF'
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo=:P0'
      '')
    Left = 28
    Top = 576
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInfIntermedEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfIntermedEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrInfIntermedEntiCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrInfIntermedEntiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrInfIntermedEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrInfIntermedEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrInfIntermedEntiCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrInfIntermedEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrInfIntermedEntiFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrInfIntermedEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrInfIntermedEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrInfIntermedEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrInfIntermedEntiTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrInfIntermedEntiNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrInfIntermedEntiNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrInfIntermedEntiNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrInfIntermedEntiENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 60
      Calculated = True
    end
    object QrInfIntermedEntiNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrInfIntermedEntiNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrInfIntermedEntiCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrInfIntermedEntiUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrInfIntermedEntiCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
  end
  object QrGraGXVend: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT gfm.Nome NO_MARCA, gg1.Referencia '
      'FROM gragxvend cpl '
      'LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle '
      'WHERE cpl.GraGruX>3351 '
      ' ')
    Left = 936
    Top = 476
    object QrGraGXVendNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Size = 60
    end
    object QrGraGXVendReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
  object QrStqMovNFsA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM stqmovnfsa'
      'WHERE Tipo=1'
      'AND OriCodi>0'
      'AND Empresa=-11')
    Left = 936
    Top = 522
    object QrStqMovNFsAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrStqMovNFsATipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrStqMovNFsAOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Required = True
    end
    object QrStqMovNFsAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrStqMovNFsASerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Required = True
    end
    object QrStqMovNFsASerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Required = True
      Size = 5
    end
    object QrStqMovNFsANumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Required = True
    end
    object QrStqMovNFsAIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Required = True
    end
    object QrStqMovNFsAFreteVal: TFloatField
      FieldName = 'FreteVal'
      Required = True
    end
    object QrStqMovNFsASeguro: TFloatField
      FieldName = 'Seguro'
      Required = True
    end
    object QrStqMovNFsAOutros: TFloatField
      FieldName = 'Outros'
      Required = True
    end
    object QrStqMovNFsAPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Size = 2
    end
    object QrStqMovNFsAPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Size = 8
    end
    object QrStqMovNFsARNTC: TWideStringField
      FieldName = 'RNTC'
    end
    object QrStqMovNFsAQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Size = 30
    end
    object QrStqMovNFsAEspecie: TWideStringField
      FieldName = 'Especie'
      Size = 60
    end
    object QrStqMovNFsAMarca: TWideStringField
      FieldName = 'Marca'
      Size = 60
    end
    object QrStqMovNFsANumero: TWideStringField
      FieldName = 'Numero'
      Size = 60
    end
    object QrStqMovNFsAkgBruto: TFloatField
      FieldName = 'kgBruto'
      Required = True
    end
    object QrStqMovNFsAkgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Required = True
    end
    object QrStqMovNFsAObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrStqMovNFsACFOP1: TWideStringField
      FieldName = 'CFOP1'
      Size = 6
    end
    object QrStqMovNFsADtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Required = True
    end
    object QrStqMovNFsADtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Required = True
    end
    object QrStqMovNFsAHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Required = True
    end
    object QrStqMovNFsAinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrStqMovNFsAUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Size = 2
    end
    object QrStqMovNFsAxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Size = 60
    end
    object QrStqMovNFsAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrStqMovNFsAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrStqMovNFsAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Required = True
    end
    object QrStqMovNFsAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrStqMovNFsAvagao: TWideStringField
      FieldName = 'vagao'
    end
    object QrStqMovNFsAbalsa: TWideStringField
      FieldName = 'balsa'
    end
    object QrStqMovNFsACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrStqMovNFsACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrStqMovNFsACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrStqMovNFsAdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
      Required = True
    end
    object QrStqMovNFsAdhSaiEntTZD: TFloatField
      FieldName = 'dhSaiEntTZD'
      Required = True
    end
    object QrStqMovNFsAHrEmi: TTimeField
      FieldName = 'HrEmi'
      Required = True
    end
  end
  object QrStqMovNFsRef: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabb nfeb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 928
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrStqMovNFsRefFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrStqMovNFsRefFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrStqMovNFsRefEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovNFsRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrStqMovNFsRefrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrStqMovNFsRefrefNFeSig: TWideStringField
      FieldName = 'refNFeSig'
      Size = 44
    end
    object QrStqMovNFsRefrefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
    end
    object QrStqMovNFsRefrefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
      DisplayFormat = '0000'
    end
    object QrStqMovNFsRefrefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Size = 14
    end
    object QrStqMovNFsRefrefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
      DisplayFormat = '0'
    end
    object QrStqMovNFsRefrefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
      DisplayFormat = '000'
    end
    object QrStqMovNFsRefrefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
      DisplayFormat = '000000000'
    end
    object QrStqMovNFsRefLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrStqMovNFsRefDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrStqMovNFsRefDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrStqMovNFsRefUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrStqMovNFsRefUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrStqMovNFsRefAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqMovNFsRefAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqMovNFsRefrefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
    end
    object QrStqMovNFsRefrefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
    end
    object QrStqMovNFsRefrefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Size = 14
    end
    object QrStqMovNFsRefrefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Size = 11
    end
    object QrStqMovNFsRefrefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Size = 14
    end
    object QrStqMovNFsRefrefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
    end
    object QrStqMovNFsRefrefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
    end
    object QrStqMovNFsRefrefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
    end
    object QrStqMovNFsRefrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrStqMovNFsRefrefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Size = 2
    end
    object QrStqMovNFsRefrefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
    end
    object QrStqMovNFsRefrefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
    end
    object QrStqMovNFsRefQualNFref: TSmallintField
      FieldName = 'QualNFref'
    end
  end
  object DsStqMovNFsRef: TDataSource
    DataSet = QrStqMovNFsRef
    Left = 928
    Top = 396
  end
  object QrNFeEveRCab1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento, '
      'Status, ret_cStat, ret_nProt, ret_dhRegEvento, '
      'XML_Eve, XML_retEve, versao, duf.Nome UF_Nome, '
      'cOrgao, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF,'
      'nSeqEvento, verEvento'
      'FROM nfeevercab'
      'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao ')
    Left = 1020
    object QrNFeEveRCab1nSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrNFeEveRCab1CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrNFeEveRCab1UF_Nome: TWideStringField
      FieldName = 'UF_Nome'
    end
    object QrNFeEveRCab1cOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeEveRCab1versao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveRCab1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCab1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCab1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCab1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCab1tpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCab1chNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCab1dhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCab1tpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCab1descEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCab1chNFe_NF_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object QrNFeEveRCab1chNFe_NF_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object QrNFeEveRCab1ret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCab1Status: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCab1ret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrNFeEveRCab1ret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRCab1XML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCab1XML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCab1verEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrNFeEveRCab1STATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object MySQLQuery1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento, '
      'Status, ret_cStat, ret_nProt, ret_dhRegEvento, '
      'XML_Eve, XML_retEve, versao, duf.Nome UF_Nome, '
      'cOrgao, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF,'
      'nSeqEvento, verEvento'
      'FROM nfeevercab'
      'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao ')
    Left = 664
    Top = 576
    object SmallintField11: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object WideStringField5: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object WideStringField6: TWideStringField
      FieldName = 'UF_Nome'
    end
    object SmallintField12: TSmallintField
      FieldName = 'cOrgao'
    end
    object FloatField20: TFloatField
      FieldName = 'versao'
    end
    object IntegerField17: TIntegerField
      FieldName = 'FatID'
    end
    object IntegerField18: TIntegerField
      FieldName = 'FatNum'
    end
    object IntegerField19: TIntegerField
      FieldName = 'Empresa'
    end
    object IntegerField20: TIntegerField
      FieldName = 'Controle'
    end
    object SmallintField13: TSmallintField
      FieldName = 'tpAmb'
    end
    object WideStringField7: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'dhEvento'
    end
    object IntegerField21: TIntegerField
      FieldName = 'tpEvento'
    end
    object WideStringField8: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object WideStringField9: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object WideStringField10: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object IntegerField22: TIntegerField
      FieldName = 'ret_cStat'
    end
    object IntegerField23: TIntegerField
      FieldName = 'Status'
    end
    object WideStringField11: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object WideMemoField1: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object WideMemoField2: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object FloatField21: TFloatField
      FieldName = 'verEvento'
    end
    object WideStringField12: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object QrNFeEveRCan: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM nfeevercan'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 1020
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCanFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCanFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCanEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCanControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCanConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCannProt: TLargeintField
      FieldName = 'nProt'
    end
    object QrNFeEveRCannJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrNFeEveRCanxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrNFeEveRCanLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object QrNFeEveRCCe: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 1020
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCCeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCCeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCCeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCCeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCCexCorrecao: TWideMemoField
      FieldName = 'xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
    object QrNFeEveRCCeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEveRCCeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEveRCCeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEveRCCeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEveRCCeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEveRCCeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEveRCCeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNfeEveRMDe: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM nfeevermde'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 1020
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNfeEveRMDeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNfeEveRMDeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNfeEveRMDeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNfeEveRMDeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNfeEveRMDeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNfeEveRMDecSitConf: TSmallintField
      FieldName = 'cSitConf'
    end
    object QrNfeEveRMDetpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNfeEveRMDenJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrNfeEveRMDexJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
  end
  object QrSumNFsInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VL_DESC, VL_MERC, VL_FRT, '
      'VL_SEG, VL_OUT_DA, VL_BC_ICMS, '
      'VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, '
      'VL_IPI, VL_PIS, VL_COFINS'
      'FROM efdinnnfscab')
    Left = 1028
    Top = 348
    object QrSumNFsInnVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrSumNFsInnVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
    end
    object QrSumNFsInnVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
    end
    object QrSumNFsInnVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
    end
    object QrSumNFsInnVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
    end
    object QrSumNFsInnVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrSumNFsInnVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrSumNFsInnVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrSumNFsInnVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrSumNFsInnVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrSumNFsInnVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrSumNFsInnVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrSumNFsInnVL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
  end
  object QrEfdLinkCab: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle, SqLinked  '
      'FROM efdinnnfscab '
      'WHERE MovFatID=10 '
      'AND MovFatNum=0 '
      'AND MovimCod=0 '
      'AND Empresa=-11 ')
    Left = 1028
    Top = 401
    object QrEfdLinkCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
    end
    object QrEfdLinkCabControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEfdLinkIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle, SqLinked  '
      'FROM efdinnnfscab '
      'WHERE MovFatID=10 '
      'AND MovFatNum=0 '
      'AND MovimCod=0 '
      'AND Empresa=-11 ')
    Left = 1028
    Top = 457
    object QrEfdLinkItsSqLinked: TIntegerField
      FieldName = 'SqLinked'
    end
    object QrEfdLinkItsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrTG1: TMySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT t.* FROM '
      'tbspedcods130 t'
      'WHERE t.CodTxt="000"'
      'AND "2022-04-15" > DataIni'
      'AND ('
      '  "2022-04-15" < DataFim'
      '  OR (DataFim < "1900-01-01")'
      ')')
    Left = 1028
    Top = 532
    object QrTG1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 511
    end
  end
  object QrCSTsInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'frc.GenCtbD RegFisCad_CtbD, frc.GenCtbC RegFisCad_CtbC,'
      'frp.GenCtbD RegFisCFOP_CtbD, frp.GenCtbC RegFisCFOP_CtbC,'
      'fru.GenCtbD RegFisUFs_CtbD, fru.GenCtbC RegFisUFs_CtbC,'
      'frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, '
      'fru.* '
      'FROM fisregufs fru '
      'LEFT JOIN fisregcfop frp ON frp.Codigo=fru.Codigo '
      '  AND frp.Interno=0'
      '  AND frp.Contribui=1'
      '  AND frp.Proprio=1'
      '  AND frp.SubsTrib=0'
      '  AND frp.Servico=0'
      '  AND frp.OriCFOP="6.101" '#9
      'LEFT JOIN fisregcad frc ON frc.Codigo=fru.Codigo '
      'WHERE fru.Codigo=3'
      'AND fru.Interno=0'
      'AND fru.Contribui=1'
      'AND fru.Proprio=1'
      'AND fru.SubsTrib=0'
      'AND fru.Servico=0'
      'AND fru.OriCFOP="6.101" '
      'AND fru.UFEmit="SP" '
      'AND fru.UFDest="MS" ')
    Left = 1028
    Top = 296
    object QrCSTsInnICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
    end
    object QrCSTsInnPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
    end
    object QrCSTsInnCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
    end
    object QrCSTsInnCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCSTsInnInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
    end
    object QrCSTsInnContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
    end
    object QrCSTsInnProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
    end
    object QrCSTsInnUFEmit: TWideStringField
      FieldName = 'UFEmit'
      Required = True
      Size = 2
    end
    object QrCSTsInnUFDest: TWideStringField
      FieldName = 'UFDest'
      Required = True
      Size = 2
    end
    object QrCSTsInnSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
      Required = True
    end
    object QrCSTsInnICMSAliq: TFloatField
      FieldName = 'ICMSAliq'
      Required = True
    end
    object QrCSTsInnCST_B: TWideStringField
      FieldName = 'CST_B'
      Size = 2
    end
    object QrCSTsInnpRedBC: TFloatField
      FieldName = 'pRedBC'
      Required = True
    end
    object QrCSTsInnmodBC: TSmallintField
      FieldName = 'modBC'
      Required = True
    end
    object QrCSTsInnpBCUFDest: TFloatField
      FieldName = 'pBCUFDest'
      Required = True
    end
    object QrCSTsInnpFCPUFDest: TFloatField
      FieldName = 'pFCPUFDest'
      Required = True
    end
    object QrCSTsInnpICMSUFDest: TFloatField
      FieldName = 'pICMSUFDest'
      Required = True
    end
    object QrCSTsInnpICMSInter: TFloatField
      FieldName = 'pICMSInter'
      Required = True
    end
    object QrCSTsInnpICMSInterPart: TFloatField
      FieldName = 'pICMSInterPart'
      Required = True
    end
    object QrCSTsInnUsaInterPartLei: TSmallintField
      FieldName = 'UsaInterPartLei'
      Required = True
    end
    object QrCSTsInncBenef: TWideStringField
      FieldName = 'cBenef'
      Size = 10
    end
    object QrCSTsInnpDif: TFloatField
      FieldName = 'pDif'
      Required = True
    end
    object QrCSTsInnCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 3
    end
    object QrCSTsInnOriCST_ICMS: TWideStringField
      FieldName = 'OriCST_ICMS'
      Required = True
      Size = 3
    end
    object QrCSTsInnOriCST_IPI: TWideStringField
      FieldName = 'OriCST_IPI'
      Size = 2
    end
    object QrCSTsInnOriCST_PIS: TWideStringField
      FieldName = 'OriCST_PIS'
      Size = 2
    end
    object QrCSTsInnOriCST_COFINS: TWideStringField
      FieldName = 'OriCST_COFINS'
      Size = 2
    end
    object QrCSTsInnOriTES: TIntegerField
      FieldName = 'OriTES'
      Required = True
    end
    object QrCSTsInnEFD_II_C195: TSmallintField
      FieldName = 'EFD_II_C195'
      Required = True
    end
    object QrCSTsInnGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrCSTsInnGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrCSTsInnTES_ICMS: TSmallintField
      FieldName = 'TES_ICMS'
    end
    object QrCSTsInnTES_IPI: TSmallintField
      FieldName = 'TES_IPI'
    end
    object QrCSTsInnTES_PIS: TSmallintField
      FieldName = 'TES_PIS'
    end
    object QrCSTsInnTES_COFINS: TSmallintField
      FieldName = 'TES_COFINS'
    end
    object QrCSTsInnPISAliq: TFloatField
      FieldName = 'PISAliq'
    end
    object QrCSTsInnCOFINSAliq: TFloatField
      FieldName = 'COFINSAliq'
    end
    object QrCSTsInnRegFisCad_CtbD: TIntegerField
      FieldName = 'RegFisCad_CtbD'
      Required = True
    end
    object QrCSTsInnRegFisCad_CtbC: TIntegerField
      FieldName = 'RegFisCad_CtbC'
      Required = True
    end
    object QrCSTsInnRegFisCFOP_CtbD: TIntegerField
      FieldName = 'RegFisCFOP_CtbD'
      Required = True
    end
    object QrCSTsInnRegFisCFOP_CtbC: TIntegerField
      FieldName = 'RegFisCFOP_CtbC'
      Required = True
    end
    object QrCSTsInnRegFisUFs_CtbD: TIntegerField
      FieldName = 'RegFisUFs_CtbD'
      Required = True
    end
    object QrCSTsInnRegFisUFs_CtbC: TIntegerField
      FieldName = 'RegFisUFs_CtbC'
      Required = True
    end
    object QrCSTsInnOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Required = True
      Size = 5
    end
    object QrCSTsInnServico: TSmallintField
      FieldName = 'Servico'
      Required = True
    end
    object QrCSTsInnTES_BC_ICMS: TSmallintField
      FieldName = 'TES_BC_ICMS'
      Required = True
    end
    object QrCSTsInnTES_BC_IPI: TSmallintField
      FieldName = 'TES_BC_IPI'
      Required = True
    end
    object QrCSTsInnTES_BC_PIS: TSmallintField
      FieldName = 'TES_BC_PIS'
      Required = True
    end
    object QrCSTsInnTES_BC_COFINS: TSmallintField
      FieldName = 'TES_BC_COFINS'
      Required = True
    end
  end
  object QrNFEItsVA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsva'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 188
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFEItsVAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrNFEItsVAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFEItsVAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNFEItsVAnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrNFEItsVAVAItem: TSmallintField
      FieldName = 'VAItem'
    end
    object QrNFEItsVAobsCont_xCampo: TWideStringField
      FieldName = 'obsCont_xCampo'
    end
    object QrNFEItsVAobsCont_xTexto: TWideStringField
      FieldName = 'obsCont_xTexto'
      Size = 60
    end
    object QrNFEItsVAobsFisco_xCampo: TWideStringField
      FieldName = 'obsFisco_xCampo'
    end
    object QrNFEItsVAobsFisco_xTexto: TWideStringField
      FieldName = 'obsFisco_xTexto'
      Size = 60
    end
  end
  object QrNFeEveREPEC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeeverepec'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 1024
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveREPECFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrNFeEveREPECFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFeEveREPECEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNFeEveREPECControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNFeEveREPECConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrNFeEveREPECcOrgaoAutor: TSmallintField
      FieldName = 'cOrgaoAutor'
      Required = True
    end
    object QrNFeEveREPECtpAutor: TSmallintField
      FieldName = 'tpAutor'
      Required = True
    end
    object QrNFeEveREPECverAplic: TWideStringField
      FieldName = 'verAplic'
      Required = True
    end
    object QrNFeEveREPECdhEmi: TDateTimeField
      FieldName = 'dhEmi'
      Required = True
    end
    object QrNFeEveREPECdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
      Required = True
    end
    object QrNFeEveREPECtpNF: TSmallintField
      FieldName = 'tpNF'
      Required = True
    end
    object QrNFeEveREPECIE: TWideStringField
      FieldName = 'IE'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Required = True
      Size = 2
    end
    object QrNFeEveREPECdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Required = True
      Size = 11
    end
    object QrNFeEveREPECdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
      Required = True
    end
    object QrNFeEveREPECdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECvNF: TFloatField
      FieldName = 'vNF'
      Required = True
    end
    object QrNFeEveREPECvICMS: TFloatField
      FieldName = 'vICMS'
      Required = True
    end
    object QrNFeEveREPECvST: TFloatField
      FieldName = 'vST'
      Required = True
    end
    object QrNFeEveREPECversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveREPECdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 15
    end
  end
end
