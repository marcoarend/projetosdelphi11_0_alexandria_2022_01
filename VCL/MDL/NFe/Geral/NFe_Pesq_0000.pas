unit NFe_Pesq_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  dmkCheckGroup, frxExportPDF, UnDmkEnums, dmkCompoStore, dmkPermissoes,
  {$IfNDef VER320}
  frxExportBaseDialog,
  {$EndIf}
  dmkDBGridZTO, UnGrl_Consts, Vcl.OleCtrls, SHDocVw;

type
  TQuemEmit = (tqeEmitidas, tqeRecebidas, tqeAmbos);
  THackDBGrid = class(TdmkDBGrid);
  TFmNFe_Pesq_0000 = class(TForm)
    PnDados: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    DBGNFes: TdmkDBGridZTO;
    QrNFeCabA_: TmySQLQuery;
    DsNFeCabA_: TDataSource;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    QrNFeCabA_NO_Cli: TWideStringField;
    QrNFeCabA_FatID: TIntegerField;
    QrNFeCabA_FatNum: TIntegerField;
    QrNFeCabA_Empresa: TIntegerField;
    QrNFeCabA_LoteEnv: TIntegerField;
    QrNFeCabA_Id: TWideStringField;
    QrNFeCabA_ide_natOp: TWideStringField;
    QrNFeCabA_ide_serie: TIntegerField;
    QrNFeCabA_ide_nNF: TIntegerField;
    QrNFeCabA_ide_dEmi: TDateField;
    QrNFeCabA_ide_tpNF: TSmallintField;
    QrNFeCabA_ICMSTot_vProd: TFloatField;
    QrNFeCabA_ICMSTot_vNF: TFloatField;
    QrNFeCabA_xMotivo: TWideStringField;
    QrNFeCabA_dhRecbto: TWideStringField;
    QrNFeCabA_IDCtrl: TIntegerField;
    PMLeArq: TPopupMenu;
    Cancelamento1: TMenuItem;
    QrNFeCabAMsg_: TmySQLQuery;
    DsNFeCabAMsg_: TDataSource;
    QrNFeCabAMsg_FatID: TIntegerField;
    QrNFeCabAMsg_FatNum: TIntegerField;
    QrNFeCabAMsg_Empresa: TIntegerField;
    QrNFeCabAMsg_Controle: TIntegerField;
    QrNFeCabAMsg_Solicit: TIntegerField;
    QrNFeCabAMsg_Id: TWideStringField;
    QrNFeCabAMsg_tpAmb: TSmallintField;
    QrNFeCabAMsg_verAplic: TWideStringField;
    QrNFeCabAMsg_dhRecbto: TDateTimeField;
    QrNFeCabAMsg_nProt: TWideStringField;
    QrNFeCabAMsg_digVal: TWideStringField;
    QrNFeCabAMsg_cStat: TIntegerField;
    QrNFeCabAMsg_xMotivo: TWideStringField;
    frxDsA_: TfrxDBDataset;
    QrA_: TmySQLQuery;
    QrA_FatID: TIntegerField;
    QrA_FatNum: TIntegerField;
    QrA_Empresa: TIntegerField;
    QrA_LoteEnv: TIntegerField;
    QrA_versao: TFloatField;
    QrA_Id: TWideStringField;
    QrA_ide_cUF: TSmallintField;
    QrA_ide_cNF: TIntegerField;
    QrA_ide_natOp: TWideStringField;
    QrA_ide_indPag: TSmallintField;
    QrA_ide_mod: TSmallintField;
    QrA_ide_serie: TIntegerField;
    QrA_ide_nNF: TIntegerField;
    QrA_ide_dEmi: TDateField;
    QrA_ide_dSaiEnt: TDateField;
    QrA_ide_tpNF: TSmallintField;
    QrA_ide_cMunFG: TIntegerField;
    QrA_ide_tpImp: TSmallintField;
    QrA_ide_tpEmis: TSmallintField;
    QrA_ide_cDV: TSmallintField;
    QrA_ide_tpAmb: TSmallintField;
    QrA_ide_finNFe: TSmallintField;
    QrA_ide_procEmi: TSmallintField;
    QrA_ide_verProc: TWideStringField;
    QrA_emit_CNPJ: TWideStringField;
    QrA_emit_CPF: TWideStringField;
    QrA_emit_xNome: TWideStringField;
    QrA_emit_xFant: TWideStringField;
    QrA_emit_xLgr: TWideStringField;
    QrA_emit_nro: TWideStringField;
    QrA_emit_xCpl: TWideStringField;
    QrA_emit_xBairro: TWideStringField;
    QrA_emit_cMun: TIntegerField;
    QrA_emit_xMun: TWideStringField;
    QrA_emit_UF: TWideStringField;
    QrA_emit_CEP: TIntegerField;
    QrA_emit_cPais: TIntegerField;
    QrA_emit_xPais: TWideStringField;
    QrA_emit_fone: TWideStringField;
    QrA_emit_IE: TWideStringField;
    QrA_emit_IEST: TWideStringField;
    QrA_emit_IM: TWideStringField;
    QrA_emit_CNAE: TWideStringField;
    QrA_dest_CNPJ: TWideStringField;
    QrA_dest_CPF: TWideStringField;
    QrA_dest_xNome: TWideStringField;
    QrA_dest_xLgr: TWideStringField;
    QrA_dest_nro: TWideStringField;
    QrA_dest_xCpl: TWideStringField;
    QrA_dest_xBairro: TWideStringField;
    QrA_dest_cMun: TIntegerField;
    QrA_dest_xMun: TWideStringField;
    QrA_dest_UF: TWideStringField;
    QrA_dest_CEP: TWideStringField;
    QrA_dest_cPais: TIntegerField;
    QrA_dest_xPais: TWideStringField;
    QrA_dest_fone: TWideStringField;
    QrA_dest_IE: TWideStringField;
    QrA_dest_ISUF: TWideStringField;
    QrA_ICMSTot_vBC: TFloatField;
    QrA_ICMSTot_vICMS: TFloatField;
    QrA_ICMSTot_vBCST: TFloatField;
    QrA_ICMSTot_vST: TFloatField;
    QrA_ICMSTot_vProd: TFloatField;
    QrA_ICMSTot_vFrete: TFloatField;
    QrA_ICMSTot_vSeg: TFloatField;
    QrA_ICMSTot_vDesc: TFloatField;
    QrA_ICMSTot_vII: TFloatField;
    QrA_ICMSTot_vIPI: TFloatField;
    QrA_ICMSTot_vPIS: TFloatField;
    QrA_ICMSTot_vCOFINS: TFloatField;
    QrA_ICMSTot_vOutro: TFloatField;
    QrA_ICMSTot_vNF: TFloatField;
    QrA_ISSQNtot_vServ: TFloatField;
    QrA_ISSQNtot_vBC: TFloatField;
    QrA_ISSQNtot_vISS: TFloatField;
    QrA_ISSQNtot_vPIS: TFloatField;
    QrA_ISSQNtot_vCOFINS: TFloatField;
    QrA_RetTrib_vRetPIS: TFloatField;
    QrA_RetTrib_vRetCOFINS: TFloatField;
    QrA_RetTrib_vRetCSLL: TFloatField;
    QrA_RetTrib_vBCIRRF: TFloatField;
    QrA_RetTrib_vIRRF: TFloatField;
    QrA_RetTrib_vBCRetPrev: TFloatField;
    QrA_RetTrib_vRetPrev: TFloatField;
    QrA_ModFrete: TSmallintField;
    QrA_Transporta_CNPJ: TWideStringField;
    QrA_Transporta_CPF: TWideStringField;
    QrA_Transporta_XNome: TWideStringField;
    QrA_Transporta_IE: TWideStringField;
    QrA_Transporta_XEnder: TWideStringField;
    QrA_Transporta_XMun: TWideStringField;
    QrA_Transporta_UF: TWideStringField;
    QrA_RetTransp_vServ: TFloatField;
    QrA_RetTransp_vBCRet: TFloatField;
    QrA_RetTransp_PICMSRet: TFloatField;
    QrA_RetTransp_vICMSRet: TFloatField;
    QrA_RetTransp_CFOP: TWideStringField;
    QrA_RetTransp_CMunFG: TWideStringField;
    QrA_VeicTransp_Placa: TWideStringField;
    QrA_VeicTransp_UF: TWideStringField;
    QrA_VeicTransp_RNTC: TWideStringField;
    QrA_Cobr_Fat_NFat: TWideStringField;
    QrA_Cobr_Fat_vOrig: TFloatField;
    QrA_Cobr_Fat_vDesc: TFloatField;
    QrA_Cobr_Fat_vLiq: TFloatField;
    QrA_InfAdic_InfAdFisco: TWideMemoField;
    QrA_InfAdic_InfCpl: TWideMemoField;
    QrA_Exporta_UFEmbarq: TWideStringField;
    QrA_Exporta_XLocEmbarq: TWideStringField;
    QrA_Compra_XNEmp: TWideStringField;
    QrA_Compra_XPed: TWideStringField;
    QrA_Compra_XCont: TWideStringField;
    QrA_Status: TIntegerField;
    QrA_infProt_Id: TWideStringField;
    QrA_infProt_tpAmb: TSmallintField;
    QrA_infProt_verAplic: TWideStringField;
    QrA_infProt_dhRecbto: TDateTimeField;
    QrA_infProt_nProt: TWideStringField;
    QrA_infProt_digVal: TWideStringField;
    QrA_infProt_cStat: TIntegerField;
    QrA_infProt_xMotivo: TWideStringField;
    QrA__Ativo_: TSmallintField;
    QrA_Lk: TIntegerField;
    QrA_DataCad: TDateField;
    QrA_DataAlt: TDateField;
    QrA_UserCad: TIntegerField;
    QrA_UserAlt: TIntegerField;
    QrA_AlterWeb: TSmallintField;
    QrA_Ativo: TSmallintField;
    QrA_IDCtrl: TIntegerField;
    QrA_infCanc_Id: TWideStringField;
    QrA_infCanc_tpAmb: TSmallintField;
    QrA_infCanc_verAplic: TWideStringField;
    QrA_infCanc_dhRecbto: TDateTimeField;
    QrA_infCanc_nProt: TWideStringField;
    QrA_infCanc_digVal: TWideStringField;
    QrA_infCanc_cStat: TIntegerField;
    QrA_infCanc_xMotivo: TWideStringField;
    QrA_infCanc_cJust: TIntegerField;
    QrA_infCanc_xJust: TWideStringField;
    QrA_ID_TXT: TWideStringField;
    QrA_EMIT_ENDERECO: TWideStringField;
    QrA_EMIT_FONE_TXT: TWideStringField;
    QrA_EMIT_IE_TXT: TWideStringField;
    QrA_EMIT_IEST_TXT: TWideStringField;
    QrA_EMIT_CNPJ_TXT: TWideStringField;
    QrA_DEST_CNPJ_CPF_TXT: TWideStringField;
    QrA_DEST_ENDERECO: TWideStringField;
    QrA_DEST_CEP_TXT: TWideStringField;
    QrA_DEST_FONE_TXT: TWideStringField;
    QrA_DEST_IE_TXT: TWideStringField;
    QrA_TRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrA_TRANSPORTA_IE_TXT: TWideStringField;
    QrI_: TmySQLQuery;
    QrI_prod_cProd: TWideStringField;
    QrI_prod_cEAN: TWideStringField;
    QrI_prod_xProd: TWideStringField;
    QrI_prod_NCM: TWideStringField;
    QrI_prod_EXTIPI: TWideStringField;
    QrI_prod_genero: TSmallintField;
    QrI_prod_CFOP: TIntegerField;
    QrI_prod_uCom: TWideStringField;
    QrI_prod_qCom: TFloatField;
    QrI_prod_vUnCom: TFloatField;
    QrI_prod_vProd: TFloatField;
    QrI_prod_cEANTrib: TWideStringField;
    QrI_prod_uTrib: TWideStringField;
    QrI_prod_qTrib: TFloatField;
    QrI_prod_vUnTrib: TFloatField;
    QrI_prod_vFrete: TFloatField;
    QrI_prod_vSeg: TFloatField;
    QrI_prod_vDesc: TFloatField;
    QrI_Tem_IPI: TSmallintField;
    frxDsI_: TfrxDBDataset;
    QrN_: TmySQLQuery;
    QrO_: TmySQLQuery;
    frxDsN_: TfrxDBDataset;
    frxDsO_: TfrxDBDataset;
    QrI_nItem: TIntegerField;
    QrN_nItem: TIntegerField;
    QrN_ICMS_Orig: TSmallintField;
    QrN_ICMS_CST: TSmallintField;
    QrN_ICMS_modBC: TSmallintField;
    QrN_ICMS_pRedBC: TFloatField;
    QrN_ICMS_vBC: TFloatField;
    QrN_ICMS_pICMS: TFloatField;
    QrN_ICMS_vICMS: TFloatField;
    QrN_ICMS_modBCST: TSmallintField;
    QrN_ICMS_pMVAST: TFloatField;
    QrN_ICMS_pRedBCST: TFloatField;
    QrN_ICMS_vBCST: TFloatField;
    QrN_ICMS_pICMSST: TFloatField;
    QrN_ICMS_vICMSST: TFloatField;
    QrO_nItem: TIntegerField;
    QrO_IPI_clEnq: TWideStringField;
    QrO_IPI_CNPJProd: TWideStringField;
    QrO_IPI_cSelo: TWideStringField;
    QrO_IPI_qSelo: TFloatField;
    QrO_IPI_cEnq: TWideStringField;
    QrO_IPI_CST: TWideStringField;
    QrO_IPI_vBC: TFloatField;
    QrO_IPI_qUnid: TFloatField;
    QrO_IPI_vUnid: TFloatField;
    QrO_IPI_pIPI: TFloatField;
    QrO_IPI_vIPI: TFloatField;
    QrA_DOC_SEM_VLR_JUR: TWideStringField;
    QrA_DOC_SEM_VLR_FIS: TWideStringField;
    PMImprime: TPopupMenu;
    CampospreenchidosnoXMLdaNFe1: TMenuItem;
    QrNFeXMLI_: TmySQLQuery;
    frxDsNFeXMLI_: TfrxDBDataset;
    frxDsNFeCabA_: TfrxDBDataset;
    QrNFeXMLI_Codigo: TWideStringField;
    QrNFeXMLI_ID: TWideStringField;
    QrNFeXMLI_Valor: TWideStringField;
    QrNFeXMLI_Pai: TWideStringField;
    QrNFeXMLI_Descricao: TWideStringField;
    QrNFeXMLI_Campo: TWideStringField;
    QrY_: TmySQLQuery;
    frxDsY_: TfrxDBDataset;
    QrY_nDup: TWideStringField;
    QrY_dVenc: TDateField;
    QrY_vDup: TFloatField;
    QrNFeYIts_: TmySQLQuery;
    QrNFeYIts_Seq1: TIntegerField;
    QrNFeYIts_nDup1: TWideStringField;
    QrNFeYIts_dVenc1: TDateField;
    QrNFeYIts_vDup1: TFloatField;
    QrNFeYIts_Seq2: TIntegerField;
    QrNFeYIts_nDup2: TWideStringField;
    QrNFeYIts_dVenc2: TDateField;
    QrNFeYIts_vDup2: TFloatField;
    QrNFeYIts_Seq3: TIntegerField;
    QrNFeYIts_nDup3: TWideStringField;
    QrNFeYIts_dVenc3: TDateField;
    QrNFeYIts_vDup3: TFloatField;
    QrNFeYIts_Seq4: TIntegerField;
    QrNFeYIts_nDup4: TWideStringField;
    QrNFeYIts_dVenc4: TDateField;
    QrNFeYIts_vDup4: TFloatField;
    QrNFeYIts_Seq5: TIntegerField;
    QrNFeYIts_nDup5: TWideStringField;
    QrNFeYIts_dVenc5: TDateField;
    QrNFeYIts_vDup5: TFloatField;
    QrNFeYIts_Linha: TIntegerField;
    frxDsNFeYIts_: TfrxDBDataset;
    QrNFeYIts_xVenc1: TWideStringField;
    QrNFeYIts_xVenc2: TWideStringField;
    QrNFeYIts_xVenc3: TWideStringField;
    QrNFeYIts_xVenc4: TWideStringField;
    QrNFeYIts_xVenc5: TWideStringField;
    QrXVol_: TmySQLQuery;
    frxDsXvol_: TfrxDBDataset;
    QrXVol_qVol: TFloatField;
    QrXVol_esp: TWideStringField;
    QrXVol_marca: TWideStringField;
    QrXVol_nVol: TWideStringField;
    QrXVol_pesoL: TFloatField;
    QrXVol_pesoB: TFloatField;
    QrV_: TmySQLQuery;
    frxDsV_: TfrxDBDataset;
    QrV_nItem: TIntegerField;
    QrV_InfAdProd: TWideMemoField;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabANO_Cli: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAnProt: TWideStringField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAdhRecbto: TWideStringField;
    QrNFeCabAIDCtrl: TIntegerField;
    DsNFeCabA: TDataSource;
    frxDsNFeCabA: TfrxDBDataset;
    QrNFeCabAMsg: TmySQLQuery;
    QrNFeCabAMsgFatID: TIntegerField;
    QrNFeCabAMsgFatNum: TIntegerField;
    QrNFeCabAMsgEmpresa: TIntegerField;
    QrNFeCabAMsgControle: TIntegerField;
    QrNFeCabAMsgSolicit: TIntegerField;
    QrNFeCabAMsgId: TWideStringField;
    QrNFeCabAMsgtpAmb: TSmallintField;
    QrNFeCabAMsgverAplic: TWideStringField;
    QrNFeCabAMsgdhRecbto: TDateTimeField;
    QrNFeCabAMsgnProt: TWideStringField;
    QrNFeCabAMsgdigVal: TWideStringField;
    QrNFeCabAMsgcStat: TIntegerField;
    QrNFeCabAMsgxMotivo: TWideStringField;
    DsNFeCabAMsg: TDataSource;
    frxDsA: TfrxDBDataset;
    QrA: TmySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_NFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAIDCtrl: TIntegerField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrAID_TXT: TWideStringField;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_IEST_TXT: TWideStringField;
    QrAEMIT_CNPJ_TXT: TWideStringField;
    QrADEST_CNPJ_CPF_TXT: TWideStringField;
    QrADEST_ENDERECO: TWideStringField;
    QrADEST_CEP_TXT: TWideStringField;
    QrADEST_FONE_TXT: TWideStringField;
    QrADEST_IE_TXT: TWideStringField;
    QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrATRANSPORTA_IE_TXT: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    QrI: TmySQLQuery;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrITem_IPI: TSmallintField;
    QrInItem: TIntegerField;
    frxDsI: TfrxDBDataset;
    frxDsN: TfrxDBDataset;
    QrN: TmySQLQuery;
    QrNnItem: TIntegerField;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrO: TmySQLQuery;
    QrOnItem: TIntegerField;
    QrOIPI_clEnq: TWideStringField;
    QrOIPI_CNPJProd: TWideStringField;
    QrOIPI_cSelo: TWideStringField;
    QrOIPI_qSelo: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrOIPI_vIPI: TFloatField;
    frxDsO: TfrxDBDataset;
    frxDsY: TfrxDBDataset;
    QrY: TmySQLQuery;
    QrYnDup: TWideStringField;
    QrYdVenc: TDateField;
    QrYvDup: TFloatField;
    QrNFeYIts: TmySQLQuery;
    QrNFeYItsSeq1: TIntegerField;
    QrNFeYItsnDup1: TWideStringField;
    QrNFeYItsdVenc1: TDateField;
    QrNFeYItsvDup1: TFloatField;
    QrNFeYItsSeq2: TIntegerField;
    QrNFeYItsnDup2: TWideStringField;
    QrNFeYItsdVenc2: TDateField;
    QrNFeYItsvDup2: TFloatField;
    QrNFeYItsSeq3: TIntegerField;
    QrNFeYItsnDup3: TWideStringField;
    QrNFeYItsdVenc3: TDateField;
    QrNFeYItsvDup3: TFloatField;
    QrNFeYItsSeq4: TIntegerField;
    QrNFeYItsnDup4: TWideStringField;
    QrNFeYItsdVenc4: TDateField;
    QrNFeYItsvDup4: TFloatField;
    QrNFeYItsSeq5: TIntegerField;
    QrNFeYItsnDup5: TWideStringField;
    QrNFeYItsdVenc5: TDateField;
    QrNFeYItsvDup5: TFloatField;
    QrNFeYItsLinha: TIntegerField;
    QrNFeYItsxVenc1: TWideStringField;
    QrNFeYItsxVenc2: TWideStringField;
    QrNFeYItsxVenc3: TWideStringField;
    QrNFeYItsxVenc4: TWideStringField;
    QrNFeYItsxVenc5: TWideStringField;
    frxDsNFeYIts: TfrxDBDataset;
    frxDsXvol: TfrxDBDataset;
    QrXVol: TmySQLQuery;
    QrXVolqVol: TFloatField;
    QrXVolesp: TWideStringField;
    QrXVolmarca: TWideStringField;
    QrXVolnVol: TWideStringField;
    QrXVolpesoL: TFloatField;
    QrXVolpesoB: TFloatField;
    QrV: TmySQLQuery;
    QrVnItem: TIntegerField;
    QrVInfAdProd: TWideMemoField;
    frxDsV: TfrxDBDataset;
    QrNFeXMLI: TmySQLQuery;
    QrNFeXMLICodigo: TWideStringField;
    QrNFeXMLIID: TWideStringField;
    QrNFeXMLIValor: TWideStringField;
    QrNFeXMLIPai: TWideStringField;
    QrNFeXMLIDescricao: TWideStringField;
    QrNFeXMLICampo: TWideStringField;
    frxDsNFeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_NFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    NFe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    Panel3: TPanel;
    QrNFeCabAdest_xNome: TWideStringField;
    QrAide_DSaiEnt_Txt: TWideStringField;
    QrNFeCabA_versao: TFloatField;
    QrNFeCabAversao: TFloatField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAide_hSaiEnt: TTimeField;
    QrAide_dhCont: TDateTimeField;
    QrAide_xJust: TWideStringField;
    QrAemit_CRT: TSmallintField;
    QrAdest_email: TWideStringField;
    QrAVagao: TWideStringField;
    QrABalsa: TWideStringField;
    QrAMODFRETE_TXT: TWideStringField;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaNFes: TfrxReport;
    N2: TMenuItem;
    Listadasnotaspesquisadas1: TMenuItem;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabANOME_tpEmis: TWideStringField;
    QrNFeCabANOME_tpNF: TWideStringField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFe_100: TmySQLQuery;
    frxDsNFe_100: TfrxDBDataset;
    QrNFe_XXX: TmySQLQuery;
    frxDsNFe_XXX: TfrxDBDataset;
    QrNFe_XXXOrdem: TIntegerField;
    QrNFe_XXXcStat: TIntegerField;
    QrNFe_XXXide_nNF: TIntegerField;
    QrNFe_XXXide_serie: TIntegerField;
    QrNFe_XXXide_AAMM_AA: TWideStringField;
    QrNFe_XXXide_AAMM_MM: TWideStringField;
    QrNFe_XXXide_dEmi: TDateField;
    QrNFe_XXXNOME_tpEmis: TWideStringField;
    QrNFe_XXXNOME_tpNF: TWideStringField;
    QrNFe_XXXStatus: TIntegerField;
    QrNFe_XXXMotivo: TWideStringField;
    QrNFe_100ide_nNF: TIntegerField;
    QrNFe_100ide_serie: TIntegerField;
    QrNFe_100ide_dEmi: TDateField;
    QrNFe_100ICMSTot_vProd: TFloatField;
    QrNFe_100ICMSTot_vST: TFloatField;
    QrNFe_100ICMSTot_vFrete: TFloatField;
    QrNFe_100ICMSTot_vSeg: TFloatField;
    QrNFe_100ICMSTot_vIPI: TFloatField;
    QrNFe_100ICMSTot_vOutro: TFloatField;
    QrNFe_100ICMSTot_vDesc: TFloatField;
    QrNFe_100ICMSTot_vNF: TFloatField;
    QrNFe_100ICMSTot_vBC: TFloatField;
    QrNFe_100ICMSTot_vICMS: TFloatField;
    QrNFe_100NOME_tpEmis: TWideStringField;
    QrNFe_100NOME_tpNF: TWideStringField;
    QrNFe_100Ordem: TIntegerField;
    QrNFe_XXXNOME_ORDEM: TWideStringField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFe_101: TmySQLQuery;
    frxDsNFe_101: TfrxDBDataset;
    QrNFe_101Ordem: TIntegerField;
    QrNFe_101ide_nNF: TIntegerField;
    QrNFe_101ide_serie: TIntegerField;
    QrNFe_101ide_AAMM_AA: TWideStringField;
    QrNFe_101ide_AAMM_MM: TWideStringField;
    QrNFe_101ide_dEmi: TDateField;
    QrNFe_101NOME_tpEmis: TWideStringField;
    QrNFe_101NOME_tpNF: TWideStringField;
    QrNFe_101infCanc_dhRecbto: TDateTimeField;
    QrNFe_101infCanc_nProt: TWideStringField;
    QrNFe_101Motivo: TWideStringField;
    QrNFe_101Id: TWideStringField;
    QrNFe_101Id_TXT: TWideStringField;
    PreviewdaNFe1: TMenuItem;
    QrADEST_XMUN_TXT: TWideStringField;
    QrNFe_100ICMSTot_vPIS: TFloatField;
    QrNFe_100ICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabA_cStat: TFloatField;
    QrNFeCabAcStat: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    BtEvento: TBitBtn;
    BtCancela: TBitBtn;
    BtInutiliza: TBitBtn;
    BtLeArq: TBitBtn;
    BtArq: TBitBtn;
    BtConsulta: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrNICMS_CSOSN: TIntegerField;
    frxA4A_002: TfrxReport;
    Splitter2: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGEventos: TDBGrid;
    TabSheet2: TTabSheet;
    DBGMensagens: TDBGrid;
    QrNFeEveRRet: TmySQLQuery;
    DsNFeEveRRet: TDataSource;
    QrNFeEveRRetControle: TIntegerField;
    QrNFeEveRRetSubCtrl: TIntegerField;
    QrNFeEveRRetret_versao: TFloatField;
    QrNFeEveRRetret_Id: TWideStringField;
    QrNFeEveRRetret_tpAmb: TSmallintField;
    QrNFeEveRRetret_verAplic: TWideStringField;
    QrNFeEveRRetret_cOrgao: TSmallintField;
    QrNFeEveRRetret_cStat: TIntegerField;
    QrNFeEveRRetret_xMotivo: TWideStringField;
    QrNFeEveRRetret_chNFe: TWideStringField;
    QrNFeEveRRetret_tpEvento: TIntegerField;
    QrNFeEveRRetret_xEvento: TWideStringField;
    QrNFeEveRRetret_nSeqEvento: TIntegerField;
    QrNFeEveRRetret_CNPJDest: TWideStringField;
    QrNFeEveRRetret_CPFDest: TWideStringField;
    QrNFeEveRRetret_emailDest: TWideStringField;
    QrNFeEveRRetret_dhRegEvento: TDateTimeField;
    QrNFeEveRRetret_TZD_UTC: TFloatField;
    QrNFeEveRRetret_nProt: TWideStringField;
    QrNFeEveRRetNO_EVENTO: TWideStringField;
    Panel5: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    BtReabre: TBitBtn;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Ck100e101: TCheckBox;
    Panel6: TPanel;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    CGcSitConf: TdmkCheckGroup;
    QrNFeCabAcSitNFe: TSmallintField;
    QrNFeCabAcSitConf: TSmallintField;
    QrNFeCabANO_cSitConf: TWideStringField;
    PMEventos: TPopupMenu;
    CorrigeManifestao1: TMenuItem;
    Panel7: TPanel;
    PMcSitConf: TPopupMenu;
    Manifestar1: TMenuItem;
    frxA4A_003: TfrxReport;
    QrANFeNT2013_003LTT: TSmallintField;
    QrM: TmySQLQuery;
    frxDsM: TfrxDBDataset;
    QrMFatID: TIntegerField;
    QrMFatNum: TIntegerField;
    QrMEmpresa: TIntegerField;
    QrMnItem: TIntegerField;
    QrMiTotTrib: TSmallintField;
    QrMvTotTrib: TFloatField;
    QrNFeCabANFeNT2013_003LTT: TSmallintField;
    mySQLQuery1: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    frxDBDataset1: TfrxDBDataset;
    QrAvTotTrib: TFloatField;
    frxPDFExport: TfrxPDFExport;
    CSTabSheetChamou: TdmkCompoStore;
    QrAInfCpl_totTrib: TWideStringField;
    QrNFeCabANO_Emi: TWideStringField;
    QrNFeCabANO_Terceiro: TWideStringField;
    N3: TMenuItem;
    ImprimeSomenteadmin1: TMenuItem;
    CkideNatOp: TCheckBox;
    frxListaNFesB: TfrxReport;
    QrNFe_100ide_NatOP: TWideStringField;
    QrNFeCabXVol: TmySQLQuery;
    QrNFeCabXVolqVol: TFloatField;
    QrNFeCabXVolesp: TWideStringField;
    QrNFeCabXVolmarca: TWideStringField;
    QrNFeCabXVolnVol: TWideStringField;
    QrNFeCabXVolPesoL: TFloatField;
    QrNFeCabXVolPesoB: TFloatField;
    QrNFe_100qVol: TFloatField;
    QrNFe_100esp: TWideStringField;
    QrNFe_100marca: TWideStringField;
    QrNFe_100nVol: TWideStringField;
    QrNFe_100PesoL: TFloatField;
    QrNFe_100PesoB: TFloatField;
    este1: TMenuItem;
    porNatOp1: TMenuItem;
    PB1: TProgressBar;
    N5: TMenuItem;
    porCFOPtotaiseitensNFe1: TMenuItem;
    porNCMtotaiseitensNFe1: TMenuItem;
    QrNFeCabAemit_CNPJ: TWideStringField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFeCabANO_Cli2: TWideStringField;
    QrIFracio: TSmallintField;
    QrIprod_qCom_TXT: TWideStringField;
    PMMenu: TPopupMenu;
    AbrirNFenoportalnacional1: TMenuItem;
    MostrarchavedeacessodaNFe1: TMenuItem;
    CorrigesrienmeroNFerecebidaspesquisadas1: TMenuItem;
    LaTotal: TLabel;
    Listadefaturasdasnotaspesquisadas1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    EditadadosdeentradaEFDC1701: TMenuItem;
    N4: TMenuItem;
    porCFOPeProdutototaiseitensNFe1: TMenuItem;
    QrNFeCabAinfCCe_nSeqEvento: TIntegerField;
    N6: TMenuItem;
    Abrirfaturamento1: TMenuItem;
    QrNFeCabAide_tpAmb: TSmallintField;
    frxA4A_0031: TfrxReport;
    QrF: TmySQLQuery;
    QrG: TmySQLQuery;
    QrGFatID: TIntegerField;
    QrGFatNum: TIntegerField;
    QrGEmpresa: TIntegerField;
    QrGentrega_CNPJ: TWideStringField;
    QrGentrega_xLgr: TWideStringField;
    QrGentrega_nro: TWideStringField;
    QrGentrega_xCpl: TWideStringField;
    QrGentrega_xBairro: TWideStringField;
    QrGentrega_cMun: TIntegerField;
    QrGentrega_xMun: TWideStringField;
    QrGentrega_UF: TWideStringField;
    QrG_Ativo_: TSmallintField;
    QrGLk: TIntegerField;
    QrGDataCad: TDateField;
    QrGDataAlt: TDateField;
    QrGUserCad: TIntegerField;
    QrGUserAlt: TIntegerField;
    QrGAlterWeb: TSmallintField;
    QrGAtivo: TSmallintField;
    QrGentrega_CPF: TWideStringField;
    QrGAWServerID: TIntegerField;
    QrGAWStatSinc: TSmallintField;
    QrGentrega_xNome: TWideStringField;
    QrGentrega_CEP: TIntegerField;
    QrGentrega_cPais: TIntegerField;
    QrGentrega_xPais: TWideStringField;
    QrGentrega_fone: TWideStringField;
    QrGentrega_email: TWideStringField;
    QrGentrega_IE: TWideStringField;
    QrFFatID: TIntegerField;
    QrFFatNum: TIntegerField;
    QrFEmpresa: TIntegerField;
    QrFretirada_CNPJ: TWideStringField;
    QrFretirada_xLgr: TWideStringField;
    QrFretirada_nro: TWideStringField;
    QrFretirada_xCpl: TWideStringField;
    QrFretirada_xBairro: TWideStringField;
    QrFretirada_cMun: TIntegerField;
    QrFretirada_xMun: TWideStringField;
    QrFretirada_UF: TWideStringField;
    QrF_Ativo_: TSmallintField;
    QrFLk: TIntegerField;
    QrFDataCad: TDateField;
    QrFDataAlt: TDateField;
    QrFUserCad: TIntegerField;
    QrFUserAlt: TIntegerField;
    QrFAlterWeb: TSmallintField;
    QrFAtivo: TSmallintField;
    QrFretirada_CPF: TWideStringField;
    QrFAWServerID: TIntegerField;
    QrFAWStatSinc: TSmallintField;
    QrFretirada_xNome: TWideStringField;
    QrFretirada_CEP: TIntegerField;
    QrFretirada_cPais: TIntegerField;
    QrFretirada_xPais: TWideStringField;
    QrFretirada_fone: TWideStringField;
    QrFretirada_email: TWideStringField;
    QrFretirada_IE: TWideStringField;
    frxDsFouG: TfrxDBDataset;
    QrFretirada_CNPJ_CPF_TXT: TWideStringField;
    QrGentrega_CNPJ_CPF_TXT: TWideStringField;
    QrFretirada_ENDERECO: TWideStringField;
    QrGentrega_ENDERECO: TWideStringField;
    QrFouG: TmySQLQuery;
    QrFouGCNPJ: TWideStringField;
    QrFouGxLgr: TWideStringField;
    QrFouGnro: TWideStringField;
    QrFouGxCpl: TWideStringField;
    QrFouGxBairro: TWideStringField;
    QrFouGcMun: TIntegerField;
    QrFouGxMun: TWideStringField;
    QrFouGUF: TWideStringField;
    QrFouGCPF: TWideStringField;
    QrFouGxNome: TWideStringField;
    QrFouGCEP: TIntegerField;
    QrFouGcPais: TIntegerField;
    QrFouGxPais: TWideStringField;
    QrFouGfone: TWideStringField;
    QrFouGemail: TWideStringField;
    QrFouGIE: TWideStringField;
    QrFouGCNPJ_CPF_TXT: TWideStringField;
    QrFouGENDERECO: TWideStringField;
    QrFouGTITULO: TWideStringField;
    QrFouGCEP_TXT: TWideStringField;
    QrFouGFONE_TXT: TWideStringField;
    QrFouGIE_TXT: TWideStringField;
    frxA4A_0021: TfrxReport;
    porCFOPTipodeprodutpoeProdutototaiseitensNFe1: TMenuItem;
    QrNFeCabAide_mod: TSmallintField;
    BtAutoriza: TBitBtn;
    N7: TMenuItem;
    ExportaparaPDF1: TMenuItem;
    odaspesquisadas1: TMenuItem;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    BtLe2: TBitBtn;
    QrNFeCabAAtrelaStaLnk: TSmallintField;
    QrNFeCabAAtrelaFatNum: TIntegerField;
    QrNFeCabAAtrelaFatID: TIntegerField;
    PMQuery: TPopupMenu;
    LocalizaNfepelachave1: TMenuItem;
    TabSheet3: TTabSheet;
    NFeemAbaFormatado1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    WBXML: TWebBrowser;
    MeXML: TMemo;
    QrNFeCabAFisRegCad: TIntegerField;
    PMNovo: TPopupMenu;
    NFeselecionada1: TMenuItem;
    AlteraraRegraFiscal1: TMenuItem;
    QrAinfEPEC_dhRegEvento: TDateTimeField;
    QrAinfEPEC_dhRegEventoTZD: TFloatField;
    QrAinfEPEC_nProt: TWideStringField;
    QrAinfEPEC_cStat: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure Cancelamento1Click(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BtLeArqClick(Sender: TObject);
    procedure DBGNFesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConsultaClick(Sender: TObject);
    procedure BtInutilizaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxA4A_000_GetValue(const VarName: string; var Value: Variant);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrIAfterScroll(DataSet: TDataSet);
    procedure QrYAfterOpen(DataSet: TDataSet);
    procedure QrNFeYItsCalcFields(DataSet: TDataSet);
    procedure QrXVolAfterOpen(DataSet: TDataSet);
    procedure PMArqPopup(Sender: TObject);
    procedure NFe1Click(Sender: TObject);
    procedure Autorizao1Click(Sender: TObject);
    procedure Cancelamento2Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure DiretriodoarquivoXML1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure Listadasnotaspesquisadas1Click(Sender: TObject);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure QrNFe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaNFesGetValue(const VarName: string; var Value: Variant);
    procedure frxA4A_002GetValue(const VarName: string; var Value: Variant);
    procedure QrNFe_101CalcFields(DataSet: TDataSet);
    procedure PreviewdaNFe1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure QrNFeEveRRetCalcFields(DataSet: TDataSet);
    procedure DBGEventosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMEventosPopup(Sender: TObject);
    procedure CorrigeManifestao1Click(Sender: TObject);
    procedure CGcSitConfClick(Sender: TObject);
    procedure Manifestar1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGNFesDblClick(Sender: TObject);
    procedure ImprimeSomenteadmin1Click(Sender: TObject);
    procedure porNatOp1Click(Sender: TObject);
    procedure porCFOPtotaiseitensNFe1Click(Sender: TObject);
    procedure porNCMtotaiseitensNFe1Click(Sender: TObject);
    procedure QrICalcFields(DataSet: TDataSet);
    procedure AbrirNFenoportalnacional1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure MostrarchavedeacessodaNFe1Click(Sender: TObject);
    procedure CorrigesrienmeroNFerecebidaspesquisadas1Click(Sender: TObject);
    procedure Listadefaturasdasnotaspesquisadas1Click(Sender: TObject);
    procedure EditadadosdeentradaEFDC1701Click(Sender: TObject);
    procedure porCFOPeProdutototaiseitensNFe1Click(Sender: TObject);
    procedure Abrirfaturamento1Click(Sender: TObject);
    procedure QrFouGCalcFields(DataSet: TDataSet);
    procedure porCFOPTipodeprodutpoeProdutototaiseitensNFe1Click(
      Sender: TObject);
    procedure BtAutorizaClick(Sender: TObject);
    procedure odaspesquisadas1Click(Sender: TObject);
    procedure BtLe2Click(Sender: TObject);
    procedure BtArqClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure LocalizaNfepelachave1Click(Sender: TObject);
    procedure NFeemAbaFormatado1Click(Sender: TObject);
    procedure AlteraraRegraFiscal1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    { Private declarations }
    FNFeYIts: String;
    FLote, FEmpresa, FIDCtrl, FideModelo: Integer;
    FChaveNFe, FProtocolo: String;
    // CabY
    FFaturas: array[0..14] of array[0..2] of String;
    // CabXVol
    Fesp, Fmarca, FnVol: String;
    FpesoL, FpesoB, FqVol: Double;
    // Impress�o de lista de NFes
    F_NFe_100, F_NFe_101, F_NFe_XXX: String;
    //
    FSo_O_ID: Boolean;
    //
    procedure DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
    procedure DefineVarsDePreparacao();
    procedure ImprimeNFe(Preview: Boolean);
    procedure ImprimeNFCe(Preview: Boolean);
    procedure ImprimeListaNova(PorCFOP: TNFEAgruRelNFs);
    function  LiberaPor(QuemEmit: TQuemEmit; emit_CNPJ: String;
              MostraMsg: Boolean): Boolean;
    function  ExportaXML_e_DANFE(const CNPJ_CPF: String; var DiretDANFE, DiretXML:
              String; const DirDef: String = ''): Boolean;
  public
    { Public declarations }
    FDANFEImpresso, FMailEnviado: Boolean;
    //
    procedure FechaNFeCabA();
    procedure ReopenNFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
    procedure ReopenNFeCabAMsg();
    procedure ReopenNFeEveRRet();
    //
    function  DefineQual_frxNFe(OQueFaz: TfrxImpComo): TfrxReport;
  end;

  var
  FmNFe_Pesq_0000: TFmNFe_Pesq_0000;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
  NFeInut_0000, UMySQLModule, Module, ModuleNFe_0000, UnGrade_Create,
  //{$IfNDef semNFe_v 0 2 0 0} NFeSteps_0200, {$EndIF}
  {$IfNDef semNFCe_0000}NFCeLEnU_0400, NFCe_PF, {$EndIF}
   NFeXMLGerencia, NFeEveRCab,
  MeuFrx, DmkDAC_PF, UnMailEnv, MyGlyfs, Principal, NFe_PF, UnGFat_Jan,
  NFeImporta_0400, NFe_DownXML;

{$R *.DFM}

procedure TFmNFe_Pesq_0000.BtConsultaClick(Sender: TObject);
var
  ide_mod: Integer;
begin
  PageControl1.ActivePageIndex := 1;
  //
  DefineVarsDePreparacao();
  Geral.ide_mod_de_ChaveNFe(FChaveNFe, ide_mod);
  UnNFe_PF.MostraFormStepsNFe_ConsultaNFe(FEmpresa, FIDCtrl, ide_mod, FChaveNFe, nil);
  //
  ReopenNFeCabA(FIDCtrl, False);
end;

procedure TFmNFe_Pesq_0000.BtImprimeClick(Sender: TObject);
begin
  {
  if QrNfeCabANFeNT2013_003LTT.Value = 3 then
  begin
    MyObjects.frxDefineDataSets(frxA4A_003, [
    frxDsA,
    frxDsI,
    frxDsM,
    frxDsN,
    frxDsNfeYIts,
    frxDsO,
    frxDsV,
    frxDsY
    ]);
    DefineFrx(frxA4A_003, ficMostra);
  end else
    DefineFrx(frxA4A_002, ficMostra);
  }
  //DefineQual_frxNFe(ficMostra);
  if QrNFeCabAide_mod.Value = CO_MODELO_NFE_55 then
  begin
    if QrNFeCabAcStat.Value = 485 then
      //485:
      Geral.MB_Info(
      'Como n�o existe WS para consultar EPEC j� autorizada, ' +
      'e, o CNPJ e o n�mero da NFe est�o na consulta ent�o ' +
      'podemos considerara que o EPEC existe no servidor. ' +
      'Por�m n�o vem o protocolo que � necessario para ' +
      'imprimir o DANFE. Ent�o � necess�rio fazer outra nota ' +
      'substituindo essa. Tamb�m � necesssario autorizar ' +
      'essa nota (com status 485) e depois cancelar ela! ')
    else
      // 35 (off-line), 100, 101, 136 (EPEC), VAR_FATID_0053 (Recebidas) etc.
      ImprimeNFe(False);
    //end;
  end else
  if QrNFeCabAide_mod.Value = CO_MODELO_NFE_65 then
    ImprimeNFCe(True)
  else
    Geral.MB_Info('Modelo de notra fiscal n�o definido ou n�o implemetado: ' +
    Geral.FF0(QrNFeCabAide_mod.Value));
end;

procedure TFmNFe_Pesq_0000.DBGEventosMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMEventos, DBGEventos, X,Y);
end;

procedure TFmNFe_Pesq_0000.DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
var
  I: Integer;
  //Page: TfrxReportPage;
begin
  Screen.Cursor := crHourGlass;
  //
  DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
  //
  QrA.Close;
  QrA.Database := Dmod.MyDB;
  QrA.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrA.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrA.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrF.Close;
  QrF.Database := Dmod.MyDB;
  QrF.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrF.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrF.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrF, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrG.Close;
  QrG.Database := Dmod.MyDB;
  QrG.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrG.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrG.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrG, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrI.Close;
  QrI.Database := Dmod.MyDB;
  QrI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrY.Close;
  QrY.Database := Dmod.MyDB;
  QrY.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrY.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrY.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrY, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrXVol.Close;
  QrXVol.Database := Dmod.MyDB;
  QrXVol.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrXVol.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrXVol.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrXVol, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  case OQueFaz of
    ficMostra: //MyObjects.frxMostra(Report, 'NFe' + QrAId.Value);
    begin
      Report.PreviewOptions.ZoomMode := zmPageWidth;
      MyObjects.frxPrepara(Report, 'NFe' + QrAId.Value);
      for I := 0 to Report.PreviewPages.Count -1 do
      begin
        Report.PreviewPages.Page[I].PaperSize := DMPAPER_A4;
        Report.PreviewPages.ModifyPage(I, Report.PreviewPages.Page[I]);
      end;
      FmMeuFrx.BtEdit.Enabled := False;
      FmMeuFrx.ShowModal;
      FmMeuFrx.Destroy;
      FDANFEImpresso := True;
    end;
    {ficImprime:
    ficSalva}
    ficNone: ;// N�o faz nada!
    ficExporta: MyObjects.frxPrepara(Report, 'NFe' + QrAId.Value);
    else Geral.MensagemBox('A��o n�o definida para frx da DANFE!',
    'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.BtInutilizaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeInut_0000, FmNFeInut_0000, afmoNegarComAviso) then
  begin
    FmNFeInut_0000.FFilial := EdFilial.ValueVariant;
    FmNFeInut_0000.ShowModal;
    FmNFeInut_0000.Destroy;
  end;
end;

procedure TFmNFe_Pesq_0000.Abrirfaturamento1Click(Sender: TObject);
var
  CodUsu: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT pvd.CodUsu',
      'FROM fatpedcab fpc',
      'LEFT JOIN pedivda pvd ON fpc.Pedido = pvd.Codigo',
      'WHERE fpc.Codigo=' + Geral.FF0(QrNFeCabAFatNum.Value),
      '']);
    if Qry.RecordCount > 0 then
    begin
      CodUsu := Qry.FieldByName('CodUsu').AsInteger;
      //
      if CodUsu > 0 then
        GFat_Jan.MostraFormFatPedCab(QrNFeCabAFatNum.Value)
      else
        GFat_Jan.MostraFormFatDivGer(QrNFeCabAFatNum.Value);
    end else
      Geral.MB_Aviso('N�o foi poss�vel localizar o faturamento!');
  finally
    Qry.Free;
  end;
end;

procedure TFmNFe_Pesq_0000.AbrirNFenoportalnacional1Click(Sender: TObject);
begin
  UnNFe_PF.MostraNFeNoPortalNacional(QrNFeCabAide_tpAmb.Value, QrNFeCabAId.Value);
end;

procedure TFmNFe_Pesq_0000.AlteraraRegraFiscal1Click(Sender: TObject);
const
  MostraDados = False;
var
  Atual, FatID, FatNum, Empresa, FisRegCad, IDCtrl: Integer;
  //
  function SelecionaFisRegCad(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Regra Fiscal';
    Prompt = 'Informe a Regra: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Codigo: Variant;
  begin
    Result := False;
    Atual := QrNFeCabAFisRegCad.Value;
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Atual,
      ['SELECT Codigo, Nome Descricao ',
      'FROM fisregcad ',
      'ORDER BY Descricao '], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      FisRegCad := Codigo;
      Result    := True;
      //
    end;
  end;
begin
  if SelecionaFisRegCad() then
  begin
    IDCtrl   := QrNfeCabAIDCtrl.Value;
    FatID    := QrNfeCabAFatID.Value;
    FatNum   := QrNfeCabAFatNum.Value;
    Empresa  := QrNfeCabAEmpresa.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'FisRegCad'], [
    'FatID', 'FatNum', 'Empresa'], [
    FisRegCad], [
    FatID, FatNum, Empresa], True) then
      ReopenNFeCabA(IDCtrl, False);
  end;
end;

procedure TFmNFe_Pesq_0000.Autorizao1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Aut.Value, 'XML Autoriza��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.BtReabreClick(Sender: TObject);
var
  IDCtrl: Integer;
begin
  IDCtrl := 0;
  if (QrNFeCabA.State <> dsInactive) then IDCtrl := QrNFeCabAIDCtrl.Value;
  ReopenNFeCabA(IDCtrl, False);
end;

procedure TFmNFe_Pesq_0000.BtEnviaClick(Sender: TObject);

  procedure ExportaXMLeDANFE(const CNPJ_CPF: String; var DiretDANFE, DiretXML: String);
  var
    Id, ArqDANFE, ArqXML, DirNFeXML, DirDANFEs, XML_Distribuicao: String;
    Frx: TfrxReport;
    ExportouDANFE, ExportouXML: Boolean;
  begin
    ExportouDANFE := False;
    ExportouXML   := False;
    DiretDANFE    := '';
    DiretXML      := '';
    Id            := Geral.SoNumero_TT(QrNFeCabAId.Value);
    DirNFeXML     := DmNFe_0000.QrFilialDirNFeProt.Value;
    DirDANFEs     := DmNFe_0000.QrFilialDirDANFEs.Value;
    //
    if DirNFeXML = '' then
      DirNFeXML := CO_DIR_RAIZ_DMK + '\NFe\' + Geral.FF0(VAR_USUARIO) + '\Clientes\';
    if DirDANFEs = '' then
      DirDANFEs := CO_DIR_RAIZ_DMK + '\NFe\' + Geral.FF0(VAR_USUARIO) + '\Clientes\';
    //
    Geral.VerificaDir(DirNFeXML, '\', '', True);
    DirNFeXML := DirNFeXML + CNPJ_CPF;
    Geral.VerificaDir(DirDANFEs, '\', '', True);
    DirDANFEs := DirDANFEs + CNPJ_CPF;
    //
    // DANFE
    //
    if QrNFeCabAcStat.Value = 100 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo PDF do DANFE');
      //
      Frx      := DefineQual_frxNFe(ficNone);
      ArqDANFE := DirDANFEs;
      //
      Geral.VerificaDir(ArqDANFE, '\', 'Diret�rio tempor�rio do DANFE', True);
      //
      if DModG.QrPrmsEmpNFeNoDANFEMail.Value = 0 then
        ArqDANFE := ArqDANFE + Id + '.pdf'
      else
        ArqDANFE := ArqDANFE + 'NF ' + Geral.FF0(QrNFeCabAide_nNF.Value) + ' ' +
                    Geral.SoNumeroELetra_TT(QrNFeCabANO_Cli2.Value) + '.pdf';
      //
      try
        frxPDFExport.FileName := ArqDANFE;
        MyObjects.frxPrepara(Frx, 'NFe' + ID);
        Frx.Export(frxPDFExport);
        //
        ExportouDANFE := True;
      except
        ExportouDANFE := False;
      end;
    end;
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da NFe');
    //
    ArqXML := DirNFeXML;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
    //
    ArqXML := ArqXML + ID + '.xml';
    //
    DmNFe_0000.QrArq.Close;
    DmNFe_0000.QrArq.Database := Dmod.MyDB;
    DmNFe_0000.QrArq.Params[0].AsInteger := QrNFeCabAIDCtrl.Value;
    UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //
    if NFeXMLGeren.XML_DistribuiNFe(Id, Trunc(QrNFeCabAcStat.Value),
      DmNFe_0000.QrArqXML_NFe.Value, DmNFe_0000.QrArqXML_Aut.Value,
      DmNFe_0000.QrArqXML_Can.Value, True, XML_Distribuicao,
      QrNFeCabAversao.Value, 'na base de dados')
    then
      ExportouXML := NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    if ExportouXML then
      DiretXML := ArqXML;
    if ExportouDANFE then
      DiretDANFE := ArqDANFE;
  end;
var
  CNPJ_CPF, DANFE, XML: String;
  Entidade: Integer;
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  if QrNFeCabAcStat.Value <> 100 then
  begin
    Geral.MB_Aviso('Esta nota n�o pode ser enviada por e-mail!' + sLineBreak +
      'Motivo: O statu da nota difere de 100!');
  end;
  DmNFe_0000.ReopenOpcoesNFe(DModG.QrEmpresasCodigo.Value, True);
  //
  if QrNFeCabAdest_CNPJ.Value <> '' then
    CNPJ_CPF := QrNFeCabAdest_CNPJ.Value
  else
    CNPJ_CPF := QrNFeCabAdest_CPF.Value;
  //
  ExportaXMLeDANFE(CNPJ_CPF, DANFE, XML);
  //
  CNPJ_CPF := Geral.SoNumero_TT(CNPJ_CPF);
  //
  DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
  //
  if MyObjects.FIC(DANFE = '', nil, 'Falha ao exportar DANFE!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  //
  if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  //
  if UMailEnv.Monta_e_Envia_Mail([Entidade, DModG.QrEmpresasCodigo.Value,
    QrNFeCabAcStat.Value], meNFe, [DANFE, XML], [QrNFeCabAdest_xNome.Value,
    QrNFeCabAId.Value, Geral.FF0(QrNFeCabAide_serie.Value),
    Geral.FF0(QrNFeCabAide_nNF.Value), QrNFeCabANO_Cli2.Value], True) then
  begin
    FMailEnviado := True;
    Geral.MB_Aviso('E-mail enviado com sucesso!');
  end;
end;

procedure TFmNFe_Pesq_0000.BtEventoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeEveRCab, FmNFeEveRCab, afmoNegarComAviso) then
  begin
    FmNFeEveRCab.ReopenNfeCabA(
      QrNFeCabAFatID.Value, QrNFeCabAFatNum.Value, QrNFeCabAEmpresa.Value);
    FmNFeEveRCab.ShowModal;
    FmNFeEveRCab.Destroy;
    //
    Screen.Cursor := crHourGlass;
    try
      ReopenNFeCabA(0, False);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.BtExcluiClick(Sender: TObject);

  function VerificaSeNumeroFoiInutilizado(NFeNum: Integer): Boolean;
  begin
    //True  = Libera Exclus�o
    //False = Bloqueia Exclus�o
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT nNFIni, nNFFim ',
      'FROM nfeinut ',
      'WHERE ' + Geral.FF0(NFeNum) + ' BETWEEN nNFIni AND nNFFim ',
      '']);
    if DModG.QrAux.RecordCount > 0 then
      Result := True;
  end;

var
  Status, FatID, FatNum, Empresa, ide_nNF: Integer;
  ChaveNFe: String;
  Continua: Boolean;
begin
  if not LiberaPor(tqeAmbos, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  if (QrNFeCabA.State = dsInactive) or (QrNFeCabA.RecordCount = 0) then Exit;
  //
  Continua := False;
  Status   := Trunc(QrNFeCabAcStat.Value);
  FatID    := QrNFeCabAFatID.Value;
  FatNum   := QrNFeCabAFatNum.Value;
  Empresa  := QrNFeCabAEmpresa.Value;
  ChaveNFe := QrNFeCabAId.Value;
  //
  if Geral.MB_Pergunta('Deseja realmente excluir a NFe:' + sLineBreak +
    'S�rie: ' + FormatFloat('000', QrNFeCabAide_serie.Value) + sLineBreak +
    'N�: ' + FormatFloat('000', QrNFeCabAide_nNF.Value) + sLineBreak + sLineBreak +
    'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!') = ID_YES then
  begin
    if (QrNFeCabAFatId.Value = VAR_FATID_0053) and (QrNFeCabAcStat.Value = 0) then //Recebidas
      Continua := True
    else
      Continua := VerificaSeNumeroFoiInutilizado(QrNFeCabAide_nNF.Value);
    //
    if Continua then
    begin
      if DmNFe_0000.CancelaFaturamento(ChaveNFe) then //Primeiro exclui o faturamento e estoque depois a nota
      begin
        DmNFe_0000.ExcluiNfe(Status, FatID, FatNum, Empresa, True, True);
        //
        QrNfeCabA.Next;
        ide_nNF := UMyMod.ProximoRegistro(QrNFeCabA, 'ide_nNF', QrNFeCabAide_nNF.Value);
        //
        BtReabreClick(Self);
        QrNfeCabA.Locate('ide_nNF', ide_nNF, []);
        Geral.MensagemBox('NFe exclu�da com sucesso!',
        'Informa��o', MB_OK+MB_ICONINFORMATION);
      end;
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este n�mero n�o foi inutilizado!' + sLineBreak +
        'Inutilize este n�mero e tente novamente!');
  end;
end;

procedure TFmNFe_Pesq_0000.BtLe2Click(Sender: TObject);
(*
const
  FTabLcta = 'lct0001a';
  NFeCabA: TNFeCabA = nil;
  AvisaFaltaStatus100: Boolean = True;
var
  Pasta, Arquivo: String;
  Texto: WideString;
var
  FatID, FatNum, Empresa, IDCtrl, CodInfoEmit, Transporta, cab_qVol: Integer;
  cab_PesoB, cab_PesoL: Double;
  DataFiscal: TDateTime;
  //TabLcta: String;
  //NFeCabA: TNFeCabA = nil;
*)
var
  IDCtrl: Integer;
begin
  IDCtrl := QrNFeCabAIDCtrl.Value;
  //if QrNFeCabAFatID.Value = VAR_FATID_0053 then
  begin
    if DBCheck.CriaFm(TFmNFe_DownXML, FmNFe_DownXML, afmoNegarComAviso) then
    begin
      //FmNFe_DownXML.FEmpresa      := QrNFeCabAEmpresa.Value;
      FmNFe_DownXML.FArqUnico     := QrNFeCabAID.Value + '.XML';
      FmNFe_DownXML.FJanela       := 'FmNFe_Pesq_0000';
      FmNFe_DownXML.FFatIdUnico   := QrNFeCabAFatID.Value;
      FmNFe_DownXML.FFatNumUnico  := QrNFeCabAFatNum.Value;
      FmNFe_DownXML.FEmpresaUnica := QrNFeCabAEmpresa.Value;
      //
      FmNFe_DownXML.ShowModal;
      IDCtrl := FmNFe_DownXML.FIDCtrl;
      FmNFe_DownXML.Destroy;
    end;
    ReopenNFeCabA(IDCtrl, False);
  end;
end;

procedure TFmNFe_Pesq_0000.BtArqClick(Sender: TObject);
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  QrArq.Close;
  QrArq.Database := Dmod.MyDB;
  QrArq.Params[0].AsInteger := QrNFeCabAIDCtrl.Value;
  UnDmkDAC_PF.AbreQuery(QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
  //
  MyObjects.MostraPopUpDeBotao(PMArq, BtArq);
end;

procedure TFmNFe_Pesq_0000.BtAutorizaClick(Sender: TObject);
begin
{$IfNDef semNFCe_0000}
  UnNFCe_PF.MostraFormNFCeLEnU(Self, QrNFeCabAide_tpEmis.Value, QrNFeCabAStatus.Value);
  ReopenNFeCabA(QrNFeCabAIDCtrl.Value, False);
{$Else}
  Geral.MB_Info('NFC-e n�o habilitado para este ERP');
{$EndIf};
end;

procedure TFmNFe_Pesq_0000.BtCancelaClick(Sender: TObject);
begin
  // Eliminado da NFe
  // Fazer Evento de cancelamento autom�tico???
  //UnNFe_PF.MostraFormStepsNFe_CancelaNFe();
end;

procedure TFmNFe_Pesq_0000.BtLeArqClick(Sender: TObject);
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMLeArq, BtLeArq);
end;

procedure TFmNFe_Pesq_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTROX := QrNFeCabAId.Value;
  VAR_CADASTRO  := QrNFeCabAIDCtrl.Value;
  //
  if TFmNFe_Pesq_0000(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFe_Pesq_0000.CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  QrNFeXMLI.Close;
  QrNFeXMLI.Database := Dmod.MyDB;
  QrNFeXMLI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeXMLI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeXMLI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeXMLI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  MyObjects.frxDefineDataSets(frxCampos, [
    DModG.frxDsDono,
    frxDsNFeCabA,
    frxDsNFeXMLI
    ]);
  MyObjects.frxMostra(frxCampos, 'Campos preenchidos no XML');
end;

procedure TFmNFe_Pesq_0000.Cancelamento1Click(Sender: TObject);
const
  SohLer = True;
begin
  //UnNFe_PF.MostraFormStepsNFe_CancelaNFe(SohLer);
end;

procedure TFmNFe_Pesq_0000.Cancelamento2Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Can.Value, 'XML Cancelamento', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.CGcSitConfClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.CkEmitClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.CorrigeManifestao1Click(Sender: TObject);
var
  IDCtrl, cSitConf: Integer;
begin
  case QrNFeEveRRetret_tpEvento.Value of
    NFe_CodEventoMDeConfirmacao, // = 210200;
    NFe_CodEventoMDeCiencia    , // = 210210;
    NFe_CodEventoMDeDesconhece , // = 210220;
    NFe_CodEventoMDeNaoRealizou: // = 210240;
    begin
      if (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfNaoConsultada)
      or (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfMDeSemManifestacao) then
      begin
        if QrNFeEveRRetret_cStat.Value = 135 then
        begin
          IDCtrl   := QrNFeCabAIDCtrl.Value;
          cSitConf := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(
            QrNFeEveRRetret_tpEvento.Value);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
          (*'cSitNFe',*) 'cSitConf'
          ], [
          'IDCtrl'
          ], [
          (*cSitNFe,*) cSitConf
          ], [
          IDCtrl], True) then
            ReopenNFeCabA(IDCtrl, FSo_O_ID);
        end
        else Geral.MB_Aviso('O evento selecionado n�o foi vinculado na sefaz!');
      end
      else Geral.MB_Aviso('O evento selecionado n�o permite altera��o!');
    end
    else Geral.MB_Aviso('N�o h� implementa��o para o evento selecionado!');
  end;
end;

procedure TFmNFe_Pesq_0000.CorrigesrienmeroNFerecebidaspesquisadas1Click(
  Sender: TObject);
var
  Id: String;
  IDCtrl, I, ide_serie, ide_nNF: Integer;
begin
  QrNFeCabA.First;
  while not QrNFeCabA.Eof do
  begin
    Id := QrNFeCabAId.Value;
    //
    if (QrNFeCabAide_nNF.Value = 0) and (Length(Id) = 44) then
    begin
      IDCtrl := QrNFeCabAIDCtrl.Value;
      //
      UnNFe_PF.ObtemSerieNumeroByID(Id, ide_serie, ide_nNF);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
        'ide_Serie', 'ide_nNF'], [
        'IDCtrl', 'Id'], [
        ide_Serie, ide_nNF], [
        IDCtrl, Id], True);
    end;
    QrNFeCabA.Next;
  end;
  ReopenNFeCabA(0, False);
end;

procedure TFmNFe_Pesq_0000.DefineVarsDePreparacao();
begin
  FLote      := QrNFeCabALoteEnv.Value;
  FEmpresa   := QrNFeCabAEmpresa.Value;
  FChaveNFe  := QrNFeCabAId.Value;
  FProtocolo := QrNFeCabAnProt.Value;
  FIDCtrl    := QrNFeCabAIDCtrl.Value;
  //Geral.FatID_To_ide_mod(QrNFeCabAFatID.Value, Fide_mod);
end;

function TFmNFe_Pesq_0000.DefineQual_frxNFe(OQueFaz: TfrxImpComo): TfrxReport;
var
  FouG: Boolean;
begin
  FDANFEImpresso := False;
  QrF.Close;
  QrF.Database := Dmod.MyDB;
  QrF.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrF.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrF.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrF, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
  //
  QrG.Close;
  QrG.Database := Dmod.MyDB;
  QrG.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrG.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrG.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrG, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
  //
  FouG := (QrF.RecordCount > 0) or (QrG.RecordCount > 0);
  if FouG then
  begin
    if QrF.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFouG, Dmod.MyDB, [
      'SELECT "INFORMA��ES DO LOCAL DE RETIRADA" TITULO, ',
      'retirada_CNPJ CNPJ, retirada_xLgr xLgr, retirada_nro nro, ',
      'retirada_xCpl xCpl, retirada_xBairro xBairro, retirada_cMun cMun, ',
      'retirada_xMun xMun, retirada_UF UF, retirada_CPF CPF, ',
      'retirada_xNome xNome, retirada_CEP CEP, retirada_cPais cPais, ',
      'retirada_xPais xPais, retirada_fone fone, retirada_email email, ',
      'retirada_IE IE ',
      'FROM nfecabf ',
      'WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
      'AND FatNum='  + Geral.FF0(QrNFeCabAFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
      '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFouG, Dmod.MyDB, [
      'SELECT "INFORMA��ES DO LOCAL DE ENTREGA" TITULO, ',
      'entrega_CNPJ CNPJ, entrega_xLgr xLgr, entrega_nro nro, ',
      'entrega_xCpl xCpl, entrega_xBairro xBairro, entrega_cMun cMun, ',
      'entrega_xMun xMun, entrega_UF UF, entrega_CPF CPF, ',
      'entrega_xNome xNome, entrega_CEP CEP, entrega_cPais cPais, ',
      'entrega_xPais xPais, entrega_fone fone, entrega_email email, ',
      'entrega_IE IE ',
      'FROM nfecabg ',
      'WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
      'AND FatNum='  + Geral.FF0(QrNFeCabAFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
      '']);
    end;
  end;
  //
  if QrNfeCabANFeNT2013_003LTT.Value = 3 then
  begin
    if FouG then
    begin
      MyObjects.frxDefineDataSets(frxA4A_0031, [
      frxDsA,
      frxDsFouG,
      frxDsI,
      frxDsM,
      frxDsN,
      frxDsNfeYIts,
      frxDsO,
      frxDsV,
      frxDsY
      ]);
      DefineFrx(frxA4A_0031, OQueFaz);
      Result := frxA4A_0031;
    end else
    begin
      MyObjects.frxDefineDataSets(frxA4A_003, [
      frxDsA,
      frxDsI,
      frxDsM,
      frxDsN,
      frxDsNfeYIts,
      frxDsO,
      frxDsV,
      frxDsY
      ]);
      DefineFrx(frxA4A_003, OQueFaz);
      Result := frxA4A_003;
    end;
  end else
  begin
    if FouG then
    begin
      MyObjects.frxDefineDataSets(frxA4A_0021, [
      frxDsA,
      frxDsFouG,
      frxDsI,
      frxDsM,
      frxDsN,
      frxDsNfeYIts,
      frxDsO,
      frxDsV,
      frxDsY
      ]);
      DefineFrx(frxA4A_0021, OQueFaz);
      Result := frxA4A_0021;
    end else
    begin
      MyObjects.frxDefineDataSets(frxA4A_002, [
      frxDsA,
      frxDsI,
      frxDsM,
      frxDsN,
      frxDsNfeYIts,
      frxDsO,
      frxDsV,
      frxDsY
      ]);
      DefineFrx(frxA4A_002, OQueFaz);
      Result := frxA4A_002;
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.DiretriodoarquivoXML1Click(Sender: TObject);
var
  Dir, Arq, Ext, Arquivo: String;
begin
  if not DmNFe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value) then Exit;
  //
  Arq := QrNFeCabAId.Value;
  Ext := NFE_EXT_NFE_XML;
  //
  if DmNFe_0000.ObtemDirXML(Ext, Dir, True) then
  begin
    Arquivo := Dir + Arq + Ext;
    Geral.MensagemBox(Arquivo, 'Caminho do arquivo XML', MB_OK+MB_ICONWARNING);
  end; 
end;

procedure TFmNFe_Pesq_0000.DBGNFesDblClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo = 'Descricao';
var
  Fld: String;
  FisRegCad: Variant;
  IDCtrl: Integer;
begin
  Fld := Uppercase(DBGNFes.Columns[THackDBGrid(DBGNFes).Col -1].FieldName);
  //
  if Fld = Uppercase('Id') then
  begin
    if (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0) then
      UnNFe_PF.MostraNFeChave(QrNFeCabAId.Value);
  end else
  if Fld = Uppercase('FisRegCad') then
  begin
    if DBCheck.LiberaPelaSenhaBoss() then
    begin

      FisRegCad := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT Codigo, Nome ' + Campo,
        'FROM fisregcad ',
        'WHERE Codigo <> 0',
        'ORDER BY ' + Campo,
        ''], Dmod.MyDB, True);
      //
      if FisRegCad <> Null then
      begin
        IDCtrl := QrNFeCabAIDCtrl.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
        'FisRegCad'], ['IDCtrl'], [
        FisRegCad], [IDCtrl], True) then
        begin
          UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB);
          QrNFeCabA.Locate('IDCtrl', IDCtrl, []);
        end;
      end;
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.DBGNFesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(DBGNFes), Rect, DataCol, Column,
    State, Trunc(QrNFeCabAcStat.Value), QrNFeCabAide_mod.Value);
  //
  DmNFe_0000.ColoreinfCCe_nSeqEventoEmDBGrid(TDBGrid(DBGNFes), Rect, DataCol, Column,
    State, QrNFeCabAinfCCe_nSeqEvento.Value);
  //
  DmNFe_0000.ColoreSitConfNFeEmDBGrid(TDBGrid(DBGNFes), Rect, DataCol, Column,
    State, QrNFeCabAcSitConf.Value);
end;

procedure TFmNFe_Pesq_0000.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdClienteExit(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdFilialChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EdFilialExit(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.EditadadosdeentradaEFDC1701Click(Sender: TObject);
begin
{}
  UnNFe_PF.MostraFormNFeEFC_C170_Cab(QrNFeCabAFatID.Value,
    QrNFeCabAFatNum.Value, QrNFeCabAEmpresa.Value);
{}
end;

function TFmNFe_Pesq_0000.ExportaXML_e_DANFE(const CNPJ_CPF: String; var
DiretDANFE, DiretXML: String; const DirDef: String = ''): Boolean;
var
  Id, ArqDANFE, ArqXML, DirNFeXML, DirDANFEs, XML_Distribuicao: String;
  Frx: TfrxReport;
  ExportouDANFE, ExportouXML: Boolean;
begin
  Result := False;
  //
  ExportouDANFE := False;
  ExportouXML   := False;
  DiretDANFE    := '';
  DiretXML      := '';
  Id            := Geral.SoNumero_TT(QrNFeCabAId.Value);
  DirNFeXML     := DmNFe_0000.QrFilialDirNFeProt.Value;
  if DirDef <> '' then
  begin
    DirDANFEs     := DirDef + '\DANFEs\';
    DirNFeXML     := DirDef + '\XMLs\';
    ForceDirectories(DirDANFEs);
    ForceDirectories(DirNFeXML);
  end else
  begin
    DirDANFEs     := DmNFe_0000.QrFilialDirDANFEs.Value;
    //
    if DirNFeXML = '' then
      DirNFeXML := CO_DIR_RAIZ_DMK + '\NFe\' + Geral.FF0(VAR_USUARIO) + '\Clientes\';
    if DirDANFEs = '' then
      DirDANFEs := CO_DIR_RAIZ_DMK + '\NFe\' + Geral.FF0(VAR_USUARIO) + '\Clientes\';
    //
    Geral.VerificaDir(DirNFeXML, '\', '', True);
    DirNFeXML := DirNFeXML + CNPJ_CPF;
    Geral.VerificaDir(DirDANFEs, '\', '', True);
    DirDANFEs := DirDANFEs + CNPJ_CPF;
  end;
  //
  // DANFE
  //
  if QrNFeCabAcStat.Value = 100 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo PDF do DANFE ' +
      Geral.FF0(QrNFeCabAide_nNF.Value) + ' de ' + QrNFeCabAdhRecbto.Value);
    //
    Frx      := DefineQual_frxNFe(ficNone);
    ArqDANFE := DirDANFEs;
    //
    Geral.VerificaDir(ArqDANFE, '\', 'Diret�rio tempor�rio do DANFE', True);
    //
    if DModG.QrPrmsEmpNFeNoDANFEMail.Value = 0 then
      ArqDANFE := ArqDANFE + Id + '.pdf'
    else
      ArqDANFE := ArqDANFE + 'NF ' + Geral.FF0(QrNFeCabAide_nNF.Value) + ' ' +
                  Geral.SoNumeroELetra_TT(QrNFeCabANO_Cli2.Value) + '.pdf';
    //
    try
      frxPDFExport.FileName := ArqDANFE;
      MyObjects.frxPrepara(Frx, 'NFe' + ID);
      Frx.Export(frxPDFExport);
      if FmMeuFrx <> nil then
        FmMeuFrx.Destroy;
      //
      ExportouDANFE := True;
    except
      ExportouDANFE := False;
    end;
    Result := ExportouDANFE;
  end;
  //
{
  // XML
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da NFe');
  //
  ArqXML := DirNFeXML;
  //
  Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
  //
  ArqXML := ArqXML + ID + '.xml';
  //
  DmNFe_0000.QrArq.Close;
  DmNFe_0000.QrArq.Database := Dmod.MyDB;
  DmNFe_0000.QrArq.Params[0].AsInteger := QrNFeCabAIDCtrl.Value;
  UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  if NFeXMLGeren.XML_DistribuiNFe(Id, Trunc(QrNFeCabAcStat.Value),
    DmNFe_0000.QrArqXML_NFe.Value, DmNFe_0000.QrArqXML_Aut.Value,
    DmNFe_0000.QrArqXML_Can.Value, True, XML_Distribuicao,
    QrNFeCabAversao.Value, 'na base de dados')
  then
    ExportouXML := NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  if ExportouXML then
    DiretXML := ArqXML;
  if ExportouDANFE then
    DiretDANFE := ArqDANFE;
}
end;

procedure TFmNFe_Pesq_0000.FormActivate(Sender: TObject);
begin
  if TFmNFe_Pesq_0000(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFe_Pesq_0000.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ImgTipo.SQLType := stPsq;
  FDANFEImpresso := False;
  FMailEnviado := False;
  PageControl1.ActivePageIndex := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT Codigo, Tipo, CNPJ, CPF, ',
  'IF(Tipo=0, RazaoSocial, Nome) NO_Enti ',
  'FROM Entidades ',
  'WHERE Cliente1="V" ',
  'OR Cliente2="V" ',
  'OR Cliente3="V" ',
  'OR Cliente4="V" ',
  'OR Fornece1="V" ',
  'OR Fornece2="V" ',
  'OR Fornece3="V" ',
  'OR Fornece4="V" ',
  'OR Fornece5="V" ',
  'OR Fornece6="V" ',
  'OR Fornece7="V" ',
  'OR Fornece8="V" ',
  'ORDER BY NO_Enti ',
  '']);
  TPDataI.Date := Date - 10; // m�x permitido pelo fisco
  TPDataF.Date := Date;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  CBFilial.ListSource := DModG.DsEmpresas;
  RGAmbiente.ItemIndex := DModG.QrPrmsEmpNFeide_tpAmb.Value;
  //
  ReopenNFeCabA(0, False);
  //
  QrNFeYIts.Database := DModG.MyPID_DB;
  //
  CGcSitConf.SetMaxValue();
  //
  DBGNFes.PopupMenu := PMMenu;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFe_Pesq_0000.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFe_Pesq_0000.frxA4A_000_GetValue(const VarName: string; var Value: Variant);
{
var
  I, J: Integer;
}
begin

{
  if Copy(VarName, 1, 9) = 'VARF_FAT_' then
  begin
    I := StrToInt(Copy(VarName, 10, 2));
    J := StrToInt(Copy(VarName, 13, 2));
    //
    Value := FFaturas[I,J];
  end;
}
end;

procedure TFmNFe_Pesq_0000.frxA4A_002GetValue(const VarName: string;
  var Value: Variant);
var
  Texto: String;
begin
  if VarName = 'VARF_XVOL_ESP' then Value := Fesp else
  if VarName = 'VARF_XVOL_MARCA' then Value := Fmarca else
  if VarName = 'VARF_XVOL_NVOL' then Value := FnVol else
  if VarName = 'VARF_XVOL_PESOL' then Value := FpesoL else
  if VarName = 'VARF_XVOL_PESOB' then Value := FpesoB else
  if VarName = 'VARF_XVOL_QVOL' then Value := FqVol else
  if VarName = 'LogoNFeExiste' then Value := FileExists(DModG.QrPrmsEmpNFePathLogoNF.Value) else
  if VarName = 'LogoNFePath' then Value := DModG.QrPrmsEmpNFePathLogoNF.Value else
  if VarName = 'NFeItsLin' then
  begin
    case DModG.QrPrmsEmpNFeNFeItsLin.Value of
      1:   Value := 12.47244;    // 1 linha
      2:   Value := 21.54331;    // 2 linhas
      else Value := 32.12598;  // 3 linhas
    end;
  end;
  //
  if VarName = 'VAR_NFE_RECEBIDA' then
  begin
    if QrNFeCabAFatId.Value = VAR_FATID_0053 then //Recebidas
      Value := True
    else
      Value := False;
  end;
  //
  if VarName = 'VAR_MSG_01' then
  begin
    if QrNFeCabAcStat.Value = 101 then
      Value := 'NF-E CANCELADA'
    else if QrNFeCabAFatId.Value = VAR_FATID_0053 then //Recebidas
      Value := 'NF-E IMPORTADA'
    else
      Value := ' SEM VALOR';
  end;
  if VarName = 'VAR_MSG_02' then
  begin
    if QrNFeCabAcStat.Value = 101 then
      Value := 'CANCELADA'
    else if QrNFeCabAFatId.Value = VAR_FATID_0053 then //Recebidas
      Value := 'IMPORTADA'
    else
      Value := '   PREVIEW';
  end;
  if VarName = 'VAR_MSG_03' then
  begin
    if QrNFeCabAcStat.Value = 101 then
      Value := 'Protocolo: ' + QrNFeCabAinfCanc_nProt.Value +
               sLineBreak + 'Data / hora: ' + Geral.FDT(QrNFeCabAinfCanc_dhRecbto.Value, 0)
    else if QrNFeCabAFatId.Value = VAR_FATID_0053 then //Recebidas
      Value := 'NF-e importada atrav�s da SEFAZ'
    else
      Value := '';
  end;
  //
  if VarName = 'NFeFTRazao' then Value := DModG.QrPrmsEmpNFeNFeFTRazao.Value else
  if VarName = 'NFeFTEnder' then Value := DModG.QrPrmsEmpNFeNFeFTEnder.Value else
  if VarName = 'NFeFTFones' then Value := DModG.QrPrmsEmpNFeNFeFTFones.Value else
  if VarName = 'InformacoesAdcionais' then
  begin
    //
(*
    Geral.MB_Info(QrAInfAdic_InfAdFisco.Value + sLineBreak +
      QrAInfAdic_InfCpl.Value + sLineBreak +
      QrAInfCpl_totTrib.Value + sLineBreak);
[frxDsA."InfAdic_InfAdFisco"]
[frxDsA."InfAdic_InfCpl"]
[frxDsA."InfCpl_totTrib"]
*)
    //
    Value := '';
    Texto := QrAInfAdic_InfAdFisco.Value;
    if Texto <> '' then
      Value := Value + Texto + sLineBreak;
    //
    Texto := QrAInfAdic_InfCpl.Value;
    if Texto <> '' then
      Value := Value + Texto + sLineBreak;
    //
    if QrNfeCabANFeNT2013_003LTT.Value = 3 then
    begin
      Texto := QrAInfCpl_totTrib.Value;
      if Texto <> '' then
        Value := Value + Texto + sLineBreak;
    end;
  end else
  // ini 2021-11-18 a pinc�pio s� no frxA4A_002
  if VarName = 'VARF_FONE_OU_MAIL' then
  begin
    //   QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
    if QrAemit_fone.Value <> '' then
      Value := 'TELEFONE: ' + QrAEMIT_FONE_TXT.Value
    else
      Value := '';
  end else
  // fim 2021-11-18
end;

procedure TFmNFe_Pesq_0000.frxListaNFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmNFe_Pesq_0000.ImprimeListaNova(PorCFOP: TNFEAgruRelNFs);
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  UnNFe_PF.MostraFormNFe_Pesq_0000_ImpLista(PorCFOP, EdFilial.ValueVariant,
    TPDataI.Date, TPDataF.Date, RGQuemEmit.ItemIndex, RGAmbiente.ItemIndex,
    EdCliente.ValueVariant, QrClientesTipo.Value, RGOrdem1.ItemIndex,
    RGOrdem2.ItemIndex, Ck100e101.Checked, QrClientesCNPJ.Value,
    QrClientesCPF.Value, CGcSitConf, CkideNatOp.Checked, CBFilial.Text,
    CBCliente.Text);
end;

procedure TFmNFe_Pesq_0000.ImprimeNFCe(Preview: Boolean);
const
  sProcName = 'TFmNFe_Pesq_0000.ImprimeNFCe()';
begin
{$IfNDef semNFCe_0000}
    if DBCheck.CriaFm(TFmNFCeLEnU_0400, FmNFCeLEnU_0400, afmoNegarComAviso) then
    begin
      FmNFCeLEnU_0400.EdFatID.ValueVariant   := QrNFeCabAFatID.Value;
      FmNFCeLEnU_0400.EdFatNum.ValueVariant  := QrNFeCabAFatNum.Value;
      FmNFCeLEnU_0400.EdEmpresa.ValueVariant := QrNFeCabAEmpresa.Value;
      FmNFCeLEnU_0400.EdIDCtrl.ValueVariant  := QrNFeCabAIDCtrl.Value;
      //FmNFCeLEnU_0400.ShowModal;
      case QrNFeCabAide_tpEmis.Value of
         //1: FmNFCeLEnU_0400.Imprime_Autorizada(TfrxImpComo.ficMostra);
         //9: FmNFCeLEnU_0400.Imprime_Off_Line(TfrxImpComo.ficMostra);
         1: FmNFCeLEnU_0400.Imprime_Autorizada(TfrxImpComo.ficMostra);
         9: FmNFCeLEnU_0400.Imprime_Off_Line(TfrxImpComo.ficMostra);
        else Geral.MB_Info(
        'Impress�o n�o habilitada para o ide_tpEmis selecionado!' + sLineBreak +
        sProcName);
      end;
      FmNFCeLEnU_0400.Destroy;
    end;
{$Else}
  Geral.MB_Info('NFC-e n�o habilitado para este ERP');
{$EndIf};
end;

procedure TFmNFe_Pesq_0000.ImprimeNFe(Preview: Boolean);
var
  I: Integer;
(*  MD_NIVEL1: TfrxMasterData;
*)
  //frxReportPage: TfrxReportPage;
  //Nomes, Tipos: array of String;
  Report: TfrxReport;
  Memo: TfrxMemoView;
  Recebidas: Boolean;
begin
  if not LiberaPor(tqeAmbos, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
(*
  MD_NIVEL1 := frxErros.FindObject('MD_NIVEL1') as TfrxMasterData;
  MEMO1 := frxErros.FindObject('MEMO1') as TfrxMemoView;
*)

  Recebidas := QrNFeCabAFatId.Value = VAR_FATID_0053; //Recebidas

  //Report := frxA4A_002;
  Report := DefineQual_frxNFe(ficNone);
  try
    (*
    SetLength(Nomes, Report.ComponentCount);
    SetLength(Tipos, Report.ComponentCount);
    *)
    for I := 0 to Report.ComponentCount - 1 do
    begin
      (*
      Nomes[I] := TComponent(Report.Components[I]).Name;
      Tipos[I] := TComponent(Report.Components[I]).ClassName;
      *)
      //
      if Report.Components[I] is TfrxMemoView then
      begin
        Memo := TfrxMemoView(Report.Components[I]);
        //Memo.Frame.Typ := [];
        case Memo.Tag of
          0:
          begin
            if QrNFeCabAcStat.Value = 101 then //Cancelado
              Memo.VIsible := False
            else
              Memo.Visible := not Preview;
          end;
          1: ; // Deixa como est�
          2:
          begin
            if QrNFeCabAcStat.Value = 101 then //Cancelado
              Memo.Visible := True
            else
              Memo.Visible := Preview or Recebidas;
          end;
        end;
      end
      else if (Report.Components[I] is TfrxLineView) and (Preview) then
        TfrxLineView(Report.Components[I]).Visible := False
      else if (Report.Components[I] is TfrxBarCodeView) and (Preview) then
        TfrxBarCodeView(Report.Components[I]).Visible := False
    end;
    //Geral.MensagemArray(MB_OK+MB_ICONINFORMATION, 'Componentes', Nomes, Tipos, nil);
    DefineFrx(Report, ficMostra);
  finally
    //Report.Free;
  end;
end;

procedure TFmNFe_Pesq_0000.ImprimeSomenteadmin1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  BtImprimeClick(Self);
end;

function TFmNFe_Pesq_0000.LiberaPor(QuemEmit: TQuemEmit; emit_CNPJ: String;
  MostraMsg: Boolean): Boolean;
begin
  Result := True;
  Exit;
  //
  if QuemEmit = tqeAmbos then
  begin
    Result := True;
  end else
  if QuemEmit = tqeEmitidas then
  begin
    if emit_CNPJ = DModG.QrEmpresasCNPJ_CPF.Value then
    begin
      Result := True;
    end else
    begin
      if MostraMsg then
        Geral.MB_Aviso('Voc� deve selecionar uma nota fiscal do tipo: "Emitida"!');
      //
      Result := False;
    end;
  end else
  begin
    if emit_CNPJ <> DModG.QrEmpresasCNPJ_CPF.Value then
    begin
      Result := True;
    end else
    begin
      if MostraMsg then
        Geral.MB_Aviso('Voc� deve selecionar uma nota fiscal do tipo: "Recebida"!');
      //
      Result := False;
    end;
  end;
end;

procedure TFmNFe_Pesq_0000.Listadasnotaspesquisadas1Click(Sender: TObject);
var
  ide_nNF, ide_serie, cStat, Ordem, Status: Integer;
  ide_dEmi: String;
  ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
  ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS: Double;
  NOME_tpEmis, NOME_tpNF, Motivo,
  ide_AAMM_AA, ide_AAMM_MM, Id, infCanc_dhRecbto, infCanc_nProt,
  ide_natOp, Ordem100: String;
  //
  esp, marca, nVol: String;
  qVol, PesoL, PesoB: Double;
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
{ TODO : Incluir Notas inutilizadas }
  Screen.Cursor := crHourGlass;
  try
    F_NFe_100 := GradeCriar.RecriaTempTableNovo(ntrttNFe_100, DmodG.QrUpdPID1, False);
    F_NFe_101 := GradeCriar.RecriaTempTableNovo(ntrttNFe_101, DmodG.QrUpdPID1, False);
    F_NFe_XXX := GradeCriar.RecriaTempTableNovo(ntrttNFe_XXX, DmodG.QrUpdPID1, False);
    //
    QrNFeCabA.First;
    while not QrNFeCabA.Eof do
    begin
      // 0..999
      cStat            := Trunc(QrNFeCabAcStat.Value);
      ide_nNF          := QrNFeCabAide_nNF.Value;
      ide_serie        := QrNFeCabAide_serie.Value;
      ide_dEmi         := Geral.FDT(QrNFeCabAide_dEmi.Value, 1);
      // 100
      ICMSTot_vProd    := QrNFeCabAICMSTot_vProd.Value;
      ICMSTot_vST      := QrNFeCabAICMSTot_vST.Value;
      ICMSTot_vFrete   := QrNFeCabAICMSTot_vFrete.Value;
      ICMSTot_vSeg     := QrNFeCabAICMSTot_vSeg.Value;
      ICMSTot_vIPI     := QrNFeCabAICMSTot_vIPI.Value;
      ICMSTot_vOutro   := QrNFeCabAICMSTot_vOutro.Value;
      ICMSTot_vDesc    := QrNFeCabAICMSTot_vDesc.Value;
      ICMSTot_vNF      := QrNFeCabAICMSTot_vNF.Value;
      ICMSTot_vBC      := QrNFeCabAICMSTot_vBC.Value;
      ICMSTot_vICMS    := QrNFeCabAICMSTot_vICMS.Value;
      ICMSTot_vPIS     := QrNFeCabAICMSTot_vPIS.Value;
      ICMSTot_vCOFINS  := QrNFeCabAICMSTot_vCOFINS.Value;
      NOME_tpEmis      := QrNFeCabANOME_tpEmis.Value;
      NOME_tpNF        := QrNFeCabANOME_tpNF.Value;
      ide_natOp        := QrNFeCabAide_natOp.Value;
      // 101
      ide_AAMM_AA      := Geral.FDT(QrNFeCabAide_dEmi.Value, 21);
      ide_AAMM_MM      := Geral.FDT(QrNFeCabAide_dEmi.Value, 22);
      Motivo           := QrNFeCabAinfCanc_xJust.Value;
      Id               := QrNFeCabAId.Value;
      infCanc_dhRecbto := Geral.FDT(QrNFeCabAinfCanc_dhRecbto.Value, 9);
      infCanc_nProt    := QrNFeCabAinfCanc_nProt.Value;
      // Outros
      Status           := QrNFeCabAStatus.Value;
      //
      // 2015-10-17
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXVol, Dmod.MyDB, [
      'SELECT SUM(qVol) qVol, esp, marca,  ',
      'nVol, SUM(PesoL) PesoL, SUM(PesoB) PesoB  ',
      'FROM nfecabxvol ',
      'WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
      'AND FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
      'GROUP BY FatID, FatNum, Empresa ',
      '']);
      qVol             := QrNFeCabXVolqVol.Value;
      esp              := QrNFeCabXVolesp.Value;
      marca            := QrNFeCabXVolmarca.Value;
      nVol             := QrNFeCabXVolnVol.Value;
      PesoL            := QrNFeCabXVolPesoL.Value;
      PesoB            := QrNFeCabXVolPesoB.Value;
      // Fim 2015-10-17
      case cStat of
        100:
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_100, True, [
          'Ordem',
          'ide_nNF', 'ide_serie', 'ide_dEmi',
          'ICMSTot_vProd', 'ICMSTot_vST', 'ICMSTot_vFrete',
          'ICMSTot_vSeg', 'ICMSTot_vIPI', 'ICMSTot_vOutro',
          'ICMSTot_vDesc', 'ICMSTot_vNF', 'ICMSTot_vBC',
          'ICMSTot_vICMS', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
          'NOME_tpEmis', 'NOME_tpNF', 'ide_natOp',
          'qVol', 'esp', 'marca',
          'nVol', 'PesoL', 'PesoB'], [
          ], [
          0,
          ide_nNF, ide_serie, ide_dEmi,
          ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
          ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
          ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
          ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS,
          NOME_tpEmis, NOME_tpNF, ide_natOp,
          qVol, esp, marca,
          nVol, PesoL, PesoB
          ], [
          ], False);
        end;
        101:
        begin
          Ordem := 0;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_101, False, [
          'Ordem', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Id', 'infCanc_dhRecbto', 'infCanc_nProt', 'Motivo'], [
          ], [
          Ordem, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Id, infCanc_dhRecbto, infCanc_nProt, Motivo], [
          ], False);
        end;
        else begin
          if Status < 100 then
            Ordem := 2
          else
            Ordem := 1;
          Motivo := NFeXMLGeren.Texto_StatusNFe(Status, QrNFeCabAversao.Value);
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_XXX, False, [
          'Ordem', 'cStat', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Status', 'Motivo'], [
          ], [
          Ordem, cStat, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Status, Motivo], [
          ], False);
        end;
      end;
      //
      QrNFeCabA.Next;
    end;
    //
    case RGOrdem1.ItemIndex of
      0: Ordem100 := 'ide_dEmi, ide_nNF ';
      1: Ordem100 := 'ide_nNF, ide_dEmi';
      2: Ordem100 := 'cStat, ide_nNF ';
      3: Ordem100 := 'cStat, ide_dEmi ';
    end;
    //
    if CkideNatOp.Checked then
      Ordem100 := 'ORDER BY ide_NatOp, ' + Ordem100
    else
      Ordem100 := 'ORDER BY ' + Ordem100;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_100, DModG.MyPID_DB, [
    'SELECT * FROM nfe_100 ',
    Ordem100,
    '']);
    //
    // 2022-04-10
    //QrNFe_101.Close;
    //QrNFe_101.Database := DModG.MyPID_DB;
    //UnDmkDAC_PF.AbreQuery(QrNFe_101, DModG.MyPID_DB); // 2022-02-20 - Antigo . O p e n ;
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_101, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM nfe_101 ',
    'ORDER BY ide_nNF',
    '']);
    //
    QrNFeCabA.DisableControls;
    try
      QrNFe_XXX.Database := DModG.MyPID_DB;
      if CkideNatOp.Checked then
        MyObjects.frxMostra(frxListaNFesB, 'Lista de NF-e(s)')
      else
        MyObjects.frxMostra(frxListaNFes, 'Lista de NF-e(s)');
    finally
      QrNFeCabA.EnableControls;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFe_Pesq_0000.Listadefaturasdasnotaspesquisadas1Click(
  Sender: TObject);
begin
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  UnNFe_PF.MostraFormNFe_Pesq_0000_ImpFatur(CGcSitConf, EdFilial.ValueVariant,
    RGQuemEmit.ItemIndex, EdCliente.ValueVariant, RGAmbiente.ItemIndex,
    QrClientesTipo.Value, CBFilial.Text, CBCliente.Text, QrClientesCNPJ.Value,
    QrClientesCPF.Value, TPDataI.Date, TPDataF.Date, Ck100e101.Checked);
end;

procedure TFmNFe_Pesq_0000.LocalizaNfepelachave1Click(Sender: TObject);
var
  CHV_NFE: String;
  IDCtrl: Integer;
begin
  if InputQuery('Chave NFe', 'Informe a chave da NFe:', CHV_NFE) = True then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT IDCtrl',
    'FROM nfecaba',
    'WHERE ID="' + CHV_NFE + '"',
    '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      IDCtrl := Dmod.QrAux.Fields[0].AsInteger;
      ReopenNFeCabA(IDCtrl, False);
    end else
      Geral.MB_Info('A chave ' + CHV_NFE + ' n�o foi localizada!');
  end;
end;

procedure TFmNFe_Pesq_0000.Manifestar1Click(Sender: TObject);
begin
  RGQuemEmit.ItemIndex := 1;
  CGcSitConf.Value := 3;
  BtReabreClick(Self);
end;

procedure TFmNFe_Pesq_0000.MostrarchavedeacessodaNFe1Click(Sender: TObject);
begin
  UnNFe_PF.MostraNFeChave(QrNFeCabAId.Value);
end;

procedure TFmNFe_Pesq_0000.NFe1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_NFe.Value, 'XML NFe', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFe_Pesq_0000.NFeemAbaFormatado1Click(Sender: TObject);
begin
  MeXML.Text := dmkPF.TextoToHTMLUnicode(QrArqXML_NFe.Value);
  DmNFe_0000.LoadXML(MeXML, WBXML, PageControl1, 2);
  PageControl2.ActivePageIndex := 1;
end;

procedure TFmNFe_Pesq_0000.odaspesquisadas1Click(Sender: TObject);
var
  CNPJ_CPF, DANFE, XML, Caminho: String;
  Entidade, Vezes: Integer;
  Exportou: Boolean;
begin
  if (QrNFeCabA.State = dsInactive) or (QrNFeCabA.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� NFes para exportar!');
    Exit;
  end;
  if not LiberaPor(tqeEmitidas, QrNFeCabAemit_CNPJ.Value, True) then
    Exit;
  //
  Caminho := MyObjects.DefineDiretorio(Self, nil);
  Caminho := Caminho + '\' + FormatDateTime('YYY_MM_DD', DmodG.ObtemAgora());
  //
  QrNFeCabA.DisableControls;
  try
    Screen.Cursor := crHourGlass;
    PB1.Visible := True;
    Memo1.Visible := True;
    Memo2.Visible := True;
    DBGNFes.DataSource := nil;
    DBGNFes.Visible := False;
    //
    DmNFe_0000.ReopenOpcoesNFe(DModG.QrEmpresasCodigo.Value, True);
    //
    PB1.Position := 0;
    PB1.Max := QrNFeCabA.RecordCount;
    QrNFeCabA.First;
    while not QrNFeCabA.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso3, LaAviso4);
      //
      if QrNFeCabAcStat.Value = 100 then
      begin
        if QrNFeCabAdest_CNPJ.Value <> '' then
          CNPJ_CPF := QrNFeCabAdest_CNPJ.Value
        else
          CNPJ_CPF := QrNFeCabAdest_CPF.Value;
        //
        Exportou := False;
        Vezes := 0;
        while (Exportou = False) and (Vezes < 10) do
        begin
          if Vezes > 0 then
          begin
            Memo1.Text := Geral.FF0(Vezes) + 'x ' + QrNFeCabAID.Value + sLineBreak + Memo1.Text;
            Sleep(3);
          end;
          Application.ProcessMessages;
          Exportou :=
            ExportaXML_e_DANFE(CNPJ_CPF, DANFE, XML, Caminho + '\_' + Geral.FF0(Trunc(QrNFeCabAcStat.Value)));
          Vezes := Vezes + 1;
        end;
        if not Exportou then
           Memo2.Text := 'N�o foi poss�vel exportar a NFe ' + Geral.FF0(QrNFeCabA_ide_nNF.Value) + sLineBreak + Memo1.Text;
        //
      end;
      QrNFeCabA.Next;
    end;
  finally
    DBGNFes.DataSource := DsNFeCabA;
    DBGNFes.Visible    := True;
    QrNFeCabA.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFe_Pesq_0000.PMArqPopup(Sender: TObject);
begin
  NFe1         .Enabled := QrArqXML_NFe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmNFe_Pesq_0000.PMEventosPopup(Sender: TObject);
begin
  CorrigeManifestao1.Enabled :=
    (QrNFeCabA.State <> DsInactive) and (QrNFeCabA.RecordCount > 0) and (
      (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfNaoConsultada) or
      (QrNFeCabAcSitConf.Value = NFe_CodEventoSitConfMDeSemManifestacao)
    );
end;

procedure TFmNFe_Pesq_0000.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  CorrigesrienmeroNFerecebidaspesquisadas1.Enabled := RGQuemEmit.ItemIndex = 1;
  Enab := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
  //
  AbrirNFenoportalnacional1.Enabled   := Enab;
  MostrarchavedeacessodaNFe1.Enabled  := Enab;
  EditadadosdeentradaEFDC1701.Enabled := Enab and (QrNFeCabAFatID.Value = VAR_FATID_0051);
  Abrirfaturamento1.Enabled           := Enab;
end;

procedure TFmNFe_Pesq_0000.porCFOPeProdutototaiseitensNFe1Click(
  Sender: TObject);
begin
  ImprimeListaNova(nfearnProd);
end;

procedure TFmNFe_Pesq_0000.porCFOPTipodeprodutpoeProdutototaiseitensNFe1Click(
  Sender: TObject);
begin
  ImprimeListaNova(nfearnTipPrd);
end;

procedure TFmNFe_Pesq_0000.porCFOPtotaiseitensNFe1Click(Sender: TObject);
begin
  ImprimeListaNova(nfearnCOFP);
end;

procedure TFmNFe_Pesq_0000.porNatOp1Click(Sender: TObject);
begin
  ImprimeListaNova(nfearnNatOP);
end;

procedure TFmNFe_Pesq_0000.porNCMtotaiseitensNFe1Click(Sender: TObject);
begin
  ImprimeListaNova(nfearnNCM);
end;

procedure TFmNFe_Pesq_0000.PreviewdaNFe1Click(Sender: TObject);
begin
  ImprimeNFe(True);
end;

procedure TFmNFe_Pesq_0000.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := DmNFe_0000.FormataID_NFe(QrAId.Value);
{
  QrAId_TXT.Value := Copy(QrAId.Value, 01, 04) + ' ' +
                     Copy(QrAId.Value, 05, 04) + ' ' +
                     Copy(QrAId.Value, 09, 04) + ' ' +
                     Copy(QrAId.Value, 13, 04) + ' ' +
                     Copy(QrAId.Value, 17, 04) + ' ' +
                     Copy(QrAId.Value, 21, 04) + ' ' +
                     Copy(QrAId.Value, 25, 04) + ' ' +
                     Copy(QrAId.Value, 29, 04) + ' ' +
                     Copy(QrAId.Value, 33, 04) + ' ' +
                     Copy(QrAId.Value, 37, 04) + ' ' +
                     Copy(QrAId.Value, 41, 04);
}
  //
  QrAEMIT_ENDERECO.Value := AnsiUppercase(QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + sLineBreak +
    QrAemit_xBairro.Value + sLineBreak + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + sLineBreak +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value)) + '  ' + QrAemit_xPais.Value);

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  QrAEMIT_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value);
  //
    //
    ////// DESTINAT�RIO
    //
  //
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrPrmsEmpNFeNFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + sLineBreak +
      DModG.QrPrmsEmpNFeNFeShowURL.Value;
  if DModG.QrPrmsEmpNFeNFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
    //
    ////// TRANSPORTADORA - REBOQUE???
    //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  {
  QrATRANSPORTA_ENDERECO.Value := QrAtransporta_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAtransporta_nro.Value, True) + ' ' + QrAtransporta_xCpl.Value;
  //
  QrATRANSPORTA_CEP_TXT.Value := Geral.FormataCEP_TT(QrAtransporta_CEP.Value);
  QrATRANSPORTA_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAtransporta_fone.Value);
  //
  }
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if (QrAinfEPEC_cStat.Value = 136) and (QrAinfProt_cStat.Value < 100) then
  begin
    if QrAide_tpAmb.Value = 2 then
    begin
      QrADOC_SEM_VLR_JUR.Value :=
        'NF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
      QrADOC_SEM_VLR_FIS.Value := 'EPEC (homologa��o). ' + QrAinfEPEC_nProt.Value + ' ' + Geral.FDT(QrAinfEPEC_dhRegEvento.Value, 00) + 'SEM VALIDADE JUR�DICA';
    end else begin
      QrADOC_SEM_VLR_JUR.Value := '';
      QrADOC_SEM_VLR_FIS.Value := 'EPEC ' + QrAinfEPEC_nProt.Value + ' ' + Geral.FDT(QrAinfEPEC_dhRegEvento.Value, 00);
    end;
  end
  else
  begin
    if QrAide_tpAmb.Value = 2 then
    begin
      QrADOC_SEM_VLR_JUR.Value :=
        'NF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
      QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
    end else begin
      QrADOC_SEM_VLR_JUR.Value := '';
      QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
    end;
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
{ p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
}
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
end;

procedure TFmNFe_Pesq_0000.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabAMsg.Close;
  BtLeArq.Enabled     := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Visible   := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
end;

procedure TFmNFe_Pesq_0000.QrFouGCalcFields(DataSet: TDataSet);
begin
  if QrFouGCNPJ.Value <> '' then
    QrFouGCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrFouGCNPJ.Value)
  else
    QrFouGCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrFouGCPF.Value);
  //
  QrFouGENDERECO.Value := QrFouGxLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrFouGxLgr.Value, QrFouGnro.Value, True) + ' ' + QrFouGxCpl.Value;
  //
  QrFouGCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrFouGCEP.Value));
  QrFouGFONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrFouGfone.Value);
  //
  QrFouGIE_TXT.Value := Geral.Formata_IE(QrFouGIE.Value, QrFouGUF.Value, '??');
end;

procedure TFmNFe_Pesq_0000.QrIAfterScroll(DataSet: TDataSet);
begin
  QrM.Close;
  QrM.Database := Dmod.MyDB;
  QrM.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrM.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrM.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrM.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrM, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrN.Close;
  QrN.Database := Dmod.MyDB;
  QrN.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrN.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrN.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrN.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrO.Close;
  QrO.Database := Dmod.MyDB;
  QrO.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrO.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrO.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrO.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrV.Close;
  QrV.Database := Dmod.MyDB;
  QrV.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrV.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrV.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrV.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrV, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
end;

procedure TFmNFe_Pesq_0000.QrICalcFields(DataSet: TDataSet);
var
  Texto: String;
  P, Casas: Integer;
begin
  Texto := FloatToStr(QrIprod_qCom.Value);
  Casas := Pos('.', Texto);
  if Casas = 0 then
    Casas := Pos(',', Texto);
  if Casas > 0 then
    Casas := Length(Texto) - Casas;
  if QrIFracio.Value > Casas then
    Casas := QrIFracio.Value;
  //QrIprod_qCom_TXT.Value := Geral.FFT(QrIprod_qCom.Value, QrIFracio.Value, siNegativo);
  QrIprod_qCom_TXT.Value := Geral.FFT(QrIprod_qCom.Value, Casas, siNegativo);
end;

procedure TFmNFe_Pesq_0000.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible    := True;
  BtConsulta.Enabled := QrNFeCabA.RecordCount > 0;
  SbImprime.Enabled  := QrNFeCabA.RecordCount > 0;
  SbNovo.Enabled     := QrNFeCabA.RecordCount > 0;
end;

procedure TFmNFe_Pesq_0000.QrNFeCabAAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  BtArq.Enabled       := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Visible   := False;
  BtAutoriza.Enabled  := False;
  BtInutiliza.Enabled := True;
  BtExclui.Enabled    := False;
  // ini 2022-04-10
  //BtConsulta.Enabled  := False;
  BtConsulta.Enabled  := True;
  // fim 2022-04-10
  BtLe2.Enabled       := False;
  //
  if (QrNFeCabAFatId.Value = VAR_FATID_0002) //NFCe
  and (QrNFeCabAStatus.Value = 35) then //Emiss�o off-line
  begin
    BtLeArq.Enabled     := False;
    BtImprime.Enabled   := True;
    BtEnvia.Enabled     := False;
    BtEvento.Enabled    := False;
    BtCancela.Visible   := False;
    BtInutiliza.Enabled := False;
    BtExclui.Enabled    := False; //QrNFeCabAcStat.Value = 0;
    BtAutoriza.Enabled  := True;
  end else
  if QrNFeCabAFatId.Value = VAR_FATID_0053 then //Recebidas
  begin
    BtArq.Enabled       := True; // 2022-04-23
    BtLeArq.Enabled     := False;
    BtImprime.Enabled   := True;
    BtEnvia.Enabled     := False;
    BtEvento.Enabled    := True;
    BtCancela.Visible   := False;
    BtInutiliza.Enabled := True;
    BtExclui.Enabled    := QrNFeCabAcStat.Value = 0;
    BtLe2.Enabled       := QrNFeCabAcStat.Value = 100;
  end else
  begin
    BtArq.Enabled      := True;
    BtLeArq.Enabled    := True;
    BtConsulta.Enabled := True;
    //
    case Trunc(QrNFeCabAcStat.Value) of
      0:
      begin
        if QrNFeCabAide_tpEmis.Value <> 4 then // diferente de EPEC
          BtExclui.Enabled := True
        else
          BtEvento.Enabled := True; // EPEC!
      end;
      1..99, 201..203, 205..467, 469..484, 486..999:  // No status 204 "Rejei��o: Duplicidade de NF-e" n�o deixar excluir
      begin                                 // No status 468 "Rejei��o: Evento registrado, mas n�o vinculado a NF-e" n�o deixar excluir
        BtExclui.Enabled := True;
      end;
      100:
      begin
        BtImprime.Enabled := True;
        BtEvento.Enabled  := True;
        BtCancela.Visible := False; //N�o usa mais
        BtEnvia.Enabled   := True;
      end;
      101:
      begin
        BtImprime.Enabled := True;
        BtEnvia.Enabled   := False;
        BtEvento.Enabled  := True;
      end;
      136:
      begin
        BtImprime.Enabled := True;
        BtEnvia.Enabled   := True;
        BtEvento.Enabled  := False;
      end;
      485:
      begin
        BtImprime.Enabled := True;
        BtEnvia.Enabled   := False;
        BtEvento.Enabled  := False;
      end;
      //100: BtImprime.Enabled := True;
    end;
  end;
  // ini 2023-06-28
  // Desenvolvendo Aqui!
  //Habilita := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
  //BtEvento.Enabled := Habilita; // EPEC!
  // fim 2023-06-28
  ReopenNFeEveRRet();
  ReopenNFeCabAMsg();
  //
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrNFeCabA.RecordCount)
end;

procedure TFmNFe_Pesq_0000.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible    := False;
  BtExclui.Enabled   := False;
  BtConsulta.Enabled := False;
  BtArq.Enabled      := False;
  BtLeArq.Enabled    := False;
  BtCancela.Enabled  := False;
  BtEvento.Enabled   := False;
  BtEnvia.Enabled    := False;
  BtImprime.Enabled  := False;
  SbImprime.Enabled  := False;
  SbNovo.Enabled     := False;
  QrNFeCabAMsg.Close;
  QrNFeEveRRet.Close;
  //
  LaTotal.Caption := '';
end;

procedure TFmNFe_Pesq_0000.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  case QrNfeCabAide_tpEmis.Value of
    1: QrNfeCabANome_tpEmis.Value := 'Normal';
    2: QrNfeCabANome_tpEmis.Value := 'Conting�ncia FS';
    // ini 2023-06-09
    //3: QrNfeCabANome_tpEmis.Value := 'Conting�ncia SCAN';
    3: QrNfeCabANome_tpEmis.Value := 'Regime Especial NFF';
    // fim 2023-06-09
    4: QrNfeCabANome_tpEmis.Value := 'Conting�ncia EPEC';
    5: QrNfeCabANome_tpEmis.Value := 'Conting�ncia FSDA';
    6: QrNfeCabANome_tpEmis.Value := 'Conting�ncia SVC - AN';
    7: QrNfeCabANome_tpEmis.Value := 'Conting�ncia SVC - RS';
    9: QrNfeCabANome_tpEmis.Value := 'Conting�ncia off-line da NFC-e';
    else QrNfeCabANome_tpEmis.Value := '? ? ? ?';
  end;
  case QrNfeCabAide_tpNF.Value of
    0: QrNfeCabANome_tpNF.Value := 'E';
    1: QrNfeCabANome_tpNF.Value := 'S';
    else QrNfeCabANome_tpNF.Value := '?';
  end;
end;

procedure TFmNFe_Pesq_0000.QrNFeEveRRetCalcFields(DataSet: TDataSet);
begin
  if QrNFeEveRRetret_xEvento.Value <> '' then
    QrNFeEveRRetNO_EVENTO.Value := QrNFeEveRRetret_xEvento.Value
  else
    QrNFeEveRRetNO_EVENTO.Value :=
      NFeXMLGeren.Obtem_DeEvento_Nome(QrNFeEveRRetret_tpEvento.Value);
end;

procedure TFmNFe_Pesq_0000.QrNFeYItsCalcFields(DataSet: TDataSet);
begin
  QrNFeYItsxVenc1.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc1.Value, 2);
  QrNFeYItsxVenc2.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc2.Value, 2);
  QrNFeYItsxVenc3.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc3.Value, 2);
  QrNFeYItsxVenc4.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc4.Value, 2);
  QrNFeYItsxVenc5.Value := dmkPF.FDT_NULO(QrNFeYItsdVenc5.Value, 2);
end;

procedure TFmNFe_Pesq_0000.QrNFe_101CalcFields(DataSet: TDataSet);
begin
  QrNFe_101Id_TXT.Value := DmNFe_0000.FormataID_NFe(QrNFe_101Id.Value);
end;

procedure TFmNFe_Pesq_0000.QrNFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrNFe_XXXOrdem.Value of
    0: QrNFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrNFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrNFe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrNFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmNFe_Pesq_0000.QrXVolAfterOpen(DataSet: TDataSet);
begin
  Fesp    := '';
  Fmarca  := '';
  FnVol   := '';
  //
  FpesoL  := 0;
  FpesoB  := 0;
  FqVol   := 0;
  //
  while not QrXVol.Eof do
  begin
    if (QrXVolesp.Value <> '') and (Fesp <> '') then Fesp := Fesp + '/';
    Fesp    := Fesp    + QrXVolesp.Value;
    if (QrXVolmarca.Value <> '') and (Fmarca <> '') then Fmarca := Fmarca + '/';
    Fmarca  := Fmarca  + QrXVolmarca.Value;
    if (QrXVolnVol.Value <> '') and (FnVol <> '') then FnVol := FnVol + '/';
    FnVol   := FnVol   + QrXVolnVol.Value;
    //         //
    FpesoL  := FpesoL  + QrXVolpesoL.Value;
    FpesoB  := FpesoB  + QrXVolpesoB.Value;
    FqVol   := FqVol   + QrXVolqVol.Value;
    //
    QrXVol.Next;
  end;
end;

procedure TFmNFe_Pesq_0000.QrYAfterOpen(DataSet: TDataSet);
  function CorrigeDataVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
  function CorrigeNumeroVazio(Numero: String): String;
  begin
    if Numero = '' then
      Result := '0'
    else
      Result := Numero;
  end;
  function ProximoSeq(var nSeq: Integer): String;
  begin
    nSeq := nSeq + 1;
    Result := FormatFloat('0', nSeq);
  end;
const
  Fields = 03; // (nDup, dVencv, Dup) + 1 Seq  = 4 no total
  //MaxRow = 02;
  MaxCol = 14; // 5 grupos de 3 campos > 0..14 = 15 itens
  //
var
  Seq, MaxRow, Linha, Col, Row, ColsInRow: Integer;
  Faturas: array of array of String;
  Seq1, nDup1, dVenc1, vDup1,
  Seq2, nDup2, dVenc2, vDup2,
  Seq3, nDup3, dVenc3, vDup3,
  Seq4, nDup4, dVenc4, vDup4,
  Seq5, nDup5, dVenc5, vDup5: String;
begin
  ColsInRow := (MaxCol + 1) div Fields;
  // Aumentar para arredondar para cima
  MaxRow := QrY.RecordCount + ColsInRow - 1;
  // Total de linhas
  MaxRow := (MaxRow div ColsInRow) - 1; // > 0..?

  //
  //  Limpar vari�veis
  SetLength(Faturas, MaxCol + 1, MaxRow + 1);
  for Col := 0 to MaxCol do
    for Row := 0 to MaxRow do
      FFaturas[Col,Row] := '';

//begin
  //

  Col := 0;
  Row := 0;
  QrY.First;
  while not QrY.Eof do
  begin
    Faturas[Col, Row] :=  QrYnDup.Value;
    Col := Col + 1;
    Faturas[Col, Row] :=  Geral.FDT(QrYdVenc.Value, 1);
    Col := Col + 1;
    Faturas[Col, Row] :=  dmkPF.FFP(QrYvDup.Value, 2);
    //
    if Col = MaxCol then
    begin
      Col := 0;
      Row := Row + 1;
    end else
      Col := Col + 1;
    //
    QrY.Next;
  end;

  //

  FNFeYIts := UCriar.RecriaTempTable('NFeYIts', DmodG.QrUpdPID1, False);

  Seq := 0;
  if MaxRow > -1 then
  begin
    for Row := 0 to MaxRow do
    begin
      Linha  := Row + 1;
      Seq1   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup1  := Faturas[00, Row];
      dVenc1 := CorrigeDataVazio(Faturas[01, Row]);
      vDup1  := CorrigeNumeroVazio(Faturas[02, Row]);

      Seq2   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup2  := Faturas[03, Row];
      dVenc2 := CorrigeDataVazio(Faturas[04, Row]);
      vDup2  := CorrigeNumeroVazio(Faturas[05, Row]);

      Seq3   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup3  := Faturas[06, Row];
      dVenc3 := CorrigeDataVazio(Faturas[07, Row]);
      vDup3  := CorrigeNumeroVazio(Faturas[08, Row]);

      Seq4   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup4  := Faturas[09, Row];
      dVenc4 := CorrigeDataVazio(Faturas[10, Row]);
      vDup4  := CorrigeNumeroVazio(Faturas[11, Row]);

      Seq5   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup5  := Faturas[12, Row];
      dVenc5 := CorrigeDataVazio(Faturas[13, Row]);
      vDup5  := CorrigeNumeroVazio(Faturas[14, Row]);
      //
      //for Col := 0 to MaxCol do
      //if
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
        'Seq1', 'nDup1', 'dVenc1', 'vDup1',
        'Seq2', 'nDup2', 'dVenc2', 'vDup2',
        'Seq3', 'nDup3', 'dVenc3', 'vDup3',
        'Seq4', 'nDup4', 'dVenc4', 'vDup4',
        'Seq5', 'nDup5', 'dVenc5', 'vDup5',
        'Linha'], [
      ], [
        Seq1, nDup1, dVenc1, vDup1,
        Seq2, nDup2, dVenc2, vDup2,
        Seq3, nDup3, dVenc3, vDup3,
        Seq4, nDup4, dVenc4, vDup4,
        Seq5, nDup5, dVenc5, vDup5,
        Linha], [
      ], False);
    end;
  end else begin
    Seq1    := CorrigeNumeroVazio('');
    nDup1   := '';
    dVenc1  := CorrigeDataVazio('');
    vDup1   := CorrigeNumeroVazio('');
    Seq2    := CorrigeNumeroVazio('');
    nDup2   := '';
    dVenc2  := CorrigeDataVazio('');
    vDup2   := CorrigeNumeroVazio('');
    Seq3    := CorrigeNumeroVazio('');
    nDup3   := '';
    dVenc3  := CorrigeDataVazio('');
    vDup3   := CorrigeNumeroVazio('');
    Seq4    := CorrigeNumeroVazio('');
    nDup4   := '';
    dVenc4  := CorrigeDataVazio('');
    vDup4   := CorrigeNumeroVazio('');
    Seq5    := CorrigeNumeroVazio('');
    nDup5   := '';
    dVenc5  := CorrigeDataVazio('');
    vDup5   := CorrigeNumeroVazio('');
    Linha   := 1;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
      'Seq1', 'nDup1', 'dVenc1', 'vDup1',
      'Seq2', 'nDup2', 'dVenc2', 'vDup2',
      'Seq3', 'nDup3', 'dVenc3', 'vDup3',
      'Seq4', 'nDup4', 'dVenc4', 'vDup4',
      'Seq5', 'nDup5', 'dVenc5', 'vDup5',
      'Linha'], [
    ], [
      Seq1, nDup1, dVenc1, vDup1,
      Seq2, nDup2, dVenc2, vDup2,
      Seq3, nDup3, dVenc3, vDup3,
      Seq4, nDup4, dVenc4, vDup4,
      Seq5, nDup5, dVenc5, vDup5,
      Linha], [
    ], False);
  end;
  QrNFeYIts.Close;
  QrNFeYIts.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrNFeYIts, DModG.MyPID_DB); // 2022-02-20 - Antigo . O p e n ;
end;

procedure TFmNFe_Pesq_0000.FechaNFeCabA();
begin
  QrNFeCabA.Close;
end;

procedure TFmNFe_Pesq_0000.RGOrdem1Click(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.RGOrdem2Click(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
var
  Empresa, DOC1, Ord2, SitConfs: String;
  //Loc,Atual: Integer;
begin
  Screen.Cursor := crHourGlass;
  FSo_o_ID := So_O_ID;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  //

  (*
  Loc := IDCtrl;
  if Loc = 0 then
  begin
    if QrNFeCabA.State <> dsInactive then
      Loc := QrNFeCabAIDCtrl.Value;
  end;
  //
  *)
  QrNFeCabA.Close;
  QrNFeCabA.Database := Dmod.MyDB;
  if Empresa <> '' then
  begin
    QrNFeCabA.SQL.Clear;
    QrNFeCabA.SQL.Add('SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,');
    QrNFeCabA.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,');
    QrNFeCabA.SQL.Add('IF(cli.Tipo=0,');
    QrNFeCabA.SQL.Add('IF(cli.Fantasia <> "", cli.Fantasia, cli.RazaoSocial),');
    QrNFeCabA.SQL.Add('IF(cli.Apelido <> "", cli.Apelido, cli.Nome)) NO_Cli2, ');
    // 2014-11-24
    QrNFeCabA.SQL.Add('IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi,');
    QrNFeCabA.SQL.Add('IF(nfa.FatID IN (' +
      Geral.FF0(VAR_FATID_0051) + ', ' +
      Geral.FF0(VAR_FATID_0053) +
      '), ');
    QrNFeCabA.SQL.Add('IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome),');
    QrNFeCabA.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro,');
    // FIM 2014-11-24
    QrNFeCabA.SQL.Add('nfa.infCCe_nSeqEvento, nfa.FatID, nfa.FatNum, ');
    QrNFeCabA.SQL.Add('nfa.Empresa, nfa.LoteEnv, nfa.Id,');
    QrNFeCabA.SQL.Add('ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,');
    // ini 2020-11-01
    QrNFeCabA.SQL.Add('ide_mod,');
    // fim 2020-11-01
    QrNFeCabA.SQL.Add('ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF, ');
    QrNFeCabA.SQL.Add('dest_xNome, ');
    // ini 2023-07-06
(*
    QrNFeCabA.SQL.Add('IF(Importado>0, Status, ');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) + 0.000 cStat,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, ' +
    // 2012-08-31
    'DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ' +
    'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,');
    // Fim 2012-08-31
*)
    QrNFeCabA.SQL.Add('IF(Importado>0, Status, ');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_cStat, IF(infEPEC_cStat>0 AND infProt_cStat <> 100, infEPEC_cStat, infProt_cStat))) + 0.000 cStat,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_nProt, IF(infEPEC_cStat>0 AND infProt_cStat <> 100, infEPEC_nProt, infProt_nProt)) nProt,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_xMotivo, IF(infEPEC_cStat>0 AND infProt_cStat <> 100, infEPEC_xMotivo, infProt_xMotivo)) xMotivo,');
    QrNFeCabA.SQL.Add('IF(infCanc_cStat>0, ' +
    'DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ' +
    'IF(infEPEC_cStat>0 AND infProt_cStat <> 100, DATE_FORMAT(infEPEC_dhRegEvento, "%d/%m/%Y %H:%i:%S"), ' +
    'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S"))) dhRecbto,');
    // fim 2023-07-06
    QrNFeCabA.SQL.Add('IDCtrl, versao, ');
    // 2011-01-11
    QrNFeCabA.SQL.Add('ide_tpEmis, infCanc_xJust, Status, ');
    QrNFeCabA.SQL.Add('ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg, ');
    QrNFeCabA.SQL.Add('ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc, ');
    QrNFeCabA.SQL.Add('ICMSTot_vBC, ICMSTot_vICMS, ');
    // fim 2011-02-11
    // 2012-05-09
    QrNFeCabA.SQL.Add('ICMSTot_vPIS, ICMSTot_vCOFINS, ');
    // fim 2012-05-09
    // 2013-05-05
    QrNFeCabA.SQL.Add('cSitNFe, cSitConf,');
    QrNFeCabA.SQL.Add('ELT(cSitConf+2, "N�o consultada", "Sem manifesta��o", "Confirmada",');
    QrNFeCabA.SQL.Add('"Desconhecida", "N�o realizada", "Ci�ncia", "? ? ?") NO_cSitConf,');
    // fim 2013-05-05
    // 2011-08-20
    QrNFeCabA.SQL.Add('nfa.infCanc_dhRecbto, nfa.infCanc_nProt,');
    // fim 2011-08-20
    // 2013-05-10
    QrNFeCabA.SQL.Add('nfa.NFeNT2013_003LTT, nfa.emit_CNPJ, ');
    // fim 2013-05-10
    // 2022-04-14
    QrNFeCabA.SQL.Add('nfa.AtrelaFatID, nfa.AtrelaFatNum, nfa.AtrelaStaLnk, nfa.FisRegCad, ');
    // fim 2022-04-14
    QrNFeCabA.SQL.Add('nfa.ide_tpAmb');
    QrNFeCabA.SQL.Add('FROM nfecaba nfa');
    QrNFeCabA.SQL.Add('LEFT JOIN entidades cli ON');
    //2010-04-08
    {
    QrNFeCabA.SQL.Add('  IF(nfa.dest_CNPJ<>"",nfa.dest_CNPJ=cli.CNPJ,');
    QrNFeCabA.SQL.Add('  nfa.dest_CPF=cli.CPF)');
    }
    // fim 2010-04-08
    QrNFeCabA.SQL.Add('cli.Codigo=nfa.CodInfoDest');
    // 2014-11-24
    QrNFeCabA.SQL.Add('LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmit');
    // FIM 2014-11-24
    QrNFeCabA.SQL.Add('');


    // 2011-08-24
    if FSo_O_ID then
    begin
      QrNFeCabA.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', IDCtrl));
      QrNFeCabA.Database := Dmod.MyDB;
      UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    // Fim 2011-08-24
    end else begin
      QrNFeCabA.SQL.Add(dmkPF.SQL_Periodo('WHERE ide_dEmi ', TPDataI.Date, TPDataF.Date, True, True));
      QrNFeCabA.SQL.Add('AND nfa.Empresa = ' + Empresa);
      case RGQuemEmit.ItemIndex of
        0: QrNFeCabA.SQL.Add('AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"');
        1: QrNFeCabA.SQL.Add('AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)');
      end;
      if Ck100e101.Checked  then
        QrNFeCabA.SQL.Add('AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)');
      case RGAmbiente.ItemIndex of
        0: ; // Tudo
        1: QrNFeCabA.SQL.Add('AND nfa.ide_tpAmb=1');
        2: QrNFeCabA.SQL.Add('AND nfa.ide_tpAmb=2');
      end;
      if EdCliente.ValueVariant <> 0 then
      begin
        if QrClientesTipo.Value = 0 then
        begin
          Doc1 := Geral.TFD(QrClientesCNPJ.Value, 14, siPositivo);
          QrNFeCabA.SQL.Add('AND (dest_CNPJ="' + Doc1 + '"');
          QrNFeCabA.SQL.Add('  OR emit_CNPJ="' + Doc1 + '")');
        end else begin
          Doc1 := Geral.TFD(QrClientesCPF.Value, 11, siPositivo);
          QrNFeCabA.SQL.Add('AND dest_CPF="' + Doc1 + '"');
        end;
      end;
      SitConfs := CGcSitConf.GetIndexesChecked(True, -1);
      QrNFeCabA.SQL.Add('AND nfa.cSitConf IN (' + SitConfs + ')');
      //
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: QrNFeCabA.SQL.Add('ORDER BY nfa.ide_dEmi ' + Ord2 + ', nfa.ide_nNF ' + Ord2);
        1: QrNFeCabA.SQL.Add('ORDER BY nfa.ide_nNF ' + Ord2 + ', nfa.ide_dEmi ' + Ord2 );
        2: QrNFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_nNF ' + Ord2);
        3: QrNFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_dEmi ' + Ord2);
      end;
      //Geral.MB_SQL(Self, QrNFeCabA);
      QrNFeCabA.Database := Dmod.MyDB;
      UnDmkDAC_PF.AbreQuery(QrNFeCabA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      QrNFeCabA.Locate('IDCtrl', IDCtrl, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmNFe_Pesq_0000.ReopenNFeCabAMsg;
begin
  QrNFeCabAMsg.Close;
  QrNFeCabAMsg.Database := Dmod.MyDB;
  QrNFeCabAMsg.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeCabAMsg.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeCabAMsg.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeCabAMsg, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
end;

procedure TFmNFe_Pesq_0000.ReopenNFeEveRRet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRRet, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeeverret',
  'WHERE ret_chNFe="' + QrNFeCabAId.Value + '" ',
  'ORDER BY ret_dhRegEvento DESC',
  '']);
end;

procedure TFmNFe_Pesq_0000.RGAmbienteClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFe_Pesq_0000.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmNFe_Pesq_0000.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMQuery, SbQuery);
end;

procedure TFmNFe_Pesq_0000.TPDataIChange(Sender: TObject);
begin
  FechaNFeCabA;
end;

procedure TFmNFe_Pesq_0000.TPDataIClick(Sender: TObject);
begin
  FechaNFeCabA;
end;

{
infAdProd na descri��o do produto
informa��es adicionais nas complementares
QrV - fazer Informa��es adicionais


No evento OnCalcFields da Query QrACalcFields: Modificar �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value;� para �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);�
No componentes frxA4A_002, frxReport1, frxA4A_000, frxA4A_001 nos labels que mostram o �Protocolo de autoriza��o de uso� mudar a fonte de 12px para 10px;
}

end.

