object FmNFeEFD_C170_Cab: TFmNFeEFD_C170_Cab
  Left = 339
  Top = 185
  Caption = 'NFe-PESQU-003 :: Cabe'#231'alho de NFe - EFD'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 307
        Height = 32
        Caption = 'Cabe'#231'alho de NFe - EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 307
        Height = 32
        Caption = 'Cabe'#231'alho de NFe - EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 307
        Height = 32
        Caption = 'Cabe'#231'alho de NFe - EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 529
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 321
        Align = alTop
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 61
          Width = 1004
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label29: TLabel
            Left = 8
            Top = 4
            Width = 42
            Height = 13
            Caption = 'CNPJ ...:'
            FocusControl = DBEdit26
          end
          object Label61: TLabel
            Left = 124
            Top = 4
            Width = 50
            Height = 13
            Caption = '... ou CPF:'
            FocusControl = DBEdit72
          end
          object Label31: TLabel
            Left = 240
            Top = 4
            Width = 112
            Height = 13
            Caption = 'Raz'#227'o Social ou Nome:'
            FocusControl = DBEdit28
          end
          object Label32: TLabel
            Left = 636
            Top = 4
            Width = 74
            Height = 13
            Caption = 'Nome Fantasia:'
            FocusControl = DBEdit29
          end
          object Label37: TLabel
            Left = 868
            Top = 4
            Width = 90
            Height = 13
            Caption = 'Inscri'#231#227'o Estadual:'
            FocusControl = DBEdit48
          end
          object DBEdit26: TDBEdit
            Left = 8
            Top = 20
            Width = 112
            Height = 21
            DataField = 'emit_CNPJ'
            DataSource = DsNFeCabA
            TabOrder = 0
          end
          object DBEdit72: TDBEdit
            Left = 124
            Top = 20
            Width = 112
            Height = 21
            DataField = 'emit_CPF'
            DataSource = DsNFeCabA
            TabOrder = 1
          end
          object DBEdit28: TDBEdit
            Left = 240
            Top = 20
            Width = 393
            Height = 21
            DataField = 'emit_xNome'
            DataSource = DsNFeCabA
            TabOrder = 2
          end
          object DBEdit29: TDBEdit
            Left = 636
            Top = 20
            Width = 229
            Height = 21
            DataField = 'emit_xFant'
            DataSource = DsNFeCabA
            TabOrder = 3
          end
          object DBEdit48: TDBEdit
            Left = 868
            Top = 20
            Width = 120
            Height = 21
            DataField = 'emit_IE'
            DataSource = DsNFeCabA
            TabOrder = 4
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 105
          Width = 1004
          Height = 214
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitLeft = 0
          ExplicitTop = 85
          ExplicitWidth = 1000
          ExplicitHeight = 208
          object Label01: TLabel
            Left = 4
            Top = 4
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 32
            Top = 4
            Width = 109
            Height = 13
            Caption = 'Natureza da opera'#231#227'o:'
            FocusControl = DBEdit3
          end
          object Label5: TLabel
            Left = 804
            Top = 4
            Width = 165
            Height = 13
            Caption = 'Indicador de forma de pagamento: '
            FocusControl = DBEdit4
          end
          object Label6: TLabel
            Left = 4
            Top = 44
            Width = 27
            Height = 13
            Caption = 'Mod.:'
            FocusControl = DBEdit5
          end
          object Label10: TLabel
            Left = 36
            Top = 44
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
            FocusControl = DBEdit6
          end
          object Label11: TLabel
            Left = 72
            Top = 44
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
            FocusControl = DBEdit14
          end
          object Label18: TLabel
            Left = 156
            Top = 44
            Width = 82
            Height = 13
            Caption = 'Data da emiss'#227'o:'
            FocusControl = DBEdit15
          end
          object Label19: TLabel
            Left = 272
            Top = 44
            Width = 117
            Height = 13
            Caption = 'Data da sa'#237'sa / entrada:'
            FocusControl = DBEdit16
          end
          object Label20: TLabel
            Left = 408
            Top = 44
            Width = 90
            Height = 13
            Caption = 'Tipo do doc. fiscal:'
            FocusControl = DBEdit17
          end
          object Label21: TLabel
            Left = 504
            Top = 44
            Width = 178
            Height = 13
            Caption = 'C'#243'digo do munic'#237'pio do fato gerador: '
            FocusControl = DBEdit18
          end
          object Label22: TLabel
            Left = 879
            Top = 44
            Width = 89
            Height = 13
            Caption = 'Tipo de impress'#227'o:'
            FocusControl = DBEdit19
          end
          object Label23: TLabel
            Left = 4
            Top = 84
            Width = 138
            Height = 13
            Caption = 'Forma de impress'#227'o da NF-e:'
            FocusControl = DBEdit20
          end
          object Label24: TLabel
            Left = 4
            Top = 124
            Width = 18
            Height = 13
            Caption = 'DV:'
            FocusControl = DBEdit21
          end
          object Label25: TLabel
            Left = 28
            Top = 124
            Width = 125
            Height = 13
            Caption = 'Identifica'#231#227'o do ambiente:'
            FocusControl = DBEdit22
          end
          object Label26: TLabel
            Left = 160
            Top = 124
            Width = 148
            Height = 13
            Caption = 'Finalidade de emiss'#227'o da NF-e:'
            FocusControl = DBEdit23
          end
          object Label27: TLabel
            Left = 312
            Top = 124
            Width = 144
            Height = 13
            Caption = 'Processo de emiss'#227'o da NF-e:'
            FocusControl = DBEdit24
          end
          object Label28: TLabel
            Left = 830
            Top = 124
            Width = 153
            Height = 13
            Caption = 'Vers'#227'o do processo de emiss'#227'o:'
            FocusControl = DBEdit25
          end
          object Label376: TLabel
            Left = 4
            Top = 164
            Width = 222
            Height = 13
            Caption = 'Identifica'#231#227'o do local de destino da opera'#231#227'o: '
          end
          object Label377: TLabel
            Left = 228
            Top = 164
            Width = 152
            Height = 13
            Caption = 'Opera'#231#227'o com consumidor final:'
          end
          object Label381: TLabel
            Left = 384
            Top = 164
            Width = 396
            Height = 13
            Caption = 
              'Indicador de presen'#231'a do comprador no estabelecimento no momento' +
              ' da opera'#231#227'o:'
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 20
            Width = 25
            Height = 21
            DataField = 'ide_cUF'
            DataSource = DsNFeCabA
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 32
            Top = 20
            Width = 769
            Height = 21
            DataField = 'ide_natOp'
            DataSource = DsNFeCabA
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 804
            Top = 20
            Width = 25
            Height = 21
            DataField = 'ide_indPag'
            DataSource = DsNFeCabA
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 4
            Top = 60
            Width = 29
            Height = 21
            DataField = 'ide_mod'
            DataSource = DsNFeCabA
            TabOrder = 3
          end
          object DBEdit6: TDBEdit
            Left = 36
            Top = 60
            Width = 33
            Height = 21
            DataField = 'ide_serie'
            DataSource = DsNFeCabA
            TabOrder = 4
          end
          object DBEdit14: TDBEdit
            Left = 72
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ide_nNF'
            DataSource = DsNFeCabA
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit15: TDBEdit
            Left = 156
            Top = 60
            Width = 112
            Height = 21
            DataField = 'ide_dEmi'
            DataSource = DsNFeCabA
            TabOrder = 6
          end
          object DBEdit16: TDBEdit
            Left = 272
            Top = 60
            Width = 134
            Height = 21
            DataField = 'ide_dSaiEnt'
            DataSource = DsNFeCabA
            TabOrder = 7
          end
          object DBEdit17: TDBEdit
            Left = 408
            Top = 60
            Width = 21
            Height = 21
            DataField = 'ide_tpNF'
            DataSource = DsNFeCabA
            TabOrder = 8
          end
          object DBEdit18: TDBEdit
            Left = 504
            Top = 60
            Width = 50
            Height = 21
            DataField = 'ide_cMunFG'
            DataSource = DsNFeCabA
            TabOrder = 9
          end
          object DBEdit19: TDBEdit
            Left = 879
            Top = 60
            Width = 25
            Height = 21
            DataField = 'ide_tpImp'
            DataSource = DsNFeCabA
            TabOrder = 10
          end
          object DBEdit20: TDBEdit
            Left = 4
            Top = 100
            Width = 25
            Height = 21
            DataField = 'ide_tpEmis'
            DataSource = DsNFeCabA
            TabOrder = 11
          end
          object DBEdit21: TDBEdit
            Left = 4
            Top = 140
            Width = 21
            Height = 21
            DataField = 'ide_cDV'
            DataSource = DsNFeCabA
            TabOrder = 12
          end
          object DBEdit22: TDBEdit
            Left = 28
            Top = 140
            Width = 25
            Height = 21
            DataField = 'ide_tpAmb'
            DataSource = DsNFeCabA
            TabOrder = 13
          end
          object DBEdit23: TDBEdit
            Left = 160
            Top = 140
            Width = 21
            Height = 21
            DataField = 'ide_finNFe'
            DataSource = DsNFeCabA
            TabOrder = 14
          end
          object DBEdit24: TDBEdit
            Left = 312
            Top = 140
            Width = 25
            Height = 21
            DataField = 'ide_procEmi'
            DataSource = DsNFeCabA
            TabOrder = 15
          end
          object DBEdit25: TDBEdit
            Left = 830
            Top = 140
            Width = 157
            Height = 21
            DataField = 'ide_verProc'
            DataSource = DsNFeCabA
            TabOrder = 16
          end
          object DBEdit30: TDBEdit
            Left = 828
            Top = 20
            Width = 160
            Height = 21
            DataField = 'ide_indPag_TXT'
            DataSource = DsNFeCabA
            TabOrder = 17
          end
          object DBEdit31: TDBEdit
            Left = 428
            Top = 60
            Width = 74
            Height = 21
            DataField = 'ide_tpNF_TXT'
            DataSource = DsNFeCabA
            TabOrder = 18
          end
          object DBEdit32: TDBEdit
            Left = 556
            Top = 60
            Width = 321
            Height = 21
            DataField = 'ide_cMunFG_TXT'
            DataSource = DsNFeCabA
            TabOrder = 19
          end
          object DBEdit33: TDBEdit
            Left = 907
            Top = 60
            Width = 81
            Height = 21
            DataField = 'ide_tpImp_TXT'
            DataSource = DsNFeCabA
            TabOrder = 20
          end
          object DBEdit34: TDBEdit
            Left = 32
            Top = 100
            Width = 956
            Height = 21
            DataField = 'ide_tpEmis_TXT'
            DataSource = DsNFeCabA
            TabOrder = 21
          end
          object DBEdit35: TDBEdit
            Left = 56
            Top = 140
            Width = 101
            Height = 21
            DataField = 'ide_tpAmb_TXT'
            DataSource = DsNFeCabA
            TabOrder = 22
          end
          object DBEdit36: TDBEdit
            Left = 184
            Top = 140
            Width = 125
            Height = 21
            DataField = 'ide_finNFe_TXT'
            DataSource = DsNFeCabA
            TabOrder = 23
          end
          object DBEdit37: TDBEdit
            Left = 340
            Top = 140
            Width = 485
            Height = 21
            DataField = 'ide_procEmi_TXT'
            DataSource = DsNFeCabA
            TabOrder = 24
          end
          object EdDBide_indFinal: TDBEdit
            Left = 228
            Top = 180
            Width = 25
            Height = 21
            DataField = 'ide_indFinal'
            DataSource = DsNFeCabA
            TabOrder = 25
            OnChange = EdDBide_indFinalChange
          end
          object EdDBide_indPres: TDBEdit
            Left = 384
            Top = 180
            Width = 25
            Height = 21
            DataField = 'ide_indPres'
            DataSource = DsNFeCabA
            TabOrder = 26
            OnChange = EdDBide_indPresChange
          end
          object EdDBide_idDest: TDBEdit
            Left = 4
            Top = 180
            Width = 25
            Height = 21
            DataField = 'ide_idDest'
            DataSource = DsNFeCabA
            TabOrder = 27
            OnChange = EdDBide_idDestChange
          end
          object EdDBide_idDest_TXT: TdmkEdit
            Left = 32
            Top = 180
            Width = 193
            Height = 21
            ReadOnly = True
            TabOrder = 28
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDBide_indFinal_TXT: TdmkEdit
            Left = 256
            Top = 180
            Width = 125
            Height = 21
            ReadOnly = True
            TabOrder = 29
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDBide_indPres_TXT: TdmkEdit
            Left = 412
            Top = 180
            Width = 573
            Height = 21
            ReadOnly = True
            TabOrder = 30
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label2: TLabel
            Left = 8
            Top = 4
            Width = 29
            Height = 13
            Caption = 'FatID:'
            FocusControl = DBEdit7
          end
          object Label12: TLabel
            Left = 72
            Top = 4
            Width = 46
            Height = 13
            Caption = 'Fat.Num.:'
            FocusControl = DBEdit8
          end
          object Label13: TLabel
            Left = 140
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            FocusControl = DBEdit9
          end
          object Label14: TLabel
            Left = 188
            Top = 4
            Width = 26
            Height = 13
            Caption = 'IDCtrl'
            FocusControl = DBEdit10
          end
          object Label15: TLabel
            Left = 256
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
            FocusControl = DBEdit11
          end
          object Label16: TLabel
            Left = 396
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit12
          end
          object Label3: TLabel
            Left = 440
            Top = 4
            Width = 58
            Height = 13
            Caption = 'C'#243'd. chave:'
            FocusControl = DBEdit2
          end
          object Label17: TLabel
            Left = 508
            Top = 4
            Width = 264
            Height = 13
            Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
            FocusControl = DBEdit13
          end
          object Label314: TLabel
            Left = 784
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Status:'
          end
          object Label306: TLabel
            Left = 824
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
            FocusControl = DBEdit212
          end
          object Label307: TLabel
            Left = 860
            Top = 4
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
            FocusControl = DBEdit213
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 20
            Width = 64
            Height = 21
            DataField = 'FatID'
            DataSource = DsNFeCabA
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 72
            Top = 20
            Width = 64
            Height = 21
            DataField = 'FatNum'
            DataSource = DsNFeCabA
            TabOrder = 1
          end
          object DBEdit9: TDBEdit
            Left = 140
            Top = 20
            Width = 45
            Height = 21
            DataField = 'Empresa'
            DataSource = DsNFeCabA
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 188
            Top = 20
            Width = 64
            Height = 21
            DataField = 'IDCtrl'
            DataSource = DsNFeCabA
            TabOrder = 3
          end
          object DBEdit11: TDBEdit
            Left = 256
            Top = 20
            Width = 134
            Height = 21
            DataField = 'LoteEnv'
            DataSource = DsNFeCabA
            TabOrder = 4
          end
          object DBEdit12: TDBEdit
            Left = 396
            Top = 20
            Width = 41
            Height = 21
            DataField = 'versao'
            DataSource = DsNFeCabA
            TabOrder = 5
          end
          object DBEdit2: TDBEdit
            Left = 440
            Top = 20
            Width = 64
            Height = 21
            DataField = 'ide_cNF'
            DataSource = DsNFeCabA
            TabOrder = 6
          end
          object DBEdit13: TDBEdit
            Left = 508
            Top = 20
            Width = 271
            Height = 21
            DataField = 'Id'
            DataSource = DsNFeCabA
            TabOrder = 7
          end
          object DBEdit210: TDBEdit
            Left = 784
            Top = 20
            Width = 33
            Height = 21
            DataField = 'Status'
            DataSource = DsNFeCabA
            TabOrder = 8
          end
          object DBEdit212: TDBEdit
            Left = 820
            Top = 20
            Width = 33
            Height = 21
            DataField = 'ide_serie'
            DataSource = DsNFeCabA
            TabOrder = 9
          end
          object DBEdit213: TDBEdit
            Left = 856
            Top = 20
            Width = 80
            Height = 21
            DataField = 'ide_nNF'
            DataSource = DsNFeCabA
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
          end
        end
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 321
        Width = 1008
        Height = 208
        Align = alClient
        DataSource = DsNFeEFD_C170
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'nItem'
            Title.Caption = 'Item NF'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_ITEM'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QTD'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UNID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_ITEM'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_DESC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IND_MOV'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CST_ICMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CFOP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_NAT'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_BC_ICMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ALIQ_ICMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_ICMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IND_APUR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CST_IPI'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_ENQ'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_BC_IPI'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ALIQ_IPI'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_IPI'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_BC_PIS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ALIQ_PIS_p'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_PIS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_BC_COFINS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ALIQ_COFINS_p'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VL_COFINS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_CTA'
            Visible = True
          end
          item
            Expanded = False
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_COMPL'
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtItem: TBitBtn
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Item'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtItemClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 4
    Top = 3
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrNFeCabAAfterScroll
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT mun1.Nome ide_cMunFG_TXT,  cnan.Nome CNAE_TXT,'
      'frc.Nome NO_FisRegCad, car.Nome NO_CARTEMISS,'
      'car.Tipo TP_CART, tpc.Nome NO_TabelaPrc,'
      'ppc.Nome NO_CondicaoPG, ppc.JurosMes,'
      'ppc.MedDDSimpl, ppc.MedDDReal,'
      'frc.ISS_Usa, frc.ISS_Alq, nfea.*'
      'FROM nfecaba nfea'
      
        'LEFT JOIN locbdermall.cnae21Cad cnan ON cnan.CodAlf=nfea.emit_CN' +
        'AE'
      
        'LEFT JOIN locbdermall.dtb_munici mun1 ON mun1.Codigo=nfea.ide_cM' +
        'unFG'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad'
      'LEFT JOIN carteiras car ON car.Codigo=nfea.CartEmiss'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=nfea.TabelaPrc'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=nfea.CondicaoPg')
    Left = 348
    Top = 536
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfecaba.FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfecaba.FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfecaba.Empresa'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfecaba.IDCtrl'
    end
    object QrNFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
      Origin = 'nfecaba.LoteEnv'
    end
    object QrNFeCabAversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfecaba.versao'
      DisplayFormat = '0.00'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfecaba.Id'
      Size = 44
    end
    object QrNFeCabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
      Origin = 'nfecaba.ide_cUF'
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
      Origin = 'nfecaba.ide_cNF'
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'nfecaba.ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
      Origin = 'nfecaba.ide_indPag'
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'nfecaba.ide_mod'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'nfecaba.ide_serie'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Origin = 'nfecaba.ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'nfecaba.ide_dEmi'
    end
    object QrNFeCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      Origin = 'nfecaba.ide_dSaiEnt'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrNFeCabAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
      Origin = 'nfecaba.ide_cMunFG'
    end
    object QrNFeCabAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'nfecaba.ide_tpImp'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'nfecaba.ide_tpEmis'
    end
    object QrNFeCabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
      Origin = 'nfecaba.ide_cDV'
    end
    object QrNFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'nfecaba.ide_tpAmb'
    end
    object QrNFeCabAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
      Origin = 'nfecaba.ide_finNFe'
    end
    object QrNFeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
      Origin = 'nfecaba.ide_procEmi'
    end
    object QrNFeCabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
      Origin = 'nfecaba.ide_verProc'
    end
    object QrNFeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Origin = 'nfecaba.emit_CNPJ'
      Size = 14
    end
    object QrNFeCabAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Origin = 'nfecaba.emit_CPF'
      Size = 11
    end
    object QrNFeCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Origin = 'nfecaba.emit_xNome'
      Size = 60
    end
    object QrNFeCabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Origin = 'nfecaba.emit_xFant'
      Size = 60
    end
    object QrNFeCabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Origin = 'nfecaba.emit_xLgr'
      Size = 60
    end
    object QrNFeCabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Origin = 'nfecaba.emit_nro'
      Size = 60
    end
    object QrNFeCabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Origin = 'nfecaba.emit_xCpl'
      Size = 60
    end
    object QrNFeCabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Origin = 'nfecaba.emit_xBairro'
      Size = 60
    end
    object QrNFeCabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
      Origin = 'nfecaba.emit_cMun'
    end
    object QrNFeCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Origin = 'nfecaba.emit_xMun'
      Size = 60
    end
    object QrNFeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Origin = 'nfecaba.emit_UF'
      Size = 2
    end
    object QrNFeCabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
      Origin = 'nfecaba.emit_CEP'
    end
    object QrNFeCabAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
      Origin = 'nfecaba.emit_cPais'
    end
    object QrNFeCabAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Origin = 'nfecaba.emit_xPais'
      Size = 60
    end
    object QrNFeCabAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Origin = 'nfecaba.emit_fone'
      Size = 14
    end
    object QrNFeCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Origin = 'nfecaba.emit_IE'
      Size = 14
    end
    object QrNFeCabAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Origin = 'nfecaba.emit_IEST'
      Size = 14
    end
    object QrNFeCabAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Origin = 'nfecaba.emit_IM'
      Size = 15
    end
    object QrNFeCabAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Origin = 'nfecaba.emit_CNAE'
      Size = 7
    end
    object QrNFeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Origin = 'nfecaba.dest_CNPJ'
      Size = 14
    end
    object QrNFeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Origin = 'nfecaba.dest_CPF'
      Size = 11
    end
    object QrNFeCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Origin = 'nfecaba.dest_xNome'
      Size = 60
    end
    object QrNFeCabAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Origin = 'nfecaba.dest_xLgr'
      Size = 60
    end
    object QrNFeCabAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Origin = 'nfecaba.dest_nro'
      Size = 60
    end
    object QrNFeCabAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Origin = 'nfecaba.dest_xCpl'
      Size = 60
    end
    object QrNFeCabAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Origin = 'nfecaba.dest_xBairro'
      Size = 60
    end
    object QrNFeCabAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
      Origin = 'nfecaba.dest_cMun'
    end
    object QrNFeCabAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Origin = 'nfecaba.dest_xMun'
      Size = 60
    end
    object QrNFeCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Origin = 'nfecaba.dest_UF'
      Size = 2
    end
    object QrNFeCabAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Origin = 'nfecaba.dest_CEP'
      Size = 8
    end
    object QrNFeCabAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
      Origin = 'nfecaba.dest_cPais'
    end
    object QrNFeCabAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Origin = 'nfecaba.dest_xPais'
      Size = 60
    end
    object QrNFeCabAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Origin = 'nfecaba.dest_fone'
      Size = 14
    end
    object QrNFeCabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Origin = 'nfecaba.dest_IE'
      Size = 14
    end
    object QrNFeCabAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Origin = 'nfecaba.dest_ISUF'
      Size = 9
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Origin = 'nfecaba.ICMSTot_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Origin = 'nfecaba.ICMSTot_vICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Origin = 'nfecaba.ICMSTot_vBCST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Origin = 'nfecaba.ICMSTot_vST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Origin = 'nfecaba.ICMSTot_vProd'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Origin = 'nfecaba.ICMSTot_vFrete'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Origin = 'nfecaba.ICMSTot_vSeg'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Origin = 'nfecaba.ICMSTot_vDesc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Origin = 'nfecaba.ICMSTot_vII'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Origin = 'nfecaba.ICMSTot_vIPI'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Origin = 'nfecaba.ICMSTot_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Origin = 'nfecaba.ICMSTot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Origin = 'nfecaba.ICMSTot_vOutro'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Origin = 'nfecaba.ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
      Origin = 'nfecaba.ISSQNtot_vServ'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
      Origin = 'nfecaba.ISSQNtot_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
      Origin = 'nfecaba.ISSQNtot_vISS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
      Origin = 'nfecaba.ISSQNtot_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
      Origin = 'nfecaba.ISSQNtot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
      Origin = 'nfecaba.RetTrib_vRetPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
      Origin = 'nfecaba.RetTrib_vRetCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
      Origin = 'nfecaba.RetTrib_vRetCSLL'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
      Origin = 'nfecaba.RetTrib_vBCIRRF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
      Origin = 'nfecaba.RetTrib_vIRRF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
      Origin = 'nfecaba.RetTrib_vBCRetPrev'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
      Origin = 'nfecaba.RetTrib_vRetPrev'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Origin = 'nfecaba.ModFrete'
    end
    object QrNFeCabATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Origin = 'nfecaba.Transporta_CNPJ'
      Size = 14
    end
    object QrNFeCabATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Origin = 'nfecaba.Transporta_CPF'
      Size = 11
    end
    object QrNFeCabATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Origin = 'nfecaba.Transporta_XNome'
      Size = 60
    end
    object QrNFeCabATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Origin = 'nfecaba.Transporta_IE'
      Size = 14
    end
    object QrNFeCabATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Origin = 'nfecaba.Transporta_XEnder'
      Size = 60
    end
    object QrNFeCabATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Origin = 'nfecaba.Transporta_XMun'
      Size = 60
    end
    object QrNFeCabATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Origin = 'nfecaba.Transporta_UF'
      Size = 2
    end
    object QrNFeCabARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
      Origin = 'nfecaba.RetTransp_vServ'
    end
    object QrNFeCabARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
      Origin = 'nfecaba.RetTransp_vBCRet'
    end
    object QrNFeCabARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
      Origin = 'nfecaba.RetTransp_PICMSRet'
    end
    object QrNFeCabARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
      Origin = 'nfecaba.RetTransp_vICMSRet'
    end
    object QrNFeCabARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Origin = 'nfecaba.RetTransp_CFOP'
      Size = 4
    end
    object QrNFeCabARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Origin = 'nfecaba.RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeCabAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Origin = 'nfecaba.VeicTransp_Placa'
      Size = 8
    end
    object QrNFeCabAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Origin = 'nfecaba.VeicTransp_UF'
      Size = 2
    end
    object QrNFeCabAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
      Origin = 'nfecaba.VeicTransp_RNTC'
    end
    object QrNFeCabACobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Origin = 'nfecaba.Cobr_Fat_nFat'
      Size = 60
    end
    object QrNFeCabACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
      Origin = 'nfecaba.Cobr_Fat_vOrig'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
      Origin = 'nfecaba.Cobr_Fat_vDesc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
      Origin = 'nfecaba.Cobr_Fat_vLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      Origin = 'nfecaba.InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeCabAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Origin = 'nfecaba.Exporta_UFEmbarq'
      Size = 2
    end
    object QrNFeCabAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Origin = 'nfecaba.Exporta_XLocEmbarq'
      Size = 60
    end
    object QrNFeCabACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'nfecaba.Compra_XNEmp'
      Size = 17
    end
    object QrNFeCabACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'nfecaba.Compra_XPed'
      Size = 60
    end
    object QrNFeCabACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'nfecaba.Compra_XCont'
      Size = 60
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrNFeCabAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Origin = 'nfecaba.infProt_Id'
      Size = 30
    end
    object QrNFeCabAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
      Origin = 'nfecaba.infProt_tpAmb'
    end
    object QrNFeCabAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Origin = 'nfecaba.infProt_verAplic'
      Size = 30
    end
    object QrNFeCabAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
      Origin = 'nfecaba.infProt_dhRecbto'
    end
    object QrNFeCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Origin = 'nfecaba.infProt_nProt'
      Size = 15
    end
    object QrNFeCabAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Origin = 'nfecaba.infProt_digVal'
      Size = 28
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Origin = 'nfecaba.infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Origin = 'nfecaba.infCanc_Id'
      Size = 30
    end
    object QrNFeCabAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
      Origin = 'nfecaba.infCanc_tpAmb'
    end
    object QrNFeCabAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Origin = 'nfecaba.infCanc_verAplic'
      Size = 30
    end
    object QrNFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
      Origin = 'nfecaba.infCanc_dhRecbto'
    end
    object QrNFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Origin = 'nfecaba.infCanc_nProt'
      Size = 15
    end
    object QrNFeCabAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Origin = 'nfecaba.infCanc_digVal'
      Size = 28
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Origin = 'nfecaba.infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
      Origin = 'nfecaba.infCanc_cJust'
    end
    object QrNFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Origin = 'nfecaba.infCanc_xJust'
      Size = 255
    end
    object QrNFeCabA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Origin = 'nfecaba._Ativo_'
    end
    object QrNFeCabALk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfecaba.Lk'
    end
    object QrNFeCabADataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfecaba.DataCad'
    end
    object QrNFeCabADataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfecaba.DataAlt'
    end
    object QrNFeCabAUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfecaba.UserCad'
    end
    object QrNFeCabAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfecaba.UserAlt'
    end
    object QrNFeCabAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfecaba.AlterWeb'
    end
    object QrNFeCabAAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfecaba.Ativo'
    end
    object QrNFeCabAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Origin = 'nfecaba.FisRegCad'
    end
    object QrNFeCabATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'nfecaba.TabelaPrc'
    end
    object QrNFeCabACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'nfecaba.CartEmiss'
    end
    object QrNFeCabACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Origin = 'nfecaba.CondicaoPg'
    end
    object QrNFeCabAMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrNFeCabAMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrNFeCabAJurosMes: TFloatField
      FieldName = 'JurosMes'
      Origin = 'pediprzcab.JurosMes'
    end
    object QrNFeCabAide_cMunFG_TXT: TWideStringField
      FieldName = 'ide_cMunFG_TXT'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrNFeCabACNAE_TXT: TWideStringField
      FieldName = 'CNAE_TXT'
      Size = 255
    end
    object QrNFeCabANO_CARTEMISS: TWideStringField
      FieldName = 'NO_CARTEMISS'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrNFeCabANO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Origin = 'fisregcad.Nome'
      Size = 50
    end
    object QrNFeCabANO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Size = 50
    end
    object QrNFeCabANO_CondicaoPG: TWideStringField
      FieldName = 'NO_CondicaoPG'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrNFeCabATP_CART: TIntegerField
      FieldName = 'TP_CART'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrNFeCabAide_tpNF_TXT: TWideStringField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'ide_tpNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrNFeCabAide_indPag_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_indPag_TXT'
      Calculated = True
    end
    object QrNFeCabAide_procEmi_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_procEmi_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_finNFe_TXT: TWideStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'ide_finNFe_TXT'
      Calculated = True
    end
    object QrNFeCabAide_tpAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_tpAmb_TXT'
      Size = 12
      Calculated = True
    end
    object QrNFeCabAide_tpEmis_TXT: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'ide_tpEmis_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_tpImp_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_tpImp_TXT'
      Size = 10
      Calculated = True
    end
    object QrNFeCabAModFrete_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ModFrete_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeCabAISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
      Origin = 'fisregcad.ISS_Usa'
    end
    object QrNFeCabAISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
      Origin = 'fisregcad.ISS_Alq'
    end
    object QrNFeCabAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
      Origin = 'nfecaba.FreteExtra'
    end
    object QrNFeCabASegurExtra: TFloatField
      FieldName = 'SegurExtra'
      Origin = 'nfecaba.SegurExtra'
    end
    object QrNFeCabAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Origin = 'nfecaba.ICMSRec_pRedBC'
    end
    object QrNFeCabAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Origin = 'nfecaba.ICMSRec_vICMS'
    end
    object QrNFeCabAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Origin = 'nfecaba.IPIRec_pRedBC'
    end
    object QrNFeCabAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Origin = 'nfecaba.PISRec_pRedBC'
    end
    object QrNFeCabACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Origin = 'nfecaba.COFINSRec_pRedBC'
    end
    object QrNFeCabAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Origin = 'nfecaba.IPIRec_vIPI'
    end
    object QrNFeCabAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Origin = 'nfecaba.PISRec_vPIS'
    end
    object QrNFeCabACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Origin = 'nfecaba.COFINSRec_vCOFINS'
    end
    object QrNFeCabAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Origin = 'nfecaba.ICMSRec_vBC'
    end
    object QrNFeCabAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Origin = 'nfecaba.IPIRec_vBC'
    end
    object QrNFeCabAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Origin = 'nfecaba.PISRec_vBC'
    end
    object QrNFeCabACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Origin = 'nfecaba.COFINSRec_vBC'
    end
    object QrNFeCabAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNFeCabAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNFeCabAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNFeCabACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNFeCabADataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrNFeCabASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrNFeCabASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrNFeCabASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrNFeCabASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrNFeCabASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrNFeCabASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrNFeCabASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrNFeCabASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrNFeCabASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrNFeCabASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrNFeCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrNFeCabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrNFeCabAprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrNFeCabAretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
    end
    object QrNFeCabAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeCabAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNFeCabAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNFeCabAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrNFeCabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNFeCabAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNFeCabAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNFeCabABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrNFeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrNFeCabACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
    end
    object QrNFeCabAOrdemServ: TIntegerField
      FieldName = 'OrdemServ'
    end
    object QrNFeCabAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFeCabAide_dhEmi: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhEmi'
      Calculated = True
    end
    object QrNFeCabAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrNFeCabAide_dhSaiEntTZD: TFloatField
      FieldName = 'ide_dhSaiEntTZD'
    end
    object QrNFeCabAide_dhSaiEnt: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhSaiEnt'
      Calculated = True
    end
    object QrNFeCabAide_idDest: TSmallintField
      FieldName = 'ide_idDest'
    end
    object QrNFeCabAide_indFinal: TSmallintField
      FieldName = 'ide_indFinal'
    end
    object QrNFeCabAide_indPres: TSmallintField
      FieldName = 'ide_indPres'
    end
    object QrNFeCabAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
    end
    object QrNFeCabAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrNFeCabAdest_indIEDest: TSmallintField
      FieldName = 'dest_indIEDest'
    end
    object QrNFeCabAdest_IM: TWideStringField
      FieldName = 'dest_IM'
      Size = 15
    end
    object QrNFeCabAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrNFeCabASituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrNFeCabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrNFeCabAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAExporta_XLocDespacho: TWideStringField
      FieldName = 'Exporta_XLocDespacho'
      Size = 60
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 348
    Top = 584
  end
  object QrNFeEFD_C170: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeefd_c170 '
      'WHERE FatID=51 '
      'AND FatNum=3 '
      'AND Empresa=-11 '
      'AND nItem=1')
    Left = 440
    Top = 544
    object QrNFeEFD_C170FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEFD_C170FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEFD_C170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEFD_C170nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeEFD_C170GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeEFD_C170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrNFeEFD_C170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrNFeEFD_C170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrNFeEFD_C170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrNFeEFD_C170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrNFeEFD_C170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrNFeEFD_C170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrNFeEFD_C170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrNFeEFD_C170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrNFeEFD_C170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrNFeEFD_C170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrNFeEFD_C170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrNFeEFD_C170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrNFeEFD_C170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrNFeEFD_C170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrNFeEFD_C170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrNFeEFD_C170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrNFeEFD_C170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrNFeEFD_C170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrNFeEFD_C170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrNFeEFD_C170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrNFeEFD_C170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrNFeEFD_C170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrNFeEFD_C170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrNFeEFD_C170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrNFeEFD_C170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrNFeEFD_C170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrNFeEFD_C170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrNFeEFD_C170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrNFeEFD_C170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrNFeEFD_C170Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEFD_C170DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEFD_C170DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEFD_C170UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEFD_C170UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEFD_C170AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEFD_C170Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeEFD_C170: TDataSource
    DataSet = QrNFeEFD_C170
    Left = 444
    Top = 592
  end
  object PMItem: TPopupMenu
    Left = 156
    Top = 644
    object Alteraitem1: TMenuItem
      Caption = '&Altera item'
      OnClick = Alteraitem1Click
    end
  end
end
