﻿unit ModuleNFe_0000;

interface

uses
  Windows, Forms, SysUtils, Classes, dmkGeral, DB, mySQLDbTables, Controls,
  StdCtrls, Variants, Dialogs, ComCtrls, UrlMon, InvokeRegistry, Rio, dmkEdit,
  SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans,
  //JwaWinCrypt,
  WinInet, DateUtils,
  OleCtrls, SHDocVw, Grids, DBGrids, Graphics, DmkDAC_PF, UnGrl_Consts,
  UnGrl_Vars,
  {
  Messages, Graphics, GIFImg, ExtCtrls,
  SOAPHTTPClient,
  dmkRadioGroup,
  }
  ACBrNFe,
  XMLDoc, xmldom, XMLIntf, msxmldom,
  {$IFNDef semNFe_v0110}nfe_v110, cabecMsg_v102, {$EndIf}
  UnDmkACBr_ParamsEmp,
  UnDmkProcFunc, UnDmkEnums, UnProjGroup_Consts, TypInfo;
type
  TDmNFe_0000 = class(TDataModule)
    QrCFOP: TmySQLQuery;
    QrCFOPCFOP: TWideStringField;
    QrOpcoesNFe: TmySQLQuery;
    QrOpcoesNFeversao: TFloatField;
    QrOpcoesNFeide_mod: TSmallintField;
    QrOpcoesNFeide_tpImp: TSmallintField;
    QrOpcoesNFeide_tpAmb: TSmallintField;
    QrOpcoesNFeLk: TIntegerField;
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaCPF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaRG: TWideStringField;
    QrEmpresaCNAE: TWideStringField;
    QrEmpresaNO_ENT: TWideStringField;
    QrEmpresaFANTASIA: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTe1: TWideStringField;
    QrEmpresaNO_LOGRAD: TWideStringField;
    QrEmpresaNO_Munici: TWideStringField;
    QrEmpresaNO_UF: TWideStringField;
    QrEmpresaDTB_UF: TWideStringField;
    QrEmpresaNO_Pais: TWideStringField;
    QrEmpresaIEST: TWideStringField;
    QrDest: TmySQLQuery;
    QrDestCodigo: TIntegerField;
    QrDestTipo: TSmallintField;
    QrDestCNPJ: TWideStringField;
    QrDestCPF: TWideStringField;
    QrDestIE: TWideStringField;
    QrDestRG: TWideStringField;
    QrDestCNAE: TWideStringField;
    QrDestNO_ENT: TWideStringField;
    QrDestFANTASIA: TWideStringField;
    QrDestRUA: TWideStringField;
    QrDestCOMPL: TWideStringField;
    QrDestBAIRRO: TWideStringField;
    QrDestTe1: TWideStringField;
    QrDestNO_LOGRAD: TWideStringField;
    QrDestNO_Munici: TWideStringField;
    QrDestNO_UF: TWideStringField;
    QrDestNO_Pais: TWideStringField;
    QrDestSUFRAMA: TWideStringField;
    QrDestL_CNPJ: TWideStringField;
    QrDestL_Ativo: TSmallintField;
    QrDestLLograd: TSmallintField;
    QrDestLRua: TWideStringField;
    QrDestLCompl: TWideStringField;
    QrDestLNumero: TIntegerField;
    QrDestLBairro: TWideStringField;
    QrDestLCidade: TWideStringField;
    QrDestLUF: TSmallintField;
    QrDestNO_LLOGRAD: TWideStringField;
    QrDestLCodMunici: TIntegerField;
    QrDestNO_LMunici: TWideStringField;
    QrDestNO_LUF: TWideStringField;
    QrCFOP1: TmySQLQuery;
    QrCFOP1Codigo: TWideStringField;
    QrCFOP1Nome: TWideStringField;
    QrTotal1: TmySQLQuery;
    QrTotal1Total: TFloatField;
    QrTotal1Qtde: TFloatField;
    QrProds1: TmySQLQuery;
    QrTransporta: TMySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaTipo: TSmallintField;
    QrTransportaCNPJ: TWideStringField;
    QrTransportaCPF: TWideStringField;
    QrTransportaIE: TWideStringField;
    QrTransportaRG: TWideStringField;
    QrTransportaCNAE: TWideStringField;
    QrTransportaNO_ENT: TWideStringField;
    QrTransportaFANTASIA: TWideStringField;
    QrTransportaRUA: TWideStringField;
    QrTransportaCOMPL: TWideStringField;
    QrTransportaBAIRRO: TWideStringField;
    QrTransportaTe1: TWideStringField;
    QrTransportaNO_LOGRAD: TWideStringField;
    QrTransportaNO_Munici: TWideStringField;
    QrTransportaNO_UF: TWideStringField;
    QrTransportaENDERECO: TWideStringField;
    QrFilial: TmySQLQuery;
    QrFilialSimplesFed: TSmallintField;
    QrFilialUF_WebServ: TWideStringField;
    QrFilialDirNFeGer: TWideStringField;
    QrFilialDirNFeAss: TWideStringField;
    QrFilialDirEnvLot: TWideStringField;
    QrFilialDirRec: TWideStringField;
    QrFilialDirPedRec: TWideStringField;
    QrFilialDirProRec: TWideStringField;
    QrFilialDirDen: TWideStringField;
    QrFilialDirPedCan: TWideStringField;
    QrFilialDirCan: TWideStringField;
    QrFilialDirPedInu: TWideStringField;
    QrFilialDirInu: TWideStringField;
    QrFilialDirPedSit: TWideStringField;
    QrFilialDirSit: TWideStringField;
    QrFilialDirPedSta: TWideStringField;
    QrFilialDirSta: TWideStringField;
    QrFilialSiglaCustm: TWideStringField;
    QrFilialInfoPerCuz: TSmallintField;
    QrNFECabA: TmySQLQuery;
    QrNFECabAFatID: TIntegerField;
    QrNFECabAFatNum: TIntegerField;
    QrNFECabAEmpresa: TIntegerField;
    QrNFECabAversao: TFloatField;
    QrNFECabAId: TWideStringField;
    QrNFECabAide_cUF: TSmallintField;
    QrNFECabAide_cNF: TIntegerField;
    QrNFECabAide_natOp: TWideStringField;
    QrNFECabAide_indPag: TSmallintField;
    QrNFECabAide_mod: TSmallintField;
    QrNFECabAide_serie: TIntegerField;
    QrNFECabAide_nNF: TIntegerField;
    QrNFECabAide_dEmi: TDateField;
    QrNFECabAide_dSaiEnt: TDateField;
    QrNFECabAide_tpNF: TSmallintField;
    QrNFECabAide_cMunFG: TIntegerField;
    QrNFECabAide_tpImp: TSmallintField;
    QrNFECabAide_tpEmis: TSmallintField;
    QrNFECabAide_cDV: TSmallintField;
    QrNFECabAide_tpAmb: TSmallintField;
    QrNFECabAide_finNFe: TSmallintField;
    QrNFECabAide_procEmi: TSmallintField;
    QrNFECabAide_verProc: TWideStringField;
    QrNFECabAemit_CNPJ: TWideStringField;
    QrNFECabAemit_CPF: TWideStringField;
    QrNFECabAemit_xNome: TWideStringField;
    QrNFECabAemit_xFant: TWideStringField;
    QrNFECabAemit_xLgr: TWideStringField;
    QrNFECabAemit_nro: TWideStringField;
    QrNFECabAemit_xCpl: TWideStringField;
    QrNFECabAemit_xBairro: TWideStringField;
    QrNFECabAemit_cMun: TIntegerField;
    QrNFECabAemit_xMun: TWideStringField;
    QrNFECabAemit_UF: TWideStringField;
    QrNFECabAemit_CEP: TIntegerField;
    QrNFECabAemit_cPais: TIntegerField;
    QrNFECabAemit_xPais: TWideStringField;
    QrNFECabAemit_fone: TWideStringField;
    QrNFECabAemit_IE: TWideStringField;
    QrNFECabAemit_IEST: TWideStringField;
    QrNFECabAemit_IM: TWideStringField;
    QrNFECabAemit_CNAE: TWideStringField;
    QrNFECabAdest_CNPJ: TWideStringField;
    QrNFECabAdest_CPF: TWideStringField;
    QrNFECabAdest_xNome: TWideStringField;
    QrNFECabAdest_xLgr: TWideStringField;
    QrNFECabAdest_nro: TWideStringField;
    QrNFECabAdest_xCpl: TWideStringField;
    QrNFECabAdest_xBairro: TWideStringField;
    QrNFECabAdest_cMun: TIntegerField;
    QrNFECabAdest_xMun: TWideStringField;
    QrNFECabAdest_UF: TWideStringField;
    QrNFECabAdest_CEP: TWideStringField;
    QrNFECabAdest_cPais: TIntegerField;
    QrNFECabAdest_xPais: TWideStringField;
    QrNFECabAdest_fone: TWideStringField;
    QrNFECabAdest_IE: TWideStringField;
    QrNFECabAdest_ISUF: TWideStringField;
    QrNFECabAICMSTot_vBC: TFloatField;
    QrNFECabAICMSTot_vICMS: TFloatField;
    QrNFECabAICMSTot_vBCST: TFloatField;
    QrNFECabAICMSTot_vST: TFloatField;
    QrNFECabAICMSTot_vProd: TFloatField;
    QrNFECabAICMSTot_vFrete: TFloatField;
    QrNFECabAICMSTot_vSeg: TFloatField;
    QrNFECabAICMSTot_vDesc: TFloatField;
    QrNFECabAICMSTot_vII: TFloatField;
    QrNFECabAICMSTot_vIPI: TFloatField;
    QrNFECabAICMSTot_vPIS: TFloatField;
    QrNFECabAICMSTot_vCOFINS: TFloatField;
    QrNFECabAICMSTot_vOutro: TFloatField;
    QrNFECabAICMSTot_vNF: TFloatField;
    QrNFECabAISSQNtot_vServ: TFloatField;
    QrNFECabAISSQNtot_vBC: TFloatField;
    QrNFECabAISSQNtot_vISS: TFloatField;
    QrNFECabAISSQNtot_vPIS: TFloatField;
    QrNFECabAISSQNtot_vCOFINS: TFloatField;
    QrNFECabARetTrib_vRetPIS: TFloatField;
    QrNFECabARetTrib_vRetCOFINS: TFloatField;
    QrNFECabARetTrib_vRetCSLL: TFloatField;
    QrNFECabARetTrib_vBCIRRF: TFloatField;
    QrNFECabARetTrib_vIRRF: TFloatField;
    QrNFECabARetTrib_vBCRetPrev: TFloatField;
    QrNFECabARetTrib_vRetPrev: TFloatField;
    QrNFECabAModFrete: TSmallintField;
    QrNFECabATransporta_CNPJ: TWideStringField;
    QrNFECabATransporta_CPF: TWideStringField;
    QrNFECabATransporta_XNome: TWideStringField;
    QrNFECabATransporta_IE: TWideStringField;
    QrNFECabATransporta_XEnder: TWideStringField;
    QrNFECabATransporta_XMun: TWideStringField;
    QrNFECabATransporta_UF: TWideStringField;
    QrNFECabARetTransp_vServ: TFloatField;
    QrNFECabARetTransp_vBCRet: TFloatField;
    QrNFECabARetTransp_PICMSRet: TFloatField;
    QrNFECabARetTransp_vICMSRet: TFloatField;
    QrNFECabARetTransp_CFOP: TWideStringField;
    QrNFECabARetTransp_CMunFG: TWideStringField;
    QrNFECabAVeicTransp_Placa: TWideStringField;
    QrNFECabAVeicTransp_UF: TWideStringField;
    QrNFECabAVeicTransp_RNTC: TWideStringField;
    QrNFECabACobr_Fat_NFat: TWideStringField;
    QrNFECabACobr_Fat_vOrig: TFloatField;
    QrNFECabACobr_Fat_vDesc: TFloatField;
    QrNFECabACobr_Fat_vLiq: TFloatField;
    QrNFECabAInfAdic_InfCpl: TWideMemoField;
    QrNFECabAExporta_UFEmbarq: TWideStringField;
    QrNFECabAExporta_XLocEmbarq: TWideStringField;
    QrNFECabACompra_XNEmp: TWideStringField;
    QrNFECabACompra_XPed: TWideStringField;
    QrNFECabACompra_XCont: TWideStringField;
    QrNFECabAStatus: TSmallintField;
    QrNFECabA_Ativo_: TSmallintField;
    QrNFECabALk: TIntegerField;
    QrNFECabADataCad: TDateField;
    QrNFECabADataAlt: TDateField;
    QrNFECabAUserCad: TIntegerField;
    QrNFECabAUserAlt: TIntegerField;
    QrNFECabAAlterWeb: TSmallintField;
    QrNFECabAAtivo: TSmallintField;
    QrNFeLayI: TmySQLQuery;
    QrNFeLayIGrupo: TWideStringField;
    QrNFeLayICodigo: TWideStringField;
    QrNFeLayIID: TWideStringField;
    QrNFeLayICampo: TWideStringField;
    QrNFeLayIDescricao: TWideStringField;
    QrNFeLayIElemento: TWideStringField;
    QrNFeLayIPai: TWideStringField;
    QrNFeLayITipo: TWideStringField;
    QrNFeLayIOcorMin: TSmallintField;
    QrNFeLayIOcorMax: TIntegerField;
    QrNFeLayITamMin: TSmallintField;
    QrNFeLayITamMax: TIntegerField;
    QrNFeLayITamVar: TWideStringField;
    QrNFeLayIDeciCasas: TSmallintField;
    QrNFeLayIObservacao: TWideMemoField;
    QrNFeLayILeftZeros: TSmallintField;
    QrNFeLayIInfoVazio: TSmallintField;
    QrNFeLayINO_Grupo: TWideStringField;
    QrNFEItsN: TmySQLQuery;
    QrNFEItsNFatID: TIntegerField;
    QrNFEItsNFatNum: TIntegerField;
    QrNFEItsNEmpresa: TIntegerField;
    QrNFEItsNnItem: TIntegerField;
    QrNFEItsNICMS_Orig: TSmallintField;
    QrNFEItsNICMS_CST: TSmallintField;
    QrNFEItsNICMS_modBC: TSmallintField;
    QrNFEItsNICMS_pRedBC: TFloatField;
    QrNFEItsNICMS_vBC: TFloatField;
    QrNFEItsNICMS_pICMS: TFloatField;
    QrNFEItsNICMS_vICMS: TFloatField;
    QrNFEItsNICMS_modBCST: TSmallintField;
    QrNFEItsNICMS_pMVAST: TFloatField;
    QrNFEItsNICMS_pRedBCST: TFloatField;
    QrNFEItsNICMS_vBCST: TFloatField;
    QrNFEItsNICMS_pICMSST: TFloatField;
    QrNFEItsNICMS_vICMSST: TFloatField;
    QrNFEItsO: TmySQLQuery;
    QrNFeTotI: TmySQLQuery;
    QrNFeTotITem_IPI: TFloatField;
    QrNFeTotIprod_vProd: TFloatField;
    QrNFeTotIprod_vFrete: TFloatField;
    QrNFeTotIprod_vSeg: TFloatField;
    QrNFeTotIprod_vDesc: TFloatField;
    QrNFeTotN: TmySQLQuery;
    QrNFeTotNICMS_vBC: TFloatField;
    QrNFeTotNICMS_vICMS: TFloatField;
    QrNFeTotNICMS_vBCST: TFloatField;
    QrNFeTotNICMS_vICMSST: TFloatField;
    QrNFeTotO: TmySQLQuery;
    QrNFeTotOIPI_vIPI: TFloatField;
    QrNFeXMLi: TmySQLQuery;
    QrNFeXMLiFatID: TIntegerField;
    QrNFeXMLiFatNum: TIntegerField;
    QrNFeXMLiEmpresa: TIntegerField;
    QrNFeXMLiOrdem: TIntegerField;
    QrNFeXMLiCodigo: TWideStringField;
    QrNFeXMLiID: TWideStringField;
    QrNFeXMLiValor: TWideStringField;
    QrNFeLEnC: TmySQLQuery;
    QrNFeLEnCId: TWideStringField;
    QrNFeLEnCFatID: TIntegerField;
    QrNFeLEnCFatNum: TIntegerField;
    QrNFeInut: TmySQLQuery;
    QrNFeInutCodigo: TIntegerField;
    QrVolumes: TmySQLQuery;
    QrVolumesqVol: TWideStringField;
    QrVolumesesp: TWideStringField;
    QrVolumesmarca: TWideStringField;
    QrVolumesnVol: TWideStringField;
    QrVolumespesoL: TFloatField;
    QrVolumespesoB: TFloatField;
    QrFatYIts: TmySQLQuery;
    QrFatYItsFatID: TIntegerField;
    QrFatYItsFatNum: TFloatField;
    QrFatYItsVencimento: TDateField;
    QrFatYItsFatParcela: TIntegerField;
    QrFatYItsDuplicata: TWideStringField;
    QrFatYItsValor: TFloatField;
    QrFatYItsControle: TIntegerField;
    QrFatYItsSub: TSmallintField;
    QrFatYTot: TmySQLQuery;
    QrFatYTotFatID: TIntegerField;
    QrFatYTotFatNum: TFloatField;
    QrFatYTotValor: TFloatField;
    QrNFECabG: TmySQLQuery;
    QrNFECabGentrega_CNPJ: TWideStringField;
    QrNFECabGentrega_xLgr: TWideStringField;
    QrNFECabGentrega_nro: TWideStringField;
    QrNFECabGentrega_xCpl: TWideStringField;
    QrNFECabGentrega_xBairro: TWideStringField;
    QrNFECabGentrega_cMun: TIntegerField;
    QrNFECabGentrega_xMun: TWideStringField;
    QrNFECabGentrega_UF: TWideStringField;
    QrNFECabB: TmySQLQuery;
    QrNFECabBrefNFe: TWideStringField;
    QrNFECabBrefNF_cUF: TSmallintField;
    QrNFECabBrefNF_AAMM: TIntegerField;
    QrNFECabBrefNF_CNPJ: TWideStringField;
    QrNFECabBrefNF_mod: TSmallintField;
    QrNFECabBrefNF_serie: TIntegerField;
    QrNFECabBrefNF_nNF: TIntegerField;
    QrNFECabF: TmySQLQuery;
    QrNFECabFretirada_CNPJ: TWideStringField;
    QrNFECabFretirada_xLgr: TWideStringField;
    QrNFECabFretirada_nro: TWideStringField;
    QrNFECabFretirada_xCpl: TWideStringField;
    QrNFECabFretirada_xBairro: TWideStringField;
    QrNFECabFretirada_cMun: TIntegerField;
    QrNFECabFretirada_xMun: TWideStringField;
    QrNFECabFretirada_UF: TWideStringField;
    QrNFECabXReb: TmySQLQuery;
    QrNFECabXRebplaca: TWideStringField;
    QrNFECabXRebUF: TWideStringField;
    QrNFECabXRebRNTC: TWideStringField;
    QrNFECabXVol: TmySQLQuery;
    QrNFECabXVolqVol: TFloatField;
    QrNFECabXVolesp: TWideStringField;
    QrNFECabXVolmarca: TWideStringField;
    QrNFECabXVolnVol: TWideStringField;
    QrNFECabXVolpesoL: TFloatField;
    QrNFECabXVolpesoB: TFloatField;
    QrNFECabXVolControle: TIntegerField;
    QrNFECabXLac: TmySQLQuery;
    QrNFECabXLacnLacre: TWideStringField;
    QrNFECabY: TmySQLQuery;
    QrNFECabYnDup: TWideStringField;
    QrNFECabYdVenc: TDateField;
    QrNFECabYvDup: TFloatField;
    QrCustomiz_: TmySQLQuery;
    QrCustomiz_SiglaCustm: TWideStringField;
    QrNFEItsV: TmySQLQuery;
    QrNFEItsVInfAdProd: TWideMemoField;
    QrNFEInfCuz: TmySQLQuery;
    QrNFEInfCuzNome: TWideStringField;
    QrFisRegCad: TmySQLQuery;
    QrFisRegCadICMS_Usa: TSmallintField;
    QrFisRegCadIPI_Usa: TSmallintField;
    QrFisRegCadPIS_Usa: TSmallintField;
    QrFisRegCadCOFINS_Usa: TSmallintField;
    QrFisRegCadIR_Usa: TSmallintField;
    QrFisRegCadIR_Alq: TFloatField;
    QrFisRegCadCS_Usa: TSmallintField;
    QrFisRegCadCS_Alq: TFloatField;
    QrFisRegCadISS_Usa: TSmallintField;
    QrFisRegCadISS_Alq: TFloatField;
    QrAliq: TmySQLQuery;
    QrAliqICMSAliq: TFloatField;
    QrServico_: TmySQLQuery;
    QrServico_infAdProd: TWideStringField;
    QrServico_cListServ: TIntegerField;
    QrServico_Codigo: TIntegerField;
    QrServico_CodUsu: TIntegerField;
    QrServico_Nome: TWideStringField;
    QrServico_PrecoPc: TFloatField;
    QrServico_PrecoM2: TFloatField;
    QrServico_Precokg: TFloatField;
    QrNFeTotU: TmySQLQuery;
    QrNFeTotUISSQN_vBC: TFloatField;
    QrNFeTotUISSQN_vISSQN: TFloatField;
    QrNFEItsQ: TmySQLQuery;
    QrNFEItsQFatID: TIntegerField;
    QrNFEItsQFatNum: TIntegerField;
    QrNFEItsQEmpresa: TIntegerField;
    QrNFEItsQnItem: TIntegerField;
    QrNFEItsQPIS_CST: TSmallintField;
    QrNFEItsQPIS_vBC: TFloatField;
    QrNFEItsQPIS_pPIS: TFloatField;
    QrNFEItsQPIS_vPIS: TFloatField;
    QrNFEItsQPIS_qBCProd: TFloatField;
    QrNFEItsQPIS_vAliqProd: TFloatField;
    QrNFEItsS: TmySQLQuery;
    QrNFEItsSFatID: TIntegerField;
    QrNFEItsSFatNum: TIntegerField;
    QrNFEItsSEmpresa: TIntegerField;
    QrNFEItsSnItem: TIntegerField;
    QrNFEItsSCOFINS_CST: TSmallintField;
    QrNFEItsSCOFINS_vBC: TFloatField;
    QrNFEItsSCOFINS_pCOFINS: TFloatField;
    QrNFEItsSCOFINS_qBCProd: TFloatField;
    QrNFEItsSCOFINS_vAliqProd: TFloatField;
    QrNFEItsSCOFINS_vCOFINS: TFloatField;
    QrNFEItsU: TMySQLQuery;
    QrNFEItsUFatID: TIntegerField;
    QrNFEItsUFatNum: TIntegerField;
    QrNFEItsUEmpresa: TIntegerField;
    QrNFEItsUnItem: TIntegerField;
    QrNFEItsUISSQN_vBC: TFloatField;
    QrNFEItsUISSQN_vAliq: TFloatField;
    QrNFEItsUISSQN_vISSQN: TFloatField;
    QrNFEItsUISSQN_cMunFG: TIntegerField;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    DsDTB_Munici: TDataSource;
    QrNFeTotP: TmySQLQuery;
    QrNFeTotPII_vII: TFloatField;
    QrNFeTotQ: TmySQLQuery;
    QrNFeTotQPIS_vPIS: TFloatField;
    QrNFeTotS: TmySQLQuery;
    QrNFeTotSCOFINS_vCOFINS: TFloatField;
    QrGraSrv1: TmySQLQuery;
    QrGraSrv1Codigo: TIntegerField;
    QrGraSrv1CodUsu: TIntegerField;
    QrGraSrv1Nome: TWideStringField;
    QrGraSrv1infAdProd: TWideStringField;
    QrGraSrv1cListServ: TIntegerField;
    DsGraSrv1: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXNO_GGX: TWideStringField;
    DsGraGRuX: TDataSource;
    QrLocFisRegCad: TmySQLQuery;
    QrLocFisRegCadICMS_Usa: TSmallintField;
    QrLocFisRegCadICMS_Alq: TFloatField;
    QrLocFisRegCadICMS_Frm: TWideMemoField;
    QrLocFisRegCadIPI_Usa: TSmallintField;
    QrLocFisRegCadIPI_Alq: TFloatField;
    QrLocFisRegCadIPI_Frm: TWideMemoField;
    QrLocFisRegCadPIS_Usa: TSmallintField;
    QrLocFisRegCadPIS_Alq: TFloatField;
    QrLocFisRegCadPIS_Frm: TWideMemoField;
    QrLocFisRegCadCOFINS_Usa: TSmallintField;
    QrLocFisRegCadCOFINS_Alq: TFloatField;
    QrLocFisRegCadCOFINS_Frm: TWideMemoField;
    QrLocFisRegCadIR_Usa: TSmallintField;
    QrLocFisRegCadIR_Alq: TFloatField;
    QrLocFisRegCadIR_Frm: TWideMemoField;
    QrLocFisRegCadCS_Usa: TSmallintField;
    QrLocFisRegCadCS_Alq: TFloatField;
    QrLocFisRegCadCS_Frm: TWideMemoField;
    QrLocFisRegCadISS_Usa: TSmallintField;
    QrLocFisRegCadISS_Alq: TFloatField;
    QrLocFisRegCadISS_Frm: TWideMemoField;
    DsDTB_UFs: TDataSource;
    QrDTB_UFs: TmySQLQuery;
    QrDTBMunici1: TmySQLQuery;
    QrDTBMunici1Codigo: TIntegerField;
    QrDTBMunici1Nome: TWideStringField;
    DsDTBMunici1: TDataSource;
    DsBACEN_Pais: TDataSource;
    QrBACEN_Pais: TmySQLQuery;
    QrBACEN_PaisCodigo: TIntegerField;
    QrBACEN_PaisNome: TWideStringField;
    QrnNFDupl: TmySQLQuery;
    QrnNFInut: TmySQLQuery;
    QrRefer1: TmySQLQuery;
    QrRefer1DataE: TDateField;
    QrRefer1NFInn: TIntegerField;
    QrRefer1GraGruX: TIntegerField;
    QrRefer1TipoNF: TSmallintField;
    QrRefer1refNFe: TWideStringField;
    QrRefer1modNF: TSmallintField;
    QrRefer1Serie: TIntegerField;
    QrRefer1OutQtdkg: TFloatField;
    QrRefer1No_Peso: TWideStringField;
    QrRefer1OutQtdPc: TFloatField;
    QrRefer1NO_Pecas: TWideStringField;
    QrRefer1OutQtdM2: TFloatField;
    QrRefer1NO_Area: TWideStringField;
    QrRefer1OutQtdVal: TFloatField;
    QrRefer1Benefikg: TFloatField;
    QrRefer1BenefiPc: TFloatField;
    QrRefer1BenefiM2: TFloatField;
    QrRefer1Cliente: TIntegerField;
    QrRefer1CNPJ_CPF: TWideStringField;
    QrRefer1DTB_UF: TWideStringField;
    QrProds1_: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    FloatField2: TFloatField;
    QrProds1ICMS_Val: TFloatField;
    QrProds1IPI_Val: TFloatField;
    FloatField3: TFloatField;
    IntegerField4: TIntegerField;
    StringField2: TWideStringField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    StringField5: TWideStringField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    StringField7: TWideStringField;
    IntegerField7: TIntegerField;
    LargeintField1: TLargeintField;
    StringField8: TWideStringField;
    StringField9: TWideStringField;
    StringField10: TWideStringField;
    StringField11: TWideStringField;
    IntegerField8: TIntegerField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    StringField12: TWideStringField;
    StringField13: TWideStringField;
    StringField14: TWideStringField;
    StringField15: TWideStringField;
    StringField16: TWideStringField;
    StringField17: TWideStringField;
    StringField18: TWideStringField;
    StringField19: TWideStringField;
    SmallintField3: TSmallintField;
    StringField20: TWideStringField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    StringField21: TWideStringField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    FloatField14: TFloatField;
    SmallintField4: TSmallintField;
    SmallintField5: TSmallintField;
    FloatField15: TFloatField;
    FloatField16: TFloatField;
    FloatField17: TFloatField;
    FloatField18: TFloatField;
    SmallintField6: TSmallintField;
    SmallintField7: TSmallintField;
    SmallintField8: TSmallintField;
    FloatField19: TFloatField;
    SmallintField9: TSmallintField;
    IntegerField9: TIntegerField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaFilial: TIntegerField;
    QrListServ: TmySQLQuery;
    DsListServ: TDataSource;
    QrListServCodTxt: TWideStringField;
    QrListServCodigo: TIntegerField;
    QrListServNome: TWideStringField;
    QrListServDescricao: TWideMemoField;
    QrParcela: TmySQLQuery;
    QrParcelaFatParcela: TIntegerField;
    QrProds1NO_UNIDADE: TWideStringField;
    QrProds1Empresa: TIntegerField;
    QrProds1Preco: TFloatField;
    QrProds1CU_PRODUTO: TIntegerField;
    QrProds1CONTROLE: TIntegerField;
    QrProds1InfAdCuztm: TIntegerField;
    QrProds1PercCustom: TFloatField;
    QrProds1Total: TFloatField;
    QrProds1Qtde: TFloatField;
    QrProds1CFOP: TWideStringField;
    QrProds1CFOP_Contrib: TSmallintField;
    QrProds1CFOP_MesmaUF: TSmallintField;
    QrProds1CFOP_Proprio: TSmallintField;
    QrProds1CU_GRUPO: TIntegerField;
    QrProds1NO_GRUPO: TWideStringField;
    QrProds1CST_A: TSmallintField;
    QrProds1CST_B: TSmallintField;
    QrProds1CST_T: TWideStringField;
    QrProds1NCM: TWideStringField;
    QrProds1IPI_CST: TSmallintField;
    QrProds1IPI_cEnq: TWideStringField;
    QrProds1GraCorCad: TIntegerField;
    QrProds1CU_COR: TIntegerField;
    QrProds1NO_COR: TWideStringField;
    QrProds1NO_TAM: TWideStringField;
    QrProds1CO_GRUPO: TIntegerField;
    QrProds1Desc_Per: TLargeintField;
    QrProds1InfAdProd: TWideStringField;
    QrProds1MedidaC: TFloatField;
    QrProds1MedidaL: TFloatField;
    QrProds1MedidaA: TFloatField;
    QrProds1MedidaE: TFloatField;
    QrProds1Medida1: TWideStringField;
    QrProds1Medida2: TWideStringField;
    QrProds1Medida3: TWideStringField;
    QrProds1Medida4: TWideStringField;
    QrProds1Sigla1: TWideStringField;
    QrProds1Sigla2: TWideStringField;
    QrProds1Sigla3: TWideStringField;
    QrProds1Sigla4: TWideStringField;
    QrProds1PrintTam: TSmallintField;
    QrProds1PIS_CST: TSmallintField;
    QrProds1PIS_AlqV: TFloatField;
    QrProds1PISST_AlqV: TFloatField;
    QrProds1COFINS_CST: TSmallintField;
    QrProds1COFINS_AlqP: TFloatField;
    QrProds1COFINS_AlqV: TFloatField;
    QrProds1COFINSST_AlqP: TFloatField;
    QrProds1COFINSST_AlqV: TFloatField;
    QrProds1ICMS_modBC: TSmallintField;
    QrProds1ICMS_modBCST: TSmallintField;
    QrProds1ICMS_pRedBC: TFloatField;
    QrProds1ICMS_pRedBCST: TFloatField;
    QrProds1ICMS_pMVAST: TFloatField;
    QrProds1ICMS_pICMSST: TFloatField;
    QrProds1IPI_vUnid: TFloatField;
    QrProds1IPI_TpTrib: TSmallintField;
    QrProds1IPI_pIPI: TFloatField;
    QrProds1ICMS_Pauta: TFloatField;
    QrProds1ICMS_MaxTab: TFloatField;
    QrProds1cGTIN_EAN: TWideStringField;
    QrProds1EX_TIPI: TWideStringField;
    QrProds1Servico: TIntegerField;
    QrProds1PrintCor: TSmallintField;
    QrServi1: TmySQLQuery;
    QrServi1Codigo: TIntegerField;
    QrServi1Nome: TWideStringField;
    QrServi1infAdProd: TWideStringField;
    QrServi1cListServ: TIntegerField;
    QrServi1Sigla: TWideStringField;
    QrServi1UnidMed: TIntegerField;
    QrFisRegCadide_natOp: TWideStringField;
    QrAliqICMS_Usa: TSmallintField;
    QrAliqPIS_Usa: TSmallintField;
    QrNFECabAFreteExtra: TFloatField;
    QrNFECabASegurExtra: TFloatField;
    QrNFEItsP: TmySQLQuery;
    QrNFEItsPFatID: TIntegerField;
    QrNFEItsPFatNum: TIntegerField;
    QrNFEItsPEmpresa: TIntegerField;
    QrNFEItsPnItem: TIntegerField;
    QrNFEItsPII_vBC: TFloatField;
    QrNFEItsPII_vDespAdu: TFloatField;
    QrNFEItsPII_vII: TFloatField;
    QrNFEItsPII_vIOF: TFloatField;
    QrFilialUF_Servico: TWideStringField;
    QrFilialNFeSerNum: TWideStringField;
    QrEmpresaNIRE: TWideStringField;
    QrDestNIRE: TWideStringField;
    QrTransportaNIRE: TWideStringField;
    QrOpcoesNFeAppCode: TSmallintField;
    QrAliqCOFINS_Usa: TSmallintField;
    QrProds1PIS_pRedBC: TFloatField;
    QrProds1PISST_pRedBCST: TFloatField;
    QrProds1COFINS_pRedBC: TFloatField;
    QrProds1COFINSST_pRedBCST: TFloatField;
    QrProds1PISST_AlqP: TFloatField;
    QrProds1PIS_AlqP: TFloatField;
    QrSumRetFis: TmySQLQuery;
    QrSumRetFisprod_vProd: TFloatField;
    QrSumRetFisICMSRec_vBC: TFloatField;
    QrSumRetFisICMSRec_pRedBC: TFloatField;
    QrSumRetFisICMSRec_pAliq: TFloatField;
    QrSumRetFisICMSRec_vICMS: TFloatField;
    QrSumRetFisIPIRec_vBC: TFloatField;
    QrSumRetFisIPIRec_pRedBC: TFloatField;
    QrSumRetFisIPIRec_pAliq: TFloatField;
    QrSumRetFisIPIRec_vIPI: TFloatField;
    QrSumRetFisPISRec_vBC: TFloatField;
    QrSumRetFisPISRec_pRedBC: TFloatField;
    QrSumRetFisPISRec_pAliq: TFloatField;
    QrSumRetFisPISRec_vPIS: TFloatField;
    QrSumRetFisCOFINSRec_vBC: TFloatField;
    QrSumRetFisCOFINSRec_pRedBC: TFloatField;
    QrSumRetFisCOFINSRec_pAliq: TFloatField;
    QrSumRetFisCOFINSRec_vCOFINS: TFloatField;
    QrEnt_Doc: TmySQLQuery;
    QrEnt_DocCodigo: TIntegerField;
    QrLocNFe: TmySQLQuery;
    QrLocNFeFatNum: TIntegerField;
    QrEmb: TmySQLQuery;
    QrEmbNivel1: TIntegerField;
    QrEmbICMSRec_pRedBC: TFloatField;
    QrEmbIPIRec_pRedBC: TFloatField;
    QrEmbPISRec_pRedBC: TFloatField;
    QrEmbCOFINSRec_pRedBC: TFloatField;
    QrCod: TmySQLQuery;
    QrCodNivel1: TIntegerField;
    QrCodICMSRec_pRedBC: TFloatField;
    QrCodIPIRec_pRedBC: TFloatField;
    QrCodPISRec_pRedBC: TFloatField;
    QrCodCOFINSRec_pRedBC: TFloatField;
    QrCodICMSRec_pAliq: TFloatField;
    QrCodIPIRec_pAliq: TFloatField;
    QrCodPISRec_pAliq: TFloatField;
    QrCodCOFINSRec_pAliq: TFloatField;
    QrCodICMSRec_tCalc: TSmallintField;
    QrCodIPIRec_tCalc: TSmallintField;
    QrCodPISRec_tCalc: TSmallintField;
    QrCodCOFINSRec_tCalc: TSmallintField;
    QrA: TmySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrAIDCtrl: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_nFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrAFreteExtra: TFloatField;
    QrASegurExtra: TFloatField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAICMSRec_pRedBC: TFloatField;
    QrAICMSRec_vBC: TFloatField;
    QrAICMSRec_pAliq: TFloatField;
    QrAICMSRec_vICMS: TFloatField;
    QrAIPIRec_pRedBC: TFloatField;
    QrAIPIRec_vBC: TFloatField;
    QrAIPIRec_pAliq: TFloatField;
    QrAIPIRec_vIPI: TFloatField;
    QrAPISRec_pRedBC: TFloatField;
    QrAPISRec_vBC: TFloatField;
    QrAPISRec_pAliq: TFloatField;
    QrAPISRec_vPIS: TFloatField;
    QrACOFINSRec_pRedBC: TFloatField;
    QrACOFINSRec_vBC: TFloatField;
    QrACOFINSRec_pAliq: TFloatField;
    QrACOFINSRec_vCOFINS: TFloatField;
    QrI: TmySQLQuery;
    QrInItem: TIntegerField;
    QrINivel1: TIntegerField;
    QrIprod_qCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrN: TmySQLQuery;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrO: TmySQLQuery;
    QrOIPI_vIPI: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrQ: TmySQLQuery;
    QrQPIS_CST: TSmallintField;
    QrQPIS_vBC: TFloatField;
    QrQPIS_pPIS: TFloatField;
    QrQPIS_vPIS: TFloatField;
    QrQPIS_qBCProd: TFloatField;
    QrQPIS_vAliqProd: TFloatField;
    QrQPISST_vBC: TFloatField;
    QrQPISST_pPIS: TFloatField;
    QrQPISST_qBCProd: TFloatField;
    QrQPISST_vAliqProd: TFloatField;
    QrQPISST_vPIS: TFloatField;
    QrS: TmySQLQuery;
    QrSCOFINS_CST: TSmallintField;
    QrSCOFINS_vBC: TFloatField;
    QrSCOFINS_pCOFINS: TFloatField;
    QrSCOFINS_qBCProd: TFloatField;
    QrSCOFINS_vAliqProd: TFloatField;
    QrSCOFINS_vCOFINS: TFloatField;
    QrSCOFINSST_vBC: TFloatField;
    QrSCOFINSST_pCOFINS: TFloatField;
    QrSCOFINSST_qBCProd: TFloatField;
    QrSCOFINSST_vAliqProd: TFloatField;
    QrSCOFINSST_vCOFINS: TFloatField;
    QrXVol: TmySQLQuery;
    QrXVolControle: TIntegerField;
    QrXVolqVol: TFloatField;
    QrXVolesp: TWideStringField;
    QrXVolmarca: TWideStringField;
    QrXVolnVol: TWideStringField;
    QrXVolpesoL: TFloatField;
    QrXVolpesoB: TFloatField;
    QrLocNFeDataFiscal: TDateField;
    Qr_D_E: TmySQLQuery;
    Qr_D_EFatID: TIntegerField;
    Qr_D_EFatNum: TIntegerField;
    Qr_D_EEmpresa: TIntegerField;
    Qr_D_ECodInfoDest: TIntegerField;
    Qr_D_ECodInfoEmit: TIntegerField;
    Qr_D_Eemit_CNPJ: TWideStringField;
    Qr_D_Eemit_CPF: TWideStringField;
    Qr_D_Edest_CNPJ: TWideStringField;
    Qr_D_Edest_CPF: TWideStringField;
    QrEmit: TmySQLQuery;
    QrADataFiscal: TDateField;
    QrASINTEGRA_ExpDeclNum: TWideStringField;
    QrASINTEGRA_ExpDeclDta: TDateField;
    QrASINTEGRA_ExpNat: TWideStringField;
    QrASINTEGRA_ExpRegNum: TWideStringField;
    QrASINTEGRA_ExpRegDta: TDateField;
    QrASINTEGRA_ExpConhNum: TWideStringField;
    QrASINTEGRA_ExpConhDta: TDateField;
    QrASINTEGRA_ExpConhTip: TWideStringField;
    QrASINTEGRA_ExpPais: TWideStringField;
    QrASINTEGRA_ExpAverDta: TDateField;
    QrACodInfoEmit: TIntegerField;
    QrACodInfoDest: TIntegerField;
    QrEmitCodigo: TIntegerField;
    QrEmitTipo: TSmallintField;
    QrEmitCNPJ: TWideStringField;
    QrEmitCPF: TWideStringField;
    QrEmitIE: TWideStringField;
    QrEmitRG: TWideStringField;
    QrEmitCNAE: TWideStringField;
    QrEmitNO_ENT: TWideStringField;
    QrEmitFANTASIA: TWideStringField;
    QrEmitRUA: TWideStringField;
    QrEmitCOMPL: TWideStringField;
    QrEmitBAIRRO: TWideStringField;
    QrEmitTe1: TWideStringField;
    QrEmitNO_LOGRAD: TWideStringField;
    QrEmitNO_Munici: TWideStringField;
    QrEmitNO_UF: TWideStringField;
    QrEmitNO_Pais: TWideStringField;
    QrEmitSUFRAMA: TWideStringField;
    QrEmitL_CNPJ: TWideStringField;
    QrEmitL_Ativo: TSmallintField;
    QrEmitLLograd: TSmallintField;
    QrEmitLRua: TWideStringField;
    QrEmitLCompl: TWideStringField;
    QrEmitLNumero: TIntegerField;
    QrEmitLBairro: TWideStringField;
    QrEmitLCidade: TWideStringField;
    QrEmitLUF: TSmallintField;
    QrEmitNO_LLOGRAD: TWideStringField;
    QrEmitLCodMunici: TIntegerField;
    QrEmitNO_LMunici: TWideStringField;
    QrEmitNO_LUF: TWideStringField;
    QrEmitNIRE: TWideStringField;
    QrEmitIEST: TWideStringField;
    Ds_D_E: TDataSource;
    Qr_D_EIDCtrl: TIntegerField;
    Qr_D_EFatID_TXT: TWideStringField;
    QrAtoArq: TmySQLQuery;
    QrAtoArqIDCtrl: TIntegerField;
    QrNFeArq: TmySQLQuery;
    QrNFeArqIDCtrl_CabA: TIntegerField;
    QrNFeArqIDCtrl_Arq: TIntegerField;
    QrNFeArqId: TWideStringField;
    QrNFeArqFatID: TIntegerField;
    QrNFeArqFatNum: TIntegerField;
    QrNFeArqEmpresa: TIntegerField;
    QrNFeAut: TmySQLQuery;
    QrNFeAutIDCtrl: TIntegerField;
    QrNFeAutLoteEnv: TIntegerField;
    QrNFeAutId: TWideStringField;
    QrNFeCan: TmySQLQuery;
    QrNFeCanIDCtrl: TIntegerField;
    QrNFeCanId: TWideStringField;
    QrNFECabAIDCtrl: TIntegerField;
    QrNFeInu: TmySQLQuery;
    QrNFeInuCodigo: TIntegerField;
    QrNFeInuCodUsu: TIntegerField;
    QrNFeInuNome: TWideStringField;
    QrNFeInuEmpresa: TIntegerField;
    QrNFeInuDataC: TDateField;
    QrNFeInuversao: TFloatField;
    QrNFeInuId: TWideStringField;
    QrNFeInutpAmb: TSmallintField;
    QrNFeInucUF: TSmallintField;
    QrNFeInuano: TSmallintField;
    QrNFeInuCNPJ: TWideStringField;
    QrNFeInumodelo: TSmallintField;
    QrNFeInuSerie: TIntegerField;
    QrNFeInunNFIni: TIntegerField;
    QrNFeInunNFFim: TIntegerField;
    QrNFeInuJustif: TIntegerField;
    QrNFeInuxJust: TWideStringField;
    QrNFeInucStat: TIntegerField;
    QrNFeInuxMotivo: TWideStringField;
    QrNFeInudhRecbto: TDateTimeField;
    QrNFeInunProt: TWideStringField;
    QrNFeInuLk: TIntegerField;
    QrNFeInuDataCad: TDateField;
    QrNFeInuDataAlt: TDateField;
    QrNFeInuUserCad: TIntegerField;
    QrNFeInuUserAlt: TIntegerField;
    QrNFeInuAlterWeb: TSmallintField;
    QrNFeInuAtivo: TSmallintField;
    QrNFeInuXML_Inu: TWideMemoField;
    QrOpcoesNFeEntiTipCto: TIntegerField;
    QrOpcoesNFeMyEmailNFe: TWideStringField;
    QrFilialSINTEGRA_Path: TWideStringField;
    QrFilialDirDANFEs: TWideStringField;
    QrFilialDirNFeProt: TWideStringField;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_NFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqUserAlt: TIntegerField;
    QrArqAtivo: TSmallintField;
    QrNFECabACodInfoDest: TIntegerField;
    QrOpcoesNFeAssDigMode: TSmallintField;
    QrNFEItsOFatID: TIntegerField;
    QrNFEItsOFatNum: TIntegerField;
    QrNFEItsOEmpresa: TIntegerField;
    QrNFEItsOnItem: TIntegerField;
    QrNFEItsOIPI_clEnq: TWideStringField;
    QrNFEItsOIPI_CNPJProd: TWideStringField;
    QrNFEItsOIPI_cSelo: TWideStringField;
    QrNFEItsOIPI_qSelo: TFloatField;
    QrNFEItsOIPI_cEnq: TWideStringField;
    QrNFEItsOIPI_CST: TSmallintField;
    QrNFEItsOIPI_vBC: TFloatField;
    QrNFEItsOIPI_qUnid: TFloatField;
    QrNFEItsOIPI_vUnid: TFloatField;
    QrNFEItsOIPI_pIPI: TFloatField;
    QrNFEItsOIPI_vIPI: TFloatField;
    QrNFEItsO_Ativo_: TSmallintField;
    QrNFEItsOLk: TIntegerField;
    QrNFEItsODataCad: TDateField;
    QrNFEItsODataAlt: TDateField;
    QrNFEItsOUserCad: TIntegerField;
    QrNFEItsOUserAlt: TIntegerField;
    QrNFEItsOAlterWeb: TSmallintField;
    QrNFEItsOAtivo: TSmallintField;
    QrFilialNFeSerVal: TDateField;
    QrFilialNFeSerAvi: TSmallintField;
    QrGraGruCST: TmySQLQuery;
    QrGraGruCSTCST_B: TSmallintField;
    QrAliqCST_B: TWideStringField;
    QrNFECabALoteEnv: TIntegerField;
    QrNFECabAinfProt_Id: TWideStringField;
    QrNFECabAinfProt_tpAmb: TSmallintField;
    QrNFECabAinfProt_verAplic: TWideStringField;
    QrNFECabAinfProt_dhRecbto: TDateTimeField;
    QrNFECabAinfProt_nProt: TWideStringField;
    QrNFECabAinfProt_digVal: TWideStringField;
    QrNFECabAinfProt_cStat: TIntegerField;
    QrNFECabAinfProt_xMotivo: TWideStringField;
    QrNFECabAinfCanc_Id: TWideStringField;
    QrNFECabAinfCanc_tpAmb: TSmallintField;
    QrNFECabAinfCanc_verAplic: TWideStringField;
    QrNFECabAinfCanc_dhRecbto: TDateTimeField;
    QrNFECabAinfCanc_nProt: TWideStringField;
    QrNFECabAinfCanc_digVal: TWideStringField;
    QrNFECabAinfCanc_cStat: TIntegerField;
    QrNFECabAinfCanc_xMotivo: TWideStringField;
    QrNFECabAinfCanc_cJust: TIntegerField;
    QrNFECabAinfCanc_xJust: TWideStringField;
    QrNFECabAFisRegCad: TIntegerField;
    QrNFECabACartEmiss: TIntegerField;
    QrNFECabATabelaPrc: TIntegerField;
    QrNFECabACondicaoPg: TIntegerField;
    QrNFECabAICMSRec_pRedBC: TFloatField;
    QrNFECabAICMSRec_vBC: TFloatField;
    QrNFECabAICMSRec_pAliq: TFloatField;
    QrNFECabAICMSRec_vICMS: TFloatField;
    QrNFECabAIPIRec_pRedBC: TFloatField;
    QrNFECabAIPIRec_vBC: TFloatField;
    QrNFECabAIPIRec_pAliq: TFloatField;
    QrNFECabAIPIRec_vIPI: TFloatField;
    QrNFECabAPISRec_pRedBC: TFloatField;
    QrNFECabAPISRec_vBC: TFloatField;
    QrNFECabAPISRec_pAliq: TFloatField;
    QrNFECabAPISRec_vPIS: TFloatField;
    QrNFECabACOFINSRec_pRedBC: TFloatField;
    QrNFECabACOFINSRec_vBC: TFloatField;
    QrNFECabACOFINSRec_pAliq: TFloatField;
    QrNFECabACOFINSRec_vCOFINS: TFloatField;
    QrNFECabADataFiscal: TDateField;
    QrNFECabAprotNFe_versao: TFloatField;
    QrNFECabAretCancNFe_versao: TFloatField;
    QrNFECabASINTEGRA_ExpDeclNum: TWideStringField;
    QrNFECabASINTEGRA_ExpDeclDta: TDateField;
    QrNFECabASINTEGRA_ExpNat: TWideStringField;
    QrNFECabASINTEGRA_ExpRegNum: TWideStringField;
    QrNFECabASINTEGRA_ExpRegDta: TDateField;
    QrNFECabASINTEGRA_ExpConhNum: TWideStringField;
    QrNFECabASINTEGRA_ExpConhDta: TDateField;
    QrNFECabASINTEGRA_ExpConhTip: TWideStringField;
    QrNFECabASINTEGRA_ExpPais: TWideStringField;
    QrNFECabASINTEGRA_ExpAverDta: TDateField;
    QrNFECabACodInfoEmit: TIntegerField;
    QrNFECabACriAForca: TSmallintField;
    QrNFECabAide_hSaiEnt: TTimeField;
    QrNFECabAide_dhCont: TDateTimeField;
    QrNFECabAide_xJust: TWideStringField;
    QrNFECabAemit_CRT: TSmallintField;
    QrNFECabAdest_email: TWideStringField;
    QrNFECabBrefNFP_cUF: TSmallintField;
    QrNFECabBrefNFP_AAMM: TSmallintField;
    QrNFECabBrefNFP_CNPJ: TWideStringField;
    QrNFECabBrefNFP_CPF: TWideStringField;
    QrNFECabBrefNFP_IE: TWideStringField;
    QrNFECabBrefNFP_mod: TSmallintField;
    QrNFECabBrefNFP_serie: TSmallintField;
    QrNFECabBrefNFP_nNF: TIntegerField;
    QrNFECabBrefCTe: TWideStringField;
    QrNFECabBrefECF_mod: TWideStringField;
    QrNFECabBrefECF_nECF: TSmallintField;
    QrNFECabBrefECF_nCOO: TIntegerField;
    QrNFECabFretirada_CPF: TWideStringField;
    QrNFECabGentrega_CPF: TWideStringField;
    QrNFEItsNICMS_vBCSTRet: TFloatField;
    QrNFEItsNICMS_vICMSSTRet: TFloatField;
    QrNFEItsNICMS_CSOSN: TIntegerField;
    QrNFEItsNICMS_UFST: TWideStringField;
    QrNFEItsNICMS_pBCOp: TFloatField;
    QrNFEItsNICMS_motDesICMS: TSmallintField;
    QrNFEItsNICMS_pCredSN: TFloatField;
    QrNFEItsNICMS_vCredICMSSN: TFloatField;
    QrNFEItsUISSQN_cSitTrib: TWideStringField;
    QrNFECabAVagao: TWideStringField;
    QrNFECabABalsa: TWideStringField;
    QrNFECabAInfAdic_InfAdFisco: TWideMemoField;
    QrProds1prod_indTot: TSmallintField;
    QrNfeItsIDI: TmySQLQuery;
    QrNfeItsIDIFatID: TIntegerField;
    QrNfeItsIDIFatNum: TIntegerField;
    QrNfeItsIDIEmpresa: TIntegerField;
    QrNfeItsIDInItem: TIntegerField;
    QrNfeItsIDIDI_nDI: TWideStringField;
    QrNfeItsIDIDI_dDI: TDateField;
    QrNfeItsIDIDI_xLocDesemb: TWideStringField;
    QrNfeItsIDIDI_UFDesemb: TWideStringField;
    QrNfeItsIDIDI_dDesemb: TDateField;
    QrNfeItsIDIDI_cExportador: TWideStringField;
    QrNfeItsIDIControle: TIntegerField;
    QrNfeItsIDIa: TmySQLQuery;
    QrNfeItsIDIaAdi_nAdicao: TIntegerField;
    QrNfeItsIDIaAdi_nSeqAdic: TIntegerField;
    QrNfeItsIDIaAdi_cFabricante: TWideStringField;
    QrNfeItsIDIaAdi_vDescDI: TFloatField;
    QrNfeItsIDIaConta: TIntegerField;
    QrProds1pCredSN: TFloatField;
    QrCNAE: TmySQLQuery;
    DsCNAE: TDataSource;
    QrNFEItsR: TmySQLQuery;
    QrNFEItsT: TmySQLQuery;
    QrNFEItsRFatID: TIntegerField;
    QrNFEItsRFatNum: TIntegerField;
    QrNFEItsREmpresa: TIntegerField;
    QrNFEItsRnItem: TIntegerField;
    QrNFEItsRPISST_vBC: TFloatField;
    QrNFEItsRPISST_pPIS: TFloatField;
    QrNFEItsRPISST_qBCProd: TFloatField;
    QrNFEItsRPISST_vAliqProd: TFloatField;
    QrNFEItsRPISST_vPIS: TFloatField;
    QrNFEItsRPISST_fatorBCST: TFloatField;
    QrNFEItsTFatID: TIntegerField;
    QrNFEItsTFatNum: TIntegerField;
    QrNFEItsTEmpresa: TIntegerField;
    QrNFEItsTnItem: TIntegerField;
    QrNFEItsTCOFINSST_vBC: TFloatField;
    QrNFEItsTCOFINSST_pCOFINS: TFloatField;
    QrNFEItsTCOFINSST_qBCProd: TFloatField;
    QrNFEItsTCOFINSST_vAliqProd: TFloatField;
    QrNFEItsTCOFINSST_vCOFINS: TFloatField;
    QrNFEItsTCOFINSST_fatorBCST: TFloatField;
    QrAliqpRedBC: TFloatField;
    QrAliqmodBC: TSmallintField;
    QrExce: TmySQLQuery;
    QrExceNivel: TSmallintField;
    QrExceICMSAliq: TFloatField;
    QrExceCST_B: TWideStringField;
    QrExcepRedBC: TFloatField;
    QrExcemodBC: TSmallintField;
    QrFilialNFetpEmis: TSmallintField;
    QrFilialSCAN_Ser: TIntegerField;
    QrFilialSCAN_nNF: TIntegerField;
    QrNFeLayIFormatStr: TWideStringField;
    QrFilialDirNFeRWeb: TWideStringField;
    QrFisRegCadII_Usa: TSmallintField;
    QrNFeTotIprod_vOutro: TFloatField;
    QrEveTp: TmySQLQuery;
    QrEveTpnSeqEvento: TSmallintField;
    QrFilialDirEveEnvLot: TWideStringField;
    QrFilialDirEveRetLot: TWideStringField;
    QrFilialDirEvePedCCe: TWideStringField;
    QrFilialDirEvePedCan: TWideStringField;
    QrFilialDirEveRetCan: TWideStringField;
    QrFilialDirEveRetCCe: TWideStringField;
    QrNFeEveRCab: TmySQLQuery;
    QrNFeEveRCabFatID: TIntegerField;
    QrNFeEveRCabFatNum: TIntegerField;
    QrNFeEveRCabEmpresa: TIntegerField;
    QrNFeEveRCabControle: TIntegerField;
    QrNFeEveRCabtpAmb: TSmallintField;
    QrNFeEveRCabchNFe: TWideStringField;
    QrNFeEveRCabdhEvento: TDateTimeField;
    QrNFeEveRCabtpEvento: TIntegerField;
    QrNFeEveRCabdescEvento: TWideStringField;
    QrNFeEveRCabchNFe_NF_SER: TWideStringField;
    QrNFeEveRCabchNFe_NF_NUM: TWideStringField;
    QrNFeEveRCabret_cStat: TIntegerField;
    QrNFeEveRCabStatus: TIntegerField;
    QrNFeEveRCabId: TWideStringField;
    QrEveIts: TmySQLQuery;
    QrEveItsControle: TIntegerField;
    QrEveSub: TmySQLQuery;
    QrEveSubSubCtrl: TIntegerField;
    QrFilialDirEveProcCCe: TWideStringField;
    QrOpcoesNFeEntiTipCt1: TIntegerField;
    QrOpcoesNFeNO_ETC_0: TWideStringField;
    QrOpcoesNFeNO_ETC_1: TWideStringField;
    QrFilialPreMailEveCCe: TIntegerField;
    QrEmpresaNumero: TFloatField;
    QrEmpresaCEP: TFloatField;
    QrEmpresaCodiPais: TFloatField;
    QrEmpresaCodMunici: TFloatField;
    QrEmpresaUF: TFloatField;
    QrDestNumero: TFloatField;
    QrDestCEP: TFloatField;
    QrDestCodiPais: TFloatField;
    QrDestCodMunici: TFloatField;
    QrEmitUF: TFloatField;
    QrEmitNumero: TFloatField;
    QrEmitCEP: TFloatField;
    QrEmitCodiPais: TFloatField;
    QrEmitCodMunici: TFloatField;
    QrDestUF: TFloatField;
    QrTotal1Volumes: TFloatField;
    QrTransportaNumero: TFloatField;
    QrTransportaCEP: TFloatField;
    QrTransportaUF: TFloatField;
    QrTransportaCodMunici: TFloatField;
    DsCNAE21Cad: TDataSource;
    QrCNAE21cad: TmySQLQuery;
    QrLocCabY: TmySQLQuery;
    QrLocCabYControle: TIntegerField;
    QrFilialDirRetNfeDes: TWideStringField;
    QrCNAENome: TWideStringField;
    QrFilialDirEvePedMDe: TWideStringField;
    QrFilialDirEveRetMDe: TWideStringField;
    QrOpcoesNFeUF_MDeMDe: TWideStringField;
    QrOpcoesNFeUF_MDeDes: TWideStringField;
    QrOpcoesNFeUF_MDeNFe: TWideStringField;
    QrFilialDirDowNFeDes: TWideStringField;
    QrFilialDirDowNFeNFe: TWideStringField;
    QrFilialNFeNT2013_003LTT: TSmallintField;
    QrProds1iTotTrib: TSmallintField;
    QrProds1vTotTrib: TFloatField;
    QrOpcoesNFeNFeNT2013_003LTT: TSmallintField;
    QrNFeItsM: TmySQLQuery;
    QrNFeTotM: TmySQLQuery;
    QrNFeTotMvTotTrib: TFloatField;
    QrNFECabANFeNT2013_003LTT: TSmallintField;
    QrNFECabAvTotTrib: TFloatField;
    QrLocEve: TmySQLQuery;
    QrLocEveITENS: TLargeintField;
    QrCNAECodAlf: TWideStringField;
    QrCNAECodTxt: TWideStringField;
    QrLoc2: TmySQLQuery;
    QrLoc: TmySQLQuery;
    QrNFECabAide_dhEmi: TDateTimeField;
    QrNFECabAide_dhSaiEnt: TDateTimeField;
    QrNFECabAide_idDest: TSmallintField;
    QrNFECabAide_indFinal: TSmallintField;
    QrNFECabAide_indPres: TSmallintField;
    QrNFECabAide_dhContTZD: TFloatField;
    QrNFECabAdest_idEstrangeiro: TWideStringField;
    QrNFECabAdest_indIEDest: TSmallintField;
    QrNFECabAdest_IM: TWideStringField;
    QrNFeCabGA: TmySQLQuery;
    QrNFeCabGAFatID: TIntegerField;
    QrNFeCabGAFatNum: TIntegerField;
    QrNFeCabGAEmpresa: TIntegerField;
    QrNFeCabGAautXML_CNPJ: TWideStringField;
    QrNFeCabGAautXML_CPF: TWideStringField;
    QrNFeCabGALk: TIntegerField;
    QrNFeCabGADataCad: TDateField;
    QrNFeCabGADataAlt: TDateField;
    QrNFeCabGAUserCad: TIntegerField;
    QrNFeCabGAUserAlt: TIntegerField;
    QrNFeCabGAAlterWeb: TSmallintField;
    QrNFeCabGAAtivo: TSmallintField;
    QrNFeCabGAAddForma: TSmallintField;
    QrNFeCabGAAddIDCad: TSmallintField;
    QrNfeItsIDIDI_tpViaTransp: TSmallintField;
    QrNfeItsIDIDI_vAFRMM: TFloatField;
    QrNfeItsIDIDI_tpIntermedio: TSmallintField;
    QrNfeItsIDIDI_CNPJ: TWideStringField;
    QrNfeItsIDIDI_UFTerceiro: TWideStringField;
    QrNfeItsIDIaAdi_nDraw: TFloatField;
    QrNFeItsI03: TmySQLQuery;
    QrNFeItsI03FatID: TIntegerField;
    QrNFeItsI03FatNum: TIntegerField;
    QrNFeItsI03Empresa: TIntegerField;
    QrNFeItsI03nItem: TIntegerField;
    QrNFeItsI03Controle: TIntegerField;
    QrNFeItsI03detExport_nDraw: TLargeintField;
    QrNFeItsI03detExport_nRE: TLargeintField;
    QrNFeItsI03detExport_chNFe: TWideStringField;
    QrNFeItsI03detExport_qExport: TFloatField;
    QrNFeItsI03Lk: TIntegerField;
    QrNFeItsI03DataCad: TDateField;
    QrNFeItsI03DataAlt: TDateField;
    QrNFeItsI03UserCad: TIntegerField;
    QrNFeItsI03UserAlt: TIntegerField;
    QrNFeItsI03AlterWeb: TSmallintField;
    QrNFeItsI03Ativo: TSmallintField;
    QrNFEItsNICMS_vICMSDeson: TFloatField;
    QrNFEItsNICMS_vICMSOp: TFloatField;
    QrNFEItsNICMS_pDif: TFloatField;
    QrNFEItsNICMS_vICMSDif: TFloatField;
    QrNFEItsUISSQN_vDeducao: TFloatField;
    QrNFEItsUISSQN_vOutro: TFloatField;
    QrNFEItsUISSQN_vDescIncond: TFloatField;
    QrNFEItsUISSQN_vDescCond: TFloatField;
    QrNFEItsUISSQN_vISSRet: TFloatField;
    QrNFEItsUISSQN_indISS: TSmallintField;
    QrNFEItsUISSQN_cServico: TWideStringField;
    QrNFEItsUISSQN_cMun: TIntegerField;
    QrNFEItsUISSQN_cPais: TIntegerField;
    QrNFEItsUISSQN_nProcesso: TWideStringField;
    QrNFEItsUISSQN_indIncentivo: TSmallintField;
    QrNFECabAICMSTot_vICMSDeson: TFloatField;
    QrNFEItsUISSQN_cListServ: TWideStringField;
    QrNFeTotNICMS_vICMSDeson: TFloatField;
    QrDestEstrangDef: TSmallintField;
    QrDestEstrangTip: TSmallintField;
    QrDestEstrangNum: TWideStringField;
    QrDestindIEDest: TSmallintField;
    QrNFECabAEstrangDef: TSmallintField;
    QrNFeCabGAControle: TIntegerField;
    QrNFeCabGATipo: TSmallintField;
    QrNFeItsINVE: TmySQLQuery;
    QrNFeItsINVEFatID: TIntegerField;
    QrNFeItsINVEFatNum: TIntegerField;
    QrNFeItsINVEEmpresa: TIntegerField;
    QrNFeItsINVEnItem: TIntegerField;
    QrNFeItsINVEControle: TIntegerField;
    QrNFeItsINVEImportacia: TIntegerField;
    QrNFeItsINVENVE: TWideStringField;
    QrNFeItsINVELk: TIntegerField;
    QrNFeItsINVEDataCad: TDateField;
    QrNFeItsINVEDataAlt: TDateField;
    QrNFeItsINVEUserCad: TIntegerField;
    QrNFeItsINVEUserAlt: TIntegerField;
    QrNFeItsINVEAlterWeb: TSmallintField;
    QrNFeItsINVEAtivo: TSmallintField;
    QrNFECabAide_hEmi: TTimeField;
    QrNFECabAide_dhEmiTZD: TFloatField;
    QrNFECabAide_dhSaiEntTZD: TFloatField;
    QrProds1pTotTrib: TFloatField;
    QrIBPTax: TmySQLQuery;
    QrIBPTaxCodigo: TLargeintField;
    QrIBPTaxEx: TIntegerField;
    QrIBPTaxTabela: TIntegerField;
    QrIBPTaxNome: TWideStringField;
    QrIBPTaxDescricao: TWideMemoField;
    QrIBPTaxAliqNac: TFloatField;
    QrIBPTaxAliqImp: TFloatField;
    QrIBPTaxVersao: TWideStringField;
    QrIBPTaxLk: TIntegerField;
    QrIBPTaxDataCad: TDateField;
    QrIBPTaxDataAlt: TDateField;
    QrIBPTaxUserCad: TIntegerField;
    QrIBPTaxUserAlt: TIntegerField;
    QrIBPTaxAlterWeb: TSmallintField;
    QrIBPTaxAtivo: TSmallintField;
    QrNFeItsMFatID: TIntegerField;
    QrNFeItsMFatNum: TIntegerField;
    QrNFeItsMEmpresa: TIntegerField;
    QrNFeItsMnItem: TIntegerField;
    QrNFeItsMiTotTrib: TSmallintField;
    QrNFeItsMvTotTrib: TFloatField;
    QrNFeItsMLk: TIntegerField;
    QrNFeItsMDataCad: TDateField;
    QrNFeItsMDataAlt: TDateField;
    QrNFeItsMUserCad: TIntegerField;
    QrNFeItsMUserAlt: TIntegerField;
    QrNFeItsMAlterWeb: TSmallintField;
    QrNFeItsMAtivo: TSmallintField;
    QrNFeItsMpTotTrib: TFloatField;
    QrNFeItsMtabTotTrib: TSmallintField;
    QrNFeItsMverTotTrib: TWideStringField;
    QrNFeItsMtpAliqTotTrib: TSmallintField;
    QrNFeTotMvBasTrib: TFloatField;
    QrProds1ICMS_pDif: TFloatField;
    QrProds1ICMS_pICMSDeson: TFloatField;
    QrProds1ICMS_motDesICMS: TSmallintField;
    QrNFeItsMfontTotTrib: TSmallintField;
    QrNFECabAInfCpl_totTrib: TWideStringField;
    QrOpcoesNFeUF_DistDFeInt: TWideStringField;
    QrFilialDirDistDFeInt: TWideStringField;
    QrFilialDirRetDistDFeInt: TWideStringField;
    QrNFeDFeINFe: TmySQLQuery;
    QrNFeDFeINFeCodigo: TIntegerField;
    QrNFeDFeINFeControle: TLargeintField;
    QrNFeDFeINFeConta: TLargeintField;
    QrNFeDFeINFeversao: TFloatField;
    QrNFeDFeINFetpAmb: TSmallintField;
    QrNFeDFeINFechNFe: TWideStringField;
    QrNFeDFeINFeCNPJ: TWideStringField;
    QrNFeDFeINFeCPF: TWideStringField;
    QrNFeDFeINFeIE: TWideStringField;
    QrNFeDFeINFedhEmi: TDateTimeField;
    QrNFeDFeINFedhEmiTZD: TFloatField;
    QrNFeDFeINFetpNF: TSmallintField;
    QrNFeDFeINFevNF: TFloatField;
    QrNFeDFeINFedigVal: TWideStringField;
    QrNFeDFeINFedhRecbto: TDateTimeField;
    QrNFeDFeINFedhRecbtoTZD: TFloatField;
    QrNFeDFeINFenProt: TWideStringField;
    QrNFeDFeINFecSitNFe: TSmallintField;
    QrNFeDFeINFeLk: TIntegerField;
    QrNFeDFeINFeDataCad: TDateField;
    QrNFeDFeINFeDataAlt: TDateField;
    QrNFeDFeINFeUserCad: TIntegerField;
    QrNFeDFeINFeUserAlt: TIntegerField;
    QrNFeDFeINFeAlterWeb: TSmallintField;
    QrNFeDFeINFeAtivo: TSmallintField;
    QrNFeDFeINFexNome: TWideStringField;
    QrNFeDFeINFeNFa_IDCtrl: TIntegerField;
    QrNFeDFeINFeEmpresa: TIntegerField;
    QrDup: TmySQLQuery;
    QrFilialDirDowNFeCnf: TWideStringField;
    QrAux: TmySQLQuery;
    QrIBPTaxFonteStr: TWideStringField;
    QrIBPTaxChave: TWideStringField;
    QrIBPTaxVigenIni: TDateField;
    QrIBPTaxVigenFim: TDateField;
    QrNFEItsI: TmySQLQuery;
    QrNFEItsIFatID: TIntegerField;
    QrNFEItsIFatNum: TIntegerField;
    QrNFEItsIEmpresa: TIntegerField;
    QrNFEItsInItem: TIntegerField;
    QrNFEItsIprod_cProd: TWideStringField;
    QrNFEItsIprod_cEAN: TWideStringField;
    QrNFEItsIprod_xProd: TWideStringField;
    QrNFEItsIprod_NCM: TWideStringField;
    QrNFEItsIprod_EXTIPI: TWideStringField;
    QrNFEItsIprod_genero: TSmallintField;
    QrNFEItsIprod_CFOP: TIntegerField;
    QrNFEItsIprod_uCom: TWideStringField;
    QrNFEItsIprod_qCom: TFloatField;
    QrNFEItsIprod_vUnCom: TFloatField;
    QrNFEItsIprod_vProd: TFloatField;
    QrNFEItsIprod_cEANTrib: TWideStringField;
    QrNFEItsIprod_uTrib: TWideStringField;
    QrNFEItsIprod_qTrib: TFloatField;
    QrNFEItsIprod_vUnTrib: TFloatField;
    QrNFEItsIprod_vFrete: TFloatField;
    QrNFEItsIprod_vSeg: TFloatField;
    QrNFEItsIprod_vDesc: TFloatField;
    QrNFEItsI_Ativo_: TSmallintField;
    QrNFEItsILk: TIntegerField;
    QrNFEItsIDataCad: TDateField;
    QrNFEItsIDataAlt: TDateField;
    QrNFEItsIUserCad: TIntegerField;
    QrNFEItsIUserAlt: TIntegerField;
    QrNFEItsIAlterWeb: TSmallintField;
    QrNFEItsIAtivo: TSmallintField;
    QrNFEItsITem_IPI: TSmallintField;
    QrNFEItsIInfAdCuztm: TIntegerField;
    QrNFEItsIprod_vOutro: TFloatField;
    QrNFEItsIprod_indTot: TSmallintField;
    QrNFEItsITem_II: TSmallintField;
    QrNFEItsIprod_xPed: TWideStringField;
    QrNFEItsIprod_nItemPed: TIntegerField;
    QrNFEItsIprod_nFCI: TWideStringField;
    QrNFEItsIEX_TIPI: TWideStringField;
    QrProds1prod_CEST: TIntegerField;
    QrNFEItsIprod_CEST: TIntegerField;
    QrAliqpBCUFDest: TFloatField;
    QrAliqpFCPUFDest: TFloatField;
    QrAliqpICMSUFDest: TFloatField;
    QrAliqpICMSInter: TFloatField;
    QrAliqpICMSInterPart: TFloatField;
    QrAliqUsaInterPartLei: TSmallintField;
    QrNFeItsNA: TmySQLQuery;
    QrNFeTotNA: TmySQLQuery;
    QrNFeTotNAICMSTot_vFCPUFDest: TFloatField;
    QrNFeTotNAICMSTot_vICMSUFDest: TFloatField;
    QrNFeTotNAICMSTot_vICMSUFRemet: TFloatField;
    QrNFeItsNAFatID: TIntegerField;
    QrNFeItsNAFatNum: TIntegerField;
    QrNFeItsNAEmpresa: TIntegerField;
    QrNFeItsNAnItem: TIntegerField;
    QrNFeItsNAICMSUFDest_vBCUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pFCPUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSInter: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSInterPart: TFloatField;
    QrNFeItsNAICMSUFDest_vFCPUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_vICMSUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_vICMSUFRemet: TFloatField;
    QrNFECabAICMSTot_vFCPUFDest: TFloatField;
    QrNFECabAICMSTot_vICMSUFDest: TFloatField;
    QrNFECabAICMSTot_vICMSUFRemet: TFloatField;
    QrDTB_UFsDTB: TWideStringField;
    QrDTB_UFsNome: TWideStringField;
    QrNFECabBQualNFref: TSmallintField;
    QrNFeCabZCon: TmySQLQuery;
    QrNFeCabZConFatID: TIntegerField;
    QrNFeCabZConFatNum: TIntegerField;
    QrNFeCabZConEmpresa: TIntegerField;
    QrNFeCabZConControle: TIntegerField;
    QrNFeCabZConxCampo: TWideStringField;
    QrNFeCabZConxTexto: TWideStringField;
    QrNFeCabZFis: TmySQLQuery;
    QrNFeCabZFisFatID: TIntegerField;
    QrNFeCabZFisFatNum: TIntegerField;
    QrNFeCabZFisEmpresa: TIntegerField;
    QrNFeCabZFisControle: TIntegerField;
    QrNFeCabZFisxCampo: TWideStringField;
    QrNFeCabZFisxTexto: TWideStringField;
    QrNFeCabZPro: TmySQLQuery;
    QrNFeCabZProFatID: TIntegerField;
    QrNFeCabZProFatNum: TIntegerField;
    QrNFeCabZProEmpresa: TIntegerField;
    QrNFeCabZProControle: TIntegerField;
    QrNFeCabZPronProc: TWideStringField;
    QrNFeCabZProindProc: TSmallintField;
    QrNFECabAExporta_XLocDespacho: TWideStringField;
    QrProds1ID: TIntegerField;
    QrProds1IDCtrl: TIntegerField;
    QrNFECabAICMSTot_vFCP: TFloatField;
    QrNFECabAICMSTot_vFCPST: TFloatField;
    QrNFECabAICMSTot_vFCPSTRet: TFloatField;
    QrNFECabAICMSTot_vIPIDevol: TFloatField;
    QrNFEItsIprod_indEscala: TWideStringField;
    QrNFEItsIprod_CNPJFab: TWideStringField;
    QrNFEItsIprod_cBenef: TWideStringField;
    QrNFEItsNICMS_vBCFCP: TFloatField;
    QrNFEItsNICMS_pFCP: TFloatField;
    QrNFEItsNICMS_vFCP: TFloatField;
    QrNFEItsNICMS_vBCFCPST: TFloatField;
    QrNFEItsNICMS_pFCPST: TFloatField;
    QrNFEItsNICMS_vFCPST: TFloatField;
    QrNFEItsNICMS_pST: TFloatField;
    QrNFEItsNICMS_vBCFCPSTRet: TFloatField;
    QrNFEItsNICMS_pFCPSTRet: TFloatField;
    QrNFEItsNICMS_vFCPSTRet: TFloatField;
    QrNFeItsNAICMSUFDest_vBCFCPUFDest: TFloatField;
    QrProds1prod_indEscala: TWideStringField;
    QrNFeTotNICMS_vFCP: TFloatField;
    QrNFECabYA: TmySQLQuery;
    QrNFECabYAFatID: TIntegerField;
    QrNFECabYAFatNum: TIntegerField;
    QrNFECabYAEmpresa: TIntegerField;
    QrNFECabYAControle: TIntegerField;
    QrNFECabYAtPag: TSmallintField;
    QrNFECabYAvPag: TFloatField;
    QrNFECabYAtpIntegra: TSmallintField;
    QrNFECabYACNPJ: TWideStringField;
    QrNFECabYAtBand: TSmallintField;
    QrNFECabYAcAut: TWideStringField;
    QrNFECabYAvTroco: TFloatField;
    QrNFECabYFatParcela: TIntegerField;
    QrRetiradaEnti: TmySQLQuery;
    QrRetiradaEntiCodigo: TIntegerField;
    QrRetiradaEntiCadastro: TDateField;
    QrRetiradaEntiENatal: TDateField;
    QrRetiradaEntiPNatal: TDateField;
    QrRetiradaEntiTipo: TSmallintField;
    QrRetiradaEntiRespons1: TWideStringField;
    QrRetiradaEntiRespons2: TWideStringField;
    QrRetiradaEntiENumero: TIntegerField;
    QrRetiradaEntiPNumero: TIntegerField;
    QrRetiradaEntiELograd: TSmallintField;
    QrRetiradaEntiPLograd: TSmallintField;
    QrRetiradaEntiECEP: TIntegerField;
    QrRetiradaEntiPCEP: TIntegerField;
    QrRetiradaEntiNOME_ENT: TWideStringField;
    QrRetiradaEntiNO_2_ENT: TWideStringField;
    QrRetiradaEntiCNPJ_CPF: TWideStringField;
    QrRetiradaEntiIE_RG: TWideStringField;
    QrRetiradaEntiNIRE_: TWideStringField;
    QrRetiradaEntiRUA: TWideStringField;
    QrRetiradaEntiSITE: TWideStringField;
    QrRetiradaEntiCOMPL: TWideStringField;
    QrRetiradaEntiBAIRRO: TWideStringField;
    QrRetiradaEntiCIDADE: TWideStringField;
    QrRetiradaEntiNOMELOGRAD: TWideStringField;
    QrRetiradaEntiNOMEUF: TWideStringField;
    QrRetiradaEntiPais: TWideStringField;
    QrRetiradaEntiLograd: TSmallintField;
    QrRetiradaEntiTE1: TWideStringField;
    QrRetiradaEntiFAX: TWideStringField;
    QrRetiradaEntiEMAIL: TWideStringField;
    QrRetiradaEntiTRATO: TWideStringField;
    QrRetiradaEntiENDEREF: TWideStringField;
    QrRetiradaEntiCODMUNICI: TFloatField;
    QrRetiradaEntiCODPAIS: TFloatField;
    QrRetiradaEntiRG: TWideStringField;
    QrRetiradaEntiSSP: TWideStringField;
    QrRetiradaEntiDataRG: TDateField;
    QrRetiradaEntiIE: TWideStringField;
    QrEntregaEnti: TmySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    QrEntiEntrega: TmySQLQuery;
    QrEntiEntregaL_CNPJ: TWideStringField;
    QrEntiEntregaL_CPF: TWideStringField;
    QrEntiEntregaL_Nome: TWideStringField;
    QrEntiEntregaLLograd: TSmallintField;
    QrEntiEntregaLRua: TWideStringField;
    QrEntiEntregaLNumero: TIntegerField;
    QrEntiEntregaLCompl: TWideStringField;
    QrEntiEntregaLBairro: TWideStringField;
    QrEntiEntregaLCodMunici: TIntegerField;
    QrEntiEntregaxMunicipio: TWideStringField;
    QrEntiEntregaLUF: TSmallintField;
    QrEntiEntregaLCEP: TIntegerField;
    QrEntiEntregaLCodiPais: TIntegerField;
    QrEntiEntregaxPais: TWideStringField;
    QrEntiEntregaLTel: TWideStringField;
    QrEntiEntregaLEmail: TWideStringField;
    QrEntiEntregaL_IE: TWideStringField;
    QrEntiEntregaL_Ativo: TSmallintField;
    QrEntiEntregaNO_LLograd: TWideStringField;
    QrEntiEntregaNO_LUF: TWideStringField;
    QrNFECabFretirada_xNome: TWideStringField;
    QrNFECabFretirada_CEP: TIntegerField;
    QrNFECabFretirada_cPais: TIntegerField;
    QrNFECabFretirada_xPais: TWideStringField;
    QrNFECabFretirada_fone: TWideStringField;
    QrNFECabFretirada_email: TWideStringField;
    QrNFECabFretirada_IE: TWideStringField;
    QrNFECabGentrega_xNome: TWideStringField;
    QrNFECabGentrega_CEP: TIntegerField;
    QrNFECabGentrega_cPais: TIntegerField;
    QrNFECabGentrega_xPais: TWideStringField;
    QrNFECabGentrega_fone: TWideStringField;
    QrNFECabGentrega_email: TWideStringField;
    QrNFECabGentrega_IE: TWideStringField;
    QrEntregaEntiNUMERO: TFloatField;
    QrEntregaEntiCEP: TFloatField;
    QrRetiradaEntiNUMERO: TFloatField;
    QrRetiradaEntiCEP: TFloatField;
    QrExcecBenef: TWideStringField;
    QrAliqcBenef: TWideStringField;
    QrNFeItsLA: TMySQLQuery;
    QrNFeItsLAFatID: TIntegerField;
    QrNFeItsLAFatNum: TIntegerField;
    QrNFeItsLAEmpresa: TIntegerField;
    QrNFeItsLAnItem: TIntegerField;
    QrNFeItsLADetComb_cProdANP: TIntegerField;
    QrNFeItsLADetComb_descANP: TWideStringField;
    QrNFeItsLADetComb_pGLP: TFloatField;
    QrNFeItsLADetComb_pGNn: TFloatField;
    QrNFeItsLADetComb_pGNi: TFloatField;
    QrNFeItsLADetComb_vPart: TFloatField;
    QrNFeItsLADetComb_CODIF: TWideStringField;
    QrNFeItsLADetComb_qTemp: TFloatField;
    QrNFeItsLADetComb_UFCons: TWideStringField;
    QrNFeItsLADetComb_CIDE: TSmallintField;
    QrNFeItsLADetComb_CIDE_qBCProd: TFloatField;
    QrNFeItsLADetComb_CIDE_vAliqProd: TFloatField;
    QrNFeItsLADetComb_CIDE_vCIDE: TFloatField;
    QrNFeItsLADetComb_encerrante: TSmallintField;
    QrNFeItsLADetComb_encerrante_nBico: TIntegerField;
    QrNFeItsLADetComb_encerrante_nBomba: TIntegerField;
    QrNFeItsLADetComb_encerrante_nTanque: TIntegerField;
    QrNFeItsLADetComb_encerrante_vEncIni: TFloatField;
    QrNFeItsLADetComb_encerrante_vEncFin: TFloatField;
    QrNFeItsLALk: TIntegerField;
    QrNFeItsLADataCad: TDateField;
    QrNFeItsLADataAlt: TDateField;
    QrNFeItsLAUserCad: TIntegerField;
    QrNFeItsLAUserAlt: TIntegerField;
    QrNFeItsLAAlterWeb: TSmallintField;
    QrNFeItsLAAWServerID: TIntegerField;
    QrNFeItsLAAWStatSinc: TSmallintField;
    QrNFeItsLAAtivo: TSmallintField;
    QrProds1prod_vFrete: TFloatField;
    QrProds1prod_vSeg: TFloatField;
    QrProds1prod_vDesc: TFloatField;
    QrProds1prod_vOutro: TFloatField;
    QrQRCode_WS: TMySQLQuery;
    QrNFECabAURL_QrCode: TWideStringField;
    QrNFECabAURL_Consulta: TWideStringField;
    QrIBPTaxEstadual: TFloatField;
    QrIBPTaxMunicipal: TFloatField;
    QrNFeTotMvTribFed: TFloatField;
    QrNFeTotMvTribEst: TFloatField;
    QrNFeTotMvTribMun: TFloatField;
    QrNFECabACNPJCPFAvulso: TWideStringField;
    QrNFECabARazaoNomeAvulso: TWideStringField;
    QrExcepDif: TFloatField;
    QrAliqpDif: TFloatField;
    QrLocCEST: TMySQLQuery;
    QrLocCESTCEST: TWideStringField;
    QrLocCESTNCM: TWideStringField;
    QrLocCESTNome: TWideStringField;
    QrSMVA: TMySQLQuery;
    QrSMVAMadeBy: TSmallintField;
    QrSMVAUsaSubsTrib: TSmallintField;
    QrSMVAID: TIntegerField;
    QrProds1CSOSN_Manual: TIntegerField;
    QrProds1CSOSN_GG1: TIntegerField;
    QrAliqCSOSN: TWideStringField;
    QrExceCSOSN: TWideStringField;
    QrProds1UsaSubsTrib: TSmallintField;
    QrNFEItsNICMS_vICMSSubstituto: TFloatField;
    QrNFEItsNICMS_pRedBCEfet: TFloatField;
    QrNFEItsNICMS_vBCEfet: TFloatField;
    QrNFEItsNICMS_pICMSEfet: TFloatField;
    QrNFEItsNICMS_vICMSEfet: TFloatField;
    QrNFECabAide_indIntermed: TSmallintField;
    QrNFEItsUA: TMySQLQuery;
    QrNFEItsUAFatID: TIntegerField;
    QrNFEItsUAFatNum: TIntegerField;
    QrNFEItsUAEmpresa: TIntegerField;
    QrNFEItsUAnItem: TIntegerField;
    QrNFEItsUAimpostoDevol_pDevol: TFloatField;
    QrNFEItsUAimpostoDevol_vIPIDevol: TFloatField;
    QrNFECabAInfIntermed_CNPJ: TWideStringField;
    QrNFECabAInfIntermed_idCadIntTran: TWideStringField;
    QrNFEItsNICMS_vICMSSTDeson: TFloatField;
    QrNFEItsNICMS_pFCPDif: TFloatField;
    QrNFEItsNICMS_vFCPDif: TFloatField;
    QrNFEItsNICMS_vFCPEfet: TFloatField;
    QrNFEItsNICMS_motDesICMSST: TSmallintField;
    QrProds1prod_cBarraGG1: TWideStringField;
    QrProds1prod_cBarraGGX: TWideStringField;
    QrNFeLayIDtIniProd: TDateField;
    QrNFeLayIDtIniHomo: TDateField;
    QrNFEItsIprod_cBarra: TWideStringField;
    QrNFEItsIprod_cBarraTrib: TWideStringField;
    QrInfIntermedEnti: TMySQLQuery;
    QrInfIntermedEntiCodigo: TIntegerField;
    QrInfIntermedEntiTipo: TSmallintField;
    QrInfIntermedEntiCNPJ: TWideStringField;
    QrInfIntermedEntiCPF: TWideStringField;
    QrInfIntermedEntiIE: TWideStringField;
    QrInfIntermedEntiRG: TWideStringField;
    QrInfIntermedEntiCNAE: TWideStringField;
    QrInfIntermedEntiNO_ENT: TWideStringField;
    QrInfIntermedEntiFANTASIA: TWideStringField;
    QrInfIntermedEntiRUA: TWideStringField;
    QrInfIntermedEntiCOMPL: TWideStringField;
    QrInfIntermedEntiBAIRRO: TWideStringField;
    QrInfIntermedEntiTe1: TWideStringField;
    QrInfIntermedEntiNO_LOGRAD: TWideStringField;
    QrInfIntermedEntiNO_Munici: TWideStringField;
    QrInfIntermedEntiNO_UF: TWideStringField;
    QrInfIntermedEntiENDERECO: TWideStringField;
    QrInfIntermedEntiNIRE: TWideStringField;
    QrInfIntermedEntiNumero: TFloatField;
    QrInfIntermedEntiCEP: TFloatField;
    QrInfIntermedEntiUF: TFloatField;
    QrInfIntermedEntiCodMunici: TFloatField;
    QrNFECabAEmiteAvulso: TSmallintField;
    QrGraGXVend: TMySQLQuery;
    QrGraGXVendNO_MARCA: TWideStringField;
    QrGraGXVendReferencia: TWideStringField;
    QrNFEItsRPISST_indSomaPISST: TSmallintField;
    QrNFEItsTCOFINSST_indSomaCOFINSST: TSmallintField;
    QrStqMovNFsA: TMySQLQuery;
    QrStqMovNFsAIDCtrl: TIntegerField;
    QrStqMovNFsATipo: TSmallintField;
    QrStqMovNFsAOriCodi: TIntegerField;
    QrStqMovNFsAEmpresa: TIntegerField;
    QrStqMovNFsASerieNFCod: TIntegerField;
    QrStqMovNFsASerieNFTxt: TWideStringField;
    QrStqMovNFsANumeroNF: TIntegerField;
    QrStqMovNFsAIncSeqAuto: TSmallintField;
    QrStqMovNFsAFreteVal: TFloatField;
    QrStqMovNFsASeguro: TFloatField;
    QrStqMovNFsAOutros: TFloatField;
    QrStqMovNFsAPlacaUF: TWideStringField;
    QrStqMovNFsAPlacaNr: TWideStringField;
    QrStqMovNFsARNTC: TWideStringField;
    QrStqMovNFsAQuantidade: TWideStringField;
    QrStqMovNFsAEspecie: TWideStringField;
    QrStqMovNFsAMarca: TWideStringField;
    QrStqMovNFsANumero: TWideStringField;
    QrStqMovNFsAkgBruto: TFloatField;
    QrStqMovNFsAkgLiqui: TFloatField;
    QrStqMovNFsAObservacao: TWideStringField;
    QrStqMovNFsACFOP1: TWideStringField;
    QrStqMovNFsADtEmissNF: TDateField;
    QrStqMovNFsADtEntraSai: TDateField;
    QrStqMovNFsAHrEntraSai: TTimeField;
    QrStqMovNFsAinfAdic_infCpl: TWideMemoField;
    QrStqMovNFsAUFembarq: TWideStringField;
    QrStqMovNFsAxLocEmbarq: TWideStringField;
    QrStqMovNFsAide_dhCont: TDateTimeField;
    QrStqMovNFsAide_xJust: TWideStringField;
    QrStqMovNFsAemit_CRT: TSmallintField;
    QrStqMovNFsAdest_email: TWideStringField;
    QrStqMovNFsAvagao: TWideStringField;
    QrStqMovNFsAbalsa: TWideStringField;
    QrStqMovNFsACompra_XNEmp: TWideStringField;
    QrStqMovNFsACompra_XPed: TWideStringField;
    QrStqMovNFsACompra_XCont: TWideStringField;
    QrStqMovNFsAdhEmiTZD: TFloatField;
    QrStqMovNFsAdhSaiEntTZD: TFloatField;
    QrStqMovNFsAHrEmi: TTimeField;
    QrStqMovNFsRef: TMySQLQuery;
    QrStqMovNFsRefFatID: TIntegerField;
    QrStqMovNFsRefFatNum: TIntegerField;
    QrStqMovNFsRefEmpresa: TIntegerField;
    QrStqMovNFsRefControle: TIntegerField;
    QrStqMovNFsRefrefNFe: TWideStringField;
    QrStqMovNFsRefrefNF_cUF: TSmallintField;
    QrStqMovNFsRefrefNF_AAMM: TIntegerField;
    QrStqMovNFsRefrefNF_CNPJ: TWideStringField;
    QrStqMovNFsRefrefNF_mod: TSmallintField;
    QrStqMovNFsRefrefNF_serie: TIntegerField;
    QrStqMovNFsRefrefNF_nNF: TIntegerField;
    QrStqMovNFsRefLk: TIntegerField;
    QrStqMovNFsRefDataCad: TDateField;
    QrStqMovNFsRefDataAlt: TDateField;
    QrStqMovNFsRefUserCad: TIntegerField;
    QrStqMovNFsRefUserAlt: TIntegerField;
    QrStqMovNFsRefAlterWeb: TSmallintField;
    QrStqMovNFsRefAtivo: TSmallintField;
    QrStqMovNFsRefrefNFP_cUF: TSmallintField;
    QrStqMovNFsRefrefNFP_AAMM: TSmallintField;
    QrStqMovNFsRefrefNFP_CNPJ: TWideStringField;
    QrStqMovNFsRefrefNFP_CPF: TWideStringField;
    QrStqMovNFsRefrefNFP_IE: TWideStringField;
    QrStqMovNFsRefrefNFP_mod: TSmallintField;
    QrStqMovNFsRefrefNFP_serie: TSmallintField;
    QrStqMovNFsRefrefNFP_nNF: TIntegerField;
    QrStqMovNFsRefrefCTe: TWideStringField;
    QrStqMovNFsRefrefECF_mod: TWideStringField;
    QrStqMovNFsRefrefECF_nECF: TSmallintField;
    QrStqMovNFsRefrefECF_nCOO: TIntegerField;
    QrStqMovNFsRefQualNFref: TSmallintField;
    DsStqMovNFsRef: TDataSource;
    QrNFeEveRCab1: TMySQLQuery;
    MySQLQuery1: TMySQLQuery;
    SmallintField11: TSmallintField;
    WideStringField5: TWideStringField;
    WideStringField6: TWideStringField;
    SmallintField12: TSmallintField;
    FloatField20: TFloatField;
    IntegerField17: TIntegerField;
    IntegerField18: TIntegerField;
    IntegerField19: TIntegerField;
    IntegerField20: TIntegerField;
    SmallintField13: TSmallintField;
    WideStringField7: TWideStringField;
    DateTimeField2: TDateTimeField;
    IntegerField21: TIntegerField;
    WideStringField8: TWideStringField;
    WideStringField9: TWideStringField;
    WideStringField10: TWideStringField;
    IntegerField22: TIntegerField;
    IntegerField23: TIntegerField;
    WideStringField11: TWideStringField;
    DateTimeField3: TDateTimeField;
    WideMemoField1: TWideMemoField;
    WideMemoField2: TWideMemoField;
    FloatField21: TFloatField;
    WideStringField12: TWideStringField;
    QrNFeEveRCab1nSeqEvento: TSmallintField;
    QrNFeEveRCab1CNPJ_CPF: TWideStringField;
    QrNFeEveRCab1UF_Nome: TWideStringField;
    QrNFeEveRCab1cOrgao: TSmallintField;
    QrNFeEveRCab1versao: TFloatField;
    QrNFeEveRCab1FatID: TIntegerField;
    QrNFeEveRCab1FatNum: TIntegerField;
    QrNFeEveRCab1Empresa: TIntegerField;
    QrNFeEveRCab1Controle: TIntegerField;
    QrNFeEveRCab1tpAmb: TSmallintField;
    QrNFeEveRCab1chNFe: TWideStringField;
    QrNFeEveRCab1dhEvento: TDateTimeField;
    QrNFeEveRCab1tpEvento: TIntegerField;
    QrNFeEveRCab1descEvento: TWideStringField;
    QrNFeEveRCab1chNFe_NF_SER: TWideStringField;
    QrNFeEveRCab1chNFe_NF_NUM: TWideStringField;
    QrNFeEveRCab1ret_cStat: TIntegerField;
    QrNFeEveRCab1Status: TIntegerField;
    QrNFeEveRCab1ret_nProt: TWideStringField;
    QrNFeEveRCab1ret_dhRegEvento: TDateTimeField;
    QrNFeEveRCab1XML_Eve: TWideMemoField;
    QrNFeEveRCab1XML_retEve: TWideMemoField;
    QrNFeEveRCab1verEvento: TFloatField;
    QrNFeEveRCab1STATUS_TXT: TWideStringField;
    QrNFeEveRCan: TMySQLQuery;
    QrNFeEveRCanFatID: TIntegerField;
    QrNFeEveRCanFatNum: TIntegerField;
    QrNFeEveRCanEmpresa: TIntegerField;
    QrNFeEveRCanControle: TIntegerField;
    QrNFeEveRCanConta: TIntegerField;
    QrNFeEveRCannProt: TLargeintField;
    QrNFeEveRCannJust: TIntegerField;
    QrNFeEveRCanxJust: TWideStringField;
    QrNFeEveRCanLk: TIntegerField;
    QrNFeEveRCCe: TMySQLQuery;
    QrNFeEveRCCeFatID: TIntegerField;
    QrNFeEveRCCeFatNum: TIntegerField;
    QrNFeEveRCCeEmpresa: TIntegerField;
    QrNFeEveRCCeControle: TIntegerField;
    QrNFeEveRCCeConta: TIntegerField;
    QrNFeEveRCCexCorrecao: TWideMemoField;
    QrNFeEveRCCenCondUso: TSmallintField;
    QrNFeEveRCCeLk: TIntegerField;
    QrNFeEveRCCeDataCad: TDateField;
    QrNFeEveRCCeDataAlt: TDateField;
    QrNFeEveRCCeUserCad: TIntegerField;
    QrNFeEveRCCeUserAlt: TIntegerField;
    QrNFeEveRCCeAlterWeb: TSmallintField;
    QrNFeEveRCCeAtivo: TSmallintField;
    QrNfeEveRMDe: TMySQLQuery;
    QrNfeEveRMDeFatID: TIntegerField;
    QrNfeEveRMDeFatNum: TIntegerField;
    QrNfeEveRMDeEmpresa: TIntegerField;
    QrNfeEveRMDeControle: TIntegerField;
    QrNfeEveRMDeConta: TIntegerField;
    QrNfeEveRMDecSitConf: TSmallintField;
    QrNfeEveRMDetpEvento: TIntegerField;
    QrNfeEveRMDenJust: TIntegerField;
    QrNfeEveRMDexJust: TWideStringField;
    QrSumNFsInn: TMySQLQuery;
    QrSumNFsInnVL_DESC: TFloatField;
    QrSumNFsInnVL_MERC: TFloatField;
    QrSumNFsInnVL_FRT: TFloatField;
    QrSumNFsInnVL_SEG: TFloatField;
    QrSumNFsInnVL_OUT_DA: TFloatField;
    QrSumNFsInnVL_BC_ICMS: TFloatField;
    QrSumNFsInnVL_ICMS: TFloatField;
    QrSumNFsInnVL_BC_ICMS_ST: TFloatField;
    QrSumNFsInnVL_ICMS_ST: TFloatField;
    QrSumNFsInnVL_IPI: TFloatField;
    QrSumNFsInnVL_PIS: TFloatField;
    QrSumNFsInnVL_COFINS: TFloatField;
    QrSumNFsInnVL_DOC: TFloatField;
    QrEfdLinkCab: TMySQLQuery;
    QrEfdLinkCabSqLinked: TIntegerField;
    QrEfdLinkCabControle: TIntegerField;
    QrEfdLinkIts: TMySQLQuery;
    QrEfdLinkItsSqLinked: TIntegerField;
    QrEfdLinkItsConta: TIntegerField;
    QrTG1: TMySQLQuery;
    QrTG1Nome: TWideStringField;
    QrDestMadeBy: TSmallintField;
    QrCSTsInn: TMySQLQuery;
    QrCSTsInnICMS_Usa: TSmallintField;
    QrCSTsInnPIS_Usa: TSmallintField;
    QrCSTsInnCOFINS_Usa: TSmallintField;
    QrCSTsInnCodigo: TIntegerField;
    QrCSTsInnInterno: TSmallintField;
    QrCSTsInnContribui: TSmallintField;
    QrCSTsInnProprio: TSmallintField;
    QrCSTsInnUFEmit: TWideStringField;
    QrCSTsInnUFDest: TWideStringField;
    QrCSTsInnSubsTrib: TSmallintField;
    QrCSTsInnICMSAliq: TFloatField;
    QrCSTsInnCST_B: TWideStringField;
    QrCSTsInnpRedBC: TFloatField;
    QrCSTsInnmodBC: TSmallintField;
    QrCSTsInnpBCUFDest: TFloatField;
    QrCSTsInnpFCPUFDest: TFloatField;
    QrCSTsInnpICMSUFDest: TFloatField;
    QrCSTsInnpICMSInter: TFloatField;
    QrCSTsInnpICMSInterPart: TFloatField;
    QrCSTsInnUsaInterPartLei: TSmallintField;
    QrCSTsInncBenef: TWideStringField;
    QrCSTsInnpDif: TFloatField;
    QrCSTsInnCSOSN: TWideStringField;
    QrCSTsInnOriCST_ICMS: TWideStringField;
    QrCSTsInnOriCST_IPI: TWideStringField;
    QrCSTsInnOriCST_PIS: TWideStringField;
    QrCSTsInnOriCST_COFINS: TWideStringField;
    QrCSTsInnOriTES: TIntegerField;
    QrCSTsInnEFD_II_C195: TSmallintField;
    QrCSTsInnGenCtbD: TIntegerField;
    QrCSTsInnGenCtbC: TIntegerField;
    QrCSTsInnTES_ICMS: TSmallintField;
    QrCSTsInnTES_IPI: TSmallintField;
    QrCSTsInnTES_PIS: TSmallintField;
    QrCSTsInnTES_COFINS: TSmallintField;
    QrCSTsInnPISAliq: TFloatField;
    QrCSTsInnCOFINSAliq: TFloatField;
    QrCSTsInnRegFisCad_CtbD: TIntegerField;
    QrCSTsInnRegFisCad_CtbC: TIntegerField;
    QrCSTsInnRegFisCFOP_CtbD: TIntegerField;
    QrCSTsInnRegFisCFOP_CtbC: TIntegerField;
    QrCSTsInnRegFisUFs_CtbD: TIntegerField;
    QrCSTsInnRegFisUFs_CtbC: TIntegerField;
    QrCSTsInnOriCFOP: TWideStringField;
    QrCSTsInnServico: TSmallintField;
    QrCodGraGruX: TIntegerField;
    QrCSTsInnTES_BC_ICMS: TSmallintField;
    QrCSTsInnTES_BC_IPI: TSmallintField;
    QrCSTsInnTES_BC_PIS: TSmallintField;
    QrCSTsInnTES_BC_COFINS: TSmallintField;
    QrLocNFeFatID: TIntegerField;
    QrLocNFeEmpresa: TIntegerField;
    QrCodFatorQtd: TFloatField;
    QrNFECabBrefNFeSig: TWideStringField;
    QrStqMovNFsRefrefNFeSig: TWideStringField;
    QrNFeCabZProtpAto: TSmallintField;
    QrProds1obsCont_xCampo: TWideStringField;
    QrProds1obsCont_xTexto: TWideStringField;
    QrProds1obsFisco_xCampo: TWideStringField;
    QrProds1obsFisco_xTexto: TWideStringField;
    QrNFEItsVA: TMySQLQuery;
    QrNFEItsVAFatID: TIntegerField;
    QrNFEItsVAFatNum: TIntegerField;
    QrNFEItsVAEmpresa: TIntegerField;
    QrNFEItsVAnItem: TIntegerField;
    QrNFEItsVAobsCont_xCampo: TWideStringField;
    QrNFEItsVAobsCont_xTexto: TWideStringField;
    QrNFEItsVAobsFisco_xCampo: TWideStringField;
    QrNFEItsVAobsFisco_xTexto: TWideStringField;
    QrNFEItsVAVAItem: TSmallintField;
    QrFilialDirEvePedEPEC: TWideStringField;
    QrFilialDirEveRetEPEC: TWideStringField;
    QrNFeEveREPEC: TMySQLQuery;
    QrNFeEveREPECFatID: TIntegerField;
    QrNFeEveREPECFatNum: TIntegerField;
    QrNFeEveREPECEmpresa: TIntegerField;
    QrNFeEveREPECControle: TIntegerField;
    QrNFeEveREPECConta: TIntegerField;
    QrNFeEveREPECcOrgaoAutor: TSmallintField;
    QrNFeEveREPECtpAutor: TSmallintField;
    QrNFeEveREPECverAplic: TWideStringField;
    QrNFeEveREPECdhEmi: TDateTimeField;
    QrNFeEveREPECdhEmiTZD: TFloatField;
    QrNFeEveREPECtpNF: TSmallintField;
    QrNFeEveREPECIE: TWideStringField;
    QrNFeEveREPECdest_UF: TWideStringField;
    QrNFeEveREPECdest_CNPJ: TWideStringField;
    QrNFeEveREPECdest_CPF: TWideStringField;
    QrNFeEveREPECdest_idEstrangeiro: TWideStringField;
    QrNFeEveREPECdest_IE: TWideStringField;
    QrNFeEveREPECvNF: TFloatField;
    QrNFeEveREPECvICMS: TFloatField;
    QrNFeEveREPECvST: TFloatField;
    QrNFeEveREPECversao: TFloatField;
    QrNFeEveREPECdescEvento: TWideStringField;
    QrOpcoesNFeNFeUF_EPEC: TWideStringField;
    procedure QrTransportaCalcFields(DataSet: TDataSet);
    procedure Qr_D_ECalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrInfIntermedEntiCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    FMsg: String;
    FLastVerNFeLayI: Double;
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
    function DefineCESTdoNCM(const Nome, cProd, prod_NCM: String; var prod_CEST: Integer): Boolean;

    function  AssinarArquivoXML_EDmk(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
              TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
    function  AssinarArquivoXML_ACBr(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
              TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo

  public
    { Public declarations }
    ACBrNFe1: TACBrNFe;
    FJaConfigurouACBrNFe: Boolean;
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function LocalizaFiliaByDocum(Docum: String): Integer;
    function Alltrim(const Search: string): string;
    function StrZero(Num : Real; Zeros, Deci: Integer): String;
    //
    procedure AvisaAmbNaoExiste(LaAviso1, LaAviso2: TLabel);
    procedure AvisaUFNaoExiste(LaAviso1, LaAviso2: TLabel);
    //
    function TituloArqXML(const Ext: String; var Titulo, Descri: String): Boolean;
    function ObtemDirXML(const Ext: String; var Dir: String; Assinado:
             Boolean): Boolean;
    //
    function MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
              nNFFim: String; var Id: String): Boolean;
    function MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime; CNPJ: String;
             Modelo, Serie, NumeroNF: Integer; var ide_cNF: Integer;
             var cDV: String; var ChaveDeAcesso: String; const cNF_Atual,
             ide_tpEmis: Integer; versao: Double; MostraMsg: Boolean = True): Boolean;
    { Estão no NFeXMLGerencia!
    function DesmontaChaveDeAcesso(const Chave: String; const VersaoNFe:
             Double; var UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis,
             AleNFe: String): Boolean;
    function ChaveDeAcessoDesmontada(const Chave: String;
             const VersaoNFe: Double): String;
    }
    function  VerificaChavedeAcesso(Chave: String; PermiteNulo: Boolean): Boolean;
    function  DesmontaID_NFe(const ID: String; var Gru, Ext: String;
              var Codigo: Integer): Boolean;
    function  ExcluiNfe(Status, FatID, FatNum, Empresa: Integer;
              DeleteDI, DeleteGA: Boolean): Boolean;
    function  InvalidaNFe(Status, FatID, FatNum, Empresa: Integer;
              InvalidaDI, InvalidaGA: Boolean): Boolean;
    function  ExcluiEntradaItsDeNFe(FatID, FatNum, Empresa, nItem, OriCtrl:
              Integer): Boolean;
    function  ReopenEmpresa(Empresa: Integer): Boolean;
    function  ReopenEmit(Emit: Integer): Boolean;
    function  ReopenDest(Dest: Integer): Boolean;
    procedure ReopenTransporta(Transporta: Integer);
    procedure ReopenInfIntermedEnti(InfIntermedEnti: Integer);
    function  ReopenOpcoesNFe(Empresa: Integer; Avisa: Boolean): Boolean;
    procedure ReopenAliq(IDCtrl, FisRegCad, CFOP_MesmaUF, CFOP_Contrib,
              CFOP_Proprio, CFOP_SubsTrib: Integer; UF_Emit, UF_Dest: String);
    procedure ReopenCSTsInn(FisRegCad, CFOP_MesmaUF, CFOP_Contrib, CFOP_Proprio,
              CFOP_Servico, CFOP_SubsTrib: Integer; CFOPFormatado, UF_Emit,
              UF_Dest: String);
    function  ObtemNaturezaDaOperacao_Txt(CFOP2003: String): String;
    function  InsUpdNFeCabF(FatID, FatNum, Empresa, RetiradaUsa, RetiradaEnti:
              Integer; LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit):
              Boolean;
    function  InsUpdNFeCabG(FatID, FatNum, Empresa, EntregaUsa, EntregaEnti,
              L_Ativo, Cliente: Integer; LaAviso1, LaAviso2: TLabel; REWarning:
              TRichEdit): Boolean;
    function  InsUpdNFeCabXVol(FatID, FatNum, Empresa: Integer;
              SQL_VOLUMES: String; LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
    function  InsUpdNFeCabY(FatID, FatNum, Empresa: Integer;
              SQL_FAT_ITS: String; LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
    function  InsUpdNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
    function  InsUpdNFeIts(FatID, FatNum, Empresa, ICMS_Usa, IPI_Usa,
              II_Usa, PIS_Usa, COFINS_Usa, ISS_Usa: Integer; ISS_Alq: Double;
              UF_Emit, UF_Dest: String; FisRegCad: Integer; SQL_ITS, SQL_TOT,
              SQL_CUSTOMZ: String; LaAviso1, LaAviso2: TLabel; emit_CRT: Integer;
              CalculaAutomatico: Boolean; idDest, indFinal, dest_indIEDest:
              Integer; ide_dEmi: TDateTime): Boolean;
    function  TotaisNFe(FatID, FatNum, Empresa: Integer;
              LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
    function  StepNFeCab(FatID, FatNum, Empresa, Status: Integer;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  {05}stepNFePedido(): Integer;
    function  {10}stepNFeDados(): Integer;
    function  {20}stepNFeGerada(): Integer;
    function  {30}stepNFeAssinada(): Integer;
    function  {35}stepNFCeOffLine(): Integer; // 2020-11-02
    function  {40}stepLoteRejeitado(): Integer;
    function  {50}stepNFeAdedLote(): Integer;
    function  {60}stepLoteEnvEnviado(): Integer;
    function  {70}stepLoteEnvConsulta(): Integer;
    function  {90}stepNFeCabecalhoDwnl(): Integer;
    function  PodeAlterarNFe(Status: Integer): Boolean;

    function  NotaRejeitada(cStat: Integer): Boolean;
    function  NotaDenegada(cStat: Integer): Boolean;

    function  DefX(Codigo, ID: String; Texto: String): String;
    function  DefI(Codigo, ID: String; ValMin, ValMax, Numero: Integer): Integer;
    function  DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
    function  MensagemDeID_NFe(Codigo, ID, Valor: String; Texto: String; ID_Chamou: Integer): String;
    //function  ID_De_ID_NFe(ID: String): String;
    //function  EnvioDeLote(siglaUF, nomeCertificado: String): Boolean;

    // chamadas da DLL
    {
    function  pStatusServico(siglaUF, tpAmb, titCertificado: String;
              LaAviso1, LaAviso2: TLabel): String;
    }
    {
    function  pAssinarXML(pathDocXML, tipDocXML, titCertificado: String;
              out aResultado: String): Integer;
    function  pEnviaLoteXML(siglaUF, tpAmb, titCertificado, MsgCabec, MsgDados:
              String; LaAviso1, LaAviso2: TLabel): String;
    function  pConsultaRetNFe(siglaUF, tpAmb, titCertificado, Recibo:
              String; LaAviso1, LaAviso2: TLabel): String;
    function  pCancelaNFe(siglaUF, tpAmb, titCertificado, cChaveNFe,
              cProtocoloNFe, cJust: String; LaAviso1, LaAviso2: TLabel): String;
    function  pInutilizarNFe(siglaUF, tpAmb, titCertificado, emitCNPJ,
              iJust: String; nNFIni, nNFFim, Modelo, Serie, Ano: String;
              LaAviso1, LaAviso2: TLabel): String;
    }
    {
    function  AssinarXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa, TipoDoc:
              Integer; var XMLAssinado_Dir: String): Boolean;
    }
    function  SalvaXML(Ext, Arq: String; Texto: String; rtfRetWS:
              TMemo; Assinado: Boolean): String;
    procedure AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean);
    function  InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
              Associada, RegrFiscal, GraGruX: Integer; NO_tabelaPrc,
              NO_TbFatPedFis: String; StqCenCad, FatSemEstq, AFP_Sit: Integer;
              AFP_Per, Qtde: Double; Cli_Tipo: Integer; Cli_IE: String; Cli_UF,
              EMP_UF, EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
              Item_IPI_ALq: Double; Preco_PrecoF, Preco_PercCustom,
              Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE: Double;
              Preco_MedOrdem: Integer; SQLType: TSQLType; PediVda, OriPart,
              InfAdCuztm, TipoNF, modNF, Serie, nNF, SitDevolu, Servico: Integer;
              refNFe, refNFeSig: String; TotalPreCalc: Double; var OriCtrl: Integer; Pecas,
              AreaM2, AreaP2, Peso: Double; TipoCalc, prod_indTot: Integer;
              IDCtrl_PreDef: Integer): Boolean;
    function  SQL_ITS_ITS_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
    function  SQL_ITS_TOT_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
    function  SQL_FAT_ITS_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
    function  SQL_FAT_TOT_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
    function  SQL_VOLUMES_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;

    function  ObtemNumeroCFOP_Emissao(Tabela_FatPedFis: String; Parametros: array of integer;
              Empresa, Cliente, Item_MadeBy, RegrFiscal: Integer;
              EhServico, EhSubsTrib: Boolean; var CFOP: String; var ICMS_Aliq: Double;
              var CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer): Boolean;
    //////////////////////////////////////////////////////////////////////////////////////////
    function  ObtemNumeroCFOP_Entrada(
              (*Tabela_FatPedFis: String; Parametros: array of integer;*)
              const Empresa, Fornecedor, (*Item_MadeBy,*) PVD_MadBy, RegrFiscal:
              Integer;
              const EhServico(*, EhSubsTrib*): Boolean;
              const CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido: Integer;
              const CFOP_Emitido: String;
              var CFOP: String; (*var ICMS_Aliq: Double;*)
              var CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
              var ICMS_CST, IPI_CST, PIS_CST, COFINS_CST: String;
              var OriTES, EFD_II_C195, GenCtbD, GenCtbC,
              TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
              TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
              var ICMSAliq, PISAliq, COFINSAliq: Double;
              var FisRegGenCtbD, FisRegGenCtbC: Integer): Boolean;

    /////////////////////////////////////////////////////////////////////////////////////////
    //function AppCodeNFe(Empresa: Integer): Byte;
    function  DesfazEncerramento(Tabela: String; FatID, FatNum, Empresa: Integer;
              AutorizarBoss: Boolean = False): Boolean;
    function  Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String; Assinado: Boolean): String;
    procedure AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean; WebBrowser: TWebBrowser);
    function  AtualizaRecuperacaoDeImpostos(FatID, FatNum, Empresa: Integer): Boolean;
    function  DefineEntidadePeloDoc(CNPJ, CPF: String; SomenteCliInt: Boolean = False): Integer;
    function  ReopenQrA(FatID, FatNum, Empresa: Integer): Boolean;
    function  ReopenQrI(FatID, FatNum, Empresa: Integer): Boolean;
    function  ReopenQrN_QrS(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    procedure VerificaNFeCabA();
    function  NomeFatID_NFe(FatID: Integer): String;
    function  AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
    function  AtualizaXML_No_BD_NFe(const IDCtrl: Integer; var Dir, Aviso:
              String): Boolean;
    function  AtualizaXML_No_BD_EveConfirmado(
                    EventoLote: Integer; Id, cOrgao,
                    tpAmb: String; TipoEnt: Integer; CNPJ,
                    CPF, chNFe, dhEvento: String;
                    TZD_UTC: Double; verEvento, tpEvento,
                    nSeqEvento, versao, descEvento,
                    XML_Eve, XML_retEve, Status,
                    ret_versao, ret_Id, ret_tpAmb,
                    ret_verAplic, ret_cOrgao, ret_cStat,
                    ret_xMotivo, ret_chNFe, ret_tpEvento,
                    ret_xEvento, ret_nSeqEvento, ret_CNPJDest,
                    ret_CPFDest, ret_emailDest, ret_dhRegEvento: String;
                    ret_TZD_UTC: Double; ret_nProt: String): Boolean;
    function  AtualizaXML_No_BD_AutConfirmada(const chNFe, XML_Aut: String): Boolean;
    function  AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Aut_Sinc(const IDCtrl, LoteEnv: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_ConsultaNFe(const Chave, Id: String;
              const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Can(const IDCtrl: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir, Aviso:
              String): Boolean;
    function  FormataLoteNFe(Lote: Integer): String;
    function  FormataNSU_NFe(NSU: Int64): String;
    procedure LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
              PageControl: TPageControl; ActivePageIndex: Integer);
    function  AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
              TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
    function  AvisaUsoIndevidoICMS_doCSOSN(): Boolean;
    function  AvisaFaltaDaMudancaDaAliquotaSimplesNacional(): Boolean;
    procedure VerificaExcecao(const FisRegCad, MesmaUF, Contrib, Proprio,
              SubsTrib: Integer; const UF_Emit, UF_Dest: String; const GraGruX: Integer;
              var CST_B: String; var ICMSAliq, pRedBC, pDif: Double; var modBC,
              AchouFisc: Integer; var cBenef, CSOSN: String);
    function  Obtem_Serie_e_NumNF_Novo_NFe(const SerieDesfeita, NFDesfeita,
              SerieNormal, Controle, Empresa, Filial, MaxSeq: Integer;
              EdSerieNF, EdNumeroNF: TdmkEdit(*;
              var Serie, NumNF: Integer*)): Boolean;
    function  Obtem_Serie_e_NumNF_Novo_NFCe(const SerieDesfeita, NFDesfeita,
              SerieNormal, Controle, Empresa, FIlial, MaxSeq: Integer;
              EdSerieNF, EdNumeroNF: TdmkEdit): Boolean;
    procedure ColoreStatusNFeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const Status, Modelo: Integer);
    procedure ColoreSitConfNFeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const SitConf: Integer);
    procedure ColoreinfCCe_nSeqEventoEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const infCCe_nSeqEvento: Integer);
    procedure ConfigurarComponenteNFe();
    function  Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento: Integer):
              Integer;
    function  EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
              chNFe: String): Integer;
    procedure EventoObtemSub(const Controle: Integer; const IdLote: String;
              var SubCtrl: Integer; var SQLType: TSQLType);
    function  FormataID_NFe(Id: String): String;
    function  LocLctCabY(const FatID, FatNum, FatParcela, Empresa: Integer;
              var Controle: Integer): Boolean;
    function  LocalizaNFeInfoCCe(const ChNFe: String; nSeqEvento: Integer; var
              CNPJ, CPF: String; var
              dhEvento: TDateTime; var dhEventoTZD, verEvento: Double; var
              nCondUso: Integer; var xCorrecao: String): Boolean;
    function  LocalizaNFeInfoCanEve(const ChNFe: String; var Id, xJust: String;
              var cJust: Integer): Boolean;
    function  LocalizaNFeInfoEPECEve(const ChNFe: String; var Id: String): Boolean;
    function  LocalizaNFeInfoMDeEve(const ChNFe: String; var Id, xJust: String;
              var cJust, tpEvento: Integer): Boolean;
    function  ObtemIDCtrlDeChaveNFe(ChaveNFe: String): Integer;
    function  ReopenLocEve(Id: String; Evento: Integer): Boolean;
    procedure AtualizaLctoFaturaNFe(FatID, FatNum, Empresa: Integer);
    function  CancelaFaturamento(ChaveNFe: String): Boolean;
    (*
    function  InsereFaturaNFes(QrPrzX, QrPrzT, QrSumX: TmySQLQuery; CondicaoPG,
              AFP_Sit, EMP_FaturaNum, EMP_IDDuplicata, NumeroNF, EMP_FaturaSeq,
              TIPOCART, CartEmis, EMP_CtaFaturas, Empresa, NF_Emp_Numer,
              Represen: Integer; TotalNFe, AFP_Per: Double; EMP_TpDuplicata,
              EMP_FaturaSep, EMP_TxtFaturas, NF_Emp_Serie: String;
              DataFat: TDateTime): Boolean;
    *)
    function  ValorICMSST_0(ValAtu, ValFut, pICMS: Double): Double;
    procedure CorrigeFatParcelaNFeCabY(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
    //
    function  ObtemSiglaUFdeDTBdeCidade(Cidade: Integer): String;
    //
    procedure ReopenNFeArq(IDCtrl: Integer);
    procedure ReopenNFeLayI();
    function  ReopenNFeCabA(FatID, FatNum, Empresa: Integer): Integer;
    procedure ReopenNFeCabB(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabF(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabG(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabGA(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabXLac(FatID, FatNum, Empresa, Controle: Integer);
    procedure ReopenNFeCabXReb(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabXVol(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabY(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabYA(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabZCon(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabZFis(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeCabZPro(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeItsI(FatID, FatNum, Empresa: Integer);
    procedure ReopenNFeItsIDI(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsI03(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsINVE(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsLA(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsM(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsN(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsNA(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsO(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsP(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsQ(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsR(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsS(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsT(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsU(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsUA(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsV(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsVA(FatID, FatNum, Empresa, nItem: Integer);
    procedure ReopenNFeItsIDIa(FatID, FatNum, Empresa, nItem, Controle: Integer);
    //
    procedure ObtemValorAproximadoDeTributos1(const UF, prod_NCM, prod_EXTIPI:
              String; ICMS_Orig: Integer; const prod_vProd, prod_vDesc: Double;
              var vBasTrib, pTotTrib, vTotTrib: Double; var tabTotTrib: Integer;
              var verTotTrib: String; var tpAliqTotTrib, FontTotTrib: Integer);
    procedure ObtemValorAproximadoDeTributos2(const UF, prod_NCM, prod_EXTIPI:
              String; ICMS_Orig: Integer; const prod_vProd, prod_vDesc: Double;
              var vBasTrib, pTotTrib, vTotTrib: Double; var tabTotTrib: Integer;
              var verTotTrib: String; var tpAliqTotTrib, FontTotTrib: Integer;
              var vTribFed, pTribFed, vTribEst, pTribEst, vTribMun, pTribMun:
              Double);
    function  CalculaICMSDeson(ICMS_VICMS: Double): Double;
    function  VerificaICMSDeson(const ICMS_vICMSDeson: Double; var
              ICMS_motDesICMS: Integer): Boolean;
    procedure ReopenIBPTax1(Tabela: Integer; EX, prod_NCM, UF: String;
              var VigenFim: TDate);
    procedure ReopenIBPTax2(Tabela: Integer; EX, prod_NCM, UF: String;
              var VigenFim: TDate);
    function  UpdSerieNFDesfeita(Tabela: String; FatNum, Serie, nNF: Integer): Boolean;
    function  ObtemURLs_QRCode(const UF: String; const Versao: Double; const
              tpAmb: Integer; var URL_QrCode, URL_Consulta: String): Boolean;
    procedure ReopenSMVA(Tipo, Empresa, FatPedCab: Integer);
    procedure AtualizaTotaisEFDInnNFs(Controle: Integer);
    procedure ReopenEfdLinkCab(MovFatID, MovFatNum, MovimCod, Empresa: Integer);
    procedure ReopenEfdLinkIts(MovFatID, MovFatNum, MovimCod, Empresa, SqLinked: Integer);
    // Tabelas do governo
    function  ObtemDescricao_TabGov_1(Codigo: Integer; Tabela: String; DataRef: TDateTime): String;
    function  CST_ICMS_Eh_SubsTrib(CRT, CST_A, CST_B, CSOSN: Integer): Boolean;
    function  DefineCodFornece(const SQLType: TSQLType; const CodInfoEmit:
              Integer; const prod_cProd, prod_xProd, prod_cEAN, prod_NCM,
              prod_uCom, InfAdProd: String; const prod_CFOP, ICMS_Orig,
              ICMS_CST, IPI_CST, PIS_CST, COFINS_CST: Integer;
              const prod_EXTIPI, IPI_cEnq: String; var Nivel1,
              GraGruEIts_GraGruX, GraGruX: Integer;
              var GraGruEIts_NomeGGX: String; var FatorQtd: Double;
              Continuar: Boolean = False): Boolean;
  end;

var
  DmNFe_0000: TDmNFe_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, MyListas, UnMLAGeral, ModuleGeral,
{$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
{$IFNDef semPediVda}ModPediVda, {$EndIf}
{$IFNDef semNFe_v0000}
  NFeDocOrfao, NFeXMLGerencia, NFe_PF, NFe_Pesq_0000, NFaEditCSOSN202,
  NFaEditCSOSN500,
{$EndIF}
{$IfNDef semNFCe_0000}NFCe_PF,{$EndIf}
//{$IFNDef semNFe_v 0 2 0 0}NFeSteps_0200, {$EndIF}
  UnInternalConsts, Entidade2, MyDBCheck, Principal, UnGrade_Tabs, ModProd,
  ModuleFatura, UnGrade_Jan;

{$R *.dfm}

const
  CO_VersaoQrCode = 2;

    (* Essa função retira os pontos de valores numéricos e no lugar da vírgula coloca um ponto...*)
procedure TDmNFe_0000.DataModuleCreate(Sender: TObject);
begin
  FLastVerNFeLayI := 0;
  ACBrNFe1 := TACBrNFe.Create(Self);
  FJaConfigurouACBrNFe := False;
end;

procedure TDmNFe_0000.DataModuleDestroy(Sender: TObject);
begin
  try
    FreeAndNil(ACBrNFe1);
  except
    // nada
  end;
end;

function TDmNFe_0000.Alltrim(const Search: string): string;
const
  BlackSpace = [#33..#126];
var
  Index: byte;
begin
  Index:=1;
  while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
    begin
      Index:=Index + 1;
    end;
  Result:=Copy(Search, Index, 255);
  Index := Length(Result);
  while (Index > 0) and not (Result[Index] in BlackSpace) do
    begin
      Index:=Index - 1;
    end;
  Result := Copy(Result, 1, Index);
end;

function TDmNFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String;
  Empresa, TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
const
  sProcName = 'TDmNFe_0000.AssinarArquivoXML()';
var
  Msg: String;
begin
  Result := False;
  // TFormaAssinaXXe = (faxxeCAPICOM=0, faxxeDotNetFrameWork=1, faxxeACBr=2);
  case TFormaAssinaXXe(VAR_AssDigMode) of
    (*0*)faxxeCAPICOM:
      Result := AssinarArquivoXML_EDmk(XMLGerado_Arq, XMLGerado_Dir, Empresa,
      TipoDoc, IDCtrl, XMLAssinado_Dir);
    //(*1*)faxxeDotNetFrameWork:
    (*2*)faxxeACBr:
      Result := AssinarArquivoXML_ACBr(XMLGerado_Arq, XMLGerado_Dir, Empresa,
      TipoDoc, IDCtrl, XMLAssinado_Dir);
    (*?*)else
    begin
      Msg := GetEnumName(TypeInfo(TFormaAssinaXXe), Integer(VarType(VAR_AssDigMode)));
      Geral.MB_Erro('Forma de assinatura (' + Msg + ') não definida em ' + sProcName);
    end;
  end;
end;

function TDmNFe_0000.AssinarArquivoXML_ACBr(XMLGerado_Arq,
  XMLGerado_Dir: String; Empresa, TipoDoc, IDCtrl: Integer;
  var XMLAssinado_Dir: String): Boolean;
const
  sProcName= 'TDmNFe_0000.AssinarArquivoXML_ACBr()';
var
  fEntrada: file;
  NumRead, I: integer;
  buf: char;
  xmldoc: String;
  Ext, FonteXML: String;
  Assinada, Aviso, Dir: String;
begin
  Result := False;
  // Abrir    Arquivo := XMLGerado_Dir + XMLGerado_Arq;
  if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
  FonteXML := XMLGerado_Dir + XMLGerado_Arq;
  if MyObjects.FIC_ArquivoNaoExiste(XMLGerado_Dir, XMLGerado_Arq, FonteXML) then
    Exit;
  //
  ConfigurarComponenteNFe();
(*
  for I := 0 to ACBrNFe1.NotasFiscais.Count - 1 do
  begin
    if to ACBrNFe1.NotasFiscais[I].ide.nNFe =
  end;
*)
  case ACBrNFe1.NotasFiscais.Count of
    0: ; // ok!
    1: ACBrNFe1.NotasFiscais.Delete(0);
    else //ver o que fazer aqui, pois podem ter varias NFs no lote!
    begin
      Geral.MB_Erro('Lote com mais de uma NFe não implementado ainda! AVISE A DERMATEK!');
      Exit;
    end;
  end;




  ACBrNFe1.NotasFiscais.LoadFromFile(FonteXML, False);

  if ACBrNFe1.NotasFiscais.Count > 0 then
  begin

    ACBrNFe1.NotasFiscais.Assinar;
    Result := Length(ACBrNFe1.NotasFiscais[0].XMLAssinado) > 0;
    //

    case TipoDoc of
      0: Ext := NFE_EXT_NFE_XML;
      1: Ext := NFE_EXT_CAN_XML;
      2: Ext := NFE_EXT_INU_XML;
      else Ext := '???';
    end;

    Assinada := ACBrNFe1.NotasFiscais[0].XMLAssinado;
    SalvaXML(Ext, XMLGerado_Arq,  Assinada, nil, True);

    ////////////////////////////////////////////////////////////////////////////
    // Verificar se ACBrNFe1 mudou o XML original.
    // Caso mudou salvar novamente!
    AssignFile(fEntrada, XMLGerado_Dir + XMLGerado_Arq);
    Reset(fEntrada, 1);
    xmlDoc := '';
    for I := 1 to FileSize(fEntrada) do
    begin
      Blockread(fEntrada, buf, 1, NumRead);
      xmlDoc := xmlDoc + buf;
    end;
    CloseFile(fEntrada);
    //
    if xmlDoc <> ACBrNFe1.NotasFiscais[0].XMLOriginal then
      SalvaXML(Ext, XMLGerado_Arq,  ACBrNFe1.NotasFiscais[0].XMLOriginal, nil, False);
    ////////////////////////////////////////////////////////////////////////////

    // Gravar no BD o XML da NFe
    if TipoDoc = 0 then
    begin
      DModG.ReopenParamsEmp(Empresa);
      Dir := DModG.QrPrmsEmpNFeDirNFeAss.Value;
      Aviso := '';
      AtualizaXML_No_BD_NFe(IDCtrl, Dir, Aviso);
      if Aviso <> '' then
        Geral.MB_Aviso(
        'O arquivo abaixo não foi localizado para ser adicionado ao banco de dados:'
        + sLineBreak + Aviso);
    end;
  end else
    Geral.MB_Erro('XML Não foi carregado em: ' + sProcName);
end;

function TDmNFe_0000.AssinarArquivoXML_EDmk(XMLGerado_Arq,
  XMLGerado_Dir: String; Empresa, TipoDoc, IDCtrl: Integer;
  var XMLAssinado_Dir: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
  xmldoc, ref: String;
  Ext, FonteXML: String;
  //
  NumeroSerial: String;
  Cert: ICertificate2;
  Assinada: String;
  Aviso, Dir: String;
  //
  _CSC, DigestValue, XMLAntes, XMLDepois, XMLQrCode, AssinadaComQrCode, UF,
  URL_QrCode, URL_Consulta, HashQrCode, AllTxtLink, CSCpos: String;
  PosIni, Tam: Integer;
begin
  Result := False;
  // Abrir    Arquivo := XMLGerado_Dir + XMLGerado_Arq;
  if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;

  (*
  FonteXML := XMLGerado_Dir + XMLGerado_Arq;
  if not FileExists(FonteXML) then
  begin
    Geral.MensagemBox('Arquivo não encontrado: ' + FonteXML,
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  *)
  if MyObjects.FIC_ArquivoNaoExiste(XMLGerado_Dir, XMLGerado_Arq, FonteXML) then
    Exit;

  //
  AssignFile(fEntrada, XMLGerado_Dir + XMLGerado_Arq);
  Reset(fEntrada, 1);
  xmlDoc := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    xmlDoc := xmlDoc + buf;
  end;

  CloseFile(fEntrada);

  //

  if xmlDoc <> '' then
  begin
    ReopenEmpresa(Empresa);
    if QrFilialNFeSerNum.Value <> '' then
    begin
      NumeroSerial := QrFilialNFeSerNum.Value;
      //
      case TipoDoc of
        0: ref := 'infNFe';
        1: ref := 'infCanc';
        2: ref := 'infInut';
        else ref := '???';
      end;

      case TipoDoc of
        0: Ext := NFE_EXT_NFE_XML;
        1: Ext := NFE_EXT_CAN_XML;
        2: Ext := NFE_EXT_INU_XML;
        else Ext := '???';
      end;
      if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
      begin
        Result := NFeXMLGeren.AssinarMSXML(xmlDoc, Cert, Assinada);
        if Result then
        begin
          //Fazer aqui tag do QrCode?
          if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
          begin
        {$IfNDef semNFCe_0000}
            if UnNFCe_PF.ObtemDadosCSC(_CSC, CSCpos) then
            begin
              PosIni := pos('<DigestValue>', Assinada);
              posIni := PosIni + Length('<DigestValue>');
              Tam    := pos('</DigestValue>', Assinada) - PosIni;
              DigestValue := Copy(Assinada, PosIni, Tam);
              //
              HashQrCode := UnNFCe_PF.HashQrCode(
              DmNFe_0000.QrNFECabAide_tpEmis.Value,
              DmNFe_0000.QrNFECabAId.Value,
              CO_VersaoQrCode, DmNFe_0000.QrNFECabAide_tpAmb.Value,
              CSCPos, _CSC, DmNFe_0000.QrNFECabAide_dEmi.Value,
              DmNFe_0000.QrNFECabAICMSTot_vNF.Value, DigestValue);
              //
              UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(DmNFe_0000.QrNFECabAide_cUF.Value);
              URL_QrCode   :=  DmNFe_0000.QrNFECabAURL_QrCode.Value;
              URL_Consulta :=  DmNFe_0000.QrNFECabAURL_Consulta.Value;
              (*
              if DmProd.ObtemURLs_QRCode(UF, DmNFe_0000.QrNFECabAversao.Value,
              DmNFe_0000.QrNFECabAide_tpAmb.Value, URL_QrCode, URL_Consulta) then
              *)
              begin
                AllTxtLink := URL_QrCode + 'p=' + Uppercase(HashQrCode);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'AllTxtLink'], ['IDCtrl'], [AllTxtLink], [IDCtrl], True);
                //
                posIni := pos('</infNFe>', Assinada) + Length('</infNFe>');
                XMLAntes   := Copy(Assinada, 0, posIni -1);
                XMLDepois  := Copy(Assinada, posIni);
                // ...
                //Colocar texto no XML!
                XMLQrCode :=
                '<infNFeSupl>' +
                  '<qrCode>' +
                    '<![CDATA[' +//http://www.fazenda.pr.gov.br/nfce/qrcode/?p=41180808187168000160652550000700231000000013%7C2%7C2%7C1%7CC461A4498B608ABBBA3DE6E7E2FEAF7EED4F7017
                    AllTxtLink+
                    ']]>' +
                  '</qrCode>' +
                  '<urlChave>' + //http://www.sefaz.mt.gov.br/nfce/consultanfce
                    URL_Consulta +
                  '</urlChave>' +
                '</infNFeSupl>';
                //
                AssinadaComQrCode   := XMLAntes + XMLQrCode + XMLDepois;
                //Geral.MB_Info(AssinadaComQrCode);
                Assinada := AssinadaComQrCode;
              end;

//http://www.fazenda.pr.gov.br/nfce/qrcode/?p=41180808187168000160652550000700231000000013|2|2|1|C461A4498B608ABBBA3DE6E7E2FEAF7EED4F7017
            end;
            {$Else}
              Geral.MB_Info('NFC-e não habilitado para este ERP');
            {$EndIf};
          end;

          SalvaXML(Ext, XMLGerado_Arq,  Assinada, nil, True);
          if TipoDoc = 0 then
          begin
            DModG.ReopenParamsEmp(Empresa);
            Dir := DModG.QrPrmsEmpNFeDirNFeAss.Value;
            Aviso := '';
            AtualizaXML_No_BD_NFe(IDCtrl, Dir, Aviso);
            if Aviso <> '' then
              Geral.MB_Aviso(
              'O arquivo abaixo não foi localizado para ser adicionado ao banco de dados:'
              + sLineBreak + Aviso);
          end;
        end;
      end;// else Result := False;
    end
    else
      Geral.MensagemBox('Serial do Certificado não informado...',
      'Aviso', MB_OK+MB_ICONWARNING);
  end
  else
    Geral.MensagemBox('Documento XML para assinatura não informado...',
    'Aviso', MB_OK+MB_ICONWARNING);
  Result := True;
end;

function TDmNFe_0000.CalculaICMSDeson(ICMS_VICMS: Double): Double;
begin
  Result := Geral.RoundC(ICMS_VICMS * QrProds1ICMS_pICMSDeson.Value, 2) / 100;
  //
  if Result < 0 then
    Result := 0;
end;

function TDmNFe_0000.CancelaFaturamento(ChaveNFe: String): Boolean;
const
  sProcName = 'TDmNFe_0000.CancelaFaturamento()';
  //
  procedure AtualizaLct(Lancto, LctSit: Integer; Tabela: String);
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
      ['Debito', 'Credito', 'Sit'], ['Controle'], [0, 0, LctSit], [Lancto], True);
  end;
  //
const
  Baixa   = 0;
  ValiStq = 101; //Cancelado
  LctSit  = 4; //Cancelado
var
  IDCtrl, Lancto, FatId, FatNum, Filial: Integer;
  Tabela, TabLctA, TabLctB, TabLctD: String;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT nfe.FatID, nfe.FatNum, cli.CodFilial Filial ',
    'FROM nfecaba nfe ',
    'LEFT JOIN enticliint cli ON cli.CodEnti = nfe.Empresa ',
    'WHERE nfe.Id="' + ChaveNFe + '"',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    FatId  := QrLoc.FieldByName('FatID').AsInteger;
    FatNum := QrLoc.FieldByName('FatNum').AsInteger;
    Filial := QrLoc.FieldByName('Filial').AsInteger;
  end else
    Exit;
  //
  try
    QrLoc.DisableControls;
    Screen.Cursor := crHourGlass;
    //
    //Ini => Cancela estoque
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT IDCtrl, "stqmovitsa" Tabela ',
      'FROM stqmovitsa ',
      //'WHERE Tipo=1 ',
      'WHERE Tipo=' + Geral.FF0(FatID),
      'AND OriCodi=' + Geral.FF0(FatNum),
      ' UNION ',
      'SELECT IDCtrl, "stqmovitsb" Tabela ',
      'FROM stqmovitsb ',
      //'WHERE Tipo=1 ',
      'WHERE Tipo=' + Geral.FF0(FatID),
      'AND OriCodi=' + Geral.FF0(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        IDCtrl := QrLoc.FieldByName('IDCtrl').AsInteger;
        Tabela := LowerCase(QrLoc.FieldByName('Tabela').AsString);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
          ['Baixa', 'ValiStq'], ['IDCtrl'], [Baixa, ValiStq], [IDCtrl], False);
        //
        QrLoc.Next;
      end;
    end;
    if (FatID = VAR_FATID_0002) and (FatNum <> 0) then
    begin
      case CO_DMKID_APP of
        28: // T o o l r e n t
        begin
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
          'UPDATE loccconits SET NF_Stat=' + Geral.FF0(CO_101_NFeCancelada),
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND ide_Mod=' + Geral.FF0(CO_MODELO_NFE_65),
          '']));
        end;
        else Geral.MB_Erro('Tabela de venda ao conumidor não definida em ' +
        sProcName)
      end;
    end;
    //Fim => Cancela estoque
    //Ini => Cancela financeiro
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT Lancto ',
      'FROM nfecaby ',
      'WHERE FatID=' + Geral.FF0(FatId),
      'AND FatNum=' + Geral.FF0(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      //
      while not QrLoc.Eof do
      begin
        Lancto := QrLoc.FieldByName('Lancto').AsInteger;
        //
        if Lancto <> 0 then
        begin
          {$IfDef DEFINE_VARLCT}
            //Financeiro novo
            try
              QrLoc2.DisableControls;
              //
              TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
              TabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, Filial);
              TabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, Filial);
              //
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctA,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctA);
              //
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctB,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctB);
              ///
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctD,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctD);
            finally
              QrLoc2.EnableControls;
            end;
          {$Else}
            //Financeiro antigo
            AtualizaLct(Lancto, LctSit, VAR_LCT);
          {$EndIf}
        end;
        QrLoc.Next;
      end;
    end;
    //Fim => Cancela financeiro
  finally
    Screen.Cursor := crDefault;
    QrLoc.EnableControls;
    Result := True;
  end;
end;

function TDmNFe_0000.StrZero(Num : Real; Zeros, Deci: Integer): String;
var
  tam, z : Integer;
  res, zer : String;
begin
  str(Num:Zeros:Deci, res);
  res := Alltrim(res);
  tam := length(res);
  zer := '';
  for z := 1 to (Zeros-tam) do
    zer := zer + '0';
  result := zer+res
end;

function TDmNFe_0000.DefI(Codigo, ID: String; ValMin, ValMax, Numero: Integer): Integer;
  function Txt: String;
  begin
    Result := IntToStr(Numero);
  end;
begin
  Result := Numero;
  if QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if (QrNFeLayIOcorMin.Value > 0) and (Numero < ValMin) then
    begin
      if FMsg = '' then
        FMsg := MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer abaixo do mínimo!', 4);
    end else
    if (QrNFeLayIOcorMin.Value > 0) and (Numero > ValMax) then
    begin
      if FMsg = '' then
        FMsg := MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer acima do máximo!', 5);
    end;
  end else FMsg := 'Não foi possível definir o item #' + Codigo + '- ID = ' +
    ID + ' pelo layout da NF-e!';
end;

function TDmNFe_0000.DefineCESTdoNCM(const Nome, cProd, prod_NCM: String; var
  prod_CEST: Integer): Boolean;
var
  NCM: String;
  //
  function SelCEST(): Integer;
  const
    Aviso  = '...';
    Titulo = 'Seleção de CEST';
    Prompt = 'CEST do ';
    Campo  = 'Descricao';
  var
    _CEST: Variant;
  begin
    Result := 0;
    //
    _CEST := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt + Nome, nil, nil, Campo, 0, [
    'SELECT Codigo, CONCAT(NCM, " - ", Nome) ' + Campo,
    'FROM cest ',
    'WHERE NCM="' + NCM + '" ',
    'ORDER BY NCM DESC ',
    ''], Dmod.MyDB, True);
    if _CEST <> Null then
    begin
      Result := _CEST;
    end;
  end;
begin
  NCM := prod_NCM;
  prod_CEST := 0;
  while (prod_CEST = 0) and (NCM <> EmptyStr) do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocCEST, Dmod.MyDB, [
    'SELECT CEST, NCM, Nome ',
    'FROM cest ',
    'WHERE NCM="' + NCM + '" ',
    'ORDER BY NCM DESC ',
    '']);
    case QrLocCEST.RecordCount of
      0: ;
      1: prod_CEST := Geral.IMV(QrLocCESTCEST.Value);
      else prod_CEST := SelCEST();
    end;
    if prod_CEST <> 0 then
      Exit
    else
      NCM := Copy(NCM, 1, Length(NCM)-1)
  end;
end;

function TDmNFe_0000.DefineCodFornece(const SQLType: TSQLType; const
  CodInfoEmit(*, PrdGrupTip*): Integer; const prod_cProd, prod_xProd, prod_cEAN,
  prod_NCM, prod_uCom, InfAdProd: String; const prod_CFOP, ICMS_Orig, ICMS_CST,
  IPI_CST, PIS_CST, COFINS_CST: Integer; const prod_EXTIPI, IPI_cEnq: String;
  var Nivel1, GraGruEIts_GraGruX, GraGruX: Integer; var GraGruEIts_NomeGGX:
  String; var FatorQtd: Double; Continuar: Boolean = False): Boolean;
  //
  procedure ReopenCod();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCod, Dmod.MyDB, [
    'SELECT gg1.Nivel1, ggx.Controle GraGruX, ',
    'gg1.ICMSRec_pRedBC, gg1.IPIRec_pRedBC, gg1.PISRec_pRedBC, gg1.COFINSRec_pRedBC, ',
    'gg1.ICMSRec_pAliq, gg1.IPIRec_pAliq, gg1.PISRec_pAliq, gg1.COFINSRec_pAliq, ',
    'gg1.ICMSRec_tCalc, gg1.IPIRec_tCalc, gg1.PISRec_tCalc, gg1.COFINSRec_tCalc, ',
    'gge.FatorQtd ',
    'FROM gragrueits gge ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gge.Nivel1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ',
    'WHERE gge.Fornece=' + Geral.FF0(CodInfoEmit),
    'AND gge.cProd="' + prod_cProd + '"',
    'AND (gge.uCom="' + prod_uCom + '" OR gge.uCom="")',
    ' ']);
    FatorQtd := QrCodFatorQtd.Value;
  end;
  var
    ThisSQLType: TSQLType;
begin
  ReopenCod();
  //
  case QrCod.RecordCount of
    0:
    begin
      //if CO_DMKID_APP <> 2 then //Bluederm => O Marco não quer que configure o produto
      begin
        VAR_NOME_NOVO_GG1 := prod_xProd;
        Grade_Jan.MostraFormGraGruEIts(stIns, CodInfoEmit,
          prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
          infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
          PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
          GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
        //
        if GraGruEIts_GraGruX <> 0 then
          GraGruX := GraGruEIts_GraGruX;
        //
        VAR_NOME_NOVO_GG1 := EmptyStr;
        //
        ReopenCod();
        //
        Nivel1 := QrCodNivel1.Value;
        if GraGruX = 0 then
          GraGruX := Dmod.ObtemGraGruXDeGraGru1(Nivel1)
      end;
    end;
    else
    begin
      if QrCodFatorQtd.Value < 0.0000000001 then
      begin
        VAR_NOME_NOVO_GG1 := prod_xProd;
        GraGruEIts_GraGruX := QrCodGraGruX.Value;
        //
        Grade_Jan.MostraFormGraGruEIts(stUpd, CodInfoEmit,
          prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
          infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
          PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
          GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
        //
        if GraGruEIts_GraGruX <> 0 then
          GraGruX := GraGruEIts_GraGruX;
        //
        VAR_NOME_NOVO_GG1 := EmptyStr;
        //
        ReopenCod();
        //
        Nivel1 := QrCodNivel1.Value;
        if GraGruX = 0 then
          GraGruX := Dmod.ObtemGraGruXDeGraGru1(Nivel1)
      end else
      begin
        Nivel1 := QrCodNivel1.Value;
        GraGruX := Dmod.ObtemGraGruXDeGraGru1(Nivel1)
      end;
    end;
  end;
end;

function TDmNFe_0000.DefineEntidadePeloDoc(CNPJ, CPF: String;
  SomenteCliInt: Boolean = False): Integer;
var
  SQLCompl, Doc: String;
begin
  Result := 0;
  //
  Doc := Trim(CNPJ);
  //
  if (Doc <> '') and (Geral.SoNumero1a9_TT(Doc) <> '') then
  begin
    SQLCompl := 'WHERE ent.CNPJ="'+ Doc +'"';
  end else
  begin
    Doc := Trim(CPF);
    //
    if (Doc <> '') and (Geral.SoNumero1a9_TT(Doc) <> '') then
      SQLCompl := 'WHERE ent.CPF="'+ Doc +'"';
  end;
  if Doc = '' then
  begin
    Geral.MB_Aviso('Não foi definido nenhum documento (CNPJ ou CPF) para ' + 'localização da entidade!');
    Exit;
  end;
  //
  if SomenteCliInt then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEnt_Doc, Dmod.MyDB, [
      'SELECT ent.Codigo ',
      'FROM enticliint cli ',
      'LEFT JOIN entidades ent ON ent.Codigo = cli.CodEnti ',
      SQLCompl,
      'ORDER BY ent.Codigo ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEnt_Doc, Dmod.MyDB, [
      'SELECT ent.Codigo ',
      'FROM entidades ent ',
      SQLCompl,
      'ORDER BY ent.Codigo ',
      '']);
  end;
  Result := QrEnt_DocCodigo.Value;
end;

procedure TDmNFe_0000.DefineProperties(Filer: TFiler);  // Alexandria -> Tokyo
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmNFe_0000.DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
  procedure AlteraDest;
  var
    Dest: Integer;
  begin
    //
    if QrDest.State <> dsInactive then
      Dest := QrDestCodigo.Value
    else
      Dest := 0;
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Dest <> 0 then
        FmEntidade2.LocCod(Dest, Dest);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    if QrDest.State <> dsInactive then
    begin
      QrDest.Close;
      QrDest.Database := Dmod.MyDB; // 2020-08-31
      UnDmkDAC_PF.AbreQuery(QrDest, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    end;
  end;
var
  Msg1, Msg2: String;
  Cod: String;
begin
  Msg1 := MensagemDeID_NFe(Codigo, ID, Valor, '[Extra] ' + MsgExtra, 6);
  if FMsg = '' then FMsg := Msg1;

  //  Redirecionamento
  if Uppercase(ID[1]) = 'E' then // Emitente
  begin
    Cod := IntToStr(QrDestCodigo.Value);
    Msg2 := 'Desejo alterar agora o cadastro da entidade nº ' + Cod;
    MyObjects.MessageDlgCheck(Msg1, mtConfirmation, [mbOK], 0, mrOK,
      True, True, Msg2, @AlteraDest);
    FMsg := 'Exit';
  end;
  //
  Result := True;
end;

function TDmNFe_0000.DefX(Codigo, ID: String; Texto: String): String;
var
  t: Integer;
  OK: Boolean;
begin
  //Result := ValidaTexto_XML(Texto, Codigo, ID);
  Result := Texto;
  //
  if (QrNFeLayI.State = dsInactive) or (QrNFeLayI.RecordCount = 0) then
    ReopenNFeLayI();
  //
  if QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    t := Length(Result);
    OK := False;
    if (QrNFeLayIOcorMin.Value > 0) or (t > 0) then
    begin
      if QrNFeLayITamVar.Value <> '' then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT ' + FormatFloat('0', T) + ' in (' +
        QrNFeLayITamVar.Value + ') Tem');
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB, 'TDmNFe_0000.DefX()');
        OK := Dmod.QrAux.FieldByName('Tem').AsInteger = 1;
      end;
      if not OK then
      begin
        if t < QrNFeLayITamMin.Value then
          DefMsg(Codigo, ID, 'Tamanho do texto abaixo do mínimo!', Result);
        if t > QrNFeLayITamMax.Value then
        begin
          if QrNFeLayITamMin.Value = QrNFeLayITamMax.Value then
            DefMsg(Codigo, ID, 'Tamanho do texto difere do esperado!', Result)
          else
            Result := Copy(Result, 1, QrNFeLayITamMax.Value);
        end;
      end;
    end;
  end else DefMsg(Codigo, ID, 'Item não localizado na tabela "NFeLayI"', Texto);
end;

function TDmNFe_0000.EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
  chNFe: String): Integer;
var
  Msg: String;
begin
  Result := 0;
  Msg := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveIts, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM nfeevercab ',
  'WHERE EventoLote=' + Geral.FF0(EventoLote),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'AND nSeqEvento=' + Geral.FF0(nSeqEvento),
  'AND chNFe="' + chNFe + '"',
  '']);
  //
  case QrEveIts.RecordCount of
    0: Msg := 'Item de evento não localizado!';
    1: Result := QrEveItsControle.Value;
    else Msg := 'Item de evento duplicado!';
  end;
  if Msg <> '' then
    Geral.MensagemBox(Msg, 'ERRO', MB_OK+MB_ICONERROR);
end;

procedure TDmNFe_0000.EventoObtemSub(const Controle: Integer;
  const IdLote: String; var SubCtrl: Integer; var SQLType: TSQLType);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveSub, Dmod.MyDB, [
  'SELECT SubCtrl ',
  'FROM nfeeverret ',
  'WHERE Controle=' + Geral.FF0(Controle),
  'AND ret_Id="' + IdLote + '"',
  '']);
  //
  if QrEveSub.RecordCount = 0 then
  begin
    SQLType := stIns;
    SubCtrl := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverret', '', 0)
  end else begin
    SQLType := stUpd;
    SubCtrl := QrEveSubSubCtrl.Value;
  end;
end;

function TDmNFe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa,
tpEvento: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveTp, Dmod.MyDB, [
  'SELECT nSeqEvento ',
  'FROM nfeevercab ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'ORDER BY nSeqEvento DESC ',
  '']);
  if QrEveTp.RecordCount = 0 then
    Result := 1
  else
    Result := QrEveTpnSeqEvento.Value + 1;
end;

function TDmNFe_0000.ExcluiEntradaItsDeNFe(FatID, FatNum, Empresa, nItem,
  OriCtrl: Integer): Boolean;
begin
  Result := False;
  if OriCtrl <> 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND OriCtrl=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := OriCtrl;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  if (FatID <> 0) and (FatNum <> 0) and (Empresa <> 0) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitso WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsn WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsna WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsi WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
  end;
  //
  Result := True;
end;

function TDmNFe_0000.ExcluiNfe(Status, FatID, FatNum, Empresa: Integer;
DeleteDI, DeleteGA: Boolean): Boolean;
begin
  Result := False;
  if (FatID = 0) or (FatNum = 0) then Geral.MB_Aviso(
    'Dados insuficientes para criar/recriar NFe! FatID = ' + Geral.FF0(FatID) +
    ', FatNum = ' + Geral.FF0(FatNum) + '.')
  else if (Status > stepNFeAdedLote()) and (not NotaRejeitada(Status)) then
    Geral.MB_Aviso(
      'Nota fiscal não pode ser mais (excluída para ser) recriada! Status = ' +
      Geral.FF0(Status))
  else begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecaba WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabb WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabf WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabg WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    if DeleteGA then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfecabga WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabxreb WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabxvol WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabxlac WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecaby WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabzpro WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    // I T E N S
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsi WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    if DeleteDI then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsidi WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsidia WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsp WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsm WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsn WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsna WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitso WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsq WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsr WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitss WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitst WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsu WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsv WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsva WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsi80 WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Result := True;
  end;
end;

function TDmNFe_0000.InvalidaNFe(Status, FatID, FatNum, Empresa: Integer;
InvalidaDI, InvalidaGA: Boolean): Boolean;
begin
  Result := False;
  if (FatID = 0) or (FatNum = 0) then Geral.MB_Aviso(
    'Dados insuficientes para criar/recriar NFe! FatID = ' + Geral.FF0(FatID) +
    ', FatNum = ' + Geral.FF0(FatNum) + '.')
  else if (Status > stepNFeAdedLote()) and (not NotaRejeitada(Status)) then
    Geral.MB_Aviso('Nota fiscal não pode ser mais recriada! Status = ' +
      Geral.FF0(Status))
  else begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET FatNum=-FatNum, FatID=-FatID, IDCtrl=-IDCtrl WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabb SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabf SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabg SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    if InvalidaGA then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfecabga SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabxreb SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabxvol SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecabxlac SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfecaby SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    // I T E N S
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsi SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    if InvalidaDI then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeitsidi SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeitsidia SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeitsp SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsm SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsn SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsna SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitso SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsq SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsr SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitss SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitst SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsu SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfeitsv SET FatNum=-FatNum, FatID=-FatID WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    Result := True;
  end;
end;

function TDmNFe_0000.FormataID_NFe(Id: String): String;
begin
  Result :=
    Copy(Id, 01, 04) + ' ' +
    Copy(Id, 05, 04) + ' ' +
    Copy(Id, 09, 04) + ' ' +
    Copy(Id, 13, 04) + ' ' +
    Copy(Id, 17, 04) + ' ' +
    Copy(Id, 21, 04) + ' ' +
    Copy(Id, 25, 04) + ' ' +
    Copy(Id, 29, 04) + ' ' +
    Copy(Id, 33, 04) + ' ' +
    Copy(Id, 37, 04) + ' ' +
    Copy(Id, 41, 04);
end;

function TDmNFe_0000.FormataLoteNFe(Lote: Integer): String;
begin
  Result :=  FormatFloat('000000000', Lote);
end;

function TDmNFe_0000.FormataNSU_NFe(NSU: Int64): String;
begin
  Result :=  FormatFloat('000000000000000', NSU);
end;

(*
function TDmNFe_0000.InsereFaturaNFes(QrPrzX, QrPrzT, QrSumX: TmySQLQuery;
  CondicaoPG, AFP_Sit, EMP_FaturaNum, EMP_IDDuplicata, NumeroNF, EMP_FaturaSeq,
  TIPOCART, CartEmis, EMP_CtaFaturas, Empresa, NF_Emp_Numer, Represen: Integer;
  TotalNFe, AFP_Per: Double; EMP_TpDuplicata, EMP_FaturaSep, EMP_TxtFaturas,
  NF_Emp_Serie: String; DataFat: TDateTime): Boolean;
var
  TemAssociada: Boolean;
  V1, P1, F1: Double;
  Parcela: Integer;
  Duplicata, FaturaNum, NovaDup: String;
begin
  TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
  F1           := TotalNFe;
  //
  if TemAssociada then
    P1 := 100 - AFP_Per
  else
    P1 := 100;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrzX, Dmod.MyDB, [
    'SELECT Controle, Dias, Percent1 Percent ',
    'FROM pediprzits',
    'WHERE Percent1 > 0',
    'AND Codigo=' + Geral.FF0(CondicaoPG),
    'ORDER BY Dias',
    '']);
  QrPrzX.First;
  while not QrPrzX.Eof do
  begin
    if QrPrzX.RecordCount = QrPrzX.RecNo then
      V1 := TotalNFe
    else
    begin
      if P1 = 0 then
        V1 := 0
      else
        V1 := (Round(TotalNFe * (QrPrzX.FieldByName('Percent').AsFloat / P1 * 100))) / 100;
      F1 := F1 - V1;
    end;
    QrPrzT.Locate('Controle', QrPrzX.FieldByName('Controle').AsInteger, []);
    Parcela := QrPrzT.RecNo;
    //
    if EMP_FaturaNum = 0 then
      FaturaNum := Geral.FF0(EMP_IDDuplicata)
    else
      FaturaNum := Geral.FF0(NumeroNF);

    Duplicata := EMP_TpDuplicata + FaturaNum + EMP_FaturaSep +
                 dmkPF.ParcelaFatura(QrPrzX.RecNo, EMP_FaturaSeq);
    // 2011-08-21
    // Teste para substituir no futuro (uso no form FmFatDivCms)
    NovaDup := DmProd.MontaDuplicata(EMP_IDDuplicata, NumeroNF, Parcela,
                 EMP_FaturaNum, EMP_FaturaSeq, EMP_TpDuplicata, EMP_FaturaSep);
    //
    if NovaDup <> Duplicata then
      Geral.MB_Aviso('Definição da duplicata:' + sLineBreak + Duplicata +
        ' <> ' + NovaDup);
    // Fim 2011-08-21
    IncluiLancto(V1, DataFat, DataFat + QrPrzX.FieldByName('Dias').AsInteger,
      Duplicata, EMP_TxtFaturas, TIPOCART, CartEmis, EMP_CtaFaturas, Empresa,
      Parcela, NF_Emp_Numer, Represen, NF_Emp_Serie, True);
    //
    QrPrzX.Next;
  end;

  // Faturamento associada
  if TemAssociada then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumX, Dmod.MyDB, [
      'SELECT SUM(Total) Total',
      'FROM stqmovvala',
      'WHERE Tipo='  + Geral.FF0(F_Tipo));
      'AND OriCodi=' + Geral.FF0(F_OriCodi));
      'AND Empresa=' + Geral.FF0(F_Associada));
    //
    /Configurar o inserelançamento da janela NFaEdit para recriar os lançamentos com notas de desconto
    T2 := QrSumXTotal.Value;
    F2 := T2;
    //
    QrPrzX.Close;
    QrPrzX.SQL.Clear;
    QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
    QrPrzX.SQL.Add('FROM pediprzits');
    QrPrzX.SQL.Add('WHERE Percent2 > 0');
    QrPrzX.SQL.Add('AND Codigo=:P0');
    QrPrzX.SQL.Add('ORDER BY Dias');
    QrPrzX.Params[00].AsInteger := F_CondicaoPG;
    UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
    QrPrzX.First;
    while not QrPrzX.Eof do
    begin
      if QrPrzX.RecordCount = QrPrzX.RecNo then
        V2 := F2
      else begin
        if P2 = 0 then V2 := 0 else
          V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
        F2 := F2 - V2;
      end;
      QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
      Parcela := QrPrzT.RecNo;
      if F_EMP_FaturaNum = 0 then
        FaturaNum := FormatFloat('000000', F_ASS_IDDuplicata)
      else
        FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
      Duplicata := F_ASS_TpDuplicata + FormatFloat('000000', F_ASS_IDDuplicata) +
        F_ASS_FaturaSep + dmkPF.ParcelaFatura(
        QrPrzX.RecNo, F_ASS_FaturaSeq);
      IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
        F_ASS_TxtFaturas, TIPOCART,
        F_CartEmis, F_ASS_CtaFaturas,
        F_Associada, Parcela, NF_Ass_Numer,
        F_Represen, NF_Ass_Serie, False);
      //
      QrPrzX.Next;
    end;
  end;
end;
*)

procedure TDmNFe_0000.AtualizaLctoFaturaNFe(FatID, FatNum, Empresa: Integer);
var
  Campo, TabLctA: String;
  Lancto, Filial, Financeiro, CondicaoPg, Controle: Integer;
  T1, F1, V1: Double;
begin
  //Tabela com os lançamentos
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT cab.ICMSTot_vNF, cay.Lancto, cin.CodFilial, ',
    'pvd.CondicaoPg, frc.Financeiro, cay.Controle ',
    'FROM nfecaby cay ',
    'LEFT JOIN pedivda pvd ON pvd.Codigo = cay.FatNum ',
    'LEFT JOIN nfecaba cab ON (cab.FatID = cay.FatID AND cay.FatNum = cab.FatNum AND cay.Empresa = cab.Empresa) ',
    'LEFT JOIN enticliint cin ON cin.CodEnti = cay.Empresa ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo = cab.FisRegCad ',
    'WHERE cay.FatID=' + Geral.FF0(FatID),
    'AND cay.FatNum=' + Geral.FF0(FatNum),
    'AND cay.Empresa=' + Geral.FF0(Empresa),
    '']);
  if (QrLoc.RecordCount > 0) then
  begin
    CondicaoPg := QrLoc.FieldByName('CondicaoPg').AsInteger;
    Financeiro := QrLoc.FieldByName('Financeiro').AsInteger;
    //
    //Tabela com o porcentagem da condição de pageamento
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
      'SELECT Controle, Dias, Percent1 Percent ',
      'FROM pediprzits ',
      'WHERE Percent1 > 0 ',
      'AND Codigo=' + Geral.FF0(CondicaoPg),
      'ORDER BY Dias ',
      '']);
    if (QrLoc.RecordCount = QrLoc2.RecordCount) then
    begin
      //Ajustar somente da empresa correta a assossiada não tem desconto
      T1 := QrLoc.FieldByName('ICMSTot_vNF').AsFloat;
      F1 := T1;
      //
      QrLoc.First;
      QrLoc2.First;
      //
      while not QrLoc2.Eof do
      begin
        if QrLoc2.RecordCount = QrLoc2.RecNo then
          V1 := F1
        else begin
          V1 := (Round(T1 * QrLoc2.FieldByName('Percent').AsFloat)) / 100;
          F1 := F1 - V1;
        end;
        Filial   := QrLoc.FieldByName('CodFilial').AsInteger;
        Lancto   := QrLoc.FieldByName('Lancto').AsInteger;
        Controle := QrLoc.FieldByName('Controle').AsInteger;
        //
        if Financeiro = 1 then
          Campo := 'Credito'
        else
        if Financeiro = 2 then
          Campo := 'Debito'
        else
          Campo := '';
        //
        if Campo <> '' then
        begin
          {$IfDef DEFINE_VARLCT}
            TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
          {$Else}
            TabLctA := VAR_LCT;
          {$ENdIf}
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False,
            [Campo], ['Controle'], [V1], [Lancto], True);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaby', False,
            ['vDup'], ['Controle'], [V1], [Controle], True);
        end;
        QrLoc.Next;
        QrLoc2.Next;
      end;
    end;
  end;
end;

function TDmNFe_0000.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa,
  Cliente, Associada, RegrFiscal, GraGruX: Integer; NO_tabelaPrc,
  NO_TbFatPedFis: String; StqCenCad, FatSemEstq, AFP_Sit: Integer; AFP_Per,
  Qtde: Double; Cli_Tipo: Integer; Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer; Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA,
  Preco_MedidaE: Double; Preco_MedOrdem: Integer; SQLType: TSQLType; PediVda,
  OriPart, InfAdCuztm: Integer; TipoNF, modNF, Serie, nNF, SitDevolu,
  Servico: Integer; refNFe, refNFeSig: String; TotalPreCalc: Double; var OriCtrl: Integer;
  Pecas, AreaM2, AreaP2, Peso: Double; TipoCalc, prod_indTot: Integer;
  IDCtrl_PreDef: Integer): Boolean;

  procedure AlteraCliente(Cliente: Integer; QrCli: TmySQLQuery);
  begin
    DModG.CadastroDeEntidade(Cliente, fmcadEntidade2, fmcadEntidade2);
    if QrCli <> nil then
    begin
      QrCli.Close;
      UnDmkDAC_PF.AbreQuery(QrCli, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    end;
  end;

var
  DataHora, Msg: String;
  ID, IDCtrl, AlterWeb, Ativo,
  Continua: Integer;
  Sdo, Falta, Preco, Total, PercCustom: Double;
  //
  CFOP_Contrib,
  CFOP_MesmaUF,
  CFOP_Proprio, CFOP_SubsTrib: Integer;
  CFOP_Emp, CFOP_Ass: String;
  TemAssociada: Boolean;
  Campo: String;
  MedidaC, MedidaL, MedidaA, MedidaE: Double;
  MedOrdem: Integer;
  //
  EhServico: Boolean;
  ICMS_Aliq, Qtd: Double;
  Baixa, Fator: Integer;
  EhSubsTrib: Boolean;
begin
  Screen.Cursor := crHourGlass;
  OriCtrl := 0;
  Result  := False;
  //MedidaC := 0;
  //
  TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
  //
  if not (SQLType in [stIns, stUpd]) then
  begin
    Geral.MensagemBox('Status da janela sem ação definida!' + sLineBreak +
      'INFORME A DERMATEK', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (GraGruX = 0) and (Servico = 0) then
  begin
    Geral.MensagemBox('Reduzido não definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;

  // CFOP Empresa principal
  EhServico  := Servico <> 0;
  DmProd.ReopenProd(GraGruX);
  EhSubsTrib := DmProd.QrProdUsaSubsTrib.Value = 1;
  //
  if not ObtemNumeroCFOP_Emissao(NO_TbFatPedFis, [OriCodi, OriCnta, GraGruX], Empresa,
    Cliente, Item_MadeBy, RegrFiscal, EhServico, EhSubsTrib, CFOP_Emp, ICMS_Aliq,
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib)
  then
      Exit;
  // CFOP Empresa associada
  if TemAssociada then
  begin
    if ASS_CO_UF = 0 then
    begin
       Geral.MB_Aviso('UF da empresa ' + Geral.FF0(ASS_FILIAL) + ' não definida!');
       Screen.Cursor := crDefault;
       Exit;
    end;
    if not ObtemNumeroCFOP_Emissao(NO_TbFatPedFis, [OriCodi, OriCnta, GraGruX], Empresa,
      Cliente, Item_MadeBy, RegrFiscal, EhServico, EhSubsTrib, CFOP_Ass, ICMS_Aliq,
      CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib)
    then
        Exit;
  end;
  //
  //  dados gerais
  DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  AlterWeb  := 1;
  Ativo     := 0;
  //
  case TipoCalc of
      1: Fator :=  1;
      2: Fator := -1;
    else Fator :=  0;
  end;
  Qtd := Qtde * Fator;
  //
  Continua := DmProd.VerificaEstoqueProduto(Qtd, Fator, GraGruX, StqCenCad,
                RegrFiscal, OriCodi, OriCnta, Empresa, FatSemEstq, False, Msg);
  //
  if Continua = ID_YES then
  begin
    MedidaC    := Preco_MedidaC;
    MedidaL    := Preco_MedidaL;
    MedidaA    := Preco_MedidaA;
    MedidaE    := Preco_MedidaE;
    MedOrdem   := Preco_MedOrdem;
    //
    Campo     := 'stqmov' + FormatFloat('000', NFe_FatID);
    OriCtrl   := DModG.BuscaProximoCodigoInt('controle', Campo(*'stqmov001'*), '', 0);
    //
    case TipoCalc of
      0: Baixa :=   0; //Nada
      1: Baixa :=   1; //Adiciona
      2: Baixa :=  -1; //Subtrai
      else begin
        Geral.MensagemBox('Tipo de cálculo de estoque não definido!',
        'ERRO!', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    if IDCtrl_PreDef = 0 then
      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0)
    else
      IDCtrl := IDCtrl_PreDef;

    //  Não perder tempo com pesquisa de ID ?
    if (SQLType = stUpd) and (IDCtrl_PreDef <> 0) then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala ');
      Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[0].AsInteger := IDCtrl_PreDef;
      Dmod.QrUpd.ExecSQL;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
    'DataHora', 'OriCnta', 'Empresa',
    'StqCenCad', 'GraGruX', 'Qtde',
    'OriPart',
    'Pecas', 'AreaM2', 'AreaP2', 'Peso',
    'Baixa', 'AlterWeb', 'Ativo',
    'Tipo', 'OriCodi', 'OriCtrl'
    ], ['IDCtrl'
    ], [
    DataHora, OriCnta, Empresa,
    StqCenCad, GraGruX, Qtde,
    OriPart,
    Pecas, AreaM2, AreaP2, Peso,
    Baixa, AlterWeb, Ativo,
    NFe_FatID, OriCodi, OriCtrl
    ], [IDCtrl
    ], False) then
    begin
      if TemAssociada then
      begin
        Preco := MLAGeral.FFF(Preco_PrecoF * AFP_Per / 100,
          Dmod.QrControleCasasProd.Value, siNegativo);
        PercCustom := Preco_PercCustom;
        Associada  := Associada;
        if TotalPreCalc >= 0.01 then
          Total := TotalPreCalc
        else
          Total      := Qtde * Preco;
        //
        ID := UMyMod.BuscaEmLivreY_Def('stqmovvala', 'ID', (*SQLType*)stIns, 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, (*SQLType*)stIns, 'stqmovvala', False, [
        'Tipo', 'OriCodi', 'OriCtrl', 'OriCnta', 'Empresa',
        'StqCenCad', 'GraGruX', 'Qtde', 'Preco', 'Total',
        'CFOP', 'IDCtrl', 'SeqInReduz', 'InfAdCuztm', 'PercCustom', 'MedidaC',
        'MedidaL', 'MedidaA', 'MedidaE', 'MedOrdem',
        'TipoNF', 'refNFe',
        'refNFeSig', // 2023-06-09
        'modNF', 'Serie', 'nNF', 'SitDevolu', 'Servico',
        'CFOP_Contrib', 'CFOP_MesmaUF', 'CFOP_Proprio', 'prod_indTot'
        ], ['ID'], [
        NFe_FatID, OriCodi, OriCtrl, OriCnta, ASSOCIADA, // <- CUIDADO!!! Não trocar por empresa!
        StqCenCad, GraGruX, Qtde, Preco, Total,
        CFOP_Ass, IDCtrl, 0, InfAdCuztm, PercCustom, MedidaC,
        MedidaL, MedidaA, MedidaE, MedOrdem,
        TipoNF, refNFe,
        refNfeSig,  // 2023-06-09
        modNF, Serie, nNF, SitDevolu, Servico,
        CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, prod_indTot
      ], [ID], False);
      end else Preco := 0;
      //
      PercCustom := Preco_PercCustom;
      Preco      := Preco_PrecoF - Preco;
      Total      := Qtde * Preco;
      //
      ID := UMyMod.BuscaEmLivreY_Def('stqmovvala', 'ID', (*SQLType*)stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, (*SQLType*)stIns, 'stqmovvala', False, [
      'Tipo', 'OriCodi', 'OriCtrl', 'OriCnta', 'Empresa',
      'StqCenCad', 'GraGruX', 'Qtde', 'Preco', 'Total',
      'CFOP', 'IDCtrl', 'SeqInReduz', 'InfAdCuztm', 'PercCustom', 'MedidaC',
      'MedidaL', 'MedidaA', 'MedidaE', 'MedOrdem',
      'TipoNF', 'refNFe',
      'refNfeSig', // 2023-06-09
      'modNF', 'Serie', 'nNF', 'SitDevolu', 'Servico',
      'CFOP_Contrib', 'CFOP_MesmaUF', 'CFOP_Proprio', 'prod_indTot'
      ], ['ID'], [
      NFe_FatID, OriCodi, OriCtrl, OriCnta, Empresa,
      StqCenCad, GraGruX, Qtde, Preco, Total,
      CFOP_Emp, IDCtrl, 1, InfAdCuztm, PercCustom, MedidaC,
      MedidaL, MedidaA, MedidaE, MedOrdem,
      TipoNF, refNFe,
      refNfeSig, // 2023-06-09
      modNF, Serie, nNF, SitDevolu, Servico,
      CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, prod_indTot
      ], [ID], False) then
     {$IFNDef semPediVda}DmPediVda.AtualizaUmItemPediVda(OriPart){$EndIf};
    end;
  end;
  Result := OriCtrl <> 0;
  Screen.Cursor := crDefault;
end;

function TDmNFe_0000.InsUpdNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
  procedure InsereAtual();
  var
    refNFe, refNFeSig, refNF_CNPJ, refNFP_CNPJ, refNFP_CPF, refNFP_IE, refCTe,
    refECF_mod: String;
    FatID, FatNum, Empresa, Controle, refNF_cUF, refNF_AAMM, refNF_mod,
    refNF_serie, refNF_nNF, refNFP_cUF, refNFP_AAMM, refNFP_mod, refNFP_serie,
    refNFP_nNF, refECF_nECF, refECF_nCOO, QualNFref: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    FatID          := QrStqMovNFsRefFatID      .Value;
    FatNum         := QrStqMovNFsRefFatNum     .Value;
    Empresa        := QrStqMovNFsRefEmpresa    .Value;
    Controle       := QrStqMovNFsRefControle   .Value;
    refNFe         := QrStqMovNFsRefrefNFe     .Value;
    refNFeSig      := QrStqMovNFsRefrefNFeSig  .Value; // 2023-06-09
    refNF_cUF      := QrStqMovNFsRefrefNF_cUF  .Value;
    refNF_AAMM     := QrStqMovNFsRefrefNF_AAMM .Value;
    refNF_CNPJ     := QrStqMovNFsRefrefNF_CNPJ .Value;
    refNF_mod      := QrStqMovNFsRefrefNF_mod  .Value;
    refNF_serie    := QrStqMovNFsRefrefNF_serie.Value;
    refNF_nNF      := QrStqMovNFsRefrefNF_nNF  .Value;
    refNFP_cUF     := QrStqMovNFsRefrefNFP_cUF .Value;
    refNFP_AAMM    := QrStqMovNFsRefrefNFP_AAMM.Value;
    refNFP_CNPJ    := QrStqMovNFsRefrefNFP_CNPJ.Value;
    refNFP_CPF     := QrStqMovNFsRefrefNFP_CPF .Value;
    refNFP_IE      := QrStqMovNFsRefrefNFP_IE  .Value;
    refNFP_mod     := QrStqMovNFsRefrefNFP_mod .Value;
    refNFP_serie   := QrStqMovNFsRefrefNFP_serie.Value;
    refNFP_nNF     := QrStqMovNFsRefrefNFP_nNF .Value;
    refCTe         := QrStqMovNFsRefrefCTe     .Value;
    refECF_mod     := QrStqMovNFsRefrefECF_mod .Value;
    refECF_nECF    := QrStqMovNFsRefrefECF_nECF.Value;
    refECF_nCOO    := QrStqMovNFsRefrefECF_nCOO.Value;
    QualNFref      := QrStqMovNFsRefQualNFref  .Value;

    //
    Controle := DModG.BuscaProximoInteiro('nfecabb', 'Controle', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabb', False, [
    'refNFe',
    'refNFeSig', // 2023-06-09
    'refNF_cUF', 'refNF_AAMM',
    'refNF_CNPJ', 'refNF_mod', 'refNF_serie',
    'refNF_nNF', 'refNFP_cUF', 'refNFP_AAMM',
    'refNFP_CNPJ', 'refNFP_CPF', 'refNFP_IE',
    'refNFP_mod', 'refNFP_serie', 'refNFP_nNF',
    'refCTe', 'refECF_mod', 'refECF_nECF',
    'refECF_nCOO', 'QualNFref'], [
    'FatID', 'FatNum', 'Empresa', 'Controle'], [
    refNFe,
    refNFeSig, // 2023-06-09
    refNF_cUF, refNF_AAMM,
    refNF_CNPJ, refNF_mod, refNF_serie,
    refNF_nNF, refNFP_cUF, refNFP_AAMM,
    refNFP_CNPJ, refNFP_CPF, refNFP_IE,
    refNFP_mod, refNFP_serie, refNFP_nNF,
    refCTe, refECF_mod, refECF_nECF,
    refECF_nCOO, QualNFref], [
    FatID, FatNum, Empresa, Controle], True) then
    begin
      //
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovNFsRef, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqmovnfsref ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  if QrStqMovNFsRef.RecordCount > 0 then
  begin
    QrStqMovNFsRef.First;
    while not QrStqMovNFsRef.Eof do
    begin
      InsereAtual();
      //
      QrStqMovNFsRef.Next;
    end;
  end;
  //
  Result := True;
end;

function TDmNFe_0000.InsUpdNFeCabF(FatID, FatNum, Empresa, RetiradaUsa,
  RetiradaEnti: Integer; LaAviso1, LaAviso2: TLabel;
  REWarning: TRichEdit): Boolean;
const
  sProcName = 'TDmNFe_0000.InsUpdNFeCabF()';
var
  retirada_CNPJ, retirada_xLgr, retirada_nro, retirada_xCpl, retirada_xBairro,
  retirada_xMun, retirada_UF, retirada_CPF, retirada_xNome, retirada_xPais,
  retirada_fone, retirada_email, retirada_IE: String;
  retirada_cMun, retirada_CEP, retirada_cPais: Integer;
  SQLType: TSQLType;
  //
  function Endereco(): String;
  begin
    Result := '';
    if Trim(QrRetiradaEntiNOMELOGRAD.Value) <> '' then
      Result := Trim(QrRetiradaEntiNOMELOGRAD.Value) + ' ';
    //
    Result := Result + Trim(QrRetiradaEntiRUA.Value);
  end;
begin
  Result := False;
  if (RetiradaUsa = 0) and (RetiradaEnti = 0) then
  begin
    Result := True;
    Exit;
  end else
  if (RetiradaUsa >= 1) and (RetiradaEnti <> 0) then
  begin
    SQLType        := stIns; //ImgTipo.SQLType?;
    //FatID          := ;
    //FatNum         := ;
    //Empresa        := ;
    retirada_CNPJ    := '';
    retirada_CPF     := '';
    retirada_xNome   := '';
    retirada_xLgr    := '';
    retirada_nro     := '';
    retirada_xCpl    := '';
    retirada_xBairro := '';
    retirada_cMun    := 0;
    retirada_xMun    := '';
    retirada_UF      := '';
    retirada_CEP     := 0;
    retirada_cPais   := 0;
    retirada_xPais   := '';
    retirada_fone    := '';
    retirada_email   := '';
    retirada_IE      := '';
    DModG.ReopenEndereco3(QrRetiradaEnti, RetiradaEnti);
    if QrRetiradaEnti.RecordCount = 1 then
    begin
      retirada_xNome   := QrRetiradaEntiNOME_ENT.Value;
      retirada_xLgr    := Endereco();
      retirada_nro     := Geral.FF0(Trunc(QrRetiradaEntiNUMERO.Value));
      retirada_xCpl    := QrRetiradaEntiCOMPL.Value;
      retirada_xBairro := QrRetiradaEntiBAIRRO.Value;
      retirada_cMun    := Trunc(QrRetiradaEntiCODMUNICI.Value);
      retirada_xMun    := QrRetiradaEntiCIDADE.Value;
      retirada_UF      := QrRetiradaEntiNOMEUF.Value;
      retirada_CEP     := Trunc(QrRetiradaEntiCEP.Value);
      retirada_cPais   := Trunc(QrRetiradaEntiCODPAIS.Value);
      retirada_xPais   := QrRetiradaEntiPais.Value;
      retirada_fone    := Geral.SoNumero_TT(QrRetiradaEntiTE1.Value);
      retirada_email   := QrRetiradaEntiEMAIL.Value;
      case QrRetiradaEntiTipo.Value of
        0:
        begin
          retirada_CNPJ    := Geral.SoNumero_TT(QrRetiradaEntiCNPJ_CPF.Value);
          retirada_IE      := Geral.SoNumero_TT(QrRetiradaEntiIE.Value);
        end;
        1:
        begin
          retirada_CPF     := Geral.SoNumero_TT(QrRetiradaEntiCNPJ_CPF.Value);
        end;
      end;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabf', False, [
      'retirada_CNPJ', 'retirada_xLgr', 'retirada_nro',
      'retirada_xCpl', 'retirada_xBairro', 'retirada_cMun',
      'retirada_xMun', 'retirada_UF', 'retirada_CPF',
      'retirada_xNome', 'retirada_CEP', 'retirada_cPais',
      'retirada_xPais', 'retirada_fone', 'retirada_email',
      'retirada_IE'], [
      'FatID', 'FatNum', 'Empresa'], [
      retirada_CNPJ, retirada_xLgr, retirada_nro,
      retirada_xCpl, retirada_xBairro, retirada_cMun,
      retirada_xMun, retirada_UF, retirada_CPF,
      retirada_xNome, retirada_CEP, retirada_cPais,
      retirada_xPais, retirada_fone, retirada_email,
      retirada_IE], [
      FatID, FatNum, Empresa], True);
    end;
  end;
  if not Result then
    Geral.MB_Erro(
    'Não foi possível incluir endereço de retirada!' + sLinebreak +
    'FatID: ' + Geral.FF0(FatID) + sLinebreak +
    'FatNum: ' + Geral.FF0(FatNum) + sLinebreak +
    'Empresa: ' + Geral.FF0(Empresa) + sLinebreak +
    'RetiradaUsa: ' + Geral.FF0(RetiradaUsa) + sLinebreak +
    'RetiradaEnti: ' + Geral.FF0(RetiradaEnti) + sLinebreak +
    '' + sLinebreak +
    sProcName);
end;

function TDmNFe_0000.InsUpdNFeCabG(FatID, FatNum, Empresa, EntregaUsa,
  EntregaEnti, L_Ativo, Cliente: Integer; LaAviso1, LaAviso2: TLabel;
  REWarning: TRichEdit): Boolean;
const
  sProcName = 'TDmNFe_0000.InsUpdNFeCabG()';
var
  entrega_CNPJ, entrega_xLgr, entrega_nro, entrega_xCpl, entrega_xBairro,
  entrega_xMun, entrega_UF, entrega_CPF, entrega_xNome, entrega_xPais,
  entrega_fone, entrega_email, entrega_IE: String;
  entrega_cMun, entrega_CEP, entrega_cPais: Integer;
  SQLType: TSQLType;
  //
  function Endereco3(): String;
  begin
    Result := '';
    if Trim(QrEntregaEntiNOMELOGRAD.Value) <> '' then
      Result := Trim(QrEntregaEntiNOMELOGRAD.Value) + ' ';
    //
    Result := Result + Trim(QrEntregaEntiRUA.Value);
  end;
  function Endereco4(): String;
  begin
    Result := '';
    if Trim(QrEntiEntregaNO_LLograd.Value) <> '' then
      Result := Trim(QrEntiEntregaNO_LLograd.Value) + ' ';
    //
    Result := Result + Trim(QrEntiEntregaLRua.Value);
  end;
begin
  Result := False;
  if (EntregaUsa = 0) and (EntregaEnti = 0) and (L_Ativo = 0) then
  begin
    Result := True;
    Exit;
  end else
  if (EntregaUsa >= 1) and (EntregaEnti <> 0) then
  begin
    SQLType        := stIns; //ImgTipo.SQLType?;
    //FatID          := ;
    //FatNum         := ;
    //Empresa        := ;
    entrega_CNPJ    := '';
    entrega_CPF     := '';
    entrega_xNome   := '';
    entrega_xLgr    := '';
    entrega_nro     := '';
    entrega_xCpl    := '';
    entrega_xBairro := '';
    entrega_cMun    := 0;
    entrega_xMun    := '';
    entrega_UF      := '';
    entrega_CEP     := 0;
    entrega_cPais   := 0;
    entrega_xPais   := '';
    entrega_fone    := '';
    entrega_email   := '';
    entrega_IE      := '';
    DModG.ReopenEndereco3(QrEntregaEnti, EntregaEnti);
    if QrEntregaEnti.RecordCount = 1 then
    begin
      entrega_xNome   := QrEntregaEntiNOME_ENT.Value;
      entrega_xLgr    := Endereco3();
      entrega_nro     := Geral.FF0(Trunc(QrEntregaEntiNUMERO.Value));
      entrega_xCpl    := QrEntregaEntiCOMPL.Value;
      entrega_xBairro := QrEntregaEntiBAIRRO.Value;
      entrega_cMun    := Trunc(QrEntregaEntiCODMUNICI.Value);
      entrega_xMun    := QrEntregaEntiCIDADE.Value;
      entrega_UF      := QrEntregaEntiNOMEUF.Value;
      entrega_CEP     := Trunc(QrEntregaEntiCEP.Value);
      entrega_cPais   := Trunc(QrEntregaEntiCODPAIS.Value);
      entrega_xPais   := QrEntregaEntiPais.Value;
      entrega_fone    := Geral.SoNumero_TT(QrEntregaEntiTE1.Value);
      entrega_email   := QrEntregaEntiEMAIL.Value;
      case QrEntregaEntiTipo.Value of
        0:
        begin
          entrega_CNPJ    := Geral.SoNumero_TT(QrEntregaEntiCNPJ_CPF.Value);
          entrega_IE      := Geral.SoNumero_TT(QrEntregaEntiIE.Value);
        end;
        1:
        begin
          entrega_CPF     := Geral.SoNumero_TT(QrEntregaEntiCNPJ_CPF.Value);
        end;
      end;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabg', False, [
      'entrega_CNPJ', 'entrega_xLgr', 'entrega_nro',
      'entrega_xCpl', 'entrega_xBairro', 'entrega_cMun',
      'entrega_xMun', 'entrega_UF', 'entrega_CPF',
      'entrega_xNome', 'entrega_CEP', 'entrega_cPais',
      'entrega_xPais', 'entrega_fone', 'entrega_email',
      'entrega_IE'], [
      'FatID', 'FatNum', 'Empresa'], [
      entrega_CNPJ, entrega_xLgr, entrega_nro,
      entrega_xCpl, entrega_xBairro, entrega_cMun,
      entrega_xMun, entrega_UF, entrega_CPF,
      entrega_xNome, entrega_CEP, entrega_cPais,
      entrega_xPais, entrega_fone, entrega_email,
      entrega_IE], [
      FatID, FatNum, Empresa], True);
    end;
  end else
  if L_Ativo > 0 then
  begin
    DModG.ReopenEndereco4(QrEntiEntrega, Cliente);
    if QrEntiEntrega.RecordCount > 0 then
    begin
      entrega_xNome   := QrEntiEntregaL_Nome.Value;
      entrega_xLgr    := Endereco4();
      entrega_nro     := Geral.FF0(QrEntiEntregaLNumero.Value);
      entrega_xCpl    := QrEntiEntregaLCompl.Value;
      entrega_xBairro := QrEntiEntregaLBairro.Value;
      entrega_cMun    := Trunc(QrEntiEntregaLCodMunici.Value);
      entrega_xMun    := QrEntiEntregaxMunicipio.Value;
      entrega_UF      := QrEntiEntregaNO_LUF.Value;
      entrega_CEP     := QrEntiEntregaLCEP.Value;
      entrega_cPais   := Trunc(QrEntiEntregaLCodiPais.Value);
      entrega_xPais   := QrEntiEntregaxPais.Value;
      entrega_fone    := Geral.SoNumero_TT(QrEntiEntregaLTel.Value);
      entrega_email   := QrEntiEntregaLEmail.Value;
      if Geral.SoNumero_TT(QrEntiEntregaL_CNPJ.Value) <> '' then
      begin
        entrega_CNPJ    := Geral.SoNumero_TT(QrEntiEntregaL_CNPJ.Value);
        entrega_IE      := Geral.SoNumero_TT(QrEntiEntregaL_IE.Value);
      end else
      begin
        entrega_CPF     := Geral.SoNumero_TT(QrEntiEntregaL_CPF.Value);
      end;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabg', False, [
    'entrega_CNPJ', 'entrega_xLgr', 'entrega_nro',
    'entrega_xCpl', 'entrega_xBairro', 'entrega_cMun',
    'entrega_xMun', 'entrega_UF', 'entrega_CPF',
    'entrega_xNome', 'entrega_CEP', 'entrega_cPais',
    'entrega_xPais', 'entrega_fone', 'entrega_email',
    'entrega_IE'], [
    'FatID', 'FatNum', 'Empresa'], [
    entrega_CNPJ, entrega_xLgr, entrega_nro,
    entrega_xCpl, entrega_xBairro, entrega_cMun,
    entrega_xMun, entrega_UF, entrega_CPF,
    entrega_xNome, entrega_CEP, entrega_cPais,
    entrega_xPais, entrega_fone, entrega_email,
    entrega_IE], [
    FatID, FatNum, Empresa], True);
  end;
  //
  if not Result then
    Geral.MB_Erro(
    'Não foi possível incluir endereço de entrega!' + sLinebreak +
    'FatID: ' + Geral.FF0(FatID) + sLinebreak +
    'FatNum: ' + Geral.FF0(FatNum) + sLinebreak +
    'Empresa: ' + Geral.FF0(Empresa) + sLinebreak +
    'EntregaUsa: ' + Geral.FF0(EntregaUsa) + sLinebreak +
    'EntregaEnti: ' + Geral.FF0(EntregaEnti) + sLinebreak +
    '' + sLinebreak +
    sProcName);
end;

function TDmNFe_0000.InsUpdNFeCabXVol(FatID, FatNum, Empresa: Integer;
  SQL_VOLUMES: String; LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
var
  Controle: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados de volumes');
  if Trim(SQL_VOLUMES) <> '' then
  begin
    QrVolumes.Close;
    QrVolumes.SQL.Clear;
    QrVolumes.SQL.Add(SQL_VOLUMES);
    UMyMod.AbreQuery(QrVolumes, Dmod.MyDB, 'TDmNFe_0000.InsUpdNFeCabXVol()');
    //
    if QrVolumes.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo dados de volumes');
      while not QrVolumes.Eof do
      begin
        if QrVolumesqVol.Value <> EmptyStr then
        begin
          Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxvol', '', 0);
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
            'qVol', 'esp', 'marca',
            'nVol', 'pesoL', 'pesoB'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            QrVolumesqVol.Value, QrVolumesesp.Value, QrVolumesmarca.Value,
            QrVolumesnVol.Value, QrVolumespesoL.Value, QrVolumespesoB.Value], [
            FatID, FatNum, Empresa, Controle], True);
          //
        end;
        QrVolumes.Next;
      end;
    end;
  end else REWarning.Text :=
  'Não há informação de volumes, espécie, marca, número kg líq. e bruto dos volumes transportados!'
  + sLineBreak + ReWarning.Text;
  Result := True;
end;

function TDmNFe_0000.InsUpdNFeCabY(FatID, FatNum, Empresa: Integer;
  SQL_FAT_ITS: String; LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
var
  FatParcela, Controle, Lancto: Integer;
  nDup: String;
  dVenc: TDateTime;
  vDup: Double;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados de faturamento');
  if Trim(SQL_FAT_ITS) <> '' then
  begin
    QrFatYIts.Close;
    QrFatYIts.SQL.Clear;
    QrFatYIts.SQL.Add(SQL_FAT_ITS);
    UMyMod.AbreQuery(QrFatYIts, Dmod.MyDB, 'TDmNFe_0000.InsUpdNFeCabY()');
    //
    if QrFatYIts.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo dados de faturamento');
      try
        while not QrFatYIts.Eof do
        begin
          Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
          nDup     := QrFatYItsDuplicata.Value;
          dVenc    := QrFatYItsVencimento.Value;
          vDup     := QrFatYItsValor.Value;

          // Mudado em 2013-02-12
  {        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaby', False, [
          'nDup', 'dVenc', 'vDup'], [
          'FatID', 'FatNum', 'Empresa', 'Controle'], [
          nDup, dVenc, vDup], [
          FatID, FatNum, Empresa, Controle], True);}
          FatParcela := QrFatYIts.RecNo;
          Lancto     := QrFatYItsControle.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaby', False, [
          'nDup', 'dVenc', 'vDup', 'Lancto'], [
          'FatID', 'FatNum', 'FatParcela',
          'Empresa', 'Controle'], [
          nDup, dVenc, vDup, Lancto], [
          FatID, FatNum, FatParcela,
          Empresa, Controle], True);
          // FIM Mudado em 2013-02-12
          //
          QrFatYIts.Next;
        end;
      finally
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
    end;
  end else
    REWarning.Text := 'Não há informação de fatura / duplicata!' + sLineBreak +
      ReWarning.Text;
  Result := True;
end;

function TDmNFe_0000.InsUpdNFeIts(FatID, FatNum, Empresa, ICMS_Usa, IPI_Usa,
  II_Usa, PIS_Usa, COFINS_Usa, ISS_Usa: Integer; ISS_Alq: Double;
  UF_Emit, UF_Dest: String; FisRegCad: Integer; SQL_ITS, SQL_TOT,
  SQL_CUSTOMZ: String; LaAviso1, LaAviso2: TLabel; emit_CRT: Integer;
  CalculaAutomatico: Boolean; idDest, indFinal, dest_indIEDest: Integer;
  ide_dEmi: TDateTime): Boolean;
const
  sProcName = 'TDmNFe_0000.InsUpdNFeIts()';

  procedure MensagemCalcularManualCST(CST: Integer);
  begin
    Geral.MensagemBox('Calcule os impostos manualmente, pois o CST do ' +
    'ICMS selecionado (' + IntToStr(CST) + ') não tem seu cáculo implementado ' +
    'neste aplicativo! INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;

  procedure MensagemCalcularManualCSOSN(CSOSN: Integer);
  begin
    Geral.MensagemBox('Calcule os impostos manualmente, pois o CSOSN do ' +
    'ICMS selecionado (' + IntToStr(CSOSN) + ') não tem seu cáculo implementado ' +
    'neste aplicativo! INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;

  function MargemValorAgregado(Valor, Margem: Double): Double;
  begin
    // Parei Aqui Fazer!
    Geral.MensagemBox('Calcule os impostos manualmente, pois a ' +
    'modalidade selecionada para determinação da base de cálculo (Margem ' +
    'Valor Agregado) não está implementada neste aplicativo! ' +
    'INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Result := Valor;
  end;

  function Pauta(QtdeItens, ValPauta: Double): Double;
  begin
    // Parei Aqui Fazer!
    Geral.MensagemBox('Calcule os impostos manualmente, pois a ' +
    'modalidade selecionada para determinação da base de cálculo (Pauta) ' +
    'não está implementada neste aplicativo! ' +
    'INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Result := QtdeItens * ValPauta;
  end;

  function PrecoTabelado(QtdeItens, PrecoTabelado: Double): Double;
  begin
    // Parei Aqui Fazer!
    Geral.MensagemBox('Calcule os impostos manualmente, pois a ' +
    'modalidade selecionada para determinação da base de cálculo (Preço ' +
    'Tabelado) não está implementada neste aplicativo! ' +
    'INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Result := QtdeItens * PrecoTabelado;
  end;

var
  nItem, InfAdCuztm, StqMovValA: Integer;
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EXTIPI: String;
  prod_genero: Integer;
  prod_CFOP, prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;
  prod_cEANTrib, prod_uTrib: String;
  prod_qTrib, prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  Tem_IPI, Tem_II: Integer;
  ICMS_Orig, ICMS_CST, ICMS_ModBC: Integer;
  ICMS_PRedBC, ICMS_VBC, ICMS_PICMS,ICMS_VICMS: Double;
  ICMS_ModBCST: Integer;
  ICMS_PMVAST,
  ICMS_PRedBCST, ICMS_VBCST, ICMS_PICMSST,
  ICMS_VICMSST: Double;
  ICMS_vBCSTRet, ICMS_vICMSSTRet: Double;
  ICMS_CSOSN, ICMS_UFST: Integer;
  ICMS_pBCOp: Double;
  ICMS_motDesICMS: Integer;
  ICMS_pCredSN, ICMS_vCredICMSSN: Double;
  ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif, ICMS_vICMSDeson: Double;
  IPI_CST: Integer;
  IPI_ClEnq, IPI_CNPJProd, IPI_CSelo: String;
  IPI_QSelo: Double; // int64
  IPI_CEnq: String;
  IPI_VBC, IPI_QUnid,
  IPI_VUnid, IPI_PIPI, IPI_VIPI
          { É Preenchido manualmente no form NFeItsI_0000
  ,II_VBC, II_VDespAdu, II_VII, II_VIOF
          }
  : Double;
  PIS_CST: Integer;
  PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd,
  PIS_vAliqProd, PISST_vBC, PISST_pPIS,
  PISST_qBCProd, PISST_vAliqProd, PISST_vPIS: Double;
  COFINS_CST: Integer;
  COFINS_vBC, COFINS_pCOFINS,
  COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd,
  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
  COFINSST_vAliqProd, COFINSST_vCOFINS: Double;
  ISSQN_vBC, ISSQN_vAliq, ISSQN_vISSQN: Double;
  ISSQN_cMunFG, ISSQN_cListServ: Integer;
  InfAdProd: String;
  //
  X, Mensagem, Txt_A, Txt_B, Txt_C: String;
  Continua: Boolean;
  Qtd, TotToBase, Prc, Per, FatRedBC: Double;
  PIS_fatorBC, PISST_fatorBCST, COFINS_fatorBC, COFINSST_fatorBCST: Double;
  prod_indTot: Integer;
  // 2011-08-19
  pRedBC: Double;
  modBC: Integer;
  // Fim 2011-08-19
  // 2011-08-23
  AchouFisc: Integer;
  CST_B: String;
  CSOSN: String;
  ICMSAliq: Double;
  // fim 2011-08-23
  //
  iTotTrib: Integer;
  vTotTrib, pTotTrib, vBasTrib: Double;
  NCM, EX, tabTotTrib: Integer;
  verTotTrib: String;
  tpAliqTotTrib, fontTotTrib: Integer;
  InfCplXtra, prod_indEscala: String;
  prod_CEST: Integer; // 2015-10-14
  // 2019-09-04
  prod_cBenef: String;
  // fim 2019-09-04
  //
  GeraGrupoNA: Boolean;
  ICMSUFDest_vBCUFDest, ICMSUFDest_pFCPUFDest, ICMSUFDest_pICMSUFDest,
  ICMSUFDest_pICMSInter, ICMSUFDest_pICMSInterPart, ICMSUFDest_vFCPUFDest,
  ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet: Double;
  Ano, Mes, Dia: Word;
  //vICMSTotGrupoNA, vICMSDestSemFCP: Double;
  vTribFed, pTribFed, vTribEst, pTribEst, vTribMun, pTribMun: Double;
  // 2021-01-26
  pDif: Double;
  Erros: Integer;
  // 2021-03-11
  prod_cBarra, prod_cBarraTrib: String;
  // 2021-06-26
  NFeUsaMarca, NFeUsaReferencia: Integer;
  NFePreMarca, NFePosMarca, NFePreReferencia, NFePosReferencia, xTmp1, xTmp2,
  xTmpPre, xTmpPos: String;
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String;
  VAItem: Integer;
begin
  Erros := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando produtos da NFe');
  //
  ReopenNFeLayI();
  //
  Mensagem := '';
  //
  QrProds1.Close;
  QrProds1.SQL.Clear;
  QrProds1.SQL.Add(SQL_ITS);
  //Geral.MB_Teste(SQL_ITS);
  UMyMod.AbreQuery(QrProds1, Dmod.MyDB, 'TDmNFe_0000.InsUpdNFeIts()');
  //
  QrTotal1.Close;
  QrTotal1.SQL.Clear;
  QrTotal1.SQL.Add(SQL_TOT);
  UMyMod.AbreQuery(QrTotal1, Dmod.MyDB, 'TDmNFe_0000.InsUpdNFeIts()');
  //
  QrProds1.First;
  //
  nItem := 0;
  while not QrProds1.Eof do
  begin
    nItem := nItem + 1;
    // Zerar tudo para fazer só o que precisa!
    iTotTrib           := 0;
    vTotTrib           := 0.0000;
    ICMS_ModBC         := 0;
    ICMS_PRedBC        := 0.00;
    ICMS_VBC           := 0.00;
    ICMS_PICMS         := 0.00;
    ICMS_vICMSOp       := 0.00;
    ICMS_pDif          := 0.00;
    ICMS_vICMSDif      := 0.00;
    ICMS_VICMS         := 0.00;
    ICMS_vICMSDeson    := 0.00;
    ICMS_motDesICMS    := 0;
    ICMS_ModBCST       := 0;
    ICMS_PMVAST        := 0.00;
    ICMS_PRedBCST      := 0.00;
    ICMS_VBCST         := 0.00;
    ICMS_PICMSST       := 0.00;
    ICMS_VICMSST       := 0.00;
    ICMS_vBCSTRet      := 0.00;
    ICMS_vICMSSTRet    := 0.00;
    ICMS_CSOSN         := 0;
    ICMS_UFST          := 0;
    ICMS_pBCOp         := 0.00;
    ICMS_motDesICMS    := 0;
    ICMS_pCredSN       := 0.00;
    ICMS_vCredICMSSN   := 0.00;
    //
    ICMSUFDest_vBCUFDest      := 0.00;
    ICMSUFDest_pFCPUFDest     := 0.0000;
    ICMSUFDest_pICMSUFDest    := 0.0000;
    ICMSUFDest_pICMSInter     := 0.0000;
    ICMSUFDest_pICMSInterPart := 0.0000;
    ICMSUFDest_vFCPUFDest     := 0.00;
    ICMSUFDest_vICMSUFDest    := 0.00;
    ICMSUFDest_vICMSUFRemet   := 0.00;
    //
    IPI_ClEnq          := '';
    IPI_CNPJProd       := '';
    IPI_CSelo          := '';
    IPI_QSelo          := 0;
    IPI_CEnq           := '';
    IPI_CST            := 0;
    IPI_VBC            := 0.00;
    IPI_QUnid          := 0.00;
    IPI_VUnid          := 0.00;
    IPI_PIPI           := 0.00;
    IPI_VIPI           := 0.00;

          { É Preenchido manualmente no form NFeItsI_0000
    II_VBC             := 0.00;
    II_VDespAdu        := 0.00;
    II_VII             := 0.00;
    II_VIOF            := 0.00;
}
    PIS_CST            := 0;
    PIS_vBC            := 0.00;
    PIS_pPIS           := 0.00;
    PIS_vPIS           := 0.00;
    PIS_qBCProd        := 0.00;
    PIS_vAliqProd      := 0.00;
    PISST_vBC          := 0.00;
    PISST_pPIS         := 0.00;
    PISST_qBCProd      := 0.00;
    PISST_vAliqProd    := 0.00;
    PISST_vPIS         := 0.00;

    COFINS_CST         := 0;
    COFINS_vBC         := 0.00;
    COFINS_pCOFINS     := 0.00;
    COFINS_vCOFINS     := 0.00;
    COFINS_qBCProd     := 0.00;
    COFINS_vAliqProd   := 0.00;
    COFINSST_vBC       := 0.00;
    COFINSST_pCOFINS   := 0.00;
    COFINSST_qBCProd   := 0.00;
    COFINSST_vAliqProd := 0.00;
    COFINSST_vCOFINS   := 0.00;

    ISSQN_vBC          := 0.00;
    ISSQN_vAliq        := 0.00;
    ISSQN_vISSQN       := 0.00;
    ISSQN_cMunFG       := 0;
    ISSQN_cListServ    := 0;

    prod_cBenef        := '';

    if QrProds1Servico.Value = 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Adicionando registro do item de produto ' + Geral.FF0(nItem) + ' de ' +
        Geral.FF0(QrProds1.RecordCount) + ' na tabela de produtos e serviços da NFe');
      //
      prod_cProd := FormatFloat('0', QrProds1CU_PRODUTO.Value);
      prod_cEAN  := QrProds1cGTIN_EAN.Value;
      prod_xProd := QrProds1NO_GRUPO.Value;
      if QrProds1prod_cBarraGGX.Value <> EmptyStr then
      begin
        prod_cBarra     := QrProds1prod_cBarraGGX.Value;
        prod_cBarraTrib := QrProds1prod_cBarraGGX.Value;
      end
      else
      begin
        prod_cBarra     := QrProds1prod_cBarraGG1.Value;
        prod_cBarraTrib := QrProds1prod_cBarraGG1.Value;
      end;
      //
      if (QrProds1NO_COR.Value <> '') and (QrProds1PrintCor.Value > 0) then
        prod_xProd   := prod_xProd + ' ' + QrProds1NO_COR.Value;
      if (QrProds1NO_TAM.Value <> '') and (QrProds1PrintTam.Value = 1)then
        prod_xProd   := prod_xProd + ' ' + QrProds1NO_TAM.Value;

      prod_NCM    := DefX('104', 'I05', Geral.SoNumero_TT(QrProds1NCM.Value));




      //prod_CEST   := QrProds1prod_CEST.Value;
      prod_CEST   := 0;
      DModG.ReopenParamsEmp(Empresa);
      //
      case DModG.QrPrmsEmpNFeNFeUsoCEST.Value of
        // Apenas do cadastro do produto
        0: prod_CEST   := QrProds1prod_CEST.Value;
        //Preferencialmente do cadastro do produto
        1:
        begin
          prod_CEST := QrProds1prod_CEST.Value;
          if prod_CEST = 0 then
            DefineCESTdoNCM(prod_xProd, prod_cProd, prod_NCM, prod_CEST);
        end;
        //Apenas da tabela CEST X NCM
        2: DefineCESTdoNCM(prod_xProd, prod_cProd, prod_NCM, prod_CEST);
        //Preferencialmente da tabela CEST X NCM
        3:
        begin
          DefineCESTdoNCM(prod_xProd, prod_cProd, prod_NCM, prod_CEST);
          if prod_CEST = 0 then
            prod_CEST := QrProds1prod_CEST.Value;
        end;
        //Não informar
        4: prod_CEST := 0;
        else
        begin
          prod_CEST := 0;
          Geral.MB_Erro('Definição de CEST não implementada em ' + sProcName);
        end;
      end;




      prod_EXTIPI := QrProds1EX_TIPI.Value;
      //
      if Length(prod_NCM) <> 8 then prod_genero := 0 else
        prod_genero   := Geral.IMV(Copy(prod_NCM, 1, 2));
      prod_CFOP      := Geral.SoNumero_TT(QrProds1CFOP.Value);
      prod_uCom      := QrProds1NO_UNIDADE.Value;
      prod_qCom      := QrProds1Qtde.Value;
      prod_vUnCom    := QrProds1Preco.Value;
      prod_vProd     := QrProds1Total.Value;
      prod_cEANTrib  := QrProds1cGTIN_EAN.Value;
      prod_uTrib     := prod_uCom; // Parei aqui - é diferente?
      prod_qTrib     := prod_qCom;  // Parei aqui - é diferente?
      prod_vUnTrib   := prod_vUnCom; // Parei aqui - é diferente?
(*
      // Ini 2020-10-31
      prod_vFrete    := 0.00; // Parei aqui
      prod_vSeg      := 0.00; // Parei aqui
      prod_vDesc     := 0.00; // Parei aqui
      prod_vOutro    := 0.00; // Parei aqui  NFe 2.00*)
      //
      prod_vFrete    := QrProds1prod_vFrete.Value;
      prod_vSeg      := QrProds1prod_vSeg.Value;
      prod_vDesc     := QrProds1prod_vDesc.Value;
      prod_vOutro    := QrProds1prod_vOutro.Value;
(*
     // Fim 2020-10-31
*)
      prod_indEscala := QrProds1prod_indEscala.Value;
      //
      StqMovValA := QrProds1ID.Value;
      //
      // Calculo automatico!
      InfAdProd := QrProds1InfAdProd.Value;
      //
      if CalculaAutomatico then
      begin
        prod_indTot    := QrProds1prod_indTot.Value; // NFe 2.00
        // itens diferenciados pelo CST (00,10,20,30,40,41,50,51,70,90)
        ICMS_Orig      := QrProds1CST_A.Value;
        // Parei Aqui CST >> como fazer quando o CST é diferente para cada estado? muda a alíquota de ICMS?
        QrGraGruCST.Close;
        QrGraGruCST.Database := Dmod.MyDB;
        QrGraGruCST.Params[00].AsInteger := QrProds1CU_PRODUTO.Value;
        QrGraGruCST.Params[01].AsString  := UF_Emit;
        QrGraGruCST.Params[02].AsString  := UF_Dest;
        UnDmkDAC_PF.AbreQuery(QrGraGruCST, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
(*      2015-10-24
        QrAliq.Close;
        QrAliq.Params[00].AsInteger := FisRegCad;
        QrAliq.Params[01].AsInteger := QrProds1CFOP_MesmaUF.Value;
        QrAliq.Params[02].AsInteger := QrProds1CFOP_Contrib.Value;
        QrAliq.Params[03].AsInteger := QrProds1CFOP_Proprio.Value;
        QrAliq.Params[04].AsString  := UF_Emit;
        QrAliq.Params[05].AsString  := UF_Dest;
        QrAliq.O p e n ;
*)
        ReopenAliq(QrProds1IDCtrl.Value, FisRegCad, QrProds1CFOP_MesmaUF.Value,
          QrProds1CFOP_Contrib.Value, QrProds1CFOP_Proprio.Value,
          QrProds1UsaSubsTrib.Value, UF_Emit, UF_Dest);
        // FIM 2015-10-24
        //
        // 2011-08-23
        if QrAliq.RecordCount > 0 then
        begin
          AchouFisc   := 1;
          CST_B       := QrAliqCST_B.Value;
          ICMSAliq    := QrAliqICMSAliq.Value;
          pRedBC      := QrAliqpRedBC.Value;
          modBC       := QrAliqmodBC.Value;
          prod_cBenef := QrAliqcBenef.Value;
          pDif        := QrAliqpDif.Value;
          CSOSN       := QrAliqCSOSN.Value;
        end else
        begin
          AchouFisc   := 0;
          CST_B       := '';
          ICMSAliq    := 0;
          pRedBC      := 0;
          modBC       := 0;
          prod_cBenef := '';
          CSOSN       := '';
        end;
        VerificaExcecao(FisRegCad, QrProds1CFOP_MesmaUF.Value,
          QrProds1CFOP_Contrib.Value, QrProds1CFOP_Proprio.Value,
          QrProds1UsaSubsTrib.Value, UF_Emit,
          UF_Dest, QrProds1CU_PRODUTO.Value, CST_B, ICMSAliq, pRedBC, pDif, modBC,
          AchouFisc, prod_cBenef, CSOSN);
        //
        if CST_B <> '' then
        begin
          // Usado CST da guia de excessoes da regra fiscal
          ICMS_CST := Geral.IMV(CST_B);
          //Show Message('Usado CST ICMS da guia de excessoes da regra fiscal! > ' + FormatFloat('00', ICMS_CST));
        end else
        if QrGraGruCST.RecordCount > 0 then
        begin
          // Usado CST da sub guia "Situações especiais" da guia "Fiscal" do cadastro de Produto de Grade (FmGraGruN > FmGraGruCST)
          ICMS_CST := QrGraGruCSTCST_B.Value;
          //Show Message('Usado CST ICMS Especial! > ' + FormatFloat('00', ICMS_CST));
        end else
          ICMS_CST := QrProds1CST_B.Value;
        //
        Qtd := QrProds1Qtde.Value;
        TotToBase := QrProds1Total.Value
                     + QrProds1prod_vFrete.Value
                     - QrProds1prod_vDesc.Value
                     + QrProds1prod_vOutro.Value
                     + QrProds1prod_vSeg.Value;
        Prc := QrProds1ICMS_Val.Value;
        Per := ICMSAliq;//QrProds1ICMS_Per.Value;
        //
        if (ICMS_Usa = 1) or (emit_CRT in [1,2]) then
        begin
          if (emit_CRT <> 1)
          // Devolução de mercadoria!  2011-09-18
          // 2021-02-27 and (QrProds1CSOSN.Value = 0)
          and (QrProds1CSOSN_Manual.Value = 0)
          then
          begin
            if AchouFisc < 2 then  //  não é exceção
            begin
              if pRedBC = 0 then
                pRedBC := QrProds1ICMS_pRedBC.Value;
              //
              if modBC = -1 then
                modBC := QrProds1ICMS_modBC.Value;
              //
              if pDif = 0 then
                pDif := QrProds1ICMS_pDif.Value;
            end;
            //
            if AchouFisc = 0 then
            begin
              Result := False;
              if QrProds1CFOP_MesmaUF.Value = 1 then Txt_A := 'Sim' else Txt_A := 'Não';
              if QrProds1CFOP_Contrib.Value = 1 then Txt_B := 'Sim' else Txt_B := 'Não';
              if QrProds1CFOP_Proprio.Value = 1 then Txt_C := 'Sim' else Txt_C := 'Não';
              Geral.MB_Aviso('Geração de NF-e abortada! ' + 'A mercadoria "' +
                Geral.FF0(QrProds1CO_GRUPO.Value) + ' - ' + QrProds1NO_GRUPO.Value
                + ' ' + QrProds1NO_COR.Value + ' ' + QrProds1NO_TAM.Value +
                '" não tem impostos definidos para a seguinte configuração:' + sLineBreak +
                'UF emitente: ' + UF_Emit + sLineBreak + 'UF Destino: ' + UF_Dest + sLineBreak +
                'Mesma UF: ' + Txt_A + sLineBreak + 'Contribuinte (I.E.): ' + Txt_B + sLineBreak +
                'Produto de fabricação própria: ' + Txt_C + sLineBreak +
                'ID da regra fiscal: ' + Geral.FF0(FisRegCad));
              Exit;
            end;
            case ICMS_CST of
              0:
              begin
                ICMS_ModBC     := modBC;
                // Não Usa ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_PICMS     := ICMSAliq;
                ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                // Não Usa ICMS_ModBCST   := QrProds1ICMS_.Value;
                // Não Usa ICMS_PMVAST    := QrProds1ICMS_.Value;
                // Não Usa ICMS_PRedBCST  := QrProds1ICMS_.Value;
                // Não Usa ICMS_VBCST     := QrProds1ICMS_.Value;
                // Não Usa ICMS_PICMSST   := QrProds1ICMS_.Value;
                // Não Usa ICMS_VICMSST   := QrProds1ICMS_.Value;
              end;
              10:
              begin
                ICMS_ModBC     := modBC;
                // Não Usa ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_PICMS     := ICMSAliq;
                ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                // Parei Aqui
                MensagemCalcularManualCST(ICMS_CST);
                ICMS_ModBCST   := QrProds1ICMS_ModBCST .Value;
                ICMS_PMVAST    := QrProds1ICMS_PMVAST  .Value;
                ICMS_PRedBCST  := QrProds1ICMS_PRedBCST.Value;
                // Parei Aqui
                ICMS_VBCST     := 0;
                ICMS_PICMSST   := QrProds1ICMS_PICMSST .Value;
                ICMS_VICMSST   := 0;
              end;
              20:
              begin
                ICMS_ModBC     := modBC;
                ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_VBC        := Geral.RoundC(ICMS_VBC * (100 - pRedBC) / 100, 2);
                ICMS_PICMS      := ICMSAliq;
                ICMS_VICMS      := Round(ICMS_VBC * ICMS_PICMS) / 100;
  (*==============================================================================
   // NFe 3.10                                                                  //
  Informar apenas nos motivos de desoneração documentados abaixo.               *)
                                                                                //
                ICMS_vICMSDeson := CalculaICMSDeson(ICMS_VICMS);                //
                                                                                //
  (*==============================================================================
  Campo será preenchido quando o campo anterior estiver preenchido.             //
  Informar o motivo da desoneração:                                             //
                3=Uso na agropecuária;                                          //
                9=Outros;                                                       //
                12=Órgão de fomento e desenvolvimento agropecuário.             *)
                                                                                //
                VerificaICMSDeson(ICMS_vICMSDeson, ICMS_motDesICMS);            //
                                                                                //
                // FIM NFe 3.10                                                 //
  (*============================================================================*)

                (*
                ICMS_ModBCST   := QrProds1ICMS_.Value;
                ICMS_PMVAST    := QrProds1ICMS_.Value;
                ICMS_PRedBCST  := QrProds1ICMS_.Value;
                ICMS_VBCST     := QrProds1ICMS_.Value;
                ICMS_PICMSST   := QrProds1ICMS_.Value;
                ICMS_VICMSST   := QrProds1ICMS_.Value;
                *)
              end;
              30:
              begin
                // Não Usa ICMS_ModBC     := modBC;
                // Não Usa ICMS_PRedBC    := pRedBC;
                // Não Usa case ICMS_ModBC of
                // Não Usa   0: ICMS_VBC := MargemValorAgregado(Tot, Per);
                // Não Usa   1: ICMS_VBC := Pauta(Qtd, Prc);
                // Não Usa   2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                // Não Usa   3: ICMS_VBC := Tot;
                // Não Usa end;
                // Não Usa ICMS_PICMS     := ICMSAliq;
                // Não Usa ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                // Parei Aqui
                MensagemCalcularManualCST(ICMS_CST);
                ICMS_ModBCST   := QrProds1ICMS_ModBCST .Value;
                ICMS_PMVAST    := QrProds1ICMS_PMVAST  .Value;
                ICMS_PRedBCST  := QrProds1ICMS_PRedBCST.Value;
                // Parei Aqui
                ICMS_VBCST     := 0;
                ICMS_PICMSST   := QrProds1ICMS_PICMSST .Value;
                ICMS_VICMSST   := 0;
  (*==============================================================================
   // NFe 3.10                                                                  //
  Informar apenas nos motivos de desoneração documentados abaixo.               *)
                                                                                //
                ICMS_vICMSDeson := CalculaICMSDeson(ICMS_VICMS);                //
                                                                                //
  (*==============================================================================
  Campo será preenchido quando o campo anterior estiver preenchido.             //
  Informar o motivo da desoneração:                                             //
                6=Utilitários e Motocicletas da Amazônia Ocidental e Áreas de   //
                  Livre Comércio (Resolução 714/88 e 790/94 – CONTRAN e suas    //
                  alterações);                                                  //
                7=SUFRAMA;                                                      //
                9=Outros;                                                       //
                                                                                *)
                VerificaICMSDeson(ICMS_vICMSDeson, ICMS_motDesICMS);            //
                                                                                //
                // FIM NFe 3.10                                                 //
  (*============================================================================*)
              end;
              40, 41, 50:
              begin
                // Nada.
                // Só tem ICMS nas vendas de veículos beneficiados com
                // desoneração nacional do ICMS. 2.00
  (*==============================================================================
   // NFe 3.10                                                                  //
  Informar apenas nas operações:                                                //
    a) com produtos beneficiados com a desoneração condicional do ICMS.         //
    b) destinadas à SUFRAMA, informando-se o valor que seria devido se não      //
       houvesse isenção.                                                        //
    c) de venda a órgão da administração pública direta e suas fundações e      //
       autarquias com isenção do ICMS. (NT 2011/004)                            *)
                                                                                //
                ICMS_vICMSDeson := CalculaICMSDeson(ICMS_VICMS);                //
                                                                                //
  (*==============================================================================
  Campo será preenchido quando o campo anterior estiver preenchido.             //
  Informar o motivo da desoneração:                                             //
                1=Táxi;                                                         //
                3=Produtor Agropecuário;                                        //
                4=Frotista/Locadora;                                            //
                5=Diplomático/Consular;                                         //
                6=Utilitários e Motocicletas da Amazônia Ocidental e Áreas de   //
                  Livre Comércio (Resolução 714/88 e 790/94 – CONTRAN e suas    //
                  alterações);                                                  //
                7=SUFRAMA;                                                      //
                8=Venda a Órgão Público;                                        //
                9=Outros. (NT 2011/004); 10=Deficiente Condutor (Convênio ICMS  //
                  38/12);                                                       //
                11=Deficiente Não Condutor (Convênio ICMS 38/12).               //
                                                                                //
                Observação:                                                     //
                Revogada a partir da versão 3.01 a possibilidade de usar o      //
                motivo 2=Deficiente Físico                                      //
                                                                                *)
                VerificaICMSDeson(ICMS_vICMSDeson, ICMS_motDesICMS);            //
                                                                                //
                // FIM NFe 3.10                                                 //
  (*============================================================================*)
              end;
              51:
              begin
                ICMS_ModBC     := modBC;
                ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_VBC       := Geral.RoundC(ICMS_VBC * (100 - pRedBC) / 100, 2);
                ICMS_PICMS     := ICMSAliq;
  (*==============================================================================
     Mudou na Versao 3.10                                                       *)
                                                                                //
                ICMS_vICMSOp   := Round(ICMS_VBC * ICMS_PICMS) / 100;           //
                //
                //
                //
                // ini 2021-01-26
                //ICMS_pDif      := QrProds1ICMS_pDif.Value;
                ICMS_pDif      := pDif;
                // fim 2021-01-26
                //
                ICMS_vICMSDif  := Geral.RoundC(ICMS_pDif * ICMS_vICMSOp / 100, 2);
                ICMS_vICMS     := ICMS_vICMSOp - ICMS_vICMSDif;                 //
  (*============================================================================*)
                // Não Usa ICMS_ModBCST   := QrProds1ICMS_.Value;
                // Não Usa ICMS_PMVAST    := QrProds1ICMS_.Value;
                // Não Usa ICMS_PRedBCST  := QrProds1ICMS_.Value;
                // Não Usa ICMS_VBCST     := QrProds1ICMS_.Value;
                // Não Usa ICMS_PICMSST   := QrProds1ICMS_.Value;
                // Não Usa ICMS_VICMSST   := QrProds1ICMS_.Value;
              end;
              60:
              begin
                // Não Usa ICMS_ModBC     := modBC;
                // Não Usa ICMS_PRedBC    := QrProds1ICMS_PRedBC.Value;
                // Não Usa case ICMS_ModBC of
                // Não Usa   0: ICMS_VBC := MargemValorAgregado(Tot, Per);
                // Não Usa   1: ICMS_VBC := Pauta(Qtd, Prc);
                // Não Usa   2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                // Não Usa   3: ICMS_VBC := Tot;
                // Não Usa end;
                // Não Usa ICMS_VBC       := Round(ICMS_VBC * Red) / 100;
                // Não Usa ICMS_PICMS     := ICMSAliq;
                // Não Usa ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                // Parei Aqui
                MensagemCalcularManualCST(ICMS_CST);
                ICMS_VBCST     := 0;
                ICMS_VICMSST   := 0;
              end;
              70:
              begin
                ICMS_ModBC     := modBC;
                ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_VBC       := Geral.RoundC(ICMS_VBC * (100 - pRedBC) / 100, 2);
                ICMS_PICMS     := ICMSAliq;
                ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                ICMS_ModBCST   := QrProds1ICMS_ModBCST .Value;
                ICMS_PMVAST    := QrProds1ICMS_PMVAST  .Value;
                ICMS_PRedBCST  := QrProds1ICMS_PRedBCST.Value;
                // Parei Aqui
                ICMS_VBCST     := 0;
                ICMS_PICMSST   := QrProds1ICMS_PICMSST .Value;
                ICMS_VICMSST   := 0;
  (*==============================================================================
   // NFe 3.10                                                                  //
  Informar apenas nos motivos de desoneração documentados abaixo.               *)
                                                                                //
                ICMS_vICMSDeson := CalculaICMSDeson(ICMS_VICMS);                //
                                                                                //
  (*==============================================================================
  Campo será preenchido quando o campo anterior estiver preenchido.             //
  Informar o motivo da desoneração:                                             //
                3=Uso na agropecuária;                                          //
                9=Outros;                                                       //
                12=Órgão de fomento e desenvolvimento agropecuário.             *)
                                                                                //
                VerificaICMSDeson(ICMS_vICMSDeson, ICMS_motDesICMS);            //
                                                                                //
                // FIM NFe 3.10                                                 //
  (*============================================================================*)
                MensagemCalcularManualCST(ICMS_CST);
              end;
              90:
              begin
                ICMS_ModBC     := modBC;
                ICMS_PRedBC    := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC := Pauta(Qtd, Prc);
                  2: ICMS_VBC := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC := TotToBase;
                end;
                ICMS_VBC       := Geral.RoundC(ICMS_VBC * (100 - pRedBC) / 100, 2);
                ICMS_PICMS     := ICMSAliq;
                ICMS_VICMS     := Round(ICMS_VBC * ICMS_PICMS) / 100;
                ICMS_ModBCST   := QrProds1ICMS_ModBCST .Value;
                ICMS_PMVAST    := QrProds1ICMS_PMVAST  .Value;
                ICMS_PRedBCST  := QrProds1ICMS_PRedBCST.Value;
                // Parei Aqui
                ICMS_VBCST     := 0;
                ICMS_PICMSST   := QrProds1ICMS_PICMSST .Value;
                ICMS_VICMSST   := 0;
                MensagemCalcularManualCST(ICMS_CST);
  (*==============================================================================
   // NFe 3.10                                                                  //
  Informar apenas nos motivos de desoneração documentados abaixo.               *)
                                                                                //
                ICMS_vICMSDeson := CalculaICMSDeson(ICMS_VICMS);                //
                                                                                //
  (*==============================================================================
  Campo será preenchido quando o campo anterior estiver preenchido.             //
  Informar o motivo da desoneração:                                             //
                3=Uso na agropecuária;                                          //
                9=Outros;                                                       //
                12=Órgão de fomento e desenvolvimento agropecuário.             *)
                                                                                //
                VerificaICMSDeson(ICMS_vICMSDeson, ICMS_motDesICMS);            //
                                                                                //
                // FIM NFe 3.10                                                 //
  (*============================================================================*)
              end;
              else Geral.MensagemBox('CST do ICMS (' + IntToStr(ICMS_CST) +
              ') inválido ou não implementado!', 'Aviso', MB_OK+MB_ICONWARNING);
            end;
          end else begin
            ICMS_CST     := 0;
            // Parei aqui 2021-02-27
            // ini 2021-02-27
            //Priorizar manual, depois regra, depois produto, depois ParamsEmp
            //ICMS_CSOSN   := QrProds1CSOSN.Value; // DmodG.QrParamsEmpCSOSN.Value;
            if QrProds1CSOSN_Manual.Value > 0 then
              ICMS_CSOSN   := QrProds1CSOSN_Manual.Value
            else
            // Regra Fiscal
            if (CSOSN <> EmptyStr) and (Geral.IMV(CSOSN) > 0) then
              ICMS_CSOSN   := Geral.IMV(CSOSN)
            else
            if QrProds1CSOSN_GG1.Value > 0 then
              ICMS_CSOSN   := QrProds1CSOSN_GG1.Value
            else
              ICMS_CSOSN   := DmodG.QrPrmsEmpNFeCSOSN.Value;
            //
            // fim 2021-02-27

            ICMS_pCredSN := 0;
            case ICMS_CSOSN of
              101:
              begin
                // 2011-02-19
                ICMS_pCredSN     := QrProds1pCredSN.Value;
                ICMS_vCredICMSSN := Geral.RoundC(ICMS_pCredSN * QrProds1Total.Value / 100, 2);
              end;
              102, 103, 300, 400:
              begin
                // nada - já OK
              end;
              201:
              begin
                { Parei aqui # 2.00
                ICMS_modBCST     := ?
                ICMS_pMVAST      := ?
                ICMS_pRedBCST    := ?
                ICMS_vBCST       := ?
                ICMS_pICMSST     := ?
                ICMS_vICMSST     := ?
                ICMS_pCredSN     := ?
                ICMS_pCredICMSSN := ?
                }
                MensagemCalcularManualCSOSN(ICMS_CSOSN);
              end;
              202, 203:
              begin
                //2014-07-23
                { Parei aqui # 2.00}
                ICMS_modBCST     := QrProds1ICMS_modBCST.Value;
                ICMS_pMVAST      := QrProds1ICMS_pMVAST.Value;
                ICMS_pRedBCST    := QrProds1ICMS_pRedBCST.Value;
                ICMS_vBCST       := Geral.RoundC((ICMS_pMVAST + 100) * TotToBase, 2) / 100;
                ICMS_pICMSST     := QrProds1ICMS_pICMSST.Value;
                ICMS_vICMSST     := ValorICMSST_0(TotToBase, ICMS_vBCST, ICMS_pICMSST);
                //MensagemCalcularManualCSOSN(ICMS_CSOSN);
                if ICMS_pRedBCST >= 0.01 then
                  Geral.MB_Aviso('"ICMS_pRedBCST" não implementado!' +  slineBreak +
                  'Solicite à Dermatek sua implementação antes da emissão desta NFe!');
                if DBCheck.CriaFm(TFmNFaEditCSOSN202, FmNFaEditCSOSN202, afmoLiberado) then
                begin
                  FmNFaEditCSOSN202.EdCodigo.Valuevariant     := QrProds1CU_PRODUTO.Value;
                  FmNFaEditCSOSN202.EdNome.Valuevariant       := QrProds1NO_GRUPO.Value;
                  //
                  FmNFaEditCSOSN202.RGICMS_modBCST.ItemIndex     := ICMS_modBCST;
                  FmNFaEditCSOSN202.EdICMS_pMVAST.ValueVariant   := ICMS_pMVAST;
                  FmNFaEditCSOSN202.EdICMS_pRedBCST.ValueVariant := ICMS_pRedBCST;
                  FmNFaEditCSOSN202.EdICMS_vBCST.ValueVariant    := ICMS_vBCST;
                  FmNFaEditCSOSN202.EdICMS_pICMSST.ValueVariant  := ICMS_pICMSST;
                  FmNFaEditCSOSN202.EdICMS_vICMSST.ValueVariant  := ICMS_vICMSST;
                  //
                  FmNFaEditCSOSN202.ShowModal;
                  if FmNFaEditCSOSN202.FOK then
                  begin
                    ICMS_modBCST  := FmNFaEditCSOSN202.RGICMS_modBCST.ItemIndex;
                    ICMS_pMVAST   := FmNFaEditCSOSN202.EdICMS_pMVAST.ValueVariant;
                    ICMS_pRedBCST := FmNFaEditCSOSN202.EdICMS_pRedBCST.ValueVariant;
                    ICMS_vBCST    := FmNFaEditCSOSN202.EdICMS_vBCST.ValueVariant;
                    ICMS_pICMSST  := FmNFaEditCSOSN202.EdICMS_pICMSST.ValueVariant;
                    ICMS_vICMSST  := FmNFaEditCSOSN202.EdICMS_vICMSST.ValueVariant;
                  end;
                  FmNFaEditCSOSN202.Destroy;
                end;
              end;
              500:
              begin
(*              2021-02-27 REativar código quando necessário
                Desativado (Tisolin)
                if DBCheck.CriaFm(TFmNFaEditCSOSN500, FmNFaEditCSOSN500, afmoLiberado) then
                begin
                  FmNFaEditCSOSN500.EdCodigo.Valuevariant     := QrProds1CU_PRODUTO.Value;
                  FmNFaEditCSOSN500.EdNome.Valuevariant       := QrProds1NO_GRUPO.Value;
                  //
                  FmNFaEditCSOSN500.ShowModal;
                  if FmNFaEditCSOSN500.FOK then
                  begin
                    ICMS_vBCSTRet    := FmNFaEditCSOSN500.EdICMS_vBCSTRet.ValueVariant;
                    ICMS_vICMSSTRet  := FmNFaEditCSOSN500.EdICMS_vICMSSTRet.ValueVariant;
                  end;
                  FmNFaEditCSOSN500.Destroy;
                end;
                fim 2021-02-27
*)
                //FIM 2014-07-23
              end;
              900:
              begin
                // 2011-09-18
                ICMS_ModBC       := modBC;
                ICMS_PRedBC      := pRedBC;
                case ICMS_ModBC of
                  0: ICMS_VBC    := MargemValorAgregado(TotToBase, Per);
                  1: ICMS_VBC    := Pauta(Qtd, Prc);
                  2: ICMS_VBC    := PrecoTabelado(Qtd, Prc);
                  3: ICMS_VBC    := TotToBase;
                end;
                ICMS_VBC         := Geral.RoundC(ICMS_VBC * (100 - pRedBC) / 100, 2);
                ICMS_PICMS       := ICMSAliq;
                ICMS_VICMS       := Geral.RoundC(ICMS_VBC * ICMS_PICMS / 100, 2);
                ICMS_ModBCST     := QrProds1ICMS_ModBCST .Value;
                ICMS_PMVAST      := QrProds1ICMS_PMVAST  .Value;
                ICMS_PRedBCST    := QrProds1ICMS_PRedBCST.Value;
                // Parei Aqui
                ICMS_VBCST       := 0;
                ICMS_PICMSST     := QrProds1ICMS_PICMSST .Value;
                ICMS_VICMSST     := 0;
                ICMS_pCredSN     := QrProds1pCredSN.Value;
                ICMS_vCredICMSSN := Geral.RoundC(ICMS_pCredSN * ICMS_VBC / 100, 2);
              end;
              else
              begin
                Geral.MB_Aviso('CSOSN do ICMS = ' + IntToStr(ICMS_CSOSN) +
                ' do item ' + Geral.FF0(nItem) + ' inválido ou não implementado!');
                //
                Erros := Erros + 1;
              end;
            end;
          end;
        end;
////////////////////////////////////////////////////////////////////////////////
        GeraGrupoNA := UnNFe_PF.GeraGrupoNA_0310(ide_dEmi)
        // (QrProds1Servico.Value = 0 ) // nao pode ser servico!!
        and (idDest=2) // Operacao interestadual
        and (indFinal=1) // Consumidor final
        and (dest_indIEDest = 2); // Operacao com nao contribuinte
////////////////////////////////////////////////////////////////////////////////
        if GeraGrupoNA then
        begin
          // Valor da Base de Cálculo do ICMS na UF de destino.
          ICMSUFDest_vBCUFDest      := (QrAliqpBCUFDest.Value * ICMS_VBC) / 100;
          //Percentual adicional inserido na alíquota interna da UF de destino,
          //relativo ao Fundo de Combate à Pobreza (FCP) naquela UF.
          //Nota: Percentual máximo de 2%, conforme a legislação.
          ICMSUFDest_pFCPUFDest     := QrAliqpFCPUFDest.Value;
          //Alíquota adotada nas operações internas na UF de destino para o
          //produto / mercadoria. A alíquota do Fundo de Combate a Pobreza, se
          //existente para o produto / mercadoria, deve ser informada no campo
          //próprio (pFCPUFDest) não devendo ser somada à essa alíquota interna.
          ICMSUFDest_pICMSUFDest    := QrAliqpICMSUFDest.Value;
          //Alíquota interestadual das UF envolvidas:
          //- 4% alíquota interestadual para produtos importados;
          //- 7% para os Estados de origem do Sul e Sudeste (exceto ES), destinado para os Estados do Norte, Nordeste, Centro-Oeste e Espírito Santo;
          //- 12% para os demais casos.
          ICMSUFDest_pICMSInter     := QrAliqpICMSInter.Value;
          //Percentual de ICMS Interestadual para a UF de destino:
          //- 40% em 2016;
          //- 60% em 2017;
          //- 80% em 2018;
          //- 100% a partir de 2019.
          if QrAliqUsaInterPartLei.Value = 1 then
          begin
            DecodeDate(ide_dEmi, Ano, Mes, Dia);
            case Ano
              of 0..2014: ICMSUFDest_pICMSInterPart := 0;
              2015: ICMSUFDest_pICMSInterPart := 20; // começa só em 01/01/2016!
              2016: ICMSUFDest_pICMSInterPart := 40;
              2017: ICMSUFDest_pICMSInterPart := 60;
              2018: ICMSUFDest_pICMSInterPart := 80;
              else ICMSUFDest_pICMSInterPart := 100;
            end;
          end else
            ICMSUFDest_pICMSInterPart := QrAliqpICMSInterPart.Value;
{
          //Valor do ICMS relativo ao Fundo de Combate à Pobreza (FCP) da UF de destino.
          ICMSUFDest_vFCPUFDest     := Geral.RoundC(ICMSUFDest_pFCPUFDest * ICMSUFDest_vBCUFDest / 100, 2);
          //Valor do ICMS Interestadual para a UF de destino, já considerando o
          //valor do ICMS relativo ao Fundo de Combate à Pobreza naquela UF.
          if ICMSUFDest_pICMSUFDest <= ICMSUFDest_pICMSInter then
          begin
            ICMSUFDest_vICMSUFDest  := 0;
            ICMSUFDest_vICMSUFRemet := 0;
          end else
          begin
            vICMSTotGrupoNA         := Geral.RoundC(ICMSUFDest_vBCUFDest *
            (ICMSUFDest_pICMSUFDest - ICMSUFDest_pICMSInter) / 100, 2);
            vICMSDestSemFCP := Geral.RoundC(vICMSTotGrupoNA * ICMSUFDest_pICMSInterPart / 100, 2);
            //
            //Valor do ICMS Interestadual para a UF de destino, já considerando o
            //valor do ICMS relativo ao Fundo de Combate à Pobreza naquela UF.
            ICMSUFDest_vICMSUFDest  := vICMSDestSemFCP + ICMSUFDest_vFCPUFDest;
            //Valor do ICMS Interestadual para a UF do remetente.
            //Nota: A partir de 2019, este valor será zero.
            ICMSUFDest_vICMSUFRemet := vICMSTotGrupoNA - vICMSDestSemFCP;
}
          UnNFe_PF.CalculaValoresICMS_OIVCF(ICMSUFDest_pFCPUFDest,
          ICMSUFDest_vBCUFDest, ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
          ICMSUFDest_pICMSInterPart, ICMSUFDest_vFCPUFDest,
          ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet);
        end; // Fim if GeraGrupoNA

        //
        // IPI
        //if IPI_Usa = 1 then
        //begin
          IPI_ClEnq      := '';
          IPI_CNPJProd   := '';
          IPI_CSelo      := '';
          IPI_QSelo      := 0; // int64
          IPI_CEnq       := QrProds1IPI_cEnq.Value;
          IPI_CST        := QrProds1IPI_CST.Value;
          case QrProds1IPI_TpTrib.Value of
            0: // Nada
            begin
              IPI_VBC        := 0.00;
              IPI_QUnid      := 0.00;
              IPI_VUnid      := 0.00;
              IPI_PIPI       := 0.00;
              IPI_VIPI       := 0.00;
            end;
            1: // Unidade
            begin
              IPI_VBC        := QrProds1Total.Value; // ???
              IPI_QUnid      := QrProds1Qtde.Value;
              IPI_VUnid      := QrProds1IPI_vUnid.Value;
              IPI_PIPI       := 0.00;
              IPI_VIPI       := Round(IPI_QUnid * IPI_VUnid * 100) / 100 ;
            end;
            2: // Aliquota
            begin
              IPI_VBC        := QrProds1Total.Value;
              IPI_QUnid      := 0.00;
              IPI_VUnid      := 0.00;
              IPI_PIPI       := QrProds1IPI_pIPI.Value; //
              IPI_VIPI       := Round(IPI_VBC * IPI_PIPI) / 100;
            end;
          end;           // 2011-11-16
        if (IPI_Usa = 1) (*or (IPI_VIPI >= 0.01)*) then
          Tem_IPI := 1
        else
/// ini 2021-10-11
        if QrProds1IPI_TpTrib.Value > 0 then
          Tem_IPI := 1
        else
/// fim 2021-10-11
          Tem_IPI := 0;

        if (II_Usa = 1) (*or (II_VII >= 0.01)*) then
          Tem_II := 1
        else
          Tem_II := 0;
            { É Preenchido manualmente no form NFeItsI_0000
        II_VBC             := 0;
        II_VDespAdu        := 0;
        II_VII             := 0;
        II_VIOF            := 0;
           }

        // PIS
        if PIS_Usa = 1 then
        begin
          PIS_fatorBC       := 1; // 100%
          PIS_CST           := QrProds1PIS_CST.Value;
          PIS_vBC           := 0.00;
          PIS_pPIS          := 0.00;
          PIS_vPIS          := 0.00;
          PIS_qBCProd       := 0.00;
          PIS_vAliqProd     := 0.00;
          case PIS_CST of
            1,2:
            begin
              PIS_fatorBC       := (100 - QrProds1PIS_pRedBC.Value) / 100;
              // ini 2023-05-31
              // http://direitoecontabilidade.com.br/631-2/
              // Como calcular o PIS, COFINS, ICMS e IPI após a decisão do STF sobre a base de cálculo das contribuições?
              //
              if DModG.QrParamsEmpRegimPisCofins.Value = 1 then
              begin
                PIS_vBC           := (QrProds1Total.Value - ICMS_VICMS) * PIS_fatorBC;
              end else
              begin
              // Fim 2023-05-31
                PIS_vBC           := QrProds1Total.Value * PIS_fatorBC;
              end;
              PIS_pPIS          := QrProds1PIS_AlqP.Value;
              PIS_vPIS          := Round(PIS_vBC * PIS_pPIS) / 100;
            end;
            3:
            begin
              PIS_qBCProd       := QrProds1Qtde.Value;
              PIS_vAliqProd     := QrProds1PIS_AlqV.Value;
              PIS_vPIS          := Round(QrProds1Qtde.Value * QrProds1PIS_AlqV.Value) / 100;
            end;
            4,6,7,8,9:
            begin
              // Nada
            end;
            99:
            begin
              if QrProds1PIS_AlqV.Value > 0 then
              begin
                PIS_qBCProd     := QrProds1Qtde.Value;
                PIS_vAliqProd   := QrProds1PIS_AlqV.Value;
                PIS_vPIS        := Round(QrProds1Qtde.Value * QrProds1PIS_AlqV.Value) / 100;
              end else begin
                PIS_fatorBC       := (100 - QrProds1PIS_pRedBC.Value) / 100;
                // ini 2023-05-31
                // http://direitoecontabilidade.com.br/631-2/
                // Como calcular o PIS, COFINS, ICMS e IPI após a decisão do STF sobre a base de cálculo das contribuições?
                //
                if DModG.QrParamsEmpRegimPisCofins.Value = 1 then
                begin
                  PIS_vBC           := (QrProds1Total.Value - ICMS_VICMS) * PIS_fatorBC;
                end else
                begin
                  PIS_vBC           := QrProds1Total.Value * PIS_fatorBC;
                end;
                PIS_pPIS          := QrProds1PIS_AlqP.Value;
                PIS_vPIS          := Round(PIS_vBC * PIS_pPIS) / 100;
              end;
            end;
          end;
          // PISST
          PISST_fatorBCST   := 1; // 100%
          PISST_vBC         := 0.00;
          PISST_pPIS        := 0.00;
          PISST_qBCProd     := 0.00;
          PISST_vAliqProd   := 0.00;
          //PISST_vPIS        := 0.00;
          if QrProds1PISST_AlqV.Value > 0 then
          begin
            PISST_qBCProd   := QrProds1Qtde.Value;
            PISST_vAliqProd := QrProds1PISST_AlqV.Value;
            PISST_vPIS      := Round(QrProds1Qtde.Value * QrProds1PISST_AlqV.Value) / 100;
          end else begin
            PISST_fatorBCST := (100 - QrProds1PISST_pRedBCST.Value) / 100;
            PISST_vBC       := QrProds1Total.Value * PISST_fatorBCST;
            PISST_pPIS      := QrProds1PISST_AlqP.Value;
            PISST_vPIS      := Round(PISST_vBC * PISST_pPIS) / 100;
          end;
        end else
        begin
          // Não existe CST = zero
          PIS_CST := 1;
        end;
        //

        // COFINS
        if COFINS_Usa = 1 then
        begin
          COFINS_fatorBC       := 1; // 100%
          COFINS_CST           := QrProds1COFINS_CST.Value;
          COFINS_vBC           := 0.00;
          COFINS_pCOFINS       := 0.00;
          COFINS_vCOFINS       := 0.00;
          COFINS_qBCProd       := 0.00;
          COFINS_vAliqProd     := 0.00;
          case COFINS_CST of
            1,2:
            begin
              COFINS_fatorBC          := (100 - QrProds1COFINS_pRedBC.Value) / 100;
              // ini 2023-05-31
              // http://direitoecontabilidade.com.br/631-2/
              // Como calcular o COFINS, COFINS, ICMS e IPI após a decisão do STF sobre a base de cálculo das contribuições?
              //
              if DModG.QrParamsEmpRegimPisCofins.Value = 1 then  //  regime não cumulativo
              begin
                COFINS_vBC           := (QrProds1Total.Value - ICMS_VICMS) * COFINS_fatorBC;
              end else
              begin
              // Fim 2023-05-31
                COFINS_vBC              := QrProds1Total.Value * COFINS_fatorBC;
              end;
              COFINS_pCOFINS          := QrProds1COFINS_AlqP.Value;
              COFINS_vCOFINS          := Round(COFINS_vBC * COFINS_pCOFINS) / 100;
            end;
            3:
            begin
              COFINS_qBCProd          := QrProds1Qtde.Value;
              COFINS_vAliqProd        := QrProds1COFINS_AlqV.Value;
              COFINS_vCOFINS          := Round(QrProds1Qtde.Value * QrProds1COFINS_AlqV.Value) / 100;
            end;
            4,6,7,8,9:
            begin
              // Nada
            end;
            99:
            begin
              if QrProds1COFINS_AlqV.Value > 0 then
              begin
                COFINS_qBCProd     := QrProds1Qtde.Value;
                COFINS_vAliqProd   := QrProds1COFINS_AlqV.Value;
                COFINS_vCOFINS     := Round(QrProds1Qtde.Value * QrProds1COFINS_AlqV.Value) / 100;
              end else begin
                COFINS_fatorBC     := (100 - QrProds1COFINS_pRedBC.Value) / 100;
                // ini 2023-05-31
                // http://direitoecontabilidade.com.br/631-2/
                // Como calcular o COFINS, COFINS, ICMS e IPI após a decisão do STF sobre a base de cálculo das contribuições?
                //
                if DModG.QrParamsEmpRegimPisCofins.Value = 1 then  //  regime não cumulativo
                begin
                  COFINS_vBC           := (QrProds1Total.Value - ICMS_VICMS) * COFINS_fatorBC;
                end else
                begin
                // Fim 2023-05-31
                  COFINS_vBC         := QrProds1Total.Value * COFINS_fatorBC;
                end;
                COFINS_pCOFINS     := QrProds1COFINS_AlqP.Value;
                COFINS_vCOFINS     := Round(COFINS_vBC * COFINS_pCOFINS) / 100;
              end;
            end;
          end;
          // COFINSST
          COFINSST_fatorBCST   := 1; // 100%
          COFINSST_vBC         := 0.00;
          COFINSST_pCOFINS     := 0.00;
          COFINSST_qBCProd     := 0.00;
          COFINSST_vAliqProd   := 0.00;
          //COFINSST_vCOFINS     := 0.00;
          if QrProds1COFINSST_AlqV.Value > 0 then
          begin
            COFINSST_qBCProd   := QrProds1Qtde.Value;
            COFINSST_vAliqProd := QrProds1COFINSST_AlqV.Value;
            COFINSST_vCOFINS   := Round(QrProds1Qtde.Value * QrProds1COFINSST_AlqV.Value) / 100;
          end else begin
            COFINSST_fatorBCST := (100 - QrProds1COFINSST_pRedBCST.Value) / 100;
            COFINSST_vBC       := QrProds1Total.Value * COFINSST_fatorBCST;
            COFINSST_pCOFINS   := QrProds1COFINSST_AlqP.Value;
            COFINSST_vCOFINS   := Round(COFINSST_vBC * COFINSST_pCOFINS) / 100;
          end;
        end else begin
          // Não existe CST=zero
          COFINS_CST :=1;
        end;
        //
        //Informações adicionais do produto!
        InfAdProd := QrProds1InfAdProd.Value;
        InfAdCuztm := QrProds1InfAdCuztm.Value;

        // Comprimento
        if QrProds1MedidaC.Value > 0 then
        begin
          if QrProds1Medida1.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Medida1.Value;
          InfAdProd := InfAdProd + ' ' + FloatToStr(QrProds1MedidaC.Value);
          if QrProds1Sigla1.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Sigla1.Value;
        end;

        // Largura
        if QrProds1MedidaL.Value > 0 then
        begin
          if QrProds1Medida2.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Medida2.Value;
          InfAdProd := InfAdProd + ' ' + FloatToStr(QrProds1MedidaL.Value);
          if QrProds1Sigla2.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Sigla2.Value;
        end;

        // Altura
        if QrProds1MedidaA.Value > 0 then
        begin
          if QrProds1Medida3.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Medida3.Value;
          InfAdProd := InfAdProd + ' ' + FloatToStr(QrProds1MedidaA.Value);
          if QrProds1Sigla3.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Sigla3.Value;
        end;

        // Peso específico (ou densidade?)
        if QrProds1MedidaE.Value > 0 then
        begin
          if QrProds1Medida4.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Medida4.Value;
          InfAdProd := InfAdProd + ' ' + FloatToStr(QrProds1MedidaE.Value);
          if QrProds1Sigla4.Value <> '' then
          InfAdProd := InfAdProd + ' ' + QrProds1Sigla4.Value;
        end;

        // Encargos (cobrança) de customização!
        if QrProds1PercCustom.Value > 0 then
        begin
          if Trim(QrFilialSiglaCustm.Value) <> '' then
            InfAdProd := InfAdProd + ' ' + QrFilialSiglaCustm.Value;

          if QrFilialInfoPerCuz.Value = 1 then
            InfAdProd := InfAdProd + ' ' + Geral.FFT(QrProds1PercCustom.Value,
            2, siPositivo);
        end;
        if InfAdCuztm > 0 then
        begin
          QrNFEInfCuz.Close;
          QrNFEInfCuz.Database := Dmod.MyDB; // 2020-08-31
          QrNFEInfCuz.Params[0].AsInteger := InfAdCuztm;
          UnDmkDAC_PF.AbreQuery(QrNFEInfCuz, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
          //
          if QrNFEInfCuz.RecordCount > 0 then
            InfAdProd := InfAdProd + ' - ' + QrNFEInfCuzNome.Value;
        end;
        // ini 2021-06-26
        if VAR_APP_HABILITADO_IMPRESSAO_MARCA_OU_REFERENCIA then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXVend, Dmod.MyDB, [
          'SELECT gfm.Nome NO_MARCA, gg1.Referencia ',
          'FROM gragxvend cpl ',
          'LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle ',
          'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
          'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle ',
          'WHERE cpl.GraGruX=' + Geral.FF0(QrProds1CU_PRODUTO.Value),
          '']);
          if QrGraGXVend.RecordCount > 0 then
          begin
            xTmpPos := '';
            xTmpPre := '';
            //
            NFeUsaMarca         := DmProd.QrOpcoesGrad.FieldByName('NFeUsaMarca').AsInteger;
            NFePreMarca         := DmProd.QrOpcoesGrad.FieldByName('NFePreMarca').AsString;
            NFePosMarca         := DmProd.QrOpcoesGrad.FieldByName('NFePosMarca').AsString;
            NFeUsaReferencia    := DmProd.QrOpcoesGrad.FieldByName('NFeUsaReferencia').AsInteger;
            NFePreReferencia    := DmProd.QrOpcoesGrad.FieldByName('NFePreReferencia').AsString;
            NFePosReferencia    := DmProd.QrOpcoesGrad.FieldByName('NFePosReferencia').AsString;
            /////////////////////////// MARCA //////////////////////////////////
            xTmp1 := Trim(QrGraGXVendNO_MARCA.Value);
            if (NFeUsaMarca > 0) and (xTmp1 <> '') then
            begin
              if NFePreMarca <> '' then
                xTmp1 := Trim(NfePreMarca + ' ' + xTmp1);
              if NFePosMarca <> '' then
                xTmp1 := xTmp1 + ' ' + NfePosMarca;
              case NFeUsaMarca of
                0: ;// nada
                // Antes da descricao
                1: xTmpPre := xTmpPre + xTmp1;
                // Depois da descricao
                2: xTmpPos := xTmpPos + ' ' + xTmp1;
                else ; // nada
              end;
            end;
            ////////////////////////// REFERENCIA //////////////////////////////
            xTmp2 := Trim(QrGraGXVendReferencia.Value);
            if (NFeUsaReferencia > 0) and (xTmp2 <> '') then
            begin
              if NFePreReferencia <> '' then
                xTmp2 := Trim(NfePreReferencia + ' ' + xTmp2);
              if NFePosReferencia <> '' then
                xTmp2 := xTmp2 + ' ' + NfePosReferencia;
              case NFeUsaReferencia of
                0: ;// nada
                // Antes da descricao
                1: xTmpPre := xTmpPre +  ' ' + xTmp2;
                // Depois da descricao
                2: xTmpPos := xTmpPos + ' ' + xTmp2;
                else ; // nada
              end;
            end;
            InfAdProd := Trim(xTmpPre + ' ' + InfAdProd + ' ' + xTmpPos);
          end;
        end;
        // fim 2021-06-26
        //
        InfAdProd := Trim(InfAdProd);
        //
        obsCont_xCampo  := QrProds1obsCont_xCampo.Value;
        obsCont_xTexto  := QrProds1obsCont_xTexto.Value;
        obsFisco_xCampo := QrProds1obsFisco_xCampo.Value;
        obsFisco_xTexto := QrProds1obsFisco_xTexto.Value;
      end; // Fim sem Calculo!
    end else
    begin
      //////////////////////
      //   S E R V I Ç O  //
      //////////////////////
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando registro do item de serviço ' +
        IntToStr(nItem) + ' de ' + IntToStr(QrProds1.RecordCount) +
        ' na tabela de produtos e serviços da NFe');
      //
      QrServi1.Close;
      QrServi1.Database := Dmod.MyDB; // 2020-08-31
      QrServi1.Params[0].AsInteger := QrProds1Servico.Value;
      UnDmkDAC_PF.AbreQuery(QrServi1, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
      //
      prod_cProd     := FormatFloat('0', QrServi1Codigo.Value);
      prod_cEAN      := '';
      prod_xProd     := QrServi1Nome.Value;
      prod_NCM       := '';
      prod_CEST      := 0;
      prod_EXTIPI    := '';
      prod_genero    := 0;
      prod_CFOP      := Geral.SoNumero_TT(QrProds1CFOP.Value);
      prod_uCom      := QrServi1Sigla.Value;
      prod_qCom      := QrProds1Qtde.Value;
      prod_vUnCom    := QrProds1Preco.Value;
      prod_vProd     := QrProds1Total.Value;
      prod_indEscala := QrProds1prod_indEscala.Value;
      prod_cEANTrib  := '';
      prod_uTrib     := prod_uCom; // Parei aqui - é diferente?
      prod_qTrib     := prod_qCom;  // Parei aqui - é diferente?
      prod_vUnTrib   := prod_vUnCom; // Parei aqui - é diferente?
(*
      // Ini 2020-10-31
      prod_vFrete    := 0.00; // Parei aqui
      prod_vSeg      := 0.00; // Parei aqui
      prod_vDesc     := 0.00; // Parei aqui
      prod_vOutro    := 0.00; // Parei aqui  NFe 2.00*)
      //
      prod_vFrete    := QrProds1prod_vFrete.Value;
      prod_vSeg      := QrProds1prod_vSeg.Value;
      prod_vDesc     := QrProds1prod_vDesc.Value;
      prod_vOutro    := QrProds1prod_vOutro.Value;
(*
     // Fim 2020-10-31
*)
      prod_cBenef    := '';
      // itens diferenciados pelo CST (00,10,20,30,40,41,50,51,70,90)
      ICMS_Orig      := 0;
      ICMS_CST       := 0;
      //
      if CalculaAutomatico then
      begin
        InfAdProd      := QrServi1infAdProd.Value;
        InfAdCuztm     := 0;
        // ISSQN
        if ISS_Usa = 1 then
        begin
          ISSQN_vBC          := QrProds1Total.Value;
          ISSQN_vAliq        := ISS_Alq;
          ISSQN_vISSQN       := Round(QrProds1Total.Value * ISS_Alq) / 100;
          ISSQN_cMunFG       := Trunc(QrEmpresaCodMunici.Value);
          (*
          QrServico.Close;
          QrServico.Params[0].AsInteger := Servico;
          UnDmkDAC_PF.AbreQuery(QrServico, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
          ISSQN_cListServ    := QrServicocListServ.Value;
          *)
          ISSQN_cListServ    := QrServi1cListServ.Value;
          // Parei aqui # 2.00
          //ISSQN_cSitTrib  := '';
          //
          //Informações adicionais do produto!
          InfAdProd := QrServi1InfAdProd.Value;
          Geral.MensagemBox('Calcule os impostos manualmente, pois o ISSQN ' +
          'não tem seu cáculo implementado neste aplicativo!' + sLineBreak +
          'INFORME A DERMATEK SOBRE A FÓRMULA DE CÁLCULO', 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;
      end;

      X := FormatFloat('0', QrProds1CU_GRUPO.Value) + ' - ' + prod_xProd;
      (*
      if Length(prod_NCM) < 8 then
        Mensagem := 'Quantidade de números no NCM inválido para ' + X + '!';
      if Length(prod_CFOP) < 4 then
        Mensagem := 'Quantidade de números no CFOP inválido para ' + X + '!';
      if Length(prod_uCom) < 1 then
        Mensagem := 'Unidade comercial não definida para ' + X + '!';
      *)
      //
    end;
    if Mensagem <> '' then
    begin
       Geral.MB_Aviso(Mensagem);
       Screen.Cursor := crDefault;
       Exit;
    end else begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
        'prod_cProd', 'prod_cEAN', 'prod_xProd',
        'prod_NCM', 'prod_EXTIPI', 'prod_genero',
        'prod_CFOP', 'prod_uCom', 'prod_qCom',
        'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
        'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
        'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
        'prod_vOutro', 'Tem_IPI', 'InfAdCuztm',
        'prod_indTot', 'Tem_II', 'prod_CEST',
        'StqMovValA', 'prod_indEscala', 'prod_cBenef',
        'prod_cBarra', 'prod_cBarraTrib',
        '_Ativo_'
      ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
        prod_cProd, prod_cEAN, prod_xProd,
        prod_NCM, prod_EXTIPI, prod_genero,
        prod_CFOP, prod_uCom, prod_qCom,
        prod_vUnCom, prod_vProd, prod_cEANTrib,
        prod_uTrib, prod_qTrib, prod_vUnTrib,
        prod_vFrete, prod_vSeg, prod_vDesc,
        prod_vOutro, Tem_IPI, InfAdCuztm,
        prod_indTot, Tem_II, prod_CEST,
        StqMovValA, prod_indEscala, prod_cBenef,
        prod_cBarra, prod_cBarraTrib,
        1
      ], [FatID, FatNum, Empresa, nItem], True) then
      {
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
        'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
        'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
        'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
        'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
        'ICMS_vICMSST', 'motDesICMS', 'vBCSTRet',
        'vICMSSTRet',
        '_Ativo_'
      ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
        ICMS_Orig, ICMS_CST, ICMS_modBC,
        ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
        ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
        ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
        ICMS_vICMSST, motDesICMS, vBCSTRet,
        vICMSSTRet,
        1
      ], [FatID, FatNum, Empresa, nItem], True) then
      }
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
      'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
      'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
      'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
      'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
      'ICMS_vICMSST', '_Ativo_', 'ICMS_vBCSTRet',
      'ICMS_vICMSSTRet', 'ICMS_CSOSN', 'ICMS_UFST',
      'ICMS_pBCOp', 'ICMS_motDesICMS', 'ICMS_pCredSN',
      'ICMS_vCredICMSSN',
      // NFe 3.10
      'ICMS_vICMSOp', 'ICMS_pDif', 'ICMS_vICMSDif',
      'ICMS_vICMSDeson'
      //
      ], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      ICMS_Orig, ICMS_CST, ICMS_modBC,
      ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
      ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
      ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
      ICMS_vICMSST, 1, ICMS_vBCSTRet,
      ICMS_vICMSSTRet, ICMS_CSOSN, ICMS_UFST,
      ICMS_pBCOp, ICMS_motDesICMS, ICMS_pCredSN,
      ICMS_vCredICMSSN,
      // NFe 3.10
      ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif,
      ICMS_vICMSDeson
      //
      ], [
      FatID, FatNum, Empresa, nItem], True) then
      begin
        if GeraGrupoNA then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsna', False, [
          'ICMSUFDest_vBCUFDest', 'ICMSUFDest_pFCPUFDest',
          'ICMSUFDest_pICMSUFDest', 'ICMSUFDest_pICMSInter',
          'ICMSUFDest_pICMSInterPart', 'ICMSUFDest_vFCPUFDest',
          'ICMSUFDest_vICMSUFDest', 'ICMSUFDest_vICMSUFRemet'], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          ICMSUFDest_vBCUFDest, ICMSUFDest_pFCPUFDest,
          ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
          ICMSUFDest_pICMSInterPart, ICMSUFDest_vFCPUFDest,
          ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet], [
          FatID, FatNum, Empresa, nItem], True);
        end;
        // 2017-01-30 Início
        //if QrFilialNFeNT2013_003LTT.Value > 0 then
        if UnNFe_PF.VerificaSeCalculaTributos(QrFilialNFeNT2013_003LTT.Value,
          DModG.QrPrmsEmpNFeNFe_indFinalCpl.Value, indFinal) then
        // 2017-01-30 Fim
        begin
          pTotTrib      := 0;
          vTotTrib      := 0.0000;
          tabTotTrib    := Integer(TIBPTaxTabs.ibptaxtabNCM);
          verTotTrib    := '';
          tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNaoInfo);
          fontTotTrib   := Integer(TIBPTaxFont.ibptaxfontNaoInfo);
          //
          vTribFed       := 0.00;
          pTribFed       := 0.0000;
          vTribEst       := 0.00;
          pTribEst       := 0.0000;
          vTribMun       := 0.00;
          pTribMun       := 0.0000;
          iTotTrib := QrProds1iTotTrib.Value;
          if iTotTrib = 1 then
          begin
            vTotTrib := QrProds1vTotTrib.Value;
            pTotTrib := QrProds1pTotTrib.Value;
          end else
(*        ini 2020-11-01
            ObtemValorAproximadoDeTributos2(
              Geral.GetSiglaUF_do_CodigoUF(Trunc(QrEmpresaUF.Value)), prod_NCM,
              prod_EXTIPI, ICMS_Orig, prod_vProd, prod_vDesc, vBasTrib,
              pTotTrib, vTotTrib, tabTotTrib, verTotTrib, tpAliqTotTrib,
              fontTotTrib,
              vTribFed, pTribFed, vTribEst, pTribEst, vTribMun, pTribMun
              );
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsm', False, [
          'iTotTrib', 'vTotTrib', 'pTotTrib', 'vBasTrib',
          'tabTotTrib', 'verTotTrib', 'tpAliqTotTrib',
          'fontTotTrib'], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          iTotTrib, vTotTrib, pTotTrib, vBasTrib,
          tabTotTrib, verTotTrib, tpAliqTotTrib,
          fontTotTrib], [
          FatID, FatNum, Empresa, nItem], True);
        end;
*)
            ObtemValorAproximadoDeTributos2(
              Geral.GetSiglaUF_do_CodigoUF(Trunc(QrEmpresaUF.Value)), prod_NCM,
              prod_EXTIPI, ICMS_Orig, prod_vProd, prod_vDesc, vBasTrib,
              pTotTrib, vTotTrib, tabTotTrib, verTotTrib, tpAliqTotTrib,
              fontTotTrib,
              vTribFed, pTribFed, vTribEst, pTribEst, vTribMun, pTribMun
              );
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsm', False, [
          'iTotTrib', 'vTotTrib', 'pTotTrib', 'vBasTrib',
          'tabTotTrib', 'verTotTrib', 'tpAliqTotTrib',
          'fontTotTrib',  'vTribFed',
          'pTribFed', 'vTribEst', 'pTribEst',
          'vTribMun', 'pTribMun'], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          iTotTrib, vTotTrib, pTotTrib, vBasTrib,
          tabTotTrib, verTotTrib, tpAliqTotTrib,
          fontTotTrib, vTribFed,
          pTribFed, vTribEst, pTribEst,
          vTribMun, pTribMun], [
          FatID, FatNum, Empresa, nItem], True);
        end;
(*
          Fim 2020-11-01
*)
        //
        if QrFilialSimplesFed.Value = 0 then // não é super simples nacional
        begin
          if Tem_IPI > 0 then
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
              'IPI_clEnq', 'IPI_CNPJProd',
              'IPI_cSelo', 'IPI_qSelo', 'IPI_cEnq',
              'IPI_CST', 'IPI_vBC', 'IPI_qUnid',
              'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI',
              '_Ativo_'
            ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
              IPI_clEnq, IPI_CNPJProd,
              IPI_cSelo, IPI_qSelo, IPI_cEnq,
              IPI_CST, IPI_vBC, IPI_qUnid,
              IPI_vUnid, IPI_pIPI, IPI_vIPI,
              1
            ], [FatID, FatNum, Empresa, nItem], True) then Continua := True;
          end else Continua := True;
          if Continua then
          { É Preenchido manualmente no form NFeItsI_0000
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsp', False, [
            'II_vBC', 'II_vDespAdu', 'II_vII',
            'II_vIOF',
            '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            II_vBC, II_vDespAdu, II_vII,
            II_vIOF,
            1
          ], [FatID, FatNum, Empresa, nItem], True) then
          }
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsq', False, [
            'PIS_CST', 'PIS_vBC', 'PIS_fatorBC',
            'PIS_pPIS', 'PIS_vPIS', 'PIS_qBCProd',
            'PIS_vAliqProd', '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            PIS_CST, PIS_vBC, PIS_fatorBC,
            PIS_pPIS, PIS_vPIS, PIS_qBCProd,
            PIS_vAliqProd, 1
          ], [FatID, FatNum, Empresa, nItem], True) then
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsr', False, [
            'PISST_vBC', 'PISST_pPIS',
            'PISST_qBCProd', 'PISST_vAliqProd', 'PISST_vPIS',
            'PISST_fatorBCST', '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            PISST_vBC, PISST_pPIS,
            PISST_qBCProd, PISST_vAliqProd, PISST_vPIS,
            PISST_fatorBCST, 1
          ], [FatID, FatNum, Empresa, nItem], True) then
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitss', False, [
            'COFINS_CST', 'COFINS_vBC', 'COFINS_fatorBC',
            'COFINS_pCOFINS', 'COFINS_vCOFINS', 'COFINS_qBCProd',
            'COFINS_vAliqProd', '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            COFINS_CST, COFINS_vBC, COFINS_fatorBC,
            COFINS_pCOFINS, COFINS_vCOFINS, COFINS_qBCProd,
            COFINS_vAliqProd, 1
          ], [FatID, FatNum, Empresa, nItem], True) then
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitst', False, [
            'COFINSST_vBC', 'COFINSST_pCOFINS',
            'COFINSST_qBCProd', 'COFINSST_vAliqProd', 'COFINSST_vCOFINS',
            'COFINSST_fatorBCST', '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            COFINSST_vBC, COFINSST_pCOFINS,
            COFINSST_qBCProd, COFINSST_vAliqProd, COFINSST_vCOFINS,
            COFINSST_fatorBCST, 1
          ], [FatID, FatNum, Empresa, nItem], True) then
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsu', False, [
            'ISSQN_vBC',
            'ISSQN_vAliq', 'ISSQN_vISSQN', 'ISSQN_cMunFG',
            'ISSQN_cListServ',
            '_Ativo_'
          ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
            ISSQN_vBC,
            ISSQN_vAliq, ISSQN_vISSQN, ISSQN_cMunFG,
            ISSQN_cListServ,
            1
          ], [FatID, FatNum, Empresa, nItem], True) then

          ;

        end;
        if infAdProd <> '' then
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsv', False, [
          'InfAdProd',

          '_Ativo_'
        ], ['FatID', 'FatNum', 'Empresa', 'nItem'], [
          InfAdProd,
          1
        ], [FatID, FatNum, Empresa, nItem], True) //then
          (*Result := True*);

        if (obsCont_xCampo <> EmptyStr) or (obsFisco_xCampo <> EmptyStr) then
        begin
          VAItem := 1;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsva', False, [
          'obsCont_xCampo', 'obsCont_xTexto', 'obsFisco_xCampo',
          'obsFisco_xTexto'], [
          'FatID', 'FatNum', 'Empresa', 'nItem', 'VAItem'], [
          obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo,
          obsFisco_xTexto], [
          FatID, FatNum, Empresa, nItem, VAItem], True);
        end;
        //
      end;
    end;
    QrProds1.Next;
  end;
  //
  Result := QrProds1.RecordCount > 0;
  if not Result then
  begin
    Geral.MB_Aviso('Geração de NF-e abortada! Não há itens (mercadorias) para informar!');
  end else
  begin
    Result := Erros = 0;
    //
    if not Result then
      Geral.MB_Aviso('Geração de NF-e abortada! Erros ao gerar itens da NF-e!');
  end;
end;

procedure TDmNFe_0000.ReopenIBPTax1(Tabela: Integer; EX, prod_NCM, UF: String;
  var VigenFim: TDate);
var
  Qry: TmySQLQuery;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIBPTax, DModG.AllID_DB, [
    'SELECT * ',
    'FROM ibptax ',
    'WHERE Tabela="' + Geral.FF0(Tabela) + '"',
    'AND Codigo="' + Geral.SoNumero_TT(prod_NCM) + '"',
    'AND Ex="' + EX + '"',
    'AND UF="' + UF + '"',
    'AND VigenFim >= "' + Geral.FDT(DModG.ObtemAgora, 1) + '"',
    'ORDER BY VigenFim DESC',
    '']);
  //Verifica vigência fim
  Qry := TmySQLQuery.Create(TDataModule(DModG.AllID_DB.Owner));
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.AllID_DB, [
      'SELECT VigenFim ',
      'FROM ibptax ',
      'WHERE Tabela="' + Geral.FF0(Tabela) + '"',
      'AND Codigo="' + Geral.SoNumero_TT(prod_NCM) + '"',
      'AND Ex="' + EX + '"',
      'AND UF="' + UF + '"',
      'ORDER BY VigenFim DESC ',
      '']);
    //
    VigenFim := Qry.FieldByName('VigenFim').AsDateTime
  finally
    Qry.Free;
  end;
end;

procedure TDmNFe_0000.ReopenIBPTax2(Tabela: Integer; EX, prod_NCM, UF: String;
  var VigenFim: TDate);
begin
  VigenFim := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrIBPTax, DModG.AllID_DB, [
  'SELECT * ',
  'FROM ibptax ',
  'WHERE Tabela="' + Geral.FF0(Tabela) + '"',
  'AND Codigo="' + Geral.SoNumero_TT(prod_NCM) + '"',
  'AND Ex="' + EX + '"',
  'AND UF="' + UF + '"',
  //'AND VigenFim >= "' + Geral.FDT(DModG.ObtemAgora, 1) + '"',
  'ORDER BY VigenFim DESC',
  '']);
  if QrIBPTax.RecordCount > 0 then
    VigenFim := QrIBPTaxVigenFim.Value;
end;

procedure TDmNFe_0000.ReopenInfIntermedEnti(InfIntermedEnti: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrInfIntermedEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
    'en.IE, en.RG, en.NIRE, en.CNAE, ',
    'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
    'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
    'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
    'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
    'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
    'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
    'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
    'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
    'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
    'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
    'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF ',
    'FROM entidades en ',
    'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
    'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
    'WHERE en.Codigo=' + Geral.FF0(InfIntermedEnti),
    '']);
end;

procedure TDmNFe_0000.LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
  PageControl: TPageControl; ActivePageIndex: Integer);
var
  Arq: String;
begin
  Arq := ExtractFileDir(application.ExeName)+'\temp.xml';
  //
  MyWebBrowser.Visible := True;
  MyMemo.Lines.SaveToFile(Arq);
  MyWebBrowser.Navigate(Arq);
  DeleteFile(Arq);
  if (PageControl <> nil) and (ActivePageIndex > -1) then
    PageControl.ActivePageIndex := ActivePageIndex;
  Application.ProcessMessages;
end;

function TDmNFe_0000.LocalizaNFeInfoCanEve(const ChNFe: String; var Id, xJust: String;
  var cJust: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Id    := '';
  xJust := '';
  cJust := 0;
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, can.nJust, can.xJust',
    'FROM nfeevercab cab',
    'LEFT JOIN nfeevercan can ON can.Controle = cab.Controle',
    'WHERE cab.chNFe="' + ChNFe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Id     := Qry.FieldByName('Id').AsString;
      xJust  := Qry.FieldByName('xJust').AsString;
      cJust  := Qry.FieldByName('nJust').AsInteger;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.LocalizaNFeInfoCCe(const ChNFe: String; nSeqEvento:
  Integer; var CNPJ, CPF: String; var dhEvento: TDateTime; var dhEventoTZD,
  verEvento: Double; var nCondUso: Integer; var xCorrecao: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  CNPJ         := '';
  CPF          := '';
  dhEvento     := 0;
  dhEventoTZD  := 0;
  verEvento    := 0;
  nCondUso     := 0;
  xCorrecao    := '';
  //
  Result   := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT cab.Controle,',
      'cab.CNPJ, cab.CPF, cab.dhEvento, cab.TZD_UTC, cab.verEvento,',
      'cce.nCondUso, cce.xCorrecao, cab.nSeqEvento ',
      'FROM nfeevercab cab ',
      'LEFT JOIN nfeevercce cce ON cce.Controle = cab.Controle',
      'WHERE cab.chNFe="' + ChNFe + '"',
      'AND cab.nSeqEvento=' + Geral.FF0(nSeqEvento),
      'ORDER BY cce.Conta DESC',
      '']);
    if Qry.RecordCount > 0 then
    begin
      CNPJ        := Qry.FieldByName('CNPJ').AsString;
      CPF         := Qry.FieldByName('CPF').AsString;
      dhEvento    := Qry.FieldByName('dhEvento').AsDateTime;
      dhEventoTZD := Qry.FieldByName('TZD_UTC').AsFloat;
      verEvento   := Qry.FieldByName('verEvento').AsFloat;
      nCondUso    := Qry.FieldByName('nCondUso').AsInteger;
      xCorrecao   := Qry.FieldByName('xCorrecao').AsString;
      //
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.LocalizaNFeInfoEPECEve(const ChNFe: String;
  var Id: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Id    := '';
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id',
    'FROM nfeevercab cab',
    'LEFT JOIN nfeevercan can ON can.Controle = cab.Controle',
    'WHERE cab.chNFe="' + ChNFe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Id     := Qry.FieldByName('Id').AsString;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.LocalizaNFeInfoMDeEve(const ChNFe: String; var Id,
  xJust: String; var cJust, tpEvento: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Id       := '';
  xJust    := '';
  cJust    := 0;
  tpEvento := 0;
  Result   := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, mde.tpEvento, mde.xJust, mde.nJust ',
    'FROM nfeevercab cab ',
    'LEFT JOIN nfeevermde mde ON mde.Controle = cab.Controle ',
    'WHERE cab.chNFe="' + ChNFe + '"',
    'GROUP BY cab.Id ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Id       := Qry.FieldByName('Id').AsString;
      xJust    := Qry.FieldByName('xJust').AsString;
      cJust    := Qry.FieldByName('nJust').AsInteger;
      tpEvento := Qry.FieldByName('tpEvento').AsInteger;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.LocLctCabY(const FatID, FatNum, FatParcela, Empresa:
  Integer; var Controle: Integer): Boolean;
var
  TabLctA: String;
  Filial: Integer;
begin
  Filial := DModG.ObtemFilialDeEntidade(Empresa);
  if Filial <> 0 then
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial)
  else
    TabLctA := sTabLctErr;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCabY, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND FatParcela=' + Geral.FF0(FatParcela),
  'AND CliInt=' + Geral.FF0(Empresa),
  '']);
  //
  Controle := QrLocCabYControle.Value;
  Result := Controle <> 0;
end;

function TDmNFe_0000.DesfazEncerramento(Tabela: String; FatID, FatNum, Empresa:
Integer; AutorizarBoss: Boolean = False): Boolean;
var
  Serie, nNF, Filial: Integer;
  TabLctA: String;
begin
  Result := False;
  if AutorizarBoss then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  end else
  begin
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    ReopenNFeCabA(FatID, FatNum, Empresa);
    Serie := QrNFeCabAide_Serie.Value;
    nNF   := QrNFeCabAide_nNF.Value;
    //  Exclui NFs geradas
    UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovNFsA, Dmod.MyDB, [
    'SELECT *  ',
    'FROM stqmovnfsa ',
    'WHERE Tipo=' + Geral.FF0(VAR_FATID_0001),
    'AND OriCodi=' + Geral.FF0(FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    //DmPediVda.QrFPC1Codigo.Value
    '']);
    // buscar dados antes de excluir!
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 ');
    Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 ');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    // Exclui lançamentos de contas a receber
{$IfDef DEFINE_VARLCT}
    Filial := DmFatura.ObtemFilialDeEntidade(Empresa);
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum, Empresa, 0,
    dmkPF.MotivDel_ValidaCodigo(311), False, TabLctA);
{$Else}
    UFinanceiro.ExcluiLct_FatNum(
      QrNFeCabA.Database, FatID, FatNum, Empresa, 0, False);
{$EndIf}
    //
    // Desativa itens no movimento
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=0');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    // Altera status de encerramento
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + Lowercase(Tabela) + ' SET ');
    Dmod.QrUpd.SQL.Add('Status=8, Encerrou=0, DataFat=0, ');
    Dmod.QrUpd.SQL.Add('SerieDesfe=:P0, ');
    Dmod.QrUpd.SQL.Add('NFDesfeita=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := Serie;
    Dmod.QrUpd.Params[01].AsInteger := nNF;
    Dmod.QrUpd.Params[02].AsInteger := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmNFe_0000.UpdSerieNFDesfeita(Tabela: String; FatNum, Serie,
  nNF: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + Lowercase(Tabela) + ' SET ',
    'SerieDesfe=' + Geral.FF0(Serie) + ', ',
    'NFDesfeita=' + Geral.FF0(nNF),
    'WHERE Codigo=' + Geral.FF0(FatNum),
    '']);
end;

function TDmNFe_0000.DesmontaID_NFe(const ID: String; var Gru, Ext: String;
var Codigo: Integer): Boolean;
var
  I: Integer;
  X: Char;
  Cod: String;
begin
  X := ' ';
  Gru := '';
  Cod := '';
  Ext := '';
  i := 0;
  while not (X in (['0'..'9'])) do
  begin
    i := i + 1;
    X := ID[i];
    if not (X in (['0'..'9'])) then
      Gru := Gru + X
    else
      Cod := X;
  end;
  while X in (['0'..'9']) do
  begin
    i := i + 1;
    X := ID[i];
    if X in (['0'..'9']) then
      Cod := Cod + X
    else
      Ext := X;
  end;
  while (*not X in (['0'..'9']) and*) (i < Length(ID)) do
  begin
    i := i + 1;
    X := ID[i];
    //if not X in (['0'..'9']) then
      Ext := Ext + X
    //else
      //??? := X;
  end;
  Result := True;
end;

function TDmNFe_0000.MensagemDeID_NFe(Codigo, ID, Valor, Texto: String;
ID_Chamou: Integer): String;
var
  Txt, Obs: String;
begin
  if Valor = '' then
    Txt := '(valor não definido)'
  else
    Txt := Valor;
  //
  Result := 'TDmNFe_0000.MensagemDeID_NFe()'+sLineBreak+
            'ID Chamada: ' + FormatFloat('0', ID_Chamou) + sLineBreak +
            'Não foi possível definir o item abaixo pelo layout da NF-e:' +
  sLineBreak +
  sLineBreak + 'Grupo: ' + QrNFeLayINO_Grupo.Value +
  sLineBreak + 'Campo: ' + QrNFeLayIDescricao.Value +
  sLineBreak + 'Valor: ' + Txt;
  //
  //if Trim(Texto) <> '' then
  Result := Result + sLineBreak + 'Motivo: ' + Texto;
  //
  Obs := Trim(QrNFeLayIObservacao.Value);
  if Obs <> '' then
  Result := Result + sLineBreak + 'Observações: ' + Obs;
  //
  Result := Result + sLineBreak + sLineBreak +
    'Identificadores no "Manual de integração - Contribuinte":' +
    sLineBreak + '# : ' + Codigo + sLineBreak + 'ID: ' + ID;
end;

function TDmNFe_0000.MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime;
  CNPJ: String; Modelo, Serie, NumeroNF: Integer; var ide_cNF: Integer;
  var cDV: String; var ChaveDeAcesso: String; const cNF_Atual, ide_tpEmis: Integer;
  versao: double; MostraMsg: Boolean = True): Boolean;
var
  UF_IBGE, AAMM, ModNFe, Mensagem, SerNFe, NumNFe, AleNFe, tpEmis: String;
begin
  Mensagem := '';
  Result := False;
  UF_IBGE := FormatFloat('00', cUF);
  if (cUF < FMinUF_IBGE) or (cUF > FMaxUF_IBGE) then
    Mensagem := 'Código IBGE da UF inválido: ' + UF_IBGE;
  if Length(UF_IBGE) <> 2 then
    Mensagem := 'Código IBGE da UF inválido: ' + UF_IBGE;
  //
  AAMM := FormatDateTime('YYMM', Emissao);
  if Emissao < 2 then
    Mensagem := 'Ano/mês de emissão inválido: ' + AAMM;
  //
  if Length(CNPJ) < 14 then
    Mensagem := 'Tamanho inválido para o CNPJ!';
  //
  ModNFe := FormatFloat('00', Modelo);
  if Length(ModNFe) <> 2 then
    Mensagem := 'Modelo de NFe inválido: ' + ModNFe;
  //
  SerNFe := FormatFloat('000', Serie);
  if Length(SerNFe) <> 3 then
    Mensagem := 'Série da NFe inválida: ' + SerNFe;
  //
  NumNFe := FormatFloat('000000000', NumeroNF);
  if Length(NumNFe) <> 9 then
    Mensagem := 'Número de NFe inválido: ' + NumNFe;
  //
  if Versao < 2.00 then
  begin
    // versão 1.10
    if cNF_Atual <> 0 then
    begin
      ide_cNF := cNF_Atual;
      //
      if MostraMsg then
        Geral.MB_Aviso('Foi detectado que o código numérico já existe para esta NF-e.'
          + sLineBreak + 'Será utilizado o mesmo: ' + FormatFloat('000000000', ide_cNF));
    end else begin
      //  Número aleatório
      Randomize;
      ide_cNF := 0;
      while (ide_cNF <= 0) or (ide_cNF > 999999998) do
        ide_cNF := Random(1000000000);
    end;
    //
    AleNFe := FormatFloat('000000000', ide_cNF);
    if Length(AleNFe) <> 9 then
      Mensagem := 'Código numérico da chave da NFe inválido: ' + AleNFe;
    //
    ChaveDeAcesso := UF_IBGE + AAMM + CNPJ + ModNFe + SerNFe + NumNFe + AleNFe;
  end else begin
    // versão 2.00
    if (cNF_Atual <> 0) and (cNF_Atual < 100000000) then
    begin
      ide_cNF := cNF_Atual;
      //
      if MostraMsg then
        Geral.MB_Aviso('Foi detectado que o código numérico já existe para esta NF-e.'
          + sLineBreak + 'Será utilizado o mesmo: ' + FormatFloat('00000000', ide_cNF));
    end else
    begin
      //  Número aleatório
      Randomize;
      ide_cNF := 0;
      while (ide_cNF <= 0) or (ide_cNF > 99999998) do
        ide_cNF := Random(100000000);
    end;
    //
    AleNFe := FormatFloat('00000000', ide_cNF);
    if Length(AleNFe) <> 8 then
      Mensagem := 'Código numérico da chave da NFe inválido: ' + AleNFe;
    //
    tpEmis := FormatFloat('0', ide_tpEmis);
    if (ide_tpEmis < 1) or (ide_tpEmis > 9) then
      Mensagem := 'Tipo de emissão (#25 ID: B21) inválido: ' + tpEmis;
    //
    if ((ide_tpEmis <> 3) and (Serie >= 900))
    or ((ide_tpEmis = 3) and (Serie < 900)) then
      Mensagem := 'Tipo de emissão (#25 ID: B21): "' + tpEmis +
      '" é incompatível com a série: "' + SerNFe + '"';

    // Nova concatenação para a chave de acesso na NFe 2.0
    ChaveDeAcesso :=
      UF_IBGE + AAMM + CNPJ + ModNFe + SerNFe + NumNFe + tpEmis + AleNFe;
  end;
  cDV := Geral.Modulo11_2a9_Back(ChaveDeAcesso, 0);
  ChaveDeAcesso := ChaveDeAcesso + cDV;
  if Length(ChaveDeAcesso) <> 44 then
    Mensagem := 'Tamanho da chave de acesso inválido. Deveria ser 44 mas é ' +
    IntToStr(Length(ChaveDeAcesso));
  if Mensagem <> '' then
    Geral.MB_Aviso(Mensagem + #13+#10 + 'Function: "MontaChaveDeAcesso"')
  else
    Result := True;
end;

//  TipoStqMovIts
function TDmNFe_0000.NomeFatID_NFe(FatID: Integer): String;
begin
  case FatID of
    (*PQx*)VAR_FATID__006 : Result := 'Balanco de PQx (diferenca do lancamento)';
    (*PQx*)VAR_FATID__005 : Result := 'Retorno de PQ sem entrada e/ou baixa no estoque para retorno (Retorno antecipado e incorreto)!)';
    (*PQx*)VAR_FATID__004 : Result := 'Acerto inicial de retorno de insumo (baixa)';   // Baixa de Baixa de PQ (de retorno sem emissao de NFe)
    (*PQx*)VAR_FATID__003 : Result := 'Acerto inicial de retorno de insumo (entrada)'; // Baixa de entrada de PQ (de retorno sem emissao de NFe)
    VAR_FATID__002 : Result := 'Consumo programado'; // Baixa retroativa forçada
    VAR_FATID__001 : Result := 'Balanço de Uso e consumo'; // Balanco
    VAR_FATID_0000 : Result := 'Balanço de Grade'; // Balanco
    // Mudado em 2020-10-24
    //VAR_FATID_0001 : Result := 'Faturamento de Pedido'; // Faturanento de pedido
    //VAR_FATID_0002 : Result := 'Faturamento sem Pedido'; // Faturamento sem pedido (balcão também?)
    VAR_FATID_0001 : Result := 'Faturamento NF-e';
    VAR_FATID_0002 : Result := 'Faturamento NFC-e';
    // fim 2020-10-24
    VAR_FATID_0003 : Result := 'Saída Condicional'; // Saída condicional
    VAR_FATID_0004 : Result := 'Retorno Condicional'; // Retorno Condicional
    VAR_FATID_0005 : Result := 'Entrada por compra'; // Entrada por compra

   (*PQx*)VAR_FATID_0010 : Result := 'Entrada de Insumo por NF (compra ou remessa de industrializacao)';
   (*PQx*)VAR_FATID_0030 : Result := 'Entrada de Insumo por diluição / mistura';

    VAR_FATID_0013 : Result := 'Entrada de Itens de Matéria-prima por NFe'; // MPinIts (NFe ) no BlueDerm

    VAR_FATID_0051 : Result := 'Entrada de Uso e Consumo por NFe'; // Entrada de Uso e consumo por NFe
    //VAR_FATID_0052 : Result := 'Entrada de Uso e Consumo por importação de SPED EFD'; //Entrada de Uso e Consumo por SPED EFD
    VAR_FATID_0053 : Result := 'NFe de entrada por XML'; // Entrada de Uso e consumo por NFe

    VAR_FATID_0099 : Result := 'Venda Avulsa'; // Avulso
    //
    VAR_FATID_0100 : Result := '????????'; // Usado apenas para separar as entradas das saídas de PQx!!!!!!!!!!!!!
    VAR_FATID_0101 : Result := 'Gerado em classificação de WB'; // Classificação de WB no Blue derm
    VAR_FATID_0102 : Result := 'Devolução de Matéria-prima de terceiros'; // CMPTOut no BlueDerm
    //VAR_FATID_0103 : Result := 'Entrada de Matéria-prima por compra'; // MPInn no BlueDerm
    VAR_FATID_0104 : Result := 'Baixa por classificação de WB'; // Classificação de WB no Blue derm
    VAR_FATID_0105 : Result := 'Gerado por envio para industrialização por terceiros'; // Gerado por envio para industrialização por terceiros no Blue derm
    VAR_FATID_0106 : Result := 'Baixa por envio para industrialização por terceiros'; // Baixa por envio para industrialização por terceiros no Blue derm
    VAR_FATID_0107 : Result := 'Gerado por reclassificação de WB'; // Gerado por reclassificação de WB no Blue derm
    VAR_FATID_0108 : Result := 'Baixa por reclassificação de WB'; // Baixa por reclassificação de WB no Blue derm

    (*PQx*)VAR_FATID_0110 : Result := 'Baixa de Insumo por receita '; // PQx Baixa de PQ
    (*PQx*)VAR_FATID_0130 : Result := 'Baixa de Insumo por diluição / mistura ';

    VAR_FATID_0113 : Result := 'Entrada de Matéria-prima por NF normal'; // MPinIts (NF normal) no BlueDerm

    (*PQx*)VAR_FATID_0150 : Result := 'Uso e consumo - baixa de ETE'; // Baixa ETE no BlueDerm (Uso e consumo)
    VAR_FATID_0151 : Result := 'Entrada de Uso e Consumo por NF normal'; // Entrada de Uso e consumo por NF modelo normal no Blue Derm
    //
    (*PQx*)VAR_FATID_0170 : Result := 'Uso e consumo - devolução de outros'; // Baixa por devolução no Blue Derm (Uso e consumo)
    //
    (*PQx*) VAR_FATID_0180 : Result := 'Baixa de IPI';
    //
    (*PQx*) VAR_FATID_0185 : Result := 'Baixa de Material de Manutenção';
    //
    (*PQx*)VAR_FATID_0190 : Result := 'Uso e consumo - baixa de outros'; // Baixa de Outros no Blue Derm (Uso e consumo)

    VAR_FATID_0213 : Result := 'Entrada de Matéria-prima por SPED EFD'; // MPinIts (SPED EFD) no BlueDerm

    VAR_FATID_0251 : Result := 'Entrada de Uso e Consumo por SPED EFD'; // Entrada de Uso e consumo por SPED EFD no Blue Derm
    //
    VAR_FATID_1003: Result := 'Entrada de matéria-prima in natura';
    VAR_FATID_1041: Result := 'Entrada de matéria prima '; // wet blue

    VAR_FATID_1110 : Result := 'Uso e Consumo - baixa em receita'; // Baixa por Pesagem no Blue Derm (Uso e consumo)
    VAR_FATID_1111 : Result := 'Uso e Consumo - baixa extra'; // Baixa por Consumo extra no Blue Derm (Uso e consumo)
    //
    VAR_FATID_1150 : Result := 'Uso e consumo - baixa de ETE'; // Baixa ETE no BlueDerm (Uso e consumo)
    //
    VAR_FATID_1190 : Result := 'Uso e consumo - baixa de outros'; // Baixa de Outros no Blue Derm (Uso e consumo)
    //
    VAR_FATID_1901 : Result := 'Frete'; // Frete
    //
    VAR_FATID_4001: Result := 'Faturamento de serviço'; // Faturamento de serviço
    VAR_FATID_4101: Result := 'Fórmula de aplicacação'; //Baixa na OS (tabela osfrmrec)
    VAR_FATID_4102: Result := 'Fórmula de monitoramento'; //Baixa na OS (tabela osmomrec)
    VAR_FATID_4103: Result := 'Monitoramento > adição'; //Baixa na OS (tabela ospipitspr > adiciona)
    VAR_FATID_4104: Result := 'Monitoramento > substituição'; //Baixa na OS (tabela ospipitspr > substitui)
    //
    VAR_FATID_999999999: Result := 'Emissão manual de NFe'; // Emissão manual de NFe
    else Result := '? ? ?';
  end;
end;

function TDmNFe_0000.NotaDenegada(cStat: Integer): Boolean;
begin
  Result := (cStat > 300) and (cStat < 400);
end;

function TDmNFe_0000.NotaRejeitada(cStat: Integer): Boolean;
begin
  Result :=
    ((cStat > 200) and (cStat < 300))
  or
     (CStat > 400);
end;

function TDmNFe_0000.TituloArqXML(const Ext: String; var Titulo, Descri: String): Boolean;
begin
  Result := True;
  if Ext = NFE_EXT_NFE_XML then
  begin
    Titulo := 'NF-e';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-nfe.xml”';
  end else
  if Ext = NFE_EXT_ENV_LOT_XML then
  begin
    Titulo := 'Envio de Lote de NF-e';
    Descri := 'O nome do arquivo será o número do lote com extensão “-env-lot.xml”';
  end else
  if Ext = NFE_EXT_REC_XML then
  begin
    Titulo := 'Recibo';
    Descri := 'O nome do arquivo será o número do lote com extensão “-rec.xml”';
  end else
  if Ext = NFE_EXT_PED_REC_XML then
  begin
    Titulo := 'Pedido do Resultado do Processamento do Lote de NF-e';
    Descri := 'O nome do arquivo será o número do recibo com extensão “-ped-rec.xml”';
  end else
  if Ext = NFE_EXT_PRO_REC_XML then
  begin
    Titulo := 'Resultado do Processamento do Lote de NF-e';
    Descri := 'O nome do arquivo será o número do recibo com extensão “-pro-rec.xml”';
  end else
  if Ext = NFE_EXT_DEN_XML then
  begin
    Titulo := 'Denegação de Uso';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-den.xml”';
  end else
  if Ext = NFE_EXT_PED_CAN_XML then
  begin
    Titulo := 'Pedido de Cancelamento de NF-e';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-ped-can.xml”';
  end else
  if Ext = NFE_EXT_CAN_XML then
  begin
    Titulo := 'Cancelamento de NF-e';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-can.xml”';
  end else
  if Ext = NFE_EXT_PED_INU_XML then
  begin
    Titulo := 'Pedido de Inutilização de Numeração';
    Descri := 'O nome do arquivo será composto por: UF + Ano de inutilização + CNPJ do emitente + Modelo + Série + Número Inicial + Número Final com extensão “-ped-inu.xml”';
  end else
  if Ext = NFE_EXT_INU_XML then
  begin
    Titulo := 'Inutilização de Numeração';
    Descri := 'O nome do arquivo será composto por: Ano de inutilização + CNPJ do emitente + Modelo + Série + Número Inicial + Número Final com extensão “-inu.xml”';
  end else
  if Ext = NFE_EXT_PED_SIT_XML then
  begin
    Titulo := 'Pedido de Consulta Situação Atual da NF-e';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-ped-sit.xml”';
  end else
  if Ext = NFE_EXT_SIT_XML then
  begin
    Titulo := 'Situação Atual da NF-e';
    Descri := 'O nome do arquivo será a chave de acesso completa com extensão “-sit.xml”';
  end else
  if Ext = NFE_EXT_PED_STA_XML then
  begin
    Titulo := 'Pedido de Consulta do Status do Serviço';
    Descri := 'O nome do arquivo será: “AAAAMMDDTHHMMSS” do momento da consulta com extensão “-ped-sta.xml”';
  end else
  if Ext = NFE_EXT_STA_XML then
  begin
    Titulo := 'Status do Serviço';
    Descri := 'O nome do arquivo será: “AAAAMMDDTHHMMSS” do momento da consulta com extensão “-sta.xml”';
  end else
  begin
    Result := False;
    Titulo := 'Tipo de arquivo XML desconhecido para NF-e';
    Descri := 'AVISE A DERMATEK!';
  end;
end;

function TDmNFe_0000.TotaisNFe(FatID, FatNum, Empresa: Integer;
  LaAviso1, LaAviso2: TLabel; REWarning: TRichEdit): Boolean;
var
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST,
  ICMSTot_vProd, ICMSTot_vServ, ICMSTot_vFrete, ICMSTot_vSeg,
  ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro,
  ICMSTot_vNF, ISSQNtot_vServ, ISSQNtot_vBC,
  ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
  RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
  RetTrib_vRetPrev, ICMSTot_vICMSDeson, ICMSTot_vFCP: Double;
  Cobr_Fat_nFat: String;
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq: Double;
  SQL_FAT_TOT,
  prod_NCM, prod_EXTIPI, EX: String;
  //
  FreteExtra, SegurExtra, vBasTrib, pTotTrib: Double;
  FonteStr, InfCpl_totTrib: String;
  indFinal: TNFeIndFinal;
  InformaTribCpl: Boolean;
  Qry: TmySQLQuery;
  VigenFim: TDate;
  ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet: Double;
  vTotTrib, vTribFed, vTribEst, vTribMun: Double;
  ICMSTot_vIPIDevol, ICMSTot_vFCPST: Double;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo totais da NF-e');

  ReopenNFeCabA(FatID, FatNum, Empresa);
  //
  FreteExtra := QrNFeCabAFreteExtra.Value;
  SegurExtra := QrNFeCabASegurExtra.Value;
  indFinal   := TNFeIndFinal(QrNFeCabAide_indFinal.Value);
  //
(*
object QrNFeTotA: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT ICMSTot_vOutro'
    'FROM nfecaba'
    'WHERE FatID=:P0'
    'AND FatNum=:P1'
    'AND Empresa=:P2')
  Left = 504
  Top = 340
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end
    item
      DataType = ftUnknown
      Name = 'P1'
      ParamType = ptUnknown
    end
    item
      DataType = ftUnknown
      Name = 'P2'
      ParamType = ptUnknown
    end>
  object QrNFeTotAICMSTot_vOutro: TFloatField
    FieldName = 'ICMSTot_vOutro'
  end
end
*)
(*
  QrNFeTotI.Close;
  QrNFeTotI.Params[00].AsInteger := FatID;
  QrNFeTotI.Params[01].AsInteger := FatNum;
  QrNFeTotI.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //IIIII
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotI, Dmod.MyDB, [
  'SELECT SUM(Tem_IPI) Tem_IPI, ',
  'SUM(IF(prod_indTot=1, prod_vProd, 0)) prod_vProd, ',
  'SUM(prod_vFrete) prod_vFrete, ',
  'SUM(prod_vSeg) prod_vSeg, ',
  'SUM(prod_vDesc) prod_vDesc, ',
  'SUM(prod_vOutro) prod_vOutro ',
  'FROM nfeitsi ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  //
  'AND EhServico=0 ',
  //
  '']);
  //
(*
  QrNFeTotM.Close;
  QrNFeTotM.Params[00].AsInteger := FatID;
  QrNFeTotM.Params[01].AsInteger := FatNum;
  QrNFeTotM.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotM, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
//MMMMM
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotM, Dmod.MyDB, [
  'SELECT ',
  'SUM(vTribFed) vTribFed, ',
  'SUM(vTribEst) vTribEst, ',
  'SUM(vTribMun) vTribMun, ',
  'SUM(vTotTrib) vTotTrib, ',
  'SUM(vBasTrib) vBasTrib  ',
  'FROM nfeitsm ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  //
(*
  QrNFeTotN.Close;
  QrNFeTotN.Params[00].AsInteger := FatID;
  QrNFeTotN.Params[01].AsInteger := FatNum;
  QrNFeTotN.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //NNNNN
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotN, Dmod.MyDB, [
  'SELECT ',
  'SUM(ICMS_vBC) ICMS_vBC, ',
  'SUM(ICMS_vICMS) ICMS_vICMS, ',
  'SUM(ICMS_vICMSDeson) ICMS_vICMSDeson, ',
  'SUM(ICMS_vBCST) ICMS_vBCST, ',
  'SUM(ICMS_vICMSST) ICMS_vICMSST, ',
  'SUM(ICMS_vFCP) ICMS_vFCP ',
  'FROM nfeitsn ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  //NA
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotNA, Dmod.MyDB, [
  'SELECT ',
  'SUM(ICMSUFDest_vFCPUFDest) ICMSTot_vFCPUFDest, ',
  'SUM(ICMSUFDest_vICMSUFDest) ICMSTot_vICMSUFDest, ',
  'SUM(ICMSUFDest_vICMSUFRemet) ICMSTot_vICMSUFRemet ',
  'FROM nfeitsna ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
(*
  QrNFeTotO.Close;
  QrNFeTotO.Params[00].AsInteger := FatID;
  QrNFeTotO.Params[01].AsInteger := FatNum;
  QrNFeTotO.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //OOOOO
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotO, Dmod.MyDB, [
  'SELECT ',
  'SUM(IPI_vIPI) IPI_vIPI ',
  'FROM nfeitso ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
(*
  QrNFeTotP.Close;
  QrNFeTotP.Params[00].AsInteger := FatID;
  QrNFeTotP.Params[01].AsInteger := FatNum;
  QrNFeTotP.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotP, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //PPPP
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotP, Dmod.MyDB, [
  'SELECT SUM(II_vII) II_vII ',
  'FROM nfeitsp ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
(*
  QrNFeTotQ.Close;
  QrNFeTotQ.Params[00].AsInteger := FatID;
  QrNFeTotQ.Params[01].AsInteger := FatNum;
  QrNFeTotQ.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotQ, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //QQQQ
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotQ, Dmod.MyDB, [
  'SELECT SUM(PIS_vPIS) PIS_vPIS ',
  'FROM nfeitsq ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
(*
  QrNFeTotS.Close;
  QrNFeTotS.Params[00].AsInteger := FatID;
  QrNFeTotS.Params[01].AsInteger := FatNum;
  QrNFeTotS.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotS, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //
  //SSSS
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotS, Dmod.MyDB, [
  'SELECT SUM(COFINS_vCOFINS) COFINS_vCOFINS ',
  'FROM nfeitss ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
(*
  QrNFeTotU.Close;
  QrNFeTotU.Params[00].AsInteger := FatID;
  QrNFeTotU.Params[01].AsInteger := FatNum;
  QrNFeTotU.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrNFeTotU, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  //
  //UUUUU
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeTotU, Dmod.MyDB, [
  'SELECT SUM(ISSQN_vBC) ISSQN_vBC, ',
  'SUM(ISSQN_vISSQN) ISSQN_vISSQN ',
  'FROM nfeitsu ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  //
  //
  ICMSTot_vBC         := QrNFeTotNICMS_vBC.Value;
  ICMSTot_vICMS       := QrNFeTotNICMS_vICMS.Value;
  // NFe 3.10
  ICMSTot_vICMSDeson  := QrNFeTotNICMS_vICMSDeson.Value;
  //
  ICMSTot_vFCP        := QrNFeTotNICMS_vFCP.Value;
  ICMSTot_vBCST       := QrNFeTotNICMS_vBCST.Value;
  ICMSTot_vST         := QrNFeTotNICMS_vICMSST.Value;
  ICMSTot_vProd       := QrNFeTotIprod_vProd.Value;
  ICMSTot_vServ       := 0; // Parei Aqui!!! Fazer Qry total do U-ISSQN (QrItsU)
  ICMSTot_vFrete      := QrNFeTotIprod_vFrete.Value + FreteExtra;
  ICMSTot_vSeg        := QrNFeTotIprod_vSeg.Value + SegurExtra;
  ICMSTot_vDesc       := QrNFeTotIprod_vDesc.Value;
  ICMSTot_vII         := QrNFeTotPII_vII.Value;
  ICMSTot_vIPI        := QrNFeTotOIPI_vIPI.Value;
  ICMSTot_vPIS        := QrNFeTotQPIS_vPIS.Value;
  ICMSTot_vCOFINS     := QrNFeTotSCOFINS_vCOFINS.Value;
  ICMSTot_vOutro      := QrNFeTotIprod_vOutro.Value;
  //
  ICMSTot_vIPIDevol   := QrNFEItsUAimpostoDevol_vIPIDevol.Value;
  ICMSTot_vFCPST      := QrNFEItsNICMS_vFCPST.Value;
{
________________________________________________________________________________
_____ Nota Técnica 2013.005_____________________________________________________
                                                                    pág 119/141
________________________________________________________________________________
W16-10 55/65 -Total do vNF (id:W16) difere do somatório de:
//##############################################      // 2021-10-20 ############
         (+) vProd (id:W07)                       (+) vProd (id:W07)
         (-) vDesc (id:W10)                       (-) vDesc (id:W10)
         (-) vICMSDeson (id:W04a)                 (-) vICMSDeson (id:W04a)
         (+) vST (id:W06)                         (+) vST (id:W06)
         (+) vFrete (id:W08)                      (+) vFCPST (id:W06a)
         (+) vSeg (id:W09)                        (+) vFrete (id:W08)
         (+) vOutro (id:W15)                      (+) vSeg (id:W09)
         (+) vII (id:W11)                         (+) vOutro (id:W15)
         (+) vIPI (id:W12)                        (+) vII (id:W11)
         (+) vServ (id:W18) (*3)                  (+) vIPI (id:W12)
                                                  (+) vIPIDevol (id: W12a)
(NT 2011/005) Exceção 1:
Faturamento direto de veículos novos: Se informad(+) vServ (id:W18) (*3) (NT 2011/005)a operação de Faturamento
Direto para veículos novos (tpOp = 2, id:J02):
– Total do vNF (id:W16) difere do somatório de:
         (+) vProd (id:W07)
         (-) vDesc (id:W10)
         (-) vICMSDeson (id:W04a)
         (+) vFrete (id:W08)
         (+) vSeg (id:W09)
         (+) vOutro (id:W15)
         (+) vII (id:W11)
         (+) vIPI (id:W12)
         (+) vServ (id:W18) (*3)
(NT 2011/005) Exceção 2:
Esta regra não se aplica nas operações de importação (CFOP inicia com “3”).
________________________________________________________________________________
}

  ICMSTot_vNF         := ICMSTot_vProd      -
                         ICMSTot_vDesc      -
                         ICMSTot_vICMSDeson +
                         ICMSTot_vST        +
                         ICMSTot_vFrete     +
                         ICMSTot_vSeg       +
                         ICMSTot_vOutro     +
                         ICMSTot_vII        +
                         ICMSTot_vIPI       +
                         ICMSTot_vIPIDevol  +  // 2021-10-20
                         ICMSTot_vServ;
  ISSQNtot_vServ      := QrNFeTotUISSQN_vBC.Value; // Parei aqui ?!?!?
  ISSQNtot_vBC        := QrNFeTotUISSQN_vBC.Value;
  ISSQNtot_vISS       := QrNFeTotUISSQN_vISSQN.Value;

  //  Direto no FmNFeCabA
  ISSQNtot_vPIS       := QrNFECabAISSQNtot_vPIS     .Value;
  ISSQNtot_vCOFINS    := QrNFECabAISSQNtot_vCOFINS  .Value;
  //                              //
  RetTrib_vRetPIS     := QrNFECabARetTrib_vRetPIS   .Value;
  RetTrib_vRetCOFINS  := QrNFECabARetTrib_vRetCOFINS.Value;
  RetTrib_vRetCSLL    := QrNFECabARetTrib_vRetCSLL  .Value;
  RetTrib_vBCIRRF     := QrNFECabARetTrib_vBCIRRF   .Value;
  RetTrib_vIRRF       := QrNFECabARetTrib_vIRRF     .Value;
  RetTrib_vBCRetPrev  := QrNFECabARetTrib_vBCRetPrev.Value;
  RetTrib_vRetPrev    := QrNFECabARetTrib_vRetPrev  .Value;

  //
  SQL_FAT_TOT := SQL_FAT_TOT_Prod1_Padrao(FatID, FatNum, Empresa);
  QrFatYTot.Close;
  QrFatYTot.SQL.Text := SQL_FAT_TOT;
  UMyMod.AbreQuery(QrFatYTot, Dmod.MyDB, 'TDmNFe_0000.TotaisNFe()');
  //
  if ICMSTot_vProd <> QrFatYTotValor.Value then
  begin
    REWarning.Text :=
    'O valor das duplicatas não conferem com o valor total dos produtos! Duplic. = ' +
    Dmod.QrControle.FieldByName('Moeda').AsString + ' ' + Geral.FFT(QrFatYTotValor.Value, 2,
    siPositivo) + sLineBreak + ReWarning.Text;
  end;
  // ini 2022-07-29
  //Cobr_Fat_nFat  := FormatFloat('000', QrFatYTotFatID.Value) + '-' +
  //                  FormatFloat('000000000', QrFatYTotFatNum.Value);
  case DmodG.QrParamsEmpFaturaNum.Value of
    0: // Código do Faturamento
      Cobr_Fat_nFat  := FormatFloat('000', QrFatYTotFatID.Value) + '-' + FormatFloat('000000000', QrFatYTotFatNum.Value);
    1: // Numero da NFe
      Cobr_Fat_nFat  := FormatFloat('000', QrFatYTotFatID.Value) + '-' + FormatFloat('000000000', QrNFeCabAide_nNF.Value);
    2: // Origem (Contrato, etc).
      Cobr_Fat_nFat  := FormatFloat('000', QrFatYTotFatID.Value) + '-' + FormatFloat('000000000', QrFatYTotFatNum.Value);
    else // // Numero da NFe
      Cobr_Fat_nFat  := FormatFloat('000', QrFatYTotFatID.Value) + '-' + FormatFloat('000000000', QrNFeCabAide_nNF.Value);
  end;
  // fim 2022-07-29
  Cobr_Fat_vOrig := ICMSTot_vNF;
  Cobr_Fat_vLiq  := QrFatYTotValor.Value; // Parei Aqui o que fazer?
  (*
  Errado!!! Devolução de produtos daria desconto de todo valor!
  if Cobr_Fat_vOrig - Cobr_Fat_vLiq > 0 then
  begin

    Cobr_Fat_vDesc := Cobr_Fat_vOrig - Cobr_Fat_vLiq; // Parei Aqui!! tá certo?
    Geral.MensagemBox('ATENÇÃO:' + sLineBreak + 'Desconto no XML ' +
    '(item: 393 - Y05) de $ ' + FormatFloat('#,###,##0.00', Cobr_Fat_vDesc) + '.'),
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  end else
  *)
    Cobr_Fat_vDesc := 0;
  //
(* Ini 2020-11-01
  vTotTrib := QrNFeTotMvTotTrib.Value;
  vBasTrib := QrNFeTotMvBasTrib.Value;
  if vBasTrib <> 0 then
    pTotTrib := vTotTrib / vBasTrib * 100
  else
    pTotTrib := 0;
  //
*)
  vTotTrib := QrNFeTotMvTotTrib.Value;
  vTribFed := QrNFeTotMvTribFed.Value;
  vTribEst := QrNFeTotMvTribEst.Value;
  vTribMun := QrNFeTotMvTribMun.Value;


  // fim 2020-11-01

  //  Informacao do total aproximados dos tributos nas informacoes adicionais da DANFE:
  InfCpl_totTrib := '';
  InformaTribCpl := False;
  case TindFinalTribNFe(DModG.QrPrmsEmpNFeNFe_indFinalCpl.Value) of
    infFinTribIndefinido: InformaTribCpl := False;
    infFinTribSoConsumidorFinal: InformaTribCpl :=  IndFinal = indfinalConsumidorFinal;
    infFinTribTodos: InformaTribCpl := True;
    else Geral.MB_Erro(
    '"InformaTribCpl" não implementado em "DmNFe_0000.TotaisNFe()"');
  end;
  if InformaTribCpl then
  begin
    FonteStr := '';
    (*2020-11-01 NFC-e *)
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DISTINCT nfm.fontTotTrib',
        'FROM nfeitsm nfm',
        'WHERE nfm.FatID=' + Geral.FF0(FatID),
        'AND nfm.FatNum=' + Geral.FF0(FatNum),
        'AND nfm.Empresa=' + Geral.FF0(Empresa),
        '']);
      //
      if Qry.RecordCount <> 1 then
        FonteStr := ''
      else begin
   (* *)
        case TIBPTaxFont(Qry.FieldByName('fontTotTrib').AsInteger) of
          ibptaxfontNaoInfo:
            FonteStr := ''; //'Fonte: Própria';
          ibptaxfontIBPTax:
          begin
            FonteStr := 'Fonte: ';
            //
            ReopenNFeItsI(FatID, FatNum, Empresa);
            //
            if QrNFeItsI.RecordCount > 0 then
            begin
              while not QrNFEItsI.Eof do
              begin
                if QrNFeLayI.State = dsInactive then
                  ReopenNFeLayI();
                //
                prod_NCM    := DefX('104', 'I05', Geral.SoNumero_TT(QrNFEItsIprod_NCM.Value));
                prod_EXTIPI := QrNFEItsIEX_TIPI.Value;
                //
                if prod_EXTIPI = '' then
                  EX := '0'
                else
                  EX := Geral.SoNumero_TT(prod_EXTIPI);
                //
                // ini 2020-11-01
                //ReopenIBPTax1(Integer(TIBPTaxTabs.ibptaxtabNCM), EX, prod_NCM, QrNFECabAemit_UF.Value, VigenFim);
                ReopenIBPTax2(Integer(TIBPTaxTabs.ibptaxtabNCM), EX, prod_NCM, QrNFECabAemit_UF.Value, VigenFim);
                // fim 2020-11-01
                //
                if Pos(QrIBPTaxFonteStr.Value + ' ' + QrIBPTaxChave.Value, FonteStr) = 0 then
                  FonteStr := FonteStr + QrIBPTaxFonteStr.Value + ' ' + QrIBPTaxChave.Value + ' ';
                //
                QrNFEItsI.Next;
              end;
            end;
          end;
          ibptaxfontCalculoProprio:
            FonteStr := 'Fonte: Cálculo próprio';
          else Geral.MB_Erro(
          '"InformaTribCpl" não implementado em "DmNFe_0000.TotaisNFe()"');
        end;
      (**)
      end;
      //
      //InfCpl_totTrib := 'Trib aprox ';
      InfCpl_totTrib := EmptyStr;
      if vTribFed <> 0 then
        InfCpl_totTrib := InfCpl_totTrib + 'R$ ' +
          Geral.FFT(vTribFed, 2, siPositivo) + ' Federal ';
      if vTribEst <> 0 then
        InfCpl_totTrib := InfCpl_totTrib + 'R$ ' +
          Geral.FFT(vTribEst, 2, siPositivo) + ' Estadual ';
      if vTribMun <> 0 then
        InfCpl_totTrib := InfCpl_totTrib + 'R$ ' +
          Geral.FFT(vTribMun, 2, siPositivo) + ' Municipal ';
      //
      if InfCpl_totTrib = EmptyStr then
      begin
        if vTotTrib <> 0 then
          InfCpl_totTrib := 'Val. Aprox. dos Tributos R$ ' +
            Geral.FFT(vTotTrib, 2, siPositivo) + ' (' +
            Geral.FFT(pTotTrib, 2, siPositivo) + '%) ' + FonteStr
        else
          InfCpl_totTrib := '';
      end else
        InfCpl_totTrib := 'Trib aprox ' + InfCpl_totTrib + FonteStr;
    (* *)
    finally
      Qry.Free;
    end;
   (* *) // Fim 2020-11-01
  end;
(* Nota texnica 2015/03
C. Total da Nota Fiscal
Criados novos campos no grupo de totais da Nota Fiscal, para identificar a distribuição do ICMS Interestadual para a UF de destino na operação interestadual de venda para consumidor final não contribuinte, atendendo ao disposto na Emenda Constitucional 87 de 2015.
# ID Campo Descrição Ele Pai Tipo Ocor. Tam. Observação
329.03 W04c vFCPUFDest Valor total do ICMS relativo Fundo de Combate à Pobreza (FCP) da UF de destino E W02 N 0-1 13v2 Valor total do ICMS relativo ao Fundo de Combate à Pobreza (FCP) para a UF de destino.
329.05 W04e vICMSUFDest Valor total do ICMS Interestadual para a UF de destino E W02 N 0-1 13v2 Valor total do ICMS Interestadual para a UF de destino, já considerando o valor do ICMS relativo ao Fundo de Combate à Pobreza naquela UF.
329.07 W04g vICMSUFRemet Valor total do ICMS Interestadual para a UF do remetente E W02 N 0-1 13v2 Valor total do ICMS Interestadual para a UF do remetente. Nota: A partir de 2019, este valor será zero.
*)
  ICMSTot_vFCPUFDest   := QrNFeTotNAICMSTot_vFCPUFDest.Value;
  ICMSTot_vICMSUFDest  := QrNFeTotNAICMSTot_vICMSUFDest.Value;
  ICMSTot_vICMSUFRemet := QrNFeTotNAICMSTot_vICMSUFRemet.Value;
  // FIM Nota Tecnica 2015/03
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vFCP',
    'ICMSTot_vBCST', 'ICMSTot_vST', 'ICMSTot_vProd',
    'ICMSTot_vFrete', 'ICMSTot_vSeg', 'ICMSTot_vDesc',
    'ICMSTot_vII', 'ICMSTot_vIPI', 'ICMSTot_vPIS',
    'ICMSTot_vCOFINS', 'ICMSTot_vOutro', 'ICMSTot_vNF',
    'ISSQNtot_vServ', 'ISSQNtot_vBC', 'ISSQNtot_vISS',
    'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS',
    'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF',
    'RetTrib_vIRRF', 'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',
    'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
    'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
    'vTotTrib', 'vBasTrib', 'pTotTrib',
    'ICMSTot_vICMSDeson', 'InfCpl_totTrib',
    'ICMSTot_vFCPUFDest', 'ICMSTot_vICMSUFDest', 'ICMSTot_vICMSUFRemet',
    'ICMSTot_vIPIDevol', 'ICMSTot_vFCPST'
  ], ['FatID', 'FatNum', 'Empresa'], [
    ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vFCP,
    ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
    ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc,
    ICMSTot_vII, ICMSTot_vIPI, ICMSTot_vPIS,
    ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF,
    ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS,
    ISSQNtot_vPIS, ISSQNtot_vCOFINS, RetTrib_vRetPIS,
    RetTrib_vRetCOFINS, RetTrib_vRetCSLL, RetTrib_vBCIRRF,
    RetTrib_vIRRF, RetTrib_vBCRetPrev, RetTrib_vRetPrev,
    Cobr_Fat_nFat, Cobr_Fat_vOrig,
    Cobr_Fat_vDesc, Cobr_Fat_vLiq,
    vTotTrib, vBasTrib, pTotTrib,
    ICMSTot_vICMSDeson, InfCpl_totTrib,
    ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet,
    ICMSTot_vIPIDevol, ICMSTot_vFCPST
  ], [FatID, FatNum, Empresa], True);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
end;

function TDmNFe_0000.ValorICMSST_0(ValAtu, ValFut, pICMS: Double): Double;
var
  ValI, ValF: Double;
begin
  ValI := Geral.RoundC(ValAtu * pICMS, 2) / 100;
  ValF := Geral.RoundC(ValFut * pICMS, 2) / 100;
  //
  Result := ValF - ValI;
end;

function TDmNFe_0000.VerificaChavedeAcesso(Chave: String;
  PermiteNulo: Boolean): Boolean;
begin
  Result := NFeXMLGeren.VerificaChaveAcesso(Chave, PermiteNulo);
end;

procedure TDmNFe_0000.VerificaExcecao(const FisRegCad, MesmaUF, Contrib, Proprio,
  SubsTrib: Integer; const UF_Emit, UF_Dest: String; const GraGruX: Integer;
              var CST_B: String; var ICMSAliq, pRedBC, pDif: Double; var modBC,
              AchouFisc: Integer; var cBenef, CSOSN: String);

var
  GGX, FisRegCad_TXT, Interno_TXT, Contrib_TXT, Proprio_TXT, SubsTrib_TXT: String;
begin
  GGX := FormatFloat('0', GraGruX);
  //
  FisRegCad_TXT := FormatFloat('0', FisRegCad);
  Interno_TXT   := FormatFloat('0', MesmaUF);
  Contrib_TXT   := FormatFloat('0', Contrib);
  Proprio_TXT   := FormatFloat('0', Proprio);
  SubsTrib_TXT  := Geral.FF0(SubsTrib);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrExce, Dmod.MyDB, [
    'SELECT fru.Nivel, fru.pDif, ',
    'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC, fru.cBenef, fru.CSOSN ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN fisregufx fru ON fru.Nivel=1 AND fru.CodNiv=gg1.Nivel1 ',
    'WHERE fru.Nivel IS NOT NULL ',
    'AND fru.Codigo=' + FisRegCad_TXT,
    'AND fru.Interno=' + Interno_TXT,
    'AND fru.Contribui=' + Contrib_TXT,
    'AND Proprio=' + Proprio_TXT,
    'AND fru.SubsTrib=' + SubsTrib_TXT,
    'AND UFEmit="' + UF_Emit + '"',
    'AND UFDest="' + UF_Dest + '"',
    'AND ggx.Controle=' + GGX,
    ' ',
    'UNION ',
    ' ',
    'SELECT fru.Nivel, fru.pDif, ',
    'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC, fru.cBenef, fru.CSOSN ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN fisregufx fru ON fru.Nivel=2 AND fru.CodNiv=gg1.Nivel2 ',
    'WHERE fru.Nivel IS NOT NULL ',
    'AND fru.Codigo=' + FisRegCad_TXT,
    'AND fru.Interno=' + Interno_TXT,
    'AND fru.Contribui=' + Contrib_TXT,
    'AND fru.SubsTrib=' + SubsTrib_TXT,
    'AND Proprio=' + Proprio_TXT,
    'AND UFEmit="' + UF_Emit + '"',
    'AND UFDest="' + UF_Dest + '"',
    'AND ggx.Controle=' + GGX,
    ' ',
    'UNION ',
    ' ',
    'SELECT fru.Nivel, fru.pDif, ',
    'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC, fru.cBenef, fru.CSOSN ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN fisregufx fru ON fru.Nivel=3 AND fru.CodNiv=gg1.Nivel3 ',
    'WHERE fru.Nivel IS NOT NULL ',
    'AND fru.Codigo=' + FisRegCad_TXT,
    'AND fru.Interno=' + Interno_TXT,
    'AND fru.Contribui=' + Contrib_TXT,
    'AND Proprio=' + Proprio_TXT,
    'AND fru.SubsTrib=' + SubsTrib_TXT,
    'AND UFEmit="' + UF_Emit + '"',
    'AND UFDest="' + UF_Dest + '"',
    'AND ggx.Controle=' + GGX,
    ' ',
    'UNION ',
    ' ',
    'SELECT fru.Nivel, fru.pDif, ',
    'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC, fru.cBenef, fru.CSOSN ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN fisregufx fru ON fru.Nivel=4 AND fru.CodNiv=gg1.PrdGrupTip ',
    'WHERE fru.Nivel IS NOT NULL ',
    'AND fru.Codigo=' + FisRegCad_TXT,
    'AND fru.Interno=' + Interno_TXT,
    'AND fru.Contribui=' + Contrib_TXT,
    'AND Proprio=' + Proprio_TXT,
    'AND fru.SubsTrib=' + SubsTrib_TXT,
    'AND UFEmit="' + UF_Emit + '"',
    'AND UFDest="' + UF_Dest + '"',
    'AND ggx.Controle=' + GGX,
    ' ',
    'UNION ',
    ' ',
    'SELECT fru.Nivel, fru.pDif, ',
    'fru.ICMSAliq, fru.CST_B, fru.pRedBC, fru.modBC, fru.cBenef, fru.CSOSN ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN fisregufx fru ON fru.Nivel=5 AND fru.CodNiv=0 ',
    'WHERE fru.Nivel IS NOT NULL ',
    'AND fru.Codigo=' + FisRegCad_TXT,
    'AND fru.Interno=' + Interno_TXT,
    'AND fru.Contribui=' + Contrib_TXT,
    'AND Proprio=' + Proprio_TXT,
    'AND fru.SubsTrib=' + SubsTrib_TXT,
    'AND UFEmit="' + UF_Emit + '"',
    'AND UFDest="' + UF_Dest + '"',
    'AND ggx.Controle=' + GGX,
    ' ',
    ' ',
    'ORDER BY Nivel ',
    '']);
    //Geral.MB_Teste(QrExce.SQL.Text);
  //
  if QrExce.RecordCount > 0 then
  begin
    QrExce.First;
    AchouFisc := 2;
    //
    CST_B    := QrExceCST_B.Value;
    ICMSAliq := QrExceICMSAliq.Value;
    pRedBC   := QrExcepRedBC.Value;
    modBC    := QrExcemodBC.Value;
    cBenef   := QrExcecBenef.Value;
    pDif     := QrExcepDif.Value;
    CSOSN    := QrExceCSOSN.Value;
  end;
end;

function TDmNFe_0000.VerificaICMSDeson(const ICMS_vICMSDeson: Double;
  var ICMS_motDesICMS: Integer): Boolean;
begin
  ICMS_motDesICMS := QrProds1ICMS_motDesICMS.Value;
  if ((ICMS_motDesICMS > 0) and (ICMS_vICMSDeson < 0.01))
  or ((ICMS_motDesICMS = 0) and (ICMS_vICMSDeson >= 0.01)) then
  begin
    Result := False;
    Geral.MB_Aviso(
    'Na desoneração do ICMS deve ser informado o valor e o motivo!' +
    sLineBreak + 'Dados apurados:' +
    'Valor da desoneração: ' + Geral.FFT(ICMS_vICMSDeson, 2, siNegativo) +
    sLineBreak + 'Motivo da desoneração: ' + Geral.FF0(ICMS_motDesICMS) + '-' +
    UnNFe_PF.TextoDeCodigoNFe(nfeCTmotDesICMS, ICMS_motDesICMS));
  end else
    Result := True;
end;

procedure TDmNFe_0000.VerificaNFeCabA();
var
  MyCursor: TCursor;
  //CNPJ, CPF: String;
  (*Emit, Dest, Fim, *)Ini: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    // erro no planning
    UnDmkDAC_PF.AbreMySQLQuery0(Qr_D_E, Dmod.MyDB, [
    'SELECT IDCtrl, FatID, FatNum, Empresa, ',
    'CodInfoDest, CodInfoEmit, ',
    'emit_CNPJ, emit_CPF, dest_CNPJ, dest_CPF ',
    'FROM nfecaba ',
    'WHERE (CodInfoDest = 0 OR CodInfoEmit=0) ',
    'AND emit_CNPJ IN ',
    '( ',
    'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) Docum ',
    'FROM enticliint eci ',
    'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti ',
    ')',
    '']);
  except
    Exit;
  end;
  Ini := Qr_D_E.RecordCount;
  if Ini > 0 then
  begin
    Geral.MB_Aviso(
      'Verificação de destinatário da NFe desativada mas necessária!' + sLineBreak
      + 'Avise a DERMATEK!');
(*
////////////////////////////////////////////////////////////////////////////////
  Desativado em 2014-10-20 por MLA por causa da NFe 3.10
  Ver se precisa reativar!
  Observar que tem um campo novo na NFe 3.10 > dest_idEstrangeiro!!!!
////////////////////////////////////////////////////////////////////////////////

    Qr_D_E.First;
    while not Qr_D_E.Eof do
    begin
      if Qr_D_ECodInfoEmit.Value = 0 then
      begin
        CNPJ := Qr_D_Eemit_CNPJ.Value;
        CPF  := Qr_D_Eemit_CPF.Value;
        Emit := DefineEntidadePeloDoc(CNPJ, CPF);
        if Emit <> 0 then
        begin
          // Define o valor do CodInfoEmit
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
          'CodInfoEmit'], ['FatID', 'FatNum', 'Empresa'], [Emit],
          [Qr_D_EFatID.Value, Qr_D_EFatNum.Value, Qr_D_EEmpresa.Value], True);
        end;
      end;
      if Qr_D_ECodInfoDest.Value = 0 then
      begin
        CNPJ := Qr_D_Edest_CNPJ.Value;
        CPF  := Qr_D_Edest_CPF.Value;
        dest_idEstrangeiro
        Dest := DefineEntidadePeloDoc(CNPJ, CPF);
        if Dest <> 0 then
        begin
          // Define o valor do CodInfoDest
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
          'CodInfoDest'], ['FatID', 'FatNum', 'Empresa'], [Dest],
          [Qr_D_EFatID.Value, Qr_D_EFatNum.Value, Qr_D_EEmpresa.Value], True);
        end;
      end;
      Qr_D_E.Next;
    end;
    Qr_D_E.Close;
    UnDmkDAC_PF.AbreQuery(Qr_D_E, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    Fim := Qr_D_E.RecordCount;
    //
    Geral.MensagemBox('Foram localizadas ' + IntToStr(Ini) +
    ' NFe-s sem "CodInfoDest" ou "CodInfoEmit", sendo que ' + IntToStr(Fim) +
    ' ficaram sem serem atualizadas!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    if Fim > 0 then
    begin
      if DBCheck.CriaFm(TFmNFeDocOrfao, FmNFeDocOrfao, afmoLiberado) then
      begin
        FmNFeDocOrfao.ShowModal;
        FmNFeDocOrfao.Destroy;
      end;
    end;
*)
  end;
  Screen.Cursor := MyCursor;
end;

procedure TDmNFe_0000.WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

function TDmNFe_0000.ObtemDescricao_TabGov_1(Codigo: Integer; Tabela: String;
  DataRef: TDateTime): String;
var
  DataTxt: String;
begin
  try
    DataTxt := Geral.FDT(DataRef, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrTG1, DModG.AllID_DB, [
    'SELECT t.Nome FROM ',
    Lowercase(Tabela) + ' t',
    'WHERE t.Codigo=' + Geral.FF0(Codigo),
    'AND "' + DataTxt + '" > DataIni',
    'AND (',
    '  "' + DataTxt + '" < DataFim',
    '  OR (DataFim < "1900-01-01")',
    ')',
    'ORDER BY DataFim DESC',
    '']);
    Result := QrTG1Nome.Value;
  except
    on E: Exception do
    begin
      if VAR_USUARIO = -1 then
        Geral.MB_Erro(E.Message);
    end;
  end;
end;

function TDmNFe_0000.ObtemDirXML(const Ext: String; var Dir: String; Assinado:
  Boolean): Boolean;
var
  Titulo, Descri: String;
begin
  //Result := False;
  Dir := '';
  if Ext = NFE_EXT_NFE_XML  then
  begin
    if Assinado then
      Dir := QrFilialDirNFeAss.Value
    else
      Dir := QrFilialDirNFeGer.Value;
  end else
    if Ext = NFE_EXT_ENV_LOT_XML then
      Dir := QrFilialDirEnvLot.Value
  else
    if Ext = NFE_EXT_REC_XML then
      Dir := QrFilialDirRec.Value
  else
    if Ext = NFE_EXT_PRO_REC_XML then
      Dir := QrFilialDirProRec.Value
  else
    if Ext = NFE_EXT_DEN_XML then
      Dir := QrFilialDirDen.Value
  else
    if Ext = NFE_EXT_PED_CAN_XML then
      Dir := QrFilialDirPedCan.Value
  else
    if Ext = NFE_EXT_CAN_XML then
      Dir := QrFilialDirCan.Value
  else
    if Ext = NFE_EXT_PED_INU_XML then
      Dir := QrFilialDirPedInu.Value
  else
    if Ext = NFE_EXT_INU_XML then
      Dir := QrFilialDirInu.Value
  else
    if Ext = NFE_EXT_PED_SIT_XML then
      Dir := QrFilialDirPedSit.Value
  else
    if Ext = NFE_EXT_SIT_XML then
      Dir := QrFilialDirSit.Value
  else
    if Ext = NFE_EXT_PED_STA_XML then
      Dir := QrFilialDirPedSta.Value
  else
    if Ext = NFE_EXT_STA_XML then
      Dir := QrFilialDirSta.Value
  else
    if Ext = NFE_EXT_NFE_WEB_XML then
      Dir := QrFilialDirNFeRWeb.Value
  else
    if Ext = NFE_EXT_EVE_ENV_LOT_XML then
      Dir := QrFilialDirEveEnvLot.Value
  else
    if Ext = NFE_EXT_EVE_RET_LOT_XML then
      Dir := QrFilialDirEveRetLot.Value
  else
    if Ext = NFE_EXT_EVE_ENV_CCE_XML then
      Dir := QrFilialDirEvePedCCe.Value
  else
    if Ext = NFE_EXT_EVE_RET_CCE_XML then
      Dir := QrFilialDirEveRetCCe.Value
  else
    if Ext = NFE_EXT_EVE_ENV_CAN_XML then
      Dir := QrFilialDirEvePedCan.Value
  else
    if Ext = NFE_EXT_EVE_RET_CAN_XML then
      Dir := QrFilialDirEveRetCan.Value
  else
    if Ext = NFE_EXT_RET_NFE_DES_XML then
      Dir := QrFilialDirRetNfeDes.Value
  else
    if Ext = NFE_EXT_EVE_ENV_MDE_XML then
      Dir := QrFilialDirEvePedMDe.Value
  else
    if Ext = NFE_EXT_EVE_RET_MDE_XML then
      Dir := QrFilialDirEveRetMDe.Value
  else
    if Ext = NFE_EXT_RET_DOW_NFE_DES_XML then
      Dir := QrFilialDirDowNFeDes.Value
  else
    if Ext = NFE_EXT_RET_DOW_NFE_NFE_XML then
      Dir := QrFilialDirDowNFeNFe.Value
  else
    if Ext = NFE_EXT_PED_DFE_DIS_INT_XML then
      Dir := QrFilialDirDistDFeInt.Value
  else
    if Ext = NFE_EXT_RET_DFE_DIS_INT_XML then
      Dir := QrFilialDirRetDistDFeInt.Value
  else
    if Ext = NFE_EXT_RET_DOW_NFE_CNF_XML then
      Dir := QrFilialDirDowNFeCnf.Value
  else
    if Ext = NFE_EXT_RET_CNF_NFE_NFE_XML then
      Dir := QrFilialDirDowNFeNFe.Value
  else
    if Ext = NFE_EXT_EVE_ENV_EPEC_XML then
      Dir := QrFilialDirEvePedEPEC.Value
  else
    if Ext = NFE_EXT_EVE_RET_EPEC_XML then
      Dir := QrFilialDirEveRetEPEC.Value
  //

  else Geral.MensagemBox('Tipo de (final de) nome de arquivo XML ' +
  'desconhecido: ' + Ext, 'Erro', MB_OK+MB_ICONERROR);
  //
  if Dir <> '' then
  begin
    TituloArqXML(Ext, Titulo, Descri);
    Result := Geral.VerificaDir(Dir, '\', Titulo + sLineBreak + Descri, True);
  end else
  begin
    Geral.MensagemBox(
    'Diretório XML não definido no cadastro da filial para o tipo: ' +
    Ext, 'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
  end;
end;

function TDmNFe_0000.ObtemIDCtrlDeChaveNFe(ChaveNFe: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IDCtrl ',
    'FROM nfecaba ',
    'WHERE Id="' + ChaveNFe + '" ',
    '']);
    Result := Qry.FieldByName('IDCtrl').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.ObtemNaturezaDaOperacao_Txt(CFOP2003: String): String;
begin
  QrCFOP1.Close;
  QrCFOP1.Params[0].AsString := CFOP2003;
  UMyMod.AbreQuery(QrCFOP1, Dmod.MyDB, 'TDmNFe_0000.ObtemNaturezaDaOperacao_Txt()');
  //
  Result := QrCFOP1Nome.Value;
end;

function TDmNFe_0000.ObtemNumeroCFOP_Emissao(Tabela_FatPedFis: String;
  Parametros: array of integer; Empresa, Cliente, Item_MadeBy,
  RegrFiscal: Integer; EhServico, EhSubsTrib: Boolean; var CFOP: String;
  var ICMS_Aliq: Double;
  //OriCodi, OriCnta, GraGruX
  var CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer): Boolean;

  procedure AvisoFaltaRegraCFOP(RegrFiscal, Contrib, MesmaUF, Proprio,
    Servico, SubsTrib: Integer);
  begin
    Geral.MB_Aviso('Regra CFOP não definida na regra fiscal:' +
      sLineBreak + 'Regra fiscal: ' + FormatFloat('000', RegrFiscal) +
      sLineBreak +
      sLineBreak + '[TIE] Contribuinte ICMS: ' + dmkPF.IntToSimNao(Contrib) +
      sLineBreak + '[VMU] Venda para mesma UF: ' + dmkPF.IntToSimNao(MesmaUF) +
      sLineBreak + '[PFP] Fabricação própria: ' + dmkPF.IntToSimNao(Proprio) +
      sLineBreak + '[PMO] É serviço: '+ dmkPF.IntToSimNao(Servico) +
      sLineBreak + '[S.T] É substituto tributário: '+ dmkPF.IntToSimNao(SubsTrib) +
      sLineBreak);
  end;

  procedure AvisoFaltaRegraAliq(RegrFiscal, Contrib, MesmaUF, Proprio,
    Servico, SubsTrib: Integer; CFOP, UF_Emp, UF_Cli: String);
  begin
    Geral.MB_Aviso('Alíquotas não definidas na regra fiscal:' +
      sLineBreak + 'Regra fiscal: ' + FormatFloat('000', RegrFiscal) +
      sLineBreak +
      sLineBreak + '[TIE] Contribuinte ICMS: ' + dmkPF.IntToSimNao(Contrib) +
      sLineBreak + '[VMU] Venda para mesma UF: ' + dmkPF.IntToSimNao(MesmaUF) +
      sLineBreak + '[PFP] Fabricação própria: ' + dmkPF.IntToSimNao(Proprio) +
      sLineBreak + '[PMO] É serviço: '+ dmkPF.IntToSimNao(Servico) +
      sLineBreak + '[S.T] É substituto tributário: '+ dmkPF.IntToSimNao(SubsTrib) +
      sLineBreak +
      sLineBreak + 'CFOP: ' + CFOP +
      sLineBreak + 'Minha UF: ' + UF_Emp +
      sLineBreak + 'UF cliente/fornecedor: ' + UF_Cli);
  end;

  procedure AlteraEntidade;
  begin
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Cliente <> 0 then
        FmEntidade2.LocCod(Cliente, Cliente);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    QrDest.Close;
    QrDest.Database := Dmod.MyDB; // 2020-08-31
    UnDmkDAC_PF.AbreQuery(QrDest, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
  end;

var
  QryFatPedFis, QryFisRegCad: TmySQLQuery;
  Cli_Tipo, CFOP_Servico: Byte;
  Cli_IE, Msg, UF_Cli, UF_Emp: String;
  Cli_UF, Emp_UF, OriCodi, OriCnta, GraGruX: Integer;
begin
  DModG.ReopenParamsEmp(Empresa);
  //
  if DModG.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    //Screen.Cursor := crHourGlass;
    ICMS_Aliq   := 0;
    {
    PIS_ALiq    := 0;
    COFINS_Aliq := 0;
    }
    //
    Result := False;
    //
    ReopenEmpresa(Empresa);
    ReopenDest(Cliente);
    //
    Cli_Tipo := QrDestTipo.Value;
    Cli_IE   := QrDestIE.Value;
    Cli_UF   := Trunc(QrDestUF.Value);
    Emp_UF   := Trunc(QrEmpresaUF.Value);
    //
    if Cli_Tipo = 1 then // CFP é não contribuinte!
      CFOP_Contrib := 0
    else
    begin
      if Trim(Cli_IE) = '' then  // CNPJ sem IE
      begin
        Msg := PChar('Pessoa jurídica (código: ' + Geral.FF0(Cliente) +
                 ') sem I.E. para definição do CFOP apropriado! ' +
                 'Caso seja isento informe "ISENTO" no cadastro.');

        MyObjects.MessageDlgCheck(Msg, mtConfirmation, [mbOK], 0, mrOK,
          True, True, 'Desejo alterar agora cadastro do cliente nº ' +
          Geral.FF0(Cliente), @AlteraEntidade);
        //
        Screen.Cursor := crDefault;
        Exit;
      end;
      if Uppercase(Trim(Cli_IE)) = 'ISENTO' then   // CNPJ Isento
        CFOP_Contrib := 0
      else
        CFOP_Contrib := 1; // Contribuinte
    end;
    if Cli_UF = 0 then
    begin
      Geral.MB_Aviso('UF do Cliente não definida!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if EMP_UF = 0 then
    begin
       Geral.MB_Aviso('UF da empresa ' + Geral.FF0(QrEmpresaFilial.Value) +
         ' não definida!');
       Screen.Cursor := crDefault;
       Exit;
    end;
    if Cli_UF = EMP_UF then
      CFOP_MesmaUF := 1
    else
      CFOP_MesmaUF := 0;
    if Item_MadeBy = 1 then
      CFOP_Proprio := 1
    else
      CFOP_Proprio := 0;
    if EhServico then
      CFOP_Servico := 1
    else
      CFOP_Servico := 0;
    if EhSubsTrib then
      CFOP_SubsTrib := 1
    else
      CFOP_SubsTrib := 0;
    //
    if Tabela_FatPedFis <> '' then
    begin
      if LowerCase(Tabela_FatPedFis) = 'fatpedfis' then
      begin
        QryFatPedFis := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryFatPedFis, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + Tabela_FatPedFis,
          'WHERE OriCtrl=' + Geral.FF0(Parametros[0]),
          '']);
      end else
      begin
        QryFatPedFis := TmySQLQuery.Create(TDataModule(DModG.MyPID_DB.Owner));
        //
        OriCodi := Parametros[0];
        OriCnta := Parametros[1];
        GraGruX := Parametros[2];
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryFatPedFis, DModG.MyPID_DB, [
          'SELECT fpf.* ',
          'FROM ' + Tabela_FatPedFis + ' fpf ',
          'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.GraGru1 = fpf.GraGru1',
          'WHERE fpf.OriCodi=' + Geral.FF0(OriCodi),
          'AND fpf.OriCnta=' + Geral.FF0(OriCnta),
          'AND ggx.Controle=' + Geral.FF0(GraGruX),
          'GROUP BY ggx.GraGru1',
          '']);
      end;
    end;
    if (Tabela_FatPedFis <> '') and (QryFatPedFis.RecordCount > 0) then
    begin
      if (QryFatPedFis.FieldByName('CFOP').AsString = '') or
        (QryFatPedFis.FieldByName('CFOP').AsString = '0') then
      begin
         AvisoFaltaRegraCFOP(RegrFiscal, CFOP_Contrib, CFOP_MesmaUF,
         CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib);
         CFOP := '';
      end else
      begin
        CFOP   := QryFatPedFis.FieldByName('CFOP').AsString;
        Result := True;
        //
        if EhServico then
        begin
          //
        end else
        begin
          if QrFilialSimplesFed.Value = 0 then
          begin
            UF_Cli := Geral.GetSiglaUF_do_CodigoUF(Cli_UF);
            UF_Emp := Geral.GetSiglaUF_do_CodigoUF(Emp_UF);
            //
            QryFisRegCad := TmySQLQuery.Create(TDataModule(DMod.MyDB.Owner));
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QryFisRegCad, DMod.MyDB, [
              'SELECT ICMS_Usa ',
              'FROM fisregcad ',
              'WHERE Codigo=' + Geral.FF0(RegrFiscal),
              '']);
            if QryFisRegCad.RecordCount > 0 then
            begin
              if QryFisRegCad.FieldByName('ICMS_Usa').AsInteger = 1 then
                ICMS_Aliq := QryFatPedFis.FieldByName('ICMSAliq').AsFloat;
            end else
            begin
              Geral.MB_Aviso('Regra Fiscal não localizada!');
              Result := False;
            end;
            QryFisRegCad.Free;
          end;
        end;
      end;
      QryFatPedFis.Free;
    end else
    begin
(*
      QrCFOP.Params[00].AsInteger := RegrFiscal;
      QrCFOP.Params[01].AsInteger := CFOP_Contrib;
      QrCFOP.Params[02].AsInteger := CFOP_MesmaUF;
      QrCFOP.Params[03].AsInteger := CFOP_Proprio;
      QrCFOP.Params[04].AsInteger := CFOP_Servico;
      //
      UMyMod.AbreQuery(QrCFOP, Dmod.MyDB, 'TFmFatPedIts.InsereItem()');
*)
      UnDMkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
      'SELECT CFOP  ',
      'FROM fisregcfop ',
      'WHERE Codigo='  + Geral.FF0(RegrFiscal),
      'AND Contribui=' + Geral.FF0(CFOP_Contrib),
      'AND Interno='   + Geral.FF0(CFOP_MesmaUF),
      'AND Proprio='   + Geral.FF0(CFOP_Proprio),
      'AND Servico='   + Geral.FF0(CFOP_Servico),
      'AND SubsTrib='  + Geral.FF0(CFOP_SubsTrib),
      '']);
      //
      if QrCFOP.RecordCount = 0 then
      begin
         AvisoFaltaRegraCFOP(RegrFiscal, CFOP_Contrib, CFOP_MesmaUF,
         CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib);
         CFOP := '';
      end else
      begin
        CFOP   := QrCFOPCFOP.Value;
        Result := True;
        //
        if EhServico then
        begin
          //
        end else
        begin
          if QrFilialSimplesFed.Value = 0 then
          begin
            UF_Cli := Geral.GetSiglaUF_do_CodigoUF(Cli_UF);
            UF_Emp := Geral.GetSiglaUF_do_CodigoUF(Emp_UF);
(*        2015-10-24
            QrAliq.Close;
            QrAliq.Params[00].AsInteger := RegrFiscal;
            QrAliq.Params[01].AsInteger := CFOP_MesmaUF;
            QrAliq.Params[02].AsInteger := CFOP_Contrib;
            QrAliq.Params[03].AsInteger := CFOP_Proprio;
            QrAliq.Params[04].AsString  := UF_Emp;
            QrAliq.Params[05].AsString  := UF_Cli;
            QrAliq.O p e n;
*)
            ReopenAliq(0, RegrFiscal, CFOP_MesmaUF, CFOP_Contrib, CFOP_Proprio,
              CFOP_SubsTrib, UF_Emp, UF_Cli);
            // FIM 2015-10-24
            if QrAliq.RecordCount > 0 then
            begin
              if QrAliqICMS_Usa.Value = 1 then
                ICMS_Aliq := QrAliqICMSAliq.Value;
              {
              if QrAliqPIS_Usa.Value = 1 then
                PIS_ALiq    := QrAliqPISAliq.Value;
              if QrAliqCOFINS_Usa.Value = 1 then
                COFINS_Aliq := QrAliqCOFINSAliq.Value;
              }
            end else
            begin
              AvisoFaltaRegraAliq(RegrFiscal, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio,
                CFOP_Servico, CFOP_SubsTrib, CFOP, UF_Emp, UF_Cli);
              Result := False;
            end;
          end;
        end;
      end;
      //
      //Screen.Cursor := crDefault;
    end;
  end else
    Result := True;
end;




function TDmNFe_0000.ObtemNumeroCFOP_Entrada(
              (*Tabela_FatPedFis: String; Parametros: array of integer;*)
              const Empresa, Fornecedor, (*Item_MadeBy,*) PVD_MadBy, RegrFiscal:
              Integer;
              const EhServico(*, EhSubsTrib*): Boolean;
              const CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido: Integer;
              const CFOP_Emitido: String;
              var CFOP: String; (*var ICMS_Aliq: Double;*)
              var CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
              var ICMS_CST, IPI_CST, PIS_CST, COFINS_CST: String;
              var OriTES, EFD_II_C195, GenCtbD, GenCtbC,
              TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
              TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
              var ICMSAliq, PISAliq, COFINSAliq: Double;
              var FisRegGenCtbD, FisRegGenCtbC: Integer): Boolean;
  function AvisoFaltaRegraCFOP(RegrFiscal, Contrib, MesmaUF, Proprio,
    Servico, SubsTrib: Integer; CFOP_Emitido: String; Mostra: Boolean): String;
  begin
    Result := 'Regra CFOP não definida na regra fiscal:' +
      sLineBreak + 'Regra fiscal: ' + FormatFloat('000', RegrFiscal) +
      sLineBreak +
      sLineBreak + '[TIE] Contribuinte ICMS: ' + dmkPF.IntToSimNao(Contrib) +
      sLineBreak + '[VMU] Venda para mesma UF: ' + dmkPF.IntToSimNao(MesmaUF) +
      sLineBreak + '[PFP] Fabricação própria: ' + dmkPF.IntToSimNao(Proprio) +
      sLineBreak + '[PMO] É serviço: '+ dmkPF.IntToSimNao(Servico) +
      sLineBreak + '[S.T] É substituto tributário: '+ dmkPF.IntToSimNao(SubsTrib) +
      sLineBreak + '[CFO] CFOP da emissão: '+ CFOP_Emitido +
      sLineBreak;
    if Mostra then
      Geral.MB_Aviso(Result);
  end;

  function AvisoFaltaRegraAliq(RegrFiscal, Contrib, MesmaUF, Proprio,
    Servico, SubsTrib: Integer; CFOP, UF_Emp, UF_Cli: String;
    Mostra: Boolean): String;
  begin
    Result := 'Alíquotas não definidas na regra fiscal:' +
      sLineBreak + 'Regra fiscal: ' + FormatFloat('000', RegrFiscal) +
      sLineBreak +
      sLineBreak + '[TIE] Contribuinte ICMS: ' + dmkPF.IntToSimNao(Contrib) +
      sLineBreak + '[VMU] Venda para mesma UF: ' + dmkPF.IntToSimNao(MesmaUF) +
      sLineBreak + '[PFP] Fabricação própria: ' + dmkPF.IntToSimNao(Proprio) +
      sLineBreak + '[PMO] É serviço: '+ dmkPF.IntToSimNao(Servico) +
      sLineBreak + '[ST ] É substituto tributário: '+ dmkPF.IntToSimNao(SubsTrib) +
      sLineBreak +
      sLineBreak + 'CFOP: ' + CFOP +
      sLineBreak + 'Minha UF: ' + UF_Emp +
      sLineBreak + 'UF cliente/fornecedor: ' + UF_Cli;
      //
    if Mostra then
      Geral.MB_Aviso(Result);
  end;

  procedure AlteraEntidade();
  begin
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Fornecedor <> 0 then
        FmEntidade2.LocCod(Fornecedor, Fornecedor);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    QrDest.Close;
    QrDest.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreQuery(QrDest, Dmod.MyDB);
  end;

  procedure AlteraRegraFiscal();
  begin
    Grade_Jan.MostraFormFisRegCad(RegrFiscal);
  end;
var
  QryFatPedFis, QryFisRegCad: TmySQLQuery;
  Frn_Tipo, CFOP_Servico: Byte;
  Frn_IE, _Msg, Msg, UF_Cli, UF_Emp: String;
  Frn_UF, Emp_UF, OriCodi, OriCnta, GraGruX: Integer;
  CFOPFormatado: String;
begin
  CFOP         := '';
  ICMS_CST     := '';
  IPI_CST      := '';
  PIS_CST      := '';
  COFINS_CST   := '';
  //
  OriTES       := 0;
  EFD_II_C195  := 0;
  GenCtbD      := 0;
  GenCtbC      := 0;
  //
  TES_ICMS       := 0;
  TES_IPI        := 0;
  TES_PIS        := 0;
  TES_COFINS     := 0;

  TES_BC_ICMS    := 0;
  TES_BC_IPI     := 0;
  TES_BC_PIS     := 0;
  TES_BC_COFINS  := 0;
  //
  DModG.ReopenParamsEmp(Empresa);
  //
  //if DModG.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    //ICMS_Aliq   := 0;
    //
    Result := False;
    //
    ReopenEmpresa(Empresa);
    ReopenDest(Fornecedor);
    //
    Frn_Tipo := QrDestTipo.Value;
    Frn_IE   := QrDestIE.Value;
    Frn_UF   := Trunc(QrDestUF.Value);
    Emp_UF   := Trunc(QrEmpresaUF.Value);
    //
    if Frn_Tipo = 1 then // CFP é não contribuinte!
      CFOP_Contrib := 0
    else
    begin
      if Trim(Frn_IE) = '' then  // CNPJ sem IE
      begin
        Msg := PChar('Pessoa jurídica (código: ' + Geral.FF0(Fornecedor) +
                 ') sem I.E. para definição do CFOP apropriado! ' +
                 'Caso seja isento informe "ISENTO" no cadastro.');

        MyObjects.MessageDlgCheck(Msg, mtConfirmation, [mbOK], 0, mrOK,
          True, True, 'Desejo alterar agora cadastro do cliente nº ' +
          Geral.FF0(Fornecedor), @AlteraEntidade);
        //
        Screen.Cursor := crDefault;
        Exit;
      end;
      if Uppercase(Trim(Frn_IE)) = 'ISENTO' then   // CNPJ Isento
        CFOP_Contrib := 0
      else
        CFOP_Contrib := 1; // Contribuinte
    end;
    if Frn_UF = 0 then
    begin
      Geral.MB_Aviso('UF do Cliente não definida!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if EMP_UF = 0 then
    begin
       Geral.MB_Aviso('UF da empresa ' + Geral.FF0(QrEmpresaFilial.Value) +
         ' não definida!');
       Screen.Cursor := crDefault;
       Exit;
    end;
    if Frn_UF = EMP_UF then
      CFOP_MesmaUF := 1
    else
      CFOP_MesmaUF := 0;
    //if Item_MadeBy = 1 then
    //if QrDestMadeBy.Value = 1 then
    if PVD_MadBy = 1 then
      CFOP_Proprio := 1
    else
      CFOP_Proprio := 0;
    if EhServico then
      CFOP_Servico := 1
    else
      CFOP_Servico := 0;
    //
    if CST_ICMS_Eh_SubsTrib(CRT_Emitido, CST_A_Emitido, CST_B_Emitido,
    CSOSN_Emitido) then
      CFOP_SubsTrib := 1
    else
      CFOP_SubsTrib := 0;
    //
    (*
    if Tabela_FatPedFis <> '' then
    begin
      if LowerCase(Tabela_FatPedFis) = 'fatpedfis' then
      begin
        QryFatPedFis := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryFatPedFis, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + Tabela_FatPedFis,
          'WHERE OriCtrl=' + Geral.FF0(Parametros[0]),
          '']);
      end else
      begin
        QryFatPedFis := TmySQLQuery.Create(TDataModule(DModG.MyPID_DB.Owner));
        //
        OriCodi := Parametros[0];
        OriCnta := Parametros[1];
        GraGruX := Parametros[2];
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryFatPedFis, DModG.MyPID_DB, [
          'SELECT fpf.* ',
          'FROM ' + Tabela_FatPedFis + ' fpf ',
          'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.GraGru1 = fpf.GraGru1',
          'WHERE fpf.OriCodi=' + Geral.FF0(OriCodi),
          'AND fpf.OriCnta=' + Geral.FF0(OriCnta),
          'AND ggx.Controle=' + Geral.FF0(GraGruX),
          'GROUP BY ggx.GraGru1',
          '']);
      end;
    end;
    if (Tabela_FatPedFis <> '') and (QryFatPedFis.RecordCount > 0) then
    begin
      if (QryFatPedFis.FieldByName('CFOP').AsString = '') or
        (QryFatPedFis.FieldByName('CFOP').AsString = '0') then
      begin
         AvisoFaltaRegraCFOP(RegrFiscal, CFOP_Contrib, CFOP_MesmaUF,
         CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib);
         CFOP := '';
      end else
      begin
        CFOP   := QryFatPedFis.FieldByName('CFOP').AsString;
        Result := True;
        //
        if EhServico then
        begin
          //
        end else
        begin
          if QrFilialSimplesFed.Value = 0 then
          begin
            UF_Cli := Geral.GetSiglaUF_do_CodigoUF(Frn_UF);
            UF_Emp := Geral.GetSiglaUF_do_CodigoUF(Emp_UF);
            //
            QryFisRegCad := TmySQLQuery.Create(TDataModule(DMod.MyDB.Owner));
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QryFisRegCad, DMod.MyDB, [
              'SELECT ICMS_Usa ',
              'FROM fisregcad ',
              'WHERE Codigo=' + Geral.FF0(RegrFiscal),
              '']);
            if QryFisRegCad.RecordCount > 0 then
            begin
              if QryFisRegCad.FieldByName('ICMS_Usa').AsInteger = 1 then
                ICMS_Aliq := QryFatPedFis.FieldByName('ICMSAliq').AsFloat;
            end else
            begin
              Geral.MB_Aviso('Regra Fiscal não localizada!');
              Result := False;
            end;
            QryFisRegCad.Free;
          end;
        end;
      end;
      QryFatPedFis.Free;
    end else*)
    begin
      CFOPFormatado := Geral.FormataCFOP(Geral.SoNumero_TT(CFOP_Emitido));
      //
      UnDMkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
      'SELECT CFOP  ',
      'FROM fisregcfop ',
      'WHERE Codigo='  + Geral.FF0(RegrFiscal),
      'AND Contribui=' + Geral.FF0(CFOP_Contrib),
      'AND Interno='   + Geral.FF0(CFOP_MesmaUF),
      'AND Proprio='   + Geral.FF0(CFOP_Proprio),
      'AND Servico='   + Geral.FF0(CFOP_Servico),
      'AND SubsTrib='  + Geral.FF0(CFOP_SubsTrib),
      'AND OriCFOP="'  + CFOPFormatado + '"',
      '']);
      //Geral.MB_Teste(QrCFOP.SQL.Text);
      //
      if QrCFOP.RecordCount = 0 then
      begin
         CFOP := '';
         //
         _Msg :=
         AvisoFaltaRegraCFOP(RegrFiscal, CFOP_Contrib, CFOP_MesmaUF,
         CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib, CFOPFormatado, False);
         Msg := PChar(_Msg);
        VAR_FisRegCad_ToAlt := RegrFiscal;
        MyObjects.MessageDlgCheck(Msg, mtConfirmation, [mbOK], 0, mrOK,
          True, True, 'Desejo alterar agora a regra fiscal nº ' +
          Geral.FF0(RegrFiscal), @AlteraRegraFiscal);
     end else
      begin
        CFOP   := QrCFOPCFOP.Value;
        Result := True;
        //
        if EhServico then
        begin
          //
        end else
        begin
          if QrFilialSimplesFed.Value = 0 then
          begin
            UF_Cli := Geral.GetSiglaUF_do_CodigoUF(Frn_UF);
            UF_Emp := Geral.GetSiglaUF_do_CodigoUF(Emp_UF);
            ReopenCSTsInn(RegrFiscal, CFOP_MesmaUF, CFOP_Contrib, CFOP_Proprio,
              CFOP_Servico, CFOP_SubsTrib, CFOPFormatado, UF_Emp, UF_Cli);
            if QrCSTsInn.RecordCount > 0 then
            begin
              //CFOP       acima!
              ICMS_CST        := QrCSTsInnOriCST_ICMS.Value;
              IPI_CST         := QrCSTsInnOriCST_IPI.Value;
              PIS_CST         := QrCSTsInnOriCST_PIS.Value;
              COFINS_CST      := QrCSTsInnOriCST_COFINS.Value;
              //
              OriTES          := QrCSTsInnOriTES.Value;
              EFD_II_C195     := QrCSTsInnEFD_II_C195.Value;
              GenCtbD         := QrCSTsInnGenCtbD.Value;
              GenCtbC         := QrCSTsInnGenCtbC.Value;
              //
              TES_ICMS        := QrCSTsInnTES_ICMS.Value;
              TES_IPI         := QrCSTsInnTES_IPI.Value;
              TES_PIS         := QrCSTsInnTES_PIS.Value;
              TES_COFINS      := QrCSTsInnTES_COFINS.Value;
              //
              TES_BC_ICMS     := QrCSTsInnTES_BC_ICMS.Value;
              TES_BC_IPI      := QrCSTsInnTES_BC_IPI.Value;
              TES_BC_PIS      := QrCSTsInnTES_BC_PIS.Value;
              TES_BC_COFINS   := QrCSTsInnTES_BC_COFINS.Value;
              //
              ICMSAliq        := QrCSTsInnICMSAliq.Value;
              PISAliq         := QrCSTsInnPISAliq.Value;
              COFINSAliq      := QrCSTsInnCOFINSAliq.Value;
              //
              FisRegGenCtbD   := QrCSTsInnRegFisCad_CtbD.Value;
              FisRegGenCtbC   := QrCSTsInnRegFisCad_CtbC.Value;
              (*RegFisCad_CtbD
              RegFisCad_CtbC
              RegFisCFOP_CtbD := QrCSTsInnRegFisCFOP_CtbD.Value;
              RegFisCFOP_CtbC := QrCSTsInnRegFisCFOP_CtbC.Value;
              RegFisUFs_CtbD  := QrCSTsInnRegFisUFs_CtbD.Value;
              RegFisUFs_CtbC  := QrCSTsInnRegFisUFs_CtbC.Value;*)
              //
            end else
            begin
              _Msg := AvisoFaltaRegraAliq(RegrFiscal, CFOP_Contrib,
              CFOP_MesmaUF, CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib,
              CFOPFormatado, UF_Emp, UF_Cli, False);
              //
              Msg := PChar(_Msg);
              //
              VAR_FisRegCad_ToAlt := RegrFiscal;
              MyObjects.MessageDlgCheck(Msg, mtConfirmation, [mbOK], 0, mrOK,
                True, True, 'Desejo alterar agora a regra fiscal nº ' +
                Geral.FF0(RegrFiscal), @AlteraRegraFiscal);
              //
              Result := False;
            end;
          end;
        end;
      end;
      //
    end;
  end (*else
    Result := True;*)
end;

function TDmNFe_0000.ObtemSiglaUFdeDTBdeCidade(Cidade: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Result := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Nome ',
    'FROM ufs ',
    'WHERE DTB=' + Copy(Geral.FF0(Cidade), 1, 2),
    '']);
    Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.ObtemURLs_QRCode(const UF: String; const Versao: Double;
  const tpAmb: Integer; var URL_QrCode, URL_Consulta: String): Boolean;
const
  sProcName = 'TDmProd.ObtemURL_QRCode()';
begin
  Result := False;
  URL_QrCode   := EmptyStr;
  URL_Consulta := EmptyStr;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrQRCode_WS, DModG.AllID_DB, [
  'SELECT Servico, URL  ',
  'FROM qrcode_ws ',
  'WHERE UF="' + UF + '" ',
  'AND Versao=' + Geral.FFT_Dot(Versao, 4, siPositivo),
  'AND tpAmb=' + Geral.FF0(tpAmb),
  '']);
  QrQRCode_WS.First;
  while not QrQRCode_WS.Eof do
  begin
    if Uppercase(QrQRCode_WS.FieldByName('Servico').AsString) = Uppercase('QRCODE') then
      URL_QrCode := QrQRCode_WS.FieldByName('URL').AsString;
    if Uppercase(QrQRCode_WS.FieldByName('Servico').AsString) = Uppercase('CONSULTA') then
      URL_Consulta := QrQRCode_WS.FieldByName('URL').AsString;
    //
    QrQRCode_WS.Next;
  end;
  Result := (URL_QrCode <> EmptyStr) and (URL_Consulta <> EmptyStr);
  if not Result then
    Geral.MB_Erro('Não foi possível obter as "URL QRCode" e "URL Consulta" em ' + sProcName);
end;

procedure TDmNFe_0000.ObtemValorAproximadoDeTributos1(const UF, prod_NCM, prod_EXTIPI:
              String; ICMS_Orig: Integer; const prod_vProd, prod_vDesc: Double;
              var vBasTrib, pTotTrib, vTotTrib: Double; var tabTotTrib: Integer;
              var verTotTrib: String; var tpAliqTotTrib, fontTotTrib: Integer);
var
  EX: String;
  DiasExp: Integer;
  VigenFim: TDate;
begin
  pTotTrib      := 0;
  vTotTrib      := 0.0000;
  tabTotTrib    := Integer(TIBPTaxTabs.ibptaxtabNCM);
  verTotTrib    := '';
  tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNaoInfo);
  fontTotTrib   := Integer(TIBPTaxFont.ibptaxfontNaoInfo);
  //
  if prod_NCM = '' then
  begin
    Geral.MB_Aviso('Não foi possível obter o % de impostos da tabela IBPTax!' +
    slineBreak + 'NCM: não informado!' + sLineBreak + 'EX: ' + prod_EXTIPI);
    Exit;
  end else
  begin
    if prod_EXTIPI = '' then
      EX := '0'
    else
      EX := Geral.SoNumero_TT(prod_EXTIPI);
    //
    // ini 2020-11-01
    //ReopenIBPTax1(Integer(TIBPTaxTabs.ibptaxtabNCM), EX, prod_NCM, UF, VigenFim);
    ReopenIBPTax2(Integer(TIBPTaxTabs.ibptaxtabNCM), EX, prod_NCM, UF, VigenFim);
    // fim 2020-11-01
    if QrIBPTax.RecordCount > 0 then
    begin
      DiasExp := DaysBetween(DModG.ObtemAgora, VigenFim);
      //
      // 2020-11-01
      //if (DiasExp < 0) then
      if (DiasExp > 0) or (VigenFim < 2) then
      // Fim 2020-11-01
      begin
        Geral.MB_Aviso('O arquivo CSV com alíquotas para atendimento da lei de transparência está desatualizado.');
        //
        //Não calcula
        pTotTrib      := 0;
        tpAliqTotTrib := 0;
      end else
      begin
        if DiasExp <= 10 then
          Geral.MB_Aviso('O arquivo CVS com alíquotas para atendimento da lei de transparência fiscal precisa ser atualizado.' +
            sLineBreak + 'Caso contrário, o cálculo deixará de aparecer no cupom fiscal.' +
            sLineBreak + ' Faltam "' + Geral.FF0(DiasExp) + '" dias.');
      // ini 2020-11-01
      end;
        //
      begin
      // fim 2020-11-01
        if ICMS_Orig in ([0, 3, 4, 5]) then
        begin
          pTotTrib      := QrIBPTaxAliqNac.Value;
          tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNacional);
        end else
        begin
          pTotTrib      := QrIBPTaxAliqImp.Value;
          tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriImportado);
        end;
      end;
(*==============================================================================
LEI Nº 12.741, DE 8 DE DEZEMBRO DE 2012.
DECRETO Nº 8.264, DE 5 DE JUNHO DE 2014
§ 1º  Em relação à estimativa do valor dos tributos referidos no caput, não
serão computados valores que tenham sido eximidos por força de imunidades,
isenções, reduções e não incidências eventualmente ocorrentes.
================================================================================
Manual de Integração de Olho no Imposto - versão 0.0.6
b) Quando temos desconto no valor total do cupom como fica?
- No caso de desconto pelo total, para efeitos de cálculo do valor do imposto o
valor total descontado deve ser atribuído item a item, e, sobre o valor com
desconto incondicional deve ser aplicada a alíquota aproximada disponibilizada
pelo IBPT
==============================================================================*)
      vBasTrib      := prod_vProd - prod_vDesc;
      vTotTrib      := vBasTrib * pTotTrib / 100;
      tabTotTrib    := Integer(TIBPTaxTabs.ibptaxtabNCM);
      verTotTrib    := QrIBPTaxVersao.Value;
      fontTotTrib   := Integer(TIBPTaxFont.ibptaxfontIBPTax);
    end else
      Geral.MB_Aviso('Não foi possível obter o % de impostos da tabela IBPTax!' +
      sLineBreak + 'Verifique se a tabela está atualizada e/ou o NCM eo EXTIPI estão corretos! ' +
      slineBreak + 'NCM: ' + prod_NCM + sLineBreak + 'EX: ' + prod_EXTIPI);
  end;
(*
Presidência da República
Casa Civil
Subchefia para Assuntos Jurídicos

LEI Nº 12.741, DE 8 DE DEZEMBRO DE 2012.

DECRETO Nº 8.264, DE 5 DE JUNHO DE 2014
§ 1º  Em relação à estimativa do valor dos tributos referidos no caput, não serão computados valores que tenham sido eximidos por força de imunidades, isenções, reduções e não incidências eventualmente ocorrentes.


Vigência
Mensagem de veto
Regulamento
(Vide Constituição)

Dispõe sobre as medidas de esclarecimento ao consumidor, de que trata o § 5º do artigo 150 da Constituição Federal; altera o inciso III do art. 6º e o inciso IV do art. 106 da Lei nº 8.078, de 11 de setembro de 1990 - Código de Defesa do Consumidor.

A PRESIDENTA DA REPÚBLICA Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:

Art. 1º Emitidos por ocasião da venda ao consumidor de mercadorias e serviços, em todo território nacional, deverá constar, dos documentos fiscais ou equivalentes, a informação do valor aproximado correspondente à totalidade dos tributos federais, estaduais e municipais, cuja incidência influi na formação dos respectivos preços de venda.

§ 1º A apuração do valor dos tributos incidentes deverá ser feita em relação a cada mercadoria ou serviço, separadamente, inclusive nas hipóteses de regimes jurídicos tributários diferenciados dos respectivos fabricantes, varejistas e prestadores de serviços, quando couber.

§ 2º A informação de que trata este artigo poderá constar de painel afixado em local visível do estabelecimento, ou por qualquer outro meio eletrônico ou impresso, de forma a demonstrar o valor ou percentual, ambos aproximados, dos tributos incidentes sobre todas as mercadorias ou serviços postos à venda.

§ 3º Na hipótese do § 2º, as informações a serem prestadas serão elaboradas em termos de percentuais sobre o preço a ser pago, quando se tratar de tributo com alíquota ad valorem, ou em valores monetários (no caso de alíquota específica); no caso de se utilizar meio eletrônico, este deverá estar disponível ao consumidor no âmbito do estabelecimento comercial.

§ 4º ( VETADO).

§ 5º Os tributos que deverão ser computados são os seguintes:

I - Imposto sobre Operações relativas a Circulação de Mercadorias e sobre Prestações de Serviços de Transporte Interestadual e Intermunicipal e de Comunicação (ICMS);

II - Imposto sobre Serviços de Qualquer Natureza (ISS);

III - Imposto sobre Produtos Industrializados (IPI);

IV - Imposto sobre Operações de Crédito, Câmbio e Seguro, ou Relativas a Títulos ou Valores Mobiliários (IOF);

V - (VETADO);

VI - (VETADO);

VII - Contribuição Social para o Programa de Integração Social (PIS) e para o Programa de Formação do Patrimônio do Servidor Público (Pasep) - (PIS/Pasep);

VIII - Contribuição para o Financiamento da Seguridade Social (Cofins);

IX - Contribuição de Intervenção no Domínio Econômico, incidente sobre a importação e a comercialização de petróleo e seus derivados, gás natural e seus derivados, e álcool etílico combustível (Cide).

§ 6º Serão informados ainda os valores referentes ao imposto de importação, PIS/Pasep/Importação e Cofins/Importação, na hipótese de produtos cujos insumos ou componentes sejam oriundos de operações de comércio exterior e representem percentual superior a 20% (vinte por cento) do preço de venda.

§ 7º Na hipótese de incidência do imposto sobre a importação, nos termos do § 6o, bem como da incidência do Imposto sobre Produtos Industrializados - IPI, todos os fornecedores constantes das diversas cadeias produtivas deverão fornecer aos adquirentes, em meio magnético, os valores dos 2 (dois) tributos individualizados por item comercializado.

§ 8º Em relação aos serviços de natureza financeira, quando não seja legalmente prevista a emissão de documento fiscal, as informações de que trata este artigo deverão ser feitas em tabelas afixadas nos respectivos estabelecimentos.

§ 9º ( VETADO).

§ 10. A indicação relativa ao IOF (prevista no inciso IV do § 5º) restringe-se aos produtos financeiros sobre os quais incida diretamente aquele tributo.

§ 11. A indicação relativa ao PIS e à Cofins (incisos VII e VIII do § 5º), limitar-se-á à tributação incidente sobre a operação de venda ao consumidor.

§ 12. Sempre que o pagamento de pessoal constituir item de custo direto do serviço ou produto fornecido ao consumidor, deve ser divulgada, ainda, a contribuição previdenciária dos empregados e dos empregadores incidente, alocada ao serviço ou produto.

Art. 2º Os valores aproximados de que trata o art. 1º serão apurados sobre cada operação, e poderão, a critério das empresas vendedoras, ser calculados e fornecidos, semestralmente, por instituição de âmbito nacional reconhecidamente idônea, voltada primordialmente à apuração e análise de dados econômicos.

Art. 3º O inciso III do art 6º da Lei nº 8.078, de 11 de setembro de 1990, passa a vigorar com a seguinte redação:

"Art. 6º .......................................................................................................................

....................................................................................................................................

III - a informação adequada e clara sobre os diferentes produtos e serviços, com especificação correta de quantidade, características, composição, qualidade, tributos incidentes e preço, bem como sobre os riscos que apresentem;"

.....................................................................................................................................(NR)

Art. 4º ( VETADO).

Art. 5º O descumprimento do disposto nesta Lei sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 1990.
Art. 5o  Decorrido o prazo de doze meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Medida Provisória nº 620, de 2013)
Art. 5o  Decorrido o prazo de 12 (doze) meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Lei nº 12.868, de 2013)
Art. 5º  A fiscalização, no que se refere à informação relativa à carga tributária objeto desta Lei, será exclusivamente orientadora até 31 de dezembro de 2014.        (Redação dada pela Medida Provisória nº 649, de 2014)     (Vigência encerrada)
Art. 5o  Decorrido o prazo de 12 (doze) meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Lei nº 12.868, de 2013)

Art. 6º Esta Lei entra em vigor 6 (seis) meses após a data de sua publicação.

Brasília, 8 de dezembro de 2012; 191º da Independência e 124º da República.

DILMA ROUSSEFF
José Eduardo Cardozo
Guido Mantega

Este texto não substitui o publicado no DOU de 10.12.2012

*)
end;

procedure TDmNFe_0000.ObtemValorAproximadoDeTributos2(const UF, prod_NCM,
  prod_EXTIPI: String; ICMS_Orig: Integer; const prod_vProd, prod_vDesc: Double;
  var vBasTrib, pTotTrib, vTotTrib: Double; var tabTotTrib: Integer;
  var verTotTrib: String; var tpAliqTotTrib, FontTotTrib: Integer; var vTribFed,
  pTribFed, vTribEst, pTribEst, vTribMun, pTribMun: Double);
var
  EX: String;
  DiasExp: Integer;
  VigenFim: TDate;
begin
  pTotTrib      := 0;
  vTotTrib      := 0.0000;
  tabTotTrib    := Integer(TIBPTaxTabs.ibptaxtabNCM);
  verTotTrib    := '';
  tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNaoInfo);
  fontTotTrib   := Integer(TIBPTaxFont.ibptaxfontNaoInfo);
  vTribFed      := 0.00;
  pTribFed      := 0.0000;
  vTribEst      := 0.00;
  pTribEst      := 0.0000;
  vTribMun      := 0.00;
  pTribMun      := 0.00;
  //
  if prod_NCM = '' then
  begin
    Geral.MB_Aviso('Não foi possível obter o % de impostos da tabela IBPTax!' +
    slineBreak + 'NCM: não informado!' + sLineBreak + 'EX: ' + prod_EXTIPI);
    Exit;
  end else
  begin
    if prod_EXTIPI = '' then
      EX := '0'
    else
      EX := Geral.SoNumero_TT(prod_EXTIPI);
    //
    ReopenIBPTax2(Integer(TIBPTaxTabs.ibptaxtabNCM), EX, prod_NCM, UF, VigenFim);
    if QrIBPTax.RecordCount > 0 then
    begin
      DiasExp := DaysBetween(DModG.ObtemAgora, VigenFim);
      //
      if (DiasExp > 0) or (VigenFim < 2) then
      begin
        Geral.MB_Aviso('O arquivo CSV com alíquotas para atendimento da lei de transparência está desatualizado.');
      end else
      begin
        if DiasExp <= 10 then
          Geral.MB_Aviso('O arquivo CVS com alíquotas para atendimento da lei de transparência fiscal precisa ser atualizado.' +
            sLineBreak + 'Caso contrário, o cálculo deixará de aparecer no cupom fiscal.' +
            sLineBreak + ' Faltam "' + Geral.FF0(DiasExp) + '" dias.');
      end;
      //
      if ICMS_Orig in ([0, 3, 4, 5]) then
      begin
        //pTotTrib      := QrIBPTaxAliqNac.Value;
        tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNacional);
        pTribFed      := QrIBPTaxAliqNac.Value;
      end else
      begin
        //pTotTrib      := QrIBPTaxAliqImp.Value;
        tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriImportado);
        pTribFed      := QrIBPTaxAliqImp.Value;
      end;
      pTribEst := QrIBPTaxEstadual.Value;
      pTribMun := QrIBPTaxMunicipal.Value;
      //
      pTotTrib := pTribFed + pTribEst + pTribMun;
(*==============================================================================
LEI Nº 12.741, DE 8 DE DEZEMBRO DE 2012.
DECRETO Nº 8.264, DE 5 DE JUNHO DE 2014
§ 1º  Em relação à estimativa do valor dos tributos referidos no caput, não
serão computados valores que tenham sido eximidos por força de imunidades,
isenções, reduções e não incidências eventualmente ocorrentes.
================================================================================
Manual de Integração de Olho no Imposto - versão 0.0.6
b) Quando temos desconto no valor total do cupom como fica?
- No caso de desconto pelo total, para efeitos de cálculo do valor do imposto o
valor total descontado deve ser atribuído item a item, e, sobre o valor com
desconto incondicional deve ser aplicada a alíquota aproximada disponibilizada
pelo IBPT
==============================================================================*)
      vBasTrib      := prod_vProd - prod_vDesc;
      vTribFed      := vBasTrib * pTribFed / 100;
      vTribEst      := vBasTrib * pTribEst / 100;
      vTribMun      := vBasTrib * pTribMun / 100;
      vTotTrib      := vBasTrib * pTotTrib / 100;
      tabTotTrib    := Integer(TIBPTaxTabs.ibptaxtabNCM);
      verTotTrib    := QrIBPTaxVersao.Value;
      fontTotTrib   := Integer(TIBPTaxFont.ibptaxfontIBPTax);
    end else
      Geral.MB_Aviso('Não foi possível obter o % de impostos da tabela IBPTax!' +
      sLineBreak + 'Verifique se a tabela está atualizada e/ou o NCM eo EXTIPI estão corretos! ' +
      slineBreak + 'NCM: ' + prod_NCM + sLineBreak + 'EX: ' + prod_EXTIPI);
  end;
(*
Presidência da República
Casa Civil
Subchefia para Assuntos Jurídicos

LEI Nº 12.741, DE 8 DE DEZEMBRO DE 2012.

DECRETO Nº 8.264, DE 5 DE JUNHO DE 2014
§ 1º  Em relação à estimativa do valor dos tributos referidos no caput, não serão computados valores que tenham sido eximidos por força de imunidades, isenções, reduções e não incidências eventualmente ocorrentes.


Vigência
Mensagem de veto
Regulamento
(Vide Constituição)

Dispõe sobre as medidas de esclarecimento ao consumidor, de que trata o § 5º do artigo 150 da Constituição Federal; altera o inciso III do art. 6º e o inciso IV do art. 106 da Lei nº 8.078, de 11 de setembro de 1990 - Código de Defesa do Consumidor.

A PRESIDENTA DA REPÚBLICA Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:

Art. 1º Emitidos por ocasião da venda ao consumidor de mercadorias e serviços, em todo território nacional, deverá constar, dos documentos fiscais ou equivalentes, a informação do valor aproximado correspondente à totalidade dos tributos federais, estaduais e municipais, cuja incidência influi na formação dos respectivos preços de venda.

§ 1º A apuração do valor dos tributos incidentes deverá ser feita em relação a cada mercadoria ou serviço, separadamente, inclusive nas hipóteses de regimes jurídicos tributários diferenciados dos respectivos fabricantes, varejistas e prestadores de serviços, quando couber.

§ 2º A informação de que trata este artigo poderá constar de painel afixado em local visível do estabelecimento, ou por qualquer outro meio eletrônico ou impresso, de forma a demonstrar o valor ou percentual, ambos aproximados, dos tributos incidentes sobre todas as mercadorias ou serviços postos à venda.

§ 3º Na hipótese do § 2º, as informações a serem prestadas serão elaboradas em termos de percentuais sobre o preço a ser pago, quando se tratar de tributo com alíquota ad valorem, ou em valores monetários (no caso de alíquota específica); no caso de se utilizar meio eletrônico, este deverá estar disponível ao consumidor no âmbito do estabelecimento comercial.

§ 4º ( VETADO).

§ 5º Os tributos que deverão ser computados são os seguintes:

I - Imposto sobre Operações relativas a Circulação de Mercadorias e sobre Prestações de Serviços de Transporte Interestadual e Intermunicipal e de Comunicação (ICMS);

II - Imposto sobre Serviços de Qualquer Natureza (ISS);

III - Imposto sobre Produtos Industrializados (IPI);

IV - Imposto sobre Operações de Crédito, Câmbio e Seguro, ou Relativas a Títulos ou Valores Mobiliários (IOF);

V - (VETADO);

VI - (VETADO);

VII - Contribuição Social para o Programa de Integração Social (PIS) e para o Programa de Formação do Patrimônio do Servidor Público (Pasep) - (PIS/Pasep);

VIII - Contribuição para o Financiamento da Seguridade Social (Cofins);

IX - Contribuição de Intervenção no Domínio Econômico, incidente sobre a importação e a comercialização de petróleo e seus derivados, gás natural e seus derivados, e álcool etílico combustível (Cide).

§ 6º Serão informados ainda os valores referentes ao imposto de importação, PIS/Pasep/Importação e Cofins/Importação, na hipótese de produtos cujos insumos ou componentes sejam oriundos de operações de comércio exterior e representem percentual superior a 20% (vinte por cento) do preço de venda.

§ 7º Na hipótese de incidência do imposto sobre a importação, nos termos do § 6o, bem como da incidência do Imposto sobre Produtos Industrializados - IPI, todos os fornecedores constantes das diversas cadeias produtivas deverão fornecer aos adquirentes, em meio magnético, os valores dos 2 (dois) tributos individualizados por item comercializado.

§ 8º Em relação aos serviços de natureza financeira, quando não seja legalmente prevista a emissão de documento fiscal, as informações de que trata este artigo deverão ser feitas em tabelas afixadas nos respectivos estabelecimentos.

§ 9º ( VETADO).

§ 10. A indicação relativa ao IOF (prevista no inciso IV do § 5º) restringe-se aos produtos financeiros sobre os quais incida diretamente aquele tributo.

§ 11. A indicação relativa ao PIS e à Cofins (incisos VII e VIII do § 5º), limitar-se-á à tributação incidente sobre a operação de venda ao consumidor.

§ 12. Sempre que o pagamento de pessoal constituir item de custo direto do serviço ou produto fornecido ao consumidor, deve ser divulgada, ainda, a contribuição previdenciária dos empregados e dos empregadores incidente, alocada ao serviço ou produto.

Art. 2º Os valores aproximados de que trata o art. 1º serão apurados sobre cada operação, e poderão, a critério das empresas vendedoras, ser calculados e fornecidos, semestralmente, por instituição de âmbito nacional reconhecidamente idônea, voltada primordialmente à apuração e análise de dados econômicos.

Art. 3º O inciso III do art 6º da Lei nº 8.078, de 11 de setembro de 1990, passa a vigorar com a seguinte redação:

"Art. 6º .......................................................................................................................

....................................................................................................................................

III - a informação adequada e clara sobre os diferentes produtos e serviços, com especificação correta de quantidade, características, composição, qualidade, tributos incidentes e preço, bem como sobre os riscos que apresentem;"

.....................................................................................................................................(NR)

Art. 4º ( VETADO).

Art. 5º O descumprimento do disposto nesta Lei sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 1990.
Art. 5o  Decorrido o prazo de doze meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Medida Provisória nº 620, de 2013)
Art. 5o  Decorrido o prazo de 12 (doze) meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Lei nº 12.868, de 2013)
Art. 5º  A fiscalização, no que se refere à informação relativa à carga tributária objeto desta Lei, será exclusivamente orientadora até 31 de dezembro de 2014.        (Redação dada pela Medida Provisória nº 649, de 2014)     (Vigência encerrada)
Art. 5o  Decorrido o prazo de 12 (doze) meses, contado do início de vigência desta Lei, o descumprimento de suas disposições sujeitará o infrator às sanções previstas no Capítulo VII do Título I da Lei nº 8.078, de 11 de setembro de 1990.        (Redação dada pela Lei nº 12.868, de 2013)

Art. 6º Esta Lei entra em vigor 6 (seis) meses após a data de sua publicação.

Brasília, 8 de dezembro de 2012; 191º da Independência e 124º da República.

DILMA ROUSSEFF
José Eduardo Cardozo
Guido Mantega

Este texto não substitui o publicado no DOU de 10.12.2012

*)
end;

function TDmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFCe(const SerieDesfeita,
  NFDesfeita, SerieNormal, Controle, Empresa, Filial, MaxSeq: Integer; EdSerieNF,
  EdNumeroNF: TdmkEdit): Boolean;
const
  sProcName = 'TDmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFCe(';
var
  Serie, NumNF: Integer;
begin
  Result := False;
  DModG.ReopenParamsEmp(Empresa);
  //
  if (SerieDesfeita = SerieNormal) and (NFDesfeita <> 0) then
  begin
    Geral.MB_Info('Foi detectado que já havia sido usado o número ' +
    FormatFloat('000000000', NFDesfeita) +
    ' para esta NFe. Será usado este mesmo número novamente na recriação!');
    Serie := SerieDesfeita;
    NumNF := NFDesfeita;
    EdSerieNF.ValueVariant  := Serie;
    EdNumeroNF.ValueVariant := NumNF;
    Result := True;
  end else
  begin
    case DModG.QrParamsEmpNFetpEmis.Value of
      0:
      begin
        Geral.MB_Aviso(
        'A filial selecionada não tem configuração para "Tipo de emissão da NF-e"!'
        + sLineBreak + 'Empresa: ' + Geral.FF0(Empresa)
        + sLineBreak + 'Filial: ' + Geral.FF0(Filial));
      end;
      1, 6, 7, 9: // Normal, SVC-AN, SVC-RS e OFF LINE
      begin
        Serie := SerieNormal;
        Result := DModG.BuscaProximoCodigoInt_Novo('paramsnfcs', 'Sequencial',
        'WHERE Controle=' + dmkPF.FFP(Controle, 0), 0, 'Série: ' +
        FormatFloat('000', Serie) +
        sLineBreak + 'Filial: ' + dmkPF.FFP(Filial, 0) + sLineBreak +
        'Verifique a configuração da NF-e da filal informada!', NumNF, MaxSeq);
        EdSerieNF.ValueVariant  := Serie;
        EdNumeroNF.ValueVariant := NumNF;
      end;
      3:
      begin
        if Geral.MensagemBox(
        'A filial selecionada está configurada para envio de NF-e em contingência SCAN!'
        + sLineBreak + 'Tem certeza que deseja continuar?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          Serie := DModG.QrPrmsEmpNFeSCAN_Ser.Value;
          Result := DModG.BuscaProximoCodigoInt_Novo('paramsemp', 'SCAN_nNFC',
          'WHERE Codigo=' + dmkPF.FFP(Empresa, 0), 0, 'Série: ' +
          FormatFloat('000', Serie) +
          sLineBreak + 'Filial: ' + dmkPF.FFP(Filial, 0) + sLineBreak +
          'Verifique a configuração da NF-e em SCAN da filal informada!',
          NumNF, MaxSeq);
          EdSerieNF.ValueVariant  := Serie;
          EdNumeroNF.ValueVariant := NumNF;
        end;
      end;
      else Geral.MB_Erro('ide_tpEmis não configurado em ' + sProcName);
    end;
  end;
end;

function TDmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(const SerieDesfeita, NFDesfeita,
              SerieNormal, Controle, Empresa, Filial, MaxSeq: Integer;
              EdSerieNF, EdNumeroNF: TdmkEdit(*;
              var Serie, NumNF: Integer*)): Boolean;
const
  sProcName = 'TDmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(';
var
  Serie, NumNF: Integer;
begin
  Result := False;
  DModG.ReopenParamsEmp(Empresa);
  //
  if (SerieDesfeita = SerieNormal) and (NFDesfeita <> 0) then
  begin
    Geral.MB_Info('Foi detectado que já havia sido usado o número ' +
    FormatFloat('000000000', NFDesfeita) +
    ' para esta NFe. Será usado este mesmo número novamente na recriação!');
    Serie := SerieDesfeita;
    NumNF := NFDesfeita;
    EdSerieNF.ValueVariant  := Serie;
    EdNumeroNF.ValueVariant := NumNF;
    Result := True;
  end else
  begin
    case DModG.QrParamsEmpNFetpEmis.Value of
      1, 4, 6, 7: // 4 > 2023-07-08
      begin
        Serie := SerieNormal;
        Result := DModG.BuscaProximoCodigoInt_Novo('paramsnfs', 'Sequencial',
        'WHERE Controle=' + dmkPF.FFP(Controle, 0), 0, 'Série: ' +
        FormatFloat('000', Serie) +
        sLineBreak + 'Filial: ' + dmkPF.FFP(Filial, 0) + sLineBreak +
        'Verifique a configuração da NF-e da filal informada!', NumNF, MaxSeq);
        EdSerieNF.ValueVariant  := Serie;
        EdNumeroNF.ValueVariant := NumNF;
      end;
      3:
      begin
        if Geral.MensagemBox(
        'A filial selecionada está configurada para envio de NF-e em contingência SCAN!'
        + sLineBreak + 'Tem certeza que deseja continuar?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          Serie := DModG.QrPrmsEmpNFeSCAN_Ser.Value;
          Result := DModG.BuscaProximoCodigoInt_Novo('paramsemp', 'SCAN_nNF',
          'WHERE Codigo=' + dmkPF.FFP(Empresa, 0), 0, 'Série: ' +
          FormatFloat('000', Serie) +
          sLineBreak + 'Filial: ' + dmkPF.FFP(Filial, 0) + sLineBreak +
          'Verifique a configuração da NF-e em SCAN da filal informada!',
          NumNF, MaxSeq);
          EdSerieNF.ValueVariant  := Serie;
          EdNumeroNF.ValueVariant := NumNF;
        end;
      end;
      else Geral.MB_Erro('ide_tpEmis não configurado em ' + sProcName);
    end;
  end;
end;

procedure TDmNFe_0000.QrInfIntermedEntiCalcFields(DataSet: TDataSet);
begin
  QrInfIntermedEntiENDERECO.Value := Trim(QrInfIntermedEntiNO_LOGRAD.Value + ' ' +
    QrInfIntermedEntiRua.Value) + ', ' + Geral.FormataNumeroDeRua(
    QrInfIntermedEntiRua.Value, FormatFloat('0',
    QrInfIntermedEntiNumero.Value), False);
  if QrInfIntermedEntiCOMPL.Value <> '' then
    QrInfIntermedEntiENDERECO.Value := QrInfIntermedEntiENDERECO.Value + ' ' +
    QrInfIntermedEntiCOMPL.Value;
  if QrInfIntermedEntiBAIRRO.Value <> '' then
    QrInfIntermedEntiENDERECO.Value := QrInfIntermedEntiENDERECO.Value + ' - ' +
    QrInfIntermedEntiBAIRRO.Value;
  if QrInfIntermedEntiCEP.Value <> 0 then
    QrInfIntermedEntiENDERECO.Value := QrInfIntermedEntiENDERECO.Value + ' CEP ' +
    Geral.FormataCEP_TT(IntToStr(Trunc(QrInfIntermedEntiCEP.Value)));
end;

procedure TDmNFe_0000.QrTransportaCalcFields(DataSet: TDataSet);
begin
  QrTransportaENDERECO.Value := Trim(QrTransportaNO_LOGRAD.Value + ' ' +
    QrTransportaRua.Value) + ', ' + Geral.FormataNumeroDeRua(
    QrTransportaRua.Value, FormatFloat('0',
    QrTransportaNumero.Value), False);
  if QrTransportaCOMPL.Value <> '' then
    QrTransportaENDERECO.Value := QrTransportaENDERECO.Value + ' ' +
    QrTransportaCOMPL.Value;
  if QrTransportaBAIRRO.Value <> '' then
    QrTransportaENDERECO.Value := QrTransportaENDERECO.Value + ' - ' +
    QrTransportaBAIRRO.Value;
  if QrTransportaCEP.Value <> 0 then
    QrTransportaENDERECO.Value := QrTransportaENDERECO.Value + ' CEP ' +
    Geral.FormataCEP_TT(IntToStr(Trunc(QrTransportaCEP.Value)));

end;

procedure TDmNFe_0000.Qr_D_ECalcFields(DataSet: TDataSet);
begin
  Qr_D_EFatID_TXT.Value := NomeFatID_NFe(Qr_D_EFatID.Value);
end;

procedure TDmNFe_0000.ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmNFe_0000.ReopenAliq(IDCtrl, FisRegCad, CFOP_MesmaUF, CFOP_Contrib,
  CFOP_Proprio, CFOP_SubsTrib: Integer; UF_Emit, UF_Dest: String);
var
  Continua: Boolean;
begin
  Continua := False;
  //
  if IDCtrl <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAliq, Dmod.MyDB, [
      'SELECT frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, fru.* ',
      'FROM fatpedfis fru ',
      'LEFT JOIN fisregcad frc ON frc.Codigo=fru.FisRegCad ',
      'WHERE fru.FisRegCad=' + Geral.FF0(FisRegCad),
      'AND fru.IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
      //Geral.MB_SQL(nil, QrAliq);
    Continua := QrAliq.RecordCount = 0;
  end else
    Continua := True;
  //
  if Continua then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAliq, Dmod.MyDB, [
      'SELECT frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, fru.* ',
      'FROM fisregufs fru ',
      'LEFT JOIN fisregcad frc ON frc.Codigo=fru.Codigo ',
      'WHERE fru.Codigo=' + Geral.FF0(FisRegCad),
      'AND fru.Interno=' + Geral.FF0(CFOP_MesmaUF),
      'AND fru.Contribui=' + Geral.FF0(CFOP_Contrib),
      'AND fru.Proprio=' + Geral.FF0(CFOP_Proprio),
      'AND fru.SubsTrib=' + Geral.FF0(CFOP_SubsTrib),
      'AND fru.UFEmit="' + UF_Emit + '" ',
      'AND fru.UFDest="' + UF_Dest + '" ',
      '']);
      //Geral.MB_SQL(nil, QrAliq);
  end;
end;

procedure TDmNFe_0000.ReopenCSTsInn(FisRegCad, CFOP_MesmaUF, CFOP_Contrib,
  CFOP_Proprio, CFOP_Servico, CFOP_SubsTrib: Integer; CFOPFormatado, UF_Emit, UF_Dest: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCSTsInn, Dmod.MyDB, [
    'SELECT ',
    'frc.GenCtbD RegFisCad_CtbD, frc.GenCtbC RegFisCad_CtbC,',
    'frp.GenCtbD RegFisCFOP_CtbD, frp.GenCtbC RegFisCFOP_CtbC,',
    'fru.GenCtbD RegFisUFs_CtbD, fru.GenCtbC RegFisUFs_CtbC,',
    'frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, ',
    'frc.OriTES, frc.EFD_II_C195, ',
    'fru.* ',
    'FROM fisregufs fru ',
    'LEFT JOIN fisregcfop frp ON frp.Codigo=fru.Codigo ',
    '  AND frp.Interno=' + Geral.FF0(CFOP_MesmaUF),
    '  AND frp.Contribui=' + Geral.FF0(CFOP_Contrib),
    '  AND frp.Proprio=' + Geral.FF0(CFOP_Proprio),
    '  AND frp.SubsTrib=' + Geral.FF0(CFOP_SubsTrib),
    '  AND frp.Servico=' + Geral.FF0(CFOP_Servico),
    '  AND frp.OriCFOP="' + CFOPFormatado + '" ',
    'LEFT JOIN fisregcad frc ON frc.Codigo=fru.Codigo ',
    'WHERE fru.Codigo=' + Geral.FF0(FisRegCad),
    'AND fru.Interno=' + Geral.FF0(CFOP_MesmaUF),
    'AND fru.Contribui=' + Geral.FF0(CFOP_Contrib),
    'AND fru.Proprio=' + Geral.FF0(CFOP_Proprio),
    'AND fru.SubsTrib=' + Geral.FF0(CFOP_SubsTrib),
    'AND fru.Servico=' + Geral.FF0(CFOP_Servico),
    'AND fru.OriCFOP="' + CFOPFormatado + '" ',
    'AND fru.UFEmit="' + UF_Emit + '" ',
    'AND fru.UFDest="' + UF_Dest + '" ',
    '']);
    //Geral.MB_Teste(QrCSTsInn.SQL.Text);
end;

function TDmNFe_0000.ReopenDest(Dest: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDest, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  // NFe 3.10
  'en.EstrangDef, en.EstrangTip, en.EstrangNum, en.indIEDest,',
  //
  'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo, ',
  'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, ',
  'LUF, l2.Nome NO_LLOGRAD, LCodMunici, ',
  'ml.Nome NO_LMunici, ul.Nome NO_LUF, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',
  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.MadeBy ',
  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici ml ON ml.Codigo=en.LCodMunici ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ufs ul ON ul.Codigo=en.LUF ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Dest),
  '']);
  Result := (QrDest.RecordCount > 0);
  if not Result then
    Geral.MB_Aviso('Não foi possível reabrir a tabela da entidade ' + Geral.FF0(Dest) + '!');
end;

procedure TDmNFe_0000.ReopenEfdLinkCab(MovFatID, MovFatNum, MovimCod,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdLinkCab, Dmod.MyDB, [
  'SELECT Controle, SqLinked  ',
  'FROM efdinnnfscab ',
  'WHERE MovFatID=' + Geral.FF0(MovFatID),
  'AND MovFatNum=' +  Geral.FF0(MovFatNum),
  'AND MovimCod=' +  Geral.FF0(MovimCod),
  'AND Empresa=' +  Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenEfdLinkIts(MovFatID, MovFatNum, MovimCod, Empresa,
  SqLinked: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdLinkIts, Dmod.MyDB, [
  'SELECT Conta, SqLinked  ',
  'FROM efdinnnfsits ',
  'WHERE MovFatID=' + Geral.FF0(MovFatID),
  'AND MovFatNum=' +  Geral.FF0(MovFatNum),
  'AND MovimCod=' +  Geral.FF0(MovimCod),
  'AND Empresa=' +  Geral.FF0(Empresa),
  'AND SqLinked=' +  Geral.FF0(SqLinked),
  '']);
end;

function TDmNFe_0000.ReopenEmit(Emit: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo, ',
  'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, ',
  'LUF, l2.Nome NO_LLOGRAD, LCodMunici, ',
  'ml.Nome NO_LMunici, ul.Nome NO_LUF, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',
  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.IEST ',
  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici ml ON ml.Codigo=en.LCodMunici ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ufs ul ON ul.Codigo=en.LUF ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Emit),
  '']);
  //
  Result := (QrEmit.RecordCount > 0);
  if not Result then
    Geral.MB_Aviso('Não foi possível reabrir as tabelas da entidade ' + Geral.FF0(Emit) + '!');
end;

function TDmNFe_0000.ReopenEmpresa(Empresa: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',

  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial ',

  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Empresa),
  '']);
  //
  QrFilial.Close;
  QrFilial.Params[0].AsInteger := Empresa;
  UMyMod.AbreQuery(QrFilial, Dmod.MyDB, 'TDmNFe_0000.ReopenEmpresa()');
  //
  Result := (QrEmpresa.RecordCount > 0) and (QrFilial.RecordCount > 0);
  if not Result then Geral.MensagemBox(
  'Não foi possível reabrir as tabelas da empresa ' + IntToStr(Empresa) + '!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

function TDmNFe_0000.ReopenLocEve(Id: String; Evento: Integer): Boolean;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrLocEve, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM nfeevercab ',
  'WHERE chNFe="' + Id + '" ',
  'AND tpEvento=' + Geral.FF0(Evento),
  'AND ret_cStat in (135,136) ',
  '']);
  //
  Result := QrLocEveITENS.Value >= 1;
end;

procedure TDmNFe_0000.ReopenNFeArq(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, ',
  'caba.Id, caba.FatID, caba.FatNum, caba.Empresa ',
  'FROM nfecaba caba ',
  'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE caba.IDCtrl=' + Geral.FF0(IDCtrl),
  '']);
end;

function TDmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  if QrNFECabA.RecordCount = 0 then
    Result := -1
  else
    Result := QrNFECabAStatus.Value;
end;

procedure TDmNFe_0000.ReopenNFeCabB(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabB, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabb ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabF(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabF, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabf ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabG(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabG, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabg ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabGA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabGA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabga ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabXLac(FatID, FatNum, Empresa, Controle:
  Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXLac, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxlac ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabXReb(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXReb, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxreb ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabXVol(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXVol, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxvol ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabY(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabY, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecaby ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabYA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabYA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabya ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabZCon(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZCon, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzcon ',
  'WHERE FatID='  + Geral.FF0(FatID),
  'AND FatNum='   + Geral.FF0(FatNum),
  'AND Empresa='  + Geral.FF0(Empresa),
  '']);
  //
end;

procedure TDmNFe_0000.ReopenNFeCabZFis(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZFis, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzfis ',
  'WHERE FatID='  + Geral.FF0(FatID),
  'AND FatNum='   + Geral.FF0(FatNum),
  'AND Empresa='  + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeCabZPro(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZPro, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzpro ',
  'WHERE FatID='  + Geral.FF0(FatID),
  'AND FatNum='   + Geral.FF0(FatNum),
  'AND Empresa='  + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeLayI();
begin
  if (QrNFeLayI.State = dsInactive) or
//  (QrNFeLayI.Params[0].AsFloat <> DModG.QrPrmsEmpNFeversao.Value) then
  (FLastVerNFeLayI <> DModG.QrPrmsEmpNFeversao.Value) then
  begin
    FLastVerNFeLayI := DModG.QrPrmsEmpNFeversao.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFeLayI, Dmod.MyDB, [
    'SELECT layc.Nome NO_Grupo, layi.* ',
    'FROM nfelayi layi ',
    'LEFT JOIN nfelayc layc ON layc.Grupo=layi.Grupo ',
    'WHERE layi.Versao=' + Geral.FFT_Dot(FLastVerNFeLayI, 2, siNegativo),
    '']);
    //Geral.MB_Info(QrNFeLayI.SQL.Text)
  end;
end;

procedure TDmNFe_0000.ReopenNFeItsI(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI, Dmod.MyDB, [
  'SELECT iti.*, gg1.EX_TIPI ',
  'FROM nfeitsi iti ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=iti.GraGruX ',
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE iti.FatID=' + Geral.FF0(FatID),
  'AND iti.FatNum=' + Geral.FF0(FatNum),
  'AND iti.Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsI03(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI03, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsi03 ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsIDI(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsIDI, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsidi ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsIDIa(FatID, FatNum, Empresa, nItem,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsIDIa, Dmod.MyDB, [
  'SELECT nfedia.Adi_nDraw + 0.000 Adi_nDraw, nfedia.* ',
  'FROM nfeitsidia nfedia ',
  'WHERE nfedia.FatID=' + Geral.FF0(FatID),
  'AND nfedia.FatNum=' + Geral.FF0(FatNum),
  'AND nfedia.Empresa=' + Geral.FF0(Empresa),
  'AND nfedia.nItem=' + Geral.FF0(nItem),
  'AND nfedia.Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsINVE(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsINVE, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsinve ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsLA(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsLA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsla ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsM(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsM, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsm ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsN(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsN, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsn ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsNA(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsNA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsna ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsO(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsO, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitso ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsP(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsP, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsp ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsQ(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsQ, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsq ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsR(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsR, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsr ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsS(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsS, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitss ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsT(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsT, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitst ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsU(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsU, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsu ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsUA(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsUA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsua ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsV(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsV, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsv ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

procedure TDmNFe_0000.ReopenNFeItsVA(FatID, FatNum, Empresa, nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsVA, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsva ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND nItem=' + Geral.FF0(nItem),
  '']);
end;

function TDmNFe_0000.ReopenOpcoesNFe(Empresa: Integer; Avisa: Boolean): Boolean;
begin
  if Empresa > 0 then
    Geral.MensagemBox('CUIDADO!!! Empresa = ' + IntToStr(Empresa) +
    ' (Opções NF-e)', 'Aviso', MB_OK+MB_ICONWARNING);
  QrOpcoesNFe.Close;
  QrOpcoesNFe.Params[0].AsInteger := Empresa;
  UMyMod.AbreQuery(QrOpcoesNFe, Dmod.MyDB, 'TDmNFe_0000.ReopenOpcoesNFe()');
  Result := QrOpcoesNFe.RecordCount > 0;
  if not Result and Avisa then
    Geral.MB_Aviso('Não existem opções de NF-e cadastradas!');
end;

procedure TDmNFe_0000.ReopenSMVA(Tipo, Empresa, FatPedCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSMVA, Dmod.MyDB, [
  'SELECT pgt.MadeBy, gg1.UsaSubsTrib, smv.ID',
  'FROM stqmovvala smv',
  'LEFT JOIN gragrux ggx ON ggx.Controle=smv.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE smv.Tipo=' + Geral.FF0(Tipo),
  'AND smv.Empresa=' + Geral.FF0(Empresa),
  'AND smv.OriCodi=' + Geral.FF0(FatPedCab),
  '']);
end;

{
function TDmNFe_0000.AppCodeNFe(Empresa: Integer): Byte;
begin
  ReopenOpcoesNFe(Empresa, True);
  Result := QrOpcoesNFeAppCode.Value;
end;
}

function TDmNFe_0000.AtualizaRecuperacaoDeImpostos(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq,
  ICMSRec_vICMS, IPIRec_pRedBC, IPIRec_vBC,
  IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
  PISRec_vBC, PISRec_pAliq, PISRec_vPIS,
  COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
  COFINSRec_vCOFINS: Double;
begin
  QrSumRetFis.Close;
  QrSumRetFis.Database := Dmod.MyDB; // 2020-08-31
  QrSumRetFis.Params[00].AsInteger := FatID;
  QrSumRetFis.Params[01].AsInteger := FatNum;
  QrSumRetFis.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrSumRetFis, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  ICMSRec_pRedBC     := QrSumRetFisICMSRec_pRedBC   .Value;
  ICMSRec_vBC        := QrSumRetFisICMSRec_vBC      .Value;
  ICMSRec_pAliq      := QrSumRetFisICMSRec_pAliq    .Value;
  ICMSRec_vICMS      := QrSumRetFisICMSRec_vICMS    .Value;
  IPIRec_pRedBC      := QrSumRetFisIPIRec_pRedBC    .Value;
  IPIRec_vBC         := QrSumRetFisIPIRec_vBC       .Value;
  IPIRec_pAliq       := QrSumRetFisIPIRec_pAliq     .Value;
  IPIRec_vIPI        := QrSumRetFisIPIRec_vIPI      .Value;
  PISRec_pRedBC      := QrSumRetFisPISRec_pRedBC    .Value;
  PISRec_vBC         := QrSumRetFisPISRec_vBC       .Value;
  PISRec_pAliq       := QrSumRetFisPISRec_pAliq     .Value;
  PISRec_vPIS        := QrSumRetFisPISRec_vPIS      .Value;
  COFINSRec_pRedBC   := QrSumRetFisCOFINSRec_pRedBC .Value;
  COFINSRec_vBC      := QrSumRetFisCOFINSRec_vBC    .Value;
  COFINSRec_pAliq    := QrSumRetFisCOFINSRec_pAliq  .Value;
  COFINSRec_vCOFINS  := QrSumRetFisCOFINSRec_vCOFINS.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  'ICMSRec_pRedBC', 'ICMSRec_vBC', 'ICMSRec_pAliq',
  'ICMSRec_vICMS', 'IPIRec_pRedBC', 'IPIRec_vBC',
  'IPIRec_pAliq', 'IPIRec_vIPI', 'PISRec_pRedBC',
  'PISRec_vBC', 'PISRec_pAliq', 'PISRec_vPIS',
  'COFINSRec_pRedBC', 'COFINSRec_vBC', 'COFINSRec_pAliq',
  'COFINSRec_vCOFINS'], [
  'FatID', 'FatNum', 'Empresa'], [
  ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq,
  ICMSRec_vICMS, IPIRec_pRedBC, IPIRec_vBC,
  IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
  PISRec_vBC, PISRec_pAliq, PISRec_vPIS,
  COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
  COFINSRec_vCOFINS], [
  FatID, FatNum, Empresa], True);
end;

procedure TDmNFe_0000.AtualizaTotaisEFDInnNFs(Controle: Integer);
var
  VL_DOC, VL_DESC, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, VL_ICMS_ST,VL_IPI, VL_PIS, VL_COFINS: Double;
  SQLType: TSQLType;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumNFsInn, Dmod.MyDB, [
  'SELECT ',
  'SUM(VL_ITEM) VL_DOC, ',
  'SUM(VL_DESC) VL_DESC, ',
  'SUM(prod_vProd) VL_MERC, ',
  'SUM(prod_vFrete) VL_FRT, ',
  'SUM(prod_vSeg) VL_SEG, ',
  'SUM(prod_vOutro) VL_OUT_DA, ',
  'SUM(VL_BC_ICMS) VL_BC_ICMS, ',
  'SUM(VL_ICMS) VL_ICMS, ',
  'SUM(VL_BC_ICMS_ST) VL_BC_ICMS_ST, ',
  'SUM(VL_ICMS_ST) VL_ICMS_ST, ',
  'SUM(VL_BC_IPI) VL_BC_IPI, ',
  'SUM(VL_IPI) VL_IPI, ',
  'SUM(VL_BC_PIS) VL_BC_PIS, ',
  'SUM(VL_PIS) VL_PIS, ',
  'SUM(VL_BC_COFINS) VL_BC_COFINS, ',
  'SUM(VL_COFINS) VL_COFINS ',
  'FROM efdinnnfsits ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
  VL_DOC        := QrSumNFsInnVL_DOC.Value;
  VL_DESC       := QrSumNFsInnVL_DESC.Value;
  VL_MERC       := QrSumNFsInnVL_MERC.Value;
  VL_FRT        := QrSumNFsInnVL_FRT.Value;
  VL_SEG        := QrSumNFsInnVL_SEG.Value;
  VL_OUT_DA     := QrSumNFsInnVL_OUT_DA.Value;
  VL_BC_ICMS    := QrSumNFsInnVL_BC_ICMS.Value;
  VL_ICMS       := QrSumNFsInnVL_ICMS.Value;
  VL_BC_ICMS_ST := QrSumNFsInnVL_BC_ICMS_ST.Value;
  VL_ICMS_ST    := QrSumNFsInnVL_ICMS_ST.Value;
  VL_IPI        := QrSumNFsInnVL_IPI.Value;
  VL_PIS        := QrSumNFsInnVL_PIS.Value;
  VL_COFINS     := QrSumNFsInnVL_COFINS.Value;
  //
  SQLType := stUpd;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnnfscab', False, [
  'VL_DESC', ' VL_MERC', ' VL_FRT',
  'VL_SEG', ' VL_OUT_DA', ' VL_BC_ICMS',
  'VL_ICMS', ' VL_BC_ICMS_ST', ' VL_ICMS_ST',
  'VL_IPI', ' VL_PIS', ' VL_COFINS',
  'VL_DOC'], [
  'Controle'], [
  VL_DESC, VL_MERC, VL_FRT,
  VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_IPI, VL_PIS, VL_COFINS,
  VL_DOC
  ], [
  Controle], True) then
  begin
    //
  end;
end;

function TDmNFe_0000.AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
var
  Aviso, CNPJ_CPF, Dir: String;
begin
  Screen.Cursor := crHourGlass;
{
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE nfecaba SET protNFe_versao=1.10');
  DMod.QrUpd.SQL.Add('WHERE protNFe_versao=0.0 AND infProt_cStat=100');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE nfecaba SET retCancNFe_versao=1.07');
  DMod.QrUpd.SQL.Add('WHERE retCancNFe_versao=0.0 AND infCanc_cStat=101');
  DMod.QrUpd.ExecSQL;
}
  //
  if VAR_LIB_EMPRESA_SEL = 0 then
  begin
    Geral.MensagemBox('Empresa logada não definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
    Exit;
  end;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  CNPJ_CPF := DModG.QrParamsEmpCNPJ_CPF.Value;
  if CNPJ_CPF = '' then
  begin
    Geral.MensagemBox('CNPJ ou CPF da Empresa logada não definido!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
    Exit;
  end;
  Aviso := '';

  //

  // TODO NFe 3.10
  // Permitir cliente editar nfearq e dizer que nao quer mais que procure o XML
  Dir := DModG.QrPrmsEmpNFeDirNFeAss.Value;
(*
  QrAtoArq.Close;
  QrAtoArq.Params[00].AsString := CNPJ_CPF;
  QrAtoArq.Params[01].AsString := CNPJ_CPF;
  QrAtoArq. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtoArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl ',
  'FROM nfecaba caba ',
  'LEFT JOIN nfearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE arq.IDCtrl IS NULL ',
  'AND Id <> "" ',
  'AND ( ',
  '  emit_CNPJ="' + CNPJ_CPF + '" ',
  '  OR ',
  '  emit_CPF="' + CNPJ_CPF + '" ',
  ') ',
  '']);
  //
  QrAtoArq.First;
  while not QrAtoArq.Eof do
  begin
    AtualizaXML_No_BD_NFe(QrAtoArqIDCtrl.Value, Dir, Aviso);
    //
    QrAtoArq.Next;
  end;

  //

  Dir := DModG.QrPrmsEmpNFeDirProRec.Value;
  QrNFeAut.Close;
  QrNFeAut.Database := Dmod.MyDB; // 2020-08-31
  QrNFeAut.Params[00].AsString := CNPJ_CPF;
  QrNFeAut.Params[01].AsString := CNPJ_CPF;
  UnDmkDAC_PF.AbreQuery(QrNFeAut, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrNFeAut.First;
  while not QrNFeAut.Eof do
  begin
    AtualizaXML_No_BD_Aut(QrNFeAutIDCtrl.Value, QrNFeAutLoteEnv.Value,
      QrNFeAutId.Value, Dir, Aviso);
    //
    QrNFeAut.Next;
  end;

  //

  Dir := DModG.QrPrmsEmpNFeDirCan.Value;
  QrNFeCan.Close;
  QrNFeCan.Database := Dmod.MyDB; // 2020-08-31
  QrNFeCan.Params[00].AsString := CNPJ_CPF;
  QrNFeCan.Params[01].AsString := CNPJ_CPF;
  UnDmkDAC_PF.AbreQuery(QrNFeCan, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrNFeCan.First;
  while not QrNFeCan.Eof do
  begin
    AtualizaXML_No_BD_Can(QrNFeCanIDCtrl.Value, QrNFeCanId.Value, Dir, Aviso);
    //
    QrNFeCan.Next;
  end;

  //

  Dir := DModG.QrPrmsEmpNFeDirInu.Value;
  QrNFeInu.Close;
  QrNFeInu.Database := Dmod.MyDB; // 2020-08-31
  UnDmkDAC_PF.AbreQuery(QrNFeInu, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrNFeInu.First;
  while not QrNFeInu.Eof do
  begin
    AtualizaXML_No_BD_Inu(QrNFeInuCodigo.Value, Dir, Aviso);
    //
    QrNFeInu.Next;
  end;

  //
  if (VAR_USUARIO = -1) then
  begin
    if Aviso <> '' then Geral.MensagemBox(
    'Os arquivos abaixo não foram localizados:' + sLineBreak + Aviso,
    'Aviso', MB_OK+MB_ICONWARNING);
  end;

  if InfoTermino then
    Geral.MensagemBox('Verificação finalizada', 'Aviso', MB_OK+MB_ICONWARNING);

  Screen.Cursor := crDefault;
  Result := true;
end;

function TDmNFe_0000.AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer; const Id:
 String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Aut: String;
  Texto: TStringList;
  XML_LoteEnv: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := FormataLoteNFe(LoteEnv) + NFE_EXT_PRO_REC_XML;
  Geral.VerificaDir(Dir, '\', 'Lote de NFe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_LoteEnv := PChar(Texto.Text);
    buf := Copy(XML_LoteEnv, 1);
    Ini := pos('<protNFe', buf);
    while Ini > 0 do
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protNFe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protNFe>', buf);
        XML_Aut := '<' + Copy(buf, 1, Fim + Length('</protNFe>') - 1);
        XML_Aut := Geral.WideStringToSQLString(XML_Aut);
        if (pos(Id, XML_Aut) > 0) and (pos('<cStat>100</cStat>', XML_Aut) > 0) then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
          'XML_Aut'], ['IDCtrl'], [XML_Aut], [IDCtrl], True);
        end;
      end;
      Ini := pos('<protNFe', buf);
    end;
    Texto.Free;
  end;
  Result := True;
end;

function TDmNFe_0000.AtualizaXML_No_BD_AutConfirmada(const chNFe, XML_Aut: String): Boolean;
var
  IDCtrl, FatID, FatNum, Empresa: Integer;
  SQLType: TSQLType;
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IDCtrl ',
      'FROM nfecaba ',
      'WHERE id="' + chNFe + '" ',
      '']);
    //
    IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
    //
    if IDCtrl = 0 then
    begin
      (* Não mostrar mensagem
      Geral.MB_Erro('"IDCtrl" não definido em "DmNFe_0000.AtualizaXML_No_BD_AutConfirmada()"');
      *)
      Exit;
    end;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
      'XML_Aut'], ['IDCtrl'], [XML_Aut], [IDCtrl], True);
    //
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.AtualizaXML_No_BD_Aut_Sinc(const IDCtrl, LoteEnv: Integer; const Id:
 String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Aut: String;
  Texto: TStringList;
  XML_LoteEnv: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
(*
- <protNFe versao="3.10">
- <infProt Id="ID141140001620054">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <chNFe>41141002717861000110550010000045811360413650</chNFe>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
  <nProt>141140001620054</nProt>
  <digVal>gzIavxRcIKOO6WbOu2M+HwEH0Cg=</digVal>
  <cStat>100</cStat>
  <xMotivo>Autorizado o uso da NF-e</xMotivo>
  </infProt>
  </protNFe>
  </retEnviNFe>

  /
*)


  // Pega do recibo e nao do retorno!
  //Arq := FormataLoteNFe(LoteEnv) + NFE_EXT_PRO_REC_XML;
  Arq := FormataLoteNFe(LoteEnv) + NFE_EXT_REC_XML;
  Geral.VerificaDir(Dir, '\', 'Lote de NFe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_LoteEnv := PChar(Texto.Text);
    buf := Copy(XML_LoteEnv, 1);
    Ini := pos('<protNFe', buf);
    while Ini > 0 do
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protNFe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protNFe>', buf);
        XML_Aut := '<' + Copy(buf, 1, Fim + Length('</protNFe>') - 1);
        XML_Aut := Geral.WideStringToSQLString(XML_Aut);
        if (pos(Id, XML_Aut) > 0) and (pos('<cStat>100</cStat>', XML_Aut) > 0) then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
          'XML_Aut'], ['IDCtrl'], [XML_Aut], [IDCtrl], True);
        end;
      end;
      Ini := pos('<protNFe', buf);
    end;
    Texto.Free;
  end;
  Result := True;
end;

function TDmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(const Chave, Id: String;
const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Sit: String;
  Texto: TStringList;
  XML_ConsultaNFe: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := Chave + NFE_EXT_SIT_XML;
  Geral.VerificaDir(Dir, '\', 'Lote de NFe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_ConsultaNFe := PChar(Texto.Text);
    buf := Copy(XML_ConsultaNFe, 1);
    Ini := pos('<protNFe', buf);
    if Ini > 0 then
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protNFe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protNFe>', buf);
        XML_Sit := '<' + Copy(buf, 1, Fim + Length('</protNFe>') - 1);
        XML_Sit := Geral.WideStringToSQLString(XML_Sit);
        //
        // Autorizado
        if (pos(Id, XML_Sit) > 0) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
        begin
          QrArq.Close;
          QrArq.Database := Dmod.MyDB; // 2020-08-31
          QrArq.Params[00].AsInteger := IDCtrl;
          UnDmkDAC_PF.AbreQuery(QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
          if QrArqXML_Aut.Value = '' then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
            'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
          end;
        end;
      end;
    end
    else
    begin
//<retCancNFe versao="2.00" xmlns="http://www.portalfiscal.inf.br/nfe"><infCanc Id="ID415100008891288"><tpAmb>1</tpAmb><verAplic>SVAN_4.00</verAplic><cStat>101</cStat><xMotivo>Cancelamento de NF-e homologado</xMotivo><cUF>15</cUF><chNFe>15100804333952000188550010000005458605003992</chNFe><dhRecbto>2010-09-01T15:17:44</dhRecbto><nProt>415100008891288</nProt></infCanc></retCancNFe>
//<retConsSitNFe versao="1.07" xmlns="http://www.portalfiscal.inf.br/nfe"><infProt Id="ID15100404333952000188550010000000019456724199"><tpAmb>1</tpAmb><verAplic>4.00</verAplic><cStat>101</cStat><xMotivo>Cancelamento de NF-e homologado</xMotivo><cUF>15</cUF><chNFe>15100404333952000188550010000000019456724199</chNFe><dhRecbto>2010-04-06T09:01:06</dhRecbto><nProt>415100002950345</nProt><digVal>FJMyliJj5bz1SF4co5msqKc6NhE=</digVal></infProt></retConsSitNFe>
      Ini := pos('<retConsSitNFe', buf);
      if Ini > 0 then
      begin
        Prot := '';
        Geral.SeparaPrimeiraOcorrenciaDeTexto('<retConsSitNFe', buf, Prot, buf);
        if Prot <> '' then
        begin
          Fim := pos('</retConsSitNFe>', buf);
          XML_Sit := '<' + Copy(buf, 1, Fim + Length('</retConsSitNFe>') - 1);
          XML_Sit := Geral.WideStringToSQLString(XML_Sit);
          //
          // Autorizado
          // neste caso o ID é a própria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Database := Dmod.MyDB; // 2020-08-31
            QrArq.Params[00].AsInteger := IDCtrl;
            UnDmkDAC_PF.AbreQuery(QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
              'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
          //
          // Cancelado
          // neste caso o ID é a própria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>101</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Database := Dmod.MyDB; // 2020-08-31
            QrArq.Params[00].AsInteger := IDCtrl;
            UnDmkDAC_PF.AbreQuery(QrArq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'NFeArq', False, [
              'XML_Can'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
        end;
      end;
    end;
    //
    Texto.Free;
  end;
  Result := True;
end;

function TDmNFe_0000.AtualizaXML_No_BD_EveConfirmado(
                    EventoLote: Integer; Id, cOrgao,
                    tpAmb: String; TipoEnt: Integer; CNPJ,
                    CPF, chNFe, dhEvento: String;
                    TZD_UTC: Double; verEvento, tpEvento,
                    nSeqEvento, versao, descEvento,
                    XML_Eve, XML_retEve, Status,
                    ret_versao, ret_Id, ret_tpAmb,
                    ret_verAplic, ret_cOrgao, ret_cStat,
                    ret_xMotivo, ret_chNFe, ret_tpEvento,
                    ret_xEvento, ret_nSeqEvento, ret_CNPJDest,
                    ret_CPFDest, ret_emailDest, ret_dhRegEvento: String;
                    ret_TZD_UTC: Double; ret_nProt: String): Boolean;
var
  IDCtrl, FatID, FatNum, Empresa, Controle: Integer;
  //
  SQLType: TSQLType;
  Qry: TmySQLQuery;
  xmlNode: IXMLNode;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IDCtrl ',
    'FROM nfecaba ',
    'WHERE id="' + chNFe + '" ',
    '']);
    IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
    //
    if IDCtrl = 0 then
    begin
      (* Não mostrar mensagem
      Geral.MB_Erro(
      '"IDCtrl" não definido em "DmNFe_0000.AtualizaXML_No_BD_EveConfirmado()"');
      *)
      Exit;
    end;
    ReopenNfeArq(IDCtrl);
    //
    FatID        := QrNFeArqFatID.Value;
    FatNum       := QrNFeArqFatNum.Value;
    Empresa      := QrNFeArqEmpresa.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM nfeevercab ',
    'WHERE chNFe="' + chNFe + '" ',
    'AND tpEvento=' + tpEvento,
    'AND nSeqEvento=' + nSeqEvento,
    '']);
    Controle := Qry.FieldByName('Controle').AsInteger;
    //
    if Controle = 0 then
    begin
      SQLType := stIns;
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevercab', '', 0);
    end else begin
      SQLType := stUpd;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeevercab', False, [
    'FatID', 'FatNum', 'Empresa',
    'EventoLote', 'Id', 'cOrgao',
    'tpAmb', 'TipoEnt', 'CNPJ',
    'CPF', 'chNFe', 'dhEvento',
    'TZD_UTC', 'verEvento', 'tpEvento',
    'nSeqEvento', 'versao', 'descEvento',
    'XML_Eve', 'XML_retEve', 'Status',
    'ret_versao', 'ret_Id', 'ret_tpAmb',
    'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
    'ret_xMotivo', 'ret_chNFe', 'ret_tpEvento',
    'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
    'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
    'ret_TZD_UTC', 'ret_nProt'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    EventoLote, Id, cOrgao,
    tpAmb, TipoEnt, CNPJ,
    CPF, chNFe, dhEvento,
    TZD_UTC, verEvento, tpEvento,
    nSeqEvento, versao, descEvento,
    XML_Eve, XML_retEve, Status,
    ret_versao, ret_Id, ret_tpAmb,
    ret_verAplic, ret_cOrgao, ret_cStat,
    ret_xMotivo, ret_chNFe, ret_tpEvento,
    ret_xEvento, ret_nSeqEvento, ret_CNPJDest,
    ret_CPFDest, ret_emailDest, ret_dhRegEvento,
    ret_TZD_UTC, ret_nProt], [
    Controle], True);
  finally
    Qry.Free;
  end;
end;

function TDmNFe_0000.AtualizaXML_No_BD_Can(const IDCtrl: Integer; const Id: String;
  var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_Can: String;
  Texto: TStringList;
  XML_All: PChar;
  Ini: Integer;
begin
  ReopenNFeArq(IDCtrl);
  //
  //if QrNFeArqIDCtrl_Arq.Value = 0 then
  //begin
    Arq := QrNFeArqId.Value + NFE_EXT_CAN_XML;
    Geral.VerificaDir(Dir, '\', 'NFe cancelada', True);
    DirArq := Dir + Arq;
    if not FileExists(DirArq) then
    begin
      Aviso := Aviso + DirArq + sLineBreak;
    end else begin
      buf := '';
      Texto := TStringList.Create;
      Texto.Clear;
      Texto.LoadFromFile(DirArq);
      XML_All := PChar(Texto.Text);
      buf          := Copy(XML_All, 1);
      Ini          := pos('<retCancNFe', buf);
      XML_Can      := Copy(buf, Ini);
      XML_Can      := Geral.WideStringToSQLString(XML_Can);
      //
      //Geral.MensagemBox(XML_Can, 'Ativo', MB_OK+MB_ICONWARNING);
      if XML_Can <> '' then      
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, [
      'XML_Can'], ['IDCtrl'], [XML_Can ], [ IDCtrl ], True);

      Texto.Free;
    end;
  //end;
  Result := True;
end;

function TDmNFe_0000.AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  Id, XML_Inu: String;
  Texto: TStringList;
  XML_Txt: PChar;
  //FatID, FatNum, Empresa: Integer;
begin
  MontaID_Inutilizacao(FormatFloat('00', QrNFeInucUF.Value),
  FormatFloat('00', QrNFeInuano.Value), QrNFeInuCNPJ.Value,
  FormatFloat('00', QrNFeInumodelo.Value),
  FormatFloat('0', QrNFeInuSerie.Value),
  FormatFloat('000000000', QrNFeInunNFIni.Value),
  FormatFloat('000000000', QrNFeInunNFFim.Value), Id);
  //
  Arq := Id + '_' + FormatFloat('000000000', Codigo) + NFE_EXT_INU_XML;
  Geral.VerificaDir(Dir, '\', 'NFe inutilizada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Txt      := PChar(Texto.Text);
    buf          := Copy(XML_Txt, 1);
    XML_Inu      := Geral.WideStringToSQLString(buf);
    //
    if XML_Inu <> '' then
    
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeinut', False, [
    'XML_Inu'], ['Codigo'], [XML_Inu ], [Codigo], True);
    Texto.Free;
  end;
  Result := true;
end;

function TDmNFe_0000.AtualizaXML_No_BD_NFe(const IDCtrl: Integer; var Dir, Aviso:
String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_NFe: String;
  Texto: TStringList;
  XML_Assinado: PChar;
  FatID, FatNum, Empresa: Integer;
  //
  SQLType: TSQLType;
begin
  ReopenNFeArq(IDCtrl);
  //
  Arq := QrNFeArqId.Value + NFE_EXT_NFE_XML;
  Geral.VerificaDir(Dir, '\', 'NFe assinada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Assinado := PChar(Texto.Text);
    buf          := Copy(XML_Assinado, 1);
    XML_NFe      := Geral.WideStringToSQLString(buf);
    //
    FatID        := QrNFeArqFatID.Value;
    FatNum       := QrNFeArqFatNum.Value;
    Empresa      := QrNFeArqEmpresa.Value;
    //
    if QrNFeArqIDCtrl_Arq.Value = 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfearq WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[00].AsInteger := IDCtrl;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfearq ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      SQLType := stIns;
    end else begin
      SQLType := stUpd;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfearq', False, [
    'IDCtrl', 'XML_NFe'], ['FatID', 'FatNum', 'Empresa'], [
     IDCtrl,   XML_NFe ], [ FatID,   FatNum,   Empresa ], True);
    Texto.Free;
  end;
  Result := True;
end;

function TDmNFe_0000.LocalizaFiliaByDocum(Docum: String): Integer;
var
  Doc, DBDoc: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Doc    := Geral.SoNumero_TT(Docum);
  //
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ent.Codigo, IF(ent.Tipo = 0, ent.CNPJ, ent.CPF) Docum ',
      'FROM enticliint eci ',
      'LEFT JOIN entidades ent ON ent.Codigo = CodEnti ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        DBDoc := Geral.SoNumero_TT(Qry.FieldByName('Docum').AsString);
        //
        if DBDoc = Doc then
        begin
          Result := Qry.FieldByName('Codigo').AsInteger;
          Exit;
        end;
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmNFe_0000.ReopenTransporta(Transporta: Integer);
begin
  {
  QrTransporta.Close;
  QrTransporta.Params[0].AsInteger := Transporta;
  UMyMod.AbreQuery(QrTransporta, Dmod.MyDB, 'TDmNFe_0000.ReopenTransporta()');
  }
  UnDmkDAC_PF.AbreMySQLQuery0(QrTransporta, Dmod.MyDB, [
    'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
    'en.IE, en.RG, en.NIRE, en.CNAE, ',
    'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
    'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
    'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
    'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
    'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
    'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
    'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
    'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
    'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
    'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
    'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF ',
    'FROM entidades en ',
    'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
    'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
    'WHERE en.Codigo=' + Geral.FF0(Transporta),
    '']);
end;

function TDmNFe_0000.SalvaXML(Ext, Arq: String; Texto: String; rtfRetWS:
  TMemo; Assinado: Boolean): String;
var
  Dir, Arquivo, xxx: String;
  TxtXML: TextFile;
  P: Integer;
begin
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    P := pos(Ext, Arq);
    if P = 0 then
    begin
      // Usar extensão correta para xml de NFe!
      if (Ext = NFE_EXT_RET_DOW_NFE_NFE_XML)
      or (Ext = NFE_EXT_RET_CNF_NFE_NFE_XML) then
        xxx := NFE_EXT_NFE_XML
      else
        xxx := Ext;
      Arquivo := Dir + Arq + xxx
    end else
      Arquivo := Dir + Arq; // já tem Ext ?
    //
    AssignFile(TxtXML, Arquivo);
    Rewrite(TxtXML);
    Write(TxtXML, Texto);
    CloseFile(TxtXML);
    //
    Result := Arquivo;
    if rtfRetWS <> nil then
      rtfRetWS.Text := Texto;
  end else
    Result := '';
end;

procedure TDmNFe_0000.AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean);
var
  Dir, Arquivo: String;
  PWC: array[0..127] of WideChar;
  Tam: Integer;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    Tam := Length(Arquivo) + 1;
    StringToWideChar(Arquivo, PWC, Tam);
    try
      HlinkNavigateString(nil, PWC);
    finally
      //
    end;
  end;
end;

function TDmNFe_0000.Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean): String;
var
  Dir: String;
begin
  Result := '';
  //
  if not ReopenEmpresa(Empresa) then Exit;
  //
  if ObtemDirXML(Ext, Dir, Assinado) then
    Result := Dir + Arq + Ext;
end;

procedure TDmNFe_0000.AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean; WebBrowser: TWebBrowser);
var
  Dir, Arquivo: String;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    WebBrowser.Navigate(Arquivo);
  end;
end;

function TDmNFe_0000.StepNFeCab(FatID, FatNum, Empresa, Status: Integer;
LaAviso1, LaAviso2: TLabel): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando status da NFe para ' + IntToStr(Status));
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=:P0');
  Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 AND Empresa=:P3');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := FatID;
  Dmod.QrUpd.Params[02].AsInteger := FatNum;
  Dmod.QrUpd.Params[03].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  Result := True;
end;

function TDmNFe_0000.stepNFePedido: Integer; // 2020-11-21
begin
  Result := CO_005_StepNFePedido;
end;

function TDmNFe_0000.stepNFeDados: Integer;
begin
  Result := CO_010_StepNFeDados;
end;

function TDmNFe_0000.stepNFeGerada: Integer;
begin
  Result := CO_020_StepNFeGerada;
end;

function TDmNFe_0000.stepNFeAssinada: Integer;
begin
  Result := CO_030_StepNFeAssinada;
end;

function TDmNFe_0000.stepNFCeOffLine: Integer; // 2020-11-02
begin
  Result := CO_035_StepNFCeOffLine;
end;

function TDmNFe_0000.stepLoteRejeitado: Integer;
begin
  Result := CO_040_StepLoteRejeitado;
end;

function TDmNFe_0000.stepNFeAdedLote: Integer;
begin
  Result := CO_050_StepNFeAdedLote;
end;

function TDmNFe_0000.stepLoteEnvEnviado: Integer;
begin
  Result := CO_060_StepLoteEnvEnviado;
end;

function TDmNFe_0000.stepLoteEnvConsulta: Integer;
begin
  Result := CO_070_StepLoteEnvConsulta;
end;

function TDmNFe_0000.stepNFeCabecalhoDwnl: Integer;
begin
  // NFe baixada em Distribuicao DF-e sob  FatID 53
  // e depois Importada de XMO sob FatID 51
  Result := CO_090_StepNFeCabecalhoDwnl;
end;

{
function TDmNFe_0000.AssinarXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
  TipoDoc: Integer; var XMLAssinado_Dir: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
  //
  xmldoc, ref, nome, mensagem: String;
  //
  ArqPath, Ext, FonteXML: String;
begin
  Result := False;
  // Abrir    Arquivo := XMLGerado_Dir + XMLGerado_Arq;
  if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
  FonteXML := XMLGerado_Dir + XMLGerado_Arq;
  if not FileExists(FonteXML) then
  begin
    Geral.MB_Aviso('Arquivo não encontrado: ' + FonteXML);
    Exit;
  end;
  //
  AssignFile(fEntrada, XMLGerado_Dir + XMLGerado_Arq);
  Reset(fEntrada, 1);
  xmlDoc := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    xmlDoc := xmlDoc + buf;
  end;

  CloseFile(fEntrada);

  //

  if xmlDoc <> '' then
  begin
    ReopenEmpresa(Empresa);
    if QrFilialC e r D i g i t a l.Value <> '' then
    begin
      case TipoDoc of
        0: ref := 'infNFe';
        1: ref := 'infCanc';
        2: ref := 'infInut';
        else ref := '???';
      end;

      case TipoDoc of
        0: Ext := NFE_EXT_NFE_XML;
        1: Ext := NFE_EXT_CAN_XML;
        2: Ext := NFE_EXT_INU_XML;
        else Ext := '???';
      end;

      nome := QrFilial C e r D i g i t a l.Value;
      //
      ArqPath := SalvaXML(Ext, XMLGerado_Arq,  xmlDoc, nil, True);
      i := pAssinarXML(ArqPath, ref, nome, mensagem);
      //
      if i <> 0 then
      begin
        Geral.MensagemBox('Processo de assinatura falhou...' +
        sLineBreak + String(Mensagem)), 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end
    else
      Geral.MB_Aviso('Nome do titular do Certificado não informado...');
  end
  else
    Geral.MB_Aviso('Documento XML para assinatura não informado...');
  Result := True;
end;
}

procedure TDmNFe_0000.AvisaAmbNaoExiste(LaAviso1, LaAviso2: TLabel);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Tipo de Ambiente Inexistente!');
  Geral.MB_Aviso('Tipo de Ambiente Inexistente!');
end;

function TDmNFe_0000.AvisaFaltaDaMudancaDaAliquotaSimplesNacional(): Boolean;
begin
  Geral.MensagemBox('Para utilizar o CSOSN 101 ou 201 é necessário atualizar o ' +
  'campo"AAAAMM de validade" nas opções de NFe do cadastro da Filial selecionada!',
  'Aviso', MB_OK+MB_ICONWARNING);
  //
  Result := True;
end;

procedure TDmNFe_0000.AvisaUFNaoExiste(LaAviso1, LaAviso2: TLabel);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Sigla da UF Inexistente!');
  Geral.MB_Aviso('Sigla da UF Inexistente!');
end;

function TDmNFe_0000.AvisaUsoIndevidoICMS_doCSOSN: Boolean;
begin
  Geral.MensagemBox('Para utilizar o CSOSN 101 ou 201 o cliente ser pessoa ' +
  'jurídica e usar a mercadoria para comercialização ou industrialização!',
  'Aviso', MB_OK+MB_ICONWARNING);
  //
  Result := True;
end;

procedure TDmNFe_0000.ColoreinfCCe_nSeqEventoEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState; const infCCe_nSeqEvento: Integer);
var
  Bold: Integer;
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'infCCe_nSeqEvento') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    if infCCe_nSeqEvento <> 0 then
    begin
      Txt  := $000080FF;
      Bak  := clWindow;
      Bold := 1;
    end else
    begin
      Txt  := clBlack;
      Bak  := clWindow;
      Bold := 0;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect, Txt, Bak, Column.Alignment,
      Column.Field.DisplayText, Bold);
  end;
end;

procedure TDmNFe_0000.ColoreSitConfNFeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const SitConf: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_cSitConf') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case SitConf of
      -1:  begin Txt := clYellow ; Bak := clBlack  end;
      0:   begin Txt := clFuchsia; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clBlack  ; Bak := clRed;   end;
      3:   begin Txt := clBlack  ; Bak := clWindow;end;
      4:   begin Txt := $000080FF; Bak := clWindow end; // Laranja intenso
      else begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmNFe_0000.ColoreStatusNFeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const Status, Modelo: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'cStat')
  or (Column.FieldName = 'NO_cStat') then
  begin
    Txt := clWindow;
    Bak := clBlack;
    case Status of
       35:      begin Txt := clMaroon ; Bak := clBlue   end;
      100:
      begin
        if Modelo in ([55,65]) then
          Txt := clBlue
        else

          Txt := $000080FF; // Laranja
        Bak := clWindow
      end;
      101..102: begin Txt := clFuchsia; Bak := clWindow end;
      103..135, 137..199: begin Txt := clBlack  ; Bak := clWindow end;
      136, 468:
      begin
        if Modelo in ([55,65]) then
          Txt := $000080FF // Laranja
        else
          Txt := clRed;
        Bak := clWindow
      end;
      201..299: begin Txt := clRed    ; Bak := clWindow end;
      301..399: begin Txt := clWindow ; Bak := clRed;   end;
      401..467, 469..999: begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end
  else
  if (Column.FieldName = 'NO_cSitNFe') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case Status of
      //-1,0:begin Txt := clBlack  ; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clFuchsia; Bak := clWindow end;
      3:   begin Txt := clWindow ; Bak := clRed;   end;
      else begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmNFe_0000.ConfigurarComponenteNFe();
begin
  if FJaConfigurouACBrNFe = False then
    FJaConfigurouACBrNFe := DmkACBr_ParamsEmp.ConfigurarComponente(ACBrNFe1);
end;

procedure TDmNFe_0000.CorrigeFatParcelaNFeCabY(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
var
  QryCab, QryIts: TmySQLQuery;
  FatID, FatNum, Empresa, FatParcela, Controle, Itens: Integer;
begin
  Itens := 0;
  QryCab := TmySQLQuery.Create(Dmod);
  try
    QryIts := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryCab, Dmod.MyDB, [
      'SELECT DISTINCT FatID, FatNum, Empresa ',
      'FROM nfecaby ',
      'WHERE FatParcela=0 ',
      'ORDER BY FatNum ',
      '']);
      PB1.Position := 0;
      PB1.Max := QryCab.RecordCount;
      QryCab.First;
      while not QryCab.Eof do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        //
        FatID   := QryCab.FieldByName('FatID').AsInteger;
        FatNum  := QryCab.FieldByName('FatNum').AsInteger;
        Empresa := QryCab.FieldByName('Empresa').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryIts, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM nfecaby ',
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'ORDER BY nDup, Controle ',
        '']);
        //
        FatParcela := 0;
        QryIts.First;
        while not QryIts.Eof do
        begin
          Itens := Itens + 1;
          FatParcela := FatParcela + 1;
          Controle   := QryIts.FieldByName('Controle').AsInteger;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaby', False, [
          'FatParcela'], ['Controle'], [FatParcela], [Controle], False);
          //
          QryIts.Next;
        end;
        //
        QryCab.Next;
      end;
    finally
      QryIts.Free;
    end;
  finally
      QryCab.Free;
  end;
  Geral.MB_Aviso('Correção encerrada! ' + sLineBreak + Geral.FF0(Itens) +
  ' itens foram corrigidos!');
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TDmNFe_0000.CST_ICMS_Eh_SubsTrib(CRT, CST_A, CST_B, CSOSN: Integer): Boolean;
const
  sProcName = 'TDmNFe_0000.CST_ICMS_Eh_SubsTrib()';
var
  CST: Integer;
begin
  Result := False;
  case CRT of
    1,2: // Simples Nacional
    begin
      case CSOSN of
        101, 102, 103,
        //201, 202, 203,
        300, 400,
        //500
        900 : Result := False;
        201, 202, 203,
        500: Result := True;
        else
          Geral.MB_Erro('CSOSN ' + Geral.FF0(CSOSN) + ' não implementado em ' + sProcName)
      end;
    end;
    3: // N ormal
    begin
      case CST_B of
         0, 20, 40, 41, 50, 51, 90: Result := False;
        10, 30, 60, 70: Result := True;
        else
          Geral.MB_Erro('CST "B" ' + Geral.FF0(CST_B) + ' não implementado em ' + sProcName)
      end;
    end;
  end;
end;

{
function TDmNFe_0000.pAssinarXML(pathDocXML, tipDocXML, titCertificado: String;
  out aResultado: String): Integer;
var
  cAssinarXML : _AssinarXML;
  strResultado : String;
  intResultado : Integer;

    ///  AssinarXML : Assinatura Digital XML no padrão do Projeto NF-e
    ///
    ///
    ///    Entradas:
    ///     XMLString: string XML a ser assinada
    ///     RefUri : Referência da URI a ser assinada (Ex. infNFe
    ///     X509Cert : certificado digital a ser utilizado na assinatura digital
    ///
    ///    Retornos:
    ///
    ///      vResultado: código do resultado
    ///
    ///              0 - Assinatura realizada com sucesso
    ///              1 - Erro: Problema ao acessar o certificado digital - %exceção%
    ///              2 - Certificado digital inexistente para %nome%
    ///              3 - XML mal formado + exceção
    ///              4 - A tag de assinatura %RefUri% inexiste
    ///              5 - A tag de assinatura %RefUri% não é unica
    ///              6 - Erro Ao assinar o documento - ID deve ser string %RefUri(Atributo)%
    ///              7 - Erro: Ao assinar o documento - %exceção%
    ///
    ///
    ///      vResultadoString  : Literal da mensagem resultado

begin
  try
    cAssinarXML := CoAssinarXML.Create;
    cAssinarXML.Assinar(pathDocXML, tipDocXML, titCertificado, intResultado, strResultado);
    aResultado := strResultado;
    Result := intResultado;
  finally
    cAssinarXML := nil;
  end;
end;
}

function TDmNFe_0000.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
  nNFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + Ano + emitCNPJ + Modelo + Serie +
    StrZero(StrToInt(nNFIni),9,0) + StrZero(StrToInt(nNFFim),9,0);
  K := Length(Id);
  if K in ([41,42,43]) then
    Result := True
  else begin
    Result := False;
    Geral.MensagemBox('ID de inutilização com tamanho inválido: "' +
    Id + '". Deveria ter 41, 42 ou 43 carateres e tem ' + IntToStr(K) + '.',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

{
function TDmNFe_0000.pCancelaNFe(siglaUF, tpAmb, titCertificado, cChaveNFe,
  cProtocoloNFe, cJust: String; LaAviso1, LaAviso2: TLabel): String;
var
  cTransmissaoXML: _TransmissaoXML;
  MsgRetWS, strDados, aResultado: String;
  CaminhoXML, strVersaoDados, strCabecXML: String;
  //canXML: TextFile;
  mTexto: TStringList;

    //  5. Cancelamento de NFe : Acessa o WS de Cancelamento de NFe da SEFAZ
    //
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.

begin
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando arquivo XML temporário');
          strVersaoDados := '1.07';
          strCabecXML := '<?xml version="1.0" encoding="UTF-8" ?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>';
          strDados := '<?xml version="1.0" encoding="UTF-8"?><cancNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07"><infCanc Id="ID' + cChaveNFe + '"><tpAmb>' + tpAmb + '</tpAmb><xServ>CANCELAR</xServ><chNFe>' + cChaveNFe + '</chNFe><nProt>' + cProtocoloNFe + '</nProt><xJust>' + cJust + '</xJust></infCanc></cancNFe>';
          //
          CaminhoXML := SalvaXML(NFE_EXT_PED_CAN_XML, cChaveNFe, strDados, nil, False);
          //
          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML temporário');
            if pAssinarXML(CaminhoXML, 'infCanc', titCertificado, aResultado) > 0 then
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Não foi possível assinar a NFe');
                Geral.MB_Erro('Não foi possível assinar a NFe, Erro: ' + aResultado));
                Abort;
              end
            else
              begin
                mTexto := TStringList.Create;
                mTexto.LoadFromFile(CaminhoXML);
                strDados := mTexto.Text;
                mTexto.Free;
              end;
          except
            on E: Exception do
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Não foi possível assinar a NFe');
                Geral.MB_Erro('Não foi possível assinar a NFe, Erro: ' + E.Message);
              end;
          end;

          //rtfCabMsg.Text := strCabecXML;
          //rtfDadosMsg.Text := strDados;

          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando arquivo XML temporário ao servidor do fisco');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeCancelamento(siglaUF, StrToInt(tpAmb), titCertificado, strCabecXML, strDados, MsgRetWS) then
              begin
                Result := MsgRetWS;
              end
            else
              begin
                Result := 'Erros: ' + MsgRetWS;
              end;
          finally
            cTransmissaoXML := nil;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;
}
{
function TDmNFe_0000.pConsultaRetNFe(siglaUF, tpAmb, titCertificado, Recibo:
  String; LaAviso1, LaAviso2: TLabel): String;
var
  cTransmissaoXML: _TransmissaoXML;
  MsgRetWS, strDados, strCabecXML, strVersaoDados: String;

    //  5. Consulta Retorno de Lote : Acessa o WS de Retorno de Lote de NFe da SEFAZ
    //
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.

begin
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando texto XML');
          strVersaoDados := '1.10';
          strCabecXML := '<?xml version="1.0" encoding="UTF-8" ?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>';
          strDados := '<?xml version="1.0" encoding="UTF-8" ?><consReciNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.10"><tpAmb>' + tpAmb + '</tpAmb><nRec>' + Recibo + '</nRec></consReciNFe>';

          //rtfDadosMsg.Text := strDados;
          //rtfCabMsg.Text := strCabecXML;

          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando texto XML ao servidor do fisco');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeRetRecepcao(siglaUF, StrToInt(tpAmb), titCertificado, strCabecXML, strDados, MsgRetWS) then
              begin
                Result := MsgRetWS;
              end
            else
              begin
                Result := 'Erros: ' + MsgRetWS;
              end;
          finally
            cTransmissaoXML := nil;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;

function TDmNFe_0000.ConsultarNFe(UFServico: String; Ambiente, CodigoUF: Integer;
  Certificado: String; ChaveNFe: String; LaAviso1, LaAviso2: TLabel): String;
var
  //cTransmissaoXML: _TransmissaoXML;
  MsgRetWS, strDados: String;
  strCabecXML, strVersaoDados: String;

    //  5. Consulta de NFe : Acessa o WS de Consulta de NFe da SEFAZ
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.

begin
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando arquivo XML temporário');
          strVersaoDados := '1.07';
          strCabecXML := '<?xml version="1.0" encoding="UTF-8" ?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>';
          strDados := '<?xml version="1.0" encoding="UTF-8"?><consSitNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07"><tpAmb>' + tpAmb + '</tpAmb><xServ>CONSULTAR</xServ><chNFe>' + ChaveNFe + '</chNFe></consSitNFe>';
          //rtfDadosMsg.Text := strDados;
          //rtfCabMsg.Text := strCabecXML;

          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando arquivo XML temporário ao servidor do fisco');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeConsulta(siglaUF, StrToInt(tpAmb), titCertificado, strCabecXML, strDados, MsgRetWS) then
              begin
                Result := MsgRetWS;
              end
            else
              begin
                Result := 'Erros: ' + MsgRetWS;
              end;
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Mensagem recebida do fisco');
          finally
            cTransmissaoXML := nil;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;

function TDmNFe_0000.pEnviaLoteXML(siglaUF, tpAmb, titCertificado, MsgCabec,
  MsgDados: String; LaAviso1, LaAviso2: TLabel): String;
var
  cTransmissaoXML : _TransmissaoXML;
  //Resultado : Integer;
  MsgRetWS: String;

    //  5. EnviaLote : Envio de lotes de NF-e para o WS de recepção da SEFAZ
    //
    //  O ERP do contribuinte deve formar os lote de acordo com o leiaute.
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.

begin
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote XML ao servidor do fisco');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeRecepcao(siglaUF, StrToInt(tpAmb), titCertificado, MsgCabec, MsgDados, MsgRetWS) then
            begin
              Result := MsgRetWS;
            end
            else
            begin
              Result := 'ERRO: ' + MsgRetWS;
            end;
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Mensagem recebida do servidor do fisco');
          finally
            cTransmissaoXML := nil;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;

function TDmNFe_0000.pInutilizarNFe(siglaUF, tpAmb, titCertificado, emitCNPJ,
  iJust: String; nNFIni, nNFFim, Modelo, Serie, Ano: String;
  LaAviso1, LaAviso2: TLabel): String;
var
  cTransmissaoXML: _TransmissaoXML;
  MsgRetWS, strDados, iResultado: String;
  CaminhoXML, strVersaoDados, strCabecXML: String;
  //inuXML: TextFile;
  mTexto, mTexto2: TStringList;
  cUF, Id, NomeArq: String;


    //  5. Inutilização de NFe : Acessa o WS de Inutilização de NFe da SEFAZ
    //
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.

begin
  mTexto2 := TStringList.Create;
  cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(SiglaUF));
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando arquivo XML temporário');
          strVersaoDados := '1.07';
          MontaID_Inutilizacao(cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
            nNFFim, Id);
          strCabecXML := '<?xml version="1.0" encoding="UTF-8" ?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>';
          mTexto2.Add('<?xml version="1.0" encoding="UTF-8"?><inutNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07"><infInut Id="' + Id + '"><tpAmb>'+ tpAmb +'</tpAmb><xServ>INUTILIZAR</xServ><cUF>'+cUF+'</cUF><ano>'+ Ano +'</ano>');
          mTexto2.Add('<CNPJ>'+ emitCNPJ +'</CNPJ><mod>'+Modelo+'</mod><serie>'+ Serie +'</serie><nNFIni>'+ nNFIni +'</nNFIni><nNFFin>'+ nNFFim +'</nNFFin><xJust>'+ iJust +'</xJust></infInut></inutNFe>');
          //
          NomeArq := SiglaUF + Ano + emitCNPJ + Modelo + Serie +
            StrZero(StrToInt(nNFIni),9,0) + StrZero(StrToInt(nNFFim),9,0);
          CaminhoXML := SalvaXML(NFE_EXT_PED_INU_XML, NomeArq, mTexto2.Text, nil, False);
          //
          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML temporário');
            if pAssinarXML(CaminhoXML, 'infInut', titCertificado, iResultado) > 0 then
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Não foi possível assinar a NFe');
                Geral.MB_Erro('Não foi possível assinar a NFe, Erro: ' + iResultado);
                Abort;
              end
            else
              begin
                mTexto := TStringList.Create;
                mTexto.LoadFromFile(CaminhoXML);
                strDados := mTexto.Text;
                mTexto.Free;
              end;
          except
            on E: Exception do
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Não foi possível assinar a NFe');
                Geral.MB_Erro('Não foi possível assinar a NFe, Erro: ' + E.Message);
              end;
          end;

          //rtfCabMsg.Text := strCabecXML;
          //rtfDadosMsg.Text := strDados;

          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando arquivo XML temporário e aguardadndo resposta');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeInutilizacao(siglaUF, StrToInt(tpAmb), titCertificado, strCabecXML, strDados, MsgRetWS) then
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Mensagem recebida do servidor do fisco');
                Result := MsgRetWS;
              end
            else
              begin
                Result := 'Erros: ' + MsgRetWS;
              end;
          finally
            cTransmissaoXML := nil;
            mTexto2.Free;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;

function TDmNFe_0000.pStatusServico(siglaUF, tpAmb, titCertificado: String;
LaAviso1, LaAviso2: TLabel): String;
var
  cTransmissaoXML: _TransmissaoXML;
  MsgRetWS, strDados: String;
  cUF, strVersaoDados, strCabecXML: String;

    //  5. Status do Serviço : Status do Serviço dos WS de recepção da SEFAZ
    //
    //  O ERP do contribuinte deve formar os lote de acordo com o leiaute.
    //
    //    Entradas:
    //
    //            siglaUF: Sigla da UF do WS chamado
    //       tipoAmbiente: Código do tipo de ambiente = 1-Produção / 2-Homologação
    //    nomeCertificado: Nome do titular do certificado a ser utlizado na conexão SSL
    //           MsgCabec: Cabeçalho da Mensagem a ser enviada ao WebService
    //           MsgDados: Mensagem a ser enviada ao WbeService
    //
    //Retorno....: A Função Retorna um Valor Boolean [True/False]:
    //             MsgRetWS - em caso de algum erro, esta propriedade vai
    //                        receber um texto contendo o erro para ser
    //                        analisado, em caso de sucesso essa propriedade
    //                        vai receber a Resposta do WebService.


begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando texto XML');
  if siglaUF <> '' then
    begin
      if tpAmb <> '' then
        begin
          cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(SiglaUF));
          strVersaoDados := '1.07';
          strCabecXML := '<?xml version="1.0" encoding="UTF-8" ?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>';
          strDados := '<?xml version="1.0" encoding="UTF-8" ?><consStatServ xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07"><tpAmb>'+ tpAmb +'</tpAmb><cUF>' + cUF + '</cUF><xServ>STATUS</xServ></consStatServ>';

          //rtfDadosMsg.Text := strDados;
          //rtfCabMsg.Text := strCabecXML;

          try
            MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Solicitando status do serviço e aguardadndo resposta');
            cTransmissaoXML := CoTransmissaoXML.Create;
            if cTransmissaoXML.NfeStatusServico(siglaUF, StrToInt(tpAmb), titCertificado, strCabecXML, strDados, MsgRetWS) then
            begin
              Result := MsgRetWS;
            end
            else
              begin
                Result := 'Erros: ' + MsgRetWS;
              end;
          finally
            cTransmissaoXML := nil;
          end;
        end
      else AvisaAmbNaoExiste(Label1);
    end
  else AvisaUFNaoExiste(Label1);
end;
}

function TDmNFe_0000.PodeAlterarNFe(Status: Integer): Boolean;
begin
  Result :=
     (Status < 50)
  or ((Status > 200) and (Status < 300))
  or (Status > 400);
end;

function TDmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
begin
  Result :=
  'SELECT med.Sigla NO_UNIDADE, smva.IDCtrl,                         ' + sLineBreak +
  'smva.Empresa, smva.Preco, smva.GraGruX CU_PRODUTO,                ' + sLineBreak +
  'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,               ' + sLineBreak +
  'SUM(smva.Total) Total, SUM(smva.Qtde) Qtde, smva.CFOP,            ' + sLineBreak +
  'smva.CFOP_Contrib, smva.CFOP_MesmaUF, smva.CFOP_Proprio,          ' + sLineBreak +
  'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                           ' + sLineBreak +
  'gg1.CST_A, gg1.CST_B,                                             ' + sLineBreak +
  'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,                ' + sLineBreak +
  'gg1.NCM, gg1.IPI_CST, gg1.IPI_cEnq,                               ' + sLineBreak +
  'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,                ' + sLineBreak +
  'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,                ' + sLineBreak +
  'gg1.InfAdProd, smva.MedidaC, smva.MedidaL, smva.MedidaA,          ' + sLineBreak +
  'smva.MedidaE, mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4, ' + sLineBreak +
  'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4, gta.PrintTam,     ' + sLineBreak +
  'gg1.PIS_CST, gg1.PIS_AlqV, gg1.PISST_AlqV, gg1.COFINS_CST,        ' + sLineBreak +
  'gg1.COFINS_AlqP, gg1.COFINS_AlqV, gg1.COFINSST_AlqP,              ' + sLineBreak +
  'gg1.COFINSST_AlqV, gg1.ICMS_modBC, gg1.ICMS_modBCST,              ' + sLineBreak +
  'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST, gg1.ICMS_pMVAST,              ' + sLineBreak +
  'gg1.ICMS_pICMSST,  gg1.IPI_vUnid, gg1.IPI_TpTrib,                 ' + sLineBreak +
  'gg1.IPI_pIPI, gg1.ICMS_Pauta, gg1.ICMS_MaxTab,                    ' + sLineBreak +
  'gg1.PIS_pRedBC, gg1.PISST_pRedBCST,                               ' + sLineBreak +
  'gg1.COFINS_pRedBC, gg1.COFINSST_pRedBCST,                         ' + sLineBreak +
  'gg1.cGTIN_EAN, gg1.EX_TIPI, gg1.PISST_AlqP, gg1.PIS_AlqP,         ' + sLineBreak +
  'smva.Servico, gcc.PrintCor, smva.prod_indTot,                     ' + sLineBreak +
  'smva.CSOSN CSOSN_Manual, gg1.CSOSN CSOSN_GG1, smva.pCredSN,       ' + sLineBreak + // 2011-02-19  alterado 2021-02-27
  'smva.iTotTrib, smva.vTotTrib,                                     ' + sLineBreak + // 2013-05-10
  'smva.pTotTrib, gg1.ICMS_pDif, gg1.ICMS_pICMSDeson,                ' + sLineBreak + // 2014-10-21
  'gg1.ICMS_motDesICMS,                                              ' + sLineBreak + // 2014-10-21
  'gg1.prod_CEST, gg1.prod_indEscala, smva.ID,                       ' + sLineBreak + // 2015-10-14
  'smva.prod_vFrete, smva.prod_vSeg,                                 ' + sLineBreak + // 2020-10-31
  'smva.prod_vDesc, smva.prod_vOutro,                                ' + sLineBreak + // 2020-10-31
  'gg1.UsaSubsTrib,                                                  ' + sLineBreak + // 2021-02-27
  'gg1.prod_cBarra prod_cBarraGG1,                                   ' + sLineBreak + // 2021-03-11
  'ggx.prod_cBarra prod_cBarraGGX,                                   ' + sLineBreak + // 2021-03-11
  'smva.obsCont_xCampo, smva.obsCont_xTexto,                         ' + sLineBreak + // 2023-06-14
  'smva.obsFisco_xCampo, smva.obsFisco_xTexto                        ' + sLineBreak + // 2023-06-14
  'FROM stqmovvala smva                                              ' + sLineBreak +
  'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX                ' + sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1                 ' + sLineBreak +
  'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC               ' + sLineBreak +
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad               ' + sLineBreak +
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI               ' + sLineBreak +
  'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo                  ' + sLineBreak +
  'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem                ' + sLineBreak +
  'LEFT JOIN gragrux ggx1 ON ggx1.Controle=smva.RefProd              ' + sLineBreak +
  'LEFT JOIN gragru1 gg11 ON gg11.Nivel1=ggx1.GraGru1                ' + sLineBreak +
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                   ' + sLineBreak +
  'WHERE smva.Tipo = ' + FormatFloat('0', Tipo) +                    sLineBreak +
  'AND smva.OriCodi=' + FormatFloat('0', OriCodi) +                  sLineBreak +
  'AND smva.Empresa=' + FormatFloat('0', Empresa) +                  sLineBreak +
  'GROUP BY smva.Preco, smva.GraGruX  ';
end;

function TDmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
begin
  Result :=
  'SELECT SUM(smva.Total) Total, SUM(smva.Qtde) Qtde,              ' + sLineBreak +
  'COUNT(DISTINCT OriCnta) + 0.000 Volumes                         ' + sLineBreak +
  'FROM stqmovvala smva                                            ' + sLineBreak +
  'WHERE smva.Tipo=' + FormatFloat('0', Tipo) +                        sLineBreak +
  'AND smva.OriCodi='+ FormatFloat('0', OriCodi) +                     sLineBreak +
  'AND smva.Empresa='+ FormatFloat('0', Empresa);
end;

function TDmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
var
{$IfDef DEFINE_VARLCT}
  Filial: Integer;
{$ENdIf}
  TabLctA: String;
begin
{$IfDef DEFINE_VARLCT}
  Filial  := DmodG.ObtemFilialDeEntidade(Empresa);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
{$Else}
  TabLctA := LAN_CTOS;
{$ENdIf}
  //
  Result :=
  'SELECT FatID, FatNum, Vencimento, Credito Valor,                ' + sLineBreak +
  'FatParcela, Duplicata, Controle, Sub                            ' + sLineBreak +
  'FROM ' + TabLctA + '                                            ' + sLineBreak +
  'WHERE FatID = ' + FormatFloat('0', Tipo) +                       sLineBreak +
  'AND FatNum=' + FormatFloat('0', OriCodi) +                       sLineBreak +
  'AND CliInt=' + FormatFloat('0', Empresa);
end;

function TDmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(Tipo, OriCodi, Empresa: Integer): String;
var
{$IfDef DEFINE_VARLCT}
  Filial: Integer;
{$ENdIf}
  TabLctA: String;
begin
{$IfDef DEFINE_VARLCT}
  Filial  := DmodG.ObtemFilialDeEntidade(Empresa);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
{$Else}
  TabLctA := LAN_CTOS;
{$ENdIf}
  //
  Result :=
  'SELECT FatID, FatNum, SUM(Credito) Valor                        ' + sLineBreak +
  'FROM ' + TabLctA + '                                            ' + sLineBreak +
  'WHERE FatID = ' + FormatFloat('0', Tipo) +                       sLineBreak +
  'AND FatNum=' + FormatFloat('0', OriCodi) +                       sLineBreak +
  'AND CliInt=' + FormatFloat('0', Empresa) +                       sLineBreak +
  'GROUP BY CliInt';
end;

function TDmNFe_0000.SQL_VOLUMES_Prod1_Padrao(Tipo, OriCodi,
  Empresa: Integer): String;
begin
  Result :=
  'SELECT Quantidade qVol, Especie esp, Marca marca,               ' + sLineBreak +
  'Numero nVol, kgLiqui pesoL, kgBruto pesoB                       ' + sLineBreak +
  'FROM stqmovnfsa                                                 ' + sLineBreak +
  'WHERE Tipo=' + FormatFloat('0', Tipo) +                          sLineBreak +
  'AND OriCodi='+ FormatFloat('0', OriCodi) +                       sLineBreak +
  'AND Empresa='+ FormatFloat('0', Empresa);
end;

function TDmNFe_0000.ReopenQrA(FatID, FatNum, Empresa: Integer): Boolean;
begin
  QrA.Close;
  QrA.Database := Dmod.MyDB; // 2020-08-31
  QrA.Params[00].AsInteger := FatID;
  QrA.Params[01].AsInteger := FatNum;
  QrA.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  Result := QrA.RecordCount > 0;
end;

function TDmNFe_0000.ReopenQrI(FatID, FatNum, Empresa: Integer): Boolean;
begin
  QrI.Close;
  QrI.Database := Dmod.MyDB; // 2020-08-31
  QrI.Params[00].AsInteger := FatID;
  QrI.Params[01].AsInteger := FatNum;
  QrI.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  Result := QrI.RecordCount > 0;
end;

function TDmNFe_0000.ReopenQrN_QrS(FatID, FatNum, Empresa, nItem: Integer): Boolean;
begin
  QrN.Close;
  QrN.Database := Dmod.MyDB; // 2020-08-31
  QrN.Params[00].AsInteger := FatID;
  QrN.Params[01].AsInteger := FatNum;
  QrN.Params[02].AsInteger := Empresa;
  QrN.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  QrO.Close;
  QrO.Database := Dmod.MyDB; // 2020-08-31
  QrO.Params[00].AsInteger := FatID;
  QrO.Params[01].AsInteger := FatNum;
  QrO.Params[02].AsInteger := Empresa;
  QrO.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  QrQ.Close;
  QrQ.Database := Dmod.MyDB; // 2020-08-31
  QrQ.Params[00].AsInteger := FatID;
  QrQ.Params[01].AsInteger := FatNum;
  QrQ.Params[02].AsInteger := Empresa;
  QrQ.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrQ, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  QrS.Close;
  QrS.Database := Dmod.MyDB; // 2020-08-31
  QrS.Params[00].AsInteger := FatID;
  QrS.Params[01].AsInteger := FatNum;
  QrS.Params[02].AsInteger := Empresa;
  QrS.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrS, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  Result := QrN.RecordCount = 0;
end;

{ Excluido no território do couro
    DELETE FROM nfecaba WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabb WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabf WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabg WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabxreb WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabxvol WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecabxlac WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfecaby WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsi WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsidi WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsidia WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsp WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsm WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsn WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsna WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitso WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsq WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsr WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitss WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitst WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsu WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
    DELETE FROM nfeitsv WHERE FatID=1 AND FatNum=3451 AND Empresa=-11;
}

end.
