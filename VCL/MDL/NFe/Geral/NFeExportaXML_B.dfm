object FmNFeExportaXML_B: TFmNFeExportaXML_B
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Monta XML de NF-e Autorizada'
  ClientHeight = 884
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 446
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Monta XML de NF-e Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 446
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Monta XML de NF-e Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 446
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Monta XML de NF-e Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 684
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 684
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 684
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnA: TPanel
          Left = 2
          Top = 18
          Width = 961
          Height = 535
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 960
          object Label4: TLabel
            Left = 10
            Top = 5
            Width = 55
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Emitente:'
          end
          object Label14: TLabel
            Left = 10
            Top = 54
            Width = 75
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Destinat'#225'rio:'
          end
          object Label1: TLabel
            Left = 10
            Top = 276
            Width = 282
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pasta onde est'#227'o as NF-e assinadas (-nfe.xml):'
          end
          object SbRaiz: TSpeedButton
            Left = 926
            Top = 295
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbRaizClick
          end
          object Label5: TLabel
            Left = 10
            Top = 473
            Width = 299
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nome da pasta (e arquivo zip) a ser(em) criado(s):'
          end
          object Label6: TLabel
            Left = 10
            Top = 325
            Width = 323
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pasta onde est'#227'o os lotes processados (-pro-rec.xml):'
          end
          object SpeedButton1: TSpeedButton
            Left = 926
            Top = 345
            Width = 25
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label7: TLabel
            Left = 10
            Top = 423
            Width = 68
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pasta Raiz:'
          end
          object Label2: TLabel
            Left = 798
            Top = 167
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Visible = False
          end
          object Label3: TLabel
            Left = 798
            Top = 226
            Width = 59
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            Visible = False
          end
          object Label9: TLabel
            Left = 10
            Top = 374
            Width = 294
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pasta onde est'#227'o as NF-e canceladas (-can.xml):'
          end
          object SpeedButton2: TSpeedButton
            Left = 926
            Top = 394
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object SpeedButton3: TSpeedButton
            Left = 926
            Top = 443
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton3Click
          end
          object EdEmpresa: TdmkEditCB
            Left = 10
            Top = 25
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 79
            Top = 25
            Width = 709
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdDest: TdmkEditCB
            Left = 10
            Top = 74
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDest
            IgnoraDBLookupComboBox = False
          end
          object CBDest: TdmkDBLookupComboBox
            Left = 79
            Top = 74
            Width = 709
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsDest
            TabOrder = 3
            dmkEditCB = EdDest
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
          end
          object GroupBox2: TGroupBox
            Left = 793
            Top = 18
            Width = 158
            Height = 67
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Per'#237'odo: '
            TabOrder = 4
            object LaMes: TLabel
              Left = 15
              Top = 37
              Width = 29
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'M'#234's:'
            end
            object EdMes: TdmkEdit
              Left = 59
              Top = 31
              Width = 67
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taCenter
              TabOrder = 0
              FormatType = dmktfMesAno
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              QryCampo = 'Mez'
              UpdType = utYes
              Obrigatorio = True
              PermiteNulo = False
              ValueVariant = Null
              ValWarn = False
            end
          end
          object CGStatus: TdmkCheckGroup
            Left = 10
            Top = 103
            Width = 778
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Status da NFe: '
            Columns = 4
            Enabled = False
            ItemIndex = 2
            Items.Strings = (
              'Autorizadas'
              'Canceladas'
              'Inutilizadas'
              'Denegadas')
            TabOrder = 5
            UpdType = utYes
            Value = 4
            OldValor = 0
          end
          object GroupBox3: TGroupBox
            Left = 10
            Top = 167
            Width = 779
            Height = 105
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Outros filtros: '
            TabOrder = 7
            object CkProt: TCheckBox
              Left = 15
              Top = 25
              Width = 751
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Incluir protocolo de autoriza'#231#227'o / cancelamento da NFe.'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 0
            end
            object CkUmArqPorNFe: TCheckBox
              Left = 15
              Top = 49
              Width = 751
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Gerar um arquivo para cada NFe / inutiliza'#231#227'o.'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 1
            end
            object CkZipar: TCheckBox
              Left = 15
              Top = 74
              Width = 751
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Zipar a pasta gerada.'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
          end
          object EdDirNFeAss: TEdit
            Left = 10
            Top = 295
            Width = 912
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 8
            Text = 'C:\Dermatek\Nfe\{NOME_EMPRESA}\NfeAss'
          end
          object EdArq: TEdit
            Left = 10
            Top = 492
            Width = 941
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 12
          end
          object EdDirProRec: TEdit
            Left = 10
            Top = 345
            Width = 912
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 9
            Text = 'C:\Dermatek\Nfe\{NOME_EMPRESA}\ProRec'
          end
          object EdRaiz: TEdit
            Left = 10
            Top = 443
            Width = 912
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 11
            Text = 'C:\Dermatek\ExportaXML\Montado'
          end
          object TPIni: TdmkEditDateTimePicker
            Left = 798
            Top = 192
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40182.966222291670000000
            Time = 40182.966222291670000000
            TabOrder = 13
            Visible = False
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPFim: TdmkEditDateTimePicker
            Left = 798
            Top = 246
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40182.966222291670000000
            Time = 40182.966222291670000000
            TabOrder = 14
            Visible = False
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object GroupBox4: TGroupBox
            Left = 793
            Top = 97
            Width = 158
            Height = 67
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Vers'#227'o: '
            TabOrder = 6
            object Label8: TLabel
              Left = 15
              Top = 37
              Width = 33
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'NF-e:'
            end
            object EdVersao: TdmkEdit
              Left = 59
              Top = 30
              Width = 67
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '3,10'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 3.100000000000000000
              ValWarn = False
            end
          end
          object EdDirNFeCan: TEdit
            Left = 10
            Top = 394
            Width = 912
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 10
            Text = 'C:\Dermatek\Nfe\{NOME_EMPRESA}\Can'
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 553
          Width = 961
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = 'Panel5'
          TabOrder = 1
          ExplicitWidth = 960
          object ListBox1: TListBox
            Left = 1
            Top = 1
            Width = 958
            Height = 41
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ItemHeight = 13
            TabOrder = 0
          end
          object ListBox2: TListBox
            Left = 1
            Top = 1
            Width = 959
            Height = 41
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
          end
          object ListBox3: TListBox
            Left = 1
            Top = 1
            Width = 959
            Height = 41
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ItemHeight = 13
            TabOrder = 2
          end
        end
        object Memo1: TMemo
          Left = 2
          Top = 596
          Width = 961
          Height = 86
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ExplicitWidth = 960
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 743
    Width = 965
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 960
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 798
    Width = 965
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 785
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 783
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDest: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'Tipo, IF(Tipo=0,CNPJ, CPF) CNPJ_CPF'
      'FROM Entidades'
      'WHERE ('
      '     (Tipo=0 AND CNPJ <> "")'
      '     OR'
      '     (Tipo=1 AND CPF <> "")'
      ')'
      'ORDER BY NO_ENT'
      '')
    Left = 504
    Top = 12
    object QrDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrDestTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDestCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsDest: TDataSource
    DataSet = QrDest
    Left = 532
    Top = 12
  end
end
