object FmNFeNewVer: TFmNFeNewVer
  Left = 339
  Top = 185
  Caption = 'NFe-LAYOU-003 :: Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 455
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 812
        Height = 455
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = 'Grupos'
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 427
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 804
              Height = 427
              Align = alClient
              DataSource = DsNFeLayC
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Grupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 731
                  Visible = True
                end>
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Itens'
          ImageIndex = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 427
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 804
              Height = 50
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 8
                Width = 72
                Height = 13
                Caption = 'Vers'#227'o Origem:'
              end
              object Label2: TLabel
                Left = 92
                Top = 8
                Width = 73
                Height = 13
                Caption = 'Vers'#227'o destino:'
              end
              object Label3: TLabel
                Left = 476
                Top = 4
                Width = 58
                Height = 13
                Caption = 'Filtrar grupo:'
              end
              object Label4: TLabel
                Left = 540
                Top = 4
                Width = 79
                Height = 13
                Caption = 'Filtrar SubGrupo:'
              end
              object EdVerOrig: TdmkEdit
                Left = 8
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '4,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 4.000000000000000000
                ValWarn = False
              end
              object EdVerDest: TdmkEdit
                Left = 92
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '4,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 4.000000000000000000
                ValWarn = False
              end
              object BtCopiar: TBitBtn
                Tag = 14
                Left = 348
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Copiar'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtCopiarClick
              end
              object CkNaoSobrepor: TdmkCheckBox
                Left = 184
                Top = 24
                Width = 157
                Height = 17
                Caption = 'N'#227'o sobrepor existentes.'
                Checked = True
                State = cbChecked
                TabOrder = 3
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object BtReopen: TBitBtn
                Tag = 14
                Left = 644
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Reabrir'
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtReopenClick
              end
              object EdPsqGrupo: TdmkEdit
                Left = 476
                Top = 20
                Width = 61
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'A'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'A'
                ValWarn = False
              end
              object EdPsqSubGrupo: TdmkEdit
                Left = 540
                Top = 20
                Width = 101
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 50
              Width = 804
              Height = 377
              Align = alClient
              DataSource = DsDest
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Grupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SubGrupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ID'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Campo'
                  Width = 69
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Width = 538
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Elemento'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pai'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tipo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorMin'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorMax'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamMin'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamMax'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamVar'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DeciCasas'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observacao'
                  Width = 23
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LeftZeros'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'InfoVazio'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FormatStr'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Versao'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CodigoN'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CodigoX'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtIniHomo'
                  Title.Caption = 'Dt. ini Homo'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtIniProd'
                  Title.Caption = 'Dt. ini. Prod'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AlterWeb'
                  Width = 49
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 27
                  Visible = True
                end>
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Testar '
          ImageIndex = 2
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 427
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 804
              Height = 33
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label5: TLabel
                Left = 4
                Top = 4
                Width = 42
                Height = 13
                Caption = 'Arquivo: '
              end
              object SbFile: TSpeedButton
                Left = 748
                Top = 4
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SbFileClick
              end
              object SbLoad: TSpeedButton
                Left = 772
                Top = 4
                Width = 23
                Height = 22
                Caption = '>'
                OnClick = SbLoadClick
              end
              object EdFile: TdmkEdit
                Left = 52
                Top = 4
                Width = 693
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 
                  'C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\MDL\NFe\v_0' +
                  '4_00\Geral\NFeGeraXML_0400.pas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 
                  'C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\MDL\NFe\v_0' +
                  '4_00\Geral\NFeGeraXML_0400.pas'
                ValWarn = False
              end
            end
            object PageControl2: TPageControl
              Left = 0
              Top = 33
              Width = 804
              Height = 394
              ActivePage = TabSheet4
              Align = alClient
              TabOrder = 1
              object TabSheet4: TTabSheet
                Caption = ' Conteudo do arquivo '
                object MeFile: TMemo
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 366
                  Align = alClient
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Lucida Console'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
              end
              object TabSheet5: TTabSheet
                Caption = ' Itens n'#227'o enontrados '
                ImageIndex = 1
                object PageControl3: TPageControl
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 366
                  ActivePage = TabSheet7
                  Align = alClient
                  TabOrder = 0
                  object TabSheet7: TTabSheet
                    Caption = ' Elementos '
                    object MeNotFound: TMemo
                      Left = 0
                      Top = 0
                      Width = 788
                      Height = 338
                      Align = alClient
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Lucida Console'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                    end
                  end
                  object TabSheet8: TTabSheet
                    Caption = ' Grupos '
                    ImageIndex = 1
                    object MeNaoElmt: TMemo
                      Left = 0
                      Top = 0
                      Width = 788
                      Height = 338
                      Align = alClient
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Lucida Console'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      ExplicitWidth = 796
                      ExplicitHeight = 366
                    end
                  end
                end
              end
              object TabSheet6: TTabSheet
                Caption = ' Itens arquivo '
                ImageIndex = 2
                object Grade: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 366
                  Align = alClient
                  DefaultColWidth = 40
                  DefaultRowHeight = 21
                  TabOrder = 0
                  ExplicitLeft = 172
                  ExplicitTop = 188
                  ExplicitWidth = 320
                  ExplicitHeight = 120
                  ColWidths = (
                    40
                    64
                    69
                    90
                    40)
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 503
    Width = 812
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 23
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 440
        Height = 17
        Caption = 
          'Na aba "Grupos" d'#234' um duplo clique para abrir os itens cadastrad' +
          'os'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 440
        Height = 17
        Caption = 
          'Na aba "Grupos" d'#234' um duplo clique para abrir os itens cadastrad' +
          'os'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB: TProgressBar
      Left = 2
      Top = 38
      Width = 808
      Height = 16
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 130
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 250
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtDuplica: TBitBtn
        Left = 370
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Duplica'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtDuplicaClick
      end
      object BtTestar: TBitBtn
        Left = 490
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Testar'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtTestarClick
      end
    end
  end
  object QrOrigI: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfelayi'
      'WHERE Versao=2.0')
    Left = 36
    Top = 172
    object QrOrigICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 11
    end
    object QrOrigIID: TWideStringField
      FieldName = 'ID'
      Size = 7
    end
    object QrOrigICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrOrigIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrOrigIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrOrigIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrOrigITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrOrigIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrOrigIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrOrigITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrOrigITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrOrigITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrOrigIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrOrigIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOrigIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOrigIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOrigILeftZeros: TSmallintField
      FieldName = 'LeftZeros'
    end
    object QrOrigIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrOrigIFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrOrigIVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrOrigICodigoN: TFloatField
      FieldName = 'CodigoN'
    end
    object QrOrigICodigoX: TWideStringField
      FieldName = 'CodigoX'
      Size = 10
    end
    object QrOrigISubGrupo: TWideStringField
      FieldName = 'SubGrupo'
    end
    object QrOrigIGrupo: TWideStringField
      FieldName = 'Grupo'
      Size = 3
    end
  end
  object DsDest: TDataSource
    DataSet = TbDest
    Left = 36
    Top = 264
  end
  object TbDest: TMySQLTable
    Filter = 'Versao=2.00'
    IndexName = 'UNIQUE1'
    TableName = 'nfelayi'
    Left = 36
    Top = 220
    object TbDestSubGrupo: TWideStringField
      FieldName = 'SubGrupo'
    end
    object TbDestCodigo: TWideStringField
      FieldName = 'Codigo'
      Required = True
      Size = 11
    end
    object TbDestID: TWideStringField
      FieldName = 'ID'
      Required = True
      Size = 7
    end
    object TbDestCampo: TWideStringField
      FieldName = 'Campo'
      Required = True
      Size = 30
    end
    object TbDestDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbDestElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object TbDestPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 10
    end
    object TbDestTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object TbDestOcorMin: TSmallintField
      FieldName = 'OcorMin'
      Required = True
    end
    object TbDestOcorMax: TIntegerField
      FieldName = 'OcorMax'
      Required = True
    end
    object TbDestTamMin: TSmallintField
      FieldName = 'TamMin'
      Required = True
    end
    object TbDestTamMax: TIntegerField
      FieldName = 'TamMax'
      Required = True
    end
    object TbDestTamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object TbDestDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
      Required = True
    end
    object TbDestObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object TbDestAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbDestAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbDestLeftZeros: TSmallintField
      FieldName = 'LeftZeros'
      Required = True
    end
    object TbDestInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
      Required = True
    end
    object TbDestFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object TbDestVersao: TFloatField
      FieldName = 'Versao'
      Required = True
    end
    object TbDestCodigoN: TFloatField
      FieldName = 'CodigoN'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object TbDestCodigoX: TWideStringField
      FieldName = 'CodigoX'
      Required = True
      Size = 10
    end
    object TbDestGrupo: TWideStringField
      DisplayWidth = 4
      FieldName = 'Grupo'
      Size = 4
    end
    object TbDestDtIniProd: TDateField
      FieldName = 'DtIniProd'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbDestDtIniHomo: TDateField
      FieldName = 'DtIniHomo'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object DsNFeLayC: TDataSource
    DataSet = TbNFeLayC
    Left = 108
    Top = 224
  end
  object TbNFeLayC: TMySQLTable
    TableName = 'nfelayc'
    Left = 108
    Top = 176
    object TbNFeLayCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object TbNFeLayCGrupo: TWideStringField
      DisplayWidth = 4
      FieldName = 'Grupo'
      Size = 4
    end
  end
  object PMTestar: TPopupMenu
    Left = 532
    Top = 524
    object Itensdoarquivonatabela1: TMenuItem
      Caption = 'Itens do arquivo na tabela'
      OnClick = Itensdoarquivonatabela1Click
    end
    object Itensdatabelanoarquivo1: TMenuItem
      Caption = 'Itens da tabela no arquivo'
      OnClick = Itensdatabelanoarquivo1Click
    end
  end
  object QrPsq: TMySQLQuery
    Left = 296
    Top = 316
  end
end
