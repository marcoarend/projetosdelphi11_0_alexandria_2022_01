object FmNFeCertificados: TFmNFeCertificados
  Left = 339
  Top = 185
  Caption = 'NFE-CERTI-001 :: Certificados Digitais'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Certificados Digitais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PCStore: TPageControl
      Left = 1
      Top = 42
      Width = 782
      Height = 355
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LBPessoal: TListBox
          Left = 0
          Top = 0
          Width = 774
          Height = 327
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'TabSheet2'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LBOutras: TListBox
          Left = 0
          Top = 0
          Width = 774
          Height = 327
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'TabSheet3'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LBIntermediarias: TListBox
          Left = 0
          Top = 0
          Width = 774
          Height = 327
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'TabSheet4'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LBRaiz: TListBox
          Left = 0
          Top = 0
          Width = 774
          Height = 327
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 41
      Align = alTop
      TabOrder = 1
      ExplicitLeft = 68
      ExplicitTop = 112
      ExplicitWidth = 185
      object CBStores: TComboBox
        Left = 8
        Top = 8
        Width = 765
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'CBStores'
        OnChange = CBStoresChange
        Items.Strings = (
          'Pessoal'
          'Outras Pessoas'
          'Autoridades de Certifica'#231#227'o Intermedi'#225'rias'
          'Autoridades de Certifica'#231#227'o Raiz Confi'#225'veis')
      end
    end
  end
end
