unit CreateNFe;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttNFeArqs, ntrttNFePR01, ntrttNFeCP01, ntrttNFeCR01, ntrttNFeDP01,
    ntrttNFeDR01, ntrttNFeXR01, ntrttNFeYR01, ntrttNFeZR01,
    ntrttNFeA, ntrttNFeB, ntrttNFeB12a, ntrttNFeC, ntrttNFeD, ntrttNFeE,
    ntrttNFeF, ntrttNFeG, ntrttNFeGA, ntrttNFeI, ntrttNFeIDi, ntrttNFeIDiA,
    ntrttNFeM, ntrttNFeN, ntrttNFeO, ntrttNFeP, ntrttNFeQ, ntrttNFeR, ntrttNFeS,
    ntrttNFeT, ntrttNFeU, ntrttNFeV, ntrttNFeW02, ntrttNFeW17, ntrttNFeW23,
    ntrttNFeX22, ntrttNFeX01, ntrttNFeX26, ntrttNFeX33, ntrttNFeY01,
    ntrttNFeY07, ntrttNFeZ01, ntrttNFeZ04, ntrttNFeZ07, ntrttNFeZ10,
    ntrttNFeZA01, ntrttNFeZB01, ntrttNFeChaves, ntrttNFeCabY);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TCreateNFe = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttNFeChaves(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeCabY(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeArqs(Qry: TmySQLQuery);
    procedure Cria_ntrttNFePR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeCP01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeCR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeDP01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeDR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeXR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeYR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZR01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeA(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeB(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeB12a(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeC(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeD(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeE(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeF(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeG(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeGA(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeI(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeIDi(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeIDiA(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeM(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeN(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeO(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeP(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeQ(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeR(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeS(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeT(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeU(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeV(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeW02(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeW17(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeW23(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeX22(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeX01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeX26(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeX33(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeY01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeY07(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZ01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZ04(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZ07(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZ10(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZA01(Qry: TmySQLQuery);
    procedure Cria_ntrttNFeZB01(Qry: TmySQLQuery);
  public
    { Public declarations }
    function NomeTabela(Tabela: TNomeTabRecriaTempTable): String;
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateNFe: TCreateNFe;

implementation

uses UnMyObjects, Module;

procedure TCreateNFe.Cria_ntrttNFeA(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  IDCtrl               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  LoteEnv              bigint(15)   NOT NULL                      ,');
  Qry.SQL.Add('  versao               double(4,2)  NOT NULL                      ,');
  Qry.SQL.Add('  Id                   varchar(44)  NOT NULL                      ,');

  Qry.SQL.Add('  ErrEmit              tinyint(1)   NOT NULL DEFAULT -1           ,');
  Qry.SQL.Add('  ErrDest              tinyint(1)   NOT NULL DEFAULT -1           ,');
  Qry.SQL.Add('  ErrTrsp              tinyint(1)   NOT NULL DEFAULT -1           ,');
  Qry.SQL.Add('  ErrProd              tinyint(1)   NOT NULL DEFAULT -1           ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeArqs(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  Versao               double(15,1) NOT NULL                      ,'); // 1.0 conforme http://www.w3.org/TR/xml/#NT-VersionNum
  Qry.SQL.Add('  Codificacao          varchar(30)  NOT NULL                      ,'); // UTF-8
  Qry.SQL.Add('  LocalName            varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  Prefix               varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  NodeName             varchar(100) NOT NULL                      ,'); // Elemento Raiz
  Qry.SQL.Add('  NamespaceURI         varchar(255) NOT NULL                      ,'); // http://www.portalfiscal.inf.br/nfe
  Qry.SQL.Add('  ArqDir               varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  ArqName              varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  xTipoDoc             varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  Assinado             tinyint(1)   NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeB(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  ide_cUF              tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xUF              char(2)      NOT NULL DEFAULT "??"         ,');
  Qry.SQL.Add('  ide_cNF              int(9)       NOT NULL                      ,');
  Qry.SQL.Add('  ide_natOp            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_indPag           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xIndPag          varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_mod              tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_serie            int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  ide_nNF              int(9)       NOT NULL                      ,');
  Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
  Qry.SQL.Add('  ide_dSaiEnt          date         NOT NULL                      ,');
  Qry.SQL.Add('  ide_hSaiEnt          time         NOT NULL                      ,');
  Qry.SQL.Add('  ide_tpNF             tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xTpNF            varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_cMunFG           int(7)       NOT NULL                      ,');
  Qry.SQL.Add('  ide_xMunFG           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_tpImp            tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xTpImp           varchar(40)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_tpEmis           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xTpEmis          varchar(100) NOT NULL                      ,');
  Qry.SQL.Add('  ide_cDV              tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_tpAmb            tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xAmb             varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  ide_finNFe           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xFInNFe          varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_procEmi          tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ide_xProcEmi         varchar(100) NOT NULL                      ,');
  Qry.SQL.Add('  ide_verProc          varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  ide_dhCont           datetime     NOT NULL                      ,');
  Qry.SQL.Add('  ide_xJust            varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  ide_hEmi             time         NOT NULL  DEFAULT "00:00:00"  ,');
  Qry.SQL.Add('  ide_dhEmiTZD         double(15,10) NOT NULL DEFAULT "0.0000000000",');
  Qry.SQL.Add('  ide_dhSaiEntTZD      double(15,10) NOT NULL DEFAULT "0.0000000000",');
  Qry.SQL.Add('  ide_idDest           tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  ide_indFinal         tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  ide_indPres          tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  ide_indIntermed      tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeB12a(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  refNFe               varchar(44)  NOT NULL                      ,');
  Qry.SQL.Add('  refNF_cUF            tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  refNF_xUF            char(2)      NOT NULL DEFAULT "??"         ,');
  Qry.SQL.Add('  refNF_AAMM           int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  refNF_CNPJ           varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  refNF_mod            tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  refNF_serie          int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  refNF_nNF            int(9)       NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_cUF           tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_xUF           char(2)      NOT NULL DEFAULT "??"         ,');
  Qry.SQL.Add('  refNFP_AAMM          smallint(4)  NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_CNPJ          varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_CPF           varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_IE            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_mod           tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_serie         smallint(3)  NOT NULL                      ,');
  Qry.SQL.Add('  refNFP_nNF           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  refCTe               varchar(44)  NOT NULL                      ,');
  Qry.SQL.Add('  refECF_mod           char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  refECF_nECF          smallint(3)  NOT NULL                      ,');
  Qry.SQL.Add('  refECF_nCOO          mediumint(6) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeC(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  emit_CNPJ            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_CPF             varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_xNome           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_xFant           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_xLgr            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_nro             varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_xCpl            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_xBairro         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_cMun            int(7)       NOT NULL                      ,');
  Qry.SQL.Add('  emit_xMun            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_UF              char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  emit_CEP             int(8)       NOT NULL                      ,');
  Qry.SQL.Add('  emit_cPais           int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  emit_xPais           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_fone            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_IE              varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_IEST            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_IM              varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  emit_CNAE            varchar(7)   NOT NULL                      ,');
  Qry.SQL.Add('  emit_CRT             tinyint(1)   NOT NULL                      ,');

  Qry.SQL.Add('  emit_xCNAE           varchar(100)                               ,');
  Qry.SQL.Add('  emit_xCRT            varchar(100)                               ,');
  Qry.SQL.Add('  CodInfoEmit          int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NomeEmit             varchar(100) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeChaves(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ChaveNFe             varchar(44)                                ,');
  Qry.SQL.Add('  UF_IBGE              char(2)                                    ,');
  Qry.SQL.Add('  AAMM                 varchar(4)                                 ,');
  Qry.SQL.Add('  CNPJ                 varchar(18)                                ,');
  Qry.SQL.Add('  RazaoSocial          varchar(100)                               ,');
  Qry.SQL.Add('  ModNFe               char(2)                                    ,');
  Qry.SQL.Add('  SerNFe               char(3)                                    ,');
  Qry.SQL.Add('  NumNFe               varchar(11)                                ,');
  Qry.SQL.Add('  tpEmis               char(1)                                    ,');
  Qry.SQL.Add('  AleNFe               varchar(11)                                ,');
  Qry.SQL.Add('  emit_UF              char(2)                                    ,');
  Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (ChaveNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeCabY(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatParcela           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  nDup                 varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dVenc                date         NOT NULL                      ,');
  Qry.SQL.Add('  vDup                 double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Lancto               int(11)                                    ,');
  Qry.SQL.Add('  Sub                  int(11)                                    ,');
  Qry.SQL.Add('  Genero               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Descricao            varchar(100) NOT NULL                      ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
{
function TCreateNFe.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
}
procedure TCreateNFe.Cria_ntrttNFeCP01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  cancNFe_versao       double(4,2)  NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_Id           varchar(46)  NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_tpAmb        tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  infCanc_xServ        varchar(8)   NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_chNFe        varchar(44)  NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_nProt        bigint(15)   NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_xJust        varchar(255) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeCR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  retCanc_versao       double(4,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  infCanc_Id           varchar(60)  NOT NULL                      ,'); // tamanho indefinido!
  Qry.SQL.Add('  infCanc_tpAmb        tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  infCanc_verAplic     varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_cStat        mediumint(3) NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_xMotivo      varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_cUF          tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_xUF          char(2)      NOT NULL DEFAULT "??"         ,');
  Qry.SQL.Add('  infCanc_chNFe        varchar(44)                                ,');
  Qry.SQL.Add('  infCanc_dhRecbto     datetime                                   ,');
  Qry.SQL.Add('  infCanc_nProt        bigint(15)                                 ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeD(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  avulsa_CNPJ          varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_xOrgao        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_matr          varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_xAgente       varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_fone          varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_UF            char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_nDAR          varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_dEmi          date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  avulsa_vDAR          double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  avulsa_repEmi        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  avulsa_dPag          date         NOT NULL DEFAULT "0000-00-00" ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeDP01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  inutNFe_versao       double(4,2)  NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_Id           varchar(43)  NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_tpAmb        tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  inutNFe_xServ        varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_cUF          tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_xUF          char(2)                                    ,');
  Qry.SQL.Add('  inutNFe_ano          tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  inutNFe_CNPJ         varchar(14)                                ,');
  Qry.SQL.Add('  inutNFe_mod          tinyint(2)   NOT NULL DEFAULT "55"         ,');
  Qry.SQL.Add('  inutNFe_serie        mediumint(3) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  inutNFe_nNFIni       int(9)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  inutNFe_nNFFin       int(9)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  inutNFe_xJust        varchar(255) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeDR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  retInutNFe_versao    double(4,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  retInutNFe_Id        varchar(17)  NOT NULL                      ,'); // tamanho indefinido!
  Qry.SQL.Add('  retInutNFe_tpAmb     tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_xAmb      varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  retInutNFe_verAplic  varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_cStat     mediumint(3) NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_xMotivo   varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_cUF       tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_xUF       char(2)      NOT NULL DEFAULT "??"         ,');

  Qry.SQL.Add('  retInutNFe_ano       tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  retInutNFe_CNPJ      varchar(14)                                ,');
  Qry.SQL.Add('  retInutNFe_mod       tinyint(2)   NOT NULL DEFAULT "55"         ,');
  Qry.SQL.Add('  retInutNFe_serie     mediumint(3) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  retInutNFe_nNFIni    int(9)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  retInutNFe_nNFFin    int(9)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  retInutNFe_dhRecbto  datetime                                   ,');
  Qry.SQL.Add('  retInutNFe_nProt     bigint(15)                                 ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeE(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  dest_CNPJ            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_CPF             varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_xNome           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_xLgr            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_nro             varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_xCpl            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_xBairro         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_cMun            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  dest_xMun            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_UF              char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  dest_CEP             varchar(8)   NOT NULL                      ,');
  Qry.SQL.Add('  dest_cPais           int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  dest_xPais           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_fone            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_IE              varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_ISUF            varchar(9)   NOT NULL                      ,');
  Qry.SQL.Add('  dest_email           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_IM              varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  dest_indIEDest       tinyint(1)   NOT NULL                      ,');

  Qry.SQL.Add('  CodInfoDest          int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NomeDest             varchar(100) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeF(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  retirada_CNPJ        varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_CPF         varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_xLgr        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_nro         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_xCpl        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_xBairro     varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_cMun        int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  retirada_xMun        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  retirada_UF          char(2)      NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeG(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  entrega_CNPJ         varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_CPF          varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_xLgr         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_nro          varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_xCpl         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_xBairro      varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_cMun         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  entrega_xMun         varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  entrega_UF           char(2)      NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeGA(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  autXML_CNPJ          varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  autXML_CPF           varchar(11)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, autXML_CNPJ, autXML_CPF)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  prod_cProd           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  prod_cEAN            varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  prod_xProd           varchar(120) NOT NULL                      ,');
  Qry.SQL.Add('  prod_NCM             varchar(8)   NOT NULL                      ,');
  Qry.SQL.Add('  prod_EXTIPI          char(3)      NOT NULL                      ,');
  //Qry.SQL.Add('  prod_genero          tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  prod_CFOP            int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  prod_uCom            varchar(6)   NOT NULL                      ,');
  Qry.SQL.Add('  prod_qCom            double(12,4) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vUnCom          double(21,10) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vProd           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  prod_cEANTrib        varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  prod_uTrib           varchar(6)   NOT NULL                      ,');
  Qry.SQL.Add('  prod_qTrib           double(12,4) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vUnTrib         double(21,10) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vFrete          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vSeg            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vDesc           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  prod_vOutro          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  prod_indTot          tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  prod_xPed            varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  prod_nItemPed        int(6)       NOT NULL                      ,');
{
  Qry.SQL.Add('  Tem_IPI              tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  _Ativo_              tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  InfAdCuztm           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  EhServico            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ICMSRec_pRedBC       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSRec_vBC          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSRec_pAliq        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSRec_vICMS        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  IPIRec_pRedBC        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  IPIRec_vBC           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  IPIRec_pAliq         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  IPIRec_vIPI          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISRec_pRedBC        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISRec_vBC           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISRec_pAliq         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISRec_vPIS          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSRec_pRedBC     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSRec_vBC        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSRec_pAliq      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSRec_vCOFINS    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  MeuID                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Lk                   int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DataCad              date         NOT NULL                      ,');
  Qry.SQL.Add('  DataAlt              date         NOT NULL                      ,');
  Qry.SQL.Add('  UserCad              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  UserAlt              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  AlterWeb             tinyint(1)   NOT NULL                      ,');
}
  Qry.SQL.Add('  prod_nFCI            varchar(36)                                ,');
  Qry.SQL.Add('  prod_CEST            int(7)       NOT NULL  DEFAULT "0"         ,');


  Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NomeGGX              varchar(120) NOT NULL                      ,');

  Qry.SQL.Add('  KlsGGXE              tinyint(1)   NOT NULL  DEFAULT 0           ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0           ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeIDi(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DI_nDI               varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  DI_dDI               date         NOT NULL                      ,');
  Qry.SQL.Add('  DI_xLocDesemb        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  DI_UFDesemb          char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  DI_dDesemb           date         NOT NULL                      ,');
  Qry.SQL.Add('  DI_cExportador       varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeIDiA(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DI_nDI               varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Adi_nAdicao          int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Adi_nSeqAdic         int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Adi_cFabricante      varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Adi_vDescDI          double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem, Controle, Conta)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeM(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FatNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  nItem                          int(3)       NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  iTotTrib                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  vTotTrib                       double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  //Qry.SQL.Add('  vBasTrib                       double(15,2)           DEFAULT "0.00"      ,');
  //Qry.SQL.Add('  pTotTrib                       double(15,4)           DEFAULT "0.0000"    ,');
  //Qry.SQL.Add('  fontTotTrib                    tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  tabTotTrib                     tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  verTotTrib                     varchar(30)  NOT NULL                      ,');
  //Qry.SQL.Add('  tpAliqTotTrib                  tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeN(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_Orig            tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_CST             tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_modBC           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pRedBC          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vBC             double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pICMS           double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMS           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_modBCST         tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pMVAST          double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pRedBCST        double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vBCST           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pICMSST         double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMSST         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_CSOSN           mediumint(3) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_UFST            char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pBCOp           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vBCSTRet        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMSSTRet      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_motDesICMS      tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pCredSN         double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vCredICMSSN     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_pDif            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMSDif        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMSDeson      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMS_vICMSOp         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0           ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeO(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  IPI_clEnq            varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  IPI_CNPJProd         varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  IPI_cSelo            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  IPI_qSelo            double(12,0) NOT NULL                      ,');
  Qry.SQL.Add('  IPI_cEnq             char(3)      NOT NULL                      ,');
  Qry.SQL.Add('  IPI_CST              tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  IPI_vBC              double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  IPI_qUnid            double(16,4) NOT NULL                      ,');
  Qry.SQL.Add('  IPI_vUnid            double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  IPI_pIPI             double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  IPI_vIPI             double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeP(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  II_vBC               double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  II_vDespAdu          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  II_vII               double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  II_vIOF              double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFePR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  protNFe_versao       double(4,2)  NOT NULL                      ,');
  Qry.SQL.Add('  infProt_Id           varchar(46)  NOT NULL                      ,');
  Qry.SQL.Add('  infProt_tpAmb        tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  infProt_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  infProt_verAplic     varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  infProt_chNFe        varchar(44)                                ,');
  Qry.SQL.Add('  infProt_dhRecbto     datetime                                   ,');
  Qry.SQL.Add('  infProt_nProt        bigint(15)                                 ,');
  Qry.SQL.Add('  infProt_digVal       varchar(28)                                ,');
  Qry.SQL.Add('  infProt_cStat        mediumint(3) NOT NULL                      ,');
  Qry.SQL.Add('  infProt_xMotivo      varchar(255) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeQ(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  PIS_CST              tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  PIS_vBC              double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PIS_pPIS             double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  PIS_vPIS             double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PIS_qBCProd          double(16,4) NOT NULL                      ,');
  Qry.SQL.Add('  PIS_vAliqProd        double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  PIS_fatorBC          double(6,4)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  PISST_vBC            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISST_pPIS           double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  PISST_qBCProd        double(16,4) NOT NULL                      ,');
  Qry.SQL.Add('  PISST_vAliqProd      double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  PISST_vPIS           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  PISST_fatorBCST      double(6,4)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeS(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_CST           tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_vBC           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_pCOFINS       double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_qBCProd       double(16,4) NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_vAliqProd     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_vCOFINS       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINS_fatorBC       double(6,4)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeT(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_vBC         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_pCOFINS     double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_qBCProd     double(16,4) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_vAliqProd   double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_vCOFINS     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  COFINSST_fatorBCST   double(6,4)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeU(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vBC            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vAliq          double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vISSQN         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_cMunFG         int(7)       NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_cListServ      int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_cSitTrib       char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vDeducao       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vOutro         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vDescIncond    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vDescCond      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_vISSRet        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_indISS         tinyint(2)   NOT NULL  DEFAULT 0           ,');
  Qry.SQL.Add('  ISSQN_cServico       varchar(20)                                ,');
  Qry.SQL.Add('  ISSQN_cMun           int(7)       NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_cPais          int(4)       NOT NULL                      ,');
  Qry.SQL.Add('  ISSQN_nProcesso      varchar(30)                                ,');
  Qry.SQL.Add('  ISSQN_indIncentivo   tinyint(1)   NOT NULL  DEFAULT 0           ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeV(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  InfAdProd            text         NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeW02(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  ICMSTot_vBC          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vICMS        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vBCST        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vST          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vProd        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vFrete       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vSeg         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vDesc        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vII          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vIPI         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vPIS         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vCOFINS      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vOutro       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vNF          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vICMSDeson   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vTotTrib     double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  ICMSTot_vFCPUFDest   double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ICMSTot_vICMSUFDest  double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ICMSTot_vICMSUFRemet double(15,2)           DEFAULT "0.00"      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeW17(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  ISSQNtot_vServ       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQNtot_vBC         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQNtot_vISS        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQNtot_vPIS        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ISSQNtot_vCOFINS     double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeW23(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  RetTrib_vRetPIS      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vRetCOFINS   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vRetCSLL     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vBCIRRF      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vIRRF        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vBCRetPrev   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTrib_vRetPrev     double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeX01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  ModFrete             tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  xModFrete            varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_CNPJ      varchar(14)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_CPF       varchar(11)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_XNome     varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_IE        varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_XEnder    varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_XMun      varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Transporta_UF        char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_vServ      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_vBCRet     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_PICMSRet   double(5,2)  NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_vICMSRet   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_CFOP       varchar(4)   NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_CMunFG     varchar(7)   NOT NULL                      ,');
  Qry.SQL.Add('  RetTransp_xMunFG     varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  VeicTransp_Placa     varchar(8)   NOT NULL                      ,');
  Qry.SQL.Add('  VeicTransp_UF        char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  VeicTransp_RNTC      varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  Vagao                varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  Balsa                varchar(20)  NOT NULL                      ,');

  Qry.SQL.Add('  CodInfoTrsp          int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NomeTrsp             varchar(100) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeX22(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  placa                varchar(8)   NOT NULL                      ,');
  Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  RNTC                 varchar(20)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeX26(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  qVol                 double(15,0) NOT NULL                      ,');
  Qry.SQL.Add('  esp                  varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  marca                varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  nVol                 varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  pesoL                double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  pesoB                double(15,3) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeX33(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  nLacre               varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle, Conta)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeXR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  nfeProcNFe_versao    double(4,2)  NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeY01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  Cobr_Fat_nFat        varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Cobr_Fat_vOrig       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Cobr_Fat_vDesc       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Cobr_Fat_vLiq        double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeY07(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  nDup                 varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  dVenc                date         NOT NULL                      ,');
  Qry.SQL.Add('  vDup                 double(15,2) NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeYR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  procCancNFe_versao   double(4,2)  NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZ01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  InfAdic_InfAdFisco   text         NOT NULL                      ,');
  Qry.SQL.Add('  InfAdic_InfCpl       text         NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZ04(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  xCampo               varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  xTexto               varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZ07(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  xCampo               varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  xTexto               varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZ10(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  nProc                varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  indProc              tinyint(1)   NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZA01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  Exporta_UFEmbarq     char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  Exporta_XLocEmbarq   varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Exporta_XLocDespacho varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZB01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  FatNum               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');

  Qry.SQL.Add('  Compra_XNEmp         varchar(17)  NOT NULL                      ,');
  Qry.SQL.Add('  Compra_XPed          varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  Compra_XCont         varchar(60)  NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateNFe.Cria_ntrttNFeZR01(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  procInutNFe_versao   double(4,2)  NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TCreateNFe.NomeTabela(Tabela: TNomeTabRecriaTempTable): String;
begin
  case Tabela of
    //
    ntrttNFeArqs:         Result := Lowercase('_NFe_Arqs_');
    ntrttNFeCP01:         Result := Lowercase('_NFe_CP01_');
    ntrttNFeCR01:         Result := Lowercase('_NFe_CR01_');
    ntrttNFeDP01:         Result := Lowercase('_NFe_DP01_');
    ntrttNFeDR01:         Result := Lowercase('_NFe_DR01_');
    ntrttNFePR01:         Result := Lowercase('_NFe_PR01_');
    ntrttNFeXR01:         Result := Lowercase('_NFe_XR01_');
    ntrttNFeYR01:         Result := Lowercase('_NFe_YR01_');
    ntrttNFeZR01:         Result := Lowercase('_NFe_ZR01_');
    ntrttNFeA:            Result := Lowercase('_NFe_A_');
    ntrttNFeB:            Result := Lowercase('_NFe_B_');
    ntrttNFeB12a:         Result := Lowercase('_NFe_B_12a');
    ntrttNFeC:            Result := Lowercase('_NFe_C_');
    ntrttNFeD:            Result := Lowercase('_NFe_D_');
    ntrttNFeE:            Result := Lowercase('_NFe_E_');
    ntrttNFeF:            Result := Lowercase('_NFe_F_');
    ntrttNFeG:            Result := Lowercase('_NFe_G_');
    ntrttNFeGA:           Result := Lowercase('_NFe_GA_');
    ntrttNFeI:            Result := Lowercase('_NFe_I_');
    ntrttNFeIDi:          Result := Lowercase('_NFe_I_Di');
    ntrttNFeIDiA:         Result := Lowercase('_NFe_I_DiA');
    ntrttNFeM:            Result := Lowercase('_NFe_M_');
    ntrttNFeN:            Result := Lowercase('_NFe_N_');
    ntrttNFeO:            Result := Lowercase('_NFe_O_');
    ntrttNFeP:            Result := Lowercase('_NFe_P_');
    ntrttNFeQ:            Result := Lowercase('_NFe_Q_');
    ntrttNFeR:            Result := Lowercase('_NFe_R_');
    ntrttNFeS:            Result := Lowercase('_NFe_S_');
    ntrttNFeT:            Result := Lowercase('_NFe_T_');
    ntrttNFeU:            Result := Lowercase('_NFe_U_');
    ntrttNFeV:            Result := Lowercase('_NFe_V_');
    ntrttNFeW02:          Result := Lowercase('_NFe_W_02');
    ntrttNFeW17:          Result := Lowercase('_NFe_W_17');
    ntrttNFeW23:          Result := Lowercase('_NFe_W_23');
    ntrttNFeX01:          Result := Lowercase('_NFe_X_01');
    ntrttNFeX22:          Result := Lowercase('_NFe_X_22');
    ntrttNFeX26:          Result := Lowercase('_NFe_X_26');
    ntrttNFeX33:          Result := Lowercase('_NFe_X_33');
    ntrttNFeY01:          Result := Lowercase('_NFe_Y_01');
    ntrttNFeY07:          Result := Lowercase('_NFe_Y_07');
    ntrttNFeZ01:          Result := Lowercase('_NFe_Z_01');
    ntrttNFeZ04:          Result := Lowercase('_NFe_Z_04');
    ntrttNFeZ07:          Result := Lowercase('_NFe_Z_07');
    ntrttNFeZ10:          Result := Lowercase('_NFe_Z_10');
    ntrttNFeZA01:         Result := Lowercase('_NFe_ZA_01');
    ntrttNFeZB01:         Result := Lowercase('_NFe_ZB_01');
    ntrttNFeCabY:         Result := Lowercase('_NFe_CabY');
    //
    ntrttNFeChaves:       Result := Lowercase('_NFe_Chaves');
    // ...
    else Result := '';
  end;
end;

function TCreateNFe.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
    Nome := NomeTabela(Tabela)
  else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela temporária sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (números negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttNFeChaves:         Cria_ntrttNFeChaves(Qry);
    ntrttNFeCabY:           Cria_ntrttNFeCabY(Qry);
    //
    ntrttNFeArqs:           Cria_ntrttNFeArqs(Qry);
    ntrttNFeCP01:           Cria_ntrttNFeCP01(Qry); // Pedido de Cancelamento de NF-e
    ntrttNFeCR01:           Cria_ntrttNFeCR01(Qry); // Resultado da Solicitação de Cancelamento de NF-e
    ntrttNFeDP01:           Cria_ntrttNFeDP01(Qry); // Solicitação de Inutilização de Numeração de NF-e
    ntrttNFeDR01:           Cria_ntrttNFeDR01(Qry); // Resultado da Solicitação de Inutilização de Numeração de NF-e
    ntrttNFePR01:           Cria_ntrttNFePR01(Qry); // Protocolo de Recebimento da NF-e
    ntrttNFeXR01:           Cria_ntrttNFeXR01(Qry); // Leiaute da Distribuição: NF-e (Autorizada)
    ntrttNFeYR01:           Cria_ntrttNFeYR01(Qry); // Leiaute de Distribuição: Cancelamento de NF-e
    ntrttNFeZR01:           Cria_ntrttNFeZR01(Qry); // Leiaute de Distribuição: Inutilização de Numeração de NF-e
    ntrttNFeA:              Cria_ntrttNFeA   (Qry);
    ntrttNFeB:              Cria_ntrttNFeB   (Qry);
    ntrttNFeB12a:           Cria_ntrttNFeB12a(Qry);
    ntrttNFeC:              Cria_ntrttNFeC   (Qry);
    ntrttNFeD:              Cria_ntrttNFeD   (Qry);
    ntrttNFeE:              Cria_ntrttNFeE   (Qry);
    ntrttNFeF:              Cria_ntrttNFeF   (Qry);
    ntrttNFeG:              Cria_ntrttNFeG   (Qry);
    ntrttNFeGA:             Cria_ntrttNFeGA  (Qry);
    ntrttNFeI:              Cria_ntrttNFeI   (Qry);
    ntrttNFeIDi:            Cria_ntrttNFeIDi (Qry);
    ntrttNFeIDiA:           Cria_ntrttNFeIDiA(Qry);
    ntrttNFeM:              Cria_ntrttNFeM   (Qry);
    ntrttNFeN:              Cria_ntrttNFeN   (Qry);
    ntrttNFeO:              Cria_ntrttNFeO   (Qry);
    ntrttNFeP:              Cria_ntrttNFeP   (Qry);
    ntrttNFeQ:              Cria_ntrttNFeQ   (Qry);
    ntrttNFeR:              Cria_ntrttNFeR   (Qry);
    ntrttNFeS:              Cria_ntrttNFeS   (Qry);
    ntrttNFeT:              Cria_ntrttNFeT   (Qry);
    ntrttNFeU:              Cria_ntrttNFeU   (Qry);
    ntrttNFeV:              Cria_ntrttNFeV   (Qry);
    ntrttNFeW02:            Cria_ntrttNFeW02 (Qry);
    ntrttNFeW17:            Cria_ntrttNFeW17 (Qry);
    ntrttNFeW23:            Cria_ntrttNFeW23 (Qry);
    ntrttNFeX01:            Cria_ntrttNFeX01 (Qry);
    ntrttNFeX22:            Cria_ntrttNFeX22 (Qry);
    ntrttNFeX26:            Cria_ntrttNFeX26 (Qry);
    ntrttNFeX33:            Cria_ntrttNFeX33 (Qry);
    ntrttNFeY01:            Cria_ntrttNFeY01 (Qry);
    ntrttNFeY07:            Cria_ntrttNFeY07 (Qry);
    ntrttNFeZ01:            Cria_ntrttNFeZ01 (Qry);
    ntrttNFeZ04:            Cria_ntrttNFeZ04 (Qry);
    ntrttNFeZ07:            Cria_ntrttNFeZ07 (Qry);
    ntrttNFeZ10:            Cria_ntrttNFeZ10 (Qry);
    ntrttNFeZA01:           Cria_ntrttNFeZA01(Qry);
    ntrttNFeZB01:           Cria_ntrttNFeZB01(Qry);
    //
    else Geral.MensagemBox('Não foi possível criar a tabela temporária "' +
    Nome + '" por falta de implementação!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

