unit NFeDocOrfao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkImage, dmkGeral,
  UnDmkEnums;

type
  TFmNFeDocOrfao = class(TForm)
    Panel1: TPanel;
    Grade: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeDocOrfao: TFmNFeDocOrfao;

implementation

uses UnMyObjects, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeDocOrfao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeDocOrfao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeDocOrfao.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Grade.DataSource := DmNFe_0000.Ds_D_E;
end;

procedure TFmNFeDocOrfao.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
