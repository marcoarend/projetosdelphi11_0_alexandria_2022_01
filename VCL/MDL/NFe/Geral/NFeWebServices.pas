unit NFeWebServices;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkRadioGroup,
  mySQLDbTables, System.Variants;

type
  TFmNFeWebServices = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel9: TPanel;
    BtBaixar: TBitBtn;
    BtAbre: TBitBtn;
    Panel1: TPanel;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdCaminho: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    Grade1: TStringGrid;
    BtSalvar: TBitBtn;
    RGtpAmb: TdmkRadioGroup;
    QrNFe_WS: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNFe_WS();
    procedure ObtemDados(const Linha: String; var UF, Versoes, URL: String);
    procedure ListaLinha(UF, Versoes, URL: String);
    procedure CarregaXLS();
  public
    { Public declarations }
  end;

  var
  FmNFeWebServices: TFmNFeWebServices;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

const
  //FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/CNAE_CONCLA_21.xls';
  FDestFile = 'C:\_Compilers\Delphi_XE2\NFe\Web Services 2014_10_28_Homologacao_CSV.csv';

procedure TFmNFeWebServices.BtAbreClick(Sender: TObject);
var
  I: Integer;
  Arquivo, Linha, UF, Versoes, URL: String;
  Versao: Double;
  lstArq: TStringList;
begin
  Memo1.Lines.Clear;
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  if MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2, 1) then
  begin
    //BtCarrega.Enabled := True;
    CarregaXLS();
    PageControl1.ActivePageIndex := 1;
    //
    BtSalvar.Visible := True;
  end;
{
  Arquivo := EdCaminho.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    lstArq := TStringList.Create;
    try
      lstArq.LoadFromFile(Arquivo);
      if lstArq.Count > 0 then
      begin
        PB1.Position := 0;
        PB1.Max := lstArq.Count;
        //
        Linha := lstArq[0];
        for I := 1 to lstArq.Count - 1 do
        begin
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //
          Linha := lstArq[I];
          //
          ObtemDados(Linha, UF, Versoes, URL);
          //
          ListaLinha(UF, Versoes, URL);
        end;
      end;
      //
      Geral.MB_Aviso('Carregamento finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB1.Position := 0;
    finally
      if lstArq <> nil then
        lstArq.Free;
    end;
  end;
}
end;

procedure TFmNFeWebServices.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeWebServices.BtSalvarClick(Sender: TObject);
var
  I, J, TotVersoes, tpAmb: Integer;
  UF, Servico, Versoes, URL: String;
  Versao: Double;
  Res: Boolean;
begin
  Res          := False;
  tpAmb        := RGtpAmb.ItemIndex;
  PB1.Max      := Grade1.RowCount -1;
  PB1.Position := 0;

  UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrAllUpd, DModG.AllID_DB, [
    'DELETE FROM nfe_ws WHERE tpAmb=' + Geral.FF0(tpAmb),
    '']);

  if MyObjects.FIC(tpAmb = 0, RGtpAmb, 'Defina a identifica��o do ambiente!') then Exit;

  ReopenNFe_WS;

  for I := 1 to Grade1.RowCount -1 do
  begin
    UF := Trim(Geral.Substitui(Grade1.Cells[01, I], #9, ' '));

    if UF <> '' then
    begin
      Versoes    := Trim(Geral.Substitui(Grade1.Cells[03, I], #9, ' '));
      Versoes    := Geral.SoNumero_TT(Versoes);
      TotVersoes := Round(Length(Versoes) / 4);

      for J := 1 to TotVersoes do
      begin
        if J = 1 then
          Versao := Geral.DMV(Copy(Versoes, 1, 3)) / 100
        else
          Versao := Geral.DMV(Copy(Versoes, ((J - 1) * 3) + 1, 3))  / 100;

        Servico := Trim(Geral.Substitui(Grade1.Cells[02, I], #9, ' '));
        URL     := Trim(Geral.Substitui(Grade1.Cells[04, I], #9, ' '));

        if not QrNFe_WS.Locate('UF; Servico; Versao; URL; tpAmb',
          VarArrayOf([UF, Servico, Versao, URL, tpAmb]), []) then
        begin
          if not UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'nfe_ws', False,
            ['UF', 'Servico', 'Versao', 'URL', 'tpAmb'], [],
            [UF, Servico, Versao, URL, tpAmb], [], True) then
          begin
            Geral.MB_Erro('Falha ao carregar arquivo!');
            Exit;
          end else
            Res := True;
        end;
      end;
    end;
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
  end;

  if Res then
    Geral.MB_Aviso('Dados importados com sucesso!');

  PB1.Position := 0;
end;

procedure TFmNFeWebServices.CarregaXLS;
const
  c1 = 'if x = Uppercase(''';
  c2 = ''') then FURL := ''';
  c3 = ''' else';
var
  Linha: String;
var
  //LeftZeros, InfoVazio: Byte;
  R, K(*, P, L, N*): Integer;
  //Grupo, Nome, ID, Campo, Descricao, Elemento, Pai, Tipo, Tamanho, Ocorrencia,
  //Codigo, Observacao, Casas, TamVar, CodTxt, MinTxt, MaxTxt: String;
  //OcorMin, OcorMax, TamMin, TamMax, DeciCasas, Ordem: Integer;
  UF, Servico, Versoes, Versao, URL: String;
begin
  for R := 1 to Grade1.RowCount -1 do
  begin
    PB1.Position := PB1.Position + 1;
    UF      := Trim(Geral.Substitui(Grade1.Cells[01, R], #9, ' '));
    Servico := Trim(Geral.Substitui(Grade1.Cells[02, R], #9, ' '));
    Versoes := Trim(Geral.Substitui(Grade1.Cells[03, R], #9, ' '));
    URL     := Trim(Geral.Substitui(Grade1.Cells[04, R], #9, ' '));
    if UF <> '' then
    begin
      K := pos('/', Versoes);
      while K > 0 do
      begin
        Versao := Trim(Copy(Versoes, 1, K - 1));
        if Versao <> '' then
          Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3;
        Versoes := Trim(Copy(Versoes, K + 1));
        //
        K := pos('/', Versoes);
      end;
      Versao := Versoes;
      if Versao <> '' then
        Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3
    end
    else
      Linha := '//' + UF + ' ' + Servico + ' ' + Versoes + ' ' + URL;
    //
    Memo1.Lines.Add(Linha);
  end;
(*
      Nome := Trim(Grade1.Cells[01,R]);
      P := pos('-', Nome);
      Grupo := Trim(Copy(Nome, 1, P - 1));
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelayc', False, [
      'Nome'], ['Grupo'], [Nome], [Grupo], False);
    end else
    if CodTxt <> '' then
    begin
      if CodTxt[1] in (['0'..'9']) then
        Codigo := CodTxt
      else
        Codigo := '';
      if Codigo <> '' then
      begin
        L := R + 1;
        while (Trim(Grade1.Cells[02,L]) = '') and (L < Grade1.RowCount) do
          L := L + 1;
        L := L -1;
        //#	ID	Campo	Descri��o	Ele	Pai	Tipo	Ocorr�ncia	tamanho	Dec.	Observa��o
        ID         := Grade1.Cells[03,R];
        Campo      := Grade1.Cells[04,R];
        Descricao  := Grade1.Cells[05,R];
        Elemento   := Grade1.Cells[06,R];
        Pai        := Grade1.Cells[07,R];
        Tipo       := Grade1.Cells[08,R];
        Ocorrencia := Grade1.Cells[09,R];
        Tamanho    := Grade1.Cells[10,R];
        Casas      := Grade1.Cells[11,R];
        Observacao := Grade1.Cells[12,R];
        for N := R + 1 to L do
        begin
          ID         := ID         + Grade1.Cells[03,N];
          Campo      := Campo      + Grade1.Cells[04,N];
          Descricao  := Descricao  + ' ' + Grade1.Cells[05,N];
          Elemento   := Elemento   + Grade1.Cells[06,N];
          Pai        := Pai        + Grade1.Cells[07,N];
          Tipo       := Tipo       + Grade1.Cells[08,N];
          Ocorrencia := Ocorrencia + Grade1.Cells[09,N];
          Tamanho    := Tamanho    + Grade1.Cells[10,N];
          Casas      := Casas      + Grade1.Cells[11,N];
          Observacao := Observacao + ' ' + Grade1.Cells[12,N];
        end;
        DeciCasas := Geral.IMV(Trim(Casas));
        //
        P := pos('-', Ocorrencia);
        if P > 0 then
        begin
          MinTxt   := Copy(Ocorrencia, 1, p - 1);
          OcorMin  := Geral.IMV(MinTxt);
          MaxTxt   := Copy(Ocorrencia, p + 1);
          if Uppercase(MaxTxt) = 'N' then
            OcorMax  := High(Integer)
          else
            OcorMax  := Geral.IMV(MaxTxt);
        end else begin
          OcorMin    := Geral.IMV(Ocorrencia);
          OcorMax    := OcorMin;
        end;
        //
        MLAGeral.SoNumeroDeTamanhoDeCampoDeNFe(Tamanho, Tamanho);
        //if not k then
        MLAGeral.SeparaIntervalosDeTamanhosDeNFe(Tamanho, Tamanho, TamVar);
        //
        Tamanho := Trim(Tamanho);
        //T := Length(Tamanho);
        P := pos('-', Tamanho);
        if P > 0 then
        begin
          MinTxt  := Trim(Copy(Tamanho, 1, p - 1));
          TamMin  := Geral.IMV(MinTxt);
          //
          MaxTxt  := Trim(Copy(Tamanho, P + 1));
          if Uppercase(MaxTxt) = 'N' then
            TamMax  := High(Integer)
          else
            TamMax  := Geral.IMV(MaxTxt);
          //
          //TamVar  := '';
        end else begin
          TamMin  := Geral.IMV(Tamanho);
          TamMax  := TamMin;
          //TamVar  := '';
        end;
        if pos('CNPJ', Campo) > 0 then LeftZeros := 1 else
        if pos('CPF',  Campo) > 0 then LeftZeros := 1 else
        if pos('CEP',  Campo) > 0 then LeftZeros := 1 else
        if pos('CST',  Campo) > 0 then LeftZeros := 1 else
        if pos('NCM',  Campo) > 0 then LeftZeros := 1 else
        if pos('EAN',  Campo) > 0 then LeftZeros := 1 else
        if Campo = 'cNF'          then LeftZeros := 1 else
                                       LeftZeros := 0;
        if pos('EAN',  Campo) > 0 then InfoVazio := 1 else
                                       InfoVazio := 0;
        Ordem := Ordem + 1;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelayi', False, [
        'Grupo', 'Codigo', 'ID',
        'Campo', 'Descricao', 'Elemento',
        'Pai', 'Tipo', 'OcorMin',
        'OcorMax', 'TamMin', 'TamMax',
        'TamVar', 'DeciCasas', 'Observacao',
        'LeftZeros', 'InfoVazio'], [
        'Ordem'], [
        Grupo, Codigo, ID,
        Campo, Descricao, Elemento,
        Pai, Tipo, OcorMin,
        OcorMax, TamMin, TamMax,
        TamVar, DeciCasas, Observacao,
        LeftZeros, InfoVazio
        ], [
        Ordem], False);
      end;
    end
  end;
  PB1.Position := PB1.Max;
  ReopenNFeLayI('', '');
finally
  Screen.Cursor := crDefault;
*)
end;

procedure TFmNFeWebServices.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeWebServices.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  BtSalvar.Visible  := False;
  RGtpAmb.ItemIndex := 0;
end;

procedure TFmNFeWebServices.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeWebServices.ListaLinha(UF, Versoes, URL: String);
const
  c1 = 'if x = Uppercase(''';
  c2 = ''') then FURL := ''';
  c3 = '''';
var
  Linha: String;
begin
  if UF <> '' then
    Linha := c1 + Trim(Uppercase(UF)) + ' ' + Versoes + ' ' + c2 + URL + c3
  else
    Linha := '//' + UF + ' ' + Versoes + ' ' + URL;
  //
  Memo1.Lines.Add(Linha);
end;

procedure TFmNFeWebServices.ObtemDados(const Linha: String; var UF, Versoes,
  URL: String);
var
  lstLin: TStringList;
  P: Integer;
begin
  UF      := '';
  Versoes := '';
  URL     := '';
  //
  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      //
      if lstLin.Count > 0 then
        UF    := lstLin[00];
      if lstLin.Count > 1 then
        Versoes := lstLin[01];
      if lstLin.Count > 2 then
        URL := lstLin[02];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
  end;
end;

procedure TFmNFeWebServices.ReopenNFe_WS;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_WS, DModG.AllID_DB, [
    'SELECT * ',
    'FROM nfe_ws ',
    '']);
end;

procedure TFmNFeWebServices.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir  := ExtractFileDir(FDestFile);
  Arquivo := ExtractFileName(FDestFile);
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo', '', [], Arquivo) then
    EdCaminho.Text := Arquivo;
end;

end.
