object FmNFeExportaXML: TFmNFeExportaXML
  Left = 339
  Top = 185
  Caption = 'XML-EXPOR-001 :: Exporta XML de NF-e'
  ClientHeight = 508
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 765
    Height = 355
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object TPanel
      Left = 0
      Top = 0
      Width = 765
      Height = 351
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Emitente:'
      end
      object Label14: TLabel
        Left = 8
        Top = 43
        Width = 59
        Height = 13
        Caption = 'Destinat'#225'rio:'
      end
      object Label1: TLabel
        Left = 8
        Top = 272
        Width = 54
        Height = 13
        Caption = 'Pasta Raiz:'
      end
      object SbRaiz: TSpeedButton
        Left = 741
        Top = 287
        Width = 20
        Height = 21
        Caption = '...'
        OnClick = SbRaizClick
      end
      object Label5: TLabel
        Left = 8
        Top = 311
        Width = 233
        Height = 13
        Caption = 'Nome da pasta (e arquivo zip) a ser(em) criado(s):'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 55
        Height = 20
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 63
        Top = 20
        Width = 567
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDest: TdmkEditCB
        Left = 8
        Top = 59
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDest
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDest: TdmkDBLookupComboBox
        Left = 63
        Top = 59
        Width = 567
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsDest
        TabOrder = 3
        dmkEditCB = EdDest
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object GroupBox2: TGroupBox
        Left = 634
        Top = 14
        Width = 127
        Height = 117
        Caption = ' Per'#237'odo: '
        TabOrder = 4
        object Label2: TLabel
          Left = 8
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label3: TLabel
          Left = 8
          Top = 63
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object TPIni: TdmkEditDateTimePicker
          Left = 8
          Top = 35
          Width = 110
          Height = 21
          Date = 40182.000000000000000000
          Time = 0.966222291666781500
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPFim: TdmkEditDateTimePicker
          Left = 8
          Top = 78
          Width = 110
          Height = 21
          Date = 40182.000000000000000000
          Time = 0.966222291666781500
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
      object CGStatus: TdmkCheckGroup
        Left = 8
        Top = 82
        Width = 622
        Height = 49
        Caption = ' Status da NFe: '
        Columns = 4
        ItemIndex = 2
        Items.Strings = (
          'Autorizadas'
          'Canceladas'
          'Inutilizadas'
          'Denegadas')
        TabOrder = 5
        OnClick = CGStatusClick
        UpdType = utYes
        Value = 4
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 185
        Width = 753
        Height = 84
        Caption = ' Outros filtros: '
        TabOrder = 7
        object CkProt: TCheckBox
          Left = 12
          Top = 20
          Width = 730
          Height = 17
          Caption = 'Incluir protocolo de autoriza'#231#227'o / cancelamento da NFe.'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 0
        end
        object CkUmArqPorNFe: TCheckBox
          Left = 12
          Top = 39
          Width = 730
          Height = 17
          Caption = 'Gerar um arquivo para cada NFe / inutiliza'#231#227'o.'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 1
        end
        object CkZipar: TCheckBox
          Left = 12
          Top = 59
          Width = 730
          Height = 17
          Caption = 'Zipar a pasta gerada.'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
      object EdRaiz: TEdit
        Left = 8
        Top = 287
        Width = 730
        Height = 21
        TabOrder = 8
        Text = 'C:\Dermatek\ExportaXML'
      end
      object EdArq: TEdit
        Left = 8
        Top = 327
        Width = 753
        Height = 21
        TabOrder = 9
      end
      object CGEventos: TdmkCheckGroup
        Left = 8
        Top = 134
        Width = 753
        Height = 48
        Caption = ' Eventos: '
        Columns = 4
        ItemIndex = 1
        Items.Strings = (
          'Cartas de corre'#231#227'o'
          'Canceladas')
        TabOrder = 6
        OnClick = CGStatusClick
        UpdType = utYes
        Value = 2
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 247
        Height = 31
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 31
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 247
        Height = 31
        Caption = 'Exporta XML de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 402
    Width = 993
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 445
    Width = 993
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 846
        Top = 0
        Width = 143
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 18
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 765
    Top = 47
    Width = 228
    Height = 355
    Align = alClient
    DataSource = DsA
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object QrDest: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'Tipo, IF(Tipo=0,CNPJ, CPF) CNPJ_CPF'
      'FROM Entidades'
      'WHERE ('
      '     (Tipo=0 AND CNPJ <> "")'
      '     OR'
      '     (Tipo=1 AND CPF <> "")'
      ')'
      'ORDER BY NO_ENT'
      '')
    Left = 120
    Top = 92
    object QrDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrDestTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDestCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsDest: TDataSource
    DataSet = QrDest
    Left = 148
    Top = 92
  end
  object QrA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE ide_mod=55'
      'AND emit_CNPJ="02595943000130"'
      'AND ide_dEmi BETWEEN "2009-09-01" AND "2009-09-30"')
    Left = 8
    Top = 4
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 36
    Top = 4
  end
  object QrNFeEveRCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeevercab'
      'WHERE dhEvento BETWEEN "2012/11/29 12:02:48"'
      'AND "2013/05/23 10:03:24"'
      'OR chNFe IN ('
      'SELECT caba.Id'
      'FROM nfecaba caba'
      'WHERE caba.ide_mod IN (55)'
      'AND caba.emit_CNPJ="10783968000195"'
      'AND caba.Status IN (100)'
      
        ' AND caba.ide_dEmi  BETWEEN "2013-05-01" AND "2013-07-31 23:59:5' +
        '9"'
      ')'
      'ORDER BY dhEvento')
    Left = 216
    Top = 92
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrNFeEveRCabId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrNFeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrNFeEveRCabCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFeEveRCabCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrNFeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrNFeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCabret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrNFeEveRCabret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrNFeEveRCabret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrNFeEveRCabret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrNFeEveRCabret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrNFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCabret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrNFeEveRCabret_chNFe: TWideStringField
      FieldName = 'ret_chNFe'
      Size = 44
    end
    object QrNFeEveRCabret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrNFeEveRCabret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrNFeEveRCabret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrNFeEveRCabret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrNFeEveRCabret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrNFeEveRCabret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrNFeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRCabret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrNFeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrNFeEveRCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEveRCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEveRCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEveRCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEveRCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEveRCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEveRCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
