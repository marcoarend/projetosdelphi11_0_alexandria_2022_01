unit NFeXMLGerencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, Rio, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans,
  {$IfDef VER320} JwaWinCrypt, {$EndIf}
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, StrUtils, ComObj,
  ModuleNFe_0000, UnInternalConsts, UnXXe_PF, UnGrl_Vars;

const
  //NFe_VerAtual        = '3.00';
  //NFe_AllVersoes      = '3.00'; // SQL ?
  //NFe_ModeloAtual     = '55';
  NFe_AllModelos      = '55'; // SQL 55,?,?
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
  //NFe_CodRejeitad  = ' entre 201 e 299 e > 400
  // XML
  sXML_Version  = '1.0';
  sXML_Encoding = 'UTF-8';

  // DesmontarChaveNFE
  DEMONTA_CHV_INI_SerNFe = 23;
  DEMONTA_CHV_TAM_SerNFe = 03;
  DEMONTA_CHV_INI_NumNFe = 26;
  DEMONTA_CHV_TAM_NumNFe = 09;

  // Eventos
  verEnviEvento_Versao     = '1.00'; // Envia Lote de Eventos da NF-e
  verProcCCeNFe_Versao     = '1.00'; // Armazenamento e disposi��o da carta de corre��o

  NFe_CodsEveWbserverUserDef = '210200, 210210, 210220, 210240';

  NFe_CodEventoCCe = 110110;
  NFe_CodEventoCan = 110111;
  NFe_CodEventoEPEC = 110140;
  NFe_CodEventoMDeNaoConsultada = 0;
  NFe_CodEventoMDeConfirmacao   = 210200;
  NFe_CodEventoMDeCiencia       = 210210;
  NFe_CodEventoMDeDesconhece    = 210220;
  NFe_CodEventoMDeNaoRealizou   = 210240;

  NFe_CodEventoSitNFeNaoConsultada = 0;
  NFe_CodEventoSitNFeMDeAutorizada = 1;
  NFe_CodEventoSitNFeMDeDenegada   = 2;
  NFe_CodEventoSitNFeMDeCancelada  = 3;

  NFe_CodEventoSitConfNaoConsultada      = -1;
  NFe_CodEventoSitConfMDeSemManifestacao = 0;
  NFe_CodEventoSitConfMDeConfirmada      = 1;
  NFe_CodEventoSitConfMDeDesconhecida    = 2;
  NFe_CodEventoSitConfMDeNaoRealizada    = 3;
  NFe_CodEventoSitConfMDeCiencia         = 4;
  CO_STATUS_NFE_INDEFINIDO = 'Status n�o definido! Informe a DERMATEK';
type
  TEventosNFe = (eveCCe,   // Carta de corre��o
                 eveCan,   // Cancelamento da NFe como Evento da NF-e
                 eveMDe,   // Manifesta��o do deestinat�rio
                 eveEPEC  // Evento Pr�vio de Emiss�o em Conting�ncia
                 );
  TNFeXMLGerencia = class(TObject)
  private
    //function Evento_Obtem_tpEvento(Evento: TEventosNFe): Integer;
    { Private declarations }
    function ObtemCertificado_NumSerie(const NumeroSerial: String;
             out Cert: ICertificate2): Boolean;
    function ObtemCertificado_Arquivo((*out Cert: ICertificate2*)): Boolean;

  public
    { Public declarations }
    // NFe
    function DesmontaChaveDeAcesso(const Chave: String; const VersaoNFe:
             Double; var UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis,
             AleNFe: String): Boolean;
    function ChaveDeAcessoDesmontada(const Chave: String;
             const VersaoNFe: Double): String;
    function VerificaChaveAcesso(Chave: String; PermiteNulo: Boolean): Boolean;
    function Texto_StatusNFe(Status: Integer; VersaoNFe: Double): String;
    function SalvaXML(Arquivo: String; XML: String): Boolean;
    function TipoXML(NoStandAlone: Boolean): String;
    function XML_DistribuiNFe(const Id: String; const Status: Integer;
             const XML_NFe, XML_Aut, XML_Can: String; const Protocolar: Boolean;
             var XML_Distribuicao: String; const VersaoNFe: Double;
             const InfoLocal: String): Boolean;
    function XML_DistribuiEvento(const Id: String;
             const Status: Integer; const XML_Env, XML_Ret: String;
             const Protocolar: Boolean; var XML_Distribuicao: String;
             const Versao: Double; const InfoLocal: String): Boolean;
  //
    function XML_DistribuiCartaDeCorrecao(const VersaoCCe: String;
             const XML_Evento, XML_Protocolo: String;
             var XML_Distribuicao: String): Boolean;
    //
    function AssinarMSXML(XML: String; Certificado:
             ICertificate2; out XMLAssinado: String): Boolean;
    function PosEx(const SubStr, S: AnsiString; Offset: Cardinal = 1): Integer;
    function PosLast(const SubStr, S: AnsiString ): Integer;
    //
    function ObtemCertificado(const NumeroSerial: String;
             out Cert: ICertificate2): Boolean;
    function NFG_SerieToide_serie(const Modelo: Integer; const NFG_Serie:
             String; var ide_Serie: Integer): Boolean;
    function TextoAmbiente(Ambiente: Byte): String;
    // Eventos NFE
       // CCe - Carta de corre��o eletr�nica
    function  Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
              const EventoNFe: TEventosNFe; var
              tpEvento: Integer; var versao: Double; var descEvento: String):
              Boolean;
    function  Evento_MontaId(tpEvento: Integer; Chave: String; nSeqEvento:
              Integer): String;
    function  Obtem_DeManifestacao_de_tpEvento_cSitConf(tpEvento: Integer): Integer;
    function  Obtem_DeManifestacao_de_cSitConf_tpEvento(cSitConf: Integer): Integer;
    function  Obtem_DeManifestacao_de_cSitConf_xEvento(cSitConf: Integer): String;
    function  Obtem_DeManifestacao_de_cSitNFe_xSituacao(cSitNFe: Integer): String;
    function  Obtem_DeEvento_Nome(Evento: Integer): String;
    function  Obtem_cStat_De_cSitNFe(cSitNFe: Integer): Integer;
    function  Obtem_cSitNFe_De_cStat(cStat: Integer): Integer;
    procedure AutoridadeCertificadoraRaizBrasileiraV5();
    function  ObtemCNPJDeChaveDeAcesso(chNFe: String): String;
    procedure ObtemCNPJeModeloNFeDeChaveDeAcesso(const chNFe: String; var
              CNPJ_CPF: String; var ModeloNFe: Integer);
  end;

var
  NFeXMLGeren: TNFeXMLGerencia;

const
  // assinatura
  INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;
  DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';

implementation

//uses UnXXe_PF;

var
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;

function TNFeXMLGerencia.AssinarMSXML(XML: String;
  Certificado: ICertificate2; out XMLAssinado: String): Boolean;
var
 I, J, PosIni, PosFim : Integer;
 URI           : String ;
 Tipo : Integer;

 xmlHeaderAntes, xmlHeaderDepois : String ;
 xmldoc  : IXMLDOMDocument3;
 xmldsig : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
  if Pos('<Signature',XML) <= 0 then
  begin
    I := pos('<infNFe',XML) ;
    Tipo := 1;

    if I = 0  then
    begin
      I := pos('<infCanc',XML) ;
      if I > 0 then
        Tipo := 2
      else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
          Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
          begin
            I := Pos('<Evento', XML);
            if I > 0 then
              Tipo := 6
            else
              Tipo := 5 // '<evento'
          end else
            Tipo := 4;
        end;
     end;
    end;

      I := PosEx('Id=',XML,6) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: Id=') ;
      I := PosEx('"',XML,I+2) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas inicial') ;
      J := PosEx('"',XML,I+1) ;
      if J = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas final') ;

      URI := copy(XML,I+1,J-I-1) ;

      if Tipo = 1 then
         XML := copy(XML,1,pos('</NFe>',XML)-1)
      else if Tipo = 2 then
         XML := copy(XML,1,pos('</cancNFe>',XML)-1)
      else if Tipo = 3 then
         XML := copy(XML,1,pos('</inutNFe>',XML)-1)
      else if Tipo = 4 then
         XML := copy(XML,1,pos('</envDPEC>',XML)-1)
      else if Tipo = 5 then
         XML := copy(XML,1,pos('</evento>',XML)-1);
      {
      else if Tipo = 6 then
         XML := copy(XML,1,pos('</Evento>',XML)-1)
      }

      XML := XML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />';
      XML := XML + '<Reference URI="#'+URI+'">';
      XML := XML + '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />';
      XML := XML + '<DigestValue></DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature>';

      if Tipo = 1 then
         XML := XML + '</NFe>'
      else if Tipo = 2 then
         XML := XML + '</cancNFe>'
      else if Tipo = 3 then
         XML := XML + '</inutNFe>'
      else if Tipo = 4 then
         XML := XML + '</envDPEC>'
      else if Tipo = 5 then
         XML := XML + '</evento>';
      {
      else if Tipo = 6 then
         XML := XML + '</Evento>';
      }   
   end;

   // Lendo Header antes de assinar //
   xmlHeaderAntes := '' ;
   I := pos('?>',XML) ;
   if I > 0 then
      xmlHeaderAntes := copy(XML,1,I+1) ;

   xmldoc := CoDOMDocument50.Create;

   xmldoc.async              := False;
   xmldoc.validateOnParse    := False;
   xmldoc.preserveWhiteSpace := True;

   xmldsig := CoMXDigitalSignature50.Create;

   if (not xmldoc.loadXML(XML) ) then
   begin
     Geral.MensagemBox('N�o foi poss�vel carregar o arquivo: ' + #13#10 + XML, 'ERRO', MB_OK+MB_ICONERROR);
     raise Exception.Create('N�o foi poss�vel carregar o arquivo: '+XML);
   end;
   xmldoc.setProperty('SelectionNamespaces', DSIGNS);

   xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
   //xmldsig.signature := xmldoc.selectSingleNode('Signature');

   if (xmldsig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   if NumCertCarregado <> Certificado.SerialNumber then
      CertStoreMem := nil;

   if  CertStoreMem = nil then
   begin
      if VAR_CERTIFICADO_DIGITAL = nil then
      begin
        CertStore := CoStore.Create;
        CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

        CertStoreMem := CoStore.Create;
        CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

        Certs := CertStore.Certificates as ICertificates2;
        for i:= 1 to Certs.Count do
        begin
          Cert := IInterface(Certs.Item[i]) as ICertificate2;
          if Cert.SerialNumber = Certificado.SerialNumber then
          begin
            CertStoreMem.Add(Cert);
            NumCertCarregado := Certificado.SerialNumber;
          end;
        end;
      end
     else
     begin
       CertStoreMem := CoStore.Create;
       CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
       CertStoreMem.Add(VAR_CERTIFICADO_DIGITAL);
       NumCertCarregado := VAR_CERTIFICADO_DIGITAL.SerialNumber;
     end;
   end;


   OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
   xmldsig.store := CertStoreMem;

   dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
   if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP. (assinatura digital)');

   signedKey := xmldsig.sign(dsigKey, $00000002);
   if (signedKey <> nil) then
    begin
      XMLAssinado := xmldoc.xml;
      XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] ) ;
      XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] ) ;
      PosIni := Pos('<SignatureValue>',XMLAssinado)+length('<SignatureValue>');
      XMLAssinado := copy(XMLAssinado,1,PosIni-1)+StringReplace( copy(XMLAssinado,PosIni,length(XMLAssinado)), ' ', '', [rfReplaceAll] ) ;
      PosIni := Pos('<X509Certificate>',XMLAssinado)-1;
      PosFim := PosLast('<X509Certificate>',XMLAssinado);

      XMLAssinado := copy(XMLAssinado,1,PosIni)+copy(XMLAssinado,PosFim,length(XMLAssinado));
    end
   else
     raise Exception.Create('Ocorreu uma falha ao tentar assinar o XML!');
      //raise Exception.Create('Assinatura Falhou.');
   if xmlHeaderAntes <> '' then
   begin
      I := pos('?>',XMLAssinado) ;
      if I > 0 then
       begin
         xmlHeaderDepois := copy(XMLAssinado,1,I+1) ;
         if xmlHeaderAntes <> xmlHeaderDepois then
            XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes) ;
       end
      else
         XMLAssinado := xmlHeaderAntes + XMLAssinado ;
   end ;

   dsigKey   := nil;
   signedKey := nil;
   xmldoc    := nil;
   xmldsig   := nil;

   Result := True;
end;

procedure TNFeXMLGerencia.AutoridadeCertificadoraRaizBrasileiraV5();
var
  Serial: String;
  I, J: Integer;
  OID: TOID;
  ExtendedKeyUsage: IExtendedKeyUsage;
  ExtendedKeyUsageDisp: IExtendedKeyUsageDisp;
  KeyUsage: IKeyUsage;
  ExtendedProperties: IExtendedProperties;
  EKUs: IEKUs;
  EKU: IEKU;
  BasicConstraints: IBasicConstraints;
begin
  Serial := '01';
    CertStore := CoStore.Create;
    CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'CA', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

    CertStoreMem := CoStore.Create;
    CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

    Certs := CertStore.Certificates as ICertificates2;
    for i:= 1 to Certs.Count do
    begin
      Cert := IInterface(Certs.Item[i]) as ICertificate2;
      if Cert.SerialNumber = Serial then
      begin
        //if pos('v5', String(Cert.SubjectName)) > 0 then
        begin
        Geral.MB_Info(Cert.SubjectName);
        Geral.MB_Info(Cert.IssuerName);
        Geral.MB_Info(Cert.GetInfo(CAPICOM_CERT_INFO_SUBJECT_EMAIL_NAME));
        Geral.MB_Info(Geral.FDT(Cert.ValidToDate, 1));
        Geral.MB_Info(Cert.Thumbprint);


        //CertStoreMem.Add(Cert);
        //NumCertCarregado := Serial;
        //Cert.ExtendedProperties()
        OleCheck(IDispatch(Cert.ExtendedKeyUsage).QueryInterface(IExtendedKeyUsageDisp, ExtendedKeyUsageDisp));
        //OleCheck(IDispatch(ExtendedKeyUsageDisp.EKUs).QueryInterface(IEKUs, EKUS));
        //OleCheck(IDispatch(Cert.ExtendedKeyUsage).QueryInterface(IExtendedKeyUsage, ExtendedKeyUsage));
       // OleCheck(IDispatch(Cert.KeyUsage).QueryInterface(IKeyUsage, KeyUsage));
       // OleCheck(IDispatch(ExtendedKeyUsage.EKUs).QueryInterface(IEKUs, EKUs));
        //OleCheck(IDispatch(Cert.BasicConstraints).QueryInterface(IBasicConstraints, BasicConstraints));

        //ExtendedKeyUsage.GetTypeInfoCount()
        Geral.MB_Info(Geral.FF0(EKUS.Count));
(*
        for J := 0 to ExtendedProperties.Count - 1 do
        begin
          OleCheck(IDispatch(ExtendedProperties.).QueryInterface(IExtendedProperties, ExtendedProperties));
          Geral.MB_Info(ExtendedProperties.Item[J]);
        end;
*)

//        EKUs := ExtendedKeyUsage.EKUs;
        //EKU := String(EKUS.Item[0]);
        //OleCheck(IDispatch(EKUS.Item[0]).QueryInterface(IEKU, EKU));
        //Geral.MB_Info(KeyUsage.)

   //dsigKey := xmldsig.createKeyFromCSP(ExtendedKeyUsage.ProviderType, ExtendedKeyUsage.ProviderName, ExtendedKeyUsage.ContainerName, 0);

//        OID := Cert.ExtendedKeyUsage();
        end;
      end;
    end;

end;

function TNFeXMLGerencia.ChaveDeAcessoDesmontada(const Chave: String;
  const VersaoNFe: Double): String;
var
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  DesmontaChaveDeAcesso(Chave, VersaoNFe,
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
  Result :=  sLineBreak +
    'UF:           ' + UF_IBGE + sLineBreak +
    'AnoMes:       ' + AAMM    + sLineBreak +
    'CNPJ:         ' + CNPJ    + sLineBreak +
    'Modelo NF:    ' + ModNFe  + sLineBreak +
    'S�rie NF:     ' + SerNFe  + sLineBreak +
    'N�mero NF:    ' + NumNFe  + sLineBreak +
    'Tipo emiss�o: ' + tpEmis  + sLineBreak +
    'N� Rand�mico: ' + AleNFe  + sLineBreak;
end;

function TNFeXMLGerencia.DesmontaChaveDeAcesso(const Chave: String;
  const VersaoNFe: Double; var UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe,
  tpEmis, AleNFe: String): Boolean;
begin
  Result := VerificaChaveAcesso(Chave, False);
  if Result then
  begin
    UF_IBGE := Copy(Chave,  1,  2);
    AAMM    := Copy(Chave,  3,  4);
    CNPJ    := Copy(Chave,  7, 14);
    ModNFe  := Copy(Chave, 21,  2);
    SerNFe  := Copy(Chave, DEMONTA_CHV_INI_SerNFe,  DEMONTA_CHV_TAM_SerNFe);
    NumNFe  := Copy(Chave, DEMONTA_CHV_INI_NumNFe,  DEMONTA_CHV_TAM_NumNFe);
    if VersaoNFe < 2.00 then
    begin
      tpEmis  := '?';
      AleNFe  := Copy(Chave, 35,  9);
    end else begin
      tpEmis  := Copy(Chave, 35,  1);
      AleNFe  := Copy(Chave, 36,  8);
    end;
  end else
  begin
    UF_IBGE := '';
    AAMM    := '';
    CNPJ    := '';
    ModNFe  := '';
    SerNFe  := '';
    NumNFe  := '';
    tpEmis  := '';
    AleNFe  := '';
  end;
end;

function TNFeXMLGerencia.Evento_MontaId(tpEvento: Integer; Chave: String;
  nSeqEvento: Integer): String;
const
  TamStr = 54;
begin
//ID 110110 3511031029073900013955001000000001105112804108 01
  Result := 'ID' + Geral.FFN(tpEvento, 6) + Chave + Geral.FFn(nSeqEvento, 2);
  if Length(Result) <> TamStr then
    Geral.MensagemBox('Tamanho do Id difere de ' + Geral.FF0(TamStr) +
    '! - NFeXMLGerencia.Evento_MontaId', 'ERRO', MB_OK+MB_ICONERROR);
end;

function TNFeXMLGerencia.Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
  const EventoNFe: TEventosNFe; var tpEvento: Integer; var versao: Double;
  var descEvento: String): Boolean;
begin
  Result := True;
  case Integer(EventoNFe) of
    Integer(eveCCe):
    begin
      descEvento := 'Carta de Correcao';
      tpEvento := NFe_CodEventoCCe;
      versao := 1;
    end;
    Integer(eveCan):
    begin
      descEvento := 'Cancelamento';
      tpEvento := NFe_CodEventoCan;
      versao := 1;
    end;
    Integer(eveMDe):
    begin
      descEvento := Obtem_DeManifestacao_de_cSitConf_xEvento(EvePreDef);
      tpEvento := Obtem_DeManifestacao_de_cSitConf_tpEvento(EvePreDef);
      versao := 1;
    end;
    Integer(eveEPEC):
    begin
      descEvento := 'EPEC';
      tpEvento := NFe_CodEventoEPEC;
      versao := 1;
    end;
    else begin
      Result := False;
      tpEvento := 0;
      versao := 0;
      descEvento := 'Evento n�o definido em NFeXMLGerencia.Evento_Obtem_tpEvento';
      Geral.MensagemBox(descEvento, 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

{
function TNFeXMLGerencia.Evento_Obtem_tpEvento(Evento: TEventosNFe): Integer;
begin
end;
}

function TNFeXMLGerencia.NFG_SerieToide_serie(const Modelo: Integer;
  const NFG_Serie: String; var ide_Serie: Integer): Boolean;
var
  Txt, Msg: String;
begin
  // ver: C:\Projetos_Aux\_Manuais e Tutoriais\SINTEGRA\MT\6.2.8_Documentos_Fiscais_Serie_e_Sub_Series.pdf
  // e ver: Conv�nio 57/95 item 11.1.9
  ide_Serie := 0;
  Msg := '';
  if Modelo = 55 then
  begin
    Result := False;
    Txt := Geral.SoNumero_TT(Trim(NFG_Serie));
    if NFG_Serie = '' then
      Msg := 'NF-e deve ter s�rie numerada de 0 a 999!'
    else
    if Trim(NFG_Serie) <> Txt then
      Msg := 'NF-e deve ter apenas numeros de 0 a 999!';
    if Msg <> '' then
       Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING)
    else begin
      ide_Serie := Geral.IMV(Trim(NFG_Serie));
      Result := True;
    end;
  end else
    Result := True;
end;

{$IfDef VER320}
function TNFeXMLGerencia.ObtemCertificado_Arquivo((*out Cert: ICertificate2*)): Boolean;
var
  //Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  //
  Path_Para_o_PFX, Password_para_o_PFX: String;
  Data: Pointer;
begin
  Result := False;
  if VAR_CERTIFICADO_DIGITAL = nil then
  begin
    Cert   := nil;
    Path_Para_o_PFX     := VAR_CertDigPfxCam;
    Password_para_o_PFX := VAR_CertDigPfxPwd;
    try
      //'C:\_Sincro\_MLArend\Clientes\Tisolin\Compras XML\BACKUP_CERT_TISOLIN LOCACOES_SENHA 22121962.pfx';
      //Password_para_o_PFX := '22121962';
      //
      //Loading a certificate from a .P12 file.
      //Cert := CreateComObject(CLASS_Certificate) as ICertificate2;
      VAR_CERTIFICADO_DIGITAL := CreateComObject(CLASS_Certificate) as ICertificate2;
      //Cert.Load(Path_Para_o_PFX, Password_para_o_PFX, CAPICOM_KEY_STORAGE_DEFAULT, CAPICOM_CURRENT_USER_KEY);
      VAR_CERTIFICADO_DIGITAL.Load(Path_Para_o_PFX, Password_para_o_PFX, CAPICOM_KEY_STORAGE_DEFAULT, CAPICOM_CURRENT_USER_KEY);
      //CertContext := Cert as ICertContext;
      CertContext := VAR_CERTIFICADO_DIGITAL as ICertContext;
      CertContext.Get_CertContext( Integer( PCertContext ) );
      // Pone el certificado para la comunicaci�n SSL
  (*
      if InternetSetOption( Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT,
      PCertContext, Sizeof( CERT_CONTEXT ) ) = False then
      begin
        //Cert   := nil;
        Geral.MB_Erro('N�o foi poss�vel carregar o certificado digital');
      end else
      begin
        Result := True;
        //VAR_CERTIFICADO_DIGITAL := Cert;
      end;
   *)
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro(
        'N�o foi poss�vel carregar o arquivo:"' + Path_Para_o_PFX + '"' +
        sLineBreak + 'Erro: "' + E.Message +  '"');
      end;
    end;
  end else
    Result := True;
end;
{$Else}
function TNFeXMLGerencia.ObtemCertificado_Arquivo((*out Cert: ICertificate2*)): Boolean;
var
  //Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  //
  Path_Para_o_PFX, Password_para_o_PFX: String;
  Data: Pointer;
begin
  Result := False;
  if VAR_CERTIFICADO_DIGITAL = nil then
  begin
    Cert   := nil;
    Path_Para_o_PFX     := VAR_CertDigPfxCam;
    Password_para_o_PFX := VAR_CertDigPfxPwd;
    try
      //'C:\_Sincro\_MLArend\Clientes\Tisolin\Compras XML\BACKUP_CERT_TISOLIN LOCACOES_SENHA 22121962.pfx';
      //Password_para_o_PFX := '22121962';
      //
      //Loading a certificate from a .P12 file.
      //Cert := CreateComObject(CLASS_Certificate) as ICertificate2;
      VAR_CERTIFICADO_DIGITAL := CreateComObject(CLASS_Certificate) as ICertificate2;
      //Cert.Load(Path_Para_o_PFX, Password_para_o_PFX, CAPICOM_KEY_STORAGE_DEFAULT, CAPICOM_CURRENT_USER_KEY);
      VAR_CERTIFICADO_DIGITAL.Load(Path_Para_o_PFX, Password_para_o_PFX, CAPICOM_KEY_STORAGE_DEFAULT, CAPICOM_CURRENT_USER_KEY);
      //CertContext := Cert as ICertContext;
      CertContext := VAR_CERTIFICADO_DIGITAL as ICertContext;
      CertContext.Get_CertContext( Integer( PCertContext ) );
      // Pone el certificado para la comunicaci�n SSL
  (*
      if InternetSetOption( Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT,
      PCertContext, Sizeof( CERT_CONTEXT ) ) = False then
      begin
        //Cert   := nil;
        Geral.MB_Erro('N�o foi poss�vel carregar o certificado digital');
      end else
      begin
        Result := True;
        //VAR_CERTIFICADO_DIGITAL := Cert;
      end;
   *)
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro(
        'N�o foi poss�vel carregar o arquivo:"' + Path_Para_o_PFX + '"' +
        sLineBreak + 'Erro: "' + E.Message +  '"');
      end;
    end;
  end else
    Result := True;
end;
{$EndIf}

function TNFeXMLGerencia.ObtemCertificado_NumSerie(const NumeroSerial: String;
  out Cert: ICertificate2): Boolean;
var
  Store        : IStore;
  Certs        : ICertificates;
  //CertContext  : ICertContext;
  i : Integer;
  //sXML: AnsiString;
begin
  Result := False;
  Cert   := nil;
  Store  := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open(CAPICOM_CURRENT_USER_STORE, 'MY', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     //Cria objeto para acesso a leitura do certificado
     Cert := IInterface(Certs.Item[i + 1]) as ICertificate2;
     if UpperCase(Cert.SerialNumber) = Uppercase(NumeroSerial) then
     begin
       //se o n�mero do serial for igual ao que queremos utilizar
       Result := True;
       Exit;
     end;
     i := i + 1;
  end;
  //
  if Result = False then
  begin
    Cert := nil;
    ShowMessage('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!');
    {
    Geral.MensagemBox('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!'),
      'Aviso', MB_OK+MB_ICONWARNING);
    }
  end;
  //
end;

function TNFeXMLGerencia.ObtemCNPJDeChaveDeAcesso(chNFe: String): String;
var
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  DesmontaChaveDeAcesso(chNFe, 2.00,
        UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
  Result := CNPJ;
end;

procedure TNFeXMLGerencia.ObtemCNPJeModeloNFeDeChaveDeAcesso(const chNFe: String;
  var CNPJ_CPF: String; var ModeloNFe: Integer);
var
  UF_IBGE, AAMM, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
  DesmontaChaveDeAcesso(chNFe, 2.00,
        UF_IBGE, AAMM, CNPJ_CPF, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
  ModeloNFe := Geral.IMV(ModNFe);
end;

function TNFeXMLGerencia.ObtemCertificado(const NumeroSerial: String;
  out Cert: ICertificate2): Boolean;
begin
{
  if VAR_CertDigPfxWay = 0 then
    Result := ObtemCertificado_NumSerie(NumeroSerial, Cert)
  else
  begin
    Result := ObtemCertificado_Arquivo();
    Cert := VAR_CERTIFICADO_DIGITAL;
  end;
}
  case VAR_CertDigPfxWay of
    0: // Numero serial pelo windows
    begin
      Result := ObtemCertificado_NumSerie(NumeroSerial, Cert);
    end;
    1: // do arquivo PFX
    begin
      Result := ObtemCertificado_Arquivo();
      Cert := VAR_CERTIFICADO_DIGITAL;
    end;
    2: // definido no cumputador local!
    begin
      case VAR_CertDigPfxLoc of
        0: // Numero serial pelo windows
        begin
          Result := ObtemCertificado_NumSerie(NumeroSerial, Cert);
        end;
        1: // do arquivo PFX
        begin
          Result := ObtemCertificado_Arquivo();
          Cert := VAR_CERTIFICADO_DIGITAL;
        end;
        else
        begin
          Cert := nil;
          Geral.MB_Aviso('Certificado digital n�o definido no computador local!')
        end;
      end;
    end;
    else
    begin
      Cert := nil;
      Geral.MB_Aviso('Certificado digital n�o definido nos par�metros da filial!')
    end;
  end;
end;

function TNFeXMLGerencia.Obtem_cSitNFe_De_cStat(cStat: Integer): Integer;
begin
  Result := -1;
  case cStat of
    0: Result := 0;
    100: Result := 1; // Autorizada
    110: Result := 2; // Denegada
    101: Result := 3; // Cancelada
    else
    begin
      Geral.MB_Erro(
      '"cStat" n�o implementado na fun��o "NFeXMLGerencia.Obtem_cSitNFe_De_cStat()"');
    end;
  end;
end;

function TNFeXMLGerencia.Obtem_cStat_De_cSitNFe(cSitNFe: Integer): Integer;
begin
  Result := -1;
  case cSitNFe of
    0: Result := 0;
    1: Result := 100; // Autorizada
    2: Result := 110; // Denegada
    3: Result := 101; // Cancelada
    else
    begin
      Geral.MB_Erro(
      '"cSitNFe" n�o implementado na fun��o "NFeXMLGerencia.Obtem_cStat_De_cSitNFe()"');
    end;
  end;
end;

function TNFeXMLGerencia.Obtem_DeEvento_Nome(Evento: Integer): String;
begin
  case Evento of
    NFe_CodEventoCCe (*= 110110*) : Result := 'Carta de Corre��o';
    NFe_CodEventoCan (*= 110111*) : Result := 'Cancelamento';
    NFe_CodEventoEPEC (*= 110140*) : Result := 'EPEC';
    //
    NFe_CodEventoMDeNaoConsultada (*= 0*)      : Result := 'Sem evento definido';
    NFe_CodEventoMDeConfirmacao   (*= 210200*) : Result := 'Opera��o confirmada';
    NFe_CodEventoMDeCiencia       (*= 210210*) : Result := 'Ciente da opera��o ';
    NFe_CodEventoMDeDesconhece    (*= 210220*) : Result := 'Desconhece a opera��o';
    NFe_CodEventoMDeNaoRealizou   (*= 210240*) : Result := 'Opera��o n�o realizada';
    else  Result := '***Fale com a DERMATEK - Nome de evento n�o implementado***';
  end;
end;

function TNFeXMLGerencia.Obtem_DeManifestacao_de_cSitConf_tpEvento(
  cSitConf: Integer): Integer;
begin
  case cSitConf of
    NFe_CodEventoSitConfNaoConsultada      : Result := NFe_CodEventoMDeNaoConsultada;
    NFe_CodEventoSitConfMDeConfirmada      : Result := NFe_CodEventoMDeConfirmacao;
    NFe_CodEventoSitConfMDeDesconhecida    : Result := NFe_CodEventoMDeDesconhece;
    NFe_CodEventoSitConfMDeNaoRealizada    : Result := NFe_CodEventoMDeNaoRealizou;
    NFe_CodEventoSitConfMDeCiencia         : Result := NFe_CodEventoMDeCiencia;
    else                                     Result := 0;
    //NFe_CodEventoSitConfMDeSemManifestacao :
  end;
end;

function TNFeXMLGerencia.Obtem_DeManifestacao_de_cSitConf_xEvento(
  cSitConf: Integer): String;
begin
  case cSitConf of
    NFe_CodEventoSitConfNaoConsultada      : Result := '** Situa��o n�o verificada **';
    NFe_CodEventoSitConfMDeSemManifestacao : Result := 'Sem manifesta��o do destinat�rio';
    NFe_CodEventoSitConfMDeConfirmada      : Result := 'Confirmacao da Operacao';
    NFe_CodEventoSitConfMDeDesconhecida    : Result := 'Desconhecimento da Operacao';
    NFe_CodEventoSitConfMDeNaoRealizada    : Result := 'Operacao nao Realizada';
    NFe_CodEventoSitConfMDeCiencia         : Result := 'Ciencia da Operacao';
    else                                     Result := '? ? ? ?';
  end;
end;

function TNFeXMLGerencia.Obtem_DeManifestacao_de_cSitNFe_xSituacao(
  cSitNFe: Integer): String;
begin
  case cSitNFe of
    NFe_CodEventoSitNFeNaoConsultada      : Result := '** NF-e n�o consultada **';
    NFe_CodEventoSitNFeMDeAutorizada      : Result := 'Autorizada';
    NFe_CodEventoSitNFeMDeDenegada        : Result := 'Denegada';
    NFe_CodEventoSitNFeMDeCancelada       : Result := 'Cancelada';
    else                                    Result := '? ? ? ?';
    //NFe_CodEventoSitConfMDeSemManifestacao :
  end;
end;

function TNFeXMLGerencia.Obtem_DeManifestacao_de_tpEvento_cSitConf(
  tpEvento: Integer): Integer;
begin
  case tpEvento of
    NFe_CodEventoMDeNaoConsultada : Result := NFe_CodEventoSitConfNaoConsultada;
    NFe_CodEventoMDeConfirmacao   : Result := NFe_CodEventoSitConfMDeConfirmada;
    NFe_CodEventoMDeDesconhece    : Result := NFe_CodEventoSitConfMDeDesconhecida;
    NFe_CodEventoMDeNaoRealizou   : Result := NFe_CodEventoSitConfMDeNaoRealizada;
    NFe_CodEventoMDeCiencia       : Result := NFe_CodEventoSitConfMDeCiencia;
    //NFe_CodEventoSitConfNaoConsultada:
    else                                      Result := -1; //
  end;
end;

function TNFeXMLGerencia.PosEx(const SubStr, S: AnsiString;
  Offset: Cardinal): Integer;
var
  I,X: Integer;
  Len, LenSubStr: Integer;
begin
  if Offset = 1 then
    Result := Pos(SubStr, S)
  else
  begin
    I := Offset;
    LenSubStr := Length(SubStr);
    Len := Length(S) - LenSubStr + 1;
    while I <= Len do
    begin
      if S[I] = SubStr[1] then
      begin
        X := 1;
        while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do
          Inc(X);
        if (X = LenSubStr) then
        begin
          Result := I;
          exit;
        end;
      end;
      Inc(I);
    end;
    Result := 0;
  end;
end;

function TNFeXMLGerencia.PosLast(const SubStr, S: AnsiString): Integer;
var
  P : Integer;
begin
  Result := 0;
  P := Pos( SubStr, S);
  while P <> 0 do
  begin
     Result := P;
     P := PosEx( SubStr, S, P+1);
  end;
end;

function TNFeXMLGerencia.SalvaXML(Arquivo: String; XML: String): Boolean;
var
  TxtFile: TextFile;
begin
//  Result := False;
  AssignFile(TxtFile, Arquivo);
  Rewrite(TxtFile);
  Write(TxtFile, XML);
  CloseFile(TxtFile);
  //
  Result := True;
end;

function TNFeXMLGerencia.TextoAmbiente(Ambiente: Byte): String;
begin
  case Ambiente of
    1: Result := 'Produ��o';
    2: Result := 'Homologa��o';
    else Result := '[Desconhecido]';
  end;
end;

function TNFeXMLGerencia.Texto_StatusNFe(Status: Integer; VersaoNFe: Double): String;
var
  Versao: Integer;
begin
  Versao := Trunc(VersaoNFe * 100 + 0.5);
  if Versao = 0 then
    Versao := Trunc(NFE_EMI_VERSAO_MAX_USO * 100 + 0.5);
  case Versao of
    {
    110:
    case Status of
      0  : Result := '';
      //
      10 : Result := 'Registros da NFe inclu�dos na Base da Dados';
      20 : Result := 'XML da NFe foi gerada';
      30 : Result := 'XML da NFe est� assinado';
      40 : Result := 'Lote de NFe(s) rejeitado';
      50 : Result := 'NFe adicionada ao lote';
      60 : Result := 'Lote ao qual a NFe pertence foi enviado';
      70 : Result := 'Lote ao qual a NFe pertence em fase de consulta';
      //
      100: Result := 'Autorizado o uso da NF-e';
      101: Result := 'Cancelamento de NF-e homologado';
      102: Result := 'Inutiliza��o de n�mero homologado';
      103: Result := 'Lote recebido com sucesso';
      104: Result := 'Lote processado';
      105: Result := 'Lote em processamento';
      106: Result := 'Lote n�o localizado';
      107: Result := 'Servi�o em Opera��o';
      108: Result := 'Servi�o Paralisado Momentaneamente (curto prazo)';
      109: Result := 'Servi�o Paralisado sem Previs�o';
      110: Result := 'Uso Denegado';
      111: Result := 'Consulta cadastro com uma ocorr�ncia';
      112: Result := 'Consulta cadastro com mais de uma ocorr�ncia';
      201: Result := 'Rejei��o: O numero m�ximo de numera��o de NF-e a inutilizar ultrapassou o limite';
      202: Result := 'Rejei��o: Falha no reconhecimento da autoria ou integridade do arquivo digital';
      203: Result := 'Rejei��o: Emissor n�o habilitado para emiss�o da NF-e';
      204: Result := 'Rejei��o: Duplicidade de NF-e';
      205: Result := 'Rejei��o: NF-e est� denegada na base de dados da SEFAZ';
      206: Result := 'Rejei��o: NF-e j� est� inutilizada na Base de dados da SEFAZ';
      207: Result := 'Rejei��o: CNPJ do emitente inv�lido';
      208: Result := 'Rejei��o: CNPJ do destinat�rio inv�lido';
      209: Result := 'Rejei��o: IE do emitente inv�lida ';
      210: Result := 'Rejei��o: IE do destinat�rio inv�lida';
      211: Result := 'Rejei��o: IE do substituto inv�lida';
      212: Result := 'Rejei��o: Data de emiss�o NF-e posterior a data de recebimento';
      213: Result := 'Rejei��o: CNPJ-Base do Emitente difere do CNPJ-Base do Certificado Digital';
      214: Result := 'Rejei��o: Tamanho da mensagem excedeu o limite estabelecido';
      215: Result := 'Rejei��o: Falha no schema XML';
      216: Result := 'Rejei��o: Chave de Acesso difere da cadastrada';
      217: Result := 'Rejei��o: NF-e n�o consta na base de dados da SEFAZ';
      218: Result := 'Rejei��o: NF-e  j� esta cancelada na base de dados da SEFAZ';
      219: Result := 'Rejei��o: Circula��o da NF-e verificada';
      220: Result := 'Rejei��o: NF-e autorizada h� mais de 7 dias (168 horas)';
      221: Result := 'Rejei��o: Confirmado o recebimento da NF-e pelo destinat�rio';
      222: Result := 'Rejei��o: Protocolo de Autoriza��o de Uso difere do cadastrado';
      223: Result := 'Rejei��o: CNPJ do transmissor do lote difere do CNPJ do transmissor da consulta';
      224: Result := 'Rejei��o: A faixa inicial � maior que a faixa final';
      225: Result := 'Rejei��o: Falha no Schema XML da NFe';
      226: Result := 'Rejei��o: C�digo da UF do Emitente diverge da UF autorizadora';
      227: Result := 'Rejei��o: Erro na Chave de Acesso - Campo ID';
      228: Result := 'Rejei��o: Data de Emiss�o muito atrasada';
      229: Result := 'Rejei��o: IE do emitente n�o informada';
      230: Result := 'Rejei��o: IE do emitente n�o cadastrada';
      231: Result := 'Rejei��o: IE do emitente n�o vinculada ao CNPJ';
      232: Result := 'Rejei��o: IE do destinat�rio n�o informada';
      233: Result := 'Rejei��o: IE do destinat�rio n�o cadastrada';
      234: Result := 'Rejei��o: IE do destinat�rio n�o vinculada ao CNPJ';
      235: Result := 'Rejei��o: Inscri��o SUFRAMA inv�lida';
      236: Result := 'Rejei��o: Chave de Acesso com d�gito verificador inv�lido';
      237: Result := 'Rejei��o: CPF do destinat�rio inv�lido';
      238: Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML superior a Vers�o vigente';
      239: Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML n�o suportada';
      240: Result := 'Rejei��o: Cancelamento/Inutiliza��o - Irregularidade Fiscal do Emitente';
      241: Result := 'Rejei��o: Um n�mero da faixa j� foi utilizado';
      242: Result := 'Rejei��o: Cabe�alho - Falha no Schema XML';
      243: Result := 'Rejei��o: XML Mal Formado';
      244: Result := 'Rejei��o: CNPJ do Certificado Digital difere do CNPJ da Matriz e do CNPJ do Emitente';
      245: Result := 'Rejei��o: CNPJ Emitente n�o cadastrado';
      246: Result := 'Rejei��o: CNPJ Destinat�rio n�o cadastrado';
      247: Result := 'Rejei��o: Sigla da UF do Emitente diverge da UF autorizadora';
      248: Result := 'Rejei��o: UF do Recibo diverge da UF autorizadora';
      249: Result := 'Rejei��o: UF da Chave de Acesso diverge da UF autorizadora';
      250: Result := 'Rejei��o: UF diverge da UF autorizadora';
      251: Result := 'Rejei��o: UF/Munic�pio destinat�rio n�o pertence a SUFRAMA';
      252: Result := 'Rejei��o: Ambiente informado diverge do Ambiente de recebimento';
      253: Result := 'Rejei��o: Digito Verificador da chave de acesso composta inv�lida';
      254: Result := 'Rejei��o: NF-e complementar n�o possui NF referenciada';
      255: Result := 'Rejei��o: NF-e complementar possui mais de uma NF referenciada';
      256: Result := 'Rejei��o: Uma NF-e da faixa j� est� inutilizada na Base de dados da SEFAZ';
      257: Result := 'Rejei��o: Solicitante n�o habilitado para emiss�o da NF-e';
      258: Result := 'Rejei��o: CNPJ da consulta inv�lido';
      259: Result := 'Rejei��o: CNPJ da consulta n�o cadastrado como contribuinte na UF';
      260: Result := 'Rejei��o: IE da consulta inv�lida';
      261: Result := 'Rejei��o: IE da consulta n�o cadastrada como contribuinte na UF';
      262: Result := 'Rejei��o: UF n�o fornece consulta por CPF';
      263: Result := 'Rejei��o: CPF da consulta inv�lido';
      264: Result := 'Rejei��o: CPF da consulta n�o cadastrado como contribuinte na UF';
      265: Result := 'Rejei��o: Sigla da UF da consulta difere da UF do Web Service';
      266: Result := 'Rejei��o: S�rie utilizada n�o permitida no Web Service';
      267: Result := 'Rejei��o: NF Complementar referencia uma NF-e inexistente';
      268: Result := 'Rejei��o: NF Complementar referencia uma outra NF-e Complementar';
      269: Result := 'Rejei��o: CNPJ Emitente da NF Complementar difere do CNPJ da NF Referenciada';
      270: Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: d�gito inv�lido';
      271: Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: difere da UF do emitente';
      272: Result := 'Rejei��o: C�digo Munic�pio do Emitente: d�gito inv�lido';
      273: Result := 'Rejei��o: C�digo Munic�pio do Emitente: difere da UF do emitente';
      274: Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: d�gito inv�lido';
      275: Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: difere da UF do Destinat�rio';
      276: Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: d�gito inv�lido';
      277: Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: difere da UF do Local de Retirada';
      278: Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega: d�gito inv�lido';
      279: Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega:  difere da UF do Local de Entrega';
      280: Result := 'Rejei��o: Certificado Transmissor inv�lido';
      281: Result := 'Rejei��o: Certificado Transmissor Data Validade';
      282: Result := 'Rejei��o: Certificado Transmissor sem CNPJ';
      283: Result := 'Rejei��o: Certificado Transmissor - erro Cadeia de Certifica��o';
      284: Result := 'Rejei��o: Certificado Transmissor revogado';
      285: Result := 'Rejei��o: Certificado Transmissor difere ICP-Brasil';
      286: Result := 'Rejei��o: Certificado Transmissor erro no acesso a LCR';
      287: Result := 'Rejei��o: C�digo Munic�pio do FG - ISSQN: d�gito inv�lido';
      288: Result := 'Rejei��o: C�digo Munic�pio do FG - Transporte: d�gito inv�lido';
      289: Result := 'Rejei��o: C�digo da UF informada diverge da UF solicitada';
      290: Result := 'Rejei��o: Certificado Assinatura inv�lido';
      291: Result := 'Rejei��o: Certificado Assinatura Data Validade';
      292: Result := 'Rejei��o: Certificado Assinatura sem CNPJ';
      293: Result := 'Rejei��o: Certificado Assinatura - erro Cadeia de Certifica��o';
      294: Result := 'Rejei��o: Certificado Assinatura revogado';
      295: Result := 'Rejei��o: Certificado Assinatura difere ICP-Brasil';
      296: Result := 'Rejei��o: Certificado Assinatura erro no acesso a LCR';
      297: Result := 'Rejei��o: Assinatura difere do calculado';
      298: Result := 'Rejei��o: Assinatura difere do padr�o do Projeto';
      299: Result := 'Rejei��o: XML da �rea de cabe�alho com codifica��o diferente de UTF-8';

      301: Result := 'Uso Denegado : Irregularidade fiscal do emitente';
      302: Result := 'Uso Denegado : Irregularidade fiscal do destinat�rio';

      401: Result := 'Rejei��o: CPF do remetente inv�lido';
      402: Result := 'Rejei��o: XML da �rea de dados com codifica��o diferente de UTF-8';
      403: Result := 'Rejei��o: O grupo de informa��es da NF-e avulsa � de uso exclusivo do Fisco';
      404: Result := 'Rejei��o: Uso de prefixo de namespace n�o permitido';
      405: Result := 'Rejei��o: C�digo do pa�s do emitente: d�gito inv�lido';
      406: Result := 'Rejei��o: C�digo do pa�s do destinat�rio: d�gito inv�lido';
      407: Result := 'Rejei��o: O CPF s� pode ser informado no campo emitente para a NF-e avulsa';


      478: Result := 'Rejei��o: Local da entrega n�o informado para faturamento direto de ve�culos novos';

      999: Result := 'Rejei��o: Erro n�o catalogado (informar a mensagem de erro capturado no tratamento da exce��o)';
      else Result := 'Status n�o definido! Informe a DERMATEK';
    end;
    }
    200, 310:
    case Status of
      0  : Result := '';
      //
      10 : Result := 'Registros da NFe inclu�dos na Base da Dados';
      20 : Result := 'XML da NFe foi gerada';
      30 : Result := 'XML da NFe est� assinado';
      40 : Result := 'Lote de NFe(s) rejeitado';
      50 : Result := 'NFe adicionada ao lote';
      60 : Result := 'Lote ao qual a NFe pertence foi enviado';
      70 : Result := 'Lote ao qual a NFe pertence em fase de consulta';
      //
      100: Result := 'Autorizado o uso da NF-e';
      101: Result := 'Cancelamento de NF-e homologado';
      102: Result := 'Inutiliza��o de n�mero homologado';
      103: Result := 'Lote recebido com sucesso';
      104: Result := 'Lote processado';
      105: Result := 'Lote em processamento';
      106: Result := 'Lote n�o localizado';
      107: Result := 'Servi�o em Opera��o';
      108: Result := 'Servi�o Paralisado Momentaneamente (curto prazo)';
      109: Result := 'Servi�o Paralisado sem Previs�o';
      110: Result := 'Uso Denegado';
      111: Result := 'Consulta cadastro com uma ocorr�ncia';
      112: Result := 'Consulta cadastro com mais de uma ocorr�ncia';

      126: Result := 'Rejei��o: Enviado lote com mais de 1 NFC-e';

      128: Result := 'Lote de Evento Processado';
      //129: Result := 'Lote de Evento Processado'; ???

      135: Result := 'Evento registrado e vinculado a NF-e';
      136: Result := 'Evento registrado, mas n�o vinculado a NF-e';
      137: Result := 'Nenhum documento localizado para o Destinat�rio';
      138: Result := 'Documento localizado para o Destinat�rio';
      139: Result := 'Pedido de Download processado';
      140: Result := 'Download disponibilizado';

      142: Result := 'Rejei��o: Ambiente de Conting�ncia EPEC bloqueado para o Emitente';
      //
      /// C�DIGO MOTIVOS DE N�O ATENDIMENTO DA SOLICITA��O

      201: Result := 'Rejei��o: O numero m�ximo de numera��o de NF-e a inutilizar ultrapassou o limite';
      202: Result := 'Rejei��o: Falha no reconhecimento da autoria ou integridade do arquivo digital';
      203: Result := 'Rejei��o: Emissor n�o habilitado para emiss�o da NF-e';
      204: Result := 'Rejei��o: Duplicidade de NF-e';
      205: Result := 'Rejei��o: NF-e est� denegada na base de dados da SEFAZ';
      206: Result := 'Rejei��o: NF-e j� est� inutilizada na Base de dados da SEFAZ';
      207: Result := 'Rejei��o: CNPJ do emitente inv�lido';
      208: Result := 'Rejei��o: CNPJ do destinat�rio inv�lido';
      209: Result := 'Rejei��o: IE do emitente inv�lida';
      210: Result := 'Rejei��o: IE do destinat�rio inv�lida';
      211: Result := 'Rejei��o: IE do substituto inv�lida';
      212: Result := 'Rejei��o: Data de emiss�o NF-e posterior a data de recebimento';
      213: Result := 'Rejei��o: CNPJ-Base do Emitente difere do CNPJ-Base do Certificado Digital';
      214: Result := 'Rejei��o: Tamanho da mensagem excedeu o limite estabelecido';
      215: Result := 'Rejei��o: Falha no schema XML';
      216: Result := 'Rejei��o: Chave de Acesso difere da cadastrada';
      217: Result := 'Rejei��o: NF-e n�o consta na base de dados da SEFAZ';
      218: Result := 'Rejei��o: NF-e j� esta cancelada na base de dados da SEFAZ';
      219: Result := 'Rejei��o: Circula��o da NF-e verificada';
      220: Result := 'Rejei��o: NF-e autorizada h� mais de 7 dias (168 horas)';
      221: Result := 'Rejei��o: Confirmado o recebimento da NF-e pelo destinat�rio';
      222: Result := 'Rejei��o: Protocolo de Autoriza��o de Uso difere do cadastrado';
      //223: Result := 'Rejei��o: CNPJ do transmissor do lote difere do CNPJ do transmissor da consulta';
      223: Result := 'Rejei��o: CNPJ/CPF do transmissor do lote difere do CNPJ/CPF do transmissor da consulta';
      224: Result := 'Rejei��o: A faixa inicial � maior que a faixa final';
      225: Result := 'Rejei��o: Falha no Schema XML do lote de NFe';
      226: Result := 'Rejei��o: C�digo da UF do Emitente diverge da UF autorizadora';
      227: Result := 'Rejei��o: CPF do Emitente difere do CPF do Certificado Digital';
      228: Result := 'Rejei��o: Data de Emiss�o muito atrasada';
      229: Result := 'Rejei��o: IE do emitente n�o informada';
      230: Result := 'Rejei��o: IE do emitente n�o cadastrada';
      231: Result := 'Rejei��o: IE do emitente n�o vinculada ao CNPJ';
      232: Result := 'Rejei��o: IE do destinat�rio n�o informada';
      233: Result := 'Rejei��o: IE do destinat�rio n�o cadastrada';
      234: Result := 'Rejei��o: IE do destinat�rio n�o vinculada ao CNPJ';
      235: Result := 'Rejei��o: Inscri��o SUFRAMA inv�lida';
      236: Result := 'Rejei��o: Chave de Acesso com d�gito verificador inv�lido';
      237: Result := 'Rejei��o: CPF do destinat�rio inv�lido';
      238: Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML superior a Vers�o vigente';
      239: Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML n�o suportada';
      240: Result := 'Rejei��o: Irregularidade Fiscal do Emitente';
      241: Result := 'Rejei��o: Um n�mero da faixa j� foi utilizado';
      242: Result := 'Rejei��o: Mensagem SOAP inv�lida';
      243: Result := 'Rejei��o: XML Mal Formado';
      //244: Result := 'Rejei��o: CNPJ do Certificado Digital difere do CNPJ da Matriz e do CNPJ do Emitente';
      244: Result := 'Rejei��o: Processo de Emiss�o pelo Contribuinte incompat�vel com a S�rie da NF';
      245: Result := 'Rejei��o: CNPJ Emitente n�o cadastrado';
      246: Result := 'Rejei��o: CNPJ Destinat�rio n�o cadastrado';
      247: Result := 'Rejei��o: Sigla da UF do Emitente diverge da UF autorizadora';
      248: Result := 'Rejei��o: UF do Recibo diverge da UF autorizadora';
      249: Result := 'Rejei��o: UF da Chave de Acesso diverge da UF autorizadora';
      250: Result := 'Rejei��o: UF diverge da UF autorizadora';
      251: Result := 'Rejei��o: UF/Munic�pio destinat�rio n�o pertence a SUFRAMA';
      252: Result := 'Rejei��o: Ambiente informado diverge do Ambiente de recebimento';
      253: Result := 'Rejei��o: Digito Verificador da chave de acesso composta inv�lida';
      254: Result := 'Rejei��o: NF-e complementar n�o possui NF referenciada';
      255: Result := 'Rejei��o: NF-e complementar possui mais de uma NF referenciada';
      256: Result := 'Rejei��o: Uma NF-e da faixa j� est� inutilizada na Base de dados da SEFAZ';
      257: Result := 'Rejei��o: Solicitante n�o habilitado para emiss�o da NF-e';
      258: Result := 'Rejei��o: CNPJ da consulta inv�lido';
      259: Result := 'Rejei��o: CNPJ da consulta n�o cadastrado como contribuinte na UF';
      260: Result := 'Rejei��o: IE da consulta inv�lida';
      261: Result := 'Rejei��o: IE da consulta n�o cadastrada como contribuinte na UF';
      262: Result := 'Rejei��o: UF n�o fornece consulta por CPF';
      263: Result := 'Rejei��o: CPF da consulta inv�lido';
      264: Result := 'Rejei��o: CPF da consulta n�o cadastrado como contribuinte na UF';
      265: Result := 'Rejei��o: Sigla da UF da consulta difere da UF do Web Service';
      266: Result := 'Rejei��o: S�rie utilizada n�o permitida no Web Service';
      267: Result := 'Rejei��o: NF Complementar referencia uma NF-e inexistente';
      268: Result := 'Rejei��o: NF Complementar referencia uma outra NF-e Complementar';
      //269: Result := 'Rejei��o: CNPJ Emitente da NF Complementar difere do CNPJ da NF Referenciada';
      269: Result := 'Rejei��o: CNPJ/CPF Emitente da NF Complementar difere do CNPJ/CPF da NF Referenciada';
      270: Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: d�gito inv�lido';
      271: Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: difere da UF do emitente';
      272: Result := 'Rejei��o: C�digo Munic�pio do Emitente: d�gito inv�lido';
      273: Result := 'Rejei��o: C�digo Munic�pio do Emitente: difere da UF do emitente';
      274: Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: d�gito inv�lido';
      275: Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: difere da UF do Destinat�rio';
      276: Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: d�gito inv�lido';
      277: Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: difere da UF do Local de Retirada';
      278: Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega: d�gito inv�lido';
      279: Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega: difere da UF do Local de Entrega';
      280: Result := 'Rejei��o: Certificado Transmissor inv�lido';
      281: Result := 'Rejei��o: Certificado Transmissor Data Validade';
      282: Result := 'Rejei��o: Certificado Transmissor sem CNPJ/CPF';
      283: Result := 'Rejei��o: Certificado Transmissor - erro Cadeia de Certifica��o';
      284: Result := 'Rejei��o: Certificado Transmissor revogado';
      285: Result := 'Rejei��o: Certificado Transmissor difere ICP-Brasil';
      286: Result := 'Rejei��o: Certificado Transmissor erro no acesso a LCR';
      287: Result := 'Rejei��o: C�digo Munic�pio do FG - ISSQN: d�gito inv�lido';
      288: Result := 'Rejei��o: C�digo Munic�pio do FG - Transporte: d�gito inv�lido';
      289: Result := 'Rejei��o: C�digo da UF informada diverge da UF solicitada';
      290: Result := 'Rejei��o: Certificado Assinatura inv�lido';
      291: Result := 'Rejei��o: Certificado Assinatura Data Validade';
      //292: Result := 'Rejei��o: Certificado Assinatura sem CNPJ';
      292: Result := 'Rejei��o: Certificado Assinatura sem CNPJ/CPF';
      293: Result := 'Rejei��o: Certificado Assinatura - erro Cadeia de Certifica��o';
      294: Result := 'Rejei��o: Certificado Assinatura revogado';
      295: Result := 'Rejei��o: Certificado Assinatura difere ICP-Brasil';
      296: Result := 'Rejei��o: Certificado Assinatura erro no acesso a LCR';
      297: Result := 'Rejei��o: Assinatura difere do calculado';
      298: Result := 'Rejei��o: Assinatura difere do padr�o do Projeto';
      299: Result := 'Rejei��o: XML da �rea de cabe�alho com codifica��o diferente de UTF-8';
      /// C�DIGO MOTIVOS DE DENEGA��O DE USO
      301: Result := 'Uso Denegado: Irregularidade fiscal do emitente';
      302: Result := 'Uso Denegado: Irregularidade fiscal do destinat�rio';
      303: Result := 'Uso Denegado: Destinat�rio n�o habilitado a operar na UF';

      305: Result := 'Rejei��o: Destinat�rio bloqueado na UF';
      306: Result := 'Rejei��o: IE do destinat�rio n�o est� ativa na UF';
















      321: Result := 'REJEI��O: NF-E DE DEVOLU��O N�O POSSUI CONHECIMENTO FISCAL REFERENCIADO';
      322: Result := 'REJEI��O: NF-E DE DEVOLU��O COM MAIS DE UM DOCUMENTO FISCAL REFERENCIADO';
      323: Result := 'REJEI��O: CNPJ AUTORIZADO PARA DOWNLOAD INV�LIDO';
      324: Result := 'REJEI��O: CNPJ DO DESTINAT�RIO JA AUTORIZADO PARA DOWNLOAD';
      325: Result := 'REJEI��O: CPF AUTORIZADO PARA DOWNLOAD INV�LIDO';
      326: Result := 'REJEI��O: CPF DO DESTINAT�RIO AUTORIZADO PARA DOWNLOAD';
      327: Result := 'REJEI��O: CFOP INV�LIDO PARA NF-E COM FINALIDADE DE DEVOLU��O';
      328: Result := 'REJEI��O: CFOP INV�LIDO PARA NF-E QUE N�O TEM FINALIDADE DE DEVOLU��O';
      329: Result := 'REJEI��O: N�MERO DA DI/DSI INV�LIDO';
      330: Result := 'REJEI��O: INFORMAR O VALOR DA AFRMM NA IMPORTA��O POR VIA MAR�TIMA';
      331: Result := 'REJEI��O: INFORMAR O CNPJ DO ADQUIRENTE OU DO ENCOMENDANTE NESTA FORMA DE IMPORTA��O';
      332: Result := 'REJEI��O: CNPJ DO ADQUIRENTE OU DO ENCOMENDANTE DA IMPORTA��O INV�LIDO';
      333: Result := 'REJEI��O: INFORMAR A UF DO ADQUIRENTE OU DO ENCOMENDANTE NESTA FORMA DE IMPORTA��O';
      334: Result := 'REJEI��O: N�MERO DO PROCESSO DE DRAWBACK N�O INFORMADO NA IMPORTA��O';
      335: Result := 'REJEI��O: N�MERO DO PROCESSO DE DRAWBACK NA IMPORTA��O INV�LIDO';
      336: Result := 'REJEI��O: INFORMADO O GRUPO DE EXPORTA��O NO ITEM PARA CFOP QUE N�O � DE EXPORTA��O';
      337: Result := 'REJEI��O: N�O INFORMADO O GRUPO DE EXPORTA��O NO ITEM';
      338: Result := 'REJEI��O: N�MERO DE PROCESSO DE DRAWBACK N�O INFORMADO NA EXPORTA��O';
      339: Result := 'REJEI��O: N�MERO DE PROCESSO DE DRAWBACK NA EXPORTA��O INV�LIDO';
      340: Result := 'REJEI��O: N�O INFORMADO O GRUPO DE EXPORTA��O INDIRETA NO ITEM';
      341: Result := 'REJEI��O: N�MERO DO REGISTRO DE EXPORTA��O INV�LIDO';
      342: Result := 'REJEI��O: CHAVE DE ACESSO INFORMADA NA EXPORTA��O INDIRETA COM DV INV�LIDO';
      343: Result := 'REJEI��O: MODELO DA NF-E INFORMADA NA EXPORTA��O INDIRETA DIFERENTE DE 55';
      344: Result := 'REJEI��O: DUPLICIDADE DE NF-E INFORMADA NA EXPORTA��O INDIRETA (CHAVE ACESSO INFORMADA MAIS DE UMA VEZ)';
      345: Result := 'REJEI��O: CHAVE DE ACESSO INFORMADA NA EXPORTA��O INDIRETA N�O CONSTA COMO NF-E REFERENCIADA';
      346: Result := 'REJEI��O: SOMAT�RIO QUANTIDADES INFORMADAS NA EXPORTA��O INDIRETA N�O CORRESPONDE TOTAL DO ITEM';
      347: Result := 'Rejei��o: Descri��o  do  Combust�vel diverge da descri��o adotada pela ANP';
      348: Result := 'REJEI��O: NFC-E COM GRUPO RECOPI';
      349: Result := 'REJEI��O: N�MERO RECOPI N�O INFORMADO';
      350: Result := 'REJEI��O: N�MERO RECOPI INV�LIDO';
      351: Result := 'REJEI��O: VALOR DO ICMS DA OPERA��O NO ICMS-ST=51 DIFERE DO PRODUTO BC E AL�QUOTA';
      352: Result := 'REJEI��O: VALOR DO ICMS DIFERIDO NO CST=51 DIFERE DO PRODUTO VALOR ICMS OPERA��O E ICMS DIFERIDO';
      353: Result := 'REJEI��O: VALOR DO ICMS NO CST=51 N�O CORRESPONDE A DIFEREN�A DO ICMS OPERA��O E ICMS DIFERIDO';
      354: Result := 'REJEI��O: INFORMADO GRUPO DE DEVOLU��ODE TRIBUTOS PARA NF-E E QUE N�O TEM FINALIDADE DE DEVOLU��O';
      355: Result := 'Rejei��o: Informar o local de sa�da do Pais no caso da exporta��o';
      356: Result := 'Rejei��o: Informar o local de sa�da do Pais somente no caso da exporta��o';
      357: Result := 'EJEI��O: CHAVE DE ACESSO DO GRUPO DE EXPORTA��O INDIRETA INEXISTENTE';
      358: Result := 'EJEI��O: CHAVE DE ACESSO NO GRUPO DE EXPORTA��O INDIRETA CANCELADA OU DENEGADA';
      359: Result := 'Rejei��o: NF-e de venda a �rg�o P�blico sem informar a Nota de Empenho';
      360: Result := 'Rejei��o: NF-e com Nota de Empenho inv�lida para a UF';
      361: Result := 'Rejei��o: NF-e com Nota de Empenho inexistente na UF';
      362: Result := 'Rejei��o: Venda de combust�vel sem informa��o do Transportador';
      363: Result := 'EJEI��O: TOTAL DO ISS DIFERE DO SOMAT�RIO DOS ITENS';
      364: Result := 'Rejei��o: Total do valor da dedu��o do ISS difere do somat�rio dos itens somat�rio dos itens';
      365: Result := 'Rejei��o: Total de outras reten��es difere do somat�riodos itens';
      366: Result := 'Rejei��o: Total do desconto incondicionado ISS difere do somat�rio dos itens';
      367: Result := 'Rejei��o: Total do desconto condicionado ISS difere do somat�rio dos itens';
      368: Result := 'Rejei��o: Total de ISS retido difere do somat�rio dos itens';
      369: Result := 'Rejei��o: N�o informado o grupo avulsa na emiss�o pelo Fisco';
      //370: Result := 'Rejei��o: Nota Fiscal Avulsa com tipo de emiss�o inv�lido';
      370: Result := 'Rejei��o: Processo de emiss�o pelo Fisco com Tipo de emiss�o inv�lido';
      371: Result := 'Rejei��o: CNPJ/CPF Autorizado n�o � emitente de CT-e';





























      401: Result := 'Rejei��o: CPF do remetente inv�lido';
      402: Result := 'Rejei��o: XML da �rea de dados com codifica��o diferente de UTF-8';
      403: Result := 'Rejei��o: O grupo de informa��es da NF-e avulsa � de uso exclusivo do Fisco';
      404: Result := 'Rejei��o: Uso de prefixo de namespace n�o permitido';
      405: Result := 'Rejei��o: C�digo do pa�s do emitente: d�gito inv�lido';
      406: Result := 'Rejei��o: C�digo do pa�s do destinat�rio: d�gito inv�lido';
      //407: Result := 'Rejei��o: O CPF s� pode ser informado no campo emitente para a NF-e avulsa';
      407: Result := 'Rejei��o: CPF do Emitente somente no servi�o de Nota Fiscal Avulsa no site do Fisco';
//CTE   408|Rejei��o: Lote com CT-e de diferentes UF
      409: Result := 'Rejei��o: Campo cUF inexistente no elemento nfeCabecMsg do SOAP Header';
      410: Result := 'Rejei��o: UF informada no campo cUF n�o � atendida pelo Web Service';
      411: Result := 'Rejei��o: Campo versaoDados inexistente no elemento nfeCabecMsg do SOAP Header';
//CTE   413|Rejei��o: C�digo de Munic�pio de t�rmino da presta��o: d�gito inv�lido
//CTE   414|Rejei��o: C�digo de Munic�pio diverge da UF de t�rmino da presta��o
//CTE   415|Rejei��o: CNPJ do remetente inv�lido
//CTE   416|Rejei��o: CPF do remetente inv�lido
      417: Result := 'Rejei��o: Total do ICMS superior ao valor limite estabelecido';
      418: Result := 'Rejei��o: Total do ICMS ST superior ao valor limite estabelecido';
//CTE   417|Rejei��o: C�digo de Munic�pio de localiza��o remetente: d�gito inv�lido
//CTE   418|Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o remetente
//CTE   419|Rejei��o: IE do remetente inv�lida
      420: Result := 'Rejei��o: Cancelamento para NF-e j� cancelada';
//CTE   421|Rejei��o: IE do remetente n�o cadastrada
//CTE   422|Rejei��o: IE do remetente n�o vinculada ao CNPJ
//CTE   423|Rejei��o: C�digo de Munic�pio de localiza��o destinat�rio: d�gito inv�lido
//CTE   424|Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o destinat�rio
        421: Result := 'Rejei��o: Informado o CNPJ/CPF do Emitente';
        422: Result := 'Rejei��o: Informado o CNPJ/CPF do Destinat�rio';
        423: Result := 'Rejei��o: CNPJ/CPF j� est� autorizado a acessar o XML da NF-e';
//CTE 425|Rejei��o: CNPJ destinat�rio n�o cadastrado
//CTE   426|Rejei��o: IE do destinat�rio n�o cadastrada
//CTE   427|Rejei��o: IE do destinat�rio n�o vinculada ao CNPJ
//CTE   428|Rejei��o: CNPJ do expedidor inv�lido
//CTE   429|Rejei��o: CPF do expedidor inv�lido
//CTE   430|Rejei��o: C�digo de Munic�pio de localiza��o expedidor: d�gito inv�lido
//CTE   431|Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o expedidor
//CTE   432|Rejei��o: IE do expedidor inv�lida
//CTE   433|Rejei��o: CNPJ expedidor n�o cadastrado
//CTE   434|Rejei��o: IE do expedidor n�o cadastrada

        434: Result := 'Rejei��o: NF-e sem indicativo do intermediador';
        435: Result := 'Rejei��o: NF-e n�o pode ter o indicativo do intermediador';
        436: Result := 'Rejei��o: Informado 99-Outros como meio de pagamento';
        437: Result := 'Rejei��o: CNPJ da institui��o de pagamento inv�lido';
        438: Result := 'Rejei��o: Obrigat�ria as informa��es do intermediador da transa��o para opera��o por site de terceiros';
        439: Result := 'Rejei��o: Informa��es do intermediador da transa��o para opera��o por site de terceiros preenchido indevidamente';
        440: Result := 'Rejei��o: CNPJ do intermediador da transa��o inv�lido';

//CTE   435|Rejei��o: IE do expedidor n�o vinculada ao CNPJ
//CTE   436|Rejei��o: CNPJ do recebedor inv�lido
//CTE   437|Rejei��o: CPF do recebedor inv�lido
//CTE   438|Rejei��o: C�digo de Munic�pio de localiza��o do recebedor: d�gito inv�lido
//CTE   439|Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o recebedor
//CTE   440|Rejei��o: IE do recebedor inv�lida
//CTE   441|Rejei��o: CNPJ recebedor n�o cadastrado
//CTE   442|Rejei��o: IE do recebedor n�o cadastrada
//CTE   443|Rejei��o: IE do recebedor n�o vinculada ao CNPJ
//CTE   444|Rejei��o: CNPJ do tomador inv�lido
//CTE   445|Rejei��o: CPF do tomador inv�lido
//CTE   446|Rejei��o: C�digo de Munic�pio de localiza��o tomador: d�gito inv�lido
      446: Result := 'Rejei��o: Informado CEST inexistente [nItem:999]';
//CTE   447|Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o tomador
//CTE   448|Rejei��o: IE do tomador inv�lida
//CTE   449|Rejei��o: CNPJ tomador n�o cadastrado
      448: Result := 'Rejei��o: CNPJ/CPF Autor n�o � emitente de CT-e';
      449: Result := 'Rejei��o: Modalidade de Frete n�o � por conta do Destinat�rio';
      450: Result := 'Rejei��o: Modelo da NF-e diferente de 55';
      //451: Result := 'Rejei��o: Processo de emiss�o informado inv�lido';
      451: Result := 'Rejei��o: Processo de Emiss�o pelo Fisco incompat�vel com a S�rie da NF';
      //452: Result := 'Rejei��o: Tipo Autorizador do Recibo diverge do �rg�o Autorizador';
      452: Result := 'Rejei��o: Solicitada resposta ass�ncrona para Lote com somente 1 (uma) NFC-e';
      453: Result := 'Rejei��o: Ano de inutiliza��o n�o pode ser superior ao Ano atual';
      454: Result := 'Rejei��o: Ano de inutiliza��o n�o pode ser inferior a 2006';
//CTE   455|Rejei��o: C�digo de Munic�pio de in�cio da presta��o: d�gito inv�lido
      455: Result := 'Rejei��o: �rg�o Autor do evento diferente da UF da Chave de Acesso';
//CTE   456|Rejei��o: C�digo de Munic�pio diverge da UF de in�cio da presta��o
//CTE   457|Rejei��o: O lote cont�m CT-e de mais de um estabelecimento emissor
//CTE   458|Rejei��o: Grupo de CT-e normal n�o informado para CT-e normal
//CTE   459|Rejei��o: Grupo de CT-e complementar n�o informado para CT-e complementar
//CTE   460|Rejei��o: N�o informado os dados do remetente indicado como tomador do servi�o
      //461: Result := 'REJEI��O: INFORMADO PERCENTUAL DE G�S NATURAL NA MISTURA PARA PRODUTO DIFERENTE DE GLP';
      461: Result := 'Rejei��o: Informado campos de percentual de GLP e/ou GLGNn e/ou GLGNi para produto diferente de GLP [nItem: nnn]';
//CTE   462|Rejei��o: N�o informado os dados do recebedor indicado como tomador do servi�o
//CTE   463|Rejei��o: N�o informado os dados do destinat�rio indicado como tomador do servi�o

      465: Result := 'REJEI��O: N�MERO DE CONTROLE DA FCI INEXISTENTE';

      466: Result := 'Rejei��o: Evento com Tipo de Autor incompat�vel';
      467: Result := 'Rejei��o: Dados da NF-e divergentes do EPEC [tag:xxxx]';
      468: Result := 'Rejei��o: NF-e com Tipo Emiss�o = 4, sem EPEC correspondente';
//CTE   469|Rejei��o: Remetente deve ser informado para tipo de servi�o diferente de redespacho intermedi�rio ou Servi�o vinculado a multimodal
//CTE   470|Rejei��o: Destinat�rio deve ser informado para tipo de servi�o diferente de redespacho intermedi�rio ou servi�o vinculado a multimodal
//CTE   471|Rejei��o: Ano de inutiliza��o n�o pode ser superior ao Ano atual
//CTE   472|Rejei��o: Ano de inutiliza��o n�o pode ser inferior a 2008
//CTE   473|Rejei��o: Tipo Autorizador do Recibo diverge do �rg�o Autorizador
//CTE   474|Rejei��o: Expedidor deve ser informado para tipo de servi�o de redespacho intermedi�rio e servi�o vinculado a multimodal
//CTE   475|Rejei��o: Recebedor deve ser informado para tipo de servi�o de redespacho intermedi�rio e servi�o vinculado a multimodal
      474: Result := 'Rejei��o: FCP n�o deve ser destacado na NF-e conforme legisla��o estadual [nItem:999]';
      475: Result := 'Rejei��o: Opera��o n�o permitida para n�o contribuinte [nItem:999]';
      476: Result := 'Rejei��o: C�digo da UF diverge da UF da primeira NF-e do Lote';

      478: Result := 'Rejei��o: Local da entrega n�o informado para faturamento direto de ve�culos novos';
      479: Result := 'REJEI��O: EMISSOR EM SITUA��O IRREGULAR PERANTO O FISCO';
      480: Result := 'REJEI��O: CNPJ DA CHAVE DE ACESSO DA NF-E INFORMADA DIVERGE DO CNPJ DO EMITENTE';
      481: Result := 'REJEI��O: UF DA CHAVE DE ACESSO DIVERGE DO C�DIGO DA UF INFORMADA';
      482: Result := 'REJEI��O: AA DA CHAVE DE ACESSO INV�LIDA';
      483: Result := 'REJEI��O: MM DA CHAVE DE ACESSO INV�LIDO';
      484: Result := 'Rejei��o: Chave de Acesso com tipo de emiss�o diferente de 4 (posi��o 35 da Chave de Acesso)';
      485: Result := 'Rejei��o: Duplicidade de numera��o do EPEC (Modelo, CNPJ ou CPF, S�rie e N�mero)';
      486: Result := 'REJEI��O: DPEC N�O LOCALIZADA PARA O N�MERO DE REGISTRO DE DPEC INFORMADO';
      487: Result := 'REJEI��O: NENHUMA DPEC LOCALIZADA PARA A CHAVE DE ACESSO INFORMADA';
      488: Result := 'REJEI��O: REQUISITANTE DE CONSULTA N�O TEM O MESMO CNPJ BASE DO EMISSOR DA DPEC';
      489: Result := 'Rejei��o: CNPJ informado inv�lido (DV ou zeros)';
      490: Result := 'Rejei��o: CPF informado inv�lido (DV ou zeros)';
      491: Result := 'Rejei��o: O cEvento informado inv�lido';
      492: Result := 'Rejei��o: O verEvento informado inv�lido';
      493: Result := 'Rejei��o: Evento n�o atende o Schema XML espec�fico';
      //494: Result := 'Rejei��o: NF-e vinculada inexistente';
      494: Result := 'Rejei��o: Chave de Acesso inexistente [chNFe:99999999999999999999999999999999999999999999]';
      495: Result := 'Rejei��o: CPF do Emitente com S�rie incompat�vel';
      496: Result := 'Rejei��o: NF-e n�o se encontra cancelada';
      497: Result := 'Rejei��o: Destinat�rio desconhece a opera��o';
      498: Result := 'Rejei��o: Destinat�rio devolveu a mercadoria';
      499: Result := 'Rejei��o: data de saida/entrada anterior a data de autoriza��o da NF-e';
      500: Result := 'Rejei��o: hora da sa�da/entrada anterior a hora da autoriza��o da NF-e';
      501: Result := 'Rejei��o: NF-e autorizda h� mais de 30 dias (720 horas)';
      502: Result := 'Rejei��o: Erro na Chave de Acesso - Campo Id n�o corresponde � concatena��o dos campos correspondentes';
      //503: Result := 'Rejei��o: S�rie utilizada fora da faixa permitida no SCAN (900-999)';
      503: Result := 'Rejei��o: CNPJ do emitente com S�rie incompat�vel';
      504: Result := 'Rejei��o: Data de Entrada/Sa�da posterior ao permitido';
      505: Result := 'Rejei��o: Data de Entrada/Sa�da anterior ao permitido';
      506: Result := 'Rejei��o: Data de Sa�da menor que a Data de Emiss�o';
      507: Result := 'Rejei��o: O CNPJ do destinat�rio/remetente n�o deve ser informado em opera��o com o exterior';
      508: Result := 'Rejei��o: O CNPJ com conte�do nulo s� � v�lido em opera��o com exterior';
      509: Result := 'Rejei��o: Informado c�digo de munic�pio diferente de �9999999� para opera��o com o exterior';
      510: Result := 'Rejei��o: Opera��o com Exterior e C�digo Pa�s destinat�rio � 1058 (Brasil) ou n�o informado';
      511: Result := 'Rejei��o: N�o � de Opera��o com Exterior e C�digo Pa�s destinat�rio difere de 1058 (Brasil)';
      512: Result := 'Rejei��o: CNPJ do Local de Retirada inv�lido';
      513: Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada deve ser 9999999 para UF retirada = EX';
      514: Result := 'Rejei��o: CNPJ do Local de Entrega inv�lido';
      515: Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega deve ser 9999999 para UF entrega = EX';
      516: Result := 'Rejei��o: Falha no schema XML � inexiste a tag raiz esperada para a mensagem';
      517: Result := 'Rejei��o: Falha no schema XML � inexiste atributo versao na tag raiz da mensagem';
      518: Result := 'Rejei��o: CFOP de entrada para NF-e de sa�da';
      519: Result := 'Rejei��o: CFOP de sa�da para NF-e de entrada';
      520: Result := 'Rejei��o: CFOP de Opera��o com Exterior e UF destinat�rio difere de EX';
      521: Result := 'Rejei��o: CFOP n�o � de Opera��o com Exterior e UF destinat�rio � EX';
      522: Result := 'Rejei��o: CFOP de Opera��o Estadual e UF emitente difere UF destinat�rio.';
      523: Result := 'Rejei��o: CFOP n�o � de Opera��o Estadual e UF emitente igual a UF destinat�rio.';
      524: Result := 'Rejei��o: CFOP de Opera��o com Exterior e n�o informado NCM';
      525: Result := 'Rejei��o: CFOP de Importa��o e n�o informado dados da DI';
      526: Result := 'Rejei��o: CFOP de Exporta��o e n�o informado Local de Embarque';
      527: Result := 'Rejei��o: Opera��o de Exporta��o com informa��o de ICMS incompat�vel';
      528: Result := 'Rejei��o: Valor do ICMS difere do produto BC e Al�quota';
      529: Result := 'Rejei��o: NCM de informa��o obrigat�ria para produto tributado pelo IPI';
      530: Result := 'Rejei��o: Opera��o com tributa��o de ISSQN sem informar a Inscri��o Municipal';
      531: Result := 'Rejei��o: Total da BC ICMS difere do somat�rio dos itens';
      532: Result := 'Rejei��o: Total do ICMS difere do somat�rio dos itens';
      533: Result := 'Rejei��o: Total da BC ICMS-ST difere do somat�rio dos itens';
      534: Result := 'Rejei��o: Total do ICMS-ST difere do somat�rio dos itens';
      535: Result := 'Rejei��o: Total do Frete difere do somat�rio dos itens';
      536: Result := 'Rejei��o: Total do Seguro difere do somat�rio dos itens';
      537: Result := 'Rejei��o: Total do Desconto difere do somat�rio dos itens';
      538: Result := 'Rejei��o: Total do IPI difere do somat�rio dos itens';
      539: Result := 'Rejei��o: Duplicidade de NF-e, com diferen�a na Chave de Acesso [99999999999999999999999999999999999999999]';
      540: Result := 'Rejei��o: CPF do Local de Retirada inv�lido';
      541: Result := 'Rejei��o: CPF do Local de Entrega inv�lido';
      542: Result := 'Rejei��o: CNPJ do Transportador inv�lido';
      543: Result := 'Rejei��o: CPF do Transportador inv�lido';
      544: Result := 'Rejei��o: IE do Transportador inv�lida';
      545: Result := 'Rejei��o: Falha no schema XML � vers�o informada na versaoDados do SOAPHeader diverge da vers�o da mensagem';
      546: Result := 'Rejei��o: Erro na Chave de Acesso � Campo Id � falta a literal NFe';
      547: Result := 'Rejei��o: D�gito Verificador da Chave de Acesso da NF-e Referenciada inv�lido';
      548: Result := 'Rejei��o: CNPJ da NF referenciada inv�lido.';
      549: Result := 'Rejei��o: CNPJ da NF referenciada de produtor inv�lido.';
      550: Result := 'Rejei��o: CPF da NF referenciada de produtor inv�lido.';
      551: Result := 'Rejei��o: IE da NF referenciada de produtor inv�lido.';
      //552: Result := 'Rejei��o: D�gito Verificador da Chave de Acesso do CT-e Referenciado inv�lido';
      552: Result := 'Rejei��o: Chave de Acesso referenciada com CNPJ/CPF inv�lido [nOcor:nnn]';
      553: Result := 'Rejei��o: Tipo autorizador do recibo diverge do �rg�o Autorizador.';
      //554: Result := 'Rejei��o: S�rie difere da faixa 0-899';
      554: Result := 'Rejei��o: IE do Emitente informada como ISENTO indevidamente';
      555: Result := 'Rejei��o: Tipo autorizador do protocolo diverge do �rg�o Autorizador.';
      556: Result := 'Rejei��o: Justificativa de entrada em conting�ncia n�o deve ser informada para tipo de emiss�o normal.';
      557: Result := 'Rejei��o: A Justificativa de entrada em conting�ncia deve ser informada.';
      558: Result := 'Rejei��o: Data de entrada em conting�ncia posterior a data de emiss�o.';
      559: Result := 'Rejei��o: UF do Transportador n�o informada';
      //560: Result := 'Rejei��o: CNPJ base do emitente difere do CNPJ base da primeira NF-e do lote recebido';
      560: Result := 'Rejei��o: CNPJ base/CPF do emitente difere do CNPJ base/CPF da primeira NF-e do lote recebido';
      561: Result := 'Rejei��o: M�s de Emiss�o informado na Chave de Acesso difere do M�s de Emiss�o da NFe';
      562: Result := 'Rejei��o: C�digo Num�rico informado na Chave de Acesso difere do C�digo Num�rico da NF-e';
      563: Result := 'Rejei��o: J� existe pedido de Inutiliza��o com a mesma faixa de inutiliza��o';
      564: Result := 'Rejei��o: Total do Produto / Servi�o difere do somat�rio dos itens';
      565: Result := 'Rejei��o: Falha no schema XML � inexiste a tag raiz esperada para o lote de NF-e';

      567: Result := 'Rejei��o: Falha no schema XML � vers�o informada na versaoDados do SOAPHeader diverge da vers�o do lote de NF-e';
      568: Result := 'Rejei��o: Falha no schema XML � inexiste atributo versao na tag raiz do lote de NF-e';
      569: Result := 'Rejei��o: Data de entrada em conting�ncia muito atrasada';
      570: Result := 'Rejei��o: Tipo de Emiss�o 3, 6 ou 7 s� � v�lido nas conting�ncias SCAN/SVC'; // 570: Result := 'Rejei��o: tpEmis = 3 s� � v�lido na conting�ncia SCAN';
      //571: Result := 'Rejei��o: O tpEmis informado diferente de 3 para conting�ncia SCAN';
      571: Result := 'Rejei��o: Processo de emiss�o pelo Fisco com Certificado de Transmiss�o incompat�vel';
      572: Result := 'Rejei��o: Erro Atributo ID do evento n�o corresponde a concatena��o dos campos (�ID� + tpEvento + chNFe + nSeqEvento)';
      573: Result := 'Rejei��o: Duplicidade de Evento';
      574: Result := 'Rejei��o: O autor do evento diverge do emissor da NF-e';
      575: Result := 'Rejei��o: O autor do evento diverge do destinat�rio da NF-e';
      576: Result := 'Rejei��o: O autor do evento n�o � um �rg�o autorizado a gerar o evento';
      577: Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de emiss�o da NF-e';
      578: Result := 'Rejei��o: A data do evento n�o pode ser maior que a data do processamento';
      579: Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de autoriza��o para NF-e n�o emitida em conting�ncia';
      580: Result := 'Rejei��o: O evento exige uma NF-e autorizada';






      587: Result := 'Rejei��o: Usar somente o namespace padr�o da NF-e';
      588: Result := 'Rejei��o: N�o � permitida a presen�a de caracteres de edi��o no in�cio/fim da mensagem ou entre as tags da mensagem';
      589: Result := 'REJEI��O: N�MERO DO NSU INFORMADO SUPERIOR AO MAIOR NSU DA BASE DE DADOS DA SEFAZ';
      590: Result := 'Rejei��o: Informado CST para emissor do Simples Nacional (CRT=1)';
      591: Result := 'Rejei��o: Informado CSOSN para emissor que n�o � do Simples Nacional (CRT diferente de 1)';
      592: Result := 'Rejei��o: A NF-e deve ter pelo menos um item de produto sujeito ao ICMS';
      593: Result := 'REJEI��O: CNPJ-BASE CONSULTADO DIFERE DO CNPJ-BASE DO CERTIFICADO DIGITAL';
      594: Result := 'Rejei��o: O n�mero de sequencia do evento informado � maior que o permitido';
      595: Result := 'Rejei��o: A vers�o do leiaute da NF-e utilizada n�o � mais v�lida';
      596: Result := 'Rejei��o: Ambiente de homologa��o indispon�vel para recep��o de NF-e da vers�o 1.10.';
      597: Result := 'Rejei��o: CFOP de Importa��o e n�o informado dados de IPI';
      598: Result := 'Rejei��o: NF-e emitida em ambiente de homologa��o com Raz�o Social do destinat�rio diferente de NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
      599: Result := 'Rejei��o: CFOP de Importa��o e n�o informado dados de II';

      601: Result := 'Rejei��o: Total do II difere do somat�rio dos itens';
      602: Result := 'Rejei��o: Total do PIS difere do somat�rio dos itens sujeitos ao ICMS';
      603: Result := 'Rejei��o: Total do COFINS difere do somat�rio dos itens sujeitos ao ICMS';
      604: Result := 'Rejei��o: Total do vOutro difere do somat�rio dos itens';
      605: Result := 'Rejei��o: Total do vISS difere do somat�rio do vProd dos itens sujeitos ao ISSQN';
      606: Result := 'Rejei��o: Total do vBC do ISS difere do somat�rio dos itens';
      607: Result := 'Rejei��o: Total do ISS difere do somat�rio dos itens';
      608: Result := 'Rejei��o: Total do PIS difere do somat�rio dos itens sujeitos ao ISSQN';
      609: Result := 'Rejei��o: Total do COFINS difere do somat�rio dos itens sujeitos ao ISSQN';
      610: Result := 'Rejei��o: Total da NF difere do somat�rio dos Valores comp�e o valor Total da NF.';
      //611: Result := 'Rejei��o: cEAN inv�lido';
      611: Result := 'Rejei��o: GTIN (cEAN) inv�lido [nItem:999]';
      //612: Result := 'Rejei��o: cEANTrib inv�lido';
      612: Result := 'Rejei��o: GTIN da unidade tribut�vel (cEANTrib) inv�lido [nItem:999]';
      613: Result := 'Rejei��o: Chave de Acesso difere da existente em BD';
      614: Result := 'Rejei��o: Chave de Acesso inv�lida (C�digo UF inv�lido)';
      615: Result := 'Rejei��o: Chave de Acesso inv�lida (Ano < 05 ou Ano maior que Ano corrente)';
      616: Result := 'Rejei��o: Chave de Acesso inv�lida (M�s < 1 ou M�s > 12)';
      //617: Result := 'Rejei��o: Chave de Acesso inv�lida (CNPJ zerado ou d�gito inv�lido)';
      617: Result := 'Rejei��o: Chave de Acesso inv�lida (CNPJ/CPF zerado ou d�gito inv�lido)';
      618: Result := 'Rejei��o: Chave de Acesso inv�lida (modelo diferente de 55 e 65)';
      619: Result := 'Rejei��o: Chave de Acesso inv�lida (n�mero NF = 0)';
      620: Result := 'Rejei��o: Chave de Acesso difere da existente em BD';
      621: Result := 'Rejei��o: CPF Emitente n�o cadastrado';
      622: Result := 'Rejei��o: IE emitente n�o vinculada ao CPF';
      623: Result := 'Rejei��o: CPF Destinat�rio n�o cadastrado';
      624: Result := 'Rejei��o: IE Destinat�rio n�o vinculada ao CPF';
      625: Result := 'Rejei��o: Inscri��o SUFRAMA deve ser informada na venda com isen��o para ZFM';
      626: Result := 'Rejei��o: CFOP de opera��o isenta para ZFM diferente do previsto [nItem: 999]';
      627: Result := 'Rejei��o: O valor do ICMS desonerado deve ser informado';
      628: Result := 'Rejei��o: Total da NF superior ao valor limite estabelecido pela SEFAZ [Limite]';
      629: Result := 'Rejei��o: Valor do Produto difere do produto Valor Unit�rio de Comercializa��o e Quantidade Comercial';
      630: Result := 'Rejei��o: Valor do Produto difere do produto Valor Unit�rio de Tributa��o e Quantidade Tribut�vel';
      631: Result := 'Rejei��o: CNPJ-Base do Destinat�rio difere do CNPJ-Base do Certificado Digital';
      632: Result := 'Rejei��o: Solicita��o fora de prazo, a NF-e n�o est� mais dispon�vel para download';
      633: Result := 'Rejei��o: NF-e indispon�vel para download devido a aus�ncia de Manifesta��o do Destinat�rio';
      634: Result := 'Rejei��o: Destinat�rio da NF-e n�o tem o mesmo CNPJ raiz do solicitante do download';
      635: Result := 'Rejei��o: NF-e com mesmo n�mero e s�rie j� transmitida e aguardando processamento';


      640: Result := 'Rejei��o: Evento de "Ci�ncia da Opera��o" n�o pode ser informado ap�s a manifesta��o final do destinat�rio (Confirma��o, Opera��o n�o Realizada ou Desconhecimento)';
      641: Result := 'Rejei��o: Consumo Indevido';
      642: Result := 'Rejei��o: Falha na Consulta do Registro de Passagem, tente novamente ap�s 5 minutos';


      645: Result := 'Rejei��o: CNPJ do Certificado Digital n�o � emitente de NF-e';
      646: Result := 'Rejei��o: NF-e Cancelada, arquivo indispon�vel para download';
      647: Result := 'Rejei��o: NF-e Denegada, arquivo indispon�vel para download';


      650: Result := 'Rejei��o: Evento de "Ci�ncia da Opera��o" para NF-e Cancelada ou Denegada';
      651: Result := 'Rejei��o: Evento de "Desconhecimento da Opera��o" para NF-e Cancelada ou Denegada';
      652: Result := 'Rejei��o: NF-e para emitente pessoa f�sica';
      653: Result := 'REJEI��O: NF-E CANCELADA, ARQUIVO INDISPON�VEL PARA DOWNLOAD';
      654: Result := 'REJEI��O: NF-E DENEGADA, ARQUIVO INDISPON�VEL PARA DOWNLOAD';
      655: Result := 'REJEI��O: EVENTO DE CI�NCIA DA OPERA��O INFORMADO AP�S A MANIFESTA��O FINAL DO DESTINAT�RIO';
      656: Result := 'REJEI��O: CONSUMO INDEVIDO';
      657: Result := 'REJEI��O: C�DIGO DO �RG�O DIVERGE DO �RG�O AUTORIZADOR';
      658: Result := 'REJEI��O: UF DO DESTINAT�RIO DA CHAVE DE ACESSO DIVERGE DA UF AUTORIZADORA';
      659: Result := 'Rejei��o: Ano-M�s da Data de Emiss�o diverge do Ano_M�s da Chave de Acesso';
      660: Result := 'REJEI��O: CFOP DE COMBUST�VEL E N�O INFORMADO GRUPO DE COMBUST�VEL DA NF-E';
      661: Result := 'REJEI��O: NF-E J� EXISTENTE PARA O N�MERO DA DPEC INFORMADA';
      662: Result := 'REJEI��O: NUMERA��O DA DPEC EST� INUTILIZADA NA BASE DE DADOS DA SEFAZ';
      663: Result := 'REJEI��O: AL�Q. ICMS MAIOR QUE 4% NA SA�DA INTERESTADUAL COM PRODUTOS IMPORTADOS';














      678: Result := 'REJEI��O: NF REFERENCIADA COM UF DIFERENTE DA UF DA NF-E COMPLEMENTAR';
      679: Result := 'REJEI��O: MODELO DA NF-E REFERENCIADA DIFERENTE DE 55';
      680: Result := 'REJEI��O: DUPLICIDADE DE NF-E REFERENCIADA (CHAVE DE ACESSO REF MAIS DE UMA VEZ)';
      //681: Result := 'REJEI��O: DUPLICIDADE DE NF MODELO 1 REFERENCIADA (CNPJ, MODELO S�RIE E N�MERO)';
      681: Result := 'Rejei��o: Duplicidade de NF referenciada (CNPJ, Modelo, S�rie e N�mero) [nOcor: nnn]';
      682: Result := 'REJEI��O: DUPLICIDADE DE NF DE PRODUTOR REFERENCIADA (IE, MODELO, S�RIE E N�MERO)';
      683: Result := 'REJEI��O: MODELO DO CT-E REFERENCIADO DIFERENTE DE 57';
      684: Result := 'REJEI��O: DUPLICIDADE DE CUPOM FISCAL REFERENCIADO (MODELO, N�MERO E ORDEM E COO)';
      685: Result := 'REJEI��O: TOTAL DO VALOR APROXIMADO DOS TRIBUTOS DIFERE DO SOMAT�RIO DOS ITENS';
      686: Result := 'REJEI��O: NF COMPLEMENTAR REFERENCIA UMA NF-E CANCELADA';
      687: Result := 'REJEI��O: NF COMPLEMENTAR REFERENCIA UMA NF-E DENEGADA ';
      688: Result := 'REJEI��O: NF REFERENCIADA DE PRODUTOR COM IE INEXISTENTE (NREF: XXX) ';
      689: Result := 'REJEI��O: NF REFERENCIADA DE PRODUTOR COM IE N�O VINCULADA AO CNPJ/CPF INFORMADO (NREF: XXX)';
      690: Result := 'REJEI��O: PEDIDO DE CANCELAMENTO PARA NF-E COM CT-E OU MDF-E';
      691: Result := 'Rejei��o: Chave de Acesso da NF-e diverge da Chave de Acesso do EPEC [Chave EPEC: xxxxxxxxx]';
      692: Result := 'Rejei��o: Existe EPEC registrado para esta S�rie e N�mero [Chave EPEC: xxxxxxxxxxx]';








      701: Result := 'REJEI��O: NF-E N�O PODE UTILIZAR VER�O 3.00';
      702: Result := 'Rejei��o: NFC-e n�o � aceita pela UF do Emitente';
      703: Result := 'Rejei��o: Data-Hora de Emiss�o posterior ao hor�rio de recebimento';
      704: Result := 'Rejei��o: NFC-e com Data-Hora de emiss�o atrasada';
      705: Result := 'Rejei��o: NFC-e com data de entrada/sa�da';
      706: Result := 'Rejei��o: NFC-e para opera��o de entrada';
      707: Result := 'Rejei��o: NFC-e para opera��o interestadual ou com o exterior';
      708: Result := 'Rejei��o: NFC-e n�o pode referenciar documento fiscal';
      709: Result := 'Rejei��o: NFC-e com formato de DANFE inv�lido';
      710: Result := 'Rejei��o: NF-e com formato de DANFE inv�lido';
      711: Result := 'Rejei��o: NF-e com conting�ncia off-line';
      712: Result := 'Rejei��o: NFC-e com conting�ncia off-line para a UF';
      713: Result := 'Rejei��o: Tipo de Emiss�o diferente de 6 ou 7 para conting�ncia da SVC acessada';
      714: Result := 'Rejei��o: NFC-e com conting�ncia DPEC inexistente';
      715: Result := 'Rejei��o: NFC-e com finalidade inv�lida';
      716: Result := 'Rejei��o: NFC-e em opera��o n�o destinada a consumidor final';
      717: Result := 'Rejei��o: NFC-e em opera��o n�o presencial';
      718: Result := 'Rejei��o: NFC-e n�o deve informar IE de Substituto Tribut�rio';
      719: Result := 'Rejei��o: NF-e sem a identifica��o do destinat�rio';
      720: Result := 'Rejei��o: Na opera��o com Exterior deve ser informada tag idEstrangeiro';
      721: Result := 'Rejei��o: Opera��o interestadual deve informar CNPJ ou CPF.';
      722: Result := 'REJEI��O: OPERA��O INTERNA COM IDESTRANGEIRO INFORMADO DEVE SER PRESENCIAL';
      723: Result := 'Rejei��o: Opera��o interna com idEstrangeiro informado deve ser para consumidor final';
      724: Result := 'Rejei��o: NF-e sem o nome do destinat�rio';
      725: Result := 'Rejei��o: NFC-e com CFOP inv�lido';
      726: Result := 'Rejei��o: NF-e sem a informa��o de endere�o do destinat�rio';
      727: Result := 'Rejei��o: Opera��o com Exterior e UF diferente de EX';
      728: Result := 'Rejei��o: NF-e sem informa��o da IE do destinat�rio';
      729: Result := 'Rejei��o: NFC-e com informa��o da IE do destinat�rio';
      730: Result := 'Rejei��o: NFC-e com Inscri��o Suframa';
      731: Result := 'Rejei��o: CFOP de opera��o com Exterior e idDest <> 3';
      732: Result := 'Rejei��o: CFOP de opera��o interestadual e idDest <> 2';
      733: Result := 'Rejei��o: CFOP de opera��o interna e idDest <> 1';
      734: Result := 'Rejei��o: NFC-e com Unidade de Comercializa��o invalida';
      735: Result := 'Rejei��o: NFC-e com Unidade de Tributa��o inv�lida';
      736: Result := 'Rejei��o: NFC-e com grupo de Ve�culos novos';
      737: Result := 'Rejei��o: NFC-e com grupo de Medicamentos';
      738: Result := 'Rejei��o: NFC-e com grupo de Armamentos';
      739: Result := 'Rejei��o: NFC-e com grupo de Combust�vel';
      740: Result := 'Rejei��o: NFC-e com CST 51-Diferimento';
      741: Result := 'Rejei��o: NFC-e com Partilha de ICMS entre UF';
      742: Result := 'Rejei��o: NFC-e com grupo do IPI';
      743: Result := 'Rejei��o: NFC-e com grupo do II';

      745: Result := 'Rejei��o: NF-e sem grupo do PIS ';
      746: Result := 'Rejei��o: NFC-e com grupo do PIS-ST';

      748: Result := 'Rejei��o: NF-e sem grupo da COFINS';
      749: Result := 'Rejei��o: NFC-e com grupo da COFINS-ST';
      750: Result := 'Rejei��o: NFC-e com valor total superior ao permitido para destinat�rio n�o identificado (C�digo) [Limite';
      751: Result := 'Rejei��o: NFC-e com valor total superior ao permitido para destinat�rio n�o identificado (Nome) [Limite]';
      752: Result := 'Rejei��o: NFC-e com valor total superior ao permitido para destinat�rio n�o identificado (Endere�o) [Limite]';
      753: Result := 'Rejei��o: NFC-e com Frete';
      754: Result := 'Rejei��o: NFC-e com dados do Transportador';
      755: Result := 'Rejei��o: NFC-e com dados de Reten��o do ICMS no Transporte';
      756: Result := 'Rejei��o: NFC-e com dados do ve�culo de Transporte';
      757: Result := 'Rejei��o: NFC-e com dados de Reboque do ve�culo deTransporte';
      758: Result := 'Rejei��o: NFC-e com dados do Vag�o de Transporte';
      759: Result := 'Rejei��o: NFC-e com dados da Balsa de Transporte';
      760: Result := 'Rejei��o: NFC-e com dados de cobran�a (Fatura,Duplicata)';
      761: Result := 'Rejei��o: NFC-e com dados de Exporta��o';
      762: Result := 'Rejei��o: NFC-e com dados de compras (EmpenhoPedido, Contrato)';
      763: Result := 'Rejei��o: NFC-e com dados de aquisi��o de Cana';
      764: Result := 'Solicitada resposta s�ncrona para Lote com mais de uma NF-e (indSinc=1)';
      765: Result := 'Rejei��o: Lote s� poder� conter NF-e ou NFC-e';
      766: Result := 'Rejei��o: NFC-e com CST 50-Suspens�o';
      767: Result := 'Rejei��o: NFC-e com somat�rio dos pagamentos diferente do total da Nota Fiscal';
      768: Result := 'Rejei��o: NF-e n�o deve possuir o grupo de Formas de Pagamento';
      769: Result := 'Rejei��o: A crit�rio da UF NFC-e deve possuir o grupo de Formas de Pagamento';
      770: Result := 'No caso da NFC-e: Verificar se NFC-e est� autorizada a mais de 30 minutos.';
      771: Result := 'Rejei��o: Opera��o Interestadual e UF de destino com EX';
      772: Result := 'Rejei��o: Opera��o Interestadual e UF de destino igual � UF do emitente';
      773: Result := 'Rejei��o: Opera��o Interna e UF de destino difere da UF do emitente';
      774: Result := 'Rejei��o: NFC-e com indicador de item n�o participante do total';
      775: Result := 'Rejei��o: Modelo da NFC-e diferente de 65';
      776: Result := 'Rejei��o: NFC-e conting�ncia offline com Data-Hora de emiss�o atrasada';
      777: Result := 'Rejei��o: NFC-e deve informar NCM completo';
      778: Result := 'Rejei��o: Informado NCM inexistente';
      779: Result := 'Rejei��o: NFC-e com NCM incompat�vel';
      780: Result := 'Rejei��o: Total da NFC-e superior ao valor limite estabelecido pela SEFAZ [Limite]';
      781: Result := 'REJEI��O: EMISSOR N�O HABILITADO PARA EMISS�O DE NFC-E';
      782: Result := 'Rejei��o: NFC-e n�o � autorizada pelo SCAN';
      783: Result := 'Rejei��o: NFC-e n�o � autorizada pela SVC';
      784: Result := 'REJEI��O: NF-E COM INDICATIVO DE NFC-E COM ENTREGA A DOMICILIO';
      785: Result := 'Rejei��o: NFC-e com entrega a domic�lio n�o permitida pela UF';
      786: Result := 'Rejei��o: NFC-e de entrega a domic�lio sem dados do Transportador';
      787: Result := 'Rejei��o: NFC-e de entrega a domic�lio sem a identifica��o do destinat�rio';
      788: Result := 'Rejei��o: NFC-e de entrega a domic�lio sem o endere�o do destinat�rio';
      789: Result := 'Rejei��o: NFC-e para destinat�rio contribuinte de ICMS';
      790: Result := 'Rejei��o: Opera��o com Exterior para destinat�rio Contribuinte de ICMS';
      791: Result := 'Rejei��o: NF-e com indica��o de destinat�rio isento de IE, com a informa��o da IE do destinat�rio';
      792: Result := 'Rejei��o: Informada a IE do destinat�rio para opera��o com destinat�rio no Exterio';
      793: Result := 'REJEI��O: INFORMADO CAP�TULO DO NCM INEXISTENTE';
      794: Result := 'Rejei��o: NF-e com indicativo de NFC-e com entrega a domic�lio';

      796: Result := 'Rejei��o: Empresa sem Chave de Seguran�a para o QR-Code';

      827: Result := 'Rejei��o: Obrigat�rio informar o tipo de autoriza��o';
      828: Result := 'Rejei��o: N�o permitido informar o campo tipo de autoriza��o';
      829: Result := 'Rejei��o: Condi��o de uso n�o informado para o tipo de autoriza��o de uso';
      830: Result := 'Rejei��o: N�o permitido preencher o campo Condi��o de Uso';
      831: Result := 'Rejei��o: Transportador Contratado n�o autorizado a a liberar acesso a NF-e';

      840: Result := 'Rejei��o: NCM de medicamento e n�o informado o grupo de medicamento (med) [nItem:nnn] ';
      841: Result := 'Rejei��o: C�digo do Tipo de Ve�culo Inexistente [nItem:nnn]';
      842: Result := 'Rejei��o: C�digo da esp�cie de Ve�culo Inexistente[nItem:nnn]';
      843: Result := 'Rejei��o: C�digo da esp�cie de Ve�culo incompat�vel com o tipo do Ve�culo. [nItem:nnn]';
      844: Result := 'Rejei��o: C�digo de Item da Lista de Servi�os inexistente. [nItem:nnn]';
      845: Result := 'Rejei��o: O Grupo Transportador n�o pode ser preenchido para Modalidade do frete informada';
      846: Result := 'Rejei��o: Transporte pr�prio por conta do Remetente e CNPJ Base ou CPF do Transportador difere do CNPJ Base ou CPF do Remetente';
      847: Result := 'Rejei��o: Transporte n�o � pr�prio por conta do Remetente e CNPJ Base ou CPF do Transportador igual ao CNPJ Base ou CPF do Remetente';
      848: Result := 'Rejei��o: Transporte pr�prio por conta do Destinat�rio e CNPJ Base ou CPF do Transportador difere do CNPJ Base ou CPF do Destinat�rio';
      849: Result := 'Rejei��o: Transporte n�o � pr�prio por conta do Destinat�rio e CNPJ Base ou CPF do Transportador igual ao CNPJ Base ou CPF do Destinat�rio';
      850: Result := 'Rejei��o: Data de vencimento da parcela n�o informada ou menor que a Data de vencimento da parcela anterior [nOcor:999]';
      851: Result := 'Rejei��o: Soma do valor das parcelas difere do Valor L�quido da Fatura';
      852: Result := 'Rejei��o: N�mero da parcela inv�lido ou n�o informado [nOcor:999]';
      854: Result := 'Rejei��o: Unidade Tribut�vel incompat�vel com produto informado [nItem:nnn]';
      855: Result := 'Rejei��o: Somat�rio percentuais de GLP derivado do petr�leo, GLGNn e GLGNi diferente de 100 [nItem:nnn]';
      856: Result := 'Rejei��o: Campo valor de partida n�o preenchido para produto GLP [nItem: nnn]';
      857: Result := 'Rejei��o: Informado Duplicata Mercantil como Forma de Pagamento';
      858: Result := 'Rejei��o: Grupo de Tributa��o informado indevidamente [nItem: nnn]';
      859: Result := 'Rejei��o: Total do FCP retido anteriormente por Substitui��o Tribut�ria difere do somat�rio dos itens';
      860: Result := 'Rejei��o: Valor do FCP informado difere de base de c�lculo*al�quota [nItem:nnn]';
      861: Result := 'Rejei��o: Total do FCP difere do somat�rio dos itens';
      862: Result := 'Rejei��o: Total do FCP ST difere do somat�rio dos itens';
      863: Result := 'Rejei��o: Total do IPI devolvido difere do somat�rio dos itens';
      864: Result := 'Rejei��o: NF-e com indicativo de Opera��o presencial, fora do estabelecimento e n�o informada NF referenciada';
      865: Result := 'Rejei��o: Total dos pagamentos menor que o total da nota';
      866: Result := 'Rejei��o: Aus�ncia de troco quando o valor dos pagamentos informados for maior que o total da nota';
      867: Result := 'Rejei��o: Grupo duplicata informado e forma de pagamento n�o � Duplicata Mercantil.';
      868: Result := 'Rejei��o: Grupos Veiculo Transporte e Reboque n�o devem ser informados';
      869: Result := 'Rejei��o: Valor do troco incorreto';
      870: Result := 'Rejei��o: Data de validade incompat�vel com data de fabrica��o [nItem:nnn]';
      871: Result := 'Rejei��o: O campo Forma de Pagamento deve ser preenchido com a op��o �Sem Pagamento�';
      872: Result := 'Rejei��o: Informado Duplicata Mercantil como Forma de Pagamento e n�o preenchido o Grupo Duplicata';
      873: Result := 'Rejei��o: Opera��o com medicamentos n�o informado os campos de rastreabilidade [nItem:nnn]';
      874: Result := 'Rejei��o: Percentual de FCP inv�lido [nItem:nnn]';
      875: Result := 'Rejei��o: Percentual de FCP ST inv�lido [nItem:nnn]';
      876: Result := 'Rejei��o: Opera��o interestadual para Consumidor Final e valor do FCP informado em campo diferente de vFCPUFDest (id:NA13) [nItem:nnn]';
      877: Result := 'Rejei��o: Data de fabrica��o maior que a data de processamento [nItem:nnn]';
      878: Result := 'Rejei��o: Endere�o do site da UF da Consulta por chave de acesso diverge do previsto';
      879: Result := 'Rejei��o: Informado item �Produzido em Escala N�O Relevante� e n�o informado CNPJ do Fabricante [nItem:nnn]';
      880: Result := 'Rejei��o: Percentual de FCP igual a zero [nItem: nnn]';
      881: Result := 'Rejei��o: Percentual de FCP ST igual a zero [nItem: nnn]';
      882: Result := 'Rejei��o: GTIN (cEAN) com prefixo inv�lido [nItem:999]';
      883: Result := 'Rejei��o: GTIN (cEAN) sem informa��o [nItem:999]';
      884: Result := 'Rejei��o: GTIN da unidade tribut�vel (cEANTrib) com prefixo inv�lido [nItem:999]';
      885: Result := 'Rejei��o: GTIN informado, mas n�o informado o GTIN da unidade tribut�vel [nItem:999]';
      886: Result := 'Rejei��o: GTIN da unidade tribut�vel informado, mas n�o informado o GTIN [nItem:999]';
      887: Result := 'Rejei��o: Informado GTIN de agrupamento de produtos homog�neos (GTIN-14) no GTIN da unidade tribut�vel [nItem:999]';
      888: Result := 'Rejei��o: GTIN da unidade tribut�vel (cEANTrib) sem informa��o [nItem:999]';
      889: Result := 'Rejei��o: Obrigat�ria a informa��o do GTIN para o produto [nItem:999]';
      890: Result := 'Rejei��o: GTIN inexistente no Cadastro Centralizado de GTIN (CCG) [nItem:999]';
      891: Result := 'Rejei��o: GTIN incompat�vel com a NCM [nItem:999; NCM esperada: 99999999]';
      892: Result := 'Rejei��o: GTIN incompat�vel com CEST [nItem:999; CEST esperado: 9999999]';
      893: Result := 'Rejei��o: GTIN da unidade tribut�vel diverge do GTIN Contido cadastrado no CCG [nItem:999; GTIN Contido esperado: 99999999999999]';
      894: Result := 'Rejei��o: GTIN da unidade tribut�vel inexistente no Cadastro Centralizado de GTIN (CCG) [nItem:999]';
      895: Result := 'Rejei��o: GTIN da unidade tribut�vel incompat�vel com a NCM [nItem:999; NCM esperada: 99999999]';
      896: Result := 'Rejei��o: GTIN da unidade tribut�vel incompat�vel com CEST [nItem:999; CEST esperado: 9999999]';
      897: Result := 'Rejei��o: Valor Fatura maior que Valor Total da NF-e';
      898: Result := 'Rejei��o: Data de vencimento da parcela n�o informada ou menor que Data de Autoriza��o [nOcor:999]';
      899: Result := 'Rejei��o: Informado incorretamente o campo meio de pagamento';
      900: Result := 'Rejei��o: Data de vencimento da parcela n�o informada ou menor que Data de Emiss�o [nOcor:999]';
      901: Result := 'Rejei��o: Valor do Desconto da Fatura maior que o Valor Original da Fatura';
      902: Result := 'Rejei��o: Valor Liquido da Fatura difere do Valor Original menos o Valor do Desconto';
      903: Result := 'Rejei��o: Vers�o informada no QR-Code (�100�) n�o � mais v�lida para a data de emiss�o';
      904: Result := 'Rejei��o: Informado indevidamente campo valor de pagamento';
      905: Result := 'Rejei��o: Campos do grupo Fatura n�o informados';
      906: Result := 'Rejei��o: N�o informado campo obrigat�rio quando CST = 60 ou CSOSN=500 e opera��o com consumidor final';
      907: Result := 'Rejei��o: N�o informado campo obrigat�rio quando CST = 60 ou CSOSN=500 e opera��o com consumidor final';
      908: Result := 'Rejei��o: N�o informado campo obrigat�rio quando CST = 60 ou CSOSN=500 e opera��o com consumidor final';
      909: Result := 'Rejei��o: N�o informado campo obrigat�rio quando CST = 60 ou CSOSN=500 e opera��o com consumidor final';
      910: Result := 'Rejei��o: Chave de Acesso NF-e Substituta inv�lida (<nome do campo>)';
      911: Result := 'Rejei��o: Chave de Acesso NF-e Substituta incorreta (<nome do campo>)';
      912: Result := 'Rejei��o: NF-e Substituta inexistente';
      913: Result := 'Rejei��o: NF-e Substituta Denegada ou Cancelada';
      914: Result := 'Rejei��o: Data de emiss�o da NF-e Substituta maior que 2 horas da data de emiss�o da NFe a ser cancelada';
      915: Result := 'Rejei��o: Valor total da NF-e Substituta difere do valor da NF-e a ser cancelada';
      916: Result := 'Rejei��o: Valor total do ICMS da NF-e Substituta difere do valor da NF-e a ser cancelada';
      917: Result := 'Rejei��o: Identifica��o do destinat�rio da NF-e Substituta difere da identifica��o do destinat�rio da NF-e a ser cancelada.';
      918: Result := 'Rejei��o: Quantidade de itens da NF-e Substituta difere da quantidade de itens da NF-e a ser cancelada.';
      919: Result := 'Rejei��o: Item da NF-e Substituta difere do mesmo item da NF-e a ser cancelada.';
      920: Result := 'Rejei��o: Tipo de Emiss�o inv�lido no Cancelamento por Substitui��o';

      922: Result := 'Rejei��o: Contranota de Produtor s� pode referenciar NF-e ou NF de Produtor Modelo 4';
      923: Result := 'Rejei��o: Referenciado documento de opera��o interna em opera��o interestadual ou com o exterior';
      924: Result := 'Rejei��o: Informado Cupom Fiscal referenciado.';
      925: Result := 'Rejei��o: NF-e com identifica��o de estrangeiro e inscri��o estadual informada para destinat�rio';
      926: Result := 'Rejei��o: Opera��o com Exterior e pa�s de destino igual a Brasil.';
      927: Result := 'Rejei��o: N�mero do item fora da ordem sequencial.';
      928: Result := 'Rejei��o: Informado c�digo de benef�cio fiscal para CST sem benef�cio fiscal [nItem: nnn]';
      929: Result := 'Rejei��o: Informado CST de diferimento sem as informa��es de diferimento [nItem: nnn]';
      930: Result := 'Rejei��o: CST com benef�cio fiscal e n�o informado o c�digo de benef�cio fiscal [nItem: nnn]';
      931: Result := 'Rejei��o: CST n�o corresponde ao tipo de c�digo de benef�cio fiscal [nItem: nnn]';
      932: Result := 'Rejei��o: Informada modalidade de determina��o da BC da ST como MVA e n�o informado o campo pMVAST [nItem: nnn]';
      933: Result := 'Rejei��o: Informada modalidade de determina��o da BC da ST diferente de MVA e informado o campo pMVAST [nItem: nnn]';
      934: Result := 'Rejei��o: N�o informado valor do ICMS desonerado ou o Motivo de desonera��o [nItem: nnn]';
      935: Result := 'Rejei��o: Valor total da Base de C�lculo superior ao valor limite estabelecido [Valor Limite: R$ XXX.XXX,XX] (valor definido pela UF)';
      936: Result := 'Rejei��o: Raz�o Social do emitente diverge do informado no cadastro da SEFAZ';
      937: Result := 'Rejei��o: CFOP inv�lido para opera��o com consumidor final';
      938: Result := 'Rejei��o: N�o informada vBCSTRet, pST, vICMSSubstituto e vICMSSTRet [nItem: 999]';
      939: Result := 'Rejei��o: Pedido de Cancelamento para NF-e com evento de Averba��o para Exporta��o';
      940: Result := 'Rejei��o: Pedido de Cancelamento para NF-e com evento Financeiro';
      941: Result := 'Rejei��o: N�mero do Regime especial inv�lido.';
      942: Result := 'Rejei��o: IE do local de retirada n�o cadastrada';
      943: Result := 'Rejei��o: IE do local de retirada n�o vinculada ao CNPJ';
      944: Result := 'Rejei��o: IE do local de retirada n�o vinculada ao CPF';
      945: Result := 'Rejei��o: IE do local de entrega n�o cadastrada';
      946: Result := 'Rejei��o: Informado c�digo de benef�cio fiscal incorreto ou inexistente na UF [nItem: nnn';
      947: Result := 'Rejei��o: IE do local de entrega n�o vinculada ao CNPJ';
      948: Result := 'Rejei��o: IE do local de entrega n�o vinculada ao CPF';
      949: Result := 'Rejei��o: NFC-e sem preenchimento das Informa��es Adicionais de Interesse do Fisco';
      950: Result := 'Rejei��o: Informa��es Adicionais de Interesse do Fisco abaixo do tamanho m�nimo exigido pela UF';
      951: Result := 'Rejei��o: Chave de Acesso referenciada com c�digo num�rico zerado n�o permitida para finalidade diferente de normal. [nOcor:nnn]';
      952: Result := 'Rejei��o: Chave de Acesso referenciada com a mesma Chave Natural da Nota Fiscal atual [nOcor:nnn]';
      //953: Result := 'Rejei��o: Informado ECF referenciado para CFOP 5.929 em UF que n�o permite essa refer�ncia';
      953: Result := 'Rejei��o: Chave de Acesso referenciada com tipo de emiss�o inv�lido [nOcor:nnn]';
      954: Result := 'Rejei��o: Chave de Acesso referenciada n�o permitida para esta opera��o[nOcor:nnn]';
      955: Result := 'Rejei��o: Chave de Acesso referenciada com c�digo num�rico n�o zerado [nOcor:nnn]';
      956: Result := 'Rejei��o: Chave de Acesso referenciada com c�digo num�rico zerado n�o permitida na UF [nOcor:nnn]';

      970: Result := 'Rejei��o: C�digo de Pa�s inexistente [local de retirada/entrega]';
      971: Result := 'Rejei��o: IE inv�lida [local de retirada/entrega]';
      972: Result := 'Rejei��o: Obrigat�ria as informa��es do respons�vel t�cnico';
      973: Result := 'Rejei��o: CNPJ do respons�vel t�cnico inv�lido';
      974: Result := 'Rejei��o: CNPJ do respons�vel t�cnico diverge do cadastrado';
      975: Result := 'Rejei��o: Obrigat�ria a informa��o do identificador do CSRT e do Hash do CSRT';
      976: Result := 'Rejei��o: Identificador do CSRT n�o cadastrado na SEFAZ';
      977: Result := 'Rejei��o: Identificador do CSRT revogado';
      978: Result := 'Rejei��o: Hash do CSRT diverge do calculado';
      //
      999: Result := 'Rejei��o: Erro n�o catalogado';
      //
      else Result := CO_STATUS_NFE_INDEFINIDO;
    end;
  end;
end;

function TNFeXMLGerencia.TipoXML(NoStandAlone: Boolean): String;
begin
  //  at� a versao 2.0 n�o mudou! Parei aqui
  // <?xml version="1.0" encoding="UTF-8"?>
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

function TNFeXMLGerencia.VerificaChaveAcesso(Chave: String; PermiteNulo: Boolean): Boolean;
begin
  Result := XXe_PF.VerificaChaveAcessoXXe(Chave, PermiteNulo);
end;

function TNFeXMLGerencia.XML_DistribuiCartaDeCorrecao(const VersaoCCe: String;
  const XML_Evento, XML_Protocolo: String;
  var XML_Distribuicao: String): Boolean;
var
  TagRaiz: String;
begin
  TagRaiz := 'procEventoNFe';
  XML_Distribuicao := TipoXML(False) +
    '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/nfe" versao="' +
    VersaoCCe + '">' + XML_Evento + XML_Protocolo + '</' + TagRaiz + '>';
  Result := True;
end;

function TNFeXMLGerencia.XML_DistribuiEvento(const Id: String;
  const Status: Integer; const XML_Env, XML_Ret: String;
  const Protocolar: Boolean; var XML_Distribuicao: String;
  const Versao: Double; const InfoLocal: String): Boolean;
  //
  procedure MostraMensagem(Msg, Id: String);
  var
    Mensagem: String;
  begin
    Mensagem := Msg + ' ' + ID + ChaveDeAcessoDesmontada(ID, 2.00);
    Geral.MB_Aviso(Mensagem);
  end;
var
  TagRaiz: String;
begin
  XML_Distribuicao := '';
  TagRaiz := 'procEventoNFe';
  Result := False;
  if (Trim(XML_Env) = '')
  or (Trim(XML_Ret) = '') then
  begin
    MostraMensagem('Evento sem XML ' + InfoLocal + ':', Id);
    Exit;
  end else
  begin
    XML_Distribuicao := TipoXML(False) +
      '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/nfe" versao="' +
      MLAGeral.MudaPontuacao(FormatFloat('0.00', Versao), '.', '.') + '">' +
      XML_Env + XML_Ret + '</' + TagRaiz + '>';
    Result := True;
  end;
end;

function TNFeXMLGerencia.XML_DistribuiNFe(const Id: String;
  const Status: Integer; const XML_NFe, XML_Aut, XML_Can: String;
  const Protocolar: Boolean; var XML_Distribuicao: String;
  const VersaoNFe: Double; const InfoLocal: String): Boolean;
  //
  procedure MostraMensagem(Msg, Id: String; Mostra: Boolean = True);
  var
    Mensagem: String;
  begin
    Mensagem := Msg + ' ' + ID + ChaveDeAcessoDesmontada(ID, VersaoNFe);
    Geral.MensagemBox(Mensagem, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  P: Integer;
  NFeXML, ProtXML, TagRaiz: String;
begin
  XML_Distribuicao := '';
  TagRaiz := 'nfeProc';
  Result := False;
  if Trim(XML_NFe) = '' then
  begin
    MostraMensagem('NFe sem XML ' + InfoLocal + ':', Id);
    Exit;
  end else begin
    P := pos('<infNFe', XML_NFe);
    if P = 0 then
    begin
      MostraMensagem('NFe sem XML v�lido ' + InfoLocal + ':', Id);
      Exit;
    end;
    NFeXML := Copy(XML_NFe, P);
    if Protocolar then
    begin
      if XXe_PF.XXeEstahAutorizada(Status) then
      begin
        TagRaiz := 'nfeProc';
        if Trim(XML_Aut) = '' then
        begin
          MostraMensagem('NFe autorizada sem XML de autoriza��o ' + InfoLocal + ':', Id);
          Exit;
        end;
        ProtXML := XML_Aut;
      end else
      if XXe_PF.XXeEstahCancelada(Status) then
      begin
        TagRaiz := 'procCancNFe';
        if Trim(XML_Can) = '' then
        begin
          if not DmNFe_0000.ReopenLocEve(Id, NFe_CodEventoCan) then
          begin
            MostraMensagem('NFe cancelada sem XML de cancelamento ' + InfoLocal + ':', Id, False);
            Exit;
          end;
        end else
          ProtXML := XML_Can;
      end else
      begin
        TagRaiz := '??NFe??';
        ProtXML := '';
        MostraMensagem('NFe sem XML de autoriza��o ou cancelamento:', Id);
        //Exit;
      end;
    end else ProtXML := '';
    //
    XML_Distribuicao := TipoXML(False) +
      '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/nfe" versao="' +
      MLAGeral.MudaPontuacao(FormatFloat('0.00', VersaoNFe), '.', '.') + '">' +
      '<NFe xmlns="http://www.portalfiscal.inf.br/nfe">' +
      NfeXML + ProtXML + '</' + TagRaiz + '>';
    Result := True;
  end;
end;

end.
