object FmNFe_DownXML: TFmNFe_DownXML
  Left = 339
  Top = 185
  Caption = 'NFe-PESQU-005 :: Download de XML de NFe'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Download de XML de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Download de XML de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Download de XML de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 85
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object SbPasta: TSpeedButton
        Left = 744
        Top = 18
        Width = 25
        Height = 25
        Caption = '...'
        OnClick = SbPastaClick
      end
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Diret'#243'rio:'
      end
      object Label2: TLabel
        Left = 12
        Top = 44
        Width = 64
        Height = 13
        Caption = 'Arquivo XML:'
      end
      object EdDiretorio: TdmkEdit
        Left = 12
        Top = 20
        Width = 729
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'C:\_Sincro\_MLArend\Clientes\Colosso - Brasinha\NFes emitidas de' +
          ' 2022 02\Lidos\'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'C:\_Sincro\_MLArend\Clientes\Colosso - Brasinha\NFes emitidas de' +
          ' 2022 02\Lidos\'
        ValWarn = False
      end
      object EdArquivo: TdmkEdit
        Left = 12
        Top = 60
        Width = 729
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '[100].[001].[130].xml'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '[100].[001].[130].xml'
        ValWarn = False
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 85
      Width = 812
      Height = 382
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Avisos e mensagens'
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 804
          Height = 354
          Align = alClient
          BevelOuter = bvNone
          Caption = 'PnMensagens'
          ParentBackground = False
          TabOrder = 0
          ExplicitTop = 85
          ExplicitWidth = 812
          ExplicitHeight = 382
          object Label4: TLabel
            Left = 0
            Top = 0
            Width = 40
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Alertass:'
          end
          object Label3: TLabel
            Left = 0
            Top = 160
            Width = 58
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Mensagens:'
          end
          object MeWarn: TMemo
            Left = 0
            Top = 13
            Width = 804
            Height = 147
            Align = alTop
            ReadOnly = True
            TabOrder = 0
          end
          object MeInfo: TMemo
            Left = 0
            Top = 173
            Width = 804
            Height = 181
            Align = alClient
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Tags ausentes no XML '
        ImageIndex = 1
        object MeAusentes: TMemo
          Left = 0
          Top = 0
          Width = 804
          Height = 354
          Align = alClient
          ReadOnly = True
          TabOrder = 0
          ExplicitTop = 4
          ExplicitHeight = 147
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrStqaLocIts: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 384
    Top = 284
  end
  object DsStqaLocIts: TDataSource
    DataSet = QrStqaLocIts
    Left = 384
    Top = 340
  end
end
