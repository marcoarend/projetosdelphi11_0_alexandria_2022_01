unit NFeCntngnc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEditCB, dmkRadioGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmNFeCntngnc = class(TForm)
    PnDados: TPanel;
    DsNFeCntngnc: TDataSource;
    QrNFeCntngnc: TmySQLQuery;
    PnEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label10: TLabel;
    EddhEntrada: TdmkEdit;
    RGtpEmis: TdmkRadioGroup;
    QrNFeCntngncCodigo: TIntegerField;
    QrNFeCntngncNome: TWideStringField;
    QrNFeCntngncEmpresa: TIntegerField;
    QrNFeCntngnctpEmis: TIntegerField;
    QrNFeCntngncdhEntrada: TDateTimeField;
    QrNFeCntngncdhSaida: TDateTimeField;
    QrNFeCntngncEmissoes: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    QrNFeCntngncNO_ENT: TWideStringField;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    QrNFeCntngncENCERROU_TXT: TWideStringField;
    QrCTG: TmySQLQuery;
    QrCTGEmpresa: TIntegerField;
    QrCTGtpEmis: TIntegerField;
    QrCTGdhSaida: TDateTimeField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeCntngncAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeCntngncBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrNFeCntngncCalcFields(DataSet: TDataSet);
    procedure DBEdit3Change(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure AtualizaFiliais(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmNFeCntngnc: TFmNFeCntngnc;

const
  FFormatFloat = '00000';
  SContingeciaAberta = 'EM CONTING�NCIA';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeCntngnc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeCntngnc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeCntngncCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeCntngnc.DefParams;
begin
  VAR_GOTOTABELA := 'nfecntngnc';
  VAR_GOTOMYSQLTABLE := QrNFeCntngnc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ');
  VAR_SQLx.Add('ctg.*');
  VAR_SQLx.Add('FROM nfecntngnc ctg');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=ctg.Empresa');
  VAR_SQLx.Add('WHERE ctg.Codigo > 0');
  VAR_SQLx.Add('AND ctg.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND ctg.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ctg.Nome Like :P0');
  //
end;

procedure TFmNFeCntngnc.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible := True;
      GBDados.Visible := True;
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBDados.Visible := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeCntngnc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeCntngnc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeCntngnc.DBEdit3Change(Sender: TObject);
begin
  if DBEdit3.Text = SContingeciaAberta then
  begin
    DBEdit3.Font.Color := clRed;
    DBEdit3.Font.Style := [];//[fsBold];
  end else begin
    DBEdit3.Font.Color := clWindowText;
    DBEdit3.Font.Style := [];
  end;
end;

procedure TFmNFeCntngnc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeCntngnc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeCntngnc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeCntngnc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeCntngnc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeCntngnc.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCntngnc.AtualizaFiliais(Codigo: Integer);
var
  SQL_UPDATE, SQL_WHERE: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTG, Dmod.MyDB, [
  'SELECT Empresa, tpEmis, dhSaida',
  'FROM nfecntngnc',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
  //
  if QrCTGdhSaida.Value > 2 then
    SQL_UPDATE := '1'
  else
    SQL_UPDATE := FormatFloat('0', QrCTGtpEmis.Value);
  begin
    if QrCTGEmpresa.Value <> 0 then
      SQL_WHERE := 'WHERE Codigo=' + FormatFloat('0', QrCTGEmpresa.Value)
    else
      SQL_WHERE := '';
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE paramsemp SET ',
    'NFetpEmis=' + SQL_UPDATE,
    SQL_WHERE,
    '']);
  end;
end;

procedure TFmNFeCntngnc.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrNFeCntngnc, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'nfecntngnc');
end;

procedure TFmNFeCntngnc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeCntngncCodigo.Value;
  Close;
end;

procedure TFmNFeCntngnc.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if MyObjects.FIC(Length(EdNome.Text) < 15, EdNome,
  'Defina uma descri��o com no m�nimo 15 caracteres!') then
    Exit;
  //
  if MyObjects.FIC(RgtpEmis.ItemIndex < 2, RGtpEmis,
  'O tipo de emiss�o deve ser de conting�ncia!') then
    Exit;
  //
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfecntngnc', 'Codigo', [], [],
    ImgTipo.SQLType, EdCodigo.ValueVariant, siPositivo, EdCodigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeCntngnc, GBEdita,
  'nfecntngnc', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    AtualizaFiliais(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeCntngnc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'nfecntngnc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfecntngnc', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeCntngnc.BtExcluiClick(Sender: TObject);
var
  dhSaida: String;
  Codigo: Integer;
begin
  dhSaida :=  Geral.FDT(DModG.ObtemAgora(), 109);
  if Geral.MensagemBox('Confirma a sa�da de conting�ncia?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo := QrNFeCntngncCodigo.Value;
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE nfecntngnc SET ',
    'dhSaida="' + dhSaida + '"',
    'WHERE Codigo=' + FormatFloat('0', Codigo),
    '']);
    //
    AtualizaFiliais(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeCntngnc.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, GBEdita, QrNFeCntngnc, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'nfecntngnc');
  EddhEntrada.ValueVariant :=  DModG.ObtemAgora();
end;

procedure TFmNFeCntngnc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmNFeCntngnc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeCntngncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeCntngnc.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeCntngnc.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmNFeCntngnc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeCntngnc.QrNFeCntngncAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeCntngnc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCntngnc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeCntngncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfecntngnc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeCntngnc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCntngnc.QrNFeCntngncBeforeOpen(DataSet: TDataSet);
begin
  QrNFeCntngncCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeCntngnc.QrNFeCntngncCalcFields(DataSet: TDataSet);
begin
  if QrNFeCntngncdhSaida.Value = 0 then
    QrNFeCntngncENCERROU_TXT.Value := SContingeciaAberta
  else
    QrNFeCntngncENCERROU_TXT.Value := Geral.FDT(QrNFeCntngncdhSaida.Value, 0);
end;

end.

