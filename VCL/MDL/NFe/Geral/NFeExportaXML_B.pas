unit NFeExportaXML_B;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  UnDmkProcFunc, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmNFeExportaXML_B = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrDest: TmySQLQuery;
    QrDestCodigo: TIntegerField;
    QrDestNO_ENT: TWideStringField;
    QrDestTipo: TSmallintField;
    QrDestCNPJ_CPF: TWideStringField;
    DsDest: TDataSource;
    Label4: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    SbRaiz: TSpeedButton;
    Label5: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdDest: TdmkEditCB;
    CBDest: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    CGStatus: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    CkProt: TCheckBox;
    CkUmArqPorNFe: TCheckBox;
    CkZipar: TCheckBox;
    EdDirNFeAss: TEdit;
    EdArq: TEdit;
    Panel5: TPanel;
    ListBox1: TListBox;
    Label6: TLabel;
    EdDirProRec: TEdit;
    SpeedButton1: TSpeedButton;
    EdMes: TdmkEdit;
    LaMes: TLabel;
    ListBox2: TListBox;
    Label7: TLabel;
    EdRaiz: TEdit;
    Label2: TLabel;
    TPIni: TdmkEditDateTimePicker;
    Label3: TLabel;
    TPFim: TdmkEditDateTimePicker;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    EdVersao: TdmkEdit;
    Memo1: TMemo;
    EdDirNFeCan: TEdit;
    Label9: TLabel;
    PnA: TPanel;
    ListBox3: TListBox;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbRaizClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    function  PesquisaECarregaListaDeArqsNFe(): Boolean;
    procedure ObtemDiretoriosDaFilial(Empresa: Integer);
  public
    { Public declarations }
  end;

  var
  FmNFeExportaXML_B: TFmNFeExportaXML_B;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, NFeXMLGerencia, ZForge;

{$R *.DFM}

procedure TFmNFeExportaXML_B.BtOKClick(Sender: TObject);
var
  //Lote,
  DirRaiz, DirCria, ID, Chave, Prot, Buf, XML_Aut, XML_NFe, XML_Can: String;
  I, J, P, Ini, Fim, Status: Integer;
  MeuXMLProtocolado, MeuXMLCancelado, XML_Distribuicao, CamZip, DirMsg: String;
  mTexto: TStringList;
  Continua, Gerou: Boolean;
begin
  if PesquisaECarregaListaDeArqsNFe() then
  begin
    Screen.Cursor := crHourGlass;
    //
    DirRaiz := EdRaiz.Text;
    if not Geral.VerificaDir(DirRaiz, '\',
    'Diret�rio raiz da exporta��o de XML de NFe', True) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    DirCria := DirRaiz + EdArq.Text;
    if DirectoryExists(DirCria) then
    begin
      if dmkPF.GetAllFiles(True, DirCria + '\*.*', nil, False) > 0 then
      begin
        Geral.MensagemBox('A��o abortada! Pasta j� existe!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    Geral.VerificaDir(DirCria, '\', 'Diret�rio da exporta��o de XML de NFe', True);
    //
    try
      ListBox1.Clear;
      dmkPF.GetAllFiles(False(*CkSub1.Checked*), EdDirProRec.Text+'\'+
      '*.xml'(*MeExtensoes.Lines[I]*), ListBox1, False);
    finally
      Screen.Cursor := crDefault;
    end;
    //
    try
      ListBox3.Clear;
      dmkPF.GetAllFiles(False(*CkSub1.Checked*), EdDirNFeCan.Text+'\'+
      '*.xml'(*MeExtensoes.Lines[I]*), ListBox3, False);
    finally
      Screen.Cursor := crDefault;
    end;
    //
    for J := 0 to ListBox2.Items.Count -2 do
    begin
      XML_Can := '';
      XML_NFe := '';
      XML_Aut := '';
      //
      ID := Copy(ExtractFileName(ListBox2.Items[J]), 1, 44);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, ID);
      Chave := '<chNFe>' + ID + '</chNFe>';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(ListBox2.Items[J]);
      XML_NFe := mTexto.Text;
      mTexto.Free;
      //
      Gerou := False;
      //
      // Canceladas
      for I := 0 to ListBox3.Items.Count -2 do
      begin
        if Copy(ExtractFileName(ListBox3.Items[I]), 1, 44) = ID then
        begin
          //
          mTexto := TStringList.Create;
          mTexto.Clear;
          mTexto.LoadFromFile(ListBox3.Items[I]);
          MeuXMLCancelado := mTexto.Text;
          P := Pos(Chave, MeuXMLProtocolado);
          if P > 0  then
          begin
            if  Pos('<cStat>101</cStat>', MeuXMLCancelado) > 0 then
            begin
              Memo1.Lines.Add(ListBox3.Items[I]);
              //
              Ini := pos('<retCancNFe', MeuXMLCancelado);

              XML_Can := Copy(MeuXMLCancelado, Ini);
              Status := 101;
              //
              Continua := False;
              if NFeXMLGeren.XML_DistribuiNFe(ID, Status,
              XML_NFe, XML_Aut, XML_Can, True, XML_Distribuicao,
              EdVersao.ValueVariant, 'na pasta selecionada') then
                Continua :=
                  NFeXMLGeren.SalvaXML(DirCria + ID + '.xml', XML_Distribuicao);
              if not Continua then
              begin
                Screen.Cursor := crDefault;
                Exit;
              end else
                Gerou := True;
            end;
          end;
        end;
      end;
      // Lotes processados
      if Trim(XML_Can) = '' then
      begin
        for I := 0 to ListBox1.Count - 1 do
        begin
          if Gerou = False then
          begin
            MyObjects.Informa2(LaAviso1, LaAviso2, True, ID + ' > verificando no lote: ' + ExtractFileName(ListBox1.Items[I]));
            //
            mTexto := TStringList.Create;
            mTexto.Clear;
            mTexto.LoadFromFile(ListBox1.Items[I]);
            MeuXMLProtocolado := mTexto.Text;
            P := Pos(Chave, MeuXMLProtocolado);
            if P > 0  then
            begin
              Memo1.Lines.Add(ListBox1.Items[I]);
              buf := MeuXMLProtocolado;
              Ini := pos('<protNFe', buf);
              while Ini > 0 do
              begin
                Prot := '';
                Geral.SeparaPrimeiraOcorrenciaDeTexto('<protNFe', buf, Prot, buf);
                if Prot <> '' then
                begin
                  Fim := pos('</protNFe>', buf);
                  XML_Aut := '<' + Copy(buf, 1, Fim + Length('</protNFe>') - 1);
                  if (pos(Chave, XML_Aut) > 0) and (pos('<cStat>100</cStat>', XML_Aut) > 0) then
                  begin
                    MyObjects.Informa2(LaAviso1, LaAviso2, True, ID + ' > Gerando XML protocolado!');
                    Status := 100;
                    //
                    Continua := False;
                    if NFeXMLGeren.XML_DistribuiNFe(ID, Status,
                    XML_NFe, XML_Aut, XML_Can, True, XML_Distribuicao,
                    EdVersao.ValueVariant, 'na pasta selecionada') then
                    //
                    Continua :=
                      NFeXMLGeren.SalvaXML(DirCria + ID + '.xml', XML_Distribuicao);
                    if not Continua then
                    begin
                      Screen.Cursor := crDefault;
                      Exit;
                    end else
                      Gerou := True;
                  end;
                end;
                Ini := pos('<protNFe', buf);
              end;
            end;
            mTexto.Free;
          end;
        end;
        if Gerou = False then
          Geral.MB_Aviso('N�o foi localizado o protocolo da chave:' +
            sLineBreak + ID + sLineBreak + 'Verifique se ela realmente foi enviada ao fisco!'
            + sLineBreak + 'A NFe pode ter sido enviado com outra chave!');
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Zipando arquivo!');
    //
    (*
    //Zipar
    if CkZipar.Checked then
    begin
      MLAGeral.CompactaArquivo(DirCria, '*.*', DirRaiz, EdArq.Text+'.zip', False);
      Geral.MensagemBox('Exporta��o finalizada!' + #13#10 + 'Arquivo: ' + #13#10 +
      dmkPF.CaminhoArquivo(DirRaiz, EdArq.Text, 'zip'), 'Aviso', MB_OK+MB_ICONWARNING);
    end else
    *)
    //ZIPAR N�o usa o WinZip
    if CkZipar.Checked then
    begin
      Application.CreateForm(TFmZForge, FmZForge);
      FmZForge.Show;
      CamZip := FmZForge.ZipaArquivo(zftDiretorio, ExtractFileDir(DirRaiz), DirCria,
          EdArq.Text, '', False, False);
      FmZForge.Destroy;
      //
      if Length(CamZip) = 0 then
      begin
        Geral.MensagemBox('Falha ao zipar arquivo!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;

    if CkZipar.Checked then
      DirMsg := CamZip
    else
      DirMsg := DirCria;
    //
    Geral.MB_Aviso('Exporta��o finalizada!' + sLineBreak + 'Diret�rio: ' + sLineBreak +
      DirMsg);
    //
    if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
      Geral.AbreArquivo(DirMsg);
    //
  end else Geral.MB_Aviso('Nenhuma NF-e foi localizada para a pesquisa!');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmNFeExportaXML_B.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeExportaXML_B.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if Empresa <> 0 then
    ObtemDiretoriosDaFilial(DModG.QrEmpresasCodigo.Value);
end;

procedure TFmNFeExportaXML_B.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeExportaXML_B.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  TPIni.Date             := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPFim.Date             := Geral.PrimeiroDiaDoMes(Date) -1;
  CGStatus.Value         := 1;
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
  EdArq.Text             := FormatDateTime('YYYYMMDD"_"HHNNSS', Now());
  //
  ObtemDiretoriosDaFilial(DmodG.QrFiliLogCodigo.Value);
  //
  UnDmkDAC_PF.AbreQuery(QrDest, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmNFeExportaXML_B.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeExportaXML_B.ObtemDiretoriosDaFilial(Empresa: Integer);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DirNFeAss, DirProRec, DirCan, versao ',
      'FROM paramsemp ',
      'WHERE Codigo=' + Geral.FF0(Empresa),
      '']);
    if Qry.RecordCount > 0 then
    begin
      EdVersao.ValueVariant := Qry.FieldByName('versao').AsFloat;
      EdDirNFeAss.Text      := Qry.FieldByName('DirNFeAss').AsString;
      EdDirProRec.Text      := Qry.FieldByName('DirProRec').AsString;
      EdDirNFeCan.Text      := Qry.FieldByName('DirCan').AsString;
    end else
    begin
      EdVersao.ValueVariant := '';
      EdDirNFeAss.Text      := '';
      EdDirProRec.Text      := '';
      EdDirNFeCan.Text      := '';
    end;
  finally
    Qry.Free;
  end;
end;

function TFmNFeExportaXML_B.PesquisaECarregaListaDeArqsNFe(): Boolean;
var
  CNPJ, AAMM, Arq: String;
  P, T, I, AM, CC: Integer;
  MeuXMLAssinado: String;
  mTexto: TStringList;
begin
  Result := False;
  if MyObjects.FIC(Trim(EdMes.Text) = '', EdMes, 'Informe o m�s de emiss�o!') then
    Exit;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe o emitente!') then
    Exit;
{
  if MyObjects.FIC(EdDest.ValueVariant = 0, EdEmpresa, 'Informe o emitente!') then
    Exit;
}
  Screen.Cursor := crHourGlass;
  try
    ListBox2.Clear;
    ListBox1.Clear;
    Memo1.Lines.Clear;
    dmkPF.GetAllFiles(False(*CkSub1.Checked*), EdDirNFeAss.Text+'\'+
    '*.xml'(*MeExtensoes.Lines[I]*), ListBox1, False);
  finally
    Screen.Cursor := crDefault;
  end;

  //

  AAMM := FormatDateTime('YYMM', EdMes.ValueVariant);

  for I := 0 to ListBox1.Count - 1 do
  begin
    Arq := ExtractFileName(ListBox1.Items[I]);
    AM := Pos(AAMM, Arq);
    CC := Pos(DModG.QrEmpresasCNPJ_CPF.Value, Arq);
    if (AM = 3) and (CC = 7) then
    begin
      CNPJ := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(ListBox1.Items[I]);
      MeuXMLAssinado := mTexto.Text;
      P := Pos('<dest><CNPJ>', MeuXMLAssinado);
      if P > 0  then
      begin
        T := Length('<dest><CNPJ>');
        CNPJ := Copy(MeuXMLAssinado, P + T, 14);
      end else
      begin
        //T := Length('<dest><CPF>');
        P := Pos('<dest><CPF>', MeuXMLAssinado);
        CNPJ := Copy(MeuXMLAssinado, P, 11);
      end;
      if (CNPJ = QrDestCNPJ_CPF.Value) or (EdDest.ValueVariant = 0) then
      begin
        Memo1.Lines.Add(CNPJ);
        ListBox2.Items.Add(ListBox1.Items[I]);
      end;
      mTexto.Free;
    end;
  end;
  Result := ListBox2.Items.Count > 0;
end;

procedure TFmNFeExportaXML_B.SbRaizClick(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Dir) then
    EdDirNFeAss.Text := ExtractFileDir(Dir);
end;

procedure TFmNFeExportaXML_B.SpeedButton1Click(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Dir) then
    EdDirProRec.Text := ExtractFileDir(Dir);
end;

procedure TFmNFeExportaXML_B.SpeedButton2Click(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Dir) then
    EdDirNFeCan.Text := ExtractFileDir(Dir);
end;

procedure TFmNFeExportaXML_B.SpeedButton3Click(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Dir) then
    EdRaiz.Text := ExtractFileDir(Dir);
end;

end.
