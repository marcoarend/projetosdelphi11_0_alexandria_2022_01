object FmNFeInut_0000: TFmNFeInut_0000
  Left = 368
  Top = 194
  Caption = 'NFe-INUTI-001 :: Inutiliza'#231#245'es de N'#250'meros de NF-e'
  ClientHeight = 637
  ClientWidth = 1040
  Color = clBtnFace
  Constraints.MinHeight = 341
  Constraints.MinWidth = 841
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 126
    Width = 1040
    Height = 511
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1040
      Height = 164
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 18
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 84
        Top = 5
        Width = 75
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 194
        Top = 5
        Width = 67
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Descri'#231#227'o:'
      end
      object dmkLabel1: TdmkLabel
        Left = 5
        Top = 49
        Width = 58
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 778
        Top = 49
        Width = 64
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'S'#233'rie NF:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel3: TdmkLabel
        Left = 855
        Top = 49
        Width = 86
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'N'#186' NF inicial:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel4: TdmkLabel
        Left = 945
        Top = 49
        Width = 72
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'N'#186' NF final:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel5: TdmkLabel
        Left = 5
        Top = 93
        Width = 80
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Justificativa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel6: TdmkLabel
        Left = 699
        Top = 49
        Width = 64
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Mod. NF:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label4: TLabel
        Left = 877
        Top = 5
        Width = 67
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Data:'
      end
      object SpeedButton5: TSpeedButton
        Left = 1003
        Top = 109
        Width = 24
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 5
        Top = 22
        Width = 74
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 194
        Top = 22
        Width = 677
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 84
        Top = 22
        Width = 105
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdFilial: TdmkEditCB
        Left = 5
        Top = 66
        Width = 74
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 84
        Top = 66
        Width = 610
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 5
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSerie: TdmkEdit
        Left = 778
        Top = 66
        Width = 73
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'serie'
        UpdCampo = 'serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdnNFIni: TdmkEdit
        Left = 855
        Top = 66
        Width = 85
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'nNFIni'
        UpdCampo = 'nNFIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdnNFFim: TdmkEdit
        Left = 945
        Top = 66
        Width = 84
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'nNFFim'
        UpdCampo = 'nNFFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBNFeJust: TdmkDBLookupComboBox
        Left = 84
        Top = 110
        Width = 915
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsNFeJust
        TabOrder = 11
        dmkEditCB = EdNFeJust
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNFeJust: TdmkEditCB
        Left = 5
        Top = 110
        Width = 74
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBNFeJust
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdModelo: TdmkEdit
        Left = 699
        Top = 66
        Width = 73
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '55'
        ValMax = '65'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '55'
        QryCampo = 'Modelo'
        UpdCampo = 'Modelo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 55
        ValWarn = False
      end
      object TPDataC: TdmkEditDateTimePicker
        Left = 877
        Top = 22
        Width = 152
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 40020.000000000000000000
        Time = 0.686454548609617600
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataC'
        UpdCampo = 'DataC'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 436
      Width = 1040
      Height = 75
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 1036
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 847
          Top = 0
          Width = 189
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 4
            Width = 156
            Height = 43
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 11
          Top = 4
          Width = 158
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 126
    Width = 1040
    Height = 511
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1040
      Height = 138
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 5
        Top = 5
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label2: TLabel
        Left = 194
        Top = 5
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 84
        Top = 5
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object dmkLabel7: TdmkLabel
        Left = 5
        Top = 49
        Width = 58
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel9: TdmkLabel
        Left = 699
        Top = 49
        Width = 64
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Mod. NF:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel10: TdmkLabel
        Left = 778
        Top = 49
        Width = 64
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'S'#233'rie NF:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel11: TdmkLabel
        Left = 945
        Top = 49
        Width = 72
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'N'#186' NF final:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel12: TdmkLabel
        Left = 855
        Top = 49
        Width = 86
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'N'#186' NF inicial:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label5: TLabel
        Left = 5
        Top = 93
        Width = 114
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Justificativa cadastrada:'
      end
      object Label6: TLabel
        Left = 877
        Top = 5
        Width = 67
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Data:'
      end
      object DBEdit1: TDBEdit
        Left = 84
        Top = 22
        Width = 105
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CodUsu'
        DataSource = DsNFeInut
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 5
        Top = 66
        Width = 74
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Empresa'
        DataSource = DsNFeInut
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 84
        Top = 66
        Width = 611
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Empresa'
        DataSource = DsNFeInut
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 699
        Top = 66
        Width = 73
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'modelo'
        DataSource = DsNFeInut
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 778
        Top = 66
        Width = 73
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Serie'
        DataSource = DsNFeInut
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 855
        Top = 66
        Width = 85
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'nNFIni'
        DataSource = DsNFeInut
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 945
        Top = 66
        Width = 84
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'nNFFim'
        DataSource = DsNFeInut
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 84
        Top = 110
        Width = 945
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Justif'
        DataSource = DsNFeInut
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 5
        Top = 110
        Width = 74
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Justif'
        DataSource = DsNFeInut
        TabOrder = 8
      end
      object DBEdit10: TDBEdit
        Left = 877
        Top = 22
        Width = 152
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DataC'
        DataSource = DsNFeInut
        TabOrder = 9
      end
      object DBEdNome: TDBEdit
        Left = 194
        Top = 22
        Width = 677
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = DsNFeInut
        TabOrder = 10
      end
      object DBEdCodigo: TDBEdit
        Left = 5
        Top = 22
        Width = 74
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsNFeInut
        TabOrder = 11
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 138
      Width = 1040
      Height = 157
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Resposta do servidor: '
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      object Label10: TLabel
        Left = 5
        Top = 17
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vers'#227'o:'
        FocusControl = DBEdit11
      end
      object Label11: TLabel
        Left = 63
        Top = 17
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdit12
      end
      object Label12: TLabel
        Left = 494
        Top = 17
        Width = 47
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ambiente:'
        FocusControl = DBEdit13
      end
      object Label13: TLabel
        Left = 672
        Top = 17
        Width = 23
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'cUF:'
        FocusControl = DBEdit14
      end
      object Label14: TLabel
        Left = 719
        Top = 17
        Width = 22
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ano:'
        FocusControl = DBEdit15
      end
      object Label15: TLabel
        Left = 762
        Top = 17
        Width = 30
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CNPJ:'
        FocusControl = DBEdit16
      end
      object Label16: TLabel
        Left = 924
        Top = 17
        Width = 38
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo:'
        FocusControl = DBEdit17
      end
      object Label17: TLabel
        Left = 982
        Top = 17
        Width = 27
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'S'#233'rie:'
        FocusControl = DBEdit18
      end
      object Label18: TLabel
        Left = 5
        Top = 61
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#186' NF ini:'
        FocusControl = DBEdit20
      end
      object Label19: TLabel
        Left = 90
        Top = 61
        Width = 48
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#186' NF fim:'
        FocusControl = DBEdit21
      end
      object Label20: TLabel
        Left = 173
        Top = 61
        Width = 33
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Status:'
        FocusControl = DBEdit22
      end
      object Label21: TLabel
        Left = 5
        Top = 110
        Width = 81
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Motivo do status:'
        FocusControl = DBEdit23
      end
      object Label22: TLabel
        Left = 226
        Top = 61
        Width = 134
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data / hora do recebimento:'
        FocusControl = DBEdit24
      end
      object Label23: TLabel
        Left = 410
        Top = 61
        Width = 102
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#250'mero do protocolo:'
        FocusControl = DBEdit25
      end
      object DBEdit11: TDBEdit
        Left = 5
        Top = 33
        Width = 54
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'versao'
        DataSource = DsNFeInut
        TabOrder = 0
      end
      object DBEdit12: TDBEdit
        Left = 63
        Top = 33
        Width = 427
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Id'
        DataSource = DsNFeInut
        TabOrder = 1
      end
      object DBEdit13: TDBEdit
        Left = 494
        Top = 33
        Width = 37
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'tpAmb'
        DataSource = DsNFeInut
        TabOrder = 2
      end
      object DBEdit14: TDBEdit
        Left = 672
        Top = 33
        Width = 44
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'cUF'
        DataSource = DsNFeInut
        TabOrder = 3
      end
      object DBEdit15: TDBEdit
        Left = 719
        Top = 33
        Width = 38
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ano'
        DataSource = DsNFeInut
        TabOrder = 4
      end
      object DBEdit16: TDBEdit
        Left = 762
        Top = 33
        Width = 159
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CNPJ'
        DataSource = DsNFeInut
        TabOrder = 5
      end
      object DBEdit17: TDBEdit
        Left = 924
        Top = 33
        Width = 54
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'modelo'
        DataSource = DsNFeInut
        TabOrder = 6
      end
      object DBEdit18: TDBEdit
        Left = 982
        Top = 33
        Width = 47
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Serie'
        DataSource = DsNFeInut
        TabOrder = 7
      end
      object DBEdit19: TDBEdit
        Left = 535
        Top = 33
        Width = 132
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Ambiente'
        DataSource = DsNFeInut
        TabOrder = 8
      end
      object DBEdit20: TDBEdit
        Left = 5
        Top = 82
        Width = 79
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'nNFIni'
        DataSource = DsNFeInut
        TabOrder = 9
      end
      object DBEdit21: TDBEdit
        Left = 90
        Top = 82
        Width = 79
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'nNFFim'
        DataSource = DsNFeInut
        TabOrder = 10
      end
      object DBEdit22: TDBEdit
        Left = 173
        Top = 82
        Width = 48
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'cStat'
        DataSource = DsNFeInut
        TabOrder = 11
      end
      object DBEdit23: TDBEdit
        Left = 5
        Top = 127
        Width = 1025
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'xMotivo'
        DataSource = DsNFeInut
        TabOrder = 12
      end
      object DBEdit24: TDBEdit
        Left = 226
        Top = 82
        Width = 179
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'dhRecbto'
        DataSource = DsNFeInut
        TabOrder = 13
      end
      object DBEdit25: TDBEdit
        Left = 410
        Top = 82
        Width = 620
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'nProt'
        DataSource = DsNFeInut
        TabOrder = 14
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 321
      Width = 1040
      Height = 123
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet3
      Align = alBottom
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Mensagens '
        object GradeMsg: TDBGrid
          Left = 0
          Top = 0
          Width = 1032
          Height = 95
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsNFeInutMsg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' XML/BD (Texto plano)'
        ImageIndex = 1
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 1032
          Height = 95
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataField = 'XML_Inu'
          DataSource = DsNFeInut
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'XML/BD (Navegador)'
        ImageIndex = 2
        object WBResposta: TWebBrowser
          Left = 0
          Top = 0
          Width = 1032
          Height = 95
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 967
          ExplicitHeight = 87
          ControlData = {
            4C000000A96A0000D20900000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 444
      Width = 1040
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 195
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 143
          Top = 4
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 98
          Top = 4
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 54
          Top = 4
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 11
          Top = 4
          Width = 42
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 197
        Top = 15
        Width = 157
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 354
        Top = 15
        Width = 684
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 510
          Top = 0
          Width = 174
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 4
            Width = 158
            Height = 43
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtDados: TBitBtn
          Tag = 570
          Left = 5
          Top = 4
          Width = 158
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Dados'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDadosClick
        end
        object BtSolicita: TBitBtn
          Tag = 441
          Left = 167
          Top = 4
          Width = 156
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Solicita'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtSolicitaClick
        end
        object BtLeXML: TBitBtn
          Tag = 512
          Left = 331
          Top = 4
          Width = 158
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&L'#234' XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLeXMLClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1040
    Height = 68
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 977
      Top = 0
      Width = 63
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 16
        Width = 41
        Height = 42
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 284
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 11
        Width = 53
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 61
        Top = 11
        Width = 52
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 115
        Top = 11
        Width = 54
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 171
        Top = 11
        Width = 52
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 226
        Top = 11
        Width = 52
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 284
      Top = 0
      Width = 693
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 440
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Inutiliza'#231#245'es de N'#250'meros de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 440
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Inutiliza'#231#245'es de N'#250'meros de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 440
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Inutiliza'#231#245'es de N'#250'meros de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 68
    Width = 1040
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1036
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 17
        Top = 2
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeInut: TDataSource
    DataSet = QrNFeInut
    Left = 540
    Top = 492
  end
  object QrNFeInut: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeInutBeforeOpen
    AfterOpen = QrNFeInutAfterOpen
    AfterScroll = QrNFeInutAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial, ent.Nome) NO_Empresa, '
      'jus.Nome NO_Justif, inu.*'
      'FROM nfeinut inu'
      'LEFT JOIN entidades ent ON ent.Codigo=inu.Empresa'
      'LEFT JOIN nfejust jus ON jus.Codigo=inu.Justif')
    Left = 512
    Top = 492
    object QrNFeInutCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeInutCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeInutNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNFeInutEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeInutversao: TFloatField
      FieldName = 'versao'
      DisplayFormat = '0.00'
    end
    object QrNFeInutId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrNFeInuttpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeInutcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeInutano: TSmallintField
      FieldName = 'ano'
    end
    object QrNFeInutCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFeInutmodelo: TSmallintField
      FieldName = 'modelo'
    end
    object QrNFeInutSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrNFeInutnNFIni: TIntegerField
      FieldName = 'nNFIni'
    end
    object QrNFeInutnNFFim: TIntegerField
      FieldName = 'nNFFim'
    end
    object QrNFeInutJustif: TIntegerField
      FieldName = 'Justif'
    end
    object QrNFeInutxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrNFeInutcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeInutxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeInutLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeInutDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeInutDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeInutUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeInutUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeInutAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeInutAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeInutNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeInutNO_Justif: TWideStringField
      FieldName = 'NO_Justif'
      Size = 240
    end
    object QrNFeInutDataC: TDateField
      FieldName = 'DataC'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeInutNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
    object QrNFeInutdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeInutnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeInutXML_Inu: TWideMemoField
      FieldName = 'XML_Inu'
      BlobType = ftMemo
      Size = 4
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtLeXML
    CanUpd01 = BtSolicita
    Left = 568
    Top = 492
  end
  object QrNFeJust: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 676
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
  end
  object DsNFeJust: TDataSource
    DataSet = QrNFeJust
    Left = 704
    Top = 24
  end
  object dmkVUFilial: TdmkValUsu
    dmkEditCB = EdFilial
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 596
    Top = 492
  end
  object dmkVUNFEJust: TdmkValUsu
    dmkEditCB = EdNFeJust
    Panel = PainelEdita
    QryCampo = 'Justif'
    UpdCampo = 'Justif'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 624
    Top = 492
  end
  object QrNFeInutMsg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeinutmsg'
      'WHERE Codigo=:P0'
      'ORDER BY Controle DESC')
    Left = 652
    Top = 492
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeInutMsgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfeinutmsg.Codigo'
    end
    object QrNFeInutMsgControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'nfeinutmsg.Controle'
    end
    object QrNFeInutMsgversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfeinutmsg.versao'
    end
    object QrNFeInutMsgId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfeinutmsg.Id'
      Size = 30
    end
    object QrNFeInutMsgtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfeinutmsg.tpAmb'
    end
    object QrNFeInutMsgverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfeinutmsg.verAplic'
      Size = 30
    end
    object QrNFeInutMsgcStat: TIntegerField
      FieldName = 'cStat'
      Origin = 'nfeinutmsg.cStat'
    end
    object QrNFeInutMsgxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfeinutmsg.xMotivo'
      Size = 255
    end
    object QrNFeInutMsgcUF: TSmallintField
      FieldName = 'cUF'
      Origin = 'nfeinutmsg.cUF'
    end
  end
  object DsNFeInutMsg: TDataSource
    DataSet = QrNFeInutMsg
    Left = 680
    Top = 492
  end
  object PMDados: TPopupMenu
    Left = 316
    Top = 396
    object Incluinovoprocesso1: TMenuItem
      Caption = '&Inclui novo processo'
      OnClick = Incluinovoprocesso1Click
    end
    object Alteradadosdoprocessoatual1: TMenuItem
      Caption = '&Altera dados do processo atual'
      Enabled = False
      OnClick = Alteradadosdoprocessoatual1Click
    end
  end
end
