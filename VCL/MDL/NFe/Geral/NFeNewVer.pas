unit NFeNewVer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkCheckBox, dmkDBGridZTO, Variants, Vcl.Menus;

type
  TFmNFeNewVer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    LaTitulo1C: TLabel;
    QrOrigI: TmySQLQuery;
    QrOrigICodigo: TWideStringField;
    QrOrigIID: TWideStringField;
    QrOrigICampo: TWideStringField;
    QrOrigIDescricao: TWideStringField;
    QrOrigIElemento: TWideStringField;
    QrOrigIPai: TWideStringField;
    QrOrigITipo: TWideStringField;
    QrOrigIOcorMin: TSmallintField;
    QrOrigIOcorMax: TIntegerField;
    QrOrigITamMin: TSmallintField;
    QrOrigITamMax: TIntegerField;
    QrOrigITamVar: TWideStringField;
    QrOrigIDeciCasas: TSmallintField;
    QrOrigIObservacao: TWideMemoField;
    QrOrigIAlterWeb: TSmallintField;
    QrOrigIAtivo: TSmallintField;
    QrOrigILeftZeros: TSmallintField;
    QrOrigIInfoVazio: TSmallintField;
    QrOrigIFormatStr: TWideStringField;
    QrOrigIVersao: TFloatField;
    QrOrigICodigoN: TFloatField;
    QrOrigICodigoX: TWideStringField;
    DsDest: TDataSource;
    TbDest: TmySQLTable;
    TbDestCodigo: TWideStringField;
    TbDestID: TWideStringField;
    TbDestCampo: TWideStringField;
    TbDestDescricao: TWideStringField;
    TbDestElemento: TWideStringField;
    TbDestPai: TWideStringField;
    TbDestTipo: TWideStringField;
    TbDestOcorMin: TSmallintField;
    TbDestOcorMax: TIntegerField;
    TbDestTamMin: TSmallintField;
    TbDestTamMax: TIntegerField;
    TbDestTamVar: TWideStringField;
    TbDestDeciCasas: TSmallintField;
    TbDestObservacao: TWideMemoField;
    TbDestAlterWeb: TSmallintField;
    TbDestAtivo: TSmallintField;
    TbDestLeftZeros: TSmallintField;
    TbDestInfoVazio: TSmallintField;
    TbDestFormatStr: TWideStringField;
    TbDestVersao: TFloatField;
    TbDestCodigoN: TFloatField;
    TbDestCodigoX: TWideStringField;
    PB: TProgressBar;
    TbDestSubGrupo: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdVerOrig: TdmkEdit;
    EdVerDest: TdmkEdit;
    BtCopiar: TBitBtn;
    CkNaoSobrepor: TdmkCheckBox;
    BtReopen: TBitBtn;
    EdPsqGrupo: TdmkEdit;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DBGrid1: TDBGrid;
    DsNFeLayC: TDataSource;
    TbNFeLayC: TmySQLTable;
    TbNFeLayCNome: TWideStringField;
    QrOrigISubGrupo: TWideStringField;
    BtInclui: TBitBtn;
    QrOrigIGrupo: TWideStringField;
    TbNFeLayCGrupo: TWideStringField;
    TbDestGrupo: TWideStringField;
    Label4: TLabel;
    EdPsqSubGrupo: TdmkEdit;
    BtExclui: TBitBtn;
    BtDuplica: TBitBtn;
    BtTestar: TBitBtn;
    TabSheet3: TTabSheet;
    Panel8: TPanel;
    Panel9: TPanel;
    Label5: TLabel;
    EdFile: TdmkEdit;
    SbFile: TSpeedButton;
    PMTestar: TPopupMenu;
    Itensdoarquivonatabela1: TMenuItem;
    Itensdatabelanoarquivo1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    MeFile: TMemo;
    TabSheet5: TTabSheet;
    SbLoad: TSpeedButton;
    QrPsq: TMySQLQuery;
    TabSheet6: TTabSheet;
    Grade: TStringGrid;
    PageControl3: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    MeNotFound: TMemo;
    MeNaoElmt: TMemo;
    TbDestDtIniProd: TDateField;
    TbDestDtIniHomo: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCopiarClick(Sender: TObject);
    procedure BtReopenClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure Itensdoarquivonatabela1Click(Sender: TObject);
    procedure Itensdatabelanoarquivo1Click(Sender: TObject);
    procedure SbFileClick(Sender: TObject);
    procedure SbLoadClick(Sender: TObject);
  private
    { Private declarations }
    procedure CopiarAtual(Versao: Double);
    procedure RedefineBotoes();
    procedure ReopenNFeLayC();
    procedure ReopenNFeLayI();
    procedure InsUpdItem(SQLType: TSQLType);
    procedure CarregaArquivo();
  public
    { Public declarations }
  end;

  var
  FmNFeNewVer: TFmNFeNewVer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UmySQLModule, MyDBCheck, NFeLayout_Edit;

{$R *.DFM}

procedure TFmNFeNewVer.BtAlteraClick(Sender: TObject);
begin
  InsUpdItem(stUpd);
end;

procedure TFmNFeNewVer.BtCopiarClick(Sender: TObject);
var
  VerOrig, VerDest: Double;
begin
  if Geral.MB_Pergunta('Ser�o copiados os registros da tabela ' +
  EdVerOrig.Text + ' para a ' + EdVerDest.Text + '. Deseja continuar?') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
    //
    VerOrig := EdVerOrig.ValueVariant;
    VerDest := EdVerDest.ValueVariant;

    UnDmkDAC_PF.AbreMySQLQuery0(QrOrigI, Dmod.MyDB, [
    'SELECT * ',
    'FROM nfelayi ',
    'WHERE Versao=' + Geral.FFT_Dot(VerOrig, 2, siPositivo),
    '']);
    //
    if QrOrigI.RecordCount > 0 then
    begin
      PB.Position := 0;
      PB.Max := QrOrigI.RecordCount;
      QrOrigI.First;
      while not QrOrigI.Eof do
      begin
        MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
        CopiarAtual(VerDest);
        //
        QrOrigI.Next;
      end;

    end else Geral.MB_Aviso(
      'N�o existem registros com a vers�o de origem na base de dados!');
  end;
end;

procedure TFmNFeNewVer.BtDuplicaClick(Sender: TObject);
begin
  InsUpdItem(stCpy);
end;

procedure TFmNFeNewVer.BtExcluiClick(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaAdmin() then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item?') = ID_YES then
    begin
      TbDest.Delete;
      TbDest.Refresh;
    end;
  end;
end;

procedure TFmNFeNewVer.BtIncluiClick(Sender: TObject);
begin
  InsUpdItem(stIns);
end;

procedure TFmNFeNewVer.InsUpdItem(SQLType: TSQLType);
var
  Codigo, ID, Pai: String;
  Versao: Double;
begin
  if DBCheck.CriaFm(TFmNFeLayout_Edit, FmNFeLayout_Edit, afmoSoAdmin) then
  begin
    with FmNFeLayout_Edit do
    begin
      ImgTipo.SQLType := SQLType;
      //
      if (SQLType = stUpd)
      or (SQLType = stCpy) then
      begin
        EdDeciCasas.ValueVariant  := TbDestDeciCasas.Value;
        EdOcorMax.ValueVariant    := TbDestOcorMax.Value;
        EdTamMin.ValueVariant     := TbDestTamMin.Value;
        EdTamMax.ValueVariant     := TbDestTamMax.Value;
        EdTamVar.ValueVariant     := TbDestTamVar.Value;
        CkLeftZeros.Checked       := Geral.IntToBool(TbDestLeftZeros.Value);
        CkInfoVazio.Checked       := Geral.IntToBool(TbDestInfoVazio.Value);
        EdGrupo.ValueVariant      := TbDestGrupo.Value;
        EdCampo.ValueVariant      := TbDestCampo.Value;
        EdDescricao.ValueVariant  := TbDestDescricao.Value;
        EdElemento.ValueVariant   := TbDestElemento.Value;
        EdPai.ValueVariant        := TbDestPai.Value;
        EdTipo.ValueVariant       := TbDestTipo.Value;
        EdOcorMin.ValueVariant    := TbDestOcorMin.Value;
        MeObservacao.Text         := TbDestObservacao.Value;
        EdCodigo.ValueVariant     := TbDestCodigo.Value;
        EdID.ValueVariant         := TbDestID.Value;
        EdFormatStr.ValueVariant  := TbDestFormatStr.Value;
        EdVersao.ValueVariant     := TbDestVersao.Value;
        //
        EdSubGrupo.Text           := TbDestSubGrupo.Value;
        EdCodigoN.ValueVariant    := TbDestCodigoN.Value;
        EdCodigoX.Text            := TbDestCodigoX.Value;
        //
        FCodigo                   := TbDestCodigo.Value;
        FID                       := TbDestID.Value;
        FPai                      := TbDestPai.Value;
        FVersao                   := TbDestVersao.Value;
        (*
        EdCodigo.Enabled := False;
        EdID.Enabled := False;
        *)
        //
        MeObservacao.Text         := TbDestObservacao.Value;
        TPDtIniHomo.Date          := TbDestDtIniHomo.Value;
        TPDtIniProd.Date          := TbDestDtIniProd.Value;
      end else
      begin
        EdDeciCasas.ValueVariant  := TbDestDeciCasas.Value;
        EdOcorMax.ValueVariant    := TbDestOcorMax.Value;
        EdTamMin.ValueVariant     := TbDestTamMin.Value;
        EdTamMax.ValueVariant     := TbDestTamMax.Value;
        EdTamVar.ValueVariant     := TbDestTamVar.Value;
        CkLeftZeros.Checked       := Geral.IntToBool(TbDestLeftZeros.Value);
        CkInfoVazio.Checked       := Geral.IntToBool(TbDestInfoVazio.Value);
        EdGrupo.ValueVariant      := TbDestGrupo.Value;
        //EdCampo.ValueVariant      := TbDestCampo.Value;
        //EdDescricao.ValueVariant  := TbDestDescricao.Value;
        EdElemento.ValueVariant   := TbDestElemento.Value;
        EdPai.ValueVariant        := TbDestPai.Value;
        EdTipo.ValueVariant       := TbDestTipo.Value;
        EdOcorMin.ValueVariant    := TbDestOcorMin.Value;
        //MeObservacao.Text         := TbDestObservacao.Value;
        //EdCodigo.ValueVariant     := TbDestCodigo.Value;
        //EdID.ValueVariant         := TbDestID.Value;
        EdFormatStr.ValueVariant  := TbDestFormatStr.Value;
        EdVersao.ValueVariant     := TbDestVersao.Value;
        //
        EdSubGrupo.Text           := TbDestSubGrupo.Value;
        //EdCodigoN.ValueVariant    := TbDestCodigoN.Value;
        //EdCodigoX.Text            := TbDestCodigoX.Value;
        //
        FCodigo := '';
        FID     := '';
        FPai    := '';
        FVersao := 0;
        MeObservacao.Text         := TbDestObservacao.Value;
        TPDtIniHomo.Date          := TbDestDtIniHomo.Value;
        TPDtIniProd.Date          := TbDestDtIniProd.Value;
      end;
      ShowModal;
      //
      Codigo := EdCodigo.ValueVariant;
      ID     := EdID.ValueVariant;
      //
      Codigo := FmNFeLayout_Edit.FCodigo;
      ID     := FmNFeLayout_Edit.FID;
      Pai    := FmNFeLayout_Edit.FPai;
      Versao := FmNFeLayout_Edit.FVersao;
      //
      Destroy;
    end;
    TbDest.Close;
    UnDmkDAC_PF.AbreTable(TbDest, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
    //
    TbDest.Locate('Codigo;ID;Pai;Versao', VarArrayOf([Codigo,ID,Pai,Versao]), []);
  end;
end;

procedure TFmNFeNewVer.Itensdatabelanoarquivo1Click(Sender: TObject);
{
const
  IndiA1 = 'if Def(';
  IndiA2 = 'if Def_UTC(';
}
var
  I: Integer;
  Ht, ID, Elemento, Campo: String;
  Achou: Boolean;
  VerDest: Double;
  Texto: String;
begin
  PageControl2.ActivePageIndex := 1;
  //
  MeNotFound.Text := EmptyStr;
  MeNaoElmt.Text  := EmptyStr;
  Texto           := EmptyStr;
  Screen.Cursor := crHourGlass;
  try;
  VerDest := EdVerDest.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT * FROM nfelayi  ',
  'WHERE Versao=' + Geral.FFT_Dot(VerDest, 4, siNegativo),
  '']);
  PB.Position := 0;
  PB.Max := QrPsq.RecordCount;
  QrPsq.First;
  while not QrPsq.Eof do
  begin
    Ht := QrPsq.FieldByName('Codigo').AsString;
    ID := QrPsq.FieldByName('ID').AsString;
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
    '[' + Ht+'][' + ID + '] >> Verificando');
    //
    Achou := False;
    for I := 1 to Grade.RowCount - 1 do
    begin
      if (Grade.Cells[2, I] = Ht) and (Grade.Cells[3, I] = ID) then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      Elemento := QrPsq.FieldByName('Elemento').AsString;
      Campo    := QrPsq.FieldByName('Campo').AsString;
      if (Elemento = 'E') or (Elemento = 'CE') then
      begin
        MeNotFound.Lines.Add('[' + Ht+'][' + ID +
        '] >> Tem no BD, mas n�o foi encontrado no .pas (vers�o ' + EdVerDest.Text + ')');
        //
        Texto := Texto + '  if Def(''' + Ht + ''', ''' + ID +
          ''', DmNFe_0000.QrNFE?' + Campo + '.Value, Valor) then' +
          sLineBreak + 'cXML.InfNFe.?.' + Campo + ' := Valor; ' + sLineBreak;
      end else
      begin
        MeNaoElmt.Lines.Add('[' + Ht+'][' + ID +
        '] >> Elemento: "' + Elemento + '"');
      end;
    end;
    QrPsq.Next;
  end;
  //
  PageControl2.ActivePageIndex := 1;
  if MeNotFound.Text <> EmptyStr then
    PageControl3.ActivePageIndex := 0
  else
    PageControl3.ActivePageIndex := 1;
  //
  finally
    Screen.Cursor := crDefault;
  end;
  if Texto <> EmptyStr then
    Geral.MB_Aviso(Texto);
end;

procedure TFmNFeNewVer.Itensdoarquivonatabela1Click(Sender: TObject);
const
  IndiA1 = 'if Def(';
  IndiA2 = 'if Def_UTC(';
var
  I: Integer;
  IndiA, Linha, Texto, Ht, ID: String;
  Achou: Boolean;
  p: Integer;
  VerDest: Double;
begin
  PageControl2.ActivePageIndex := 1;
  //
  MeNotFound.Text := '';
  Screen.Cursor := crHourGlass;
  try;
  VerDest := EdVerDest.ValueVariant;
  for I := 1 to Grade.RowCount - 1 do
  begin
{
    Achou := False;
    Linha := MeFile.Lines[I];
    if (pos(IndiA1, Linha) > 0) then
    begin
      IndiA := IndiA1;
      Achou := True;
    end;
    if (pos(IndiA2, Linha) > 0) then
    begin
      IndiA := IndiA2;
      Achou := True;
    end;
    if Achou then
    begin
      Texto := Linha;
      //
      p := pos('''', Texto);
      Texto := Copy(Texto, p + 1);
      p := pos('''', Texto);
      Ht := Copy(Texto, 1, p - 1);
      //
      //p := pos('''', Texto);
      Texto := Copy(Texto, Length(Ht) + 2);
      p := pos('''', Texto);
      Texto := Copy(Texto, p + 1);
      p := pos('''', Texto);
      ID := Copy(Texto, 1, p - 1);
      //
      //
}
      Ht := Grade.Cells[2, I];
      ID := Grade.Cells[3, I];
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
      'SELECT * FROM nfelayi  ',
      'WHERE Versao=' + Geral.FFT_Dot(VerDest, 4, siNegativo),
      'AND Codigo="' + Ht + '" ',
      'AND ID="' + ID + '" ',
      '']);
      if QrPsq.RecordCount = 0 then
        MeNotFound.Lines.Add('[' + Ht+'][' + ID +
        '] >> Tem no .pas na linha ' + Grade.Cells[1, I] +
          ', mas n�o foi encontrado no BD (vers�o ' + EdVerDest.Text + ')');
    //end;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
  if MeNotFound.Text = EmptyStr then
    Geral.MB_Info('Nenhuma diverg�ncia foi encontrada!');
end;

procedure TFmNFeNewVer.PageControl1Change(Sender: TObject);
begin
  RedefineBotoes();
end;

procedure TFmNFeNewVer.BtReopenClick(Sender: TObject);
begin
  ReopenNFeLayI();
end;

procedure TFmNFeNewVer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeNewVer.BtTestarClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl2.ActivePageIndex := 0;
  MyObjects.MostraPopupDeBotao(PMTestar, BtTestar);
end;

procedure TFmNFeNewVer.CarregaArquivo;
const
  IndiA1 = 'if Def(';
  IndiA2 = 'if Def_UTC(';
var
  I, K: Integer;
  IndiA, Linha, Texto, Ht, ID: String;
  Achou: Boolean;
  p: Integer;
  VerDest: Double;
begin
  PageControl2.ActivePageIndex := 0;
  //
  MeNotFound.Text := '';
  Screen.Cursor := crHourGlass;
  try;
  K := 0;
  Grade.RowCount := 1;
  Grade.Cells[00, 0] := 'Seq';
  Grade.Cells[01, 0] := 'Lin';
  Grade.Cells[02, 0] := '#';
  Grade.Cells[03, 0] := 'ID';
  Grade.Cells[04, 0] := 'OK';
  //
  VerDest := EdVerDest.ValueVariant;
  for I := 1 to MeFile.Lines.Count do
  begin
    Achou := False;
    Linha := MeFile.Lines[I];
    if (pos(IndiA1, Linha) > 0) then
    begin
      IndiA := IndiA1;
      Achou := True;
    end;
    if (pos(IndiA2, Linha) > 0) then
    begin
      IndiA := IndiA2;
      Achou := True;
    end;
    if Achou then
    begin
      Ht := '';
      ID := '';
      Texto := Linha;
      //
      p := pos('''', Texto);
      Texto := Copy(Texto, p + 1);
      p := pos('''', Texto);
      Ht := Copy(Texto, 1, p - 1);
      //
      //p := pos('''', Texto);
      Texto := Copy(Texto, Length(Ht) + 2);
      p := pos('''', Texto);
      Texto := Copy(Texto, p + 1);
      p := pos('''', Texto);
      ID := Copy(Texto, 1, p - 1);
      //
      if (Ht <> EmptyStr) and (ID <> EmptyStr) then
      begin
        K := K + 1;
        Grade.RowCount := K + 1;
        //
        Grade.Cells[00, K] := Geral.FF0(K);
        Grade.Cells[01, K] := Geral.FF0(I);
        Grade.Cells[02, K] := Ht;
        Grade.Cells[03, K] := ID;
        //
      end;
    end;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeNewVer.CopiarAtual(Versao: Double);
var
  Grupo, SubGrupo, Codigo, ID, Campo, Descricao, Elemento, Pai, Tipo, TamVar, Observacao,
  FormatStr, CodigoX: String;
  OcorMin, OcorMax, TamMin, TamMax, DeciCasas, LeftZeros, InfoVazio: Integer;
  CodigoN: Double;
begin
  Grupo          := QrOrigIGrupo.Value;
  SubGrupo       := QrOrigISubGrupo.Value;
  Codigo         := QrOrigICodigo.Value;
  ID             := QrOrigIID.Value;
  Campo          := QrOrigICampo.Value;
  Descricao      := QrOrigIDescricao.Value;
  Elemento       := QrOrigIElemento.Value;
  Pai            := QrOrigIPai.Value;
  Tipo           := QrOrigITipo.Value;
  OcorMin        := QrOrigIOcorMin.Value;
  OcorMax        := QrOrigIOcorMax.Value;
  TamMin         := QrOrigITamMin.Value;
  TamMax         := QrOrigITamMax.Value;
  TamVar         := QrOrigITamVar.Value;
  DeciCasas      := QrOrigIDeciCasas.Value;
  Observacao     := QrOrigIObservacao.Value;
  LeftZeros      := QrOrigILeftZeros.Value;
  InfoVazio      := QrOrigIInfoVazio.Value;
  FormatStr      := QrOrigIFormatStr.Value;
  //Versao         := QrOrigIVersao.Value;
  CodigoN        := QrOrigICodigoN.Value;
  CodigoX        := QrOrigICodigoX.Value;
  //
  UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'nfelayi', False, [
  'Grupo', 'SubGrupo', 'Campo', 'Descricao',
  'Elemento', 'Tipo', 'OcorMin',
  'OcorMax', 'TamMin', 'TamMax',
  'TamVar', 'DeciCasas', 'Observacao',
  'LeftZeros', 'InfoVazio', 'FormatStr',
  'CodigoN', 'CodigoX'], [
  'Codigo', 'ID', 'Pai', 'Versao'], [
  Grupo, SubGrupo, Campo, Descricao,
  Elemento, Tipo, OcorMin,
  OcorMax, TamMin, TamMax,
  TamVar, DeciCasas, Observacao,
  LeftZeros, InfoVazio, FormatStr,
  CodigoN, CodigoX], [
  Codigo, ID, Pai, Versao], False);
end;

procedure TFmNFeNewVer.DBGrid1DblClick(Sender: TObject);
begin
  EdPsqGrupo.Text := TbNFeLayCGrupo.Value;
  EdPsqSubGrupo.Text := '';
  ReopenNFeLayI();
  PageControl1.ActivePageIndex := 1;
  RedefineBotoes();
end;

procedure TFmNFeNewVer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  RedefineBotoes();
end;

procedure TFmNFeNewVer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbNFeLayC.DataBase := Dmod.MyDB;
  TbDest.DataBase := Dmod.MyDB;
  QrOrigI.DataBase := Dmod.MyDB;
  //
  ReopenNfeLayC();
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmNFeNewVer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeNewVer.RedefineBotoes();
begin
  BtInclui.Enabled := PageControl1.ActivePageIndex = 1;
  BtAltera.Enabled := PageControl1.ActivePageIndex = 1;
  BtExclui.Enabled := PageControl1.ActivePageIndex = 1;
end;

procedure TFmNFeNewVer.ReopenNFeLayC();
begin
  UnDmkDAC_PF.AbreTable(TbNFeLayC, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNfeLayC, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfelayc ',
  'Order By Grupo, Nome ',
  '']);
*)
end;

procedure TFmNFeNewVer.ReopenNFeLayI();
var
  Filtro: String;
begin
  Filtro := 'Versao=' + Geral.FFT(EdVerDest.ValueVariant, 2, siPositivo);
  if Trim(EdPsqGrupo.Text) <> '' then
    Filtro := Filtro + ' AND Grupo=''' + Trim(EdPsqGrupo.Text) + '''';
  if Trim(EdPsqSubGrupo.Text) <> '' then
    Filtro := Filtro + ' AND SubGrupo=''' + Trim(EdPsqSubGrupo.Text) + '''';
  TbDest.Close;
  TbDest.Filter := Filtro;
  TbDest.Filtered := True;
  UnDmkDAC_PF.AbreTable(TbDest, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
end;

procedure TFmNFeNewVer.SbFileClick(Sender: TObject);
// C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\MDL\NFe\v_04_00\Geral\NFeGeraXML_0400.pas

var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdFile.ValueVariant := Arquivo
  else
    EdFile.ValueVariant := '';
end;

procedure TFmNFeNewVer.SbLoadClick(Sender: TObject);
begin
  if FileExists(EdFile.ValueVariant) then
  begin
    MeFile.Lines.LoadFromFile(EdFile.ValueVariant);
    PageControl2.ActivePageIndex := 0;
    Screen.Cursor := crHourGlass;
    try
      CarregaArquivo();
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_AViso('O arquivo "' + EdFile.ValueVariant + '" n�o foi localizado');
end;

end.
