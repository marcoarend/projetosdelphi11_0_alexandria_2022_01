unit NFe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, TypInfo, UnInternalConsts, ZCF2, StrUtils,
  dmkGeral, UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) DmkCoding,
  dmkEdit, dmkRadioGroup, dmkMemo,
  //{$IFNDef semNFe_v 0 2 0 0} NFeGeraXML_0200, {$EndIF}
  (*SHA-1*)DCPsha1, DCPTwoFish, (* fim SHA-1*)
  //NFeGeraXML_0310,
  NFeWebServices_0000, NFeWebServicesGer_0000,
  dmkCheckGroup, dmkPageControl, UnGrl_Consts,
  UnDmkACBr_PF, UnitWin, UnXXe_PF, UnGrl_Vars;

type
  TNFeCabA = class
  protected
    FinfProt_xMotivo: String;
    FinfProt_cStat: Integer;
    FinfProt_digVal: String;
    FinfProt_nProt: String;
    FinfProt_dhRecbto: TDateTime;
    FinfProt_verAplic: String;
    FinfProt_tpAmb: Integer;
    FinfProt_Id: String;
    FprotNFe_versao: Double;
    FStatus: Integer;
    FIDCtrl: Integer;
    FFatID: Integer;
    FFatNum: Integer;
    FEmpresa: Integer;
  public
    function ObtemDadosNFeCabA(DB: TMySQLDataBase; chNFe: String): Boolean;
  published
    property infProt_xMotivo: String read FinfProt_xMotivo;
    property infProt_cStat: Integer read FinfProt_cStat default 0;
    property infProt_digVal: String read FinfProt_digVal;
    property infProt_nProt: String read FinfProt_nProt;
    property infProt_dhRecbto: TDateTime read FinfProt_dhRecbto;
    property infProt_verAplic: String read FinfProt_verAplic;
    property infProt_tpAmb: Integer read FinfProt_tpAmb default 0;
    property infProt_Id: String read FinfProt_Id;
    property protNFe_versao: Double read FprotNFe_versao;
    property Status: Integer read FStatus default 0;
    property IDCtrl: Integer read FIDCtrl default 0;
    property FatID: Integer read FFatID default 0;
    property FatNum: Integer read FFatNum default 0;
    property Empresa: Integer read FEmpresa default 0;
  end;

  TNFe_PF = class(TObject)
  private
    { Private declarations }
    procedure ReabreQueryPrzT(Query: TmySQLQuery; CondicaoPG: Integer);
    procedure ReabreQueryNF_X(Query: TmySQLQuery; Tipo, OriCodi, Associada: Integer);
    procedure ReabreQuerySumT(Query: TmySQLQuery; CondicaoPG: Integer);
    procedure ReabreQueryPrzX(Query: TmySQLQuery; CondicaoPG: Integer; Campo: String);
    procedure ReabreQuerySumX(Query: TmySQLQuery; Tipo, OriCodi, Empresa: Integer);
  public
    { Public declarations }
    procedure CalculaValoresICMS_OIVCF(const ICMSUFDest_pFCPUFDest,
              ICMSUFDest_vBCUFDest, ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
              ICMSUFDest_pICMSInterPart: Double; var ICMSUFDest_vFCPUFDest,
              ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet: Double);
    procedure Configura_indFinal(RG_indFinal: TdmkRadioGroup);
    procedure Configura_idDest(EdEmpresa: TdmkEdit; RG_idDest: TdmkRadioGroup);
    function  CriarDocuNFe(VersaoNFe: Integer; FatID, FatNum, Empresa:
              Integer; var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1,
              LaAviso2: TLabel; GravaCampos, Cliente: Integer): Boolean;
    function  CriaNFe_vXX_XX(Recria: Boolean; NFeStatus, FatID, FatNum,
              Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
              RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg: Integer; FreteVal,
              Seguro, Outros: Double; ide_Serie: Variant; ide_nNF: Integer;
              ide_dEmi, ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis: Integer;
              infAdic_infAdFisco, infAdic_infCpl, VeicTransp_Placa,
              VeicTransp_UF, VeicTransp_RNTC, Exporta_UFEmbarq,
              Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS,
              SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String;
              UF_Emit, UF_Dest: String; GravaCampos, cNF_Atual,
              Financeiro: Integer; ide_hSaiEnt: TTime; ide_dhCont: TDateTime;
              ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
              Compra_XNEmp, Compra_XPed, Compra_XCont: String;
              // NFe 3.10
              idDest: Integer; ide_hEmi: TTime; ide_dhEmiTZD, ide_dhSaiEntTZD:
              Double; indFinal, indPres, finNFe: Integer;
              // NFe 4.00 NT 2018/5
              RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti,
              L_Ativo: Integer;
              // Fim NFe 4.00 NT 2018/5
              ApenasCriaXML: Boolean; CalculaAutomatico: Boolean;
              CNPJCPFAvulso, RazaoNomeAvulso: String;
              EmiteAvulso: Boolean;
              // Quiet
              EditCabGA: Boolean;
              InfIntermedEnti: Integer): Boolean;
              //
    function  DefineVarsFormNFaEdit_XXXX(): Boolean;
    function  DefineVarsFormNFaEdit_Quiet(Financeiro: Integer): Boolean;
    function  DescompactaXML(Texto: String; MeAvisoDOS: TMemo): String;
    procedure EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote, Empresa: Integer;
              SohLer: Boolean; Modelo: Integer);
    procedure EncerraFaturamento(FatSemEstq, FatPedCab, IDVolume, Tipo,
              RegrFiscal: Integer; Tabela: String(*;
              OEstoqueJaFoiBaixado: Boolean = False*));
    procedure EncerraFaturamento_Quiet(FatSemEstq, FatPedCab, PediVda, IDVolume, Tipo,
              RegrFiscal: Integer; Tabela: String(*;
              OEstoqueJaFoiBaixado: Boolean = False*); Financeiro: Integer;
              ValorTotal, ValFrete: Double; FretePor, Transporta: Integer);
    function  HashCSRT(Empresa: Integer; ChaveNFe: String): String;
    function  ImpedePelaMisturaDeWebServices(const CodsEve: String;
              EventoLote: Integer; var UF_Servico: String): Boolean;
    procedure InfoVersaoNFe();
    procedure InsereTextoObserv(Texto: String; Memo: TdmkMemo);
    //procedure LerArquivoCartaDeCorrecao(CodsEve: String; EventoLote, Empresa: Integer);
    function  ListaContemplaCodigoSelecionado(Codigo: Integer; Lista: MyArrayLista): Boolean;
    function  ListaIndicadorDePresencaComprador(): MyArrayLista;
    function  ListaTipoDeEmiss�oDaNFe(): MyArrayLista;
    function  ListaFinalidadeDeEmiss�oDaNFe(IncluiZero: Boolean = False): MyArrayLista;
    function  ListaTES_ITENS(): MyArrayLista;
    function  ListaTES_BC_ITENS(): MyArrayLista;
    function  ListaIND_PAG_EFD(): MyArrayLista;
    function  ListaIND_FRT_EFD(): MyArrayLista;
    function  ListaCOD_SIT_EFD(): MyArrayLista;
    function  ListaIND_MOV_EFD(): MyArrayLista;
    function  ListaTP_CT_e_EFD(): MyArrayLista;
    function  ListaNAT_BC_CRED(): MyArrayLista;
    function  ListaIND_NAT_FRT(): MyArrayLista;
    function  ListaIdentificacaoDoAmbiente(): MyArrayLista;
    function  ListaIdentificacaoDoLocalDeDestinoDaOperacao(): MyArrayLista;
    function  ListaOperacaoComConsumidorFinal(): MyArrayLista;
    function  ListaTipoDocFiscal(): MyArrayLista;
    function  ListaBandeirasDaOperadoraDeCartao(): MyArrayLista;
    function  ListaTiposDeIntegracaoParaPagamento(): MyArrayLista;
    function  ListaMeiosDePagamento(): MyArrayLista;
    function  ListaIndicadorDeFormaDePagamento(): MyArrayLista;
    function  ListaMotDesICMSST(): MyArrayLista;
    function  ListaCLAS_ESTAB_IND(): MyArrayLista;
    function  ListaIND_NAT_PJ(): MyArrayLista;
    function  ListaCOD_INC_TRIB(): MyArrayLista;
    function  ListaIND_APRO_CRED(): MyArrayLista;
    function  ListaCOD_TIPO_CONT(): MyArrayLista;
    function  ListaIND_REG_CUM(): MyArrayLista;

    function  MostraFormCFOPCFOP(SQLType: TSQLType; QrCab: TmySQLQuery; DsCab:
              TDataSource; QrInn, QrOut: TmySQLQuery; Codigo: Integer; Nome: String; CFOP_INN, CFOP_OUT: String): Integer;
    procedure MostraFormFatPedNFs(EMP_FILIAL, Cliente,
              CU_PediVda: Integer; ForcaCriarXML: Boolean);
    procedure MostraFormLayoutNFe();
    procedure MostraFormNFeCabA();
    procedure MostraFormNFeCabGA(FatID, FatNum, Empresa, DestRem: Integer;
              InserePadroes: Boolean);
    procedure MostraFormNFeCabXLac(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeCabXReb(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeCabXVol(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeCabZCon(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeCabZFis(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeCabZPro(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
    procedure MostraFormNFeConsulta();
    procedure MostraFormNFeLEnc(Codigo: Integer);
    function  MostraFormNFeLEnU(): Boolean;
    function  MostraFormNFeLEnU_Quiet(Form: TForm; ide_tpEmis, StatusAchieved: Integer): Boolean;
    procedure MostraFormNFeItsIDI(QrNFeItsIDI: TmySQLQuery; SQLType: TSQLType);
    procedure MostraFormNFeItsIDIa(QrNFeItsIDIa: TmySQLQuery; SQLType: TSQLType);
    procedure MostraFormNFeItsI03(QrNFeItsI03: TmySQLQuery; SQLType: TSQLType);
    procedure MostraFormNFeInut();
    procedure MostraFormNFePesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
              PageControl: TdmkPageControl; Cliente: Integer;
              QuemEmit: Integer = 0); overload;
    procedure MostraFormNFePesq_IDCtrl_Unico(AbrirEmAba: Boolean; InOwner: TWincontrol;
              PageControl: TdmkPageControl; IDCtrl: Integer);
    //
    procedure MostraFormStepsNFe_StepGenerico();
    procedure MostraFormStepsNFe_Inutilizacao(Empresa, Lote, Ano, Modelo, Serie, nNFIni, nNFFim, Justif: Integer);
    procedure MostraFormStepsNFe_StatusServico();
    // ini 2022-02-15 Nome alterado (corrigido)
    //procedure MostraFormStepsNFe_CartaDeCorrecao(SohLer: Boolean; CodsEve: String; EventoLote, Empresa, ide_mod: Integer);
    procedure MostraFormStepsNFe_EnvioLoteEvento(SohLer: Boolean; CodsEve: String; EventoLote, Empresa, ide_mod: Integer);
    // fim 2022-02-15 Nome alterado (corrigido)
    procedure MostraFormStepsNFe_ConsultaCadastroEntidade(const UF, CNPJPF:
              String; Mostra: Boolean);
    procedure MostraFormStepsNFe_ConsultaNFe(Empresa, IDCtrl, ide_mod: Integer; ChaveNFe:
              String; MeXML: TMemo);
    procedure MostraFormStepsNFe_ConsultaDocumentosDestinados();
    procedure MostraFormStepsNFe_DownloadNFeConfirmadas(Codigo, Empresa:
              Integer; QrNFeDesDowI: TmySQLQuery; SohLer: Boolean);
    function  ObtemDataEntradaEFiscal(const Serie, NumNF, Emitente: Integer;
              const ChaveNFe, NomeEmit: String; const Default: TDateTime;
              var DataEntrada, DataFiscal: TDateTime): Boolean;
    function  ObtemUltNSU_NFeDistDFeInt(Lote: Integer): Int64;
    //
    procedure ReopenFPC1(FatPedCab: Integer);
    function  SchemaNFe(Empresa: Integer): String;
    function  TextoDeCodigoNFe(Tipo: TNFeCodType; Codigo: Integer): String;
    function  TextoDeCodigoSPED_TXT(Tipo: TSPEDCodType; Codigo: String): String;
    function  TextoDeCodigoSPED_TXT2(Tipo: TSPEDCodType; Codigo: String; Lista: MyArrayLista): String;
    procedure ValidaXML_NFe(ArquivoXML: String);
    function  VersaoNFeEmUso(): Integer;
    // Geral
    function  ObtemVersaoServicoNFe(UF, Servico: String; tpAmb: Integer; MaxVersao: Double): Double;
    function  ObtemVersoesServicoNFe(UF, Servico: String; tpAmb: Integer): MyArrayLista;
    function  ObtemURLWebServerNFe(UF, Servico, Versao: String; tpAmb, Modelo:
              Integer): String;
    procedure ObtemSerieNumeroByID(Id: String; var Serie, nNF: Integer);
    // Versao 0400
    procedure MostraFormNFaEdit_0400(FatPedCab: Integer; Tabela: String(*;
              OEstoqueJaFoiBaixado: Boolean = False*));
    procedure MostraFormNFaEdit_0400_Quiet(FatPedCab, PediVda: Integer; Tabela: String(*;
              OEstoqueJaFoiBaixado: Boolean = False*);
              Financeiro, RegrFiscal: Integer; ValorTotal, ValFrete: Double;
              FretePor, Transporta: Integer);

    procedure MostraFormNFeEveRLoE(Codigo: Integer);
    procedure MostraFormNFeExportaXML_B();
    procedure MostraFormNFeNewVer();
    procedure MostraFormNFeExportaXML();
    procedure MostraFormNFeLoad_Inn();
    procedure MostraFormNFeCntngnc();
    procedure MostraFormNFeLoad_Web();
    procedure MostraFormNFeJust(Codigo: Integer = 0);
    procedure MostraFormNFeCnfDowC_0100();
    procedure MostraFormNFeDesDowC_0100();
    procedure MostraFormNFeDesConC_0101();
    procedure MostraFormNFeDistDFeInt();
    procedure MostraFormNFeLoad_Dir();
    procedure MostraFormNFeLoad_Arq();
    procedure MostraFormNFeInfCpl();
    procedure MostraFormNFaInfCpl(Memo: TdmkMemo);
    procedure MostraFormNFeWebservices();
    procedure MostraFormNFeWebservicesGer();
    procedure MostraFormNFe_Pesq_0000_ImpFatur(CGcSitConf: TdmkCheckGroup;
              EdFilial_ValueVariant: Variant; RGQuemEmit_ItemIndex,
              EdCliente_ValueVariant, RGAmbiente_ItemIndex,
              QrClientesTipo_Value: Integer; CBFilial_Text, CBCliente_Text,
              QrClientesCNPJ_Value, QrClientesCPF_Value: String;
              TPDataI_Date, TPDataF_Date: TDateTime; Ck100e101_Checked: Boolean);
    procedure MostraFormNFe_Pesq_0000_ImpLista(PorCFOP: TNFEAgruRelNFs;
              EdFilial_ValueVariant: Variant; TPDataI_Date,
              TPDataF_Date: TDateTime; RGQuemEmit_ItemIndex, RGAmbiente_ItemIndex,
              EdCliente_ValueVariant, QrClientesTipo_Value, RGOrdem1_ItemIndex,
              RGOrdem2_ItemIndex: Integer; Ck100e101_Checked: Boolean;
              QrClientesCNPJ_Value, QrClientesCPF_Value: String;
              CGcSitConf: TdmkCheckGroup; CkideNatOp_Checked: Boolean;
              CBFilial_Text, CBCliente_Text: String);
    {}
    procedure MostraFormNFeEFD_C170(SQLType: TSQLType;
              FatID, FatNum, Empresa, nItem: Integer);
    {}
    //
    function  AtualizaDadosCanNfeCabA(infEvento_chNFe, infEvento_tpAmb,
              infEvento_verAplic, infEvento_dhRegEvento, infEvento_nProt,
              infEvento_versao: String; ret_TZD_UTC: Double): Boolean;
    function  AtualizaDadosEPECNFeCabA(infEvento_chNFe, infEvento_versao,
              infEPEC_verAplic, infEPEC_dhRegEvento, infEPEC_nProt,
              infEPEC_cOrgao, infEPEC_tpAmb, infEPEC_cStat, infEPEC_xMotivo:
              String; infEPEC_dhRegEventoTZD: Double): Boolean;
    function  AtualizaDadosCCeNfeCabA(infEvento_chNFe, infCCe_verAplic,
              infCCe_chNFe, infCCe_dhRegEvento,
              infCCe_nProt: String; nSeqEvento, infCCe_cOrgao, infCCe_tpAmb,
              infCCe_tpEvento, infCCe_cStat: Integer;
              infCCe_dhRegEventoTZD: Double): Boolean;
    procedure RegistraChaveNFeParaManifestar(chNFe: String; Conta: Integer;
              MostraMsg: Boolean = False; MeAvisos: TMemo = nil);
    //
    procedure MostraNFeChave(Chave: String);
    procedure MostraNFeNoPortalNacional(ide_tpAmb: Integer; Id: String);
    //
    function  ObtemCFOPEntradaDeSaida(CFOP: String; Empresa, GraGruX: Integer):
              String;
    function  ObtemSPEDEFD_COD_ITEM(const GraGruX, SPED_EFD_ID_0200: Integer;
              var COD_ITEM: String): Boolean;
    function  ObtemSPEDEFD_GraGruX(const COD_ITEM: String; SPED_EFD_ID_0200: Integer;
              var GraGruX: Integer): Boolean;
    function  ObtemSPEDEFD_COD_PART(const Entidade, SPED_EFD_ID_0150: Integer;
              var COD_PART: String): Boolean;
    function  ObtemSPEDEFD_SPED_EFD_ID_0XXX(Registro, Empresa: Integer): Integer;
    function  IncluiLanctoFaturasNFe(Valor, MoraDia: Double; Data,
              Vencto: TDateTime; Duplicata, Descricao: String; TipoCart,
              Carteira, Genero, GenCtb, CliInt, Parcela, NotaFiscal, Account,
              Financeiro, GenCtbD, GenCtbC, Filial, Cliente, OriCodi: Integer; SerieNF: String;
              VerificaCliInt: Boolean): Integer;
    function  InsUpdFaturasNFe(Filial, Empresa, FatID, Tipo, OriCodi, CtaFaturas,
              Associada, ASS_CtaFaturas, ASS_Filial, Financeiro, GenCtbD, GenCtbC, CartEmis,
              CondicaoPG, AFP_Sit, FaturaNum, IDDuplicata, NumeroNF, FaturaSeq,
              TipoCart, Represen, ASS_IDDuplicata, ASS_FaturaSeq,
              Cliente: Integer; AFP_Per: Double;
  // ini 2021-10-23
              //FreteVal, Seguro, Desconto, Outros: Double;
  // fim 2021-10-23
              TpDuplicata, FaturaSep, TxtFaturas,
              ASS_FaturaSep, ASS_TpDuplicata, ASS_TxtFaturas: String;
              DataFat: TDateTime; QueryPrzT, QuerySumT, QueryPrzX,
              QuerySumX, QueryNF_X: TmySQLQuery; NaoMostraMsg: Boolean = False): Boolean;
    function  SelecionaStatusNFe(Atual: Integer): Integer;
    function  VerificaSeCalculaTributos(NFeNT2013_003LTT, NFe_indFinalCpl, ide_indFinal: Integer): Boolean;
    procedure ReopenUnidMed(Sigla: String; Query: TmySQLQuery; CodUpd: Variant);  //DModG.SiglaDuplicada
    {}
    procedure MostraFormNFeEFC_C170_Cab(FatID, FatNum, Empresa: Integer);
    {}
    //
    procedure Atualiza_DtEmissNF_StqMovNfsa(IDCtrl: Integer; DtEmissNF: TDate);
    //
    procedure PesquisaEMostraFormNFePesqEmitida(NF, Empresa: Integer);
    procedure PesquisaEMostraFormNFePesqRecebida(NF, Empresa, CodInfoEmit: Integer);
    function  GeraGrupoNA_0310(ide_dEmi: TDateTime): Boolean;
    // Versao 0400
    function  GeraGrupoNA_0400(ide_dEmi: TDateTime): Boolean;
    // Ano 2019
    function  CryptSHA1(const Crypt: string(*; twofish: TDCP_twofish*);
              var Crypted: String): Boolean;
    function  NFe_StatusServicoCod(const Form: TForm; const MostraForm: Boolean; var CodStatus:
              Integer; var TxtStatus: String): Integer;
    function  NFe_StatusServicoCodMul(const Form: TForm; var CodStatus: Integer; var TxtStatus:
              String): Boolean;
    function  NFe_StatusServicoCodDef(const Form: TForm; var CodStatus: Integer; var TxtStatus:
              String): Boolean;
    //Ano 2022
    // Movido do
    procedure VerificaCertificadoDigital(Empresa: Integer; EdEmitCNPJ,
              EdSerialNumber, EdUF_Servico: TdmkEdit; CBUF: TComboBox;
              LaExpiraCertDigital: TLabel);

  end;

var
  UnNFe_PF: TNFe_PF;
const
  CO_MSG_VERSAO_NFE2 = 'A vers�o: 2.00 da NF-e n�o est� dispon�vel para este aplicativo!';

implementation

uses
  ModuleGeral, MyDBCheck, ModProd, ModPediVda, DmkDAC_PF, UnDmkProcFunc, Module,
  UMySQLModule, unMyObjects, UnFinanceiro, NFaInfCpl, UnDmkWeb,
  NFeInut_0000, ModuleNFe_0000,
  (*NFeValidaXML_0000,*)
  NFeValidaXML_0001,
  NFe_Pesq_0000, NFeCabA_0000,
  NFeXMLGerencia, NFeCabGA,
  NFeEveRLoE, NFeExportaXML_B, NFeNewVer, NFeExportaXML, NFeLoad_Inn,
  NFeCntngnc, NFeLoad_Web, NFeJust,
  NFeLoad_Dir, NFeLoad_Arq, NFeInfCpl,
  NFe_Pesq_0000_ImpLista, NFe_Pesq_0000_ImpFat, NFeXMLData, NFe_LocEPesq,
  ///////////////////////////////////// NFe ////////////////////////////////////
  NFeCabXReb_0000, NFeCabXVol_0000, NFeCabXLac_0000,
  NFeCabZCon_0000, NFeCabZFis_0000, NFeCabZPro_0000,
  NFeDesDowC_0100,
  //NFeValidaXML_0110,
  //
  //
  NFeSteps_0400, FatPedNFs_0400, NFaEdit_0400, NFeItsIDI_0400,
  NFeItsIDIa_0400, NFeLEnU_0400, NFeLEnc_0400, NFeConsulta_0400,
  NFeItsI03_0400, NFeGeraXML_0400, NFaEdit_0400_Quiet, NFeLEnU_0400_Quiet,
  //
  ///////////////////////////////////// DFe ////////////////////////////////////
  NFeDistDFeInt_0100, NFeCnfDowC_0100,
  ////////////////////////////////// SPED EFD //////////////////////////////////
  {}NFeEFD_C170, NFeEFD_C170_Cab,{}
  CFOPCFOP,
  //////////////////////////////////////////////////////////////////////////////
  DmkACBrNFeSteps_0400;
{ TNFeCabA }

function TNFeCabA.ObtemDadosNFeCabA(DB: TMySQLDataBase; chNFe: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT * ',
      'FROM nfecaba',
      'WHERE id="' + chNFe + '" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      FinfProt_xMotivo  := Qry.FieldByName('infProt_xMotivo').AsString;
      FinfProt_cStat    := Qry.FieldByName('infProt_cStat').AsInteger;
      FinfProt_digVal   := Qry.FieldByName('infProt_digVal').AsString;
      FinfProt_nProt    := Qry.FieldByName('infProt_nProt').AsString;
      FinfProt_dhRecbto := Qry.FieldByName('infProt_dhRecbto').AsDateTime;
      FinfProt_verAplic := Qry.FieldByName('infProt_verAplic').AsString;
      FinfProt_tpAmb    := Qry.FieldByName('infProt_tpAmb').AsInteger;
      FinfProt_Id       := Qry.FieldByName('infProt_Id').AsString;
      FprotNFe_versao   := Qry.FieldByName('protNFe_versao').AsFloat;
      FStatus           := Qry.FieldByName('Status').AsInteger;
      FIDCtrl           := Qry.FieldByName('IDCtrl').AsInteger;
      FFatID            := Qry.FieldByName('FatID').AsInteger;
      FFatNum           := Qry.FieldByName('FatNum').AsInteger;
      FEmpresa          := Qry.FieldByName('Empresa').AsInteger;
      //
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

{ TNFe_PF }

procedure TNFe_PF.Configura_indFinal(RG_indFinal: TdmkRadioGroup);
var
  Tipo: Integer;
begin
  Tipo := DmPediVda.QrClientesTipo.Value;
  //
  if Tipo = 0 then
    RG_indFinal.ItemIndex := 0
  else
    RG_indFinal.ItemIndex := 1;
end;

procedure TNFe_PF.VerificaCertificadoDigital(Empresa: Integer; EdEmitCNPJ,
  EdSerialNumber, EdUF_Servico: TdmkEdit; CBUF: TComboBox; LaExpiraCertDigital:
  TLabel);
begin
  DmNFe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmNFe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmNFe_0000.QrFilialUF_WebServ.Value;
  //2011-08-25
  //EdUF_Servico.Text        := DmNFe_0000.QrFilialUF_Servico.Value;
  case DmNFe_0000.QrFilialNFetpEmis.Value of
    3(*SCAN*): EdUF_Servico.Text := 'SCAN';
    6(*SVC-AN*): EdUF_Servico.Text := 'SVC-AN';
    7(*SVC-RS*): EdUF_Servico.Text := 'SVC-RS';
    else EdUF_Servico.Text := DmNFe_0000.QrFilialUF_Servico.Value;
  end;
  // Fim 2011-08-25
  EdSerialNumber.Text := DmNFe_0000.QrFilialNFeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmNFe_0000.QrFilialNFeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmNFe_0000.QrFilialNFeSerVal.Value < Int(Date) then
  begin
    LaExpiraCertDigital.Caption := 'Seu certificado digital expirou!';
    //Geral.MB_Aviso('Seu certificado digital expirou!');
  end else
  if DmNFe_0000.QrFilialNFeSerVal.Value <= (Int(Date) + DmNFe_0000.QrFilialNFeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmNFe_0000.QrFilialNFeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

function TNFe_PF.VerificaSeCalculaTributos(NFeNT2013_003LTT, NFe_indFinalCpl,
  ide_indFinal: Integer): Boolean;
begin
  if NFeNT2013_003LTT > 0 then
  begin
    if TindFinalTribNFe(NFe_indFinalCpl) = infFinTribSoConsumidorFinal then
      Result := (TNFeIndFinal(ide_indFinal) = indfinalConsumidorFinal)
    else
      Result := True;
  end else
    Result := False;
end;

procedure TNFe_PF.CalculaValoresICMS_OIVCF(const ICMSUFDest_pFCPUFDest,
  ICMSUFDest_vBCUFDest, ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
  ICMSUFDest_pICMSInterPart: Double; var ICMSUFDest_vFCPUFDest,
  ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet: Double);
var
  vICMSTotGrupoNA, vICMSDestSemFCP: Double;
begin
  //Valor do ICMS relativo ao Fundo de Combate � Pobreza (FCP) da UF de destino.
  ICMSUFDest_vFCPUFDest     := Geral.RoundC(ICMSUFDest_pFCPUFDest * ICMSUFDest_vBCUFDest / 100, 2);
  //Valor do ICMS Interestadual para a UF de destino, j� considerando o
  //valor do ICMS relativo ao Fundo de Combate � Pobreza naquela UF.
  if ICMSUFDest_pICMSUFDest <= ICMSUFDest_pICMSInter then
  begin
    ICMSUFDest_vICMSUFDest  := 0;
    ICMSUFDest_vICMSUFRemet := 0;
  end else
  begin
    vICMSTotGrupoNA         := Geral.RoundC(ICMSUFDest_vBCUFDest *
    (ICMSUFDest_pICMSUFDest - ICMSUFDest_pICMSInter) / 100, 2);
    vICMSDestSemFCP := Geral.RoundC(vICMSTotGrupoNA * ICMSUFDest_pICMSInterPart / 100, 2);
    //
    //Valor do ICMS Interestadual para a UF de destino, j� considerando o
    //valor do ICMS relativo ao Fundo de Combate � Pobreza naquela UF.
    ICMSUFDest_vICMSUFDest  := vICMSDestSemFCP + ICMSUFDest_vFCPUFDest;
    //Valor do ICMS Interestadual para a UF do remetente.
    //Nota: A partir de 2019, este valor ser� zero.
    ICMSUFDest_vICMSUFRemet := vICMSTotGrupoNA - vICMSDestSemFCP;
  end;
end;

procedure TNFe_PF.Configura_idDest(EdEmpresa: TdmkEdit; RG_idDest: TdmkRadioGroup);
var
  Empresa, RemDest: Integer;
  UF_Emit, UF_RmDs: String;
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    UF_Emit := DModG.ObtemNomeUFDeEntidade(Empresa);
    UF_RmDs := DmPediVda.QrClientesNOMEUF.Value;
    //
    if (UF_Emit <> '') and (UF_RmDs <> '') then
    begin
      if UF_RmDs = 'EX' then
        RG_idDest.ItemIndex := 3
      else
      if UF_RmDs = UF_Emit then
        RG_idDest.ItemIndex := 1
      else
        RG_idDest.ItemIndex := 2;
    end;
  end;
end;

function TNFe_PF.CriaNFe_vXX_XX(Recria: Boolean; NFeStatus, FatID, FatNum,
  Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
  RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg: Integer; FreteVal, Seguro,
  Outros: Double; ide_Serie: Variant; ide_nNF: Integer; ide_dEmi,
  ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis: Integer; infAdic_infAdFisco,
  infAdic_infCpl, VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS,
  SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ, UF_Emit, UF_Dest: String;
  GravaCampos, cNF_Atual, Financeiro: Integer; ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email,
  Vagao, Balsa, Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  idDest: Integer; ide_hEmi: TTime; ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  indFinal, indPres, finNFe: Integer;
  // NFe 4.00 NT 2018/5
  RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti,
  L_Ativo: Integer;
  // Fim NFe 4.00 NT 2018/5
  ApenasCriaXML: Boolean; CalculaAutomatico: Boolean;
  // NFCe
  CNPJCPFAvulso, RazaoNomeAvulso: String;
  EmiteAvulso: Boolean;
  // Quiet
  EditCabGA: Boolean;
  InfIntermedEnti: Integer): Boolean;
var
  HrSai_TXT, HrEmi_TXT: String;
  VersaoNFe: Integer;
begin
  Result := False;
  //
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmNFeSteps_0400.Show;
        if GravaCampos <> ID_CANCEL then
        begin
          //
          HrSai_TXT := Geral.FDT(ide_hSaiEnt, 102);
          HrEmi_TXT := Geral.FDT(ide_hEmi, 102);
          //
          Result := FmNFeSteps_0400.CriaNFeNormal(Recria, NFeStatus, FatID,
          FatNum, Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta,
          RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, FreteVal,
          Seguro, Outros, ide_Serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,
          ide_tpEmis, infAdic_infAdFisco, infAdic_infCpl,
          VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
          Exporta_UFEmbarq, Exporta_xLocEmbarq,
          SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT,
          SQL_VOLUMES, SQL_CUSTOMZ, UF_Emit, UF_Dest, GravaCampos, cNF_Atual,
          Financeiro, (*ide_hSaiEnt*)HrSai_TXT, ide_dhCont, ide_xJust, emit_CRT,
          dest_email, Vagao, Balsa, Compra_XNEmp, Compra_XPed, Compra_XCont,
          // NFe 3.10
          idDest, (*ide_hEmi*)HrEmi_TXT, ide_dhEmiTZD, ide_dhSaiEntTZD, indFinal,
          indPres, finNFe,
          // NFe 4.00 NT 2018/5
          RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti,
          L_Ativo,
          // Fim NFe 4.00 NT 2018/5
          ApenasCriaXML, CalculaAutomatico,
          // NFCe
          CNPJCPFAvulso, RazaoNomeAvulso,
          EmiteAvulso,
          // Quiet
          EditCabGA,
          InfIntermedEnti);
        end else
          Geral.MensagemBox('Gera��o total da NF-e cancelada pelo usu�rio!',
            'Aviso', MB_OK+MB_ICONWARNING);
        FmNFeSteps_0400.Destroy;
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

function TNFe_PF.CriarDocuNFe(VersaoNFe, FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
  GravaCampos, Cliente: Integer): Boolean;
begin
  Result := False;
  //
  case VersaoNFe of
    400:
      Result := FmNFeGeraXML_0400.CriarDocumentoNFe(FatID, FatNum, Empresa,
                  XMLGerado_Arq, XMLGerado_Dir, LaAviso1, LaAviso2, GravaCampos,
                  Cliente);
    else
    begin
       Result := False;
       XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
    end;
  end;
end;

function TNFe_PF.CryptSHA1(const Crypt: string(*; twofish: TDCP_twofish*);
  var Crypted: String): Boolean;
var
  Qry: TmySQLQuery;
begin
(*
  twofish.InitStr('gpbe', TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o CEP.
  twofish.Burn();
*)
  Result  := False;
  Crypted := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SHA1("' + Crypt + '") SHA_1, ',
    'LENGTH(SHA1("' + Crypt + '")) Tamanho ',
    '']);
    //
    Crypted := Qry.FieldByName('SHA_1').AsString;
    Result := Crypted <> '';
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.DefineVarsFormNFaEdit_Quiet(Financeiro: Integer): Boolean;
var
  VersaoNFe: Integer;
begin
  Result    := False;
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      // Configura��o do tipo
      FmNFaEdit_0400_Quiet.F_Tipo            := VAR_FATID_0001;
      FmNFaEdit_0400_Quiet.F_OriCodi         := DmPediVda.QrFatPedCabCodigo        .Value;
      // Integer
      FmNFaEdit_0400_Quiet.F_Empresa         := DmPediVda.QrFatPedCabEmpresa       .Value;
      FmNFaEdit_0400_Quiet.F_ModeloNF        := DmPediVda.QrFatPedCabModeloNF      .Value;
      FmNFaEdit_0400_Quiet.F_Cliente         := DmPediVda.QrFatPedCabCliente       .Value;
      FmNFaEdit_0400_Quiet.F_EMP_FILIAL      := DmPediVda.QrFatPedCabEMP_FILIAL    .Value;
      FmNFaEdit_0400_Quiet.F_AFP_Sit         := DmPediVda.QrFatPedCabAFP_Sit       .Value;
      FmNFaEdit_0400_Quiet.F_Associada       := DmPediVda.QrFatPedCabAssociada     .Value;
      FmNFaEdit_0400_Quiet.F_ASS_FILIAL      := DmPediVda.QrFatPedCabASS_FILIAL    .Value;
      FmNFaEdit_0400_Quiet.F_EMP_CtaFaturas  := DmPediVda.QrFatPedCabEMP_CtaProdVen.Value;
      FmNFaEdit_0400_Quiet.F_ASS_CtaFaturas  := DmPediVda.QrFatPedCabASS_CtaProdVen.Value;
      FmNFaEdit_0400_Quiet.F_CartEmis        := DmPediVda.QrFatPedCabCartEmis      .Value;
      FmNFaEdit_0400_Quiet.F_CondicaoPG      := DmPediVda.QrFatPedCabCondicaoPG    .Value;
      if Financeiro <> -1 then
        FmNFaEdit_0400_Quiet.F_Financeiro      := Financeiro
      else
        FmNFaEdit_0400_Quiet.F_Financeiro    := DmPediVda.QrFatPedCabFinanceiro    .Value; // define na regra fiscal
      FmNFaEdit_0400_Quiet.F_CtbCadMoF       := DmPediVda.QrFatPedCabCtbCadMoF     .Value;
      FmNFaEdit_0400_Quiet.F_CtbPlaCta       := DmPediVda.QrFatPedCabCtbPlaCta     .Value;
      FmNFaEdit_0400_Quiet.F_EMP_FaturaDta   := DmPediVda.QrFatPedCabEMP_FaturaDta .Value;
      FmNFaEdit_0400_Quiet.F_EMP_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      FmNFaEdit_0400_Quiet.F_EMP_FaturaSeq   := DmPediVda.QrFatPedCabEMP_FaturaSeq .Value;
      FmNFaEdit_0400_Quiet.F_EMP_FaturaNum   := DmPediVda.QrFatPedCabEMP_FaturaNum .Value;
      FmNFaEdit_0400_Quiet.F_TIPOCART        := DmPediVda.QrFatPedCabTIPOCART      .Value;
      FmNFaEdit_0400_Quiet.F_Represen        := DmPediVda.QrFatPedCabRepresen      .Value;
      FmNFaEdit_0400_Quiet.F_ASS_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      FmNFaEdit_0400_Quiet.F_ASS_FaturaSeq   := DmPediVda.QrFatPedCabASS_FaturaSeq .Value;
      FmNFaEdit_0400_Quiet.F_ASS_FaturaNum   := DmPediVda.QrFatPedCabASS_FaturaNum .Value;
      // String
      FmNFaEdit_0400_Quiet.F_EMP_FaturaSep   := DmPediVda.QrFatPedCabEMP_FaturaSep .Value;
      FmNFaEdit_0400_Quiet.F_EMP_TxtFaturas  := DmPediVda.QrFatPedCabEMP_TxtProdVen.Value;
      FmNFaEdit_0400_Quiet.F_EMP_TpDuplicata := DmPediVda.QrFatPedCabEMP_DupProdVen.Value;
      FmNFaEdit_0400_Quiet.F_ASS_FaturaSep   := DmPediVda.QrFatPedCabASS_FaturaSep .Value;
      FmNFaEdit_0400_Quiet.F_ASS_TxtFaturas  := DmPediVda.QrFatPedCabASS_TxtProdVen.Value;
      FmNFaEdit_0400_Quiet.F_ASS_TpDuplicata := DmPediVda.QrFatPedCabASS_DupProdVen.Value;
      // Double
      FmNFaEdit_0400_Quiet.F_AFP_Per         := DmPediVda.QrFatPedCabAFP_Per       .Value;
      // TDateTime
      FmNFaEdit_0400_Quiet.F_Abertura        := DmPediVda.QrFatPedCabAbertura      .Value;
      //
      Result := True;
    end
    else
    begin
       Result := False;
       XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
    end;
  end;
end;

function TNFe_PF.DefineVarsFormNFaEdit_XXXX(): Boolean;
var
  VersaoNFe: Integer;
begin
  Result    := False;
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      // Configura��o do tipo
      FmNFaEdit_0400.F_Tipo            := VAR_FATID_0001;
      FmNFaEdit_0400.F_OriCodi         := DmPediVda.QrFatPedCabCodigo        .Value;
      // Integer
      FmNFaEdit_0400.F_Empresa         := DmPediVda.QrFatPedCabEmpresa       .Value;
      FmNFaEdit_0400.F_ModeloNF        := DmPediVda.QrFatPedCabModeloNF      .Value;
      FmNFaEdit_0400.F_Cliente         := DmPediVda.QrFatPedCabCliente       .Value;
      FmNFaEdit_0400.F_EMP_FILIAL      := DmPediVda.QrFatPedCabEMP_FILIAL    .Value;
      FmNFaEdit_0400.F_AFP_Sit         := DmPediVda.QrFatPedCabAFP_Sit       .Value;
      FmNFaEdit_0400.F_Associada       := DmPediVda.QrFatPedCabAssociada     .Value;
      FmNFaEdit_0400.F_ASS_FILIAL      := DmPediVda.QrFatPedCabASS_FILIAL    .Value;
      FmNFaEdit_0400.F_EMP_CtaFaturas  := DmPediVda.QrFatPedCabEMP_CtaProdVen.Value;
      FmNFaEdit_0400.F_ASS_CtaFaturas  := DmPediVda.QrFatPedCabASS_CtaProdVen.Value;
      FmNFaEdit_0400.F_CartEmis        := DmPediVda.QrFatPedCabCartEmis      .Value;
      FmNFaEdit_0400.F_CondicaoPG      := DmPediVda.QrFatPedCabCondicaoPG    .Value;
      FmNFaEdit_0400.F_Financeiro      := DmPediVda.QrFatPedCabFinanceiro    .Value; // define na regra fiscal
      FmNFaEdit_0400.F_CtbCadMoF       := DmPediVda.QrFatPedCabCtbCadMoF     .Value;
      FmNFaEdit_0400.F_CtbPlaCta       := DmPediVda.QrFatPedCabCtbPlaCta     .Value;
      FmNFaEdit_0400.F_tpNF            := DmPediVda.QrFatPedCabtpNF          .Value;
      FmNFaEdit_0400.F_EMP_FaturaDta   := DmPediVda.QrFatPedCabEMP_FaturaDta .Value;
      FmNFaEdit_0400.F_EMP_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      FmNFaEdit_0400.F_EMP_FaturaSeq   := DmPediVda.QrFatPedCabEMP_FaturaSeq .Value;
      FmNFaEdit_0400.F_EMP_FaturaNum   := DmPediVda.QrFatPedCabEMP_FaturaNum .Value;
      FmNFaEdit_0400.F_TIPOCART        := DmPediVda.QrFatPedCabTIPOCART      .Value;
      FmNFaEdit_0400.F_Represen        := DmPediVda.QrFatPedCabRepresen      .Value;
      FmNFaEdit_0400.F_ASS_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      FmNFaEdit_0400.F_ASS_FaturaSeq   := DmPediVda.QrFatPedCabASS_FaturaSeq .Value;
      FmNFaEdit_0400.F_ASS_FaturaNum   := DmPediVda.QrFatPedCabASS_FaturaNum .Value;
      // String
      FmNFaEdit_0400.F_EMP_FaturaSep   := DmPediVda.QrFatPedCabEMP_FaturaSep .Value;
      FmNFaEdit_0400.F_EMP_TxtFaturas  := DmPediVda.QrFatPedCabEMP_TxtProdVen.Value;
      FmNFaEdit_0400.F_EMP_TpDuplicata := DmPediVda.QrFatPedCabEMP_DupProdVen.Value;
      FmNFaEdit_0400.F_ASS_FaturaSep   := DmPediVda.QrFatPedCabASS_FaturaSep .Value;
      FmNFaEdit_0400.F_ASS_TxtFaturas  := DmPediVda.QrFatPedCabASS_TxtProdVen.Value;
      FmNFaEdit_0400.F_ASS_TpDuplicata := DmPediVda.QrFatPedCabASS_DupProdVen.Value;
      // Double
      FmNFaEdit_0400.F_AFP_Per         := DmPediVda.QrFatPedCabAFP_Per       .Value;
      // TDateTime
      FmNFaEdit_0400.F_Abertura        := DmPediVda.QrFatPedCabAbertura      .Value;
      //
      Result := True;
    end
    else
    begin
       Result := False;
       XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
    end;
  end;
end;

function TNFe_PF.DescompactaXML(Texto: String; MeAvisoDOS: TMemo): String;
var
	InnPath, OutPath: String;
const
  DirXML = 'C:\Dermatek\Temp\Orig\';
  //FArqXML = 'TextoXML';
  ExtB64 = '.cryp';
  ExtZip = '.gzip';
(*
  Txt =
  'H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwd' +
  'aUcjKasqgcplVmVdZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkra' +
  'yZ4hgKrIHz9+fB8/Ih7XefPiWZ6+W5TL5tG7pvjso3nbrh7dvXt1dTW+ujeu6ou7' +
  'ezs7u3d/7y+ev57O80W2XSybNltO84/sW7Ob3/oovczrJqs++2h3vLOjrwavraq6' +
  'zcrzoplm5bhYno8n9d3lef7R0ePpnFA8ur+zu7+7u3N/Z2/v4NNPdwj67oP79+nH' +
  'Dp4He/c/pb/37t1/sL+39/iuvPP45MXL7xyF7zy+yx8+fveiWuRHb05fvTp78+Wr' +
  'sy/Tp1+mJ19+9erL9Pmbp8eP78r3j89Oj/YO7u3tPtjfvff4Lv31eDY/XRRHe4TO' +
  '9u7u9u7+m93dRzsPHu3sbO/s0b+P70qDx+3qxbOj3cd3+efjS/yxN8b3+PXxrLj4' +
  'yaw8au6+On3x9Hr61YPvltM393/q4ef3z37i7PL3uf5FB58RJGlEXb7Kp5O2Cnrd' +
  'Qa/391yv2ubx8mVdtUe7IBhIQwTa36Fhy8ePp6+LFrQhzMyvj+8KGxz9Pz8CEx0PAgAA';
*)
var
  ArqXML: String;
  Existe, Erro: Boolean;
begin
  ArqXML := DmkPF.ObtemNomeArqRandom(15);
  ForceDirectories(DirXML);
  InnPath := DirXML + ArqXML + ExtB64;
  OutPath := DirXML + ArqXML + ExtZip;
  Erro := True;
  while Erro do
  begin
    try
      UnDmkCoding.DecriptaBase64(Texto, InnPath, OutPath, MeAvisoDOS);
      // As vezes d� erro se n�o processar as mensagens do windows!
      Application.ProcessMessages;
      (*
      Existe := FileExists(OutPath);
      while not Existe do
      begin
        Sleep(500);
        Existe := FileExists(OutPath);
      end;
      *)
      Result := UnDmkCoding.DescompactaArquivoGZipToString(OutPath);
      //
      Erro := False;
    except
      Erro := True;
    end;
  end;
  DeleteFile(InnPath);
  DeleteFile(OutPath);
end;

procedure TNFe_PF.EncerraFaturamento(FatSemEstq, FatPedCab, IDVolume, Tipo,
  RegrFiscal: Integer; Tabela: String(*; OEstoqueJaFoiBaixado: Boolean*));
var
  VersaoNFe: Integer;
begin
  //if not OEstoqueJaFoiBaixado then // 2020-10-24
  begin
    if not DmProd.VerificaEstoqueTodoFaturamento(FatSemEstq, FatPedCab, IDVolume,
      Tipo, RegrFiscal)
    then
      Exit;
  end;
  //
  ReopenFPC1(FatPedCab);
  //
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    0400:
      MostraFormNFaEdit_0400(FatPedCab, Tabela(*, OEstoqueJaFoiBaixado*));
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.EncerraFaturamento_Quiet(FatSemEstq, FatPedCab, PediVda, IDVolume,
  Tipo, RegrFiscal: Integer; Tabela: String; Financeiro: Integer;
  ValorTotal, ValFrete: Double; FretePor, Transporta: Integer);
var
  VersaoNFe: Integer;
begin
  //if not OEstoqueJaFoiBaixado then // 2020-10-24
  begin
    if not DmProd.VerificaEstoqueTodoFaturamento(FatSemEstq, FatPedCab, IDVolume,
      Tipo, RegrFiscal)
    then
      Exit;
  end;
  //
  ReopenFPC1(FatPedCab);
  //
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    0400:
      MostraFormNFaEdit_0400_Quiet(FatPedCab, PediVda, Tabela(*, OEstoqueJaFoiBaixado*),
      Financeiro, RegrFiscal, ValorTotal, ValFrete, FretePor, Transporta);
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote, Empresa: Integer;
  SohLer: Boolean; Modelo: Integer);
const
  sProcName = 'TNFe_PF.EnviaLoteAoFisco()';
var
  //ServicoStep: TNFeServicoStep;
  VersaoAcao, sMsg: String;
  VersaoNFe: Integer;
begin
  case Sincronia of
    nisAssincrono:
    begin
      //ServicoStep := nfesrvEnvioAssincronoLoteNFe;
      VersaoAcao  := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siPositivo);
    end;
    nisSincrono:
    begin
      //ServicoStep := nfesrvEnvioSincronoLoteNFe;
      VersaoAcao  := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siPositivo);
    end;
    else
    begin
      Geral.MB_Erro(
        'Forma de sincronismo n�o implementada em "NFe_PF.EnviaLoteAoFisco()"');
      Exit;
    end;
  end;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    0400:
    begin
      // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
      case TFormaGerenXXe(VAR_DFeAppCode) of
        (*0*)fgxxeCAPICOM:
        begin
          if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
          begin
            FmNFeSteps_0400.PnLoteEnv.Visible := True;
            FmNFeSteps_0400.EdVersaoAcao.ValueVariant := VersaoAcao;
            FmNFeSteps_0400.CkSoLer.Checked  := SohLer;
            FmNFeSteps_0400.Show;
            //
            FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe); // Envia lote ao fisco Assincono ou sincrono
            FmNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
            FmNFeSteps_0400.PreparaEnvioDeLoteNFe(Lote, Empresa, Sincronia, Modelo);
            FmNFeSteps_0400.FFormChamou      := 'FmNFeLEnc_0400';
            //
            //
            //FmNFeSteps_0400.Destroy;
            //
          end;
        end;
        //(*1*)fgxxeCMaisMais:
        (*2*)fgxxeACBr:
        begin
          if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
          begin
            FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
            FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := VersaoAcao;
            FmDmkACBrNFeSteps_0400.CkSoLer.Checked  := SohLer;
            FmDmkACBrNFeSteps_0400.Show;
            //
            FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe); // Envia lote ao fisco Assincono ou sincrono
            FmDmkACBrNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
            FmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteNFe(Lote, Empresa, Sincronia, Modelo);
            FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFeLEnc_0400';
            //
            //
            //FmDmkACBrNFeSteps_0400.Destroy;
            //
          end;
        end;
        (*?*)else
        begin
          sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
          Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
        end;
      end;
    end;
  end;
end;

function TNFe_PF.GeraGrupoNA_0310(ide_dEmi: TDateTime): Boolean;
var
  DataPL_008h: TDateTime;
begin
  DataPL_008h := EncodeDate(2016, 01, 01);
  //
  Result := (ide_dEmi >= DataPL_008h);
end;

function TNFe_PF.GeraGrupoNA_0400(ide_dEmi: TDateTime): Boolean;
var
  DataPL_008h: TDateTime;
begin
  DataPL_008h := EncodeDate(2016, 01, 01);
  //
  Result := (ide_dEmi >= DataPL_008h);
end;

function TNFe_PF.HashCSRT(Empresa: Integer; ChaveNFe: String): String;
var
  //Explaned,
  idCSRT, CSRT, Crypt, Crypted: String;
  I, J, N: Integer;
  X, Parte: WideString;
  Teste: AnsiChar;
  IsOK: Boolean;
  Qry: TmySQLQuery;
begin
  Result := '';
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT infRespTec_idCSRT, ',
    'AES_DECRYPT(infRespTec_CSRT, ',
    '"' + CO_CRYPT_CSRT + '") CSRT ',
    'FROM paramsemp ',
    'WHERE Codigo=' + Geral.FF0(Empresa),
    '']);
    //
    idCSRT := Qry.FieldByName('infRespTec_idCSRT').AsString;
    CSRT := Qry.FieldByName('CSRT').AsString;

    {
    G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO41180678393592000146558900000006041028190697
    Chave de Acesso: 41180678393592000146558900000006041028190697
    CSRT: G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO
    idCSRT: 01
    }
    (*
      ChaveNFe := '41180678393592000146558900000006041028190697';
      CSRT     := 'G8063VRTNDMO886SFNK5LDUDEI24XJ22YIPO';
      idCSRT   := '01';
    *)
    //
    Crypted  := '';
    // 696bfa2de10ce17eaee3ea8123639867c82b8a0c  MySQL
    // 696bfa2de10ce17eaee3ea8123639867c82b8a0c
    if UnNFe_PF.CryptSHA1(CSRT + ChaveNFE, Crypted) then
    begin
    (*
      if Esperado = Crypted then
        Explaned := 'Valor coicide!'
      else
        Explaned := 'Valor N�O coicide!';
      //
      Geral.MB_Info(
      'ChaveNFe = ' + ChaveNFe + sLineBreak +
      'CSRT = ' + CSRT + sLineBreak +
      'idCSRT = ' + idCSRT + sLineBreak +
      'Esperado = ' + Esperado + sLineBreak +
      'Resultado = ' + Crypted + sLineBreak +
       Explaned);
       //
  *)
      N := Length(Crypted) div 2;
      X := '';
      for I := 0 to N - 1 do
      begin
        J := (I * 2) + 1;
        Parte := Crypted[J] +  Crypted[J + 1];
        //X := X + Char(dmkPF.HexToInt(Parte));
        X := X + Char(StrToInt64('$' + Parte));
      end;
  (*
      Geral.MB_Info(X);
  *)
      X := dmkPF.EncodeBase64(X);
      Geral.MB_Info('hashCSRT:  ' + X);
      Result := X;
    end;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ImpedePelaMisturaDeWebServices(const CodsEve: String;
  EventoLote: Integer; var UF_Servico: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  UF_Servico := DModG.QrPrmsEmpNFeUF_Servico.Value;
  if DModG.QrPrmsEmpNFeUF_MDeMDe.Value <> UF_Servico then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    //'SUM(IF(tpEvento IN (' + NFe_CodsEveWbserverUserDef + '), 1, 0)) UsuDef, ',
    'SUM(IF(tpEvento IN (' + CodsEve + '), 1, 0)) UsuDef, ',
    //'SUM(IF(tpEvento IN (' + NFe_CodsEveWbserverUserDef + '), 0, 1)) PreDef ',
    'SUM(IF(tpEvento IN (' + CodsEve + '), 0, 1)) PreDef ',
    'FROM nfeevercab ',
    //'WHERE EventoLote=' + Geral.FF0(QrNFeEveRLoECodigo.Value),
    'WHERE EventoLote=' + Geral.FF0(EventoLote),
    '']);
    Result := (Qry.FieldByName('UsuDef').AsFloat >= 1) and
              (Qry.FieldByName('PreDef').AsFloat >= 1);
    if Result then Geral.MB_Aviso('Envio cancelado!' + #13#10 +
    'Existe mistura de web services nos eventos inclu�dos neste lote!')
    else
    if Qry.FieldByName('UsuDef').AsFloat >= 1 then
      UF_Servico := DModG.QrPrmsEmpNFeUF_MDeMDe.Value
    else
      UF_Servico := DModG.QrPrmsEmpNFeUF_Servico.Value;
  end else
    Result := False;
end;

procedure TNFe_PF.InfoVersaoNFe();
////////////////////////////////////////////////////////////////////////////////
// Ainda nao usado em lugar algum! pode ser mudado!                           //
////////////////////////////////////////////////////////////////////////////////
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    110: Geral.MB_Info('Vers�o parametrizada da NFe = 1.10');
    200: Geral.MB_Info('Vers�o parametrizada da NFe = 2.00');
    310: Geral.MB_Info('Vers�o parametrizada da NFe = 3.10');
    400: Geral.MB_Info('Vers�o parametrizada da NFe = 4.00');
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.InsereTextoObserv(Texto: String; Memo: TdmkMemo);
var
  TextoA, TextoD: String;
  Pos: Integer;
begin
  Pos := Memo.SelStart;
  //
  TextoA := Copy(Memo.Text, 1, Pos);
  TextoD := Copy(Memo.Text, Pos + 1, Length(Memo.Text));
  //
  Memo.Text := TextoA + ' ' + Texto + ' ' + TextoD;
  //
  Geral.MB_Aviso('Texto adicionado!');
end;

function TNFe_PF.IncluiLanctoFaturasNFe(Valor, MoraDia: Double; Data,
  Vencto: TDateTime; Duplicata, Descricao: String; TipoCart, Carteira, Genero, GenCtb,
  CliInt, Parcela, NotaFiscal, Account, Financeiro, GenCtbD, GenCtbC, Filial, Cliente,
  OriCodi: Integer; SerieNF: String; VerificaCliInt: Boolean): Integer;
var
  TabLctA: String;
begin
  Result := 0;
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  if Financeiro = 1 then
    FLAN_Credito := Valor
  else
  if Financeiro = 2 then
    FLAN_Debito := Valor;
  //
  FLAN_VERIFICA_CLIINT := VerificaCliInt;
  FLAN_Data       := Geral.FDT(Data, 1);
  FLAN_Tipo       := TipoCart;
  FLAN_Documento  := 0;
  FLAN_MoraDia    := MoraDia;
  FLAN_Multa      := 0;//
  FLAN_Carteira   := Carteira;
  FLAN_Genero     := Genero;  // OK para Cont�bil!!!
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
  FLAN_Cliente    := Cliente;
  FLAN_CliInt     := CliInt;
  //FLAN_Depto      := QrBolacarAApto.Value;
  //FLAN_ForneceI   := QrBolacarAPropriet.Value;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
  FLAN_FatID      := FThisFatID;
  FLAN_FatNum     := OriCodi;
  FLAN_FatParcela := Parcela;
  FLAN_Descricao  := Descricao;
  FLAN_Duplicata  := Duplicata;
  FLAN_SerieNF    := SerieNF;
  FLAN_NotaFiscal := NotaFiscal;
  FLAN_Account    := Account;
  {$IfDef DEFINE_VARLCT}
  TabLctA       := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
  //
  if UFinanceiro.InsereLancamento(TabLctA) then
  begin
    Result := FLAN_Controle;
  end;
  {$Else}
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
  //
  if UFinanceiro.InsereLancamento() then
  begin
    Result := FLAN_Controle;
  end;
  {$EndIf}
end;

function TNFe_PF.InsUpdFaturasNFe(Filial, Empresa, FatID, Tipo, OriCodi,
  CtaFaturas, Associada, ASS_CtaFaturas, ASS_Filial, Financeiro, GenCtbD, GenCtbC, CartEmis,
  CondicaoPG, AFP_Sit, FaturaNum, IDDuplicata, NumeroNF, FaturaSeq, TipoCart,
  Represen, ASS_IDDuplicata, ASS_FaturaSeq, Cliente: Integer; AFP_Per: Double;
  // ini 2021-10-23
  //FreteVal, Seguro, Desconto, Outros: Double;
  // fim 2021-10-23
  TpDuplicata, FaturaSep,
  TxtFaturas, ASS_FaturaSep, ASS_TpDuplicata, ASS_TxtFaturas: String;
  DataFat: TDateTime; QueryPrzT, QuerySumT, QueryPrzX, QuerySumX,
  QueryNF_X: TmySQLQuery; NaoMostraMsg: Boolean = False): Boolean;
var
  TemAssociada: Boolean;
{$IfDef DEFINE_VARLCT}
  TabLctA: String;
{$EndIf}
  FaturNum, Duplicata, NovaDup, NF_Emp_Serie, NF_Ass_Serie: String;
  V1, V2, F1, F2, P1, P2, T1, T2: Double;
  Parcela, NF_Emp_Numer, NF_Ass_Numer, GenCtb: Integer;
begin
  try
    Result       := False;
    // ini 2021-10-23
    //TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
    TemAssociada := False;
    // fim 2021-10-23
    //
    // Exclui lan�amentos para evitar duplica��o
    if (FatID <> 0) and (OriCodi <> 0) then
    begin
{$IfDef DEFINE_VARLCT}
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      //
      UFinanceiro.ExcluiLct_FatNum(nil, FatID, OriCodi,
        Empresa, 0, dmkPF.MotivDel_ValidaCodigo(311), False, TabLctA);
{$Else}
      UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FatID, OriCodi, Empresa, 0, False);
{$EndIf}
    end;
    ReabreQueryNF_X(QueryNF_X, Tipo, OriCodi, Empresa);
    //
    NF_Emp_Serie := QueryNF_X.FieldByName('SerieNFTxt').AsString;
    NF_Emp_Numer := QueryNF_X.FieldByName('NumeroNF').AsInteger;
    //
    if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'O n�mero de nota fiscal n�o foi definida para a empresa ' +
        FormatFloat('000', Filial) + ' !' + sLineBreak +
        'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
        'Nota Fiscal"');
      Exit;
    end;

    if TemAssociada then
    begin
      ReabreQueryNF_X(QueryNF_X, Tipo, OriCodi, Associada);
      //
      NF_Ass_Serie := QueryNF_X.FieldByName('SerieNFTxt').AsString;
      NF_Ass_Numer := QueryNF_X.FieldByName('NumeroNF').AsInteger;
      //
      if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', ASS_FILIAL) + ' !' + sLineBreak +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"');
        Exit;
      end;
      //
      P2 := AFP_Per;
      P1 := 100 - P2;
    end else
    begin
      P1 := 100;
      P2 := 0;
      NF_Ass_Serie := '';
      NF_Ass_Numer := 0;
    end;

    case DModG.QrCtrlGeralUsarFinCtb.Value of
      1: // Credito
      begin
       CtaFaturas := GenCtbC;
       GenCtb     := GenCtbD;
      end;
      2: // Debito
      begin
       CtaFaturas := GenCtbD;
       GenCtb     := GenCtbC;
      end;
    end;

    if CtaFaturas = 0 then // Parei aqui 2020-02-18
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
        Geral.FF0(Filial) + '!');
      Exit;
    end;
    if (Associada <> 0) and (ASS_CtaFaturas = 0) and TemAssociada then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          Geral.FF0(ASS_Filial) + '!');
      Exit;
    end;
    if Financeiro > 0 then
    begin
      if CartEmis = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A carteira n�o foi definida no pedido selecionado! (1)');
        Exit;
      end;
      ReabreQueryPrzT(QueryPrzT, CondicaoPG);
      //
      if QueryPrzT.RecordCount = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'N�o h� parcela(s) definida(s) para a empresa ' +
          Geral.FF0(Filial) + ' na condi��o de pagamento ' +
          'cadastrada no pedido selecionado!');
        Exit;
      end;
      if TemAssociada then
      begin
        ReabreQuerySumT(QuerySumT, CondicaoPG);
        //
        if (QuerySumT.FieldByName('Percent2').AsFloat <> AFP_Per) then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'Percentual da fatura parcial no pedido n�o confere com percentual ' +
            'nos prazos da condi��o de pagamento!');
          Exit;
        end;
        if QuerySumT.RecordCount = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'Percentual da fatura parcial no pedido n�o confere com percentual ' +
            'nos prazos da condi��o de pagamento!');
          Exit;
        end;
      end;
    end else
    begin
      if NaoMostraMsg = False then
      begin
        Geral.MB_Aviso('N�o ser� gerado lan�amentos financeiros.' + sLineBreak +
          'Na Regra Fiscal est� definido para n�o gerar lan�amentos!');
      end;
    end;
    //
    ReabreQuerySumX(QuerySumX, Tipo, OriCodi, Empresa);
    //
  // ini 2021-10-23
    //T1 := QuerySumX.FieldByName('Total').AsFloat + FreteVal + Seguro - Desconto + Outros;
    T1 := QuerySumX.FieldByName('Total').AsFloat
          + QuerySumX.FieldByName('prod_vFrete').AsFloat
          + QuerySumX.FieldByName('prod_vSeg').AsFloat
          - QuerySumX.FieldByName('prod_vDesc').AsFloat
          + QuerySumX.FieldByName('prod_vOutro').AsFloat ;
  // ini 2021-10-23
    F1 := T1;
    //
    if Financeiro > 0 then
    begin
      ReabreQueryPrzX(QueryPrzX, CondicaoPG, 'Percent1');
      //
      QueryPrzX.First;
      while not QueryPrzX.Eof do
      begin
        if QueryPrzX.RecordCount = QueryPrzX.RecNo then
          V1 := F1
        else
        begin
          if P1 = 0 then
            V1 := 0
          else
            V1 := (Round(T1 * (QueryPrzX.FieldByName('Percent').AsFloat / P1 * 100))) / 100;
          F1 := F1 - V1;
        end;
        QueryPrzT.Locate('Controle', QueryPrzX.FieldByName('Controle').AsInteger, []);
        //
        Parcela := QueryPrzT.RecNo;
        //
        if FaturaNum = 0 then
          FaturNum := FormatFloat('000000', IDDuplicata)
        else
          FaturNum := FormatFloat('000000', NumeroNF);
        Duplicata := TpDuplicata + FaturNum + FaturaSep +
          dmkPF.ParcelaFatura(QueryPrzX.RecNo, FaturaSeq);
        // 2011-08-21
        // Teste para substituir no futuro (uso no form FmFatDivCms)
        NovaDup := DmProd.MontaDuplicata(IDDuplicata, NumeroNF, Parcela,
                     FaturaNum, FaturaSeq, TpDuplicata, FaturaSep);
        //
        if NovaDup <> Duplicata then
          Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak + Duplicata + ' <> ' + NovaDup);
        // Fim 2011-08-21
        IncluiLanctoFaturasNFe(V1, QuerySumT.FieldByName('JurosMes').AsFloat,
          DataFat, DataFat + QueryPrzX.FieldByName('Dias').AsInteger, Duplicata,
          TxtFaturas, TipoCart, CartEmis, CtaFaturas, GenCtb, Empresa, Parcela,
          NF_Emp_Numer, Represen, Financeiro, GenCtbD, GenCtbC, Filial, Cliente, OriCodi,
          NF_Emp_Serie, True);
        //
        QueryPrzX.Next;
      end;
      // Faturamento associada
      if TemAssociada then
      begin
        ReabreQuerySumX(QuerySumX, Tipo, OriCodi, Associada);
        //
        T2 := QuerySumX.FieldByName('Total').AsFloat;
        F2 := T2;
        //
        ReabreQueryPrzX(QueryPrzX, CondicaoPG, 'Percent2');
        //
        QueryPrzX.First;
        while not QueryPrzX.Eof do
        begin
          if QueryPrzX.RecordCount = QueryPrzX.RecNo then
            V2 := F2
          else begin
            if P2 = 0 then V2 := 0 else
              V2 := (Round(T2 * (QueryPrzX.FieldByName('Percent').AsFloat / P2 * 100))) / 100;
            F2 := F2 - V2;
          end;
          QueryPrzT.Locate('Controle', QueryPrzX.FieldByName('Controle').AsInteger, []);
          Parcela := QueryPrzT.RecNo;
          //
          if FaturaNum = 0 then
            FaturNum := FormatFloat('000000', ASS_IDDuplicata)
          else
            FaturNum := FormatFloat('000000', NumeroNF);
          //
          Duplicata := ASS_TpDuplicata + FormatFloat('000000', ASS_IDDuplicata) +
            ASS_FaturaSep + dmkPF.ParcelaFatura(
            QueryPrzX.RecNo, ASS_FaturaSeq);
          IncluiLanctoFaturasNFe(V2, QuerySumT.FieldByName('JurosMes').AsFloat,
            DataFat, DataFat + QueryPrzX.FieldByName('Dias').AsInteger, Duplicata,
            ASS_TxtFaturas, TipoCart, CartEmis, ASS_CtaFaturas, (*GenCtb*)0, Associada, Parcela,
            NF_Ass_Numer, Represen, Financeiro, GenCtbD, GenCtbC, Filial, Cliente, OriCodi,
            NF_Ass_Serie, False);
          //
          QueryPrzX.Next;
        end;
      end;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TNFe_PF.ListaIdentificacaoDoLocalDeDestinoDaOperacao(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Opera��o interna']);
  dmkPF.AddLin(Result, Linha, '2', ['Opera��o interestadual']);
  dmkPF.AddLin(Result, Linha, '3', ['Opera��o com exterior']);
end;

function TNFe_PF.ListaOperacaoComConsumidorFinal(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['Normal']);
  dmkPF.AddLin(Result, Linha, '1', ['Consumidor final']);
end;

function TNFe_PF.ListaIdentificacaoDoAmbiente(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Produ��o']);
  dmkPF.AddLin(Result, Linha, '2', ['Homologa��o']);
end;

function TNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(IncluiZero: Boolean = False): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  if IncluiZero then
    dmkPF.AddLin(Result, Linha, '0', ['N�o informado']);
  dmkPF.AddLin(Result, Linha, '1', ['NF-e normal']);
  dmkPF.AddLin(Result, Linha, '2', ['NF-e complementar']);
  dmkPF.AddLin(Result, Linha, '3', ['NF-e de ajuste']);
  dmkPF.AddLin(Result, Linha, '4', ['Devolu��o de Mercadoria']);
end;

function TNFe_PF.ListaCLAS_ESTAB_IND(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '00', ['Industrial - Transforma��o']);
  dmkPF.AddLin(Result, Linha, '01', ['Industrial - Beneficiamento']);
  dmkPF.AddLin(Result, Linha, '02', ['Industrial - Montagem']);
  dmkPF.AddLin(Result, Linha, '03', ['Industrial - Acondicionamento ou Reacondicionamento']);
  dmkPF.AddLin(Result, Linha, '04', ['Industrial - Renova��o ou Recondicionamento']);
  dmkPF.AddLin(Result, Linha, '05', ['Equiparado a industrial - Por op��o']);
  dmkPF.AddLin(Result, Linha, '06', ['Equiparado a industrial - Importa��o Direta']);
  dmkPF.AddLin(Result, Linha, '07', ['Equiparado a industrial - Por lei espec�fica']);
  dmkPF.AddLin(Result, Linha, '08', ['Equiparado a industrial - N�o enquadrado nos c�digos 05, 06 ou 07']);
  dmkPF.AddLin(Result, Linha, '09', ['Outros']);
end;

function TNFe_PF.ListaCOD_INC_TRIB: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Escritura��o de opera��es com incid�ncia exclusivamente no regime n�o-cumulativo']);
  dmkPF.AddLin(Result, Linha, '2', ['Escritura��o de opera��es com incid�ncia exclusivamente no regime cumulativo;']);
  dmkPF.AddLin(Result, Linha, '3', ['Escritura��o de opera��es com incid�ncia nos regimes n�o-cumulativo e cumulativo.']);
end;

function TNFe_PF.ListaCOD_SIT_EFD(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '00', ['Documento regular']);
  dmkPF.AddLin(Result, Linha, '01', ['Documento regular extempor�neo']);
  dmkPF.AddLin(Result, Linha, '02', ['Documento cancelado']);
  dmkPF.AddLin(Result, Linha, '03', ['Documento cancelado extempor�neo']);
  dmkPF.AddLin(Result, Linha, '04', ['NFe ou CT-e denegada']);
  dmkPF.AddLin(Result, Linha, '05', ['NFe ou CT-e Numera��o inutilizada']);
  dmkPF.AddLin(Result, Linha, '06', ['Documento Fiscal Complementar']);
  dmkPF.AddLin(Result, Linha, '07', ['Documento Fiscal Complementar extempor�neo']);
  dmkPF.AddLin(Result, Linha, '08', ['Documento Fiscal emitido com base em Regime Especial ou Norma Espec�fica']);
end;

function TNFe_PF.ListaCOD_TIPO_CONT: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Apura��o da Contribui��o Exclusivamente a Al�quota B�sica']);
  dmkPF.AddLin(Result, Linha, '2', ['Apura��o da Contribui��o a Al�quotas Espec�ficas (Diferenciadas e/ou por Unidade de Medida de Produto)']);
end;

function TNFe_PF.ListaContemplaCodigoSelecionado(Codigo: Integer;
  Lista: MyArrayLista): Boolean;
var
  I, Inteiro: Integer;
  sInt: String;
begin
  Result := False;
  //
  for I := 0 to Length(Lista) - 1 do
  begin
    sInt := Lista[I][0];
    Inteiro := Geral.IMV(sInt);
    if Inteiro = Codigo then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TNFe_PF.ListaIND_APRO_CRED: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['M�todo de Apropria��o Direta;']);
  dmkPF.AddLin(Result, Linha, '2', ['M�todo de Rateio Proporcional (Receita Bruta)']);
end;

function TNFe_PF.ListaIND_FRT_EFD(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['Contrata��o do Frete por conta do Remetente (CIF);']);
  dmkPF.AddLin(Result, Linha, '1', ['Contrata��o do Frete por conta do Destinat�rio (FOB)']);
  dmkPF.AddLin(Result, Linha, '2', ['Contrata��o do Frete por conta de Terceiros;']);
  dmkPF.AddLin(Result, Linha, '3', ['Transporte Pr�prio por conta do Remetente;']);
  dmkPF.AddLin(Result, Linha, '4', ['Transporte Pr�prio por conta do Destinat�rio;']);
  dmkPF.AddLin(Result, Linha, '9', ['Sem Ocorr�ncia de Transporte.']);
end;

function TNFe_PF.ListaIND_MOV_EFD: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['SIM']);
  dmkPF.AddLin(Result, Linha, '1', ['N�o']);
end;

function TNFe_PF.ListaIND_NAT_FRT: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['Opera��es de vendas, com �nus suportado pelo estabelecimento vendedor;']);
  dmkPF.AddLin(Result, Linha, '1', ['Opera��es de vendas, com �nus suportado pelo adquirente;']);
  dmkPF.AddLin(Result, Linha, '2', ['Opera��es de compras (bens para revenda, mat�riasprima e outros produtos, geradores de cr�dito);']);
  dmkPF.AddLin(Result, Linha, '3', ['Opera��es de compras (bens para revenda, mat�riasprima e outros produtos, n�o geradores de cr�dito);']);
  dmkPF.AddLin(Result, Linha, '4', ['Transfer�ncia de produtos acabados entre estabelecimentos da pessoa jur�dica']);
  dmkPF.AddLin(Result, Linha, '5', ['Transfer�ncia de produtos em elabora��o entre estabelecimentos da pessoa jur�dica']);
  dmkPF.AddLin(Result, Linha, '9', ['Outras.']);
end;

function TNFe_PF.ListaIND_NAT_PJ: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '00', ['Pessoa jur�dica em geral (n�o participante de SCP como s�cia ostensiv']);
  dmkPF.AddLin(Result, Linha, '01', ['Sociedade cooperativa (n�o participante de SCP como s�cia ostensiva)']);
  dmkPF.AddLin(Result, Linha, '02', ['Entidade sujeita ao PIS/Pasep exclusivamente com base na Folha de Sal�rios']);
  dmkPF.AddLin(Result, Linha, '03', ['Pessoa jur�dica em geral participante de SCP como s�cia ostensiva']);
  dmkPF.AddLin(Result, Linha, '04', ['Sociedade cooperativa participante de SCP como s�cia ostensiva']);
  dmkPF.AddLin(Result, Linha, '05', ['Sociedade em Conta de Participa��o - SCP']);
end;

function TNFe_PF.ListaIND_PAG_EFD(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['� Vista']);
  dmkPF.AddLin(Result, Linha, '1', ['� Prazo']);
  dmkPF.AddLin(Result, Linha, '2', ['Outros']);
  //dmkPF.AddLin(Result, Linha, '9', ['Sem Pagamento']);
end;

function TNFe_PF.ListaIND_REG_CUM: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Regime de Caixa � Escritura��o consolidada (Registro F500);']);
  dmkPF.AddLin(Result, Linha, '2', ['Regime de Compet�ncia - Escritura��o consolidada (Registro F550);']);
  dmkPF.AddLin(Result, Linha, '9', ['Regime de Compet�ncia - Escritura��o detalhada, com base nos registros dos Blocos �A�, �C�, �D� e �F�.']);
end;

function TNFe_PF.ListaTES_BC_ITENS: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['N�o tributa']);
  dmkPF.AddLin(Result, Linha, '1', ['Pelo BC do XML da NFe']);
  dmkPF.AddLin(Result, Linha, '2', ['Pelo valor da opera��o']);
end;

function TNFe_PF.ListaTES_ITENS: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['N�o tributa']);
  dmkPF.AddLin(Result, Linha, '1', ['Tributa pelo XML da NFe']);
  dmkPF.AddLin(Result, Linha, '2', ['Tributa por esta regra fiscal']);
end;

function TNFe_PF.ListaTipoDeEmiss�oDaNFe(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Normal - emiss�o normal']);
  dmkPF.AddLin(Result, Linha, '2', ['Conting�ncia FS-IA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a']);
  // ini 2023-06-09
  //dmkPF.AddLin(Result, Linha, '3', ['Conting�ncia SCAN - emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional - SCAN']);
  dmkPF.AddLin(Result, Linha, '3', ['Regime Especial NFF (NT 2021.002) - Nota Fiscal F�cil']);
  // fim 2023-06-09
  dmkPF.AddLin(Result, Linha, '4', ['Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia - DPEC']);
  dmkPF.AddLin(Result, Linha, '5', ['Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA)']);
  dmkPF.AddLin(Result, Linha, '6', ['Conting�ncia SVC-AN (SEFAZ Virtual de Conting�ncia do AN)']);
  dmkPF.AddLin(Result, Linha, '7', ['Conting�ncia SVC-RS (SEFAZ Virtual de Conting�ncia do RS)']);
  dmkPF.AddLin(Result, Linha, '9', ['Conting�ncia off-line da NFC-e']);
end;

function TNFe_PF.ListaIndicadorDePresencaComprador(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['N�o se aplica (por exemplo, Nota fiscal compementar ou de ajuste)']);
  dmkPF.AddLin(Result, Linha, '1', ['Opera��o presencial']);
  dmkPF.AddLin(Result, Linha, '2', ['Opera��o n�o presencial, pela internet']);
  dmkPF.AddLin(Result, Linha, '3', ['Opera��o n�o presencial, teleatendimento']);
  dmkPF.AddLin(Result, Linha, '4', ['NFC-e em opera��o com entrega a domic�lio']);
  dmkPF.AddLin(Result, Linha, '5', ['Opera��o presencial, fora do estabelecimento']);
  dmkPF.AddLin(Result, Linha, '9', ['Opera��o n�o presencial, outros']);
end;

function TNFe_PF.ListaTipoDocFiscal(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['Entrada']);
  dmkPF.AddLin(Result, Linha, '1', ['Sa�da']);
end;

function TNFe_PF.ListaBandeirasDaOperadoraDeCartao(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1',  ['Visa']);
  dmkPF.AddLin(Result, Linha, '2',  ['Mastercard']);
  dmkPF.AddLin(Result, Linha, '3',  ['American Express']);
  dmkPF.AddLin(Result, Linha, '4',  ['Sorocred']);
  dmkPF.AddLin(Result, Linha, '5',  ['Diners Club']);
  dmkPF.AddLin(Result, Linha, '6',  ['Elo']);
  dmkPF.AddLin(Result, Linha, '7',  ['Hipercard']);
  dmkPF.AddLin(Result, Linha, '8',  ['Aura']);
  dmkPF.AddLin(Result, Linha, '9',  ['Cabal']);
  dmkPF.AddLin(Result, Linha, '99', ['Outros']);
end;

function TNFe_PF.ListaTiposDeIntegracaoParaPagamento(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1', ['Pagamento integrado com o sistema de automa��o da empresa (Ex.: equipamento TEF, Com�rcio Eletr�nico);']);
  dmkPF.AddLin(Result, Linha, '2', ['Pagamento n�o integrado com o sistema de automa��o da empresa (Ex.: equipamento POS);']);
end;

function TNFe_PF.ListaTP_CT_e_EFD: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0',  ['CT-e Normal']);
  dmkPF.AddLin(Result, Linha, '1',  ['CT-e de Complemento de Valores']);
  dmkPF.AddLin(Result, Linha, '2',  ['CT-e de Anula��o']);
  dmkPF.AddLin(Result, Linha, '3',  ['CT-e de Substitui��o']);
end;

function TNFe_PF.ListaMeiosDePagamento(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '1',  ['Dinheiro']);
  dmkPF.AddLin(Result, Linha, '2',  ['Cheque']);
  dmkPF.AddLin(Result, Linha, '3',  ['Cart�o de Cr�dito']);
  dmkPF.AddLin(Result, Linha, '4',  ['Cart�o de D�bito']);
  dmkPF.AddLin(Result, Linha, '5',  ['Cr�dito Loja']);
  dmkPF.AddLin(Result, Linha, '10', ['Vale Alimenta��o']);
  dmkPF.AddLin(Result, Linha, '11', ['Vale Refei��o']);
  dmkPF.AddLin(Result, Linha, '12', ['Vale Presente']);
  dmkPF.AddLin(Result, Linha, '13', ['Vale Combust�vel']);
  dmkPF.AddLin(Result, Linha, '15', ['Boleto Banc�rio']);
  // ini 2021-03-10
  dmkPF.AddLin(Result, Linha, '16', ['Dep�sito Banc�rio']);
  dmkPF.AddLin(Result, Linha, '17', ['Pagamento Instant�neo (PIX)']);
  dmkPF.AddLin(Result, Linha, '18', ['Transfer�ncia banc�ria, Carteira Digital']);
  dmkPF.AddLin(Result, Linha, '19', ['Programa de fidelidade, Cashback, Cr�dito Virtualend']);
  // fim 2021-03-10
  dmkPF.AddLin(Result, Linha, '90', ['Sem pagamento']);
  dmkPF.AddLin(Result, Linha, '99', ['Outros']);
end;

function TNFe_PF.ListaMotDesICMSST: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '3', ['Uso na agropecu�ria']);
  dmkPF.AddLin(Result, Linha, '9', ['Outros']);
  dmkPF.AddLin(Result, Linha, '12', ['�rg�o de fomento e desenvolvimento agropecu�rio']);
end;

function TNFe_PF.ListaNAT_BC_CRED: MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '01', ['Aquisi��o de bens para revenda']);
  dmkPF.AddLin(Result, Linha, '02', ['Aquisi��o de bens utilizados como insumo']);
  dmkPF.AddLin(Result, Linha, '03', ['Aquisi��o de servi�os utilizados como insumo']);
  dmkPF.AddLin(Result, Linha, '04', ['Energia el�trica e t�rmica, inclusive sob a forma de vapor']);
  dmkPF.AddLin(Result, Linha, '05', ['Alugu�is de pr�dios']);
  dmkPF.AddLin(Result, Linha, '06', ['Alugu�is de m�quinas e equipamentos']);
  dmkPF.AddLin(Result, Linha, '07', ['Armazenagem de mercadoria e frete na opera��o de venda']);
  dmkPF.AddLin(Result, Linha, '08', ['Contrapresta��es de arrendamento mercantil']);
  dmkPF.AddLin(Result, Linha, '09', ['M�quinas, equipamentos e outros bens incorporados ao ativo imobilizado (cr�dito sobre encargos de deprecia��o)']);
  dmkPF.AddLin(Result, Linha, '10', ['M�quinas, equipamentos e outros bens incorporados ao ativo imobilizado (cr�dito com base no valor de aquisi��o)']);
  dmkPF.AddLin(Result, Linha, '11', ['Amortiza��o e Deprecia��o de edifica��es e benfeitorias em im�veis']);
  dmkPF.AddLin(Result, Linha, '12', ['Devolu��o de Vendas Sujeitas � Incid�ncia N�o-Cumulativa']);
  dmkPF.AddLin(Result, Linha, '13', ['Outras Opera��es com Direito a Cr�dito']);
  dmkPF.AddLin(Result, Linha, '14', ['Atividade de Transporte de Cargas � Subcontrata��o']);
  dmkPF.AddLin(Result, Linha, '15', ['Atividade Imobili�ria � Custo Incorrido de Unidade Imobili�ria']);
  dmkPF.AddLin(Result, Linha, '16', ['Atividade Imobili�ria � Custo Or�ado de unidade n�o conclu�da']);
  dmkPF.AddLin(Result, Linha, '17', ['Atividade de Presta��o de Servi�os de Limpeza, Conserva��o e Manuten��o � vale-transporte, vale-refei��o ou vale-alimenta��o, fardamento ou uniforme']);
  dmkPF.AddLin(Result, Linha, '18', ['Estoque de abertura de bens']);
end;

function TNFe_PF.ListaIndicadorDeFormaDePagamento(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  dmkPF.AddLin(Result, Linha, '0', ['Pagamento � vista']);
  dmkPF.AddLin(Result, Linha, '1', ['Pagamento � prazo']);
  dmkPF.AddLin(Result, Linha, '2', ['Outros']);
end;

function TNFe_PF.MostraFormCFOPCFOP(SQLType: TSQLType; QrCab: TmySQLQuery;
  DsCab: TDataSource; QrInn, QrOut: TmySQLQuery; Codigo: Integer; Nome: String;
  CFOP_INN, CFOP_OUT: String): Integer;
begin
  if DBCheck.CriaFm(TFmCFOPCFOP, FmCFOPCFOP, afmoNegarComAviso) then
  begin
    FmCFOPCFOP.ImgTipo.SQLType := SQLType;
    FmCFOPCFOP.FQrCab := QrCab;
    FmCFOPCFOP.FDsCab := DsCab;
    FmCFOPCFOP.FQrInn := QrInn;
    FmCFOPCFOP.FQrOut := QrOut;
    if SQLType = stIns then
    begin
      FmCFOPCFOP.EdCFOP_INN.ValueVariant := Geral.FormataCFOP(CFOP_INN);
      FmCFOPCFOP.CBCFOP_INN.KeyValue     := Geral.FormataCFOP(CFOP_INN);
      FmCFOPCFOP.EdCFOP_OUT.ValueVariant := Geral.FormataCFOP(CFOP_INN);
      FmCFOPCFOP.CBCFOP_OUT.KeyValue     := Geral.FormataCFOP(CFOP_INN);
    end else
    begin
      FmCFOPCFOP.EdCodigo.ValueVariant   := Codigo;
      //
      FmCFOPCFOP.EdNome.ValueVariant     := Nome;
      FmCFOPCFOP.EdCFOP_INN.ValueVariant := Geral.FormataCFOP(CFOP_INN);
      FmCFOPCFOP.CBCFOP_INN.KeyValue     := Geral.FormataCFOP(CFOP_INN);
      FmCFOPCFOP.EdCFOP_OUT.ValueVariant := Geral.FormataCFOP(CFOP_OUT);
      FmCFOPCFOP.CBCFOP_OUT.KeyValue     := Geral.FormataCFOP(CFOP_OUT);
    end;
    FmCFOPCFOP.ShowModal;
    FmCFOPCFOP.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda: Integer;
  ForcaCriarXML: Boolean);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    0400:
    begin
      if DBCheck.CriaFm(TFmFatPedNFs_0400, FmFatPedNFs_0400, afmoNegarComAviso) then
      begin
        FmFatPedNFs_0400.EdFilial.ValueVariant  := EMP_FILIAL;
        FmFatPedNFs_0400.CBFilial.KeyValue      := EMP_FILIAL;
        FmFatPedNFs_0400.EdCliente.ValueVariant := Cliente;
        FmFatPedNFs_0400.CBCliente.KeyValue     := Cliente;
        FmFatPedNFs_0400.EdPedido.ValueVariant  := CU_PediVda;
        FmFatPedNFs_0400.FNaoCriouXML           := ForcaCriarXML;
        FmFatPedNFs_0400.ShowModal;
        FmFatPedNFs_0400.Destroy;
      end
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormLayoutNFe();
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    0:
    begin
      {$IFNDef semNFe_v 0 2 0 0}
      (*
      if DBCheck.CriaFm(TFmNFeLayout_0200, FmNFeLayout_0200, afmoSoAdmin) then
      begin
        FmNFeLayout_0200.ShowModal;
        FmNFeLayout_0200.Destroy;
      end;*)
      Geral.MB_Aviso(CO_MSG_VERSAO_NFE2);
      {$Else}
      Geral.MB_Aviso(CO_MSG_VERSAO_NFE2);
      {$EndIF}
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFaEdit_0400(FatPedCab: Integer; Tabela: String(*;
OEstoqueJaFoiBaixado: Boolean*));
var
  Especie: String;
  Quantidade: Integer;
begin
   /// 2020-10-24 SQLType? deve ser Upd para FOEstoqueJaFoiBaixado!
  if DBCheck.CriaFm(TFmNFaEdit_0400, FmNFaEdit_0400, afmoNegarComAviso) then
  begin
    //FmNFaEdit_0400.FOEstoqueJaFoiBaixado := OEstoqueJaFoiBaixado;
    DmPediVda.ReopenFatPedCab(DmPediVda.QrFPC1Codigo.Value, True);
    //
    FmNFaEdit_0400.ReopenFatPedNFs(1,0);
    //
    try
      if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
        DmPediVda.QrFPC1SerieDesfe.Value,
        DmPediVda.QrFPC1NFDesfeita.Value,
        FmNFaEdit_0400.QrImprimeSerieNF_Normal.Value,
        FmNFaEdit_0400.QrImprimeCtrl_nfs.Value,
        DmPediVda.QrFPC1Empresa.Value,
        FmNFaEdit_0400.QrFatPedNFs.FieldByName('Filial').AsInteger,
        FmNFaEdit_0400.QrImprimeMaxSeqLib.Value,
        FmNFaEdit_0400.EdSerieNF, FmNFaEdit_0400.EdNumeroNF) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;

      Especie    := '';
      Quantidade := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(DmPediVda.QrVolumes, Dmod.MyDB, [
      'SELECT COUNT(fpv.Ativo) Volumes, med.Nome NO_UnidMed ',
      'FROM fatpedvol fpv ',
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed ',
      'WHERE fpv.Codigo=' + Geral.FF0(DmPediVda.QrFPC1Codigo.Value),
      'AND fpv.Ativo=1 ',
      'GROUP BY med.Codigo ',
      '']);
      DmPediVda.QrVolumes.First;
      while not DmPediVda.QrVolumes.Eof do
      begin
        Quantidade := Quantidade + DmPediVda.QrVolumesVolumes.Value;
        if Especie <> '' then
          Especie := Especie + ' + ';
        Especie := Especie + ' ' + DmPediVda.QrVolumesNO_UnidMed.Value;
        //
        DmPediVda.QrVolumes.Next;
      end;
      FmNFaEdit_0400.FTabela := Tabela;
      //
      FmNFaEdit_0400.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
      FmNFaEdit_0400.EdEspecie.ValueVariant    := Especie;
      //
      FmNFaEdit_0400.EdSerieNF.Enabled  := (FmNFaEdit_0400.QrImprimeIncSeqAuto.Value = 0);
      FmNFaEdit_0400.EdNumeroNF.Enabled := (FmNFaEdit_0400.QrImprimeIncSeqAuto.Value = 0);
      //
      // NF-e 2.00
      if (DModG.QrPrmsEmpNFeCRT.Value = 3) and
      (DModG.QrPrmsEmpNFeNFeNT2013_003LTT.Value < 2) then
        FmNFaEdit_0400.PageControl1.ActivePageIndex := 0
      else
        FmNFaEdit_0400.PageControl1.ActivePageIndex := 1;
      //
      FmNFaEdit_0400.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(DmPediVda.QrFPC1Empresa.Value, DmPediVda.QrFPC1Cliente.Value);
      FmNFaEdit_0400.RGCRT.ItemIndex           := DModG.QrPrmsEmpNFeCRT.Value;
      FmNFaEdit_0400.EdVagao.Text              := '';
      FmNFaEdit_0400.EdBalsa.Text              := '';
      //
      FmNFaEdit_0400.EdCompra_XNEmp.Text := '';
      FmNFaEdit_0400.EdCompra_XPed.Text  := DmPediVda.QrFPC1PedidoCli.Value;
      FmNFaEdit_0400.EdCompra_XCont.Text := '';
      //
      FmNFaEdit_0400.ReopenStqMovValX(0);
      // ini2020-10-24
      (*
      if OEstoqueJaFoiBaixado then
        FmNFaEdit_0400.ImgTipo.SQLType := stUpd
      else
      // ini2020-10-24
      *)
      // Ini 2021-02-17
{
      UnDmkDAC_PF.AbreMySQLQuery0(FmNFaEdit_0400.QrStqMovNFsA, Dmod.MyDB, [
      'SELECT *  ',
      'FROM stqmovnfsa ',
      'WHERE Tipo=' + Geral.FF0(VAR_FATID_0001),
      'AND OriCodi=' + Geral.FF0(FatPedCab),
      'AND Empresa=' + Geral.FF0(DmPediVda.QrFPC1Empresa.Value),
      //DmPediVda.QrFPC1Codigo.Value
      '']);
}
      //
      //
      //FmNFaEdit_0400.ConfiguracoesIniciais();
      //
      if (DmNFe_0000.QrStqMovNFsA.State <> dsInactive) and
      (DmNFe_0000.QrStqMovNFsA.RecordCount > 0) and
      (DmNFe_0000.QrStqMovNFsATipo.Value = VAR_FATID_0001) and
      (DmNFe_0000.QrStqMovNFsAOriCodi.Value = FatPedCab) and
      (DmNFe_0000.QrStqMovNFsAEmpresa.Value = DmPediVda.QrFPC1Empresa.Value) then
      begin
        FmNFaEdit_0400.ImgTipo.SQLType := stIns;
        FmNFaEdit_0400.FReInsere       := False;
        //
        FmNFaEdit_0400.FIDCtrl                     := DmNFe_0000.QrStqMovNFsAIDCtrl.Value;
        FmNFaEdit_0400.MeinfAdic_infCpl.Text       := DmNFe_0000.QrStqMovNFsAinfAdic_infCpl.Value;
        FmNFaEdit_0400.EdkgBruto.ValueVariant      := DmNFe_0000.QrStqMovNFsAkgBruto.Value;
        FmNFaEdit_0400.EdkgLiqui.ValueVariant      := DmNFe_0000.QrStqMovNFsAkgLiqui.Value;
        FmNFaEdit_0400.EdPlacaUF.ValueVariant      := DmNFe_0000.QrStqMovNFsAPlacaUF.Value;
        FmNFaEdit_0400.EdPlacaNr.ValueVariant      := DmNFe_0000.QrStqMovNFsAPlacaNr.Value;
        FmNFaEdit_0400.EdEspecie.ValueVariant      := DmNFe_0000.QrStqMovNFsAEspecie.Value;
        FmNFaEdit_0400.EdMarca.ValueVariant        := DmNFe_0000.QrStqMovNFsAMarca.Value;
        FmNFaEdit_0400.EdNumero.ValueVariant       := DmNFe_0000.QrStqMovNFsANumero.Value;
        FmNFaEdit_0400.EdQuantidade.ValueVariant   := DmNFe_0000.QrStqMovNFsAQuantidade.Value;
        FmNFaEdit_0400.EdObservacao.ValueVariant   := DmNFe_0000.QrStqMovNFsAObservacao.Value;
        //
        FmNFaEdit_0400.Edvagao.ValueVariant        := DmNFe_0000.QrStqMovNFsAvagao.Value;
        FmNFaEdit_0400.Edbalsa.ValueVariant        := DmNFe_0000.QrStqMovNFsAbalsa.Value;
        FmNFaEdit_0400.Eddest_email.ValueVariant   := DmNFe_0000.QrStqMovNFsAdest_email.Value;
        //FmNFaEdit_0400.RGCRT.ItemIndex             := DmNFe_0000.QrStqMovNFsAemit_CRT.Value;
        //
        FmNFaEdit_0400.EdRNTC.ValueVariant         := DmNFe_0000.QrStqMovNFsARNTC.Value;
        FmNFaEdit_0400.EdCompra_XNEmp.ValueVariant := DmNFe_0000.QrStqMovNFsACompra_XNEmp.Value;
        FmNFaEdit_0400.EdCompra_XPed .ValueVariant := DmNFe_0000.QrStqMovNFsACompra_XPed.Value;
        FmNFaEdit_0400.EdCompra_XCont.ValueVariant := DmNFe_0000.QrStqMovNFsACompra_XCont.Value;
        //
        FmNFaEdit_0400.EdUFEmbarq.ValueVariant     := DmNFe_0000.QrStqMovNFsAUFEmbarq.Value;
        FmNFaEdit_0400.EdxLocEmbarq.ValueVariant   := DmNFe_0000.QrStqMovNFsAxLocEmbarq.Value;
        //
      end else
      begin
        FmNFaEdit_0400.ImgTipo.SQLType := stIns;
      end;
      DmNFe_0000.QrStqMovNFsA.Close;
      FmNFaEdit_0400.ShowModal;
    finally
      FmNFaEdit_0400.Destroy;
    end;
    // Reabrir de novo!
    ReopenFPC1(FatPedCab);
    //
    if DmPediVda.QrFPC1Encerrou.Value > 0 then
    begin
      MostraFormFatPedNFs(DmPediVda.QrFPC1EMP_FILIAL.Value,
        DmPediVda.QrFPC1Cliente.Value, DmPediVda.QrFPC1CU_PediVda.Value, True);
    end;
  end;
end;

procedure TNFe_PF.MostraFormNFaEdit_0400_Quiet(FatPedCab, PediVda: Integer;
  Tabela: String; Financeiro, RegrFiscal: Integer; ValorTotal, ValFrete: Double;
  FretePor, Transporta: Integer);
const
  CO_EhNFCe = False;
var
  Especie: String;
  Quantidade, Cliente: Integer;
  Recria, ApenasGeraXML, CalculaAutomatico: Boolean;
  ide_tpEmis: Integer;
  NaoEnviar: Boolean;
begin
  Cliente := DmPediVda.QrFPC1Cliente.Value;
   /// 2020-10-24 SQLType? deve ser Upd para FOEstoqueJaFoiBaixado!
  if DBCheck.CriaFm(TFmNFaEdit_0400_Quiet, FmNFaEdit_0400_Quiet, afmoNegarComAviso) then
  begin
    //FmNFaEdit_0400_Quiet.FOEstoqueJaFoiBaixado := OEstoqueJaFoiBaixado;
    DmPediVda.ReopenFatPedCab(DmPediVda.QrFPC1Codigo.Value, True, False, True, Financeiro);
    //
    //FmNFaEdit_0400_Quiet.ConfiguracoesIniciais();     Abaixo!!!
    //
    FmNFaEdit_0400_Quiet.FEMP_FILIAL           := DmPediVda.QrFPC1EMP_FILIAL.Value;
    FmNFaEdit_0400_Quiet.FCliente              := Cliente;
    //
(*
    //FmNFaEdit_0400_Quiet.CkEmiteAvulso.Checked := Cliente = -2;
    FmNFaEdit_0400_Quiet.LaCNPJCPFAvulso.Enabled := Cliente = -2;
    FmNFaEdit_0400_Quiet.EdCNPJCPFAvulso.Enabled := Cliente = -2;
    FmNFaEdit_0400_Quiet.LaRazaoNomeAvulso.Enabled := Cliente = -2;
    FmNFaEdit_0400_Quiet.EdRazaoNomeAvulso.Enabled := Cliente = -2;
*)
    FmNFaEdit_0400_Quiet.FCU_Pedido             := DmPediVda.QrFPC1CU_PediVda.Value;
    FmNFaEdit_0400_Quiet.EdEmpresa.ValueVariant := DmPediVda.QrFPC1Empresa.Value;
    FmNFaEdit_0400_Quiet.FEmpresa               := DmPediVda.QrFPC1Empresa.Value;
    FmNFaEdit_0400_Quiet.FFatPedCab             := FatPedCab;
    FmNFaEdit_0400_Quiet.FPediVda               := PediVda;
    //
    FmNFaEdit_0400_Quiet.EdRegrFiscal.ValueVariant  := RegrFiscal;
    FmNFaEdit_0400_Quiet.CBRegrFiscal.KeyValue      := RegrFiscal;
    FmNFaEdit_0400_Quiet.EdValFrete.ValueVariant    := ValFrete;










    FmNFaEdit_0400_Quiet.EdFretePor.ValueVariant    := FretePor;
    FmNFaEdit_0400_Quiet.CBFretePor.KeyValue    := FretePor;


    FmNFaEdit_0400_Quiet.EdTransporta.ValueVariant    := Transporta;
    FmNFaEdit_0400_Quiet.CBTransporta.KeyValue    := Transporta;








    if ValFrete > 0.01 then
    begin
      FmNFaEdit_0400_Quiet.EdFretePor.ValueVariant := 0;
      FmNFaEdit_0400_Quiet.CBFretePor.KeyValue     := 0;
    end
    else
    begin
      FmNFaEdit_0400_Quiet.EdFretePor.ValueVariant := 9;
      FmNFaEdit_0400_Quiet.CBFretePor.KeyValue     := 9;
    end;
    FmNFaEdit_0400_Quiet.EdvPag.ValueVariant    := ValorTotal;

    //
      //
      FmNFaEdit_0400_Quiet.ConfiguracoesIniciais();
      //
    try
      if FmNFaEdit_0400_Quiet.ReopenFatPedNFs(1, 0, True) then
      begin
        if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
          DmPediVda.QrFPC1SerieDesfe.Value,
          DmPediVda.QrFPC1NFDesfeita.Value,
          FmNFaEdit_0400_Quiet.QrImprimeSerieNF_Normal.Value,
          FmNFaEdit_0400_Quiet.QrImprimeCtrl_nfs.Value,
          DmPediVda.QrFPC1Empresa.Value,
          FmNFaEdit_0400_Quiet.QrFatPedNFs.FieldByName('Filial').AsInteger,
          FmNFaEdit_0400_Quiet.QrImprimeMaxSeqLib.Value,
          FmNFaEdit_0400_Quiet.EdSerieNF, FmNFaEdit_0400_Quiet.EdNumeroNF) then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;

        Especie    := '';
        Quantidade := 0;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(DmPediVda.QrVolumes, Dmod.MyDB, [
        'SELECT COUNT(fpv.Ativo) Volumes, med.Nome NO_UnidMed ',
        'FROM fatpedvol fpv ',
        'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed ',
        'WHERE fpv.Codigo=' + Geral.FF0(DmPediVda.QrFPC1Codigo.Value),
        'AND fpv.Ativo=1 ',
        'GROUP BY med.Codigo ',
        '']);
        DmPediVda.QrVolumes.First;
        while not DmPediVda.QrVolumes.Eof do
        begin
          Quantidade := Quantidade + DmPediVda.QrVolumesVolumes.Value;
          if Especie <> '' then
            Especie := Especie + ' + ';
          Especie := Especie + ' ' + DmPediVda.QrVolumesNO_UnidMed.Value;
          //
          DmPediVda.QrVolumes.Next;
        end;
        FmNFaEdit_0400_Quiet.FTabela := Tabela;
        //
        FmNFaEdit_0400_Quiet.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
        FmNFaEdit_0400_Quiet.EdEspecie.ValueVariant    := Especie;
        //
        FmNFaEdit_0400_Quiet.EdSerieNF.Enabled  := (FmNFaEdit_0400_Quiet.QrImprimeIncSeqAuto.Value = 0);
        FmNFaEdit_0400_Quiet.EdNumeroNF.Enabled := (FmNFaEdit_0400_Quiet.QrImprimeIncSeqAuto.Value = 0);
        //
        // NF-e 2.00
        if (DModG.QrPrmsEmpNFeCRT.Value = 3) and
        (DModG.QrPrmsEmpNFeNFeNT2013_003LTT.Value < 2) then
          FmNFaEdit_0400_Quiet.PageControl1.ActivePageIndex := 0
        else
          FmNFaEdit_0400_Quiet.PageControl1.ActivePageIndex := 1;
        //
        FmNFaEdit_0400_Quiet.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(DmPediVda.QrFPC1Empresa.Value, DmPediVda.QrFPC1Cliente.Value);
        FmNFaEdit_0400_Quiet.RGCRT.ItemIndex           := DModG.QrPrmsEmpNFeCRT.Value;
        FmNFaEdit_0400_Quiet.EdVagao.Text              := '';
        FmNFaEdit_0400_Quiet.EdBalsa.Text              := '';
        //
        FmNFaEdit_0400_Quiet.EdCompra_XNEmp.Text := '';
        FmNFaEdit_0400_Quiet.EdCompra_XPed.Text  := DmPediVda.QrFPC1PedidoCli.Value;
        FmNFaEdit_0400_Quiet.EdCompra_XCont.Text := '';
        //
        FmNFaEdit_0400_Quiet.ReopenStqMovValX(0);
        // ini2020-10-24
        (*
        if OEstoqueJaFoiBaixado then
          FmNFaEdit_0400_Quiet.ImgTipo.SQLType := stUpd
        else
        // ini2020-10-24
        *)
        // Ini 2021-02-17
        UnDmkDAC_PF.AbreMySQLQuery0(FmNFaEdit_0400_Quiet.QrStqMovNFsA, Dmod.MyDB, [
        'SELECT *  ',
        'FROM stqmovnfsa ',
        'WHERE Tipo=' + Geral.FF0(VAR_FATID_0001),
        'AND OriCodi=' + Geral.FF0(FatPedCab),
        'AND Empresa=' + Geral.FF0(DmPediVda.QrFPC1Empresa.Value),
        //DmPediVda.QrFPC1Codigo.Value
        '']);
        //
        if FmNFaEdit_0400_Quiet.QrStqMovNFsA.RecordCount > 0 then
        begin
          FmNFaEdit_0400_Quiet.ImgTipo.SQLType := stIns;
          FmNFaEdit_0400_Quiet.FReInsere       := True;
          //
          FmNFaEdit_0400_Quiet.FIDCtrl                     := FmNFaEdit_0400_Quiet.QrStqMovNFsAIDCtrl.Value;
          FmNFaEdit_0400_Quiet.MeinfAdic_infCpl.Text       := FmNFaEdit_0400_Quiet.QrStqMovNFsAinfAdic_infCpl.Value;
          FmNFaEdit_0400_Quiet.EdkgBruto.ValueVariant      := FmNFaEdit_0400_Quiet.QrStqMovNFsAkgBruto.Value;
          FmNFaEdit_0400_Quiet.EdkgLiqui.ValueVariant      := FmNFaEdit_0400_Quiet.QrStqMovNFsAkgLiqui.Value;
          FmNFaEdit_0400_Quiet.EdPlacaUF.ValueVariant      := FmNFaEdit_0400_Quiet.QrStqMovNFsAPlacaUF.Value;
          FmNFaEdit_0400_Quiet.EdPlacaNr.ValueVariant      := FmNFaEdit_0400_Quiet.QrStqMovNFsAPlacaNr.Value;
          FmNFaEdit_0400_Quiet.EdEspecie.ValueVariant      := FmNFaEdit_0400_Quiet.QrStqMovNFsAEspecie.Value;
          FmNFaEdit_0400_Quiet.EdMarca.ValueVariant        := FmNFaEdit_0400_Quiet.QrStqMovNFsAMarca.Value;
          FmNFaEdit_0400_Quiet.EdNumero.ValueVariant       := FmNFaEdit_0400_Quiet.QrStqMovNFsANumero.Value;
          FmNFaEdit_0400_Quiet.EdQuantidade.ValueVariant   := FmNFaEdit_0400_Quiet.QrStqMovNFsAQuantidade.Value;
          FmNFaEdit_0400_Quiet.EdObservacao.ValueVariant   := FmNFaEdit_0400_Quiet.QrStqMovNFsAObservacao.Value;
          //
          FmNFaEdit_0400_Quiet.Edvagao.ValueVariant        := FmNFaEdit_0400_Quiet.QrStqMovNFsAvagao.Value;
          FmNFaEdit_0400_Quiet.Edbalsa.ValueVariant        := FmNFaEdit_0400_Quiet.QrStqMovNFsAbalsa.Value;
          FmNFaEdit_0400_Quiet.Eddest_email.ValueVariant   := FmNFaEdit_0400_Quiet.QrStqMovNFsAdest_email.Value;
          //FmNFaEdit_0400_Quiet.RGCRT.ItemIndex             := FmNFaEdit_0400_Quiet.QrStqMovNFsAemit_CRT.Value;
          //
          FmNFaEdit_0400_Quiet.EdRNTC.ValueVariant         := FmNFaEdit_0400_Quiet.QrStqMovNFsARNTC.Value;
          FmNFaEdit_0400_Quiet.EdCompra_XNEmp.ValueVariant := FmNFaEdit_0400_Quiet.QrStqMovNFsACompra_XNEmp.Value;
          FmNFaEdit_0400_Quiet.EdCompra_XPed .ValueVariant := FmNFaEdit_0400_Quiet.QrStqMovNFsACompra_XPed.Value;
          FmNFaEdit_0400_Quiet.EdCompra_XCont.ValueVariant := FmNFaEdit_0400_Quiet.QrStqMovNFsACompra_XCont.Value;
          //
          FmNFaEdit_0400_Quiet.EdUFEmbarq.ValueVariant     := FmNFaEdit_0400_Quiet.QrStqMovNFsAUFEmbarq.Value;
          FmNFaEdit_0400_Quiet.EdxLocEmbarq.ValueVariant   := FmNFaEdit_0400_Quiet.QrStqMovNFsAxLocEmbarq.Value;
          //
        end else
        begin
          FmNFaEdit_0400_Quiet.ImgTipo.SQLType := stIns;
        end;
        FmNFaEdit_0400_Quiet.ShowModal;





        NaoEnviar := FmNFaEdit_0400_Quiet.CkNaoEnviar.Checked;
        if FmNFaEdit_0400_Quiet.FConfirmou then
        begin



    (*
        finally
          FmNFCeEmiss_0400.Destroy;
        end;
        // Reabrir de novo!
    *)
          ReopenFPC1(FatPedCab);
          //
          if DmPediVda.QrFPC1Encerrou.Value > 0 then
          begin
      (*
            MostraFormFatPedNFs(DmPediVda.QrFPC1EMP_FILIAL.Value,
              DmPediVda.QrFPC1Cliente.Value, DmPediVda.QrFPC1CU_PediVda.Value, True);
              :>>
          if DBCheck.CriaFm(TFmFatPedNFs_0400, FmFatPedNFs_0400, afmoNegarComAviso) then
          begin
            FmFatPedNFs_0400.EdFilial.ValueVariant  := EMP_FILIAL;
            FmFatPedNFs_0400.CBFilial.KeyValue      := EMP_FILIAL;
            FmFatPedNFs_0400.EdCliente.ValueVariant := Cliente;
            FmFatPedNFs_0400.CBCliente.KeyValue     := Cliente;
            FmFatPedNFs_0400.EdPedido.ValueVariant  := CU_PediVda;
            FmFatPedNFs_0400.FNaoCriouXML           := ForcaCriarXML;
            FmFatPedNFs_0400.ShowModal;
            FmFatPedNFs_0400.Destroy;
          end
      *)
            Recria            := True;
            ApenasGeraXML     := False;
            CalculaAutomatico := True;
            //
            //Retornar Aqui tpEmis para ver se eh normal ou contingencia off-line!
            if FmNFaEdit_0400_Quiet.Step1_GeraNFe(Recria, ApenasGeraXML,
            CalculaAutomatico, ide_tpEmis) then
            begin
              // erro aqui porque esta criando modelo 65 ao inv�s do 55
              if FmNFaEdit_0400_Quiet.FEditaNFeCabYA_Depois then
                FmNFaEdit_0400_Quiet.Step2_Pagamentos();
              //
              //FmNFaEdit_0400_Quiet.Step1_
              //

              if NaoEnviar then
              begin
      // Testar!
      MostraFormFatPedNFs(DmPediVda.QrFPC1EMP_FILIAL.Value,
      DmPediVda.QrFPC1Cliente.Value, DmPediVda.QrFPC1CU_PediVda.Value, True);
              end else
              begin
                //MostraFormNFCeLEnU(FmNFaEdit_0400_Quiet, ide_tpEmis, 0);
                if UnNFe_PF.MostraFormNFeLEnU_Quiet(FmNFaEdit_0400_Quiet, ide_tpEmis, 0) then ;
              end;
            end;
          end;
        end;
      end;
    finally
      FmNFaEdit_0400_Quiet.Destroy;
    end;
  {
      // Reabrir de novo!
      ReopenFPC1(FatPedCab);
      //
      if DmPediVda.QrFPC1Encerrou.Value > 0 then
      begin
        MostraFormFatPedNFs(DmPediVda.QrFPC1EMP_FILIAL.Value,
          DmPediVda.QrFPC1Cliente.Value, DmPediVda.QrFPC1CU_PediVda.Value, True);
      end;
  }
  end;
end;

procedure TNFe_PF.MostraFormNfeInfCpl();
begin
  if DBCheck.CriaFm(TFmNfeInfCpl, FmNfeInfCpl, afmoNegarComAviso) then
  begin
    FmNfeInfCpl.ShowModal;
    FmNfeInfCpl.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFaInfCpl(Memo: TdmkMemo);
begin
  if DBCheck.CriaFm(TFmNFaInfCpl, FmNFaInfCpl, afmoNegarComAviso) then
  begin
    FmNFaInfCpl.FMemoinfAdic_infCpl := Memo;
    FmNFaInfCpl.ShowModal;
    FmNFaInfCpl.Destroy;
  end;
end;

function TNFe_PF.MostraFormNFeLEnU_Quiet(Form: TForm; ide_tpEmis,
  StatusAchieved: Integer): Boolean;
const
  sProcName = 'TNFe_PF.MostraFormNFeLEnU_Quiet()';
  //
  procedure Mensagem();
  begin
    Geral.MB_Erro('ide_tpEmis n�o implementado em ' + sProcName +
    sLineBreak + 'ide_tpEmis: ' + Geral.FF0(ide_tpEmis) +
    sLineBreak + 'StatusAchieved: ' + Geral.FF0(StatusAchieved));
  end;
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  //FNaoCriouXML := False;
  //
  Result := False;
  case VersaoNFe of
    400:
    if DBCheck.CriaFm(TFmNFeLEnU_0400_Quiet, FmNFeLEnU_0400_Quiet, afmoNegarComAviso) then
    begin
      FmNFeLEnU_0400_Quiet.Fide_tpEmis := ide_tpEmis;
      FmNFeLEnU_0400_Quiet.Show;
      case StatusAchieved of
        0: // Nenhum ainda
        begin
          case ide_tpEmis of
            1: Result := FmNFeLEnU_0400_Quiet.EnviarNFe();
            9: Result := FmNFeLEnU_0400_Quiet.SohImprimeEMudaStatusParaPrecisaEnviar();
            else Mensagem();
          end;
        end;
        35: // Enviar NFe off-line impressa
        begin
          case ide_tpEmis of
            9: Result := FmNFeLEnU_0400_Quiet.AutorizarNFCeImpressaOffLine(Form);
            else Mensagem();
          end;
        end;
        else Mensagem();
      end;
      if Result then
        FmNFeLEnU_0400_Quiet.Destroy;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeItsI03(QrNFeItsI03: TmySQLQuery;
  SQLType: TSQLType);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    0400:
    begin
      UmyMod.FormInsUpd_Show(TFmNFeItsI03_0400, FmNFeItsI03_0400,
        afmoNegarComAviso, QrNFeItsI03, SQLType);
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeItsIDI(QrNFeItsIDI: TmySQLQuery; SQLType: TSQLType);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
     0400:
    begin
      UmyMod.FormInsUpd_Show(TFmNFeItsIDI_0400, FmNFeItsIDI_0400,
        afmoNegarComAviso, QrNFeItsIDI, SQLType);
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeItsIDIa(QrNFeItsIDIa: TmySQLQuery; SQLType: TSQLType);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    0400:
    begin
      UmyMod.FormInsUpd_Show(TFmNFeItsIDIa_0400, FmNFeItsIDIa_0400,
        afmoNegarComAviso, QrNFeItsIDIa, SQLType);
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeJust(Codigo: Integer = 0);
begin
  if DBCheck.CriaFm(TFmNFeJust, FmNFeJust, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmNFeJust.LocCod(Codigo, Codigo);
    FmNFeJust.ShowModal;
    FmNFeJust.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeLEnc(Codigo: Integer);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    if DBCheck.CriaFm(TFmNFeLEnc_0400, FmNFeLEnc_0400, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmNFeLEnc_0400.LocCod(Codigo, Codigo);
      FmNFeLEnc_0400.ShowModal;
      FmNFeLEnc_0400.Destroy;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

function TNFe_PF.MostraFormNFeLEnU(): Boolean;
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  //FNaoCriouXML := False;
  //
  Result := False;
  case VersaoNFe of
    400:
    if DBCheck.CriaFm(TFmNFeLEnU_0400, FmNFeLEnU_0400, afmoNegarComAviso) then
    begin
      FmNFeLEnU_0400.Show;
      Result := FmNFeLEnU_0400.EnviarNFe();
      if Result then
        FmNFeLEnU_0400.Destroy;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeLoad_Arq;
begin
  if MyObjects.CriaForm_AcessoTotal(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
  begin
    FmNFeLoad_Arq.PnCarrega.Visible := True;
    FmNFeLoad_Arq.PnAbre.Visible := True;
    FmNFeLoad_Arq.ShowModal;
    FmNFeLoad_Arq.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeLoad_Dir;
begin
  if MyObjects.CriaForm_AcessoTotal(TFmNFeLoad_Dir, FmNFeLoad_Dir) then
  begin
    FmNFeLoad_Dir.ShowModal;
    FmNFeLoad_Dir.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeLoad_Inn;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DmNFe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value);
  //
  if MyObjects.CriaForm_AcessoTotal(TFmNFeLoad_Inn, FmNFeLoad_Inn) then
  begin
    FmNFeLoad_Inn.EdFatID.ValueVariant := 51;
    FmNFeLoad_Inn.ShowModal;
    FmNFeLoad_Inn.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeLoad_Web;
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DModG.QrPrmsEmpNFeDirNFeRWeb.Value <> '' then
    ForceDirectories(DModG.QrPrmsEmpNFeDirNFeRWeb.Value);
  if MyObjects.FIC(not DirectoryExists(DModG.QrPrmsEmpNFeDirNFeRWeb.Value),
  nil, '� necess�rio informar o seguinte diret�rio nas op��es da filial:' + #13#10 +
  '"Diret�rio das NF-e recuperadas da web (autorizadas e canceladas)"') then
    Exit;
    //
  if MyObjects.CriaForm_AcessoTotal(TFmNFeLoad_Web, FmNFeLoad_Web) then
  begin
    FmNFeLoad_Web.ShowModal;
    FmNFeLoad_Web.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeNewVer();
begin
  if DBCheck.CriaFm(TFmNFeNewVer, FmNFeNewVer, afmoSoMaster) then
  begin
    FmNFeNewVer.ShowModal;
    FmNFeNewVer.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFePesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
  PageControl: TdmkPageControl; Cliente, QuemEmit: Integer);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmNFe_Pesq_0000, InOwner, PageControl, True, True);
    //
    if Cliente <> 0 then
    begin
      if Form <> nil then
      begin
        TFmNFe_Pesq_0000(Form).EdCliente.ValueVariant := Cliente;
        TFmNFe_Pesq_0000(Form).CBCliente.KeyValue     := Cliente;
        TFmNFe_Pesq_0000(Form).RGQuemEmit.ItemIndex   := QuemEmit;
      end;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      if Cliente <> 0 then
      begin
        FmNFe_Pesq_0000.EdCliente.ValueVariant := Cliente;
        FmNFe_Pesq_0000.CBCliente.KeyValue     := Cliente;
      end;
      FmNFe_Pesq_0000.RGQuemEmit.ItemIndex := QuemEmit;
      FmNFe_Pesq_0000.ShowModal;
      FmNFe_Pesq_0000.Destroy;
    end;
  end;
end;

procedure TNFe_PF.MostraFormNFePesq_IDCtrl_Unico(AbrirEmAba: Boolean;
  InOwner: TWincontrol; PageControl: TdmkPageControl; IDCtrl: Integer);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmNFe_Pesq_0000, InOwner, PageControl, True, True);
    //
    if IDCtrl <> 0 then
    begin
      if Form <> nil then
      begin
        TFmNFe_Pesq_0000(Form).ReopenNFeCabA(IDCtrl, True);
      end;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      if IDCtrl <> 0 then
        FmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl, True);
      FmNFe_Pesq_0000.ShowModal;
      FmNFe_Pesq_0000.Destroy;
    end;
  end;
end;

procedure TNFe_PF.MostraFormNFeWebservices();
begin
  if DBCheck.CriaFm(TFmNFeWebServices_0000, FmNFeWebServices_0000, afmoSoMaster) then
  begin
    FmNFeWebServices_0000.ShowModal;
    FmNFeWebServices_0000.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeWebservicesGer;
begin
  if DBCheck.CriaFm(TFmNFeWebServicesGer_0000, FmNFeWebServicesGer_0000, afmoSoMaster) then
  begin
    FmNFeWebServicesGer_0000.ShowModal;
    FmNFeWebServicesGer_0000.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFe_Pesq_0000_ImpFatur(CGcSitConf: TdmkCheckGroup;
  EdFilial_ValueVariant: Variant; RGQuemEmit_ItemIndex, EdCliente_ValueVariant,
  RGAmbiente_ItemIndex, QrClientesTipo_Value: Integer; CBFilial_Text,
  CBCliente_Text, QrClientesCNPJ_Value, QrClientesCPF_Value: String;
  TPDataI_Date, TPDataF_Date: TDateTime; Ck100e101_Checked: Boolean);
begin
  if DBCheck.CriaFm(TFmNFe_Pesq_0000_ImpFat, FmNFe_Pesq_0000_ImpFat, afmoSemVerificar) then
  begin
    FmNFe_Pesq_0000_ImpFat.TPDataI_Date           := TPDataI_Date;
    FmNFe_Pesq_0000_ImpFat.TPDataF_Date           := TPDataF_Date;
    FmNFe_Pesq_0000_ImpFat.CGcSitConf             := CGcSitConf;
    FmNFe_Pesq_0000_ImpFat.EdFilial_ValueVariant  := EdFilial_ValueVariant;
    FmNFe_Pesq_0000_ImpFat.RGQuemEmit_ItemIndex   := RGQuemEmit_ItemIndex;
    FmNFe_Pesq_0000_ImpFat.EdCliente_ValueVariant := EdCliente_ValueVariant;
    FmNFe_Pesq_0000_ImpFat.RGAmbiente_ItemIndex   := RGAmbiente_ItemIndex;
    FmNFe_Pesq_0000_ImpFat.QrClientesTipo_Value   := QrClientesTipo_Value;
    FmNFe_Pesq_0000_ImpFat.CBFilial_Text          := CBFilial_Text;
    FmNFe_Pesq_0000_ImpFat.CBCliente_Text         := CBCliente_Text;
    FmNFe_Pesq_0000_ImpFat.QrClientesCNPJ_Value   := QrClientesCNPJ_Value;
    FmNFe_Pesq_0000_ImpFat.QrClientesCPF_Value    := QrClientesCPF_Value;
    FmNFe_Pesq_0000_ImpFat.Ck100e101_Checked      := Ck100e101_Checked;
    //
    FmNFe_Pesq_0000_ImpFat.ShowModal;
    FmNFe_Pesq_0000_ImpFat.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFe_Pesq_0000_ImpLista(PorCFOP: TNFEAgruRelNFs;
  EdFilial_ValueVariant: Variant; TPDataI_Date, TPDataF_Date: TDateTime;
  RGQuemEmit_ItemIndex, RGAmbiente_ItemIndex, EdCliente_ValueVariant,
  QrClientesTipo_Value, RGOrdem1_ItemIndex, RGOrdem2_ItemIndex: Integer;
  Ck100e101_Checked: Boolean; QrClientesCNPJ_Value, QrClientesCPF_Value: String;
  CGcSitConf: TdmkCheckGroup; CkideNatOp_Checked: Boolean; CBFilial_Text,
  CBCliente_Text: String);
begin
  //� apenas um form para armazenar os FRX
  if DBCheck.CriaFm(TFmNFe_Pesq_0000_ImpLista, FmNFe_Pesq_0000_ImpLista, afmoSemVerificar) then
  begin
    //FmNFe_Pesq_0000_ImpLista.ShowModal;
    FmNFe_Pesq_0000_ImpLista.EdFilial_ValueVariant  := EdFilial_ValueVariant;
    FmNFe_Pesq_0000_ImpLista.TPDataI_Date           := TPDataI_Date;
    FmNFe_Pesq_0000_ImpLista.TPDataF_Date           := TPDataF_Date;
    FmNFe_Pesq_0000_ImpLista.RGQuemEmit_ItemIndex   := RGQuemEmit_ItemIndex;
    FmNFe_Pesq_0000_ImpLista.RGAmbiente_ItemIndex   := RGAmbiente_ItemIndex;
    FmNFe_Pesq_0000_ImpLista.EdCliente_ValueVariant := EdCliente_ValueVariant;
    FmNFe_Pesq_0000_ImpLista.QrClientesTipo_Value   := QrClientesTipo_Value;
    FmNFe_Pesq_0000_ImpLista.RGOrdem1_ItemIndex     := RGOrdem1_ItemIndex;
    FmNFe_Pesq_0000_ImpLista.RGOrdem2_ItemIndex     := RGOrdem2_ItemIndex;
    FmNFe_Pesq_0000_ImpLista.Ck100e101_Checked      := Ck100e101_Checked;
    FmNFe_Pesq_0000_ImpLista.QrClientesCNPJ_Value   := QrClientesCNPJ_Value;
    FmNFe_Pesq_0000_ImpLista.QrClientesCPF_Value    := QrClientesCPF_Value;
    FmNFe_Pesq_0000_ImpLista.CGcSitConf             := CGcSitConf;
    FmNFe_Pesq_0000_ImpLista.CkideNatOp_Checked     := CkideNatOp_Checked;
    FmNFe_Pesq_0000_ImpLista.CBFilial_Text          := CBFilial_Text;
    FmNFe_Pesq_0000_ImpLista.CBCliente_Text         := CBCliente_Text;
    //
    case PorCFOP of
      (* Antigo
      nfearnCOFP:
        FmNFe_Pesq_0000_ImpLista.ImprimeListaCFOP();
      *)
      nfearnCOFP:
        FmNFe_Pesq_0000_ImpLista.ImprimeLista_Its(nfearnCOFP);
      nfearnNCM:
        FmNFe_Pesq_0000_ImpLista.ImprimeLista_Its(nfearnNCM);
      nfearnNatOP:
        FmNFe_Pesq_0000_ImpLista.ImprimeListaNatOp();
      nfearnProd:
        FmNFe_Pesq_0000_ImpLista.ImprimeLista_Its(nfearnProd);
      nfearnTipPrd:
        FmNFe_Pesq_0000_ImpLista.ImprimeLista_Its(nfearnTipPrd);
    end;
    FmNFe_Pesq_0000_ImpLista.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeCabA;
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeCabGA(FatID, FatNum, Empresa, DestRem: Integer;
  InserePadroes: Boolean);
(*
var
  Qry: TmySQLQuery;

  procedure InsereAtual();
  var
    autXML_CNPJ, autXML_CPF: String;
    Controle, AddForma, AddIDCad, Tipo: Integer;
  begin
    Controle       := 0;
    Tipo           := Qry.FieldByName('Tipo').AsInteger;
    AddForma       := Integer(TNFeautXML.naxEntiContat); // EntiContat.Controle
    AddIDCad       := Qry.FieldByName('Controle').AsInteger;
    if Tipo = 0 then
    begin
      autXML_CNPJ    := Geral.SoNumero_TT(Qry.FieldByName('CNPJ').AsString);
      autXML_CPF     := '';
      if MyObjects.FIC(Length(autXML_CNPJ) <> 14, nil,
      'O CNPJ ' + autXML_CNPJ + ' n�o foi inclu�do pois n�p possui 14 algarismos!') then
        Exit;
    end else begin
      autXML_CNPJ    := '';
      autXML_CPF     := Geral.SoNumero_TT(Qry.FieldByName('CPF').AsString);
      if MyObjects.FIC(Length(autXML_CPF) <> 11, nil,
      'O CPF ' + autXML_CPF + ' n�o foi inclu�do pois n�p possui 11 algarismos!') then
        Exit;
    end;
    //
    Controle := UMyMod.BPGS1I32('nfecabga', 'Controle', '', '', tsPos, stIns, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabga', False, [
    'autXML_CNPJ', 'autXML_CPF', 'AddForma',
    'AddIDCad', 'Tipo'], [
    'FatID', 'FatNum', 'Empresa', 'Controle'], [
    autXML_CNPJ, autXML_CPF, AddForma,
    AddIDCad, Tipo], [
    FatID, FatNum, Empresa, Controle], True) then ;
  end;
*)
begin
  (*
  if InserePadroes then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      // Ver se j[a nao foi criado!!
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM nfecabga ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      if Qry.RecordCount = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, Tipo, CNPJ, CPF ',
        'FROM enticontat ',
        'WHERE Codigo=' + Geral.FF0(DestRem),
        'AND Aplicacao & 1 ',
        'And Ativo=1 ',
        'LIMIT 10 ',
        '']);
        if Qry.RecordCount = 0 then
          Geral.MB_Aviso(
          'Destinat�rio / remetente sem emails cadastrados para autoriza��o de obten��o de XML!')
        else
        begin
          Qry.First;
          while not Qry.Eof do
          begin
            InsereAtual();
            //
            Qry.Next;
          end;
        end
      end;
    finally
      Qry.Free;
    end;
  end;
  *)
  if DBCheck.CriaFm(TFmNFeCabGA, FmNFeCabGA, afmoNegarComAviso) then
  begin
    FmNFeCabGA.FFatID         := FatID;
    FmNFeCabGA.FFatNum        := FatNum;
    FmNFeCabGA.FEmpresa       := Empresa;
    FmNFeCabGA.FDestRem       := DestRem;
    FmNFeCabGA.FInserePadroes := InserePadroes;
    //
    FmNFeCabGA.ReopenNFeCabGA(0);
    FmNFeCabGA.ShowModal;
    FmNFeCabGA.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeCabXLac(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabXLac_0000, FmNFeCabXLac_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabXLac_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabXLac_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabXLac_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabXLac_0000.FControle := QryCab.FieldByName('Controle').AsInteger;
  FmNFeCabXLac_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
    FmNFeCabXLac_0000.FConta  := QrIts.FieldByName('Conta').AsInteger
  else
    FmNFeCabXLac_0000.FConta := 0;
  FmNFeCabXLac_0000.FQryIts   := QrIts;
  //
  FmNFeCabXLac_0000.ShowModal;
  FmNFeCabXLac_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCabXReb(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabXReb_0000, FmNFeCabXReb_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabXReb_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabXReb_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabXReb_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabXReb_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
    FmNFeCabXReb_0000.FControle := QrIts.FieldByName('Controle').AsInteger
  else
    FmNFeCabXReb_0000.FControle := 0;
  FmNFeCabXReb_0000.FQryIts   := QrIts;
  //
  FmNFeCabXReb_0000.ShowModal;
  FmNFeCabXReb_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCabXVol(SQLType: TSQLType; QryCab, QrIts:
              TMySQLQuery; MaxRegCount: Integer);
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabXVol_0000, FmNFeCabXVol_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabXVol_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabXVol_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabXVol_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabXVol_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
    FmNFeCabXVol_0000.FControle := QrIts.FieldByName('Controle').AsInteger
  else
    FmNFeCabXVol_0000.FControle := 0;
  FmNFeCabXVol_0000.FQryIts   := QrIts;
  //
  FmNFeCabXVol_0000.ShowModal;
  FmNFeCabXVol_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCabZCon(SQLType: TSQLType; QryCab,
  QrIts: TMySQLQuery; MaxRegCount: Integer);
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabZCon_0000, FmNFeCabZCon_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabZCon_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabZCon_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabZCon_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabZCon_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
    FmNFeCabZCon_0000.FControle := QrIts.FieldByName('Controle').AsInteger
  else
    FmNFeCabZCon_0000.FControle := 0;
  FmNFeCabZCon_0000.FQryIts   := QrIts;
  //
  FmNFeCabZCon_0000.ShowModal;
  FmNFeCabZCon_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCabZFis(SQLType: TSQLType; QryCab,
  QrIts: TMySQLQuery; MaxRegCount: Integer);
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabZFis_0000, FmNFeCabZFis_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabZFis_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabZFis_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabZFis_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabZFis_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
    FmNFeCabZFis_0000.FControle := QrIts.FieldByName('Controle').AsInteger
  else
    FmNFeCabZFis_0000.FControle := 0;
  FmNFeCabZFis_0000.FQryIts   := QrIts;
  //
  FmNFeCabZFis_0000.ShowModal;
  FmNFeCabZFis_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCabZPro(SQLType: TSQLType; QryCab,
  QrIts: TMySQLQuery; MaxRegCount: Integer);
var
  I, N: Integer;
begin
  if (MaxRegCount > 0) and (QrIts.RecordCount >= MaxRegCount) then
  begin
    Geral.MB_Aviso('Limite m�ximo de registros atingido!');
    Exit;
  end;
  UmyMod.FormInsUpd_Cria(TFmNFeCabZPro_0000, FmNFeCabZPro_0000, afmoNegarComAviso,
    QrIts, SQLType);
  FmNFeCabZPro_0000.FFatID    := QryCab.FieldByName('FatID').AsInteger;
  FmNFeCabZPro_0000.FFatNum   := QryCab.FieldByName('FatNum').AsInteger;
  FmNFeCabZPro_0000.FEmpresa  := QryCab.FieldByName('Empresa').AsInteger;
  FmNFeCabZPro_0000.FMaxRegCount := MaxRegCount;
  if SQLType = stUpd then
  begin
    FmNFeCabZPro_0000.FControle := QrIts.FieldByName('Controle').AsInteger;
    for I := 0 to FmNFeCabZPro_0000.RGindProc.Items.Count -1 do
    begin
      N := Geral.IMV(FmNFeCabZPro_0000.RGindProc.Items[I][1]);
      if N = QrIts.FieldByName('indProc').AsInteger then
      begin
        FmNFeCabZPro_0000.RGindProc.ItemIndex := I;
        Break;
      end;
    end;
  end else
    FmNFeCabZPro_0000.FControle := 0;
  FmNFeCabZPro_0000.FQryIts   := QrIts;
  //
  FmNFeCabZPro_0000.ShowModal;
  FmNFeCabZPro_0000.Destroy;
end;

procedure TNFe_PF.MostraFormNFeCnfDowC_0100();
begin
  if DBCheck.CriaFm(TFmNFeCnfDowC_0100, FmNFeCnfDowC_0100, afmoNegarComAviso) then
  begin
    FmNFeCnfDowC_0100.ShowModal;
    FmNFeCnfDowC_0100.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeCntngnc;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmNFeCntngnc, FmNFeCntngnc, afmoSoBoss) then
  begin
    FmNFeCntngnc.ShowModal;
    FmNFeCntngnc.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeConsulta;
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      if DBCheck.CriaFm(TFmNFeConsulta_0400, FmNFeConsulta_0400, afmoSoBoss) then
      begin
        FmNFeConsulta_0400.ShowModal;
        FmNFeConsulta_0400.Destroy;
      end;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormNFeDesConC_0101;
begin
(*  {$IFNDef semNFe_v 0 2 0 0}
  if DBCheck.CriaFm(TFmNFeDesConC_0101, FmNFeDesConC_0101, afmoNegarComAviso) then
  begin
    FmNFeDesConC_0101.ShowModal;
    FmNFeDesConC_0101.Destroy;
  end;
  {$EndIf}
*)
end;

procedure TNFe_PF.MostraFormNFeDesDowC_0100;
begin
  if DBCheck.CriaFm(TFmNFeDesDowC_0100, FmNFeDesDowC_0100, afmoNegarComAviso) then
  begin
    FmNFeDesDowC_0100.ShowModal;
    FmNFeDesDowC_0100.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeDistDFeInt();
begin
  if DBCheck.CriaFm(TFmNFeDistDFeInt_0100, FmNFeDistDFeInt_0100, afmoNegarComAviso) then
  begin
    FmNFeDistDFeInt_0100.ShowModal;
    FmNFeDistDFeInt_0100.Destroy;
  end;
end;

{}
procedure TNFe_PF.MostraFormNFeEFC_C170_Cab(FatID, FatNum, Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmNFeEFD_C170_Cab, FmNFeEFD_C170_Cab, afmoNegarComAviso) then
  begin
    FmNFeEFD_C170_Cab.FFatID   := FatID;
    FmNFeEFD_C170_Cab.FFatNum  := FatNum;
    FmNFeEFD_C170_Cab.FEmpresa := Empresa;
    //
    FmNFeEFD_C170_Cab.ReopenNfeCabA();
    //
    FmNFeEFD_C170_Cab.ShowModal;
    FmNFeEFD_C170_Cab.Destroy;
  end;
end;
{}

{}
procedure TNFe_PF.MostraFormNFeEFD_C170(SQLType: TSQLType; FatID, FatNum,
  Empresa, nItem: Integer);
var
  MySQLType: TSQLType;
  Qry: TmySQLQuery;
  CFOP, CST_ICMS, GraGruX: Integer;
  //
  function vi(Campo: String): Integer;
  begin
    Result := Qry.FieldByName(Campo).AsInteger;
  end;
  function vs(Campo: String): String;
  begin
    Result := Qry.FieldByName(Campo).AsString;
  end;
  function vf(Campo: String): Double;
  begin
    Result := Qry.FieldByName(Campo).AsFLoat;
  end;
begin
  if DBCheck.CriaFm(TFmNFeEFD_C170, FmNFeEFD_C170, afmoLiberado) then
  begin
    //FmNFeEFD_C170.ImgTipo.SQLType := MySQLType;
    FmNFeEFD_C170.FFatID          := FatID;
    FmNFeEFD_C170.FFatNum         := FatNum;
    FmNFeEFD_C170.FEmpresa        := Empresa;
    FmNFeEFD_C170.FnItem          := nItem;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(FmNFeEFD_C170.QrNfeCabA, Dmod.MyDB, [
      'SELECT Id, ide_natOp, ide_mod, ide_serie, ide_nNF, ide_dEmi,  ',
      'IF(emit_CNPJ <> "", "CNPJ", "CPF") KIND_DOC, ',
      'IF(emit_CNPJ <> "", emit_CNPJ, emit_CPF) CNPJ_CPF, ',
      'emit_xNome, emit_xMun, emit_UF ',
      'FROM nfecaba ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      //
      MySQLType := SQLType;
      if (MySQLType = stNil) or (MySQLType = stUpd) then
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT *  ',
        'FROM nfeEFD_C170 ',
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nItem=' + Geral.FF0(nItem),
        '']);
        if MySQLType = stNil then
        begin
          if Qry.RecordCount > 0 then
            MySQLType := stUpd
          else
            MySQLType := stIns;
        end;
        if MySQLType = stUpd then
        begin
          FmNFeEFD_C170.EdGraGruX.ValueVariant         := vi('GraGruX');
          FmNFeEFD_C170.MeDESCR_COMPL.Text             := vs('DESCR_COMPL');
          FmNFeEFD_C170.EdQTD.ValueVariant             := vf('QTD');
          FmNFeEFD_C170.EdUNID.Text                    := vs('UNID');
          FmNFeEFD_C170.EdVL_ITEM.ValueVariant         := vf('VL_ITEM');
          FmNFeEFD_C170.EdVL_DESC.ValueVariant         := vf('VL_DESC');
          FmNFeEFD_C170.RGIND_MOV.ItemIndex            := vi('IND_MOV');
          FmNFeEFD_C170.EdCST_ICMS.ValueVariant        := vf('CST_ICMS');
          FmNFeEFD_C170.EdCFOP.ValueVariant            := vi('CFOP');
          FmNFeEFD_C170.EdCOD_NAT.Text                 := vs('COD_NAT');
          FmNFeEFD_C170.EdVL_BC_ICMS.ValueVariant      := vf('VL_BC_ICMS');
          FmNFeEFD_C170.EdALIQ_ICMS.ValueVariant       := vf('ALIQ_ICMS');
          FmNFeEFD_C170.EdVL_ICMS.ValueVariant         := vf('VL_ICMS');
          FmNFeEFD_C170.EdVL_BC_ICMS_ST.ValueVariant   := vf('VL_BC_ICMS_ST');
          FmNFeEFD_C170.EdALIQ_ST.ValueVariant         := vf('ALIQ_ST');
          FmNFeEFD_C170.EdVL_ICMS_ST.ValueVariant      := vf('VL_ICMS_ST');
          FmNFeEFD_C170.EdIND_APUR.Text                := vs('IND_APUR');
          FmNFeEFD_C170.EdCST_IPI.Text                 := vs('CST_IPI');
          FmNFeEFD_C170.EdCOD_ENQ.Text                 := vs('COD_ENQ');
          FmNFeEFD_C170.EdVL_BC_IPI.ValueVariant       := vf('VL_BC_IPI');
          FmNFeEFD_C170.EdALIQ_IPI.ValueVariant        := vf('ALIQ_IPI');
          FmNFeEFD_C170.EdVL_IPI.ValueVariant          := vf('VL_IPI');
          FmNFeEFD_C170.EdCST_PIS.Text                 := vs('CST_PIS');
          FmNFeEFD_C170.EdVL_BC_PIS.ValueVariant       := vf('VL_BC_PIS');
          FmNFeEFD_C170.EdALIQ_PIS_p.ValueVariant      := vf('ALIQ_PIS_p');
          FmNFeEFD_C170.EdQUANT_BC_PIS.ValueVariant    := vf('QUANT_BC_PIS');
          FmNFeEFD_C170.EdALIQ_PIS_r.ValueVariant      := vf('ALIQ_PIS_r');
          FmNFeEFD_C170.EdVL_PIS.ValueVariant          := vf('VL_PIS');
          FmNFeEFD_C170.EdCST_COFINS.Text              := vs('CST_COFINS');
          FmNFeEFD_C170.EdVL_BC_COFINS.ValueVariant    := vf('VL_BC_COFINS');
          FmNFeEFD_C170.EdALIQ_COFINS_p.ValueVariant   := vf('ALIQ_COFINS_p');
          FmNFeEFD_C170.EdQUANT_BC_COFINS.ValueVariant := vf('QUANT_BC_COFINS');
          FmNFeEFD_C170.EdALIQ_COFINS_r.ValueVariant   := vf('ALIQ_COFINS_r');
          FmNFeEFD_C170.EdVL_COFINS.ValueVariant       := vf('VL_COFINS');
          FmNFeEFD_C170.EdCOD_CTA.Text                 := vs('COD_CTA');
        end else
        begin
          UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT *  ',
          'FROM nfeitsi ',
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND Empresa=' + Geral.FF0(Empresa),
          'AND nItem=' + Geral.FF0(nItem),
          '']);
          FmNFeEFD_C170.FCFOP_OUT := vi('prod_CFOP');
          GraGruX := vi('GraGruX');
          FmNFeEFD_C170.EdGraGruX.ValueVariant         := GraGruX;
          FmNFeEFD_C170.CBGraGruX.KeyValue             := GraGruX;
          //FmNFeEFD_C170.MeDESCR_COMPL.Text             := vs('DESCR_COMPL');
          FmNFeEFD_C170.EdQTD.ValueVariant             := vf('prod_qCom');
          FmNFeEFD_C170.EdUNID.Text                    := vs('prod_uCom');
          FmNFeEFD_C170.EdVL_ITEM.ValueVariant         := vf('prod_vProd');
          FmNFeEFD_C170.EdVL_DESC.ValueVariant         := vf('prod_vDesc');
          //
          FmNFeEFD_C170.RGIND_MOV.ItemIndex            := 2;
          //CFOP := vi('prod_CFOP');
          CFOP :=
            Geral.IMV(ObtemCFOPEntradaDeSaida(
            Geral.FF0(FmNFeEFD_C170.FCFOP_OUT), Empresa, GraGruX));
          FmNFeEFD_C170.EdCFOP.ValueVariant            := CFOP;
          FmNFeEFD_C170.EdCOD_NAT.Text                 := '';
          UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT *  ',
          'FROM nfeitsn ',
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND Empresa=' + Geral.FF0(Empresa),
          'AND nItem=' + Geral.FF0(nItem),
          '']);
          CST_ICMS := (vi('ICMS_Orig') * 100) + vi('ICMS_CST');
          FmNFeEFD_C170.EdCST_ICMS.ValueVariant        := CST_ICMS;
          FmNFeEFD_C170.EdVL_BC_ICMS.ValueVariant      := vf('ICMS_vBC');
          FmNFeEFD_C170.EdALIQ_ICMS.ValueVariant       := vf('ICMS_pICMS');
          FmNFeEFD_C170.EdVL_ICMS.ValueVariant         := vf('ICMS_vICMS');
          FmNFeEFD_C170.EdVL_BC_ICMS_ST.ValueVariant   := vf('ICMS_vBCST');
          FmNFeEFD_C170.EdALIQ_ST.ValueVariant         := vf('ICMS_pICMSST');
          FmNFeEFD_C170.EdVL_ICMS_ST.ValueVariant      := vf('ICMS_vICMSST');
          UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT *  ',
          'FROM nfeitso ',
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND Empresa=' + Geral.FF0(Empresa),
          'AND nItem=' + Geral.FF0(nItem),
          '']);
          FmNFeEFD_C170.EdIND_APUR.Text                := '0';
          FmNFeEFD_C170.EdCST_IPI.Text                 := '49';
          FmNFeEFD_C170.EdCOD_ENQ.Text                 := vs('IPI_cEnq');
          if FmNFeEFD_C170.EdCOD_ENQ.Text = '999' then
            FmNFeEFD_C170.EdCOD_ENQ.Text := '';
          FmNFeEFD_C170.EdVL_BC_IPI.ValueVariant       := vf('IPI_vBC');
          FmNFeEFD_C170.EdALIQ_IPI.ValueVariant        := vf('IPI_pIPI');
          FmNFeEFD_C170.EdVL_IPI.ValueVariant          := vf('IPI_vIPI');
          UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT *  ',
          'FROM nfeitsq ',
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND Empresa=' + Geral.FF0(Empresa),
          'AND nItem=' + Geral.FF0(nItem),
          '']);
          FmNFeEFD_C170.EdCST_PIS.Text                 := '98';
          FmNFeEFD_C170.EdVL_BC_PIS.ValueVariant       := vf('PIS_vBC');
          FmNFeEFD_C170.EdALIQ_PIS_p.ValueVariant      := vf('PIS_pPIS');
          FmNFeEFD_C170.EdQUANT_BC_PIS.ValueVariant    := vf('PIS_qBCProd');
          FmNFeEFD_C170.EdALIQ_PIS_r.ValueVariant      := vf('PIS_vAliqProd');
          FmNFeEFD_C170.EdVL_PIS.ValueVariant          := vf('PIS_vPIS');
          UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT *  ',
          'FROM nfeitss ',
          'WHERE FatID=' + Geral.FF0(FatID),
          'AND FatNum=' + Geral.FF0(FatNum),
          'AND Empresa=' + Geral.FF0(Empresa),
          'AND nItem=' + Geral.FF0(nItem),
          '']);
          FmNFeEFD_C170.EdCST_COFINS.Text              := '98';
          FmNFeEFD_C170.EdVL_BC_COFINS.ValueVariant    := vf('COFINS_vBC');
          FmNFeEFD_C170.EdALIQ_COFINS_p.ValueVariant   := vf('COFINS_pCOFINS');
          FmNFeEFD_C170.EdQUANT_BC_COFINS.ValueVariant := vf('COFINS_qBCProd');
          FmNFeEFD_C170.EdALIQ_COFINS_r.ValueVariant   := vf('COFINS_vAliqProd');
          FmNFeEFD_C170.EdVL_COFINS.ValueVariant       := vf('COFINS_vCOFINS');
          //////////////////////////////////////////////////////////////////////////
          FmNFeEFD_C170.EdCOD_CTA.Text                 := '';
        end;
      end;
    finally
      Qry.Free;
    end;
    FmNFeEFD_C170.ImgTipo.SQLType := MySQLType;
    FmNFeEFD_C170.ShowModal;
    FmNFeEFD_C170.Destroy;
  end;
end;
{}

procedure TNFe_PF.MostraFormNFeEveRLoE(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmNFeEveRLoE, FmNFeEveRLoE, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmNFeEveRLoE.LocCod(Codigo, Codigo);
    FmNFeEveRLoE.ShowModal;
    FmNFeEveRLoE.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeExportaXML;
begin
  if DBCheck.CriaFm(TFmNFeExportaXML, FmNFeExportaXML, afmoNegarComAviso) then
  begin
    FmNFeExportaXML.EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
    FmNFeExportaXML.CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
    FmNFeExportaXML.ShowModal;
    FmNFeExportaXML.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeExportaXML_B;
begin
  if DBCheck.CriaFm(TFmNFeExportaXML_B, FmNFeExportaXML_B, afmoNegarComAviso) then
  begin
    FmNFeExportaXML_B.ShowModal;
    FmNFeExportaXML_B.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormNFeInut();
begin
  if DBCheck.CriaFm(TFmNFeInut_0000, FmNFeInut_0000, afmoNegarComAviso) then
  begin
    FmNFeInut_0000.ShowModal;
    FmNFeInut_0000.Destroy;
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_EnvioLoteEvento(SohLer: Boolean;
  CodsEve: String; EventoLote, Empresa, ide_mod: Integer);
const
  sProcName = 'TNFe_PF.MostraFormStepsNFe_EnvioLoteEvento()';
var
  UF_Servico, sMsg: String;
  VersaoNFe: Integer;
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  if ImpedePelaMisturaDeWebServices(CodsEve, EventoLote, UF_Servico) then
    Exit;
  //
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
      case TFormaGerenXXe(VAR_DFeAppCode) of
        (*0*)fgxxeCAPICOM:
        begin
          if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
          begin
            FmNFeSteps_0400.PnLoteEnv.Visible := True;
            FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnviarLoteEventosNFe); // 6 - Enviar lote de Eventos da NFe
            FmNFeSteps_0400.CkSoLer.Checked  := SohLer;
            FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerLotEve.Value;
            FmNFeSteps_0400.Edide_mod.ValueVariant := ide_mod;
            FmNFeSteps_0400.PreparaEnvioDeLoteEvento(UF_Servico, EventoLote, Empresa);
            FmNFeSteps_0400.FFormChamou      := 'FmNFeEveRLoE';
            FmNFeSteps_0400.ShowModal;
            FmNFeSteps_0400.Destroy;
            //
            //LocCod(EventoLote, EventoLote);
          end;
        end;
        //(*1*)fgxxeCMaisMais:
        (*2*)fgxxeACBr:
        begin
          if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
          begin
            FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
            FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnviarLoteEventosNFe); // 6 - Enviar lote de Eventos da NFe
            FmDmkACBrNFeSteps_0400.CkSoLer.Checked  := SohLer;
            FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerLotEve.Value;
            FmDmkACBrNFeSteps_0400.Edide_mod.ValueVariant := ide_mod;
            FmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteEvento(UF_Servico, EventoLote, Empresa);
            FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFeEveRLoE';
            FmDmkACBrNFeSteps_0400.ShowModal;
            FmDmkACBrNFeSteps_0400.Destroy;
            //
            //LocCod(EventoLote, EventoLote);
          end;
        end;
        (*?*)else
        begin
          sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
          Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
        end;
      end;
    end;
    else
      XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_ConsultaCadastroEntidade(const UF, CNPJPF:
  String; Mostra: Boolean);
const
  sProcName = 'TNFe_PF.MostraFormStepsNFe_ConsultaCadastroEntidade()';
var
  VersaoNFe: Integer;
  sMsg: String;
begin
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    400:
    begin
      // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
      case TFormaGerenXXe(VAR_DFeAppCode) of
        (*0*)fgxxeCAPICOM:
        begin
          if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            FmNFeSteps_0400.PnLoteEnv.Visible := True;
            FmNFeSteps_0400.PnCadastroContribuinte.Visible := True;
            //
            FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultaCadastroEntidade); //7 - Consulta cadastro entidade
            FmNFeSteps_0400.PreparaConsultaCadastro(DmodG.QrFiliLogCodigo.Value);
            //
            FmNFeSteps_0400.EdContribuinte_UF.Text := UF;
            FmNFeSteps_0400.EdContribuinte_CNPJ.Text := CNPJPF;
            //
            if Mostra then
              FmNFeSteps_0400.Show
            else
            begin
              FmNFeSteps_0400.BtOKClick(FmNFeSteps_0400);
              FmNFeSteps_0400.Close;
            end;
            //
            //FmNFeSteps_0400.Destroy;
            //
          end;
         end;
        //(*1*)fgxxeCMaisMais:
        (*2*)fgxxeACBr:
        begin
           if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
            //
            FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultaCadastroEntidade); //7 - Consulta cadastro entidade
            FmDmkACBrNFeSteps_0400.PreparaConsultaCadastro(DmodG.QrFiliLogCodigo.Value);
            //
            FmDmkACBrNFeSteps_0400.EdContribuinte_UF.Text := UF;
            FmDmkACBrNFeSteps_0400.EdContribuinte_CNPJ.Text := CNPJPF;
            //
            if Mostra then
              FmDmkACBrNFeSteps_0400.Show
            else
            begin
              FmDmkACBrNFeSteps_0400.BtOKClick(FmDmkACBrNFeSteps_0400);
              FmDmkACBrNFeSteps_0400.Close;
            end;
            //
            //FmDmkACBrNFeSteps_0400.Destroy;
            //
          end;
        end;
        (*?*)else
        begin
          sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
          Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
        end;
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_ConsultaDocumentosDestinados();
begin
(*&
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
  begin
    FmNFeSteps_0200.PnLoteEnv.Visible := True;
    FmNFeSteps_0200.EdVersaoAcao.ValueVariant := verDowNFeDest_Int;
    FmNFeSteps_0200.MeChaves.Lines.Clear;
    QrNFeDesDowI.DisableControls;
    try
      QrNFeDesDowI.First;
      while not QrNFeDesDowI.Eof do
      begin
        FmNFeSteps_0200.MeChaves.Lines.Add(QrNFeDesDowIchNFe.Value);
        //
        QrNFeDesDowI.Next
      end;
      //
      QrNFeDesDowI.First;
    finally
      QrNFeDesDowI.EnableControls;
    end;
    FmNFeSteps_0200.Show;
    //
    FmNFeSteps_0200.RGAcao.ItemIndex := 10; // Download NF-es Destinadas
    FmNFeSteps_0200.PreparaDownloadDeNFeDestinadas(QrNFeDesDowCCodigo.Value, QrNFeDesDowCEmpresa.Value);
    FmNFeSteps_0200.FFormChamou      := 'FmNFeDesDowC_0100';
    //
  end;
&*)
end;

procedure TNFe_PF.MostraFormStepsNFe_ConsultaNFe(Empresa, IDCtrl, ide_mod: Integer;
  ChaveNFe: String; MeXML: TMemo);
const
  sProcName = 'TNFe_PF.MostraFormStepsNFe_ConsultaNFe()';
var
  VersaoNFe: Integer;
  sMsg: String;
begin
  if MyObjects.FIC(Length(ChaveNFe) <> 44, nil, 'Informe a chave da NFe') then
    Exit;
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    400:
    begin
      // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
      case TFormaGerenXXe(VAR_DFeAppCode) of
        (*0*)fgxxeCAPICOM:
        begin
          if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            //FmNFeSteps_0400.FNaoExecutaLeitura := True; 09/04/2015 => N�o usa! Pois na 2.00 n�o tem esta linha conforme acima
            //FmNFeSteps_0400.PnLoteEnv.Visible := True; ???
            FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConNFe.Value;
            FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep(nfesrvConsultarNFe)); // 5 - Consulta NFe
            FmNFeSteps_0400.Edide_mod.ValueVariant := ide_mod;
            //DefineVarsDePreparacao();
            FmNFeSteps_0400.PreparaConsultaNFe(Empresa, IDCtrl, ChaveNFe);
            FmNFeSteps_0400.ShowModal;
            if MeXML <> nil then
              MeXML.Text := FmNFeSteps_0400.FTextoArq;
            FmNFeSteps_0400.Destroy;
            //
          end;
        end;
        //(*1*)fgxxeCMaisMais:
        (*2*)fgxxeACBr:
        begin
          if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            //FmDmkACBrNFeSteps_0400.FNaoExecutaLeitura := True; 09/04/2015 => N�o usa! Pois na 2.00 n�o tem esta linha conforme acima
            //FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True; ???
            FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConNFe.Value;
            FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep(nfesrvConsultarNFe)); // 5 - Consulta NFe
            FmDmkACBrNFeSteps_0400.Edide_mod.ValueVariant := ide_mod;
            //DefineVarsDePreparacao();
            FmDmkACBrNFeSteps_0400.PreparaConsultaNFe(Empresa, IDCtrl, ChaveNFe);
            FmDmkACBrNFeSteps_0400.ShowModal;
            if MeXML <> nil then
              MeXML.Text := FmDmkACBrNFeSteps_0400.FTextoArq;
            FmDmkACBrNFeSteps_0400.Destroy;
            //
          end;
        end;
        (*?*)else
        begin
          sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
          Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
        end;
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_DownloadNFeConfirmadas(Codigo, Empresa:
  Integer; QrNFeDesDowI: TmySQLQuery; SohLer: Boolean);
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    400:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmNFeSteps_0400.PnLoteEnv.Visible := True;
        FmNFeSteps_0400.EdVersaoAcao.ValueVariant := verDowNFeDest_Int;
        FmNFeSteps_0400.CkSoLer.Checked := SohLer;
        FmNFeSteps_0400.MeChaves.Lines.Clear;
        QrNFeDesDowI.DisableControls;
        try
          QrNFeDesDowI.First;
          while not QrNFeDesDowI.Eof do
          begin
            FmNFeSteps_0400.MeChaves.Lines.Add(QrNFeDesDowI.FieldByName('chNFe').AsString);
            //
            QrNFeDesDowI.Next
          end;
          //
          QrNFeDesDowI.First;
        finally
          QrNFeDesDowI.EnableControls;
        end;
        FmNFeSteps_0400.Show;
        //
        FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvDownloadNFes);
        FmNFeSteps_0400.PreparaDownloadDeNFeConfirmadas(Codigo, Empresa);
        FmNFeSteps_0400.FFormChamou      := 'FmNFeDesDowC_0100';
        //
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_Inutilizacao(Empresa, Lote, Ano, Modelo,
  Serie, nNFIni, nNFFim, Justif: Integer);
const
  sProcName = 'TNFe_PF.MostraFormStepsNFe_Inutilizacao()';
var
  VersaoNFe: Integer;
  sMsg: String;
begin
  VersaoNFe := VersaoNFeEmUso();
  case VersaoNFe of
    400:
    begin
      // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
      case TFormaGerenXXe(VAR_DFeAppCode) of
        (*0*)fgxxeCAPICOM:
        begin
          if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            FmNFeSteps_0400.PnLoteEnv.Visible := True;
            FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerInuNum.Value;
            FmNFeSteps_0400.Show;
            //
            FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvPedirInutilizaCaoNumerosNFe); // 4 -Pedido de inutiliza��o
            FmNFeSteps_0400.PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo,
              Serie, nNFIni, nNFFim, Justif);
            //
            //
            //FmNFeSteps_0400.Destroy;
            //
          end;
        end;
        //(*1*)fgxxeCMaisMais:
        (*2*)fgxxeACBr:
        begin
          if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
          begin
            //
            FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
            FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerInuNum.Value;
            FmDmkACBrNFeSteps_0400.Show;
            //
            FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvPedirInutilizaCaoNumerosNFe); // 4 -Pedido de inutiliza��o
            FmDmkACBrNFeSteps_0400.PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo,
              Serie, nNFIni, nNFFim, Justif);
            //
            //
            //FmDmkACBrNFeSteps_0400.Destroy;
            //
          end;
        end;
        (*?*)else
        begin
          sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
          Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
        end;
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_StatusServico();
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
    400:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        //
        FmNFeSteps_0400.PnLoteEnv.Visible := True;
        FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerStaSer.Value;
        FmNFeSteps_0400.Show;
        //
        FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvStatusServico); // 0 - Status do servi�o
        FmNFeSteps_0400.PreparaVerificacaoStatusServico(DmodG.QrFiliLogCodigo.Value);
        //
        //FmNFeSteps_0200.Destroy;
        //
      end;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraFormStepsNFe_StepGenerico();
var
  CliInt: Integer;
var
  VersaoNFe: Integer;
begin
  VersaoNFe := VersaoNFeEmUso();
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  //
  case VersaoNFe of
     400:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoSoMaster) then
      begin
        FmNFeSteps_0400.PreparaStepGenerico(CliInt);
        //
        FmNFeSteps_0400.PnLoteEnv.Visible  := True;
        FmNFeSteps_0400.RGAcao.Enabled     := True;
        FmNFeSteps_0400.PnAbrirXML.Visible := True;
        FmNFeSteps_0400.PnAbrirXML.Enabled := True;
        FmNFeSteps_0400.CkSoLer.Enabled    := True;
        FmNFeSteps_0400.EdchNFe.ReadOnly   := False;
        FmNFeSteps_0400.PnConfirma.Visible := True;
        FmNFeSteps_0400.ShowModal;
        FmNFeSteps_0400.Destroy;
      end;
    end
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

procedure TNFe_PF.MostraNFeChave(Chave: String);
begin
  Geral.MB_Aviso('Chave de acesso da NF-e:' + sLineBreak + Chave);
end;

procedure TNFe_PF.MostraNFeNoPortalNacional(ide_tpAmb: Integer; Id: String);
const
  URL = 'http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=resumo&tipoConteudo=d09fwabTnLk=&nfe=';
  HOM_URL = 'http://hom.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=resumo&tipoConteudo=d09fwabTnLk=&nfe=';
begin
  if ide_tpAmb = 2 then
    DmkWeb.MostraWebBrowser(HOM_URL + Id, True, False, 0, 0)
  else
    DmkWeb.MostraWebBrowser(URL + Id, True, False, 0, 0);
end;

function TNFe_PF.NFe_StatusServicoCod(const Form: TForm; const MostraForm: Boolean; var CodStatus:
  Integer; var TxtStatus: String): Integer;
const
  PerguntaSobreTutorial = True;
  PerguntaSobreOpcoesInternet = True;
  PerguntaSobreSnapIn = False;
var
  VersaoNFe: Integer;
begin
  CodStatus := -2;
  TxtStatus := 'N�o consultado. Erro antes de consultar!';
  VersaoNFe := VersaoNFeEmUso();
  //
  case VersaoNFe of
     400:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        try
          FmNFeSteps_0400.FCodStausServico := -1;
          //
          FmNFeSteps_0400.PnLoteEnv.Visible := True;
          FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerStaSer.Value;
          if MostraForm then
            FmNFeSteps_0400.Show;
          //
          FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvStatusServico); // 0 - Status do servi�o
          FmNFeSteps_0400.PreparaVerificacaoStatusServico(DmodG.QrFiliLogCodigo.Value);
          //
          if FmNFeSteps_0400.RGAcao.ItemIndex = 0 then
          begin
            try
              FmNFeSteps_0400.BtOKClick(FmNFeSteps_0400);
              CodStatus := FmNFeSteps_0400.FCodStausServico;
              TxtStatus := FmNFeSteps_0400.FTxtStausServico;
            except
               //UnWin.MostraMMC_SnapIn();
              on E: exception do
                UnWin.OpcoesDaInternet(Form, E.Message, PerguntaSobreTutorial,
                PerguntaSobreOpcoesInternet, PerguntaSobreSnapIn);
            end;
            //FmNFeSteps_????.Destroy;
            //
          end else
            Geral.MB_Erro(
            'O status do servi�o da NF-e n�o foi consultado! Abortado antes da consulta! RGAcao.ItemIndex <> 0');
          //
        finally
          if not MostraForm then
            FmNFeSteps_0400.Destroy;
        end;
      end;
    end;
    else XXe_PF.AvisoNaoImplemVerNFe(VersaoNFe);
  end;
end;

function TNFe_PF.NFe_StatusServicoCodDef(const Form: TForm;
  var CodStatus: Integer; var TxtStatus: String): Boolean;
const
  MostraForm = False;
begin
  case VAR_DFeAppCode of
    // Capicom, C++
    0, 1: NFe_StatusServicoCod(Form, MostraForm, CodStatus, TxtStatus);
    // ACBr
    2: DmkACBr_PF.NFe_StatusServicoCod(MostraForm, CodStatus, TxtStatus);
  end;
end;

function TNFe_PF.NFe_StatusServicoCodMul(const Form: TForm; var CodStatus: Integer;
  var TxtStatus: String): Boolean;
const
  sProcName = 'TNFe_PF.NFe_StatusServicoCodMul()';
var
  Modo, Forma: Integer;
  MostraForm: Boolean;
begin
  Modo := MyObjects.SelRadioGroup('Forma de consulta',
  'Selecione a forma de consulta', [
  'Definido na filial: ' + sFormaGerenXXe[VAR_DFeAppCode] + ' SEM mostrar a janela',
  'Definido na filial: ' + sFormaGerenXXe[VAR_DFeAppCode] + ' MOSTRANDO a janela',
  'Forma atual SEM mostrar a janela',
  'Forma atual MOSTRANDO a janela',
  'Forma nova (em implementa��o)'],
  0);
  //
  case Modo of
    0: Forma := 10 + VAR_DFeAppCode;
    1: Forma := 20 + VAR_DFeAppCode;
    2: Forma := 10;
    3: Forma := 20;
    4: Forma := 12;
    else Forma := 20;
  end;
  //
  MostraForm := Forma >= 19;
  case Forma of
    10, 20:
    begin
      NFe_StatusServicoCod(Form, MostraForm, CodStatus, TxtStatus);
    end;
    12, 22:
    begin
      DmkACBr_PF.NFe_StatusServicoCod(MostraForm, CodStatus, TxtStatus);
    end;
    else
    Geral.MB_Erro('Forma de consulta n�o definida em ' + sProcName);
  end;
end;

function TNFe_PF.ObtemCFOPEntradaDeSaida(CFOP: String; Empresa, GraGruX:
  Integer): String;
var
  Qry: TmySQLQuery;
  //
  function SelecionaCFOPMultiplo(): String;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de CFOP de entrada';
    Prompt = 'Selecione o CFOP';
  var
    Ds: TDataSource;
  begin
    Result := '';
    Ds := TDataSource.Create(Dmod);
    try
      Ds.DataSet := Qry;
      if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, Ds, False, False) then
        Result := Qry.FieldByName('CFOP_INN').AsString;
    finally
      Ds.Free;
    end;
  end;
begin
  Result := '';
  Qry := TmySQLQuery.Create(Dmod);
  try

    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT CFOP_INN, Nome ',
    'FROM cfopcfop ',
    'WHERE REPLACE(CFOP_OUT, ".", "")="' + Geral.SoNumero_TT(CFOP)+ '" ',
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    case Qry.RecordCount of
      0:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT CFOP_INN, Nome ',
        'FROM CFOPCFOP ',
        'WHERE REPLACE(CFOP_OUT, ".", "")="' + Geral.SoNumero_TT(CFOP) + '" ',
        '']);
        case Qry.RecordCount of
          0:
          begin
           if Geral.MB_Pergunta(
           'Nenhum CFOP de entradada foi localizado para o CFOP ' + CFOP +
           sLineBreak + 'Deseja cadastrar?') = ID_YES then
           begin
             CFOP := Geral.FF0(UnNFe_PF.MostraFormCFOPCFOP(stIns,
             nil, nil, nil, nil, 0, '', CFOP, CFOP));
           end;
          end;
          1: Result := Qry.FieldByName('CFOP_INN').AsString;
          else Result := SelecionaCFOPMultiplo();
        end;
      end;
      1: Result := Qry.FieldByName('CFOP_INN').AsString;
      else Result := SelecionaCFOPMultiplo();
    end;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemDataEntradaEFiscal(const Serie, NumNF, Emitente: Integer;
  const ChaveNFe, NomeEmit: String; const Default: TDateTime; var DataEntrada,
  DataFiscal: TDateTime): Boolean;
begin
  Result := False;
  DataFiscal := Default;
  if DBCheck.CriaFm(TFmNFeXMLData, FmNFeXMLData, afmoLiberado) then
  begin
    FmNFeXMLData.TPDataEntrada.Date := Default;
    FmNFeXMLData.TPDataFiscal.Date := Default;
    FmNFeXMLData.Edide_Serie.ValueVariant := Serie;
    FmNFeXMLData.Edide_nNF.ValueVariant   := NumNF;
    FmNFeXMLData.EdChaveNFe.ValueVariant  := ChaveNFe;
    FmNFeXMLData.EdEmit.ValueVariant      := Emitente;
    FmNFeXMLData.EdEmit_TXT.Text          := NomeEmit;
    //
    FmNFeXMLData.ShowModal;
    //
    DataEntrada := FmNFeXMLData.TPDataEntrada.Date;
    DataFiscal := FmNFeXMLData.TPDataFiscal.Date;
    Result := True;
    //
    FmNFeXMLData.Destroy;
  end;
end;

function TNFe_PF.ObtemSPEDEFD_COD_ITEM(const GraGruX, SPED_EFD_ID_0200: Integer;
  var COD_ITEM: String): Boolean;
const
  Campo = 'COD_ITEM...';
var
  Valor: String;
  Qry: TmySQLQuery;
  //
  function ObtemNaoCodigo(Campo: String): String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.COD_ITEM, gg1.NCM ',
    'FROM gragru1 gg1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(GraGrux),
    '']);
    //
    Result := Qry.FieldByName(Campo).AsString;
  end;
begin
  COD_ITEM := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    case SPED_EFD_ID_0200 of
      1: COD_ITEM := Geral.FF0(GraGruX);
      2: COD_ITEM := ObtemNaoCodigo('COD_ITEM');
      3: COD_ITEM := Geral.Sonumero_TT(ObtemNaoCodigo('NCM'));
      4: COD_ITEM := 'Dmk_' + Geral.FF0(GraGruX);
      else Geral.MB_Erro(
      '"SPED_EFD_ID_0200" n�o implementado em "NFe_PF.ObtemSPEDEFD_COD_ITEM"' +
      sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
      sLineBreak + 'SPED_EFD_ID_0200: ' + Geral.FF0(SPED_EFD_ID_0200));
    end;
    Result := Trim(COD_ITEM) <> '';
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemSPEDEFD_COD_PART(const Entidade, SPED_EFD_ID_0150: Integer;
  var COD_PART: String): Boolean;
(*
  0150.02 - C100.04 - C113.04 - C160.02 - C165.02 - C176.06 - C176.21 -
  C500.04 - C510.18 - D100.04 - D130.02 - D130.03 - D240.02 - D710.02 -
  D170.03 - D400.02 - D500.04 - D510.17 - E113.02 - E240.02 - E313.02 -
  G130.03 - H010.08 - K200.06 - K280.07 - 1110.02 - 1500.04 - 1510.18 -
  1600.02 - 1923.02 -
*)
const
  Campo = 'COD_PART...';
var
  Valor: String;
  Qry: TmySQLQuery;
  //
  function ObtemNaoCodigo(Campo: String): String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COD_PART, ',
    'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(Entidade),
    '']);
    //
    Result := Qry.FieldByName(Campo).AsString;
  end;
begin
  COD_PART := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    case SPED_EFD_ID_0150 of
      1: COD_PART := Geral.FF0(Entidade);
      2: COD_PART := ObtemNaoCodigo('COD_PART');
      3: COD_PART := Geral.SoNumero_TT(ObtemNaoCodigo('CNPJ_CPF'));
      4: COD_PART := 'Dmk_' + Geral.FF0(Entidade);
      else Geral.MB_Erro(
      '"SPED_EFD_ID_0150" n�o implementado em "NFe_PF.ObtemSPEDEFD_COD_PART"' +
      sLineBreak + 'Entidade = ' + Geral.FF0(Entidade) +
      sLineBreak + 'SPED_EFD_ID_0150 = ' + Geral.FF0(SPED_EFD_ID_0150));
    end;
    Result := Trim(COD_PART) <> '';
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemSPEDEFD_GraGruX(const COD_ITEM: String;
  SPED_EFD_ID_0200: Integer; var GraGruX: Integer): Boolean;
const
  Campo = 'COD_ITEM...';
var
  Valor: String;
  Qry: TmySQLQuery;
  //
  function ObtemNaoCodigo(Campo: String): Integer;
  begin
    Result := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragru1 gg1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ',
    'WHERE ggx.COD_ITEM <> "" ',
    'AND ggx.COD_ITEM="' + COD_ITEM + '"',
    '']);
    //
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('Controle').AsInteger;
  end;
begin
  GraGruX := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    case SPED_EFD_ID_0200 of
      1: GraGruX := Geral.IMV(COD_ITEM);
      2: GraGruX := ObtemNaoCodigo('COD_ITEM');
      3: GraGruX := 0; //Geral.Sonumero_TT(ObtemNaoCodigo('NCM'));
      4: GraGruX := Geral.IMV(Copy(COD_ITEM, 5)); // 'Dmk_' + GraGruX
      else Geral.MB_Erro(
      '"SPED_EFD_ID_0200" n�o implementado em "NFe_PF.ObtemSPEDEFD_GraGruX"' +
      sLineBreak + 'COD_ITEM: ' + COD_ITEM +
      sLineBreak + 'SPED_EFD_ID_0200: ' + Geral.FF0(SPED_EFD_ID_0200));
    end;
    Result := GraGruX <> 0;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemSPEDEFD_SPED_EFD_ID_0XXX(Registro, Empresa: Integer): Integer;
var
  Qry: TmySQLQuery;
  Campo: String;
begin
  Campo := 'SPED_EFD_ID_0' + Geral.FF0(Registro);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ' + Campo,
    'FROM paramsemp ',
    'WHERE Codigo=' + Geral.FF0(Empresa),
    '']);
    //
    Result := Qry.FieldByName(Campo).AsInteger;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemUltNSU_NFeDistDFeInt(Lote: Integer): Int64;
var
  Qry: TmySQLQuery;
  sLote: String;
begin
  Result := 0;
  if Lote > 0 then
    sLote := 'WHERE Codigo < ' + Geral.FF0(Lote)
  else
    sLote := '';
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(docZIP_NSU) docZIP_NSU ',
    'FROM nfedfeiits ',
    sLote,
    '']);
    //
    Result := Qry.FieldByName('docZIP_NSU').AsLargeInt;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.ObtemURLWebServerNFe(UF, Servico, Versao: String;
  tpAmb, Modelo: Integer): String;
var
  UF_Str, Servico_Str, tpAmb_Str, Tabela: String;
  procedure AbreTabela();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'SELECT URL ',
      'FROM ' + Tabela,
      'WHERE LOWER(UF) = "' + UF_Str + '" ',
      'AND LOWER(Servico) = "' + Servico_Str + '" ',
      'AND tpAmb = "' + tpAmb_Str + '" ',
      'AND Versao = "' + Versao + '" ',
      '']);
    //Geral.MB_Teste(DModG.QrAllAux.SQL.Text);
    if DModG.QrAllAux.RecordCount > 0 then
      Result := DModG.QrAllAux.FieldByName('URL').AsString;
  end;
begin
  Result := '';
  //
  try
    UF_Str      := Trim(LowerCase(UF));
    Servico_Str := Trim(LowerCase(Servico));
    tpAmb_Str   := Geral.FF0(tpAmb);
    //
    if Modelo = CO_MODELO_NFE_65 then
    begin
      Tabela := 'qrcode_ws';
      AbreTabela();
      if Trim(Result) = EmptyStr then
      begin
        Tabela := 'nfe_ws';
        AbreTabela();
      end;
    end else
    begin
      Tabela := 'nfe_ws';
      AbreTabela();
    end;
    //
  finally
    DModG.QrAllAux.Close;
  end;
end;

procedure TNFe_PF.ObtemSerieNumeroByID(Id: String; var Serie, nNF: Integer);
begin
  Serie := Geral.IMV(Copy(Id, 23, 3));
  nNF   := Geral.IMV(Copy(Id, 26, 9));
end;

function TNFe_PF.ObtemVersaoServicoNFe(UF, Servico: String; tpAmb: Integer; MaxVersao: Double): Double;
var
  UF_Str, Servico_Str, tpAmb_Str: String;
begin
  Result := 0;
  //
  try
    UF_Str      := Trim(LowerCase(UF));
    Servico_Str := Trim(LowerCase(Servico));
    tpAmb_Str   := Geral.FF0(tpAmb);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'SELECT Versao ',
      'FROM nfe_ws ',
      'WHERE LOWER(UF) = "' + UF_Str + '" ',
      'AND LOWER(Servico) = "' + Servico_Str + '" ',
      'AND tpAmb = "' + tpAmb_Str + '" ',
      'AND Versao <= "' + Geral.FFT_Dot(MaxVersao, 2, siPositivo) + '" ',
      'ORDER BY Versao DESC LIMIT 1 ',
      '']);
    if DModG.QrAllAux.RecordCount > 0 then
      Result := DModG.QrAllAux.FieldByName('Versao').AsFloat;
  finally
    DModG.QrAllAux.Close;
  end;
end;

function TNFe_PF.ObtemVersoesServicoNFe(UF, Servico: String; tpAmb: Integer): MyArrayLista;
var
  Linha: Integer;
  UF_Str, Servico_Str, tpAmb_Str: String;
begin
  try
    SetLength(Result, 0);
    //
    Linha       := 0;
    UF_Str      := Trim(LowerCase(UF));
    Servico_Str := Trim(LowerCase(Servico));
    tpAmb_Str   := Geral.FF0(tpAmb);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'SELECT Versao ',
      'FROM nfe_ws ',
      'WHERE LOWER(UF) = "' + UF_Str + '" ',
      'AND LOWER(Servico) = "' + Servico_Str + '" ',
      'AND tpAmb = "' + tpAmb_Str + '" ',
      'ORDER BY Versao ASC ',
      '']);
    if DModG.QrAllAux.RecordCount > 0 then
    begin
      while not DModG.QrAllAux.Eof do
      begin
        SetLength(Result, Linha + 1);
        SetLength(Result[Linha], 1);
        //Result[Linha][0] := Geral.FFT_Dot(DModG.QrAllAux.FieldByName('Versao').AsFloat, 2, siPositivo);
        Result[Linha][0] := Geral.FFT(DModG.QrAllAux.FieldByName('Versao').AsFloat, 2, siPositivo);
        Inc(Linha, 1);
        //
        DModG.QrAllAux.Next;
      end;
    end;
  finally
    DModG.QrAllAux.Close;
  end;
end;

procedure TNFe_PF.PesquisaEMostraFormNFePesqEmitida(NF, Empresa: Integer);
var
  Filial: Integer;
begin
  Filial := 0;
  if Empresa <> 0 then
    Filial := DModG.ObtemFilialDeEntidade(Empresa)
  else
    Filial := DModG.ObtemFilialDeEntidade(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFe_LocEPesq, FmNFe_LocEPesq, afmoNegarComAviso) then
  begin
    FmNFe_LocEPesq.ImgTipo.SQLType         := stPsq;
    FmNFe_LocEPesq.FEmitida                := True;
    FmNFe_LocEPesq.EdEmpresa.ValueVariant  := Filial;
    FmNFe_LocEPesq.CBEmpresa.KeyValue      := Filial;
    FmNFe_LocEPesq.EdEmitente.ValueVariant := Empresa;
    FmNFe_LocEPesq.CBEmitente.KeyValue     := Empresa;
    FmNFe_LocEPesq.EdNF.ValueVariant       := NF;
    //
    FmNFe_LocEPesq.Pesquisa();
    //
    FmNFe_LocEPesq.ShowModal;
    FmNFe_LocEPesq.Destroy;
  end;
(*
var
  NFe, IDCtrl: Integer;
  Qry: TmySQLQuery;
  SQL_Empresa: String;
begin
  if Empresa = 0 then
    SQL_Empresa := 'AND Empresa=' + Geral.FF0(VAR_LIB_EMPRESA_SEL)
  else
    SQL_Empresa := 'AND Empresa=' + Geral.FF0(Empresa);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IDCtrl  ',
    'FROM nfecaba ',
    'WHERE FatID=1  ',
    SQL_Empresa,
    'AND ide_nNF=' + Geral.FF0(NF),
    'ORDER BY ide_serie DESC']);
    IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
    if IDCtrl <> 0 then
    begin
      if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
      begin
        FmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl, True);
        FmNFe_Pesq_0000.ShowModal;
        FmNFe_Pesq_0000.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
*)
end;

procedure TNFe_PF.PesquisaEMostraFormNFePesqRecebida(NF, Empresa, CodInfoEmit: Integer);
var
  Filial: Integer;
begin
  Filial := 0;
  if Empresa <> 0 then
    Filial := DModG.ObtemFilialDeEntidade(Empresa)
  else
    Filial := DModG.ObtemFilialDeEntidade(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFe_LocEPesq, FmNFe_LocEPesq, afmoNegarComAviso) then
  begin
    FmNFe_LocEPesq.ImgTipo.SQLType         := stPsq;
    FmNFe_LocEPesq.FEmitida                := False;
    FmNFe_LocEPesq.EdEmpresa.ValueVariant  := Filial;
    FmNFe_LocEPesq.CBEmpresa.KeyValue      := Filial;
    FmNFe_LocEPesq.EdEmitente.ValueVariant := CodInfoEmit;
    FmNFe_LocEPesq.CBEmitente.KeyValue     := CodInfoEmit;
    FmNFe_LocEPesq.EdNF.ValueVariant       := NF;
    //
    FmNFe_LocEPesq.Pesquisa();
    //
    FmNFe_LocEPesq.ShowModal;
    FmNFe_LocEPesq.Destroy;
  end;
end;

procedure TNFe_PF.ReabreQueryNF_X(Query: TmySQLQuery; Tipo, OriCodi,
  Associada: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SerieNFTxt, NumeroNF ',
    'FROM stqmovnfsa ',
    'WHERE Tipo=' + Geral.FF0(Tipo),
    'AND OriCodi=' + Geral.FF0(OriCodi),
    'AND Empresa=' + Geral.FF0(Associada),
    '']);
end;

procedure TNFe_PF.ReabreQueryPrzT(Query: TmySQLQuery; CondicaoPG: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Controle, Dias, Percent1, Percent2 ',
    'FROM pediprzits ',
    'WHERE Codigo=' + Geral.FF0(CondicaoPG),
    'ORDER BY Dias ',
    '']);
end;

procedure TNFe_PF.ReabreQueryPrzX(Query: TmySQLQuery; CondicaoPG: Integer;
  Campo: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Controle, Dias, ' + Campo + ' Percent ',
    'FROM pediprzits ',
    'WHERE Codigo=' + Geral.FF0(CondicaoPG),
    'AND ' + Campo + ' > 0 ',
    'ORDER BY Dias ',
    '']);
end;

procedure TNFe_PF.ReabreQuerySumT(Query: TmySQLQuery; CondicaoPG: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 , ',
    'ppc.JurosMes ',
    'FROM pediprzits ppi ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo ',
    'WHERE ppi.Codigo=' + Geral.FF0(CondicaoPG),
    'GROUP BY ppi.Codigo ',
    '']);
end;

procedure TNFe_PF.ReabreQuerySumX(Query: TmySQLQuery; Tipo, OriCodi,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  // ini 2021-10-23
    //'SELECT SUM(Total) Total ',
    'SELECT SUM(Total) Total, SUM(prod_vFrete) prod_vFrete, ',
    'SUM(prod_vSeg) prod_vSeg, SUM(prod_vDesc) prod_vDesc, ',
    'SUM(prod_vOutro) prod_vOutro ',
  // fim 2021-10-23
    'FROM stqmovvala',
    'WHERE Tipo='  + Geral.FF0(Tipo),
    'AND OriCodi=' + Geral.FF0(OriCodi),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
end;

procedure TNFe_PF.RegistraChaveNFeParaManifestar(chNFe: String; Conta: Integer;
  MostraMsg: Boolean = False; MeAvisos: TMemo = nil);

  procedure ReopenNFeDFeINFe();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeDFeINFe, Dmod.MyDB, [
      'SELECT cab.Empresa, nfe.* ',
      'FROM nfedfeinfe nfe ',
      'LEFT JOIN nfedfeicab cab ON cab.Codigo=nfe.Codigo ',
      'WHERE nfe.Conta=' + Geral.FF0(Conta),
      '']);
  end;
  //
  procedure AtualizaIDCtrlDeNFe(NFa_IDCtrl, Conta: Integer; ReopenQry: Boolean);
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedfeinfe', False,
      ['NFa_IDCtrl'], ['Conta'], [NFa_IDCtrl], [Conta], True) then
      //if ReopenQry then
        //ReopenNFeDesRIts(IDCad);*)
      ReopenNFeDFeINFe();
  end;
var
  //infCanc_dhRecbto, infCanc_Id,
  infProt_dhRecbto, ide_dEmi,
  Id, emit_CNPJ, emit_CPF, emit_xNome, emit_IE, infProt_Id, infProt_digVal,
  emit_UF, ide_hEmi: String;
  //
  //IDCad, cSitConf, infCanc_tpAmb, infCanc_cStat, cSitCan,
  NFa_IDCtrl, CodInfoEmit, CodInfoDest, cSitNFe,
  FatID, FatNum, Empresa, IDCtrl, ide_tpNF, infProt_cStat, ide_tpAmb, Status,
  infProt_tpAmb, ide_serie, ide_nNF: Integer;
  //
  ICMSTot_vNF, ide_dhEmiTZD, infProt_dhRecbtoTZD: Double;
  //
  VersaoNFe: Double;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe, Msg: String;
begin
  ReopenNFeDFeINFe();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrNFeDFeINFe, Dmod.MyDB, [
    'SELECT cab.Empresa, nfe.* ',
    'FROM nfedfeinfe nfe ',
    'LEFT JOIN nfedfeicab cab ON cab.Codigo=nfe.Codigo ',
    'WHERE nfe.Conta=' + Geral.FF0(Conta),
    '']);
  //
  NFa_IDCtrl := DmNFE_0000.ObtemIDCtrlDeChaveNFe(chNFe);
  //
  if NFa_IDCtrl <> 0 then
  begin
    Msg := 'A chave ' + chNFe +
      ' j� est� registrada na base de dados com o ID ' + Geral.FF0(NFa_IDCtrl);
    if MeAvisos <> nil then
      MeAvisos.Text := Msg + sLineBreak + MeAvisos.Text
    else
      Geral.MB_Aviso(Msg);
    //
    if DmNFe_0000.QrNFeDFeINFeNFa_IDCtrl.Value <> NFa_IDCtrl then
      AtualizaIDCtrlDeNFe(NFa_IDCtrl, Conta, True);
    //
    Exit;
  end else
  begin
    ide_tpAmb := DmNFe_0000.QrNFeDFeINFetpAmb.Value;
    Empresa   := DmNFe_0000.QrNFeDFeINFeEmpresa.Value;
    Id        := DmNFe_0000.QrNFeDFeINFechNFe.Value;
    ide_serie := Geral.IMV(Copy(Id, 23, 3));
    ide_nNF   := Geral.IMV(Copy(Id, 26, 9));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrDup, Dmod.MyDB, [
      'SELECT IDCtrl',
      'FROM nfecaba ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND Id="' + Id + '"',
      '']);
    if DmNFe_0000.QrDup.RecordCount > 0 then
    begin
      Msg := 'A chave ' + Id +
        ' j� est� registrada na base de dados com o ID ' +
        Geral.FF0(DmNFe_0000.QrDup.FieldByName('IDCtrl').AsInteger);
      if MeAvisos <> nil then
        MeAvisos.Text := Msg + sLineBreak + MeAvisos.Text
      else
        Geral.MB_Aviso(Msg);
      //
      Exit;
    end else
    begin
      emit_CNPJ           := DmNFe_0000.QrNFeDFeINFeCNPJ.Value;
      emit_CPF            := DmNFe_0000.QrNFeDFeINFeCPF.Value;
      emit_xNome          := DmNFe_0000.QrNFeDFeINFexNome.Value;
      emit_IE             := DmNFe_0000.QrNFeDFeINFeIE.Value;
      ide_dEmi            := Geral.FDT(DmNFe_0000.QrNFeDFeINFedhEmi.Value, 1);
      ide_hEmi            := Geral.FDT(DmNFe_0000.QrNFeDFeINFedhEmi.Value, 100);
      ide_dhEmiTZD        := DmNFe_0000.QrNFeDFeINFedhEmiTZD.Value;
      ide_tpNF            := DmNFe_0000.QrNFeDFeINFetpNF.Value;
      ICMSTot_vNF         := DmNFe_0000.QrNFeDFeINFevNF.Value;
      infProt_digVal      := DmNFe_0000.QrNFeDFeINFedigVal.Value;
      infProt_dhRecbto    := Geral.FDT(DmNFe_0000.QrNFeDFeINFedhRecbto.Value, 109);
      infProt_dhRecbtoTZD := DmNFe_0000.QrNFeDFeINFedhRecbtoTZD.Value;
      cSitNFe             := DmNFe_0000.QrNFeDFeINFecSitNFe.Value;
      //cSitConf            := DmNFe_0000.QrNFeDFeINFecSitConf.Value;
      infProt_cStat       := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitNFe);
      infProt_Id          := 'ID' + DmNFe_0000.QrNFeDFeINFenProt.Value;;

(*
      infCanc_Id       := DmNFe_0000.QrNFeDFeINFeCanc_chNFe     .Value;
      infCanc_dhRecbto := DmNFe_0000.QrNFeDFeINFeCanc_dhRecbto  .Value;
      cSitCan          := DmNFe_0000.QrNFeDFeINFeCanc_cSitNFe   .Value;
      infCanc_cStat    := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitCan);
*)
      infProt_tpAmb    := ide_tpAmb;
(*
      if infCanc_cStat = 101 then
      begin
        infCanc_tpAmb  := ide_tpAmb;
        Status         := infCanc_cStat;
      end else
      begin
        infCanc_tpAmb  := 0;*)
        Status         := infProt_cStat;
//      end;
      //
  {   Fazer?
      := DmNFe_0000.QrNFeDFeINFeCCe_NSU        .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_chNFe      .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_dhEvento   .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_tpEvento   .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_nSeqEvento .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_descEvento .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_xCorrecao  .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_tpNF       .Value;
      := DmNFe_0000.QrNFeDFeINFeCCe_dhRecbto   .Value;
  }

      DModG.ObtemEntidadeDeCNPJCFP(emit_CNPJ, CodInfoEmit);
      CodInfoDest:= Empresa;

      // N�o presisa saber! (at� quando?)
      VersaoNFe := 0;
      //
      NFeXMLGeren.DesmontaChaveDeAcesso(Id, VersaoNFe,
        UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
      //
      emit_UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(UF_IBGE));
      //
      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      FatID  := VAR_FATID_0053;
      // Ver como implementar no StqInCad
      FatNum := Conta;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
        'FatID', 'FatNum', 'Empresa',
        'Id', 'emit_CNPJ', 'emit_CPF',
        'emit_xNome', 'emit_IE',
        'ide_dEmi', 'ide_hEmi', 'ide_dhEmiTZD',
        'ide_tpNF', 'ICMSTot_vNF', 'infProt_digVal',
        'infProt_dhRecbto', 'infProt_cStat', 'infProt_Id',
        //'infCanc_Id', 'infCanc_dhRecbto', 'infCanc_cStat',
        'infProt_tpAmb', 'Status',
        'ide_tpAmb', 'CodInfoEmit', 'CodInfoDest',
        // 'cSitConf', 'infCanc_tpAmb',
        'cSitNFe', 'emit_UF', 'ide_Serie', 'ide_nNF'
        ], [
        'IDCtrl'
        ], [
        FatID, FatNum, Empresa,
        Id, emit_CNPJ, emit_CPF,
        emit_xNome, emit_IE,
        ide_dEmi, ide_hEmi, ide_dhEmiTZD,
        ide_tpNF, ICMSTot_vNF, infProt_digVal,
        infProt_dhRecbto, infProt_cStat, infProt_Id,
        //infCanc_Id, infCanc_dhRecbto, infCanc_cStat,
        infProt_tpAmb, Status,
        ide_tpAmb, CodInfoEmit, CodInfoDest,
        // cSitConf, infCanc_tpAmb,
        cSitNFe, emit_UF, ide_serie, ide_nNF
        ], [
        IDCtrl], True) then
      begin
        NFa_IDCtrl := IDCtrl;
        //
        AtualizaIDCtrlDeNFe(NFa_IDCtrl, Conta, True);
        //
        if DmNFe_0000.QrNFeDFeINFeNFa_IDCtrl.Value <> 0 then
        begin
          if MostraMsg = True then
          begin
            Msg := 'A chave ' + chNFe + ' foi registrada com sucesso!';
            if MeAvisos <> nil then
              MeAvisos.Text := Msg + sLineBreak + MeAvisos.Text
            else
              Geral.MB_Info(Msg);
          end;
        end else
          Geral.MB_Erro('A chave ' + chNFe + ' pode n�o ter sido registrada!');
      end;
    end;
  end;
end;

function TNFe_PF.AtualizaDadosCanNfeCabA(infEvento_chNFe, infEvento_tpAmb,
  infEvento_verAplic, infEvento_dhRegEvento, infEvento_nProt,
  infEvento_versao: String; ret_TZD_UTC: Double): Boolean;
var
  cJust, Status, infCanc_cStat: Integer;
  Id, xJust, infCanc_digVal, infCanc_xMotivo: String;
begin
  Result := False;
  //
  if not DmNFe_0000.LocalizaNFeInfoCanEve(infEvento_chNFe, Id, xJust, cJust) then
  begin
    Geral.MB_Erro('Falha ao localizar dados do cancelamento');
    Exit;
  end;
  //
  Status          := 101;
  infCanc_digVal  := '';//;N�o vem
  infCanc_cStat   := 101;
  infCanc_xMotivo := 'Cancelamento de NF-e homologado';
  //
  if infEvento_versao = '' then
    infEvento_versao := '0';
  //
  // ini 2020-11-22
  //DmNFe_0000.CancelaFaturamento(Id);
  DmNFe_0000.CancelaFaturamento(infEvento_chNFe);
  // fim 2020-11-22
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'Status', 'infCanc_Id', 'infCanc_tpAmb',
    'infCanc_verAplic', 'infCanc_dhRecbto', 'infCanc_nProt',
    'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
    'infCanc_cJust', 'infCanc_xJust', 'retCancNFe_versao',
    'infCanc_dhRecbtoTZD'],
    ['Id'], [
    Status, Id, infEvento_tpAmb,
    infEvento_verAplic, infEvento_dhRegEvento, infEvento_nProt,
    infCanc_digVal, infCanc_cStat, infCanc_xMotivo,
    cJust, xJust, infEvento_versao,
    ret_TZD_UTC],
    [infEvento_chNFe], True);
end;

function TNFe_PF.AtualizaDadosCCeNfeCabA(infEvento_chNFe, infCCe_verAplic,
  infCCe_chNFe, infCCe_dhRegEvento, infCCe_nProt: String;
  nSeqEvento, infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_cStat: Integer;
  infCCe_dhRegEventoTZD: Double): Boolean;
var
  CNPJ, CPF, xCorrecao: String;
  dhEvento: TDateTime;
  dhEventoTZD, verEvento: Double;
  nCondUso: Integer;
  infCCe_CNPJ, infCCe_dhEvento: String;
  infCCe_nSeqEvento: Integer;
  infCCe_dhEventoTZD: Double;
begin
  Result := False;
  //
  if not DmNFe_0000.LocalizaNFeInfoCCe(infEvento_chNFe, nSeqEvento, CNPJ, CPF,
    dhEvento, dhEventoTZD, verEvento, nCondUso, xCorrecao) then
  begin
    Geral.MB_Erro('Falha ao localizar dados do cancelamento');
    Exit;
  end;
  // ini 2022-02-17
  infCCe_dhEvento    := Geral.FDT(dhEvento, 109);
  infCCe_dhEventoTZD := dhEventoTZD;
  if Trim(CPF) <> EmptyStr then
    infCCe_CNPJ := CPF
  else
    infCCe_CNPJ := CNPJ;
  infCCe_nSeqEvento := nSeqEvento;
  // fim 2022-02-17
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False,
              ['infCCe_verAplic', 'infCCe_cOrgao', 'infCCe_tpAmb', 'infCCe_CNPJ',
              'infCCe_chNFe', 'infCCe_dhEvento', 'infCCe_dhEventoTZD',
              'infCCe_tpEvento', 'infCCe_nSeqEvento', 'infCCe_verEvento',
              'infCCe_xCorrecao', 'infCCe_cStat', 'infCCe_dhRegEvento',
              'infCCe_dhRegEventoTZD', 'infCCe_nProt'], ['ID'],
              [infCCe_verAplic, infCCe_cOrgao, infCCe_tpAmb, infCCe_CNPJ,
              infCCe_chNFe, infCCe_dhEvento, infCCe_dhEventoTZD, infCCe_tpEvento,
              infCCe_nSeqEvento, verEvento, xCorrecao,
              infCCe_cStat, infCCe_dhRegEvento, infCCe_dhRegEventoTZD,
              infCCe_nProt], [infEvento_chNFe], True);
end;

function TNFe_PF.AtualizaDadosEPECNFeCabA(infEvento_chNFe, infEvento_versao,
  infEPEC_verAplic, infEPEC_dhRegEvento, infEPEC_nProt, infEPEC_cOrgao,
  infEPEC_tpAmb, infEPEC_cStat, infEPEC_xMotivo: String;
  infEPEC_dhRegEventoTZD: Double): Boolean;
var
  Status: Integer;
  Id, infEPEC_digVal: String;
begin
  Result := False;
  //
  if not DmNFe_0000.LocalizaNFeInfoEPECEve(infEvento_chNFe, Id) then
  begin
    Geral.MB_Erro('Falha ao localizar dados do EPEC');
    Exit;
  end;
  //
  Status          := 136;
  //
  if infEvento_versao = '' then
    infEvento_versao := '0';
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'Status', 'infEPEC_cOrgao',
    'infEPEC_Id', 'infEPEC_tpAmb', 'infEPEC_verAplic',
    'infEPEC_dhRegEvento', 'infEPEC_dhRegEventoTZD',
    'infEPEC_nProt', 'infEPEC_cStat', 'infEPEC_xMotivo'], [
    'Id'], [
    Status, infEPEC_cOrgao,
    Id, infEPEC_tpAmb, infEPEC_verAplic,
    infEPEC_dhRegEvento, infEPEC_dhRegEventoTZD,
    infEPEC_nProt, infEPEC_cStat, infEPEC_xMotivo], [
    infEvento_chNFe], True);
end;

procedure TNFe_PF.Atualiza_DtEmissNF_StqMovNfsa(IDCtrl: Integer; DtEmissNF: TDate);
var
  DtEmissNF_Txt: String;
begin
  DtEmissNF_Txt := Geral.FDT(DtEmissNF, 01);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False,
    ['DtEmissNF'], ['IDCtrl'], [DtEmissNF_Txt], [IDCtrl], True);
end;

procedure TNFe_PF.ReopenFPC1(FatPedCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmPediVda.QrFPC1, Dmod.MyDB, [
    'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa,  ',
    'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal, ',
    'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente, ',
    'pvd.Codigo ID_Pedido, pvd.PedidoCli,  ',
    'ppc.JurosMes, frc.Nome NOMEFISREGCAD, ',
    'ppc.MedDDReal, ppc.MedDDSimpl, ',
    'par.TipMediaDD, par.Associada, par.FatSemEstq, ',
    ' ',
    'par.CtaProdVen EMP_CtaProdVen, ',
    'par.FaturaSeq EMP_FaturaSeq, ',
    'par.FaturaSep EMP_FaturaSep, ',
    'par.FaturaDta EMP_FaturaDta, ',
    'par.TxtProdVen EMP_TxtProdVen, ',
    'emp.Filial EMP_FILIAL, ',
    'ufe.Codigo EMP_UF, ',
    ' ',
    'ass.CtaProdVen ASS_CtaProdVen, ',
    'ass.FaturaSeq ASS_FaturaSeq, ',
    'ass.FaturaSep ASS_FaturaSep, ',
    'ass.FaturaDta ASS_FaturaDta, ',
    'ass.TxtProdVen ASS_TxtProdVen, ',
    'ufa.Nome ASS_NO_UF, ',
    'ufa.Codigo ASS_CO_UF, ',
    'ase.Filial ASS_FILIAL, ',
    ' ',
    'tpc.Nome NO_TabelaPrc, fpc.* ',
    'FROM fatpedcab fpc ',
    'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG ',
    'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc ',
    'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa ',
    'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada ',
    'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo ',
    'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo ',
    'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF) ',
    'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF) ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'WHERE fpc.Codigo=' + Geral.FF0(FatPedCab),
    '']);
end;

procedure TNFe_PF.ReopenUnidMed(Sigla: String; Query: TmySQLQuery; CodUpd:
  Variant);
var
  SQL_CodInUpd: String;
begin
  SQL_CodInUpd := '';
  if CodUpd <> null then
  try
    SQL_CodInUpd := 'AND Codigo<>' + Geral.FF0(Integer(CodUpd));
  except
    //
  end;
  UnDmkDAC_PF.AbreMySQLQUery0(Query, Dmod.MyDB, [
  'SELECT Codigo, Nome, Sigla ',
  'FROM unidmed ',
  'WHERE UPPER(SIGLA)="' + AnsiUppercase(Sigla) + '"',
  SQL_CodInUpd,
  'ORDER BY Codigo ',
  '']);
end;

function TNFe_PF.SchemaNFe(Empresa: Integer): String;
begin
  DModG.ReopenParamsEmp(Empresa);
  Result := DModG.QrPrmsEmpNFeDirSchema.Value;
end;

function TNFe_PF.SelecionaStatusNFe(Atual: Integer): Integer;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Status de NFe';
  Prompt = 'Informe o status: [F7 para pesquisar]';
  Campo  = 'Descricao';
  //
  MaxMsgNFe = 999;
var
  Controle, Codigo: Variant;
  PesqSQL, Nome: String;
  Qry: TmySQLQuery;
  //
  procedure VerificaRegistrosStsusNFe();
  var
    Status: Integer;
    VersaoNFe: Double;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.AllID_DB, [PesqSQL]);
    //
    if Qry.RecordCount = 0 then
    begin
      Screen.Cursor := crHourGlass;
      VAR_CAN_CLOSE_FORM_MAX := MaxMsgNFe;
      VAR_CAN_CLOSE_FORM_POS := 0;
      Geral.MB_Aguarde('Aguarde... Atualizando banco de dados de mensagens da NFe!');
      VersaoNFe := VersaoNFeEmUso() / 100;
      for Status := 0 to MaxMsgNFe do
      begin
        VAR_CAN_CLOSE_FORM_POS := VAR_CAN_CLOSE_FORM_POS + 1;
        Nome := NFeXMLGeren.Texto_StatusNFe(Status, VersaoNFe);
        if (CO_STATUS_NFE_INDEFINIDO <> Nome) and (Nome <> '') then
        begin
          //if
          UMyMod.SQLInsUpd_IGNORE(DModG.QrAllUpd, stIns, 'nfemsg', False, [
          'Nome'], [
          'Codigo'], [
          Nome], [
          Status], True);
        end;
      end;
      VAR_CAN_CLOSE_FORM_MSG := True;
      Screen.Cursor := crDefault;
    end;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Result := Atual;
    PesqSQL := Geral.ATS([
    'SELECT Codigo, Nome ' + Campo,
    'FROM nfemsg ',
    'WHERE Codigo <> 0',
    'ORDER BY ' + Campo,
    '']);
    VerificaRegistrosStsusNFe();
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Atual,
      [PesqSQL], DModG.AllID_DB, True);
    if Codigo <> Null then
      Result := Codigo;
  finally
    Qry.Free;
  end;
end;

function TNFe_PF.TextoDeCodigoNFe(Tipo: TNFeCodType; Codigo: Integer): String;
begin
  case Tipo of
    nfeCTide_TipoDoc:
      case TNFeCTide_TipoDoc(Codigo) of
        NFeTipoDoc_Desconhecido      : Result := 'Desconhecido';
        // do Manual:
        NFeTipoDoc_nfe_0             : Result := 'NF-e (n�o assinada nem protocolada)';
        NFeTipoDoc_nfe_1             : Result := 'NF-e (assinada mas n�o protocolada)';
        NFeTipoDoc_enviNFe           : Result := 'Lote de NF-e (para envio ao fisco)';
        NFeTipoDoc_cancNFe           : Result := 'Solicita��o de cancelamento de NF-e';
        NFeTipoDoc_nfeProc           : Result := 'NF-e autorizada e protocolada';
        NFeTipoDoc_retCancNFe        : Result := 'Resposta de solicita��o de cancelamento de NF-e';
        NFeTipoDoc_procCancNFe       : Result := 'Cancelamento de NF-e protocolado';
        NFeTipoDoc_inutNFe           : Result := 'Solicita��o de inutiliza��o de numera��o de NF-e';
        NFeTipoDoc_retInutNFe        : Result := 'Resposta da solicita��o de inutiliza��o de numera��o de NF-e';
        NFeTipoDoc_procInutNFe       : Result := 'Inutiliza��o de numera��o de NF-e protocolada';
      end;
    nfeCTide_tpNF:
      case Codigo of
        0: Result := 'Entrada';
        1: Result := 'Sa�da';
        else Result := '? ? ?';
      end;
    nfeCTide_indPag:
      case Codigo of
        0: Result := 'Pagamento � vista';
        1: Result := 'Pagamento � prazo';
        2: Result := 'Outros';
        else Result := '? ? ?';
      end;
    nfeCTide_procEmi:
      case Codigo of
        0: Result := 'Emiss�o de NF-e com aplicativo do contribuinte.';
        1: Result := 'Emiss�o de NF-e avulsa pelo Fisco.';
        2: Result := 'Emiss�o de NF-e avulsa, pelo contribuinte com seu certificado digital, atrav�s do site do Fisco.';
        3: Result := 'Emiss�o NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.';
        else Result := '? ? ?';
      end;
    nfeCTide_finNFe:
      case Codigo of
        1: Result := 'NF-e normal';
        2: Result := 'NF-e complementar';
        3: Result := 'NF-e de ajuste';
        4: Result := 'Devolu��o de Mercadoria';
        else Result := '? ? ?';
      end;
    nfeCTide_tpAmb:
      case Codigo of
        1: Result := 'Produ��o';
        2: Result := 'Homologa��o';
        else Result := '? ? ?';
      end;
    nfeCTide_tpEmis:
      case Codigo of
        1: Result := 'Normal - emiss�o normal';
        2: Result := 'Conting�ncia FS-IA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a';
        // ini 2023-06-09
        //3: Result := 'Conting�ncia SCAN - emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional - SCAN';
        3: Result := 'Regime Especial NFF (NT 2021.002) - Nota Fiscal F�cil';
        // fim 2023-06-09
        4: Result := 'Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia - DPEC';
        5: Result := 'Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA)';
        6: Result := 'Conting�ncia SVC-AN (SEFAZ Virtual de Conting�ncia do AN)';
        7: Result := 'Conting�ncia SVC-RS (SEFAZ Virtual de Conting�ncia do RS)';
        9: Result := 'Conting�ncia off-line da NFC-e';
        else Result := '? ? ?';
      end;
    nfeCTide_tpImp:
      case Codigo of
        0: Result := 'Sem Gera��o de DANFE';
        1: Result := 'DANFE Normal, Retrato';
        2: Result := 'DANFE Normal, Paisagem';
        3: Result := 'DANFE Simplificado';
        4: Result := 'DANFE NFC-e';
        5: Result := 'DANFE NFC-e apenas em mensagem eletr�nica';
        else Result := '? ? ?';
      end;
    nfeCTModFrete:
      case Codigo of
        0: Result := 'Contrata��o do Frete por conta do Remetente (CIF)';
        1: Result := 'Contrata��o do Frete por conta do Destinat�rio (FOB)';
        2: Result := 'Contrata��o do Frete por conta de Terceiros';
        3: Result := 'Transporte Pr�prio por conta do Remetente';
        4: Result := 'Transporte Pr�prio por conta do Destinat�rio';
        9: Result := 'Sem Ocorr�ncia de Transporte';
        else Result := '? ? ?';
      end;
    nfeCTide_indCont:
      case Codigo of
        0: Result := 'N�O';
        1: Result := 'SIM';
        else Result := '???';
      end;
    nfeCTide_idDest:
      case Codigo of
        1: Result := 'Opera��o interna';
        2: Result := 'Opera��o interestadual';
        3: Result := 'Opera��o com exterior';
        else Result := '???';
      end;
    nfeCTide_indFinal:
      case Codigo of
        0: Result := 'Normal';
        1: Result := 'Consumidor final';
        else Result := '???';
      end;
    nfeCTide_indPres:
      case Codigo of
        0: Result := 'N�o se aplica (por exemplo, para a Nota Fiscal complementar ou de ajuste)';
        1: Result := 'Opera��o presencial';
        2: Result := 'Opera��o n�o presencial, pela Internet';
        3: Result := 'Opera��o n�o presencial, Teleatendimento';
        4: Result := 'NFC-e em opera��o com entrega em domic�lio';
        5: Result := 'Opera��o presencial, fora do estabelecimento';
        9: Result := 'Opera��o n�o presencial, outros';
        else Result := '???';
      end;
    nfeCTdest_indIEDest:
    begin
      case Codigo of
        1: Result := 'Contribuinte ICMS (informar a IE do destinat�rio)';
        2: Result := 'Contribuinte isento de Inscri��o no cadastro de Contribuintes do ICMS';
        9: Result := 'N�o Contribuinte, que pode ou n�o possuir Inscri��o Estadual no Cadastro de Contribuintes do ICMS';
        else Result := '???';
      end;
    end;
    nfeCTmotDesICMS:
      case Codigo of
        00: Result := '';
        01: Result := 'T�xi;';
        //02: Result := 'Deficiente F�sico (revogado na NFe 3.01)';
        03: Result := 'Produtor Agropecu�rio;';
        04: Result := 'Frotista/Locadora;';
        05: Result := 'Diplom�tico/Consular;';
        06: Result := 'Utilit�rios e Motocicletas da Amaz�nia Ocidental e �reas de Livre Com�rcio (Resolu��o 714/88 e 790/94 � CONTRAN e suas altera��es);';
        07: Result := 'SUFRAMA;';
        08: Result := 'Venda a �rg�o P�blico;';
        09: Result := 'Outros. (NT 2011/004);';
        10: Result := 'Deficiente Condutor (Conv�nio ICMS 38/12);';
        11: Result := 'Deficiente N�o Condutor (Conv�nio ICMS 38/12);';
        //12: Result := '�rg�o de fomento e desenvolvimento agropecu�rio.';
        16: Result := 'Olimp�adas Rio 2016;';
        //: Result := '';
        else Result := '???';
      end;
    nfeIND_PAG_EFD:
    begin
      case Codigo of
        0: Result := '� Vista';
        1: Result := '� Prazo;';
        2: Result := 'Outros';
        //9: Result := 'Sem Pagamento';
        else Result := '???';
      end;
    end;
    nfeIND_FRT_EFD:
    begin
      case Codigo of
        0: Result := 'Contrata��o do Frete por conta do Remetente (CIF);';
        1: Result := 'Contrata��o do Frete por conta do Destinat�rio (FOB);';
        2: Result := 'Contrata��o do Frete por conta de Terceiros;';
        3: Result := 'Transporte Pr�prio por conta do Remetente;';
        4: Result := 'Transporte Pr�prio por conta do Destinat�rio;';
        9: Result := 'Sem Ocorr�ncia de Transporte.';
        else Result := '???';
      end;
    end;
    nfeCOD_SIT_EFD:
    begin
      case Codigo of
        00: Result := 'Documento regular';
        01: Result := 'Escritura��o extempor�nea de documento regular';
        02: Result := 'Documento cancelado';
        03: Result := 'Escritura��o extempor�nea de documento cancelado';
        04: Result := 'NF-e, NFC-e ou CT-e - denegado';
        05: Result := 'NF-e, NFC-e ou CT-e - Numera��o inutilizada';
        06: Result := 'Documento Fiscal Complementar';
        07: Result := 'Escritura��o extempor�nea de documento complementar';
        08: Result := 'Documento Fiscal emitido com base em Regime Especial ou Norma Espec�fica';
        else Result := '???';
      end;
    end;
    nfeIND_MOV_EFD:
    begin
      case Codigo of
        00: Result := 'SIM';
        01: Result := 'N�O';
        else Result := '???';
      end;
    end;
    nfeCLAS_ESTAB_IND:
    begin
      case Codigo of
        00: Result := 'Industrial - Transforma��o';
        01: Result := 'Industrial - Beneficiamento';
        02: Result := 'Industrial - Montagem';
        03: Result := 'Industrial - Acondicionamento ou Reacondicionamento';
        04: Result := 'Industrial - Renova��o ou Recondicionamento';
        05: Result := 'Equiparado a industrial - Por op��o';
        06: Result := 'Equiparado a industrial - Importa��o Direta';
        07: Result := 'Equiparado a industrial - Por lei espec�fica';
        08: Result := 'Equiparado a industrial - N�o enquadrado nos c�digos 05, 06 ou 07';
        09: Result := 'Outros';
        else Result := '???';
      end;
    end;
    nfeTP_CT_e:
    begin
      case Codigo of
        0: Result := 'CT-e Normal';
        1: Result := 'CT-e de Complemento de Valores';
        2: Result := 'CT-e de Anula��o';
        3: Result := 'CT-e de Substitui��o';
        else Result := '???';
      end;
    end;
    else Result := '??????';
  end;
end;

function TNFe_PF.TextoDeCodigoSPED_TXT(Tipo: TSPEDCodType;
  Codigo: String): String;
begin
  case Tipo of
    spedIND_NAT_PJ:
    begin
      if Codigo = '00' then
        Result := 'Pessoa jur�dica em geral (n�o participante de SCP como s�cia ostensiva)' else
      if Codigo = '01' then
        Result := 'Sociedade cooperativa (n�o participante de SCP como s�cia ostensiva)' else
      if Codigo = '02' then
        Result := 'Entidade sujeita ao PIS/Pasep exclusivamente com base na Folha de Sal�rios' else
      if Codigo = '03' then
        Result := 'Pessoa jur�dica em geral participante de SCP como s�cia ostensiva' else
      if Codigo = '04' then
        Result := 'Sociedade cooperativa participante de SCP como s�cia ostensiva' else
      if Codigo = '05' then
        Result := 'Sociedade em Conta de Participa��o - SCP' else
      Result := '???';
    end;
    spedCOD_INC_TRIB:
    begin
      if Codigo =  '1' then Result := 'Escritura��o de opera��es com incid�ncia exclusivamente no regime n�o-cumulativo' else
      if Codigo =  '2' then Result := 'Escritura��o de opera��es com incid�ncia exclusivamente no regime cumulativo;' else
      if Codigo =  '3' then Result := 'Escritura��o de opera��es com incid�ncia nos regimes n�o-cumulativo e cumulativo.' else
      Result := '???';
    end;
    spedIND_APRO_CRED:
    begin
      if Codigo =  '1' then Result := 'M�todo de Apropria��o Direta;' else
      if Codigo =  '2' then Result := 'M�todo de Rateio Proporcional (Receita Bruta)' else
      Result := ''; // pode ser vazio
    end;
    spedCOD_TIPO_CONT:
    begin
      if Codigo =  '1' then Result := 'Apura��o da Contribui��o Exclusivamente a Al�quota B�sica' else
      if Codigo =  '2' then Result := 'Apura��o da Contribui��o a Al�quotas Espec�ficas (Diferenciadas e/ou por Unidade de Medida de Produto)' else
      Result := ''; // pode ser vazio
    end;
    spedIND_REG_CUM:
    begin
      if Codigo =  '1' then Result := 'Regime de Caixa � Escritura��o consolidada (Registro F500);' else
      if Codigo =  '2' then Result := 'Regime de Compet�ncia - Escritura��o consolidada (Registro F550);' else
      if Codigo =  '9' then Result := 'Regime de Compet�ncia - Escritura��o detalhada, com base nos registros dos Blocos �A�, �C�, �D� e �F�.' else
      Result := ''; // pode ser vazio
    end;
  else Result := '??????';
  end;
end;

function TNFe_PF.TextoDeCodigoSPED_TXT2(Tipo: TSPEDCodType; Codigo: String;
  Lista: MyArrayLista): String;
var
  I: Integer;
begin
  Result := '';
  for I := Low(Lista) to High(Lista) do
  begin
    if Lista[I][0] = Codigo then
    begin
      Result := Lista[I][1];
      Break;
    end;
  end;
end;

procedure TNFe_PF.ValidaXML_NFe(ArquivoXML: String);
begin
  if DBCheck.CriaFm(TFmNFeValidaXML_0001, FmNFeValidaXML_0001, afmoNegarComAviso) then
  begin
    FmNFeValidaXML_0001.EdArquivo.ValueVariant := ArquivoXML;
    FmNFeValidaXML_0001.ShowModal;
    FmNFeValidaXML_0001.Destroy;
  end;
end;

function TNFe_PF.VersaoNFeEmUso(): Integer;
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(VAR_LIB_EMPRESAS);
  //
  DModG.ReopenParamsEmp(Empresa);
  //
  Result := Trunc((DModG.QrPrmsEmpNFeversao.Value *100) + 0.5);
end;

end.
