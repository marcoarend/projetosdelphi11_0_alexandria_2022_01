unit NFeEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkEdit, DB,
  mySQLDbTables, frxClass, frxExportPDF, ShellAPI, UnDmkProcFunc,
  // Indy
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, IdText,
  IdAttachment, IdBaseComponent, IdMessage, ActiveX, ComObj, IdAttachmentFile,
  IdExplicitTLSClientServerBase, IdSMTPBase, DBCtrls, dmkDBGrid, ComCtrls,
  dmkDBLookupComboBox, dmkEditCB, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  // Fim Indy
  dmkImage, UnDmkEnums;
type
  TFmNFeEmail = class(TForm)
    Panel1: TPanel;
    Grade: TStringGrid;
    Panel3: TPanel;
    EdNFeId: TdmkEdit;
    Label1: TLabel;
    EdNFeStatus_TXT: TEdit;
    Label2: TLabel;
    EdNFeStatus: TdmkEdit;
    EdDANFE: TdmkEdit;
    LaDANFE: TLabel;
    Label4: TLabel;
    EdNFeXML: TdmkEdit;
    EdCNPJ_CPF: TdmkEdit;
    Label5: TLabel;
    SbDANFE: TSpeedButton;
    SpeedButton2: TSpeedButton;
    QrEmails: TmySQLQuery;
    QrEmailsEMail: TWideStringField;
    frxPDFExport: TfrxPDFExport;
    EdNFeIDCtrl: TdmkEdit;
    Label3: TLabel;
    CkProt: TCheckBox;
    CkRecriarImagens: TCheckBox;
    EdPreEmail: TdmkEditCB;
    CBPreEmail: TdmkDBLookupComboBox;
    Label7: TLabel;
    QrPre: TmySQLQuery;
    DsPre: TDataSource;
    IdSMTP1: TIdSMTP;
    QrEmailsNome: TWideStringField;
    QrPreCodigo: TIntegerField;
    QrPreNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGMsg: TdmkDBGrid;
    MeMsg: TDBMemo;
    TabSheet2: TTabSheet;
    MeEncode: TMemo;
    Eddest_xNome: TEdit;
    Label8: TLabel;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    EdVersaoNFe: TdmkEdit;
    Label9: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtEnvia: TBitBtn;
    BtEntidades: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbDANFEClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure EdNFeStatusChange(Sender: TObject);
    procedure EdPreEmailChange(Sender: TObject);
    procedure IdSMTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure BtEntidadesClick(Sender: TObject);
  private
    { Private declarations }
    FAutorizada, FAtivou: Boolean;
    FIdMessage: TIdMessage;
    function EnviaEmeio(ArqDANFE, ArqXML: String; AnexaArq: Boolean): Boolean;
    function SubstituiVariaveis(Texto: WideString): String;
    procedure Info(Msg: String);
    procedure CarregaListaEmails;
  public
    { Public declarations }
    Ffrx: TfrxReport;
  end;

  var
  FmNFeEmail: TFmNFeEmail;

implementation

uses UnMyObjects, dmkGeral, ModuleGeral, NFeXMLGerencia, ModuleNFe_0000;

const
  FCaminhoEMail = 'C:\Dermatek\NFe\Emails\';
  FIniPath      = 'C:\Dermatek\Base64\';
  FBase64       = 'C:\Dermatek\Base64\Base64.exe';
  SInterno      = 'Interno';
  
{$R *.DFM}

procedure TFmNFeEmail.BtEntidadesClick(Sender: TObject);
var
  CNPJ_CPF: String;
  Entidade: Integer;
begin
  CNPJ_CPF := Geral.SoNumero_TT(EdCNPJ_CPF.Text);
  DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
  //
  if Entidade <> 0 then
  begin
    DmodG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
    CarregaListaEmails;
  end else
    Geral.MB_Info('N�o foi poss�vel localizar a entidade!0');
end;

procedure TFmNFeEmail.BtEnviaClick(Sender: TObject);
var
  Id, ArqDANFE, ArqXML: String;
  Continua: Boolean;
  XML_Distribuicao: String;
begin
  Screen.Cursor := crHourGlass;
  //
  Id := Geral.SoNumero_TT(EdNFeId.Text);
  //
  // DANFE
  //
  if FAutorizada then
  begin
    Info('Gerando arquivo PDF do DANFE');
    ArqDANFE := EdDANFE.Text;
    Geral.VerificaDir(ArqDANFE, '\', 'Diret�rio tempor�rio do DANFE', True);
    ArqDANFE := ArqDANFE + Id + '.pdf';
    //Geral.VerificaDir(DirNFeXML, '\', '', True);
    frxPDFExport.FileName := ArqDANFE;
    // j� foi feito no FmNFe_Pesq2!
    MyObjects.frxPrepara(Ffrx, 'NFe' + ID);
    Ffrx.Export(frxPDFExport);
  end;
  //
  // XML
  //
  Info('Gerando arquivo XML da NFe');
  ArqXML := EdNFeXML.Text;
  Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
  ArqXML := ArqXML + ID + '.xml';
  //
  DmNFe_0000.QrArq.Close;
  DmNFe_0000.QrArq.Params[0].AsInteger := EdNFeIDCtrl.ValueVariant;
  DmNFe_0000.QrArq.Open;
  //
  Continua := False;
  if NFeXMLGeren.XML_DistribuiNFe(Id, EdNFeStatus.ValueVariant,
  DmNFe_0000.QrArqXML_NFe.Value, DmNFe_0000.QrArqXML_Aut.Value, DmNFe_0000.QrArqXML_Can.Value,
  CkProt.Checked, XML_Distribuicao, EdVersaoNFe.ValueVariant, 'na base de dados') then
    //Geral.MensagemBox(XML_Distribuicao, 'XML da NFe protocolada', MB_OK+MB_ICONINFORMATION);
    Continua :=
      NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
  if not Continua then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  EnviaEmeio(ArqDANFE, ArqXML, True);
end;

function TFmNFeEmail.EnviaEmeio(ArqDANFE, ArqXML: String; AnexaArq: Boolean): Boolean;

  procedure AnexaDANFE();
  begin
    if ArqDANFE <> '' then
    begin
      with TIdAttachmentFile.Create(FIdMessage.MessageParts, ArqDANFE) do
      begin
        ContentType        := 'application/pdf';
        ContentDisposition := 'attachment';
        ContentTransfer    := 'base64';
      end;
    end;
  end;

  //
  procedure AnexaXML();
  begin
    with TIdAttachmentFile.Create(FIdMessage.MessageParts, ArqXML) do
    begin
      ContentType        := 'application/xml';
      ContentDisposition := 'attachment';
      ContentTransfer    := 'base64';
    end;
  end;

  //

  procedure AnexaTextoPlano;
  var
    Texto: String;
  begin
    if DmodG.QrPEM_0.RecordCount > 0 then
    begin
      Texto := '';
      DmodG.QrPEM_0.First;
      while not DmodG.QrPEM_0.Eof do
      begin
        Texto := Texto + #13+#10 + SubstituiVariaveis(DmodG.QrPEM_0Texto.Value);
        DmodG.QrPEM_0.Next;
      end;
    // � obrigat�rio mandar um texto plano
    end else Texto := 'E-mail sem texto plano!!!!';
    with TIdText.Create(FIdMessage.MessageParts) do
    begin
      Body.Text       := Texto;
      ContentTransfer := '7bit';
      ContentType     := 'text/plain';
      ParentPart      := 1;
    end;
  end;

  //

  procedure AnexaHTMLs;
  var
    TextoHTML: TIdText;
  begin
    if DmodG.QrPEM_1.RecordCount > 0 then
    begin
      DmodG.QrPEM_1.First;
      TextoHTML := TIdText.Create(FIdMessage.MessageParts);
      with TextoHTML do
      begin
        ContentTransfer := '7bit';
        ContentType     := 'text/html';
        ParentPart      := 1;
        Body.Text       := '';
        while not DmodG.QrPEM_1.Eof do
        begin
          Body.Text := Body.Text +'<br/>' + SubstituiVariaveis(DmodG.QrPEM_1Texto.Value);
          DmodG.QrPEM_1.Next;
        end;
      end;
    end;
  end;

  //

  procedure AnexaImagensCorpo;
  var
    ImgPath, TxtPath: String;
    ArqExiste: Boolean;
    Comando: PChar;
  begin
    if DmodG.QrPEM_2.RecordCount > 0 then
    begin
      DmodG.QrPEM_2.First;
      while not DmodG.QrPEM_2.Eof do
      begin
        ImgPath := FIniPath + FormatFloat('00000000', DmodG.QrPEM_2Controle.Value) +
        '\' + DmodG.QrPEM_2Descricao.Value;
        TxtPath := dmkPF.MudaExtensaoDeArquivo(ImgPath, 'txt');

        //
        ArqExiste := FileExists(ImgPath);
        if not ArqExiste or CkRecriarImagens.Checked then
        begin
          if FileExists(FBase64) then
          begin
            //if FileExist(TxtPath) then
              //DeleteFile(TxtPath);
            Geral.SalvaTextoEmArquivo(TxtPath, SubstituiVariaveis(DmodG.QrPEM_2Texto.Value), True);
            Comando := PChar(FBase64 + ' -d ' + TxtPath + ' ' + ImgPath);
            //"C:\base64 -e bg2.jpg bg2.txt"
            dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
            ArqExiste := FileExists(ImgPath);
          end else Application.MessageBox(PChar('O aplicativo "' + FBase64 +
          '" n�o existe para criar a imagem "' + ImgPath + '".'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;
        if ArqExiste then
        begin
          with TIdAttachmentFile.Create(FIdMessage.MessageParts, ImgPath) do
          begin
            ContentType        := 'image/jpg';//AcharContentType(ExtractFileExt(ImgPath));
            ContentDisposition := 'inline';
            ContentTransfer    := 'base64';
            ContentID          := '<' + DmodG.QrPEM_2CidID_Img.Value + '>';
            ParentPart         := 0;
          end;
        end;
        DmodG.QrPEM_2.Next;
      end;
    end;
  end;

  //

var
  ImageMsg, AnexoArq: Boolean;
  I: Integer;
//  Link, Msg: String;
begin
  try
    Screen.Cursor      := crHourGlass;
    Result := False;
    Info('Conectando');
    //
    if DModG.QrPreEmail.RecordCount = 0 then
    begin
      Application.MessageBox(
      'Envio cancelado! Nenhum pr�-emeio foi selecionado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //

    IdSMTP1.Disconnect(True);
    IdSMTP1.Host     := DmodG.QrPreEmailSMTPServer.Value;
    IdSMTP1.Username := DmodG.QrPreEmailLogi_Name.Value;
    IdSMTP1.Password := DmodG.QrPreEmailLogi_Pass.Value;;
    if DModG.QrPreEmailPorta_Mail.Value > 0 then
      IdSMTP1.Port := DModG.QrPreEmailPorta_Mail.Value
    else
      IdSMTP1.Port :=  25;//StrToInt(edtPorta.Text);
    if DModG.QrPreEmailLogi_SSL.Value = 1 then
    begin
      //In�cio - Verifica se DLL's existem
      if not FileExists(ExtractFilePath(Application.ExeName) + 'libeay32.dll') then
      begin
        if Geral.MensagemBox('DLL libeay32.dll n�o localizada!' + #13#10 +
         'Deseja fazer o download? ' + #13#10 +
         'Salve o arquivo no diret�rio: "' + ExtractFilePath(Application.ExeName)
         + '"', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          ShellExecute(Application.Handle, nil,
          PChar('http://www.dermatek.com.br/DLLs/libeay32.dll'), nil, nil, sw_hide);
          Info('Erro no envio do email!');
          Exit;
        end else
        begin
          Info('Erro no envio do email!');
          Exit;
        end;
      end;
      if not FileExists(ExtractFilePath(Application.ExeName) + 'ssleay32.dll') then
      begin
        if Geral.MensagemBox('DLL ssleay32.dll n�o localizada!' + #13#10 +
         'Deseja fazer o download? ' + #13#10 +
         'Salve o arquivo no diret�rio: "' + ExtractFilePath(Application.ExeName)
         + '"', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          ShellExecute(Application.Handle, nil,
          PChar('http://www.dermatek.com.br/DLLs/ssleay32.dll'), nil, nil, sw_hide);
          Info('Erro no envio do email!');
          Exit;
        end else
        begin
          Info('Erro no envio do email!');
          Exit;
        end;
      end;
      //Fim - Verifica se DLL's existem
      //
      IdSMTP1.IOHandler := IdSSL1;
      IdSMTP1.UseTLS    := utUseExplicitTLS;
      //
      IdSSL1.Host              := DModG.QrPreEmailSMTPServer.Value;
      IdSSL1.Port              := DModG.QrPreEmailPorta_Mail.Value;
      IdSSL1.SSLOptions.Mode   := sslmUnassigned;
      IdSSL1.SSLOptions.Method := sslvTLSv1;
    end;
    //
    if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
      raise Exception.Create('SMTP com dados incompletos...');

    try
      IdSMTP1.Connect;
    except
      ShowMessage('Erro na Conex�o...');
    end;

    if IdSMTP1.Connected then
    begin
      Info('Configurando email');
      //PlainMsg := DmodG.QrPEM_0.RecordCount > 0;
      //HTML_Msg := DmodG.QrPEM_1.RecordCount > 0;
      ImageMsg := DmodG.QrPEM_2.RecordCount > 0;
      AnexoArq := (FileExists(ArqDANFE) or FileExists(ArqXML)) and AnexaArq;
      //
      (*Set up the message...*)
      FIdMessage              := TIdMessage.Create(nil);
      FIdMessage.Subject      := SubstituiVariaveis(DmodG.QrPreEmailMail_Titu.Value);
      FIdMessage.From.Address := DmodG.QrPreEmailSend_Mail.Value;//edSenderEMail.Text;
      FIdMessage.From.Name    := DmodG.QrPreEmailSend_Name.Value;//edSenderName.Text;
      FIdMessage.Encoding     := meMIME;
      FIdMessage.CharSet      := 'iso-8859-1';
      for I := 1 to Grade.RowCount -1 do
      begin
        if Grade.Cells[1,I] = SInterno then
        begin
          with FIdMessage.CCList.Add do
          begin
            Name    := Grade.Cells[2,I];
            Address := Grade.Cells[3,I];
          end;
        end else begin
          with FIdMessage.Recipients.Add do
          begin
            Name    := Grade.Cells[2,I];
            Address := Grade.Cells[3,I];
          end;
        end;  
      end;
      if not ImageMsg and not AnexoArq then
      begin
        // S� e texto puro e em HMTL
        if AnexaArq then
          Application.MessageBox('N�o h� DANFE ou XML para anexar � mensagem!',
          'Aviso', MB_OK+MB_ICONWARNING);
        FIdMessage.ContentType := 'multipart/alternative';
        //AnexaTextoPlano; n�o colocar!! Erro quando n�o tem bloqueto anexado
        AnexaHTMLs;
        // Final s� e texto puro e em HMTL
      end
      else if AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML, Figura Inline e Anexo(s)
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo;
        if AnexaArq then
        begin
          AnexaXML;
          AnexaDANFE;
        end;
        // Final Texto puro, HTML, Figura Inline e Anexo(s)
      end
      else if not AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML e a figura Inline
        FIdMessage.ContentType := 'multipart/related';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo
        // Final texto puro, HTML e a figura Inline
      end
      else if AnexoArq and not ImageMsg then
      begin
        // Texto puro, HTML e anexos
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;


        AnexaTextoPlano;
        AnexaHTMLs;
        if AnexaArq then
        begin
          AnexaDANFE;
          AnexaXML;
        end;
        // Final texto puro, HTML e anexos
      end;
      try
        Info('Enviando email');
        IdSMTP1.Send(FIdMessage);
        Result := True;
        Info('Email enviado com sucesso!');
      except
        begin
          //Info('Erro no envio do email!');
          Application.MessageBox('Erro no envio do Email descrito no status',
            'Aviso', MB_OK+MB_ICONERROR);
        end;
      end;
    end;
  finally
    IdSMTP1.Disconnect;
    FIdMessage.Free;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmNFeEmail.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEmail.CarregaListaEmails;
var
  CNPJ_CPF, Emails, Email: String;
  I, Entidade, EntiTipCto: Integer;
begin
  FAtivou := True;
  Emails := DmNFe_0000.QrOpcoesNFeMyEmailNFe.Value;
  I := 0;
  MyObjects.LimpaGrade(Grade, 1, 1, True);
  Grade.Cells[00,0] := 'Seq.';
  Grade.Cells[01,0] := 'Tipo';
  Grade.Cells[02,0] := 'Nome';
  Grade.Cells[03,0] := 'Email';
  while Emails <> '' do
  begin
    Geral.SeparaPrimeiraOcorrenciaDeTexto(';', Emails, Email, Emails);
    I := I + 1;
    Grade.RowCount := I + 1;
    Grade.Cells[00,I] := FormatFloat('00', I);
    Grade.Cells[01,I] := SInterno;
    Grade.Cells[02,I] := 'Envio paralelo';
    Grade.Cells[03,I] := Email;
    //
  end;
  //
  CNPJ_CPF := Geral.SoNumero_TT(EdCNPJ_CPF.Text);
  DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
  EntiTipCto := DmNFe_0000.QrOpcoesNFeEntiTipCto.Value;
  if EntiTipCto <> 0 then
  begin
    QrEmails.Close;
    QrEmails.Params[00].AsInteger := Entidade;
    QrEmails.Params[01].AsInteger := DmNFe_0000.QrOpcoesNFeEntiTipCto.Value;
    QrEmails.Open;
    //
    if QrEmails.RecordCount > 0 then
    begin
      BtEnvia.Enabled := True;
      while not QrEmails.Eof do
      begin
        I := I + 1;
        Grade.RowCount := I + 1;
        Grade.Cells[00,I] := FormatFloat('00', I);
        Grade.Cells[01,I] := 'Terceiro';
        Grade.Cells[02,I] := QrEmailsNome.Value;
        Grade.Cells[03,I] := QrEmailsEMail.Value;
        //
        QrEmails.Next;
      end;
    end else Geral.MensagemBox(
    'Cliente sem email definido para envio de DANFE e XML!' + #13#10 +
    'Tipo de email: ' + DmNFe_0000.QrOpcoesNFeNO_ETC_0.Value,
    'Aviso', MB_OK+MB_ICONWARNING);
  end else Geral.MensagemBox(
  '"Tipo de email para envio de NFe e Carta de Corre��o" n�o definido nas op��es de NF-e!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmNFeEmail.EdNFeStatusChange(Sender: TObject);
var
  PreEmail: Integer;
begin
  FAutorizada := EdNFeStatus.ValueVariant = 100;
  LaDANFE.Enabled := FAutorizada;
  EdDANFE.Enabled := FAutorizada;
  SbDANFE.Enabled := FAutorizada;
  if FAutorizada then
    PreEmail := DModG.QrParamsEmpPreMailAut.Value
  else
    PreEmail := DModG.QrParamsEmpPreMailCan.Value;
  EdPreEmail.ValueVariant := PreEmail;
  CBPreEmail.KeyValue     := PreEmail;
end;

procedure TFmNFeEmail.EdPreEmailChange(Sender: TObject);
begin
  DModG.ReopenPreEmail(Geral.IMV(EdPreEmail.Text));
end;

procedure TFmNFeEmail.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FAtivou then
    CarregaListaEmails;
end;

procedure TFmNFeEmail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGMsg.DataSource := DModG.DsPreEmMsg;
  MeMsg.DataSource  := DModG.DsPreEmMsg;
  FAutorizada := False;
  FAtivou := False;
  QrPre.Close;
  QrPre.Open;
end;

procedure TFmNFeEmail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEmail.IdSMTP1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, AStatusText);
end;

procedure TFmNFeEmail.Info(Msg: String);
var
  Aguarde: Boolean;
begin
  Aguarde := Ord(Msg[Length(Msg)]) in ([048..122]);
  MyObjects.Informa2(LaAviso1, LaAviso2, Aguarde, Msg);
end;

procedure TFmNFeEmail.SbDANFEClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDANFE);
end;

procedure TFmNFeEmail.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdNFeXML);
end;

function TFmNFeEmail.SubstituiVariaveis(Texto: WideString): String;
//var
  //ID_Protocolo: String;
begin
  Result := Texto;
  {
  ID_Protocolo := FormatFloat('0', QrUnidCondApto.Value) + '#' +
                  FormatFloat('0', QrUnidCondProtoco.Value) + '#' +
                  FormatFloat('0', QrUnidCondBloquet.Value) + '#' +
                  FormatFloat('0', QrUnidCondIDEmeio.Value);
  Randomize;
  ID_Protocolo := MLAGeral.PWDExEncode(ID_Protocolo, Dmod.QrControleSecuritStr.Value);
  ID_Protocolo := Dmod.QrControleWeb_MyURL.Value +
    '/index.php?page=recebibloqueto&protocolo=' + ID_Protocolo;
  }
  //
  Result := Geral.Substitui(Result, '[Empresa]'       , DModG.ObtemNomeFantasiaEmpresa());
  Result := Geral.Substitui(Result, '[ChaveNFe]'      , EdNFeId.Text);
  Result := Geral.Substitui(Result, '[Saudacao]'      , DModG.QrPreEmailSaudacao.Value);
  Result := Geral.Substitui(Result, '[Cliente]'       , Eddest_xNome.Text);
  //Result := Geral.Substitui(Result, '[NumeroNF]'      , EdNFe);

  // Parei Aqui
  //[LinkConfirma]
  //mudar status
end;

end.
