object FmNFe_Pesq_0000: TFmNFe_Pesq_0000
  Left = 339
  Top = 185
  Caption = 'NFe-PESQU-001 :: Pesquisa de NFe'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 234
    Width = 1008
    Height = 388
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object Splitter2: TSplitter
      Left = 0
      Top = 253
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
    end
    object LaTotal: TLabel
      Left = 0
      Top = 240
      Width = 1008
      Height = 13
      Align = alBottom
      ExplicitTop = 242
      ExplicitWidth = 3
    end
    object dmkDBGrid1: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 1008
      Height = 240
      Align = alClient
      DataSource = DsNFeCabA
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDrawColumnCell = dmkDBGrid1DrawColumnCell
      OnDblClick = dmkDBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Fat.ID'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_serie'
          Title.Caption = 'S'#233'rie'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nNF'
          Title.Caption = 'N'#186' NF'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LoteEnv'
          Title.Caption = 'Lote'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Status'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'xMotivo'
          Title.Caption = 'Motivo do status'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'infCCe_nSeqEvento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'CCe'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_cSitConf'
          Title.Caption = 'Manifesta'#231#227'o'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dhRecbto'
          Title.Caption = 'Data/hora fisco'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Terceiro'
          Title.Caption = 'Destinat'#225'rio / remetente ou Emitente'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ICMSTot_vNF'
          Title.Caption = 'R$ NF'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ICMSTot_vProd'
          Title.Caption = 'R$ Prod.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Id'
          Title.Caption = 'Chave de acesso da NF-e'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_dEmi'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_natOp'
          Title.Caption = 'Natureza da opera'#231#227'o'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nProt'
          Title.Caption = 'N'#186' do protocolo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_tpNF'
          Title.Caption = 'tpNF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Emi'
          Title.Caption = 'Emitente'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cli'
          Width = 240
          Visible = True
        end>
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 258
      Width = 1008
      Height = 130
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Eventos enviados ou consultados:'
        object DBGEventos: TDBGrid
          Left = 0
          Top = 22
          Width = 1000
          Height = 80
          Align = alClient
          DataSource = DsNFeEveRRet
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnMouseUp = DBGEventosMouseUp
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_EVENTO'
              Title.Caption = 'Ocorr'#234'ncia'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_cStat'
              Title.Caption = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_xMotivo'
              Title.Caption = 'Motivo do stuats'
              Width = 485
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_nProt'
              Title.Caption = 'Protocolo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_dhRegEvento'
              Title.Caption = 'Data / hora'
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          Caption = 
            'Click contr'#225'rio do mouse na grade de eventos mostra menu de op'#231#245 +
            'es de eventos.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Mensagens recebidas dos web services: '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGMensagens: TDBGrid
          Left = 0
          Top = 0
          Width = 1002
          Height = 105
          Align = alClient
          DataSource = DsNFeCabAMsg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 129
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Panel5: TPanel
      Left = 621
      Top = 0
      Width = 387
      Height = 129
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 200
        Top = 7
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label2: TLabel
        Left = 200
        Top = 47
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object RGOrdem1: TRadioGroup
        Left = 4
        Top = 1
        Width = 101
        Height = 84
        Caption = ' Ordem: '
        ItemIndex = 0
        Items.Strings = (
          'Data, N'#186' NF'
          'N'#186' NF, Data'
          'Status, N'#186' NF'
          'Status, Data')
        TabOrder = 0
        OnClick = RGOrdem1Click
      end
      object RGOrdem2: TRadioGroup
        Left = 316
        Top = 5
        Width = 61
        Height = 80
        Caption = ' Sentido: '
        ItemIndex = 1
        Items.Strings = (
          'ASC'
          'DES')
        TabOrder = 1
        OnClick = RGOrdem2Click
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 288
        Top = 87
        Width = 90
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtReabreClick
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 200
        Top = 23
        Width = 112
        Height = 21
        Date = 40017.908086238430000000
        Time = 40017.908086238430000000
        TabOrder = 3
        OnClick = TPDataIClick
        OnChange = TPDataIChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 200
        Top = 63
        Width = 112
        Height = 21
        Date = 40017.908141423620000000
        Time = 40017.908141423620000000
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object Ck100e101: TCheckBox
        Left = 5
        Top = 89
        Width = 189
        Height = 17
        Caption = 'Somente autorizadas e canceladas.'
        TabOrder = 5
        OnClick = CkEmitClick
      end
      object CkideNatOp: TCheckBox
        Left = 5
        Top = 109
        Width = 189
        Height = 17
        Caption = 'Ordenar por Natureza da Opera'#231#227'o'
        TabOrder = 6
        OnClick = CkEmitClick
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 457
      Height = 129
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object dmkLabel1: TdmkLabel
        Left = 4
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Empresa (obrigat'#243'rio):'
        UpdType = utYes
        SQLType = stNil
      end
      object Label14: TLabel
        Left = 4
        Top = 44
        Width = 97
        Height = 13
        Caption = 'Cliente / fornecedor:'
      end
      object EdFilial: TdmkEditCB
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        OnExit = EdFilialExit
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 59
        Top = 20
        Width = 390
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        OnExit = EdClienteExit
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 59
        Top = 60
        Width = 390
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_Enti'
        ListSource = DsClientes
        TabOrder = 3
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object RGAmbiente: TRadioGroup
        Left = 4
        Top = 84
        Width = 189
        Height = 41
        Caption = ' Ambiente: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Ambos'
          'Produ.'
          'Homol.')
        TabOrder = 4
        OnClick = RGAmbienteClick
      end
      object RGQuemEmit: TRadioGroup
        Left = 195
        Top = 84
        Width = 254
        Height = 41
        Caption = ' Quem emitiu:  '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Emitidas'
          'Recebidas'
          'Ambas')
        TabOrder = 5
        OnClick = RGAmbienteClick
      end
    end
    object CGcSitConf: TdmkCheckGroup
      Left = 457
      Top = 0
      Width = 164
      Height = 129
      Align = alLeft
      Caption = ' Manifesta'#231#227'o do destinat'#225'rio: '
      ItemIndex = 4
      Items.Strings = (
        'Situa'#231#227'o n'#227'o verificada'
        'Sem manifesta'#231#227'o'
        'Confirmada opera'#231#227'o'
        'Desconhecida'
        'Opera'#231#227'o n'#227'o realizada'
        'Ci'#234'ncia da opera'#231#227'o')
      PopupMenu = PMcSitConf
      TabOrder = 2
      OnClick = CGcSitConfClick
      UpdType = utYes
      Value = 16
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 19
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 34
      Width = 1004
      Height = 17
      Align = alBottom
      TabOrder = 1
      Visible = False
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 94
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Envia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviaClick
      end
      object BtEvento: TBitBtn
        Tag = 526
        Left = 184
        Top = 4
        Width = 90
        Height = 40
        Caption = 'E&vento'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEventoClick
      end
      object BtCancela: TBitBtn
        Tag = 455
        Left = 274
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Ca&ncela'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtCancelaClick
      end
      object BtInutiliza: TBitBtn
        Tag = 455
        Left = 364
        Top = 4
        Width = 90
        Height = 40
        Caption = 'In&utiliza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtInutilizaClick
      end
      object BtLeArq: TBitBtn
        Tag = 40
        Left = 454
        Top = 4
        Width = 90
        Height = 40
        Caption = '&L'#234' arquivo'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtLeArqClick
      end
      object BtArq: TBitBtn
        Tag = 512
        Left = 544
        Top = 4
        Width = 90
        Height = 40
        Caption = 'L'#234' &XML/BD'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtArqClick
      end
      object BtConsulta: TBitBtn
        Tag = 528
        Left = 634
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Consulta'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtConsultaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 724
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 8
        OnClick = BtExcluiClick
      end
      object Panel2: TPanel
        Left = 909
        Top = 0
        Width = 95
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 9
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrClientesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Fornece7="V"'
      'OR Fornece8="V"'
      'ORDER BY NO_Enti')
    Left = 620
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrClientesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClientesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrClientesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 648
    Top = 12
  end
  object QrNFeCabA_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ICMSTot_vProd, ICMSTot_vNF, '
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON '
      '/*  IF(nfa.dest_CNPJ<>'#39#39',nfa.dest_CNPJ=cli.CNPJ,'
      '  nfa.dest_CPF=cli.CPF)*/'
      'cli.Codigo=nfa.CodInfoDest'
      'ORDER BY nfa.DataCad DESC, nfa.ide_nNF DESC')
    Left = 752
    Top = 364
    object QrNFeCabA_NO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrNFeCabA_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabA_LoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNFeCabA_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabA_ide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeCabA_ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFeCabA_ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabA_ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeCabA_ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNFeCabA_ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabA_ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabA_cStat: TFloatField
      FieldName = 'cStat'
    end
    object TStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeCabA_xMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeCabA_dhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrNFeCabA_IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabA_versao: TFloatField
      FieldName = 'versao'
    end
  end
  object DsNFeCabA_: TDataSource
    DataSet = QrNFeCabA_
    Left = 752
    Top = 412
  end
  object PMLeArq: TPopupMenu
    Left = 420
    Top = 432
    object Cancelamento1: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
      OnClick = Cancelamento1Click
    end
  end
  object QrNFeCabAMsg_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabamsg '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 816
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabAMsg_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAMsg_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAMsg_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAMsg_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabAMsg_Solicit: TIntegerField
      FieldName = 'Solicit'
    end
    object QrNFeCabAMsg_Id: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrNFeCabAMsg_tpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeCabAMsg_verAplic: TWideStringField
      FieldName = 'verAplic'
      Size = 30
    end
    object QrNFeCabAMsg_dhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeCabAMsg_nProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeCabAMsg_digVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrNFeCabAMsg_cStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeCabAMsg_xMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
  end
  object DsNFeCabAMsg_: TDataSource
    DataSet = QrNFeCabAMsg_
    Left = 844
    Top = 316
  end
  object frxDsA_: TfrxDBDataset
    UserName = 'frxDsA_'
    CloseDataSource = False
    DataSet = QrA_
    BCDToCurrency = False
    Left = 672
    Top = 244
  end
  object QrA_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrA_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrA_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrA_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrA_LoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrA_versao: TFloatField
      FieldName = 'versao'
    end
    object QrA_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrA_ide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrA_ide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrA_ide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrA_ide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrA_ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrA_ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrA_ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrA_ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrA_ide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrA_ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrA_ide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrA_ide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrA_ide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrA_ide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrA_ide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrA_ide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrA_ide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrA_ide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrA_emit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrA_emit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrA_emit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrA_emit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrA_emit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrA_emit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrA_emit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrA_emit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrA_emit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrA_emit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrA_emit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrA_emit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrA_emit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrA_emit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrA_emit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrA_emit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrA_emit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrA_emit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrA_emit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrA_dest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrA_dest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrA_dest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrA_dest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrA_dest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrA_dest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrA_dest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrA_dest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrA_dest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrA_dest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrA_dest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrA_dest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrA_dest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrA_dest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrA_dest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrA_dest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrA_ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrA_ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrA_ICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrA_ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrA_ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrA_ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrA_ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrA_ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrA_ICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrA_ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrA_ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrA_ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrA_ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrA_ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrA_ISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrA_ISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrA_ISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrA_ISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrA_ISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrA_RetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrA_RetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrA_RetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrA_RetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrA_RetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrA_RetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrA_RetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrA_ModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrA_Transporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrA_Transporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrA_Transporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrA_Transporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrA_Transporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrA_Transporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrA_Transporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrA_RetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrA_RetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrA_RetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrA_RetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrA_RetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrA_RetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrA_VeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrA_VeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrA_VeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrA_Cobr_Fat_NFat: TWideStringField
      FieldName = 'Cobr_Fat_NFat'
      Size = 60
    end
    object QrA_Cobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrA_Cobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrA_Cobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrA_InfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrA_InfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrA_Exporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrA_Exporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrA_Compra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrA_Compra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrA_Compra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrA_Status: TIntegerField
      FieldName = 'Status'
    end
    object QrA_infProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrA_infProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrA_infProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrA_infProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrA_infProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrA_infProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrA_infProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrA_infProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrA__Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrA_Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrA_DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrA_DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrA_UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrA_UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrA_AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrA_Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrA_IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrA_infCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrA_infCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrA_infCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrA_infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrA_infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrA_infCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrA_infCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrA_infCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrA_infCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrA_infCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrA_ID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrA_EMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_IEST_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IEST_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_DEST_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_DEST_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrA_DEST_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CEP_TXT'
      Calculated = True
    end
    object QrA_DEST_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_FONE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_DEST_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_TRANSPORTA_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_TRANSPORTA_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_DOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrA_DOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
  end
  object QrI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrI_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrI_prod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrI_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrI_prod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrI_prod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrI_prod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrI_prod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrI_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrI_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrI_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrI_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrI_prod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrI_prod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrI_prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrI_prod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrI_prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrI_prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrI_prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrI_Tem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrI_nItem: TIntegerField
      FieldName = 'nItem'
    end
  end
  object frxDsI_: TfrxDBDataset
    UserName = 'frxDsI_'
    CloseDataSource = False
    DataSet = QrI_
    BCDToCurrency = False
    Left = 672
    Top = 272
  end
  object QrN_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrN_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrN_ICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrN_ICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrN_ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrN_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrN_ICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrN_ICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrN_ICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrN_ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrN_ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrN_ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrN_ICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrN_ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrN_ICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object QrO_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrO_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrO_IPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrO_IPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrO_IPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrO_IPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrO_IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrO_IPI_CST: TWideStringField
      FieldName = 'IPI_CST'
      Size = 2
    end
    object QrO_IPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrO_IPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrO_IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrO_IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrO_IPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object frxDsN_: TfrxDBDataset
    UserName = 'frxDsN_'
    CloseDataSource = False
    DataSet = QrN_
    BCDToCurrency = False
    Left = 672
    Top = 300
  end
  object frxDsO_: TfrxDBDataset
    UserName = 'frxDsO_'
    CloseDataSource = False
    DataSet = QrO_
    BCDToCurrency = False
    Left = 672
    Top = 328
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 8
    object CampospreenchidosnoXMLdaNFe1: TMenuItem
      Caption = '&Campos preenchidos no XML da NF-e'
      OnClick = CampospreenchidosnoXMLdaNFe1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Listadasnotaspesquisadas1: TMenuItem
      Caption = '&Lista das notas pesquisadas'
      OnClick = Listadasnotaspesquisadas1Click
    end
    object este1: TMenuItem
      Caption = 'Lista das notas pesquisadas'
      object porNatOp1: TMenuItem
        Caption = 'por NatOp (totais NFe)'
        OnClick = porNatOp1Click
      end
      object porCFOPtotaiseitensNFe1: TMenuItem
        Caption = 'por CFOP (totais e itens NFe)'
        OnClick = porCFOPtotaiseitensNFe1Click
      end
      object porCFOPeProdutototaiseitensNFe1: TMenuItem
        Caption = 'por CFOP e Produto (totais e itens NFe)'
        OnClick = porCFOPeProdutototaiseitensNFe1Click
      end
      object porNCMtotaiseitensNFe1: TMenuItem
        Caption = 'por NCM (totais e itens NFe)'
        OnClick = porNCMtotaiseitensNFe1Click
      end
    end
    object Listadefaturasdasnotaspesquisadas1: TMenuItem
      Caption = 'Lista de &faturas das notas pesquisadas'
      OnClick = Listadefaturasdasnotaspesquisadas1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object PreviewdaNFe1: TMenuItem
      Caption = 'Preview da NF-e'
      OnClick = PreviewdaNFe1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ImprimeSomenteadmin1: TMenuItem
      Caption = '&Imprime (Somente admin.)'
      OnClick = ImprimeSomenteadmin1Click
    end
  end
  object QrNFeXMLI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 764
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeXMLI_Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrNFeXMLI_ID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrNFeXMLI_Valor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrNFeXMLI_Pai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeXMLI_Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeXMLI_Campo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsNFeXMLI_: TfrxDBDataset
    UserName = 'frxDsNFeXMLI_'
    CloseDataSource = False
    DataSet = QrNFeXMLI_
    BCDToCurrency = False
    Left = 792
    Top = 284
  end
  object frxDsNFeCabA_: TfrxDBDataset
    UserName = 'frxDsNFeCabA_'
    CloseDataSource = False
    DataSet = QrNFeCabA_
    BCDToCurrency = False
    Left = 780
    Top = 316
  end
  object QrY_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nDup, dVenc, vDup'
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrY_nDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrY_dVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrY_vDup: TFloatField
      FieldName = 'vDup'
    end
  end
  object frxDsY_: TfrxDBDataset
    UserName = 'frxDsY_'
    CloseDataSource = False
    DataSet = QrY_
    BCDToCurrency = False
    Left = 672
    Top = 356
  end
  object QrNFeYIts_: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeyits')
    Left = 644
    Top = 384
    object QrNFeYIts_Seq1: TIntegerField
      FieldName = 'Seq1'
    end
    object QrNFeYIts_nDup1: TWideStringField
      FieldName = 'nDup1'
      Size = 60
    end
    object QrNFeYIts_dVenc1: TDateField
      FieldName = 'dVenc1'
    end
    object QrNFeYIts_vDup1: TFloatField
      FieldName = 'vDup1'
    end
    object QrNFeYIts_Seq2: TIntegerField
      FieldName = 'Seq2'
    end
    object QrNFeYIts_nDup2: TWideStringField
      FieldName = 'nDup2'
      Size = 60
    end
    object QrNFeYIts_dVenc2: TDateField
      FieldName = 'dVenc2'
    end
    object QrNFeYIts_vDup2: TFloatField
      FieldName = 'vDup2'
    end
    object QrNFeYIts_Seq3: TIntegerField
      FieldName = 'Seq3'
    end
    object QrNFeYIts_nDup3: TWideStringField
      FieldName = 'nDup3'
      Size = 60
    end
    object QrNFeYIts_dVenc3: TDateField
      FieldName = 'dVenc3'
    end
    object QrNFeYIts_vDup3: TFloatField
      FieldName = 'vDup3'
    end
    object QrNFeYIts_Seq4: TIntegerField
      FieldName = 'Seq4'
    end
    object QrNFeYIts_nDup4: TWideStringField
      FieldName = 'nDup4'
      Size = 60
    end
    object QrNFeYIts_dVenc4: TDateField
      FieldName = 'dVenc4'
    end
    object QrNFeYIts_vDup4: TFloatField
      FieldName = 'vDup4'
    end
    object QrNFeYIts_Seq5: TIntegerField
      FieldName = 'Seq5'
    end
    object QrNFeYIts_nDup5: TWideStringField
      FieldName = 'nDup5'
      Size = 60
    end
    object QrNFeYIts_dVenc5: TDateField
      FieldName = 'dVenc5'
    end
    object QrNFeYIts_vDup5: TFloatField
      FieldName = 'vDup5'
    end
    object QrNFeYIts_Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrNFeYIts_xVenc1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc1'
      Size = 10
      Calculated = True
    end
    object QrNFeYIts_xVenc2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc2'
      Size = 10
      Calculated = True
    end
    object QrNFeYIts_xVenc3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc3'
      Size = 10
      Calculated = True
    end
    object QrNFeYIts_xVenc4: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc4'
      Size = 10
      Calculated = True
    end
    object QrNFeYIts_xVenc5: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc5'
      Size = 10
      Calculated = True
    end
  end
  object frxDsNFeYIts_: TfrxDBDataset
    UserName = 'frxDsNFeYIts_'
    CloseDataSource = False
    DataSet = QrNFeYIts_
    BCDToCurrency = False
    Left = 672
    Top = 384
  end
  object QrXVol_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrXVol_qVol: TFloatField
      FieldName = 'qVol'
    end
    object QrXVol_esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrXVol_marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrXVol_nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrXVol_pesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrXVol_pesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object frxDsXvol_: TfrxDBDataset
    UserName = 'frxDsXvol_'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 672
    Top = 412
  end
  object QrV_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrV_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrV_InfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsV_: TfrxDBDataset
    UserName = 'frxDsV_'
    CloseDataSource = False
    DataSet = QrV_
    BCDToCurrency = False
    Left = 672
    Top = 440
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFeCabAAfterOpen
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF, '
      'dest_xNome,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao, '
      'ide_tpEmis, infCanc_xJust, Status,'
      'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg, '
      'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc, '
      'ICMSTot_vBC, ICMSTot_vICMS,'
      'cSitNFe, cSitConf,'
      
        'ELT(cSitConf+2, "N'#227'o consultada", "Sem manifesta'#231#227'o", "Confirmad' +
        'a", '
      
        '"Desconhecida", "N'#227'o realizada", "Ci'#234'ncia", "? ? ?") NO_cSitConf' +
        ','
      'nfa.infCanc_dhRecbto, nfa.infCanc_nProt, nfa.NFeNT2013_003LTT'
      ''
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON '
      '  IF(nfa.dest_CNPJ<>'#39#39',nfa.dest_CNPJ=cli.CNPJ,'
      '  nfa.dest_CPF=cli.CPF)'
      'WHERE IDCtrl=0'
      'ORDER BY nfa.DataCad DESC, nfa.ide_nNF DESC'
      '')
    Left = 36
    Top = 8
    object QrNFeCabANO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeCabAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNFeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNFeCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrNFeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNFeCabANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrNFeCabANOME_tpNF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpNF'
      Size = 1
      Calculated = True
    end
    object QrNFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFeCabAcStat: TFloatField
      FieldName = 'cStat'
    end
    object QrNFeCabAcSitNFe: TSmallintField
      FieldName = 'cSitNFe'
    end
    object QrNFeCabAcSitConf: TSmallintField
      FieldName = 'cSitConf'
    end
    object QrNFeCabANO_cSitConf: TWideStringField
      FieldName = 'NO_cSitConf'
      Size = 16
    end
    object QrNFeCabANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrNFeCabANO_Emi: TWideStringField
      FieldName = 'NO_Emi'
      Size = 100
    end
    object QrNFeCabANO_Terceiro: TWideStringField
      FieldName = 'NO_Terceiro'
      Size = 100
    end
    object QrNFeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFeCabANO_Cli2: TWideStringField
      FieldName = 'NO_Cli2'
      Size = 100
    end
    object QrNFeCabAinfCCe_nSeqEvento: TIntegerField
      FieldName = 'infCCe_nSeqEvento'
    end
    object QrNFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 64
    Top = 8
  end
  object frxDsNFeCabA: TfrxDBDataset
    UserName = 'frxDsNFeCabA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Cli=NO_Cli'
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'Id=Id'
      'ide_natOp=ide_natOp'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_tpNF=ide_tpNF'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vNF=ICMSTot_vNF'
      'cStat=cStat'
      'nProt=nProt'
      'xMotivo=xMotivo'
      'dhRecbto=dhRecbto'
      'IDCtrl=IDCtrl'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'versao=versao'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ide_tpEmis=ide_tpEmis'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF')
    DataSet = QrNFeCabA
    BCDToCurrency = False
    Left = 92
    Top = 8
  end
  object QrNFeCabAMsg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabamsg '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 128
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabAMsgFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAMsgFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAMsgEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAMsgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabAMsgSolicit: TIntegerField
      FieldName = 'Solicit'
    end
    object QrNFeCabAMsgId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrNFeCabAMsgtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeCabAMsgverAplic: TWideStringField
      FieldName = 'verAplic'
      Size = 30
    end
    object QrNFeCabAMsgdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeCabAMsgnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeCabAMsgdigVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrNFeCabAMsgcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeCabAMsgxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
  end
  object DsNFeCabAMsg: TDataSource
    DataSet = QrNFeCabAMsg
    Left = 156
    Top = 8
  end
  object frxDsA: TfrxDBDataset
    UserName = 'frxDsA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'versao=versao'
      'Id=Id'
      'ide_cUF=ide_cUF'
      'ide_cNF=ide_cNF'
      'ide_natOp=ide_natOp'
      'ide_indPag=ide_indPag'
      'ide_mod=ide_mod'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_dSaiEnt=ide_dSaiEnt'
      'ide_tpNF=ide_tpNF'
      'ide_cMunFG=ide_cMunFG'
      'ide_tpImp=ide_tpImp'
      'ide_tpEmis=ide_tpEmis'
      'ide_cDV=ide_cDV'
      'ide_tpAmb=ide_tpAmb'
      'ide_finNFe=ide_finNFe'
      'ide_procEmi=ide_procEmi'
      'ide_verProc=ide_verProc'
      'emit_CNPJ=emit_CNPJ'
      'emit_CPF=emit_CPF'
      'emit_xNome=emit_xNome'
      'emit_xFant=emit_xFant'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_cMun=emit_cMun'
      'emit_xMun=emit_xMun'
      'emit_UF=emit_UF'
      'emit_CEP=emit_CEP'
      'emit_cPais=emit_cPais'
      'emit_xPais=emit_xPais'
      'emit_fone=emit_fone'
      'emit_IE=emit_IE'
      'emit_IEST=emit_IEST'
      'emit_IM=emit_IM'
      'emit_CNAE=emit_CNAE'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'dest_xLgr=dest_xLgr'
      'dest_nro=dest_nro'
      'dest_xCpl=dest_xCpl'
      'dest_xBairro=dest_xBairro'
      'dest_cMun=dest_cMun'
      'dest_xMun=dest_xMun'
      'dest_UF=dest_UF'
      'dest_CEP=dest_CEP'
      'dest_cPais=dest_cPais'
      'dest_xPais=dest_xPais'
      'dest_fone=dest_fone'
      'dest_IE=dest_IE'
      'dest_ISUF=dest_ISUF'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ICMSTot_vBCST=ICMSTot_vBCST'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vII=ICMSTot_vII'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vPIS=ICMSTot_vPIS'
      'ICMSTot_vCOFINS=ICMSTot_vCOFINS'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vNF=ICMSTot_vNF'
      'ISSQNtot_vServ=ISSQNtot_vServ'
      'ISSQNtot_vBC=ISSQNtot_vBC'
      'ISSQNtot_vISS=ISSQNtot_vISS'
      'ISSQNtot_vPIS=ISSQNtot_vPIS'
      'ISSQNtot_vCOFINS=ISSQNtot_vCOFINS'
      'RetTrib_vRetPIS=RetTrib_vRetPIS'
      'RetTrib_vRetCOFINS=RetTrib_vRetCOFINS'
      'RetTrib_vRetCSLL=RetTrib_vRetCSLL'
      'RetTrib_vBCIRRF=RetTrib_vBCIRRF'
      'RetTrib_vIRRF=RetTrib_vIRRF'
      'RetTrib_vBCRetPrev=RetTrib_vBCRetPrev'
      'RetTrib_vRetPrev=RetTrib_vRetPrev'
      'ModFrete=ModFrete'
      'Transporta_CNPJ=Transporta_CNPJ'
      'Transporta_CPF=Transporta_CPF'
      'Transporta_XNome=Transporta_XNome'
      'Transporta_IE=Transporta_IE'
      'Transporta_XEnder=Transporta_XEnder'
      'Transporta_XMun=Transporta_XMun'
      'Transporta_UF=Transporta_UF'
      'RetTransp_vServ=RetTransp_vServ'
      'RetTransp_vBCRet=RetTransp_vBCRet'
      'RetTransp_PICMSRet=RetTransp_PICMSRet'
      'RetTransp_vICMSRet=RetTransp_vICMSRet'
      'RetTransp_CFOP=RetTransp_CFOP'
      'RetTransp_CMunFG=RetTransp_CMunFG'
      'VeicTransp_Placa=VeicTransp_Placa'
      'VeicTransp_UF=VeicTransp_UF'
      'VeicTransp_RNTC=VeicTransp_RNTC'
      'Cobr_Fat_NFat=Cobr_Fat_NFat'
      'Cobr_Fat_vOrig=Cobr_Fat_vOrig'
      'Cobr_Fat_vDesc=Cobr_Fat_vDesc'
      'Cobr_Fat_vLiq=Cobr_Fat_vLiq'
      'InfAdic_InfCpl=InfAdic_InfCpl'
      'Exporta_UFEmbarq=Exporta_UFEmbarq'
      'Exporta_XLocEmbarq=Exporta_XLocEmbarq'
      'Compra_XNEmp=Compra_XNEmp'
      'Compra_XPed=Compra_XPed'
      'Compra_XCont=Compra_XCont'
      'Status=Status'
      'infProt_Id=infProt_Id'
      'infProt_tpAmb=infProt_tpAmb'
      'infProt_verAplic=infProt_verAplic'
      'infProt_dhRecbto=infProt_dhRecbto'
      'infProt_nProt=infProt_nProt'
      'infProt_digVal=infProt_digVal'
      'infProt_cStat=infProt_cStat'
      'infProt_xMotivo=infProt_xMotivo'
      '_Ativo_=_Ativo_'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'IDCtrl=IDCtrl'
      'infCanc_Id=infCanc_Id'
      'infCanc_tpAmb=infCanc_tpAmb'
      'infCanc_verAplic=infCanc_verAplic'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'infCanc_digVal=infCanc_digVal'
      'infCanc_cStat=infCanc_cStat'
      'infCanc_xMotivo=infCanc_xMotivo'
      'infCanc_cJust=infCanc_cJust'
      'infCanc_xJust=infCanc_xJust'
      'ID_TXT=ID_TXT'
      'EMIT_ENDERECO=EMIT_ENDERECO'
      'EMIT_FONE_TXT=EMIT_FONE_TXT'
      'EMIT_IE_TXT=EMIT_IE_TXT'
      'EMIT_IEST_TXT=EMIT_IEST_TXT'
      'EMIT_CNPJ_TXT=EMIT_CNPJ_TXT'
      'DEST_CNPJ_CPF_TXT=DEST_CNPJ_CPF_TXT'
      'DEST_ENDERECO=DEST_ENDERECO'
      'DEST_CEP_TXT=DEST_CEP_TXT'
      'DEST_FONE_TXT=DEST_FONE_TXT'
      'DEST_IE_TXT=DEST_IE_TXT'
      'TRANSPORTA_CNPJ_CPF_TXT=TRANSPORTA_CNPJ_CPF_TXT'
      'TRANSPORTA_IE_TXT=TRANSPORTA_IE_TXT'
      'DOC_SEM_VLR_JUR=DOC_SEM_VLR_JUR'
      'DOC_SEM_VLR_FIS=DOC_SEM_VLR_FIS'
      'ide_DSaiEnt_Txt=ide_DSaiEnt_Txt'
      'InfAdic_InfAdFisco=InfAdic_InfAdFisco'
      'ide_hSaiEnt=ide_hSaiEnt'
      'ide_dhCont=ide_dhCont'
      'ide_xJust=ide_xJust'
      'emit_CRT=emit_CRT'
      'dest_email=dest_email'
      'Vagao=Vagao'
      'Balsa=Balsa'
      'MODFRETE_TXT=MODFRETE_TXT'
      'DEST_XMUN_TXT=DEST_XMUN_TXT'
      'NFeNT2013_003LTT=NFeNT2013_003LTT'
      'InfCpl_totTrib=InfCpl_totTrib'
      'vTotTrib=vTotTrib')
    DataSet = QrA
    BCDToCurrency = False
    Left = 84
    Top = 200
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrACobr_Fat_NFat: TWideStringField
      FieldName = 'Cobr_Fat_NFat'
      Size = 60
    end
    object QrACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrAID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrAEMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IEST_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IEST_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_TXT'
      Size = 100
      Calculated = True
    end
    object QrADEST_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TXT'
      Size = 100
      Calculated = True
    end
    object QrADEST_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrADEST_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CEP_TXT'
      Calculated = True
    end
    object QrADEST_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_FONE_TXT'
      Size = 50
      Calculated = True
    end
    object QrADEST_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrATRANSPORTA_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrADOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrADOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
    object QrAide_DSaiEnt_Txt: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_DSaiEnt_Txt'
      Size = 10
      Calculated = True
    end
    object QrAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrAMODFRETE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MODFRETE_TXT'
      Calculated = True
    end
    object QrADEST_XMUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_XMUN_TXT'
      Size = 60
      Calculated = True
    end
    object QrANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrAInfCpl_totTrib: TWideStringField
      FieldName = 'InfCpl_totTrib'
      Size = 255
    end
    object QrAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object QrI: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrIAfterScroll
    OnCalcFields = QrICalcFields
    SQL.Strings = (
      'SELECT *, pgt.Fracio'
      'FROM nfeitsi nii'
      'LEFT JOIN gragrux ggx ON ggx.Controle = nii.prod_cProd'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo = gg1.PrdGrupTip'
      'WHERE nii.FatID=:P0'
      'AND nii.FatNum=:P1'
      'AND nii.Empresa=:P2')
    Left = 56
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrIFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrIprod_qCom_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'prod_qCom_TXT'
      Size = 50
      Calculated = True
    end
  end
  object frxDsI: TfrxDBDataset
    UserName = 'frxDsI'
    CloseDataSource = False
    FieldAliases.Strings = (
      'prod_cProd=prod_cProd'
      'prod_cEAN=prod_cEAN'
      'prod_xProd=prod_xProd'
      'prod_NCM=prod_NCM'
      'prod_EXTIPI=prod_EXTIPI'
      'prod_genero=prod_genero'
      'prod_CFOP=prod_CFOP'
      'prod_uCom=prod_uCom'
      'prod_qCom=prod_qCom'
      'prod_vUnCom=prod_vUnCom'
      'prod_vProd=prod_vProd'
      'prod_cEANTrib=prod_cEANTrib'
      'prod_uTrib=prod_uTrib'
      'prod_qTrib=prod_qTrib'
      'prod_vUnTrib=prod_vUnTrib'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'Tem_IPI=Tem_IPI'
      'nItem=nItem'
      'Fracio=Fracio'
      'prod_qCom_TXT=prod_qCom_TXT')
    DataSet = QrI
    BCDToCurrency = False
    Left = 84
    Top = 228
  end
  object frxDsN: TfrxDBDataset
    UserName = 'frxDsN'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nItem=nItem'
      'ICMS_Orig=ICMS_Orig'
      'ICMS_CST=ICMS_CST'
      'ICMS_modBC=ICMS_modBC'
      'ICMS_pRedBC=ICMS_pRedBC'
      'ICMS_vBC=ICMS_vBC'
      'ICMS_pICMS=ICMS_pICMS'
      'ICMS_vICMS=ICMS_vICMS'
      'ICMS_modBCST=ICMS_modBCST'
      'ICMS_pMVAST=ICMS_pMVAST'
      'ICMS_pRedBCST=ICMS_pRedBCST'
      'ICMS_vBCST=ICMS_vBCST'
      'ICMS_pICMSST=ICMS_pICMSST'
      'ICMS_vICMSST=ICMS_vICMSST'
      'ICMS_CSOSN=ICMS_CSOSN')
    DataSet = QrN
    BCDToCurrency = False
    Left = 84
    Top = 256
  end
  object QrN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      DisplayFormat = '000.###'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
  end
  object QrO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object frxDsO: TfrxDBDataset
    UserName = 'frxDsO'
    CloseDataSource = False
    DataSet = QrO
    BCDToCurrency = False
    Left = 84
    Top = 284
  end
  object frxDsY: TfrxDBDataset
    UserName = 'frxDsY'
    CloseDataSource = False
    DataSet = QrY
    BCDToCurrency = False
    Left = 84
    Top = 312
  end
  object QrY: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrYAfterOpen
    SQL.Strings = (
      'SELECT nDup, dVenc, vDup'
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrYnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrYdVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrYvDup: TFloatField
      FieldName = 'vDup'
    end
  end
  object QrNFeYIts: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrNFeYItsCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfeyits'
      'ORDER BY dVenc1')
    Left = 56
    Top = 340
    object QrNFeYItsSeq1: TIntegerField
      FieldName = 'Seq1'
    end
    object QrNFeYItsnDup1: TWideStringField
      FieldName = 'nDup1'
      Size = 60
    end
    object QrNFeYItsdVenc1: TDateField
      FieldName = 'dVenc1'
    end
    object QrNFeYItsvDup1: TFloatField
      FieldName = 'vDup1'
    end
    object QrNFeYItsSeq2: TIntegerField
      FieldName = 'Seq2'
    end
    object QrNFeYItsnDup2: TWideStringField
      FieldName = 'nDup2'
      Size = 60
    end
    object QrNFeYItsdVenc2: TDateField
      FieldName = 'dVenc2'
    end
    object QrNFeYItsvDup2: TFloatField
      FieldName = 'vDup2'
    end
    object QrNFeYItsSeq3: TIntegerField
      FieldName = 'Seq3'
    end
    object QrNFeYItsnDup3: TWideStringField
      FieldName = 'nDup3'
      Size = 60
    end
    object QrNFeYItsdVenc3: TDateField
      FieldName = 'dVenc3'
    end
    object QrNFeYItsvDup3: TFloatField
      FieldName = 'vDup3'
    end
    object QrNFeYItsSeq4: TIntegerField
      FieldName = 'Seq4'
    end
    object QrNFeYItsnDup4: TWideStringField
      FieldName = 'nDup4'
      Size = 60
    end
    object QrNFeYItsdVenc4: TDateField
      FieldName = 'dVenc4'
    end
    object QrNFeYItsvDup4: TFloatField
      FieldName = 'vDup4'
    end
    object QrNFeYItsSeq5: TIntegerField
      FieldName = 'Seq5'
    end
    object QrNFeYItsnDup5: TWideStringField
      FieldName = 'nDup5'
      Size = 60
    end
    object QrNFeYItsdVenc5: TDateField
      FieldName = 'dVenc5'
    end
    object QrNFeYItsvDup5: TFloatField
      FieldName = 'vDup5'
    end
    object QrNFeYItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrNFeYItsxVenc1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc1'
      Size = 10
      Calculated = True
    end
    object QrNFeYItsxVenc2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc2'
      Size = 10
      Calculated = True
    end
    object QrNFeYItsxVenc3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc3'
      Size = 10
      Calculated = True
    end
    object QrNFeYItsxVenc4: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc4'
      Size = 10
      Calculated = True
    end
    object QrNFeYItsxVenc5: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc5'
      Size = 10
      Calculated = True
    end
  end
  object frxDsNFeYIts: TfrxDBDataset
    UserName = 'frxDsNFeYIts'
    CloseDataSource = False
    DataSet = QrNFeYIts
    BCDToCurrency = False
    Left = 84
    Top = 340
  end
  object frxDsXvol: TfrxDBDataset
    UserName = 'frxDsXvol'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 84
    Top = 368
  end
  object QrXVol: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrXVolAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrXVolpesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrXVolpesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object QrV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrVnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsV: TfrxDBDataset
    UserName = 'frxDsV'
    CloseDataSource = False
    DataSet = QrV
    BCDToCurrency = False
    Left = 84
    Top = 396
  end
  object QrNFeXMLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 332
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeXMLICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrNFeXMLIID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrNFeXMLIValor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrNFeXMLIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeXMLIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeXMLICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsNFeXMLI: TfrxDBDataset
    UserName = 'frxDsNFeXMLI'
    CloseDataSource = False
    DataSet = QrNFeXMLI
    BCDToCurrency = False
    Left = 360
    Top = 188
  end
  object frxCampos: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 388
    Top = 188
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFeCabA
        DataSetName = 'frxDsNFeCabA'
      end
      item
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 58.582701570000000000
        Top = 79.370130000000000000
        Width = 699.213050000000000000
        object Shape7: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 151.181200000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Campos preenchidos no XML')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NF N'#186' [frxDsNFeCabA."ide_nNF"]   -   Chave NF-e: [frxDsNFeCabA."' +
              'Id"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd.'
            'ID')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pai'
            'Campo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.811070000000000000
          Top = 37.795300000000000000
          Width = 627.401980000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do campo'
            'Valor da campo no XML')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 20.787401570000000000
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
        RowCount = 0
        object Memo301: TfrxMemoView
          Width = 18.897650000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Codigo"]'
            '[frxDsNFeXMLI."ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Pai"]'
            '[frxDsNFeXMLI."Campo"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 71.811070000000000000
          Width = 627.401980000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Descricao"]'
            '[frxDsNFeXMLI."Valor"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeCabA."NO_Cli"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 380
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_NFe: TWideMemoField
      FieldName = 'XML_NFe'
      Origin = 'nfearq.XML_NFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object PMArq: TPopupMenu
    OnPopup = PMArqPopup
    Left = 500
    Top = 424
    object NFe1: TMenuItem
      Caption = '&NFe'
      Enabled = False
      OnClick = NFe1Click
    end
    object Autorizao1: TMenuItem
      Caption = '&Autoriza'#231#227'o'
      Enabled = False
      OnClick = Autorizao1Click
    end
    object Cancelamento2: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
      OnClick = Cancelamento2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DiretriodoarquivoXML1: TMenuItem
      Caption = '&Diret'#243'rio do arquivo XML'
      OnClick = DiretriodoarquivoXML1Click
    end
  end
  object frxListaNFes: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaNFesGetValue
    Left = 208
    Top = 340
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
      end
      item
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
      end
      item
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 495.118430000000000000
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 260.787570000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 86.929190000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 132.283550000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 888.189550000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 623.622450000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_100."Ordem"'
        object Memo1: TfrxMemoView
          Top = 22.677179999999990000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677179999999990000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677179999999990000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 200.315090000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 381.732530000000000000
          Top = 22.677179999999990000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 445.984540000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 506.457020000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 634.961040000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 702.992580000000000000
          Top = 22.677179999999990000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Top = 3.779529999999994000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 888.189550000000000000
          Top = 22.677179999999990000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 975.118740000000000000
          Top = 22.677179999999990000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          Left = 767.244590000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          Left = 827.717070000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677177560000000000
        Top = 298.582870000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Width = 132.283501180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 888.189550000000000000
          Width = 102.047261180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 132.283550000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 343.937230000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_101."Ordem"'
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 638.740570000000000000
          Top = 22.677180000000020000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 487.559370000000000000
          Top = 22.677180000000020000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000020000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 464.882190000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 291.023810000000000000
          Top = 22.677180000000020000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 525.354670000000100000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 442.205010000000000000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 563.149970000000100000
        Width = 990.236860000000000000
      end
    end
  end
  object QrNFe_100: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_100'
      'ORDER BY ide_nNF')
    Left = 208
    Top = 368
    object QrNFe_100Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_100ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_100ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_100ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_100ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrNFe_100ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFe_100ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFe_100ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFe_100ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFe_100ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFe_100ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFe_100ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrNFe_100ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFe_100ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFe_100ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFe_100ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFe_100NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_100NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_100ide_NatOP: TWideStringField
      FieldName = 'ide_NatOP'
      Size = 60
    end
    object QrNFe_100PesoB: TFloatField
      FieldName = 'PesoB'
    end
    object QrNFe_100PesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrNFe_100nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFe_100marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFe_100esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFe_100qVol: TFloatField
      FieldName = 'qVol'
    end
  end
  object frxDsNFe_100: TfrxDBDataset
    UserName = 'frxDsNFe_100'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_dEmi=ide_dEmi'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vNF=ICMSTot_vNF'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ICMSTot_vPIS=ICMSTot_vPIS'
      'ICMSTot_vCOFINS=ICMSTot_vCOFINS'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'ide_NatOP=ide_NatOP'
      'PesoB=PesoB'
      'PesoL=PesoL'
      'nVol=nVol'
      'marca=marca'
      'esp=esp'
      'qVol=qVol')
    DataSet = QrNFe_100
    BCDToCurrency = False
    Left = 236
    Top = 368
  end
  object QrNFe_XXX: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrNFe_XXXCalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_xxx'
      'ORDER BY Ordem, ide_nNF;')
    Left = 208
    Top = 424
    object QrNFe_XXXOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_XXXcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFe_XXXide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_XXXide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_XXXide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_XXXide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_XXXide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_XXXNOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_XXXNOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_XXXStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFe_XXXMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_XXXNOME_ORDEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_ORDEM'
      Size = 50
      Calculated = True
    end
  end
  object frxDsNFe_XXX: TfrxDBDataset
    UserName = 'frxDsNFe_XXX'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'cStat=cStat'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Status=Status'
      'Motivo=Motivo'
      'NOME_ORDEM=NOME_ORDEM')
    DataSet = QrNFe_XXX
    BCDToCurrency = False
    Left = 236
    Top = 424
  end
  object QrNFe_101: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrNFe_101CalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_101'
      'ORDER BY ide_nNF')
    Left = 208
    Top = 396
    object QrNFe_101Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_101ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_101ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_101ide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_101ide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_101ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_101NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_101NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_101Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFe_101infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFe_101infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFe_101Motivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_101Id_TXT: TWideStringField
      DisplayWidth = 100
      FieldKind = fkCalculated
      FieldName = 'Id_TXT'
      Size = 100
      Calculated = True
    end
  end
  object frxDsNFe_101: TfrxDBDataset
    UserName = 'frxDsNFe_101'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Id=Id'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'Motivo=Motivo'
      'Id_TXT=Id_TXT')
    DataSet = QrNFe_101
    BCDToCurrency = False
    Left = 236
    Top = 396
  end
  object frxA4A_002: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 43390.832654745370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;                                      '
      ''
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsN."ICMS_CSOSN"> > 0 then'
      '  begin'
      
        '    MeCSTChild1.Memo.Text  := '#39'CSOSN'#39';                          ' +
        '                          '
      
        '    MeCSTHeader1.Memo.Text := '#39'CSOSN'#39';                          ' +
        '                                      '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'000'#39', <frxDsN."ICMS_C' +
        'SOSN">);'
      '  end else'
      '  begin              '
      
        '    MeCSTChild1.Memo.Text  := '#39'CST'#39';                            ' +
        '                        '
      
        '    MeCSTHeader1.Memo.Text := '#39'CST'#39';                            ' +
        '                                    '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'0'#39', <frxDsN."ICMS_Ori' +
        'g">) + FormatFloat('#39'00'#39', <frxDsN."ICMS_CST">);'
      '  end;                  '
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if (<LogoNFeExiste> = True) and (<VAR_NFE_RECEBIDA> = False) t' +
        'hen'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      
        '    Picture2.Visible := True;                                   ' +
        '     '
      
        '    Picture3.Visible := True;                                   ' +
        '              '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      
        '    Picture2.Visible := False;                                  ' +
        '      '
      
        '    Picture3.Visible := False;                                  ' +
        '               '
      '  end;'
      '  //        '
      '  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      'end.')
    OnGetValue = frxA4A_002GetValue
    Left = 56
    Top = 172
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsY
        DataSetName = 'frxDsY'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O MUNICIPAL')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."emit_IM"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo106: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR TOTAL DOS SERVI'#199'OS')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vServ">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo108: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'BASE DE C'#193'LCULO DO ISSQN')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vBC">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo110: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR DO ISSQN')
        ParentFont = False
      end
      object Memo111: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vISS">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        FlowTo = frxA4A_002.MeFlowToHid
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[frxDsA."InfAdic_InfAdFisco"]'
          '[frxDsA."InfAdic_InfCpl"]')
        ParentFont = False
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.472438500000000000
        Top = 16.000000000000000000
        Width = 793.701300000000000000
        object Memo119: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Top = 15.118120000000010000
          Width = 733.228331810000000000
          Height = 66.141568740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_03]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Top = 16.220470000000000000
          Width = 563.149577010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'RECEBEMOS DE [frxDsA."emit_xNome"] OS PRODUTOS E/OU SERVI'#199'OS CON' +
              'STANTES DA NOTA FISCAL ELETR'#212'NICA INDICADA AO LADO')
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.220470000000000000
          Width = 170.078740160000000000
          Height = 64.251968500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 48.346454249999990000
          Width = 154.960629920000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DE RECEBIMENTO')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 185.196869920000000000
          Top = 48.346454249999990000
          Width = 408.188944650000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.118120000000000000
          Width = 170.078850000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 593.275981100000000000
          Top = 31.992135750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 615.953161100000000000
          Top = 31.992135750000000000
          Width = 147.401670000000000000
          Height = 15.874015750000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 593.496451100000000000
          Top = 48.244104250000010000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 631.291751100000000000
          Top = 48.244104250000010000
          Width = 128.504020000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 32.126011100000000000
          Top = 24.811033380000000000
          Width = 559.370244719999900000
          Height = 21.543307090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]'
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
        end
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 317.480314960630000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 355.275590551181100000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 381.732283460000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 404.409448820000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 442.204724410000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 491.338900000000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 536.692913390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 582.047244090000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 627.401574800000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 672.755905510000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 718.110236220000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 740.787401570000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 763.464566930000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 116.000000000000000000
        Width = 793.701300000000000000
        object Memo12: TfrxMemoView
          Tag = 2
          Left = 30.236220470000000000
          Top = 231.275590550000000000
          Width = 793.701300000000000000
          Height = 559.370440000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -104
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_01]')
          ParentFont = False
          Rotation = 45
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Width = 733.228344020000000000
          Height = 232.440888740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -117
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000010000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236220469999980000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 457.323130000000000000
          Top = 168.566929130000000000
          Width = 306.141654170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000010000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000010000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo9: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000010000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000050000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000010000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897639999990000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811020000000000
          Width = 192.756030000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968500000000000
          Width = 306.141732280000000000
          Height = 56.314960630000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.322834650000000000
          Top = 176.125940310000000000
          Width = 306.141732280000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000010000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000020000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360629999970000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo200: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 132.284294490000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 419.527559060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_B09_Titu: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 12.094485749999990000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 419.527559060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Me_B09_Dado: TfrxMemoView
          Tag = 1
          Left = 651.968931100000000000
          Top = 20.165342130000000000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 351.496062990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 351.496062990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 44.220469999999970000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 651.968494170000000000
          Top = 52.291326379999990000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dSaiEnt_Txt"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 76.346454250000030000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'HORA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 652.346446930000000000
          Top = 84.417310629999970000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordBreak = True
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 268.724409450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 268.724409450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 110.118381180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 122.835194880000000000
          Width = 58.582677165354330000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 88.818897640000000000
          Top = 122.835194880000000000
          Width = 39.685039370000000000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 122.835194880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354330000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 236.220472440000000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354330000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 383.622047240000000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354330000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 531.023622050000000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354330000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 678.425196850000000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818899999999000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 544.252320000000100000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 604.724409450000000000
          Top = 15.874015750000010000
          Width = 158.740167240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Tag = 1
          Left = 604.724409450000000000
          Top = 25.322834640000000000
          Width = 158.740167240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000010000
          Width = 143.622047244095000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 173.858267720000000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 173.858267720000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 317.480314960630000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          Left = 317.480314960000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBCST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 461.102362204724000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Tag = 1
          Left = 461.102362200000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 57.448818899999990000
          Width = 128.503941890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 80.125984249999990000
          Width = 211.653543310000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TRANSPORTADOR / VOLUMES TRANSPORTADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 96.000000000000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 105.448818900000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 96.000000000000000000
          Width = 306.141732283465000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 105.448818900000000000
          Width = 306.141732280000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FRETE POR CONTA')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 105.448818900000000000
          Width = 90.708658980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."MODFRETE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Tag = 1
          Left = 430.866141732283000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO ANTT')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          Left = 430.866141730000000000
          Top = 105.448818900000000000
          Width = 94.488188980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_RNTC"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 96.000000000000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 105.448818900000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 128.125984250000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 137.574803150000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."Transporta_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 128.125984250000000000
          Width = 340.157477870000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 137.574803150000000000
          Width = 340.157477870000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XEnder"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 128.125984250000000000
          Width = 230.551164020000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 137.574803150000000000
          Width = 230.551164020000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XMun"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 128.125984250000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 137.574803150000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_IE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 160.252373700000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO BRUTO')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 169.701192600000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOB>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 160.252373700000000000
          Width = 109.606299212598000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 169.701192600000000000
          Width = 109.606299210000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <VARF_XVOL_QVOL>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 160.252373700000000000
          Width = 113.385826771654000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ESP'#201'CIE')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 169.701192600000000000
          Width = 113.385826770000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_ESP]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo97: TfrxMemoView
          Tag = 1
          Left = 253.228346456693000000
          Top = 160.252373700000000000
          Width = 117.165354330000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MARCA')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Tag = 1
          Left = 253.228346460000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_MARCA]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo99: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 160.252373700000000000
          Width = 154.960612830000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 169.701192600000000000
          Width = 154.960612830000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_NVOL]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo101: TfrxMemoView
          Tag = 1
          Left = 646.299212598425000000
          Top = 160.252373700000000000
          Width = 117.165354330709000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Tag = 1
          Left = 646.299212600000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOL>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Tag = 1
          Left = 525.354330708661000000
          Top = 96.000363699999970000
          Width = 75.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PLACA DO VE'#205'CULO')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Tag = 1
          Left = 525.354330710000000000
          Top = 105.449182600000000000
          Width = 75.590551180000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_Placa"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 192.377952760000000000
          Width = 151.181102360000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DADOS DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 510.236550000000000000
        Width = 793.701300000000000000
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 58.582677165354330000
          Height = 11.338582680000000000
          DataField = 'nDup1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup1"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 88.818897637795280000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataField = 'xVenc1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc1"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup1">)]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Tag = 1
          Left = 177.637910000000000000
          Width = 58.582677165354330000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup2"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Tag = 1
          Left = 236.220472440944900000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc2"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Tag = 1
          Left = 275.905709530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup2">)]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Tag = 1
          Left = 325.039580000000000000
          Width = 58.582677165354330000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup3"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Tag = 1
          Left = 383.622047244094500000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc3"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Tag = 1
          Left = 423.307379530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup3">)]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Tag = 1
          Left = 472.441250000000000000
          Width = 58.582677165354330000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup4"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Tag = 1
          Left = 531.023622047244100000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc4"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Tag = 1
          Left = 570.709049530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup4">)]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Tag = 1
          Left = 619.842920000000000000
          Width = 58.582677165354330000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup5"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Tag = 1
          Left = 678.425196850393700000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc5"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Tag = 1
          Left = 718.110719530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup5">)]')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 816.378480000000000000
        Width = 793.701300000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          DataField = 'prod_cProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Me_prod_xProd: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 249.448843310000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"] [frxDsV."InfAdProd"]')
          ParentFont = False
        end
        object Me_prod_NCM: TfrxMemoView
          Tag = 1
          Left = 317.480346690000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_NCM"]')
          ParentFont = False
        end
        object Me_ICMS_CST: TfrxMemoView
          Tag = 1
          Left = 355.275627170000000000
          Width = 26.456695350000000000
          Height = 32.125984250000000000
          OnBeforePrint = 'Me_ICMS_CSTOnBeforePrint'
          DataSet = frxDsN
          DataSetName = 'frxDsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsN."ICMS_CST"]')
          ParentFont = False
        end
        object Me_prod_CFOP: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_CFOP"]')
          ParentFont = False
        end
        object Me_prod_uCom: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Me_prod_qCom: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom_TXT"]')
          ParentFont = False
        end
        object Me_prod_vUnCom: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39', <frxDsI."prod_vUnCom">)]')
          ParentFont = False
        end
        object Me_prod_vProd: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsI."prod_vProd">)]')
          ParentFont = False
        end
        object Me_ICMS_vBC: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vBC">)]')
          ParentFont = False
        end
        object Me_ICMS_vICMS: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vICMS">)]')
          ParentFont = False
        end
        object Me_IPI_vIPI: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsO."IPI_vIPI">)]')
          ParentFont = False
        end
        object Me_ICMS_pICMS: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsN."ICMS_pICMS">)]')
          ParentFont = False
        end
        object Me_IPI_pIPI: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsO."IPI_pIPI">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984249999990000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 249.448843310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 317.480346690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTHeader1: TfrxMemoView
          Tag = 1
          Left = 355.275627170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 313.700990000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 249.448843310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 317.480346690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTChild1: TfrxMemoView
          Tag = 1
          Left = 355.275627170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo203: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode3: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo204: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897640000000000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo210: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo212: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo215: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo217: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo250: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo259: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode4: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559059999999988000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo220: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode5: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo223: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897637795280000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo231: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo234: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo239: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo248: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode6: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture3: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236239999999990000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object QrNFeEveRRet: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeEveRRetCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfeeverret'
      'WHERE ret_chNFe="41130502717861000110550090000000061780170615" '
      'ORDER BY ret_dhRegEvento')
    Left = 172
    Top = 548
    object QrNFeEveRRetControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRRetSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
    object QrNFeEveRRetret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrNFeEveRRetret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrNFeEveRRetret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrNFeEveRRetret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrNFeEveRRetret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrNFeEveRRetret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRRetret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrNFeEveRRetret_chNFe: TWideStringField
      FieldName = 'ret_chNFe'
      Size = 44
    end
    object QrNFeEveRRetret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrNFeEveRRetret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrNFeEveRRetret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrNFeEveRRetret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrNFeEveRRetret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrNFeEveRRetret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrNFeEveRRetret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRRetret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrNFeEveRRetret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrNFeEveRRetNO_EVENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EVENTO'
      Size = 255
      Calculated = True
    end
  end
  object DsNFeEveRRet: TDataSource
    DataSet = QrNFeEveRRet
    Left = 200
    Top = 548
  end
  object PMEventos: TPopupMenu
    OnPopup = PMEventosPopup
    Left = 44
    Top = 548
    object CorrigeManifestao1: TMenuItem
      Caption = 'Corrige Manifesta'#231#227'o'
      Enabled = False
      OnClick = CorrigeManifestao1Click
    end
  end
  object PMcSitConf: TPopupMenu
    Left = 528
    Top = 140
    object Manifestar1: TMenuItem
      Caption = 'Manifestar'
      OnClick = Manifestar1Click
    end
  end
  object frxA4A_003: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 43390.831290891200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;        '
      ''
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsN."ICMS_CSOSN"> > 0 then'
      '  begin'
      
        '    MeCSTChild1.Memo.Text  := '#39'CSOSN'#39';                          ' +
        '                                          '
      
        '    MeCSTHeader1.Memo.Text := '#39'CSOSN'#39';                          ' +
        '          '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'000'#39', <frxDsN."ICMS_C' +
        'SOSN">);'
      '  end else'
      '  begin              '
      
        '    MeCSTChild1.Memo.Text  := '#39'CST'#39';                            ' +
        '                                        '
      
        '    MeCSTHeader1.Memo.Text := '#39'CST'#39';                            ' +
        '        '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'0'#39', <frxDsN."ICMS_Ori' +
        'g">) + FormatFloat('#39'00'#39', <frxDsN."ICMS_CST">);'
      '  end;                  '
      'end;'
      ''
      'begin'
      
        '  if (<LogoNFeExiste> = True) and (<VAR_NFE_RECEBIDA> = False) t' +
        'hen'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      
        '    Picture2.Visible := True;                                   ' +
        '     '
      
        '    Picture3.Visible := True;                                   ' +
        '              '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      
        '    Picture2.Visible := False;                                  ' +
        '      '
      
        '    Picture3.Visible := False;                                  ' +
        '               '
      '  end;'
      '  //        '
      '  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      'end.')
    OnGetValue = frxA4A_002GetValue
    Left = 84
    Top = 172
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsY
        DataSetName = 'frxDsY'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo12: TfrxMemoView
        Tag = 2
        Left = 30.236240000000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        Height = 559.370440000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -104
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VAR_MSG_01]')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O MUNICIPAL')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."emit_IM"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo106: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR TOTAL DOS SERVI'#199'OS')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vServ">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo108: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'BASE DE C'#193'LCULO DO ISSQN')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vBC">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo110: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR DO ISSQN')
        ParentFont = False
      end
      object Memo111: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vISS">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[frxDsA."InfAdic_InfAdFisco"]'
          '[frxDsA."InfAdic_InfCpl"]'
          '[frxDsA."InfCpl_totTrib"]')
        ParentFont = False
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.472438500000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo119: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Top = 15.118120000000000000
          Width = 733.228331810000000000
          Height = 66.141568740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_03]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Top = 16.220470000000000000
          Width = 563.149577010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'RECEBEMOS DE [frxDsA."emit_xNome"] OS PRODUTOS E/OU SERVI'#199'OS CON' +
              'STANTES DA NOTA FISCAL ELETR'#212'NICA INDICADA AO LADO')
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.220470000000000000
          Width = 170.078740160000000000
          Height = 64.251968500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 48.346454249999990000
          Width = 154.960629920000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DE RECEBIMENTO')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 185.196869920000000000
          Top = 48.346454249999990000
          Width = 408.188944650000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.118120000000000000
          Width = 170.078850000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 593.275981100000000000
          Top = 31.992135750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 615.953161100000000000
          Top = 31.992135750000000000
          Width = 147.401670000000000000
          Height = 15.874015750000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 593.496451100000000000
          Top = 48.244104250000010000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 631.291751100000000000
          Top = 48.244104250000010000
          Width = 128.504020000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 32.126011100000000000
          Top = 24.811033380000000000
          Width = 559.370244720000000000
          Height = 21.543307090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]'
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
        end
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 272.125984251969000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 309.921259842519700000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 336.377923460000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 359.055088820000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 396.850364410000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 445.984540000000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 491.338553390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 536.692884090000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 582.047214800000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 627.401545510000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 672.755876220000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 695.433041570000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 718.110206930000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 120.944960000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Width = 733.228331810000000000
          Height = 232.440888740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -117
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511749999999990000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236220469999990000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 457.323130000000000000
          Top = 168.566929130000000000
          Width = 306.141654170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511749999999990000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511749999999990000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338589999999990000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo9: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897637795300000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811020000000000
          Width = 192.756030000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968500000000000
          Width = 306.141732280000000000
          Height = 56.314960630000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.322834650000000000
          Top = 176.125940310000000000
          Width = 306.141732280000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409399999999990000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000010000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360629999990000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000010000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo200: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
      object Line11: TfrxLineView
        Left = 763.465060000000000000
        Top = 695.433520000000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 132.284294490000000000
        Top = 304.000000000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 419.527559060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_B09_Titu: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 12.094485749999990000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 419.527559060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Me_B09_Dado: TfrxMemoView
          Tag = 1
          Left = 651.968931100000000000
          Top = 20.165342130000000000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 351.496062990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 351.496062990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 44.220469999999970000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 651.968494170000000000
          Top = 52.291326379999990000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dSaiEnt_Txt"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 76.346454250000030000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'HORA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 652.346446930000000000
          Top = 84.417310629999970000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordBreak = True
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 268.724409450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 268.724409450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 110.118381180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 122.835194880000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 88.818897637795300000
          Top = 122.835194880000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 122.835194880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 236.220472440945000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 383.622047244095000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 531.023622047244000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 678.425196850394000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 488.000000000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 15.874015750000010000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 25.322834640000000000
          Width = 128.503937007874000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000010000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 120.944881889764000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBCST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 57.448818899999990000
          Width = 128.503941890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 80.125984249999990000
          Width = 211.653543310000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TRANSPORTADOR / VOLUMES TRANSPORTADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 96.000000000000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 105.448818900000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 96.000000000000000000
          Width = 306.141732283465000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 105.448818900000000000
          Width = 306.141732280000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FRETE POR CONTA')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 105.448818900000000000
          Width = 90.708658980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."MODFRETE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Tag = 1
          Left = 430.866141732283000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO ANTT')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          Left = 430.866141730000000000
          Top = 105.448818900000000000
          Width = 94.488188980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_RNTC"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 96.000000000000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 105.448818900000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 128.125984250000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 137.574803150000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."Transporta_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 128.125984250000000000
          Width = 340.157477870000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 137.574803150000000000
          Width = 340.157477870000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XEnder"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 128.125984250000000000
          Width = 230.551164020000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 137.574803150000000000
          Width = 230.551164020000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XMun"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 128.125984250000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 137.574803150000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_IE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 160.252373700000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO BRUTO')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 169.701192600000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOB>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 160.252373700000000000
          Width = 109.606299212598000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 169.701192600000000000
          Width = 109.606299210000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <VARF_XVOL_QVOL>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 160.252373700000000000
          Width = 113.385826771654000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ESP'#201'CIE')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 169.701192600000000000
          Width = 113.385826770000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_ESP]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo97: TfrxMemoView
          Tag = 1
          Left = 253.228346456693000000
          Top = 160.252373700000000000
          Width = 117.165354330000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MARCA')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Tag = 1
          Left = 253.228346460000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_MARCA]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo99: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 160.252373700000000000
          Width = 154.960612830000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 169.701192600000000000
          Width = 154.960612830000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_NVOL]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo101: TfrxMemoView
          Tag = 1
          Left = 646.299212598425000000
          Top = 160.252373700000000000
          Width = 117.165354330709000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Tag = 1
          Left = 646.299212600000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOL>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Tag = 1
          Left = 525.354330708661000000
          Top = 96.000363699999970000
          Width = 75.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PLACA DO VE'#205'CULO')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Tag = 1
          Left = 525.354330710000000000
          Top = 105.449182600000000000
          Width = 75.590551180000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_Placa"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 192.377952760000000000
          Width = 151.181102360000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DADOS DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR APROXIMADO TRIBUTOS')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 25.322834650000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."vTotTrib"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 456.000000000000000000
        Width = 793.701300000000000000
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataField = 'nDup1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup1"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 88.818897637795300000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataField = 'xVenc1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc1"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup1">)]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Tag = 1
          Left = 177.637910000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup2"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Tag = 1
          Left = 236.220472440945000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc2"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Tag = 1
          Left = 275.905709530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup2">)]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Tag = 1
          Left = 325.039580000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup3"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Tag = 1
          Left = 383.622047240000000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc3"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Tag = 1
          Left = 423.307379530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup3">)]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Tag = 1
          Left = 472.441250000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup4"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Tag = 1
          Left = 531.023622050000000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc4"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Tag = 1
          Left = 570.709049530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup4">)]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Tag = 1
          Left = 619.842920000000000000
          Width = 58.582677165354300000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup5"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Tag = 1
          Left = 678.425196850394000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc5"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Tag = 1
          Left = 718.110719530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup5">)]')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 752.000000000000000000
        Width = 793.701300000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          DataField = 'prod_cProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Me_prod_xProd: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 204.094483310000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"] [frxDsV."InfAdProd"]')
          ParentFont = False
        end
        object Me_prod_NCM: TfrxMemoView
          Tag = 1
          Left = 272.125986690000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_NCM"]')
          ParentFont = False
        end
        object Me_ICMS_CST: TfrxMemoView
          Tag = 1
          Left = 309.921267170000000000
          Width = 26.456695350000000000
          Height = 32.125984250000000000
          OnBeforePrint = 'Me_ICMS_CSTOnBeforePrint'
          DataSet = frxDsN
          DataSetName = 'frxDsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsN."ICMS_CST"]')
          ParentFont = False
        end
        object Me_prod_CFOP: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_CFOP"]')
          ParentFont = False
        end
        object Me_prod_uCom: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Me_prod_qCom: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom_TXT"]')
          ParentFont = False
        end
        object Me_prod_vUnCom: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39', <frxDsI."prod_vUnCom">)]')
          ParentFont = False
        end
        object Me_prod_vProd: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsI."prod_vProd">)]')
          ParentFont = False
        end
        object Me_ICMS_vBC: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vBC">)]')
          ParentFont = False
        end
        object Me_ICMS_vICMS: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vICMS">)]')
          ParentFont = False
        end
        object Me_IPI_vIPI: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsO."IPI_vIPI">)]')
          ParentFont = False
        end
        object Me_ICMS_pICMS: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsN."ICMS_pICMS">)]')
          ParentFont = False
        end
        object Me_IPI_pIPI: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsO."IPI_pIPI">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984249999990000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsM."vTotTrib">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 716.000000000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 204.094483310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 272.125986690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTHeader1: TfrxMemoView
          Tag = 1
          Left = 309.921267170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VAL APROX'
            'TRIBUTOS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 268.000000000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 249.448843310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 317.480346690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTChild1: TfrxMemoView
          Tag = 1
          Left = 355.275627170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 16.000000000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo203: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode3: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo204: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897640000000000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811023622000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo210: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo212: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo215: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo217: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo250: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo259: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode4: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559059999999990000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo220: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode5: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000000000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo223: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897640000000000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811023622000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo231: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo234: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo239: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000000000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo248: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode6: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture3: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object QrM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrMiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object frxDsM: TfrxDBDataset
    UserName = 'frxDsM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'nItem=nItem'
      'iTotTrib=iTotTrib'
      'vTotTrib=vTotTrib')
    DataSet = QrM
    BCDToCurrency = False
    Left = 84
    Top = 424
  end
  object mySQLQuery1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'FatID'
    end
    object IntegerField2: TIntegerField
      FieldName = 'FatNum'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Empresa'
    end
    object IntegerField4: TIntegerField
      FieldName = 'nItem'
    end
    object SmallintField1: TSmallintField
      FieldName = 'iTotTrib'
    end
    object FloatField1: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDsM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'nItem=nItem'
      'iTotTrib=iTotTrib'
      'vTotTrib=vTotTrib')
    DataSet = mySQLQuery1
    BCDToCurrency = False
    Left = 84
    Top = 452
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 416
    Top = 188
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 468
    Top = 259
  end
  object frxListaNFesB: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaNFesGetValue
    Left = 236
    Top = 340
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
      end
      item
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
      end
      item
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 495.118430000000000000
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 302.362400000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 56.692950000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 918.425790000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'esp'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_100."esp"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'qVol'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."qVol"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PesoB'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."PesoB"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PesoL'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."PesoL"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 702.992580000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_100."Ordem"'
        object Memo34: TfrxMemoView
          Top = 3.779529999999994000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677177560000000000
        Top = 377.953000000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Width = 117.165381180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 918.425790000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo117: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo118: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."qVol">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo119: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."PesoB">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo121: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."PesoL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 423.307360000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_101."Ordem"'
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 638.740570000000000000
          Top = 22.677180000000020000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 487.559370000000000000
          Top = 22.677180000000020000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000020000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 483.779840000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 544.252320000000100000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 291.023810000000000000
          Top = 22.677180000000020000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 604.724800000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 521.575140000000100000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 642.520100000000000000
        Width = 990.236860000000000000
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 245.669450000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_100."ide_NatOP"'
        object Memo99: TfrxMemoView
          Top = 18.897650000000030000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo100: TfrxMemoView
          Left = 56.692950000000000000
          Top = 18.897650000000030000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo101: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000030000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo102: TfrxMemoView
          Left = 117.165430000000000000
          Top = 18.897650000000030000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo103: TfrxMemoView
          Left = 449.764070000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo104: TfrxMemoView
          Left = 177.637910000000000000
          Top = 18.897650000000030000
          Width = 45.354330708661420000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo105: TfrxMemoView
          Left = 222.992270000000000000
          Top = 18.897650000000030000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo106: TfrxMemoView
          Left = 268.346630000000000000
          Top = 18.897650000000030000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo107: TfrxMemoView
          Left = 313.700990000000000000
          Top = 18.897650000000030000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          Left = 359.055350000000000000
          Top = 18.897650000000030000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo109: TfrxMemoView
          Left = 404.409710000000000000
          Top = 18.897650000000030000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo110: TfrxMemoView
          Left = 502.677490000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo111: TfrxMemoView
          Left = 555.590910000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo112: TfrxMemoView
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsNFe_100."ide_NatOp"]')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 918.425790000000000000
          Top = 18.897650000000030000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo114: TfrxMemoView
          Left = 975.118740000000000000
          Top = 18.897650000000030000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo115: TfrxMemoView
          Left = 608.504330000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo116: TfrxMemoView
          Left = 661.417750000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
        object Memo122: TfrxMemoView
          Left = 714.331170000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Esp'#233'cie vol.')
          ParentFont = False
          WordWrap = False
        end
        object Memo123: TfrxMemoView
          Left = 767.244590000000000000
          Top = 18.897650000000030000
          Width = 45.354330708661420000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde volumes')
          ParentFont = False
          WordWrap = False
        end
        object Memo124: TfrxMemoView
          Left = 812.598950000000000000
          Top = 18.897650000000030000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso bruto')
          ParentFont = False
          WordWrap = False
        end
        object Memo125: TfrxMemoView
          Left = 865.512370000000000000
          Top = 18.897650000000030000
          Width = 52.913385826771650000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso l'#237'q.')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 990.236860000000000000
        object Memo1: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Width = 117.165381180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFeXMLI_
          DataSetName = 'frxDsNFeXMLI_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SUB TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo126: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo127: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."qVol">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo128: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."PesoB">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo129: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."PesoL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrNFeCabXVol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(qVol) qVol, esp, marca, '
      'nVol, SUM(PesoL) PesoL, SUM(PesoB) PesoB '
      'FROM nfecabxvol'
      'WHERE FatID<>0'
      'AND FatNum>0'
      'AND Empresa<>0')
    Left = 456
    Top = 572
    object QrNFeCabXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrNFeCabXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFeCabXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFeCabXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFeCabXVolPesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrNFeCabXVolPesoB: TFloatField
      FieldName = 'PesoB'
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 528
    Top = 344
    object AbrirNFenoportalnacional1: TMenuItem
      Caption = '&Abrir NF-e no portal nacional'
      OnClick = AbrirNFenoportalnacional1Click
    end
    object MostrarchavedeacessodaNFe1: TMenuItem
      Caption = '&Mostrar chave de acesso da NF-e'
      OnClick = MostrarchavedeacessodaNFe1Click
    end
    object EditadadosdeentradaEFDC1701: TMenuItem
      Caption = '&Edita dados de entrada (EFD C170)'
      OnClick = EditadadosdeentradaEFDC1701Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object CorrigesrienmeroNFerecebidaspesquisadas1: TMenuItem
      Caption = '&Corrige s'#233'rie/n'#250'mero NFe recebidas pesquisadas'
      OnClick = CorrigesrienmeroNFerecebidaspesquisadas1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Abrirfaturamento1: TMenuItem
      Caption = '&Abrir janela de faturamento'
      OnClick = Abrirfaturamento1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanDel01 = BtExclui
    Left = 436
    Top = 356
  end
  object frxA4A_003_R: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 43390.831290891200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;        '
      ''
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsN."ICMS_CSOSN"> > 0 then'
      '  begin'
      
        '    MeCSTChild1.Memo.Text  := '#39'CSOSN'#39';                          ' +
        '                                          '
      
        '    MeCSTHeader1.Memo.Text := '#39'CSOSN'#39';                          ' +
        '          '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'000'#39', <frxDsN."ICMS_C' +
        'SOSN">);'
      '  end else'
      '  begin              '
      
        '    MeCSTChild1.Memo.Text  := '#39'CST'#39';                            ' +
        '                                        '
      
        '    MeCSTHeader1.Memo.Text := '#39'CST'#39';                            ' +
        '        '
      
        '    Me_ICMS_CST.Memo.Text  := FormatFloat('#39'0'#39', <frxDsN."ICMS_Ori' +
        'g">) + FormatFloat('#39'00'#39', <frxDsN."ICMS_CST">);'
      '  end;                  '
      'end;'
      ''
      'begin'
      
        '  if (<LogoNFeExiste> = True) and (<VAR_NFE_RECEBIDA> = False) t' +
        'hen'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      
        '    Picture2.Visible := True;                                   ' +
        '     '
      
        '    Picture3.Visible := True;                                   ' +
        '              '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      
        '    Picture2.Visible := False;                                  ' +
        '      '
      
        '    Picture3.Visible := False;                                  ' +
        '               '
      '  end;'
      '  //        '
      '  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      'end.')
    OnGetValue = frxA4A_002GetValue
    Left = 220
    Top = 264
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsY
        DataSetName = 'frxDsY'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo12: TfrxMemoView
        Tag = 2
        Left = 30.236240000000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        Height = 559.370440000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -104
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[VAR_MSG_01]')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O MUNICIPAL')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."emit_IM"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo106: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR TOTAL DOS SERVI'#199'OS')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vServ">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo108: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'BASE DE C'#193'LCULO DO ISSQN')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vBC">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo110: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR DO ISSQN')
        ParentFont = False
      end
      object Memo111: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vISS">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[frxDsA."InfAdic_InfAdFisco"]'
          '[frxDsA."InfAdic_InfCpl"]'
          '[frxDsA."InfCpl_totTrib"]')
        ParentFont = False
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.472438500000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo119: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Top = 15.118120000000000000
          Width = 733.228331810000000000
          Height = 66.141568740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_03]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Top = 16.220470000000000000
          Width = 563.149577010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'RECEBEMOS DE [frxDsA."emit_xNome"] OS PRODUTOS E/OU SERVI'#199'OS CON' +
              'STANTES DA NOTA FISCAL ELETR'#212'NICA INDICADA AO LADO')
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.220470000000000000
          Width = 170.078740160000000000
          Height = 64.251968500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 48.346454249999990000
          Width = 154.960629920000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DE RECEBIMENTO')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 185.196869920000000000
          Top = 48.346454249999990000
          Width = 408.188944650000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.118120000000000000
          Width = 170.078850000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 593.275981100000000000
          Top = 31.992135750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 615.953161100000000000
          Top = 31.992135750000000000
          Width = 147.401670000000000000
          Height = 15.874015750000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 593.496451100000000000
          Top = 48.244104250000010000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 631.291751100000000000
          Top = 48.244104250000010000
          Width = 128.504020000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 32.126011100000000000
          Top = 24.811033380000000000
          Width = 559.370244720000000000
          Height = 21.543307090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]'
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
        end
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 272.125984251969000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 309.921259842519700000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 336.377923460000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 359.055088820000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 396.850364410000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 445.984540000000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 491.338553390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 536.692884090000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 582.047214800000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 627.401545510000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 672.755876220000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 695.433041570000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 718.110206930000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 120.944960000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236240000000000000
          Width = 733.228331810000000000
          Height = 232.440888740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -117
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_MSG_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511749999999990000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236220469999990000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 457.323130000000000000
          Top = 168.566929130000000000
          Width = 306.141654170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511749999999990000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511749999999990000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338589999999990000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo9: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897637795300000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811020000000000
          Width = 192.756030000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968500000000000
          Width = 306.141732280000000000
          Height = 56.314960630000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.322834650000000000
          Top = 176.125940310000000000
          Width = 306.141732280000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409399999999990000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000010000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360629999990000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000010000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo200: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
      object Line11: TfrxLineView
        Left = 763.465060000000000000
        Top = 695.433520000000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 132.284294490000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 419.527559060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_B09_Titu: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 12.094485749999990000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 419.527559060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Me_B09_Dado: TfrxMemoView
          Tag = 1
          Left = 651.968931100000000000
          Top = 20.165342130000000000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 351.496062990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 351.496062990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 44.220469999999970000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 651.968494170000000000
          Top = 52.291326379999990000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dSaiEnt_Txt"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 76.346454250000030000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'HORA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 652.346446930000000000
          Top = 84.417310629999970000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordBreak = True
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 268.724409450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 268.724409450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 110.118381180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 122.835194880000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 88.818897637795300000
          Top = 122.835194880000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 122.835194880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 236.220472440945000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 383.622047244095000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 531.023622047244000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 122.835475590000000000
          Width = 58.582677165354300000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 678.425196850394000000
          Top = 122.835475590000000000
          Width = 39.685039370000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCTO.')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 544.252320000000100000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 15.874015750000010000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 25.322834640000000000
          Width = 128.503937007874000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000010000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 120.944881889764000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBCST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 57.448818899999990000
          Width = 128.503941890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 80.125984249999990000
          Width = 211.653543310000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TRANSPORTADOR / VOLUMES TRANSPORTADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 96.000000000000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 105.448818900000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 96.000000000000000000
          Width = 306.141732283465000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 105.448818900000000000
          Width = 306.141732280000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FRETE POR CONTA')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 105.448818900000000000
          Width = 90.708658980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."MODFRETE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Tag = 1
          Left = 430.866141732283000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO ANTT')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          Left = 430.866141730000000000
          Top = 105.448818900000000000
          Width = 94.488188980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_RNTC"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 96.000000000000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 105.448818900000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 128.125984250000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 137.574803150000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."Transporta_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 128.125984250000000000
          Width = 340.157477870000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 137.574803150000000000
          Width = 340.157477870000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XEnder"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 128.125984250000000000
          Width = 230.551164020000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 137.574803150000000000
          Width = 230.551164020000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XMun"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 128.125984250000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 137.574803150000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_IE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 160.252373700000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO BRUTO')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 169.701192600000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOB>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 160.252373700000000000
          Width = 109.606299212598000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 169.701192600000000000
          Width = 109.606299210000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <VARF_XVOL_QVOL>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 160.252373700000000000
          Width = 113.385826771654000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ESP'#201'CIE')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 169.701192600000000000
          Width = 113.385826770000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_ESP]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo97: TfrxMemoView
          Tag = 1
          Left = 253.228346456693000000
          Top = 160.252373700000000000
          Width = 117.165354330000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MARCA')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Tag = 1
          Left = 253.228346460000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_MARCA]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo99: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 160.252373700000000000
          Width = 154.960612830000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 169.701192600000000000
          Width = 154.960612830000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_NVOL]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo101: TfrxMemoView
          Tag = 1
          Left = 646.299212598425000000
          Top = 160.252373700000000000
          Width = 117.165354330709000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Tag = 1
          Left = 646.299212600000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOL>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Tag = 1
          Left = 525.354330708661000000
          Top = 96.000363699999970000
          Width = 75.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PLACA DO VE'#205'CULO')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Tag = 1
          Left = 525.354330710000000000
          Top = 105.449182600000000000
          Width = 75.590551180000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_Placa"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 192.377952760000000000
          Width = 151.181102360000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DADOS DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR APROXIMADO TRIBUTOS')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 25.322834650000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."vTotTrib"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 510.236550000000000000
        Width = 793.701300000000000000
        DataSet = frxDsNFeYIts
        DataSetName = 'frxDsNFeYIts'
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataField = 'nDup1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup1"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 88.818897637795300000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataField = 'xVenc1'
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc1"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup1">)]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Tag = 1
          Left = 177.637910000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup2"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Tag = 1
          Left = 236.220472440945000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc2"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Tag = 1
          Left = 275.905709530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup2">)]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Tag = 1
          Left = 325.039580000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup3"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Tag = 1
          Left = 383.622047240000000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc3"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Tag = 1
          Left = 423.307379530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup3">)]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Tag = 1
          Left = 472.441250000000000000
          Width = 58.582677170000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup4"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Tag = 1
          Left = 531.023622050000000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc4"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Tag = 1
          Left = 570.709049530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup4">)]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Tag = 1
          Left = 619.842920000000000000
          Width = 58.582677165354300000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."nDup5"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Tag = 1
          Left = 678.425196850394000000
          Width = 39.685039370000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeYIts."xVenc5"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Tag = 1
          Left = 718.110719530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsNFeYIts
          DataSetName = 'frxDsNFeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsNFeYI' +
              'ts."vDup5">)]')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 816.378480000000000000
        Width = 793.701300000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          DataField = 'prod_cProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Me_prod_xProd: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 204.094483310000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"] [frxDsV."InfAdProd"]')
          ParentFont = False
        end
        object Me_prod_NCM: TfrxMemoView
          Tag = 1
          Left = 272.125986690000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_NCM"]')
          ParentFont = False
        end
        object Me_ICMS_CST: TfrxMemoView
          Tag = 1
          Left = 309.921267170000000000
          Width = 26.456695350000000000
          Height = 32.125984250000000000
          OnBeforePrint = 'Me_ICMS_CSTOnBeforePrint'
          DataSet = frxDsN
          DataSetName = 'frxDsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsN."ICMS_CST"]')
          ParentFont = False
        end
        object Me_prod_CFOP: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_CFOP"]')
          ParentFont = False
        end
        object Me_prod_uCom: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Me_prod_qCom: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom_TXT"]')
          ParentFont = False
        end
        object Me_prod_vUnCom: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39', <frxDsI."prod_vUnCom">)]')
          ParentFont = False
        end
        object Me_prod_vProd: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsI."prod_vProd">)]')
          ParentFont = False
        end
        object Me_ICMS_vBC: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vBC">)]')
          ParentFont = False
        end
        object Me_ICMS_vICMS: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vICMS">)]')
          ParentFont = False
        end
        object Me_IPI_vIPI: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsO."IPI_vIPI">)]')
          ParentFont = False
        end
        object Me_ICMS_pICMS: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsN."ICMS_pICMS">)]')
          ParentFont = False
        end
        object Me_IPI_pIPI: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsO."IPI_pIPI">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984249999990000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsM."vTotTrib">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 204.094483310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 272.125986690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTHeader1: TfrxMemoView
          Tag = 1
          Left = 309.921267170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VAL APROX'
            'TRIBUTOS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 313.700990000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 249.448843310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 317.480346690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object MeCSTChild1: TfrxMemoView
          Tag = 1
          Left = 355.275627170000000000
          Width = 26.456695350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo203: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode3: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo204: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897640000000000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811023622000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo210: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo212: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo215: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo217: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo250: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo259: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode4: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559059999999988000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo220: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode5: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo223: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 58.582677170000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeEnder_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 88.818897640000000000
          Width = 192.756030000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811023622000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo231: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo234: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo239: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo248: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode6: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture3: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object QrF: TmySQLQuery
    Database = Dmod.ZZDB
    Left = 320
    Top = 288
  end
  object QrG: TmySQLQuery
    Database = Dmod.ZZDB
    Left = 320
    Top = 336
  end
end
