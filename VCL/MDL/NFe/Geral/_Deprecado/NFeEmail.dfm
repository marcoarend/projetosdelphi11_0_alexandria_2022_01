object FmNFeEmail: TFmNFeEmail
  Left = 339
  Top = 185
  Caption = 'NFe-EMAIL-001 :: Envio de NF-e e DANFE por Email'
  ClientHeight = 617
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Grade: TStringGrid
      Left = 0
      Top = 0
      Width = 784
      Height = 116
      Align = alClient
      ColCount = 4
      DefaultColWidth = 24
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 0
      ColWidths = (
        24
        147
        190
        394)
    end
    object Panel3: TPanel
      Left = 0
      Top = 116
      Width = 784
      Height = 339
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 136
        Top = 4
        Width = 72
        Height = 13
        Caption = 'Chave da NFe:'
      end
      object Label2: TLabel
        Left = 444
        Top = 4
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object LaDANFE: TLabel
        Left = 8
        Top = 44
        Width = 98
        Height = 13
        Caption = 'Caminho da DANFE:'
        Enabled = False
      end
      object Label4: TLabel
        Left = 8
        Top = 84
        Width = 113
        Height = 13
        Caption = 'Caminho da NFe (XML):'
      end
      object Label5: TLabel
        Left = 8
        Top = 4
        Width = 118
        Height = 13
        Caption = 'CNPJ / CPF destinat'#225'rio:'
      end
      object SbDANFE: TSpeedButton
        Left = 756
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbDANFEClick
      end
      object SpeedButton2: TSpeedButton
        Left = 756
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object Label3: TLabel
        Left = 720
        Top = 4
        Width = 56
        Height = 13
        Caption = 'ID Controle:'
      end
      object Label7: TLabel
        Left = 8
        Top = 124
        Width = 46
        Height = 13
        Caption = 'Pr'#233'-email:'
      end
      object Label8: TLabel
        Left = 8
        Top = 164
        Width = 80
        Height = 13
        Caption = 'Nome do cliente:'
      end
      object Label9: TLabel
        Left = 672
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Vers'#227'o:'
      end
      object EdNFeId: TdmkEdit
        Left = 136
        Top = 20
        Width = 304
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtChaveNFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNFeStatus_TXT: TEdit
        Left = 492
        Top = 20
        Width = 177
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object EdNFeStatus: TdmkEdit
        Left = 444
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdNFeStatusChange
      end
      object EdDANFE: TdmkEdit
        Left = 8
        Top = 60
        Width = 749
        Height = 21
        Enabled = False
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNFeXML: TdmkEdit
        Left = 8
        Top = 100
        Width = 749
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCNPJ_CPF: TdmkEdit
        Left = 8
        Top = 20
        Width = 124
        Height = 21
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNFeIDCtrl: TdmkEdit
        Left = 720
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdNFeStatusChange
      end
      object CkProt: TCheckBox
        Left = 488
        Top = 184
        Width = 289
        Height = 17
        Caption = 'Incluir protocolo de autoriza'#231#227'o / cancelamento da NFe.'
        Checked = True
        State = cbChecked
        TabOrder = 7
      end
      object CkRecriarImagens: TCheckBox
        Left = 488
        Top = 167
        Width = 157
        Height = 17
        Caption = 'For'#231'ar recria'#231#227'o de imagens.'
        TabOrder = 8
      end
      object EdPreEmail: TdmkEditCB
        Left = 8
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdPreEmailChange
        DBLookupComboBox = CBPreEmail
        IgnoraDBLookupComboBox = False
      end
      object CBPreEmail: TdmkDBLookupComboBox
        Left = 64
        Top = 140
        Width = 712
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPre
        TabOrder = 10
        dmkEditCB = EdPreEmail
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 204
        Width = 784
        Height = 135
        ActivePage = TabSheet1
        Align = alBottom
        TabOrder = 11
        object TabSheet1: TTabSheet
          Caption = ' Mensagem texto '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGMsg: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 237
            Height = 107
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 53
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = DModG.DsPreEmMsg
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 53
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 90
                Visible = True
              end>
          end
          object MeMsg: TDBMemo
            Left = 237
            Top = 0
            Width = 539
            Height = 107
            Align = alClient
            DataField = 'Texto'
            DataSource = DModG.DsPreEmMsg
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Codifica'#231#227'o de imagens '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object MeEncode: TMemo
            Left = 0
            Top = 0
            Width = 776
            Height = 107
            Align = alClient
            TabOrder = 0
          end
        end
      end
      object Eddest_xNome: TEdit
        Left = 8
        Top = 180
        Width = 473
        Height = 21
        ReadOnly = True
        TabOrder = 12
      end
      object EdVersaoNFe: TdmkEdit
        Left = 672
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdNFeStatusChange
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'Envio de NF-e e DANFE por Email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'Envio de NF-e e DANFE por Email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'Envio de NF-e e DANFE por Email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 503
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 547
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Envia'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEnviaClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 135
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Entidades'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEntidadesClick
      end
    end
  end
  object QrEmails: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome, etm.EMail'
      'FROM entimail etm'
      'LEFT JOIN enticontat ent ON ent.Controle=etm.Controle'
      'WHERE etm.Codigo=:P0'
      'AND etm.EntiTipCto=:P1')
    Left = 8
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmailsEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
    object QrEmailsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 312
    Top = 244
  end
  object QrPre: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pem.Codigo, pem.Nome'
      'FROM preemail pem'
      'ORDER BY pem.Nome')
    Left = 256
    Top = 244
    object QrPreCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPre: TDataSource
    DataSet = QrPre
    Left = 284
    Top = 244
  end
  object IdSMTP1: TIdSMTP
    OnStatus = IdSMTP1Status
    SASLMechanisms = <>
    Left = 340
    Top = 244
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 368
    Top = 244
  end
end
