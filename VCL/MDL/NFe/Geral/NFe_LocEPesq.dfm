object FmNFe_LocEPesq: TFmNFe_LocEPesq
  Left = 339
  Top = 185
  Caption = 'NFe-PESQU-004 :: Pesquisa e Localiza'#231#227'o de NFe'
  ClientHeight = 574
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Pesquisa e Localiza'#231#227'o de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Pesquisa e Localiza'#231#227'o de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Pesquisa e Localiza'#231#227'o de NFe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 412
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 53
        Align = alTop
        TabOrder = 0
        object Label8: TLabel
          Left = 16
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label2: TLabel
          Left = 436
          Top = 12
          Width = 32
          Height = 13
          Caption = 'N'#176' NF:'
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 72
          Top = 28
          Width = 357
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 0
          dmkEditCB = EdEmpresa
          QryName = 'QrVSGerArt'
          QryCampo = 'Empresa'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdEmpresa: TdmkEditCB
          Left = 16
          Top = 28
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrVSGerArt'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object EdEmitente: TdmkEditCB
          Left = 520
          Top = 28
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmitente
          IgnoraDBLookupComboBox = False
        end
        object CBEmitente: TdmkDBLookupComboBox
          Left = 576
          Top = 28
          Width = 425
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsEmitente
          TabOrder = 3
          dmkEditCB = EdEmitente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkEmitente: TCheckBox
          Left = 520
          Top = 8
          Width = 69
          Height = 17
          Caption = 'Emitente:'
          TabOrder = 4
        end
        object EdNF: TdmkEdit
          Left = 436
          Top = 28
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object dmkDBGrid1: TdmkDBGridZTO
        Left = 0
        Top = 53
        Width = 1008
        Height = 216
        Align = alTop
        DataSource = DsNFeCabA
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'FatID'
            Title.Caption = 'Fat.ID'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_serie'
            Title.Caption = 'S'#233'rie'
            Width = 29
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nNF'
            Title.Caption = 'N'#186' NF'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dEmi'
            Title.Caption = 'Emiss'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhRecbto'
            Title.Caption = 'Data/hora fisco'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMSTot_vNF'
            Title.Caption = 'R$ NF'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMSTot_vProd'
            Title.Caption = 'R$ Prod.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Terceiro'
            Title.Caption = 'Destinat'#225'rio / remetente ou Emitente'
            Width = 179
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cStat'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Id'
            Title.Caption = 'Chave de acesso da NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LoteEnv'
            Title.Caption = 'Lote'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xMotivo'
            Title.Caption = 'Motivo do status'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'infCCe_nSeqEvento'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'CCe'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_cSitConf'
            Title.Caption = 'Manifesta'#231#227'o'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_natOp'
            Title.Caption = 'Natureza da opera'#231#227'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nProt'
            Title.Caption = 'N'#186' do protocolo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_tpNF'
            Title.Caption = 'tpNF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Emi'
            Title.Caption = 'Emitente'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Cli'
            Width = 240
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 292
        Width = 1008
        Height = 120
        Align = alBottom
        DataSource = DsNFeItsI
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'nItem'
            Title.Caption = 'Item'
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_cProd'
            Title.Caption = 'C'#243'digo fornecedor'
            Width = 119
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_xProd'
            Title.Caption = 'Nome do produto dado pelo fornecedor'
            Width = 235
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_uCom'
            Title.Caption = 'Un.Coml'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_qCom'
            Title.Caption = 'Qtd.Coml'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_uTrib'
            Title.Caption = 'Un.Trib'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_qTrib'
            Title.Caption = 'Qtd.Trib'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_vProd'
            Title.Caption = '$ Produto'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_vDesc'
            Title.Caption = '$ deconto'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_vFrete'
            Title.Caption = '$ Frete'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_vOutro'
            Title.Caption = '$ Outros'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'prod_vSeg'
            Title.Caption = '$ Seguro'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 460
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 504
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtPsq: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPsqClick
      end
      object BtLoc: TBitBtn
        Tag = 20
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtLocClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 476
    Top = 3
  end
  object QrEmitente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 164
    Top = 188
    object QrEmitenteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitenteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEmitente: TDataSource
    DataSet = QrEmitente
    Left = 164
    Top = 236
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFeCabAAfterOpen
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF, '
      'dest_xNome,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao, '
      'ide_tpEmis, infCanc_xJust, Status,'
      'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg, '
      'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc, '
      'ICMSTot_vBC, ICMSTot_vICMS,'
      'cSitNFe, cSitConf,'
      
        'ELT(cSitConf+2, "N'#227'o consultada", "Sem manifesta'#231#227'o", "Confirmad' +
        'a", '
      
        '"Desconhecida", "N'#227'o realizada", "Ci'#234'ncia", "? ? ?") NO_cSitConf' +
        ','
      'nfa.infCanc_dhRecbto, nfa.infCanc_nProt, nfa.NFeNT2013_003LTT'
      ''
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON '
      '  IF(nfa.dest_CNPJ<>'#39#39',nfa.dest_CNPJ=cli.CNPJ,'
      '  nfa.dest_CPF=cli.CPF)'
      'WHERE IDCtrl=0'
      'ORDER BY nfa.DataCad DESC, nfa.ide_nNF DESC'
      '')
    Left = 56
    Top = 184
    object QrNFeCabANO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeCabAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNFeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNFeCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrNFeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNFeCabANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrNFeCabANOME_tpNF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpNF'
      Size = 1
      Calculated = True
    end
    object QrNFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFeCabAcStat: TFloatField
      FieldName = 'cStat'
    end
    object QrNFeCabAcSitNFe: TSmallintField
      FieldName = 'cSitNFe'
    end
    object QrNFeCabAcSitConf: TSmallintField
      FieldName = 'cSitConf'
    end
    object QrNFeCabANO_cSitConf: TWideStringField
      FieldName = 'NO_cSitConf'
      Size = 16
    end
    object QrNFeCabANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrNFeCabANO_Emi: TWideStringField
      FieldName = 'NO_Emi'
      Size = 100
    end
    object QrNFeCabANO_Terceiro: TWideStringField
      FieldName = 'NO_Terceiro'
      Size = 100
    end
    object QrNFeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFeCabANO_Cli2: TWideStringField
      FieldName = 'NO_Cli2'
      Size = 100
    end
    object QrNFeCabAinfCCe_nSeqEvento: TIntegerField
      FieldName = 'infCCe_nSeqEvento'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 56
    Top = 232
  end
  object QrNFeItsI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsi nfei'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 268
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeItsIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfeitsi.FatID'
    end
    object QrNFeItsIFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfeitsi.FatNum'
    end
    object QrNFeItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeitsi.Empresa'
    end
    object QrNFeItsInItem: TIntegerField
      FieldName = 'nItem'
      Origin = 'nfeitsi.nItem'
    end
    object QrNFeItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Origin = 'nfeitsi.prod_cProd'
      Size = 60
    end
    object QrNFeItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Origin = 'nfeitsi.prod_cEAN'
      Size = 14
    end
    object QrNFeItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Origin = 'nfeitsi.prod_xProd'
      Size = 120
    end
    object QrNFeItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Origin = 'nfeitsi.prod_NCM'
      Size = 8
    end
    object QrNFeItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Origin = 'nfeitsi.prod_EXTIPI'
      Size = 3
    end
    object QrNFeItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
      Origin = 'nfeitsi.prod_genero'
    end
    object QrNFeItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Origin = 'nfeitsi.prod_CFOP'
    end
    object QrNFeItsIprod_uCom: TWideStringField
      DisplayWidth = 6
      FieldName = 'prod_uCom'
      Origin = 'nfeitsi.prod_uCom'
      Size = 6
    end
    object QrNFeItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Origin = 'nfeitsi.prod_qCom'
      DisplayFormat = '###,###,##0.0000'
    end
    object QrNFeItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Origin = 'nfeitsi.prod_vUnCom'
      DisplayFormat = '###,###,##0.0000000000'
    end
    object QrNFeItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Origin = 'nfeitsi.prod_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Origin = 'nfeitsi.prod_cEANTrib'
      Size = 14
    end
    object QrNFeItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Origin = 'nfeitsi.prod_uTrib'
      Size = 6
    end
    object QrNFeItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Origin = 'nfeitsi.prod_qTrib'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Origin = 'nfeitsi.prod_vUnTrib'
      DisplayFormat = '#,###,##0.0000000000'
    end
    object QrNFeItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Origin = 'nfeitsi.prod_vFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Origin = 'nfeitsi.prod_vSeg'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Origin = 'nfeitsi.prod_vDesc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Origin = 'nfeitsi.Tem_IPI'
    end
    object QrNFeItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Origin = 'nfeitsi.InfAdCuztm'
    end
    object QrNFeItsIEhServico: TIntegerField
      FieldName = 'EhServico'
      Origin = 'nfeitsi.EhServico'
    end
    object QrNFeItsIEhServico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EhServico_TXT'
      Size = 1
      Calculated = True
    end
    object QrNFeItsITem_II: TSmallintField
      FieldName = 'Tem_II'
      Origin = 'nfeitsi.Tem_II'
    end
    object QrNFeItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFeItsIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrNFeItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeItsIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrNFeItsIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
    end
  end
  object DsNFeItsI: TDataSource
    DataSet = QrNFeItsI
    Left = 268
    Top = 236
  end
end
