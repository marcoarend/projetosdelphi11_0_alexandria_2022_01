unit NFeEFD_C170_Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, Vcl.Mask, Vcl.Menus;

type
  TFmNFeEFD_C170_Cab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtItem: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAversao: TFloatField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_cUF: TSmallintField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_indPag: TSmallintField;
    QrNFeCabAide_mod: TSmallintField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_dSaiEnt: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAide_cMunFG: TIntegerField;
    QrNFeCabAide_tpImp: TSmallintField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabAide_cDV: TSmallintField;
    QrNFeCabAide_tpAmb: TSmallintField;
    QrNFeCabAide_finNFe: TSmallintField;
    QrNFeCabAide_procEmi: TSmallintField;
    QrNFeCabAide_verProc: TWideStringField;
    QrNFeCabAemit_CNPJ: TWideStringField;
    QrNFeCabAemit_CPF: TWideStringField;
    QrNFeCabAemit_xNome: TWideStringField;
    QrNFeCabAemit_xFant: TWideStringField;
    QrNFeCabAemit_xLgr: TWideStringField;
    QrNFeCabAemit_nro: TWideStringField;
    QrNFeCabAemit_xCpl: TWideStringField;
    QrNFeCabAemit_xBairro: TWideStringField;
    QrNFeCabAemit_cMun: TIntegerField;
    QrNFeCabAemit_xMun: TWideStringField;
    QrNFeCabAemit_UF: TWideStringField;
    QrNFeCabAemit_CEP: TIntegerField;
    QrNFeCabAemit_cPais: TIntegerField;
    QrNFeCabAemit_xPais: TWideStringField;
    QrNFeCabAemit_fone: TWideStringField;
    QrNFeCabAemit_IE: TWideStringField;
    QrNFeCabAemit_IEST: TWideStringField;
    QrNFeCabAemit_IM: TWideStringField;
    QrNFeCabAemit_CNAE: TWideStringField;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    QrNFeCabAdest_xNome: TWideStringField;
    QrNFeCabAdest_xLgr: TWideStringField;
    QrNFeCabAdest_nro: TWideStringField;
    QrNFeCabAdest_xCpl: TWideStringField;
    QrNFeCabAdest_xBairro: TWideStringField;
    QrNFeCabAdest_cMun: TIntegerField;
    QrNFeCabAdest_xMun: TWideStringField;
    QrNFeCabAdest_UF: TWideStringField;
    QrNFeCabAdest_CEP: TWideStringField;
    QrNFeCabAdest_cPais: TIntegerField;
    QrNFeCabAdest_xPais: TWideStringField;
    QrNFeCabAdest_fone: TWideStringField;
    QrNFeCabAdest_IE: TWideStringField;
    QrNFeCabAdest_ISUF: TWideStringField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vBCST: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vII: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAISSQNtot_vServ: TFloatField;
    QrNFeCabAISSQNtot_vBC: TFloatField;
    QrNFeCabAISSQNtot_vISS: TFloatField;
    QrNFeCabAISSQNtot_vPIS: TFloatField;
    QrNFeCabAISSQNtot_vCOFINS: TFloatField;
    QrNFeCabARetTrib_vRetPIS: TFloatField;
    QrNFeCabARetTrib_vRetCOFINS: TFloatField;
    QrNFeCabARetTrib_vRetCSLL: TFloatField;
    QrNFeCabARetTrib_vBCIRRF: TFloatField;
    QrNFeCabARetTrib_vIRRF: TFloatField;
    QrNFeCabARetTrib_vBCRetPrev: TFloatField;
    QrNFeCabARetTrib_vRetPrev: TFloatField;
    QrNFeCabAModFrete: TSmallintField;
    QrNFeCabATransporta_CNPJ: TWideStringField;
    QrNFeCabATransporta_CPF: TWideStringField;
    QrNFeCabATransporta_XNome: TWideStringField;
    QrNFeCabATransporta_IE: TWideStringField;
    QrNFeCabATransporta_XEnder: TWideStringField;
    QrNFeCabATransporta_XMun: TWideStringField;
    QrNFeCabATransporta_UF: TWideStringField;
    QrNFeCabARetTransp_vServ: TFloatField;
    QrNFeCabARetTransp_vBCRet: TFloatField;
    QrNFeCabARetTransp_PICMSRet: TFloatField;
    QrNFeCabARetTransp_vICMSRet: TFloatField;
    QrNFeCabARetTransp_CFOP: TWideStringField;
    QrNFeCabARetTransp_CMunFG: TWideStringField;
    QrNFeCabAVeicTransp_Placa: TWideStringField;
    QrNFeCabAVeicTransp_UF: TWideStringField;
    QrNFeCabAVeicTransp_RNTC: TWideStringField;
    QrNFeCabACobr_Fat_nFat: TWideStringField;
    QrNFeCabACobr_Fat_vOrig: TFloatField;
    QrNFeCabACobr_Fat_vDesc: TFloatField;
    QrNFeCabACobr_Fat_vLiq: TFloatField;
    QrNFeCabAInfAdic_InfCpl: TWideMemoField;
    QrNFeCabAExporta_UFEmbarq: TWideStringField;
    QrNFeCabAExporta_XLocEmbarq: TWideStringField;
    QrNFeCabACompra_XNEmp: TWideStringField;
    QrNFeCabACompra_XPed: TWideStringField;
    QrNFeCabACompra_XCont: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAinfProt_Id: TWideStringField;
    QrNFeCabAinfProt_tpAmb: TSmallintField;
    QrNFeCabAinfProt_verAplic: TWideStringField;
    QrNFeCabAinfProt_dhRecbto: TDateTimeField;
    QrNFeCabAinfProt_nProt: TWideStringField;
    QrNFeCabAinfProt_digVal: TWideStringField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_Id: TWideStringField;
    QrNFeCabAinfCanc_tpAmb: TSmallintField;
    QrNFeCabAinfCanc_verAplic: TWideStringField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFeCabAinfCanc_digVal: TWideStringField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_cJust: TIntegerField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabA_Ativo_: TSmallintField;
    QrNFeCabALk: TIntegerField;
    QrNFeCabADataCad: TDateField;
    QrNFeCabADataAlt: TDateField;
    QrNFeCabAUserCad: TIntegerField;
    QrNFeCabAUserAlt: TIntegerField;
    QrNFeCabAAlterWeb: TSmallintField;
    QrNFeCabAAtivo: TSmallintField;
    QrNFeCabAFisRegCad: TIntegerField;
    QrNFeCabATabelaPrc: TIntegerField;
    QrNFeCabACartEmiss: TIntegerField;
    QrNFeCabACondicaoPg: TIntegerField;
    QrNFeCabAMedDDSimpl: TFloatField;
    QrNFeCabAMedDDReal: TFloatField;
    QrNFeCabAJurosMes: TFloatField;
    QrNFeCabAide_cMunFG_TXT: TWideStringField;
    QrNFeCabACNAE_TXT: TWideStringField;
    QrNFeCabANO_CARTEMISS: TWideStringField;
    QrNFeCabANO_FisRegCad: TWideStringField;
    QrNFeCabANO_TabelaPrc: TWideStringField;
    QrNFeCabANO_CondicaoPG: TWideStringField;
    QrNFeCabATP_CART: TIntegerField;
    QrNFeCabAide_tpNF_TXT: TWideStringField;
    QrNFeCabAide_indPag_TXT: TWideStringField;
    QrNFeCabAide_procEmi_TXT: TWideStringField;
    QrNFeCabAide_finNFe_TXT: TWideStringField;
    QrNFeCabAide_tpAmb_TXT: TWideStringField;
    QrNFeCabAide_tpEmis_TXT: TWideStringField;
    QrNFeCabAide_tpImp_TXT: TWideStringField;
    QrNFeCabAModFrete_TXT: TWideStringField;
    QrNFeCabAISS_Usa: TSmallintField;
    QrNFeCabAISS_Alq: TFloatField;
    QrNFeCabAFreteExtra: TFloatField;
    QrNFeCabASegurExtra: TFloatField;
    QrNFeCabAICMSRec_pRedBC: TFloatField;
    QrNFeCabAICMSRec_vICMS: TFloatField;
    QrNFeCabAIPIRec_pRedBC: TFloatField;
    QrNFeCabAPISRec_pRedBC: TFloatField;
    QrNFeCabACOFINSRec_pRedBC: TFloatField;
    QrNFeCabAIPIRec_vIPI: TFloatField;
    QrNFeCabAPISRec_vPIS: TFloatField;
    QrNFeCabACOFINSRec_vCOFINS: TFloatField;
    QrNFeCabAICMSRec_vBC: TFloatField;
    QrNFeCabAIPIRec_vBC: TFloatField;
    QrNFeCabAPISRec_vBC: TFloatField;
    QrNFeCabACOFINSRec_vBC: TFloatField;
    QrNFeCabAICMSRec_pAliq: TFloatField;
    QrNFeCabAIPIRec_pAliq: TFloatField;
    QrNFeCabAPISRec_pAliq: TFloatField;
    QrNFeCabACOFINSRec_pAliq: TFloatField;
    QrNFeCabADataFiscal: TDateField;
    QrNFeCabASINTEGRA_ExpDeclNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpDeclDta: TDateField;
    QrNFeCabASINTEGRA_ExpNat: TWideStringField;
    QrNFeCabASINTEGRA_ExpRegNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpRegDta: TDateField;
    QrNFeCabASINTEGRA_ExpConhNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpConhDta: TDateField;
    QrNFeCabASINTEGRA_ExpConhTip: TWideStringField;
    QrNFeCabASINTEGRA_ExpPais: TWideStringField;
    QrNFeCabASINTEGRA_ExpAverDta: TDateField;
    QrNFeCabACodInfoEmit: TIntegerField;
    QrNFeCabACodInfoDest: TIntegerField;
    QrNFeCabAprotNFe_versao: TFloatField;
    QrNFeCabAretCancNFe_versao: TFloatField;
    QrNFeCabAInfAdic_InfAdFisco: TWideMemoField;
    QrNFeCabAide_hSaiEnt: TTimeField;
    QrNFeCabAide_dhCont: TDateTimeField;
    QrNFeCabAide_xJust: TWideStringField;
    QrNFeCabAemit_CRT: TSmallintField;
    QrNFeCabAdest_email: TWideStringField;
    QrNFeCabAVagao: TWideStringField;
    QrNFeCabABalsa: TWideStringField;
    QrNFeCabACriAForca: TSmallintField;
    QrNFeCabACodInfoTrsp: TIntegerField;
    QrNFeCabAOrdemServ: TIntegerField;
    QrNFeCabAvTotTrib: TFloatField;
    QrNFeCabAide_dhEmi: TDateTimeField;
    QrNFeCabAide_dhEmiTZD: TFloatField;
    QrNFeCabAide_dhSaiEntTZD: TFloatField;
    QrNFeCabAide_dhSaiEnt: TDateTimeField;
    QrNFeCabAide_idDest: TSmallintField;
    QrNFeCabAide_indFinal: TSmallintField;
    QrNFeCabAide_indPres: TSmallintField;
    QrNFeCabAide_dhContTZD: TFloatField;
    QrNFeCabAdest_idEstrangeiro: TWideStringField;
    QrNFeCabAdest_indIEDest: TSmallintField;
    QrNFeCabAdest_IM: TWideStringField;
    QrNFeCabAICMSTot_vICMSDeson: TFloatField;
    QrNFeCabAEstrangDef: TSmallintField;
    QrNFeCabASituacao: TSmallintField;
    QrNFeCabAide_hEmi: TTimeField;
    QrNFeCabAICMSTot_vFCPUFDest: TFloatField;
    QrNFeCabAICMSTot_vICMSUFDest: TFloatField;
    QrNFeCabAICMSTot_vICMSUFRemet: TFloatField;
    QrNFeCabAExporta_XLocDespacho: TWideStringField;
    DsNFeCabA: TDataSource;
    QrNFeEFD_C170: TmySQLQuery;
    QrNFeEFD_C170FatID: TIntegerField;
    QrNFeEFD_C170FatNum: TIntegerField;
    QrNFeEFD_C170Empresa: TIntegerField;
    QrNFeEFD_C170nItem: TIntegerField;
    QrNFeEFD_C170GraGruX: TIntegerField;
    QrNFeEFD_C170COD_ITEM: TWideStringField;
    QrNFeEFD_C170DESCR_COMPL: TWideStringField;
    QrNFeEFD_C170QTD: TFloatField;
    QrNFeEFD_C170UNID: TWideStringField;
    QrNFeEFD_C170VL_ITEM: TFloatField;
    QrNFeEFD_C170VL_DESC: TFloatField;
    QrNFeEFD_C170IND_MOV: TWideStringField;
    QrNFeEFD_C170CST_ICMS: TIntegerField;
    QrNFeEFD_C170CFOP: TIntegerField;
    QrNFeEFD_C170COD_NAT: TWideStringField;
    QrNFeEFD_C170VL_BC_ICMS: TFloatField;
    QrNFeEFD_C170ALIQ_ICMS: TFloatField;
    QrNFeEFD_C170VL_ICMS: TFloatField;
    QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField;
    QrNFeEFD_C170ALIQ_ST: TFloatField;
    QrNFeEFD_C170VL_ICMS_ST: TFloatField;
    QrNFeEFD_C170IND_APUR: TWideStringField;
    QrNFeEFD_C170CST_IPI: TWideStringField;
    QrNFeEFD_C170COD_ENQ: TWideStringField;
    QrNFeEFD_C170VL_BC_IPI: TFloatField;
    QrNFeEFD_C170ALIQ_IPI: TFloatField;
    QrNFeEFD_C170VL_IPI: TFloatField;
    QrNFeEFD_C170CST_PIS: TSmallintField;
    QrNFeEFD_C170VL_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_r: TFloatField;
    QrNFeEFD_C170VL_PIS: TFloatField;
    QrNFeEFD_C170CST_COFINS: TSmallintField;
    QrNFeEFD_C170VL_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_r: TFloatField;
    QrNFeEFD_C170VL_COFINS: TFloatField;
    QrNFeEFD_C170COD_CTA: TWideStringField;
    QrNFeEFD_C170Lk: TIntegerField;
    QrNFeEFD_C170DataCad: TDateField;
    QrNFeEFD_C170DataAlt: TDateField;
    QrNFeEFD_C170UserCad: TIntegerField;
    QrNFeEFD_C170UserAlt: TIntegerField;
    QrNFeEFD_C170AlterWeb: TSmallintField;
    QrNFeEFD_C170Ativo: TSmallintField;
    DsNFeEFD_C170: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel6: TPanel;
    Panel7: TPanel;
    Label01: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label376: TLabel;
    Label377: TLabel;
    Label381: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    EdDBide_indFinal: TDBEdit;
    EdDBide_indPres: TDBEdit;
    EdDBide_idDest: TDBEdit;
    EdDBide_idDest_TXT: TdmkEdit;
    EdDBide_indFinal_TXT: TdmkEdit;
    EdDBide_indPres_TXT: TdmkEdit;
    Panel5: TPanel;
    Label2: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit210: TDBEdit;
    Label314: TLabel;
    Label306: TLabel;
    DBEdit212: TDBEdit;
    DBEdit213: TDBEdit;
    Label307: TLabel;
    Label29: TLabel;
    DBEdit26: TDBEdit;
    Label61: TLabel;
    DBEdit72: TDBEdit;
    DBEdit28: TDBEdit;
    Label31: TLabel;
    Label32: TLabel;
    DBEdit29: TDBEdit;
    DBEdit48: TDBEdit;
    Label37: TLabel;
    PMItem: TPopupMenu;
    Alteraitem1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure EdDBide_idDestChange(Sender: TObject);
    procedure EdDBide_indFinalChange(Sender: TObject);
    procedure EdDBide_indPresChange(Sender: TObject);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure Alteraitem1Click(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa: Integer;
    procedure ReopenNfeCabA();
  end;

  var
  FmNFeEFD_C170_Cab: TFmNFeEFD_C170_Cab;

implementation

uses UnMyObjects, DmkDAC_PF, Module, NFe_PF;

{$R *.DFM}

procedure TFmNFeEFD_C170_Cab.Alteraitem1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEFD_C170(stUpd, QrNFeEFD_C170FatID.Value,
  QrNFeEFD_C170FatNum.Value, QrNFeEFD_C170Empresa.Value,
  QrNFeEFD_C170nItem.Value);
end;

procedure TFmNFeEFD_C170_Cab.BtItemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItem, BtItem);
end;

procedure TFmNFeEFD_C170_Cab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEFD_C170_Cab.EdDBide_idDestChange(Sender: TObject);
begin
  EdDBide_idDest_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_idDest, Geral.IMV(EdDBide_idDest.Text));
end;

procedure TFmNFeEFD_C170_Cab.EdDBide_indFinalChange(Sender: TObject);
begin
  EdDBide_indFinal_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indFinal, Geral.IMV(EdDBide_indFinal.Text));
end;

procedure TFmNFeEFD_C170_Cab.EdDBide_indPresChange(Sender: TObject);
begin
  EdDBide_indPres_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPres, Geral.IMV(EdDBide_indPres.Text));
end;

procedure TFmNFeEFD_C170_Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEFD_C170_Cab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeEFD_C170_Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEFD_C170_Cab.QrNFeCabAAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEFD_C170, Dmod.MyDB, [
  'SELECT c170.* ',
  'FROM nfeefd_c170 c170',
  'WHERE c170.FatID=' + Geral.FF0(FFatID),
  'AND c170.FatNum=' + Geral.FF0(FFatNum),
  'AND c170.Empresa=' + Geral.FF0(FEmpresa),
  '']);
end;

procedure TFmNFeEFD_C170_Cab.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  QrNFeCabAide_tpNF_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF, QrNFeCabAide_tpNF.Value);
  QrNFeCabAide_indPag_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPag, QrNFeCabAide_indPag.Value);
  QrNFeCabAide_procEmi_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_procEmi, QrNFeCabAide_procEmi.Value);
  QrNFeCabAide_finNFe_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe, QrNFeCabAide_finNFe.Value);
  QrNFeCabAide_tpAmb_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, QrNFeCabAide_tpAmb.Value);
  QrNFeCabAide_tpEmis_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpEmis, QrNFeCabAide_tpEmis.Value);
  QrNFeCabAide_tpImp_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpImp, QrNFeCabAide_tpImp.Value);
  QrNFeCabAModFrete_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete, QrNFeCabAModFrete.Value);
end;

procedure TFmNFeEFD_C170_Cab.ReopenNfeCabA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
  'SELECT mun1.Nome ide_cMunFG_TXT,  cnan.Nome CNAE_TXT, ',
  'frc.Nome NO_FisRegCad, car.Nome NO_CARTEMISS, ',
  'car.Tipo TP_CART, tpc.Nome NO_TabelaPrc, ',
  'ppc.Nome NO_CondicaoPG, ppc.JurosMes, ',
  'ppc.MedDDSimpl, ppc.MedDDReal, ',
  'frc.ISS_Usa, frc.ISS_Alq, nfea.* ',
  'FROM nfecaba nfea ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21cad cnan ON cnan.CodAlf=nfea.emit_CNAE ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun1 ON mun1.Codigo=nfea.ide_cMunFG ',
  'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad ',
  'LEFT JOIN carteiras car ON car.Codigo=nfea.CartEmiss ',
  'LEFT JOIN tabeprccab tpc ON tpc.Codigo=nfea.TabelaPrc ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=nfea.CondicaoPg ',
  'WHERE nfea.FatID=' + Geral.FF0(FFatID),
  'AND nfea.FatNum=' + Geral.FF0(FFatNum),
  'AND nfea.Empresa=' + Geral.FF0(FEmpresa),
  '']);
end;

end.
