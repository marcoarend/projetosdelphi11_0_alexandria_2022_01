object FmNFeDocOrfao: TFmNFeDocOrfao
  Left = 339
  Top = 185
  Caption = 'NFe-ORFAO-001 :: Emit e/ou Dest Orf'#227'os em NF-e'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 111
    ExplicitHeight = 398
    object Grade: TDBGrid
      Left = 0
      Top = 0
      Width = 1008
      Height = 338
      Align = alClient
      DataSource = DmNFe_0000.Ds_D_E
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID NF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'ID Centro'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID_TXT'
          Title.Caption = 'Centro de emiss'#227'o'
          Width = 274
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Empresa'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'N'#186' NF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dest_CNPJ'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dest_CPF'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodInfoDest'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'emit_CNPJ'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'emit_CPF'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodInfoEmit'
          Width = 62
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 380
        Height = 32
        Caption = 'Emit e/ou Dest Orf'#227'os em NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 380
        Height = 32
        Caption = 'Emit e/ou Dest Orf'#227'os em NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 380
        Height = 32
        Caption = 'Emit e/ou Dest Orf'#227'os em NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
end
