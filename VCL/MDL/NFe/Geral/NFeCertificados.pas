unit NFeCertificados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     ComCtrls,
  CAPICOM_TLB, ComObj, Variants, msxmldom, MSXML2_TLB;

type
  TFmNFeCertificados = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    PCStore: TPageControl;
    Panel3: TPanel;
    CBStores: TComboBox;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    LBPessoal: TListBox;
    LBOutras: TListBox;
    LBIntermediarias: TListBox;
    LBRaiz: TListBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CBStoresChange(Sender: TObject);
  private
    { Private declarations }
    procedure ListarCertificados(Lista: TStrings; Certificados: ICertificates);
    function  ExecutaAssinatura(aValue: AnsiString; Certificado:
              ICertificate2; URIs: TStringList; out sXML: AnsiString): Boolean;
    function Valida_XML(XML: AnsiString; SchemaPath: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmNFeCertificados: TFmNFeCertificados;

  const
    DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';

implementation

//uses ;

{$R *.DFM}

procedure TFmNFeCertificados.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCertificados.CBStoresChange(Sender: TObject);
var
  CertStore: IStore3;
  NomeStore: String;
  Lista: TListBox;
begin
  PCStore.ActivePageIndex := CBStores.ItemIndex;
  Lista := nil;
  case CBStores.ItemIndex of
    0:
    begin
      NomeStore := 'My';
      Lista     := LBPessoal;
    end;
    1:
    begin
      NomeStore := 'AddressBook';
      Lista     := LBOutras;
    end;
    2:
    begin
      NomeStore := 'CA';
      Lista     := LBIntermediarias;
    end;
    3:
    begin
      NomeStore := 'Root';
      Lista     := LBRaiz;
    end;
  end;
  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore,
    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);
  ListarCertificados(Lista.Items, CertStore.Certificates);
  CertStore := nil;
end;

procedure TFmNFeCertificados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCertificados.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmNFeCertificados.ListarCertificados(Lista: TStrings;
  Certificados: ICertificates);
var
  I: Integer;
  Certificado: ICertificate2;
  Res: HResult;
  OV: OleVariant;
  ADispIntfIID: TGUID;
begin
  Lista.Clear;
  if (Certificados.Count > 0) then
  begin
    for I := 1 to Certificados.Count do
    begin
{
constructor TDispatchSilencer.Create(ADispatch: IUnknown;
  const ADispIntfIID: TGUID);
begin
  inherited Create;
  DispIntfIID := ADispIntfIID;
  OleCheck(ADispatch.QueryInterface(ADispIntfIID, Dispatch));
end;
}
      OleCheck(HRESULT(Certificados.Item[I].QueryInterface(Certificado)));
      {
      OV := Certificados.Item[I].QueryInterface(IID_ICertificate2, Certificado);
      Res := IDispatch(Certificados.Item[I].QueryInterface(ICertificate2,
        Certificado);
      OleCheck(Res);
      }

      {
      Certs := CertStore.Certificates as ICertificates2;
      for I := 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
        begin
          CertStoreMem.Add(Cert);
        end;
      end;
      }
      Lista.Add(Certificado.GetInfo(CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME));
    end;
  end;

end;

function TFmNFeCertificados.ExecutaAssinatura(aValue: AnsiString; Certificado:
  ICertificate2; URIs: TStringList; out sXML: AnsiString): Boolean;
(** ASSINATURA **)
VAR
  XMLDoc                           : IXMLDOMDocument3;
  XMLDSig                          : IXMLDigitalSignature;
  dsigKey                          : IXMLDSigKey;
  signedKey                        : IXMLDSigKey;
  nodePai, nodeX509Data, nodeIrmao : IXMLDOMNode;

(** CERTIFICADO **)
VAR
  CertStore                        : IStore3;
  CertStoreMem                     : IStore3;
  PrivateKey                       : IPrivateKey;
  Certs                            : ICertificates2;
  Cert                             : ICertificate2;

(** GERAL **)
VAR
  C, I                             : Integer;
  xmlHeaderAntes, xmlHeaderDepois  : String;

  FUNCTION FindNode(lstNodes : IXMLDOMNodeList; strNome:string):IXMLDOMNode;
  VAR node,
      noderet : IXMLDOMNode;
  begin
    result := nil;
    node   := lstNodes.nextNode as IXMLDOMNode;
    while node <> nil do begin
      If node.nodeName = strNome then begin
        result := node;
        exit;
      end;

      If node.hasChildNodes then begin
        noderet := findNode(node.childNodes,strNome);
        If noderet <> nil then begin
          result := noderet;
          exit;
        end;
      end;

      node := lstNodes.nextNode as IXMLDOMNode;
    end;
  end;

  FUNCTION FindNodeURI(lstNodes : IXMLDOMNodeList; Tag, URI : String) : IXMLDOMNode;
  VAR Node,
      NodeRet : IXMLDOMNode;
      TextXML : String;
  Begin
     Result := nil;
     Node   := (lstNodes.nextNode As IXMLDOMNode);
     While (Node <> nil) Do
     Begin
        TextXML := Node.XML;
        If (Node.NodeName = Tag) And (Pos(URI,TextXML) > 0) Then
        Begin
           Result := Node;
           Exit;
        End;
        If (Node.HasChildNodes) Then
        Begin
           NodeRet := FindNodeURI(Node.ChildNodes,Tag,URI);
           If (NodeRet <> nil) Then
           Begin
              Result := NodeRet;
              Exit;
           End;
        End;
        Node := (lstNodes.nextNode As IXMLDOMNode);
     End;
  End;

  procedure DeleteFindNode(TagURI : String);
  begin
    nodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', TagURI);
    nodePai := FindNode(NodePai.ChildNodes,'KeyInfo');
    If (nodePai <> nil) Then Begin
      nodeX509Data := findNode(nodePai.childNodes,'X509Data');
      nodeIrmao    := nodeX509Data.nextSibling;
      while nodeIrmao <> nil do begin
        nodePai.removeChild(nodeIrmao);
        nodeIrmao := nodeX509Data.nextSibling;
      end;
    End;
  End;

begin
  If (Trim(aValue) = '') Then
    raise Exception.Create('N�o existe informa��o para fazer a Assinatura Digital');

  aValue:=StringReplace( aValue, #10, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #13, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #9, '', [rfReplaceAll] );


   (* Pegando o header antes de assinar *)
  xmlHeaderAntes := '' ;
  I := pos('?>', aValue) ;
  if I > 0 then
    xmlHeaderAntes := copy(aValue, 1, I+1);

  (*** CONFIGURANDO O XML DOC ***)
  XMLDoc := CoDOMDocument50.Create;

  XMLDoc.async := FALSE;
  XMLDoc.validateOnParse := FALSE;
  XMLDoc.preserveWhiteSpace := TRUE;
  (******************************)

  XMLDSig := CoMXDigitalSignature50.Create;

  If (NOT XMLDoc.LoadXML(aValue) ) then
    raise Exception.Create('N�o foi poss�vel carregar o "texto" XML');

  XMLDoc.setProperty('SelectionNamespaces', DSIGNS); //DSIGNS->Constante declarada

  C:=0;
  REPEAT
    NodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', URIs.Strings[C]);

    XMLDSig.signature := NodePai;

    If (XMLDSig.signature = NIL) then
      raise Exception.Create('Falha ao setar assinatura.');

    If (XMLDSig.signature = NIL) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   if Certificado.SerialNumber <> SerialCertificado then
      CertStoreMem := nil;

    If CertStoreMem = NIL then begin

      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      For I := 1 to Certs.Count do Begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then Begin
          CertStoreMem.Add(Cert);
        end;
      end;

    end;

    OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey, PrivateKey));

    xmldsig.store := CertStoreMem;

    dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
    If (dsigKey = NIL) then
      raise Exception.Create('Erro ao criar a chave do CSP.');

    signedKey := xmldsig.sign(dsigKey, $00000002);

    If (signedKey = NIL) Then
      raise Exception.Create('Assinatura Falhou.');

    If (signedKey <> NIL) Then
      DeleteFindNode(URIs.Strings[C]);

    Inc(C);
  UNTIL C>=URIs.Count;

  sXML:=XMLDoc.Xml;

  If xmlHeaderAntes <> '' Then Begin
    I:=pos('?>', sXML);
    If I > 0 Then Begin
      xmlHeaderDepois:=copy(sXML,1,I+1);
      If xmlHeaderAntes <> xmlHeaderDepois Then
        sXML:=StuffString(sXML, 1, length(xmlHeaderDepois), xmlHeaderAntes);
    End Else
      sXML:=xmlHeaderAntes + sXML;
  End;

  Result:=TRUE;

  dsigKey   := NIL;
  signedKey := NIL;
  xmldoc    := NIL;
  xmldsig   := NIL;
end;

function TFmNFeCertificados.Valida_XML(XML: AnsiString; SchemaPath: String):
  Boolean;
var
  DOMDocument : IXMLDOMDocument3;
  ParseError  : IXMLDOMParseError;
  Schema      : XMLSchemaCache;

begin
  DOMDocument:=CoDOMDocument50.Create;

  DOMdocument.Async:=FALSE;
  DOMdocument.ResolveExternals:=FALSE;
  DOMdocument.ValidateOnParse:=TRUE;

  DOMdocument.LoadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_simples.xsd', SchemaPath+'Tipos_Simples.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_complexos.xsd', SchemaPath+'Tipos_Complexos.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/servico_enviar_lote_rps_envio.xsd', SchemaPath+'servico_enviar_lote_rps_envio.xsd');

  DOMdocument.Schemas := Schema;

  ParseError:=DOMdocument.validate;
  Result:=(ParseError.errorCode = 0);

  if ParseError.errorCode <> S_OK then
    raise Exception.Create(IntTostr(ParseError.errorCode)+' - '+ParseError.reason);

  DOMDocument:=Nil;
  ParseError:=Nil;
  Schema:=Nil;
end;

end.
