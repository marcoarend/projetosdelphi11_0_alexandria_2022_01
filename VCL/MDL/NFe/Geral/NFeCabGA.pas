unit NFeCabGA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums,
  dmkRadioGroup, UnDmkProcFunc;

type
  TFmNFeCabGA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrNFeCabGA: TmySQLQuery;
    DsNFeCabGA: TDataSource;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    Panel6: TPanel;
    Label7: TLabel;
    EdControle: TdmkEdit;
    Panel5: TPanel;
    RGTipo: TdmkRadioGroup;
    PnCPF: TPanel;
    Label8: TLabel;
    EdautXML_CPF: TdmkEdit;
    PnCNPJ: TPanel;
    Label11: TLabel;
    EdautXML_CNPJ: TdmkEdit;
    QrNFeCabGAFatID: TIntegerField;
    QrNFeCabGAFatNum: TIntegerField;
    QrNFeCabGAEmpresa: TIntegerField;
    QrNFeCabGAControle: TIntegerField;
    QrNFeCabGAAddForma: TSmallintField;
    QrNFeCabGAAddIDCad: TSmallintField;
    QrNFeCabGATipo: TSmallintField;
    QrNFeCabGAautXML_CNPJ: TWideStringField;
    QrNFeCabGAautXML_CPF: TWideStringField;
    QrNFeCabGALk: TIntegerField;
    QrNFeCabGADataCad: TDateField;
    QrNFeCabGADataAlt: TDateField;
    QrNFeCabGAUserCad: TIntegerField;
    QrNFeCabGAUserAlt: TIntegerField;
    QrNFeCabGAAlterWeb: TSmallintField;
    QrNFeCabGAAtivo: TSmallintField;
    QrNFeCabGACNPJ_CPF: TWideStringField;
    QrNFeCabGAITEM: TIntegerField;
    QrNFeCabGATpDOC: TWideStringField;
    QrNFeCabGANO_AddForma: TWideStringField;
    QrNFeCabGANO_ENT_ECO: TWideStringField;
    SbEntidade: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrNFeCabGAAfterOpen(DataSet: TDataSet);
    procedure QrNFeCabGABeforeClose(DataSet: TDataSet);
    procedure RGTipoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrNFeCabGACalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function AtualizaListaContatos(): Boolean;
  public
    { Public declarations }
    FInserePadroes: Boolean;
    FFatID, FFatNum, FEmpresa, FDestRem: Integer;
    //
    procedure ReopenNFeCabGA(Controle: Integer);
  end;

  var
  FmNFeCabGA: TFmNFeCabGA;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, UnGOTOy, ModuleGeral;

{$R *.DFM}

procedure TFmNFeCabGA.BtAlteraClick(Sender: TObject);
begin
  if QrNFeCabGA.RecordCount > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrNFeCabGA, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'nfecabga');
  end;
end;

procedure TFmNFeCabGA.BtConfirmaClick(Sender: TObject);
var
  autXML_CNPJ, autXML_CPF: String;
  FatID, FatNum, Empresa, Controle, AddForma, AddIDCad, Tipo: Integer;
begin
  FatID    := FFatID;
  FatNum   := FFatNum;
  Empresa  := FEmpresa;
  Controle := EdControle.ValueVariant;
  Tipo     := RGTipo.ItemIndex;
  AddForma := Integer(TNFeautXML.naxAvulso);
  AddIDCad := 0;
  //
  if Tipo = 0 then
  begin
    autXML_CNPJ := Geral.SoNumero_TT(EdautXML_CNPJ.Text);
    autXML_CPF  := '';
    //
    if MyObjects.FIC(autXML_CNPJ = '', EdautXML_CNPJ, 'Informe o CNPJ!') then
      Exit;
    if MyObjects.FIC(Length(autXML_CNPJ) <> 14, EdautXML_CNPJ, 'O CNPJ deve conter 14 algarismos!') then
      Exit;
  end else begin
    autXML_CNPJ := '';
    autXML_CPF  := Geral.SoNumero_TT(EdautXML_CPF.Text);
    //
    if MyObjects.FIC(autXML_CPF = '', EdautXML_CPF, 'Informe o CPF!') then
      Exit;
    if MyObjects.FIC(Length(autXML_CPF) <> 11, EdautXML_CPF, 'O CPF deve conter 11 algarismos!') then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('nfecabga', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecabga', False, [
    'FatID', 'FatNum', 'Empresa',
    'AddForma', 'AddIDCad', 'Tipo',
    'autXML_CNPJ', 'autXML_CPF'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    AddForma, AddIDCad, Tipo,
    autXML_CNPJ, autXML_CPF], [
    Controle], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    ReopenNFeCabGA(Controle);
  end;
end;

procedure TFmNFeCabGA.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmNFeCabGA.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  if QrNFeCabGA.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a retirada do setor selecionado?',
    'nfecabga', 'Controle', QrNFeCabGAControle.Value, Dmod.MyDB) = ID_YES then
    begin
      Controle := GOTOy.LocalizaPriorNextIntQr(QrNFeCabGA,
        QrNFeCabGAControle, QrNFeCabGAControle.Value);
      ReopenNFeCabGA(Controle);
    end;
  end;
end;

procedure TFmNFeCabGA.BtIncluiClick(Sender: TObject);
begin
  if QrNFeCabGA.RecordCount < 10 then
  begin
    UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrNFeCabGA, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'nfecabga');
  end else
    Geral.MB_Aviso('Limite de itens (10) atingido!');
end;

procedure TFmNFeCabGA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabGA.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabGA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align   := alClient;
end;

procedure TFmNFeCabGA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabGA.FormShow(Sender: TObject);
begin
  if FInserePadroes then
  begin
    SbEntidade.Visible := True;
    //
    Screen.Cursor := crHourGlass;
    try
      if not AtualizaListaContatos() then
        Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    SbEntidade.Visible := False;
end;

function TFmNFeCabGA.AtualizaListaContatos(): Boolean;
var
  Qry, QryLoc: TmySQLQuery;
  SQLCompl, CPF, CNPJ: String;
  Tipo: Integer;

  procedure InsereAtual();
  var
    autXML_CNPJ, autXML_CPF: String;
    Controle, AddForma, AddIDCad, Tipo: Integer;
  begin
    Controle := 0;
    Tipo     := Qry.FieldByName('Tipo').AsInteger;
    AddForma := Integer(TNFeautXML.naxEntiContat); // EntiContat.Controle
    AddIDCad := Qry.FieldByName('Controle').AsInteger;
    //
    if Tipo = 0 then
    begin
      autXML_CNPJ := Geral.SoNumero_TT(Qry.FieldByName('CNPJ').AsString);
      autXML_CPF  := '';
      //
      if MyObjects.FIC(Length(autXML_CNPJ) <> 14, nil,
      'O CNPJ ' + autXML_CNPJ + ' n�o foi inclu�do pois n�p possui 14 algarismos!') then
        Exit;
    end else begin
      autXML_CNPJ := '';
      autXML_CPF  := Geral.SoNumero_TT(Qry.FieldByName('CPF').AsString);
      //
      if MyObjects.FIC(Length(autXML_CPF) <> 11, nil,
      'O CPF ' + autXML_CPF + ' n�o foi inclu�do pois n�p possui 11 algarismos!') then
        Exit;
    end;
    //
    Controle := UMyMod.BPGS1I32('nfecabga', 'Controle', '', '', tsPos, stIns, Controle);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabga', False, [
      'autXML_CNPJ', 'autXML_CPF', 'AddForma',
      'AddIDCad', 'Tipo'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      autXML_CNPJ, autXML_CPF, AddForma,
      AddIDCad, Tipo], [
      FFatID, FFatNum, FEmpresa, Controle], True);
  end;

begin
  try
    Result := True;
    //
    Qry    := TmySQLQuery.Create(Dmod);
    QryLoc := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, Tipo, CNPJ, CPF ',
        'FROM enticontat ',
        'WHERE Codigo=' + Geral.FF0(FDestRem),
        'AND Aplicacao & 1 ',
        'And Ativo=1 ',
        'LIMIT 10 ',
        '']);
      if Qry.RecordCount = 0 then
        Geral.MB_Aviso(
          'Destinat�rio / remetente sem emails cadastrados para autoriza��o de obten��o de XML!')
      else
      begin
        Qry.First;
        //
        while not Qry.Eof do
        begin
          Tipo := Qry.FieldByName('Tipo').AsInteger;
          CPF  := Qry.FieldByName('CPF').AsString;
          CNPJ := Qry.FieldByName('CNPJ').AsString;
          //
          if Tipo = 0 then
            SQLCompl := 'AND autXML_CNPJ="' + CNPJ + '"'
          else
            SQLCompl := 'AND autXML_CPF="' + CPF + '"';
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Dmod.MyDB, [
            'SELECT * FROM nfecabga ',
            'WHERE FatID=' + Geral.FF0(FFatID),
            'AND FatNum=' + Geral.FF0(FFatNum),
            'AND Empresa=' + Geral.FF0(FEmpresa),
            SQLCompl,
            '']);
          if QryLoc.RecordCount = 0 then
            InsereAtual();
          //
          Qry.Next;
        end
      end;
    finally
      Qry.Free;
      QryLoc.Free;
    end;
  except
    Result := False;
  end;
end;

procedure TFmNFeCabGA.QrNFeCabGAAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := QrNFeCabGA.RecordCount < 10;
end;

procedure TFmNFeCabGA.QrNFeCabGABeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmNFeCabGA.QrNFeCabGACalcFields(DataSet: TDataSet);
begin
  QrNFeCabGAITEM.Value := QrNFeCabGA.RecNo;
end;

procedure TFmNFeCabGA.ReopenNFeCabGA(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabGA, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC, ',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, autXML_CNPJ, autXML_CPF) CNPJ_CPF ',
  'FROM nfecabga cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.FatID=' + Geral.FF0(FFatID),
  'AND cga.FatNum=' + Geral.FF0(FFatNum),
  'AND cga.Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabGA.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabGA.RGTipoClick(Sender: TObject);
begin
  PnCNPJ.Visible := RGTipo.ItemIndex = 0;
  PnCPF.Visible  := RGTipo.ItemIndex = 1;
end;

procedure TFmNFeCabGA.SbEntidadeClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(FDestRem, fmcadEntidade2, fmcadEntidade2);
  //
  Screen.Cursor := crHourGlass;
  try
    if not AtualizaListaContatos() then
      Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
  finally
    Screen.Cursor := crDefault;
  end;
  //
  ReopenNFeCabGA(0);
end;

end.
