unit NFe_Pesq_0000_ImpFat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, frxClass, frxDBSet, mySQLDbTables, Variants,
  dmkCheckGroup;

type
  TFmNFe_Pesq_0000_ImpFat = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    frxListaFaturasNFes: TfrxReport;
    QrFaturas: TmySQLQuery;
    QrFaturaside_natOp: TWideStringField;
    QrFaturaside_nNF: TIntegerField;
    QrFaturaside_serie: TIntegerField;
    QrFaturaside_dEmi: TDateField;
    QrFaturasICMSTot_vNF: TFloatField;
    QrFaturasNOME_tpEmis: TWideStringField;
    QrFaturasNOME_tpNF: TWideStringField;
    StringField16: TWideStringField;
    QrFaturasnDup: TWideStringField;
    QrFaturasdVenc: TDateField;
    QrFaturasvDup: TFloatField;
    QrFaturasxMotivo: TWideStringField;
    QrFaturaside_tpEmis: TSmallintField;
    QrFaturaside_tpNF: TSmallintField;
    frxDsFaturas: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFaturasCalcFields(DataSet: TDataSet);
    procedure frxListaFaturasNFesGetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ImprimeListaFaturas();
    procedure ReopenNFe_ItsFatur();
    function  SQL_Filtro(): String;
  public
    { Public declarations }
    CGcSitConf: TdmkCheckGroup;
    EdFilial_ValueVariant: Variant;
    RGQuemEmit_ItemIndex, EdCliente_ValueVariant, RGAmbiente_ItemIndex,
    QrClientesTipo_Value: Integer;
    CBFilial_Text, CBCliente_Text, QrClientesCNPJ_Value, QrClientesCPF_Value: String;
    Ck100e101_Checked: Boolean;
    TPDataI_Date, TPDataF_Date: TDateTime;
  end;

  var
  FmNFe_Pesq_0000_ImpFat: TFmNFe_Pesq_0000_ImpFat;

implementation

uses UnMyObjects, DmkDAC_PF, UnDmkProcFunc, Module, ModuleGeral;

{$R *.DFM}

procedure TFmNFe_Pesq_0000_ImpFat.BtImprimeClick(Sender: TObject);
begin
  ImprimeListaFaturas();
end;

procedure TFmNFe_Pesq_0000_ImpFat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFe_Pesq_0000_ImpFat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFe_Pesq_0000_ImpFat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataI.Date := Geral.PrimeiroDiaDoMes(Date);
  TPDataF.Date := Geral.UltimoDiaDoMes(Date);
end;

procedure TFmNFe_Pesq_0000_ImpFat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFe_Pesq_0000_ImpFat.FormShow(Sender: TObject);
begin
  TPDataI.Date := TPDataI_Date;
  TPDataF.Date := TPDataF_Date;
end;

procedure TFmNFe_Pesq_0000_ImpFat.frxListaFaturasNFesGetValue(const VarName: string;
  var Value: Variant);
var
  DataI, DataF: TDateTime;
begin
  DataI := TPDataI.Date;
  DataF := TPDataF.Date;
  //
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial_Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente_Text, EdCliente_ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(DataI, DataF, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101_Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit_ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente_ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODUÇÃO';
      2: Value := 'HOMOLOGAÇÃO';
    end;
  end
end;

procedure TFmNFe_Pesq_0000_ImpFat.ImprimeListaFaturas;
begin
  ReopenNFe_ItsFatur();
  MyObjects.frxDefineDataSets(frxListaFaturasNFes, [
    DModG.frxDsDono,
    frxDsFaturas
  ]);
  MyObjects.frxMostra(frxListaFaturasNFes, 'Lista de Faturas de NF-e(s)')
end;

procedure TFmNFe_Pesq_0000_ImpFat.QrFaturasCalcFields(DataSet: TDataSet);
begin
  case QrFaturaside_tpEmis.Value of
    1: QrFaturasNome_tpEmis.Value := 'Normal';
    2: QrFaturasNome_tpEmis.Value := 'Contingência FS';
    3: QrFaturasNome_tpEmis.Value := 'Contingência SCAN';
    4: QrFaturasNome_tpEmis.Value := 'Contingência EPEC';
    5: QrFaturasNome_tpEmis.Value := 'Contingência FS-DA';
    else QrFaturasNome_tpEmis.Value := '? ? ? ?';
  end;
  case QrFaturaside_tpNF.Value of
    0: QrFaturasNome_tpNF.Value := 'E';
    1: QrFaturasNome_tpNF.Value := 'S';
    else QrFaturasNome_tpNF.Value := '?';
  end;
end;

procedure TFmNFe_Pesq_0000_ImpFat.ReopenNFe_ItsFatur;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFaturas, Dmod.MyDB, [
    'SELECT nfa.ide_natOp, nfa.ide_nNF, nfa.ide_serie, ',
    'nfa.ide_dEmi, nfa.ICMSTot_vNF, ',
    'ELT(nfa.ide_tpEmis, "Normal", "Contingência FS", ',
    '"Contingência SCAN", "Contingência EPEC", ',
    '"Contingência FS-DA", "? ? ? ?") NOME_tpEmis, ',
    'ELT(nfa.ide_tpNF + 1, "E", "S", "?") NOME_tpNF, ',
    'IF(nfa.FatID=' + Geral.FF0(VAR_FATID_0053) + ', ',
    'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome), ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro, ',
    'IF(nfa.infCanc_cStat>0, nfa.infCanc_xMotivo, nfa.infProt_xMotivo) xMotivo, ',
    'nfy.nDup, nfy.dVenc, nfy.vDup, nfa.ide_tpEmis, nfa.ide_tpNF ',
    'FROM nfecaby nfy ',
    'LEFT JOIN nfecaba nfa ON ',
    '( ',
    '  nfa.FatID=nfy.FatID AND ',
    '  nfa.FatNum=nfy.FatNum AND ',
    '  nfa.Empresa=nfy.Empresa ',
    ') ',
    'LEFT JOIN entidades cli ON cli.Codigo=nfa.CodInfoDest ',
    'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmit ',
    SQL_Filtro(),
    'ORDER BY nfy.dVenc, nfa.ide_serie, nfa.ide_nNF ',
    '']);
  //Geral.MB_SQL(Self, QrFaturas);
end;

function TFmNFe_Pesq_0000_ImpFat.SQL_Filtro: String;
var
  Empresa, SQL_DOC, SQL_Doc1, SQL_1XX, SQL_Amb, SQL_SitConf: String;
  DataI, DataF: TDateTime;
begin
  DataI := TPDataI.Date;
  DataF := TPDataF.Date;
  //
  if (EdFilial_ValueVariant <> Null) and (EdFilial_ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  case RGQuemEmit_ItemIndex of
    0: SQL_DOC := 'AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"';
    1: SQL_DOC := 'AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)';
    else SQL_DOC := '';
  end;
  if Ck100e101_Checked  then
    SQL_1XX := 'AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)'
  else
    SQL_1XX := '';
  //
  case RGAmbiente_ItemIndex of
    //0: ; // Tudo
    1: SQL_Amb := 'AND nfa.ide_tpAmb=1';
    2: SQL_Amb := 'AND nfa.ide_tpAmb=2';
    else SQL_Amb := '';
  end;
  SQL_Doc1 := '';
  if EdCliente_ValueVariant <> 0 then
  begin
    if QrClientesTipo_Value = 0 then
    begin
      SQL_Doc1 := Geral.TFD(QrClientesCNPJ_Value, 14, siPositivo);
      SQL_Doc1 := 'AND dest_CNPJ="' + SQL_Doc1 + '"';
    end else begin
      SQL_Doc1 := Geral.TFD(QrClientesCPF_Value, 11, siPositivo);
      SQL_Doc1 := 'AND dest_CPF="' + SQL_Doc1 + '"';
    end;
  end;
  //
  SQL_SitConf := CGcSitConf.GetIndexesChecked(True, -1);
  SQL_SitConf := 'AND nfa.cSitConf IN (' + SQL_SitConf + ')';
  //
  Result := Geral.ATS([
  dmkPF.SQL_Periodo('WHERE nfy.dVenc ', DataI, DataF, True, True),
  'AND nfa.Empresa = ' + Empresa,
  SQL_DOC,
  SQL_1XX,
  SQL_Amb,
  SQL_Doc1,
  SQL_SitConf,
  '']);
end;

end.
