unit NFe_DownXML;

interface

uses
  System.IOUtils, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmNFe_DownXML = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrStqaLocIts: TMySQLQuery;
    DsStqaLocIts: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    SbPasta: TSpeedButton;
    EdDiretorio: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdArquivo: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    MeWarn: TMemo;
    MeInfo: TMemo;
    MeAusentes: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPastaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenStqaLocIts(Controle: Integer);
  public
    { Public declarations }
    FtpNF: TtpNF;
    FtpEmissNF: TtpEmissNF;
    //
    FJanela, FArqUnico: String;
    //FEmpresa,
    FIDCtrl, FFatIdUnico, FFatNumUnico, FEmpresaUnica: Integer;
  end;

  var
  FmNFe_DownXML: TFmNFe_DownXML;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnitMyXML, NFeImporta_0400;

{$R *.DFM}

procedure TFmNFe_DownXML.BtOKClick(Sender: TObject);
var
  Diretorio, Arquivo, TabLctA: String;
  Texto: WideString;
  FatID, FatNum: Integer;
  //
  Empresa, Filial, IDCtrl, CodInfoEmit, Transporta, cab_qVol: Integer;
  cab_PesoB, cab_PesoL, SumPesoLIts: Double;
  DataFiscal: TDateTime;
begin
  MeWarn.Lines.Clear;
  MeInfo.Lines.Clear;
  MeAusentes.Lines.Clear;
  //
  Diretorio := EdDiretorio.ValueVariant;
  if not Geral.VerificaDir(Diretorio, '\', 'Diret�rio n�o encontrado: ' +
    Diretorio, False) then Exit;
  Arquivo := Diretorio + EdArquivo.ValueVariant;
  if not UnMyXML.CarregaXML(Arquivo, Texto) then Exit;
  FatID     := FFatIdUnico;
  FatNum    := FFatNumUnico;
  Empresa   := FEmpresaUnica;
  //
  if Empresa <> 0 then
  begin
    Filial := DModG.ObtemFilialDeEntidade(Empresa);
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  end else
    TabLctA := sTabLctErr;
  if (FtpEmissNF = TtpEmissNF.tpemiProprio) and (FtpNF = TtpNF.tpnfSaida) then
  begin
    if FatID = 0 then
      FatID := VAR_FATID_0050;
    //if FatNum = 0 then   := FFatNumUnico;
    Empresa := FEmpresaUnica;
    if FatID = VAR_FATID_0050 then
    begin
      if UnNFeImporta_0400.ImportaDadosNFeDeXML(FtpEmissNF, FtpNF,
      MeWarn, MeInfo, MeAusentes, Texto, Self, FatID, FatNum,
      Empresa, IDCtrl, CodInfoEmit, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
      SumPesoLIts, DataFiscal, TabLcta) then
      begin
        FIDCtrl := IDCtrl;
      end;
    end else
      Geral.MB_Info('Modelo de NFe n�o implementado para importa��o pelo XML (B)!');
  end else
  if (FtpEmissNF = TtpEmissNF.tpemiTerceiros) and (FtpNF = TtpNF.tpnfEntrada) then
  begin
    if (FArqUnico <> EmptyStr) and (FFatIdUnico <> 0) and (FFatNumUnico <> 0)
    and (FEmpresaUnica <> 0) then
    begin
      //Geral.MB_Teste(Arquivo);
      //
      if FatID = VAR_FATID_0053 then
      begin
        if UnNFeImporta_0400.ImportaDadosNFeDeXML(FtpEmissNF, FtpNF,
        MeWarn, MeInfo, MeAusentes, Texto, Self, FatID, FatNum,
        Empresa, IDCtrl, CodInfoEmit, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
        SumPesoLIts, DataFiscal, TabLcta) then
        begin
          FIDCtrl := IDCtrl;
        end;
      end else
        Geral.MB_Info('Modelo de NFe n�o implementado para importa��o pelo XML (A)!');
    end else
      Geral.MB_Info('Dados indefinidos para download de NFe �nica!');
  end;
end;

procedure TFmNFe_DownXML.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFe_DownXML.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFe_DownXML.FormCreate(Sender: TObject);
var
  Diretorio, DefValue: String;
begin
  ImgTipo.SQLType := stLok;
  FtpNF := TtpNF.tpnfEntrada;
  FtpEmissNF := TtpEmissNF.tpemiTerceiros;
  //
  FArqUnico       := EmptyStr;
  FFatIdUnico     := 0;
  FFatNumUnico    := 0;
  FEmpresaUnica   := 0;
  FIDCtrl         := 0;
  //
  DefValue := System.IOUtils.TPath.GetDownloadsPath;
  //
  Diretorio := Geral.ReadAppKeyCU('DownloadXmlNFeDir', Application.Title,
    ktString, DefValue);
  EdDiretorio.ValueVariant := Diretorio ;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmNFe_DownXML.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFe_DownXML.FormShow(Sender: TObject);
begin
  if FArqUnico <> EmptyStr then
    EdArquivo.ValueVariant := FArqUnico;
end;

procedure TFmNFe_DownXML.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqalocits ',
  '']);
end;

procedure TFmNFe_DownXML.SbPastaClick(Sender: TObject);
var
  Diretorio: String;
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
  Diretorio := EdDiretorio.ValueVariant;
  Geral.WriteAppKeyCU('DownloadXmlNFeDir', Application.Title, Diretorio, ktString);
end;

end.
