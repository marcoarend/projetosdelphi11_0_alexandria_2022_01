unit NFeConsulta_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, xmldom, XMLIntf, msxmldom, XMLDoc, UnDmkEnums;

type
  TFmNFeConsulta_0400 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    EdChaveNFe: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    Label2: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Button1: TButton;
    Memo2: TMemo;
    Memo1: TMemo;
    XMLDoc1: TXMLDocument;
    Memo3: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

  var
  FmNFeConsulta_0400: TFmNFeConsulta_0400;

implementation

uses UnMyObjects, Module, ModuleGeral, NFeSteps_0400, NFeGeraXML_0400,
  MyDBCheck, NFeLeXML_0400, ModuleNFe_0000, NFe_PF;

{$R *.DFM}

const
  XML_Teste = '<?xml version="1.0" encoding="UTF-8"?>' +
  //'<NFe xmlns="http://www.portalfiscal.inf.br/nfe">'+
  '<retConsSitNFe versao="2.01" xmlns="http://www.portalfiscal.inf.' +
  'br/nfe"><tpAmb>2</tpAmb><verAplic>SVAN_4.02</verAplic><cStat>101' +
  '</cStat><xMotivo>Cancelamento de NF-e homologado</xMotivo><cUF>1' +
  '5</cUF><chNFe>15120304333952000188550090000002031780004121</chNF' +
  'e><retCancNFe versao="2.00" xmlns="http://www.portalfiscal.inf.b' +
  'r/nfe"><infCanc Id="ID415120000007429"><tpAmb>2</tpAmb><verAplic' +
  '>SVAN_4.02</verAplic><cStat>101</cStat><xMotivo>Cancelamento de ' +
  'NF-e homologado</xMotivo><cUF>15</cUF><chNFe>1512030433395200018' +
  '8550090000002031780004121</chNFe><dhRecbto>2012-03-28T18:02:43</' +
  'dhRecbto><nProt>415120000007429</nProt></infCanc></retCancNFe><p' +
  'rocEventoNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' +
  '1.00"><evento xmlns="http://www.portalfiscal.inf.br/nfe" versao=' +
  '"1.00"><infEvento Id="ID1101101512030433395200018855009000000203' +
  '178000412101"><cOrgao>15</cOrgao><tpAmb>2</tpAmb><CNPJ>043339520' +
  '00188</CNPJ><chNFe>15120304333952000188550090000002031780004121<' +
  '/chNFe><dhEvento>2012-03-28T18:01:38-03:00</dhEvento><tpEvento>1' +
  '10110</tpEvento><nSeqEvento>1</nSeqEvento><verEvento>1.00</verEv' +
  'ento><detEvento versao="1.00"><descEvento>Carta de Correcao</des' +
  'cEvento><xCorrecao>TESTE DE CARAT DE CORRECAO DE NF-E QUE SERA E' +
  'XCLUIDA PARA GERAR HISTORICO DE AUTORIZACAO, CARTA DE CORRECAO E' +
  ' CANCELAMENTO.</xCorrecao><xCondUso>A Carta de Correcao e discip' +
  'linada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de ' +
  'dezembro de 1970 e pode ser utilizada para regularizacao de erro' +
  ' ocorrido na emissao de documento fiscal, desde que o erro nao e' +
  'steja relacionado com: I - as variaveis que determinam o valor d' +
  'o imposto tais como: base de calculo, aliquota, diferenca de pre' +
  'co, quantidade, valor da operacao ou da prestacao; II - a correc' +
  'ao de dados cadastrais que implique mudanca do remetente ou do d' +
  'estinatario; III - a data de emissao ou de saida.</xCondUso></de' +
  'tEvento></infEvento><Signature xmlns="http://www.w3.org/2000/09/' +
  'xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://' +
  'www.w3.org/TR/2001/REC-xml-c14n-20010315" /><SignatureMethod Alg' +
  'orithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" /><Reference' +
  ' URI="#ID1101101512030433395200018855009000000203178000412101"><' +
  'Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmlds' +
  'ig#enveloped-signature" /><Transform Algorithm="http://www.w3.or' +
  'g/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Al' +
  'gorithm="http://www.w3.org/2000/09/xmldsig#sha1" /><DigestValue>' +
  'tu/9PWqdl29Q5hSvYJ0ZXEQ5NPc=</DigestValue></Reference></SignedIn' +
  'fo><SignatureValue>frOqyp2n0mxoJq95IfPF8J5R3DryWeYjb7mpFLrLQnz8v' +
  'o67T390RDtJnExkuMi2HhsjFyFgsWM87kXKv8xKgJiwi8Zh05r2BtdfEDAB6QC21' +
  '6/45F4p7+aoHzomT4DCMzL4fMTbQCvdAxyA19wRD0fyfKnuptd1ZZiUT7A6tA0=<' +
  '/SignatureValue><KeyInfo><X509Data><X509Certificate>MIIGwzCCBaug' +
  'AwIBAgIQJam1fDP1yBIERbWYSRo2kDANBgkqhkiG9w0BAQUFADB0MQswCQYDVQQG' +
  'EwJCUjETMBEGA1UEChMKSUNQLUJyYXNpbDEtMCsGA1UECxMkQ2VydGlzaWduIENl' +
  'cnRpZmljYWRvcmEgRGlnaXRhbCBTLkEuMSEwHwYDVQQDExhBQyBDZXJ0aXNpZ24g' +
  'TXVsdGlwbGEgRzMwHhcNMTEwODAzMDAwMDAwWhcNMTIwODAxMjM1OTU5WjCCAQIx' +
  'CzAJBgNVBAYTAkJSMRMwEQYDVQQKFApJQ1AtQnJhc2lsMRUwEwYDVQQLFAxJRCAt' +
  'IDE4MjE2MzMxLTArBgNVBAsUJEF1dGVudGljYWRvIHBvciBBUiBJbnN0aXR1dG8g' +
  'RmVuYWNvbjEbMBkGA1UECxQSQXNzaW5hdHVyYSBUaXBvIEExMRQwEgYDVQQLFAso' +
  'ZW0gYnJhbmNvKTEUMBIGA1UECxQLKGVtIGJyYW5jbykxJjAkBgNVBAMTHU0gSiBO' +
  'T1ZBRVMgREUgTElNQSBFIENJQSBMVERBMScwJQYJKoZIhvcNAQkBFhhqYW5ldGVi' +
  'aWNoYUB5YWhvby5jb20uYnIwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAKQG' +
  'WfHIY4vDDdijm7rQRMpwPn1iqV5uPCBm7+VNiuvJvEXsS2bIo5PnC4RBJ8F5a3lF' +
  'mh7DADcoWtfVKILUXp4mYfj5hDtGNDCuTFu087GYPzIEuOBP847A9S/pFP2h/lgZ' +
  'PhDS6PFtNAKqiF8ROMRzsrfSuS9cc6p/Px6C0UAVAgMBAAGjggNDMIIDPzCBvgYD' +
  'VR0RBIG2MIGzoD0GBWBMAQMEoDQEMjA4MDYxOTczMzc3OTc0NjgyNDkwMDAwMDAw' +
  'MDAwMDAwMDAwMDAwMjQyMTcyMFNTUFBBoCQGBWBMAQMCoBsEGU1BUklBIEpPU0Ug' +
  'Tk9WQUVTIERFIExJTUGgGQYFYEwBAwOgEAQOMDQzMzM5NTIwMDAxODigFwYFYEwB' +
  'AwegDgQMMDAwMDAwMDAwMDAwgRhqYW5ldGViaWNoYUB5YWhvby5jb20uYnIwCQYD' +
  'VR0TBAIwADAfBgNVHSMEGDAWgBSEsEIzNKNCJaUolz6D63fw6E/CVDAOBgNVHQ8B' +
  'Af8EBAMCBeAwVQYDVR0gBE4wTDBKBgZgTAECAQswQDA+BggrBgEFBQcCARYyaHR0' +
  'cDovL2ljcC1icmFzaWwuY2VydGlzaWduLmNvbS5ici9yZXBvc2l0b3Jpby9kcGMw' +
  'ggElBgNVHR8EggEcMIIBGDBcoFqgWIZWaHR0cDovL2ljcC1icmFzaWwuY2VydGlz' +
  'aWduLmNvbS5ici9yZXBvc2l0b3Jpby9sY3IvQUNDZXJ0aXNpZ25NdWx0aXBsYUcz' +
  'L0xhdGVzdENSTC5jcmwwW6BZoFeGVWh0dHA6Ly9pY3AtYnJhc2lsLm91dHJhbGNy' +
  'LmNvbS5ici9yZXBvc2l0b3Jpby9sY3IvQUNDZXJ0aXNpZ25NdWx0aXBsYUczL0xh' +
  'dGVzdENSTC5jcmwwW6BZoFeGVWh0dHA6Ly9yZXBvc2l0b3Jpby5pY3BicmFzaWwu' +
  'Z292LmJyL2xjci9DZXJ0aXNpZ24vQUNDZXJ0aXNpZ25NdWx0aXBsYUczL0xhdGVz' +
  'dENSTC5jcmwwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMIGgBggrBgEF' +
  'BQcBAQSBkzCBkDAoBggrBgEFBQcwAYYcaHR0cDovL29jc3AuY2VydGlzaWduLmNv' +
  'bS5icjBkBggrBgEFBQcwAoZYaHR0cDovL2ljcC1icmFzaWwuY2VydGlzaWduLmNv' +
  'bS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvQUNfQ2VydGlzaWduX011bHRp' +
  'cGxhX0czLnA3YzANBgkqhkiG9w0BAQUFAAOCAQEAQLdmRhyME0kw88W8GqpNwzmu' +
  '2KmLpbvofs+T3Et4FG7TbKcQIfLF3I16W8HVaki/OVKEyJqrMkwce+V5uY1Nvq1N' +
  'EeAGYPvKXZKsS1Et9EpfyxeokQqEssyfCZyuIcHwUbJtc/0dwLYncJrLzngaqLK9' +
  'ZTz7TUZj3M8EIs/5BjdTvUzvHworkCw7NsU651Ljtt/ZE3958YsvtWw76PjoUIWI' +
  'laHq7Dvn03/z6czVN9U5PkctJKcu9W+YS1k1Mv5Y/uYdhSRFPvykL3TlSnDk57Ce' +
  'yRbwlvexFersWx7SGZpfK1j7J87xLlxGfNJQCnVBjyE9X1NSFTL6Qs7sj2IrkQ==' +
  '</X509Certificate></X509Data></KeyInfo></Signature></evento><ret' +
  'Evento versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe">' +
  '<infEvento Id="ID415120000007428"><tpAmb>2</tpAmb><verAplic>SVAN' +
  '_4.02</verAplic><cOrgao>15</cOrgao><cStat>135</cStat><xMotivo>Ev' +
  'ento registrado e vinculado a NF-e</xMotivo><chNFe>1512030433395' +
  '2000188550090000002031780004121</chNFe><tpEvento>110110</tpEvent' +
  'o><nSeqEvento>1</nSeqEvento><dhRegEvento>2012-03-28T18:02:11-03:' +
  '00</dhRegEvento><nProt>415120000007428</nProt></infEvento></retE' +
  'vento></procEventoNFe></retConsSitNFe>';

  XML_Teste2 =  
  '<?xml version="1.0" encoding="UTF-8"?>'+
  '<Nadadores xmlns="http://www.dermatek.com.br/www/apps/swms"><infNadadores versao="0.08"><ideNadadores'+
  '><Auto_Incremento>39</Auto_Incremento></ideNadadores><detNadador fld_Int_Codigo="1"><CadNadador><fld_'+
  'Str_CodTxt>1.1.03</fld_Str_CodTxt><fld_Str_Nome>Isabela Pretel</fld_Str_Nome><fld_Num_CPF>00000000000'+
  '</fld_Num_CPF><fld_Dta_Nascim>2006-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone>'+
  '</fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="2"><CadNadador><fld_Str_CodTxt>3'+
  '.1.02</fld_Str_CodTxt><fld_Str_Nome>Beatriz Capelasso</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num'+
  '_CPF><fld_Dta_Nascim>2005-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num'+
  '_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="3"><CadNadador><fld_Str_CodTxt>4.1.02</fl'+
  'd_Str_CodTxt><fld_Str_Nome>Rafael Grava</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_'+
  'Nascim>2005-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNad'+
  'ador></detNadador><detNadador fld_Int_Codigo="4"><CadNadador><fld_Str_CodTxt>5.1.05</fld_Str_CodTxt><'+
  'fld_Str_Nome>Bianca Barczcz</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2004-'+
  '01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNa'+
  'dador><detNadador fld_Int_Codigo="5"><CadNadador><fld_Str_CodTxt>6.1.02</fld_Str_CodTxt><fld_Str_Nome'+
  '>Enzo Idogawa (Prancha)</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2004-01-0'+
  '1</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadado'+
  'r><detNadador fld_Int_Codigo="6"><CadNadador><fld_Str_CodTxt>6.2.02</fld_Str_CodTxt><fld_Str_Nome>Art'+
  'hur Ueda</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2004-01-01</fld_Dta_Nasc'+
  'im><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador f'+
  'ld_Int_Codigo="7"><CadNadador><fld_Str_CodTxt>6.2.03</fld_Str_CodTxt><fld_Str_Nome>Bruno Schiavoni</f'+
  'ld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2004-01-01</fld_Dta_Nascim><fld_Chr'+
  '_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codi'+
  'go="8"><CadNadador><fld_Str_CodTxt>6.2.04</fld_Str_CodTxt><fld_Str_Nome>Enzo Montaneri</fld_Str_Nome>'+
  '<fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2004-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld'+
  '_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="9"><CadN'+
  'adador><fld_Str_CodTxt>7.1.02</fld_Str_CodTxt><fld_Str_Nome>Juliana Rodrigues</fld_Str_Nome><fld_Num_'+
  'CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2003-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo'+
  '><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="10"><CadNadador><'+
  'fld_Str_CodTxt>9.1.01</fld_Str_CodTxt><fld_Str_Nome>Beatriz Vespa</fld_Str_Nome><fld_Num_CPF>00000000'+
  '000</fld_Num_CPF><fld_Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fo'+
  'ne></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="11"><CadNadador><fld_Str_CodT'+
  'xt>9.1.03</fld_Str_CodTxt><fld_Str_Nome>Gabriela Bayer</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Nu'+
  'm_CPF><fld_Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Nu'+
  'm_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="12"><CadNadador><fld_Str_CodTxt>9.2.03</'+
  'fld_Str_CodTxt><fld_Str_Nome>Isis M. Garcia</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_'+
  'Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="13"><CadNadador><fld_Str_CodTxt>10.1.02</fld_Str_CodTxt><fld_Str_Nome>Edua'+
  'rdo Moleirinho</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="14'+
  '"><CadNadador><fld_Str_CodTxt>10.2.02</fld_Str_CodTxt><fld_Str_Nome>Fernando Medeiros</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone>'+
  '</fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="15"><CadNadador><fld_Str_CodTxt>011.1.02</fld_Str_CodTxt><fld_Str_Nome>Larissa Moleirinho</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nasci'+
  'm>2001-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="16"><CadNadador><fld_Str_CodTxt>11.1.03</fld_Str_CodTxt><fld_Str_Nome>Luiza Borin</'+
  'fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2001-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="17"><CadNadador><f'+
  'ld_Str_CodTxt>011.2.01</fld_Str_CodTxt><fld_Str_Nome>Camila Freitas</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2001-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></C'+
  'adNadador></detNadador><detNadador fld_Int_Codigo="18"><CadNadador><fld_Str_CodTxt>012.1.01</fld_Str_CodTxt><fld_Str_Nome>Gabreil Souza Brianezi</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2001-01-01</'+
  'fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="19"><CadNadador><fld_Str_CodTxt>080.1.03</fld_Str_CodTxt><fld_Str_Nome>Pedro Obertier</fld_Str_No'+
  'me><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2006-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="20"><CadNadador><fld_Str_Cod'+
  'Txt>015.1.03</fld_Str_CodTxt><fld_Str_Nome>Ana Maria Crozatti</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1999-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNada'+
  'dor></detNadador><detNadador fld_Int_Codigo="21"><CadNadador><fld_Str_CodTxt>016.1.02</fld_Str_CodTxt><fld_Str_Nome>Matheus Tuzuki da Silva</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1999-01-01</fld_D'+
  'ta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="22"><CadNadador><fld_Str_CodTxt>016.1.04</fld_Str_CodTxt><fld_Str_Nome>Lucas Pires de Oliveira</fld_St'+
  'r_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1999-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="23"><CadNadador><fld_Str'+
  '_CodTxt>018.1.01</fld_Str_CodTxt><fld_Str_Nome>Pedro Chiarotti Reis</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1998-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></C'+
  'adNadador></detNadador><detNadador fld_Int_Codigo="24"><CadNadador><fld_Str_CodTxt>018.2.02</fld_Str_CodTxt><fld_Str_Nome>Andr� Mendes</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1998-01-01</fld_Dta_Na'+
  'scim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="25"><CadNadador><fld_Str_CodTxt>018.3.03</fld_Str_CodTxt><fld_Str_Nome>Gustavo Souza Alves</fld_Str_Nome><f'+
  'ld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1998-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="26"><CadNadador><fld_Str_CodTxt>0'+
  '22.1.02</fld_Str_CodTxt><fld_Str_Nome>Danilo do Amaral</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1996-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></d'+
  'etNadador><detNadador fld_Int_Codigo="27"><CadNadador><fld_Str_CodTxt>025.1.02</fld_Str_CodTxt><fld_Str_Nome>Sarah Santa Barbara</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1993-01-01</fld_Dta_Nascim><'+
  'fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="28"><CadNadador><fld_Str_CodTxt>025.1.03</fld_Str_CodTxt><fld_Str_Nome>Fernanda Santa Barbara</fld_Str_Nome><fld_'+
  'Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1994-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="29"><CadNadador><fld_Str_CodTxt>059.'+
  '1.04</fld_Str_CodTxt><fld_Str_Nome>Emily Bazan Montanari</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2003-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador><'+
  '/detNadador><detNadador fld_Int_Codigo="30"><CadNadador><fld_Str_CodTxt>060.1.02</fld_Str_CodTxt><fld_Str_Nome>Guilherme Haida</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2003-01-01</fld_Dta_Nascim><fl'+
  'd_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="31"><CadNadador><fld_Str_CodTxt>061.1.01</fld_Str_CodTxt><fld_Str_Nome>D�bora Borek Correa</fld_Str_Nome><fld_Num_C'+
  'PF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2002-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="32"><CadNadador><fld_Str_CodTxt>064.1.02<'+
  '/fld_Str_CodTxt><fld_Str_Nome>Leonardo Lagos</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2001-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador>'+
  '<detNadador fld_Int_Codigo="33"><CadNadador><fld_Str_CodTxt>065.1.03</fld_Str_CodTxt><fld_Str_Nome>Daiane Naomi Yamada Hakutake</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2000-01-01</fld_Dta_Nascim><f'+
  'ld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="34"><CadNadador><fld_Str_CodTxt>066.1.02</fld_Str_CodTxt><fld_Str_Nome>Matheus Sanatana</fld_Str_Nome><fld_Num_CPF'+
  '>00000000000</fld_Num_CPF><fld_Dta_Nascim>2000-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="35"><CadNadador><fld_Str_CodTxt>067.1.02</f'+
  'ld_Str_CodTxt><fld_Str_Nome>Bruna Suzuki</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1999-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><det'+
  'Nadador fld_Int_Codigo="36"><CadNadador><fld_Str_CodTxt>068.1.02</fld_Str_CodTxt><fld_Str_Nome>Leonardo Marques</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1999-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</f'+
  'ld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="37"><CadNadador><fld_Str_CodTxt>081.1.01</fld_Str_CodTxt><fld_Str_Nome>Isabela Medeiros</fld_Str_Nome><fld_Num_CPF>00000000000</fl'+
  'd_Num_CPF><fld_Dta_Nascim>2005-01-01</fld_Dta_Nascim><fld_Chr_Sexo>F</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><detNadador fld_Int_Codigo="38"><CadNadador><fld_Str_CodTxt>101.1.03</fld_Str_CodTxt><f'+
  'ld_Str_Nome>Marco Aur�lio dos Santos Silva</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>1995-01-01</fld_Dta_Nascim><fld_Chr_Sexo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><d'+
  'etNadador fld_Int_Codigo="39"><CadNadador><fld_Str_CodTxt>000.0.00</fld_Str_CodTxt><fld_Str_Nome>Teste cadastro nadador</fld_Str_Nome><fld_Num_CPF>00000000000</fld_Num_CPF><fld_Dta_Nascim>2000-01-01</fld_Dta_Nascim><fld_Chr_S'+
  'exo>M</fld_Chr_Sexo><fld_Num_Fone></fld_Num_Fone></CadNadador></detNadador><fimNadadores><qtdRegistros>39</qtdRegistros></fimNadadores></infNadadores></Nadadores>'+
  '';

procedure TFmNFeConsulta_0400.BtOKClick(Sender: TObject);
const
  IDCtrl = 0; // ???
var
  Empresa, ide_mod: Integer;
  ChaveNFe: String;
begin
  if not DModG.DefineEmpresaSelecionada(EdEmpresa, Empresa) then
    Exit;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if not DmNFe_0000.ReopenEmpresa(Empresa) then
    Exit;
  //
  ChaveNFe := Geral.SoNumero_TT(EdChaveNFe.Text);
  Geral.ide_mod_de_ChaveNFe(ChaveNFe, ide_mod);

  //
  UnNFe_PF.MostraFormStepsNFe_ConsultaNFe(Empresa, IDCtrl, ide_mod, ChaveNFe, Memo1);
end;

procedure TFmNFeConsulta_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeConsulta_0400.Button1Click(Sender: TObject);
var
  ChaveNFe: String;
  XML: WideString;
  Empresa: Integer;
begin
  if not DModG.DefineEmpresaSelecionada(EdEmpresa, Empresa) then
    Exit;
  //
  if not DmNFe_0000.ReopenEmpresa(Empresa) then
    Exit;
  //
  ChaveNFe := Geral.SoNumero_TT(EdChaveNFe.Text);
  if MyObjects.FIC(Length(ChaveNFe) <> 44, EdChaveNFe, 'Informe a chave da NFe') then
    Exit;
  XML := XML_Teste;
  //
  Memo3.Lines.Clear;
  UnNFeLeXML_0400.LeXML(retConsSitNFe, XMLDoc1, NFE_EXT_PED_STA_XML, ChaveNFe,
    XML, Memo2, False, Memo3);
end;

procedure TFmNFeConsulta_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmNFeConsulta_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
