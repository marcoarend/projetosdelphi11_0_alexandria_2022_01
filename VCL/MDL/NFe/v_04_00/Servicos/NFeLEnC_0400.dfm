object FmNFeLEnC_0400: TFmNFeLEnC_0400
  Left = 368
  Top = 194
  Caption = 'NFe-LOTES-001 :: Lotes de NF-e'
  ClientHeight = 654
  ClientWidth = 1066
  Color = clBtnFace
  Constraints.MinHeight = 341
  Constraints.MinWidth = 841
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 113
    Width = 1066
    Height = 541
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1066
      Height = 121
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 11
        Top = 4
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 73
        Top = 4
        Width = 57
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 157
        Top = 4
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 11
        Top = 47
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 11
        Top = 21
        Width = 59
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 157
        Top = 21
        Width = 622
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 73
        Top = 21
        Width = 78
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdEmpresa: TdmkEditCB
        Left = 11
        Top = 64
        Width = 59
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 73
        Top = 64
        Width = 705
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 468
      Width = 1066
      Height = 73
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1062
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 872
          Top = 0
          Width = 190
          Height = 56
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 2
            Width = 128
            Height = 43
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 11
          Top = 4
          Width = 128
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
    object RGIndSinc: TdmkRadioGroup
      Left = 11
      Top = 90
      Width = 267
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Envio da NF-e ao Fisco (NFe 3.10):'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Ass'#237'ncrono'
        'S'#237'ncrono')
      TabOrder = 2
      QryCampo = 'IndSinc'
      UpdCampo = 'IndSinc'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 113
    Width = 1066
    Height = 541
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1066
      Height = 206
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 778
        Height = 206
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 6
          Top = 4
          Width = 14
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 144
          Top = 4
          Width = 51
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label3: TLabel
          Left = 64
          Top = 4
          Width = 36
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdNome
        end
        object Label5: TLabel
          Left = 6
          Top = 47
          Width = 20
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdNome: TDBEdit
          Left = 64
          Top = 21
          Width = 77
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CodUsu'
          DataSource = DsNFeLEnC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 6
          Top = 64
          Width = 56
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Filial'
          DataSource = DsNFeLEnC
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 64
          Top = 64
          Width = 501
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_Empresa'
          DataSource = DsNFeLEnC
          TabOrder = 2
        end
        object GroupBox1: TGroupBox
          Left = 6
          Top = 92
          Width = 490
          Height = 105
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Resposta do envio do XML: '
          TabOrder = 3
          object Label11: TLabel
            Left = 6
            Top = 17
            Width = 85
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o do leiaute:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 103
            Top = 17
            Width = 85
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de ambiente:'
            FocusControl = DBEdit7
          end
          object Label13: TLabel
            Left = 273
            Top = 17
            Width = 162
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o do aplic. que receb. o lote:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 6
            Top = 60
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Resposta:'
            FocusControl = DBEdit9
          end
          object Label15: TLabel
            Left = 453
            Top = 17
            Width = 17
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'UF:'
            FocusControl = DBEdit12
          end
          object DBEdit6: TDBEdit
            Left = 6
            Top = 34
            Width = 95
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'versao'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 103
            Top = 34
            Width = 22
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'tpAmb'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 273
            Top = 34
            Width = 175
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'verAplic'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 6
            Top = 77
            Width = 43
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'cStat'
            DataSource = DsNFeLEnC
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 49
            Top = 77
            Width = 434
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'xMotivo'
            DataSource = DsNFeLEnC
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 126
            Top = 34
            Width = 143
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_Ambiente'
            DataSource = DsNFeLEnC
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 453
            Top = 34
            Width = 29
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'cUF'
            DataSource = DsNFeLEnC
            TabOrder = 6
          end
        end
        object GroupBox2: TGroupBox
          Left = 495
          Top = 92
          Width = 274
          Height = 105
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Recibo de aceite do lote: '
          TabOrder = 4
          object Label16: TLabel
            Left = 6
            Top = 17
            Width = 87
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#250'mero do recibo:'
            FocusControl = DBEdit13
          end
          object Label10: TLabel
            Left = 6
            Top = 60
            Width = 58
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data / hora:'
            FocusControl = DBEdit5
          end
          object Label17: TLabel
            Left = 181
            Top = 60
            Width = 84
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 't m'#233'd. resp. (seg):'
            FocusControl = DBEdit14
          end
          object DBEdit13: TDBEdit
            Left = 6
            Top = 33
            Width = 262
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'nRec'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 6
            Top = 77
            Width = 171
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'DataHora_TXT'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 181
            Top = 77
            Width = 89
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'tMed'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
        end
        object DBEdit4: TDBEdit
          Left = 144
          Top = 21
          Width = 621
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Nome'
          DataSource = DsNFeLEnC
          TabOrder = 5
        end
        object DBEdCodigo: TDBEdit
          Left = 6
          Top = 21
          Width = 56
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsNFeLEnC
          TabOrder = 6
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 571
          Top = 47
          Width = 198
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Envio da NF-e ao Fisco (NFe 3.10):'
          Columns = 2
          DataField = 'indSinc'
          DataSource = DsNFeLEnC
          Items.Strings = (
            'Ass'#237'ncrono'
            'S'#237'ncrono')
          TabOrder = 7
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
      end
      object DBGrid2: TDBGrid
        Left = 778
        Top = 0
        Width = 288
        Height = 206
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsNFeLEnM
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 206
      Width = 1066
      Height = 254
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Informa'#231#245'es das notas do lote '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1058
          Height = 226
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsNFeCabA
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'rie'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'Nota fiscal'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSTot_vNF'
              Title.Caption = 'Valor total'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'id'
              Title.Caption = 'Chave de acesso da NF-e'
              Width = 286
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_cStat'
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_xMotivo'
              Width = 704
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1058
          Height = 226
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 991
          ExplicitHeight = 210
          ControlData = {
            4C000000596D00005C1700000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 457
      Width = 1066
      Height = 84
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 191
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 139
          Top = 5
          Width = 42
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 94
          Top = 5
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 49
          Top = 5
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 43
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 193
        Top = 15
        Width = 118
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 311
        Top = 15
        Width = 753
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 610
          Top = 0
          Width = 143
          Height = 67
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 128
            Height = 43
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Tag = 570
          Left = 9
          Top = 5
          Width = 128
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtNFe: TBitBtn
          Tag = 441
          Left = 141
          Top = 5
          Width = 128
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&NFe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeClick
        end
        object BtXML: TBitBtn
          Tag = 512
          Left = 271
          Top = 5
          Width = 128
          Height = 43
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1066
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1010
      Top = 0
      Width = 56
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 12
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 231
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 5
        Width = 43
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 49
        Top = 5
        Width = 43
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 94
        Top = 5
        Width = 43
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 139
        Top = 5
        Width = 42
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 183
        Top = 5
        Width = 43
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 231
      Top = 0
      Width = 779
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 184
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 184
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 184
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 55
    Width = 1066
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1062
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 17
        Top = 2
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 15
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeLEnC: TDataSource
    DataSet = QrNFeLEnC
    Left = 40
    Top = 65524
  end
  object QrNFeLEnC: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeLEnCBeforeOpen
    AfterOpen = QrNFeLEnCAfterOpen
    AfterScroll = QrNFeLEnCAfterScroll
    OnCalcFields = QrNFeLEnCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfelenc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 65524
    object QrNFeLEnCDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrNFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfelenc.Codigo'
    end
    object QrNFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'nfelenc.CodUsu'
    end
    object QrNFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfelenc.Nome'
      Size = 30
    end
    object QrNFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfelenc.Empresa'
    end
    object QrNFeLEnCversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfelenc.versao'
      DisplayFormat = '0.00'
    end
    object QrNFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfelenc.tpAmb'
    end
    object QrNFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfelenc.verAplic'
    end
    object QrNFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
      Origin = 'nfelenc.cStat'
    end
    object QrNFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfelenc.xMotivo'
      Size = 255
    end
    object QrNFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
      Origin = 'nfelenc.cUF'
    end
    object QrNFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Origin = 'nfelenc.nRec'
      Size = 15
    end
    object QrNFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
      Origin = 'nfelenc.dhRecbto'
    end
    object QrNFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
      Origin = 'nfelenc.tMed'
    end
    object QrNFeLEnCFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeLEnCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeLEnCNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
    object QrNFeLEnCIndSinc: TSmallintField
      FieldName = 'IndSinc'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtXML
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PnEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 568
    Top = 476
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object Verificalotenofisco1: TMenuItem
      Caption = 'Verifica lote no fisco'
      Enabled = False
      OnClick = Verificalotenofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      object Enviodelote1: TMenuItem
        Caption = '&Processamento de lote (s'#237'ncrono ou ass'#237'ncrono)'
        OnClick = Enviodelote1Click
      end
      object Consultadelote1: TMenuItem
        Caption = '&Consulta de lote'
        OnClick = Consultadelote1Click
      end
    end
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 664
    Top = 476
    object IncluiNFeaoloteatual1: TMenuItem
      Caption = '&Inclui NFe ao lote atual'
      OnClick = IncluiNFeaoloteatual1Click
    end
    object RemoveNFesselecionadas1: TMenuItem
      Caption = '&Remove NF-e(s) selecionada(s)'
      OnClick = RemoveNFesselecionadas1Click
    end
  end
  object DsNFeLEnI: TDataSource
    DataSet = QrNFeLEnI
    Left = 512
    Top = 8
  end
  object QrNFeLEnI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 8
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, ide_nNF, ide_dEmi, '
      'id, ide_cNF, ide_serie, Status, ICMSTot_vNF,'
      'infProt_cStat, infProt_xMotivo, ide_Mod'
      'FROM nfecaba '
      'WHERE LoteEnv=:P0'
      '')
    Left = 428
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      DisplayFormat = '000000000'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeCabAid: TWideStringField
      FieldName = 'id'
      Size = 44
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
      DisplayFormat = '000'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 456
    Top = 8
  end
  object QrNFeLEnM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfelenm'
      'WHERE Codigo=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 544
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnMcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeLEnMxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeLEnMnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrNFeLEnMdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeLEnMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeLEnMversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeLEnMtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeLEnMverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeLEnMcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeLEnMtMed: TIntegerField
      FieldName = 'tMed'
    end
  end
  object DsNFeLEnM: TDataSource
    DataSet = QrNFeLEnM
    Left = 572
    Top = 8
  end
  object PMXML: TPopupMenu
    OnPopup = PMXMLPopup
    Left = 744
    Top = 472
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote de envio'
        OnClick = Lote1Click
      end
      object RetornodoLotedeenvio1: TMenuItem
        Caption = '&Retorno do Lote de envio'
        OnClick = RetornodoLotedeenvio1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaNFEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML da NFE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assina1Click
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ValidarXMLdaNFE1: TMenuItem
      Caption = 'Validar XML da NFE'
      object Assinada2: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada2Click
      end
      object Lotedeenvio1: TMenuItem
        Caption = '&Lote de envio'
        OnClick = Lotedeenvio1Click
      end
    end
  end
end
