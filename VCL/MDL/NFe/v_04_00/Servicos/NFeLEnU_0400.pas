unit NFeLEnU_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums, UnGrl_Consts, UnGrl_Vars, TypInfo,
  Variants;

type
  TFmNFeLEnU_0400 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    QrNFeLEnC: TmySQLQuery;
    QrNFeLEnCCodigo: TIntegerField;
    QrNFeLEnCCodUsu: TIntegerField;
    QrNFeLEnCNome: TWideStringField;
    QrNFeLEnCEmpresa: TIntegerField;
    QrNFeLEnCversao: TFloatField;
    QrNFeLEnCtpAmb: TSmallintField;
    QrNFeLEnCverAplic: TWideStringField;
    QrNFeLEnCcStat: TIntegerField;
    QrNFeLEnCxMotivo: TWideStringField;
    QrNFeLEnCcUF: TSmallintField;
    QrNFeLEnCnRec: TWideStringField;
    QrNFeLEnCdhRecbto: TDateTimeField;
    QrNFeLEnCtMed: TIntegerField;
    QrNFeLEnCcMsg: TWideStringField;
    QrNFeLEnCxMsg: TWideStringField;
    QrNFeLEnCindSinc: TSmallintField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DsNFeLEnC: TDataSource;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAMotivo: TWideStringField;
    DsNFeCabA: TDataSource;
    CheckBox7: TCheckBox;
    QrNFeCabAIDCtrl: TIntegerField;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeLEnCBeforeClose(DataSet: TDataSet);
    procedure QrNFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure DBEdit2Change(Sender: TObject);
    procedure DBEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure TraduzMensagem(CodErro: Integer);
  public
    { Public declarations }
    FIDCtrl: Integer;
    //
    function EnviarNFe(): Boolean;
    procedure ReabreNFeLEnC(Codigo: Integer);
    procedure ReopenNFeCabA();
    function Interrompe(Passo: Integer): Boolean;
  end;

  var
  FmNFeLEnU_0400: TFmNFeLEnU_0400;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule,
  ModuleNFe_0000, NFeSteps_0400, MyDBCheck, NFe_Pesq_0000, FatPedNFs_0400,
  DmkACBrNFeSteps_0400;

{$R *.DFM}

procedure TFmNFeLEnU_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLEnU_0400.DBEdit1Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeLEnC.State <> dsInactive then
  begin
    case QrNFeLEnCcStat.Value of
      0..99: Cor := clBlack;
      103: Cor := clGreen;
      104: Cor := clBlue;
      105: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit1.Font.Color := Cor;
    DBMemo1.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFeLEnU_0400.DBEdit2Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeCabA.State <> dsInactive then
  begin
    case QrNFeCabAStatus.Value of
      0..99: Cor := clBlack;
      100: Cor := clBlue;
      101..199: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit2.Font.Color := Cor;
    DBMemo2.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFeLEnU_0400.TraduzMensagem(CodErro: Integer);
begin
  case CodErro of
    232: Geral.MB_Aviso('Destinat�rio com IE n�o informado!' + slinebreak +
      'A UF iformou que o destinat�rio possui IE ativa na UF!');
  end;
end;

function TFmNFeLEnU_0400.EnviarNFe(): Boolean;
const
  sProcName = 'TFmNFeLEnU_0400.EnviarNFe()';
const
  Modelo = CO_MODELO_NFE_55;
var
  CodUsu, Codigo, Empresa, LoteEnv, FatID, FatNum, Status, infProt_cStat,
  indSinc: Integer;
  infProt_xMotivo: String;
  Sincronia: TXXeIndSinc;
  sMsg: String;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando XML da NF-e');
  if FmFatPedNFs_0400.QrNFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na NF-e 3.10
    if not FmFatPedNFs_0400.GerarNFe(True, True, True) then
      if Interrompe(1) then Exit;
    *)
  end else
  begin
    if not FmFatPedNFs_0400.GerarNFe(True, False, True) then
      if Interrompe(1) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := FmFatPedNFs_0400.QrNFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  Empresa := FmFatPedNFs_0400.QrFatPedNFsEmpresa.Value;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  indSinc := FmFatPedNFs_0400.QrFatPedCabindSinc.Value;
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(2) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando NF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreNFeLEnC(LoteEnv);
  FatID := FmFatPedNFs_0400.QrFatPedNFsCabA_FatID.Value;
  FatNum := FmFatPedNFs_0400.QrFatPedNFsOriCodi.Value;
  Status  := DmNFe_0000.stepNFeAdedLote();
  infProt_cStat   := 0; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  LoteEnv, Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(3) then Exit;
  CheckBox3.Checked := True;


  // ini 2022-03-24
  //Sincronia := TXXeIndSinc(indSinc);
  Sincronia := TXXeIndSinc.nisAssincrono;
  // fim 2022-03-24

  // ini 2022
  //SincronoACBr :=  Sincronia = TXXeIndSinc.nisSincrono;
  //
  // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
  case TFormaGerenXXe(VAR_DFeAppCode) of
    (*0*)fgxxeCAPICOM:
    begin
      //Result := EnviarNFe_EDmk(Modelo);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmNFeSteps_0400.PnLoteEnv.Visible := True;
        FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmNFeSteps_0400.Show;
        //
        FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmNFeSteps_0400.FFormChamou      := 'FmNFeLEnU_0400';
        //
        FmNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          FmNFeSteps_0400.Destroy;
          if Interrompe(4) then Exit;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmNFeSteps_0400.FSegundos < FmNFeSteps_0400.FSecWait do
          begin
            FmNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmNFeSteps_0400.Destroy;
      end else if
        Interrompe(5) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmNFeSteps_0400.PnLoteEnv.Visible := True;
          FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmNFeSteps_0400.Show;
          //
          FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmNFeSteps_0400.FFormChamou      := 'FmNFeLEnU_0400';
          //
          FmNFeSteps_0400.BtOKClick(Self);
          //
          FmNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(7) then Exit;
        end else if
          Interrompe(6) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := FmFatPedNFs_0400.QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
      //
    end;
    //(*1*)fgxxeCMaisMais:
    (*2*)fgxxeACBr:
    begin
      //Result := EnviarNFe_ACBr(Modelo);
      // DmNFe_0000.ACBrNFe1.Enviar(LoteEnv, ImprimirACBr, SincronoACBr, ZipadoACBr);
      // FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Retorno.RetornoWS;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
      if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
        FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmDmkACBrNFeSteps_0400.Show;
        //
        FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmDmkACBrNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFeLEnU_0400';
        //
        FmDmkACBrNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          FmDmkACBrNFeSteps_0400.Destroy;
          if Interrompe(4) then Exit;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmDmkACBrNFeSteps_0400.FSegundos < FmDmkACBrNFeSteps_0400.FSecWait do
          begin
            FmDmkACBrNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmDmkACBrNFeSteps_0400.Destroy;
      end else if
        Interrompe(5) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
          FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmDmkACBrNFeSteps_0400.Show;
          //
          FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmDmkACBrNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFeLEnU_0400';
          //
          FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          //
          FmDmkACBrNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(7) then Exit;
        end else if
          Interrompe(6) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := FmFatPedNFs_0400.QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
    end;
    (*?*)else
    begin
      sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
      Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
    end;
  end;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Localizando NF-e');
  //

  FIDCtrl := FmFatPedNFs_0400.QrNFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      //
      FmNFe_Pesq_0000.ReopenNFeCabA(FIDCtrl, True);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      if (FmNFe_Pesq_0000.QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (FmNFe_Pesq_0000.QrNFeCabA.RecordCount = 1) then
        begin
          if (FmNFe_Pesq_0000.QrNFeCabAStatus.Value = 100) then
          //if FmNFe_Pesq_0000.BtImprime.Enabled then
          begin
            //FmNFe_Pesq_0000.DefineFrx(FmNFe_Pesq_0000.frxA4A_002, ficMostra);
            FmNFe_Pesq_0000.DefineQual_frxNFe(ficMostra);
            CheckBox6.Checked := FmNFe_Pesq_0000.FDANFEImpresso;
            //
            FmNFe_Pesq_0000.BtEnviaClick(Self);
            //
            FmNFe_Pesq_0000.Destroy;
            CheckBox7.Checked := FmNFe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else begin
            FmNFe_Pesq_0000.Destroy;
            if Interrompe(12) then Exit;
          end;
        end else
        begin
          FmNFe_Pesq_0000.Destroy;
          if Interrompe(11) then Exit;
        end;
      end else
      begin
        FmNFe_Pesq_0000.Destroy;
        if Interrompe(10) then Exit;
      end;
    end else if
      Interrompe(9) then Exit;
    CheckBox5.Checked := True;
    //
    Result := True;
    //
  end else
    if Interrompe(8) then Exit;
end;
// fim 2022-02-13

procedure TFmNFeLEnU_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFeLEnU_0400.FormCreate(Sender: TObject);
begin
  FIDCtrl := 0;
end;

procedure TFmNFeLEnU_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeLEnU_0400.Interrompe(Passo: Integer): Boolean;
var
  Aviso: String;
begin
  Result := True;
  Aviso := 'ABORTADO NO PASSO ' + FormatFloat('0', Passo) +
    ' POR ERRO EM: ' + LaAviso1.Caption;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
  GBRodaPe.Visible := True;
  ReopenNFeCabA();
end;

procedure TFmNFeLEnU_0400.QrNFeLEnCBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmNFeLEnU_0400.QrNFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  ReopenNFeCabA();
end;

procedure TFmNFeLEnU_0400.ReopenNFeCabA();
begin
  if FIDCtrl <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
    'SELECT Status, IDCtrl, ',
    'IF(Status=infProt_cStat, infProt_xMotivo,  ',
    'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  ',
    'FROM nfecaba  ',
    'WHERE IDCtrl=' + FormatFloat('0', FIDCtrl),
    '']);
  end;
end;

procedure TFmNFeLEnU_0400.ReabreNFeLEnC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeLEnC, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfelenc ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
end;

end.
