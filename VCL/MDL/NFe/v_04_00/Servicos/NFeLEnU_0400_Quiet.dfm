object FmNFeLEnU_0400_Quiet: TFmNFeLEnU_0400_Quiet
  Left = 339
  Top = 185
  Caption = 'NFe-LOTES-004 :: Envio de NF-e '#218'nica'
  ClientHeight = 534
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 924
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 868
      Top = 0
      Width = 56
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 11
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 55
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 55
      Top = 0
      Width = 813
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 267
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 267
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 267
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 462
    Width = 924
    Height = 72
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    Visible = False
    object PnSaiDesis: TPanel
      Left = 733
      Top = 15
      Width = 189
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 4
        Width = 128
        Height = 43
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 731
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 55
    Width = 924
    Height = 349
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 924
      Height = 349
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 924
        Height = 349
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 920
          Height = 215
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 243
            Height = 215
            Align = alLeft
            TabOrder = 0
            object CheckBox1: TCheckBox
              Left = 16
              Top = 21
              Width = 267
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Gerar XML da NFC-e'
              TabOrder = 0
            end
            object CheckBox2: TCheckBox
              Left = 16
              Top = 47
              Width = 267
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Criar novo lote de envio.'
              TabOrder = 1
            end
            object CheckBox3: TCheckBox
              Left = 16
              Top = 74
              Width = 267
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Adicionar NFC-e ao lote.'
              TabOrder = 2
            end
            object CheckBox4: TCheckBox
              Left = 16
              Top = 100
              Width = 267
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Enviar lote ao Fisco.'
              TabOrder = 3
            end
            object CheckBox5: TCheckBox
              Left = 16
              Top = 126
              Width = 267
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Consultar lote no fisco.'
              TabOrder = 4
            end
            object CheckBox6: TCheckBox
              Left = 16
              Top = 153
              Width = 267
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Imprime DANFCE.'
              TabOrder = 5
            end
            object CheckBox7: TCheckBox
              Left = 16
              Top = 178
              Width = 267
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Envio de DANFCE e XML por email'
              TabOrder = 6
            end
          end
          object Panel7: TPanel
            Left = 243
            Top = 0
            Width = 677
            Height = 215
            Align = alClient
            TabOrder = 1
            object Label13: TLabel
              Left = 124
              Top = 9
              Width = 44
              Height = 13
              Caption = 'Empresa:'
              FocusControl = EdEmpresa
            end
            object Label10: TLabel
              Left = 38
              Top = 9
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = EdFatNum
            end
            object Label3: TLabel
              Left = 4
              Top = 9
              Width = 14
              Height = 13
              Caption = 'ID:'
              FocusControl = EdFatID
            end
            object Label4: TLabel
              Left = 175
              Top = 9
              Width = 29
              Height = 13
              Caption = 'IDCtrl:'
              FocusControl = EdIDCtrl
            end
            object EdFatNum: TdmkEdit
              Left = 38
              Top = 26
              Width = 81
              Height = 22
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdCampo = 'FatNum'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdFatNumRedefinido
            end
            object EdEmpresa: TdmkEdit
              Left = 124
              Top = 26
              Width = 48
              Height = 22
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdCampo = 'Empresa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdEmpresaRedefinido
            end
            object EdFatID: TdmkEdit
              Left = 4
              Top = 26
              Width = 30
              Height = 22
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdCampo = 'FatID'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdFatIDRedefinido
            end
            object EdIDCtrl: TdmkEdit
              Left = 175
              Top = 26
              Width = 107
              Height = 22
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdCampo = 'Empresa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdIDCtrlRedefinido
            end
            object ListBox1: TListBox
              Left = 337
              Top = 9
              Width = 325
              Height = 197
              ItemHeight = 13
              TabOrder = 4
            end
          end
        end
        object Panel8: TPanel
          Left = 2
          Top = 230
          Width = 920
          Height = 117
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 15
            Top = 2
            Width = 72
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Status do Lote:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 15
            Top = 54
            Width = 74
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Status da NF-e:'
            FocusControl = DBEdit2
          end
          object DBEdit1: TDBEdit
            Left = 16
            Top = 17
            Width = 42
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'cStat'
            DataSource = DsNFeLEnC
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnChange = DBEdit1Change
          end
          object DBEdit2: TDBEdit
            Left = 15
            Top = 68
            Width = 42
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Status'
            DataSource = DsNFeCabA
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnChange = DBEdit2Change
          end
          object DBMemo1: TDBMemo
            Left = 60
            Top = 17
            Width = 856
            Height = 36
            DataField = 'xMotivo'
            DataSource = DsNFeLEnC
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object DBMemo2: TDBMemo
            Left = 60
            Top = 66
            Width = 856
            Height = 36
            DataField = 'Motivo'
            DataSource = DsNFeCabA
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 404
    Width = 924
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 920
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 17
        Top = 2
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrNFeLEnC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeOpen = QrNFeLEnCBeforeOpen
    BeforeClose = QrNFeLEnCBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM nfelenc'
      'WHERE Codigo=:P0')
    Left = 280
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrNFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeLEnCversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrNFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
    end
    object QrNFeLEnCcMsg: TWideStringField
      FieldName = 'cMsg'
      Size = 4
    end
    object QrNFeLEnCxMsg: TWideStringField
      FieldName = 'xMsg'
      Size = 200
    end
    object QrNFeLEnCindSinc: TSmallintField
      FieldName = 'indSinc'
    end
  end
  object DsNFeLEnC: TDataSource
    DataSet = QrNFeLEnC
    Left = 280
    Top = 192
  end
  object QrNFeCabA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrNFeCabAAfterOpen
    SQL.Strings = (
      'SELECT Status, IF(Status=infProt_cStat, infProt_xMotivo,  '
      'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  '
      'FROM nfecaba  '
      'WHERE IDCtrl=106138 ')
    Left = 352
    Top = 148
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeCabAMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 352
    Top = 192
  end
  object frxNFCe_01_Autorizada_UniversLight_Condensed: TfrxReport
    Version = '2022.1'
    DataSet = Dmod.frxDsMaster
    DataSetName = 'frxDsMaster'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44135.834781759300000000
    ReportOptions.LastChange = 44135.834781759300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MDSeguro.Visible := <frxDsA."ICMSTot_vSeg"> <> 0;             ' +
        '                                                     '
      
        '  MDFrete.Visible := <frxDsA."ICMSTot_vFrete"> <> 0;            ' +
        '                                                      '
      
        '  MDOutros.Visible := <frxDsA."ICMSTot_vOutro"> <> 0;           ' +
        '                                                       '
      '  MDDesconto.Visible := <frxDsA."ICMSTot_vDesc"> <> 0;'
      '  //        '
      '  if (<LogoNFeExiste> = True) then'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxNFCe_01_Autorizada_UniversLight_CondensedGetValue
    Left = 794
    Top = 7
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 300.000000000000000000
      PaperSize = 256
      LeftMargin = 1.000000000000000000
      RightMargin = 1.000000000000000000
      Frame.Typ = []
      EndlessHeight = True
      LargeDesignHeight = True
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 205.984376460000000000
        Top = 18.897650000000000000
        Width = 291.023810000000000000
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708695590000000000
          Width = 143.622057010000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ: [frxDsA."EMIT_CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 90.708695590000000000
          Width = 147.401587010000000000
          Height = 13.228346460000000000
          DataField = 'emit_xNome'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 107.716572050000000000
          Width = 291.023810000000000000
          Height = 37.795282910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 145.511854960000000000
          Width = 260.787423540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento Auxiliar da Nota Fiscal de Consumidor Eletr'#244'nica')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 158.740260000000000000
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 158.740260000000000000
          Width = 117.165430000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 158.740260000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 158.740260000000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 158.740260000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 158.740260000000000000
          Width = 34.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Unit')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 36.661417322834650000
          Top = 17.007859370000000000
          Width = 188.976500000000000000
          Height = 72.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 249.448980000000000000
        Width = 291.023810000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 117.165430000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          DataField = 'prod_uCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'prod_vProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vProd"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 34.015735830000000000
          Height = 13.228346460000000000
          DataField = 'prod_vUnCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vUnCom"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 13.228346460000000000
          Width = 260.787570000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsV."InfAdProd"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 298.582870000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde. total de itens')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_QTDE_ITENS]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 563.149970000000000000
        Width = 291.023810000000000000
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_tPAG_TXT]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181126770000000000
          Height = 13.228346460000000000
          DataField = 'vPag'
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsYA."vPag"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 525.354670000000000000
        Width = 291.023810000000000000
        Condition = 'frxDsYA."FatID"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma pagamento')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181126770000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago R$')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 600.945270000000000000
        Width = 291.023810000000000000
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 164.409529370000000000
        Top = 623.622450000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consulte pela Chave de Acesso em')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 13.228346460000000000
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_URL_Consulta_NFCe]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CHAVE_NFCe_FORMATADA]')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 196.000000000000000000
          Height = 196.000000000000000000
          StretchMode = smActualHeight
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          DataField = 'AllTxtLink'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          HexData = 
            '68007400740070003A002F002F007700770077002E00660061007A0065006E00' +
            '640061002E00700072002E0067006F0076002E00620072002F006E0066006300' +
            '65002F007100720063006F00640065003F0070003D0034003100320030003100' +
            '3100310034003600300038003600380038003000300030003100300030003600' +
            '3500300030003100300030003000300030003100300036003800310034003500' +
            '39003300350032003300320030007C0032007C0032007C0033007C0034003400' +
            '4200390036004600450044003200420031003700420045004300380036004300' +
            '3900340041003800430044003600370043003800410037004600360039003800' +
            '3100410039003000370033003400340042003900360046004500440032004200' +
            '3100370042004500430038003600430039003400410038004300440036003700' +
            '43003800410037004600360039003800310041003900300037003300'
          Zoom = 0.400000000000000000
          FontScaled = True
          QuietZone = 0
          ColorBar = clBlack
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 41.574830000000000000
          Width = 173.858221340000000000
          Height = 35.905526460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsA."DEST_CNPJ_CPF_TIT"] [frxDsA."DEST_CNPJ_CPF_TXT"] - [frx' +
              'DsA."dest_xNome"] - [frxDsA."DEST_ENDERECO"] [frxDsA."dest_xMun"' +
              '] [frxDsA."dest_UF"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 77.480310080000000000
          Width = 173.858221340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'NFC-e n'#186' [FormatFloat('#39'000000'#39', <frxDsA."ide_nNF">)] S'#233'rie [Form' +
              'atFloat('#39'000'#39', <frxDsA."ide_serie">)] [FormatDateTime('#39'dd/mm/yyy' +
              'y'#39', <frxDsA."ide_dEmi">)] [FormatDateTime('#39'hh:nn:ss'#39', <frxDsA."i' +
              'de_hEmi">)]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 90.708720000000000000
          Width = 173.858221340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo de Autoriza'#231#227'o [frxDsA."infProt_nProt"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 103.937002990000000000
          Width = 173.858221340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data de Autoriza'#231#227'o [frxDsA."infProt_dhRecbto"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Top = 128.504020000000000000
          Width = 260.787411340000000000
          Height = 35.905509370000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."InfCpl_totTrib"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
      end
      object MDSeguro: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 336.378170000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor seguro R$')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
        end
      end
      object MDFrete: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 374.173470000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor Frete R$')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
        end
      end
      object MDOutros: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 411.968770000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor outros R$')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
        end
      end
      object MDDesconto: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 449.764070000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Desconto R$')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 487.559370000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor a Pagar R$')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 812.598950000000000000
        Width = 291.023810000000000000
        RowCount = 1
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsA."InfAdic_InfCpl"]')
          ParentFont = False
        end
      end
    end
  end
  object QrA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 456
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrACobr_Fat_NFat: TWideStringField
      FieldName = 'Cobr_Fat_NFat'
      Size = 60
    end
    object QrACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrAID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrAEMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IEST_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IEST_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_TXT'
      Size = 100
      Calculated = True
    end
    object QrADEST_CNPJ_CPF_TXT: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TXT'
      Size = 255
      Calculated = True
    end
    object QrADEST_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrADEST_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CEP_TXT'
      Calculated = True
    end
    object QrADEST_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_FONE_TXT'
      Size = 50
      Calculated = True
    end
    object QrADEST_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrATRANSPORTA_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrADOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrADOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
    object QrAide_DSaiEnt_Txt: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_DSaiEnt_Txt'
      Size = 10
      Calculated = True
    end
    object QrAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrAMODFRETE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MODFRETE_TXT'
      Calculated = True
    end
    object QrADEST_XMUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_XMUN_TXT'
      Size = 60
      Calculated = True
    end
    object QrANFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
    object QrAInfCpl_totTrib: TWideStringField
      FieldName = 'InfCpl_totTrib'
      Size = 255
    end
    object QrAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrAURL_Consulta: TWideStringField
      FieldName = 'URL_Consulta'
      Size = 255
    end
    object QrAAllTxtLink: TWideStringField
      FieldName = 'AllTxtLink'
      Size = 510
    end
    object QrADEST_CNPJ_CPF_TIT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TIT'
      Size = 30
      Calculated = True
    end
    object QrAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
      Required = True
    end
    object QrAide_dhSaiEntTZD: TFloatField
      FieldName = 'ide_dhSaiEntTZD'
      Required = True
    end
    object QrAide_idDest: TSmallintField
      FieldName = 'ide_idDest'
      Required = True
    end
    object QrAide_indFinal: TSmallintField
      FieldName = 'ide_indFinal'
      Required = True
    end
    object QrAide_indPres: TSmallintField
      FieldName = 'ide_indPres'
      Required = True
    end
    object QrAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
      Required = True
    end
    object QrAEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
      Required = True
    end
    object QrAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrAdest_indIEDest: TSmallintField
      FieldName = 'dest_indIEDest'
      Required = True
    end
    object QrAdest_IM: TWideStringField
      FieldName = 'dest_IM'
      Size = 15
    end
    object QrAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
      Required = True
    end
    object QrAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
      Required = True
    end
    object QrAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
      Required = True
    end
    object QrAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
      Required = True
    end
    object QrAICMSTot_vFCP: TFloatField
      FieldName = 'ICMSTot_vFCP'
      Required = True
    end
    object QrAICMSTot_vFCPST: TFloatField
      FieldName = 'ICMSTot_vFCPST'
      Required = True
    end
    object QrAICMSTot_vFCPSTRet: TFloatField
      FieldName = 'ICMSTot_vFCPSTRet'
      Required = True
    end
    object QrAICMSTot_vIPIDevol: TFloatField
      FieldName = 'ICMSTot_vIPIDevol'
      Required = True
    end
    object QrAISSQNtot_dCompet: TDateField
      FieldName = 'ISSQNtot_dCompet'
      Required = True
    end
    object QrAISSQNtot_vDeducao: TFloatField
      FieldName = 'ISSQNtot_vDeducao'
      Required = True
    end
    object QrAISSQNtot_vOutro: TFloatField
      FieldName = 'ISSQNtot_vOutro'
      Required = True
    end
    object QrAISSQNtot_vDescIncond: TFloatField
      FieldName = 'ISSQNtot_vDescIncond'
      Required = True
    end
    object QrAISSQNtot_vDescCond: TFloatField
      FieldName = 'ISSQNtot_vDescCond'
      Required = True
    end
    object QrAISSQNtot_vISSRet: TFloatField
      FieldName = 'ISSQNtot_vISSRet'
      Required = True
    end
    object QrAISSQNtot_cRegTrib: TSmallintField
      FieldName = 'ISSQNtot_cRegTrib'
      Required = True
    end
    object QrAExporta_XLocDespacho: TWideStringField
      FieldName = 'Exporta_XLocDespacho'
      Size = 60
    end
    object QrAprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
      Required = True
    end
    object QrAinfProt_dhRecbtoTZD: TFloatField
      FieldName = 'infProt_dhRecbtoTZD'
      Required = True
    end
    object QrAretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
      Required = True
    end
    object QrAinfCanc_dhRecbtoTZD: TFloatField
      FieldName = 'infCanc_dhRecbtoTZD'
      Required = True
    end
    object QrAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Required = True
    end
    object QrACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Required = True
    end
    object QrATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Required = True
    end
    object QrAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
      Required = True
    end
    object QrASegurExtra: TFloatField
      FieldName = 'SegurExtra'
      Required = True
    end
    object QrAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrADataFiscal: TDateField
      FieldName = 'DataFiscal'
      Required = True
    end
    object QrASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
      Required = True
    end
    object QrASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
      Required = True
    end
    object QrASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
      Required = True
    end
    object QrASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Required = True
      Size = 2
    end
    object QrASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Required = True
      Size = 4
    end
    object QrASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
      Required = True
    end
    object QrACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
      Required = True
    end
    object QrACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
      Required = True
    end
    object QrACriAForca: TSmallintField
      FieldName = 'CriAForca'
      Required = True
    end
    object QrACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
      Required = True
    end
    object QrACodInfoCliI: TIntegerField
      FieldName = 'CodInfoCliI'
      Required = True
    end
    object QrAOrdemServ: TIntegerField
      FieldName = 'OrdemServ'
      Required = True
    end
    object QrASituacao: TSmallintField
      FieldName = 'Situacao'
      Required = True
    end
    object QrAAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrANFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrANF_ICMSAlq: TFloatField
      FieldName = 'NF_ICMSAlq'
      Required = True
    end
    object QrANF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Size = 4
    end
    object QrAImportado: TSmallintField
      FieldName = 'Importado'
      Required = True
    end
    object QrANFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrANFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
      Required = True
    end
    object QrANFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
      Required = True
    end
    object QrANFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
      Required = True
    end
    object QrACOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrACOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrAVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrAEFD_INN_AnoMes: TIntegerField
      FieldName = 'EFD_INN_AnoMes'
      Required = True
    end
    object QrAEFD_INN_Empresa: TIntegerField
      FieldName = 'EFD_INN_Empresa'
      Required = True
    end
    object QrAEFD_INN_LinArq: TIntegerField
      FieldName = 'EFD_INN_LinArq'
      Required = True
    end
    object QrAICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrAICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrAEFD_EXP_REG: TWideStringField
      FieldName = 'EFD_EXP_REG'
      Required = True
      Size = 4
    end
    object QrAICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrAeveMDe_Id: TWideStringField
      FieldName = 'eveMDe_Id'
      Size = 17
    end
    object QrAeveMDe_tpAmb: TSmallintField
      FieldName = 'eveMDe_tpAmb'
      Required = True
    end
    object QrAeveMDe_verAplic: TWideStringField
      FieldName = 'eveMDe_verAplic'
    end
    object QrAeveMDe_cOrgao: TSmallintField
      FieldName = 'eveMDe_cOrgao'
      Required = True
    end
    object QrAeveMDe_cStat: TIntegerField
      FieldName = 'eveMDe_cStat'
      Required = True
    end
    object QrAeveMDe_xMotivo: TWideStringField
      FieldName = 'eveMDe_xMotivo'
      Size = 255
    end
    object QrAeveMDe_chNFe: TWideStringField
      FieldName = 'eveMDe_chNFe'
      Size = 44
    end
    object QrAeveMDe_tpEvento: TIntegerField
      FieldName = 'eveMDe_tpEvento'
      Required = True
    end
    object QrAeveMDe_xEvento: TWideStringField
      FieldName = 'eveMDe_xEvento'
      Size = 60
    end
    object QrAeveMDe_nSeqEvento: TSmallintField
      FieldName = 'eveMDe_nSeqEvento'
      Required = True
    end
    object QrAeveMDe_CNPJDest: TWideStringField
      FieldName = 'eveMDe_CNPJDest'
      Size = 18
    end
    object QrAeveMDe_CPFDest: TWideStringField
      FieldName = 'eveMDe_CPFDest'
      Size = 18
    end
    object QrAeveMDe_emailDest: TWideStringField
      FieldName = 'eveMDe_emailDest'
      Size = 60
    end
    object QrAeveMDe_dhRegEvento: TDateTimeField
      FieldName = 'eveMDe_dhRegEvento'
      Required = True
    end
    object QrAeveMDe_TZD_UTC: TFloatField
      FieldName = 'eveMDe_TZD_UTC'
      Required = True
    end
    object QrAeveMDe_nProt: TWideStringField
      FieldName = 'eveMDe_nProt'
      Size = 15
    end
    object QrAcSitNFe: TSmallintField
      FieldName = 'cSitNFe'
      Required = True
    end
    object QrAcSitConf: TSmallintField
      FieldName = 'cSitConf'
      Required = True
    end
    object QrAvBasTrib: TFloatField
      FieldName = 'vBasTrib'
      Required = True
    end
    object QrApTotTrib: TFloatField
      FieldName = 'pTotTrib'
      Required = True
    end
    object QrAinfCCe_verAplic: TWideStringField
      FieldName = 'infCCe_verAplic'
      Size = 30
    end
    object QrAinfCCe_cOrgao: TSmallintField
      FieldName = 'infCCe_cOrgao'
      Required = True
    end
    object QrAinfCCe_tpAmb: TSmallintField
      FieldName = 'infCCe_tpAmb'
      Required = True
    end
    object QrAinfCCe_CNPJ: TWideStringField
      FieldName = 'infCCe_CNPJ'
      Size = 18
    end
    object QrAinfCCe_CPF: TWideStringField
      FieldName = 'infCCe_CPF'
      Size = 18
    end
    object QrAinfCCe_chNFe: TWideStringField
      FieldName = 'infCCe_chNFe'
      Size = 44
    end
    object QrAinfCCe_dhEvento: TDateTimeField
      FieldName = 'infCCe_dhEvento'
      Required = True
    end
    object QrAinfCCe_dhEventoTZD: TFloatField
      FieldName = 'infCCe_dhEventoTZD'
      Required = True
    end
    object QrAinfCCe_tpEvento: TIntegerField
      FieldName = 'infCCe_tpEvento'
      Required = True
    end
    object QrAinfCCe_nSeqEvento: TIntegerField
      FieldName = 'infCCe_nSeqEvento'
      Required = True
    end
    object QrAinfCCe_verEvento: TFloatField
      FieldName = 'infCCe_verEvento'
      Required = True
    end
    object QrAinfCCe_xCorrecao: TWideMemoField
      FieldName = 'infCCe_xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAinfCCe_cStat: TIntegerField
      FieldName = 'infCCe_cStat'
      Required = True
    end
    object QrAinfCCe_dhRegEvento: TDateTimeField
      FieldName = 'infCCe_dhRegEvento'
      Required = True
    end
    object QrAinfCCe_dhRegEventoTZD: TFloatField
      FieldName = 'infCCe_dhRegEventoTZD'
      Required = True
    end
    object QrAinfCCe_nProt: TWideStringField
      FieldName = 'infCCe_nProt'
      Size = 15
    end
    object QrAinfCCe_nCondUso: TIntegerField
      FieldName = 'infCCe_nCondUso'
      Required = True
    end
    object QrAAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrAAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrAURL_QRCode: TWideStringField
      FieldName = 'URL_QRCode'
      Size = 255
    end
  end
  object frxDsA: TfrxDBDataset
    UserName = 'frxDsA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'versao=versao'
      'Id=Id'
      'ide_cUF=ide_cUF'
      'ide_cNF=ide_cNF'
      'ide_natOp=ide_natOp'
      'ide_indPag=ide_indPag'
      'ide_mod=ide_mod'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_dSaiEnt=ide_dSaiEnt'
      'ide_tpNF=ide_tpNF'
      'ide_cMunFG=ide_cMunFG'
      'ide_tpImp=ide_tpImp'
      'ide_tpEmis=ide_tpEmis'
      'ide_cDV=ide_cDV'
      'ide_tpAmb=ide_tpAmb'
      'ide_finNFe=ide_finNFe'
      'ide_procEmi=ide_procEmi'
      'ide_verProc=ide_verProc'
      'emit_CNPJ=emit_CNPJ'
      'emit_CPF=emit_CPF'
      'emit_xNome=emit_xNome'
      'emit_xFant=emit_xFant'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_cMun=emit_cMun'
      'emit_xMun=emit_xMun'
      'emit_UF=emit_UF'
      'emit_CEP=emit_CEP'
      'emit_cPais=emit_cPais'
      'emit_xPais=emit_xPais'
      'emit_fone=emit_fone'
      'emit_IE=emit_IE'
      'emit_IEST=emit_IEST'
      'emit_IM=emit_IM'
      'emit_CNAE=emit_CNAE'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'dest_xLgr=dest_xLgr'
      'dest_nro=dest_nro'
      'dest_xCpl=dest_xCpl'
      'dest_xBairro=dest_xBairro'
      'dest_cMun=dest_cMun'
      'dest_xMun=dest_xMun'
      'dest_UF=dest_UF'
      'dest_CEP=dest_CEP'
      'dest_cPais=dest_cPais'
      'dest_xPais=dest_xPais'
      'dest_fone=dest_fone'
      'dest_IE=dest_IE'
      'dest_ISUF=dest_ISUF'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ICMSTot_vBCST=ICMSTot_vBCST'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vII=ICMSTot_vII'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vPIS=ICMSTot_vPIS'
      'ICMSTot_vCOFINS=ICMSTot_vCOFINS'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vNF=ICMSTot_vNF'
      'ISSQNtot_vServ=ISSQNtot_vServ'
      'ISSQNtot_vBC=ISSQNtot_vBC'
      'ISSQNtot_vISS=ISSQNtot_vISS'
      'ISSQNtot_vPIS=ISSQNtot_vPIS'
      'ISSQNtot_vCOFINS=ISSQNtot_vCOFINS'
      'RetTrib_vRetPIS=RetTrib_vRetPIS'
      'RetTrib_vRetCOFINS=RetTrib_vRetCOFINS'
      'RetTrib_vRetCSLL=RetTrib_vRetCSLL'
      'RetTrib_vBCIRRF=RetTrib_vBCIRRF'
      'RetTrib_vIRRF=RetTrib_vIRRF'
      'RetTrib_vBCRetPrev=RetTrib_vBCRetPrev'
      'RetTrib_vRetPrev=RetTrib_vRetPrev'
      'ModFrete=ModFrete'
      'Transporta_CNPJ=Transporta_CNPJ'
      'Transporta_CPF=Transporta_CPF'
      'Transporta_XNome=Transporta_XNome'
      'Transporta_IE=Transporta_IE'
      'Transporta_XEnder=Transporta_XEnder'
      'Transporta_XMun=Transporta_XMun'
      'Transporta_UF=Transporta_UF'
      'RetTransp_vServ=RetTransp_vServ'
      'RetTransp_vBCRet=RetTransp_vBCRet'
      'RetTransp_PICMSRet=RetTransp_PICMSRet'
      'RetTransp_vICMSRet=RetTransp_vICMSRet'
      'RetTransp_CFOP=RetTransp_CFOP'
      'RetTransp_CMunFG=RetTransp_CMunFG'
      'VeicTransp_Placa=VeicTransp_Placa'
      'VeicTransp_UF=VeicTransp_UF'
      'VeicTransp_RNTC=VeicTransp_RNTC'
      'Cobr_Fat_NFat=Cobr_Fat_NFat'
      'Cobr_Fat_vOrig=Cobr_Fat_vOrig'
      'Cobr_Fat_vDesc=Cobr_Fat_vDesc'
      'Cobr_Fat_vLiq=Cobr_Fat_vLiq'
      'InfAdic_InfCpl=InfAdic_InfCpl'
      'Exporta_UFEmbarq=Exporta_UFEmbarq'
      'Exporta_XLocEmbarq=Exporta_XLocEmbarq'
      'Compra_XNEmp=Compra_XNEmp'
      'Compra_XPed=Compra_XPed'
      'Compra_XCont=Compra_XCont'
      'Status=Status'
      'infProt_Id=infProt_Id'
      'infProt_tpAmb=infProt_tpAmb'
      'infProt_verAplic=infProt_verAplic'
      'infProt_dhRecbto=infProt_dhRecbto'
      'infProt_nProt=infProt_nProt'
      'infProt_digVal=infProt_digVal'
      'infProt_cStat=infProt_cStat'
      'infProt_xMotivo=infProt_xMotivo'
      '_Ativo_=_Ativo_'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'IDCtrl=IDCtrl'
      'infCanc_Id=infCanc_Id'
      'infCanc_tpAmb=infCanc_tpAmb'
      'infCanc_verAplic=infCanc_verAplic'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'infCanc_digVal=infCanc_digVal'
      'infCanc_cStat=infCanc_cStat'
      'infCanc_xMotivo=infCanc_xMotivo'
      'infCanc_cJust=infCanc_cJust'
      'infCanc_xJust=infCanc_xJust'
      'ID_TXT=ID_TXT'
      'EMIT_ENDERECO=EMIT_ENDERECO'
      'EMIT_FONE_TXT=EMIT_FONE_TXT'
      'EMIT_IE_TXT=EMIT_IE_TXT'
      'EMIT_IEST_TXT=EMIT_IEST_TXT'
      'EMIT_CNPJ_TXT=EMIT_CNPJ_TXT'
      'DEST_CNPJ_CPF_TXT=DEST_CNPJ_CPF_TXT'
      'DEST_ENDERECO=DEST_ENDERECO'
      'DEST_CEP_TXT=DEST_CEP_TXT'
      'DEST_FONE_TXT=DEST_FONE_TXT'
      'DEST_IE_TXT=DEST_IE_TXT'
      'TRANSPORTA_CNPJ_CPF_TXT=TRANSPORTA_CNPJ_CPF_TXT'
      'TRANSPORTA_IE_TXT=TRANSPORTA_IE_TXT'
      'DOC_SEM_VLR_JUR=DOC_SEM_VLR_JUR'
      'DOC_SEM_VLR_FIS=DOC_SEM_VLR_FIS'
      'ide_DSaiEnt_Txt=ide_DSaiEnt_Txt'
      'InfAdic_InfAdFisco=InfAdic_InfAdFisco'
      'ide_hSaiEnt=ide_hSaiEnt'
      'ide_dhCont=ide_dhCont'
      'ide_xJust=ide_xJust'
      'emit_CRT=emit_CRT'
      'dest_email=dest_email'
      'Vagao=Vagao'
      'Balsa=Balsa'
      'MODFRETE_TXT=MODFRETE_TXT'
      'DEST_XMUN_TXT=DEST_XMUN_TXT'
      'NFeNT2013_003LTT=NFeNT2013_003LTT'
      'InfCpl_totTrib=InfCpl_totTrib'
      'vTotTrib=vTotTrib'
      'URL_Consulta=URL_Consulta'
      'AllTxtLink=AllTxtLink'
      'DEST_CNPJ_CPF_TIT=DEST_CNPJ_CPF_TIT'
      'ide_hEmi=ide_hEmi'
      'ide_dhEmiTZD=ide_dhEmiTZD'
      'ide_dhSaiEntTZD=ide_dhSaiEntTZD'
      'ide_idDest=ide_idDest'
      'ide_indFinal=ide_indFinal'
      'ide_indPres=ide_indPres'
      'ide_dhContTZD=ide_dhContTZD'
      'EstrangDef=EstrangDef'
      'dest_idEstrangeiro=dest_idEstrangeiro'
      'dest_indIEDest=dest_indIEDest'
      'dest_IM=dest_IM'
      'ICMSTot_vICMSDeson=ICMSTot_vICMSDeson'
      'ICMSTot_vFCPUFDest=ICMSTot_vFCPUFDest'
      'ICMSTot_vICMSUFDest=ICMSTot_vICMSUFDest'
      'ICMSTot_vICMSUFRemet=ICMSTot_vICMSUFRemet'
      'ICMSTot_vFCP=ICMSTot_vFCP'
      'ICMSTot_vFCPST=ICMSTot_vFCPST'
      'ICMSTot_vFCPSTRet=ICMSTot_vFCPSTRet'
      'ICMSTot_vIPIDevol=ICMSTot_vIPIDevol'
      'ISSQNtot_dCompet=ISSQNtot_dCompet'
      'ISSQNtot_vDeducao=ISSQNtot_vDeducao'
      'ISSQNtot_vOutro=ISSQNtot_vOutro'
      'ISSQNtot_vDescIncond=ISSQNtot_vDescIncond'
      'ISSQNtot_vDescCond=ISSQNtot_vDescCond'
      'ISSQNtot_vISSRet=ISSQNtot_vISSRet'
      'ISSQNtot_cRegTrib=ISSQNtot_cRegTrib'
      'Exporta_XLocDespacho=Exporta_XLocDespacho'
      'protNFe_versao=protNFe_versao'
      'infProt_dhRecbtoTZD=infProt_dhRecbtoTZD'
      'retCancNFe_versao=retCancNFe_versao'
      'infCanc_dhRecbtoTZD=infCanc_dhRecbtoTZD'
      'FisRegCad=FisRegCad'
      'CartEmiss=CartEmiss'
      'TabelaPrc=TabelaPrc'
      'CondicaoPg=CondicaoPg'
      'FreteExtra=FreteExtra'
      'SegurExtra=SegurExtra'
      'ICMSRec_pRedBC=ICMSRec_pRedBC'
      'ICMSRec_vBC=ICMSRec_vBC'
      'ICMSRec_pAliq=ICMSRec_pAliq'
      'ICMSRec_vICMS=ICMSRec_vICMS'
      'IPIRec_pRedBC=IPIRec_pRedBC'
      'IPIRec_vBC=IPIRec_vBC'
      'IPIRec_pAliq=IPIRec_pAliq'
      'IPIRec_vIPI=IPIRec_vIPI'
      'PISRec_pRedBC=PISRec_pRedBC'
      'PISRec_vBC=PISRec_vBC'
      'PISRec_pAliq=PISRec_pAliq'
      'PISRec_vPIS=PISRec_vPIS'
      'COFINSRec_pRedBC=COFINSRec_pRedBC'
      'COFINSRec_vBC=COFINSRec_vBC'
      'COFINSRec_pAliq=COFINSRec_pAliq'
      'COFINSRec_vCOFINS=COFINSRec_vCOFINS'
      'DataFiscal=DataFiscal'
      'SINTEGRA_ExpDeclNum=SINTEGRA_ExpDeclNum'
      'SINTEGRA_ExpDeclDta=SINTEGRA_ExpDeclDta'
      'SINTEGRA_ExpNat=SINTEGRA_ExpNat'
      'SINTEGRA_ExpRegNum=SINTEGRA_ExpRegNum'
      'SINTEGRA_ExpRegDta=SINTEGRA_ExpRegDta'
      'SINTEGRA_ExpConhNum=SINTEGRA_ExpConhNum'
      'SINTEGRA_ExpConhDta=SINTEGRA_ExpConhDta'
      'SINTEGRA_ExpConhTip=SINTEGRA_ExpConhTip'
      'SINTEGRA_ExpPais=SINTEGRA_ExpPais'
      'SINTEGRA_ExpAverDta=SINTEGRA_ExpAverDta'
      'CodInfoEmit=CodInfoEmit'
      'CodInfoDest=CodInfoDest'
      'CriAForca=CriAForca'
      'CodInfoTrsp=CodInfoTrsp'
      'CodInfoCliI=CodInfoCliI'
      'OrdemServ=OrdemServ'
      'Situacao=Situacao'
      'Antigo=Antigo'
      'NFG_Serie=NFG_Serie'
      'NF_ICMSAlq=NF_ICMSAlq'
      'NF_CFOP=NF_CFOP'
      'Importado=Importado'
      'NFG_SubSerie=NFG_SubSerie'
      'NFG_ValIsen=NFG_ValIsen'
      'NFG_NaoTrib=NFG_NaoTrib'
      'NFG_Outros=NFG_Outros'
      'COD_MOD=COD_MOD'
      'COD_SIT=COD_SIT'
      'VL_ABAT_NT=VL_ABAT_NT'
      'EFD_INN_AnoMes=EFD_INN_AnoMes'
      'EFD_INN_Empresa=EFD_INN_Empresa'
      'EFD_INN_LinArq=EFD_INN_LinArq'
      'ICMSRec_vBCST=ICMSRec_vBCST'
      'ICMSRec_vICMSST=ICMSRec_vICMSST'
      'EFD_EXP_REG=EFD_EXP_REG'
      'ICMSRec_pAliqST=ICMSRec_pAliqST'
      'eveMDe_Id=eveMDe_Id'
      'eveMDe_tpAmb=eveMDe_tpAmb'
      'eveMDe_verAplic=eveMDe_verAplic'
      'eveMDe_cOrgao=eveMDe_cOrgao'
      'eveMDe_cStat=eveMDe_cStat'
      'eveMDe_xMotivo=eveMDe_xMotivo'
      'eveMDe_chNFe=eveMDe_chNFe'
      'eveMDe_tpEvento=eveMDe_tpEvento'
      'eveMDe_xEvento=eveMDe_xEvento'
      'eveMDe_nSeqEvento=eveMDe_nSeqEvento'
      'eveMDe_CNPJDest=eveMDe_CNPJDest'
      'eveMDe_CPFDest=eveMDe_CPFDest'
      'eveMDe_emailDest=eveMDe_emailDest'
      'eveMDe_dhRegEvento=eveMDe_dhRegEvento'
      'eveMDe_TZD_UTC=eveMDe_TZD_UTC'
      'eveMDe_nProt=eveMDe_nProt'
      'cSitNFe=cSitNFe'
      'cSitConf=cSitConf'
      'vBasTrib=vBasTrib'
      'pTotTrib=pTotTrib'
      'infCCe_verAplic=infCCe_verAplic'
      'infCCe_cOrgao=infCCe_cOrgao'
      'infCCe_tpAmb=infCCe_tpAmb'
      'infCCe_CNPJ=infCCe_CNPJ'
      'infCCe_CPF=infCCe_CPF'
      'infCCe_chNFe=infCCe_chNFe'
      'infCCe_dhEvento=infCCe_dhEvento'
      'infCCe_dhEventoTZD=infCCe_dhEventoTZD'
      'infCCe_tpEvento=infCCe_tpEvento'
      'infCCe_nSeqEvento=infCCe_nSeqEvento'
      'infCCe_verEvento=infCCe_verEvento'
      'infCCe_xCorrecao=infCCe_xCorrecao'
      'infCCe_cStat=infCCe_cStat'
      'infCCe_dhRegEvento=infCCe_dhRegEvento'
      'infCCe_dhRegEventoTZD=infCCe_dhRegEventoTZD'
      'infCCe_nProt=infCCe_nProt'
      'infCCe_nCondUso=infCCe_nCondUso'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'URL_QRCode=URL_QRCode')
    DataSet = QrA
    BCDToCurrency = False
    DataSetOptions = []
    Left = 456
    Top = 188
  end
  object QrI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterScroll = QrIAfterScroll
    SQL.Strings = (
      'SELECT *, pgt.Fracio'
      'FROM nfeitsi nii'
      'LEFT JOIN gragrux ggx ON ggx.Controle = nii.prod_cProd'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo = gg1.PrdGrupTip'
      'WHERE nii.FatID=:P0'
      'AND nii.FatNum=:P1'
      'AND nii.Empresa=:P2')
    Left = 456
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrIFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrIprod_qCom_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'prod_qCom_TXT'
      Size = 50
      Calculated = True
    end
  end
  object frxDsI: TfrxDBDataset
    UserName = 'frxDsI'
    CloseDataSource = False
    FieldAliases.Strings = (
      'prod_cProd=prod_cProd'
      'prod_cEAN=prod_cEAN'
      'prod_xProd=prod_xProd'
      'prod_NCM=prod_NCM'
      'prod_EXTIPI=prod_EXTIPI'
      'prod_genero=prod_genero'
      'prod_CFOP=prod_CFOP'
      'prod_uCom=prod_uCom'
      'prod_qCom=prod_qCom'
      'prod_vUnCom=prod_vUnCom'
      'prod_vProd=prod_vProd'
      'prod_cEANTrib=prod_cEANTrib'
      'prod_uTrib=prod_uTrib'
      'prod_qTrib=prod_qTrib'
      'prod_vUnTrib=prod_vUnTrib'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'Tem_IPI=Tem_IPI'
      'nItem=nItem'
      'Fracio=Fracio'
      'prod_qCom_TXT=prod_qCom_TXT')
    DataSet = QrI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 456
    Top = 284
  end
  object QrN: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 456
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      DisplayFormat = '000.###'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
  end
  object frxDsN: TfrxDBDataset
    UserName = 'frxDsN'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nItem=nItem'
      'ICMS_Orig=ICMS_Orig'
      'ICMS_CST=ICMS_CST'
      'ICMS_modBC=ICMS_modBC'
      'ICMS_pRedBC=ICMS_pRedBC'
      'ICMS_vBC=ICMS_vBC'
      'ICMS_pICMS=ICMS_pICMS'
      'ICMS_vICMS=ICMS_vICMS'
      'ICMS_modBCST=ICMS_modBCST'
      'ICMS_pMVAST=ICMS_pMVAST'
      'ICMS_pRedBCST=ICMS_pRedBCST'
      'ICMS_vBCST=ICMS_vBCST'
      'ICMS_pICMSST=ICMS_pICMSST'
      'ICMS_vICMSST=ICMS_vICMSST'
      'ICMS_CSOSN=ICMS_CSOSN')
    DataSet = QrN
    BCDToCurrency = False
    DataSetOptions = []
    Left = 456
    Top = 376
  end
  object QrV: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 596
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrVnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsV: TfrxDBDataset
    UserName = 'frxDsV'
    CloseDataSource = False
    DataSet = QrV
    BCDToCurrency = False
    DataSetOptions = []
    Left = 596
    Top = 192
  end
  object QrO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 576
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object frxDsO: TfrxDBDataset
    UserName = 'frxDsO'
    CloseDataSource = False
    DataSet = QrO
    BCDToCurrency = False
    DataSetOptions = []
    Left = 576
    Top = 336
  end
  object QrM: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 576
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrMiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object frxDsM: TfrxDBDataset
    UserName = 'frxDsM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'nItem=nItem'
      'iTotTrib=iTotTrib'
      'vTotTrib=vTotTrib')
    DataSet = QrM
    BCDToCurrency = False
    DataSetOptions = []
    Left = 576
    Top = 424
  end
  object QrYA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabya nfeya'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle')
    Left = 656
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrYAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrYAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrYAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrYAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrYAtPag: TSmallintField
      FieldName = 'tPag'
    end
    object QrYAvPag: TFloatField
      FieldName = 'vPag'
    end
    object QrYAtpIntegra: TSmallintField
      FieldName = 'tpIntegra'
    end
    object QrYACNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrYAtBand: TSmallintField
      FieldName = 'tBand'
    end
    object QrYAcAut: TWideStringField
      FieldName = 'cAut'
    end
    object QrYAvTroco: TFloatField
      FieldName = 'vTroco'
    end
  end
  object frxDsYA: TfrxDBDataset
    UserName = 'frxDsYA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'tPag=tPag'
      'vPag=vPag'
      'tpIntegra=tpIntegra'
      'CNPJ=CNPJ'
      'tBand=tBand'
      'cAut=cAut'
      'vTroco=vTroco')
    DataSet = QrYA
    BCDToCurrency = False
    DataSetOptions = []
    Left = 658
    Top = 277
  end
  object frxNFCe_02_Off_Line_Univers_Light_Condensed: TfrxReport
    Version = '2022.1'
    DataSet = Dmod.frxDsMaster
    DataSetName = 'frxDsMaster'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44135.834781759300000000
    ReportOptions.LastChange = 44135.834781759300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Linha: Integer;'
      '    '
      'procedure MasterData7OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Linha = 2 then'
      '    Linha := 0;                                       '
      
        '  Linha := Linha + 1;                                           ' +
        ' '
      '  case Linha of'
      '    1: MeViaDeQuem.Memo.Text := '#39'Via Cliente'#39';  '
      '    2: MeViaDeQuem.Memo.Text := '#39'Via Estabelecimento'#39';  '
      '    else MeViaDeQuem.Memo.Text := '#39#39';  '
      '  end;              '
      'end;'
      ''
      'begin'
      '  Linha := 0;                          '
      
        '  MDSeguro.Visible := <frxDsA."ICMSTot_vSeg"> <> 0;             ' +
        '                                                     '
      
        '  MDFrete.Visible := <frxDsA."ICMSTot_vFrete"> <> 0;            ' +
        '                                                      '
      
        '  MDOutros.Visible := <frxDsA."ICMSTot_vOutro"> <> 0;           ' +
        '                                                       '
      '  MDDesconto.Visible := <frxDsA."ICMSTot_vDesc"> <> 0;        '
      '  //        '
      '  if (<LogoNFeExiste> = True) then'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxNFCe_01_Autorizada_UniversLight_CondensedGetValue
    Left = 794
    Top = 55
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 260.787570000000000000
        OnBeforePrint = 'MasterData7OnBeforePrint'
        RowCount = 2
        object Subreport1: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxNFCe_02_Off_Line_Univers_Light_Condensed.Page3
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 136.062992130000000000
        Top = 18.897650000000000000
        Width = 260.787570000000000000
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 37.795275590000000000
          Width = 90.708637010000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ: [frxDsA."EMIT_CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 37.795275590000000000
          Width = 128.503937010000000000
          Height = 13.228346460000000000
          DataField = 'emit_xNome'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 51.023622050000000000
          Width = 222.992270000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 77.480314960000000000
          Width = 260.787423540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento Auxiliar da Nota Fiscal de Consumidor Eletr'#244'nica')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 122.834645670000000000
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 122.834645670000000000
          Width = 117.165430000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 122.834645670000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 122.834645670000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 122.834645670000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 122.834645670000000000
          Width = 34.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Unit')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 260.787570000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251968500000000000
          Top = 92.598425200000000000
          Width = 132.283464570000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'EMITIDA EM CONTING'#202'NCIA')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251968500000000000
          Top = 107.716535430000000000
          Width = 132.283464570000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Pendente de autoriza'#231#227'o')
          ParentFont = False
          VAlign = vaBottom
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Top = 39.685039370078740000
          Width = 37.795300000000000000
          Height = 37.795275590000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 177.637910000000000000
        Width = 260.787570000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 117.165430000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          DataField = 'prod_uCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'prod_vProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vProd"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 34.015735830000000000
          Height = 13.228346460000000000
          DataField = 'prod_vUnCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vUnCom"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 13.228346460000000000
          Width = 260.787570000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsV."InfAdProd"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 226.771800000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde. total de itens')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_QTDE_ITENS]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 491.338900000000000000
        Width = 260.787570000000000000
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_tPAG_TXT]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181126770000000000
          Height = 13.228346460000000000
          DataField = 'vPag'
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsYA."vPag"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 453.543600000000000000
        Width = 260.787570000000000000
        Condition = 'frxDsYA."FatID"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma pagamento')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181126770000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago R$')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 529.134200000000000000
        Width = 260.787570000000000000
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 166.299320000000000000
        Top = 551.811380000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consulte pela Chave de Acesso em')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 13.228346460000000000
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_URL_Consulta_NFCe]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 260.787411340000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CHAVE_NFCe_FORMATADA]')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 196.000000000000000000
          Height = 196.000000000000000000
          StretchMode = smActualHeight
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          DataField = 'AllTxtLink'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          HexData = 
            '68007400740070003A002F002F007700770077002E00660061007A0065006E00' +
            '640061002E00700072002E0067006F0076002E00620072002F006E0066006300' +
            '65002F007100720063006F00640065003F0070003D0034003100320030003100' +
            '3100310034003600300038003600380038003000300030003100300030003600' +
            '3500300030003100300030003000300030003100300036003800310034003500' +
            '39003300350032003300320030007C0032007C0032007C0033007C0034003400' +
            '4200390036004600450044003200420031003700420045004300380036004300' +
            '3900340041003800430044003600370043003800410037004600360039003800' +
            '3100410039003000370033003400340042003900360046004500440032004200' +
            '3100370042004500430038003600430039003400410038004300440036003700' +
            '43003800410037004600360039003800310041003900300037003300'
          Zoom = 0.400000000000000000
          FontScaled = True
          QuietZone = 0
          ColorBar = clBlack
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 41.574830000000000000
          Width = 173.858221340000000000
          Height = 35.905526460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsA."DEST_CNPJ_CPF_TIT"] [frxDsA."DEST_CNPJ_CPF_TXT"] - [frx' +
              'DsA."dest_xNome"] - [frxDsA."DEST_ENDERECO"] [frxDsA."dest_xMun"' +
              '] [frxDsA."dest_UF"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Top = 128.504020000000000000
          Width = 260.787411340000000000
          Height = 35.905509370000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."InfCpl_totTrib"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 88.818897637795000000
          Width = 170.078764570000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'EMITIDA EM CONTING'#202'NCIA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 105.826830230000000000
          Width = 170.078764570000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Pendente de autoriza'#231#227'o')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeViaDeQuem: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 77.480314960000000000
          Width = 173.858331180000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VIA ???')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MDSeguro: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 264.567100000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor seguro R$')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
        end
      end
      object MDFrete: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 302.362400000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor Frete R$')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
        end
      end
      object MDOutros: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor outros R$')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
        end
      end
      object MDDesconto: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 377.953000000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Desconto R$')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 415.748300000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor a Pagar R$')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 740.787880000000000000
        Width = 260.787570000000000000
        RowCount = 1
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsA."InfAdic_InfCpl"]')
          ParentFont = False
        end
      end
    end
  end
  object frxNFCe_01_Autorizada: TfrxReport
    Version = '2022.1'
    DataSet = Dmod.frxDsMaster
    DataSetName = 'frxDsMaster'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44135.834781759300000000
    ReportOptions.LastChange = 44135.834781759300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MDSeguro.Visible := <frxDsA."ICMSTot_vSeg"> <> 0;             ' +
        '                                                     '
      
        '  MDFrete.Visible := <frxDsA."ICMSTot_vFrete"> <> 0;            ' +
        '                                                      '
      
        '  MDOutros.Visible := <frxDsA."ICMSTot_vOutro"> <> 0;           ' +
        '                                                       '
      '  MDDesconto.Visible := <frxDsA."ICMSTot_vDesc"> <> 0;'
      '  //        '
      '  if (<LogoNFeExiste> = True) then'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxNFCe_01_Autorizada_UniversLight_CondensedGetValue
    Left = 798
    Top = 107
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 300.000000000000000000
      PaperSize = 256
      LeftMargin = 1.000000000000000000
      RightMargin = 5.000000000000000000
      Frame.Typ = []
      EndlessHeight = True
      LargeDesignHeight = True
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 205.984376460000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 275.905690000000000000
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267755590000000000
          Width = 275.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CNPJ: [frxDsA."EMIT_CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 113.385875590000000000
          Width = 275.905511810000000000
          Height = 18.897637800000000000
          DataField = 'emit_xNome'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 134.173282050000000000
          Width = 275.905511810000000000
          Height = 37.795282910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 173.858267720000000000
          Width = 275.905543540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento Auxiliar da Nota Fiscal de Consumidor Eletr'#244'nica')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 192.756030000000000000
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 192.756030000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 192.756030000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 192.756030000000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 192.756030000000000000
          Width = 49.133865590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 192.756030000000000000
          Width = 41.574795830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Unit')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 40.440947320000000000
          Top = 17.007859370000000000
          Width = 188.976500000000000000
          Height = 72.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 30.236220470000000000
        ParentFont = False
        Top = 249.448980000000000000
        Width = 275.905690000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 139.842519690000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"] [frxDsI."prod_xProd"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Width = 45.354342910000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"] [frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 49.133865590000000000
          Height = 15.118110240000000000
          DataField = 'prod_vProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vProd"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Width = 41.574795830000000000
          Height = 15.118110240000000000
          DataField = 'prod_vUnCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vUnCom"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = -3.779530000000000000
          Top = 15.118110240000000000
          Width = 279.685220000000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsV."InfAdProd"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 302.362400000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde. total de itens')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_QTDE_ITENS]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 570.709030000000000000
        Width = 275.905690000000000000
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_tPAG_TXT]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          DataField = 'vPag'
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsYA."vPag"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 17.007876460000000000
        ParentFont = False
        Top = 529.134200000000000000
        Width = 275.905690000000000000
        Condition = 'frxDsYA."FatID"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma pagamento')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago R$')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Top = 608.504330000000000000
        Width = 275.905690000000000000
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 338.267909370000000000
        ParentFont = False
        Top = 631.181510000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consulte pela Chave de Acesso em')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118110240000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_URL_Consulta_NFCe]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CHAVE_NFCe_FORMATADA]')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          AllowVectorExport = True
          Left = 80.881889760000000000
          Top = 109.606370000000000000
          Width = 196.000000000000000000
          Height = 196.000000000000000000
          StretchMode = smActualHeight
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          DataField = 'AllTxtLink'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          HexData = 
            '68007400740070003A002F002F007700770077002E00660061007A0065006E00' +
            '640061002E00700072002E0067006F0076002E00620072002F006E0066006300' +
            '65002F007100720063006F00640065003F0070003D0034003100320030003100' +
            '3100310034003600300038003600380038003000300030003100300030003600' +
            '3500300030003100300030003000300030003100300036003800310034003500' +
            '39003300350032003300320030007C0032007C0032007C0033007C0034003400' +
            '4200390036004600450044003200420031003700420045004300380036004300' +
            '3900340041003800430044003600370043003800410037004600360039003800' +
            '3100410039003000370033003400340042003900360046004500440032004200' +
            '3100370042004500430038003600430039003400410038004300440036003700' +
            '43003800410037004600360039003800310041003900300037003300'
          Zoom = 0.600000000000000000
          FontScaled = True
          QuietZone = 0
          ColorBar = clBlack
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 275.905511810000000000
          Height = 41.574820240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[frxDsA."DEST_CNPJ_CPF_TIT"] [frxDsA."DEST_CNPJ_CPF_TXT"] - [frx' +
              'DsA."dest_xNome"] - [frxDsA."DEST_ENDERECO"] [frxDsA."dest_xMun"' +
              '] [frxDsA."dest_UF"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 245.669318190000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NFC-e n'#186' [FormatFloat('#39'000000'#39', <frxDsA."ide_nNF">)] S'#233'rie [Form' +
              'atFloat('#39'000'#39', <frxDsA."ide_serie">)] [FormatDateTime('#39'dd/mm/yyy' +
              'y'#39', <frxDsA."ide_dEmi">)] [FormatDateTime('#39'hh:nn:ss'#39', <frxDsA."i' +
              'de_hEmi">)]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 260.787428430000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo de Autoriza'#231#227'o [frxDsA."infProt_nProt"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Top = 275.905538660000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de Autoriza'#231#227'o [frxDsA."infProt_dhRecbto"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Top = 291.023810000000000000
          Width = 275.905511810000000000
          Height = 47.244099370000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."InfCpl_totTrib"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
      end
      object MDSeguro: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 340.157700000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor seguro R$')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
        end
      end
      object MDFrete: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 377.953000000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor Frete R$')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
        end
      end
      object MDOutros: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 415.748300000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor outros R$')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
        end
      end
      object MDDesconto: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 453.543600000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Desconto R$')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 491.338900000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor a Pagar R$')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503937010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 30.236240000000000000
        ParentFont = False
        Top = 994.016390000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Width = 275.905511810000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsA."InfAdic_InfCpl"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 136.063080000000000000
        Top = 1084.725110000000000000
        Width = 275.905690000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 113.385900000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '.')
        end
      end
    end
  end
  object frxNFCe_02_Off_Line: TfrxReport
    Version = '2022.1'
    DataSet = Dmod.frxDsMaster
    DataSetName = 'frxDsMaster'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44135.834781759300000000
    ReportOptions.LastChange = 44135.834781759300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Linha: Integer;'
      '    '
      'procedure MasterData7OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Linha = 2 then'
      '    Linha := 0;                                       '
      
        '  Linha := Linha + 1;                                           ' +
        ' '
      '  case Linha of'
      '    1: MeViaDeQuem.Memo.Text := '#39'Via Cliente'#39';  '
      '    2: MeViaDeQuem.Memo.Text := '#39'Via Estabelecimento'#39';  '
      '    else MeViaDeQuem.Memo.Text := '#39#39';  '
      '  end;              '
      'end;'
      ''
      'begin'
      '  Linha := 0;                          '
      
        '  MDSeguro.Visible := <frxDsA."ICMSTot_vSeg"> <> 0;             ' +
        '                                                     '
      
        '  MDFrete.Visible := <frxDsA."ICMSTot_vFrete"> <> 0;            ' +
        '                                                      '
      
        '  MDOutros.Visible := <frxDsA."ICMSTot_vOutro"> <> 0;           ' +
        '                                                       '
      '  MDDesconto.Visible := <frxDsA."ICMSTot_vDesc"> <> 0;        '
      '  //        '
      '  if (<LogoNFeExiste> = True) then'
      '  begin'
      
        '    Picture1.Visible := True;                                   ' +
        '           '
      '    //          '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture1.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxNFCe_01_Autorizada_UniversLight_CondensedGetValue
    Left = 798
    Top = 159
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsM
        DataSetName = 'frxDsM'
      end
      item
        DataSet = frxDsN
        DataSetName = 'frxDsN'
      end
      item
        DataSet = frxDsO
        DataSetName = 'frxDsO'
      end
      item
        DataSet = frxDsV
        DataSetName = 'frxDsV'
      end
      item
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 1.000000000000000000
      RightMargin = 5.000000000000000000
      Frame.Typ = []
      EndlessHeight = True
      MirrorMode = []
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 275.905690000000000000
        OnBeforePrint = 'MasterData7OnBeforePrint'
        RowCount = 2
        object Subreport1: TfrxSubreport
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxNFCe_02_Off_Line.Page3
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 79.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 1.000000000000000000
      RightMargin = 5.000000000000000000
      Frame.Typ = []
      EndlessHeight = True
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 238.110302130000000000
        Top = 18.897650000000000000
        Width = 275.905690000000000000
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 188.976500000000000000
          Width = 275.905690000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = clGray
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811112720000000000
          Top = 190.866205200000000000
          Width = 132.283464570000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'EMITIDA EM CONTING'#202'NCIA')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811112720000000000
          Top = 205.984315430000000000
          Width = 132.283464570000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Pendente de autoriza'#231#227'o')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 96.378016220000000000
          Width = 275.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CNPJ: [frxDsA."EMIT_CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 111.496136220000000000
          Width = 275.905511810000000000
          Height = 18.897637800000000000
          DataField = 'emit_xNome'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283542680000000000
          Width = 275.905511810000000000
          Height = 37.795282910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 171.968528350000000000
          Width = 275.905543540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento Auxiliar da Nota Fiscal de Consumidor Eletr'#244'nica')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 221.102530630000000000
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 221.102530630000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 221.102530630000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 221.102530630000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 221.102530630000000000
          Width = 49.133865590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 221.102530630000000000
          Width = 41.574795830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vl Unit')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 40.440947320000000000
          Top = 15.118120000000000000
          Width = 188.976500000000000000
          Height = 72.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 30.236220470000000000
        ParentFont = False
        Top = 279.685220000000000000
        Width = 275.905690000000000000
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 139.842519690000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"] [frxDsI."prod_xProd"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Width = 45.354342910000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"] [frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 49.133865590000000000
          Height = 15.118110240000000000
          DataField = 'prod_vProd'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vProd"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 41.574795830000000000
          Height = 15.118110240000000000
          DataField = 'prod_vUnCom'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_vUnCom"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118110240000000000
          Width = 279.685220000000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsV."InfAdProd"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 332.598640000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde. total de itens')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_QTDE_ITENS]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 600.945270000000000000
        Width = 275.905690000000000000
        DataSet = frxDsYA
        DataSetName = 'frxDsYA'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_tPAG_TXT]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          DataField = 'vPag'
          DataSet = frxDsYA
          DataSetName = 'frxDsYA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsYA."vPag"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 17.007876460000000000
        ParentFont = False
        Top = 559.370440000000000000
        Width = 275.905690000000000000
        Condition = 'frxDsYA."FatID"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma pagamento')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago R$')
          ParentFont = False
        end
      end
      object MDSeguro: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 370.393940000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor seguro R$')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
        end
      end
      object MDFrete: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 408.189240000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor Frete R$')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
        end
      end
      object MDOutros: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 445.984540000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor outros R$')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
        end
      end
      object MDDesconto: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 483.779840000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Desconto R$')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 521.575140000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor a Pagar R$')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283467010000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 338.267909370000000000
        ParentFont = False
        Top = 638.740570000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consulte pela Chave de Acesso em')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118110240000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_URL_Consulta_NFCe]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CHAVE_NFCe_FORMATADA]')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          AllowVectorExport = True
          Left = 80.881889760000000000
          Top = 109.606370000000000000
          Width = 196.000000000000000000
          Height = 196.000000000000000000
          StretchMode = smActualHeight
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          DataField = 'AllTxtLink'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          HexData = 
            '68007400740070003A002F002F007700770077002E00660061007A0065006E00' +
            '640061002E00700072002E0067006F0076002E00620072002F006E0066006300' +
            '65002F007100720063006F00640065003F0070003D0034003100320030003100' +
            '3100310034003600300038003600380038003000300030003100300030003600' +
            '3500300030003100300030003000300030003100300036003800310034003500' +
            '39003300350032003300320030007C0032007C0032007C0033007C0034003400' +
            '4200390036004600450044003200420031003700420045004300380036004300' +
            '3900340041003800430044003600370043003800410037004600360039003800' +
            '3100410039003000370033003400340042003900360046004500440032004200' +
            '3100370042004500430038003600430039003400410038004300440036003700' +
            '43003800410037004600360039003800310041003900300037003300'
          Zoom = 0.600000000000000000
          FontScaled = True
          QuietZone = 0
          ColorBar = clBlack
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 275.905511810000000000
          Height = 41.574820240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[frxDsA."DEST_CNPJ_CPF_TIT"] [frxDsA."DEST_CNPJ_CPF_TXT"] - [frx' +
              'DsA."dest_xNome"] - [frxDsA."DEST_ENDERECO"] [frxDsA."dest_xMun"' +
              '] [frxDsA."dest_UF"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 245.669318190000000000
          Width = 275.905511810000000000
          Height = 15.118110240000000000
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NFC-e n'#186' [FormatFloat('#39'000000'#39', <frxDsA."ide_nNF">)] S'#233'rie [Form' +
              'atFloat('#39'000'#39', <frxDsA."ide_serie">)] [FormatDateTime('#39'dd/mm/yyy' +
              'y'#39', <frxDsA."ide_dEmi">)] [FormatDateTime('#39'hh:nn:ss'#39', <frxDsA."i' +
              'de_hEmi">)]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Top = 264.567100000000000000
          Width = 275.905604570000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'EMITIDA EM CONTING'#202'NCIA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 281.575032590000000000
          Width = 275.905604570000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Pendente de autoriza'#231#227'o')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        Height = 30.236240000000000000
        ParentFont = False
        Top = 1001.575450000000000000
        Width = 275.905690000000000000
        RowCount = 1
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 275.905511810000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsA."InfAdic_InfCpl"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 1092.284170000000000000
        Width = 275.905690000000000000
      end
    end
  end
end
