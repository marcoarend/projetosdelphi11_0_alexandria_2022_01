
{***********************************************************************************************************************}
{                                                                                                                       }
{                                                   XML Data Binding                                                    }
{                                                                                                                       }
{         Generated on: 30/06/2018 18:30:34                                                                             }
{       Generated from: C:\_Compilers\Delphi_XE2\VCL\MDL\NFe\v_04_00\XSD\PL_009_V4_2016_002_v160\consSitNFe_v4.00.xsd   }
{   Settings stored in: C:\_Compilers\Delphi_XE2\VCL\MDL\NFe\v_04_00\XSD\PL_009_V4_2016_002_v160\consSitNFe_v4.00.xdb   }
{                                                                                                                       }
{***********************************************************************************************************************}

unit consSitNFe_v400;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsSitNFe = interface;
  IXMLTRetConsSitNFe = interface;
  IXMLTProtNFe = interface;
  IXMLInfProt = interface;
  IXMLSignatureType_ = interface;
  IXMLSignedInfoType_ = interface;
  IXMLCanonicalizationMethod_ = interface;
  IXMLSignatureMethod_ = interface;
  IXMLReferenceType_ = interface;
  IXMLTransformsType_ = interface;
  IXMLTransformType_ = interface;
  IXMLDigestMethod_ = interface;
  IXMLSignatureValueType_ = interface;
  IXMLKeyInfoType_ = interface;
  IXMLX509DataType_ = interface;
  IXMLTRetCancNFe = interface;
  IXMLInfCanc = interface;
  IXMLTProcEvento = interface;
  IXMLTProcEventoList = interface;
  IXMLTEvento = interface;
  IXMLInfEvento = interface;
  IXMLDetEvento = interface;
  IXMLTRetEvento = interface;

{ IXMLTConsSitNFe }

  IXMLTConsSitNFe = interface(IXMLNode)
    ['{9EE92CAD-52C9-4CB8-B72C-E5D1AC940374}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChNFe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
  end;

{ IXMLTRetConsSitNFe }

  IXMLTRetConsSitNFe = interface(IXMLNode)
    ['{236BA97A-DC41-4016-85A1-6A70B6E21CAC}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_ProtNFe: IXMLTProtNFe;
    function Get_RetCancNFe: IXMLTRetCancNFe;
    function Get_ProcEventoNFe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
    property ProtNFe: IXMLTProtNFe read Get_ProtNFe;
    property RetCancNFe: IXMLTRetCancNFe read Get_RetCancNFe;
    property ProcEventoNFe: IXMLTProcEventoList read Get_ProcEventoNFe;
  end;

{ IXMLTProtNFe }

  IXMLTProtNFe = interface(IXMLNode)
    ['{AC148960-3EFC-4413-A9E1-E132240AC26F}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfProt: IXMLInfProt read Get_InfProt;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfProt }

  IXMLInfProt = interface(IXMLNode)
    ['{DE15B7B7-9B4B-445B-8816-60952765C6DB}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
    property DigVal: UnicodeString read Get_DigVal write Set_DigVal;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
  end;

{ IXMLSignatureType_ }

  IXMLSignatureType_ = interface(IXMLNode)
    ['{1839FFFC-2567-4CBE-951B-80A2510C07D7}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ }

  IXMLSignedInfoType_ = interface(IXMLNode)
    ['{4512EA7F-2A6D-47AD-9E57-B188C25D4ECB}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ }

  IXMLCanonicalizationMethod_ = interface(IXMLNode)
    ['{71E7A7C8-A566-4584-B0FF-E58DEEC983C8}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ }

  IXMLSignatureMethod_ = interface(IXMLNode)
    ['{C7175F5B-64E5-4FC5-91FF-AE3B9B31B0D1}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ }

  IXMLReferenceType_ = interface(IXMLNode)
    ['{3973FE95-D009-49E0-BF1C-8EBA35BAF12A}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ }

  IXMLTransformsType_ = interface(IXMLNodeCollection)
    ['{8EBAE959-F5B1-4181-B439-4DFDFD21F79A}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    { Methods & Properties }
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
    property Transform[Index: Integer]: IXMLTransformType_ read Get_Transform; default;
  end;

{ IXMLTransformType_ }

  IXMLTransformType_ = interface(IXMLNodeCollection)
    ['{C915276D-C1ED-4B2A-B6CE-32B4E5A91083}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ }

  IXMLDigestMethod_ = interface(IXMLNode)
    ['{A4B79D9B-064F-4BF3-91E2-3CE5C8216570}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ }

  IXMLSignatureValueType_ = interface(IXMLNode)
    ['{25488612-B521-4764-9EAF-40A86F28937B}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ }

  IXMLKeyInfoType_ = interface(IXMLNode)
    ['{F13E4BA7-B6E0-4399-AEEF-9F5F480477CD}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ read Get_X509Data;
  end;

{ IXMLX509DataType_ }

  IXMLX509DataType_ = interface(IXMLNode)
    ['{8C59FA62-0366-46DB-A27F-FA35136FCE50}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTRetCancNFe }

  IXMLTRetCancNFe = interface(IXMLNode)
    ['{9EEA5588-F169-4DA6-8F22-2C88C832A7FA}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfCanc: IXMLInfCanc read Get_InfCanc;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfCanc }

  IXMLInfCanc = interface(IXMLNode)
    ['{1BA05FF5-1067-468C-B45D-8A34F7024565}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
  end;

{ IXMLTProcEvento }

  IXMLTProcEvento = interface(IXMLNode)
    ['{B41A420E-CD58-4EE3-A201-1AFB22B00B9E}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Evento: IXMLTEvento;
    function Get_RetEvento: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Evento: IXMLTEvento read Get_Evento;
    property RetEvento: IXMLTRetEvento read Get_RetEvento;
  end;

{ IXMLTProcEventoList }

  IXMLTProcEventoList = interface(IXMLNodeCollection)
    ['{EAE2089A-05F7-425C-A186-98D512956E2F}']
    { Methods & Properties }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
    property Items[Index: Integer]: IXMLTProcEvento read Get_Item; default;
  end;

{ IXMLTEvento }

  IXMLTEvento = interface(IXMLNode)
    ['{9C5A3595-B04E-475F-B8EE-7E76766778D0}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfEvento }

  IXMLInfEvento = interface(IXMLNode)
    ['{7CB99D5C-103C-48ED-A3B9-205A35F28DD7}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_VerEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
    procedure Set_VerEvento(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property COrgao: UnicodeString read Get_COrgao write Set_COrgao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
    property DhEvento: UnicodeString read Get_DhEvento write Set_DhEvento;
    property TpEvento: UnicodeString read Get_TpEvento write Set_TpEvento;
    property NSeqEvento: UnicodeString read Get_NSeqEvento write Set_NSeqEvento;
    property VerEvento: UnicodeString read Get_VerEvento write Set_VerEvento;
    property DetEvento: IXMLDetEvento read Get_DetEvento;
  end;

{ IXMLDetEvento }

  IXMLDetEvento = interface(IXMLNode)
    ['{F3BDD3F5-3D6C-4B2D-BDD3-65691806D484}']
  end;

{ IXMLTRetEvento }

  IXMLTRetEvento = interface(IXMLNode)
    ['{CA48EACA-9967-4D32-A487-25851318BB69}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ Forward Decls }

  TXMLTConsSitNFe = class;
  TXMLTRetConsSitNFe = class;
  TXMLTProtNFe = class;
  TXMLInfProt = class;
  TXMLSignatureType_ = class;
  TXMLSignedInfoType_ = class;
  TXMLCanonicalizationMethod_ = class;
  TXMLSignatureMethod_ = class;
  TXMLReferenceType_ = class;
  TXMLTransformsType_ = class;
  TXMLTransformType_ = class;
  TXMLDigestMethod_ = class;
  TXMLSignatureValueType_ = class;
  TXMLKeyInfoType_ = class;
  TXMLX509DataType_ = class;
  TXMLTRetCancNFe = class;
  TXMLInfCanc = class;
  TXMLTProcEvento = class;
  TXMLTProcEventoList = class;
  TXMLTEvento = class;
  TXMLInfEvento = class;
  TXMLDetEvento = class;
  TXMLTRetEvento = class;

{ TXMLTConsSitNFe }

  TXMLTConsSitNFe = class(TXMLNode, IXMLTConsSitNFe)
  protected
    { IXMLTConsSitNFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChNFe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
  end;

{ TXMLTRetConsSitNFe }

  TXMLTRetConsSitNFe = class(TXMLNode, IXMLTRetConsSitNFe)
  private
    FProcEventoNFe: IXMLTProcEventoList;
  protected
    { IXMLTRetConsSitNFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_ProtNFe: IXMLTProtNFe;
    function Get_RetCancNFe: IXMLTRetCancNFe;
    function Get_ProcEventoNFe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProtNFe }

  TXMLTProtNFe = class(TXMLNode, IXMLTProtNFe)
  protected
    { IXMLTProtNFe }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfProt }

  TXMLInfProt = class(TXMLNode, IXMLInfProt)
  protected
    { IXMLInfProt }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
  end;

{ TXMLSignatureType_ }

  TXMLSignatureType_ = class(TXMLNode, IXMLSignatureType_)
  protected
    { IXMLSignatureType_ }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ }

  TXMLSignedInfoType_ = class(TXMLNode, IXMLSignedInfoType_)
  protected
    { IXMLSignedInfoType_ }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ }

  TXMLCanonicalizationMethod_ = class(TXMLNode, IXMLCanonicalizationMethod_)
  protected
    { IXMLCanonicalizationMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ }

  TXMLSignatureMethod_ = class(TXMLNode, IXMLSignatureMethod_)
  protected
    { IXMLSignatureMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ }

  TXMLReferenceType_ = class(TXMLNode, IXMLReferenceType_)
  protected
    { IXMLReferenceType_ }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ }

  TXMLTransformsType_ = class(TXMLNodeCollection, IXMLTransformsType_)
  protected
    { IXMLTransformsType_ }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ }

  TXMLTransformType_ = class(TXMLNodeCollection, IXMLTransformType_)
  protected
    { IXMLTransformType_ }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ }

  TXMLDigestMethod_ = class(TXMLNode, IXMLDigestMethod_)
  protected
    { IXMLDigestMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ }

  TXMLSignatureValueType_ = class(TXMLNode, IXMLSignatureValueType_)
  protected
    { IXMLSignatureValueType_ }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ }

  TXMLKeyInfoType_ = class(TXMLNode, IXMLKeyInfoType_)
  protected
    { IXMLKeyInfoType_ }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ }

  TXMLX509DataType_ = class(TXMLNode, IXMLX509DataType_)
  protected
    { IXMLX509DataType_ }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTRetCancNFe }

  TXMLTRetCancNFe = class(TXMLNode, IXMLTRetCancNFe)
  protected
    { IXMLTRetCancNFe }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCanc }

  TXMLInfCanc = class(TXMLNode, IXMLInfCanc)
  protected
    { IXMLInfCanc }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
  end;

{ TXMLTProcEvento }

  TXMLTProcEvento = class(TXMLNode, IXMLTProcEvento)
  protected
    { IXMLTProcEvento }
    function Get_Versao: UnicodeString;
    function Get_Evento: IXMLTEvento;
    function Get_RetEvento: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEventoList }

  TXMLTProcEventoList = class(TXMLNodeCollection, IXMLTProcEventoList)
  protected
    { IXMLTProcEventoList }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
  end;

{ TXMLTEvento }

  TXMLTEvento = class(TXMLNode, IXMLTEvento)
  protected
    { IXMLTEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfEvento }

  TXMLInfEvento = class(TXMLNode, IXMLInfEvento)
  protected
    { IXMLInfEvento }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_ChNFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_VerEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
    procedure Set_VerEvento(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDetEvento }

  TXMLDetEvento = class(TXMLNode, IXMLDetEvento)
  protected
    { IXMLDetEvento }
  end;

{ TXMLTRetEvento }

  TXMLTRetEvento = class(TXMLNode, IXMLTRetEvento)
  protected
    { IXMLTRetEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetconsSitNFe(Doc: IXMLDocument): IXMLTConsSitNFe;
function LoadconsSitNFe(const FileName: string): IXMLTConsSitNFe;
function NewconsSitNFe: IXMLTConsSitNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsSitNFe(Doc: IXMLDocument): IXMLTConsSitNFe;
begin
  Result := Doc.GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

function LoadconsSitNFe(const FileName: string): IXMLTConsSitNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

function NewconsSitNFe: IXMLTConsSitNFe;
begin
  Result := NewXMLDocument.GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

{ TXMLTConsSitNFe }

function TXMLTConsSitNFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsSitNFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsSitNFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsSitNFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsSitNFe.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsSitNFe.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsSitNFe.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLTConsSitNFe.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

{ TXMLTRetConsSitNFe }

procedure TXMLTRetConsSitNFe.AfterConstruction;
begin
  RegisterChildNode('protNFe', TXMLTProtNFe);
  RegisterChildNode('retCancNFe', TXMLTRetCancNFe);
  RegisterChildNode('procEventoNFe', TXMLTProcEvento);
  FProcEventoNFe := CreateCollection(TXMLTProcEventoList, IXMLTProcEvento, 'procEventoNFe') as IXMLTProcEventoList;
  inherited;
end;

function TXMLTRetConsSitNFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsSitNFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLTRetConsSitNFe.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

function TXMLTRetConsSitNFe.Get_ProtNFe: IXMLTProtNFe;
begin
  Result := ChildNodes['protNFe'] as IXMLTProtNFe;
end;

function TXMLTRetConsSitNFe.Get_RetCancNFe: IXMLTRetCancNFe;
begin
  Result := ChildNodes['retCancNFe'] as IXMLTRetCancNFe;
end;

function TXMLTRetConsSitNFe.Get_ProcEventoNFe: IXMLTProcEventoList;
begin
  Result := FProcEventoNFe;
end;

{ TXMLTProtNFe }

procedure TXMLTProtNFe.AfterConstruction;
begin
  RegisterChildNode('infProt', TXMLInfProt);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTProtNFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProtNFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProtNFe.Get_InfProt: IXMLInfProt;
begin
  Result := ChildNodes['infProt'] as IXMLInfProt;
end;

function TXMLTProtNFe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfProt }

function TXMLInfProt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfProt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfProt.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfProt.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfProt.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLInfProt.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLInfProt.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLInfProt.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

function TXMLInfProt.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfProt.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfProt.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfProt.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

function TXMLInfProt.Get_DigVal: UnicodeString;
begin
  Result := ChildNodes['digVal'].Text;
end;

procedure TXMLInfProt.Set_DigVal(Value: UnicodeString);
begin
  ChildNodes['digVal'].NodeValue := Value;
end;

function TXMLInfProt.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLInfProt.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLInfProt.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLInfProt.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

{ TXMLSignatureType_ }

procedure TXMLSignatureType_.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_);
  inherited;
end;

function TXMLSignatureType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_.Get_SignedInfo: IXMLSignedInfoType_;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_;
end;

function TXMLSignatureType_.Get_SignatureValue: IXMLSignatureValueType_;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_;
end;

function TXMLSignatureType_.Get_KeyInfo: IXMLKeyInfoType_;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_;
end;

{ TXMLSignedInfoType_ }

procedure TXMLSignedInfoType_.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_);
  RegisterChildNode('Reference', TXMLReferenceType_);
  inherited;
end;

function TXMLSignedInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_;
end;

function TXMLSignedInfoType_.Get_SignatureMethod: IXMLSignatureMethod_;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_;
end;

function TXMLSignedInfoType_.Get_Reference: IXMLReferenceType_;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_;
end;

{ TXMLCanonicalizationMethod_ }

function TXMLCanonicalizationMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ }

function TXMLSignatureMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ }

procedure TXMLReferenceType_.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_);
  inherited;
end;

function TXMLReferenceType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_.Get_Transforms: IXMLTransformsType_;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_;
end;

function TXMLReferenceType_.Get_DigestMethod: IXMLDigestMethod_;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_;
end;

function TXMLReferenceType_.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ }

procedure TXMLTransformsType_.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_;
  inherited;
end;

function TXMLTransformsType_.Get_Transform(Index: Integer): IXMLTransformType_;
begin
  Result := List[Index] as IXMLTransformType_;
end;

function TXMLTransformsType_.Add: IXMLTransformType_;
begin
  Result := AddItem(-1) as IXMLTransformType_;
end;

function TXMLTransformsType_.Insert(const Index: Integer): IXMLTransformType_;
begin
  Result := AddItem(Index) as IXMLTransformType_;
end;

{ TXMLTransformType_ }

procedure TXMLTransformType_.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ }

function TXMLDigestMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ }

function TXMLSignatureValueType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ }

procedure TXMLKeyInfoType_.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_);
  inherited;
end;

function TXMLKeyInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_.Get_X509Data: IXMLX509DataType_;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_;
end;

{ TXMLX509DataType_ }

function TXMLX509DataType_.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTRetCancNFe }

procedure TXMLTRetCancNFe.AfterConstruction;
begin
  RegisterChildNode('infCanc', TXMLInfCanc);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTRetCancNFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetCancNFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetCancNFe.Get_InfCanc: IXMLInfCanc;
begin
  Result := ChildNodes['infCanc'] as IXMLInfCanc;
end;

function TXMLTRetCancNFe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfCanc }

function TXMLInfCanc.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfCanc.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfCanc.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfCanc.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfCanc.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLInfCanc.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLInfCanc.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLInfCanc.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLInfCanc.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLInfCanc.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLInfCanc.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLInfCanc.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLInfCanc.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLInfCanc.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

function TXMLInfCanc.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfCanc.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfCanc.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfCanc.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

{ TXMLTProcEvento }

procedure TXMLTProcEvento.AfterConstruction;
begin
  RegisterChildNode('evento', TXMLTEvento);
  RegisterChildNode('retEvento', TXMLTRetEvento);
  inherited;
end;

function TXMLTProcEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProcEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProcEvento.Get_Evento: IXMLTEvento;
begin
  Result := ChildNodes['evento'] as IXMLTEvento;
end;

function TXMLTProcEvento.Get_RetEvento: IXMLTRetEvento;
begin
  Result := ChildNodes['retEvento'] as IXMLTRetEvento;
end;

{ TXMLTProcEventoList }

function TXMLTProcEventoList.Add: IXMLTProcEvento;
begin
  Result := AddItem(-1) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Insert(const Index: Integer): IXMLTProcEvento;
begin
  Result := AddItem(Index) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Get_Item(Index: Integer): IXMLTProcEvento;
begin
  Result := List[Index] as IXMLTProcEvento;
end;

{ TXMLTEvento }

procedure TXMLTEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfEvento }

procedure TXMLInfEvento.AfterConstruction;
begin
  RegisterChildNode('detEvento', TXMLDetEvento);
  inherited;
end;

function TXMLInfEvento.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfEvento.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfEvento.Get_COrgao: UnicodeString;
begin
  Result := ChildNodes['cOrgao'].Text;
end;

procedure TXMLInfEvento.Set_COrgao(Value: UnicodeString);
begin
  ChildNodes['cOrgao'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfEvento.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfEvento.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLInfEvento.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLInfEvento.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLInfEvento.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLInfEvento.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLInfEvento.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DhEvento: UnicodeString;
begin
  Result := ChildNodes['dhEvento'].Text;
end;

procedure TXMLInfEvento.Set_DhEvento(Value: UnicodeString);
begin
  ChildNodes['dhEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpEvento: UnicodeString;
begin
  Result := ChildNodes['tpEvento'].Text;
end;

procedure TXMLInfEvento.Set_TpEvento(Value: UnicodeString);
begin
  ChildNodes['tpEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_NSeqEvento: UnicodeString;
begin
  Result := ChildNodes['nSeqEvento'].Text;
end;

procedure TXMLInfEvento.Set_NSeqEvento(Value: UnicodeString);
begin
  ChildNodes['nSeqEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_VerEvento: UnicodeString;
begin
  Result := ChildNodes['verEvento'].Text;
end;

procedure TXMLInfEvento.Set_VerEvento(Value: UnicodeString);
begin
  ChildNodes['verEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DetEvento: IXMLDetEvento;
begin
  Result := ChildNodes['detEvento'] as IXMLDetEvento;
end;

{ TXMLDetEvento }

{ TXMLTRetEvento }

procedure TXMLTRetEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTRetEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTRetEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

end.