
{****************************************************************************************************************************}
{                                                                                                                            }
{                                                      XML Data Binding                                                      }
{                                                                                                                            }
{         Generated on: 30/06/2018 18:46:33                                                                                  }
{       Generated from: C:\_Compilers\Delphi_XE2\VCL\MDL\NFe\v_04_00\XSD\PL_009_V4_2016_002_v160\retConsStatServ_v4.00.xsd   }
{   Settings stored in: C:\_Compilers\Delphi_XE2\VCL\MDL\NFe\v_04_00\XSD\PL_009_V4_2016_002_v160\retConsStatServ_v4.00.xdb   }
{                                                                                                                            }
{****************************************************************************************************************************}

unit retConsStatServ_v400;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTRetConsStatServ = interface;
  IXMLTConsStatServ = interface;

{ IXMLTRetConsStatServ }

  IXMLTRetConsStatServ = interface(IXMLNode)
    ['{E8BD0502-4B53-4EF0-9A99-526999CF3357}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: UnicodeString;
    function Get_DhRetorno: UnicodeString;
    function Get_XObs: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: UnicodeString);
    procedure Set_DhRetorno(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property TMed: UnicodeString read Get_TMed write Set_TMed;
    property DhRetorno: UnicodeString read Get_DhRetorno write Set_DhRetorno;
    property XObs: UnicodeString read Get_XObs write Set_XObs;
  end;

{ IXMLTConsStatServ }

  IXMLTConsStatServ = interface(IXMLNode)
    ['{8D50D403-DB33-47D8-8AEE-5A77A5F2C720}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_XServ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
  end;

{ Forward Decls }

  TXMLTRetConsStatServ = class;
  TXMLTConsStatServ = class;

{ TXMLTRetConsStatServ }

  TXMLTRetConsStatServ = class(TXMLNode, IXMLTRetConsStatServ)
  protected
    { IXMLTRetConsStatServ }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: UnicodeString;
    function Get_DhRetorno: UnicodeString;
    function Get_XObs: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: UnicodeString);
    procedure Set_DhRetorno(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
  end;

{ TXMLTConsStatServ }

  TXMLTConsStatServ = class(TXMLNode, IXMLTConsStatServ)
  protected
    { IXMLTConsStatServ }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_XServ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
  end;

implementation

{ TXMLTRetConsStatServ }

function TXMLTRetConsStatServ.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsStatServ.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsStatServ.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsStatServ.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsStatServ.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_TMed: UnicodeString;
begin
  Result := ChildNodes['tMed'].Text;
end;

procedure TXMLTRetConsStatServ.Set_TMed(Value: UnicodeString);
begin
  ChildNodes['tMed'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRetorno: UnicodeString;
begin
  Result := ChildNodes['dhRetorno'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRetorno(Value: UnicodeString);
begin
  ChildNodes['dhRetorno'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XObs: UnicodeString;
begin
  Result := ChildNodes['xObs'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XObs(Value: UnicodeString);
begin
  ChildNodes['xObs'].NodeValue := Value;
end;

{ TXMLTConsStatServ }

function TXMLTConsStatServ.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsStatServ.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsStatServ.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsStatServ.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsStatServ.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTConsStatServ.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTConsStatServ.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsStatServ.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

end.