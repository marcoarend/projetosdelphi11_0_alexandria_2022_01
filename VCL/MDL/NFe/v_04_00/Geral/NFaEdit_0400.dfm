object FmNFaEdit_0400: TFmNFaEdit_0400
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-001 :: Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
  ClientHeight = 694
  ClientWidth = 1076
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 109
    Width = 1076
    Height = 585
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Dados Gerais '
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1068
        Height = 487
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cabe'#231'alho e rodap'#233' da NF '
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1060
            Height = 459
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 1060
              Height = 459
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
              object Panel3: TPanel
                Left = 2
                Top = 15
                Width = 1056
                Height = 173
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object LaSerieNF: TLabel
                  Left = 11
                  Top = 4
                  Width = 44
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'S'#233'rie NF:'
                end
                object Label15: TLabel
                  Left = 62
                  Top = 4
                  Width = 61
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = #218'lt. NF emit.:'
                  FocusControl = DBEdit2
                end
                object Label16: TLabel
                  Left = 142
                  Top = 4
                  Width = 45
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#186' Limite:'
                  FocusControl = DBEdit3
                end
                object LaNumeroNF: TLabel
                  Left = 223
                  Top = 4
                  Width = 71
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#186' Nota Fiscal:'
                end
                object Label4: TLabel
                  Left = 11
                  Top = 47
                  Width = 58
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Quantidade:'
                end
                object Label5: TLabel
                  Left = 99
                  Top = 47
                  Width = 41
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Esp'#233'cie:'
                end
                object Label6: TLabel
                  Left = 335
                  Top = 47
                  Width = 33
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Marca:'
                end
                object Label7: TLabel
                  Left = 572
                  Top = 47
                  Width = 40
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#250'mero:'
                end
                object Label8: TLabel
                  Left = 809
                  Top = 47
                  Width = 54
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Peso bruto:'
                end
                object Label9: TLabel
                  Left = 889
                  Top = 47
                  Width = 62
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Peso l'#237'quido:'
                end
                object Label18: TLabel
                  Left = 303
                  Top = 4
                  Width = 115
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data e hora Emiss'#227'o NF'
                end
                object Label19: TLabel
                  Left = 640
                  Top = 4
                  Width = 138
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data e hora Sa'#237'da / entrada:'
                end
                object SpeedButton21: TSpeedButton
                  Left = 534
                  Top = 21
                  Width = 23
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '?'
                  OnClick = SpeedButton21Click
                end
                object Label10: TLabel
                  Left = 478
                  Top = 4
                  Width = 50
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'TZD UTC:'
                end
                object Label25: TLabel
                  Left = 818
                  Top = 4
                  Width = 50
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'TZD UTC:'
                end
                object Label26: TLabel
                  Left = 562
                  Top = 4
                  Width = 67
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Hor'#225'rio ver'#227'o:'
                end
                object Label27: TLabel
                  Left = 876
                  Top = 4
                  Width = 67
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Hor'#225'rio ver'#227'o:'
                end
                object Label22: TLabel
                  Left = 11
                  Top = 90
                  Width = 90
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Vag'#227'o (transporte):'
                end
                object Label23: TLabel
                  Left = 187
                  Top = 90
                  Width = 85
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Balsa (transporte):'
                end
                object Label24: TLabel
                  Left = 11
                  Top = 132
                  Width = 103
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'E-mail do destinat'#225'rio:'
                end
                object DBEdit2: TDBEdit
                  Left = 62
                  Top = 21
                  Width = 77
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  DataField = 'Sequencial'
                  DataSource = DsImprime
                  TabOrder = 1
                end
                object DBEdit3: TDBEdit
                  Left = 142
                  Top = 21
                  Width = 77
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  DataField = 'MaxSeqLib'
                  DataSource = DsImprime
                  TabOrder = 2
                end
                object EdNumeroNF: TdmkEdit
                  Left = 223
                  Top = 21
                  Width = 77
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdQuantidade: TdmkEdit
                  Left = 11
                  Top = 64
                  Width = 85
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 12
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdEspecie: TdmkEdit
                  Left = 99
                  Top = 64
                  Width = 233
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 13
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdMarca: TdmkEdit
                  Left = 335
                  Top = 64
                  Width = 232
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 14
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdNumero: TdmkEdit
                  Left = 572
                  Top = 64
                  Width = 232
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 15
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdkgBruto: TdmkEdit
                  Left = 809
                  Top = 64
                  Width = 76
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 16
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdkgLiqui: TdmkEdit
                  Left = 889
                  Top = 64
                  Width = 76
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 17
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object TPDtEmissNF: TdmkEditDateTimePicker
                  Left = 303
                  Top = 21
                  Width = 111
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 44644.000000000000000000
                  Time = 0.587303645843348900
                  TabOrder = 4
                  OnClick = TPDtEmissNFChange
                  OnChange = TPDtEmissNFChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDtEntraSai: TdmkEditDateTimePicker
                  Left = 640
                  Top = 21
                  Width = 111
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 44644.000000000000000000
                  Time = 0.587380277778720500
                  TabOrder = 8
                  OnClick = TPDtEmissNFChange
                  OnChange = TPDtEmissNFChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdSerieNF: TdmkEdit
                  Left = 11
                  Top = 21
                  Width = 48
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EddhEmiTZD: TdmkEdit
                  Left = 478
                  Top = 21
                  Width = 55
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 6
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfTZD_UTC
                  Texto = '+00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EddhSaiEntTZD: TdmkEdit
                  Left = 818
                  Top = 21
                  Width = 56
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 10
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfTZD_UTC
                  Texto = '+00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EddhEmiVerao: TdmkEdit
                  Left = 562
                  Top = 21
                  Width = 75
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -16
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EddhSaiEntVerao: TdmkEdit
                  Left = 876
                  Top = 21
                  Width = 68
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -16
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 11
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdHrEmi: TdmkEdit
                  Left = 416
                  Top = 21
                  Width = 60
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 5
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  QryCampo = 'HrEmi'
                  UpdCampo = 'HrEmi'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdHrEntraSai: TdmkEdit
                  Left = 753
                  Top = 21
                  Width = 60
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 9
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  QryCampo = 'HrEntraSai'
                  UpdCampo = 'HrEntraSai'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdVagao: TdmkEdit
                  Left = 11
                  Top = 107
                  Width = 172
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 18
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdBalsa: TdmkEdit
                  Left = 187
                  Top = 107
                  Width = 172
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 19
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object Eddest_email: TdmkEdit
                  Left = 11
                  Top = 149
                  Width = 351
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 20
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object RGCRT: TdmkRadioGroup
                  Left = 369
                  Top = 95
                  Width = 652
                  Height = 77
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CRT (C'#243'digo de Regime Tribut'#225'rio): '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Simples Nacional'
                    '2 - Simples Nacional - excesso de sublimite de receita bruta'
                    '3 - Regime Normal')
                  TabOrder = 21
                  UpdType = utYes
                  OldValor = 0
                end
              end
              object MeinfAdic_infCpl: TdmkMemo
                Left = 2
                Top = 285
                Width = 1056
                Height = 172
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 1
                OnKeyDown = MeinfAdic_infCplKeyDown
                UpdType = utYes
              end
              object StaticText1: TStaticText
                Left = 2
                Top = 268
                Width = 1056
                Height = 17
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Observa'#231#245'es adicionais NF-e [F4]'
                TabOrder = 2
              end
              object Panel11: TPanel
                Left = 2
                Top = 188
                Width = 1056
                Height = 80
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 3
                object SbGenCtbCPsqGC: TSpeedButton
                  Left = 728
                  Top = 55
                  Width = 23
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'GC'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  OnClick = SbGenCtbCPsqGCClick
                end
                object SbGenCtbC: TSpeedButton
                  Left = 752
                  Top = 55
                  Width = 22
                  Height = 23
                  Hint = 'Inclui conta'
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  OnClick = SbGenCtbCClick
                end
                object SbGenCtbD: TSpeedButton
                  Left = 752
                  Top = 15
                  Width = 22
                  Height = 22
                  Hint = 'Inclui conta'
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  OnClick = SbGenCtbDClick
                end
                object SbGenCtbDPsqGC: TSpeedButton
                  Left = 728
                  Top = 15
                  Width = 23
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'GC'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  OnClick = SbGenCtbDPsqGCClick
                end
                object Label66: TLabel
                  Left = 2
                  Top = 40
                  Width = 141
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Conta a cr'#233'dito [F7] pesquisa:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object Label3: TLabel
                  Left = 2
                  Top = 0
                  Width = 138
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Conta a d'#233'bito [F7] pesquisa:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object CBGenCtbD: TdmkDBLookupComboBox
                  Left = 164
                  Top = 16
                  Width = 562
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsContas1
                  ParentFont = False
                  TabOrder = 2
                  dmkEditCB = EdGenCtbD
                  QryCampo = 'Genero'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object CBGenCtbC: TdmkDBLookupComboBox
                  Left = 164
                  Top = 56
                  Width = 562
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsContas2
                  ParentFont = False
                  TabOrder = 5
                  dmkEditCB = EdGenCtbC
                  QryCampo = 'GenCtb'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdGenCtbC: TdmkEditCB
                  Left = 105
                  Top = 56
                  Width = 55
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'GenCtb'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnRedefinido = EdGenCtbCRedefinido
                  DBLookupComboBox = CBGenCtbC
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object DBNiveis2: TDBEdit
                  Left = 3
                  Top = 56
                  Width = 98
                  Height = 21
                  TabStop = False
                  DataField = 'Niveis'
                  DataSource = DsContas2
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 3
                end
                object EdGenCtbD: TdmkEditCB
                  Left = 105
                  Top = 16
                  Width = 55
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'Genero'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnRedefinido = EdGenCtbDRedefinido
                  DBLookupComboBox = CBGenCtbD
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object DBNiveis1: TDBEdit
                  Left = 3
                  Top = 16
                  Width = 98
                  Height = 21
                  TabStop = False
                  DataField = 'Niveis'
                  DataSource = DsContas1
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
              end
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Outras informa'#231#245'es'
          ImageIndex = 1
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 1060
            Height = 459
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1060
              Height = 78
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object GroupBox10: TGroupBox
                Left = 0
                Top = 0
                Width = 679
                Height = 78
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 
                  ' Informa'#231#245'es de compras (Obs.: A nota de  empenho se refere a co' +
                  'mpras p'#250'blicas): '
                TabOrder = 0
                object Label183: TLabel
                  Left = 16
                  Top = 17
                  Width = 88
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Nota de empenho:'
                  FocusControl = EdCompra_XNEmp
                end
                object Label184: TLabel
                  Left = 112
                  Top = 17
                  Width = 36
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Pedido:'
                  FocusControl = EdCompra_XPed
                end
                object Label185: TLabel
                  Left = 396
                  Top = 17
                  Width = 96
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Contrato de compra:'
                  FocusControl = EdCompra_XCont
                end
                object EdCompra_XNEmp: TdmkEdit
                  Left = 16
                  Top = 34
                  Width = 92
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XNEmp'
                  UpdCampo = 'Compra_XNEmp'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XPed: TdmkEdit
                  Left = 112
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XPed'
                  UpdCampo = 'Compra_XPed'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XCont: TdmkEdit
                  Left = 396
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XCont'
                  UpdCampo = 'Compra_XCont'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox3: TGroupBox
                Left = 679
                Top = 0
                Width = 381
                Height = 78
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Exporta'#231#227'o: (NF-e: ZA - Informa'#231#245'es de com'#233'rcio exterior) '
                TabOrder = 1
                object Label20: TLabel
                  Left = 16
                  Top = 17
                  Width = 17
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'UF:'
                end
                object Label21: TLabel
                  Left = 58
                  Top = 17
                  Width = 224
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Local onde ocorrer'#225' o embarque dos produtos: '
                end
                object EdUFEmbarq: TdmkEdit
                  Left = 16
                  Top = 34
                  Width = 36
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 2
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdxLocEmbarq: TdmkEdit
                  Left = 58
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 78
              Width = 1060
              Height = 79
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Ve'#237'culo: '
              TabOrder = 1
              object Label11: TLabel
                Left = 16
                Top = 21
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Placa:'
              end
              object Label12: TLabel
                Left = 100
                Top = 21
                Width = 17
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'UF:'
              end
              object Label13: TLabel
                Left = 272
                Top = 21
                Width = 358
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Observa'#231#245'es: (Nome motorista, tel. doc., etc.) N'#227'o '#233' impresso na' +
                  ' NF/NF-e!!'
              end
              object RNTC: TLabel
                Left = 134
                Top = 21
                Width = 71
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'RNTC (ANTT):'
              end
              object EdPlacaNr: TdmkEdit
                Left = 16
                Top = 37
                Width = 79
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPlacaUF: TdmkEdit
                Left = 100
                Top = 37
                Width = 28
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdObservacao: TdmkEdit
                Left = 272
                Top = 37
                Width = 375
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRNTC: TdmkEdit
                Left = 134
                Top = 37
                Width = 134
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                MaxLength = 20
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
      object GBRodaPe: TGroupBox
        Left = 0
        Top = 487
        Width = 1068
        Height = 70
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1064
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1a: TLabel
            Left = 222
            Top = 7
            Width = 465
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
              'DID-005 '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2a: TLabel
            Left = 221
            Top = 6
            Width = 465
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
              'DID-005 '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso1b: TLabel
            Left = 222
            Top = 29
            Width = 466
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
              'or item).'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2b: TLabel
            Left = 221
            Top = 28
            Width = 466
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
              'or item).'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PnSaiDesis: TPanel
            Left = 876
            Top = 0
            Width = 188
            Height = 53
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida: TBitBtn
              Tag = 13
              Left = 2
              Top = 4
              Width = 128
              Height = 43
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Fechar'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtOk: TBitBtn
            Tag = 14
            Left = 27
            Top = 4
            Width = 128
            Height = 43
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&OK'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtOkClick
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Itens da NF'
      ImageIndex = 1
      object GradeItens: TDBGrid
        Left = 0
        Top = 33
        Width = 1068
        Height = 461
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsStqMovValX
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GradeItensDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'CSOSN'
            PickList.Strings = (
              '101'
              '102'
              '103'
              '201'
              '202'
              '203'
              '300'
              '400'
              '500'
              '900')
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pCredSN'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD'
            Title.Caption = 'Nome do Produto'
            Width = 287
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tam.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_iTotTrib'
            Title.Caption = 'I'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vTotTrib'
            Title.Caption = '$ Tot.Trib.'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pTotTrib'
            Title.Caption = '% Trib'
            Width = 43
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 494
        Width = 1068
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitTop = 493
        object BtTodos: TBitBtn
          Tag = 127
          Left = 27
          Top = 5
          Width = 128
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtTodosClick
        end
        object BtSelecionados: TBitBtn
          Tag = 241
          Left = 159
          Top = 5
          Width = 128
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Selecionados'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtSelecionadosClick
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1068
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Label1: TLabel
          Left = 12
          Top = 2
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label2: TLabel
          Left = 11
          Top = 1
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'NFes referenciadas'
      ImageIndex = 2
      object Panel8: TPanel
        Left = 0
        Top = 504
        Width = 1068
        Height = 53
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel9: TPanel
          Left = 926
          Top = 0
          Width = 142
          Height = 53
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
        end
        object BtStqMovNFsRef: TBitBtn
          Tag = 456
          Left = 132
          Top = 4
          Width = 128
          Height = 43
          Cursor = crHandPoint
          Caption = 'NFs &ref.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtStqMovNFsRefClick
        end
      end
      object DBGStqMovNFsRef: TDBGrid
        Left = 0
        Top = 0
        Width = 1068
        Height = 504
        Align = alClient
        DataSource = DsStqMovNFsRef
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'refNFe'
            Title.Caption = 'Chave de acesso da NF-e'
            Width = 286
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_cUF'
            Title.Caption = 'UF do emitente'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_AAMM'
            Title.Caption = 'Ano e m'#234's da emiss'#227'o da NF'
            Width = 151
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_CNPJ'
            Title.Caption = 'CNPJ'
            Width = 123
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_mod'
            Title.Caption = 'Modelo da NF'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_serie'
            Title.Caption = 'S'#233'rie da NF'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_nNF'
            Title.Caption = 'N'#250'mero da NF'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_cUF'
            Title.Caption = 'NFP UF'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNF_AAMM'
            Title.Caption = 'NFP AAMM'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_CNPJ'
            Title.Caption = 'NFP CNPJ'
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_CPF'
            Title.Caption = 'NFP CPF'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_IE'
            Title.Caption = 'NFP IE'
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_mod'
            Title.Caption = 'NFP Mod.'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_serie'
            Title.Caption = 'NFP Serie'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refNFP_nNF'
            Title.Caption = 'NFP n.NF'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refCTe'
            Title.Caption = 'NFP CTe'
            Width = 286
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refECF_mod'
            Title.Caption = 'ECF Mod.'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refECF_nECF'
            Title.Caption = 'Num ECF'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'refECF_nCOO'
            Title.Caption = 'CCO ECF'
            Width = 68
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1076
    Height = 51
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1021
      Top = 0
      Width = 55
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 9
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 55
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 55
      Top = 0
      Width = 966
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 1076
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1072
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 17
        Top = 2
        Width = 120
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 120
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPesqPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Preco '
      'FROM tabeprcgrg'
      'WHERE Nivel1=:P0')
    Left = 564
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqPrcPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 836
    Top = 56
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT smva.ID) Itens, smva.CFOP, '
      'frc.OrdCFOPGer, cfp.Nome, Descricao, Complementacao'
      'FROM stqmovvala smva'
      'LEFT JOIN fisregcfop frc ON frc.Codigo=1 AND frc.CFOP=smva.CFOP'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=smva.CFOP'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'GROUP BY smva.CFOP'
      'ORDER BY frc.OrdCFOPGer DESC')
    Left = 836
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCFOPItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 6
    end
    object QrCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrFatPedNFs: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP, '
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*'
      'FROM stqmovvala smva'
      'LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa'
      'LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa '
      '          AND smva.Tipo=:P0 '
      '          AND smna.OriCodi=:P1'
      'WHERE smva.Tipo=:P2'
      'AND smva.OriCodi=:P3'
      'AND ent.Codigo<>:P4'
      'ORDER BY ent.Filial')
    Left = 636
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrImprimeAfterOpen
    BeforeClose = QrImprimeBeforeClose
    SQL.Strings = (
      'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprimeempr ime'
      'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=ime.ParamsNFs'
      'WHERE ime.Empresa=:P0'
      'AND ime.Codigo=:P1')
    Left = 700
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
      Origin = 'imprime.SerieNF'
      Required = True
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
      Origin = 'paramsnfs.Sequencial'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'paramsnfs.IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Origin = 'paramsnfs.Controle'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Origin = 'paramsnfs.MaxSeqLib'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
      Origin = 'imprime.TipoImpressao'
      Required = True
    end
    object QrImprimeSerieNF_Normal: TIntegerField
      FieldName = 'SerieNF_Normal'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Associada, emp.AssocModNF, emp.CRT, emp.CSOSN,'
      
        'emp.pCredSNAlq, emp.pCredSNMez, emp.NFeInfCpl, cpl.Texto NFeInfC' +
        'pl_TXT'
      'FROM paramsemp emp'
      'LEFT JOIN nfeinfcpl cpl ON cpl.Codigo = emp.NFeInfCpl'
      'WHERE emp.Codigo=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
    end
    object QrParamsEmppCredSN_Cfg: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'pCredSN_Cfg'
      Calculated = True
    end
    object QrParamsEmpNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
    end
    object QrParamsEmpNFeInfCpl_TXT: TWideMemoField
      FieldName = 'NFeInfCpl_TXT'
      BlobType = ftWideMemo
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 700
    Top = 56
  end
  object QrNF_X: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2')
    Left = 240
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 288
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 340
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 388
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1 '
      'AND Empresa=:P2')
    Left = 444
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrSumXprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrSumXprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrSumXprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object DsStqMovValX: TDataSource
    DataSet = QrStqMovValX
    Left = 952
    Top = 56
  end
  object TbStqMovValX_: TMySQLTable
    Database = Dmod.MyDB
    AfterInsert = TbStqMovValX_AfterInsert
    TableName = 'stqmovvalx'
    Left = 564
    Top = 56
  end
  object QrEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CRT'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 892
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCRT: TSmallintField
      FieldName = 'CRT'
    end
  end
  object QrStqMovValX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 952
    Top = 8
    object QrStqMovValXID: TIntegerField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QrStqMovValXGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object QrStqMovValXCFOP: TWideStringField
      FieldName = 'CFOP'
      ReadOnly = True
      Size = 6
    end
    object QrStqMovValXQtde: TFloatField
      FieldName = 'Qtde'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.000;-#,###,###,###,##0.000; '
    end
    object QrStqMovValXPreco: TFloatField
      FieldName = 'Preco'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXTotal: TFloatField
      FieldName = 'Total'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrStqMovValXpCredSN: TFloatField
      FieldName = 'pCredSN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      ReadOnly = True
      Size = 50
    end
    object QrStqMovValXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      ReadOnly = True
      Size = 5
    end
    object QrStqMovValXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      ReadOnly = True
      Size = 30
    end
    object QrStqMovValXiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrStqMovValXvTotTrib: TFloatField
      FieldName = 'vTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovValXTXT_iTotTrib: TWideStringField
      FieldName = 'TXT_iTotTrib'
      Size = 1
    end
    object QrStqMovValXpTotTrib: TFloatField
      FieldName = 'pTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrCSOSN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM stqmovvala smva'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'AND smva.CSOSN=0')
    Left = 636
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCSOSNItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object PMStqMovNFsRef: TPopupMenu
    OnPopup = PMStqMovNFsRefPopup
    Left = 185
    Top = 567
    object IncluiNFreferenciada1: TMenuItem
      Caption = '&Inclui NF referenciada'
      OnClick = IncluiNFreferenciada1Click
    end
    object Alterarefenciamentoatual1: TMenuItem
      Caption = '&Altera refenciamento atual'
      OnClick = Alterarefenciamentoatual1Click
    end
    object Excluireferenciamentos1: TMenuItem
      Caption = '&Exclui referenciamento(s)'
      OnClick = Excluireferenciamentos1Click
    end
  end
  object QrStqMovNFsRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabb nfeb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 500
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrStqMovNFsRefFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrStqMovNFsRefFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrStqMovNFsRefEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovNFsRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrStqMovNFsRefrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrStqMovNFsRefrefNFeSig: TWideStringField
      FieldName = 'refNFeSig'
      Size = 44
    end
    object QrStqMovNFsRefrefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
    end
    object QrStqMovNFsRefrefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
      DisplayFormat = '0000'
    end
    object QrStqMovNFsRefrefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Size = 14
    end
    object QrStqMovNFsRefrefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
      DisplayFormat = '0'
    end
    object QrStqMovNFsRefrefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
      DisplayFormat = '000'
    end
    object QrStqMovNFsRefrefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
      DisplayFormat = '000000000'
    end
    object QrStqMovNFsRefLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrStqMovNFsRefDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrStqMovNFsRefDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrStqMovNFsRefUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrStqMovNFsRefUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrStqMovNFsRefAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqMovNFsRefAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqMovNFsRefrefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
    end
    object QrStqMovNFsRefrefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
    end
    object QrStqMovNFsRefrefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Size = 14
    end
    object QrStqMovNFsRefrefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Size = 11
    end
    object QrStqMovNFsRefrefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Size = 14
    end
    object QrStqMovNFsRefrefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
    end
    object QrStqMovNFsRefrefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
    end
    object QrStqMovNFsRefrefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
    end
    object QrStqMovNFsRefrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrStqMovNFsRefrefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Size = 2
    end
    object QrStqMovNFsRefrefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
    end
    object QrStqMovNFsRefrefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
    end
    object QrStqMovNFsRefQualNFref: TSmallintField
      FieldName = 'QualNFref'
    end
  end
  object DsStqMovNFsRef: TDataSource
    DataSet = QrStqMovNFsRef
    Left = 500
    Top = 60
  end
  object QrContas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT('
      '  pl.Codigo, ".", '
      '  cj.Codigo, ".", '
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".", '
      '  co.Codigo) Niveis, '
      ' co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 652
    Top = 456
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas1Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas1Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas1Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas1Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas1Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas1Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas1Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas1Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas1Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas1Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas1Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas1Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas1Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas1UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas1UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas1NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas1NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas1Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas1Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 652
    Top = 504
  end
  object QrContas2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT('
      '  pl.Codigo, ".", '
      '  cj.Codigo, ".", '
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".", '
      '  co.Codigo) Niveis, '
      'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 712
    Top = 456
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas2Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas2Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas2ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas2Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas2Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas2Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas2Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas2Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas2Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas2Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas2Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas2Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas2Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas2Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas2UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas2UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas2NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas2NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas2Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas2Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas2: TDataSource
    DataSet = QrContas2
    Left = 712
    Top = 504
  end
end
