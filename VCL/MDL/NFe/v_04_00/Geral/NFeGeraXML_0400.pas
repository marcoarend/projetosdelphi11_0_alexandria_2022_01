unit NFeGeraXML_0400;

interface

uses
  System.Net.HttpClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, DB, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Variants, Math, mySQLDbTables, dmkImage,
  UnDmkEnums, CAPICOM_TLB, SOAPHTTPTrans, Soap.InvokeRegistry,
  System.Net.URLClient, Soap.Rio, Soap.SOAPHTTPClient, (*JwaWinCrypt,*) XMLDoc, WinInet, SOAPConst,
  consCad_v200,
  distDFeInt_v101,
  nfe_v400, consReciNFe_v400, consStatServ_v400, inutNFe_v400,
  consSitNFe_v400, UnProjGroup_Vars, UnGrl_Vars, UnGrl_Consts,
  System.Win.ComObj;
  (*Soap.InvokeRegistry, Soap.Rio, Soap.SOAPHTTPClient,
  System.Net.URLClient;*)

type
  TEnumeracao = (enumProcessoEmissao,
                 enumtpEmis,
                 enumFinNFe,
                 enumTpNF,
                 enumOrigemMercadoria,
                 enumCSTICMS);
  TTipoConsumoWS = (tcwsNFeAutorizacao,
                    tcwsNfeConsultaProtocolo,
                    tcwsNfeInutilizacao,
                    tcwsNFeRetAutorizacao,
                    tcwsNfeStatusServico,
                    tcwsRecepcaoEvento,
                    tcwsNfeConsultaCadastro,
                    //tcwsConsultaNFeDest,
                    //
                    //tcwsNFeDownloadNF,
                    //tcwsAutorizacao,
                    //tcwsRetAutorizacao,
                    //
                    tcwsDistDFeInt);
  TFmNFeGeraXML_0400 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    HTTPRIO1: THTTPRIO;
    HTTPReqResp1: THTTPReqResp;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function  Def(const Codigo, ID: String; const Source: Variant; var Dest:
              String; Forca: Boolean = False): Boolean;
    function  GerarXMLdaNFe(FatID, FatNum, Empresa: Integer; const nfeID: String;
              GravaCampos, Cliente: Integer): Boolean;
    function  ObtemWebServer2(UFServico: String; Ambiente, CodigoUF: Integer;
              Acao: TTipoConsumoWS; sAviso: String; Modelo: Integer): Boolean;
    function  VersaoWS(Acao: TTipoConsumoWS; Formata: Boolean = True): String;
    function  SeparaDados(Texto: String; Chave: String; MantemChave: Boolean = False ): String;
    function  SeparaDados2(Texto: String; ChaveA, ChaveB: String; MantemChave: Boolean = False ): String;
    function  Def_UTC(const Codigo, ID: String; const Source1, Source2, Source3: Variant;
              var Dest: String): Boolean;
    function  OutroPais_emit(): Boolean;
    function  FMT_IE(Valor: String): String;
    function  OutroPais_dest(): Boolean;
    function  OutroPais_retirada(): Boolean;
    function  OutroPais_entrega(): Boolean;
    procedure ConfiguraReqResp( ReqResp : THTTPReqResp);
  public
    { Public declarations }
    // at� Delphi Tokyo
    {$IfDef VER320}
      procedure OnBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
    {$Else}
    // Delphi Alexandria
      procedure OnBeforePost(const HTTPReqResp: THTTPReqResp; Client: THTTPClient);
    {$EndIf}

    function  NomeAcao(Acao: TTipoConsumoWS): String;
    function  WS_RecepcaoEvento(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; Modelo: Integer): String;
    function  WS_NFeRecepcaoLote(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc; Modelo:
              Integer): String;
    function  WS_NFeInutilizacaoNFe(UFServico: String; Ambiente, CodigoUF, Ano: Byte;
              Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
              XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; Modelo: Integer): String;
    function  DesmontaID_Inutilizacao(const Id: String; var cUF, Ano,
              emitCNPJ, Modelo, Serie, nNFIni, nNFFim: String): Boolean;
    function  WS_NFeConsultaCadastro(Servico_UF, Contribuinte_UF,
              Contribuinte_CNPJ: String; Certificado: String; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
    function  CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
              var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
              GravaCampos, Cliente: Integer): Boolean;
    function  GerarLoteEvento(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    function  GerarLoteNFeNovo(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia: TXXeIndSinc): Boolean;
    function  GerarLoteDownloadNFeConfirmadas(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    function  WS_NFeRetRecepcao(UFServico: String; Ambiente, CodigoUF: Byte;
              Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit; Modelo: Integer): String;
    function  WS_NFeStatusServico(UFServico: String; Ambiente,
              CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; Modelo: Integer): String;
    function  XML_ConsReciNFe(TpAmb: Integer; NRec: String): String;
    function  XML_ConsStatServ(TpAmb, CUF: Integer): String;
    function  StrZero(Num : Real; Zeros, Deci: Integer): String;
    function  XML_ConsCad(xUF, Contribuinte_CNPJ: String): String;
    function  XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte; Ano: Integer;
              CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): String;
    function  Alltrim(const Search: string): string;
    function  TipoXML(NoStandAlone: Boolean): String;
    function  MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
              nNFFim: String; var Id: String): Boolean;
    function  WS_NFeConsultaNF(UFServico: String; Ambiente, CodigoUF: Byte;
              ChaveNFe: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; Modelo: Integer): String;
    function  XML_ConsSitNFe(TpAmb: Integer; ChNFe, VersaoAcao: String): String;
    function  WS_NFeConsultaDistDFeInt(Servico_UF, Interesse_UF: String; TpAmb:
              Integer; CNPJ, CPF: String; ultNSU, NSU: String; Certificado:
              String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit; ItemStr: String): String;
    function  XML_DistDFeInt(TpAmb: Integer; UFAutor, CNPJ, CPF:
              String; ultNSU, NSU: String): String;
  end;

  var
    FmNFeGeraXML_0400: TFmNFeGeraXML_0400;

implementation

uses UnMyObjects, Module, ModuleNFe_0000, UnInternalConsts, UMySQLModule,
  ModuleGeral, DmkDAC_PF, UnDmkProcFunc, NFe_PF, NFeSteps_0400, dmkGeral,
{$IfNDef semNFCe_0000} NFCe_PF, {$EndIf}
  NFeXMLGerencia, UnXXe_PF,
  UnDmkACBr_ParamsEmp;

var
  Cert: ICertificate2;
  arqXML: TXMLDocument;
  cXML: nfe_v400.IXMLTNFe;
  cRefLista: nfe_v400.IXMLNFref;
  cAutXML: nfe_v400.IXMLAutXML;
  cDetLista: nfe_v400.IXMLDet;
  cDILista: nfe_v400.IXMLDI;
  cAdiLista: nfe_v400.IXMLAdi;
  cDetExport: nfe_v400.IXMLDetExport;
  cRebLista: nfe_v400.IXMLTVeiculo;
  cVolLista: nfe_v400.IXMLVol;
  cLacLista: nfe_v400.IXMLLacres;
  cDupLista: nfe_v400.IXMLDup;
  cPagLista: nfe_v400.IXMLDetPag;
  cPagCard: nfe_v400.IXMLCard;
  //cObsCont: nfe_v400.IXMLObsCont;
  //cObsFisco: nfe_v400.IXMLObsFisco;
  cObsCont: nfe_v400.IXMLObsCont_nota;
  cObsFisco: nfe_v400.IXMLObsFisco_nota;
  cProcRefLista: nfe_v400.IXMLProcRef;

  FdocXML: TXMLDocument;
  FURL, FDadosTxt, SerieNF, NumeroNF, CDV, FAssinTxt: String;
  FLaAviso1, FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa, FOrdem, FGravaCampo: Integer;

const
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';

{$R *.DFM}

var
  FHoje: TDateTime;

//

function TFmNFeGeraXML_0400.WS_NFeRetRecepcao(UFServico: String; Ambiente,
  CodigoUF: Byte; Recibo: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
const
  //TipoConsumo = tcwsConsultaLote;
  TipoConsumo = tcwsNFeRetAutorizacao;
var
  sAviso, VersaoDados, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Screen.Cursor := crHourGlass;
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, Modelo) then Exit;
  //
  FDadosTxt := XML_ConsReciNFe(Ambiente, Recibo);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'NfeRetAutorizacao';
  //ProcedimentoRet abaixo!

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text       := FURL;
    //
    ReqResp.Execute(Acao.Text, Stream);
    //
    StrStream := TStringStream.Create('');
    //
    StrStream.CopyFrom(Stream, 0);
    //
    ProcedimentoRet := 'nfeResultMsg';
    Result          := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.WS_NFeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
const
  TipoConsumo = tcwsNfeStatusServico;
{
  Teste =
  '<?xml version="1.0" encoding="utf-8"?><soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="ht' +
  'tp://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope"><soap12:Header><nfeCabecMsg xmlns="h' +
  'ttp://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico4"><cUF>41</cUF><versaoDados>4.00</versaoDados></nfeCabecMsg></soap1' +
  '2:Header><soap12:Body><nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico4"><consStatServ xmlns="h' +
  'ttp://www.portalfiscal.inf.br/nfe" versao="4.00"><tpAmb>1</tpAmb><cUF>41</cUF><xServ>STATUS</xServ></consStatServ></nfeDado' +
  'sMsg></soap12:Body></soap12:Envelope><env:Envelope xmlns:env=''http://www.w3.org/2003/05/soap-envelope''><env:Body xmlns:en' +
  'v=''http://www.w3.org/2003/05/soap-envelope''><nfeResultMsg xmlns=''http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServic' +
  'o4''><retConsStatServ versao=''4.00'' xmlns=''http://www.portalfiscal.inf.br/nfe''><tpAmb>1</tpAmb><verAplic>PR-v4_4_9</ver' +
  'Aplic><cStat>107</cStat><xMotivo>Servico em Operacao</xMotivo><cUF>41</cUF><dhRecbto>2019-09-16T10:15:08-03:00</dhRecbto><t' +
  'Med>1</tMed></retConsStatServ></nfeResultMsg></env:Body></env:Envelope>';
}
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  //Stream: TBytesStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  VerServ: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, Modelo) then Exit;
  //
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);



 {
  VerServ := MyObjects.SelRadioGroup('Selecione a vers�o ', '', ['Auto', '1', '2', '3', '4'], 2);
  case VerServ of
    0:
    begin
      if Pos('NFESTATUSSERVICO4', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico4';
        ProcedimentoRet := 'nfeResultMsg';
      end else
      if Pos('NFESTATUSSERVICO3', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico3';
        ProcedimentoRet := 'nfeStatusServicoNFResult';
      end else
      if Pos('NFESTATUSSERVICO2', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico2';
        ProcedimentoRet := 'nfeStatusServicoNF2Result';
      end else
      begin
        ProcedimentoEnv := 'NfeStatusServico';
        ProcedimentoRet := 'nfeResultMsg';
      end;
    end;
    1:
    begin
      ProcedimentoEnv := 'NfeStatusServico';
      ProcedimentoRet := 'nfeResultMsg';
    end;
    2:
    begin
      ProcedimentoEnv := 'NfeStatusServico2';
      ProcedimentoRet := 'nfeStatusServicoNF2Result';
    end;
    3:
    begin
      ProcedimentoEnv := 'NfeStatusServico3';
      ProcedimentoRet := 'nfeStatusServicoNFResult';
    end;
    4:
}
    begin
      //ProcedimentoEnv := 'NfeStatusServico4';
      ProcedimentoEnv := 'NFeStatusServico4';
        ProcedimentoRet := 'nfeResultMsg';
    end;
  //end;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  //Stream := TBytesStream.Create;
{
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  //Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +     '<nfeDadosMsg>'; // xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';
}

{
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';
}

  // ini 2022-03-01
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">'; //NFeStatusServico4">';
  Texto := Texto +       '<consStatServ xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + VersaoDados + '">'; // 4.00
  //Texto := Texto +           '<tpAmb>2</tpAmb><cUF>35</cUF><xServ>STATUS</xServ>';
  Texto := Texto +           '<tpAmb>' + Geral.FF0(Ambiente) + '</tpAmb>'; // 1 ou 2
  Texto := Texto +           '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +           '<xServ>STATUS</xServ>';
  Texto := Texto +       '</consStatServ>';
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto + '</soap12:Envelope>';

{
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
  //Texto := Texto + '<soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServico4.</faultstring><detail /></soap:Fault>' +
  Texto := Texto +   '<soap:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap:Header>';
  Texto := Texto +   '<soap:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto + '</soap:Body>';
  Texto := Texto + '</soap:Envelope>';
}  // Fim 2022-03-01

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;

     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
     //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServico4/nfeStatusServicoNF';
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
////////////////////////////////////////////////////////////////////////////////
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
////////////////////////////////////////////////////////////////////////////////

{
         StrStream := TStringStream.Create('');
         ReqResp.Execute(Acao.Text, StrStream);
         //StrStream.CopyFrom(Stream, 0);
}
////////////////////////////////////////////////////////////////////////////////



         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //Result := SeparaDados(Teste, ProcedimentoRet);
         //
         StrStream.Free;
    except
      on E: Exception do
      begin
        Abort;
{
       raise Exception.Create('WebService Consulta Status servi�o:' + sLineBreak +
                              '- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
}
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.GerarLoteDownloadNFeConfirmadas(Lote,
  Empresa: Integer; out PathLote, XML_Lote: String; LaAviso1,
  LaAviso2: TLabel): Boolean;
begin
  Result := True;
end;

function TFmNFeGeraXML_0400.GerarLoteNFeNovo(Lote, Empresa: Integer;
  out PathLote, XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia: TXXeIndSinc): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I, IndSinc: Integer;
  XML_STR: String;
  VersaoDados: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeLEnC.Close;
  DmNFe_0000.QrNFeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeLEnC, Dmod.MyDB, 'TFmNFeGeraXML_0400.GerarLoteNFeNovo()');
  //
  if DmNFe_0000.QrNFeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem NF-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmNFe_0000.QrFilialDirNFeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);
(*==============================================================================
Schema XML: enviNFe_v2.00.xsd #
Campo Ele Pai Tipo Ocor. Tam. Dec. Descri��o/Observa��o
AP01 enviNFe Rai z - - - -  TAG raiz
AP02 versao A AP01 N 1-1 1-4 2 Vers�o do leiaute
AP03 idLote E AP01 N 1-1 1-15  Identificador de controle do envio do lote.
     N�mero sequencial auto-incremental, de controle correspondente ao
     identificador �nico do lote enviado. A responsabilidade de gerar e
     controlar esse n�mero � exclusiva do contribuinte.
--------------------------------------------------------------------------------
------------------ Novo 3.10 ---------------------------------------------------
AP03a indSinc E AP01 N 1-1 1
      0=N�o
      1=Empresa solicita processamento s�ncrono do Lote de NF-e (sem a gera��o
      de Recibo para consulta futura); Observa��o: O processamento s�ncrono do
      Lote corresponde a entrega da resposta do processamento das NF-e do Lote,
      sem a gera��o de um Recibo de Lote para consulta futura. A resposta de
      forma s�ncrona pela SEFAZ Autorizadora s� ocorrer� se: - a empresa
      solicitar e constar unicamente uma NF-e no Lote; - a SEFAZ Autorizadora
      implementar o processamento s�ncrono para a resposta do Lote de NF-e.
--------------------------------------------------------------------------------
AP04 NFe G AP01 xml 1-50 -  Conjunto de NF-e transmitidas (m�ximo de 50 NF-e),
     seguindo defini��o do Anexo I - Leiaute
==============================================================================*)
  case Sincronia of
    nisAssincrono: indSInc := 0;
    nisSincrono:   indSinc := 1;
    else Geral.MB_Erro('Sincronia n�o definida em "FmNFeGeraXML_0400.GerarLoteNFeNovo()"');
  end;
    begin

  end;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+
  VersaoDados + '">' +
  '<idLote>' + LoteStr + '</idLote>' +
  '<indSinc>' + Geral.FF0(indSinc) + '</indSinc>';

  // ini 2022-02-14
  if TFormaGerenXXe(VAR_DFeAppCode) = fgxxeACBr then
  begin
    DmNFe_0000.ConfigurarComponenteNFe();
    // Limpar componente
    while DmNFe_0000.ACBrNFe1.NotasFiscais.Count > 0 do
      DmNFe_0000.ACBrNFe1.NotasFiscais.Delete(0);
    //
  end;
  // end 2022-02-14

  //repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmNFe_0000.QrNFeLEnC.First;
  while not DmNFe_0000.QrNFeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmNFe_0000.QrNFeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_NFE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      // Nesse ponto vc est� copiando o conteudo da tag "NFe" para o buffer...
      buf := Copy(MeuXMLAssinado, Pos('<NFe', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      //Write(fArquivoTexto, buf);
      mTexto.Free;
    end;

    // ini 2022-02-14
    if TFormaGerenXXe(VAR_DFeAppCode) = fgxxeACBr then
      DmNFe_0000.ACBrNFe1.NotasFiscais.LoadFromFile(XMLArq, False);
    // end 2022-02-14

    DmNFe_0000.QrNFeLEnC.Next;
  end;
  XML_Lote := XML_Lote + '</enviNFe>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;
  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

function TFmNFeGeraXML_0400.GerarXMLdaNFe(FatID, FatNum, Empresa: Integer;
  const nfeID: String; GravaCampos, Cliente: Integer): Boolean;
const
  ForcaSim = True;
var
  K, h, j, nItem, Controle: Integer;
  Valor, IM: String;
  infAdProd, CSRT, QrCode, CNPJCPFAvulso: String;

  EmiteAvulso: Boolean;

  GeraNT2015_03: Boolean;
  //_CSC, CSCpos: String;
begin
  GeraNT2015_03 := UnNFe_PF.GeraGrupoNA_0400(DmNFe_0000.QrNFECabAide_dEmi.Value);
  FHoje := Int(Now());
  //
  Result := False;
  FGravaCampo := GravaCampos;
  DmNFe_0000.ReopenNFeLayI();
  //P�g. 108/232  Manual_Integra��o_Contribuinte_vers�o_4.01-NT2009.006.pdf
   //    A - Dados da Nota Fiscal eletr�nica
   //    '1', 'A01' = Grupo das informa��es da NFe
(* Informa��es da TAG InfNFe... *)
  // Vers�o do leiaute
  if Def('2', 'A02', DmNFe_0000.QrNFECabAversao.Value, Valor) then
    cXML.InfNFe.Versao := Valor;
  //if Def('3', 'A03', nfeID, Valor) then
  cXML.InfNFe.Id := nfeID;
  //if Def('4', 'A04', N�o preencher, Valor) then
  // O contribuinte n�o deve preencher.
  //
  // B - Identifica��o da Nota Fiscal eletr�nica
(* Informa��es da TAG IDE... *)
  //if Def('5', 'B01', GRUPO NFe
  // C�digo da UF do emitente do Documento Fiscal. Utilizar a Tabela do IBGE
  if Def('6', 'B02', DmNFe_0000.QrNFECabAide_cUF.Value, Valor) then
    cXML.InfNFe.Ide.CUF := Valor;
  //C�digo num�rico que comp�e a Chave de Acesso. N�mero aleat�rio gerado pelo emitente para cada NF-e
  if Def('7', 'B03', DmNFe_0000.QrNFECabAide_cNF.Value, Valor) then
    cXML.InfNFe.Ide.CNF := Valor;
  //Descri��o da Natureza da Opera��o
  if Def('8', 'B04', DmNFe_0000.QrNFECabAide_natOp.Value, Valor) then
    cXML.InfNFe.Ide.NatOp := Valor;
  ///
  //////////////////////////////////////////////////////////////////////////////
  // N�o preencher mais! Abolido!!!!
  // 9 B05 indPag Indicador da forma de pagamento
  //////////////////////////////////////////////////////////////////////////////
  ///
  //C�digo do modelo do Documento Fiscal. Utilizar 55 para identifica��o da NF-e, emitida em substitui��o ao modelo 1 e 1A.
  if Def('10', 'B06', DmNFe_0000.QrNFECabAide_mod.Value, Valor) then
    cXML.InfNFe.Ide.Mod_ := Valor;//'55'; CO_MODELO_NFE_55
  //S�rie do Documento Fiscal
  if Def('11', 'B07', DmNFe_0000.QrNFECabAide_serie.Value, Valor) then
    cXML.InfNFe.Ide.Serie := Valor;
  //N�mero do Documento Fiscal
  if Def('12', 'B08', DmNFe_0000.QrNFECabAide_nNF.Value, Valor) then
    cXML.InfNFe.Ide.NNF := Valor;


  // 03.2 Data e Hora de Emiss�o e Outros Hor�rios Alterado o campo de Data de
  // Emiss�o para representar tamb�m a Hora de Emiss�o, no formato UTC.
  // Este tipo de representa��o de dados j� � utilizado atualmente no
  // Web Service de Eventos da NF-e e � tecnicamente adequado para a
  // representa��o do hor�rio para um Pa�s com dimens�es continentais como
  // o Brasil. Todos os demais campos com hor�rio foram migrados para este
  // tipo de dado, inclusive os hor�rios que constam nas mensagens de resposta
  // fornecidas pelas SEFAZ. Nesta nova vers�o do leiaute, ser�o aceitos os
  // hor�rios de qualquer regi�o do mundo (faixa de hor�rio UTC de -11 a +12)
  // e n�o apenas as faixas de hor�rio do Brasil.
  // # ID Campo Descri��o Ele Pai Tipo Ocor. Tam. Observa��o
  // 13 B09 dhEmi Data e Hora de emiss�o do Documento Fiscal E B01 D 1-1
  //        Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal Coordinated Time)
  // 14 B10 dhSaiEnt Data e Hora de Sa�da da Mercadoria/Produto.
  //        No caso da NF de entrada, esta � a Data e Hora de entrada.
  //        E B01 D 0-1  Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal
  //        Coordinated Time)
  // 4a B10a hSaiEnt *** Eliminado ***
  //        29C B28 dhCont Data e Hora da entrada em conting�ncia
  //        E B01 D 0-1  Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal Coordinated Time)
  // Exemplo: no formato UTC para os campos de Data-Hora, "TZD" pode ser -02:00
  // (Fernando de Noronha), -03:00 (Bras�lia) ou -04:00 (Manaus), no hor�rio de
  // ver�o ser�o -01:00, -02:00 e -03:00. Exemplo: "2010-08-19T13:00:15-03:00".

  //Data e hora no formato UTC (Universal Coordinated Time): AAAA-MM-DDThh:mm:ssTZD
  if Def_UTC('13', 'B09',
  DmNFe_0000.QrNFECabAide_dEmi.Value,
  DmNFe_0000.QrNFECabAide_hEmi.Value,
  DmNFe_0000.QrNFECabAide_dhEmiTZD.Value, Valor) then
    cXML.InfNFe.Ide.DhEmi := Valor;

  //Data de sa�da ou de entrada da mercadoria / produto (AAAA-MM-DD)
  (* Opcional... *)
  // N�o informar este campo para a NFC-e.
  if DmNFe_0000.QrNFECabAide_mod.Value <> CO_MODELO_NFE_65 then
  begin
    if Def_UTC('14', 'B10',
    DmNFe_0000.QrNFECabAide_dSaiEnt.Value,
    DmNFe_0000.QrNFECabAide_hSaiEnt.Value,
    DmNFe_0000.QrNFECabAide_dhSaiEntTZD.Value, Valor) then
      cXML.InfNFe.Ide.DhSaiEnt := Valor;
  end;
(* Eliminado na NFe 3.10
  //Hora de sa�da ou de entrada da mercadoria / produto (HH:MM:SS) NFe 2.00
  if D e f ('14a', 'B10a', DmNFe_0000.QrNFECabAide_hSaiEnt.Value, Valor) then
    cXML.InfNFe.Ide.HSaiEnt := Valor;//
*)

  //Tipo do Documento Fiscal (0 - entrada; 1 - sa�da)
  if Def('15', 'B11', DmNFe_0000.QrNFECabAide_tpNF.Value, Valor) then
    if DmNFe_0000.QrNFECabAide_tpNF.Value in ([0,1]) then
      cXML.InfNFe.Ide.TpNF := Valor
    else begin
      Geral.MB_Aviso('Tipo de emiss�o inv�lido: ' + FormatFloat('0', DmNFe_0000.QrNFECabAide_tpNF.Value) +
      sLineBreak  + 'Valor deve ser (0-entrada/1-sa�da)');
      Exit;
    end;

  //Identificador do local de destino da operacao  NFe 3.10
  if Def('15a', 'B11a', DmNFe_0000.QrNFECabAide_idDest.Value, Valor) then
    cXML.InfNFe.Ide.idDest := Valor;

  //C�digo do Munic�pio de Ocorr�ncia do Fato Gerador (utilizar a tabela do IBGE)
  if Def('16', 'B12', DmNFe_0000.QrNFECabAide_cMunFG.Value, Valor) then
    cXML.InfNFe.Ide.CMunFG := Valor;









  if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
  begin
    //Geral.MB_Info('Ver NFeGeraXML_0400.628 tpImp de NFC-e!  tpImp 4=Imprime ou 5=envio somente eletronico');
    //  Fazer aqui!
    if Def('25', 'B21', (*DmNFe_0000.QrNFECabAide_tpImp.Value*)4, Valor) then
      cXML.InfNFe.Ide.TpImp := Valor;
  end else
  begin
    if Def('25', 'B21', DmNFe_0000.QrNFECabAide_tpImp.Value, Valor) then
      cXML.InfNFe.Ide.TpImp := Valor;
  end;

  if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
  begin
   {  Fazer
    Geral.MB_Info('26  B22  tpEmis  Tipo de Emiss�o da NF-e ' + sLineBreak +
    '9=Conting�ncia off-line da NFC-e (as demais op��es de conting�ncia s�o v�lidas tamb�m para a NFC-e). ' + sLineBreak +
    'Para a NFC-e somente est�o dispon�veis e s�o v�lidas as op��es de conting�ncia 5 e 9.');
    }
  end;
  if Def('26', 'B22', DmNFe_0000.QrNFECabAide_tpEmis.Value, Valor) then
    cXML.InfNFe.Ide.TpEmis := Valor;
  if Def('27', 'B23', DmNFe_0000.QrNFECabAide_cDV.Value, Valor) then
    cXML.InfNFe.Ide.CDV := Valor;
  if Def('28', 'B24', DmNFe_0000.QrNFECabAide_tpAmb.Value, Valor) then
    cXML.InfNFe.Ide.TpAmb := Valor;
  if Def('29', 'B25', DmNFe_0000.QrNFECabAide_finNFe.Value, Valor) then
    cXML.InfNFe.Ide.FinNFe := Valor;

  if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
  begin
    if Def('29.1', 'B25a', 1(*consumidor final*), Valor) then
      cXML.InfNFe.Ide.indFinal := Valor;
  end else
  begin
    if Def('29.1', 'B25a', DmNFe_0000.QrNFECabAide_indFinal.Value, Valor) then
      cXML.InfNFe.Ide.indFinal := Valor;
  end;



  if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
  begin
    { Fazer
    Geral.MB_Info('29.2  B25b  indPres  Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o ' + sLineBreak +
    '4=NFC-e em opera��o com entrega a domic�lio;');
    }
  end;
  if Def('29.2', 'B25b', DmNFe_0000.QrNFECabAide_indPres.Value, Valor) then
    cXML.InfNFe.Ide.indPres := Valor;
  // ini 2021-03-08 - Indicador de intermediador/marketplace
  if (DmNFe_0000.QrNFECabAide_indPres.Value in ([1, 2, 3, 4, 9])) then
  begin
    // ForcaSim = True devido ao OcorMin = 0!
    if Def('29.03', 'B25c', DmNFe_0000.QrNFECabAide_indIntermed.Value, Valor, ForcaSim) then
      cXML.InfNFe.Ide.indIntermed := Valor;
  end;
  // fim 2021-03-08
  if Def('29a', 'B26', DmNFe_0000.QrNFECabAide_procEmi.Value, Valor) then
    cXML.InfNFe.Ide.ProcEmi := Valor;
  if Def('29b', 'B27', DmNFe_0000.QrNFECabAide_verProc.Value, Valor) then
    cXML.InfNFe.Ide.VerProc := Valor;
  if Def_UTC('29c', 'B28', DmNFe_0000.QrNFECabAide_dhCont.Value,0,
    DmNFe_0000.QrNFECabAide_dhContTZD.Value, Valor) then
    cXML.InfNFe.Ide.dhCont := Valor;
  if Def('29d', 'B29', DmNFe_0000.QrNFECabAide_xJust.Value, Valor) then
    cXML.InfNFe.Ide.xJust := Valor;










  (* Informa��es da TAG Notas Fiscais Referenciadas... se Houver... *)
(*
Utilizar esta TAG para referenciar
uma Nota Fiscal Eletr�nica emitida
anteriormente, vinculada a NF-e
atual.
Esta informa��o ser� utilizada nas
hip�teses previstas na legisla��o.
(Ex.: Devolu��o de Mercadorias,
Substitui��o de NF cancelada,
Complementa��o de NF, etc.).
*)
  (* Essa TAG � Opcional e s� aparece no XML se Houver NFes a serem referenciadas na Nota...*)
      // '29x.1', 'BA01' = Informa��o das NF/NF-e Referenciadas
  DmNFe_0000.ReopenNFeCabB(FatID, FatNum, Empresa);
  if DmNFe_0000.QrNFeCabB.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFeCabB.Eof do
    begin
      cRefLista := cXML.InfNFe.Ide.NFref.Add;
      //
      case DmNFe_0000.QrNFECabBQualNFref.Value of
        0:
        begin
          if DmNFe_0000.QrNFECabBrefNFeSig.Value = EmptyStr then
          begin
            if Def('29x.2', 'BA02', DmNFe_0000.QrNFECabBrefNFe.Value, Valor) then
              cRefLista.RefNFe := Valor;
          end else
          //if DmNFe_0000.QrNFECabBrefNFeSig.Value <> EmptyStr then
          begin
            if Def('29x.2a', 'BA02a', DmNFe_0000.QrNFECabBrefNFeSig.Value, Valor) then
              cRefLista.RefNFeSig := Valor;
          end;
        end;
        1:
        begin
          //    '29x.3', 'BA03' = informa��o das NF referenciadas (NFs normais > A1 etc)
          if Def('29x.4', 'BA04', DmNFe_0000.QrNFECabBrefNF_cUF.Value, Valor) then
            cRefLista.RefNF.CUF := Valor;
          if Def('29x.5', 'BA05', DmNFe_0000.QrNFECabBrefNF_AAMM.Value, Valor) then
            cRefLista.RefNF.AAMM := Valor;
          if Def('29x.6', 'BA06', DmNFe_0000.QrNFECabBrefNF_CNPJ.Value, Valor) then
            cRefLista.RefNF.CNPJ := Valor;
          if Def('29x.7', 'BA07', DmNFe_0000.QrNFECabBrefNF_mod.Value, Valor) then
            cRefLista.RefNF.Mod_ := Valor;
          if Def('29x.8', 'BA08', DmNFe_0000.QrNFECabBrefNF_serie.Value, Valor) then
            cRefLista.RefNF.Serie := Valor;
          if Def('29x.9', 'BA09', DmNFe_0000.QrNFECabBrefNF_nNF.Value, Valor) then
            cRefLista.RefNF.NNF := Valor;
        end;
        2:
        begin
          // Falta implementar os dados de origem
          // NFe 2.00
          //if Def('29x.10', 'BA10', // Produtor Rural
          if Def('29x.11', 'BA11', DmNFe_0000.QrNFECabBrefNFP_cUF.Value, Valor) then
            cRefLista.RefNFP.cUF := Valor;
          if Def('29x.12', 'BA12', DmNFe_0000.QrNFECabBrefNFP_AAMM.Value, Valor) then
            cRefLista.RefNFP.AAMM := Valor;
          if Geral.SoNumero_TT(DmNFe_0000.QrNFECabBrefNFP_CNPJ.Value) <> '' then
          begin
            if Def('29x.13', 'BA13', DmNFe_0000.QrNFECabBrefNFP_CNPJ.Value, Valor) then
              cRefLista.RefNFP.CNPJ := Valor;
          end else begin
            if Def('29x.14', 'BA14', DmNFe_0000.QrNFECabBrefNFP_CPF.Value, Valor) then
              cRefLista.RefNFP.CPF := Valor;
          end;
          if Def('29x.15', 'BA15', DmNFe_0000.QrNFECabBrefNFP_IE.Value, Valor) then
            cRefLista.RefNFP.IE := Valor;
          if Def('29x.16', 'BA16', DmNFe_0000.QrNFECabBrefNFP_mod.Value, Valor) then
            cRefLista.RefNFP.mod_ := Valor;
          if Def('29x.17', 'BA17', DmNFe_0000.QrNFECabBrefNFP_serie.Value, Valor) then
            cRefLista.RefNFP.serie := Valor;
          if Def('29x.18', 'BA18', DmNFe_0000.QrNFECabBrefNFP_nNF.Value, Valor) then
            cRefLista.RefNFP.nNF := Valor;
          // CTe
          if Def('29x.19', 'BA19', DmNFe_0000.QrNFECabBrefCTe.Value, Valor) then
            cRefLista.RefCTe := Valor;
        end;
        3:
        begin
          //if Def('29x.20', 'BA20', ECF
          if Def('29x.21', 'BA21', DmNFe_0000.QrNFECabBrefECF_mod.Value, Valor) then
            cRefLista.RefECF.mod_ := Valor;
          if Def('29x.22', 'BA22', DmNFe_0000.QrNFECabBrefECF_nECF.Value, Valor) then
            cRefLista.RefECF.nECF := Valor;
          if Def('29x.23', 'BA23', DmNFe_0000.QrNFECabBrefECF_nCOO.Value, Valor) then
            cRefLista.RefECF.nCOO := Valor;
        end;
      end;
      //
      DmNFe_0000.QrNFeCabB.Next;
    end;
  end;









  (* C - Informa��es da TAG EMIT... *)
  // '30', 'C01'  Grupo de identifica��o do emitente da NF-e
  if Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value) <> '' then
  begin
    if Def('31', 'C02', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value), Valor) then
    cXML.InfNFe.Emit.CNPJ := Valor;
  end else begin
    if Def('31a', 'C02a', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CPF.Value), Valor) then
    cXML.InfNFe.Emit.CPF := Valor;
  end;
  if Def('32', 'C03', DmNFe_0000.QrNFECabAemit_xNome.Value, Valor) then
    cXML.InfNFe.Emit.XNome := Valor;
  if Def('33', 'C04', DmNFe_0000.QrNFECabAemit_xFant.Value, Valor) then
    cXML.InfNFe.Emit.XFant := Valor;

  (* TAG EnderEMIT... *)
  //     '34', 'C05'  = Grupo de endere�o do emitente
  if Def('35', 'C06', DmNFe_0000.QrNFECabAemit_xLgr.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XLgr := Valor;
  if Def('36', 'C07', DmNFe_0000.QrNFECabAemit_nro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Nro := Valor;
  if Def('37', 'C08', DmNFe_0000.QrNFECabAemit_xCpl.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XCpl := Valor;
  if Def('38', 'C09', DmNFe_0000.QrNFECabAemit_xBairro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XBairro := Valor;
  if Def('39', 'C10', DmNFe_0000.QrNFECabAemit_cMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.CMun := '9999999'
    else
      cXML.InfNFe.Emit.EnderEmit.CMun := Valor;
  end;
  if Def('40', 'C11', DmNFe_0000.QrNFECabAemit_xMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.XMun := 'EXTERIOR'
    else
      cXML.InfNFe.Emit.EnderEmit.XMun := Valor;
  end;
  if Def('41', 'C12', DmNFe_0000.QrNFECabAemit_UF.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.UF := 'EX'
    else
      cXML.InfNFe.Emit.EnderEmit.UF := Valor;
  end;
  if Def('42', 'C13', DmNFe_0000.QrNFECabAemit_CEP.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CEP := Valor;
  if Def('43', 'C14', DmNFe_0000.QrNFECabAemit_cPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CPais := Valor;
  if Def('44', 'C15', DmNFe_0000.QrNFECabAemit_xPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XPais := Valor;
  if Def('45', 'C16', DmNFe_0000.QrNFECabAemit_fone.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Fone := Valor;
  if Def('46', 'C17', DmNFe_0000.QrNFECabAemit_IE.Value, Valor) then
    cXML.InfNFe.Emit.IE := FMT_IE(Valor);
  if Def('47', 'C18', DmNFe_0000.QrNFECabAemit_IEST.Value, Valor) then
    cXML.InfNFe.Emit.IEST := FMT_IE(Valor);
  IM := DmNFe_0000.QrNFECabAemit_IM.Value;
  if IM = '0' then IM := '';
  if Def('48', 'C19', IM, Valor) then
    cXML.InfNFe.Emit.IM := Valor;
  if Def('49', 'C20', DmNFe_0000.QrNFECabAemit_CNAE.Value, Valor) then
    cXML.InfNFe.Emit.CNAE := Valor;
  if Def('49a', 'C21', DmNFe_0000.QrNFECabAemit_CRT.Value, Valor) then
    cXML.InfNFe.Emit.CRT := Valor;
  //
  //----------------------------------------------------------------------------
  //
  // D Itentifica��o do Fisco emitente da NF-e
  //
  //----------------------------------------------------------------------------
  // N�o preencher (� do fisco)
{
  if Def('51', 'D02', DmNFe_0000.QrNFE?CNPJ.Value, Valor) then
    cXML.InfNFe.?.CNPJ := Valor;
  if Def('52', 'D03', DmNFe_0000.QrNFE?xOrgao.Value, Valor) then
    XML.InfNFe.?.xOrgao := Valor;
  if Def('53', 'D04', DmNFe_0000.QrNFE?matr.Value, Valor) then
    XML.InfNFe.?.matr := Valor;
  if Def('54', 'D05', DmNFe_0000.QrNFE?xAgente.Value, Valor) then
    XML.InfNFe.?.xAgente := Valor;
  if Def('55', 'D06', DmNFe_0000.QrNFE?fone.Value, Valor) then
    XML.InfNFe.?.fone := Valor;
  if Def('56', 'D07', DmNFe_0000.QrNFE?UF.Value, Valor) then
    XML.InfNFe.?.UF := Valor;
  if Def('57', 'D08', DmNFe_0000.QrNFE?nDAR.Value, Valor) then
    XML.InfNFe.?.nDAR := Valor;
  if Def('58', 'D09', DmNFe_0000.QrNFE?dEmi.Value, Valor) then
    XML.InfNFe.?.dEmi := Valor;
  if Def('59', 'D10', DmNFe_0000.QrNFE?vDAR.Value, Valor) then
    XML.InfNFe.?.vDAR := Valor;
  if Def('60', 'D11', DmNFe_0000.QrNFE?repEmi.Value, Valor) then
    XML.InfNFe.?.repEmi := Valor;
  if Def('61', 'D12', DmNFe_0000.QrNFE?dPag.Value, Valor) then
    XML.InfNFe.?.dPag := Valor;
}
  //
  //----------------------------------------------------------------------------
  //
  //  E - Informa��es da TAG DESTINATARIO
  //
  //----------------------------------------------------------------------------
  //
  //       '62', 'E01' - Grupo de identifica��o do destinat�rio da NF-e
  //
  //----------------------------------------------------------------------------
  //
  ///
  if (DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65)
  //and (Cliente = -2 (*Consumidor*)) then
  and (DmNFe_0000.QrNFECabAEmiteAvulso.Value = 1) then
  begin
    //Geral.MB_Info('62  E01  dest...  Identifica��o do Destinat�rio da NF-e ' + sLineBreak +
    //'obrigat�ria para a NF-e (modelo 55) e opcional para a NFC-e.;');
    CNPJCPFAvulso := Geral.SoNumero_TT(DmNFe_0000.QrNFECabACNPJCPFAvulso.Value);
    if (CNPJCPFAvulso <> '') then
    begin
      if Length(CNPJCPFAvulso) > 11 then
      begin
        if Def('63', 'E02', CNPJCPFAvulso, Valor) then
          cXML.InfNFe.Dest.CNPJ := Valor;
      end else
      begin
        if Def('64', 'E03', CNPJCPFAvulso, Valor) then
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;
    //
    if DmNFe_0000.QrNFECabARazaoNomeAvulso.Value <> emptyStr then
      if Def('65', 'E04', DmNFe_0000.QrNFECabARazaoNomeAvulso.Value, Valor) then
        cXML.InfNFe.Dest.XNome := Valor;
  end else
  begin
    if DmNFe_0000.QrNFECabAEstrangDef.Value = 1 then
    begin
      if Def('64a', 'E03a', Geral.SoNumero_TT(
      DmNFe_0000.QrNFECabAdest_idEstrangeiro.Value), Valor) then
         cXML.InfNFe.Dest.idEstrangeiro := Valor
      else
        cXML.InfNFe.Dest.idEstrangeiro := ''
    end else
    begin
      if (DmNFe_0000.QrNFECabAdest_CNPJ.Value <> '') then
      begin
        if Def('63', 'E02', Geral.SoNumero_TT(
        DmNFe_0000.QrNFECabAdest_CNPJ.Value), Valor) then
          cXML.InfNFe.Dest.CNPJ := Valor;
      end else if DmNFe_0000.QrNFECabAdest_CPF.Value <> '' then
      begin
        if Def('64', 'E03', Geral.SoNumero_TT(
        DmNFe_0000.QrNFECabAdest_CPF.Value), Valor) then
          cXML.InfNFe.Dest.CPF := Valor;
      end else
      begin
        //  Erro!
        Geral.MB_Erro(
        'CNPJ ou CPF do remetente/destinat�rio n�o pode ser vazio para venda interna a partir da NFe 3.10!');
        //cXML.InfNFe.Dest.CNPJ := '';
      end;
    end;
    if Def('65', 'E04', DmNFe_0000.QrNFECabAdest_xNome.Value, Valor) then
        cXML.InfNFe.Dest.XNome := Valor;
  end;
  (* TAG EnderDEST... *)
  //     '66', 'E05' - Grupo de endere�o do destinat�rio da NF-e
  if (DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65)
  and (DmNFe_0000.QrNFECabAEmiteAvulso.Value = 1)
  and (
    (DmNFe_0000.QrNFECabACNPJCPFAvulso.Value = '')
    or (DmNFe_0000.QrNFECabAide_indPres.Value <> 4)
  ) then
  begin
    // nada do endere�o de destinat�rio
  end else
  begin
    if Def('67', 'E06', DmNFe_0000.QrNFECabAdest_xLgr.Value, Valor) then
     cXML.InfNFe.Dest.EnderDest.XLgr    :=  Valor;
    if Def('68', 'E07', DmNFe_0000.QrNFECabAdest_nro.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.Nro     :=  Valor     ;
    if Def('69', 'E08', DmNFe_0000.QrNFECabAdest_xCpl.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.XCpl    :=  Valor    ;
    if Def('70', 'E09', DmNFe_0000.QrNFECabAdest_xBairro.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.XBairro :=  Valor ;
    if Def('71', 'E10', DmNFe_0000.QrNFECabAdest_cMun.Value, Valor) then
    begin
      if OutroPais_dest() then
        cXML.InfNFe.Dest.EnderDest.CMun    :=  '9999999'
      else
        cXML.InfNFe.Dest.EnderDest.CMun    :=  Valor    ;
    end;
    if Def('72', 'E11', DmNFe_0000.QrNFECabAdest_xMun.Value, Valor) then
    begin
      if OutroPais_dest() then
        cXML.InfNFe.Dest.EnderDest.XMun    :=  'EXTERIOR'
      else
        cXML.InfNFe.Dest.EnderDest.XMun    :=  Valor    ;
    end;
    if Def('73', 'E12', DmNFe_0000.QrNFECabAdest_UF.Value, Valor) then
    begin
      if OutroPais_dest() then
        cXML.InfNFe.Dest.EnderDest.UF      :=  'EX'
      else
        cXML.InfNFe.Dest.EnderDest.UF      :=  Valor      ;
    end;
    if Def('74', 'E13', DmNFe_0000.QrNFECabAdest_CEP.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.CEP     :=  Valor     ;
    if Def('75', 'E14', DmNFe_0000.QrNFECabAdest_cPais.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.CPais   :=  Valor   ;
    if Def('76', 'E15', DmNFe_0000.QrNFECabAdest_xPais.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.XPais   :=  Valor   ;
    if Def('77', 'E16', DmNFe_0000.QrNFECabAdest_fone.Value, Valor) then
      cXML.InfNFe.Dest.EnderDest.Fone    :=  Valor    ;
  end;
  if (DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65)
  and (DmNFe_0000.QrNFECabAEmiteAvulso.Value = 1)
  and  (DmNFe_0000.QrNFECabACNPJCPFAvulso.Value = '') then
  begin
    // nada
  end else
  begin
    if DmNFe_0000.QrNFECabAide_mod.Value = CO_MODELO_NFE_65 then
    begin
      //Nota 1: No caso de NFC-e informar indIEDest=9 e n�o informar a tag IE do destinat�rio;
      if Def('77a', 'E16a', 9, Valor) then
        cXML.InfNFe.Dest.indIEDest    :=  Valor    ;
      //cXML.InfNFe.Dest.IE := '00'; //Erro no Paran�
    end else
    begin
      if Def('77a', 'E16a', DmNFe_0000.QrNFECabAdest_indIEDest.Value, Valor) then
        cXML.InfNFe.Dest.indIEDest    :=  Valor    ;
      case  DmNFe_0000.QrNFECabAdest_indIEDest.Value of
        1:
        begin
          if Def('78', 'E17', DmNFe_0000.QrNFECabAdest_IE.Value, Valor) then
          begin
            if OutroPais_dest() then
              //cXML.InfNFe.Dest.IE              :=  ''
              Valor :=  '';
            if Valor <> '' then
              cXML.InfNFe.Dest.IE              :=  FMT_IE(Valor)      ;
          end;
        end;
        2, 9:
          ;//cXML.InfNFe.Dest.IE := '00'; //Erro no Paran�
        else
          Geral.MB_Aviso('"indIEDest" n�o implementado!');
      end;
    end;
    if Def('79', 'E18', DmNFe_0000.QrNFECabAdest_ISUF.Value, Valor) then
      cXML.InfNFe.Dest.ISUF := Valor;
    if Def('79.1', 'E18a', DmNFe_0000.QrNFECabAdest_IM.Value, Valor) then
      cXML.InfNFe.Dest.IM := Valor;
    if Def('79a', 'E19', DmNFe_0000.QrNFECabAdest_email.Value, Valor) then
      cXML.InfNFe.Dest.email := Valor;
  end;
(* F - Informa��es da TAG RETIRADA... se Houver *)

(*
  DmNFe_0000.QrNFECabF.Close;
  DmNFe_0000.QrNFECabF.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabF.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabF.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabF. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabF(FatID, FatNum, Empresa);
  //
  (*Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de RETIRADA...*)
  if DmNFe_0000.QrNFECabF.RecordCount > 0 then
  begin
    //     '80', 'F01' = Identifica��o do local de retirada
    if Geral.SoNumero_TT(DmNFe_0000.QrNFECabFretirada_CNPJ.Value) <> '' then
    begin
      if Def('81', 'F02', DmNFe_0000.QrNFECabFretirada_CNPJ.Value, Valor) then
        cXML.InfNFe.Retirada.CNPJ := Valor;
    end else begin
      if Def('81a', 'F02a', DmNFe_0000.QrNFECabFretirada_CPF.Value, Valor) then
        cXML.InfNFe.Retirada.CPF := Valor;
    end;
    if VAR_NT2018_05v120 then
      if Def('81b', 'F02b', DmNFe_0000.QrNFECabFretirada_xNome.Value, Valor) then
        cXML.InfNFe.Retirada.xNome := Valor;
    //
    if Def('82', 'F03', DmNFe_0000.QrNFECabFretirada_xLgr.Value, Valor) then
      cXML.InfNFe.Retirada.XLgr := Valor;
    if Def('83', 'F04', DmNFe_0000.QrNFECabFretirada_Nro.Value, Valor) then
      cXML.InfNFe.Retirada.Nro := Valor;
    if Def('84', 'F05', DmNFe_0000.QrNFECabFretirada_xCpl.Value, Valor) then
      cXML.InfNFe.Retirada.XCpl := Valor;
    if Def('85', 'F06', DmNFe_0000.QrNFECabFretirada_xBairro.Value, Valor) then
      cXML.InfNFe.Retirada.XBairro := Valor;
    if Def('86', 'F07', DmNFe_0000.QrNFECabFretirada_cMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.CMun := '9999999'
      else
        cXML.InfNFe.Retirada.CMun := Valor;
    end;
    if Def('87', 'F08', DmNFe_0000.QrNFECabFretirada_xMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Retirada.XMun := Valor;
    end;
    if Def('88', 'F09', DmNFe_0000.QrNFECabFretirada_UF.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.UF := 'EX'
      else
        cXML.InfNFe.Retirada.UF := Valor;
    end;
    if VAR_NT2018_05v120 then
    begin
      if Def('88a', 'F10', DmNFe_0000.QrNFECabFretirada_CEP.Value, Valor) then
        cXML.InfNFe.Retirada.CEP := Valor;
      if Def('88b', 'F11', DmNFe_0000.QrNFECabFretirada_cPais.Value, Valor) then
        cXML.InfNFe.Retirada.CPais := Valor;
      if Def('88c', 'F12', DmNFe_0000.QrNFECabFretirada_xPais.Value, Valor) then
        cXML.InfNFe.Retirada.XPais := Valor;
      if Def('88d', 'F13', DmNFe_0000.QrNFECabFretirada_fone.Value, Valor) then
        cXML.InfNFe.Retirada.Fone := Valor;
      if Def('88e', 'F14', DmNFe_0000.QrNFECabFretirada_email.Value, Valor) then
        cXML.InfNFe.Retirada.Email := Valor;
      if Def('88f', 'F15', DmNFe_0000.QrNFECabFretirada_IE.Value, Valor) then
        cXML.InfNFe.Retirada.IE := Valor;
    end;
  end;

  //
(* G - Informa��es da TAG ENTREGA... se Houver *)
(*
  DmNFe_0000.QrNFECabG.Close;
  DmNFe_0000.QrNFECabG.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabG.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabG.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabG. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabG(FatID, FatNum, Empresa);
  //
  //
  (*Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de ENTREGA...*)
  if DmNFe_0000.QrNFECabG.RecordCount > 0 then
  begin
    //     '89', 'G01' = Identifica��o do local de entrega
    if DmNFe_0000.QrNFECabGentrega_CNPJ.Value <> '' then
    begin
      if Def('90', 'G02', DmNFe_0000.QrNFECabGentrega_CNPJ.Value, Valor) then
        cXML.InfNFe.Entrega.CNPJ := Valor;
    end else begin
      if Def('90a', 'G02a', DmNFe_0000.QrNFECabGentrega_CPF.Value, Valor) then
        cXML.InfNFe.Entrega.CPF := Valor;
    end;
    if VAR_NT2018_05v120 then
      if Def('90b', 'G02b', DmNFe_0000.QrNFECabGentrega_xNome.Value, Valor) then
        cXML.InfNFe.Entrega.xNome := Valor;
    if Def('91', 'G03', DmNFe_0000.QrNFECabGentrega_xLgr.Value, Valor) then
      cXML.InfNFe.Entrega.XLgr := Valor;
    if Def('92', 'G04', DmNFe_0000.QrNFECabGentrega_Nro.Value, Valor) then
      cXML.InfNFe.Entrega.Nro := Valor;
    if Def('93', 'G05', DmNFe_0000.QrNFECabGentrega_xCpl.Value, Valor) then
      cXML.InfNFe.Entrega.XCpl := Valor;
    if Def('94', 'G06', DmNFe_0000.QrNFECabGentrega_xBairro.Value, Valor) then
      cXML.InfNFe.Entrega.XBairro := Valor;
    if Def('95', 'G07', DmNFe_0000.QrNFECabGentrega_cMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.CMun := '9999999'
      else
        cXML.InfNFe.Entrega.CMun := Valor;
    end;
    if Def('96', 'G08', DmNFe_0000.QrNFECabGentrega_xMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Entrega.XMun := Valor;
    end;
    if Def('97', 'G09', DmNFe_0000.QrNFECabGentrega_UF.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.UF := 'EX'
      else
        cXML.InfNFe.Entrega.UF := Valor;
    end;
    if VAR_NT2018_05v120 then
    begin
      if Def('97a', 'G10', DmNFe_0000.QrNFECabGentrega_CEP.Value, Valor) then
        cXML.InfNFe.Entrega.CEP := Valor;
      if Def('97b', 'G11', DmNFe_0000.QrNFECabGentrega_cPais.Value, Valor) then
        cXML.InfNFe.Entrega.CPais := Valor;
      if Def('97c', 'G12', DmNFe_0000.QrNFECabGentrega_xPais.Value, Valor) then
        cXML.InfNFe.Entrega.XPais := Valor;
      if Def('97d', 'G13', DmNFe_0000.QrNFECabGentrega_fone.Value, Valor) then
        cXML.InfNFe.Entrega.Fone := Valor;
      if Def('97e', 'G14', DmNFe_0000.QrNFECabGentrega_email.Value, Valor) then
        cXML.InfNFe.Entrega.Email := Valor;
      if Def('97f', 'G15', DmNFe_0000.QrNFECabGentrega_IE.Value, Valor) then
        cXML.InfNFe.Entrega.IE := Valor;
    end;
  end;

(* GA - Informa��es da TAG autXML... *)
(*
  DmNFe_0000.QrNFECabGA.Close;
  DmNFe_0000.QrNFECabGA.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabGA.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabGA.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabGA. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabGA(FatID, FatNum, Empresa);
  if DmNFe_0000.QrNFECabGA.RecordCount > 0 then
  begin
    DmNFe_0000.QrNFECabGA.First;
    while not DmNFe_0000.QrNFECabGA.Eof do
    begin
      if (DmNFe_0000.QrNFeCabGAautXML_CNPJ.Value <> '')
      or (DmNFe_0000.QrNFeCabGAautXML_CPF.Value <> '') then
      begin
        cAutXML := cXML.InfNFe.autXML.Add;
        //
        if (DmNFe_0000.QrNFeCabGATipo.Value = 0) then
        begin
          if Def('97a.2', 'GA02', DmNFe_0000.QrNFECabGAautXML_CNPJ.Value, Valor) then
            cAutXML.CNPJ := Valor
          else
            Geral.MB_Aviso('Item ' + Geral.FF0(DmNFe_0000.QrNFECabGA.RecNo) +
            ' da lista de "Autoriza��o para obter o XML" com CNPJ indefinido!');
        end else
        begin
          if (DmNFe_0000.QrNFeCabGAautXML_CPF.Value <> '') then
            if Def('97a.3', 'GA03', DmNFe_0000.QrNFeCabGAautXML_CPF.Value, Valor) then
              cAutXML.CPF := Valor
          else
            Geral.MB_Aviso('Item ' + Geral.FF0(DmNFe_0000.QrNFECabGA.RecNo) +
            ' da lista de "Autoriza��o para obter o XML" com CPF indefinido!');
        end;
      end;
      DmNFe_0000.QrNFECabGA.Next;
    end;
  end;

(* H - Informa��es da TAG DET... *)
(*
  DmNFe_0000.QrNFEItsI.Close;
  DmNFe_0000.QrNFEItsI.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFEItsI.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFEItsI.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFEItsI. O p e n ;
*)
  DmNFe_0000.ReopenNFeItsI(FatID, FatNum, Empresa);
  //
  while not DmNFe_0000.QrNFEItsI.Eof do
  begin
    nItem := DmNFe_0000.QrNFEItsInItem.Value;
    //j := 1;  // M�ximo 990 itens
        // '98', 'H01' = Grupo de detalhamento de produtos e servi�os da NF-e
    cDetLista := cXML.InfNFe.Det.Add;
    if Def('99', 'H02', DmNFe_0000.QrNFEItsInItem.Value, Valor) then
      cDetLista.NItem := Valor;
        // '100', 'I01' = Grupo de detalhamento de produtos e servi�os da NF-e
    if Def('101', 'I02', DmNFe_0000.QrNFEItsIprod_cProd.Value, Valor) then
      cDetLista.Prod.CProd := Valor;
    //
    if Def('102', 'I03', DmNFe_0000.QrNFEItsIprod_cEAN.Value, Valor) then
    begin
      if Valor = '' then
        Valor := 'SEM GTIN';
      //
      cDetLista.Prod.CEAN := Valor;//''; (* Se n�o tiver EAN tem que colocar em Branco...*)
    end;
    //
    // ini 2021-03-11
    if Def('102.01', 'I03a', DmNFe_0000.QrNFEItsIprod_cBarra.Value, Valor) then
      cDetLista.Prod.cBarra := Valor;
    // fim 2021-03-11

    if DmNFe_0000.QrNFECabAide_tpAmb.Value = 2 then
      //if Def('103', 'I04', ''NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL'', Valor) then
        cDetLista.Prod.XProd := 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL'
    else
      if Def('103', 'I04', DmNFe_0000.QrNFEItsIprod_xProd.Value, Valor) then
        cDetLista.Prod.XProd := Valor;
    //
    if Def('104', 'I05', DmNFe_0000.QrNFEItsIprod_NCM.Value, Valor) then
      cDetLista.Prod.NCM := Valor;
    //
    // NFe 3.10
    DmNFe_0000.ReopenNFeItsINVE(FatID, FatNum, Empresa, nItem);
    if DmNFe_0000.QrNFEItsINVE.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFEItsINVE.Eof do
      begin
        Controle := DmNFe_0000.QrNFeItsINVEControle.Value;
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFEItsIDI.RecNo;
        //
        //cXML.InfNFe.Det.Items[h].Prod.NVE.Add;
        if Def('104.01', 'I05a', DmNFe_0000.QrNFeItsINVENVE.Value, Valor) then
          cXML.InfNFe.Det.Items[h].Prod.NVE.Add(Valor);
        //
        DmNFe_0000.QrNFEItsINVE.Next;
      end;
    end;
    // Fim NFe 3.10
    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    // NFe 4.00
    // 104.02 I05b -x- Sequ�ncia XML G I01 0-1 (Inclu�do na NT2016.002)
    if GeraNT2015_03 then
    begin
      if Def('104.03', 'I05c', DmNFe_0000.QrNFEItsIprod_CEST.Value, Valor) then
        cDetLista.Prod.CEST := Valor;
      if Def('104.04', 'I05d', DmNFe_0000.QrNFEItsIprod_indEscala.Value, Valor) then
        cDetLista.Prod.indEscala := Valor;
      if Def('104.05', 'I05e', DmNFe_0000.QrNFEItsIprod_CNPJFab.Value, Valor) then
        cDetLista.Prod.CNPJFab := Valor;
      if Def('104.06', 'I05f', DmNFe_0000.QrNFEItsIprod_cBenef.Value, Valor) then
        cDetLista.Prod.cBenef := Valor;
    end;
    //Fim PL 008h
    if Def('105', 'I06', DmNFe_0000.QrNFEItsIprod_EXTIPI.Value, Valor) then
    begin
      if Valor <> '0' then
        cDetLista.Prod.EXTIPI := Valor;
    end;
    (* foi tirado da NFe 2.00
    if D e f ('106', 'I07', DmNFe_0000.QrNFEItsIprod_genero.Value, Valor) then
      cDetLista.Prod.Genero := Valor;
    *)
    if Def('107', 'I08', DmNFe_0000.QrNFEItsIprod_CFOP.Value, Valor) then
      cDetLista.Prod.CFOP := Valor;
    if Def('108', 'I09', DmNFe_0000.QrNFEItsIprod_uCom.Value, Valor) then
      cDetLista.Prod.UCom := Valor;
    if Def('109', 'I10', DmNFe_0000.QrNFEItsIprod_qCom.Value, Valor) then
      cDetLista.Prod.QCom := Valor;
    if Def('109.01', 'I10a', DmNFe_0000.QrNFEItsIprod_vUnCom.Value, Valor) then
      cDetLista.Prod.VUnCom := Valor;
    if Def('110', 'I11', DmNFe_0000.QrNFEItsIprod_vProd.Value, Valor) then
      cDetLista.Prod.VProd := Valor;
    //
    if Def('111', 'I12', DmNFe_0000.QrNFEItsIprod_cEANTrib.Value, Valor) then
    begin
      if Valor = '' then
        Valor := 'SEM GTIN';
      //
      cDetLista.Prod.CEANTrib := Valor;//''; (* Se n�o tiver EAN Trib. tem que colocar em Branco...*)
    end;
    //
    // ini 2021-03-11
    if Def('111.01', 'I12a', DmNFe_0000.QrNFEItsIprod_cBarraTrib.Value, Valor) then
      cDetLista.Prod.cBarraTrib := Valor;
    // fim 2021-03-11

    if Def('112', 'I13', DmNFe_0000.QrNFEItsIprod_uTrib.Value, Valor) then
      cDetLista.Prod.UTrib := Valor;
    if Def('113', 'I14', DmNFe_0000.QrNFEItsIprod_qTrib.Value, Valor) then
      cDetLista.Prod.QTrib := Valor;
    if Def('113.01', 'I14a', DmNFe_0000.QrNFEItsIprod_vUnTrib.Value, Valor) then
      cDetLista.Prod.VUnTrib := Valor;
    if Def('114', 'I15', DmNFe_0000.QrNFEItsIprod_vFrete.Value, Valor) then
      cDetLista.Prod.VFrete := Valor;
    if Def('115', 'I16', DmNFe_0000.QrNFEItsIprod_vSeg.Value, Valor) then
      cDetLista.Prod.VSeg := Valor;
    if Def('116', 'I17', DmNFe_0000.QrNFEItsIprod_vDesc.Value, Valor) then
      cDetLista.Prod.VDesc := Valor;
    if Def('116a', 'I17a', DmNFe_0000.QrNFEItsIprod_vOutro.Value, Valor) then
      cDetLista.Prod.VOutro := Valor;
    if Def('116b', 'I17b', DmNFe_0000.QrNFEItsIprod_indTot.Value, Valor) then
      cDetLista.Prod.indTot := Valor;

    (*     '117', 'I18' = Informa��es da TAG DI... Opcional *)
(*
    DmNFe_0000.QrNFeItsIDI.Close;
    DmNFe_0000.QrNFeItsIDI.Params[00].AsInteger := DmNFe_0000.QrNFeItsIFatID.Value;
    DmNFe_0000.QrNFeItsIDI.Params[01].AsInteger := DmNFe_0000.QrNFeItsIFatNum.Value;
    DmNFe_0000.QrNFeItsIDI.Params[02].AsInteger := DmNFe_0000.QrNFeItsIEmpresa.Value;
    DmNFe_0000.QrNFeItsIDI.Params[03].AsInteger := DmNFe_0000.QrNFeItsInItem.Value;
    DmNFe_0000.QrNFeItsIDI. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsIDI(FatID, FatNum, Empresa, nItem);
    //
    if DmNFe_0000.QrNFEItsIDI.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFEItsIDI.Eof do
      begin
        Controle := DmNFe_0000.QrNFeItsIDIControle.Value;
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFEItsIDI.RecNo;
        //
        cDILista := cXML.InfNFe.Det.Items[h].Prod.DI.Add;
        if Def('118', 'I19', DmNFe_0000.QrNFEItsIDIDI_nDI.Value, Valor) then
          cDILista.NDI := Valor;
        if Def('119', 'I20', DmNFe_0000.QrNFEItsIDIDI_dDI.Value, Valor) then
          cDILista.DDI := Valor;
        if Def('120', 'I21', DmNFe_0000.QrNfeItsIDIDI_xLocDesemb.Value, Valor) then
          cDILista.XLocDesemb := Valor;
        if Def('121', 'I22', DmNFe_0000.QrNfeItsIDIDI_UFDesemb.Value, Valor) then
          cDILista.UFDesemb := Valor;
        if Def('122', 'I23', DmNFe_0000.QrNfeItsIDIDI_dDesemb.Value, Valor) then
          cDILista.DDesemb := Valor;
        if Def('122.01', 'I23a', DmNFe_0000.QrNfeItsIDIDI_tpViaTransp.Value, Valor) then
          cDILista.tpViaTransp := Valor;
        if Def('122.02', 'I23b', DmNFe_0000.QrNfeItsIDIDI_vAFRMM.Value, Valor) then
          cDILista.vAFRMM := Valor;
        if Def('122.03', 'I23c', DmNFe_0000.QrNfeItsIDIDI_tpIntermedio.Value, Valor) then
          cDILista.tpIntermedio := Valor;
        if Def('122.04', 'I23d', DmNFe_0000.QrNfeItsIDIDI_CNPJ.Value, Valor) then
          cDILista.CNPJ := Valor;
        if Def('122.05', 'I23e', DmNFe_0000.QrNfeItsIDIDI_UFTerceiro.Value, Valor) then
          cDILista.UFTerceiro := Valor;
        if Def('123', 'I24', DmNFe_0000.QrNfeItsIDIDI_cExportador.Value, Valor) then
          cDILista.CExportador := Valor;

        /////////////////////
        ///
       (* Informa��es da TAG ADI... Opcional *)
{
        DmNFe_0000.QrNFeItsIDIa.Close;
        DmNFe_0000.QrNFeItsIDIa.Params[00].AsInteger := DmNFe_0000.QrNFeItsIDIFatID.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[01].AsInteger := DmNFe_0000.QrNFeItsIDIFatNum.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[02].AsInteger := DmNFe_0000.QrNFeItsIDIEmpresa.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[03].AsInteger := DmNFe_0000.QrNFeItsIDInItem.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[04].AsInteger := DmNFe_0000.QrNFeItsIDIControle.Value;
        DmNFe_0000.QrNFeItsIDIa. O p e n ;
}
        DmNFe_0000.ReopenNFeItsIDIa(FatID, FatNum, Empresa, nItem, Controle);
        //
        if DmNFe_0000.QrNFEItsIDIa.RecordCount > 0 then
        begin
          j := DmNFe_0000.QrNFEItsIDI.RecNo - 1;//DmNFe_0000.QrNFeItsIDIa.RecNo;
          //
          cAdiLista := cXML.InfNFe.Det.Items[h].Prod.DI.Items[j].Adi.Add;
          if Def('125', 'I26', DmNFe_0000.QrNfeItsIDIaAdi_nAdicao.Value, Valor) then
            cAdiLista.NAdicao := Valor;
          if Def('126', 'I27', DmNFe_0000.QrNfeItsIDIaAdi_nSeqAdic.Value, Valor) then
            cAdiLista.NSeqAdic := Valor;
          if Def('127', 'I28', DmNFe_0000.QrNfeItsIDIaAdi_cFabricante.Value, Valor) then
            cAdiLista.CFabricante := Valor;
          if Def('128', 'I29', DmNFe_0000.QrNfeItsIDIaAdi_vDescDI.Value, Valor) then
            cAdiLista.VDescDI := Valor;
          if Def('128.01', 'I29a', DmNFe_0000.QrNfeItsIDIaAdi_nDraw.Value, Valor) then
            cAdiLista.nDraw := Valor;
        end else Geral.MB_Aviso('Declara��o de importa��o sem Adi��es!');
        ///
        /////////////////////
        DmNFe_0000.QrNFEItsIDI.Next;
      end;

    end;

//------------------------------------------------------------------------------
(* I03 - Informa��es da TAG detExport... *)
//------------------------------------------------------------------------------
(*
    DmNFe_0000.QrNFeItsI03.Close;
    DmNFe_0000.QrNFeItsI03.Params[00].AsInteger := DmNFe_0000.QrNFeItsIFatID.Value;
    DmNFe_0000.QrNFeItsI03.Params[01].AsInteger := DmNFe_0000.QrNFeItsIFatNum.Value;
    DmNFe_0000.QrNFeItsI03.Params[02].AsInteger := DmNFe_0000.QrNFeItsIEmpresa.Value;
    DmNFe_0000.QrNFeItsI03.Params[03].AsInteger := DmNFe_0000.QrNFeItsInItem.Value;
    DmNFe_0000.QrNFeItsI03. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsI03(FatID, FatNum, Empresa, nItem);

    //
    // '128.20', 'I50' detExport
    //Grupo de informa��es de exporta��o para o item G I01 0-500
    //Informar apenas no Drawback e nas exporta��es
    if DmNFe_0000.QrNFeItsI03.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFeItsI03.Eof do
      begin
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFeItsI03.RecNo;
        //
        cDetExport := cXML.InfNFe.Det.Items[h].Prod.detExport.Add;
        //
        if Def('128.21', 'I51', DmNFe_0000.QrNFeItsI03detExport_nDraw.Value, Valor) then
          cDetExport.nDraw := Valor;
        //  '128.22', 'I52'  Grupo exportInd
        if (DmNFe_0000.QrNFeItsI03detExport_nRE.Value <> 0)
        or (DmNFe_0000.QrNFeItsI03detExport_chNFe.Value <> '')
        or (DmNFe_0000.QrNFeItsI03detExport_qExport.Value <> 0) then
        begin
          if Def('128.23', 'I53', DmNFe_0000.QrNFeItsI03detExport_nRE.Value, Valor) then
            cDetExport.exportInd.nRE := Valor;
          if Def('128.24', 'I54', DmNFe_0000.QrNFeItsI03detExport_chNFe.Value, Valor) then
            cDetExport.exportInd.chNFe := Valor;
          if Def('128.25', 'I55', DmNFe_0000.QrNFeItsI03detExport_qExport.Value, Valor) then
            cDetExport.exportInd.qExport := Valor;
        end;
        //
        DmNFe_0000.QrNFeItsI03.Next;
      end;
    end;
//------------------------------------------------------------------------------
//  Grupo I05. Produtos e Servi�os / Pedido de Compra
//------------------------------------------------------------------------------
    (*
    if D e f ('128a', 'I30', DmNFe_0000.QrNfeItsIprod_xPed.Value, Valor) then
      cDetLista.xPed := Valor;
    if D e f ('128b', 'I31', DmNFe_0000.QrNfeItsIprod_nItemPed.Value, Valor) then
    *)
    // I05. Produtos e servicos / Pedido de compra
    // Informacao de interesse do emissor para controle de B2B
    if Def('128.40', 'I60', DmNFe_0000.QrNfeItsIprod_xPed.Value, Valor) then
      cDetLista.prod.xPed := Valor;
    if Trim(DmNFe_0000.QrNfeItsIprod_xPed.Value) <> '' then
    begin
      if Def('128.41', 'I61', DmNFe_0000.QrNfeItsIprod_nItemPed.Value, Valor) then
        cDetLista.prod.nItemPed := Valor;
    end;
//------------------------------------------------------------------------------
    // I07. Produtos e servicos / Grupo Diversos
//------------------------------------------------------------------------------
    // Numero de controle da FCI - Ficha de Conteudo de  Importacao
    if Def('128.60', 'I70', DmNFe_0000.QrNfeItsIprod_nFCI.Value, Valor) then
      cDetLista.prod.nFCI := Valor;

////////////////////////////////////////////////////////////////////////////////
///  Grupo I80. Rastreabilidade de produto
///  Grupo criado para permitir a rastreabilidade de qualquer produto sujeito a
///  regula��es sanit�rias, casos de recolhimento/recall, al�m de defensivos
///  agr�colas, produtos veterin�rios, odontol�gicos, medicamentos, bebidas,
///  �guas envasadas, embalagens, etc., a partir da indica��o de informa��es de
///  n�mero de lote, data de fabrica��o/produ��o, data de validade, etc.
///  Obrigat�rio o preenchimento deste grupo no caso de medicamentos e produtos
///  farmac�uticos.
//------------------------------------------------------------------------------
//# ID       Campo  Descri��o              Ele Pai Tipo Ocor. Tam. Observa��o
//------------------------------------------------------------------------------
//128.70|I80|rastro|Detalhamento de produto sujei|G|I01|0-500|Informar apenas quando se tratar de produto a ser rastreado posteriormente(Grupo criado na NT/2016/002)
//      |   |      |to a rastreabilidade         |  |  |     |
//------------------------------------------------------------------------------
//128.71|I81|nLote |N�mero do Lote do produto    |E|I80|C|1-1|1- 20|
//128.72|I82|qLote |Quantidade de produto no Lote|E|I80|N|1-1|8v3
//128.73|I83|dFab  |Data de fabrica��o/ Produ��o |E|I80|D|1-1|Formato: �AAAA-MM-DD�
//128.74|I84|dVal  |Data de validade             |E|I80|D|1-1|Formato: �AAAA-MM-DD� Informar o �ltimo dia do m�s caso a validade n�o especifique o dia.
//128.75|I85|cAgreg|C�digo de Agrega��o          |E|I80|N|0-1|1-20
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
(*
  if Def('128.71', 'I81', DmNFe_0000.QrNFE?nLote.Value, Valor) then
    cXML.InfNFe.?.nLote := Valor;
  if Def('128.72', 'I82', DmNFe_0000.QrNFE?qLote.Value, Valor) then
    XML.InfNFe.?.qLote := Valor;
  if Def('128.73', 'I83', DmNFe_0000.QrNFE?dFab.Value, Valor) then
    XML.InfNFe.?.dFab := Valor;
  if Def('128.74', 'I84', DmNFe_0000.QrNFE?dVal.Value, Valor) then
    XML.InfNFe.?.dVal := Valor;
  if Def('128.75', 'I85', DmNFe_0000.QrNFE?cAgreg.Value, Valor) then
    XML.InfNFe.?.cAgreg := Valor;
*)


    (* J - Informa��es da TAG VEICPROD... Opcional *)
//------------------------------------------------------------------------------
//Grupo JA. Detalhamento Espec�fico de Ve�culos novos
//------------------------------------------------------------------------------
    (*
    cDetLista.Prod.VeicProd.TpOp := Valor; // Zero � Aceitav�l como valor...
    cDetLista.Prod.VeicProd.Chassi := Valor;
    cDetLista.Prod.VeicProd.CCor := Valor;
    cDetLista.Prod.VeicProd.XCor := Valor;
    cDetLista.Prod.VeicProd.Pot := Valor;
    cDetLista.Prod.VeicProd.CM3 := Valor;
    cDetLista.Prod.VeicProd.PesoL := Valor;
    cDetLista.Prod.VeicProd.PesoB := Valor;
    cDetLista.Prod.VeicProd.NSerie := Valor;
    cDetLista.Prod.VeicProd.TpComb := Valor;
    cDetLista.Prod.VeicProd.NMotor := Valor;
    cDetLista.Prod.VeicProd.CMKG := Valor;
    cDetLista.Prod.VeicProd.Dist := Valor;
    cDetLista.Prod.VeicProd.RENAVAM := Valor;
    cDetLista.Prod.VeicProd.AnoMod := Valor;
    cDetLista.Prod.VeicProd.AnoFab := Valor;
    cDetLista.Prod.VeicProd.TpPint := Valor;
    cDetLista.Prod.VeicProd.TpVeic := Valor;
    cDetLista.Prod.VeicProd.EspVeic := Valor;
    cDetLista.Prod.VeicProd.VIN := Valor;
    cDetLista.Prod.VeicProd.CondVeic := Valor;
    cDetLista.Prod.VeicProd.CMod := Valor;
    *)
(*
  if Def('130', 'J02', DmNFe_0000.QrNFE?tpOp.Value, Valor) then
cXML.InfNFe.?.tpOp := Valor;
  if Def('131', 'J03', DmNFe_0000.QrNFE?chassi.Value, Valor) then
cXML.InfNFe.?.chassi := Valor;
  if Def('132', 'J04', DmNFe_0000.QrNFE?cCor.Value, Valor) then
cXML.InfNFe.?.cCor := Valor;
  if Def('133', 'J05', DmNFe_0000.QrNFE?xCor.Value, Valor) then
cXML.InfNFe.?.xCor := Valor;
  if Def('134', 'J06', DmNFe_0000.QrNFE?pot.Value, Valor) then
cXML.InfNFe.?.pot := Valor;
  if Def('135', 'J07', DmNFe_0000.QrNFE?cilin.Value, Valor) then
cXML.InfNFe.?.cilin := Valor;
  if Def('136', 'J08', DmNFe_0000.QrNFE?pesoL.Value, Valor) then
cXML.InfNFe.?.pesoL := Valor;
  if Def('137', 'J09', DmNFe_0000.QrNFE?pesoB.Value, Valor) then
cXML.InfNFe.?.pesoB := Valor;
  if Def('138', 'J10', DmNFe_0000.QrNFE?nSerie.Value, Valor) then
cXML.InfNFe.?.nSerie := Valor;
  if Def('139', 'J11', DmNFe_0000.QrNFE?tpComb.Value, Valor) then
cXML.InfNFe.?.tpComb := Valor;
  if Def('140', 'J12', DmNFe_0000.QrNFE?nMotor.Value, Valor) then
cXML.InfNFe.?.nMotor := Valor;
  if Def('141', 'J13', DmNFe_0000.QrNFE?CMT.Value, Valor) then
cXML.InfNFe.?.CMT := Valor;
  if Def('142', 'J14', DmNFe_0000.QrNFE?dist.Value, Valor) then
cXML.InfNFe.?.dist := Valor;
  if Def('144', 'J16', DmNFe_0000.QrNFE?anoMod.Value, Valor) then
cXML.InfNFe.?.anoMod := Valor;
  if Def('145', 'J17', DmNFe_0000.QrNFE?anoFab.Value, Valor) then
cXML.InfNFe.?.anoFab := Valor;
  if Def('146', 'J18', DmNFe_0000.QrNFE?tpPint.Value, Valor) then
cXML.InfNFe.?.tpPint := Valor;
  if Def('147', 'J19', DmNFe_0000.QrNFE?tpVeic.Value, Valor) then
cXML.InfNFe.?.tpVeic := Valor;
  if Def('148', 'J20', DmNFe_0000.QrNFE?espVeic.Value, Valor) then
cXML.InfNFe.?.espVeic := Valor;
  if Def('149', 'J21', DmNFe_0000.QrNFE?VIN.Value, Valor) then
cXML.InfNFe.?.VIN := Valor;
  if Def('150', 'J22', DmNFe_0000.QrNFE?condVeic.Value, Valor) then
cXML.InfNFe.?.condVeic := Valor;
  if Def('151', 'J23', DmNFe_0000.QrNFE?cMod.Value, Valor) then
cXML.InfNFe.?.cMod := Valor;
  if Def('151a', 'J24', DmNFe_0000.QrNFE?cCorDENATRAN.Value, Valor) then
cXML.InfNFe.?.cCorDENATRAN := Valor;
  if Def('151b', 'J25', DmNFe_0000.QrNFE?lota.Value, Valor) then
cXML.InfNFe.?.lota := Valor;
  if Def('151c', 'J26', DmNFe_0000.QrNFE?tpRest.Value, Valor) then
cXML.InfNFe.?.tpRest := Valor;
*)
//------------------------------------------------------------------------------
    (* K - Informa��es da TAG MED... Opcional *)
//------------------------------------------------------------------------------
    (*
    cMedLista := cXML.InfNFe.Det.Items[J - 1].Prod.Med.Add;
    cMedLista.NLote := Valor;
    cMedLista.QLote := DmNFe_0000.DecimalPonto(FormatFloat('##0.000', Valor));;
    cMedLista.DFab := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.DVal := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.VPMC := DmNFe_0000.DecimalPonto(FormatFloat('##0.00', Valor));
    *)
(*
  if Def('152a', 'K01a', DmNFe_0000.QrNFE?cProdANVISA.Value, Valor) then
cXML.InfNFe.?.cProdANVISA := Valor;
  if Def('152b', 'K01b', DmNFe_0000.QrNFE?xMotivoIsencao.Value, Valor) then
cXML.InfNFe.?.xMotivoIsencao := Valor;
  if Def('157', 'K06', DmNFe_0000.QrNFE?vPMC.Value, Valor) then
cXML.InfNFe.?.vPMC := Valor;
*)
//------------------------------------------------------------------------------
    (* L - Informa��o da TAG ARMA... Opcional *)
//------------------------------------------------------------------------------
    (*
    cArmLista := cXML.InfNFe.Det.Items[j].Prod.Arma.Add;
    cArmLista.TpArma := Valor; // Zero � Aceitav�l como valor...
    cArmLista.NSerie := Valor;
    cArmLista.NCano := Valor;
    cArmLista.Descr := Valor;
    *)
(*
  if Def('159', 'L02', DmNFe_0000.QrNFE?tpArma.Value, Valor) then
cXML.InfNFe.?.tpArma := Valor;
  if Def('160', 'L03', DmNFe_0000.QrNFE?nSerie.Value, Valor) then
cXML.InfNFe.?.nSerie := Valor;
  if Def('161', 'L04', DmNFe_0000.QrNFE?nCano.Value, Valor) then
cXML.InfNFe.?.nCano := Valor;
  if Def('162', 'L05', DmNFe_0000.QrNFE?descr.Value, Valor) then
cXML.InfNFe.?.descr := Valor;
*)
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
    (* LA - Informa��es da TAG COMB... Opcional *)
//------------------------------------------------------------------------------
    DmNFe_0000.ReopenNFeItsLA(FatID, FatNum, Empresa, nItem);
    //
    if DmNFe_0000.QrNFeItsLA.RecordCount > 0 then
    begin
      if Def('162b', 'LA02', DmNFe_0000.QrNFeItsLADetComb_cProdANP.Value, Valor) then
        cDetLista.Prod.Comb.CProdANP := Valor;
      if Def('162b1', 'LA03', DmNFe_0000.QrNFeItsLADetComb_descANP.Value, Valor) then
        cDetLista.Prod.Comb.DescANP := Valor;
      if Def('162b2', 'LA03a', DmNFe_0000.QrNFeItsLADetComb_pGLP.Value, Valor) then
        cDetLista.Prod.Comb.PGLP := Valor;
      if Def('162b3', 'LA03b', DmNFe_0000.QrNFeItsLADetComb_pGNn.Value, Valor) then
        cDetLista.Prod.Comb.PGNn := Valor;
      if Def('162b4', 'LA03c', DmNFe_0000.QrNFeItsLADetComb_pGNi.Value, Valor) then
        cDetLista.Prod.Comb.PGNi := Valor;
      if Def('162b5', 'LA03d', DmNFe_0000.QrNFeItsLADetComb_vPart.Value, Valor) then
        cDetLista.Prod.Comb.VPart := Valor;
      if Def('162c', 'LA04', DmNFe_0000.QrNFeItsLADetComb_CODIF.Value, Valor) then
        cDetLista.Prod.Comb.CODIF := Valor;
      if Def('162d', 'LA05', DmNFe_0000.QrNFeItsLADetComb_qTemp.Value, Valor) then
        cDetLista.Prod.Comb.QTemp := Valor;
      if Def('162e', 'LA06', DmNFe_0000.QrNFeItsLADetComb_UFCons.Value, Valor) then
        cDetLista.Prod.Comb.UFCons := Valor;

      //if Def('162f', 'LA07',
      if DmNFe_0000.QrNFeItsLADetComb_CIDE.Value = 1 then
      begin
        if Def('162g', 'LA08', DmNFe_0000.QrNFeItsLADetComb_CIDE_qBCProd.Value, Valor) then
          cDetLista.Prod.Comb.CIDE.QBCProd := Valor;
        if Def('162h', 'LA09', DmNFe_0000.QrNFeItsLADetComb_CIDE_vAliqProd.Value, Valor) then
          cDetLista.Prod.Comb.CIDE.VAliqProd := Valor;
        if Def('162i', 'LA10', DmNFe_0000.QrNFeItsLADetComb_CIDE_vCIDE.Value, Valor) then
          cDetLista.Prod.Comb.CIDE.VCIDE := Valor;
      end;
      //if Def('162j', 'LA11',
      if DmNFe_0000.QrNFeItsLADetComb_Encerrante.Value = 1 then
      begin
        if Def('162k', 'LA12', DmNFe_0000.QrNFeItsLADetComb_encerrante_nBico.Value, Valor) then
          cDetLista.Prod.Comb.Encerrante.NBico := Valor;
        if Def('162l', 'LA13', DmNFe_0000.QrNFeItsLADetComb_encerrante_nBomba.Value, Valor) then
          cDetLista.Prod.Comb.Encerrante.NBomba := Valor;
        if Def('162m', 'LA14', DmNFe_0000.QrNFeItsLADetComb_encerrante_nTanque.Value, Valor) then
          cDetLista.Prod.Comb.Encerrante.NTanque := Valor;
        if Def('162n', 'LA15', DmNFe_0000.QrNFeItsLADetComb_encerrante_vEncIni.Value, Valor) then
          cDetLista.Prod.Comb.Encerrante.VEncIni := Valor;
        if Def('162o', 'LA16', DmNFe_0000.QrNFeItsLADetComb_encerrante_vEncFin.Value, Valor) then
          cDetLista.Prod.Comb.Encerrante.VEncFin := Valor;
      end;
    end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  Grupo LB. Detalhamento Espec�fico para Opera��o com Papel Imune
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
(*
  if Def('162j', 'LB01', DmNFe_0000.QrNFE?nRECOPI.Value, Valor) then
cXML.InfNFe.?.nRECOPI := Valor;
*)
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

    // M - Tributos incidentes no produto ou servi�o
    // '163', 'M01'  = Grupo de tributos incidentes no produto ou servi�o

  // FIM 2013-05-07
    // '163a', 'M012'  = {vTotTrib) Valor aproximado total de tributos federais, estaduais e municipais
    //if DmNFe_0000.QrFilialNFeNT2013_003LTT.Value > 0 then
    begin
      DmNFe_0000.QrNFEItsM.Close;
      DmNFe_0000.QrNFEItsM.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsM.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsM.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsM.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrNFEItsM, Dmod.MyDB);
      //
      //if DmNFe_0000.QrNFEItsMiTotTrib.Value > 0 then
        if Def('163a', 'M02', DmNFe_0000.QrNFEItsMvTotTrib.Value, Valor) then
          cDetLista.Imposto.vTotTrib  := Valor;
    end;
  // FIM 2013-05-07


  (*  Ser� necess�rio criar regras de neg�cio  de acordo com o ERP na qual definir� a ocorr�ncia das TAGs de IMPOSTO...*)

    // N - ICMS Normal e ST
    // '164', 'N01'  = Grupo do ICMS na opera��o pr�pria e ST
    DmNFe_0000.ReopenNFEItsN(FatID, FatNum, Empresa, nItem);
    //
    (*
    Informar apenas um dos grupos N02, N03, N04, N05, N06, N07, N08, N09, N10,
    N10a, N10b ou N10c com base no conte�do informado na TAG Tributa��o do ICMS.
    (v2.0)
    *)
    if (DmNFe_0000.QrNFECabAemit_CRT.Value <> 1) and
    // 2011-09-18
    (DmNFe_0000.QrNFEItsNICMS_CSOSN.Value = 0) then
    begin
      case DmNFe_0000.QrNFEItsNICMS_CST.Value of
        0:
        begin
        (* TAG ICMS.ICMS00... *)
              // '165', 'N02'  = Grupo do CST = 00
          if Def('166', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.Orig  := Valor;
          if Def('167', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.CST   := Valor;
          if Def('168', 'N13', DmNFe_0000.QrNFEItsNICMS_ModBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.ModBC := Valor;
          if Def('169', 'N15', DmNFe_0000.QrNFEItsNICMS_VBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.VBC   := Valor;
          if Def('170', 'N16', DmNFe_0000.QrNFEItsNICMS_PICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.PICMS := Valor;
          if Def('171', 'N17', DmNFe_0000.QrNFEItsNICMS_VICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.VICMS := Valor;
          //
          // 171.01 N17.1 -x- Sequ�ncia XML G N02 0-1 (Criada na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value <> 0 then
          begin
            if Def('171.02', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS00.pFCP := Valor;
            if Def('171.03', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS00.vFCP := Valor;
          end;
        end;
        10:
        begin
          (* TAG ICMS.ICMS10... *)
              // '172', 'N03'  = Grupo do CST = 10
          if Def('173', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.Orig      := Valor;
          if Def('174', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.CST       := Valor;
          if Def('175', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.ModBC     := Valor;
          if Def('176', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VBC       := Valor;
          if Def('177', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PICMS     := Valor;
          if Def('178', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VICMS     := Valor;
          //
          // 178.01 N17.0 -x- Sequ�ncia XML G N03 0-1
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value >= 0.01 then
          begin
            if Def('178.02', 'N17a', DmNFe_0000.QrNFEItsNICMS_vBCFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.vBCFCP    := Valor;
            if Def('178.03', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.pFCP      := Valor;
            if Def('178.04', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.vFCP      := Valor;
          end;
          if Def('179', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.ModBCST   := Valor;
          if Def('180', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PMVAST    := Valor;
          if Def('181', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PRedBCST  := Valor;
          if Def('182', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VBCST     := Valor;
          if Def('183', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PICMSST   := Valor;
          if Def('184', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VICMSST   := Valor;
          //
          // 184.0 N23.1 -x- Sequ�ncia XML G N03 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value >= 0.01 then
          begin
            if Def('184.1', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.vBCFCPST  := Valor;
            if Def('184.2', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.pFCPST    := Valor;
            if Def('184.4', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.vFCPST    := Valor;
          end;
          //
          // 184.10 N33.1 -x- Sequ�ncia XML G N03
          if DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value <> 0 then
          begin
            if Def('184.11', 'N33a', DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.VICMSSTDeson := Valor;
            if Def('184.12', 'N33b', DmNFe_0000.QrNFEItsNICMS_motDesICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.motDesICMSST := Valor;
          end;
          //
        end;
        20:
        begin
          (* TAG ICMS.ICMS20... *)
              // '185', 'N04'  = Grupo do CST = 20
          if Def('186', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.Orig      := Valor;
          if Def('187', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.CST       := Valor;
          if Def('188', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.ModBC     := Valor;
          if Def('189', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.PRedBC    := Valor;
          if Def('190', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VBC       := Valor;
          if Def('191', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.PICMS     := Valor;
          if Def('192', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VICMS     := Valor;
          //
          // 192.0 N17.1 -x- Sequ�ncia XML G N04 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value <> 0 then
          begin
            if Def('192.w', 'N17a', DmNFe_0000.QrNFEItsNICMS_vBCFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.vBCFCP    := Valor;
            if Def('192.x', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.pFCP      := Valor;
            if Def('192.y', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.vFCP      := Valor;
          end;
          //Mudado em 03/09/2015 => In�cio
          //
          // 192.1 N27.1 -x- Sequ�ncia XML G N04 0-1 Grupo opcional.
          //
          if DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value <> 0 then
          begin
            if Def('192.2', 'N28a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.VICMSDeson     := Valor;
            if Def('192.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.motDesICMS     := Valor;
          end;
          (*
          if D e f ('192.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VICMSDeson     := Valor;
          if D e f ('192.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.motDesICMS     := Valor;
          *)
          //Mudado em 03/09/2015 => Fim
        end;
        30:
        begin
          (* TAG ICMS.ICMS30... *)
              // '193', 'N05'  = Grupo do CST = 30
          if Def('194', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.Orig      := Valor;
          if Def('195', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.CST       := Valor;
          if Def('196', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.ModBCST   := Valor;
          if Def('197', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PMVAST    := Valor;
          if Def('198', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PRedBCST  := Valor;
          if Def('199', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VBCST     := Valor;
          if Def('200', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PICMSST   := Valor;
          if Def('201', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VICMSST   := Valor;
          //
          // 201.0 N23.1 -x- Sequ�ncia XML G N05 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
            if Def('201.w', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.vBCFCPST  := Valor;
            if Def('201.x', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.pFCPST    := Valor;
            if Def('201.y', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.vFCPST    := Valor;
          end;
          //
          // 201.1 N27.1 -x- Sequ�ncia XML G N05 0-1 Grupo opcional.
          //
          if Def('201.2', 'N28a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VICMSDeson     := Valor;
          if Def('201.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.motDesICMS     := Valor;
        end;
        // modificado 2010-07-07 faltava CST 41 e 50!
        40,41,50:
        begin
          { Mudado em 12/05/2015 - Ini
          (* TAG ICMS.ICMS40... *)
              // '202', 'N06'  = Grupo do CST = 40, 41, 50
          if D e f ('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
          if D e f ('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
          //if D e f ('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          //  cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
          if D e f ('204.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.VICMSDeson     := Valor;
          if D e f ('204.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.motDesICMS     := Valor;
          }
          (* TAG ICMS.ICMS40... *)
              // '202', 'N06'  = Grupo do CST = 40, 41, 50
          if Def('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
          if Def('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
          //if D e f ('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          //  cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
          //
          // 204.00 N27.1 -x- Sequ�ncia XML G N06 0-1 Grupo opcional.
          //
          if DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value <> 0 then
          begin
            if Def('204.01', 'N28a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.VICMSDeson := Valor;
            if Def('204.02', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.motDesICMS := Valor;
          end;
          //Mudado em 12/05/2015 - Fim
        end;
        51:
        begin
          (* TAG ICMS.ICMS51... *)
              // '205', 'N07'  = Grupo do CST = 51
          if Def('206', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.Orig      := Valor;
          if Def('207', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.CST       := Valor;
          if Def('208', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.ModBC     := Valor;
          if Def('209', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.PRedBC    := Valor;
          if Def('210', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.VBC       := Valor;
          if Def('211', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.PICMS     := Valor;
          if Def('211.01', 'N16a', DmNFe_0000.QrNFEItsNICMS_vICMSOp.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.vICMSOp     := Valor;
          if Def('211.02', 'N16b', DmNFe_0000.QrNFEItsNICMS_pDif.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.pDif     := Valor;
          if Def('211.03', 'N16c', DmNFe_0000.QrNFEItsNICMS_vICMSDif.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.vICMSDif     := Valor;
          if Def('212', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.VICMS     := Valor;
          //
          // 212.01 N17.1 -x- Sequ�ncia XML G N07 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value <> 0 then
          begin
            if Def('212.02', 'N17a', DmNFe_0000.QrNFEItsNICMS_vBCFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.vBCFCP    := Valor;
            if Def('212.03', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.pFCP      := Valor;
            if Def('212.04', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.vFCP      := Valor;
          end;
          //212.05 N17.2 -x- Sequ�ncia XML G N07 0-1 Grupo Opcional
          if (DmNFe_0000.QrNFEItsNICMS_pFCPDif.Value <> 0)
          or (DmNFe_0000.QrNFEItsNICMS_vFCPDif.Value <> 0)
          or (DmNFe_0000.QrNFEItsNICMS_vFCPEfet.Value <> 0) then
          begin
            if Def('212.06', 'N17d', DmNFe_0000.QrNFEItsNICMS_pFCPDif.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.pFCPDif    := Valor;
            if Def('212.07', 'N17e', DmNFe_0000.QrNFEItsNICMS_vFCPDif.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.vFCPDif    := Valor;
            if Def('212.08', 'N17f', DmNFe_0000.QrNFEItsNICMS_vFCPEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.vFCPEfet    := Valor;
          end;
        end;
        60:
        begin
          (* TAG ICMS.ICMS60... *)
              // '213', 'N08'  = Grupo do CST = 60
          if Def('214', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.Orig      := Valor;
          if Def('215', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.CST       := Valor;
////////////////////////////////////////////////////////////////////////////////
//         ini 2021-03-08
(*
          if Def('216', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.VBCSTRet     := Valor;
          if Def('216.1', 'N26a', DmNFe_0000.QrNFEItsNICMS_pST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.pST          := Valor;
          if Def('217', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.VICMSSTRet   := Valor;
*)
          //
          // 215.1 N25.1 -x- Sequ�ncia XML G N08 0-1 Grupo opcional.
          //
          if (DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_pST.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value > 0) then
          begin
            if Def('216', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vBCSTRet        := Valor;
            if Def('216.1', 'N26a', DmNFe_0000.QrNFEItsNICMS_pST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.pST             := Valor;
            if Def('216.2', 'N26b', DmNFe_0000.QrNFEItsNICMS_vICMSSubstituto.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vICMSSubstituto := Valor;
            if Def('217', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vICMSSTRet := Valor;
          end;
//         fim 2021-03-08
////////////////////////////////////////////////////////////////////////////////
          //
          // 217.0 N27.1 - -x- Sequ�ncia XML G N08 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPSTRet.Value <> 0 then
          begin
            if Def('217.w', 'N27a', DmNFe_0000.QrNFEItsNICMS_vBCFCPSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vBCFCPSTRet  := Valor;
            if Def('217.x', 'N27b', DmNFe_0000.QrNFEItsNICMS_pFCPSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.pFCPSTRet    := Valor;
            if Def('217.y', 'N27d', DmNFe_0000.QrNFEItsNICMS_vFCPSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vFCPSTRet    := Valor;
          end;
//         ini 2021-03-08
////////////////////////////////////////////////////////////////////////////////
          //
          // 217.1 N33 -x- Sequ�ncia XML G N08 0-1 Grupo opcional para informa��es do ICMS Efetivo (Inclu�do na NT2016.002)4
          if (DmNFe_0000.QrNFEItsNICMS_pRedBCEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_vBCEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_pICMSEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_vICMSEfet.Value > 0) then
          begin
            if Def('217.2', 'N34', DmNFe_0000.QrNFEItsNICMS_pRedBCEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.pRedBCEfet  := Valor;
            if Def('217.3', 'N35', DmNFe_0000.QrNFEItsNICMS_vBCEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vBCEfet  := Valor;
            if Def('217.4', 'N36', DmNFe_0000.QrNFEItsNICMS_pICMSEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.pICMSEfet  := Valor;
            if Def('217.5', 'N37', DmNFe_0000.QrNFEItsNICMS_vICMSEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.vICMSEfet  := Valor;
          end;
//         fim 2021-03-08
////////////////////////////////////////////////////////////////////////////////
          //
        end;
        70:
        begin
          (* TAG ICMS.ICMS70... *)
              // '218', 'N09'  = Grupo do CST = 70
          if Def('219', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.Orig      := Valor;
          if Def('220', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.CST       := Valor;
          if Def('221', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.ModBC     := Valor;
          if Def('222', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PRedBC    := Valor;
          if Def('223', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VBC       := Valor;
          if Def('224', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PICMS     := Valor;
          if Def('225', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMS     := Valor;
          //
          // 225.01 N17.0 -x- Sequ�ncia XML G N09 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value <> 0 then
          begin
            if Def('225.02', 'N17a', DmNFe_0000.QrNFEItsNICMS_vBCFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.vBCFCP    := Valor;
            if Def('225.03', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.pFCP      := Valor;
            if Def('225.04', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.vFCP      := Valor;
          end;
          if Def('226', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.ModBCST   := Valor;
          if Def('227', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PMVAST    := Valor;
          if Def('228', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PRedBCST  := Valor;
          if Def('229', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VBCST     := Valor;
          if Def('230', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PICMSST   := Valor;
          if Def('231', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMSST   := Valor;
          //
          // 231.10 N23.1 -x- Sequ�ncia XML G N09 0-1 Grupo opcional. (Inclu�do na NT2016.002
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
           if Def('231.11', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.vBCFCPST  := Valor;
            if Def('231.12', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.pFCPST    := Valor;
            if Def('231.13', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.vFCPST    := Valor;
          end;
          //
          // 231.20 N27.1 -x- Sequ�ncia XML G N09 0-1 Grupo opcional.
          //
          if Def('231.21', 'N28a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMSDeson     := Valor;
          if Def('231.22', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.motDesICMS     := Valor;
          //
          //231.30 N33.1 -x- Sequ�ncia XML
          if DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value <> 0 then
          begin
          if Def('231.31', 'N33a', DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMSSTDeson     := Valor;
          if Def('231.32', 'N33b', DmNFe_0000.QrNFEItsNICMS_motDesICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.MotDesICMSST     := Valor;
          end;
        end;

        90:
        begin
          (* TAG ICMS.ICMS90... *)
              // '232', 'N10'  = Grupo do CST = 90
          if Def('233', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.Orig       := Valor;
          if Def('234', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.CST        := Valor;
          //
          // 234.1 N12.1 -x- Sequ�ncia XML G N10 0-1 Grupo opcional.
          //
          if Def('235', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.ModBC      := Valor;
          if Def('236', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VBC        := Valor;
          if Def('237', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PRedBC     := Valor;
          if Def('238', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PICMS      := Valor;
          if Def('239', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VICMS      := Valor;
          //
          // 239.01 N17.0 -x- Sequ�ncia XML G N12.1 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCP.Value <> 0 then
          begin
            if Def('239.02', 'N17a', DmNFe_0000.QrNFEItsNICMS_vBCFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.vBCFCP     := Valor;
            if Def('239.03', 'N17b', DmNFe_0000.QrNFEItsNICMS_pFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.pFCP       := Valor;
            if Def('239.04', 'N17c', DmNFe_0000.QrNFEItsNICMS_vFCP.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.vFCP       := Valor;
          end;
          //
          // 239.10 N17.1 -x- Sequ�ncia XML G N10 0-1 Grupo opcional.
          //
          if Def('240', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.ModBCST    := Valor;
          if Def('241', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PMVAST     := Valor;
          if Def('242', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PRedBCST   := Valor;
          if Def('243', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VBCST      := Valor;
          if Def('244', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PICMSST    := Valor;
          if Def('245', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VICMSST    := Valor;
          //
          // 245.20 N23.1 -x- Sequ�ncia XML G N17.1 0-1 Grupo opcional. (Inclu�do na NT2016.002)
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
            if Def('245.21', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.vBCFCPST   := Valor;
            if Def('245.22', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.pFCPST     := Valor;
            if Def('245.23', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.vFCPST     := Valor;
          end;
          //
          // 245.24 N27.1 -x- Sequ�ncia XML G N10 0-1 Grupo opcional.
          //
          if DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value <> 0 then
          begin
            if Def('245.25', 'N28a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VICMSDeson     := Valor;
            if Def('245.26', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.motDesICMS     := Valor;
          end;
          //
          // 245.30 N33.1 -x- Sequ�ncia XML
          //
          if DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value <> 0 then
          begin
            if Def('245.31', 'N33a', DmNFe_0000.QrNFEItsNICMS_vICMSSTDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VICMSSTDeson     := Valor;
            if Def('245.32', 'N33b', DmNFe_0000.QrNFEItsNICMS_motDesICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.MotDesICMSST     := Valor;
          end;
        end;
(*
245.01 N10a ICMSPart Partilha do ICMS entre a UF
de origem e UF de destino ou
a UF definida na legisla��o.
CG N01 1-1 Opera��o interestadual para
consumidor final com partilha do
ICMS devido na opera��o entre
a UF de origem e a UF do
destinat�rio ou a UF definida na
P�gina 136 de 232
# ID Campo Descri��o Ele Pai Tipo Ocorr�ncia tamanho Dec. Observa��o
legisla��o. (Ex. UF da
concession�ria de entrega do
ve�culos) (v2.0)
245.02 N11 orig Origem da mercadoria E N10a N 1-1 1 Origem da mercadoria:
0 � Nacional;
1 � Estrangeira � Importa��o
direta;
2 � Estrangeira � Adquirida no
mercado interno. (v2.0)
245.03 N12 CST Tributa��o do ICMS E N10a N 1-1 2 Tributa��o pelo ICMS
10 - Tributada e com cobran�a
do ICMS por substitui��o
tribut�ria;
90 � Outros.
245.04 N13 modBC Modalidade de determina��o
da BC do ICMS
E N10a N 1-1 1 0 - Margem Valor Agregado (%);
1 - Pauta (Valor);
2 - Pre�o Tabelado M�x. (valor);
3 - valor da opera��o.
(v2.0)
245.05 N15 vBC Valor da BC do ICMS E N10a N 1-1 15 2 (v2.0)
245.06 N14 pRedBC Percentual da Redu��o de BC E N10a N 0-1 5 2 (v2.0)
245.07 N16 pICMS Al�quota do imposto E N10a N 1-1 5 2 (v2.0)
245.08 N17 vICMS Valor do ICMS E N10a N 1-1 15 2
245.09 N18 modBCST Modalidade de determina��o
da BC do ICMS ST
E N10a N 1-1 1 0 � Pre�o tabelado ou m�ximo
sugerido;
1 - Lista Negativa (valor);
2 - Lista Positiva (valor);
3 - Lista Neutra (valor);
4 - Margem Valor Agregado (%);
5 - Pauta (valor);
245.10 N19 pMVAST Percentual da margem de
valor Adicionado do ICMS ST
E N10a N 0-1 5 2 (v2.0)
245.11 N20 pRedBCST Percentual da Redu��o de BC
do ICMS ST
E N10a N 0-1 5 2 (v2.0)
245.12 N21 vBCST Valor da BC do ICMS ST E N10a N 1-1 15 2 (v2.0)
245.13 N22 pICMSST Al�quota do imposto do ICMS E N10a N 1-1 5 2 (v2.0)
P�gina 137 de 232
# ID Campo Descri��o Ele Pai Tipo Ocorr�ncia tamanho Dec. Observa��o
ST
245.14 N23 vICMSST Valor do ICMS ST E N10a N 1-1 15 2 Valor do ICMS ST(v2.0)
245.15 N25 pBCOp Percentual da BC opera��o
pr�pria
E N10a N 1-1 5 2 Percentual para determina��o
do valor da Base de C�lculo da
opera��o pr�pria. (v2.0)
245.16 N24 UFST UF para qual � devido o ICMS
ST
E N10a C 1-1 2 Sigla da UF para qual � devido
o ICMS ST da opera��o. (v2.0)

(*
  if Def('245.02', 'N11', DmNFe_0000.QrNFE?orig.Value, Valor) then
cXML.InfNFe.?.orig := Valor;
  if Def('245.03', 'N12', DmNFe_0000.QrNFE?CST.Value, Valor) then
cXML.InfNFe.?.CST := Valor;
  if Def('245.04', 'N13', DmNFe_0000.QrNFE?modBC.Value, Valor) then
cXML.InfNFe.?.modBC := Valor;
  if Def('245.05', 'N15', DmNFe_0000.QrNFE?vBC.Value, Valor) then
cXML.InfNFe.?.vBC := Valor;
  if Def('245.06', 'N14', DmNFe_0000.QrNFE?pRedBC.Value, Valor) then
cXML.InfNFe.?.pRedBC := Valor;
  if Def('245.07', 'N16', DmNFe_0000.QrNFE?pICMS.Value, Valor) then
cXML.InfNFe.?.pICMS := Valor;
  if Def('245.08', 'N17', DmNFe_0000.QrNFE?vICMS.Value, Valor) then
cXML.InfNFe.?.vICMS := Valor;
  if Def('245.09', 'N18', DmNFe_0000.QrNFE?modBCST.Value, Valor) then
cXML.InfNFe.?.modBCST := Valor;
  if Def('245.10', 'N19', DmNFe_0000.QrNFE?pMVAST.Value, Valor) then
cXML.InfNFe.?.pMVAST := Valor;
  if Def('245.11', 'N20', DmNFe_0000.QrNFE?pRedBCST.Value, Valor) then
cXML.InfNFe.?.pRedBCST := Valor;
  if Def('245.12', 'N21', DmNFe_0000.QrNFE?VBCST.Value, Valor) then
cXML.InfNFe.?.VBCST := Valor;
  if Def('245.13', 'N22', DmNFe_0000.QrNFE?pICMSST.Value, Valor) then
cXML.InfNFe.?.pICMSST := Valor;
  if Def('245.14', 'N23', DmNFe_0000.QrNFE?vICMSST.Value, Valor) then
cXML.InfNFe.?.vICMSST := Valor;
  if Def('245.15', 'N25', DmNFe_0000.QrNFE?pBCOp.Value, Valor) then
cXML.InfNFe.?.pBCOp := Valor;
  if Def('245.16', 'N24', DmNFe_0000.QrNFE?UFST.Value, Valor) then
cXML.InfNFe.?.UFST := Valor;
*)



////////////////////////////////////////////////////////////////////////////////

(*
245.17 N10b ICMSST ICMS ST � repasse de ICMS
ST retido anteriormente em
opera��es interestaduais com
repasses atrav�s do Substituto
Tribut�rio
CG N01 1-1 Grupo de informa��o do ICMS
ST devido para a UF de destino,
nas opera��es interestaduais de
produtos que tiveram reten��o
antecipada de ICMS por ST na
UF do remetente. Repasse via
Substituto Tribut�rio. (v2.0)
245.18 N11 orig Origem da mercadoria E N10b N 1-1 1 Origem da mercadoria:
0 � Nacional;
1 � Estrangeira � Importa��o
direta;
2 � Estrangeira � Adquirida no
mercado interno. (v2.0)
245.19 N12 CST Tributa��o do ICMS E N10b N 1-1 2 Tributa��o pelo ICMS
41 � N�o Tributado (v2.0)
245.20 N26 vBCSTRet Valor do BC do ICMS ST
retido na UF remetente
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST retido na UF remetente
(v2.0)
245.21 N27 vICMSSTRet Valor do ICMS ST retido na
UF remetente
E N10b N 1-1 15 2 Informar o valor do ICMS ST
retido na UF remetente (iv2.0)
245.22 N31 vBCSTDest Valor da BC do ICMS ST da
UF destino
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST da UF destino (v2.0)
245.23 N32 vICMSSTDes
t
Valor do ICMS ST da UF
destino
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST da UF destino (v2.0)
*)

(*
  if Def('245.18', 'N11', DmNFe_0000.QrNFE?orig.Value, Valor) then
cXML.InfNFe.?.orig := Valor;
  if Def('245.19', 'N12', DmNFe_0000.QrNFE?CST.Value, Valor) then
cXML.InfNFe.?.CST := Valor;
  if Def('245.20', 'N26', DmNFe_0000.QrNFE?vBCSTRet.Value, Valor) then
cXML.InfNFe.?.vBCSTRet := Valor;
  if Def('245.20a', 'N26a', DmNFe_0000.QrNFE?pST.Value, Valor) then
cXML.InfNFe.?.pST := Valor;
  if Def('245.20b', 'N26b', DmNFe_0000.QrNFE?vICMSSubstituto.Value, Valor) then
cXML.InfNFe.?.vICMSSubstituto := Valor;
  if Def('245.21', 'N27', DmNFe_0000.QrNFE?vICMSSTRet.Value, Valor) then
cXML.InfNFe.?.vICMSSTRet := Valor;
  if Def('245.21b', 'N27a', DmNFe_0000.QrNFE?vBCFCPSTRet.Value, Valor) then
cXML.InfNFe.?.vBCFCPSTRet := Valor;
  if Def('245.21c', 'N27b', DmNFe_0000.QrNFE?pFCPSTRet.Value, Valor) then
cXML.InfNFe.?.pFCPSTRet := Valor;
  if Def('245.21d', 'N27d', DmNFe_0000.QrNFE?vFCPSTRet.Value, Valor) then
cXML.InfNFe.?.vFCPSTRet := Valor;
  if Def('245.22', 'N31', DmNFe_0000.QrNFE?vBCSTDest.Value, Valor) then
cXML.InfNFe.?.vBCSTDest := Valor;
  if Def('245.23', 'N32', DmNFe_0000.QrNFE?vICMSSTDest.Value, Valor) then
cXML.InfNFe.?.vICMSSTDest := Valor;
  if Def('245.23b', 'N34', DmNFe_0000.QrNFE?pRedBCEfet.Value, Valor) then
cXML.InfNFe.?.pRedBCEfet := Valor;
  if Def('245.23c', 'N35', DmNFe_0000.QrNFE?vBCEfet.Value, Valor) then
cXML.InfNFe.?.vBCEfet := Valor;
  if Def('245.23d', 'N36', DmNFe_0000.QrNFE?pICMSEfet.Value, Valor) then
cXML.InfNFe.?.pICMSEfet := Valor;
  if Def('245.23e', 'N37', DmNFe_0000.QrNFE?vICMSEfet.Value, Valor) then
cXML.InfNFe.?.vICMSEfet := Valor;
*)
        else Geral.MB_Aviso('CST do ICMS n�o implementado!');
      end;
    end else // Simples Nacional!
    begin
//------------------------------------------------------------------------------
//Grupo N10c. Grupo CRT=1 (CSON 101)
//------------------------------------------------------------------------------
      case DmNFe_0000.QrNFEItsNICMS_CSOSN.Value of
        101:
        begin
        (* TAG ICMS.ICMSSN101... *)
              // '245.24', 'N10c'  = Grupo CRT=1 - Simples Nacional e CSOSN = 101
          if Def('245.25', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.Orig  := Valor;
          if Def('245.26', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.CSOSN  := Valor;
          if Def('245.27', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.pCredSN  := Valor;
          if Def('245.28', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.vCredICMSSN  := Valor;
        end;
//------------------------------------------------------------------------------
//Grupo N10d. Grupo CRT=1 (CSON 102, 103, 300 ou 400)
//------------------------------------------------------------------------------
        102, 103, 300, 400:
        begin
        (* TAG ICMS.ICMSSN102... *)
              // '245.24', 'N10d'  = Grupo CRT=1 - Simples Nacional e CSOSN = 102, 103, 300 ou 400
          if Def('245.25', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN102.Orig  := Valor;
          if Def('245.26', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN102.CSOSN  := Valor;
        end;
//------------------------------------------------------------------------------
//Grupo N10e. Grupo CRT=1 (CSON 201)
//------------------------------------------------------------------------------
        201:
        begin
          (*TAG ICMS.ICMSSN201... *)
              // '245.27', 'N10e'  = Grupo CRT=1 - Simples Nacional e CSOSN = 201
          if Def('245.28', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.Orig  := Valor;
          if Def('245.29', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.CSOSN  := Valor;
          if Def('245.30', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.modBCST  := Valor;
          if Def('245.31', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pMVAST  := Valor;
          if Def('224.32', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pRedBCST  := Valor;
          if Def('245.33', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vBCST  := Valor;
          if Def('245.34', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pICMSST  := Valor;
          if Def('245.35', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vICMSST  := Valor;
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
            if Def('245.35w', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.imposto.ICMS.ICMSSN201.vBCFCPST := Valor;
            if Def('245.35x', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.imposto.ICMS.ICMSSN201.pFCPST := Valor;
            if Def('245.35y', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.imposto.ICMS.ICMSSN201.vFCPST := Valor;
          end;
          if Def('245.36', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pCredSN  := Valor;
          if Def('245.37', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vCredICMSSN  := Valor;
        end;
//------------------------------------------------------------------------------
//Grupo N10f. Grupo CRT=1 (CSON 202 ou 203)
//------------------------------------------------------------------------------
        202, 203:
        begin
        (* TAG ICMS.ICMSSN202... *)
              // '245.38', 'N10f'  = Grupo CRT=1 - Simples Nacional e CSOSN = 202 ou 203
          if Def('245.39', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.Orig  := Valor;
          if Def('245.40', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.CSOSN  := Valor;
          if Def('245.41', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.modBCST  := Valor;
          if Def('245.42', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pMVAST  := Valor;
          if Def('245.43', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pRedBCST  := Valor;
          if Def('245.44', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.vBCST  := Valor;
          if Def('245.45', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pICMSST  := Valor;
          if Def('245.46', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.vICMSST  := Valor;
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
            if Def('245.46w', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN202.vBCFCPST := Valor;
            if Def('245.46x', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN202.pFCPST := Valor;
            if Def('245.46y', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN202.vFCPST := Valor;
          end;
        end;
//------------------------------------------------------------------------------
//Grupo N10g. Grupo CRT=1 (CSON 500)
//------------------------------------------------------------------------------
        500:
        begin
        (* TAG ICMS.ICMSSN500... *)
              // '245.47', 'N10g'  = Grupo CRT=1 - Simples Nacional e CSOSN = 500
          if Def('245.48', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.Orig  := Valor;
          if Def('245.49', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.CSOSN  := Valor;

          // 2021-02-27 *** Provi�rio *** Ver o que fazer quando necess�rio!!!!
          if (DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value <> 0)
          or (DmNFe_0000.QrNFEItsNICMS_pST.Value <> 0)
          or (DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value <> 0) then
          begin
            if Def('245.50', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vBCSTRet  := Valor;
            if Def('245.50.0', 'N26a', DmNFe_0000.QrNFEItsNICMS_pST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.pST := Valor;
            if Def('245.50.1', 'N26b', DmNFe_0000.QrNFEItsNICMS_vICMSSubstituto.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vICMSSubstituto := Valor;
            if Def('245.51', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vICMSSTRet  := Valor;
          end;
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPSTRet.Value <> 0 then
          begin
            if Def('245.51w', 'N27a', DmNFe_0000.QrNFEItsNICMS_vBCFCPSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vBCFCPSTRet := Valor;
            if Def('245.51x', 'N27b', DmNFe_0000.QrNFEItsNICMS_pFCPSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.pFCPSTRet := Valor;
            if Def('245.51y', 'N27d', DmNFe_0000.QrNFEItsNICMS_vFCPSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vFCPSTRet := Valor;
          end;
          //
//         ini 2021-03-08
////////////////////////////////////////////////////////////////////////////////
          //
// 245.51.1 N33 - x - Sequ�ncia XML G N10g 0-1 Grupo opcional para informa��es do ICMS Efetivo. (Inclu�do na NT2016.002)
          if (DmNFe_0000.QrNFEItsNICMS_pRedBCEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_vBCEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_pICMSEfet.Value > 0)
          or (DmNFe_0000.QrNFEItsNICMS_vICMSEfet.Value > 0) then
          begin
            if Def('245.51.2', 'N34', DmNFe_0000.QrNFEItsNICMS_pRedBCEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.pRedBCEfet  := Valor;
            if Def('245.51.3', 'N35', DmNFe_0000.QrNFEItsNICMS_vBCEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vBCEfet  := Valor;
            if Def('245.51.4', 'N36', DmNFe_0000.QrNFEItsNICMS_pICMSEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.pICMSEfet  := Valor;
            if Def('245.51.5', 'N37', DmNFe_0000.QrNFEItsNICMS_vICMSEfet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN500.vICMSEfet  := Valor;
          end;
//         fim 2021-03-08
////////////////////////////////////////////////////////////////////////////////
        end;
//------------------------------------------------------------------------------
//Grupo N10h. Grupo CRT=1 (CSON 900)
//------------------------------------------------------------------------------
        900:
        begin
        (* TAG ICMS.ICMSSN900... *)
              // '245.52', 'N10h'  = Grupo CRT=1 - Simples Nacional e CSOSN = 900
          if Def('245.53', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.Orig  := Valor;
          if Def('245.54', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.CSOSN  := Valor;
          if Def('245.55', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.modBC  := Valor;
          if Def('245.56', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vBC  := Valor;
          if Def('245.57', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pRedBC  := Valor;
          if Def('245.58', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pICMS  := Valor;
          if Def('245.59', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vICMS  := Valor;
          if Def('245.60', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.modBCST  := Valor;
          if Def('245.61', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pMVAST  := Valor;
          if Def('245.62', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pRedBCST  := Valor;
          if Def('245.63', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vBCST  := Valor;
          if Def('245.64', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pICMSST  := Valor;
          if Def('245.65', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vICMSST  := Valor;
          //
          if DmNFe_0000.QrNFEItsNICMS_pFCPST.Value <> 0 then
          begin
            if Def('245.65w', 'N23a', DmNFe_0000.QrNFEItsNICMS_vBCFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN900.vBCFCPST := Valor;
            if Def('245.65x', 'N23b', DmNFe_0000.QrNFEItsNICMS_pFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN900.pFCPST := Valor;
            if Def('245.65y', 'N23d', DmNFe_0000.QrNFEItsNICMS_vFCPST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMSSN900.vFCPST := Valor;
          end;
          if Def('245.52', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pCredSN  := Valor;
          if Def('245.53', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vCredICMSSN  := Valor;
        end;
        else Geral.MB_Aviso('CSOSN (' +
        FormatFloat('0', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value) +
        ') do CRT=1 n�o implementado!');
      end;
    end;
{
    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    if GeraNT2015_03 then
    begin
      DmNFe_0000.ReopenNFeItsNA(FatID, FatNum, Empresa, nItem);
      if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
      begin
        (* TAG ICMS para a UF de destino... *)
            // '245a.01',  'NA01'  = Grupo de Tributa��o do ICMS para a UF de destino
        if Def('245a.03', 'NA03', DmNFe_0000.QrNFeItsNAICMSUFDest_vBCUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VBCUFDest := Valor;
        if Def('245a.05', 'NA05', DmNFe_0000.QrNFeItsNAICMSUFDest_pFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PFCPUFDest := Valor;
        if Def('245a.07', 'NA07', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSUFDest := Valor;
        if Def('245a.09', 'NA09', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInter.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInter := Valor;
        if Def('245a.11', 'NA11', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInterPart.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInterPart := Valor;
        if Def('245a.13', 'NA13', DmNFe_0000.QrNFeItsNAICMSUFDest_vFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VFCPUFDest := Valor;
        if Def('245a.15', 'NA15', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFDest := Valor;
        if Def('245a.17', 'NA17', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFRemet.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFRemet := Valor;
      end;
    end;
    //Fim PL 008h
}

    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    if GeraNT2015_03 then
    begin
      DmNFe_0000.ReopenNFeItsNA(FatID, FatNum, Empresa, nItem);
      if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
      begin
    {  ini 2023-10-31
        (* TAG ICMS para a UF de destino... *)
            // '245a.01',  'NA01'  = Grupo de Tributa��o do ICMS para a UF de destino
        if Def('245a.03', 'NA03', DmNFe_0000.QrNFeItsNAICMSUFDest_vBCUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VBCUFDest := Valor;
        if Def('245a.04', 'NA04', DmNFe_0000.QrNFeItsNAICMSUFDest_vBCFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.vBCFCPUFDest := Valor;
        if Def('245a.05', 'NA05', DmNFe_0000.QrNFeItsNAICMSUFDest_pFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PFCPUFDest := Valor;
        if Def('245a.07', 'NA07', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSUFDest := Valor;
        if Def('245a.09', 'NA09', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInter.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInter := Valor;
        if Def('245a.11', 'NA11', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInterPart.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInterPart := Valor;
        if Def('245a.13', 'NA13', DmNFe_0000.QrNFeItsNAICMSUFDest_vFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VFCPUFDest := Valor;
        if Def('245a.15', 'NA15', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFDest := Valor;
        if Def('245a.17', 'NA17', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFRemet.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFRemet := Valor;

   fim 2023-10-31}
      end;
    end;

    //Fim PL 008h




{  ini 2021-02-27
    if (*(DmNFe_0000.QrFilialSimplesFed.Value = 0) and*)
    (DmNFe_0000.QrNFEItsITem_IPI.Value = 1) then
}
(*
NOTA EXPLICATIVA:
O C�digo de Situa��o da Opera��o no Simples Nacional - CSOSN ser� usado na Nota
Fiscal Eletr�nica exclusivamente quando o C�digo de Regime Tribut�rio - CRT for
igual a "1", e substituir� os c�digos da Tabela B - Tributa��o pelo ICMS do
Anexo C�digo de Situa��o Tribut�ria - CST do Conv�nio s/n� de 15 de dezembro de
1970. Ver exemplo AQUI.

Se for informado CRT=1 (Simples Nacional) N�O dever� ser informado o CST, e sim
CSOSN. Caso contr�rio haver� Rejei��o 590 : Informado CST para emissor do
Simples Nacional (CRT=1)

Preencher os campos abaixo (PIS/COFINS) da forma indicada, sem preencher as
informa��es sobre o IPI:
"campo CST - Situa��o Tribut�ria" preencher - "99" (99- outras opera��es) "
"tipo de c�lculo " em valor", mais:
Al�quota (em reais) - 0 (zero);
Quantidade vendida - 0 (zero); e Valor (PIS ou COFINS) - 0 (zero)
*)

{
// Ini > Comentado em 2021-10-08
    if (DmNFe_0000.QrFilialSimplesFed.Value = 0) then
    begin
      // Nada. N�o preencher conforme nota explicativa acima!
    end else
// Fim > Comentado em 2021-10-08
}

    if(DmNFe_0000.QrNFEItsITem_IPI.Value = 1) then
    begin
(*
      DmNFe_0000.QrNFEItsO.Close;
      DmNFe_0000.QrNFEItsO.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsO.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsO.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsO.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsO. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsO(FatID, FatNum, Empresa, nItem);
      //
      (* TAG IPI... *)
          // '246', 'O01'  = Grupo do IPI
      if Def('248', 'O03', DmNFe_0000.QrNFEItsOIPI_CNPJProd.Value, Valor) then
        cDetLista.Imposto.IPI.CNPJProd := Valor;
      if Def('249', 'O04', DmNFe_0000.QrNFEItsOIPI_cSelo.Value, Valor) then
        cDetLista.Imposto.IPI.CSelo := Valor;
      if Def('250', 'O05', DmNFe_0000.QrNFEItsOIPI_qSelo.Value, Valor) then
        cDetLista.Imposto.IPI.QSelo := Valor;
      if Def('251', 'O06', DmNFe_0000.QrNFEItsOIPI_cEnq.Value, Valor) then
        cDetLista.Imposto.IPI.CEnq := Valor;

      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([0,49,50,99]) then
      begin
        (* TAG IPI.IPITRIB... *)
            // '252', 'O08'  = Grupo do CST 00, 40, 50 e 99
        if Def('253', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
        begin
          if Valor = '0' then
            Valor := '00';
          cDetLista.Imposto.IPI.IPITrib.CST := Valor;
        end;
        if DmNFe_0000.QrNFEItsOIPI_vUnid.Value = 0 then
        begin
          if Def('254', 'O10', DmNFe_0000.QrNFEItsOIPI_vBC.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.VBC := Valor;
          if Def('257', 'O13', DmNFe_0000.QrNFEItsOIPI_pIPI.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.PIPI := Valor;
        end else begin
          if Def('255', 'O11', DmNFe_0000.QrNFEItsOIPI_qUnid.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.QUnid := Valor;
          if Def('256', 'O12', DmNFe_0000.QrNFEItsOIPI_vUnid.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.VUnid := Valor;
        end;
            // '258', '???'  = ?????
        if Def('259', 'O14', DmNFe_0000.QrNFEItsOIPI_vIPI.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.VIPI := Valor;
      end else
      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([1,2,3,4,51,52,53,54,55]) then
      begin
        (* TAG IPI.IPINT... *)
            // '260', 'O08'  = Grupo do CST 01, 02, 03, 04, 51, 52, 53, 54 e 55
        if Def('261', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
          cDetLista.Imposto.IPI.IPINT.CST := FormatFloat('00', DmNFe_0000.QrNFEItsOIPI_CST.Value)
      end else
        Geral.MB_Erro('CST do IPI desconhecido!');
    end;
    (* TAG II... *)
(*
    DmNFe_0000.QrNFEItsP.Close;
    DmNFe_0000.QrNFEItsP.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsP.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsP.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsP.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsP. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsP(FatID, FatNum, Empresa, nItem);
    // '262', 'P01'  = Grupo do II
    if DmNFe_0000.QrNFEItsP.RecordCount > 0 then
    begin
      if (DmNFe_0000.QrNFEItsPII_vII.Value > 0) or (DmNFe_0000.QrNFEItsITem_II.Value <> 0) then
      begin
        if Def('263', 'P02', DmNFe_0000.QrNFEItsPII_vBC.Value, Valor) then
          cDetLista.Imposto.II.VBC := Valor;
        if Def('264', 'P03', DmNFe_0000.QrNFEItsPII_vDespAdu.Value, Valor) then
          cDetLista.Imposto.II.VDespAdu := Valor;
        if Def('265', 'P04', DmNFe_0000.QrNFEItsPII_vII.Value, Valor) then
          cDetLista.Imposto.II.VII := Valor;
        if Def('266', 'P05', DmNFe_0000.QrNFEItsPII_vIOF.Value, Valor) then
          cDetLista.Imposto.II.VIOF := Valor;
      end;
    end;
    //

    (* TAGs PIS... *)
    // '267', 'Q01'  = Grupo do PIS
    // ini 2021-02-23 Tisolin
    if DmNFe_0000.QrNFECabAemit_CRT.Value = 1 then
    begin
      (* TAG PIS.PISNT... *)
      //if Def('263', 'P02', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
    {  ini 2023-10-31
        cDetLista.Imposto.PIS.PISNT.CST := '08'; // 08=Opera��o Sem Incid�ncia da Contribui��o;
      fim 2023-10-31}
    end
    else
    // fim 2021-02-23 Tisolin
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
(*
NOTA EXPLICATIVA:
O C�digo de Situa��o da Opera��o no Simples Nacional - CSOSN ser� usado na Nota
Fiscal Eletr�nica exclusivamente quando o C�digo de Regime Tribut�rio - CRT for
igual a "1", e substituir� os c�digos da Tabela B - Tributa��o pelo ICMS do
Anexo C�digo de Situa��o Tribut�ria - CST do Conv�nio s/n� de 15 de dezembro de
1970. Ver exemplo AQUI.

Se for informado CRT=1 (Simples Nacional) N�O dever� ser informado o CST, e sim
CSOSN. Caso contr�rio haver� Rejei��o 590 : Informado CST para emissor do
Simples Nacional (CRT=1)

Preencher os campos abaixo (PIS/COFINS) da forma indicada, sem preencher as
informa��es sobre o IPI:
"campo CST - Situa��o Tribut�ria" preencher - "99" (99- outras opera��es) "
"tipo de c�lculo " em valor", mais:
Al�quota (em reais) - 0 (zero);
Quantidade vendida - 0 (zero); e Valor (PIS ou COFINS) - 0 (zero)
*)
      cDetLista.Imposto.PIS.PISAliq.CST  := '99';
      cDetLista.Imposto.PIS.PISAliq.VBC  := '0.00';
      cDetLista.Imposto.PIS.PISAliq.PPIS := '0.00';
      cDetLista.Imposto.PIS.PISAliq.VPIS := '0.00';
    end
    else
    begin
(*
      DmNFe_0000.QrNFEItsQ.Close;
      DmNFe_0000.QrNFEItsQ.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsQ.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsQ.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsQ.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsQ. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsQ(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsQ.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de PIS!' +
        sLineBreak + 'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      // '268', 'Q02' Tag do PIS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([1,2]) then
      begin
        (* TAG PIS.PISALIQ... *)
        if Def('269', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.CST := Valor;
        if Def('270', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VBC := Valor;
        if Def('271', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.PPIS := Valor;
        if Def('272', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VPIS := Valor;
      end else
      // '273', 'Q03' Tag do PIS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([3]) then
      begin
        (* TAG PIS.PISQTDE... *)
        if Def('274', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.CST := Valor;
        if Def('275', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.QBCProd := Valor;
        if Def('276', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VAliqProd := Valor;
        if Def('277', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VPIS := Valor;
      end else
      // '278', 'Q04' Tag do PIS n�o tributado CST = 04, 05, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([4,5,6,7,8,9]) then
      begin
        (* TAG PIS.PISNT... *)
        if Def('279', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISNT.CST := Valor;
      end else
      // '280', 'Q05' Tag do PIS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([49,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67,70,71,72,73,74,75,98,99]) then
      begin
        (* TAG PIS.PISOUTR... *)
        if Def('281', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsQPIS_pPIS.Value > 0 then
        begin
          if Def('282', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VBC := Valor;
          if Def('283', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.PPIS := Valor;
        end else begin
          if Def('284', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.QBCProd := Valor;
          if Def('285', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VAliqProd := Valor;
        end;
        if Def('286', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.VPIS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MB_Erro('CST do PIS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value));
          //Result := False;
        Exit;
      end;
      // '287', 'R01' Tag do PISST...
      (* TAG PISST... *)
(*
      DmNFe_0000.QrNFEItsR.Close;
      DmNFe_0000.QrNFEItsR.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsR.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsR.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsR.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsR. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsR(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsR.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de PIS ST!' + sLineBreak +
        'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) or
      (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) then
        begin
          if Def('288', 'R02', DmNFe_0000.QrNFEItsRPISST_vBC.Value, Valor) then
            cDetLista.Imposto.PISST.VBC := Valor;
          if Def('289', 'R03', DmNFe_0000.QrNFEItsRPISST_pPIS.Value, Valor) then
            cDetLista.Imposto.PISST.PPIS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
        begin
          if Def('290', 'R04', DmNFe_0000.QrNFEItsRPISST_qBCProd.Value, Valor) then
            cDetLista.Imposto.PISST.QBCProd := Valor;
          if Def('291', 'R05', DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PISST.VAliqProd := Valor;
        end;
        if Def('292', 'R06', DmNFe_0000.QrNFEItsRPISST_vPIS.Value, Valor) then
          cDetLista.Imposto.PISST.VPIS := Valor;
        // ini 2021-03-12
        if Def('292.01', 'R07', DmNFe_0000.QrNFEItsRPISST_indSomaPISST.Value, Valor) then
          cDetLista.Imposto.PISST.indSomaPISST := Valor;
        // ini 2021-03-12

      end;
    end;
      //
    (* TAGs COFINS... *)
    // '293', 'S01'  = Grupo do COFINS
    if DmNFe_0000.QrNFECabAemit_CRT.Value = 1 then
    begin
      (* TAG PIS.PISNT... *)
      //if Def('305', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
    {  ini 2023-10-31
        cDetLista.Imposto.COFINS.COFINSNT.CST := '08'; // 08=Opera��o Sem Incid�ncia da Contribui��o;
      fim 2023-10-31}
    end
    else
    // fim 2021-02-23 Tisolin
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
(*
NOTA EXPLICATIVA:
O C�digo de Situa��o da Opera��o no Simples Nacional - CSOSN ser� usado na Nota
Fiscal Eletr�nica exclusivamente quando o C�digo de Regime Tribut�rio - CRT for
igual a "1", e substituir� os c�digos da Tabela B - Tributa��o pelo ICMS do
Anexo C�digo de Situa��o Tribut�ria - CST do Conv�nio s/n� de 15 de dezembro de
1970. Ver exemplo AQUI.

Se for informado CRT=1 (Simples Nacional) N�O dever� ser informado o CST, e sim
CSOSN. Caso contr�rio haver� Rejei��o 590 : Informado CST para emissor do
Simples Nacional (CRT=1)

Preencher os campos abaixo (PIS/COFINS) da forma indicada, sem preencher as
informa��es sobre o IPI:
"campo CST - Situa��o Tribut�ria" preencher - "99" (99- outras opera��es) "
"tipo de c�lculo " em valor", mais:
Al�quota (em reais) - 0 (zero);
Quantidade vendida - 0 (zero); e Valor (PIS ou COFINS) - 0 (zero)
*)
      cDetLista.Imposto.COFINS.COFINSAliq.CST     := '99';
      cDetLista.Imposto.COFINS.COFINSAliq.VBC     := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := '0.00';
    end else
    begin
(*
      DmNFe_0000.QrNFEItsS.Close;
      DmNFe_0000.QrNFEItsS.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsS.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsS.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsS.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsS. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsS(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsS.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de COFINS!' +
        sLineBreak + 'Gera��o abortada!');
        Exit;
      end;
      // '294', 'S02' Tag do COFINS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([1,2]) then
      begin
        (* TAG COFINS.COFINSALIQ... *)
        if Def('295', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.CST := Valor;
        if Def('296', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VBC := Valor;
        if Def('297', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := Valor;
        if Def('298', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := Valor;
      end else
      // '299', 'S03' Tag do COFINS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([3]) then
      begin
        (* TAG COFINS.COFINSQTDE... *)
        if Def('300', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.CST := Valor;
        if Def('301', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.QBCProd := Valor;
        if Def('302', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VAliqProd := Valor;
        if Def('303', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VCOFINS := Valor;
      end else
      // '304', 'S04' Tag do COFINS n�o tributado CST = 04, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([4,5,6,7,8,9]) then
      begin
        (* TAG COFINS.COFINSNT... *)
        if Def('305', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSNT.CST := Valor;
      end else
      // '306', 'S05' Tag do COFINS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([49,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67,70,71,72,73,74,75,98,99]) then
      begin
        (* TAG COFINS.COFINSOUTR... *)
        if Def('307', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value > 0 then
        begin
          if Def('308', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VBC := Valor;
          if Def('309', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.PCOFINS := Valor;
        end else begin
          if Def('310', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.QBCProd := Valor;
          if Def('311', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VAliqProd := Valor;
        end;
        if Def('312', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.VCOFINS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MB_Erro('CST do COFINS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value));
        Exit;
      end;
      // '313', 'T01' Tag do COFINSST...
      (* TAG COFINSST... *)
(*
      DmNFe_0000.QrNFEItsT.Close;
      DmNFe_0000.QrNFEItsT.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsT.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsT.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsT.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsT. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsT(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsT.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de COFINS ST!' +
        sLineBreak + 'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) or
      (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) then
        begin
          if Def('314', 'T02', DmNFe_0000.QrNFEItsTCOFINSST_vBC.Value, Valor) then
            cDetLista.Imposto.COFINSST.VBC := Valor;
          if Def('315', 'T03', DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINSST.PCOFINS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
        begin
          if Def('316', 'T04', DmNFe_0000.QrNFEItsTCOFINSST_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.QBCProd := Valor;
          if Def('317', 'T05', DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.VAliqProd := Valor;
        end;
        if Def('318', 'T06', DmNFe_0000.QrNFEItsTCOFINSST_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINSST.VCOFINS := Valor;
        // ini 2021-03-12
        if Def('318.01', 'T07', DmNFe_0000.QrNFEItsTCOFINSST_indSomaCOFINSST.Value, Valor) then
          cDetLista.Imposto.COFINSST.indSomaCOFINSST := Valor;
        // ini 2021-03-12
      end;
    end;
    //
    // '319', 'U01' Tag do grupo ISSQN...
    (* TAG ISSQN.. *)
(*
O grupo de ISSQN �
mutuamente exclusivo com os
grupos ICMS, IPI e II, isto � se
ISSQN for informado os grupos
ICMS, IPI e II n�o ser�o
informados e vice-versa (v2.0).
*)
(*
    DmNFe_0000.QrNFEItsU.Close;
    DmNFe_0000.QrNFEItsU.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsU.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsU.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsU.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsU. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsU(FatID, FatNum, Empresa, nItem);

    if DmNFe_0000.QrNFEItsU.RecordCount > 0 then
    begin
      if DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value > 0 then
      begin
        if Def('320', 'U02', DmNFe_0000.QrNFEItsUISSQN_vBC.Value, Valor) then
          cDetLista.Imposto.ISSQN.VBC := Valor;
        if Def('321', 'U03', DmNFe_0000.QrNFEItsUISSQN_vAliq.Value, Valor) then
          cDetLista.Imposto.ISSQN.VAliq := Valor;
        if Def('322', 'U04', DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value, Valor) then
          cDetLista.Imposto.ISSQN.VISSQN := Valor;
        if Def('323', 'U05', DmNFe_0000.QrNFEItsUISSQN_cMunFG.Value, Valor) then
          cDetLista.Imposto.ISSQN.CMunFG := Valor;
        if Def('324', 'U06', DmNFe_0000.QrNFEItsUISSQN_cListServ.Value, Valor) then
          cDetLista.Imposto.ISSQN.CListServ := Valor;
        // 2.00
        //if Def('324a', 'U07', DmNFe_0000.QrNFEItsUISSQN_cSitTrib.Value, Valor) then
        //  cDetLista.Imposto.ISSQN.cSitTrib := Valor;
        // 3.10
        if Def('324a', 'U07', DmNFe_0000.QrNFEItsUISSQN_vDeducao.Value, Valor) then
          cDetLista.Imposto.ISSQN.vDeducao := Valor;
        if Def('324b', 'U08', DmNFe_0000.QrNFEItsUISSQN_vOutro.Value, Valor) then
          cDetLista.Imposto.ISSQN.vOutro := Valor;
        if Def('324c', 'U09', DmNFe_0000.QrNFEItsUISSQN_vDescIncond.Value, Valor) then
          cDetLista.Imposto.ISSQN.vdescIncond := Valor;
        if Def('324d', 'U10', DmNFe_0000.QrNFEItsUISSQN_vDescCond.Value, Valor) then
          cDetLista.Imposto.ISSQN.vDescCond := Valor;
        if Def('324f', 'U11', DmNFe_0000.QrNFEItsUISSQN_vISSRet.Value, Valor) then
          cDetLista.Imposto.ISSQN.vISSRet := Valor;
        if Def('324g', 'U12', DmNFe_0000.QrNFEItsUISSQN_indISS.Value, Valor) then
          cDetLista.Imposto.ISSQN.indISS := Valor;
        if Def('324h', 'U13', DmNFe_0000.QrNFEItsUISSQN_cServico.Value, Valor) then
          cDetLista.Imposto.ISSQN.cServico := Valor;
        if Def('324i', 'U14', DmNFe_0000.QrNFEItsUISSQN_cMun.Value, Valor) then
          cDetLista.Imposto.ISSQN.cMun := Valor;
        if Def('324j', 'U15', DmNFe_0000.QrNFEItsUISSQN_cPais.Value, Valor) then
          cDetLista.Imposto.ISSQN.cPais := Valor;
        if Def('324k', 'U16', DmNFe_0000.QrNFEItsUISSQN_nProcesso.Value, Valor) then
          cDetLista.Imposto.ISSQN.nProcesso := Valor;
        if Def('324l', 'U17', DmNFe_0000.QrNFEItsUISSQN_indIncentivo.Value, Valor) then
          cDetLista.Imposto.ISSQN.indIncentivo := Valor;
      end;
    end;
    // ini 2021-03-08
//------------------------------------------------------------------------------
//Grupo UA. Tributos Devolvidos (para o item da NF-e)
//------------------------------------------------------------------------------
    DmNFe_0000.ReopenNFeItsUA(FatID, FatNum, Empresa, nItem);
    if DmNFe_0000.QrNFEItsUA.RecordCount > 0 then
    begin
//324p UA01 impostoDevol Informa��o do Imposto devolvido
      if DmNFe_0000.QrNFEItsUAimpostoDevol_pDevol.Value > 0 then
      begin
        if Def('324q', 'UA02', DmNFe_0000.QrNFEItsUAimpostoDevol_pDevol.Value, Valor) then
          cDetLista.ImpostoDevol.PDevol := Valor;
//324r UA03 IPI Informa��o do IPI devolvido G UA01 1-1
        if Def('324s', 'UA04', DmNFe_0000.QrNFEItsUAimpostoDevol_vIPIDevol.Value, Valor) then
          cDetLista.ImpostoDevol.IPI.vIPIDevol := Valor;
      end;
    end;
    // fim 2021-03-08


(*
    DmNFe_0000.QrNFEItsV.Close;
    DmNFe_0000.QrNFEItsV.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsV.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsV.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsV.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsV. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsV(FatID, FatNum, Empresa, nItem);

    if DmNFe_0000.QrNFEItsV.RecordCount > 0 then
      InfAdProd := DmNFe_0000.QrNFEItsVinfAdProd.Value
    else
      InfAdProd := '';
    //
    if Def('325', 'V01', InfAdProd, Valor) then
      cDetLista.InfAdProd := Valor;
    //

(* VA - Observa��es de uso livre (para o item da NF-e)... *)
    DmNFe_0000.ReopenNFeItsVA(FatID, FatNum, Empresa, nItem);

    if (DmNFe_0000.QrNFeItsVAobsCont_xCampo.Value <> EmptyStr)
    or (DmNFe_0000.QrNFeItsVAobsCont_xTexto.Value <> EmptyStr) then
    begin
      if Def('325c', 'VA03', DmNFe_0000.QrNFeItsVAobsCont_xCampo.Value, Valor) then
        cDetLista.ObsItem.ObsCont_item.XCampo := Valor;
      if Def('325d', 'VA04', DmNFe_0000.QrNFeItsVAobsCont_xTexto.Value, Valor) then
        cDetLista.ObsItem.ObsCont_item.XTexto := Valor;
    end;
    if (DmNFe_0000.QrNFeItsVAobsFisco_xCampo.Value <> EmptyStr)
    or (DmNFe_0000.QrNFeItsVAobsFisco_xTexto.Value <> EmptyStr) then
    begin
      if Def('325f', 'VA06', DmNFe_0000.QrNFeItsVAobsFisco_xCampo.Value, Valor) then
        cDetLista.ObsItem.ObsFisco_item.XCampo := Valor;
      if Def('325g', 'VA07', DmNFe_0000.QrNFeItsVAobsFisco_xTexto.Value, Valor) then
        cDetLista.ObsItem.ObsFisco_item.XTexto := Valor;
    end;
    //

    DmNFe_0000.QrNFEItsI.Next;
  end;

(* W - Informa��es da TAG TOTAL... *)
  (* TAG ICMSTOT... *)
      // '326', 'W01'  =  Grupo de valores totais da NF-e
      // '327', 'W02'  =  Grupo de valores totais referentes ao ICMS
  if Def('328', 'W03', DmNFe_0000.QrNFECabAICMSTot_vBC.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBC := Valor;
  if Def('329', 'W04', DmNFe_0000.QrNFECabAICMSTot_vICMS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VICMS := Valor;
  if Def('329.01', 'W04a', DmNFe_0000.QrNFECabAICMSTot_vICMSDeson.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vICMSDeson := Valor;
  //
  if GeraNT2015_03 then
  begin
    if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
    begin
      if Def('329.03', 'W04c', DmNFe_0000.QrNFECabAICMSTot_vFCPUFDest.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VFCPUFDest := Valor;
      if Def('329.05', 'W04e', DmNFe_0000.QrNFECabAICMSTot_vICMSUFDest.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VICMSUFDest := Valor;
      if Def('329.07', 'W04g', DmNFe_0000.QrNFECabAICMSTot_vICMSUFRemet.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VICMSUFRemet := Valor;
    end;
  end;
  if Def('329.08', 'W04h', DmNFe_0000.QrNFECabAICMSTot_vFCP.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VFCP := Valor;
  if Def('330', 'W05', DmNFe_0000.QrNFECabAICMSTot_vBCST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBCST := Valor;
  if Def('331', 'W06', DmNFe_0000.QrNFECabAICMSTot_vST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VST := Valor;
  if Def('331.01', 'W06a', DmNFe_0000.QrNFECabAICMSTot_vFCPST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VFCPST := Valor;
  if Def('331.02', 'W06b', DmNFe_0000.QrNFECabAICMSTot_vFCPSTRet.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vFCPSTRet := Valor;
  if Def('331.01', 'W06a', DmNFe_0000.QrNFECabAICMSTot_vFCPST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vFCPST := Valor;
  if Def('331.02', 'W06b', DmNFe_0000.QrNFECabAICMSTot_vFCPSTRet.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vFCPSTRet := Valor;
  if Def('332', 'W07', DmNFe_0000.QrNFECabAICMSTot_vProd.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VProd := Valor;
  if Def('333', 'W08', DmNFe_0000.QrNFECabAICMSTot_vFrete.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VFrete := Valor;
  if Def('334', 'W09', DmNFe_0000.QrNFECabAICMSTot_vSeg.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VSeg := Valor;
  if Def('335', 'W10', DmNFe_0000.QrNFECabAICMSTot_vDesc.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VDesc := Valor;
  if Def('336', 'W11', DmNFe_0000.QrNFECabAICMSTot_vII.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VII := Valor;
  if Def('337', 'W12', DmNFe_0000.QrNFECabAICMSTot_vIPI.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VIPI := Valor;
  if Def('337.01', 'W12a', DmNFe_0000.QrNFECabAICMSTot_vIPIDevol.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vIPIDevol := Valor;
  if Def('338', 'W13', DmNFe_0000.QrNFECabAICMSTot_vPIS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VPIS := Valor;
  if Def('339', 'W14', DmNFe_0000.QrNFECabAICMSTot_vCOFINS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VCOFINS := Valor;
  if Def('340', 'W15', DmNFe_0000.QrNFECabAICMSTot_vOutro.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VOutro := Valor;
  if Def('341', 'W16', DmNFe_0000.QrNFECabAICMSTot_vNF.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VNF := Valor;
  // 2013-05-07
  //if DmNFe_0000.QrFilialNFeNT2013_003LTT.Value > 0 then
  begin
    // 2017-01-30 In�cio
    //if DmNFe_0000.QrNFECabANFeNT2013_003LTT.Value > 0 then
    // 2017-01-30 Fim
    if UnNFe_PF.VerificaSeCalculaTributos(DmNFe_0000.QrNFECabANFeNT2013_003LTT.Value,
      DModG.QrPrmsEmpNFeNFe_indFinalCpl.Value, DmNFe_0000.QrNFECabAide_indFinal.Value) then
    begin
      if Def('341a', 'W16a', DmNFe_0000.QrNFECabAvTotTrib.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.vTotTrib := Valor;
    end;
  end;
  // FIM 2013-05-07
  //
  (* TAG ISSQNTOT... Opcional *)
  (*  Parei aqui
  cXML.InfNFe.Total.ISSQNtot.VServ := Valor;
  cXML.InfNFe.Total.ISSQNtot.VBC := Valor;
  cXML.InfNFe.Total.ISSQNtot.VISS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VPIS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VCOFINS := Valor;

  (*

  if Def('343', 'W18', DmNFe_0000.QrNFE?vServ.Value, Valor) then
cXML.InfNFe.?.vServ := Valor;
  if Def('344', 'W19', DmNFe_0000.QrNFE?vBC.Value, Valor) then
cXML.InfNFe.?.vBC := Valor;
  if Def('345', 'W20', DmNFe_0000.QrNFE?vISS.Value, Valor) then
cXML.InfNFe.?.vISS := Valor;
  if Def('346', 'W21', DmNFe_0000.QrNFE?vPIS.Value, Valor) then
cXML.InfNFe.?.vPIS := Valor;
  if Def('347', 'W22', DmNFe_0000.QrNFE?vCOFINS.Value, Valor) then
cXML.InfNFe.?.vCOFINS := Valor;
  if Def('347a', 'W22a', DmNFe_0000.QrNFE?dCompet.Value, Valor) then
cXML.InfNFe.?.dCompet := Valor;
  if Def('347b', 'W22b', DmNFe_0000.QrNFE?vDeducao.Value, Valor) then
cXML.InfNFe.?.vDeducao := Valor;
  if Def('347c', 'W22c', DmNFe_0000.QrNFE?vOutro.Value, Valor) then
cXML.InfNFe.?.vOutro := Valor;
  if Def('347d', 'W22d', DmNFe_0000.QrNFE?vDescIncond.Value, Valor) then
cXML.InfNFe.?.vDescIncond := Valor;
  if Def('347e', 'W22e', DmNFe_0000.QrNFE?vDescCond.Value, Valor) then
cXML.InfNFe.?.vDescCond := Valor;
  if Def('347f', 'W22f', DmNFe_0000.QrNFE?vISSRet.Value, Valor) then
cXML.InfNFe.?.vISSRet := Valor;
  if Def('347g', 'W22g', DmNFe_0000.QrNFE?cRegTrib.Value, Valor) then
cXML.InfNFe.?.cRegTrib := Valor;

*)
  (* TAG  RETTRIB... Opcional *)
  (*cXML.InfNFe.Total.RetTrib.VRetPIS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCOFINS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCSLL := Valor;
  cXML.InfNFe.Total.RetTrib.VBCIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VBCRetPrev := Valor;
  cXML.InfNFe.Total.RetTrib.VRetPrev := Valor;*)

(*
  if Def('349', 'W24', DmNFe_0000.QrNFE?vRetPIS.Value, Valor) then
cXML.InfNFe.?.vRetPIS := Valor;
  if Def('350', 'W25', DmNFe_0000.QrNFE?vRetCOFINS.Value, Valor) then
cXML.InfNFe.?.vRetCOFINS := Valor;
  if Def('351', 'W26', DmNFe_0000.QrNFE?vRetCSLL.Value, Valor) then
cXML.InfNFe.?.vRetCSLL := Valor;
  if Def('352', 'W27', DmNFe_0000.QrNFE?vBCIRRF.Value, Valor) then
cXML.InfNFe.?.vBCIRRF := Valor;
  if Def('353', 'W28', DmNFe_0000.QrNFE?vIRRF.Value, Valor) then
cXML.InfNFe.?.vIRRF := Valor;
  if Def('354', 'W29', DmNFe_0000.QrNFE?vBCRetPrev.Value, Valor) then
cXML.InfNFe.?.vBCRetPrev := Valor;
  if Def('355', 'W30', DmNFe_0000.QrNFE?vRetPrev.Value, Valor) then
cXML.InfNFe.?.vRetPrev := Valor;
*)

(* X - Informa��es da TAG TRANSP... *)
      // '356', 'X01'  =  Grupo de informa��oes do transporte da NF-e
  if Def('357', 'X02', DmNFe_0000.QrNFECabAModFrete.Value, Valor) then
    cXML.InfNFe.Transp.ModFrete := Valor;
      // '358', 'X03'  =  Grupo do transportador
  if Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value) <> '' then
  begin
    if Def('359', 'X04', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CNPJ := Valor;
  end else
  begin
    if Def('360', 'X05', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CPF.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CPF := Valor;
  end;
  if Def('361', 'X06', DmNFe_0000.QrNFECabATransporta_XNome.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XNome := Valor;
  if Def('362', 'X07', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_IE.Value), Valor) then
    cXML.InfNFe.Transp.Transporta.IE := FMT_IE(Valor);
  if Def('363', 'X08', DmNFe_0000.QrNFECabATransporta_XEnder.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XEnder := Valor;
  if Def('364', 'X09', DmNFe_0000.QrNFECabATransporta_XMun.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XMun := Valor;
  if Def('365', 'X10', DmNFe_0000.QrNFECabATransporta_UF.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.UF := Valor;
  //     '366', 'X11'  = Grupo de retenc��o do ICMS do transporte
  (*
      PAREI AQUI
  if Def('367', 'X12', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VServ := Valor;
  if Def('368', 'X13', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VBCRet := Valor;
  if Def('369', 'X14', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.PICMSRet := Valor;
  if Def('370', 'X15', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VICMSRet := Valor;
  if Def('371', 'X16', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CFOP := Valor;
  if Def('372', 'X17', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CMunFG := Valor;*)

  (* Informa��es da TAG VEICTRANSP... Opcional *)

  //     '373', 'X18'  =  Grupo Ve�culo
  if (Trim(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value) <> '')
  and (Trim(DmNFe_0000.QrNFECabAVeicTransp_UF.Value) <> '') then
  begin
    if Def('374', 'X19', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value), Valor) then
      cXML.InfNFe.Transp.VeicTransp.Placa := Valor;
    if Def('375', 'X20', DmNFe_0000.QrNFECabAVeicTransp_UF.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.UF := Valor;
    if Def('376', 'X21', DmNFe_0000.QrNFECabAVeicTransp_RNTC.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.RNTC := Valor;

    //     '377', 'X22'  =  reboque(s)
(*
    DmNFe_0000.QrNFECabXReb.Close;
    DmNFe_0000.QrNFECabXReb.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFECabXReb.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFECabXReb.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFECabXReb. O p e n ;
*)
    DmNFe_0000.ReopenNFeCabXReb(FatID, FatNum, Empresa);
    //
    if DmNFe_0000.QrNFECabXReb.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFECabXReb.Eof do
      begin
        cRebLista := cXML.InfNFe.Transp.Reboque.Add;
        if Def('378', 'X23', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabXRebplaca.Value), Valor) then
          cRebLista.Placa := Valor;
        if Def('379', 'X24', DmNFe_0000.QrNFECabXRebUF.Value, Valor) then
          cRebLista.UF := Valor;
        if Def('380', 'X25', DmNFe_0000.QrNFECabXRebRNTC.Value, Valor) then
          cRebLista.RNTC := Valor;
        //
        DmNFe_0000.QrNFECabXReb.Next;
      end;
    end;
  end;
  if Def('380a', 'X25a', DmNFe_0000.QrNFECabAVagao.Value, Valor) then
    cXML.InfNFe.Transp.Vagao := Valor;
  if Def('380b', 'X25b', DmNFe_0000.QrNFECabABalsa.Value, Valor) then
    cXML.InfNFe.Transp.Balsa := Valor;
  //
(*
  DmNFe_0000.QrNFECabXVol.Close;
  DmNFe_0000.QrNFECabXVol.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabXVol.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabXVol.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabXVol. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabXVol(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabXVol.RecordCount > 0 then
  begin
    K := -1;
    //if DmNFe_0000.QrNFECabXVolqVol.Value > 0 then
    while not DmNFe_0000.QrNFECabXVol.Eof do
    begin
      //K := 1; (* Essa vari�vel tem que ser incrementada no caso do Valume possuir mais de um lacre...*)

      (* Informa��es da TAG VOLUMES... Opcional *)
      //     '381', 'X26'  =  Grupo volumes
      cVolLista := cXML.InfNFe.Transp.Vol.Add;
      K := K + 1;
      if Def('382', 'X27', DmNFe_0000.QrNFECabXVolqVol.Value, Valor) then
        cVolLista.QVol := Valor;
      if Def('383', 'X28', DmNFe_0000.QrNFECabXVolesp.Value, Valor) then
        cVolLista.Esp := Valor;
      if Def('384', 'X29', DmNFe_0000.QrNFECabXVolmarca.Value, Valor) then
        cVolLista.Marca := Valor;
      if Def('385', 'X30', DmNFe_0000.QrNFECabXVolnVol.Value, Valor) then
        cVolLista.NVol := Valor;
      if Def('386', 'X31', DmNFe_0000.QrNFECabXVolpesoL.Value, Valor) then
        cVolLista.PesoL := Valor;
      if Def('387', 'X32', DmNFe_0000.QrNFECabXVolpesoB.Value, Valor) then
        cVolLista.PesoB := Valor;

      (* Informa��es da TAG LACRES... Opcional *)
      //  '387a', 'X33'
(*
      DmNFe_0000.QrNFECabXLac.Close;
      DmNFe_0000.QrNFECabXLac.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFECabXLac.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFECabXLac.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFECabXLac.Params[03].AsInteger := DmNFe_0000.QrNFECabXVolControle.Value;
      DmNFe_0000.QrNFECabXLac. O p e n ;
*)
      Controle := DmNFe_0000.QrNFECabXVolControle.Value;
      DmNFe_0000.ReopenNFeCabXLac(FatID, FatNum, Empresa, Controle);
      //
      //if DmNFe_0000.QrNFECabXLac.RecordCount > 0 then
      while not DmNFe_0000.QrNFECabXLac.Eof do
      begin
        cLacLista := cXML.InfNFe.Transp.Vol.Items[K].Lacres.Add;
        if Def('388', 'X34', DmNFe_0000.QrNFECabXLacnLacre.Value, Valor) then
          cLacLista.NLacre := Valor;
        //
        DmNFe_0000.QrNFECabXLac.Next;
      end;

      DmNFe_0000.QrNFECabXVol.Next;
    end;
  end;

(*
  DmNFe_0000.QrNFECabY.Close;
  DmNFe_0000.QrNFECabY.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabY.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabY.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabY. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabY(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabY.RecordCount > 0 then
  begin
    (* Y - Informa��es da TAG COBR... se Houver *)
    //   //'389', 'Y01'  =  Grupo de cobran�a
    //   //'390', 'Y02'  =  Grupo da fatura
    if Def('391', 'Y03', DmNFe_0000.QrNFECabACobr_Fat_NFat  .Value, Valor) then
      cXML.InfNFe.Cobr.Fat.NFat := Valor;
    if Def('392', 'Y04', DmNFe_0000.QrNFECabACobr_Fat_vOrig.Value, Valor) then
      cXML.InfNFe.Cobr.Fat.VOrig := Valor;
    if Def('393', 'Y05', DmNFe_0000.QrNFECabACobr_Fat_vDesc.Value, Valor) then
      cXML.InfNFe.Cobr.Fat.VDesc := Valor;
    if Def('394', 'Y06', DmNFe_0000.QrNFECabACobr_Fat_vLiq.Value, Valor) then
      cXML.InfNFe.Cobr.Fat.VLiq := Valor;

    (* Informa��es da TAG DUP...*)
    //   '395', 'Y07'  =  Grupo da duplicata
    //

    while not DmNFe_0000.QrNFECabY.Eof do
    begin
      if DmNFe_0000.QrNFECabYvDup.Value > 0 then
      begin
        cDupLista := cXML.InfNFe.Cobr.Dup.Add;
        //
        if Def('396', 'Y08', DmNFe_0000.QrNFECabYFatParcela.Value, Valor) then
          cDupLista.NDup := Valor;
        if Def('397', 'Y09', DmNFe_0000.QrNFECabYdVenc.Value, Valor) then
          cDupLista.DVenc := Valor;
        if Def('398', 'Y10', DmNFe_0000.QrNFECabYvDup.Value, Valor) then
          cDupLista.VDup := Valor;
      end;
      DmNFe_0000.QrNFECabY.Next;
    end;
  end;
//------------------------------------------------------------------------------
  //Geral.MB_Info('398a  YA01  pag - Grupo de Formas de Pagamento -
//------------------------------------------------------------------------------
// 398.10 YA01a detPag Grupo Detalhamento do Pagamento G YA01 1-100
//------------------------------------------------------------------------------
  // Grupo obrigat�rio para a NFC-e, a crit�rio da UF. N�o informar para a NF-e.');
  DmNFe_0000.ReopenNFeCabYA(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabYA.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabYA.Eof do
    begin
      cPagLista := cXML.InfNFe.Pag.DetPag.Add;
      //
//******************************************************************************
{     2021-03-09 esquecido de implementar em 2016?
      // YA01b NFe 4.00
      if Def('398.11', 'YA01b', DmNFe_0000.QrNFECabYAindPag.Value, Valor) then
        cPagLista.IndPag := Valor;
}
//******************************************************************************
      if Def('398.12', 'YA02', DmNFe_0000.QrNFECabYAtPag.Value, Valor) then
        cPagLista.TPag := Valor;
      if Def('398.13', 'YA03', DmNFe_0000.QrNFECabYAvPag.Value, Valor) then
        cPagLista.vPag := Valor;
      //
      // 398.20 YA04 card Grupo de Cart�es G YA01a 0-1
      //
      cPagCard := cPagLista.Card;
      //
      if Def('398.21', 'YA04a', DmNFe_0000.QrNFECabYAtpIntegra.Value, Valor) then
        cPagCard.tpIntegra := Valor;
      if Def('398.22', 'YA05', DmNFe_0000.QrNFECabYACNPJ.Value, Valor) then
        cPagCard.CNPJ := Valor;
      if Def('398.23', 'YA06', DmNFe_0000.QrNFECabYAtBand.Value, Valor) then
        cPagCard.tBand := Valor;
      if Def('398.24', 'YA07', DmNFe_0000.QrNFECabYAcAut.Value, Valor) then
        cPagCard.cAut := Valor;
      //
      if Def('398.25', 'YA09', DmNFe_0000.QrNFECabYAvTroco.Value, Valor) then
        cXML.InfNFe.Pag.VTroco := Valor;
      //
      DmNFe_0000.QrNFECabYA.Next;
    end;
  end;

// ini 2021-03-09
//------------------------------------------------------------------------------
// Grupo YB. Informa��es do Intermediador da Transa��o
//------------------------------------------------------------------------------
  //
  //  398.26 YB01 infIntermed Grupo de Informa��es do Intermediador da Transa��o G A01 0-1
  //Obrigat�rio o preenchimento do Grupo de Informa��es do Intermediador da Transa��o nos
  //casos de �opera��o n�o presencial pela internet em site de terceiros (intermediadores)
  //(Inclu�do na NT2020.006)
  //
  if DmNFe_0000.QrNFECabAInfIntermed_CNPJ.Value <> EmptyStr then
  begin
    if Def('398.27', 'YB02', DmNFe_0000.QrNFECabAInfIntermed_CNPJ.Value, Valor) then
      cXML.InfNFe.InfIntermed.CNPJ := Valor;
    if Def('398.28', 'YB03', DmNFe_0000.QrNFECabAInfIntermed_idCadIntTran.Value, Valor) then
      cXML.InfNFe.InfIntermed.IdCadIntTran := Valor;
  end;
// fim 2021-03-09

(* Z - Informa��es da TAG INFADIC... se Houver *)
  //     '399', 'Z01' Grupo de informa��es adicionais
  if Def('400', 'Z02', DmNFe_0000.QrNFECabAInfAdic_InfAdFisco.Value, Valor) then
    cXML.InfNFe.InfAdic.InfAdFisco := Valor;
  //
  Valor := '';
  if Trim(DmNFe_0000.QrNFECabAInfAdic_InfCpl.Value) <> '' then
  Valor := DmNFe_0000.QrNFECabAInfAdic_InfCpl.Value;
  if Trim(DmNFe_0000.QrNFECabAInfCpl_totTrib.Value) <> '' then
    Valor := Trim(Valor + ' ' + DmNFe_0000.QrNFECabAInfCpl_totTrib.Value);
  if Def('401', 'Z03', Valor, Valor) then
  begin
    cXML.InfNFe.InfAdic.InfCpl := Valor;
  end;

  (* Informa��es da TAG ObsCont... *)
  //     '401a', 'Z04' Grupo do campo de uso livre do contribuinte
  DmNFe_0000.ReopenNFeCabZCon(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZCon.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZCon.Eof do
    begin
      cObsCont := cXML.InfNFe.InfAdic.ObsCont_nota.Add;
      //
      if Def('401b', 'Z05', DmNFe_0000.QrNFECabZConxCampo.Value, Valor) then
        cObsCont.XCampo := Valor;
      if Def('401c', 'Z06', DmNFe_0000.QrNFECabZConxTexto.Value, Valor) then
        cObsCont.XTexto := Valor;
      //
      DmNFe_0000.QrNFECabZCon.Next;
    end;
  end;
  (* Informa��es da TAG ObsFisco... *)
  // '401d', 'Z07' Grupo Campo de uso livre do fisco
  DmNFe_0000.ReopenNFeCabZFis(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZFis.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZFis.Eof do
    begin
      cObsFisco := cXML.InfNFe.InfAdic.ObsFisco_nota.Add;
      //
      if Def('401e', 'Z08', DmNFe_0000.QrNFECabZFisxCampo.Value, Valor) then
        cObsFisco.XCampo := Valor;
      if Def('401f', 'Z09', DmNFe_0000.QrNFECabZFisxTexto.Value, Valor) then
        cObsFisco.XTexto := Valor;
      //
      DmNFe_0000.QrNFECabZFis.Next;
    end;
  end;

  (* Informa��es da TAG ProcRef... *)
  // '401g', 'Z10'  =  Grupo de processo referenciado
  DmNFe_0000.ReopenNFeCabZPro(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZPro.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZPro.Eof do
    begin
      cProcRefLista := cXML.InfNFe.InfAdic.ProcRef.Add;
      //
      if Def('401h', 'Z11', DmNFe_0000.QrNFECabZPronProc.Value, Valor) then
        cProcRefLista.nProc := Valor;
      if Def('401i', 'Z12', DmNFe_0000.QrNFECabZProindProc.Value, Valor) then
        cProcRefLista.indProc := Valor;
      if DmNFe_0000.QrNFECabZProtpAto.Value > -1 then
        if Def('401j', 'Z13', DmNFe_0000.QrNFECabZProtpAto.Value, Valor) then
          cProcRefLista.tpAto := Valor;
      //
      DmNFe_0000.QrNFECabZPro.Next;
    end;
  end;

  (* ZA - Informa��es da TAG EXPORTA... se Houver *)
  //  '402', 'ZA01'
  if (Trim(DmNFe_0000.QrNFECabAExporta_UFEmbarq.Value) <> '')
  or (Trim(DmNFe_0000.QrNFECabAExporta_XLocEmbarq.Value) <> '') then
  begin
    if Def('403', 'ZA02', DmNFe_0000.QrNFECabAExporta_UFEmbarq.Value, Valor) then
      cXML.InfNFe.Exporta.UFSaidaPais := Valor;
    if Def('404', 'ZA03', DmNFe_0000.QrNFECabAExporta_XLocEmbarq.Value, Valor) then
      cXML.InfNFe.Exporta.XLocExporta := Valor;
    if Def('404a', 'ZA04', DmNFe_0000.QrNFECabAExporta_XLocDespacho.Value, Valor) then
      cXML.InfNFe.Exporta.XLocDespacho := Valor;
  end;
  (* ZB - Informa��es da TAG COMPRA... se Houver *)
  // '405', 'ZB01'
  // Habilitado em 2011-09-18
  //2016/01/15 17:11 => erro desconhecido est� fazendo mesmo com tag vazia
  if Trim(DmNFe_0000.QrNFECabACompra_XNEmp.Value +
    DmNFe_0000.QrNFECabACompra_XPed.Value +
    DmNFe_0000.QrNFECabACompra_XCont.Value) <> '' then
  begin
    if Def('406', 'ZB02', DmNFe_0000.QrNFECabACompra_XNEmp.Value, Valor) then
      cXML.InfNFe.Compra.XNEmp := Valor;
    if Def('407', 'ZB03', DmNFe_0000.QrNFECabACompra_XPed.Value, Valor) then
      cXML.InfNFe.Compra.XPed := Valor;
    if Def('408', 'ZB04', DmNFe_0000.QrNFECabACompra_XCont.Value, Valor) then
      cXML.InfNFe.Compra.XCont := Valor;
  end;
  // Fim 2011-09-18
  //

  (* ZC - Informa��es da assinatura digital
  // '409', 'ZC01'  -  Assinatura XML da NF-e  segundo o padr�o XML Digital signature
  *)

  (*3.5 Grupo ZD. Informa��es do Respons�vel T�cnico
    Novo grupo criado para Erro! NFe 4.00 NT 2018/5
  //ID Campo Descri��o Ele Pai Tipo Ocor. Tam. Observa��o
  ZD01 infRespTec Informa��es do Respons�vel T�cnico pela emiss�o do DF-e G A01 0-1
  Grupo para informa��es do respons�vel t�cnico pelo sistema de emiss�o do DF-e
  ZD02 CNPJ CNPJ da pessoa jur�dica respons�vel pelo sistema utilizado na emiss�o do documento fiscal eletr�nico E ZD01 N 1-1 14 Informar o CNPJ da pessoa jur�dica respons�vel pelo sistema utilizado na emiss�o do documento fiscal eletr�nico.
  ZD04 xContato Nome da pessoa a ser contatada E ZD01 C 1-1 2-60 Informar o nome da pessoa a ser contatada na empresa desenvolvedora do sistema utilizado na emiss�o do documento fiscal eletr�nico.
  ZD05 email E-mail da pessoa jur�dica a ser contatada E ZD01 C 1-1 6-60 Informar o e-mail da pessoa a ser contatada na empresa desenvolvedora do sistema.
  ZD06 fone Telefone da pessoa jur�dica/f�sica a ser contatada E ZD01 N 1-1 6-14 Informar o telefone da pessoa a ser contatada na empresa desenvolvedora do sistema. Preencher com o C�digo DDD + n�mero do telefone.
  ZD07 -x- Sequ�ncia XML G ZD01 0-1 Grupo de informa��es do C�digo de Seguran�a do Respons�vel T�cnico - CSTR
  ZD08 idCSRT Identificador do CSRT E ZD07 N 1-1 2 Identificador do CSRT utilizado para montar o hash do CSRT
  ZD09 hashCSRT Hash do CSRT E ZD07 C 1-1 28 O hashCSRT � o resultado da fun��o hash (SHA-1 � Base64) do CSRT fornecido pelo fisco mais a Chave de Acesso da NFe.
*)
(*
  if Def('410', 'ZC02', DmNFe_0000.QrNFE?safra.Value, Valor) then
cXML.InfNFe.?.safra := Valor;
  if Def('411', 'ZC03', DmNFe_0000.QrNFE?ref.Value, Valor) then
cXML.InfNFe.?.ref := Valor;
  if Def('414', 'ZC06', DmNFe_0000.QrNFE?qtde.Value, Valor) then
cXML.InfNFe.?.qtde := Valor;
  if Def('415', 'ZC07', DmNFe_0000.QrNFE?qTotMes.Value, Valor) then
cXML.InfNFe.?.qTotMes := Valor;
  if Def('416', 'ZC08', DmNFe_0000.QrNFE?qTotAnt.Value, Valor) then
cXML.InfNFe.?.qTotAnt := Valor;
  if Def('417', 'ZC09', DmNFe_0000.QrNFE?qTotGer.Value, Valor) then
cXML.InfNFe.?.qTotGer := Valor;
  if Def('419', 'ZC11', DmNFe_0000.QrNFE?xDed.Value, Valor) then
cXML.InfNFe.?.xDed := Valor;
  if Def('420', 'ZC12', DmNFe_0000.QrNFE?vDed.Value, Valor) then
cXML.InfNFe.?.vDed := Valor;
  if Def('421', 'ZC13', DmNFe_0000.QrNFE?vFor.Value, Valor) then
cXML.InfNFe.?.vFor := Valor;
  if Def('422', 'ZC14', DmNFe_0000.QrNFE?vTotDed.Value, Valor) then
cXML.InfNFe.?.vTotDed := Valor;
  if Def('423', 'ZC15', DmNFe_0000.QrNFE?vLiqFor.Value, Valor) then
cXML.InfNFe.?.vLiqFor := Valor;
*)
  if VAR_NT2018_05v120 then
  begin
    if Def('423b', 'ZD02', DModG.QrPrmsEmpNFeinfRespTec_CNPJ.Value, Valor) then
        cXML.InfNFe.InfRespTec.CNPJ := Valor;
    if Def('423c', 'ZD04', DModG.QrPrmsEmpNFeinfRespTec_xContato.Value, Valor) then
        cXML.InfNFe.InfRespTec.XContato := Valor;
    if Def('423d', 'ZD05', DModG.QrPrmsEmpNFeinfRespTec_email.Value, Valor) then
        cXML.InfNFe.InfRespTec.Email := Valor;
    if Def('423e', 'ZD06', DModG.QrPrmsEmpNFeinfRespTec_fone.Value, Valor) then
        cXML.InfNFe.InfRespTec.Fone := Valor;
    //
    //'423f', 'ZD07' -x- Sequ�ncia XML G ZD01 0-1 Grupo de informa��es do C�digo de Seguran�a do Respons�vel T�cnico - CSTR
    // Liberar s� quando nova nota tecnica obrigar!
    if (Geral.IMV(DModG.QrPrmsEmpNFeinfRespTec_idCSRT.Value) > 0)
    and (DModG.QrPrmsEmpNFeinfRespTec_CSRT.Value <> '') then
    begin
      if Def('423g', 'ZD08', DModG.QrPrmsEmpNFeinfRespTec_idCSRT.Value, Valor) then
          cXML.InfNFe.InfRespTec.IdCSRT := Valor;
      CSRT := UnNFe_PF.HashCSRT(DmodG.QrParamsEmpCodigo.Value, nfeID);
      if Def('423h', 'ZD09', CSRT, Valor) then
          //cXML.InfNFe.InfRespTec.HashCSRT := Valor;
          cXML.InfNFe.InfRespTec.HashCSRT := CSRT;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
///  Grupo ZX. Informa��es Suplementares da Nota Fiscal
///
///  � feito depois!!!! l� na assinatura!
///
////////////////////////////////////////////////////////////////////////////////
(*
  if Def('425', 'ZX02', DmNFe_0000.QrNFE?qrCode.Value, Valor) then
cXML.InfNFe.?.qrCode := Valor;
  if Def('426', 'ZX03', DmNFe_0000.QrNFE?urlChave.Value, Valor) then
cXML.InfNFe.?.urlChave := Valor;
*)
////////////////////////////////////////////////////////////////////////////////

  //
  Result := True;
end;

function TFmNFeGeraXML_0400.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ,
  Modelo, Serie, nNFIni, nNFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + Ano + emitCNPJ + Modelo + StrZero(StrToInt(Serie),3,0) +
    StrZero(StrToInt(nNFIni),9,0) + StrZero(StrToInt(nNFFim),9,0);
  K := Length(Id);
  if K = 43 then
    Result := True
  else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 43 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmNFeGeraXML_0400.GerarLoteEvento(Lote, Empresa: Integer;
  out PathLote: String; out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I: Integer;
  XML_STR: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeEveRCab.Close;
  DmNFe_0000.QrNFeEveRCab.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeEveRCab, Dmod.MyDB, 'TFmNFeGeraXML_0400.GerarLoteEvento()');
  //
  if DmNFe_0000.QrNFeEveRCab.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem eventos para serem enviados!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML de eventos', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_EVE_ENV_LOT_XML;

  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?><envEvento versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe"><idLote>' +
    Geral.FFN(Lote, 15)  + '</idLote>';

  DmNFe_0000.QrNFeEveRCab.First;
  while not DmNFe_0000.QrNFeEveRCab.Eof do
  begin
    case DmNFe_0000.QrNFeEveRCabtpEvento.Value of
      // Carta de corre��o
      NFe_CodEventoCCe: // 110110
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedCCe.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_CCE_XML;
      end;
      // Cancelamento de NFe
      NFe_CodEventoCan: // 110111
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedCan.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_CAN_XML;
      end;
      // EPEC - Evento Pr�vio de Emiss�o em Conting�ncia
      NFe_CodEventoEPEC: // 110140
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedEPEC.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_EPEC_XML;
      end;
      // Manifesta��o do destinat�rio
      NFe_CodEventoMDeConfirmacao, // = 210200;
      NFe_CodEventoMDeCiencia    , // = 210210;
      NFe_CodEventoMDeDesconhece , // = 210220;
      NFe_CodEventoMDeNaoRealizou: // = 210240;
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedMDe.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_MDE_XML;
      end
      else
      begin
        XMLArq := '### ERRO ###';
        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' + sLineBreak  +
        'TFmNFeGeraXML_0400.GerarLoteEvento()');
        Result := False;
        Exit;
      end;
    end;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      if Pos('<Evento', MeuXMLAssinado) > 0 then
        buf := Copy(MeuXMLAssinado, Pos('<Evento', MeuXMLAssinado), Length(MeuXMLAssinado))
      else
        buf := Copy(MeuXMLAssinado, Pos('<evento', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      mTexto.Free;
    end else
    begin
      Geral.MB_Aviso('Arquivo n�o encontrado: ' + sLineBreak  + XMLArq);
      Exit;
    end;
    DmNFe_0000.QrNFeEveRCab.Next;
  end;
  XML_Lote := XML_Lote + '</envEvento>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;

  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

procedure TFmNFeGeraXML_0400.ConfiguraReqResp(ReqResp: THTTPReqResp);
begin
  // ver!!! Delphi Alexandria
  ReqResp.OnBeforePost := OnBeforePost;
  // ver!!! Delphi Alexandria
end;

function TFmNFeGeraXML_0400.CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
  GravaCampos, Cliente: Integer): Boolean;
var
  strChaveAcesso: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM nfexmli WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := FatID;
  Dmod.QrUpd.Params[01].AsInteger := FatNum;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  FOrdem   := 0;
  //
  DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetNFe(arqXML);
////////////////////////////////////////////////////////////////////////////////
// Aten��o!/Aten��o!/Aten��o!/Aten��o!/Aten��o!/Aten��o!/Aten��o!/Aten��o!//////
////////////////////////////////////////////////////////////////////////////////
//// Quando crian novo nfe_vxx precisa adicionar o c�digo abaixo no arquivo! ///
////////////////////////////////////////////////////////////////////////////////
(*
{ Global Functions }

function GetNFe(Doc: IXMLDocument): IXMLTNFe;
function LoadNFe(const FileName: WideString): IXMLTNFe;
function NewNFe: IXMLTNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetNFe(Doc: IXMLDocument): IXMLTNFe;
begin
  Result := Doc.GetDocBinding('NFe', TXMLTNFe, TargetNamespace) as IXMLTNFe;
end;

function LoadNFe(const FileName: WideString): IXMLTNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('NFe', TXMLTNFe, TargetNamespace) as IXMLTNFe;
end;

function NewNFe: IXMLTNFe;
begin
  Result := NewXMLDocument.GetDocBinding('NFe', TXMLTNFe, TargetNamespace) as IXMLTNFe;
end;
*)
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  }

  SerieNF := FormatFloat('000', DmNFe_0000.QrNFECabAide_Serie.Value);
  NumeroNF := FormatFloat('00000000', DmNFe_0000.QrNFECabAide_cNF.Value); (* C�digo Aleat�rio que ir� compor a Chave de Acesso...*)

  (* Montar a Chave de Acesso da NFe de acordo com as informa��es do Registro...*)
  (* cUF=??,dEmi=...*)
  strChaveAcesso := DmNFe_0000.QrNFECabAId.Value;

  CDV := Copy(strChaveAcesso, Length(strChaveAcesso), Length(strChaveAcesso));
  if GerarXMLdaNFe(FatID, FatNum, Empresa, 'NFe' + strChaveAcesso, GravaCampos, Cliente) then
  begin
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    DmNFe_0000.SalvaXML(NFE_EXT_NFE_XML, strChaveAcesso, arqXML.XML.Text, nil, False);
    //
    Result := True;
  end;
  arqXML := nil;
end;

function TFmNFeGeraXML_0400.WS_NFeConsultaCadastro(Servico_UF, Contribuinte_UF,
  Contribuinte_CNPJ: String; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
const
  TipoConsumo = tcwsNfeConsultaCadastro;
  Ambiente = 1; // Produ��o
var
  Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso, VersaoDados: String;
  CodigoUF: Integer;
begin
  Screen.Cursor := crHourGlass;
  CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Servico_UF);
  //
  //if not ObtemWebServer(Servico_UF, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, Ambiente, CodigoUF, TipoConsumo, sAviso, Modelo) then Exit;
  FDadosTxt := XML_ConsCad(Contribuinte_UF, Contribuinte_CNPJ);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
  //VersaoDados := '2.00';
  ProcedimentoEnv := 'CadConsultaCadastro4'; //'CadConsultaCadastro2';
  ProcedimentoRet := 'nfeResultMsg';  // 'consultaCadastro2Result'

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsCad_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Cadastro Contribuinte:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.WS_NFeConsultaDistDFeInt(Servico_UF,
  Interesse_UF: String; TpAmb: Integer; CNPJ, CPF, ultNSU, NSU,
  Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit; ItemStr: String): String;
const
  TipoConsumo = tcwsDistDFeInt;
  Modelo = 55;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso: String;
  CodigoUF: Integer;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;
  //CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Interesse_UF);
  //
  //if not ObtemWebServer(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, Modelo) then Exit;
  //
  FDadosTxt := XML_DistDFEInt(TpAmb, Interesse_UF, CNPJ, CPF, ultNSU, NSU);
  //
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  //
  DmNFe_0000.SalvaXML(NFE_EXT_PED_DFE_DIS_INT_XML, ItemStr, FDadosTxt, (*RETxtEnvio*)nil, False);
  //
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto  + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto  +   '<soap12:Body>';
  Texto := Texto  +    '<nfeDistDFeInteresse xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe">';
  Texto := Texto  +      '<nfeDadosMsg>' + FDadosTxt + '</nfeDadosMsg>';
  Texto := Texto  +    '</nfeDistDFeInteresse>';
  Texto := Texto  +  '</soap12:Body>';
  Texto := Texto  + '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe';
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe/nfeDistDFeInteresse';
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString,'nfeDistDFeInteresseResult');
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Distribui��o DFe de Interesse:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.WS_NFeConsultaNF(UFServico: String; Ambiente,
  CodigoUF: Byte; ChaveNFe: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
const
  TipoConsumo = tcwsNfeConsultaProtocolo;
var
  sAviso, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConNFe.Value, 2, siNegativo);

  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso,
  Modelo) then Exit;
  //
  FDadosTxt := XML_ConsSitNFe(Ambiente, ChaveNFe, VersaoDados);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;


  if Pos('NFECONSULTA2', UpperCase(FURL)) > 0 then
  begin
    ProcedimentoEnv := 'NfeConsulta2';
    ProcedimentoRet := 'nfeConsultaNF2Result';
  end else
  if Pos('NFECONSULTA3', UpperCase(FURL)) > 0 then
  begin
    ProcedimentoEnv := 'NfeConsulta3';
    ProcedimentoRet := 'nfeConsultaNFResult';
  end else
  begin
    ProcedimentoEnv := 'nfeConsulta';
    ProcedimentoRet := 'nfeResultMsg';
  end;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';

 //TODO :       VER AQUI SE NAO FUNCIONAR CONSULTA NFE!
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
       RETxtEnvio.Text :=  Acao.Text;
       EdWS.Text := FURL;
       ReqResp.Execute(Acao.Text, Stream);
       StrStream := TStringStream.Create('');
       StrStream.CopyFrom(Stream, 0);
       Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.Def(const Codigo, ID: String; const Source: Variant;
  var Dest: String; Forca: Boolean = False): Boolean;
var
  Continua, Avisa: Boolean;
  NFeVersao: Double;
  DataIni: TDateTime;
begin
  Result := False;
  Dest   := '';
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    // ini 2021-03-10
    //if DmNFe_0000.QrNFeLayIOcorMin.Value > 0 then
    if DmNFe_0000.QrNFECabAide_tpAmb.Value = 1 then // 1-Produ��o/ 2-Homologa��o
      DataIni := DmNFe_0000.QrNFeLayIDtIniProd.Value
    else
      DataIni := DmNFe_0000.QrNFeLayIDtIniHomo.Value;
    if FHoje >= DataIni then
    begin
      if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) or (Forca = True) then
      // fim 2021-03-10
      begin
        if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
          Continua := False else
        if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
          Continua := False else
          Continua := True;
      end else
        Continua := XXe_PF.VarTypeValido(Source, Codigo + '  ' + ID);
    end else
    begin
      Result := False;
      Continua := False;
    end;
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'N' then
      begin
        // Texto
        if ((VarType(Source) = varString) or (VarType(Source) = varStrArg) or
          (VarType(Source) =  varUString) or (VarType(Source) =  varOleStr))
          and (Length(Source) > 18) then
        begin
          Dest := Source
        end
        // 2019-03-29 - Desabilitado. N�o existe mais!
        (*
        else if (Codigo = '17') and (ID = 'B13') then
          Dest := Source
        *)
        // Fim 2019-03-29
        // Double
        else if DmNFe_0000.QrNFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source, ffFixed, 15,
            DmNFe_0000.QrNFeLayIDeciCasas.Value))
        else begin
        //integer
          if DmNFe_0000.QrNFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmNFe_0000.QrNFeLayITamMax.Value, siPositivo))
          else
          begin
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source));
            // 2019-03-29
            while Length(Dest) < DmNFe_0000.QrNFeLayILeftZeros.Value do
              Dest := '0' + Dest;
            // Fim 2019-03-29
          end;
        end;
      end else
      // data
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'D' then
      begin
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source);
        end else
        begin
          if DmNFe_0000.QrNFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmNFe_0000.QrNFeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source);
        end;
      end else
      // hora
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source)
      else
      // texto
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'C' then
        Dest := Source
      else
      // desconhecido
      begin
        Dest := Source;
        Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Codigo, ID));
      if (Dest = '') and (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmNFe_0000.QrNFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
          if (Codigo = '78') and (ID = 'E17') and
          ((DmNFe_0000.QrNFECabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabAdest_CNPJ.Value) = '')) then
            Avisa := False;
          if Avisa then
            Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Codigo, ID, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else
    Geral.MB_Erro(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
      'N�o foi poss�vel definir o valor', 3));
end;

function TFmNFeGeraXML_0400.Def_UTC(const Codigo, ID: String; const Source1,
  Source2, Source3: Variant; var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  Result := False;
  Dest   := '';
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if DmNFe_0000.QrNFeLayIOcorMin.Value > 0 then
    begin
      if (Codigo = '255') and (ID = 'O11') and (Source1 = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source1 = 0) then
        Continua := False else
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source1, Codigo + '  ' + ID);
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'N' then
      begin
        // Texto
        if (Codigo = '17') and (ID = 'B13') then
          Dest := Source1
        // Double
        else if DmNFe_0000.QrNFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source1, ffFixed, 15,
            DmNFe_0000.QrNFeLayIDeciCasas.Value))
        else begin
        //integer
          if DmNFe_0000.QrNFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source1),
            DmNFe_0000.QrNFeLayITamMax.Value, siPositivo))
          else
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source1));
        end;
      end else
      // data
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'D' then
      begin
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2);
        end else
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SSTZD' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2) +
                  dmkPF.TZD_UTC_FloatToSignedStr(Source3);
        end else
        begin
          if DmNFe_0000.QrNFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmNFe_0000.QrNFeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source1);
        end;
      end else
      // hora
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source1)
      else
      // texto
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'C' then
        Dest := Source1
      else
      // desconhecido
      begin
        Dest := Source1;
        Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Codigo, ID));
      if (Dest = '') and (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmNFe_0000.QrNFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
          if (Codigo = '78') and (ID = 'E17') and
          ((DmNFe_0000.QrNFECabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabAdest_CNPJ.Value) = '')) then
            Avisa := False;
          if Avisa then
            Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True,
        'Inserindo valor de XML no banco de dados.' + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Codigo, ID, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else
    Geral.MB_Erro(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
      'N�o foi poss�vel definir o valor', 3));
end;

function TFmNFeGeraXML_0400.DesmontaID_Inutilizacao(const Id: String; var cUF,
  Ano, emitCNPJ, Modelo, Serie, nNFIni, nNFFim: String): Boolean;
var
  K, N, Z: Integer;
begin
  K := Length(Id);
  //
  if K in ([41,42,43]) then
  begin
    Z := 1 + K - 41;
    N := 3;

    cUF      := Copy(Id, 02, N);
    Ano      := Copy(Id, 02, N);
    emitCNPJ := Copy(Id, 14, N);
    Modelo   := Copy(Id, 02, N);
    Serie    := Copy(Id,  Z, N);
    nNFIni   := Copy(Id, 09, N);
    nNFFim   := Copy(Id, 09, N);
    Result   := True;
  end else
  begin
    Result := False;
    //
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
      Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmNFeGeraXML_0400.WS_NFeInutilizacaoNFe(UFServico: String; Ambiente,
  CodigoUF, Ano: Byte; Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
  XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit; Modelo: Integer): String;
const
  TipoConsumo = tcwsNfeInutilizacao;
var
  sAviso, Texto, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso,
  Modelo) then Exit;
  //
  FDadosTxt := XML_NFeInutNFe(Id, Ambiente, CodigoUF, Ano, CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if NFeXMLGeren.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
    begin
      Acao := TStringList.Create;
      Stream := TMemoryStream.Create;

      VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);

      if Pos('NFEINUTILIZACAO2', UpperCase(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeInutilizacao2';
        ProcedimentoRet := 'nfeInutilizacaoNF2Result';
      end else
      if Pos('NFEINUTILIZACAO3', UpperCase(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeInutilizacao3';
        ProcedimentoRet := 'nfeInutilizacaoNFResult';
      end else
      begin
        ProcedimentoEnv := 'nfeInutilizacao';
        ProcedimentoRet := 'nfeResultMsg';
      end;

      Texto := '<?xml version="1.0" encoding="utf-8"?>';
      Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
      Texto := Texto +   '<soap12:Header>';
      //Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2">';
      Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
      //Texto := Texto +       '<versaoDados>' + verNFeInutNFe_Versao + '</versaoDados>';
      Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
      Texto := Texto +     '</nfeCabecMsg>';
      Texto := Texto +   '</soap12:Header>';
      Texto := Texto +   '<soap12:Body>';
      //Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2">';
      Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto + FAssinTxt;
      Texto := Texto +     '</nfeDadosMsg>';
      Texto := Texto +   '</soap12:Body>';
      Texto := Texto +'</soap12:Envelope>';

      Acao.Text := Texto;
      Acao.SaveToStream(Stream);

      ReqResp := THTTPReqResp.Create(nil);
      ConfiguraReqResp( ReqResp );
      ReqResp.URL := FURL;
      ReqResp.UseUTF8InHeader := True;
      //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2';
      ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
      try
        RETxtEnvio.Text :=  Acao.Text;
        EdWS.Text := FURL;
        ReqResp.Execute(Acao.Text, Stream);
        StrStream := TStringStream.Create('');
        StrStream.CopyFrom(Stream, 0);
        //Result := SeparaDados(StrStream.DataString,'nfeInutilizacaoNF2Result');
        Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
      finally
        Acao.Free;
        Stream.Free;
      end;
    end;
  end;
end;

function TFmNFeGeraXML_0400.WS_NFeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc; Modelo: Integer): String;
var
  sAviso: String;
  Texto: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  I: Integer;
  XML_STR: String;
  VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  TipoConsumo: TTipoConsumoWS;
begin
  Screen.Cursor := crHourGlass;
  //
  TipoConsumo := tcwsNFeAutorizacao;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso,
  Modelo) then Exit;
  //
  FDadosTxt := FmNFeSteps_0400.FXML_LoteNFe;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);

  ProcedimentoEnv := 'NfeAutorizacao';
  ProcedimentoRet := 'nfeResultMsg';

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';


  XML_STR := '';
  for i := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
      XML_STR := XML_STR + Texto[I]
  end;
  Texto := XML_STR;
  //
  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  // ini 2020-11-18
  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeAutorizacao4/nfeAutorizacaoLote';
  // fim 2020-11-18
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);

    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);

    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.ObtemWebServer2(UFServico: String; Ambiente,
  CodigoUF: Integer; Acao: TTipoConsumoWS; sAviso: String; Modelo: Integer): Boolean;

  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
        1: Result := 'Produ��o';
        2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;

  function TextoAcao(Acao: TTipoConsumoWS): String;
  begin
    case Acao of
      (* tcwsNfeConsultaProtocolo
      tcwsConsultaNFe:         Result := 'Consultar NF-e';
      tcwsConsultaNFeDest:     Result := 'Consulta NFe-s destinadas';
      tcwsNFeDownloadNF:       Result := 'Download de NF-e';
      tcwsDistDFeInt:          Result := 'Distribui��o de DFe de Interesse';
      *)
      tcwsNfeStatusServico:
        Result := 'Status do servi�o';
      tcwsNfeInutilizacao:
        Result := 'Pedir inutiliza��o de n�mero(s) de NF-e';
      tcwsRecepcaoEvento:
        Result := 'Enviar lote de evento de NF-e ao fisco';
      tcwsNfeConsultaCadastro:
        Result := 'Consulta cadastro de Contribuinte';
      tcwsNFeAutorizacao:
        Result := 'Envio Lote NF-e (novo 3.10)';
      tcwsNFeRetAutorizacao:
        Result := 'Consulta Recibo Lote (novo 3.10)';
      else
        Result := '[Desconhecido]';
    end;
  end;

  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS): Byte;
  begin
    case TipoConsumoWS of
      (*
      tcwsConsultaNFeDest:
        Result := 9;
      tcwsNFeDownloadNF:
        Result := 10;
      *)
      tcwsNfeStatusServico:
        Result := 0;
      tcwsNfeInutilizacao:
        Result := 4;
      tcwsNfeConsultaProtocolo:
        Result := 5;
      tcwsRecepcaoEvento:
        Result := 6;
      tcwsNfeConsultaCadastro:
        Result := 7;
      tcwsNFeAutorizacao:
        Result := 11;
      tcwsNFeRetAutorizacao:
        Result := 12;
      tcwsDistDFeInt:
        Result := 13;
      else
        Result := Null;
    end;
  end;

var
  cUF, A: Integer;
  Versao, x: String;
begin
  FURL := UnNFe_PF.ObtemURLWebServerNFe(UFServico, NomeAcao(Acao),
  VersaoWS(Acao, True), Ambiente, Modelo);
  A    := TipoConsumoWs2Acao(Acao);
  if A = 13 then
    FURL := 'https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx';
  cUF  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  x    := Uppercase(UFServico + ' '+ NomeAcao(Acao) + ' ' + VersaoWS(Acao, True));
  //
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + sLineBreak  +
    x + sLineBreak +
    'UF SEFAZ: ' + Geral.FF0(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + sLineBreak  + 'UF Servi�o: ' + Geral.FF0(cUF) + ' = ' + UFServico
    + sLineBreak  + 'Ambiente: ' + Geral.FF0(Ambiente) + ' = ' + TextoAmbiente(
    Ambiente) + sLineBreak  + 'A��o: ' + Geral.FF0(A) + ' = ' + TextoAcao(Acao)
    + slineBreak + 'x = "' + x + '"' + sLineBreak +
    sLineBreak  + 'AVISE A DERMATEK' + sLineBreak;
    Result := False;
    Geral.MB_Aviso(sAviso);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
end;

// ver!!! Delphi Alexandria
{$IfDef VER320}
procedure TFmNFeGeraXML_0400.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
// VAR_CertDigPfxWay = 0
var

  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  ContentHeader: string;

//VAR_CertDigPfxWay = 1
var
  //Cert         : ICertificate2;   // Acima
  //CertContext  : ICertContext;    // Acima
  //PCertContext : PCCERT_CONTEXT;  // Acima
  //
  Path_Para_o_PFX, Password_para_o_PFX: String;
begin
  if VAR_CertDigPfxWay = 0 then
  begin
    Store := CoStore.Create;
    //Reposit�rios de Certifcados da M�quina

    // 2020-11-09
    //Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
    Store.Open( CAPICOM_LOCAL_MACHINE_STORE, 'MY',    CAPICOM_STORE_OPEN_READ_ONLY );
    // fim 2020-11-09

    // N�o funciona!!
    //Store.Open( CAPICOM_CURRENT_USER_STORE, 'ADDRESSBOOK',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
    //Abre a lista de certificados

    Certs := Store.Certificates ;
    //Aloca todos os certificados instalados na m�quia

    i := 0;
    //loop de procura ao certificado requerido pelo n�mero serial
    while i < Certs.Count do
     begin
       Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
       //Cria objeto para acesso a leitura do certificado
       if UpperCase(Cert.SerialNumber ) = UpperCase(FmNFeSteps_0400.EdSerialNumber.Text) then
       //se o n�mero do serial for igual ao que queremos utilizar
        begin
          //carrega informa��es do certificado
          CertContext := Cert as ICertContext;
          CertContext.Get_CertContext( Integer( PCertContext ) );

          //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
          if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
          begin
            Geral.MB_Aviso('Falha ao selecionar o certificado.');
          end;

    ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
    HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);

          i := Certs.Count;
          //encerra o loop
        end;
       i := i + 1;
    end;
  end else
  //if VAR_CertDigPfxWay = 2 then
  begin
    if VAR_CERTIFICADO_DIGITAL = nil then
    begin
      NFeXMLGeren.ObtemCertificado(FmNFeSteps_0400.EdSerialNumber.Text, VAR_CERTIFICADO_DIGITAL)
    end;
    CertContext := VAR_CERTIFICADO_DIGITAL as ICertContext;
    CertContext.Get_CertContext( Integer( PCertContext ) );
    // Pone el certificado para la comunicaci�n SSL
    if InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT,
    PCertContext, Sizeof(CERT_CONTEXT ) ) = False then
      Geral.MB_Erro('N�o foi poss�vel carregar o certificado digital');
{
  end
  else
  begin
    //CertContext := Cert as ICertContext;
    if VAR_CERTIFICADO_DIGITAL = nil then
    begin
      NFeXMLGeren.ObtemCertificado('', VAR_CERTIFICADO_DIGITAL)
    end;
    CertContext := VAR_CERTIFICADO_DIGITAL as ICertContext;
    CertContext.Get_CertContext( Integer( PCertContext ) );
    // Pone el certificado para la comunicaci�n SSL
    if InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT,
    PCertContext, Sizeof(CERT_CONTEXT ) ) = False then
      Geral.MB_Erro('N�o foi poss�vel carregar o certificado digital');
}
  end;
end;
{$Else}
procedure TFmNFeGeraXML_0400.OnBeforePost(const HTTPReqResp: THTTPReqResp; Client: THTTPClient);
begin
(*
  HTTPReqResp.UseUTF8InHeader := True;
  HTTPReqResp.URL := WSDL;
  HTTPReqResp.SoapAction := Action;
  HTTPReqResp.ConnectTimeout:=120000;
  HTTPReqResp.ReceiveTimeout:=120000;
  HTTPReqResp.SendTimeout:=120000;

  HTTPReqResp.MaxSinglePostSize := 1000000;
*)

  HTTPReqResp.ClientCertificate.SerialNum := FmNFeSteps_0400.EdSerialNumber.Text;
end;
// ver!!! Delphi Alexandria


(*

procedure TFmNFeGeraXML_0400.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Client: THTTPClient);
var
  ContentHeader: String;
begin
  with FpDFeSSL do
  begin
    if (UseCertificateHTTP) then
    begin
      if not InternetSetOption(Client, INTERNET_OPTION_CLIENT_CERT_CONTEXT,
        PCCERT_CONTEXT(FpDFeSSL.CertContextWinApi), SizeOf(CERT_CONTEXT)) then
        raise EACBrDFeException.Create('Erro ao ajustar INTERNET_OPTION_CLIENT_CERT_CONTEXT: ' +
                                       IntToStr(GetLastError));
    end;

    if (trim(ProxyUser) <> '') then
    begin
      if not InternetSetOption(Client, INTERNET_OPTION_PROXY_USERNAME,
        PChar(ProxyUser), Length(ProxyUser)) then
        raise EACBrDFeException.Create('Erro ao ajustar INTERNET_OPTION_PROXY_USERNAME: ' +
                                       IntToStr(GetLastError));

      if (trim(ProxyPass) <> '') then
        if not InternetSetOption(Client, INTERNET_OPTION_PROXY_PASSWORD,
          PChar(ProxyPass), Length(ProxyPass)) then
          raise EACBrDFeException.Create('Erro ao ajustar INTERNET_OPTION_PROXY_PASSWORD: ' +
                                         IntToStr(GetLastError));
    end;

    if (FMimeType <> '') then
    begin
      ContentHeader := Format(ContentTypeTemplate, [FMimeType]);
      HttpAddRequestHeaders(Client, PChar(ContentHeader), Length(ContentHeader),
                              HTTP_ADDREQ_FLAG_REPLACE);

    end;
  end;
  //N�o existe este m�todo CheckContentType em Soap.SOAPHTTPTrans (D10.3.1)
  //FIndyReqResp.CheckContentType;
end;
*)
{$EndIf}

function TFmNFeGeraXML_0400.OutroPais_dest: Boolean;
begin
  Result := (DmNFe_0000.QrNFECabAdest_cPais.Value <> 0) and
              (DmNFe_0000.QrNFECabAdest_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0400.OutroPais_emit: Boolean;
begin
  Result := (DmNFe_0000.QrNFECabAemit_cPais.Value <> 0) and
              (DmNFe_0000.QrNFECabAemit_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0400.OutroPais_entrega: Boolean;
begin
  Result := DmNFe_0000.QrNFECabGentrega_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0400.OutroPais_retirada: Boolean;
begin
  Result := DmNFe_0000.QrNFECabFretirada_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0400.SeparaDados(Texto, Chave: String;
  MantemChave: Boolean): String;
var
  PosIni, PosFim : Integer;
begin
  if MantemChave then
   begin
     PosIni := Pos(Chave,Texto)-1;
     PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)-1;
        PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
      end;
   end
  else
   begin
     PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
     PosFim := Pos('/'+Chave,Texto);

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
        PosFim := Pos('/ns2:'+Chave,Texto);
      end;
   end;
  Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  if Result = '' then
    Geral.MB_Erro(Texto);
end;

function TFmNFeGeraXML_0400.SeparaDados2(Texto, ChaveA, ChaveB: String;
  MantemChave: Boolean): String;
  //
  procedure TentaSepararDados(Chave: String);
var
  PosIni, PosFim : Integer;
  begin
    if MantemChave then
     begin
       PosIni := Pos(Chave,Texto)-1;
       PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

       if (PosIni = 0) or (PosFim = 0) then
        begin
          PosIni := Pos('ns2:'+Chave,Texto)-1;
          PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
        end;
     end
    else
     begin
       PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
       PosFim := Pos('/'+Chave,Texto);

       if (PosIni = 0) or (PosFim = 0) then
        begin
          PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
          PosFim := Pos('/ns2:'+Chave,Texto);
        end;
     end;
    Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  end;
begin
  TentaSepararDados(ChaveA);
  if Result = '' then
    TentaSepararDados(ChaveB);
  if Result = '' then
    Geral.MB_Erro(Texto);
end;

function TFmNFeGeraXML_0400.StrZero(Num: Real; Zeros, Deci: Integer): String;
var
  tam, z : Integer;
  res, zer : String;
begin
  str(Num:Zeros:Deci, res);
  res := Alltrim(res);
  tam := length(res);
  zer := '';
  for z := 1 to (Zeros-tam) do
    zer := zer + '0';
  result := zer+res
end;

function TFmNFeGeraXML_0400.TipoXML(NoStandAlone: Boolean): String;
begin
  Result := '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  //
  if NoStandAlone then
    Result := Result + ' standalone="no"';
  //
  Result := Result +  '?>';
end;

function TFmNFeGeraXML_0400.Alltrim(const Search: string): string;
const
  BlackSpace = [#33..#126];
var
  Index: byte;
begin
  Index:=1;
  while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
    begin
      Index:=Index + 1;
    end;
  Result:=Copy(Search, Index, 255);
  Index := Length(Result);
  while (Index > 0) and not (Result[Index] in BlackSpace) do
    begin
      Index:=Index - 1;
    end;
  Result := Copy(Result, 1, Index);
end;

procedure TFmNFeGeraXML_0400.BtOKClick(Sender: TObject);
begin
  //HTTPRIO1.HTTPWebNode1.BeforePost := OnBeforePost;
end;

procedure TFmNFeGeraXML_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmNFeGeraXML_0400.FMT_IE(Valor: String): String;
begin
  if Uppercase(Valor) <> 'ISENTO' then
    Result := Geral.SoNumero_TT(Valor)
  else
    Result := Valor;
end;

procedure TFmNFeGeraXML_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeGeraXML_0400.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeGeraXML_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeGeraXML_0400.VersaoWS(Acao: TTipoConsumoWS; Formata: Boolean = True): String;

  function Fmt(Val: Double): String;
  begin
    if Formata then
      Result := Geral.FFT_Dot(Val, 2, siNegativo)
    else
      Result := Geral.FFI(Val);
  end;

begin
  case Acao of
    (*
    tcwsDistDFeInt:
      Result := Fmt(DmodG.QrParamsEmpNFeVerDistDFeInt.Value);
    tcwsNFeDownloadNF:
      Result := Fmt(DmodG.QrParamsEmpNFeVerDowNFe.Value);
    *)
    tcwsNFeRetAutorizacao:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerConLot.Value);
(*
    tcwsNfeConsultaCadastro:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerConsCad.Value);
*)
    tcwsNFeAutorizacao:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value);
    tcwsRecepcaoEvento:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerLotEve.Value);
    tcwsNfeStatusServico:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerStaSer.Value);
    tcwsNfeConsultaProtocolo:
      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerConNFe.Value);
    //
    else Result := '4.00';
  end;
end;

function TFmNFeGeraXML_0400.NomeAcao(Acao: TTipoConsumoWS): String;
begin
  case Acao of
    tcwsNFeAutorizacao:
      Result := 'NFeAutorizacao';
    tcwsNfeConsultaProtocolo:
      Result := 'NfeConsultaProtocolo';
    tcwsNfeInutilizacao:
      Result := 'NfeInutilizacao';
    tcwsNFeRetAutorizacao:
      Result := 'NFeRetAutorizacao';
    tcwsNfeStatusServico:
      Result := 'NfeStatusServico';
    tcwsRecepcaoEvento:
      Result := 'RecepcaoEvento';
    tcwsNfeConsultaCadastro:
      Result := 'NfeConsultaCadastro';
    else
      Result := '[Desconhecido]';
  end;
end;

function TFmNFeGeraXML_0400.WS_RecepcaoEvento(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Modelo: Integer): String;
const
  TipoConsumo = tcwsRecepcaoEvento;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso,
  Modelo) then Exit;
  //
  FDadosTxt := FmNFeSteps_0400.FXML_LoteEvento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  //Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  {Texto := Texto +    '<soap12:Header>';
  //Texto := Texto +      '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/nfeRecepcaoEventoNF">';
  //Texto := Texto +      '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">';
  Texto := Texto +      '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4">';
  Texto := Texto +        '<versaoDados>' + verEnviEvento_Versao + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +        '<cUF>' + FormatFloat('00', 91) + '</cUF>';
  Texto := Texto +      '</nfeCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  }

  {
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4">' + FDadosTxt + '</nfeDadosMsg>';
  }
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto + '<soap12:Body><nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4">';
  Texto := Texto + FDadosTxt + '</nfeDadosMsg>';
  //
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

{
  //  Teste
 Texto := '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">' +
 '<soap12:Body><nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4">' +
 '<envEvento xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00"><idLote>945</idLote><evento xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00">' +
 '<infEvento Id="ID1101404123070271786100011055001000015477476737484801"><cOrgao>91</cOrgao><tpAmb>2</tpAmb><CNPJ>02717861000110</CNPJ><chNFe>41230702717861000110550010000154774767374848</chNFe>' +
 '<dhEvento>2023-07-10T10:25:05-03:00</dhEvento><tpEvento>110140</tpEvento><nSeqEvento>1</nSeqEvento><verEvento>1.00</verEvento><detEvento versao="1.00"><descEvento>EPEC</descEvento><cOrgaoAutor>41</cOrgaoAutor><tpAutor>1</tpAutor><verAplic>BDER_230630' +
 '1435</verAplic><dhEmi>2023-07-10T10:24:00-03:00</dhEmi><tpNF>1</tpNF><IE>9016939002</IE><dest><UF>SC</UF><CNPJ>85168334000121</CNPJ><IE>252716817</IE><vNF>52.00</vNF><vICMS>6.24</vICMS><vST>0.00</vST></dest></detEvento></infEvento><Signature xmlns="h' +
 'ttp://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></CanonicalizationMethod><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"></SignatureMethod><Ref' +
 'erence URI="#ID1101404123070271786100011055001000015477476737484801"><Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></Transform><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></Transfo' +
 'rm></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"></DigestMethod><DigestValue>y/9HJ3EIa/MMva3Wj1eS4oUoP+4=</DigestValue></Reference></SignedInfo><SignatureValue>YR041edHqT7rzbuvm5gNfH9QMRcNwg5veZsK1t3w56SySGhoX5DtHUs/pM' +
 'oMQ8IOy/b3zg4kR0mvqRb9jyNLx+IFW5rhX7pOoFXPWvmnJUtVVVXADRvnscQ/4g4mYTFG1stVY++9cqQQqWCbSNxthTXTKbSVIol9bbQQ5a4+vkI9AMN0uY59JF9Bre72tqv5XhFV0aO1teg3Ca9o9NZIyapgYDc6iobAPCjpGh8+rLW+HgFdToljavkP8U5JaVS8xUuFAyOvbWe7/MwhMAdDWci3RzSIoogUK/l+ISCYDa42Dz37ZKt8' +
 's/0ZsrpFTKVDZO3627Wy711yNh9mbFQiGw==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIH5DCCBcygAwIBAgIIeHuM4QtWdKswDQYJKoZIhvcNAQELBQAwdjELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyY' +
 'XNpbCAtIFJGQjEaMBgGA1UEAxMRQUMgU0FGRVdFQiBSRkIgdjUwHhcNMjMwMjAxMTA0MjM4WhcNMjQwMjAxMTA0MjM4WjCB/jELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxCzAJBgNVBAgTAlBSMRIwEAYDVQQHEwlQQVJBTkFWQUkxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXN' +
 'pbCAtIFJGQjEWMBQGA1UECxMNUkZCIGUtQ05QSiBBMTEXMBUGA1UECxMONzYwODU2MjAwMDAxMzIxEzARBgNVBAsTCnByZXNlbmNpYWwxOzA5BgNVBAMTMlNPRlRDT1VSTyBJTkRVU1RSSUEgRSBDT01FUkNJTyBMVERBOjAyNzE3ODYxMDAwMTEwMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2YEr63HzKiEl5XzzhQfs5' +
 'KCd9xG0nD4+2uMgORl/HOAoWjB9g8c9ctkEGIvy9uerm9+tNc3aGin05NiXbU5yRYIqR3pNC9WmMl6MHyUZmXsYF69QkFwEU2gxB6kMDlYMSN2UH9tr2rPWtbh2MP3CHVtLJX2R0I5sf4ZzGWOPe17ZekXj9njOg24AuQVPnsQsKIF610eu5ao6sV5IBwhMuA4g+H1RDi6rsPW6GTvMoCxn69D6C079YjkWafJm1lLsKaZp4BFif5Yb/Ie' +
 'jshQ9v7Z4bjCJh9zHtpuW1ulv2sJ3VKDvL95S1vGEsUtgEvTvX2q5lVzs6JKfBCA+Y+Sy6QIDAQABo4IC6zCCAucwHwYDVR0jBBgwFoAUKV5L1UZMu/4Wp2PBHcQm8t3Y8wUwDgYDVR0PAQH/BAQDAgXgMGkGA1UdIARiMGAwXgYGYEwBAgEzMFQwUgYIKwYBBQUHAgEWRmh0dHA6Ly9yZXBvc2l0b3Jpby5hY3NhZmV3ZWIuY29tLmJyL' +
 '2FjLXNhZmV3ZWJyZmIvZHBjLWFjc2FmZXdlYnJmYi5wZGYwga4GA1UdHwSBpjCBozBPoE2gS4ZJaHR0cDovL3JlcG9zaXRvcmlvLmFjc2FmZXdlYi5jb20uYnIvYWMtc2FmZXdlYnJmYi9sY3ItYWMtc2FmZXdlYnJmYnY1LmNybDBQoE6gTIZKaHR0cDovL3JlcG9zaXRvcmlvMi5hY3NhZmV3ZWIuY29tLmJyL2FjLXNhZmV3ZWJyZmI' +
 'vbGNyLWFjLXNhZmV3ZWJyZmJ2NS5jcmwwgbcGCCsGAQUFBwEBBIGqMIGnMFEGCCsGAQUFBzAChkVodHRwOi8vcmVwb3NpdG9yaW8uYWNzYWZld2ViLmNvbS5ici9hYy1zYWZld2VicmZiL2FjLXNhZmV3ZWJyZmJ2NS5wN2IwUgYIKwYBBQUHMAKGRmh0dHA6Ly9yZXBvc2l0b3JpbzIuYWNzYWZld2ViLmNvbS5ici9hYy1zYWZld2Vic' +
 'mZiL2FjLXNhZmV3ZWJyZmJ2NS5wN2IwgbMGA1UdEQSBqzCBqIEWU09GVC5DT1VST0BIT1RNQUlMLkNPTaAgBgVgTAEDAqAXExVIVUdPIFlVSFVESSBOQUdBU1NBV0GgGQYFYEwBAwOgEBMOMDI3MTc4NjEwMDAxMTCgOAYFYEwBAwSgLxMtMDgwNTE5NjAzNjIyNDkxMDk0NDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwoBcGBWBMAQM' +
 'HoA4TDDAwMDAwMDAwMDAwMDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwCQYDVR0TBAIwADANBgkqhkiG9w0BAQsFAAOCAgEAfCrzfCSynk2G9Q1b9tIHaeDylqvhsPFdI2wKA2No0vEtWqeCUSA1xLRAWezawabSANMVhEoxQ1mrjfMSim9HL9Gjq6ZYDm1BYsJu/l2C3yXsjk4cVcOzU3HS3XXQZnUQsqqykUmGFKhGVdL9R' +
 'yuSx6qzSM7IC+prNrDvMKyCn4pQ3U0gerz9kiheu9VGp7WTkhyknRkH998izrZeA+dV+m27Pb47nrba3xJSyzn/+kok41DEXbrTGh+iQ/zFJmRx5rVnHSwbeG1DXwW1EAQUjtpfgzm79CURkF8oFDuEgrFkNgveLWKQHk40b6FEV/RmyvQp7r7ABUXFuamMcxr5eOswJlob2QxSbub259CTYyudSfdQcOeFdnMausRsLzPUuZvI0hjUyZR' +
 'zkbrkNGjzGRrmy/SQrCMQ4N8vhxnhojWBYXgwzg+EtEbzAONtHnxnYwAL8g6E+Vx61BFTINBJDvWKVfzwsMmftGZ7WBcWmQVHe2mWXggPG9eD+pXCC7GgUq0QLrZEJ2kvVYE7jiL+Vr5t8RKvPo2CAMhnIZQad9htid8YFO+vTWA99FMePtCXy3fFMAnQxf/f93IPmEdYXdnFFBQdmwDpszdZQ4NXHLNYWcZ59jwilMXyg23U9nfjm1X/7' +
 'qZWYKWUFEjsi9ZfZbIk5IF0oAGeANIID3lDLA8=</X509Certificate></X509Data></KeyInfo></Signature></evento></envEvento></nfeDadosMsg></soap12:Body></soap12:Envelope>';
}




  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/nfeRecepcaoEvento';
  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4';
  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEvento';
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF';
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      //Result := SeparaDados(StrStream.DataString,'nfeRecepcaoEventoResult');
      //Result := SeparaDados(StrStream.DataString, 'nfeResultMsg');
      Result := SeparaDados2(StrStream.DataString, 'nfeResultMsg', 'nfeRecepcaoEventoNFResult');
      //
      StrStream.Free;
    except
      on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0400.XML_ConsCad(xUF, Contribuinte_CNPJ: String): String;
var
  XML_ConsCad_Str: IXMLTConsCad;
  VersaoDados: String;
begin
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
  //VersaoDados := '2.00'; //Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsCad_Str := GetConsCad(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;
    //XML_ConsCad_Str.Versao      := verConsCad_Versao;
    XML_ConsCad_Str.Versao      := VersaoDados;
    XML_ConsCad_Str.InfCons.XServ := 'CONS-CAD';
    XML_ConsCad_Str.InfCons.UF := xUF;
    XML_ConsCad_Str.InfCons.CNPJ := Geral.SoNumero_TT(Contribuinte_CNPJ);
    //
    Result := TipoXML(False) + XML_ConsCad_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0400.XML_ConsReciNFe(TpAmb: Integer;
  NRec: String): String;
var
  XML_ConsReciNFe_Str: consReciNFe_v400.IXMLTConsReciNFe;
  VersaoDados: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsReciNFe_Str := GetConsReciNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    //
    VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);

    //XML_ConsReciNFe_Str.Versao      := verConsReciNFe_Versao;
    XML_ConsReciNFe_Str.Versao      := VersaoDados;
    //XML_ConsReciNFe_Str.Versao      := Versao;
    XML_ConsReciNFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsReciNFe_Str.NRec        := NRec;
    //
    Result := TipoXML(False) + XML_ConsReciNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0400.XML_ConsSitNFe(TpAmb: Integer; ChNFe,
  VersaoAcao: String): String;
var
  XML_ConsSitNFe_Str: IXMLTConsSitNFe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsSitNFe_Str := GetConsSitNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    //
    XML_ConsSitNFe_Str.Versao := VersaoAcao;
    XML_ConsSitNFe_Str.TpAmb  := FormatFloat('0', TpAmb);
    XML_ConsSitNFe_Str.XServ  := 'CONSULTAR';
    XML_ConsSitNFe_Str.ChNFe  := ChNFe;
    //
    Result := TipoXML(False) + XML_ConsSitNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0400.XML_ConsStatServ(TpAmb, CUF: Integer): String;
var
  XML_ConsStatServ_Str: IXMLTConsStatServ;
  Versao: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsStatServ_Str := GetConsStatServ(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;

    Versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);

    //XML_ConsStatServ_Str.Versao      := verConsStatServ_Versao;
    XML_ConsStatServ_Str.Versao      := Versao;
    XML_ConsStatServ_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsStatServ_Str.CUF         := FormatFloat('00', CUF);
    XML_ConsStatServ_Str.XServ       := 'STATUS';
    //
    Result := TipoXML(False) + XML_ConsStatServ_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0400.XML_DistDFeInt(TpAmb: Integer; UFAutor, CNPJ, CPF,
  ultNSU, NSU: String): String;
  // Erro no WebService???
  function CompletaZeros(Texto: String): String;
  begin
    Result := Texto;
    while Length(Result) < 15 do
      Result := '0' + Result;
  end;
var
  XML_DistDFeInt_Str: IXMLDistDFeInt;
  VersaoDados: String;
begin
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_DistDFeInt_Str := GetDistDFeInt(FdocXML);
    FdocXML.Version     := sXML_Version;
    FdocXML.Encoding    := sXML_Encoding;
    XML_DistDFeInt_Str.Versao      := VersaoDados;
    XML_DistDFeInt_Str.tpAmb       := Geral.FF0(tpAmb);
    XML_DistDFeInt_Str.cUFAutor    := UFAutor;
    //
    if CNPJ <> '' then
      XML_DistDFeInt_Str.CNPJ      := Geral.SoNumero_TT(CNPJ)
    else
      XML_DistDFeInt_Str.CPF       := Geral.SoNumero_TT(CPF);
    //
    if NSU <> '' then
      XML_DistDFeInt_Str.consNSU.NSU := CompletaZeros(NSU)
    else
    if ultNSU <> '' then
      XML_DistDFeInt_Str.distNSU.ultNSU      := CompletaZeros(ultNSU)
    else
      XML_DistDFeInt_Str.distNSU.ultNSU      := CompletaZeros('0');
    //
    Result := TipoXML(False) + XML_DistDFeInt_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0400.XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte;
  Ano: Integer; CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): String;
var
  XML_NFeInutNFe_Str: IXMLTInutNFe;
  xUF, xAno, Versao: String;
begin
  xUF  := FormatFloat('00', CUF);
  xAno := FormatFloat('00', Ano);
  MontaID_Inutilizacao(xUF, xAno, CNPJ, Mod_, Serie, nNFIni, nNFFin, Id);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_NFeInutNFe_Str := GetinutNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;

    Versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);

    //XML_NFeInutNFe_Str.Versao         := verNFeInutNFe_Versao;
    XML_NFeInutNFe_Str.Versao         := Versao;
    XML_NFeInutNFe_Str.InfInut.Id     := Id;
    XML_NFeInutNFe_Str.InfInut.TpAmb  := FormatFloat('0', TpAmb);
    XML_NFeInutNFe_Str.InfInut.XServ  := 'INUTILIZAR';
    XML_NFeInutNFe_Str.InfInut.CUF    := FormatFloat('00', CUF);
    XML_NFeInutNFe_Str.InfInut.Ano    := FormatFloat('00', Ano);
    XML_NFeInutNFe_Str.InfInut.CNPJ   := CNPJ;
    XML_NFeInutNFe_Str.InfInut.Mod_   := Mod_;
    XML_NFeInutNFe_Str.InfInut.Serie  := Serie;
    XML_NFeInutNFe_Str.InfInut.NNFIni := NNFIni;
    XML_NFeInutNFe_Str.InfInut.NNFFin := NNFFin;
    XML_NFeInutNFe_Str.InfInut.XJust  := XJust;
    Result := TipoXML(False) + XML_NFeInutNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;


//{$I dmk.inc}

end.
