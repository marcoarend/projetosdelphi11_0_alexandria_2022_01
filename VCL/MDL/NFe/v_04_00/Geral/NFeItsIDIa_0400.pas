unit NFeItsIDIa_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit,
  dmkGeral, Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums;

type
  TFmNFeItsIDIa_0400 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    EdConta: TdmkEdit;
    EdAdi_nAdicao: TdmkEdit;
    Label5: TLabel;
    DBEdnItem: TdmkDBEdit;
    Label3: TLabel;
    EdAdi_vDescDI: TdmkEdit;
    Label7: TLabel;
    EdAdi_cFabricante: TdmkEdit;
    Label12: TLabel;
    Label1: TLabel;
    DBEdFatID: TdmkDBEdit;
    Label10: TLabel;
    DBEdFatNum: TdmkDBEdit;
    Label13: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    DBEdControle: TdmkDBEdit;
    Label4: TLabel;
    EdAdi_nSeqAdic: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label6: TLabel;
    EdAdi_nDraw: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeItsIDIa_0400: TFmNFeItsIDIa_0400;

implementation

uses UnMyObjects, Module, NFeCabA_0000, UMySQLModule, UnInternalConsts, MyDBCheck,
  ModuleGeral;

{$R *.DFM}

procedure TFmNFeItsIDIa_0400.BtOKClick(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeitsidia', '', EdConta.ValueVariant);
  //
(*
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'nfeitsidia', auto_increment?[
'Adi_nAdicao', 'Adi_cFabricante', 'Adi_vDescDI',
'Adi_nSeqAdic', 'Adi_nDraw'], [
'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle', 'Conta'], [
Adi_nAdicao, Adi_cFabricante, Adi_vDescDI,
Adi_nSeqAdic, Adi_nDraw], [
FatID, FatNum, Empresa, nItem, Controle, Conta], UserDataAlterweb?, IGNORE?
*)
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'nfeitsidia',
  Conta, Dmod.QrUpd) then
  begin
    FmNFeCabA_0000.ReopenNFeItsIDIa(Conta);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Declara��o inclu�da / alterada!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType            := stIns;
      EdConta.ValueVariant   := 0;
      EdAdi_nAdicao.SetFocus;
    end else Close;
  end;
end;

procedure TFmNFeItsIDIa_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsIDIa_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsIDIa_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
