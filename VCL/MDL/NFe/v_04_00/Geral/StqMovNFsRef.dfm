object FmStqMovNFsRef: TFmStqMovNFsRef
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-007 :: NFs referenciadas'
  ClientHeight = 326
  ClientWidth = 676
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 676
    Height = 170
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 41
      Width = 676
      Height = 129
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      Visible = False
      object TabSheet1: TTabSheet
        Caption = 'NF-e / NFC-e'
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 668
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          DoubleBuffered = True
          ParentBackground = False
          ParentDoubleBuffered = False
          TabOrder = 0
          object Label15: TLabel
            Left = 344
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Chave NF-e:'
          end
          object SpeedButton1: TSpeedButton
            Left = 621
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdrefNFe: TdmkEdit
            Left = 344
            Top = 20
            Width = 276
            Height = 21
            MaxLength = 44
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdrefNFeExit
          end
          object CkSigilo: TdmkCheckBox
            Left = 12
            Top = 20
            Width = 321
            Height = 17
            Caption = 'Chave NF-e zerada para manter sigilo conforme legisla'#231#227'o.'
            TabOrder = 0
            OnClick = CkSigiloClick
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
        object PnSigilo: TPanel
          Left = 0
          Top = 49
          Width = 668
          Height = 52
          Align = alBottom
          TabOrder = 1
          Visible = False
          object Label18: TLabel
            Left = 344
            Top = 8
            Width = 97
            Height = 13
            Caption = 'Chave NF-e sigilosa:'
          end
          object EdrefNFeSig: TdmkEdit
            Left = 344
            Top = 24
            Width = 276
            Height = 21
            MaxLength = 44
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Nota Fiscal (NF modelo 1/1A)'
        ImageIndex = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 668
          Height = 101
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 87
          object Label2: TLabel
            Left = 4
            Top = 4
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label4: TLabel
            Left = 84
            Top = 4
            Width = 94
            Height = 13
            Caption = 'Ano e m'#234's (AAMM):'
          end
          object Label3: TLabel
            Left = 188
            Top = 4
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
          end
          object Label16: TLabel
            Left = 312
            Top = 4
            Width = 104
            Height = 13
            Caption = 'Modelo do doc. fiscal:'
          end
          object Label1: TLabel
            Left = 424
            Top = 4
            Width = 109
            Height = 13
            Caption = 'S'#233'rie (zero para '#250'nica):'
          end
          object Label10: TLabel
            Left = 536
            Top = 4
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
          end
          object EdUF: TdmkEditCB
            Left = 4
            Top = 20
            Width = 29
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNF_cUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            DBLookupComboBox = CBUF
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBUF: TdmkDBLookupComboBox
            Left = 32
            Top = 20
            Width = 49
            Height = 21
            KeyField = 'DTB'
            ListField = 'Nome'
            ListSource = DsDTB_UFs1
            TabOrder = 1
            dmkEditCB = EdUF
            QryCampo = 'refNF_cUF'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdAAMM: TdmkEdit
            Left = 84
            Top = 20
            Width = 101
            Height = 21
            Alignment = taRightJustify
            MaxLength = 4
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0001'
            ValMax = '9912'
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            Texto = '0001'
            QryCampo = 'refNF_AAMM'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
            OnExit = EdAAMMExit
          end
          object EdCNPJ: TdmkEdit
            Left = 189
            Top = 20
            Width = 120
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNF_CNPJ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdmodNF: TdmkEdit
            Left = 312
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNF_mod'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSerie: TdmkEdit
            Left = 424
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNF_Serie'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdnNF: TdmkEdit
            Left = 536
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNF_nNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'NF Produtor Rural'
        ImageIndex = 2
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 668
          Height = 101
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 87
          object Label5: TLabel
            Left = 4
            Top = 4
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label6: TLabel
            Left = 84
            Top = 4
            Width = 94
            Height = 13
            Caption = 'Ano e m'#234's (AAMM):'
          end
          object Label7: TLabel
            Left = 188
            Top = 4
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
          end
          object Label8: TLabel
            Left = 552
            Top = 4
            Width = 104
            Height = 13
            Caption = 'Modelo do doc. fiscal:'
          end
          object Label9: TLabel
            Left = 4
            Top = 44
            Width = 109
            Height = 13
            Caption = 'S'#233'rie (zero para '#250'nica):'
          end
          object Label11: TLabel
            Left = 116
            Top = 44
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
          end
          object Label12: TLabel
            Left = 312
            Top = 4
            Width = 23
            Height = 13
            Caption = 'CPF:'
          end
          object Label13: TLabel
            Left = 268
            Top = 4
            Width = 36
            Height = 13
            Caption = '... ou ...'
          end
          object Label14: TLabel
            Left = 436
            Top = 4
            Width = 19
            Height = 13
            Caption = 'I.E.:'
          end
          object Label17: TLabel
            Left = 228
            Top = 44
            Width = 60
            Height = 13
            Caption = 'Chave CT-e:'
          end
          object EdrefNFP_cUF: TdmkEditCB
            Left = 4
            Top = 20
            Width = 29
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNFP_cUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            DBLookupComboBox = CBrefNFP_cUF
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBrefNFP_cUF: TdmkDBLookupComboBox
            Left = 32
            Top = 20
            Width = 49
            Height = 21
            KeyField = 'DTB'
            ListField = 'Nome'
            ListSource = DsDTB_UFs2
            TabOrder = 1
            dmkEditCB = EdrefNFP_cUF
            QryCampo = 'refNFP_cUF'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdrefNFP_AAMM: TdmkEdit
            Left = 84
            Top = 20
            Width = 101
            Height = 21
            Alignment = taRightJustify
            MaxLength = 4
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0001'
            ValMax = '9912'
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            Texto = '0001'
            QryCampo = 'refNFP_AAMM'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
            OnExit = EdAAMMExit
          end
          object EdrefNFP_CNPJ: TdmkEdit
            Left = 189
            Top = 20
            Width = 120
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFP_CNPJ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdrefNFP_mod: TdmkEdit
            Left = 552
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNFP_mod'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdrefNFP_serie: TdmkEdit
            Left = 4
            Top = 60
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNFP_serie'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdrefNFP_nNF: TdmkEdit
            Left = 116
            Top = 60
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refNFP_nNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdrefNFP_CPF: TdmkEdit
            Left = 313
            Top = 20
            Width = 120
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFP_CPF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdrefNFP_IE: TdmkEdit
            Left = 436
            Top = 20
            Width = 112
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refNFP_IE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdrefCTe: TdmkEdit
            Left = 228
            Top = 60
            Width = 433
            Height = 21
            MaxLength = 44
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refCTe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Cupom Fiscal'
        ImageIndex = 3
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 668
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label21: TLabel
            Left = 4
            Top = 4
            Width = 104
            Height = 13
            Caption = 'Modelo do doc. fiscal:'
          end
          object Label22: TLabel
            Left = 116
            Top = 4
            Width = 95
            Height = 13
            Caption = 'N'#186' Ordem seq. ECF:'
          end
          object Label23: TLabel
            Left = 228
            Top = 4
            Width = 41
            Height = 13
            Caption = 'N'#186' COO:'
          end
          object EdrefECF_mod: TdmkEdit
            Left = 4
            Top = 20
            Width = 109
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refECF_mod'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdrefECF_nECF: TdmkEdit
            Left = 116
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refECF_nECF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdrefECF_nCOO: TdmkEdit
            Left = 228
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'refECF_nCOO'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
    object RGQualNFref: TdmkRadioGroup
      Left = 0
      Top = 0
      Width = 676
      Height = 41
      Align = alTop
      Caption = ' Tipo de nota a ser referenciada:'
      Columns = 4
      Items.Strings = (
        'NF-e / NFC-e'
        'Nota Fiscal (NF modelo 1/1A)'
        'NF Produtor Rural'
        'Cupom Fiscal')
      TabOrder = 1
      OnClick = RGQualNFrefClick
      QryCampo = 'QualNFref'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 676
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 628
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 580
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 222
        Height = 32
        Caption = 'NFs referenciadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 222
        Height = 32
        Caption = 'NFs referenciadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 222
        Height = 32
        Caption = 'NFs referenciadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 218
    Width = 676
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 672
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 293
        Height = 16
        Caption = 'Informe apenas os dados de uma das quatro abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 293
        Height = 16
        Caption = 'Informe apenas os dados de uma das quatro abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 262
    Width = 676
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 672
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 528
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 128
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 8
        Top = 15
        Width = 113
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 2
      end
    end
  end
  object DsDTB_UFs1: TDataSource
    DataSet = QrDTB_UFs1
    Left = 340
    Top = 260
  end
  object QrDTB_UFs1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DTB, Nome'
      'FROM ufs'
      'ORDER BY Nome')
    Left = 340
    Top = 212
    object QrDTB_UFs1DTB: TWideStringField
      FieldName = 'DTB'
      Size = 2
    end
    object QrDTB_UFs1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
  end
  object DsDTB_UFs2: TDataSource
    DataSet = QrDTB_UFs2
    Left = 416
    Top = 260
  end
  object QrDTB_UFs2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DTB, Nome'
      'FROM ufs'
      'ORDER BY Nome')
    Left = 416
    Top = 212
    object QrDTB_UFs2DTB: TWideStringField
      FieldName = 'DTB'
      Size = 2
    end
    object QrDTB_UFs2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
  end
end
