unit NFeImporta_0400;

interface

uses Forms, Windows, Controls, Dialogs, SysUtils, dmkGeral, Entidade2, OmniXML,
  OmniXMLUtils, ModuleNFe_0000, UnitMyXML, MyDBCheck, ModuleGeral, mySQLDbTables,
  UMySQLModule, Module, (*GraGruE_P,*) UnDmkProcFunc, UnDmkEnums, UnXXe_PF,
  System.Classes, NFe_PF, UnGrl_Vars, Vcl.StdCtrls,
  UnInternalConsts3;

type
  TNFeImporta_0400 = class(TObject)
  private
    { Private declarations }
    function  ObtemValorCampoPeloSeqInArq(const Tabela, Campo:
              String; const SeqInArq, FatID, FatNum, Empresa: Integer; var
              SQLType: TSQLType): Integer;

  public
    { Public declarations }

    //  Importa��o de NFe de terceiros
    function  ImportaDadosNFeDeXML(const MeAvisos: TMemo; const XML: String; const Form: TForm;
              FatID: Integer; var FatNum, Empresa, IDCtrl, CodInfoEmit,
              Transporta, _qVol: Integer; _PesoB, _PesoL: Double; var
              DataFiscal: TDateTime; NFeCabA: TNFeCabA = nil): Boolean;

  end;

  var
  UnNFeImporta_0400: TNFeImporta_0400;

implementation

uses UnMyObjects, DmkDAC_PF, UnGrade_Jan, UnInternalConsts, MyListas, UnPagtos;

function TNFeImporta_0400.ImportaDadosNFeDeXML(const MeAvisos: TMemo; const XML: String; const Form:
  TForm; FatID: Integer; var FatNum, Empresa, IDCtrl, CodInfoEmit, Transporta,
  _qVol: Integer; _PesoB, _PesoL: Double; var DataFiscal: TDateTime;
  NFeCabA: TNFeCabA = nil): Boolean;

const
  Avisa = TInfoUserMsg.iumInfo;
  NaoAvisa = TInfoUserMsg.iumNoInfo;
  Atencao = TInfoUserMsg.iumWarn;
  sProcName = 'TNFeImporta_0400.ImportaDadosNFeDeXML()';
  //
  function LocalizaRegistro_nItem(Tabela: String; FatID, FatNum, Empresa, nItem: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Result := False;
    Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + Tabela,
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nItem=' + Geral.FF0(nItem),
        '']);
      if Qry.RecordCount > 0 then
        Result := True;
    finally
      Qry.Free;
    end;
  end;

  function LocalizaRegistro_nDup(const Tabela: String; const FatID, FatNum,
  FatParcela, Empresa: Integer; const nDup, dVenc: String; var Controle, Lancto,
  Sub: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Result   := False;
    Controle := 0;
    Lancto   := 0;
    Sub      := 0;
    Qry      := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, Lancto, Sub ',
        'FROM ' + Tabela,
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND FatParcela=' + Geral.FF0(FatParcela),
        'AND Empresa=' + Geral.FF0(Empresa),
        'AND nDup="' + nDup + '"',
        'AND dVenc="' + dVenc + '"',
        '']);
      //Geral.MB_Teste(Qry.SQL.Text);
      if Qry.RecordCount > 0 then
      begin
        Result := True;
        Controle := Qry.FieldByName('Controle').AsInteger;
        Lancto   := Qry.FieldByName('Lancto').AsInteger;
        Sub      := Qry.FieldByName('Sub').AsInteger;
      end;
    finally
      Qry.Free;
    end;
  end;

  function ExcluirEntrada(Form: TForm; FatID, FatNum, Empresa: Integer;
    Avisa: Boolean): Boolean;
  begin
    DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa, True, True);
    //
    if Form.Name = 'FmPQE' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM pqe WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else
    if Form.Name = 'FmMPIn' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpin WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpinits WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else
    if Form.Name = 'FmStqInnCad' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqinncad WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else
      Geral.MensagemBox('A janela "' + Form.Name +
        '" n�o est� implementada na function "ExcluirEntrada" da function "' +
        'TNFeImporta_0400.ImportaDadosNFeDeXML()"!' + sLineBreak + 'AVISA DERMATEK',
        'ERRO', MB_OK+MB_ICONERROR);
    //
    Result := True;
    //
    Screen.Cursor := crHourGlass;
    if Avisa then
      Geral.MensagemBox('Ocorreu um erro na importa��o do XML.' + sLineBreak +
        'Os dados da entrada foram exclu�dos por serem parciais!',
        'Aviso', MB_OK+MB_ICONERROR);
  end;

var
  Arquivo, emit_Doc, dest_Doc: String;
  Cont: Boolean;
  xmlDocA, xmlDocB : IXMLDocument;
  xmlNodeA, xmlNodeB, xmlNodeNiv0, xmlNodeNiv1, xmlNodeNiv2, xmlNodeNiv3,
  xmlNodeNiv4, xmlNodeNiv5, xmlNodeNiv6,
  xmlNodeNivA, xmlNodeNivB: IXMLNode;
  //
  xmlList: IXMLNodeList;
  //
  ide_natOp, ide_dEmi, ide_hEmi, ide_dSaiEnt, ide_hSaiEnt, ide_verProc,
  ide_dhCont, ide_xJust, emit_CNPJ, emit_CPF, emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro, emit_xMun, emit_UF, emit_xPais, emit_fone,
  emit_IE, emit_IEST, emit_IM, emit_CNAE, dest_CNPJ, dest_CPF,
  dest_idEstrangeiro, dest_xNome, dest_xLgr, dest_nro, dest_xCpl, dest_xBairro,
  dest_xMun, dest_UF, dest_CEP, dest_xPais, dest_fone, dest_IE, dest_ISUF,
  dest_IM, dest_email, Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
  Transporta_IE, Transporta_XEnder, Transporta_XMun, Transporta_UF,
  RetTransp_CFOP, RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
  VeicTransp_RNTC, Cobr_Fat_nFat, InfAdic_InfAdFisco, InfAdic_InfCpl,
  Exporta_UFEmbarq, Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed, Compra_XCont,
  infProt_Id, infProt_verAplic, infProt_dhRecbto, infProt_nProt, infProt_digVal,
  infProt_xMotivo, NFG_SubSerie, COD_MOD, FD_EXP_REG, eveMDe_Id,
  eveMDe_verAplic, eveMDe_xMotivo, eveMDe_chNFe, eveMDe_xEvento,
  eveMDe_CNPJDest, eveMDe_CPFDest, eveMDe_emailDest, eveMDe_dhRegEvento,
  eveMDe_nProt, InfCpl_totTrib, Exporta_XLocDespacho: String;

  LoteEnv, ide_cUF, ide_cNF, ide_indPag, ide_mod, ide_serie, ide_nNF, ide_tpNF,
  ide_idDest, ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV, ide_tpAmb, ide_finNFe,
  ide_indFinal, ide_indPres, ide_procEmi, emit_cMun, emit_CEP, emit_cPais,
  emit_CRT, EstrangDef, dest_cMun, dest_cPais, dest_indIEDest, ISSQNtot_cRegTrib,
  ModFrete, Status, infProt_tpAmb, infProt_cStat, _Ativo_, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg, CodInfoDest, CriAForca, CodInfoTrsp,
  OrdemServ, Situacao, Importado, COD_SIT,
  EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq, eveMDe_tpAmb, eveMDe_cOrgao,
  eveMDe_cStat, eveMDe_tpEvento, eveMDe_nSeqEvento, cSitNFe, cSitConf,
  NFeNT2013_003LTT, CodInfoCliI, ide_indIntermed: Integer;

  versao, ide_dhEmiTZD, ide_dhSaiEntTZD, ide_dhContTZD, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vICMSDeson, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,
  ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
  ISSQNtot_vDeducao, ISSQNtot_vOutro, ISSQNtot_vDescIncond, ISSQNtot_vDescCond,
  ISSQNtot_vISSRet, RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
  RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev, RetTrib_vRetPrev,
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet,
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq, protNFe_versao,
  retCancNFe_versao, FreteExtra,
  SegurExtra, ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
  PISRec_vBC, PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
  COFINSRec_pAliq, COFINSRec_vCOFINS, NF_ICMSAlq, NFG_ValIsen, NFG_NaoTrib,
  NFG_Outros, VL_ABAT_NT, ICMSRec_vBCST, ICMSRec_vICMSST, ICMSRec_pAliqST,
  eveMDe_TZD_UTC, vBasTrib, pTotTrib, infProt_dhRecbtoTZD,
  //
  ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet,
  ICMSTot_vFCP, ICMSTot_vFCPST, ICMSTot_vFCPSTRet, ICMSTot_vIPIDevol: Double;

  ICMS_UFST, COD_NAT: String;

  nItem, ICMS_Orig, ICMS_CST, ICMS_modBC, ICMS_modBCST, ICMS_CSOSN,
  ICMS_motDesICMS: Integer;

  ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif,
  ICMS_vICMS, ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
  ICMS_vICMSST, ICMS_pBCOp, ICMS_vBCSTRet, ICMS_vICMSSTRet, ICMS_vICMSDeson,
  ICMS_pCredSN, ICMS_vCredICMSSN, ICMS_vBCFCPST, ICMS_pFCPST, ICMS_vFCPST,
  ICMS_pST, ICMS_vBCFCPSTRet, ICMS_pFCPSTRet, ICMS_vFCPSTRet: Double;

  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EXTIPI, prod_uCom,
  prod_cEANTrib, prod_uTrib, prod_xPed, prod_nFCI: String;

  prod_genero, prod_CFOP, prod_indTot,
  prod_nItemPed, Tem_IPI, InfAdCuztm, EhServico, MeuID, Nivel1,
  GraGruX, UnidMedCom, UnidMedTrib, prod_CEST, StqMovValA: Integer;

  Tem_II: Boolean;

  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib, prod_vFrete,
  prod_vSeg, prod_vDesc, prod_vOutro: Double;

  IPI_clEnq, IPI_CNPJProd, IPI_cSelo, IPI_cEnq, IND_APUR: String;

  IPI_CST: Integer;

  IPI_qSelo, IPI_vBC, IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI: Double;

  PIS_CST: Integer;

  PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd, PIS_vAliqProd, PIS_fatorBC: Double;

  PISST_vBC, PISST_pPIS, PISST_qBCProd, PISST_vAliqProd, PISST_vPIS,
  PISST_fatorBCST: Double;

  COFINS_CST: Integer;

  COFINS_vBC, COFINS_pCOFINS, COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS,
  COFINS_fatorBC: Double;

  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS, COFINSST_fatorBCST: Double;

  InfAdProd: String;

  Texto,
  infNFe_versao, infNFe_Id

,vol_pesoL
,vol_pesoB
,vol_qVol
,vol_esp
,vol_marca
,vol_nVol

,PISST_CST

,COFINSST_CST: WideString;

  its_qVol, its_Volumes, Tipo, cab_TipoNF, cab_Pedido,
  cab_Conhecimento, Controle, Conta, cab_Serie, cab_NF, Lancto, Sub: Integer;
  Msg, cab_refNFe, cab_modNF, cab_DataE, cab_DataS, cab_Data, cab_Cancelado: String;
  _x_
  ,cab_ICMS
  ,cab_ValProd
  ,cab_Seguro
  ,cab_Desconto
  ,cab_IPI
  ,cab_PIS
  ,cab_COFINS
  ,cab_Frete
  ,cab_Outros
  ,cab_ValorNF
  ,cab_Juros
  ,cab_RICMS
  ,cab_RICMSF
  ,its_PesoVB
  ,its_PesoVL
  ,its_ValorItem
  ,its_RIPI
  ,its_CFin
  ,its_Frete
  ,its_Seguro
  ,its_Desconto
  ,its_TotalCusto
  ,its_TotalPeso
  ,its_IPI
  ,its_ICMS
  ,its_RICMS
  ,its_pesoL
  ,its_pesoB
  : Double;

  tot_Rec_ICMS, tot_Rec_IPI, tot_Rec_PIS, tot_Rec_COFINS, ICMSTot_vTotTrib: Double;

  ResMsg, CodCad: Integer;
  //
  Continua: Boolean;
  SQLTypeA: TSQLType;
  TagRaiz, ide_dhEmi, ide_dhSaiEnt, GraGruEIts_NomeGGX: String;
  SeqInArq, FatParcela, GraGruEIts_GraGruX: Integer;
  SQLTypeX, SQLTypeY: TSQLType;
  QtdSQLY: Integer;
  Ausentes: String;
  prod_indEscala, prod_CNPJFab, prod_cBenef: String;
  vTotTrib, II_vBC, II_vDespAdu, II_vII, II_vIOF: Double;
  // Duplicatas
  nDup, dVenc: String;
  vDup: Double;
  //Emissao: TDateTime;
  Arr_dVenc: TArr_dVenc;
  Arr_vDup: TArr_vDup;
  Descricao: String;
  Int_nfat, Int_nDup: Integer;
begin
  Ausentes := EmptyStr;
  Screen.Cursor := crHourGlass;
  Result        := False;
  try
    //FatID              := VAR_FATID_0051;
    //
    its_Volumes        := 1;
    Transporta         := 0;
    cab_Frete          := 0;
    cab_Seguro         := 0;
    cab_Desconto       := 0;
    cab_ICMS           := 0;
    cab_IPI            := 0;
    cab_PIS            := 0;
    cab_COFINS         := 0;
    cab_Outros         := 0;
    cab_ValorNF        := 0;
    cab_ValProd        := 0;
    Empresa            := 0;
    //
    ide_cUF            := 0;
    ide_cNF            := 0;
    ide_indPag         := 0;
    ide_mod            := 0;
    ide_serie          := 0;
    ide_nNF            := 0;
    ide_tpNF           := 0;
    ide_idDest         := 0;
    ide_cMunFG         := 0;
    ide_tpImp          := 0;
    ide_tpEmis         := 0;
    ide_indIntermed    := 0;
    ide_cDV            := 0;
    ide_indPres        := 0;
    ide_indFinal       := 0;
    ide_tpAmb          := 0;
    ide_finNFe         := 0;
    ISSQNtot_vBC       := 0;
    ISSQNtot_vISS      := 0;
    ISSQNtot_vPIS      := 0;
    ISSQNtot_vCOFINS   := 0;
    RetTrib_vRetPIS    := 0;
    RetTrib_vRetCOFINS := 0;
    RetTrib_vRetCSLL   := 0;
    RetTrib_vBCIRRF    := 0;
    RetTrib_vIRRF      := 0;
    RetTrib_vBCRetPrev := 0;
    RetTrib_vRetPrev   := 0;
    //
    ISSQNtot_vServ     := 0;
    ISSQNtot_vBC       := 0;
    ISSQNtot_vISS      := 0;
    ISSQNtot_vPIS      := 0;
    ISSQNtot_vCOFINS   := 0;
    RetTrib_vRetPIS    := 0;
    RetTrib_vRetCOFINS := 0;
    RetTrib_vRetCSLL   := 0;
    RetTrib_vBCIRRF    := 0;
    RetTrib_vIRRF      := 0;
    infProt_tpAmb      := 0;
    infProt_dhRecbto   := '0000-00-00 00:00:00';
    infProt_cStat      := 0;
    protNFe_versao     := 0;
    //
    RetTransp_vServ    := 0;
    RetTransp_vBCRet   := 0;
    RetTransp_PICMSRet := 0;
    RetTransp_vICMSRet := 0;
    RetTransp_CFOP     := '';
    RetTransp_CMunFG   := '';
    VeicTransp_Placa   := '';
    VeicTransp_UF      := '';
    VeicTransp_RNTC    := '';
    Cobr_Fat_nFat      := '';
    Cobr_Fat_vOrig     := 0;
    Cobr_Fat_vDesc     := 0;
    Cobr_Fat_vLiq      := 0;
    //
    Exporta_UFEmbarq   := '';
    Exporta_XLocEmbarq := '';
    Compra_XNEmp       := '';
    Compra_XPed        := '';
    Compra_XCont       := '';
    //
    ICMSRec_pRedBC     := 0;
    ICMSRec_vBC        := 0;
    ICMSRec_pAliq      := 0;
    ICMSRec_vICMS      := 0;
    IPIRec_pRedBC      := 0;
    IPIRec_vBC         := 0;
    IPIRec_pAliq       := 0;
    IPIRec_vIPI        := 0;
    PISRec_pRedBC      := 0;
    PISRec_vBC         := 0;
    PISRec_pAliq       := 0;
    PISRec_vPIS        := 0;
    COFINSRec_pRedBC   := 0;
    COFINSRec_vBC      := 0;
    COFINSRec_pAliq    := 0;
    COFINSRec_vCOFINS  := 0;
    //
    ICMSTot_vFCPUFDest   := 0;
    ICMSTot_vICMSUFDest  := 0;
    ICMSTot_vICMSUFRemet := 0;
    ICMSTot_vFCP         := 0;
    ICMSTot_vFCPST       := 0;
    ICMSTot_vFCPSTRet    := 0;
    ICMSTot_vIPIDevol    := 0;
    //
    II_vBC               := 0;
    II_vDespAdu          := 0;
    II_vII               := 0;
    II_vIOF              := 0;


    Status := 0;
    //
    Continua := False;
    if XML <> '' then
    begin
      Texto := XML;
      Continua := True;
    end else
    begin
      if MyObjects.FileOpenDialog(Form, CO_DIR_RAIZ_DMK + '\NFE', '', 'Informe o aqruivo XML',
        '', [], Arquivo)
      then
        Continua := UnMyXML.CarregaXML(Arquivo, Texto);
    end;

    if Continua then
    begin
      if UnMyXML.DefineXMLDoc(xmlDocA, Texto) then
      begin
      // Verifica se � envio de NFe
      TagRaiz  := '/nfeProc/NFe';
      xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz);

      if not assigned(xmlNodeA) then
      begin
        TagRaiz := '/NFe';
        xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz);
      end;

      if assigned(xmlNodeA) then
      begin
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//      Grupo A. Dados da Nota Fiscal eletr�nica                              //
//      NFE                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlDocA, MeAvisos, '', [], ['nfeProc']);
        xmlNodeNiv0 := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, '/nfeProc');
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv0, MeAvisos, 'nfeProc', ['versao', 'xmlns'], ['NFe', 'protNFe']);
        xmlNodeNiv1 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv0, 'NFe', MeAvisos);
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv1, MeAvisos, 'nfeProc.NFe', ['xmlns'], ['infNFe', 'Signature']);
        xmlNodeNiv2 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv1, 'infNFe', MeAvisos);
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv2, MeAvisos, 'nfeProc.NFe.infNFe', ['Id', 'versao'], [
          'ide', 'emit', (*'avulsa',*) 'dest', 'retirada', 'entrega', 'autXML',
          'det', 'total', 'transp', 'cobr', 'pag', 'infIntermed', 'infAdic',
          'exportar', 'infRespTec', 'infNFeSupl']);
        //
        infNFe_versao := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_versao);
        infNFe_Id     := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
        //xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe');
        xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe');

        if assigned(xmlNodeA) then
        begin
          infNFe_versao      := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_versao);
          infNFe_Id          := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
          //
          cab_refNFe         := Copy(infNFe_Id, 4); // chave NFe
          cab_modNF          := Copy(cab_refNFe, 21, 2);
          //
          UnNFe_PF.ObtemSerieNumeroByID(cab_refNFe, cab_serie, cab_NF);
        end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//      Grupo B. Identifica��o da Nota Fiscal eletr�nica                      //
//      IDENTIFICACAO NFE                                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv2, 'ide', MeAvisos);
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.ide', [], [
        //indPag - Excluido na NFe 4.0 - NT2016.002
        'cUF', 'cNF', 'natOp', (*indPag,*) 'mod', 'serie', 'nNF', 'dhEmi',
        'dhSaiEnt', 'tpNF', 'idDest', 'cMunFG', 'tpImp', 'tpEmis', 'cDV',
        'tpAmb', 'finNFe', 'indFinal', 'indPres',
        'indIntermed', // 4.00 NT 2020.006
        'procEmi', 'verProc']);
        //
        xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/ide');
        if assigned(xmlNodeA) then
        begin
          ide_cUF     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cUF    ));
          ide_cNF     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cNF    ));
          ide_natOp   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_natOp  );
          // ini indPag - Excluido na NFe 4.0 - NT2016.002
          ide_indPag  := 0; //Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indPag ));
          // fim indPag - Excluido na NFe 4.0 - NT2016.002
          ide_mod     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_mod    ));
          ide_serie   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_serie  ));
          ide_nNF     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nNF    ));
          // at� 2.00
          ide_dEmi    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dEmi   );
          ide_dSaiEnt := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dSaiEnt);
          // 3.10
          ide_dhEmi   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dhEmi   );
          if ide_dhEmi <> '' then
          begin
            XXe_PF.Ajusta_dh_XXe_UTC(ide_dhEmi, ide_dhEmiTZD);
            ide_dEmi := Copy(ide_dhEmi, 1, 10);
            ide_hEmi := Copy(ide_dhEmi, 11);
          end else
          begin
            ide_dEmi := '0000-00-00';
            ide_hEmi := '00:00:00';
          end;
          ide_dhSaiEnt   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dhSaiEnt   );
          if ide_dhSaiEnt <> '' then
          begin
            XXe_PF.Ajusta_dh_XXe_UTC(ide_dhSaiEnt, ide_dhSaiEntTZD);
            ide_dSaiEnt := Copy(ide_dhSaiEnt, 1, 10);
            ide_hSaiEnt := Copy(ide_dhSaiEnt, 11);
          end else
          begin
            ide_dSaiEnt := '0000-00-00';
            ide_hSaiEnt := '00:00:00';
          end;
          // FIM 3.10
          ide_tpNF    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpNF   ));
          // 3.10
          ide_idDest  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_idDest ));
          // FIM 3.10
          ide_cMunFG  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMunFG ));
          ide_tpImp   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpImp  ));
          ide_tpEmis  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpEmis ));
          ide_cDV     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cDV    ));
          ide_tpAmb   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpAmb  ));
          ide_finNFe  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_finNFe ));
          // 3.10
          ide_indFinal  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indFinal));
          ide_indPres   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indPres));
          // FIM 3.10
          // 4.00
          ide_indIntermed := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indIntermed));
          // fim 4.00
          ide_procEmi := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_procEmi));
          ide_verProc := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_verProc);
          //
          // 3.10
          ide_dhCont   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dhCont);
          if ide_dhCont <> '' then
            XXe_PF.Ajusta_dh_XXe(ide_dhCont)
          else
            ide_dhCont := '0000-00-00 00:00:00';
          ide_xJust   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xJust);
          // FIM 3.10
          cab_DataE   := Copy(ide_dEmi    , 1, 10);
          cab_DataS   := Copy(ide_dSaiEnt , 1, 10);
        end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//           B27.1 - ENTRADA EM CONTING�NCIA                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
///
///       // B28 NFe 2.0
///       ide_dhCont     := ???
///       ide_dhContTZD  := ???
///       ide_xJust      := ???
///
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            GRUPO BA. DOCUMENTO FISCAL REFERENCIADO                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            GRUPO C - IDENTIFICA��O DO EMITENTE DA NOTA FISCAL ELETRONICA   //
//            EMIT - FORNECEDOR                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv2, 'emit', MeAvisos);
        Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.emit', [], [
        'CNPJ', 'CPF', 'xNome', 'xFant', 'enderEmit', 'IE', 'IEST', 'IM', 'CNAE', 'CRT']);
        //
        xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/emit');
        if assigned(xmlNodeA) then
        begin
          emit_CNPJ  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ   );
          emit_CPF   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF    );
          emit_xNome := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome  );
          emit_xFant := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xFant  );
          // ... enderEmit ...
          emit_IE    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE     );
          emit_IEST  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IEST   );
          emit_IM    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IM     );
          emit_CNAE  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNAE   );
          // 3.10
          emit_CRT   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CRT    ));
          // FIM 3.10
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            ENDERE�O EMITENTE                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
          xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv3, 'enderEmit', MeAvisos);
          Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos,
          'nfeProc.NFe.infNFe.emit.enderEmit', [], [
          'xLgr', 'nro', 'xCpl', 'xBairro', 'cMun', 'xMun', 'UF', 'CEP', 'cPais',
          'xPais', 'fone']);
          //
          xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/emit/enderEmit');
          if assigned(xmlNodeA) then
          begin
            emit_xLgr    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xLgr   );
            emit_nro     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nro    );
            emit_xCpl    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xCpl   );
            emit_xBairro := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xBairro);
            emit_cMun    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMun   ));
            emit_xMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun   );
            emit_UF      := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF     );
            emit_CEP     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CEP    ));
            emit_cPais   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cPais  ));
            emit_xPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xPais  );
            emit_fone    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_fone   );
          end;

          (*
          if emit_CNPJ = '' then emit_CNPJ := '???';
          if emit_CPF = '' then emit_CPF := '???';
          *)
          Tipo := -1;
          if emit_CNPJ <> '' then Tipo := 0 else
          if emit_CPF  <> '' then Tipo := 1;
          if Tipo = -1 then
          begin
            Geral.MensagemBox(
            'N�o foi poss�vel definir o tipo de pessoa do emitente da NF-e!',
            'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end else
          begin
            CodInfoEmit := DmNFe_0000.DefineEntidadePeloDoc(emit_CNPJ, emit_CPF);
            //
            if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
            begin
              Msg := 'O fornecedor abaixo n�o est� cadastrado:' + sLineBreak +
              'Nome: ' + emit_xNome + sLineBreak + 'Fantasia: ' + emit_xFant + sLineBreak;
              //if emit_CNPJ <> '???' then
              if emit_CNPJ <> '' then
                Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(emit_CNPJ) + sLineBreak;
              //if emit_CPF <> '???' then
              if emit_CPF <> '' then
                emit_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(emit_CPF) + sLineBreak;
              Msg := Msg + 'I.E.: ' + emit_IE + sLineBreak;
              Msg := Msg + 'Munic�pio: ' + emit_xMun + sLineBreak;
              Msg := Msg + 'UF: ' + emit_UF + sLineBreak;
              Msg := Msg + sLineBreak + 'Deseja cadastr�-lo?';
              if Geral.MensagemBox(Msg, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
              begin
                if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
                begin
                  FmEntidade2.Incluinovaentidade1Click(Self);
                  //
                  FmEntidade2.CadastroPorXML_NFe_2(entNFe_emit, emit_CNPJ,
                    emit_CPF, emit_xNome, emit_xFant, emit_IE, emit_xLgr,
                    emit_nro, emit_xBairro, emit_cMun, emit_xMun,
                    emit_UF, emit_CEP, emit_cPais, emit_xPais, emit_fone,
                    emit_IEST, emit_IM, emit_CNAE, emit_xCpl, '');
                  //
                  FmEntidade2.ShowModal;
                  FmEntidade2.Destroy;
                  //
                  CodInfoEmit := DmNFe_0000.DefineEntidadePeloDoc(emit_CNPJ, emit_CPF);
                end;
              end else
                Exit;
            end;

            if DmNFe_0000.QrEnt_Doc.RecordCount > 0 then
            begin
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo D. Identifica��o do Fisco Emitente da NF-e                //
//            EMITIDO PELO FISCO                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Atencao, xmlNodeNiv2, 'avulsa', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.avulsa', [], [
              'CNPJ', 'xOrgao', 'matr', 'xAgente', 'fone', 'UF', 'nDAR', 'dEmi', 'vDAR', 'repEmi', 'dPag']);
              //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            GRUPO E - IDENTIFICA��O DO DESTINATARIO DA NOTA FISCAL ELETRONICA/
//            DEST - CLIENTE                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv2, 'dest', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.dest', [], [
              'CNPJ', 'CPF', 'idEstrangeiro', 'xNome', 'enderDest', 'indIEDest', 'IE', 'ISUF', 'IM', 'email']);
              //
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/dest');
              if assigned(xmlNodeA) then
              begin
                dest_CNPJ          := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ   );
                dest_CPF           := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF    );
                dest_idEstrangeiro := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_idEstrangeiro);
                dest_xNome         := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome  );
                // ... enderDest ...
                dest_indIEDest     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indIEDest));
                dest_IE            := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE     );
                dest_ISUF          := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_ISUF   );
                dest_IM            := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IM   );
                dest_email         := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_email   );
                {
                if dest_CNPJ = '' then dest_CNPJ := '???';
                if dest_CPF = '' then dest_CPF := '???';
                }

                //2018-01-23 => Atualiza apenas notas emitidas contra a empresa
                if emit_CNPJ <> '' then
                  emit_Doc := emit_CNPJ
                else
                  emit_Doc := emit_CPF;

                if dest_CNPJ <> '' then
                  dest_Doc := dest_CNPJ
                else
                  dest_Doc := dest_CPF;

                if (DmNFe_0000.LocalizaFiliaByDocum(emit_Doc) = 0) and
                  (DmNFe_0000.LocalizaFiliaByDocum(dest_Doc) <> 0)
                then
                  Cont := True
                else
                  Cont := False;

                if not Cont then
                begin
                  Geral.MB_Aviso('Apenas notas emitidas contra sua empresa podem ser importadas!');
                  Exit;
                end;

                Tipo := -1;
                if dest_CNPJ <> '' then Tipo := 0 else
                if dest_CPF  <> '' then Tipo := 1;
                if Tipo = -1 then
                begin
                  Msg := 'Cliente interno n�o est� cadastrado:' + sLineBreak +
                  'Nome: ' + dest_xNome + sLineBreak;
                  //if dest_CNPJ <> '???' then
                  if dest_CNPJ <> '' then
                    Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(dest_CNPJ) + sLineBreak;
                  //if dest_CPF <> '???' then
                  if dest_CPF <> '' then
                    dest_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(dest_CPF) + sLineBreak;
                  Msg := Msg + 'I.E.: ' + dest_IE + sLineBreak;
                  Msg := Msg + sLineBreak + 'Deseja cadastr�-lo';
                  Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
                  Exit;
                end else
                begin
                  Empresa := DmNFe_0000.DefineEntidadePeloDoc(dest_CNPJ, dest_CPF, True);
                  //
                  if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
                  begin
                    Geral.MensagemBox('Destinat�rio da NF-e n�o cadastrado!',
                    'Aviso', MB_OK+MB_ICONWARNING);
                    Exit;
                  end;
                end;
              end;
              xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv3, 'enderDest', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos,
              'nfeProc.NFe.infNFe.dest.enderDest', [], [
              'xLgr', 'nro', 'xCpl', 'xBairro', 'cMun', 'xMun', 'UF', 'CEP', 'cPais',
              'xPais', 'fone']);
              //
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/dest/enderDest');
              if assigned(xmlNodeA) then
              begin
                dest_xLgr    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xLgr   );
                dest_nro     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nro    );
                dest_xCpl    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xCpl   );
                dest_xBairro := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xBairro);
                dest_cMun    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMun   ));
                dest_xMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun   );
                dest_UF      := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF     );
                dest_CEP     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CEP    );
                dest_cPais   := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cPais  ));
                dest_xPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xPais  );
                dest_fone    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_fone   );
              end;
              if IDCtrl = 0 then
              begin
                {
                QrLocNFe.Close;
                QrLocNFe.SQL.Clear;
                QrLocNFe.SQL.Add('SELECT Codigo');
                QrLocNFe.SQL.Add('FROM pqe');
                QrLocNFe.SQL.Add('WHERE refNFe=:P0');
                QrLocNFe.Params[0].AsString := cab_refNFe;
                UMyMod.AbreQuery(DmNFe_0000.QrLocNFe, 'TNFeImporta_0400.ImportaDadosNFeDeXML()');
                if QrLocNFe.RecordCount = 0 then
                begin
                }
                  DmNFe_0000.QrLocNFe.Close;
                  {
                  QrLocNFe.SQL.Clear;
                  QrLocNFe.SQL.Add('SELECT FatNum Codigo');
                  QrLocNFe.SQL.Add('FROM nfecaba');
                  QrLocNFe.SQL.Add('WHERE Id=:P0');
                  }
                  DmNFe_0000.QrLocNFe.Params[0].AsString := cab_refNFe;
                  UMyMod.AbreQuery(DmNFe_0000.QrLocNFe, Dmod.MyDB, 'TNFeImporta_0400.ImportaDadosNFeDeXML()');
                //end;
                CodCad := DmNFe_0000.QrLocNFeFatNum.Value;
                if CodCad > 0 then
                begin
                  DataFiscal := DmNFe_0000.QrLocNFeDataFiscal.Value;
                  ResMsg := ID_NO;
                  ResMsg := Geral.MensagemBox('A NFe selecionada j� foi lan�ada ' +
                  'no c�digo ' + IntToStr(CodCad) + '. Deseja lan�ar novamente?',
                  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
                  if ResMsg = ID_YES then
                  begin
                    ResMsg := Geral.MensagemBox('Tem certeza que deseja lan�ar ' +
                    'novamente o xml selecionado?' + sLineBreak +
                    'Os dados j� lan�ados ser�o previamente exclu�dos!',
                    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
                  end;
                  if ResMsg = ID_YES then
                  begin
                    ExcluirEntrada(Form, FatID, CodCad, Empresa, False);
                    FatNum := CodCad;
                  end else
                    Exit;
                end else
                  DataFiscal := 0;
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo F. Identifica��o do Local de Retirada                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv2, 'retirada', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(NaoAvisa, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.retirada', [], [
              'CNPJ', 'CPF', 'xNome', 'xLgr', 'nro', 'xCpl', 'xBairro', 'cMun',
              'xMun', 'UF', 'CEP', 'cPais', 'xPais', 'fone', 'email', 'IE']);
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo G. Identifica��o do Local de Entrega                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv2, 'entrega', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(NaoAvisa, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.entrega', [], [
              'CNPJ', 'CPF', 'xNome', 'xLgr', 'nro', 'xCpl', 'xBairro', 'cMun',
              'xMun', 'UF', 'CEP', 'cPais', 'xPais', 'fone', 'email', 'IE']);
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo GA. Autoriza��o para obter XML                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv2, 'autXML', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.autXML', [], [
              'CNPJ', 'CPF']);
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
///                                                        /////////////////////
///  Grupo H. Detalhamento de Produtos e Servi�os da NF-e  //    A B A I X O  //
///                                                        /////////////////////
////////////////////////////////////////////////////////////////////////////////
///                                                        /////////////////////
///  Grupo I. Produtos e Servi�os da NF-e                  //    A B A I X O  //
///                                                        /////////////////////
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo X. Informa��es do Transporte da NF-e                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv2, 'transp', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.transp', [], [
              'modFrete', 'transporta', 'retTransp', 'veicTransp', 'reboque', 'vol', 'lacres']);
///
///
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            TRANSPORTADORA                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv4, 'transporta', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.transp.transporta', [], [
              'CNPJ', 'CPF', 'xNome', 'IE', 'xEnder', 'xMun', 'UF'
              ]);
              //
              xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv3, 'vol', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.transp.vol', [], [
              'qVol', 'esp', 'marca', 'nVol', 'pesoL', 'pesoB', ''
              ]);
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/transp');
              if assigned(xmlNodeA) then
              begin
                modFrete := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_modFrete));
              end;
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/transp/transporta');
              if assigned(xmlNodeA) then
              begin
                Transporta_CNPJ   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ);
                Transporta_CPF    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF);
                Transporta_xNome  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome);
                Transporta_IE     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE);
                Transporta_xEnder := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xEnder);
                Transporta_xMun   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun);
                Transporta_UF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF);
                {
                if Transporta_CNPJ = '' then Transporta_CNPJ := '???';
                if Transporta_CPF = '' then Transporta_CPF := '???';
                }
                Tipo := -1;
                if Transporta_CNPJ <> '' then Tipo := 0 else
                if Transporta_CPF  <> '' then Tipo := 1;
                if Tipo = -1 then
                begin
                  (* N�o mostrar aviso
                  Geral.MB_Aviso('N�o foi poss�vel definir o tipo de entidade da transportadora!');
                  *)
                  Exit;
                end else
                begin
                  Transporta := DmNFe_0000.DefineEntidadePeloDoc(Transporta_CNPJ, Transporta_CPF);
                  //
                  if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
                  begin
                    Msg := 'A transportadora abaixo n�o est� cadastrada:' + sLineBreak +
                    'Nome: ' + Transporta_xNome + sLineBreak;
                    //if Transporta_CNPJ <> '???' then
                    if Transporta_CNPJ <> '' then
                      Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(Transporta_CNPJ) + sLineBreak;
                    //if Transporta_CPF <> '???' then
                    if Transporta_CPF <> '' then
                      Transporta_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(Transporta_CPF) + sLineBreak;
                    Msg := Msg + 'I.E.: ' + Transporta_IE + sLineBreak;
                    Msg := Msg + 'Munic�pio: ' + Transporta_xMun + sLineBreak;
                    Msg := Msg + 'UF: ' + Transporta_UF + sLineBreak;
                    Msg := Msg + sLineBreak + 'Deseja cadastr�-la?';
                    if Geral.MensagemBox(Msg, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
                    begin
                      if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
                      begin
                        FmEntidade2.Incluinovaentidade1Click(Self);
                        //
                        FmEntidade2.CadastroPorXML_NFe_2(entNFe_transp,
                          Transporta_CNPJ, Transporta_CPF, Transporta_xNome,
                          '', Transporta_IE, '', '', '', 0, Transporta_xMun,
                          Transporta_UF, 0, 0, '', '', '', '', '', '',
                          Transporta_xEnder);
                        //
                        FmEntidade2.ShowModal;
                        FmEntidade2.Destroy;
                        //
                        Transporta := DmNFe_0000.DefineEntidadePeloDoc(Transporta_CNPJ, Transporta_CPF);
                      end;
                    end;
                  end;
                end;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            Grupo W. Total da NF-e                                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
///
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv2, 'total', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.total', [], [
              'ICMSTot'
              ]);
              //
              xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv3, 'ICMSTot', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.total.ICMSTot', [], [
              'vBC', 'vICMS', 'vICMSDeson', 'vFCPUFDest', 'vICMSUFDest',
              'vICMSUFRemet', 'vFCP', 'vBCST', 'vST', 'vFCPST', 'vFCPSTRet',
              'vProd', 'vFrete', 'vSeg', 'vDesc', 'vII', 'vIPI', 'vIPIDevol',
              'vPIS', 'vCOFINS', 'vOutro', 'vNF', 'vTotTrib'
              ]);
              //
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/total/ICMSTot');
              if assigned(xmlNodeA) then
              begin
                ICMSTot_vBC          := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vBC         ));
                ICMSTot_vICMS        := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vICMS       ));
                ICMSTot_vICMSDeson   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vICMSDeson  ));
                ICMSTot_vFCPUFDest   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFCPUFDest  ));
                ICMSTot_vICMSUFDest  := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vICMSUFDest ));
                ICMSTot_vICMSUFRemet := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vICMSUFRemet));
                ICMSTot_vFCP         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFCP        ));
                ICMSTot_vBCST        := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vBCST       ));
                ICMSTot_vST          := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vST         ));
                ICMSTot_vFCPST       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFCPST      ));
                ICMSTot_vFCPSTRet    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFCPSTRet   ));
                ICMSTot_vProd        := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vProd       ));
                ICMSTot_vFrete       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFrete      ));
                ICMSTot_vSeg         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vSeg        ));
                ICMSTot_vDesc        := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vDesc       ));
                ICMSTot_vII          := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vII         ));
                ICMSTot_vIPI         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vIPI        ));
                ICMSTot_vIPIDevol    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vIPIDevol   ));
                ICMSTot_vPIS         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vPIS        ));
                ICMSTot_vCOFINS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vCOFINS     ));
                ICMSTot_vOutro       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vOutro      ));
                ICMSTot_vNF          := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vNF         ));
                ICMSTot_vTotTrib     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vTotTrib    ));
                //
                cab_ICMS    := ICMSTot_vICMS;
                cab_ValProd := ICMSTot_vProd;
                cab_Frete   := ICMSTot_vFrete;
                cab_Seguro  := ICMSTot_vSeg;
                cab_Desconto:= ICMSTot_vDesc;
                cab_IPI     := ICMSTot_vIPI;
                cab_PIS     := ICMSTot_vPIS;
                cab_COFINS  := ICMSTot_vCOFINS;
                cab_Outros  := ICMSTot_vOutro;
                cab_ValorNF := ICMSTot_vNF;
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            VOLUMES - PESO                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              //
              {
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/transp/vol');
              if assigned(xmlNodeA) then
              begin
                vol_qVol  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_qVol);
                vol_pesoL := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_pesoL);
                vol_pesoB := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_pesoB);
                //
                _qVol    := Geral.IMV(vol_qVol);
                _PesoL   := Geral.DMV_Dot(vol_PesoL);
                _PesoB   := Geral.DMV_Dot(vol_PesoB);
                //
              end else
              begin
                _qVol    := 0;
                _PesoL   := 0;
                _PesoB   := 0;
              end;
              cab_Data          := FormatDateTime('yyyy-mm-dd', Date); // Entrada
              cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
              //
              cab_Pedido        := 0;
              cab_Juros         := 0;
              cab_RICMS         := 0;
              cab_RICMSF        := 0;
              cab_Conhecimento  := 0;
              cab_Cancelado     := 'V';
              //
              }
              LoteEnv := 0;
              if IDCtrl = 0 then
              begin
                SQLTypeA := stIns;
                IDCtrl := UMyMod.Busca_IDCtrl_NFe(SQLTypeA, 0);
              end else
                SQLTypeA := stUpd;


              // VOLUMES POR LISTA !!!


              _qVol    := 0;
              _PesoL   := 0;
              _PesoB   := 0;
              xmlList := xmlDocA.SelectNodes(TagRaiz + '/infNFe/transp/vol');
              if xmlList.Length > 0 then
              begin
                SeqInArq := 0;
                while xmlList.Length > 0 do
                begin
                  SeqInArq  := SeqInArq + 1;
                  nItem     := Geral.IMV(UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem));
                  vol_qVol  := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_qVol);
                  vol_esp   := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_esp);
                  vol_marca := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_marca);
                  vol_nVol  := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nVol);
                  vol_pesoL := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_pesoL);
                  vol_pesoB := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_pesoB);
                  //
                  its_qVol  := Geral.IMV(vol_qVol);
                  its_pesoL := Geral.DMV_Dot(vol_PesoL);
                  its_pesoB := Geral.DMV_Dot(vol_PesoB);
                  //
                  _qVol    := _qVol  + its_qVol;
                  _PesoL   := _PesoL + its_pesoL;
                  _PesoB   := _PesoB + its_pesoB;
                  //
                  Controle := ObtemValorCampoPeloSeqInArq('nfecabxvol',
                  'Controle', SeqInArq, FatID, FatNum, Empresa, SQLTypeX);
                  if SQLTypeX = stIns then
                    Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxvol', '', 0);
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeX, 'nfecabxvol', False, [
                  'qVol', 'esp', 'marca',
                  'nVol', 'pesoL', 'pesoB',
                  'SeqInArq'], [
                  'FatID', 'FatNum', 'Empresa', 'Controle'], [
                  its_qVol, vol_esp, vol_marca,
                  vol_nVol, its_pesoL, its_pesoB,
                  SeqInArq], [
                  FatID, FatNum, Empresa, Controle], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

{
                  nItem    := UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem);
                  UnMyXML.DefineXMLDoc(xmlDocB, xmlList.Item[0].XML);
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/prod');
                  if assigned(xmlNodeB) then
                  begin

                  end
}
                  //
                  xmlList.Remove(xmlList.Item[0]);
                  //
                end;
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            DADOS DA COBRAN�A                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/cobr/fat');
              if assigned(xmlNodeA) then
              begin
                cobr_fat_nfat        := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nFat            );
                cobr_fat_vOrig       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vOrig       ));
                cobr_fat_vDesc       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vDesc       ));
                cobr_fat_vLiq        := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vLiq        ));
              end;

(*
              _nDup    := EmptyStr;
              _dVenc   := '1899-12-31 12:00:00';
              _vDup   := 0.00;
*)
              xmlList := xmlDocA.SelectNodes(TagRaiz + '/infNFe/cobr/dup');
              if xmlList.Length > 0 then
              begin
                FatParcela := 0;
                QtdSQLY    := 0;
                SetLength(Arr_dVenc, xmlList.Length);
                SetLength(Arr_vDup,  xmlList.Length);
                while xmlList.Length > 0 do
                begin
                  FatParcela  := FatParcela + 1;
                  nDup      := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nDup);
                  dVenc     := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dVenc);
                  vDup      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_vDup));
                  //
                  Arr_dVenc[FatParcela - 1] := dVenc;
                  Arr_vDup[FatParcela - 1] := vDup;
                  //
                  int_nFat := Geral.IMV(Geral.SoNumero_TT('0' + cobr_fat_nfat));
                  int_nDup := Geral.IMV(Geral.SoNumero_TT('0' + nDup));
                  //
                  if LocalizaRegistro_nDup('nfecaby', FatID, FatNum, FatParcela,
                  Empresa, cobr_fat_nfat + '/' + nDup, dVenc, Controle, Lancto, Sub) then
                  begin
                    SQLTypeY := stUpd;
                    QtdSQLY := QtdSQLY + 1;
                  end else
                  begin
                    SQLTypeY := stIns;
                    Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
                    Lancto   := 0;
                    Sub      := 0;
                  end;
                  //
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeY, 'nfecaby', False, [
                  'FatID', 'FatNum', 'FatParcela',
                  'Empresa', 'nDup', 'dVenc',
                  'vDup', 'Lancto', 'Sub'], [
                  'Controle'], [
                  FatID, FatNum, FatParcela,
                  Empresa, nDup, dVenc,
                  vDup, Lancto, Sub], [
                  Controle], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  xmlList.Remove(xmlList.Item[0]);
                  //
                end;
                if (QtdSQLY = 0) and ((cobr_fat_vLiq > 0) or (FatParcela > 0)) then
                begin
                  Descricao := 'NF ' + Geral.FF0(ide_nNF);
                  if cobr_fat_nfat <> EmptyStr then
                    Descricao := Descricao + ' Fat ' + cobr_fat_nfat;
                  IC3_ED_FatNum  := FatNum;
                  IC3_ED_NF      := ide_nNF;
                  IC3_ED_Data    := Geral.ValidaData_YYYY_MM_DD(ide_dhEmi);
                  if (Length(Arr_dVenc) > 0) and (Arr_dVenc[0] <> EmptyStr) then
                    IC3_ED_Vencto  := Geral.ValidaData_YYYY_MM_DD(Arr_dVenc[0])
                  else
                    IC3_ED_Vencto  := Geral.ValidaData_YYYY_MM_DD(ide_dhEmi);
                  IC3_ED_SerieNF := Geral.FF0(ide_serie);
                  IC3_ED_Doc     := Geral.DMV('0' + Geral.SoNumero_TT(cobr_fat_nfat));
                  //
                  UPagtos.PgFatID_XML((*QrEmiss*)nil, TTipoPagto.tpDeb,
                  (*LocCod*)0, (*Terceiro*)CodInfoEmit, FatID, (*UpdGener*)0,
                  (*SQLType*)TSQLType.stIns, (*Titulo*) 'Entrada por XML',
                  (*Valor*)cobr_fat_vLiq, VAR_USUARIO, (*FatID_Sub*)0,
                  (*CliInt*)Empresa, TMinMax.mmAmbos, (*ValMin*)0,
                  (*ValMax*)0, (*Show*)True, (*Reconfig*)True,
                  (*Carteira*)0, FatParcela, (*Periodic*)1,
                  (*DiasParc*)-1, 'lct0001a',
                  Arr_dVenc,
                  Arr_vDup,
                  cobr_fat_nfat,
                  Descricao,
                  IntToStr(ide_Serie),
                  (*Qtde*)0,
                  (*Qtd2*)0);
                end;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            INFORMA��ES ADICIONAIS                                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv2, 'infAdic', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.infAdic', [], [
              'infAdFisco', 'infCpl'
              ]);
              //
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, TagRaiz + '/infNFe/infAdic');
              if assigned(xmlNodeA) then
              begin
                infAdic_infAdFisco := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_infAdFisco);
                infAdic_infCpl     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_infCpl);
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            CONSULTA NFe                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
///
              xmlNodeNivA := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNiv0, 'protNFe', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNivA, MeAvisos, 'nfeProc.protNFe',
              ['versao', 'xmlns'],
              ['infProt']);
              //
              xmlNodeNivB := UnMyXML.SelecionaUmNoDeNoh(NaoAvisa, xmlNodeNivA, 'infProt', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNivB, MeAvisos, 'nfeProc.protNFe.infProt',
              [],
              ['tpAmb', 'verAplic', 'chNFe', 'dhRecbto', 'nProt', 'digVal', 'cStat', 'xMotivo']);
              //
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, '/nfeProc/protNFe');
              if assigned(xmlNodeA) then
              begin
                protNFe_versao := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_versao));
              end;
              xmlNodeA := UnMyXML.SelecionaUmNoDeDoc(xmlDocA, '/nfeProc/protNFe/infProt');
              if assigned(xmlNodeA) then
              begin
                infProt_Id       := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
                Status           := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cStat));
                infProt_tpAmb    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpAmb   ));
                infProt_verAplic := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_verAplic);
                infProt_dhRecbto := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dhRecbto);
                if infProt_dhRecbto <> '' then
                begin
                  XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
                end else
                begin
                  infProt_dhRecbto := '0000-00-00 00:00:00';
                end;
                infProt_nProt    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nProt   );
                infProt_digVal   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_digVal  );
                infProt_cStat    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cStat   ));
                infProt_xMotivo  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMotivo );
              end else
                Geral.MB_Aviso('ATEN��O: tag de AUTORIZA��O n�o localizado no XML!' + sLineBreak +
                'Solicite ao forncedor o XML com a tag de autoriza��o!');

              //  Evitar erro de duplica��o
              //DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa);
              //
{
              // Modificado 2018/01/24
              / Zerar a data e colocar uma mensagem tamb�m adicionar no
              / NF-e_Pesq_0000 uma op��o de informar manualmente
              try
                if DataFiscal < 1000 then
                  if cab_DataS <> '' then
                    DataFiscal := dmkPF.ValidaDataDeSQL(cab_DataS, True) + 2;
                if DataFiscal < 1000 then
                  if cab_DataE <> '' then
                    DataFiscal := dmkPF.ValidaDataDeSQL(cab_DataE, True) + 2;
              finally
                ;
              end;
              if DataFiscal < 1000 then
                DataFiscal := Date;
              //

              if not DBCheck.ObtemData(DataFiscal, DataFiscal, 0, 0, False, 'Entrada no estoque') then
              begin
                Geral.MB_Aviso('Importa��o da NF-e atual foi abortada!' +
                  sLineBreak + 'S�rie: ' + Geral.FF0(ide_serie) + sLineBreak +
                  'N�mero: ' + Geral.FF0(ide_nNF));
                Exit;
              end;
              }
              //Valida dados de protocolo de autoriza��o
              if (SQLTypeA = stUpd) and (NFeCabA <> nil) then
              begin
                if Status = 0 then
                  Status := NFeCabA.Status;
                if protNFe_versao = 0 then
                  protNFe_versao := NFeCabA.protNFe_versao;
                if infProt_Id = '' then
                  infProt_Id := NFeCabA.infProt_Id;
                if infProt_tpAmb = 0 then
                  infProt_tpAmb := NFeCabA.infProt_tpAmb;
                if infProt_verAplic = '' then
                  infProt_verAplic := NFeCabA.infProt_verAplic;
                if (infProt_dhRecbto = '') or (infProt_dhRecbto = '0000-00-00 00:00:00') then
                  infProt_dhRecbto := Geral.FDT(NFeCabA.infProt_dhRecbto, 109);
                if infProt_nProt = '' then
                  infProt_nProt := NFeCabA.infProt_nProt;
                if infProt_digVal = '' then
                  infProt_digVal := NFeCabA.infProt_digVal;
                if infProt_cStat = 0 then
                  infProt_cStat := NFeCabA.infProt_cStat;
                if infProt_xMotivo = '' then
                  infProt_xMotivo := NFeCabA.infProt_xMotivo;
              end;
              //
              emit_CPF        := Geral.SoNumero_TT(emit_CPF);
              dest_CPF        := Geral.SoNumero_TT(dest_CPF);
              Transporta_CPF  := Geral.SoNumero_TT(Transporta_CPF);
              emit_CNPJ       := Geral.SoNumero_TT(emit_CNPJ);
              dest_CNPJ       := Geral.SoNumero_TT(dest_CNPJ);
              Transporta_CNPJ := Geral.SoNumero_TT(Transporta_CNPJ);
              //
              if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfecaba', False, [
              'IDCtrl', 'LoteEnv', 'versao',
              'Id', 'ide_cUF', 'ide_cNF',
              'ide_natOp', 'ide_indPag', 'ide_mod',
              'ide_serie', 'ide_nNF', 'ide_dEmi',
              'ide_dSaiEnt', 'ide_tpNF', 'ide_cMunFG',
              'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
              'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
              'ide_verProc', 'emit_CNPJ', 'emit_CPF',
              'emit_xNome', 'emit_xFant', 'emit_xLgr',
              'emit_nro', 'emit_xCpl', 'emit_xBairro',
              'emit_cMun', 'emit_xMun', 'emit_UF',
              'emit_CEP', 'emit_cPais', 'emit_xPais',
              'emit_fone', 'emit_IE', 'emit_IEST',
              'emit_IM', 'emit_CNAE', 'dest_CNPJ',
              'dest_CPF', 'dest_xNome', 'dest_xLgr',
              'dest_nro', 'dest_xCpl', 'dest_xBairro',
              'dest_cMun', 'dest_xMun', 'dest_UF',
              'dest_CEP', 'dest_cPais', 'dest_xPais',
              'dest_fone', 'dest_IE', 'dest_ISUF',
              'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
              'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
              'ICMSTot_vSeg', 'ICMSTot_vDesc', 'ICMSTot_vII',
              'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
              'ICMSTot_vOutro', 'ICMSTot_vNF', 'ISSQNtot_vServ',
              'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
              'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
              'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
              'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
              'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
              'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
              'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
              'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
              'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
              'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
              'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
              'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
              'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
              'Status', 'infProt_Id', 'infProt_tpAmb',
              'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
              'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
              'protNFe_versao',
              (*
              'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
              'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
              'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
              'infCanc_xJust', '_Ativo_', 'FisRegCad',
              'CartEmiss', 'TabelaPrc', 'CondicaoPg',
              'CliFor', 'FreteExtra', 'SegurExtra',
              *)
              'ICMSRec_pRedBC', 'ICMSRec_vBC', 'ICMSRec_pAliq',
              'ICMSRec_vICMS', 'IPIRec_pRedBC', 'IPIRec_vBC',
              'IPIRec_pAliq', 'IPIRec_vIPI', 'PISRec_pRedBC',
              'PISRec_vBC', 'PISRec_pAliq', 'PISRec_vPIS',
              'COFINSRec_pRedBC', 'COFINSRec_vBC', 'COFINSRec_pAliq',
              'COFINSRec_vCOFINS',
              'CodInfoEmit', 'CodInfoDest', 'DataFiscal',
              // 3.10
              'ide_hEmi', 'ide_dhEmiTZD', 'ide_idDest',
              'ide_hSaiEnt', 'ide_dhSaiEntTZD', 'ide_indFinal',
              'ide_indPres', 'ide_dhCont', 'ide_xJust',
              'emit_CRT', 'dest_idEstrangeiro', 'dest_indIEDest',
              'dest_IM', 'dest_email', 'ICMSTot_vICMSDeson',
              'vTotTrib',
              // FIM 3.10
              // ini 4.00
              'ide_indIntermed',
              'ICMSTot_vFCPUFDest', 'ICMSTot_vICMSUFDest', 'ICMSTot_vICMSUFRemet',
              'ICMSTot_vFCP', 'ICMSTot_vFCPST', 'ICMSTot_vFCPSTRet', 'ICMSTot_vIPIDevol'
              // fim 4.00
              ], [
              'FatID', 'FatNum', 'Empresa'], [
              IDCtrl, LoteEnv, infNFe_versao,
              cab_refNFe, ide_cUF, ide_cNF,
              ide_natOp, ide_indPag, ide_mod,
              ide_serie, ide_nNF, ide_dEmi,
              ide_dSaiEnt, ide_tpNF, ide_cMunFG,
              ide_tpImp, ide_tpEmis, ide_cDV,
              ide_tpAmb, ide_finNFe, ide_procEmi,
              ide_verProc, emit_CNPJ, emit_CPF,
              emit_xNome, emit_xFant, emit_xLgr,
              emit_nro, emit_xCpl, emit_xBairro,
              emit_cMun, emit_xMun, emit_UF,
              emit_CEP, emit_cPais, emit_xPais,
              emit_fone, emit_IE, emit_IEST,
              emit_IM, emit_CNAE, dest_CNPJ,
              dest_CPF, dest_xNome, dest_xLgr,
              dest_nro, dest_xCpl, dest_xBairro,
              dest_cMun, dest_xMun, dest_UF,
              dest_CEP, dest_cPais, dest_xPais,
              dest_fone, dest_IE, dest_ISUF,
              ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
              ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
              ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,
              ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
              ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,
              ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
              ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
              RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
              RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
              Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
              Transporta_IE, Transporta_XEnder, Transporta_XMun,
              Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
              RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
              RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
              VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
              Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
              InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
              Compra_XNEmp, Compra_XPed, Compra_XCont,
              Status, infProt_Id, infProt_tpAmb,
              infProt_verAplic, infProt_dhRecbto, infProt_nProt,
              infProt_digVal, infProt_cStat, infProt_xMotivo,
              protNFe_versao,
              (*
              infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
              infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
              infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
              infCanc_xJust, _Ativo_, FisRegCad,
              CartEmiss, TabelaPrc, CondicaoPg,
              CliFor, FreteExtra, SegurExtra,
              *)
              ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq,
              ICMSRec_vICMS, IPIRec_pRedBC, IPIRec_vBC,
              IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
              PISRec_vBC, PISRec_pAliq, PISRec_vPIS,
              COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
              COFINSRec_vCOFINS,
              CodInfoEmit, Empresa(*CodInfoDest*), DataFiscal,
              //
              ide_hEmi, ide_dhEmiTZD, ide_idDest,
              ide_hSaiEnt, ide_dhSaiEntTZD, ide_indFinal,
              ide_indPres, ide_dhCont, ide_xJust,
              emit_CRT, dest_idEstrangeiro, dest_indIEDest,
              dest_IM, dest_email, ICMSTot_vICMSDeson,
              ICMSTot_vTotTrib,
              //
              ide_indIntermed,
              ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet,
              ICMSTot_vFCP, ICMSTot_vFCPST, ICMSTot_vFCPSTRet, ICMSTot_vIPIDevol
              //
              ], [
              FatID, FatNum, Empresa], True) then
              begin
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                Exit;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            H. Detalhamento Produtos e Servi�os                             //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              //
              xmlNodeNiv3 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv2, 'det', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv3, MeAvisos, 'nfeProc.NFe.infNFe.det',
              ['nItem'],
              ['prod', 'imposto', 'infAdProd']);
              //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            I. Produtos e Servi�os                                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              try
              //
              xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv3, 'prod', MeAvisos);
              Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.det.prod',
              [''],
(*
              ['cProd', 'cEAN', 'xProd', 'NCM', 'CEST', 'CFOP', 'uCom', 'qCom',
               'vUnCom', 'vProd', 'cEANTrib', 'uTrib', 'qTrib', 'vUnTrib',
               'indTot', 'xPed', 'nFCI']);
*)
              ['cProd', 'cEAN', 'xProd', 'NCM', (*NVE,*) 'CEST', 'indEscala',
              'CNPJFab', 'cBenef', 'EXTIPI', 'CFOP', 'uCom', 'qCom', 'vUnCom',
              'vProd', 'cEANTrib', 'uTrib', 'qTrib', 'vUnTrib', 'vFrete',
              'vSeg', 'vDesc', 'vOutro', 'indTot', (*DI, detExport*)
              'xPed', 'nItemPed', 'nFCI'(*, rastro*)]);
              //
              xmlList := xmlDocA.SelectNodes(TagRaiz + '/infNFe/det');
              if xmlList.Length > 0 then
              begin
                while xmlList.Length > 0 do
                begin
                  prod_cProd         := '';
                  prod_cEAN          := '';
                  prod_xProd         := '';
                  prod_NCM           := '';
                  prod_EXTIPI        := '';
                  prod_Genero        := 0;
                  prod_CFOP          := 0;
                  prod_uCom          := '';
                  prod_qCom          := 0;
                  prod_vUnCom        := 0;
                  prod_vProd         := 0;
                  prod_cEANTrib      := '';
                  prod_uTrib         := '';
                  prod_qTrib         := 0;
                  prod_vUnTrib       := 0;
                  prod_vFrete        := 0;
                  prod_vSeg          := 0;
                  prod_vDesc         := 0;
                  prod_vOutro        := 0;
                  prod_indEscala     := '';
                  prod_CNPJFab       := '';
                  prod_cBenef        := '';
                  prod_nItemPed      := 0;

                  ICMS_orig          := 0;
                  ICMS_CST           := 0;
                  ICMS_modBC         := 0;
                  ICMS_vBC           := 0;
                  ICMS_pRedBC        := 0;
                  ICMS_pICMS         := 0;
                  ICMS_vICMS         := 0;
                  ICMS_modBCST       := 0;
                  ICMS_pMVAST        := 0;
                  ICMS_pRedBCST      := 0;
                  ICMS_vBCST         := 0;
                  ICMS_pICMSST       := 0;
                  ICMS_vICMSST       := 0;

                  IPI_cEnq           := '';
                  IPI_CST            := 0;
                  IPI_clEnq          := '';
                  IPI_CNPJProd       := '';
                  IPI_cSelo          := '';
                  IPI_qSelo          := 0;
                  IPI_cEnq           := '';
                  IPI_vBC            := 0;
                  IPI_qUnid          := 0;
                  IPI_vUnid          := 0;
                  IPI_pIPI           := 0;
                  IPI_vIPI           := 0;

                  PIS_CST            := 0;
                  PIS_vBC            := 0;
                  PIS_pPIS           := 0;
                  PIS_vPIS           := 0;
                  PIS_qBCProd        := 0;
                  PIS_vAliqProd      := 0;
                  PISST_CST          := '';
                  PISST_vBC          := 0;
                  PISST_pPIS         := 0;
                  PISST_vPIS         := 0;
                  PISST_qBCProd      := 0;
                  PISST_vAliqProd    := 0;

                  COFINS_CST         := 0;
                  COFINS_vBC         := 0;
                  COFINS_pCOFINS     := 0;
                  COFINS_vCOFINS     := 0;
                  COFINS_qBCProd     := 0;
                  COFINS_vAliqProd   := 0;
                  COFINSST_CST       := '';
                  COFINSST_vBC       := 0;
                  COFINSST_pCOFINS   := 0;
                  COFINSST_vCOFINS   := 0;
                  COFINSST_qBCProd   := 0;
                  COFINSST_vAliqProd := 0;

                  nItem    := Geral.IMV(UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem));
                  UnMyXML.DefineXMLDoc(xmlDocB, xmlList.Item[0].XML);
                  //
                  xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlList.Item[0], 'prod', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.det.prod',
                  [''],
                  ['cProd', 'cEAN', 'xProd', 'NCM', (*NVE,*) 'CEST', 'indEscala',
                  'CNPJFab', 'cBenef', 'EXTIPI', 'CFOP', 'uCom', 'qCom', 'vUnCom',
                  'vProd', 'cEANTrib', 'uTrib', 'qTrib', 'vUnTrib', 'vFrete',
                  'vSeg', 'vDesc', 'vOutro', 'indTot', (*DI, detExport*)
                  'xPed', 'nItemPed', 'nFCI'(*, rastro*)]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/prod');
                  if assigned(xmlNodeB) then
                  begin
                    prod_cProd     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cProd    );
                    prod_cEAN      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEAN     );
                    prod_xProd     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_xProd    );
                    prod_NCM       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_NCM      );
                    prod_EXTIPI    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_EXTIPI  );
                    prod_Genero    := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_Genero   ));
                    prod_CFOP      := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CFOP     ));
                    prod_uCom      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_uCom     );
                    prod_qCom      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qCom     ));
                    prod_vUnCom    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnCom   ));
                    prod_vProd     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vProd    ));
                    prod_cEANTrib  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEANTrib );
                    prod_uTrib     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_uTrib    );
                    prod_qTrib     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qTrib    ));
                    prod_vUnTrib   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnTrib  ));
                    prod_vFrete    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vFrete   ));
                    prod_vSeg      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vSeg     ));
                    prod_vDesc     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vDesc    ));
                    prod_vOutro    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vOutro   ));
                    prod_indEscala := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_indEscala               );
                    prod_CNPJFab   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CNPJFab                 );
                    prod_cBenef    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cBenef                  );
                    prod_nItemPed  := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_nItemPed     ));
                  end else begin
                    prod_cProd     := '';
                    prod_cEAN      := '';
                    prod_xProd     := '';
                    prod_NCM       := '';
                    prod_EXTIPI    := '';
                    prod_Genero    := 0;
                    prod_CFOP      := 0;
                    prod_uCom      := '';
                    prod_qCom      := 0.00;
                    prod_vUnCom    := 0.00;
                    prod_vProd     := 0.00;
                    prod_cEANTrib  := '';
                    prod_uTrib     := '';
                    prod_qTrib     := 0.00;
                    prod_vUnTrib   := 0.00;
                    prod_vFrete    := 0.00;
                    prod_vSeg      := 0.00;
                    prod_vDesc     := 0.00;
                    prod_vOutro    := 0.00;
                    prod_indEscala := '';
                    prod_CNPJFab   := '';
                    prod_cBenef    := '';
                    prod_nItemPed  := 0;
                  end;

                  xmlNodeNiv4 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlList.Item[0], 'imposto', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv4, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto',
                  [''],
                  ['vTotTrib', 'ICMS', 'ICMSUFDest', 'IPI', 'II', 'PIS',
                  'PISST', 'COFINS', 'COFINSST', 'ISSQN']);
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto');
                  if assigned(xmlNodeB) then
                  begin
                    vTotTrib := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vTotTrib));
                  end;

                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'ICMS', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.ICMS',
                  [''],
                  ['ICMS00', 'ICMS10', 'ICMS20', 'ICMS30', 'ICMS40', 'ICMS51',
                   'ICMS60', 'ICMS70', 'ICMS90', 'ICMSPart', 'ICMSST',
                   'ICMSSN101', 'ICMSSN102', 'ICMSSN201', 'ICMSSN202',
                   'ICMSSN500', 'ICMSSN900']);
                  //
                  ICMS_orig        := 0;
                  ICMS_CST         := 0;
                  ICMS_modBC       := 0;
                  ICMS_vBC         := 0.00;
                  ICMS_pRedBC      := 0.00;
                  ICMS_pICMS       := 0.00;
                  ICMS_vICMS       := 0.00;
                  ICMS_modBCST     := 0;
                  ICMS_pMVAST      := 0.00;
                  ICMS_pRedBCST    := 0.00;
                  ICMS_vBCST       := 0.00;
                  ICMS_pICMSST     := 0.00;
                  ICMS_vICMSST     := 0.00;
                  // CRT=1
                  ICMS_CSOSN       := 0;
                  ICMS_pCredSN     := 0.00;
                  ICMS_vCredICMSSN := 0.00;
                  ICMS_vBCFCPST    := 0.00;
                  ICMS_pFCPST      := 0.00;
                  ICMS_vFCPST      := 0.00;
                  //
                  ICMS_UFST        := '';
                  ICMS_pBCOp       := 0.00;
                  ICMS_vBCSTRet    := 0.00;
                  ICMS_pST         := 0.00;
                  ICMS_vICMSSTRet  := 0.00;
                  ICMS_vICMSDeson  := 0.00;
                  ICMS_vBCFCPSTRet := 0.00;
                  ICMS_pFCPSTRet   := 0.00;
                  ICMS_vFCPSTRet   := 0.00;
                  ICMS_motDesICMS  := 0;
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS00');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS10');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS20');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS30');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS40');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS41');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS50');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS51');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS60');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS70');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMS90');
                  //
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN101');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN102');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN201');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN202');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN500');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/ICMS/ICMSSN900');
                  //
                  if assigned(xmlNodeB) then
                  begin
                    ICMS_orig        := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_orig        ));
                    ICMS_CST         := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST         ));
                    ICMS_modBC       := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_modBC       ));
                    ICMS_vBC         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC     ));
                    ICMS_pRedBC      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pRedBC  ));
                    ICMS_pICMS       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pICMS   ));
                    ICMS_vICMS       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMS   ));
                    ICMS_modBCST     := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_modBCST     ));
                    ICMS_pMVAST      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pMVAST  ));
                    ICMS_pRedBCST    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pRedBCST));
                    ICMS_vBCST       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBCST   ));
                    ICMS_pICMSST     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pICMSST ));
                    ICMS_vICMSST     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMSST ));
                    //
                    // CRT=1
                    ICMS_CSOSN       := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr,     ttx_CSOSN       ));
                    ICMS_pCredSN     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pCredSN     ));
                    ICMS_vCredICMSSN := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCredICMSSN ));
                    ICMS_vBCFCPST    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBCFCPST    ));
                    ICMS_pFCPST      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pFCPST      ));
                    ICMS_vFCPST      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCPST       ));
                    //
                    ICMS_UFST        :=               UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUFST       );
                    ICMS_pBCOp       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pBCOp       ));
                    ICMS_vBCSTRet    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBCSTRet    ));
                    ICMS_pST         := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pST         ));
                    ICMS_vICMSSTRet  := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMSSTRet  ));
                    ICMS_vICMSDeson  := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMSDeson  ));
                    ICMS_vBCFCPSTRet := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBCFCPSTRet ));
                    ICMS_pFCPSTRet   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pFCPSTRet   ));
                    ICMS_vFCPSTRet   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vFCPSTRet   ));
                    ICMS_motDesICMS  := Geral.IMV(    UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_motDesICMS  ));
                    //
                  end;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///               IPI
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'IPI', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.IPI',
                  [''],
                  [
                  'clEnq', 'CNPJProd', 'cSelo', 'qSelo', 'cEnq', 'IPITrib', 'IPINT'
                  ]);//  clEnq: Deprecado.
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/IPI');
                  if assigned(xmlNodeB) then
                  begin
                    IPI_clEnq    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_clEnq   );
                    IPI_CNPJProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CNPJProd);
                    IPI_cSelo    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cSelo   );
                    IPI_qSelo    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qSelo   ));
                    IPI_cEnq     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEnq);
                  end else begin
                    IPI_clEnq    := '';
                    IPI_CNPJProd := '';
                    IPI_cSelo    := '';
                    IPI_qSelo    := 0;
                    IPI_cEnq     := '';
                  end;
                  xmlNodeNiv6 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv5, 'IPITrib', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv6, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.IPI.IPITrib',
                  [''],
                  [
                  'CST', 'vBC', 'pIPI', 'qUnid', 'vUnid', 'vIPI'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/IPI/IPITrib');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/IPI/IPINT');
                  if assigned(xmlNodeB) then
                  begin
                    IPI_CST      := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST));
                    IPI_vBC      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    IPI_qUnid    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qUnid));
                    IPI_vUnid    := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnid));
                    IPI_pIPI     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pIPI));
                    IPI_vIPI     := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vIPI));
                  end else begin
                    IPI_CST      := 0;
                    IPI_vBC      := 0;
                    IPI_qUnid    := 0;
                    IPI_vUnid    := 0;
                    IPI_pIPI     := 0;
                    IPI_vIPI     := 0;
                  end;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///             II - Imposto de Importa��o
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'II', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.II',
                  [''],
                  [
                  'vBC', 'vDespAdu', 'vII', 'vIOF'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/II');
                  if assigned(xmlNodeB) then
                  begin
                    Tem_II       := True;
                    II_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC     ));
                    II_vDespAdu  := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vDespAdu));
                    II_vII       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vII     ));
                    II_vIOF      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vIOF    ));
                  end else
                    Tem_II := False;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///             PIS -
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'PIS', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.PIS',
                  [''],
                  [
                  'PISAliq', 'PISQtde', 'PISNT'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/PIS/PISAliq');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/PIS/PISQtde');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/PIS/PISNT');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/PIS/PISOutr');
                  if assigned(xmlNodeB) then
                  begin
                    PIS_CST       := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST));
                    PIS_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    PIS_pPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS));
                    PIS_vPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS));
                    PIS_qBCProd   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd));
                    PIS_vAliqProd := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd));
                  end else begin
                    PIS_CST       := 0;
                    PIS_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    PIS_pPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS));
                    PIS_vPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS));
                    PIS_qBCProd   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd));
                    PIS_vAliqProd := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd));
                  end;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///               PISST
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'PISST', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.PISST',
                  [''],
                  [
                  'vBC', 'pPIS', 'qBCProd', 'vAliqProd', 'vPIS'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/PISST');
                  if assigned(xmlNodeB) then
                  begin
                    PISST_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    PISST_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    PISST_pPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS));
                    PISST_vPIS      := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS));
                    PISST_qBCProd   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd));
                    PISST_vAliqProd := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd));
                  end else begin
                    PISST_CST       := '';
                    PISST_vBC       := 0;
                    PISST_pPIS      := 0;
                    PISST_vPIS      := 0;
                    PISST_qBCProd   := 0;
                    PISST_vAliqProd := 0;
                  end;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///               COFINS
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'COFINS', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.COFINS',
                  [''],
                  [
                  'COFINSAliq', 'COFINSQtde', 'COFINSNT', 'COFINSOutr'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/COFINS/COFINSAliq');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/COFINS/COFINSQtde');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/COFINS/COFINSNT');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/COFINS/COFINSOutr');
                  if assigned(xmlNodeB) then
                  begin
                    COFINS_CST       := Geral.IMV(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST));
                    COFINS_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    COFINS_pCOFINS   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pCOFINS));
                    COFINS_vCOFINS   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCOFINS));
                    COFINS_qBCProd   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd));
                    COFINS_vAliqProd := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd));
                  end else begin
                    COFINS_CST       := 0;
                    COFINS_vBC       := 0;
                    COFINS_pCOFINS   := 0;
                    COFINS_vCOFINS   := 0;
                    COFINS_qBCProd   := 0;
                    COFINS_vAliqProd := 0;
                  end;

                  //

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///               COFINSST
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  xmlNodeNiv5 := UnMyXML.SelecionaUmNoDeNoh(Avisa, xmlNodeNiv4, 'COFINSST', MeAvisos);
                  Ausentes := Ausentes + UnMyXML.Teste(Atencao, xmlNodeNiv5, MeAvisos, 'nfeProc.NFe.infNFe.det.imposto.COFINSST',
                  [''],
                  [
                  'vBC', 'pCOFINS', 'qBCProd', 'vAliqProd', 'vCOFINS'
                  ]);
                  //
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/imposto/COFINSST');
                  if assigned(xmlNodeB) then
                  begin
                    COFINSST_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    COFINSST_vBC       := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC));
                    COFINSST_pCOFINS   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pCOFINS));
                    COFINSST_vCOFINS   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCOFINS));
                    COFINSST_qBCProd   := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd));
                    COFINSST_vAliqProd := Geral.DMV_Dot(UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd));
                  end else begin
                    COFINSST_CST       := '';
                    COFINSST_vBC       := 0;
                    COFINSST_pCOFINS   := 0;
                    COFINSST_vCOFINS   := 0;
                    COFINSST_qBCProd   := 0;
                    COFINSST_vAliqProd := 0;
                  end;

                  //
                  // Ausentes j� verificado acima!
                  xmlNodeB := UnMyXML.SelecionaUmNoDeDoc(xmlDocB, '/det/infAdProd');
                  if assigned(xmlNodeB) then
                  begin
                    infAdProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_infAdProd);
                  end else
                  begin
                    infAdProd := '';
                  end;
                  //
                  DmNFe_0000.QrCod.Close;
                  DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
                  DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
                  UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, 'TNFeImporta_0400.ImportaDadosNFeDeXML()');
                  //
                  Nivel1 := 0;
                  case DmNFe_0000.QrCod.RecordCount of
                    0:
                    begin
                      if CO_DMKID_APP <> 2 then //Bluederm => O Marco n�o quer que configure o produto
                      begin                                                     VAR_NOME_NOVO_GG1 := prod_xProd;
                        Grade_Jan.MostraFormGraGruEIts(stIns, CodInfoEmit,
                          prod_cProd, prod_xProd, prod_cEAN, prod_NCM, prod_uCom,
                          infAdProd, prod_CFOP, ICMS_Orig, ICMS_CST, IPI_CST,
                          PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq,
                          GraGruEIts_GraGruX, GraGruEIts_NomeGGX);
                        //
                        VAR_NOME_NOVO_GG1 := EmptyStr;
                        //
                        DmNFe_0000.QrCod.Close;
                        DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
                        DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
                        UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, 'TNFeImporta_0400.ImportaDadosNFeDeXML()');
                        //
                        Nivel1 := DmNFe_0000.QrCodNivel1.Value;
                      end else
                        Nivel1 := 0;
                    end;
                    else
                      Nivel1 := DmNFe_0000.QrCodNivel1.Value;
                  end;
                  //
                  if (Nivel1 = 0) and (CO_DMKID_APP <> 2) then //Bluederm
                    if ExcluirEntrada(Form, FatID, FatNum, Empresa, True) then Exit;
                  //
                  xmlList.Remove(xmlList.Item[0]);
                  //
                  Conta          := nItem;
                  its_PesoVB     := 0;
                  its_PesoVL     := prod_qCom;
                  its_ValorItem  := prod_vProd;
                  its_RIPI       := 0;
                  its_CFin       := 0;
                  its_Seguro     := prod_vSeg;
                  its_Frete      := prod_vFrete;
                  its_Desconto   := prod_vDesc;
                  its_TotalCusto := its_ValorItem - its_Desconto + IPI_vIPI;
                  its_TotalPeso  := its_PesoVL;
                  //
                  ICMSRec_pRedBC := DmNFe_0000.QrCodICMSRec_pRedBC.Value;
                  ICMSRec_vBC    := (100 - ICMSRec_pRedBC) * its_ValorItem / 100;

                  if DmNFe_0000.QrCodICMSRec_tCalc.Value = 1 then
                    ICMSRec_pAliq := ICMS_pICMS
                  else
                    ICMSRec_pAliq := DmNFe_0000.QrCodICMSRec_pAliq.Value;

                  ICMSRec_vICMS      := ICMSRec_vBC * ICMSRec_pAliq / 100;
                  //tot_Rec_ICMS       := tot_Rec_ICMS + ICMSRec_vICMS;
                  //
                  IPIRec_pRedBC      := DmNFe_0000.QrCodIPIRec_pRedBC.Value;
                  IPIRec_vBC         := (100 - IPIRec_pRedBC) * its_ValorItem / 100;
                  if DmNFe_0000.QrCodIPIRec_tCalc.Value = 1 then
                    IPIRec_pAliq      := IPI_pIPI
                  else
                    IPIRec_pAliq      := DmNFe_0000.QrCodIPIRec_pAliq.Value;
                  IPIRec_vIPI        := IPIRec_vBC * IPIRec_pAliq / 100;
                  //tot_Rec_IPI        := tot_Rec_IPI + IPIRec_vIPI;
                  //
                  PISRec_pRedBC      := DmNFe_0000.QrCodPISRec_pRedBC.Value;
                  PISRec_vBC         := (100 - PISRec_pRedBC) * its_ValorItem / 100;
                  if DmNFe_0000.QrCodPISRec_tCalc.Value = 1 then
                    PISRec_pAliq      := PIS_pPIS
                  else
                    PISRec_pAliq      := DmNFe_0000.QrCodPISRec_pAliq.Value;
                  PISRec_vPIS        := PISRec_vBC * PISRec_pAliq / 100;
                  //tot_Rec_PIS        := tot_Rec_PIS + PISRec_vPIS;
                  //
                  COFINSRec_pRedBC   := DmNFe_0000.QrCodCOFINSRec_pRedBC.Value;
                  COFINSRec_vBC      := (100 - COFINSRec_pRedBC) * its_ValorItem / 100;
                  if DmNFe_0000.QrCodCOFINSRec_tCalc.Value = 1 then
                    COFINSRec_pAliq      := COFINS_pCOFINS
                  else
                    COFINSRec_pAliq      := DmNFe_0000.QrCodCOFINSRec_pAliq.Value;
                  COFINSRec_vCOFINS  := COFINSRec_vBC * COFINSRec_pAliq / 100;
                  //tot_Rec_COFINS     := tot_Rec_ICMS + ICMSRec_vICMS;

                  {
                  Controle := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', stIns, 0);
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
                  'Codigo', 'Conta', 'Insumo',
                  'Volumes', 'PesoVB', 'PesoVL',
                  'ValorItem', 'IPI', 'RIPI',
                  'CFin', 'ICMS', 'RICMS',
                  'TotalCusto', 'TotalPeso', 'prod_cProd',
                  'prod_cEAN', 'prod_xProd', 'prod_NCM',
                  'prod_EX_TIPI', 'prod_genero', 'prod_CFOP',
                  'prod_uCom', 'prod_qCom', 'prod_vUnCom',
                  'prod_vProd', 'prod_cEANTrib', 'prod_uTrib',
                  'prod_qTrib', 'prod_vUnTrib', 'prod_vFrete',
                  'prod_vSeg', 'prod_vDesc', 'ICMS_Orig',
                  'ICMS_CST', 'ICMS_modBC', 'ICMS_pRedBC',
                  'ICMS_vBC', 'ICMS_pICMS', 'ICMS_vICMS',
                  'ICMS_modBCST', 'ICMS_pMVAST', 'ICMS_pRedBCST',
                  'ICMS_vBCST', 'ICMS_pICMSST', 'ICMS_vICMSST',
                  'IPI_cEnq', 'IPI_CST', 'IPI_vBC',
                  'IPI_qUnid', 'IPI_vUnid', 'IPI_pIPI',
                  'IPI_vIPI', 'PIS_CST', 'PIS_vBC',
                  'PIS_pPIS', 'PIS_vPIS', 'PIS_qBCProd',
                  'PIS_vAliqProd', 'PISST_vBC', 'PISST_pPIS',
                  'PISST_qBCProd', 'PISST_vAliqProd', 'PISST_vPIS',
                  'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
                  'COFINS_qBCProd', 'COFINS_vAliqProd', 'COFINS_vCOFINS',
                  'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
                  'COFINSST_vAliqProd', 'COFINSST_vCOFINS'], [
                  'Controle'], [
                  Codigo, Conta, Produto,
                  its_Volumes, its_PesoVB, its_PesoVL,
                  its_ValorItem, its_IPI, its_RIPI,
                  its_CFin, its_ICMS, its_RICMS,
                  its_TotalCusto, its_TotalPeso, prod_cProd,
                  prod_cEAN, prod_xProd, prod_NCM,
                  prod_EX_TIPI, prod_genero, prod_CFOP,
                  prod_uCom, prod_qCom, prod_vUnCom,
                  prod_vProd, prod_cEANTrib, prod_uTrib,
                  prod_qTrib, prod_vUnTrib, prod_vFrete,
                  prod_vSeg, prod_vDesc, ICMS_Orig,
                  ICMS_CST, ICMS_modBC, ICMS_pRedBC,
                  ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
                  ICMS_modBCST, ICMS_pMVAST, ICMS_pRedBCST,
                  ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST,
                  IPI_cEnq, IPI_CST, IPI_vBC,
                  IPI_qUnid, IPI_vUnid, IPI_pIPI,
                  IPI_vIPI, PIS_CST, PIS_vBC,
                  PIS_pPIS, PIS_vPIS, PIS_qBCProd,
                  PIS_vAliqProd, PISST_vBC, PISST_pPIS,
                  PISST_qBCProd, PISST_vAliqProd, PISST_vPIS,
                  COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
                  COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS,
                  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
                  COFINSST_vAliqProd, COFINSST_vCOFINS], [
                  Controle], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  }

                  //

                  if IPI_vIPI <> 0 then
                     Tem_IPI := 1
                   else
                     Tem_IPI := 0;
                  _Ativo_ := 1;
                  InfAdCuztm := 0;
                  EhServico := 0;
                  // N�o se sabe ainda!
                  MeuID := 0;

                  if LocalizaRegistro_nItem('nfeitsi', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsi', False, [
                    'prod_cProd', 'prod_cEAN', 'prod_xProd',
                    'prod_NCM', 'prod_EXTIPI', 'prod_genero',
                    'prod_CFOP', 'prod_uCom', 'prod_qCom',
                    'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
                    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
                    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
                    'Tem_IPI', '_Ativo_', 'InfAdCuztm',
                    'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
                    'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
                    'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
                    'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
                    'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
                    'COFINSRec_pAliq', 'COFINSRec_vCOFINS',
                    'MeuID', 'Nivel1',
                    // ini 2021-03-07
                    'prod_indEscala', 'prod_CNPJFab', 'prod_cBenef',
                    'prod_vOutro', 'prod_nItemPed'
                    // fim 2021-03-07
                    ], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    prod_cProd, prod_cEAN, prod_xProd,
                    prod_NCM, prod_EXTIPI, prod_genero,
                    prod_CFOP, prod_uCom, prod_qCom,
                    prod_vUnCom, prod_vProd, prod_cEANTrib,
                    prod_uTrib, prod_qTrib, prod_vUnTrib,
                    prod_vFrete, prod_vSeg, prod_vDesc,
                    Tem_IPI, _Ativo_, InfAdCuztm,
                    EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
                    ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
                    IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
                    PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
                    PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
                    COFINSRec_pAliq, COFINSRec_vCOFINS,
                    MeuID, Nivel1,
                    // ini 2021-03-07
                    prod_indEscala, prod_CNPJFab, prod_cBenef,
                    prod_vOutro, prod_nItemPed
                    // fim 2021-03-07
                    ], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if vTotTrib > 0 then
                  begin
                    if LocalizaRegistro_nItem('nfeitsm', FatID, FatNum, Empresa, nItem) then
                      SQLTypeA := stUpd
                    else
                      SQLTypeA := stIns;

                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsm', False, [
                      'vTotTrib', 'Ativo'], [
                      'FatID', 'FatNum', 'Empresa', 'nItem'], [
                      vTotTrib, _Ativo_], [
                      FatID, FatNum, Empresa, nItem], True) then
                    begin
                      //ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      //Exit;
                    end;
                  end;
                  //
                  if LocalizaRegistro_nItem('nfeitsn', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsn', False, [
                    'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
                    'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
                    'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
                    'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
                    'ICMS_vICMSST', 'ICMS_CSOSN', 'ICMS_pCredSN',
                    'ICMS_vCredICMSSN', 'ICMS_vBCFCPST', 'ICMS_pFCPST',
                    'ICMS_vFCPST', 'ICMS_UFST', 'ICMS_pBCOp',
                    'ICMS_vBCSTRet', 'ICMS_pST', 'ICMS_vICMSSTRet',
                    'ICMS_vICMSDeson', 'ICMS_vBCFCPSTRet', 'ICMS_pFCPSTRet',
                    'ICMS_vFCPSTRet', 'ICMS_motDesICMS',
                     '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    ICMS_Orig, ICMS_CST, ICMS_modBC,
                    ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
                    ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
                    ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
                    ICMS_vICMSST, ICMS_CSOSN, ICMS_pCredSN,
                    ICMS_vCredICMSSN, ICMS_vBCFCPST, ICMS_pFCPST,
                    ICMS_vFCPST, ICMS_UFST, ICMS_pBCOp,
                    ICMS_vBCSTRet, ICMS_pST, ICMS_vICMSSTRet,
                    ICMS_vICMSDeson, ICMS_vBCFCPSTRet, ICMS_pFCPSTRet,
                    ICMS_vFCPSTRet, ICMS_motDesICMS,
                    _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  if IPI_vIPI <> 0 then
                  begin
                    if LocalizaRegistro_nItem('nfeitso', FatID, FatNum, Empresa, nItem) then
                      SQLTypeA := stUpd
                    else
                      SQLTypeA := stIns;

                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitso', False, [
                      'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
                      'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
                      'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',
                      'IPI_pIPI', 'IPI_vIPI', '_Ativo_'], [
                      'FatID', 'FatNum', 'Empresa', 'nItem'], [
                      IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
                      IPI_qSelo, IPI_cEnq, IPI_CST,
                      IPI_vBC, IPI_qUnid, IPI_vUnid,
                      IPI_pIPI, IPI_vIPI, _Ativo_], [
                      FatID, FatNum, Empresa, nItem], True) then
                    begin
                      ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      Exit;
                    end;
                  end;
                  //
                  if Tem_II then
                  begin
                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsp', False, [
                    'II_vBC', 'II_vDespAdu', 'II_vII', 'II_vIOF',
                    'Ativo'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    II_vBC, II_vDespAdu, II_vII,
                    II_vIOF, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                    begin
                      //ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      //Exit;
                    end;
                  end;
                  //

                  if LocalizaRegistro_nItem('nfeitsq', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsq', False, [
                    'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
                    'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
                    '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    PIS_CST, PIS_vBC, PIS_pPIS,
                    PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
                    _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if LocalizaRegistro_nItem('nfeitsr', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsr', False, [
                    'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
                    'PISST_vAliqProd', 'PISST_vPIS', '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    PISST_vBC, PISST_pPIS, PISST_qBCProd,
                    PISST_vAliqProd, PISST_vPIS, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if LocalizaRegistro_nItem('nfeitss', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitss', False, [
                    'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
                    'COFINS_vCOFINS', 'COFINS_qBCProd', 'COFINS_vAliqProd',
                    '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
                    COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd,
                    _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if LocalizaRegistro_nItem('nfeitst', FatID, FatNum, Empresa, nItem) then
                    SQLTypeA := stUpd
                  else
                    SQLTypeA := stIns;

                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitst', False, [
                    'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
                    'COFINSST_vAliqProd', 'COFINSST_vCOFINS', '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
                    COFINSST_vAliqProd, COFINSST_vCOFINS, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if Trim(InfAdProd) <> '' then
                  begin
                    InfAdProd := Geral.TiraAspasDeTexto(InfAdProd);

                    if LocalizaRegistro_nItem('nfeitsv', FatID, FatNum, Empresa, nItem) then
                      SQLTypeA := stUpd
                    else
                      SQLTypeA := stIns;

                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeA, 'nfeitsv', False, [
                      'InfAdProd', '_Ativo_'], [
                      'FatID', 'FatNum', 'Empresa', 'nItem'], [
                      InfAdProd, _Ativo_], [
                      FatID, FatNum, Empresa, nItem], True) then
                    begin
                      ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      Exit;
                    end;
                  end;
                end;
              end;
              if not DmNFe_0000.AtualizaRecuperacaoDeImpostos(FatID, FatNum, Empresa) then
              begin
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                Exit;
              end;
              {
              LocCod(Codigo, Codigo);
              FinalizaEntrada();
              LocCod(Codigo, Codigo);
              }
              except
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
              end;
            end else Geral.MensagemBox('Importa��o de NF-e cancelada!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          end;
        end;
      end else
        Geral.MB_Aviso('O arquivo n�o � uma NF-e!');
    end;
    Result := True;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TNFeImporta_0400.ObtemValorCampoPeloSeqInArq(const Tabela, Campo:
  String; const SeqInArq, FatID, FatNum, Empresa: Integer; var SQLType:
  TSQLType): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmNFe_0000.QrAux, Dmod.MyDB, [
  'SELECT ' + Campo,
  'FROM ' + Tabela,
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND SeqInArq=' + Geral.FF0(SeqinArq),
  '']);
  //
  Result := DmNFe_0000.QrAux.FieldByName(Campo).AsInteger;
  if Result > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
end;

end.
