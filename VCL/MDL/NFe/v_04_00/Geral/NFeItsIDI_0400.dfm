object FmNFeItsIDI_0400: TFmNFeItsIDI_0400
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-008 :: Declara'#231#227'o de Importa'#231#227'o'
  ClientHeight = 569
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 416
    Width = 1008
    Height = 45
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 671
    ExplicitWidth = 670
    object CkContinuar: TCheckBox
      Left = 5
      Top = 5
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 0
    ExplicitTop = 59
    ExplicitWidth = 1030
    object Label5: TLabel
      Left = 207
      Top = 16
      Width = 23
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Item:'
      FocusControl = DBEdnItem
    end
    object Label3: TLabel
      Left = 276
      Top = 16
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 374
      Top = 16
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdit3
    end
    object Label1: TLabel
      Left = 10
      Top = 16
      Width = 14
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
      FocusControl = DBEdFatID
    end
    object Label10: TLabel
      Left = 49
      Top = 16
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
      FocusControl = DBEdFatNum
    end
    object Label13: TLabel
      Left = 148
      Top = 16
      Width = 44
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object DBEdnItem: TdmkDBEdit
      Left = 207
      Top = 32
      Width = 65
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'nItem'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 0
      UpdCampo = 'nItem'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit2: TDBEdit
      Left = 276
      Top = 32
      Width = 93
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'prod_cProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 374
      Top = 32
      Width = 627
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'prod_xProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 2
    end
    object DBEdFatID: TdmkDBEdit
      Left = 10
      Top = 32
      Width = 34
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'FatID'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 3
      UpdCampo = 'FatID'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdFatNum: TdmkDBEdit
      Left = 49
      Top = 32
      Width = 94
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'FatNum'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 4
      UpdCampo = 'FatNum'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 148
      Top = 32
      Width = 55
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Empresa'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 5
      UpdCampo = 'Empresa'
      UpdType = utYes
      Alignment = taLeftJustify
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 110
    Width = 1008
    Height = 306
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Caption = ' Dados da declara'#231#227'o da importa'#231#227'o:'
    TabOrder = 1
    ExplicitTop = 117
    ExplicitHeight = 344
    object Label2: TLabel
      Left = 15
      Top = 17
      Width = 14
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
    end
    object Label8: TLabel
      Left = 118
      Top = 17
      Width = 72
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'N'#186' DI/DSI/DA:'
    end
    object Label6: TLabel
      Left = 246
      Top = 17
      Width = 120
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data registro DI/DSI/DA:'
    end
    object Label7: TLabel
      Left = 403
      Top = 16
      Width = 111
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Local de desembara'#231'o:'
    end
    object Label9: TLabel
      Left = 964
      Top = 16
      Width = 21
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'UF*:'
    end
    object Label11: TLabel
      Left = 15
      Top = 55
      Width = 108
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data do desembara'#231'o:'
    end
    object Label12: TLabel
      Left = 158
      Top = 55
      Width = 366
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'C'#243'digo do exportador (usar o c'#243'digo do cadastro de entidade de p' +
        'refer'#234'ncia):'
    end
    object Label14: TLabel
      Left = 19
      Top = 208
      Width = 368
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Valor da AFRMM - Adicional ao Frete para Renova'#231#227'o da Marinha Me' +
        'rcante: '
    end
    object Label15: TLabel
      Left = 15
      Top = 278
      Width = 205
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'CNPJ do adquirente ou do encomendante :'
    end
    object Label16: TLabel
      Left = 399
      Top = 279
      Width = 233
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Sigla da UF do adquirente ou do encomendante :'
    end
    object EdControle: TdmkEdit
      Left = 15
      Top = 32
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDI_nDI: TdmkEdit
      Left = 118
      Top = 32
      Width = 124
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      MaxLength = 12
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_nDI'
      UpdCampo = 'DI_nDI'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPDI_dDI: TdmkEditDateTimePicker
      Left = 246
      Top = 32
      Width = 153
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 40440.871393865740000000
      Time = 40440.871393865740000000
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DI_dDI'
      UpdCampo = 'DI_dDI'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdDI_xLocDesemb: TdmkEdit
      Left = 403
      Top = 32
      Width = 558
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_xLocDesemb'
      UpdCampo = 'DI_xLocDesemb'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdDI_UFDesemb: TdmkEdit
      Left = 964
      Top = 32
      Width = 35
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_UFDesemb'
      UpdCampo = 'DI_UFDesemb'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPDI_dDesemb: TdmkEditDateTimePicker
      Left = 15
      Top = 71
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 40440.871393865740000000
      Time = 40440.871393865740000000
      TabOrder = 5
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DI_dDesemb'
      UpdCampo = 'DI_dDesemb'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdDI_cExportador: TdmkEdit
      Left = 158
      Top = 71
      Width = 839
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_cExportador'
      UpdCampo = 'DI_cExportador'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object RGDI_tpViaTransp: TdmkRadioGroup
      Left = 15
      Top = 96
      Width = 982
      Height = 105
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Via de transporte internacional informada na Declara'#231#227'o de Impor' +
        'ta'#231#227'o (DI) :'
      Columns = 4
      Items.Strings = (
        '0=Indefinido'
        '1=Mar'#237'tima'
        '2=Fluvial'
        '3=Lacustre'
        '4=A'#233'rea'
        '5=Postal '
        '6=Ferrovi'#225'ria'
        '7=Rodovi'#225'ria'
        '8=Conduto / Rede Transmiss'#227'o'
        '9=Meios Pr'#243'prios'
        '10=Entrada / Sa'#237'da ficta'
        '11=Courier'
        '12=Em m'#227'os'
        '13=Por reboque')
      TabOrder = 7
      QryCampo = 'DI_tpViaTransp'
      UpdCampo = 'DI_tpViaTransp'
      UpdType = utYes
      OldValor = 0
    end
    object EdDI_vAFRMM: TdmkEdit
      Left = 392
      Top = 203
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'DI_vAFRMM'
      UpdCampo = 'DI_vAFRMM'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGDI_tpIntermedio: TdmkRadioGroup
      Left = 15
      Top = 226
      Width = 982
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Forma de importa'#231#227'o quanto a intermedia'#231#227'o:'
      Columns = 4
      Items.Strings = (
        '0=Indefinido'
        '1=Importa'#231#227'o por conta pr'#243'pria; '
        '2=Importa'#231#227'o por conta e ordem; '
        '3=Importa'#231#227'o por encomenda;')
      TabOrder = 9
      QryCampo = 'DI_tpIntermedio'
      UpdCampo = 'DI_tpIntermedio'
      UpdType = utYes
      OldValor = 0
    end
    object EdDI_CNPJ: TdmkEdit
      Left = 237
      Top = 273
      Width = 139
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_CNPJ'
      UpdCampo = 'DI_CNPJ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdDI_UFTerceiro: TdmkEdit
      Left = 636
      Top = 274
      Width = 35
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_UFTerceiro'
      UpdCampo = 'DI_UFTerceiro'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 948
      Top = 0
      Width = 60
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 610
      ExplicitHeight = 59
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      ExplicitHeight = 59
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 889
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 551
      ExplicitHeight = 59
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 326
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 326
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 326
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 461
    Width = 1008
    Height = 38
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 484
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitWidth = 666
      ExplicitHeight = 34
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 120
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 120
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 1008
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 522
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitWidth = 666
      ExplicitHeight = 59
      object PnSaiDesis: TPanel
        Left = 826
        Top = 0
        Width = 178
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 488
        ExplicitHeight = 59
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 5
        Width = 147
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
