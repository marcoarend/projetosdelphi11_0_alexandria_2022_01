unit NFaEdit_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnDmkProcFunc, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit,
  dmkGeral, Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkMemo,
  dmkRadioGroup, Grids, DBGrids, dmkImage, UnDmkEnums, Vcl.Menus;

type
  TFormChamou = (fcFmFatPedCab, fcFmFatPedNFs);
  TFmNFaEdit_0400 = class(TForm)
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    DsCFOP: TDataSource;
    QrCFOP: TmySQLQuery;
    QrCFOPItens: TLargeintField;
    QrCFOPCFOP: TWideStringField;
    QrCFOPOrdCFOPGer: TIntegerField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    QrFatPedNFs: TmySQLQuery;
    QrImprime: TmySQLQuery;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    DsImprime: TDataSource;
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrParamsEmpAssociada: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DsStqMovValX: TDataSource;
    GradeItens: TDBGrid;
    TbStqMovValX_: TmySQLTable;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmppCredSN_Cfg: TIntegerField;
    Panel4: TPanel;
    BtTodos: TBitBtn;
    QrEnti: TmySQLQuery;
    QrEntiCRT: TSmallintField;
    QrStqMovValX: TmySQLQuery;
    QrStqMovValXID: TIntegerField;
    QrStqMovValXGraGruX: TIntegerField;
    QrStqMovValXCFOP: TWideStringField;
    QrStqMovValXQtde: TFloatField;
    QrStqMovValXPreco: TFloatField;
    QrStqMovValXTotal: TFloatField;
    QrStqMovValXCSOSN: TIntegerField;
    QrStqMovValXpCredSN: TFloatField;
    QrStqMovValXNO_PRD: TWideStringField;
    QrStqMovValXNO_TAM: TWideStringField;
    QrStqMovValXNO_COR: TWideStringField;
    QrCSOSN: TmySQLQuery;
    QrCSOSNItens: TLargeintField;
    BtSelecionados: TBitBtn;
    QrImprimeSerieNF_Normal: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrStqMovValXiTotTrib: TSmallintField;
    QrStqMovValXvTotTrib: TFloatField;
    QrStqMovValXTXT_iTotTrib: TWideStringField;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrParamsEmpNFeInfCpl: TIntegerField;
    QrParamsEmpNFeInfCpl_TXT: TWideMemoField;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    LaSerieNF: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    LaNumeroNF: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    SpeedButton21: TSpeedButton;
    Label10: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    EdNumeroNF: TdmkEdit;
    EdQuantidade: TdmkEdit;
    EdEspecie: TdmkEdit;
    EdMarca: TdmkEdit;
    EdNumero: TdmkEdit;
    EdkgBruto: TdmkEdit;
    EdkgLiqui: TdmkEdit;
    TPDtEmissNF: TdmkEditDateTimePicker;
    TPDtEntraSai: TdmkEditDateTimePicker;
    EdSerieNF: TdmkEdit;
    EddhEmiTZD: TdmkEdit;
    EddhSaiEntTZD: TdmkEdit;
    EddhEmiVerao: TdmkEdit;
    EddhSaiEntVerao: TdmkEdit;
    MeinfAdic_infCpl: TdmkMemo;
    StaticText1: TStaticText;
    EdHrEmi: TdmkEdit;
    EdHrEntraSai: TdmkEdit;
    Label22: TLabel;
    EdVagao: TdmkEdit;
    Label23: TLabel;
    EdBalsa: TdmkEdit;
    Label24: TLabel;
    Eddest_email: TdmkEdit;
    RGCRT: TdmkRadioGroup;
    QrStqMovValXpTotTrib: TFloatField;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    LaAviso1a: TLabel;
    LaAviso2a: TLabel;
    LaAviso1b: TLabel;
    LaAviso2b: TLabel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOk: TBitBtn;
    QrSumXprod_vFrete: TFloatField;
    QrSumXprod_vSeg: TFloatField;
    QrSumXprod_vDesc: TFloatField;
    QrSumXprod_vOutro: TFloatField;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    Panel9: TPanel;
    BtStqMovNFsRef: TBitBtn;
    PMStqMovNFsRef: TPopupMenu;
    IncluiNFreferenciada1: TMenuItem;
    Alterarefenciamentoatual1: TMenuItem;
    Excluireferenciamentos1: TMenuItem;
    QrStqMovNFsRef: TMySQLQuery;
    QrStqMovNFsRefFatID: TIntegerField;
    QrStqMovNFsRefFatNum: TIntegerField;
    QrStqMovNFsRefEmpresa: TIntegerField;
    QrStqMovNFsRefControle: TIntegerField;
    QrStqMovNFsRefrefNFeSig: TWideStringField;
    QrStqMovNFsRefrefNF_cUF: TSmallintField;
    QrStqMovNFsRefrefNF_AAMM: TIntegerField;
    QrStqMovNFsRefrefNF_CNPJ: TWideStringField;
    QrStqMovNFsRefrefNF_mod: TSmallintField;
    QrStqMovNFsRefrefNF_serie: TIntegerField;
    QrStqMovNFsRefrefNF_nNF: TIntegerField;
    QrStqMovNFsRefLk: TIntegerField;
    QrStqMovNFsRefDataCad: TDateField;
    QrStqMovNFsRefDataAlt: TDateField;
    QrStqMovNFsRefUserCad: TIntegerField;
    QrStqMovNFsRefUserAlt: TIntegerField;
    QrStqMovNFsRefAlterWeb: TSmallintField;
    QrStqMovNFsRefAtivo: TSmallintField;
    QrStqMovNFsRefrefNFP_cUF: TSmallintField;
    QrStqMovNFsRefrefNFP_AAMM: TSmallintField;
    QrStqMovNFsRefrefNFP_CNPJ: TWideStringField;
    QrStqMovNFsRefrefNFP_CPF: TWideStringField;
    QrStqMovNFsRefrefNFP_IE: TWideStringField;
    QrStqMovNFsRefrefNFP_mod: TSmallintField;
    QrStqMovNFsRefrefNFP_serie: TSmallintField;
    QrStqMovNFsRefrefNFP_nNF: TIntegerField;
    QrStqMovNFsRefrefCTe: TWideStringField;
    QrStqMovNFsRefrefECF_mod: TWideStringField;
    QrStqMovNFsRefrefECF_nECF: TSmallintField;
    QrStqMovNFsRefrefECF_nCOO: TIntegerField;
    QrStqMovNFsRefQualNFref: TSmallintField;
    DsStqMovNFsRef: TDataSource;
    DBGStqMovNFsRef: TDBGrid;
    TabSheet5: TTabSheet;
    Panel10: TPanel;
    Panel5: TPanel;
    GroupBox10: TGroupBox;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    EdCompra_XNEmp: TdmkEdit;
    EdCompra_XPed: TdmkEdit;
    EdCompra_XCont: TdmkEdit;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    EdUFEmbarq: TdmkEdit;
    EdxLocEmbarq: TdmkEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    RNTC: TLabel;
    EdPlacaNr: TdmkEdit;
    EdPlacaUF: TdmkEdit;
    EdObservacao: TdmkEdit;
    EdRNTC: TdmkEdit;
    QrContas1: TMySQLQuery;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    QrContas1Nome2: TWideStringField;
    QrContas1Nome3: TWideStringField;
    QrContas1ID: TWideStringField;
    QrContas1Subgrupo: TIntegerField;
    QrContas1Empresa: TIntegerField;
    QrContas1Credito: TWideStringField;
    QrContas1Debito: TWideStringField;
    QrContas1Mensal: TWideStringField;
    QrContas1Exclusivo: TWideStringField;
    QrContas1Mensdia: TSmallintField;
    QrContas1Mensdeb: TFloatField;
    QrContas1Mensmind: TFloatField;
    QrContas1Menscred: TFloatField;
    QrContas1Mensminc: TFloatField;
    QrContas1Lk: TIntegerField;
    QrContas1Terceiro: TIntegerField;
    QrContas1Excel: TWideStringField;
    QrContas1DataCad: TDateField;
    QrContas1DataAlt: TDateField;
    QrContas1UserCad: TSmallintField;
    QrContas1UserAlt: TSmallintField;
    QrContas1NOMESUBGRUPO: TWideStringField;
    QrContas1NOMEGRUPO: TWideStringField;
    QrContas1NOMECONJUNTO: TWideStringField;
    QrContas1NOMEEMPRESA: TWideStringField;
    QrContas1NOMEPLANO: TWideStringField;
    QrContas1Niveis: TWideStringField;
    QrContas1Ordens: TWideStringField;
    DsContas1: TDataSource;
    QrContas2: TMySQLQuery;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    QrContas2Nome2: TWideStringField;
    QrContas2Nome3: TWideStringField;
    QrContas2ID: TWideStringField;
    QrContas2Subgrupo: TIntegerField;
    QrContas2Empresa: TIntegerField;
    QrContas2Credito: TWideStringField;
    QrContas2Debito: TWideStringField;
    QrContas2Mensal: TWideStringField;
    QrContas2Exclusivo: TWideStringField;
    QrContas2Mensdia: TSmallintField;
    QrContas2Mensdeb: TFloatField;
    QrContas2Mensmind: TFloatField;
    QrContas2Menscred: TFloatField;
    QrContas2Mensminc: TFloatField;
    QrContas2Lk: TIntegerField;
    QrContas2Terceiro: TIntegerField;
    QrContas2Excel: TWideStringField;
    QrContas2DataCad: TDateField;
    QrContas2DataAlt: TDateField;
    QrContas2UserCad: TSmallintField;
    QrContas2UserAlt: TSmallintField;
    QrContas2NOMESUBGRUPO: TWideStringField;
    QrContas2NOMEGRUPO: TWideStringField;
    QrContas2NOMECONJUNTO: TWideStringField;
    QrContas2NOMEEMPRESA: TWideStringField;
    QrContas2NOMEPLANO: TWideStringField;
    QrContas2Niveis: TWideStringField;
    QrContas2Ordens: TWideStringField;
    DsContas2: TDataSource;
    Panel11: TPanel;
    SbGenCtbCPsqGC: TSpeedButton;
    SbGenCtbC: TSpeedButton;
    SbGenCtbD: TSpeedButton;
    SbGenCtbDPsqGC: TSpeedButton;
    Label66: TLabel;
    Label3: TLabel;
    CBGenCtbD: TdmkDBLookupComboBox;
    CBGenCtbC: TdmkDBLookupComboBox;
    EdGenCtbC: TdmkEditCB;
    DBNiveis2: TDBEdit;
    EdGenCtbD: TdmkEditCB;
    DBNiveis1: TDBEdit;
    QrStqMovNFsRefrefNFe: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrImprimeAfterOpen(DataSet: TDataSet);
    procedure QrImprimeBeforeClose(DataSet: TDataSet);
    procedure MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbStqMovValX_AfterInsert(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtSelecionadosClick(Sender: TObject);
    procedure GradeItensDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TPDtEmissNFChange(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtStqMovNFsRefClick(Sender: TObject);
    procedure PMStqMovNFsRefPopup(Sender: TObject);
    procedure IncluiNFreferenciada1Click(Sender: TObject);
    procedure Excluireferenciamentos1Click(Sender: TObject);
    procedure Alterarefenciamentoatual1Click(Sender: TObject);
    procedure SbGenCtbDPsqGCClick(Sender: TObject);
    procedure SbGenCtbCPsqGCClick(Sender: TObject);
    procedure SbGenCtbDClick(Sender: TObject);
    procedure SbGenCtbCClick(Sender: TObject);
    procedure EdGenCtbDRedefinido(Sender: TObject);
    procedure EdGenCtbCRedefinido(Sender: TObject);
  private
    { Private declarations }
    //FStqMovValX: String;
    //
    FConfirmou, FVerificouContabil: Boolean;
    function  IncluiNFs(): Boolean;
    function  AlteraNFAtual(): Boolean;
    procedure ReopenQrImprime();
    procedure ReopenParamsEmp(Empresa: Integer);
    //function ImpedePeloCRT(): Boolean;
    procedure ConfereCSOSN(Quais: TSelType);
    procedure ReopenContas();
    procedure VerificaContasContabeis();
  public
    { Public declarations }
    FFormChamou: TFormChamou;
    FThisFatID: Integer;
    FIDCtrl: Integer;
    // Configura��es cfe chamada:
    // F_Tipo=1 >> Faturamento de pedido
    // F_Tipo=102 >> CMPTOut(Blue Derm)
    F_Tipo, F_OriCodi, F_Empresa, F_ModeloNF, F_Cliente, F_EMP_FILIAL, F_AFP_Sit,
    F_Associada, F_ASS_FILIAL, F_EMP_CtaFaturas, F_ASS_CtaFaturas, F_CartEmis,
    F_CondicaoPG, F_EMP_FaturaDta, F_EMP_IDDuplicata, F_EMP_FaturaSeq,
    F_EMP_FaturaNum, F_TIPOCART, F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq,
    F_Financeiro, F_ASS_FaturaNum: Integer;
    F_EMP_TpDuplicata, F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
    F_ASS_TpDuplicata, F_ASS_TxtFaturas, FTabela: String;
    F_AFP_Per: Double;
    F_Abertura: TDateTime;
    FReInsere: Boolean;
    F_CtbCadMoF, F_CtbPlaCta, F_tpNF: Integer;
    ///
    //FOEstoqueJaFoiBaixado: Boolean;  //  2020-10-24
    ///
    procedure ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
    procedure ReopenStqMovValX(ID: Integer);
    //procedure ReopenFatPedCab(FatPedCab: Integer);
    procedure InsereTextoObserv(Texto: String);
    //function Encerra(): Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
    function Encerra2(): Boolean;
    procedure DefineTZD_UTC();
    procedure ReopenStqMovNFsRef(Controle: Integer);
    procedure MostraFormStqMovNFsRef(SQLType: TSQLType);

  end;

  var
  FmNFaEdit_0400: TFmNFaEdit_0400;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, (*NF1b,*) MyDBCheck,
  UnFinanceiro, GetData, UnInternalConsts, UCreate, NFaEditCSOSN, UnGrade_Tabs,
  ModProd, ModuleNFe_0000, ModuleFatura, DmkDAC_PF, GetValor, NFe_PF,
  StqMovNFsRef, Contas(*, ModPediVda*),
  {$IfDef UsaContabil}
  UnContabil_PF,
  {$EndIf}
  ModuleFin, UnGrade_PF;

{$R *.DFM}

function TFmNFaEdit_0400.AlteraNFAtual(): Boolean;
var
  Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEmi, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  GenCtbD, GenCtbC: Integer;
begin
  Result := False;
  //usar QrFatPedNFs deste form!!!
  ReopenFatPedNFs(1,0);
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
{
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
}
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Eddest_email.Text; // corrigido 2020-10-24
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  HrEmi        := EdHrEmi.Text;
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  //
  GenCtbD      := EdGenCtbD.ValueVariant;
  GenCtbC      := EdGenCtbC.ValueVariant;
  //
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  //
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //ReopenQrImprime;
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
    'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
    'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
    'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
    'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
    'RNTC', 'UFEmbarq', 'xLocEmbarq',
    'HrEntraSai', 'vagao', 'balsa', 'dest_email',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
    'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
    'GenCtbD', 'GenCtbC'
  ], ['IDCtrl'], [
    F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
    SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
    Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
    Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
    EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
    HrEntraSai, vagao, balsa, dest_email,
    Compra_XNEmp, Compra_XPed, Compra_XCont,
    dhEmiTZD, dhSaiEntTZD, HrEmi,
    GenCtbD, GenCtbC
  ], [FIDCtrl], True) then
  begin
    Result := True;
  end;
end;

procedure TFmNFaEdit_0400.BtStqMovNFsRefClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMStqMovNFsRef, BtStqMovNFsRef);
end;

procedure TFmNFaEdit_0400.Alterarefenciamentoatual1Click(Sender: TObject);
begin
  MostraFormStqMovNFsRef(stUpd);
end;

procedure TFmNFaEdit_0400.BtOkClick(Sender: TObject);
var
  Continua: Boolean;
begin
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    if (EdGenCtbD.ValueVariant = 0) or (EdGenCtbC.ValueVariant = 0) then
    begin
      Geral.MB_Aviso('Defina as duas contas cont�beis! (cr�dito e d�bito)');
      Exit;
    end;
  end;

  //ver horario de verao!
  FConfirmou := True;   // porque est� aqui?
  //
  if F_Tipo = VAR_FATID_0001 then
  begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpCRT.Value = 1 then
    begin
      QrCSOSN.Close;
      QrCSOSN.Params[00].AsInteger := F_Tipo;
      QrCSOSN.Params[01].AsInteger := F_OriCodi;
      QrCSOSN.Params[02].AsInteger := F_EMpresa;
      UnDmkDAC_PF.AbreQuery(QrCSOSN, Dmod.MyDB);
      //
      if QrCSOSNItens.Value > 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'Esta empresa � obridada pelo seu CST = 1 a informar o CSOSN ' +
          sLineBreak + 'para cada produto da NF. Existem ' + FormatFloat('0',
          QrCSOSNItens.Value) + ' item(ns) sem esta informa��o!');
        //
        Exit;
      end;
    end;
  end;
  if ImgTipo.SQLType = stIns then
    Continua := IncluiNFs()
  else
    Continua := AlteraNFAtual();
  //
  if Continua = False then
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant)
  else
    Close;
end;

{
function TFmNFaEdit.ImpedePeloCRT(): Boolean;
begin
  QrEnti.Close;
  QrEnti.Params[0].AsInteger := F_Cliente;
    UnDmkDAC_PF.AbreQuery(QrEnti. O p e n ;
  //
  Result := QrEntiCRT.Value <> 1;
  if Result then
  begin
    Geral.MB_(
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!',
    'Aviso', MB_OK+MB_ICONWARNING)
  end;
end;
}

procedure TFmNFaEdit_0400.IncluiNFreferenciada1Click(Sender: TObject);
begin
  MostraFormStqMovNFsRef(stIns);
end;

function TFmNFaEdit_0400.IncluiNFs(): Boolean;
  procedure ExcluiNF();
  begin
    // excluir nfe stqmovnfsa para n�o gerar erro quando tentar de novo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
    Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MB_Erro('Erro no encerramento!' + sLineBreak +
    'Feche a janela e tente encerrar novamente!' + sLineBreak +
    'Caso n�o consiga, AVISE A DERMATEK.');
    //
    Close;
  end;
var
  IDCtrl, Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF, emit_CRT: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont, HrEmi: String;
  Incluir, Encerrou: Boolean;
  SQLTYpe: TSQLType;
  GenCtbD, GenCtbC: Integer;
begin
  Result := False;
  //usar QrFatPedNFs deste form!!!
  Encerrou    := False;
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
  {
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
  }
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);

  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Eddest_email.Text;
  emit_CRT    := RGCRT.ItemIndex;
  if MyObjects.FIC(emit_CRT < 1, RGCRT, 'Informe o CRT!') then Exit;
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  HrEmi        := EdHrEmi.Text;
  //
  GenCtbD      := EdGenCtbD.ValueVariant;
  GenCtbC      := EdGenCtbC.ValueVariant;

  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  //
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  //
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  // ini 2021-02-17 Evitar erro de reinclus�o!
  // d� para aproveitar ourtros dados?
  //
  if FReInsere then
    SQLType := stUpd // registro j� exite e NF n�o foi emitido por algum outro problema
  else
    SQLType := stIns;
  //IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  IDCtrl  := UMyMod.Busca_IDCtrl_NFe(SQLType, FIDCtrl);
  // fim 2021-02-17 Evitar erro de reinclus�o!
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, (*stIns*)SQLType, 'stqmovnfsa', False, [
      'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
      'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
      'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
      'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
      'RNTC', 'UFEmbarq', 'xLocEmbarq',
      'HrEntraSai', 'vagao', 'balsa', 'dest_email', 'emit_CRT',
      'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
      'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
      'GenCtbD', 'GenCtbC'
    ], ['IDCtrl'], [
      F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
      SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
      Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
      Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
      EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
      HrEntraSai, vagao, balsa, dest_email, emit_CRT,
      Compra_XNEmp, Compra_XPed, Compra_XCont,
      dhEmiTZD, dhSaiEntTZD, HrEmi,
      GenCtbD, GenCtbC
    ], [IDCtrl], True) then
    begin
      ReopenFatPedNFs(2,0);
      if QrFatPedNFs.RecordCount > 0 then
      begin
        while not QrFatPedNFs.Eof do
        begin
          Incluir := True;
          //
          if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
          (*QrFatPedCabSerieDesfe.Value*)-1,
          (*QrFatPedCabNFDesfeita.Value*)0,
          QrImprimeCO_SerieNF.Value,
          QrImprimeCtrl_nfs.Value,
          QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger,
          QrFatPedNFs.FieldByName('Filial').AsInteger,
          QrImprimeMaxSeqLib.Value,
          EdSerieNF, EdNumeroNF(*),
          SerieNFTxt, NumeroNF*)) then
          begin
            // ????
          end;
(*
          SerieNFTxt := IntToStr(EdSerieNF.ValueVariant);
          NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfs', 'Sequencial',
          'WHERE Controle=' + dmkPF.FFP(QrImprimeCtrl_nfs.Value, 0), 0,
          QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + sLineBreak +
          'Filial: ' + dmkPF.FFP(QrFatPedNFsFilial.Value, 0));
*)
          //
          Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
          SerieNFCod  := QrImprimeCO_SerieNF.Value;
          IncSeqAuto  := QrImprimeIncSeqAuto.Value;
          //NumeroNF    := EdNumeroNF.ValueVariant;
          {
          FreteVal    := 0;
          Seguro      := 0;
          Outros      := 0;
          }
          kgBruto     := 0;
          kgLiqui     := 0;
          PlacaUF     := '';
          PlacaNr     := '';
          Especie     := '';
          Marca       := '';
          Numero      := '';
          Quantidade  := '';
          Observacao  := '';
          CFOP1       := '';
          //
          if IncSeqAuto = 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
            Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
            Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
            Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
            Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
            Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
            Dmod.QrUpd.ExecSQL;
          end;
          if NumeroNF < 1 then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir and (NumeroNF > QrImprimeMaxSeqLib.Value) then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir then
          begin
            try
              IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
              'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
              'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
              'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
              'Quantidade', 'CFOP1', 'infAdic_infCpl', 'RNTC',
              'UFEmbarq', 'xLocEmbarq',
              'HrEntraSai', 'vagao', 'balsa', 'dest_email',
              'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
              'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
              'GenCtbD', 'GenCtbC'
              ], ['IDCtrl'], [
              F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
              SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
              Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
              Quantidade, CFOP1, MeinfAdic_infCpl.Text, EdRNTC.Text,
              EdUFEmbarq.Text, EdxLocEmbarq.Text,
              HrEntraSai, vagao, balsa, dest_email,
              Compra_XNEmp, Compra_XPed, Compra_XCont,
              dhEmiTZD, dhSaiEntTZD, HrEmi,
              GenCtbD, GenCtbC
              ], [IDCtrl], True) then
              begin
              // 
              end;
            except
              ExcluiNF();
              raise;
            end;
            //
          end else
            ExcluiNF();
          QrFatPedNFs.Next;
        end;
      end;
      //
      if FFormChamou = fcFmFatPedCab then
      begin
        Encerrou := Encerra2();
      end;
      if Encerrou then
      begin
        ReopenFatPedNFs(1,0);
        Result := True;
      end;
    end;
  except
    ExcluiNF();
    raise;
  end;
end;

procedure TFmNFaEdit_0400.InsereTextoObserv(Texto: String);
var
  TextoA, TextoD: String;
  Pos: Integer;
begin
  Pos := MeinfAdic_infCpl.SelStart;
  //
  TextoA := Copy(MeinfAdic_infCpl.Text, 1, Pos);
  TextoD := Copy(MeinfAdic_infCpl.Text, Pos + 1, Length(MeinfAdic_infCpl.Text));
  //
  MeinfAdic_infCpl.Text := TextoA + ' ' + Texto + ' ' + TextoD;
end;

procedure TFmNFaEdit_0400.MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    UnNFe_PF.MostraFormNFaInfCpl(MeinfAdic_infCpl);
end;

procedure TFmNFaEdit_0400.MostraFormStqMovNFsRef(SQLType: TSQLType);
begin
  UmyMod.FormInsUpd_Cria(TFmStqMovNFsRef, FmStqMovNFsRef, afmoNegarComAviso,
    QrStqMovNFsRef, SQLType);
  FmStqMovNFsRef.FFatID    := F_Tipo;
  FmStqMovNFsRef.FFatNum   := F_OriCodi;
  FmStqMovNFsRef.FEmpresa  := F_Empresa;
  if SQLType = stUpd then
  begin
    FmStqMovNFsRef.FControle := QrStqMovNFsRefControle.Value;
    // ini 2023-06-09
    FmStqMovNFsRef.EdrefNFe.ValueVariant := QrStqMovNFsRefrefNFe.Value;
    FmStqMovNFsRef.CkSigilo.Checked := False;
    FmStqMovNFsRef.EdrefNFeSig.ValueVariant := QrStqMovNFsRefrefNFeSig.Value;
    if QrStqMovNFsRefrefNFeSig.Value <> EmptyStr then
      FmStqMovNFsRef.CkSigilo.Checked := True;
    // fim 2023-06-09
  end;
  FmStqMovNFsRef.FQryIts   := QrStqMovNFsRef;
  //
  FmStqMovNFsRef.ShowModal;
  //ReopenSTQMovNFsRef(0);
  FmStqMovNFsRef.Destroy;
end;

procedure TFmNFaEdit_0400.PMStqMovNFsRefPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := True;
  Habilita2 := (QrStqMovNFsRef.State <> dsInactive) and (QrStqMovNFsRef.RecordCount > 0);
  //
  IncluiNFreferenciada1.Enabled     := Habilita;
  Alterarefenciamentoatual1.Enabled := Habilita and Habilita2;
  Excluireferenciamentos1.Enabled   := Habilita and Habilita2;
end;

procedure TFmNFaEdit_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaEdit_0400.BtSelecionadosClick(Sender: TObject);
begin
  ConfereCSOSN(istSelecionados);
end;

procedure TFmNFaEdit_0400.BtTodosClick(Sender: TObject);
begin
  ConfereCSOSN(istTodos);
end;

procedure TFmNFaEdit_0400.ConfereCSOSN(Quais: TSelType);
  procedure AtualizaItemAtual(CSOSN, FinaliCli: Integer; pCredSN: Double);
  var
    ID: Integer;
  begin
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'CSOSN', 'pCredSN', 'FinaliCli'], ['ID'
    ], [
    CSOSN, pCredSN, FinaliCli], [ID
    ], False) then ;
  end;
var
  I, CSOSN, FinaliCli: Integer;
  pCredSN: Double;
begin
  ReopenParamsEmp(F_Empresa);
  //
  if QrParamsEmpCRT.Value <> 1 then
  begin
    Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!'
    + sLineBreak + sLineBreak +
    'Verifique com seu contador como proceder em caso de devolu��o de ' + sLineBreak +
    'mercadoria de fornecedor que emitiu NF-e pelo Simples Nacional!');
    // Deixar editar porque pode ser devolu��o de NFe!
    //Exit;
  end;
  if DBCheck.CriaFm(TFmNFaEditCSOSN, FmNFaEditCSOSN, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSN.FDtEmissNF := TPDtEmissNF.Date;
    FmNFaEditCSOSN.FEmpresa   := F_Empresa;
    //
    FmNFaEditCSOSN.ShowModal;
    Screen.Cursor := crHourGlass;
    try
      if FmNFaEditCSOSN.FConfirmou then
      begin
        CSOSN     := FmNFaEditCSOSN.EdCSOSN.ValueVariant;
        pCredSN   := FmNFaEditCSOSN.EdpCredSN.ValueVariant;
        FinaliCli := FmNFaEditCSOSN.RGFinaliCli.ItemIndex;
        case Quais of
          istTodos:
          begin
            QrStqMovValX.First;
            while not QrStqMovValX.Eof do
            begin
              AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              QrStqMovValX.Next;
            end;
          end;
          istSelecionados:
          begin
            if GradeItens.SelectedRows.Count > 0 then
            begin
              with GradeItens.DataSource.DataSet do
              for I:= 0 to GradeItens.SelectedRows.Count-1 do
              begin
                //GotoBookmark(pointer(GradeItens.SelectedRows.Items[I]));
                GotoBookmark(GradeItens.SelectedRows.Items[I]);
                AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              end;
            end else AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      FmNFaEditCSOSN.Destroy;
    end;
  end;
  ReopenStqMovValX(QrStqMovValXID.Value);
end;

procedure TFmNFaEdit_0400.DefineTZD_UTC();
var
  Data: TDateTime;
  TZD_UTC: Double;
  hVerao: Boolean;
  SimNao: String;
  Cor: Integer;
begin
  // NFE 3.10
  Data := TPDtEmissNF.Date;
  hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
  SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
  Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  EddhEmiVerao.Text := SimNao;
  EddhEmiVerao.Font.Color := Cor;
  EddhEmiTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  if TPDtEntraSai.Date <> TPDtEmissNF.Date then
  begin
    Data   := TPDtEntraSai.Date;
    hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
    SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
    Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  end;
  EddhSaiEntTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  EddhSaiEntVerao.Text := SimNao;
  EddhSaiENtVerao.Font.Color := Cor;
end;

procedure TFmNFaEdit_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  VerificaContasContabeis();
end;

procedure TFmNFaEdit_0400.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not FConfirmou then
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant)
end;

procedure TFmNFaEdit_0400.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FReInsere := False;
  FVerificouContabil := False;

  //FOEstoqueJaFoiBaixado := False;
  //
  FConfirmou := False;
  FThisFatID := 1;
  FTabela    := '';
  // N�o pode ser aqui precisa criar primeiro!!!
  // Chamar na cria��o antes do ShowModal
  //ReopenFatPedNFs(1,0);
  TPDtEmissNF.Date          := DmodG.ObtemAgora;
  TPDtEntraSai.Date         := DmodG.ObtemAgora;
  EdHrEmi.ValueVariant      := DmodG.ObtemAgora;
  EdHrEntraSai.ValueVariant := DmodG.ObtemAgora;
  //
  TbStqMovValX_.Database := DModG.MyPID_DB;
  //
  //
  DBNiveis1.DataField := '';
  DBNiveis2.DataField := '';
  //
  ReopenContas();
  //
  //DefineTZD_UTC();
end;

procedure TFmNFaEdit_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaEdit_0400.FormShow(Sender: TObject);
var
  NFeInfCpl_TXT: String;
begin
  ReopenParamsEmp(F_Empresa);
  //
  NFeInfCpl_TXT := QrParamsEmpNFeInfCpl_TXT.Value;
  //
  if QrParamsEmpCRT.Value in [1,2] then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
  //
  if F_Cliente <> 0 then //Configura endereco de entrega
  begin
    if DmodG.ReopenEntrega(F_Cliente) then
    begin
      if DmodG.QrEntregaL_Ativo.Value = 1 then
        InsereTextoObserv('Endere�o de entrega: ' + DmodG.QrEntregaE_ALL.Value);
    end;
  end;

  if NFeInfCpl_TXT <> '' then
    InsereTextoObserv(NFeInfCpl_TXT);
  //
  ReopenSTQMovNFsRef(0);
end;

procedure TFmNFaEdit_0400.GradeItensDblClick(Sender: TObject);
const
  FormCaption  = 'XXX-XXXXX-001 :: Valor Aproximado dos Tributos';
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
  iTotTrib     = 1; // Informou manualmente!
var
  vTotTrib, pTotTrib: Double;
  ValVar: Variant;
  ID: Integer;
begin
{
GetValorDmk(ComponentClass: TComponentClass; Reference:
TComponent; FormatType: TAllFormat; Default: Variant;
Casas, LeftZeros: Integer; ValMin, ValMax: String;
Obrigatorio: Boolean; FormCaption, ValCaption: String;
WidthVal: Integer; var Resultado: Variant): Boolean;
}
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    QrStqMovValXvTotTrib.Value, 4, 0, '0,00', FormatFloat('0.00',
    QrStqMovValXTotal.Value), True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    vTotTrib := Geral.DMV(ValVar);
    if QrStqMovValXTotal.Value <> 0 then
      pTotTrib := vTotTrib / QrStqMovValXTotal.Value * 100
    else
      pTotTrib := 0;
    //
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'iTotTrib', 'vTotTrib', 'pTotTrib'], [
    'ID'], [
    iTotTrib, vTotTrib, pTotTrib], [
    ID], False) then
    begin
      ReopenStqMovValX(ID);
    end;
  end;
end;

procedure TFmNFaEdit_0400.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
{
  QrVolumes.Close;
  QrVolumes.Params[00].AsInteger := F_OriCodi;
    UnDmkDAC_PF.AbreQuery(QrVolumes. O p e n ;
}
  ReopenQrImprime();
end;

procedure TFmNFaEdit_0400.QrImprimeAfterOpen(DataSet: TDataSet);
begin
  if QrImprime.RecordCount = 0 then
    Geral.MB_Aviso('Esta filial n�o est� ativa no Modelo de NF selecionado '+
    'na Regra Fiscal deste faturamento!');
  BtOK.Enabled := QrImprime.RecordCount > 0 ;
end;

procedure TFmNFaEdit_0400.QrImprimeBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

{
procedure TFmNFaEdit.ReopenFatPedCab(FatPedCab: Integer);
begin
  F_.Close;
  F_.Params[0].AsInteger := FatPedCab;
    UnDmkDAC_PF.AbreQuery(F_.O p e n ;
end;
}

procedure TFmNFaEdit_0400.ReopenContas;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas1, Dmod.MyDB, [
  'SELECT CONCAT( ',
  '  pl.Codigo, ".", ',
  '  cj.Codigo, ".", ',
  '  gr.Codigo, ".", ',
  '  sg.Codigo, ".", ',
  '  co.Codigo) Niveis, ',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
  'FROM contas co ',
  'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
(*
  'WHERE co.Terceiro=0 ',
  'AND co.Codigo>0 ',
  SQL1,
  SQL2,
*)
  'ORDER BY co.Nome ',
  '']);


  UnDmkDAC_PF.AbreMySQLQuery0(QrContas2, Dmod.MyDB, [
  'SELECT CONCAT( ',
  '  pl.Codigo, ".", ',
  '  cj.Codigo, ".", ',
  '  gr.Codigo, ".", ',
  '  sg.Codigo, ".", ',
  '  co.Codigo) Niveis, ',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
  'FROM contas co ',
  'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
(*
  'WHERE co.Terceiro=0 ',
  'AND co.Codigo>0 ',
  SQL1,
  SQL2,
*)
  'ORDER BY co.Nome ',
  '']);

end;

procedure TFmNFaEdit_0400.ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
var
  Txt: String;
begin
  if QuaisFiliais = 1 then
    Txt := '='
  else
    Txt := '<>';
  //
  QrFatPedNFs.Close;
  QrFatPedNFs.SQL.Clear;
  QrFatPedNFs.SQL.Add('SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP,');
  QrFatPedNFs.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*');
  QrFatPedNFs.SQL.Add('FROM stqmovvala smva');
  QrFatPedNFs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa');
  QrFatPedNFs.SQL.Add('LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa');
  QrFatPedNFs.SQL.Add('          AND smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('          AND smna.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('AND ent.Codigo' + Txt +  FormatFloat('0', F_Empresa));
  QrFatPedNFs.SQL.Add('ORDER BY ent.Filial');
  QrFatPedNFs.SQL.Add('');
  //
  //Geral.MB_Teste(QrFatPedNFs.SQL.Text);
  {
  QrFatPedNFs.Params[00].AsInteger := F_Tipo;
  QrFatPedNFs.Params[01].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[02].AsInteger := F_Tipo;
  QrFatPedNFs.Params[03].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[04].AsInteger := F_Empresa;
  }
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs, Dmod.MyDB);
  //
  QrFatPedNFs.Locate('Filial', FilialLoc, []);
end;

procedure TFmNFaEdit_0400.ReopenParamsEmp(Empresa: Integer);
begin
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
end;

procedure TFmNFaEdit_0400.ReopenQrImprime;
begin
  QrImprime.Close;
  if QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger = F_Empresa then
  begin
    QrImprime.Params[00].AsInteger := F_Empresa;
    QrImprime.Params[01].AsInteger := F_ModeloNF;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
  end else begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    end;
  end;
end;

procedure TFmNFaEdit_0400.ReopenStqMovNFsRef(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovNFsRef, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqmovnfsref ',
  'WHERE FatID=' + Geral.FF0(F_Tipo),
  'AND FatNum=' + Geral.FF0(F_OriCodi),
  'AND Empresa=' + Geral.FF0(F_Empresa),
  '']);
  //
  if Controle > 0 then
    QrStqMovNFsRef.Locate('Controle', Controle, []);
end;

procedure TFmNFaEdit_0400.ReopenStqMovValX(ID: Integer);
begin
  QrStqMovValX.Close;
  QrStqMovValX.SQL.Clear;
  QrStqMovValX.SQL.Add('SELECT smva.ID, smva.GraGruX, smva.CFOP, smva.Qtde,');
  QrStqMovValX.SQL.Add('smva.Preco, smva.Total, smva.CSOSN, smva.pCredSN,');
  QrStqMovValX.SQL.Add('gg1.Nome NO_PRD, gti.Nome NO_TAM, gcc.Nome NO_COR,');
  QrStqMovValX.SQL.Add('smva.iTotTrib, smva.vTotTrib, smva.pTotTrib, ');
  QrStqMovValX.SQL.Add('ELT(smva.iTotTrib + 1, "N", "S") TXT_iTotTrib ');
  QrStqMovValX.SQL.Add('FROM stqmovvala smva');
  QrStqMovValX.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smva.GraGruX');
  QrStqMovValX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrStqMovValX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrStqMovValX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrStqMovValX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrStqMovValX.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrStqMovValX.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrStqMovValX.SQL.Add('AND smva.Empresa=' + FormatFloat('0', F_Empresa));
  QrStqMovValX.SQL.Add('ORDER BY smva.IDCtrl');
  UnDmkDAC_PF.AbreQuery(QrStqMovValX, Dmod.MyDB);
  QrStqMovValX.Locate('ID', ID, []);
end;

procedure TFmNFaEdit_0400.SbGenCtbCClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := F_Cliente; //DmPediVda.QrFPC1Cliente.Value; // DmPediVda.QrFatPedCabCliente.Value;

  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContas1Codigo.Value, QrContas1Codigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    ReopenContas();
    if VAR_CADASTRO <> 0 then
    begin
      EdGenCtbD.ValueVariant := VAR_CONTA;
      CBGenCtbD.KeyValue     := VAR_CONTA;
    end;
    //
  end;
end;

procedure TFmNFaEdit_0400.SbGenCtbCPsqGCClick(Sender: TObject);
var
  GenCtbC: Integer;
begin
  VAR_CADASTRO := 0;
  GenCtbC      := EdGenCtbC.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if GenCtbC <> 0 then
      FmContas.LocCod(GenCtbC, GenCtbC);
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    //UMyMod.SetaCodigoPesquisado(EdGenCtb, CBGenCtb, QrGenCtbs, VAR_CADASTRO);
    UMyMod.SetaCodigoPesquisado(EdGenCtbC, CBGenCtbC, QrContas1, VAR_CADASTRO);
    EdGenCtbC.SetFocus;
  end;
end;

procedure TFmNFaEdit_0400.SbGenCtbDClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := F_Cliente; //DmPediVda.QrFPC1Cliente.Value; // DmPediVda.QrFatPedCab
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContas1Codigo.Value, QrContas1Codigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    ReopenContas();
    if VAR_CADASTRO <> 0 then
    begin
      EdGenCtbD.ValueVariant := VAR_CONTA;
      CBGenCtbD.KeyValue     := VAR_CONTA;
    end;
  end;
end;

procedure TFmNFaEdit_0400.SbGenCtbDPsqGCClick(Sender: TObject);
var
  GenCtbD: Integer;
begin
  VAR_CADASTRO := 0;
  GenCtbD       := EdGenCtbD.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if GenCtbD <> 0 then
      FmContas.LocCod(GenCtbD, GenCtbD);
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    //UMyMod.SetaCodigoPesquisado(EdGenCtbD, CBGenCtbD, QrGenCtbDs, VAR_CADASTRO);
    UMyMod.SetaCodigoPesquisado(EdGenCtbD, CBGenCtbD, QrContas1, VAR_CADASTRO);
    EdGenCtbD.SetFocus;
  end;
end;

procedure TFmNFaEdit_0400.SpeedButton21Click(Sender: TObject);
begin
  dmkPF.TZD_UTC_InfoHelp();
end;

procedure TFmNFaEdit_0400.TbStqMovValX_AfterInsert(DataSet: TDataSet);
begin
  TbStqMovValX_.Cancel;
end;

procedure TFmNFaEdit_0400.TPDtEmissNFChange(Sender: TObject);
begin
  DefineTZD_UTC;
end;

procedure TFmNFaEdit_0400.VerificaContasContabeis();
  procedure Verifica();
    function ObtemCliCtbPlaCta(): Integer;
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrCliCtb, Dmod.MyDB, [
      'SELECT CtbPlaCta ',
      'FROM entidades',
      'WHERE Codigo=' + Geral.FF0(F_Cliente),
      '']);
      Result := DModFin.QrCliCtbCtbPlaCta.Value;
    end;
  var
    Cliente, CliCtbPlaCta: Integer;
    TypCtbCadMoF: TTypCtbCadMoF;
    GenCtbD, GenCtbC: Integer;
    Descricao: String;
  begin
    //if FCriado or (ImgTipo.SQLType = stIns) then
    begin
      TypCtbCadMoF := TTypCtbCadMoF(F_CtbCadMoF);
      Cliente := F_Cliente;
      if Cliente <> 0 then
      begin

{$IfDef UsaContabil}
        if F_CtbCadMoF <> 0 then
        begin
          // Est� Invertido!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          case F_tpNF of  // Tipo de Opera��o da NF-e: 0=Entrada; 1=Sa�da
          // Est� Invertido!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            0: // saida
                Contabil_PF.DefineContasDevedor(TypCtbCadMoF, Cliente, EdGenCtbD, CBGenCtbD, EdGenCtbC, CBGenCtbC);
            1: // entrada
                Contabil_PF.DefineContasCredor(TypCtbCadMoF, Cliente, EdGenCtbD, CBGenCtbD, EdGenCtbC, CBGenCtbC);
          end;
        end;
{$EndIf}

        case F_tpNF of  // Tipo de Opera��o da NF-e: 0=Entrada; 1=Sa�da
          0: // saida
          begin
            if (EdGenCtbD.ValueVariant = 0) and (F_CtbPlaCta <> 0) then
            begin
              EdGenCtbD.ValueVariant := F_CtbPlaCta;
              CBGenCtbD.KeyValue     := F_CtbPlaCta;
            end;
            if (EdGenCtbC.ValueVariant = 0) and (F_CtbCadMoF <> 0)  then
            begin
              GenCtbD := 0;
              GenCtbC := 0;
              //
              if Grade_PF.DefineContasContabeisNFe_PelosItens(TypCtbCadMoF, F_Tipo,
              F_OriCodi, F_Empresa, GenCtbD, GenCtbC, Descricao) then
              begin
                EdGenCtbC.ValueVariant := GenCtbC;
              end;
            end;
          end;
          1: // entrada
          begin
            if (EdGenCtbC.ValueVariant = 0) and (F_CtbPlaCta <> 0) then
            begin
              EdGenCtbC.ValueVariant := F_CtbPlaCta;
              CBGenCtbC.KeyValue     := F_CtbPlaCta;
            end;
            if (EdGenCtbD.ValueVariant = 0) and (F_CtbCadMoF <> 0)  then
            begin
              GenCtbD := 0;
              GenCtbD := 0;
              //
              if Grade_PF.DefineContasContabeisNFe_PelosItens(TypCtbCadMoF, F_Tipo,
              F_OriCodi, F_Empresa, GenCtbD, GenCtbD, Descricao) then
              begin
                EdGenCtbD.ValueVariant := GenCtbD;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
begin
  if FVerificouContabil then Exit;
  //
  try
    if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
    begin
      Verifica();
    end;
  finally
    FVerificouContabil := True;
  end;
end;

(*
Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
function TFmNFaEdit_0400.Encerra(): Boolean;
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
  Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela,
  NotaFiscal, Account: Integer; SerieNF: String; VerificaCliInt: Boolean);
  var
    TabLctA: String;
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    if F_Financeiro = 1 then
      FLAN_Credito    := Valor
    else
    if F_Financeiro = 2 then
      FLAN_Debito    := Valor;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_MoraDia    := QrSumTJurosMes.Value;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    F L A N _ G e n e r o     := Genero;
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
    FLAN_Cliente    := F_Cliente;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
    FLAN_FatID      := FThisFatID;
    FLAN_FatNum     := F_OriCodi;
    FLAN_FatParcela := Parcela;
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := SerieNF;
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
{$IfDef DEFINE_VARLCT}
    TabLctA         := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, F_EMP_Filial);
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
    //
    if UFinanceiro.InsereLancamento(TabLctA) then
    begin
      // nada
    end;
{$Else}
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
      'Controle', VAR_LCT, VAR_LCT, 'Controle');
    //
    if UFinanceiro.InsereLancamento() then
    begin
      // nada
    end;
{$ENdIf}
  end;
var
  DataFat, DataEnc: TDateTime;
  FaturaNum, Agora, Duplicata, NovaDup, NF_Emp_Serie, NF_Ass_Serie: String;
  Parcela, NF_Emp_Numer, NF_Ass_Numer, Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  TemAssociada: Boolean;
  FreteVal, Seguro, Outros: Double;
  TabLctA: String;
begin
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      // Exclui lan�amentos para evitar duplica��o
      if (FThisFatID <> 0) and (F_OriCodi <> 0) then
      begin
{$IfDef DEFINE_VARLCT}
      Filial  := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      //
      UFinanceiro.ExcluiLct_FatNum(nil, FThisFatID, F_OriCodi,
        F_Empresa, 0, dmkPF.MotivDel_ValidaCodigo(311), True, TabLctA);
{$Else}
      UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FThisFatID, F_OriCodi,
        F_Empresa, 0, True);
{$EndIf}
      end;
      //
      QrNF_X.Close;
      QrNF_X.Params[00].AsInteger := F_Tipo;
      QrNF_X.Params[01].AsInteger := F_OriCodi;
      QrNF_X.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
      //
      NF_Emp_Serie := QrNF_XSerieNFTxt.Value;
      NF_Emp_Numer := QrNF_XNumeroNF.Value;
      //
      if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', F_EMP_FILIAL) + ' !' + sLineBreak +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"');
        Exit;
      end;
      //
      TemAssociada := (F_AFP_Sit = 1) and (F_AFP_Per > 0);
      //
      if TemAssociada then
      begin
        QrNF_X.Close;
        QrNF_X.Params[00].AsInteger := F_Tipo;
        QrNF_X.Params[01].AsInteger := F_OriCodi;
        QrNF_X.Params[02].AsInteger := F_Associada;
        UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
        NF_Ass_Serie := QrNF_XSerieNFTxt.Value;
        NF_Ass_Numer := QrNF_XNumeroNF.Value;
        if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'O n�mero de nota fiscal n�o foi definida para a empresa ' +
            FormatFloat('000', F_ASS_FILIAL) + ' !' + sLineBreak +
            'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
            'Nota Fiscal"');
          Exit;
        end;
        //
        P2 := F_AFP_Per;
        P1 := 100 - P2;
      end else
      begin
        P1 := 100;
        P2 := 0;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
      end;
      //
      if F_EMP_CtaFaturas = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_EMP_FILIAL) + '!');
        Exit;
      end;
      if (F_Associada <> 0) and
      (F_ASS_CtaFaturas = 0) and TemAssociada then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_ASS_FILIAL) + '!');
        Exit;
      end;
      if F_Financeiro > 0 then
      begin
        if F_CartEmis = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'A carteira n�o foi definida no pedido selecionado! (4)');
          Exit;
        end;
        QrPrzT.Close;
        QrPrzT.Params[0].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzT, Dmod.MyDB);
        if QrPrzT.RecordCount = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'N�o h� parcela(s) definida(s) para a empresa ' +
            IntToStr(F_EMP_FILIAL) + ' na condi��o de pagamento ' +
            'cadastrada no pedido selecionado!');
          Exit;
        end;
        if (F_AFP_Sit = 1) and
        (F_AFP_Per > 0) then
        begin
          QrSumT.Close;
          QrSumT.Params[0].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
          if (QrSumTPercent2.Value <> F_AFP_Per) then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
          if QrSumT.RecordCount = 0 then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
        end;
      end else
        Geral.MB_Aviso('N�o ser� gerado lan�amentos financeiros.' + sLineBreak +
        'Na Regra Fiscal est� definido para n�o gerar lan�amentos!');
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      // Faturamento empresa principal
      {
      FreteVal := EdFreteVal.ValueVariant;
      Seguro   := EdSeguro.ValueVariant;
      Outros   := EdOutros.ValueVariant;
      }
      //
      QrSumX.Close;
      {  Erro !!!!????
      QrSumX.Params[00].AsInteger := F_Tipo;
      QrSumX.Params[01].AsInteger := F_OriCodi;
      QrSumX.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
      }
      QrSumX.SQL.Clear;
      QrSumX.SQL.Add('SELECT SUM(Total) Total');
      QrSumX.SQL.Add('FROM stqmovvala');
      QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
      QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
      QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Empresa));
      UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
      //
      T1 := QrSumXTotal.Value + FreteVal + Seguro + Outros;
      F1 := T1;
      //
      if F_Financeiro > 0 then
      begin
        QrPrzX.Close;
        QrPrzX.SQL.Clear;
        QrPrzX.SQL.Add('SELECT Controle, Dias, Percent1 Percent ');
        QrPrzX.SQL.Add('FROM pediprzits');
        QrPrzX.SQL.Add('WHERE Percent1 > 0');
        QrPrzX.SQL.Add('AND Codigo=:P0');
        QrPrzX.SQL.Add('ORDER BY Dias');
        QrPrzX.Params[00].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
        QrPrzX.First;
        while not QrPrzX.Eof do
        begin
          if QrPrzX.RecordCount = QrPrzX.RecNo then
            V1 := F1
          else begin
            if P1 = 0 then V1 := 0 else
              V1 := (Round(T1 * (QrPrzXPercent.Value / P1 * 100))) / 100;

            F1 := F1 - V1;
          end;

          QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
          Parcela := QrPrzT.RecNo;
          if F_EMP_FaturaNum = 0 then
            FaturaNum := FormatFloat('000000', F_EMP_IDDuplicata)
          else
            FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
          Duplicata := F_EMP_TpDuplicata + FaturaNum + F_EMP_FaturaSep +
            dmkPF.ParcelaFatura(QrPrzX.RecNo, F_EMP_FaturaSeq);
          // 2011-08-21
          // Teste para substituir no futuro (uso no form FmFatDivCms)
          NovaDup := DmProd.MontaDuplicata(F_EMP_IDDuplicata, EdNumeroNF.ValueVariant,
          Parcela, F_EMP_FaturaNum, F_EMP_FaturaSeq,
          F_EMP_TpDuplicata, F_EMP_FaturaSep);
          if NovaDup <> Duplicata then
            Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak +
            Duplicata + ' <> ' + NovaDup);
          // Fim 2011-08-21
          IncluiLancto(V1, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
            F_EMP_TxtFaturas, F_TIPOCART,
            F_CartEmis, F_EMP_CtaFaturas,
            F_Empresa, Parcela, NF_Emp_Numer,
            F_Represen, NF_Emp_Serie, True);
          //
          QrPrzX.Next;
        end;


        // Faturamento associada
        if TemAssociada then
        begin
          QrSumX.Close;
          {
          QrSumX.Params[00].AsInteger := F_Tipo;
          QrSumX.Params[01].AsInteger := F_OriCodi;
          QrSumX.Params[02].AsInteger := F_Associada;
          UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
          }
          QrSumX.SQL.Clear;
          QrSumX.SQL.Add('SELECT SUM(Total) Total');
          QrSumX.SQL.Add('FROM stqmovvala');
          QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
          QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
          QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Associada));
          UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
          T2 := QrSumXTotal.Value;
          F2 := T2;
          //
          QrPrzX.Close;
          QrPrzX.SQL.Clear;
          QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
          QrPrzX.SQL.Add('FROM pediprzits');
          QrPrzX.SQL.Add('WHERE Percent2 > 0');
          QrPrzX.SQL.Add('AND Codigo=:P0');
          QrPrzX.SQL.Add('ORDER BY Dias');
          QrPrzX.Params[00].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
          QrPrzX.First;
          while not QrPrzX.Eof do
          begin
            if QrPrzX.RecordCount = QrPrzX.RecNo then
              V2 := F2
            else begin
              if P2 = 0 then V2 := 0 else
                V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
              F2 := F2 - V2;
            end;
            QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
            Parcela := QrPrzT.RecNo;
            if F_EMP_FaturaNum = 0 then
              FaturaNum := FormatFloat('000000', F_ASS_IDDuplicata)
            else
              FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
            Duplicata := F_ASS_TpDuplicata + FormatFloat('000000', F_ASS_IDDuplicata) +
              F_ASS_FaturaSep + dmkPF.ParcelaFatura(
              QrPrzX.RecNo, F_ASS_FaturaSeq);
            IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
              F_ASS_TxtFaturas, F_TIPOCART,
              F_CartEmis, F_ASS_CtaFaturas,
              F_Associada, Parcela, NF_Ass_Numer,
              F_Represen, NF_Ass_Serie, False);
            //
            QrPrzX.Next;
          end;
        end;
      end;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //

      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
*)

procedure TFmNFaEdit_0400.EdGenCtbCRedefinido(Sender: TObject);
begin
  if EdGenCtbC.ValueVariant = 0 then
    DBNiveis2.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis2.DataField := 'Ordens'
    else
      DBNiveis2.DataField := 'Niveis';
  end;
end;

procedure TFmNFaEdit_0400.EdGenCtbDRedefinido(Sender: TObject);
begin
  if EdGenCtbD.ValueVariant = 0 then
    DBNiveis1.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis1.DataField := 'Ordens'
    else
      DBNiveis1.DataField := 'Niveis';
  end;
end;

function TFmNFaEdit_0400.Encerra2(): Boolean;
var
  DataFat, DataEnc: TDateTime;
  Agora: String;
  Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  //2021-10-23 FreteVal, Seguro, Outros: Double;
  GenCtbD, GenCtbC: Integer;
begin
  Filial   := 0;
{
  // ini 2021-10-23
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  // fim 2021-10-23
}
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  //if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      Filial := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      GenCtbD := EdGenCtbD.ValueVariant;
      GenCtbC := EdGenCtbC.ValueVariant;
      //

      if not UnNFe_PF.InsUpdFaturasNFe(Filial, F_Empresa, FThisFatID, F_Tipo,
        F_OriCodi, F_EMP_CtaFaturas, F_Associada, F_ASS_CtaFaturas, F_ASS_FILIAL,
        F_Financeiro, GenCtbD, GenCtbC, F_CartEmis, F_CondicaoPG, F_AFP_Sit, F_EMP_FaturaNum,
        F_EMP_IDDuplicata, EdNumeroNF.ValueVariant, F_EMP_FaturaSeq, F_TIPOCART,
        F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq, F_Cliente,
        F_AFP_Per,
        // ini 2021-10-23
        //FreteVal, Seguro, 0, Outros,
        // fim 2021-10-23
        F_EMP_TpDuplicata,
        F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
        F_ASS_TpDuplicata, F_ASS_TxtFaturas, DataFat, QrPrzT, QrSumT, QrPrzX,
        QrSumX, QrNF_X) then Exit;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else
          Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFaEdit_0400.Excluireferenciamentos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrStqMovNFsRef, DBGStqMovNFsRef, 'stqmovnfsref',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

{ 2011-11-18

object EdFreteVal: TdmkEdit
  Left = 272
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 4
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label1: TLabel
  Left = 272
  Top = 0
  Width = 36
  Height = 13
  Caption = '$ Frete:'
  Visible = False
end
object Label2: TLabel
  Left = 344
  Top = 0
  Width = 46
  Height = 13
  Caption = '$ Seguro:'
  Visible = False
end
object EdSeguro: TdmkEdit
  Left = 344
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 5
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object EdOutros: TdmkEdit
  Left = 416
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 6
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label10: TLabel
  Left = 416
  Top = 0
  Width = 57
  Height = 13
  Caption = '$ D. acess.:'
  Visible = False
end

}

end.

