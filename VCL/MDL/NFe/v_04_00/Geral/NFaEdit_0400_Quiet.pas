unit NFaEdit_0400_Quiet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnDmkProcFunc, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit,
  dmkGeral, Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkMemo,
  dmkRadioGroup, Grids, DBGrids, dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFormChamou = (fcFmFatPedCab, fcFmFatPedNFs);
  TFmNFaEdit_0400_Quiet = class(TForm)
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    DsCFOP: TDataSource;
    QrCFOP: TmySQLQuery;
    QrCFOPItens: TLargeintField;
    QrCFOPCFOP: TWideStringField;
    QrCFOPOrdCFOPGer: TIntegerField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    QrFatPedNFs: TmySQLQuery;
    QrImprime: TmySQLQuery;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    DsImprime: TDataSource;
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrParamsEmpAssociada: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DsStqMovValX: TDataSource;
    GradeItens: TDBGrid;
    TbStqMovValX_: TmySQLTable;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmppCredSN_Cfg: TIntegerField;
    Panel4: TPanel;
    BtTodos: TBitBtn;
    QrEnti: TmySQLQuery;
    QrEntiCRT: TSmallintField;
    QrStqMovValX: TmySQLQuery;
    QrStqMovValXID: TIntegerField;
    QrStqMovValXGraGruX: TIntegerField;
    QrStqMovValXCFOP: TWideStringField;
    QrStqMovValXQtde: TFloatField;
    QrStqMovValXPreco: TFloatField;
    QrStqMovValXTotal: TFloatField;
    QrStqMovValXCSOSN: TIntegerField;
    QrStqMovValXpCredSN: TFloatField;
    QrStqMovValXNO_PRD: TWideStringField;
    QrStqMovValXNO_TAM: TWideStringField;
    QrStqMovValXNO_COR: TWideStringField;
    QrCSOSN: TmySQLQuery;
    QrCSOSNItens: TLargeintField;
    BtSelecionados: TBitBtn;
    QrImprimeSerieNF_Normal: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrStqMovValXiTotTrib: TSmallintField;
    QrStqMovValXvTotTrib: TFloatField;
    QrStqMovValXTXT_iTotTrib: TWideStringField;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrParamsEmpNFeInfCpl: TIntegerField;
    QrParamsEmpNFeInfCpl_TXT: TWideMemoField;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    QrStqMovValXpTotTrib: TFloatField;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    LaAviso1a: TLabel;
    LaAviso2a: TLabel;
    LaAviso1b: TLabel;
    LaAviso2b: TLabel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOk: TBitBtn;
    QrStqMovNFsA: TMySQLQuery;
    QrStqMovNFsAIDCtrl: TIntegerField;
    QrStqMovNFsATipo: TSmallintField;
    QrStqMovNFsAOriCodi: TIntegerField;
    QrStqMovNFsAEmpresa: TIntegerField;
    QrStqMovNFsASerieNFCod: TIntegerField;
    QrStqMovNFsASerieNFTxt: TWideStringField;
    QrStqMovNFsANumeroNF: TIntegerField;
    QrStqMovNFsAIncSeqAuto: TSmallintField;
    QrStqMovNFsAFreteVal: TFloatField;
    QrStqMovNFsASeguro: TFloatField;
    QrStqMovNFsAOutros: TFloatField;
    QrStqMovNFsAPlacaUF: TWideStringField;
    QrStqMovNFsAPlacaNr: TWideStringField;
    QrStqMovNFsARNTC: TWideStringField;
    QrStqMovNFsAQuantidade: TWideStringField;
    QrStqMovNFsAEspecie: TWideStringField;
    QrStqMovNFsAMarca: TWideStringField;
    QrStqMovNFsANumero: TWideStringField;
    QrStqMovNFsAkgBruto: TFloatField;
    QrStqMovNFsAkgLiqui: TFloatField;
    QrStqMovNFsAObservacao: TWideStringField;
    QrStqMovNFsACFOP1: TWideStringField;
    QrStqMovNFsADtEmissNF: TDateField;
    QrStqMovNFsADtEntraSai: TDateField;
    QrStqMovNFsAHrEntraSai: TTimeField;
    QrStqMovNFsAinfAdic_infCpl: TWideMemoField;
    QrStqMovNFsAUFembarq: TWideStringField;
    QrStqMovNFsAxLocEmbarq: TWideStringField;
    QrStqMovNFsAide_dhCont: TDateTimeField;
    QrStqMovNFsAide_xJust: TWideStringField;
    QrStqMovNFsAemit_CRT: TSmallintField;
    QrStqMovNFsAdest_email: TWideStringField;
    QrStqMovNFsAvagao: TWideStringField;
    QrStqMovNFsAbalsa: TWideStringField;
    QrStqMovNFsACompra_XNEmp: TWideStringField;
    QrStqMovNFsACompra_XPed: TWideStringField;
    QrStqMovNFsACompra_XCont: TWideStringField;
    QrStqMovNFsAdhEmiTZD: TFloatField;
    QrStqMovNFsAdhSaiEntTZD: TFloatField;
    QrStqMovNFsAHrEmi: TTimeField;
    QrFatPedNFs_B: TMySQLQuery;
    QrFatPedNFs_BFilial: TIntegerField;
    QrFatPedNFs_BIDCtrl: TIntegerField;
    QrFatPedNFs_BTipo: TSmallintField;
    QrFatPedNFs_BOriCodi: TIntegerField;
    QrFatPedNFs_BEmpresa: TIntegerField;
    QrFatPedNFs_BNumeroNF: TIntegerField;
    QrFatPedNFs_BIncSeqAuto: TSmallintField;
    QrFatPedNFs_BAlterWeb: TSmallintField;
    QrFatPedNFs_BAtivo: TSmallintField;
    QrFatPedNFs_BSerieNFCod: TIntegerField;
    QrFatPedNFs_BSerieNFTxt: TWideStringField;
    QrFatPedNFs_BCO_ENT_EMP: TIntegerField;
    QrFatPedNFs_BDataCad: TDateField;
    QrFatPedNFs_BDataAlt: TDateField;
    QrFatPedNFs_BDataAlt_TXT: TWideStringField;
    QrFatPedNFs_BFreteVal: TFloatField;
    QrFatPedNFs_BSeguro: TFloatField;
    QrFatPedNFs_BOutros: TFloatField;
    QrFatPedNFs_BPlacaUF: TWideStringField;
    QrFatPedNFs_BPlacaNr: TWideStringField;
    QrFatPedNFs_BEspecie: TWideStringField;
    QrFatPedNFs_BMarca: TWideStringField;
    QrFatPedNFs_BNumero: TWideStringField;
    QrFatPedNFs_BkgBruto: TFloatField;
    QrFatPedNFs_BkgLiqui: TFloatField;
    QrFatPedNFs_BQuantidade: TWideStringField;
    QrFatPedNFs_BObservacao: TWideStringField;
    QrFatPedNFs_BCFOP1: TWideStringField;
    QrFatPedNFs_BDtEmissNF: TDateField;
    QrFatPedNFs_BDtEntraSai: TDateField;
    QrFatPedNFs_BStatus: TIntegerField;
    QrFatPedNFs_BinfProt_cStat: TIntegerField;
    QrFatPedNFs_BinfAdic_infCpl: TWideMemoField;
    QrFatPedNFs_BinfCanc_cStat: TIntegerField;
    QrFatPedNFs_BDTEMISSNF_TXT: TWideStringField;
    QrFatPedNFs_BDTENTRASAI_TXT: TWideStringField;
    QrFatPedNFs_BRNTC: TWideStringField;
    QrFatPedNFs_BUFembarq: TWideStringField;
    QrFatPedNFs_BxLocEmbarq: TWideStringField;
    QrFatPedNFs_Bide_tpNF: TSmallintField;
    QrFatPedNFs_BHrEntraSai: TTimeField;
    QrFatPedNFs_Bide_dhCont: TDateTimeField;
    QrFatPedNFs_Bide_xJust: TWideStringField;
    QrFatPedNFs_Bemit_CRT: TSmallintField;
    QrFatPedNFs_Bdest_email: TWideStringField;
    QrFatPedNFs_Bvagao: TWideStringField;
    QrFatPedNFs_Bbalsa: TWideStringField;
    QrFatPedNFs_BCabA_FatID: TIntegerField;
    QrFatPedNFs_BCompra_XNEmp: TWideStringField;
    QrFatPedNFs_BCompra_XPed: TWideStringField;
    QrFatPedNFs_BCompra_XCont: TWideStringField;
    QrFatPedNFs_BdhEmiTZD: TFloatField;
    QrFatPedNFs_BdhSaiEntTZD: TFloatField;
    QrFatPedNFs_BHrEmi: TTimeField;
    QrFatPedNFs_BLoteEnv: TIntegerField;
    QrFatPedCab: TMySQLQuery;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabNO_EMP: TWideStringField;
    QrFatPedCabCODMUNICI: TFloatField;
    QrFatPedCabUF_TXT_emp: TWideStringField;
    QrFatPedCabUF_TXT_cli: TWideStringField;
    QrFatPedCabNO_CLI: TWideStringField;
    QrFatPedCabinfAdFisco: TWideStringField;
    QrFatPedCabFinanceiro: TSmallintField;
    QrFatPedCabTotalFatur: TFloatField;
    QrFatPedCabtpNF: TSmallintField;
    QrFatPedCabDTAEMISS_TXT: TWideStringField;
    QrFatPedCabDTAENTRA_TXT: TWideStringField;
    QrFatPedCabDTAINCLU_TXT: TWideStringField;
    QrFatPedCabDTAPREVI_TXT: TWideStringField;
    QrFatPedCabDtaEmiss: TDateField;
    QrFatPedCabDtaEntra: TDateField;
    QrFatPedCabDtaInclu: TDateField;
    QrFatPedCabDtaPrevi: TDateField;
    QrFatPedCabFretePor: TIntegerField;
    QrFatPedCabModeloNF: TFloatField;
    QrFatPedCabRegrFiscal: TFloatField;
    QrFatPedCabCartEmis: TFloatField;
    QrFatPedCabTabelaPrc: TFloatField;
    QrFatPedCabCondicaoPG: TFloatField;
    QrFatPedCabTransporta: TFloatField;
    QrFatPedCabEmpresa: TFloatField;
    QrFatPedCabCliente: TFloatField;
    QrFatPedCabidDest: TSmallintField;
    QrFatPedCabindFinal: TSmallintField;
    QrFatPedCabindPres: TSmallintField;
    QrFatPedCabindSinc: TSmallintField;
    QrFatPedCabTpCalcTrib: TSmallintField;
    QrFatPedCabPedidoCli: TWideStringField;
    QrFatPedCabfinNFe: TSmallintField;
    QrFatPedCabId: TIntegerField;
    QrFatPedCabRetiradaUsa: TSmallintField;
    QrFatPedCabRetiradaEnti: TIntegerField;
    QrFatPedCabEntregaUsa: TSmallintField;
    QrFatPedCabEntregaEnti: TIntegerField;
    QrFatPedCabL_Ativo: TSmallintField;
    QrFatPedNFs_A: TMySQLQuery;
    QrNFeCabA: TMySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    PnNfeCabYA: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    EdvPag: TdmkEdit;
    EdvTroco: TdmkEdit;
    EdtPag: TdmkEdit;
    EdtPag_TXT: TdmkEdit;
    CkEditaYA: TCheckBox;
    PnCard: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdCNPJ: TdmkEdit;
    EdtpIntegra: TdmkEdit;
    EdtpIntegra_TXT: TdmkEdit;
    EdtBand: TdmkEdit;
    EdtBand_TXT: TdmkEdit;
    EdcAut: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPsq: TMySQLQuery;
    TabSheet4: TTabSheet;
    Panel9: TPanel;
    Panel10: TPanel;
    Label22: TLabel;
    EdVagao: TdmkEdit;
    Label23: TLabel;
    EdBalsa: TdmkEdit;
    Label24: TLabel;
    Eddest_email: TdmkEdit;
    Panel5: TPanel;
    GroupBox10: TGroupBox;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    EdCompra_XNEmp: TdmkEdit;
    EdCompra_XPed: TdmkEdit;
    EdCompra_XCont: TdmkEdit;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    EdUFEmbarq: TdmkEdit;
    EdxLocEmbarq: TdmkEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    RNTC: TLabel;
    EdPlacaNr: TdmkEdit;
    EdPlacaUF: TdmkEdit;
    EdObservacao: TdmkEdit;
    EdRNTC: TdmkEdit;
    QrFisRegCad: TMySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrFisRegCadFinanceiro: TSmallintField;
    DsFisRegCad: TDataSource;
    RGCRT: TdmkRadioGroup;
    Panel3: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdQuantidade: TdmkEdit;
    EdEspecie: TdmkEdit;
    EdMarca: TdmkEdit;
    EdNumero: TdmkEdit;
    EdkgBruto: TdmkEdit;
    EdkgLiqui: TdmkEdit;
    Panel11: TPanel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    SbTransporta: TSpeedButton;
    EdValFrete: TdmkEdit;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    GroupBox6: TGroupBox;
    SbRegrFiscal: TSpeedButton;
    Label58: TLabel;
    Label208: TLabel;
    Label32: TLabel;
    LaCNPJCPFAvulso: TLabel;
    LaRazaoNomeAvulso: TLabel;
    RG_idDest: TdmkRadioGroup;
    RG_indFinal: TdmkRadioGroup;
    EdindPres: TdmkEdit;
    EdindPres_TXT: TEdit;
    Edide_finNFe_TXT: TdmkEdit;
    Edide_finNFe: TdmkEdit;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    RGtpEmis: TdmkRadioGroup;
    EdCNPJCPFAvulso: TdmkEdit;
    EdRazaoNomeAvulso: TdmkEdit;
    Panel12: TPanel;
    StaticText1: TStaticText;
    MeinfAdic_infCpl: TdmkMemo;
    Panel13: TPanel;
    LaSerieNF: TLabel;
    EdSerieNF: TdmkEdit;
    Label15: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label16: TLabel;
    LaNumeroNF: TLabel;
    EdNumeroNF: TdmkEdit;
    TPDtEmissNF: TdmkEditDateTimePicker;
    Label18: TLabel;
    EdHrEmi: TdmkEdit;
    EddhEmiTZD: TdmkEdit;
    Label10: TLabel;
    SpeedButton21: TSpeedButton;
    EddhEmiVerao: TdmkEdit;
    Label26: TLabel;
    Label19: TLabel;
    TPDtEntraSai: TdmkEditDateTimePicker;
    EdHrEntraSai: TdmkEdit;
    Label25: TLabel;
    EddhSaiEntTZD: TdmkEdit;
    EddhSaiEntVerao: TdmkEdit;
    Label27: TLabel;
    Label33: TLabel;
    EdEmpresa: TdmkEdit;
    QrCliente: TMySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENT: TWideStringField;
    QrClienteIE: TWideStringField;
    QrClienteindIEDest: TIntegerField;
    QrClienteTipo: TIntegerField;
    QrClienteDOCENT: TWideStringField;
    CkEditCabGA: TdmkCheckBox;
    QrFatPedCabInfIntermedEnti: TFloatField;
    Panel14: TPanel;
    Panel15: TPanel;
    Ckide_indIntermed: TdmkCheckBox;
    PnIntermediador: TPanel;
    Label37: TLabel;
    SbInfIntermedEnti: TSpeedButton;
    EdInfIntermedEnti: TdmkEditCB;
    CBInfIntermedEnti: TdmkDBLookupComboBox;
    CkNaoEnviar: TCheckBox;
    QrPediVda: TMySQLQuery;
    QrPediVdaidDest: TSmallintField;
    QrPediVdaindFinal: TSmallintField;
    QrPediVdaindPres: TSmallintField;
    QrPediVdaindSinc: TSmallintField;
    QrPediVdafinNFe: TSmallintField;
    QrSumXprod_vFrete: TFloatField;
    QrSumXprod_vSeg: TFloatField;
    QrSumXprod_vDesc: TFloatField;
    QrSumXprod_vOutro: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrImprimeAfterOpen(DataSet: TDataSet);
    procedure QrImprimeBeforeClose(DataSet: TDataSet);
    procedure MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbStqMovValX_AfterInsert(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtSelecionadosClick(Sender: TObject);
    procedure GradeItensDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TPDtEmissNFChange(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFatPedNFs_BAfterScroll(DataSet: TDataSet);
    procedure QrFatPedNFs_AAfterScroll(DataSet: TDataSet);
    procedure EdtPagChange(Sender: TObject);
    procedure EdtPagKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdtpIntegraChange(Sender: TObject);
    procedure EdtpIntegraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBandChange(Sender: TObject);
    procedure EdtBandKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdindPresChange(Sender: TObject);
    procedure EdindPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValFreteChange(Sender: TObject);
    procedure SbInfIntermedEntiClick(Sender: TObject);
    procedure Ckide_indIntermedClick(Sender: TObject);
  private
    { Private declarations }
    //FStqMovValX: String;
    //FConfirmou: Boolean;
    F_indPres, F_finNFe: MyArrayLista;
    F_tPag, F_tpIntegra, F_tBand: MyArrayLista;
    //
    function  IncluiNFs(): Boolean;
    function  AlteraNFAtual(): Boolean;
    procedure ReopenQrImprime();
    procedure ReopenParamsEmp(Empresa: Integer);
    //function ImpedePeloCRT(): Boolean;
    procedure ConfereCSOSN(Quais: TSelType);
    function  InsereNFeCabYA(): Boolean;
  public
    { Public declarations }
    FFormChamou: TFormChamou;
    FThisFatID: Integer;
    FIDCtrl: Integer;
    // Configura��es cfe chamada:
    // F_Tipo=1 >> Faturamento de pedido
    // F_Tipo=102 >> CMPTOut(Blue Derm)
    F_Tipo, F_OriCodi, F_Empresa, F_ModeloNF, F_Cliente, F_EMP_FILIAL, F_AFP_Sit,
    F_Associada, F_ASS_FILIAL, F_EMP_CtaFaturas, F_ASS_CtaFaturas, F_CartEmis,
    F_CondicaoPG, F_EMP_FaturaDta, F_EMP_IDDuplicata, F_EMP_FaturaSeq,
    F_EMP_FaturaNum, F_TIPOCART, F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq,
    F_Financeiro, F_ASS_FaturaNum: Integer;
    F_EMP_TpDuplicata, F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
    F_ASS_TpDuplicata, F_ASS_TxtFaturas, FTabela: String;
    F_AFP_Per: Double;
    F_Abertura: TDateTime;
    FEditaNFeCabYA_Depois: Boolean;
    FReInsere, FConfirmou: Boolean;
    F_CtbCadMoF, F_CtbPlaCta, F_tpNF: Integer;

    // FatPedNFs_XXXX
    FFatPedCab,
    FEMP_FILIAL, FCU_Pedido, FCliente,
    //FPediVda, FEntidadeTipo
    FEmpresa: Integer;
    //FUFCli: String;
    ///
    //FOEstoqueJaFoiBaixado: Boolean;  //  2020-10-24
    ///
    FPediVda: Integer;
    ///
    procedure ReopenStqMovValX(ID: Integer);
    //procedure ReopenFatPedCab(FatPedCab: Integer);
    procedure InsereTextoObserv(Texto: String);
    //function Encerra(): Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
    function Encerra2(): Boolean;
    procedure DefineTZD_UTC();
    function  Step1_GeraNFe(const Recria, ApenasGeraXML, CalculaAutomatico:
              Boolean; var ide_tpEmis: Integer): Boolean;
    function  ReopenFatPedNFs_B(Filial: Integer): Boolean;
    procedure ReopenFatPedCab(Id: Integer);
    procedure ReopenImprime();
    procedure ReopenNFeCabA();
    function  Step2_Pagamentos(): Boolean;
    procedure ConfiguracoesIniciais();
    function  VerificaSeHaPagamentos(): Boolean;
    procedure ReopenCliente();
    function  ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer; Avisa: Boolean): Boolean;
  end;

  var
  FmNFaEdit_0400_Quiet: TFmNFaEdit_0400_Quiet;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, (*NF1b,*) MyDBCheck,
  UnFinanceiro, GetData, UnInternalConsts, UCreate, NFaEditCSOSN, UnGrade_Tabs,
  ModProd, ModuleNFe_0000, ModuleFatura, DmkDAC_PF, GetValor, NFe_PF,
  NFeCabA_0000, ModPediVda, UnEntities;

{$R *.DFM}

function TFmNFaEdit_0400_Quiet.AlteraNFAtual(): Boolean;
var
  Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEmi, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  GenCtbD, GenCtbC: Integer;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  Result := False;
  //usar QrFatPedNFs deste form!!!
  ReopenFatPedNFs(1, 0, True);
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
{
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
}
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Eddest_email.Text; // corrigido 2020-10-24
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  HrEmi        := EdHrEmi.Text;
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  //
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  //
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //ReopenQrImprime;
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
    'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
    'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
    'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
    'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
    'RNTC', 'UFEmbarq', 'xLocEmbarq',
    'HrEntraSai', 'vagao', 'balsa', 'dest_email',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
    'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
    'GenCtbD', 'GenCtbC'
  ], ['IDCtrl'], [
    F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
    SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
    Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
    Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
    EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
    HrEntraSai, vagao, balsa, dest_email,
    Compra_XNEmp, Compra_XPed, Compra_XCont,
    dhEmiTZD, dhSaiEntTZD, HrEmi,
    GenCtbD, GenCtbC
  ], [FIDCtrl], True) then
  begin
    Result := True;
  end;
end;

procedure TFmNFaEdit_0400_Quiet.BtOkClick(Sender: TObject);
  function Confirma(): Boolean;
  var
    Continua: Boolean;
  begin
    Result := False;
(*
    //YA. Formas de Pagamento
    //Grupo obrigat�rio para a NFC-e, a crit�rio da UF.
    //N�o informar para a NF-e
    //FEditaNFeCabYA_Depois := False;
    if PnNfeCabYA.Visible then
      if InsereNfeCabYA() = False then Exit;
    FEditaNFeCabYA_Depois := (PnNfeCabYA.Visible = False) or (CkEditaYA.Checked);
    //
    // fim YA. Formas de Pagamento
*)
    //
    //FConfirmou := True;
    FConfirmou := False;
    //
{   ----------------------------------------------------------------------------
    - ini desabilitado 2021-02-27
    - At� 27/02/2021 o CSOSN era manual na janela TFmNFaEdit_0400 e semelhantes,
      ap�s essa data poder� ser definido automaticamente na function
      TDmNFe_0000.InsUpdNFeIts(...
    ----------------------------------------------------------------------------
    if F_Tipo = VAR_FATID_0001 then
    begin
      ReopenParamsEmp(F_Empresa);
      //
      if QrParamsEmpCRT.Value = 1 then
      begin
        QrCSOSN.Close;
        QrCSOSN.Params[00].AsInteger := F_Tipo;
        QrCSOSN.Params[01].AsInteger := F_OriCodi;
        QrCSOSN.Params[02].AsInteger := F_EMpresa;
        UnDmkDAC_PF.AbreQuery(QrCSOSN, Dmod.MyDB);
        //
        if QrCSOSNItens.Value > 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'Esta empresa � obridada pelo seu CST = 1 a informar o CSOSN ' +
            sLineBreak + 'para cada produto da NF. Existem ' + FormatFloat('0',
            QrCSOSNItens.Value) + ' item(ns) sem esta informa��o!');
          //
          //Exit; 2021-02-27
        end;
      end;
    end;
   fim desabilitado 2021-02-27
}
    if ImgTipo.SQLType = stIns then
      Continua := IncluiNFs()
    else
      Continua := AlteraNFAtual();
    //
    if Continua = False then
    begin
      DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
        EdNumeroNF.ValueVariant);
      //Close;
    end else
    begin
      FConfirmou := True;
      ReopenFatPedCab((*QrFatPedCabId.Value*)0);
      Result := True;
    end;
  end;
var
  Codigo, RegrFiscal, idDest, indFinal, indPres, indSinc, finNFe, modFrete,
  FretePor, Transporta, ide_tpEmis, Empresa: Integer;
  Frete_V: Double;
  SQLType: TSQLType;
  CNPJCPFAvulso, RazaoNomeAvulso, Mensagem: String;
  EmiteAvulso: Integer;
  InfIntermedEnti: Integer;
begin
  //Empresa       := EdEmPresa.ValueVariant;
  Empresa       := FEmpresa;
  CNPJCPFAvulso := Geral.SoNumero_TT(EdCNPJCPFAvulso.Text);
  RazaoNomeAvulso := EdRazaoNomeAvulso.Text;
  EmiteAvulso     := 0; //  = False;

  ide_tpEmis := Geral.IMV(RGtpEmis.Items[RGtpEmis.ItemIndex][1]);

{ Somente para NFC-e?
  ReopenCTG(Empresa);
  //
  if ide_tpEmis <> 1 then
  begin
    if QrCTG.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
      'Refa�a a configura��o de conting�ncia!');
      Exit;
    end;
  end else
  begin
    if QrCTG.RecordCount <> 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia! Para emitir em modo normal NFCe encerre a conting�ncia! ' + sLineBreak +
      'A configura��o pode ser acessada na janela principal guia NF-e)' + sLineBreak +
      'Antes de encerrar a conting�ncia � necess�rio autorizar as NFC-es emitidas em conting�ncia enviando ao fisco!');
      Exit;
    end;
  end;
}
  //
  if PnNfeCabYA.Visible then
    if InsereNfeCabYA() = False then Exit;
  FEditaNFeCabYA_Depois := (PnNfeCabYA.Visible = False) or (CkEditaYA.Checked);
  //
  SQLType := stUpd;
  //
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;

  //if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
(*  Somente para NFC-e
    if MyObjects.FIC(RG_indFinal.ItemIndex <> 1, RG_indFinal,
      'Opera��o deve ser com o consumidor final!') then Exit;
*)
    //{$IfNDef SemNFe_0000}
      if MyObjects.FIC(EdindPres.Text = '', EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;
      indPres := Geral.IMV(EdindPres.Text);
      if MyObjects.FIC(not (indPres in [0,1,2,3,4,9]), EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;

      if MyObjects.FIC(Edide_finNFe.Text = '', Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
      finNFe := Geral.IMV(Edide_finNFe.Text);
      if MyObjects.FIC(not (finNFe in [1,2,3,4]), Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
    //{$EndIf}
  end;

{
  if DmPediVda.QrFisRegCadFinanceiro.Value > 0 then
  begin
    if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
      'Informe a carteira!') then Exit;
  end;
  // 2019-03-25 Erro! informando filial como vari�vel mas o certo � o c�digo da entidade!
  //DmPediVda.ReopenParamsEmp(EdEmpresa.ValueVariant, True); /
  DmPediVda.ReopenParamsEmp(DModG.QrEmpresasCodigo.Value, True);
  // Fim 2019-03-25
  case DmPediVda.QrParamsEmpFatSemPrcL.Value of
    0: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
    'Informe a tabela de pre�os!') then Exit;
    1: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
      'ATEN��O! N�o foi informada a tabela de pre�os!') then (*Exit*);
  end;
  //if MyObjects.FIC(EdFretePor.ValueVariant = 0, EdFretePor,
    //'Informe a o respons�vel pelo frete!') then Exit;
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DmPediVda.QrParamsEmpversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
  //ErroFatura := FaturaParcialIncorreta(Txt);
  //if MyObjects.FIC(ErroFatura, CkAFP_Sit,
    //'Fatura parcial n�o permitida!' + sLineBreak + Txt) then Exit;
  //Valida regra fiscal com os dados do cliente
}
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;

  RegrFiscal := QrFisRegCadCodigo.Value;
  idDest     := RG_idDest.ItemIndex;
  indFinal   := RG_indFinal.ItemIndex;
  indPres    := Geral.IMV(EdindPres.Text);
  finNFe     := Edide_finNFe.ValueVariant;
  indSinc    := 1; // Sincrono
  Transporta := EdTransporta.ValueVariant;
  if Ckide_indIntermed.Checked then
    InfIntermedEnti := EdInfIntermedEnti.ValueVariant
  else
    InfIntermedEnti := 0;
  //
  Frete_V    := EdValFrete.ValueVariant;
  //
  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    ReopenCliente();

    if MyObjects.FIC(QrClienteDOCENT.Value = '', (*EdCliente*)nil,
      'O Destinat�rio n�o possui CNPJ / CPF cadastrado!') then
    begin
      (*
      if Geral.MB_Pergunta('Deseja editar agora?' + sLineBreak +
        'AVISO: Verifique se os demais campos est�o devidamente preenchidos!') = ID_YES then
      begin
        MostraFormEntidade2(EdCliente.ValueVariant, QrCliente,
          EdCliente, CBCliente);
      end;
      *)
      Exit;
    end;
    // NFe 3.10
    if MyObjects.FIC(RG_idDest.ItemIndex <= 0, RG_idDest,
      'Informe o Local de Destino da Opera��o!') then Exit;
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade_2(QrClienteIE.Value,
      QrClienteindIEDest.Value, QrClienteTipo.Value, idDest, indFinal, False,
      Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
  end;
(*
  if CkEntregaUsa.Checked = False then
  begin
    EdEntregaEnti.ValueVariant := 0;
    CBEntregaEnti.KeyValue := 0;
  end;
  if CkRetiradaUsa.Checked = False then
  begin
    EdRetiradaEnti.ValueVariant := 0;
    CBRetiradaEnti.KeyValue := 0;
  end;
*)





  //
(* Somente na NFC-e
  // Erro 753
  // NFC-e com Frete e n�o � entrega a domic�lio
  //(tag:modFrete<>9 e indPres<>4)
  if not DModG.DefineFretePor(EdFretePor.ValueVariant, FretePor, modFrete) then
    Exit;
  if MyObjects.FIC((modFrete <> 9) and (indPres <> 4), EdFretePor,
  'NFC-e com Frete e n�o � entrega a domic�lio!') then Exit;
  //
  // Status = 786
  // Motivo = NFC-e de entrega a domicilio sem dados do Transportador
  // indPres = 4 => NFC-e em opera��o com entrega a domic�lio;
  if MyObjects.FIC((Transporta = 0) and (indPres = 4), EdFretePor,
  'Entrega a domicilio sem dados do Transportador!') then Exit;
*)

  //
  Codigo     := FPediVda;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivda', False, [
  (*'CodUsu', 'Empresa', 'Cliente',
  'DtaEmiss', 'DtaEntra', 'DtaInclu',
  'DtaPrevi', 'Represen', 'Prioridade',
  'CondicaoPG', 'Moeda', 'Situacao',
  'TabelaPrc', 'MotivoSit', 'LoteProd',
  'PedidoCli',*) 'FretePor', 'Transporta',
  (*'Redespacho',*)
  'RegrFiscal',
  (*'DesoAces_V',
  'DesoAces_P',*) 'Frete_V', (*'Frete_P',
  'Seguro_V', 'Seguro_P', 'TotalQtd',
  'Total_Vlr', 'Total_Des', 'Total_Tot',
  'Observa', 'AFP_Sit', 'AFP_Per',
  'ComisFat', 'ComisRec', 'CartEmis',
  'QuantP', 'ValLiq', 'Tipo',
  'EntSai', 'CondPag', 'CentroCust',
  'ItemCust', 'UsaReferen',*)
  'idDest',
  'indFinal', 'indPres', 'IndSinc',
  'finNFe'
  (*, 'RetiradaUsa', 'RetiradaEnti',
  'EntregaUsa', 'EntregaEnti'*),
  'CNPJCPFAvulso', 'RazaoNomeAvulso',
  'EmiteAvulso',
  'InfIntermedEnti'], [
  'Codigo'], [
  (*CodUsu, Empresa, Cliente,
  DtaEmiss, DtaEntra, DtaInclu,
  DtaPrevi, Represen, Prioridade,
  CondicaoPG, Moeda, Situacao,
  TabelaPrc, MotivoSit, LoteProd,
  PedidoCli,*) FretePor, Transporta,
  (*Redespacho,*)
  RegrFiscal,
  (*DesoAces_V,
  DesoAces_P,*) Frete_V, (*Frete_P,
  Seguro_V, Seguro_P, TotalQtd,
  Total_Vlr, Total_Des, Total_Tot,
  Observa, AFP_Sit, AFP_Per,
  ComisFat, ComisRec, CartEmis,
  QuantP, ValLiq, Tipo,
  EntSai, CondPag, CentroCust,
  ItemCust, UsaReferen,*)
  idDest,
  indFinal, indPres, IndSinc,
  finNFe
  (*, RetiradaUsa, RetiradaEnti,
  EntregaUsa, EntregaEnti*),
  CNPJCPFAvulso, RazaoNomeAvulso,
  EmiteAvulso,
  InfIntermedEnti], [
  Codigo], True) then
  begin
    if Confirma() then
      Close
      ;
  end;
end;

{
function TFmNFaEdit.ImpedePeloCRT(): Boolean;
begin
  QrEnti.Close;
  QrEnti.Params[0].AsInteger := F_Cliente;
    UnDmkDAC_PF.AbreQuery(QrEnti. O p e n ;
  //
  Result := QrEntiCRT.Value <> 1;
  if Result then
  begin
    Geral.MB_(
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!',
    'Aviso', MB_OK+MB_ICONWARNING)
  end;
end;
}

function TFmNFaEdit_0400_Quiet.IncluiNFs(): Boolean;
  procedure ExcluiNF();
  begin
    // excluir nfe stqmovnfsa para n�o gerar erro quando tentar de novo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
    Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MB_Erro('Erro no encerramento!' + sLineBreak +
    'Feche a janela e tente encerrar novamente!' + sLineBreak +
    'Caso n�o consiga, AVISE A DERMATEK.');
    //
    Close;
  end;
var
  IDCtrl, Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF, emit_CRT: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont, HrEmi: String;
  Incluir, Encerrou: Boolean;
  SQLTYpe: TSQLType;
  GenCtbD, GenCtbC: Integer;
begin
  Result := False;
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  //usar QrFatPedNFs deste form!!!
  Encerrou    := False;
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
  {
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
  }
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);

  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Eddest_email.Text;
  emit_CRT    := RGCRT.ItemIndex;
  if MyObjects.FIC(emit_CRT < 1, RGCRT, 'Informe o CRT!') then Exit;
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  HrEmi        := EdHrEmi.Text;
  //
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  //
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  //
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  // ini 2021-02-17 Evitar erro de reinclus�o!
  // d� para aproveitar ourtros dados?
  //
  if FReInsere then
    SQLType := stUpd // registro j� exite e NF n�o foi emitido por algum outro problema
  else
    SQLType := stIns;
  //IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  IDCtrl  := UMyMod.Busca_IDCtrl_NFe(SQLType, FIDCtrl);
  // fim 2021-02-17 Evitar erro de reinclus�o!
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, (*stIns*)SQLTYpe, 'stqmovnfsa', False, [
      'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
      'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
      'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
      'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
      'RNTC', 'UFEmbarq', 'xLocEmbarq',
      'HrEntraSai', 'vagao', 'balsa', 'dest_email', 'emit_CRT',
      'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
      'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
      'GenCtbD', 'GenCtbC'
    ], ['IDCtrl'], [
      F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
      SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
      Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
      Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
      EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
      HrEntraSai, vagao, balsa, dest_email, emit_CRT,
      Compra_XNEmp, Compra_XPed, Compra_XCont,
      dhEmiTZD, dhSaiEntTZD, HrEmi,
      GenCtbD, GenCtbC
    ], [IDCtrl], True) then
    begin
      ReopenFatPedNFs(2, 0, False);
      if QrFatPedNFs.RecordCount > 0 then
      begin
        while not QrFatPedNFs.Eof do
        begin
          Incluir := True;
          //
          if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
          (*QrFatPedCabSerieDesfe.Value*)-1,
          (*QrFatPedCabNFDesfeita.Value*)0,
          QrImprimeCO_SerieNF.Value,
          QrImprimeCtrl_nfs.Value,
          QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger,
          QrFatPedNFs.FieldByName('Filial').AsInteger,
          QrImprimeMaxSeqLib.Value,
          EdSerieNF, EdNumeroNF(*),
          SerieNFTxt, NumeroNF*)) then
          begin
            // ????
          end;
(*
          SerieNFTxt := IntToStr(EdSerieNF.ValueVariant);
          NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfs', 'Sequencial',
          'WHERE Controle=' + dmkPF.FFP(QrImprimeCtrl_nfs.Value, 0), 0,
          QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + sLineBreak +
          'Filial: ' + dmkPF.FFP(QrFatPedNFsFilial.Value, 0));
*)
          //
          Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
          SerieNFCod  := QrImprimeCO_SerieNF.Value;
          IncSeqAuto  := QrImprimeIncSeqAuto.Value;
          //NumeroNF    := EdNumeroNF.ValueVariant;
          {
          FreteVal    := 0;
          Seguro      := 0;
          Outros      := 0;
          }
          kgBruto     := 0;
          kgLiqui     := 0;
          PlacaUF     := '';
          PlacaNr     := '';
          Especie     := '';
          Marca       := '';
          Numero      := '';
          Quantidade  := '';
          Observacao  := '';
          CFOP1       := '';
          //
          if IncSeqAuto = 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
            Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
            Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
            Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
            Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
            Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
            Dmod.QrUpd.ExecSQL;
          end;
          if NumeroNF < 1 then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir and (NumeroNF > QrImprimeMaxSeqLib.Value) then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir then
          begin
            try
              IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
              'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
              'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
              'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
              'Quantidade', 'CFOP1', 'infAdic_infCpl', 'RNTC',
              'UFEmbarq', 'xLocEmbarq',
              'HrEntraSai', 'vagao', 'balsa', 'dest_email',
              'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
              'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
              'GenCtbD', 'GenCtbC'
              ], ['IDCtrl'], [
              F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
              SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
              Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
              Quantidade, CFOP1, MeinfAdic_infCpl.Text, EdRNTC.Text,
              EdUFEmbarq.Text, EdxLocEmbarq.Text,
              HrEntraSai, vagao, balsa, dest_email,
              Compra_XNEmp, Compra_XPed, Compra_XCont,
              dhEmiTZD, dhSaiEntTZD, HrEmi,
              GenCtbD, GenCtbC
              ], [IDCtrl], True) then
              begin
              // 
              end;
            except
              ExcluiNF();
              raise;
            end;
            //
          end else
            ExcluiNF();
          QrFatPedNFs.Next;
        end;
      end;
      //
      if FFormChamou = fcFmFatPedCab then
      begin
        Encerrou := Encerra2();
      end;
      if Encerrou then
      begin
        ReopenFatPedNFs(1, 0, True);
        Result := True;
      end;
    end;
  except
    ExcluiNF();
    raise;
  end;
end;

function TFmNFaEdit_0400_Quiet.InsereNFeCabYA(): Boolean;
var
  FatID, FatNum, Empresa, Controle, tPag, tpIntegra, tBand: Integer;
  vPag, vTroco: Double;
  CNPJ, cAut: String;
  SQLType: TSQLType;
begin
  Result := False;
  //
  FatID    := FThisFatID;
  FatNum   := FFatPedCab;
  Empresa  := FEmpresa;
  //
  //Controle := EdControle.ValueVariant;
  Controle := 0;
  SQLType := stIns;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM nfecabya',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
    if Controle <> 0 then
      SQLType := stUpd;
  end;
  //
  tPag      := Geral.IMV(EdtPag.ValueVariant);
  vPag      := EdvPag.ValueVariant;
  vTroco    := EdvTroco.ValueVariant;
  tpIntegra := Geral.IMV(EdtpIntegra.ValueVariant);
  CNPJ      := EdCNPJ.ValueVariant;
  tBand     := Geral.IMV(EdtBand.ValueVariant);
  cAut      := EdcAut.ValueVariant;
  //
  if MyObjects.FIC(FatID = 0, nil, 'FatID n�o informado!') then Exit;
  if MyObjects.FIC(FatNum = 0, nil, 'FatNum n�o informado!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o informada!') then Exit;
  if MyObjects.FIC(tPag = 0, EdtPag, 'Informe o meio de pagamento!') then Exit;
  if MyObjects.FIC(tpIntegra = 0,  EdtpIntegra, 'Tipo de Integra��o para pagamento!') then Exit;
  //
  if SQLType = stIns then
    //EdControle.ValueVariant :=
    Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
  //
  //Controle := EdControle.ValueVariant;
  //
  Result :=  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabya', False,
    ['tPag', 'vPag', 'vTroco', 'tpIntegra', 'CNPJ', 'tBand', 'cAut'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    [tPag, vPag, vTroco, tpIntegra, CNPJ, tBand, cAut],
    [FatID, FatNum, Empresa, Controle], True);(* then
  begin
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, FmNFeCabA_0000.LaAviso1,
      FmNFeCabA_0000.LaAviso2, FmNFeCabA_0000.REWarning);
    FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value,
      FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
    //
    Close;
  end;*)
end;

procedure TFmNFaEdit_0400_Quiet.InsereTextoObserv(Texto: String);
var
  TextoA, TextoD: String;
  Pos: Integer;
begin
  Pos := MeinfAdic_infCpl.SelStart;
  //
  TextoA := Copy(MeinfAdic_infCpl.Text, 1, Pos);
  TextoD := Copy(MeinfAdic_infCpl.Text, Pos + 1, Length(MeinfAdic_infCpl.Text));
  //
  MeinfAdic_infCpl.Text := TextoA + ' ' + Texto + ' ' + TextoD;
end;

procedure TFmNFaEdit_0400_Quiet.MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    UnNFe_PF.MostraFormNFaInfCpl(MeinfAdic_infCpl);
end;

procedure TFmNFaEdit_0400_Quiet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaEdit_0400_Quiet.BtSelecionadosClick(Sender: TObject);
begin
  ConfereCSOSN(istSelecionados);
end;

procedure TFmNFaEdit_0400_Quiet.BtTodosClick(Sender: TObject);
begin
  ConfereCSOSN(istTodos);
end;

procedure TFmNFaEdit_0400_Quiet.Ckide_indIntermedClick(Sender: TObject);
begin
  PnIntermediador.Visible := Ckide_indIntermed.Checked;
end;

procedure TFmNFaEdit_0400_Quiet.ConfereCSOSN(Quais: TSelType);
  procedure AtualizaItemAtual(CSOSN, FinaliCli: Integer; pCredSN: Double);
  var
    ID: Integer;
  begin
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'CSOSN', 'pCredSN', 'FinaliCli'], ['ID'
    ], [
    CSOSN, pCredSN, FinaliCli], [ID
    ], False) then ;
  end;
var
  I, CSOSN, FinaliCli: Integer;
  pCredSN: Double;
begin
  ReopenParamsEmp(F_Empresa);
  //
  if QrParamsEmpCRT.Value <> 1 then
  begin
    Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!'
    + sLineBreak + sLineBreak +
    'Verifique com seu contador como proceder em caso de devolu��o de ' + sLineBreak +
    'mercadoria de fornecedor que emitiu NF-e pelo Simples Nacional!');
    // Deixar editar porque pode ser devolu��o de NFe!
    //Exit;
  end;
  if DBCheck.CriaFm(TFmNFaEditCSOSN, FmNFaEditCSOSN, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSN.FDtEmissNF := TPDtEmissNF.Date;
    FmNFaEditCSOSN.FEmpresa   := F_Empresa;
    //
    FmNFaEditCSOSN.ShowModal;
    Screen.Cursor := crHourGlass;
    try
      if FmNFaEditCSOSN.FConfirmou then
      begin
        CSOSN     := FmNFaEditCSOSN.EdCSOSN.ValueVariant;
        pCredSN   := FmNFaEditCSOSN.EdpCredSN.ValueVariant;
        FinaliCli := FmNFaEditCSOSN.RGFinaliCli.ItemIndex;
        case Quais of
          istTodos:
          begin
            QrStqMovValX.First;
            while not QrStqMovValX.Eof do
            begin
              AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              QrStqMovValX.Next;
            end;
          end;
          istSelecionados:
          begin
            if GradeItens.SelectedRows.Count > 0 then
            begin
              with GradeItens.DataSource.DataSet do
              for I:= 0 to GradeItens.SelectedRows.Count-1 do
              begin
                //GotoBookmark(pointer(GradeItens.SelectedRows.Items[I]));
                GotoBookmark(GradeItens.SelectedRows.Items[I]);
                AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              end;
            end else AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      FmNFaEditCSOSN.Destroy;
    end;
  end;
  ReopenStqMovValX(QrStqMovValXID.Value);
end;

procedure TFmNFaEdit_0400_Quiet.ConfiguracoesIniciais();
begin
{$IfNDef SemNFe_0000}
  {$IfDef cLocacao}
    RG_idDest.ItemIndex       := 1; // Operacao interna
    RG_indFinal.ItemIndex     := 1; // Consumidor final
    EdindPres.ValueVariant    := 1; // Presencial
    Edide_finNFe.ValueVariant := 1; // Normal
    //EdtPag.ValueVariant       := 1; // Dinheiro
    EdtpIntegra.ValueVariant  := 2; // Pagamento n�o integrado com o sistema de automa��o da empresa (Ex.: equipamento POS);
  {$Else}
  //ver o que fazer!
    //ReopenFatPedCab(0);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
    'SELECT pvd.idDest, pvd.indFinal, pvd.indPres, pvd.finNFe, pvd.indSinc ',
    'FROM pedivda pvd ',
    'WHERE CodUsu=' + Geral.FF0(FCU_Pedido),
    '']);
    //
    RG_idDest.ItemIndex       := QrPediVdaidDest.Value;
    RG_indFinal.ItemIndex     := QrPediVdaindFinal.Value;
    EdindPres.ValueVariant    := QrPediVdaindPres.Value;
    Edide_finNFe.ValueVariant := QrPediVdafinNFe.Value;
    //EdtPag.ValueVariant       := 1; // Dinheiro
    EdtpIntegra.ValueVariant  := 2; // Pagamento n�o integrado com o sistema de automa��o da empresa (Ex.: equipamento POS);
  {$EndIf}
{$EndIf}
  //
end;

procedure TFmNFaEdit_0400_Quiet.DefineTZD_UTC();
var
  Data: TDateTime;
  TZD_UTC: Double;
  hVerao: Boolean;
  SimNao: String;
  Cor: Integer;
begin
  // NFE 3.10
  Data := TPDtEmissNF.Date;
  hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
  SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
  Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  EddhEmiVerao.Text := SimNao;
  EddhEmiVerao.Font.Color := Cor;
  EddhEmiTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  if TPDtEntraSai.Date <> TPDtEmissNF.Date then
  begin
    Data   := TPDtEntraSai.Date;
    hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
    SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
    Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  end;
  EddhSaiEntTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  EddhSaiEntVerao.Text := SimNao;
  EddhSaiENtVerao.Font.Color := Cor;
end;

procedure TFmNFaEdit_0400_Quiet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    PnNfeCabYA.Visible := VerificaSeHaPagamentos() = False;
    EdtPag.SetFocus;
  except
    ;
  end;
end;

procedure TFmNFaEdit_0400_Quiet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*
  if not FConfirmou then
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant)
*)
end;

procedure TFmNFaEdit_0400_Quiet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl2.ActivePageIndex := 0;
  FConfirmou := False;
  DefineTZD_UTC();
  CBFretePor.ListSource := DmPediVda.DsFretePor;
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  if DmPediVda.QrTransportas.State = dsInactive then
    DmPediVda.ReopenTransportas();
  CBTransporta.ListSource := DmPediVda.DsTransportas;
  if DmPediVda.QrInfIntermedEnti.State = dsInactive then
    DmPediVda.ReopenInfIntermedEnti();
  CBInfIntermedEnti.ListSource := DmPediVda.DsInfIntermedEnti;

  FReInsere := False;

  //FOEstoqueJaFoiBaixado := False;
  //
  FThisFatID := 1;
  FTabela    := '';
  // N�o pode ser aqui precisa criar primeiro!!!
  // Chamar na cria��o antes do ShowModal
  //ReopenFatPedNFs(1,0);
  TPDtEmissNF.Date          := DmodG.ObtemAgora;
  TPDtEntraSai.Date         := DmodG.ObtemAgora;
  EdHrEmi.ValueVariant      := DmodG.ObtemAgora;
  EdHrEntraSai.ValueVariant := DmodG.ObtemAgora;
  //
  TbStqMovValX_.Database := DModG.MyPID_DB;
  //
  //DefineTZD_UTC();

{$IfNDef SemNFe_0000}
  F_indPres   := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe    := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
  F_tPag      := UnNFe_PF.ListaMeiosDePagamento();
  F_tpIntegra := UnNFe_PF.ListaTiposDeIntegracaoParaPagamento();
  F_tBand     := UnNFe_PF.ListaBandeirasDaOperadoraDeCartao();
{$Else}
  F_indPres   := 0;
  F_finNFe    := 0;
  F_tPag      := 0;
  F_tpIntegra := 0;
  F_tBand     := 0;
{$EndIf}
end;

procedure TFmNFaEdit_0400_Quiet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaEdit_0400_Quiet.FormShow(Sender: TObject);
var
  NFeInfCpl_TXT: String;
begin
  ReopenParamsEmp(F_Empresa);
  //
  NFeInfCpl_TXT := QrParamsEmpNFeInfCpl_TXT.Value;
  //
(* 2021-02-27
  if QrParamsEmpCRT.Value in [1,2] then
    PageControl1.ActivePageIndex := 1
  else
*)
    PageControl1.ActivePageIndex := 0;
  //
  if F_Cliente <> 0 then //Configura endereco de entrega
  begin
    if DmodG.ReopenEntrega(F_Cliente) then
    begin
      if DmodG.QrEntregaL_Ativo.Value = 1 then
        InsereTextoObserv('Endere�o de entrega: ' + DmodG.QrEntregaE_ALL.Value);
    end;
  end;

  if NFeInfCpl_TXT <> '' then
    InsereTextoObserv(NFeInfCpl_TXT);
end;

procedure TFmNFaEdit_0400_Quiet.GradeItensDblClick(Sender: TObject);
const
  FormCaption  = 'Valor Aproximado dos Tributos';
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
  iTotTrib     = 1; // Informou manualmente!
var
  vTotTrib, pTotTrib: Double;
  ValVar: Variant;
  ID: Integer;
begin
{
GetValorDmk(ComponentClass: TComponentClass; Reference:
TComponent; FormatType: TAllFormat; Default: Variant;
Casas, LeftZeros: Integer; ValMin, ValMax: String;
Obrigatorio: Boolean; FormCaption, ValCaption: String;
WidthVal: Integer; var Resultado: Variant): Boolean;
}
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    QrStqMovValXvTotTrib.Value, 4, 0, '0,00', FormatFloat('0.00',
    QrStqMovValXTotal.Value), True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    vTotTrib := Geral.DMV(ValVar);
    if QrStqMovValXTotal.Value <> 0 then
      pTotTrib := vTotTrib / QrStqMovValXTotal.Value * 100
    else
      pTotTrib := 0;
    //
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'iTotTrib', 'vTotTrib', 'pTotTrib'], [
    'ID'], [
    iTotTrib, vTotTrib, pTotTrib], [
    ID], False) then
    begin
      ReopenStqMovValX(ID);
    end;
  end;
end;

procedure TFmNFaEdit_0400_Quiet.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
{
  QrVolumes.Close;
  QrVolumes.Params[00].AsInteger := F_OriCodi;
    UnDmkDAC_PF.AbreQuery(QrVolumes. O p e n ;
}
  ReopenQrImprime();
end;

procedure TFmNFaEdit_0400_Quiet.QrFatPedNFs_AAfterScroll(DataSet: TDataSet);
begin
  ReopenImprime();
end;

procedure TFmNFaEdit_0400_Quiet.QrFatPedNFs_BAfterScroll(DataSet: TDataSet);
begin
  ReopenImprime();
  ReopenNFeCabA();
end;

procedure TFmNFaEdit_0400_Quiet.QrImprimeAfterOpen(DataSet: TDataSet);
begin
  if QrImprime.RecordCount = 0 then
    Geral.MB_Aviso('Esta filial n�o est� ativa no Modelo de NF selecionado '+
    'na Regra Fiscal deste faturamento!');
  BtOK.Enabled := QrImprime.RecordCount > 0 ;
end;

procedure TFmNFaEdit_0400_Quiet.QrImprimeBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

{
procedure TFmNFaEdit.ReopenFatPedCab(FatPedCab: Integer);
begin
  F_.Close;
  F_.Params[0].AsInteger := FatPedCab;
    UnDmkDAC_PF.AbreQuery(F_.O p e n ;
end;
}

procedure TFmNFaEdit_0400_Quiet.ReopenCliente();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliente, Dmod.MyDB, [
  'SELECT Codigo, IE, ',
  'IF(Tipo=0, RazaoSocial, Nome) NOMEENT, ',
  'IF(Tipo=0, CNPJ, CPF) DOCENT, ',
  'Observacoes, indIEDest, Tipo  ',
  'FROM entidades ent ',
  'WHERE Codigo=' + Geral.FF0(FCliente),
  '']);
end;

procedure TFmNFaEdit_0400_Quiet.ReopenFatPedCab(Id: Integer);
//var
  //Empresa: Integer;
begin
  QrFatPedCab.Close;
{
  if EdFilial.ValueVariant = 0 then
    Exit;
  //
  Empresa := QrFiliaisCodigo.Value;
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedCab, Dmod.MyDB, [
    'SELECT val.Id, ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou, ',
    'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP, ',
    'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI, ',
    'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli, ',
    'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI, ',
    'frc.ModeloNF + 0.000 ModeloNF, frc.infAdFisco, frc.Financeiro,  ',
    'SUM(val.Total) TotalFatur, frc.TipoMov tpNF,  frc.TpCalcTrib, ',
    'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,  ',
    'pvd.RegrFiscal + 0.000 RegrFiscal, pvd.CartEmis + 0.000  ',
    'CartEmis , pvd.TabelaPrc + 0.000 TabelaPrc,  ',
    'pvd.CondicaoPG + 0.000 CondicaoPG, pvd.FretePor,  ',
    'pvd.Transporta + 0.000 Transporta, pvd.Empresa + 0.000  ',
    'Empresa, pvd.Cliente + 0.000 Cliente, pvd.PedidoCli, ',
    // NFe 3.10
    'pvd.idDest, pvd.indFinal, pvd.indPres, pvd.finNFe, pvd.indSinc, ',
    // NFe 4.00 NT 2018/5
    'pvd.RetiradaUsa, pvd.RetiradaEnti, pvd.EntregaUsa, pvd.EntregaEnti, ',
    'pvd.InfIntermedEnti + 0.000 InfIntermedEnti, ',
    'cli.L_Ativo ',
    // Fim NFe 4.00 NT 2018/5
    //
    'FROM stqmovvala val  ',
    'LEFT JOIN fatpedcab fpc ON fpc.Codigo = val.OriCodi  ',
    'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF) ',
    'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF) ',
    'WHERE fpc.Encerrou > 0 ',
    'AND pvd.Empresa=' + FormatFloat('0', FEmpresa),
    Geral.ATS_If(FCU_Pedido <> 0,
      ['AND pvd.CodUsu=' + FormatFloat('0', FCU_Pedido)]),
    Geral.ATS_If(FCliente <> 0,
      ['AND pvd.Cliente=' + FormatFloat('0', FCliente)]),
    //
    'GROUP BY val.OriCodi ',
    'ORDER BY Encerrou DESC ',
    '']);
  if Id <> 0 then
    QrFatPedCab.Locate('Id', Id, []);
end;

function TFmNFaEdit_0400_Quiet.ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer;
  Avisa: Boolean): Boolean;
const
  sProcName = 'TFmNFaEdit_0400_Quiet.ReopenFatPedNFs()';
var
  Txt: String;
begin
  Result := False;
  if QuaisFiliais = 1 then
    Txt := '='
  else
    Txt := '<>';
  //
  QrFatPedNFs.Close;
  QrFatPedNFs.SQL.Clear;
  QrFatPedNFs.SQL.Add('SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP,');
  QrFatPedNFs.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*');
  QrFatPedNFs.SQL.Add('FROM stqmovvala smva');
  QrFatPedNFs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa');
  QrFatPedNFs.SQL.Add('LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa');
  QrFatPedNFs.SQL.Add('          AND smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('          AND smna.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('AND ent.Codigo' + Txt +  FormatFloat('0', F_Empresa));
  QrFatPedNFs.SQL.Add('ORDER BY ent.Filial');
  QrFatPedNFs.SQL.Add('');
  //
  //Geral.MB_Teste(QrFatPedNFs.SQL.Text);
  {
  QrFatPedNFs.Params[00].AsInteger := F_Tipo;
  QrFatPedNFs.Params[01].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[02].AsInteger := F_Tipo;
  QrFatPedNFs.Params[03].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[04].AsInteger := F_Empresa;
  }
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs, Dmod.MyDB);
  //
  if QrFatPedNFs.RecordCount = 0 then
  begin
    if Avisa then
      Geral.MB_Info('Nenhum item foi localizado no faturamento: ' +
      Geral.FF0(F_OriCodi) + sLineBreak + sProcName);
  end else
  begin
    Result := True;
    QrFatPedNFs.Locate('Filial', FilialLoc, []);
  end;
end;

function TFmNFaEdit_0400_Quiet.ReopenFatPedNFs_B(Filial: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedNFs_B, Dmod.MyDB, [
  'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, ',
  'nfe.infCanc_cStat, nfe.LoteEnv, ',
  'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.* ',
  'FROM stqmovnfsa smna ',
  'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa ',
  'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl ',
  'WHERE smna.Tipo=' + Geral.FF0(F_Tipo),//Geral.FF0(FThisFatID),
  'AND smna.OriCodi=' + Geral.FF0(QrFatPedCabCodigo.Value),
  'AND smna.Empresa=' + Geral.FF0(Trunc(QrFatPedCabEmpresa.Value)),
  'ORDER BY ent.Filial ',
  '']);
  //
  //Geral.MB_Teste(QrFatPedNFs_B.SQL.Text);
  Result := QrFatPedNFs_B.Locate('Filial', Filial, []);
end;

procedure TFmNFaEdit_0400_Quiet.ReopenImprime;
begin
  QrImprime.Close;
{
  if QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger = F_Empresa then
  begin
}



(*
    QrImprime.Params[00].AsInteger := F_Empresa;
    QrImprime.Params[01].AsInteger := F_ModeloNF;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrImprime, Dmod.MyDB, [
  'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao, ',
  'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto,  ',
  'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib ',
  'FROM imprimeempr ime ',
  'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo ',
  'LEFT JOIN paramsnfcs nfs ON nfs.Controle=ime.ParamsNFCs ',
  'WHERE ime.Empresa=' + Geral.FF0(F_Empresa),
  'AND ime.Codigo=' + Geral.FF0(F_ModeloNF),
  '']);
  //Geral.MB_Teste(QrImprime.SQL.Text);



{
  end else begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    end;
  end;
}
end;

procedure TFmNFaEdit_0400_Quiet.ReopenNFeCabA;
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[00].AsInteger := FThisFatID;
  QrNFeCabA.Params[01].AsInteger := QrFatPedCabCodigo.Value;
  QrNFeCabA.Params[02].AsInteger := FEmpresa; //QrFatPedNFs_AEmpresa.Value;
  QrNFeCabA.Params[03].AsInteger := QrFatPedNFs_BNumeroNF.Value;
  UMyMod.AbreQuery(QrNFeCabA, Dmod.MyDB, 'TFmNFaEdit_0400_Quiet.ReopenNFeCabA()');
end;

procedure TFmNFaEdit_0400_Quiet.ReopenParamsEmp(Empresa: Integer);
begin
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
end;

procedure TFmNFaEdit_0400_Quiet.ReopenQrImprime;
begin
  QrImprime.Close;
  if QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger = F_Empresa then
  begin
    QrImprime.Params[00].AsInteger := F_Empresa;
    QrImprime.Params[01].AsInteger := F_ModeloNF;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
  end else begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    end;
  end;
end;

procedure TFmNFaEdit_0400_Quiet.ReopenStqMovValX(ID: Integer);
begin
  QrStqMovValX.Close;
  QrStqMovValX.SQL.Clear;
  QrStqMovValX.SQL.Add('SELECT smva.ID, smva.GraGruX, smva.CFOP, smva.Qtde,');
  QrStqMovValX.SQL.Add('smva.Preco, smva.Total, smva.CSOSN, smva.pCredSN,');
  QrStqMovValX.SQL.Add('gg1.Nome NO_PRD, gti.Nome NO_TAM, gcc.Nome NO_COR,');
  QrStqMovValX.SQL.Add('smva.iTotTrib, smva.vTotTrib, smva.pTotTrib, ');
  QrStqMovValX.SQL.Add('ELT(smva.iTotTrib + 1, "N", "S") TXT_iTotTrib ');
  QrStqMovValX.SQL.Add('FROM stqmovvala smva');
  QrStqMovValX.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smva.GraGruX');
  QrStqMovValX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrStqMovValX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrStqMovValX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrStqMovValX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrStqMovValX.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrStqMovValX.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrStqMovValX.SQL.Add('AND smva.Empresa=' + FormatFloat('0', F_Empresa));
  QrStqMovValX.SQL.Add('ORDER BY smva.IDCtrl');
  UnDmkDAC_PF.AbreQuery(QrStqMovValX, Dmod.MyDB);
  QrStqMovValX.Locate('ID', ID, []);
end;

procedure TFmNFaEdit_0400_Quiet.SbInfIntermedEntiClick(Sender: TObject);
begin
  DmPediVda.MostraFormEntidade2(EdInfIntermedEnti.ValueVariant, DmPediVda.QrInfIntermedEnti,
    EdInfIntermedEnti, CBInfIntermedEnti);
end;

procedure TFmNFaEdit_0400_Quiet.SpeedButton21Click(Sender: TObject);
begin
  dmkPF.TZD_UTC_InfoHelp();
end;

function TFmNFaEdit_0400_Quiet.Step1_GeraNFe(const Recria, ApenasGeraXML,
  CalculaAutomatico: Boolean; var ide_tpEmis: Integer): Boolean;
const
  sProcName = 'TFmNFaEdit_0400_Quiet.Step1_GeraNFe()';
var
  ide_serie: Variant;
  ide_dEmi, ide_dSaiEnt: TDateTime;
  FatNum, Empresa, Cliente, ide_nNF, ide_tpNF,
  modFrete, Transporta, NFeStatus, FretePor: Integer;
  //
  IDCtrl: Integer;
  //
  CodTXT, EmpTXT, infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq,
  SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_PGT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
  SQL_CUSTOMZ: String;
  //
  FreteVal, Seguro, Outros: Double;
  GravaCampos, RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, cNF_Atual,
  Financeiro: Integer;
  //
  UF_TXT_Emp, UF_TXT_Cli: String;
  //
  //2.00
  ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime;
  emit_CRT: Integer;
  ide_xJust, dest_email, Vagao, Balsa: String;
  // fim 2.00
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  idDest, indFinal, indPres, finNFe: Integer;
  ide_hEmi: TTime;
  ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  // Fim NFe 3.10
  // NFe 4.00 NT 2018/5
  RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti, L_Ativo: Integer;
  // Fim NFe 4.00 NT 2018/5
  // NFCe
  CNPJCPFAvulso, RazaoNomeAvulso: String;
  EmiteAvulso: Boolean;
  InfIntermedEnti: Integer;
begin
  ReopenFatPedNFs_B(QrFatPedNFs_BFilial.Value);
  //
  CNPJCPFAvulso   := '';
  RazaoNomeAvulso := '';
  EmiteAvulso     := False;
  //
  Result     := False;
  FatNum     := QrFatPedCabCodigo.Value;
  Empresa    := QrFatPedNFs_BEmpresa.Value;
  IDCtrl     := QrFatPedNFs_BIDCtrl.Value;
  FreteVal   := QrFatPedNFs_BFreteVal.Value;
  Seguro     := QrFatPedNFs_BSeguro.Value;
  Outros     := QrFatPedNFs_BOutros.Value;
  RegrFiscal := Trunc(QrFatPedCabRegrFiscal.Value);
  CartEmiss  := Trunc(QrFatPedCabCartEmis.Value);
  TabelaPrc  := Trunc(QrFatPedCabTabelaPrc.Value);
  CondicaoPg := TrunC(QrFatPedCabCondicaoPG.Value);
  Financeiro := QrFatPedCabFinanceiro.Value;
  // NFe 3.10
  idDest     := QrFatPedCabidDest.Value;
  indFinal   := QrFatPedCabindFinal.Value;
  indPres    := QrFatPedCabindPres.Value;
  finNFe     := QrFatPedCabfinNFe.Value;
  // NFe 4.00 NT 2018/5
  RetiradaUsa  := QrFatPedCabRetiradaUsa.Value;
  RetiradaEnti := QrFatPedCabRetiradaEnti.Value;
  EntregaUsa   := QrFatPedCabEntregaUsa.Value;
  EntregaEnti  := QrFatPedCabEntregaEnti.Value;
  L_Ativo      := QrFatPedCabL_Ativo.Value;
  // Fim NFe 4.00 NT 2018/5
  //
  if not DmodG.QrFiliLog.Locate('Codigo', Empresa, []) then
  begin
    Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') diferente da logada (' + VAR_LIB_EMPRESAS +
      ')! � necess�rio logoff!');
    Exit;
  end;
  //
  //FEMP_FILIAL, FCU_Pedido, FCliente, FEmpresa
  //if Geral.IMV(EdFilial.Text) = 0 then
  if FEMP_FILIAL = 0 then
  begin
    Geral.MB_Aviso('Empresa n�o definida!');
    Exit;
  end;
  //if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
  if MyObjects.FIC(FCliente = 0, nil, 'Cliente n�o definido!') then
    Exit;
  if not DModG.DefineFretePor(QrFatPedCabFretePor.Value, FretePor, modFrete) then
    Exit;
  //
  Cliente     := FCliente; //QrClientesCodigo.Value;
  Transporta  := Trunc(QrFatPedCabTransporta.Value);
  InfIntermedEnti := Trunc(QrFatPedCabInfIntermedEnti.Value);
  ide_Serie   := QrFatPedNFs_BSerieNFTxt.Value;
  ide_nNF     := QrFatPedNFs_BNumeroNF.Value;
  ide_dEmi    := QrFatPedNFs_BDtEmissNF.Value;
  ide_dSaiEnt := QrFatPedNFs_BDtEntraSai.Value;
  ide_tpNF    := QrFatPedCabtpNF.Value; // 1 = Sa�da
  // NFe 3.10
  ide_dhEmiTZD    := QrFatPedNFs_BdhEmiTZD.Value;
  ide_dhSaiEntTZD := QrFatPedNFs_BdhSaiEntTZD.Value;
  if QrFatPedNFs_B.FieldByName('HrEmi').AsString = EMptyStr then
    ide_hEmi      := 0
  else
    ide_hEmi        := QrFatPedNFs_BHrEmi.Value;
  //
  // Nao Resolveu o erro!
   //ide_hEmi        := QrFatPedNFs_B.FieldByName('HrEmi').AsFloat;

  //ide_tpImp   :=  1-Retrato/ 2-Paisagem
  // 2020-11-01
  // Fazer aqui conting�ncia off-line!
  ide_tpEmis  := 1;
(*
  //ide_tpEmis  := QrParamsEmpNFetpEmis.Value;
  ide_tpEmis := Geral.IMV(RGtpEmis.Items[RGtpEmis.ItemIndex][1]);

  case ide_tpEmis of
    1:
    begin
      ide_dhCont  := 0;
      ide_xJust   := '';
    end;
    9: // Conting�ncia off-line NFC-e
    begin
      ReopenCTG(Empresa);
      //
      if QrCTG.RecordCount = 0 then
      begin
        Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
        ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
        'Refa�a a configura��o de conting�ncia!');
        Exit;
      end else
      begin
        ide_dhCont  := QrCTGdhEntrada.Value;
        ide_xJust   := QrCTGNome.Value;
      end;
    end;
    else Geral.MB_Erro('ide_tpEmis n�o implementado em ' + sProcName);
  end;
*)
  //
(*
  if ide_tpEmis <> 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCTG, Dmod.MyDB, [
    'SELECT IF(Empresa=0, 1, 0) ORDEM,   ',
    'tpEmis, dhEntrada, Nome ',
    'FROM nfecntngnc ',
    'WHERE dhSaida < 2 ',
    'AND Empresa = 0 ',
    'OR Empresa=' + FormatFloat('0', Empresa),
    'ORDER BY ORDEM, dhEntrada DESC ',
    '']);
    if QrCTG.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
      'Refa�a a configura��o de conting�ncia!');
      Exit;
    end else
    begin
      ide_dhCont  := QrCTGdhEntrada.Value;
      ide_xJust   := QrCTGNome.Value;
    end;
  end else
  begin
    ide_dhCont  := 0;
    ide_xJust   := '';
  end;
   1 � Normal � emiss�o normal;
   2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
   3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
   4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
   5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
   6 = Conting�ncia SVC-AN (SEFAZ Virtual de Conting�ncia do AN);
   7 = Conting�ncia SVC-RS (SEFAZ Virtual de Conting�ncia do RS);
   9 = Conting�ncia off-line da NFC-e (as demais op��es de conting�ncia s�o v�lidas tamb�m para a NFC-e).
       Para a NFC-e somente est�o dispon�veis e s�o v�lidas as op��es de conting�ncia 5 e 9.
*)

  infAdic_infAdFisco := QrFatPedCabinfAdFisco.Value;
  infAdic_infCpl     := QrFatPedNFs_BinfAdic_infCpl.Value;
  VeicTransp_Placa   := QrFatPedNFs_BPlacaNr.Value;
  VeicTransp_UF      := QrFatPedNFs_BPlacaUF.Value;
  VeicTransp_RNTC    := QrFatPedNFs_BRNTC.Value;
  Exporta_UFEmbarq   := QrFatPedNFs_BUFEmbarq.Value;
  Exporta_XLocEmbarq := QrFatPedNFs_BxLocEmbarq.Value;
  //
  CodTXT      := dmkPF.FFP(QrFatPedCabCodigo.Value, 0);
  EmpTXT      := dmkPF.FFP(QrFatPedCabEmpresa.Value, 0);
  UF_TXT_emp  := QrFatPedCabUF_TXT_emp.Value;
  UF_TXT_cli  := QrFatPedCabUF_TXT_cli.Value;
  //SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(F_Tipo, FatNum, Empresa);
{
  'SELECT med.Sigla NO_UNIDADE, smva.Empresa, smva.Preco,          'sLineBreak +
  'smva.ICMS_Per, smva.IPI_Per, smva.GraGruX CU_PRODUTO,           'sLineBreak +
  'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,             'sLineBreak +
  'SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,             'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde, smva.CFOP,      'sLineBreak +
  'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         'sLineBreak +
  'gg1.CST_A, gg1.CST_B,                                           'sLineBreak +
  'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              'sLineBreak +
  'gg1.NCM, ncm.Letras,  gg1.IPI_CST, gg1.IPI_cEnq,                'sLineBreak +
  'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              'sLineBreak +
  'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,              'sLineBreak +
  'gg1.InfAdProd, smva.MedidaC, smva.MedidaL,                      'sLineBreak +
  'smva.MedidaA,smva.MedidaE,                                      'sLineBreak +
  'mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4,             'sLineBreak +
  'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4,                 'sLineBreak +
  'gta.PrintTam, gcc.PrintCor                                      'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             'sLineBreak +
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             'sLineBreak +
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             'sLineBreak +
  'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo                'sLineBreak +
  'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           'sLineBreak +
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                 'sLineBreak +
  'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem              'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT +                                    sLineBreak +
  'GROUP BY smva.Preco, smva.ICMS_Per, smva.IPI_Per, smva.GraGruX  ';
  //
  {
  SQL_CUSTOMZ :=
  'SELECT gg1.SiglaCustm                                           'sLineBreak +
  'FROM pedivdacuz pvc                                             'sLineBreak +
  'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX             'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'WHERE pvc.Controle=:P0                                          'sLineBreak +
  'AND gg1.SiglaCustm <> "";
  }
  //SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(F_Tipo, FatNum, Empresa);
  {
  'SELECT SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,      'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 'sLineBreak +
  'COUNT(DISTINCT OriCnta) + 0.000 Volumes                         'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT;
  }
  //
  //SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(F_Tipo, FatNum, Empresa);
  {
  'SELECT FatID, FatNum, Vencimento, Credito Valor,                'sLineBreak +
  'FatParcela, Duplicata                                           'sLineBreak +
  'FROM lan ctos                                                    'sLineBreak +
  'WHERE FatID = 1                                                 'sLineBreak +
  'AND FatNum=' + CodTXT +                                          sLineBreak +
  'AND CliInt=' + EmpTXT;
  }
  //SQL_FAT_TOT :=  DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_FAT_TOT :=  DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(F_Tipo, FatNum, Empresa);
  //SQL_VOLUMES :=  DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_VOLUMES :=  DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(F_Tipo, FatNum, Empresa);
  {
  'SELECT Quantidade qVol, Especie esp, Marca marca,               'sLineBreak +
  'Numero nVol, kgLiqui pesoL, kgBruto pesoB                       'sLineBreak +
  'FROM stqmovnfsa                                                 'sLineBreak +
  'WHERE Tipo = 1                                                  'sLineBreak +
  'AND OriCodi=' + CodTXT +                                         sLineBreak +
  'AND Empresa=' + EmpTXT;
  }
(*
  NFeStatus := QrNFeCabAStatus.Value;
  if DBCheck.CriaFm(TFmNFeSteps_ 0 1 1 0 , FmNFeSteps_ 0 1 1 0 , afmoNegarComAviso) then
  begin
    FmNFeSteps_ 0 1 1 0 .FSource_IDCtrl := 'stqmovnfsa';
    FmNFeSteps_ 0 1 1 0 .Show;
    //
*)
    {
    GravaCampos := Geral.MB_Pergunta('Deseja salvar demonstrativo ' +
    ' no banco de dados para verifica��o de dados em relat�rio? ' + sLineBreak +
    'Este demostrativo n�o � obrigat�rio e sua grava��o pode ser demorada!' +
    sLineBreak + 'Deseja salvar assim mesmo?');
    }
    cNF_Atual := QrNFeCabAide_cNF.Value;
    GravaCampos := ID_NO;
(*
    if GravaCampos <> ID_CANCEL then
      FmNFeSteps_xxxx.Cria NFe Normal(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
        IDCtrl, Cliente, FretePor, modFrete, Transporta,
        RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
        ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
        SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro)
    else
      Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
    //
    FmNFeSteps_XXXX.Destroy;
    //
*)
  // 2.00
  ide_hSaiEnt := QrFatPedNFs_BHrEntraSai.Value;
  emit_CRT    := QrFatPedNFs_Bemit_CRT.Value;
  dest_email  := QrFatPedNFs_Bdest_EMail.Value;
  Vagao       := QrFatPedNFs_BVagao.Value;
  Balsa       := QrFatPedNFs_BBalsa.Value;
  // fim 2.00
  Compra_XNEmp := QrFatPedNFs_BCompra_XNEmp.Value;
  Compra_XPed  := QrFatPedNFs_BCompra_XPed.Value;
  Compra_XCont := QrFatPedNFs_BCompra_XCont.Value;
  //
  NFeStatus := 0;  // T� certo???
  if UnNFe_PF.CriaNFe_vXX_XX(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
       IDCtrl, Cliente, FretePor, modFrete, Transporta, 0,
       RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, FreteVal, Seguro, Outros,
       ide_Serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
       infAdic_infAdFisco, infAdic_infCpl,
       VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
       Exporta_UFEmbarq, Exporta_xLocEmbarq,
       SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
       SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro,
       ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
       Compra_XNEmp, Compra_XPed, Compra_XCont,
       // NFe 3.10
       idDest, ide_hEmi, ide_dhEmiTZD, ide_dhSaiEntTZD, indFinal, indPres, finNFe,
       // NFe 4.00 NT 2018/5
       RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti, L_Ativo,
       // Fim NFe 4.00 NT 2018/5
       //
       ApenasGeraXML, CalculaAutomatico,
       CNPJCPFAvulso, RazaoNomeAvulso, EmiteAvulso,
       CkEditCabGA.Checked, InfIntermedEnti)
  then
    Result := ReopenFatPedNFs_B(QrFatPedNFs_BFilial.Value)
  else
    Result := False;
(*
  end;
*)
  (*
  DESATIVADO EM: 05/04/2017
  MOTIVO: Estava excluindo as altera��es nas faturas e ao recriar estava zarando as altera��es

  if ApenasGeraXML then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DmNFe_0000.AtualizaLctoFaturaNFe(FThisFatID, FatNum, Empresa);
    finally
      Screen.Cursor := crDefault
    end;
  end;
  *)
end;

function TFmNFaEdit_0400_Quiet.Step2_Pagamentos(): Boolean;
var
  ide_tpEmis: Integer;
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    if not FmNFeCabA_0000.LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value) then
      Geral.MB_Erro('ERRO!, N�o foi poss�vel localizar o ID ' +
        FormatFloat('0', QrNFeCabAIDCtrl.Value) + ' localize manualmente!');
    FmNFeCabA_0000.FMostraSubForm := TSubForm.tsfDadosPagamento;
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
    //
    Screen.Cursor := crHourGlass;
    try
      Step1_GeraNFe(True, True, True, ide_tpEmis);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFaEdit_0400_Quiet.TbStqMovValX_AfterInsert(DataSet: TDataSet);
begin
  TbStqMovValX_.Cancel;
end;

procedure TFmNFaEdit_0400_Quiet.TPDtEmissNFChange(Sender: TObject);
begin
  DefineTZD_UTC;
end;

function TFmNFaEdit_0400_Quiet.VerificaSeHaPagamentos: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM nfecabya ',
  'WHERE FatID=' + Geral.FF0(FThisFatID),
  'AND FatNum=' + Geral.FF0(FFatPedCab),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  Result := (QrPsq.RecordCount > 0) and (QrPsq.FieldByName('Controle').AsInteger <> 0);
end;

(*
Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
function TFmNFaEdit_0400.Encerra(): Boolean;
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
  Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela,
  NotaFiscal, Account: Integer; SerieNF: String; VerificaCliInt: Boolean);
  var
    TabLctA: String;
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    if F_Financeiro = 1 then
      FLAN_Credito    := Valor
    else
    if F_Financeiro = 2 then
      FLAN_Debito    := Valor;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_MoraDia    := QrSumTJurosMes.Value;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    F L A N _ G e n e r o     := Genero;
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
    FLAN_Cliente    := F_Cliente;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
    FLAN_FatID      := FThisFatID;
    FLAN_FatNum     := F_OriCodi;
    FLAN_FatParcela := Parcela;
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := SerieNF;
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
{$IfDef DEFINE_VARLCT}
    TabLctA         := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, F_EMP_Filial);
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
    //
    if UFinanceiro.InsereLancamento(TabLctA) then
    begin
      // nada
    end;
{$Else}
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
      'Controle', VAR_LCT, VAR_LCT, 'Controle');
    //
    if UFinanceiro.InsereLancamento() then
    begin
      // nada
    end;
{$ENdIf}
  end;
var
  DataFat, DataEnc: TDateTime;
  FaturaNum, Agora, Duplicata, NovaDup, NF_Emp_Serie, NF_Ass_Serie: String;
  Parcela, NF_Emp_Numer, NF_Ass_Numer, Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  TemAssociada: Boolean;
  FreteVal, Seguro, Outros: Double;
  TabLctA: String;
begin
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      // Exclui lan�amentos para evitar duplica��o
      if (FThisFatID <> 0) and (F_OriCodi <> 0) then
      begin
{$IfDef DEFINE_VARLCT}
      Filial  := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      //
      UFinanceiro.ExcluiLct_FatNum(nil, FThisFatID, F_OriCodi,
        F_Empresa, 0, dmkPF.MotivDel_ValidaCodigo(311), True, TabLctA);
{$Else}
      UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FThisFatID, F_OriCodi,
        F_Empresa, 0, True);
{$EndIf}
      end;
      //
      QrNF_X.Close;
      QrNF_X.Params[00].AsInteger := F_Tipo;
      QrNF_X.Params[01].AsInteger := F_OriCodi;
      QrNF_X.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
      //
      NF_Emp_Serie := QrNF_XSerieNFTxt.Value;
      NF_Emp_Numer := QrNF_XNumeroNF.Value;
      //
      if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', F_EMP_FILIAL) + ' !' + sLineBreak +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"');
        Exit;
      end;
      //
      TemAssociada := (F_AFP_Sit = 1) and (F_AFP_Per > 0);
      //
      if TemAssociada then
      begin
        QrNF_X.Close;
        QrNF_X.Params[00].AsInteger := F_Tipo;
        QrNF_X.Params[01].AsInteger := F_OriCodi;
        QrNF_X.Params[02].AsInteger := F_Associada;
        UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
        NF_Ass_Serie := QrNF_XSerieNFTxt.Value;
        NF_Ass_Numer := QrNF_XNumeroNF.Value;
        if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'O n�mero de nota fiscal n�o foi definida para a empresa ' +
            FormatFloat('000', F_ASS_FILIAL) + ' !' + sLineBreak +
            'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
            'Nota Fiscal"');
          Exit;
        end;
        //
        P2 := F_AFP_Per;
        P1 := 100 - P2;
      end else
      begin
        P1 := 100;
        P2 := 0;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
      end;
      //
      if F_EMP_CtaFaturas = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_EMP_FILIAL) + '!');
        Exit;
      end;
      if (F_Associada <> 0) and
      (F_ASS_CtaFaturas = 0) and TemAssociada then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_ASS_FILIAL) + '!');
        Exit;
      end;
      if F_Financeiro > 0 then
      begin
        if F_CartEmis = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'A carteira n�o foi definida no pedido selecionado! (4)');
          Exit;
        end;
        QrPrzT.Close;
        QrPrzT.Params[0].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzT, Dmod.MyDB);
        if QrPrzT.RecordCount = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'N�o h� parcela(s) definida(s) para a empresa ' +
            IntToStr(F_EMP_FILIAL) + ' na condi��o de pagamento ' +
            'cadastrada no pedido selecionado!');
          Exit;
        end;
        if (F_AFP_Sit = 1) and
        (F_AFP_Per > 0) then
        begin
          QrSumT.Close;
          QrSumT.Params[0].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
          if (QrSumTPercent2.Value <> F_AFP_Per) then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
          if QrSumT.RecordCount = 0 then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
        end;
      end else
        Geral.MB_Aviso('N�o ser� gerado lan�amentos financeiros.' + sLineBreak +
        'Na Regra Fiscal est� definido para n�o gerar lan�amentos!');
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      // Faturamento empresa principal
      {
      FreteVal := EdFreteVal.ValueVariant;
      Seguro   := EdSeguro.ValueVariant;
      Outros   := EdOutros.ValueVariant;
      }
      //
      QrSumX.Close;
      {  Erro !!!!????
      QrSumX.Params[00].AsInteger := F_Tipo;
      QrSumX.Params[01].AsInteger := F_OriCodi;
      QrSumX.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
      }
      QrSumX.SQL.Clear;
      QrSumX.SQL.Add('SELECT SUM(Total) Total');
      QrSumX.SQL.Add('FROM stqmovvala');
      QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
      QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
      QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Empresa));
      UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
      //
      T1 := QrSumXTotal.Value + FreteVal + Seguro + Outros;
      F1 := T1;
      //
      if F_Financeiro > 0 then
      begin
        QrPrzX.Close;
        QrPrzX.SQL.Clear;
        QrPrzX.SQL.Add('SELECT Controle, Dias, Percent1 Percent ');
        QrPrzX.SQL.Add('FROM pediprzits');
        QrPrzX.SQL.Add('WHERE Percent1 > 0');
        QrPrzX.SQL.Add('AND Codigo=:P0');
        QrPrzX.SQL.Add('ORDER BY Dias');
        QrPrzX.Params[00].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
        QrPrzX.First;
        while not QrPrzX.Eof do
        begin
          if QrPrzX.RecordCount = QrPrzX.RecNo then
            V1 := F1
          else begin
            if P1 = 0 then V1 := 0 else
              V1 := (Round(T1 * (QrPrzXPercent.Value / P1 * 100))) / 100;

            F1 := F1 - V1;
          end;

          QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
          Parcela := QrPrzT.RecNo;
          if F_EMP_FaturaNum = 0 then
            FaturaNum := FormatFloat('000000', F_EMP_IDDuplicata)
          else
            FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
          Duplicata := F_EMP_TpDuplicata + FaturaNum + F_EMP_FaturaSep +
            dmkPF.ParcelaFatura(QrPrzX.RecNo, F_EMP_FaturaSeq);
          // 2011-08-21
          // Teste para substituir no futuro (uso no form FmFatDivCms)
          NovaDup := DmProd.MontaDuplicata(F_EMP_IDDuplicata, EdNumeroNF.ValueVariant,
          Parcela, F_EMP_FaturaNum, F_EMP_FaturaSeq,
          F_EMP_TpDuplicata, F_EMP_FaturaSep);
          if NovaDup <> Duplicata then
            Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak +
            Duplicata + ' <> ' + NovaDup);
          // Fim 2011-08-21
          IncluiLancto(V1, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
            F_EMP_TxtFaturas, F_TIPOCART,
            F_CartEmis, F_EMP_CtaFaturas,
            F_Empresa, Parcela, NF_Emp_Numer,
            F_Represen, NF_Emp_Serie, True);
          //
          QrPrzX.Next;
        end;


        // Faturamento associada
        if TemAssociada then
        begin
          QrSumX.Close;
          {
          QrSumX.Params[00].AsInteger := F_Tipo;
          QrSumX.Params[01].AsInteger := F_OriCodi;
          QrSumX.Params[02].AsInteger := F_Associada;
          UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
          }
          QrSumX.SQL.Clear;
          QrSumX.SQL.Add('SELECT SUM(Total) Total');
          QrSumX.SQL.Add('FROM stqmovvala');
          QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
          QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
          QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Associada));
          UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
          T2 := QrSumXTotal.Value;
          F2 := T2;
          //
          QrPrzX.Close;
          QrPrzX.SQL.Clear;
          QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
          QrPrzX.SQL.Add('FROM pediprzits');
          QrPrzX.SQL.Add('WHERE Percent2 > 0');
          QrPrzX.SQL.Add('AND Codigo=:P0');
          QrPrzX.SQL.Add('ORDER BY Dias');
          QrPrzX.Params[00].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
          QrPrzX.First;
          while not QrPrzX.Eof do
          begin
            if QrPrzX.RecordCount = QrPrzX.RecNo then
              V2 := F2
            else begin
              if P2 = 0 then V2 := 0 else
                V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
              F2 := F2 - V2;
            end;
            QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
            Parcela := QrPrzT.RecNo;
            if F_EMP_FaturaNum = 0 then
              FaturaNum := FormatFloat('000000', F_ASS_IDDuplicata)
            else
              FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
            Duplicata := F_ASS_TpDuplicata + FormatFloat('000000', F_ASS_IDDuplicata) +
              F_ASS_FaturaSep + dmkPF.ParcelaFatura(
              QrPrzX.RecNo, F_ASS_FaturaSeq);
            IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
              F_ASS_TxtFaturas, F_TIPOCART,
              F_CartEmis, F_ASS_CtaFaturas,
              F_Associada, Parcela, NF_Ass_Numer,
              F_Represen, NF_Ass_Serie, False);
            //
            QrPrzX.Next;
          end;
        end;
      end;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //

      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
*)

procedure TFmNFaEdit_0400_Quiet.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmNFaEdit_0400_Quiet.Edide_finNFeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFaEdit_0400_Quiet.EdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdindPres.Text, F_indPres, Texto, 0, 1);
  EdindPres_TXT.Text := Texto;
end;

procedure TFmNFaEdit_0400_Quiet.EdindPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindPres.Text := Geral.SelecionaItem(F_indPres, 0,
    'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
    TitCols, Screen.Width)
  end else
  MyObjects.IncrementaNumeroEditTxt(TdmkEdit(Sender), Key, Shift,
  TAllFormat.dmktfInteger);
end;

procedure TFmNFaEdit_0400_Quiet.EdtBandChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtBand.Text, F_tBand, Texto, 0, 1);
  EdtBand_TXT.Text := Texto;
end;

procedure TFmNFaEdit_0400_Quiet.EdtBandKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtBand.Text := Geral.SelecionaItem(F_tBand, 0,
    'SEL-LISTA-000 :: Bandeira da operadora de cart�o',
    TitCols, Screen.Width)
  end else
  MyObjects.IncrementaNumeroEditTxt(TdmkEdit(Sender), Key, Shift,
  TAllFormat.dmktfInteger);
end;

procedure TFmNFaEdit_0400_Quiet.EdtPagChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtPag.Text, F_tPag, Texto, 0, 1);
  EdtPag_TXT.Text := Texto;
end;

procedure TFmNFaEdit_0400_Quiet.EdtPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtPag.Text := Geral.SelecionaItem(F_tPag, 0,
    'SEL-LISTA-000 :: Meio de Pagamento',
    TitCols, Screen.Width)
  end else
  MyObjects.IncrementaNumeroEditTxt(TdmkEdit(Sender), Key, Shift,
  TAllFormat.dmktfInteger);
end;

procedure TFmNFaEdit_0400_Quiet.EdtpIntegraChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtpIntegra.Text, F_tpIntegra, Texto, 0, 1);
  EdtpIntegra_TXT.Text := Texto;
end;

procedure TFmNFaEdit_0400_Quiet.EdtpIntegraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtpIntegra.Text := Geral.SelecionaItem(F_tpIntegra, 0,
    'SEL-LISTA-000 :: Tipo de Integra��o para pagamento',
    TitCols, Screen.Width)
  end else
  MyObjects.IncrementaNumeroEditTxt(TdmkEdit(Sender), Key, Shift,
  TAllFormat.dmktfInteger);
end;

procedure TFmNFaEdit_0400_Quiet.EdValFreteChange(Sender: TObject);
begin
  if (EdValFrete.ValueVariant > 0) then
  begin
    if (EdFretePor.ValueVariant = 9) then
    begin
      EdFretePor.ValueVariant := 0;
      CBFretePor.KeyValue     := 0;
      //
    end;
    EdIndPres.ValueVariant := 4;
    //
    if EdTransporta.ValueVariant = 0 then
    begin
      EdTransporta.ValueVariant := EdEmpresa.ValueVariant;
      CBTransporta.KeyValue     := EdEmpresa.ValueVariant;
    end;
  end;
end;

function TFmNFaEdit_0400_Quiet.Encerra2(): Boolean;
const
  NaoMostraMsg = True;
var
  DataFat, DataEnc: TDateTime;
  Agora: String;
  Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  FreteVal, Seguro, Outros: Double;
  GenCtbD, GenCtbC: Integer;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  //if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      Filial := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      //
      if not UnNFe_PF.InsUpdFaturasNFe(Filial, F_Empresa, FThisFatID, F_Tipo,
        F_OriCodi, F_EMP_CtaFaturas, F_Associada, F_ASS_CtaFaturas, F_ASS_FILIAL,
        F_Financeiro, GenCtbD, GenCtbC, F_CartEmis, F_CondicaoPG, F_AFP_Sit, F_EMP_FaturaNum,
        F_EMP_IDDuplicata, EdNumeroNF.ValueVariant, F_EMP_FaturaSeq, F_TIPOCART,
        F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq, F_Cliente,
        F_AFP_Per,
        // ini 2021-10-23
        //FreteVal, Seguro, 0, Outros,
        // fim 2021-10-23
        F_EMP_TpDuplicata,
        F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
        F_ASS_TpDuplicata, F_ASS_TxtFaturas, DataFat, QrPrzT, QrSumT, QrPrzX,
        QrSumX, QrNF_X, NaoMostraMsg) then Exit;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else
          Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

{ 2011-11-18

object EdFreteVal: TdmkEdit
  Left = 272
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 4
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label1: TLabel
  Left = 272
  Top = 0
  Width = 36
  Height = 13
  Caption = '$ Frete:'
  Visible = False
end
object Label2: TLabel
  Left = 344
  Top = 0
  Width = 46
  Height = 13
  Caption = '$ Seguro:'
  Visible = False
end
object EdSeguro: TdmkEdit
  Left = 344
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 5
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object EdOutros: TdmkEdit
  Left = 416
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 6
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label10: TLabel
  Left = 416
  Top = 0
  Width = 57
  Height = 13
  Caption = '$ D. acess.:'
  Visible = False
end

}

//N�o mostrar NFE-emiss-011?
//N�o mostrar NFE-emiss-001?

end.

