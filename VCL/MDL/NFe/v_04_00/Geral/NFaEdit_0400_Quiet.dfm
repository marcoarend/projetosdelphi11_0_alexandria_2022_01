object FmNFaEdit_0400_Quiet: TFmNFaEdit_0400_Quiet
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-001 :: Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
  ClientHeight = 693
  ClientWidth = 1075
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 51
    Width = 1075
    Height = 642
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Dados Gerais '
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1067
        Height = 545
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cabe'#231'alho e rodap'#233' da NF (1)'
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 517
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object PnNfeCabYA: TPanel
              Left = 0
              Top = 48
              Width = 1059
              Height = 94
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 1059
                Height = 47
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label3: TLabel
                  Left = 431
                  Top = 4
                  Width = 67
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Valor do Pgto:'
                end
                object Label14: TLabel
                  Left = 533
                  Top = 4
                  Width = 69
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Valor do troco:'
                end
                object Label17: TLabel
                  Left = 11
                  Top = 4
                  Width = 118
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Meio de pagamento: [F4]'
                  FocusControl = EdtPag
                end
                object EdvPag: TdmkEdit
                  Left = 431
                  Top = 21
                  Width = 98
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'vPag'
                  UpdCampo = 'vPag'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdvTroco: TdmkEdit
                  Left = 533
                  Top = 21
                  Width = 98
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'vTroco'
                  UpdCampo = 'vTroco'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdtPag: TdmkEdit
                  Left = 11
                  Top = 21
                  Width = 27
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '4'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'tPag'
                  UpdCampo = 'tPag'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdtPagChange
                  OnKeyDown = EdtPagKeyDown
                end
                object EdtPag_TXT: TdmkEdit
                  Left = 38
                  Top = 21
                  Width = 390
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object CkEditaYA: TCheckBox
                  Left = 649
                  Top = 21
                  Width = 116
                  Height = 18
                  Caption = 'Inserir mais de um.'
                  TabOrder = 4
                end
              end
              object PnCard: TPanel
                Left = 0
                Top = 47
                Width = 1059
                Height = 45
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label28: TLabel
                  Left = 462
                  Top = 4
                  Width = 165
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'CNPJ da Credenciadora de cart'#227'o:'
                end
                object Label29: TLabel
                  Left = 11
                  Top = 4
                  Width = 194
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Tipo de Integra'#231#227'o para pagamento: [F4]'
                end
                object Label30: TLabel
                  Left = 655
                  Top = 4
                  Width = 180
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Bandeira da operadora de cart'#227'o: [F4]'
                end
                object Label31: TLabel
                  Left = 868
                  Top = 4
                  Width = 163
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#186' de autoriz. da opera'#231#227'o cart'#227'o:'
                end
                object EdCNPJ: TdmkEdit
                  Left = 462
                  Top = 21
                  Width = 188
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtCPFJ_NFe
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'CNPJ'
                  UpdCampo = 'CNPJ'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdtpIntegra: TdmkEdit
                  Left = 11
                  Top = 21
                  Width = 42
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'tpIntegra'
                  UpdCampo = 'tpIntegra'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdtpIntegraChange
                  OnKeyDown = EdtpIntegraKeyDown
                end
                object EdtpIntegra_TXT: TdmkEdit
                  Left = 53
                  Top = 21
                  Width = 405
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdtBand: TdmkEdit
                  Left = 655
                  Top = 21
                  Width = 43
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'tBand'
                  UpdCampo = 'tBand'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdtBandChange
                  OnKeyDown = EdtBandKeyDown
                end
                object EdtBand_TXT: TdmkEdit
                  Left = 698
                  Top = 21
                  Width = 165
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdcAut: TdmkEdit
                  Left = 868
                  Top = 21
                  Width = 183
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'cAut'
                  UpdCampo = 'cAut'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object Panel3: TPanel
              Left = 0
              Top = 142
              Width = 1059
              Height = 43
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 1
              object Label4: TLabel
                Left = 11
                Top = 0
                Width = 58
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Quantidade:'
              end
              object Label5: TLabel
                Left = 99
                Top = 0
                Width = 41
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Esp'#233'cie:'
              end
              object Label6: TLabel
                Left = 335
                Top = 0
                Width = 33
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Marca:'
              end
              object Label7: TLabel
                Left = 572
                Top = 0
                Width = 40
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N'#250'mero:'
              end
              object Label8: TLabel
                Left = 809
                Top = 0
                Width = 54
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Peso bruto:'
              end
              object Label9: TLabel
                Left = 889
                Top = 0
                Width = 62
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Peso l'#237'quido:'
              end
              object EdQuantidade: TdmkEdit
                Left = 11
                Top = 17
                Width = 85
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdEspecie: TdmkEdit
                Left = 99
                Top = 17
                Width = 233
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                MaxLength = 60
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdMarca: TdmkEdit
                Left = 335
                Top = 17
                Width = 232
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                MaxLength = 60
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNumero: TdmkEdit
                Left = 572
                Top = 17
                Width = 232
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                MaxLength = 60
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdkgBruto: TdmkEdit
                Left = 809
                Top = 17
                Width = 76
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdkgLiqui: TdmkEdit
                Left = 889
                Top = 17
                Width = 76
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object Panel11: TPanel
              Left = 0
              Top = 185
              Width = 1059
              Height = 46
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              TabOrder = 2
              object Label34: TLabel
                Left = 13
                Top = 3
                Width = 36
                Height = 13
                Caption = '$ Frete:'
              end
              object Label35: TLabel
                Left = 119
                Top = 4
                Width = 45
                Height = 13
                Caption = 'Frete por:'
              end
              object Label36: TLabel
                Left = 380
                Top = 3
                Width = 75
                Height = 13
                Caption = 'Transportadora:'
              end
              object SbTransporta: TSpeedButton
                Left = 1024
                Top = 20
                Width = 22
                Height = 23
                Caption = '...'
              end
              object EdValFrete: TdmkEdit
                Left = 13
                Top = 20
                Width = 103
                Height = 23
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValFreteChange
              end
              object EdFretePor: TdmkEditCB
                Left = 119
                Top = 20
                Width = 23
                Height = 23
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'FretePor'
                UpdCampo = 'FretePor'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBFretePor
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFretePor: TdmkDBLookupComboBox
                Left = 145
                Top = 20
                Width = 227
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                TabOrder = 2
                dmkEditCB = EdFretePor
                QryCampo = 'FretePor'
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdTransporta: TdmkEditCB
                Left = 380
                Top = 20
                Width = 59
                Height = 23
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Transporta'
                UpdCampo = 'Transporta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBTransporta
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBTransporta: TdmkDBLookupComboBox
                Left = 442
                Top = 20
                Width = 581
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                TabOrder = 4
                dmkEditCB = EdTransporta
                QryCampo = 'Transporta'
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 231
              Width = 1059
              Height = 141
              Align = alTop
              Caption = ' Fiscal:'
              TabOrder = 3
              object SbRegrFiscal: TSpeedButton
                Left = 1028
                Top = 73
                Width = 23
                Height = 22
                Caption = '...'
                Visible = False
              end
              object Label58: TLabel
                Left = 14
                Top = 99
                Width = 465
                Height = 13
                Caption = 
                  'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
                  'no momento da opera'#231#227'o: [F4]'
              end
              object Label208: TLabel
                Left = 518
                Top = 98
                Width = 134
                Height = 13
                Caption = 'Finalidade emis'#227'o NF-e: [F4]'
                FocusControl = Edide_finNFe
              end
              object Label32: TLabel
                Left = 218
                Top = 77
                Width = 162
                Height = 13
                Caption = 'Regra fiscal para emiss'#227'o de NFe:'
              end
              object LaCNPJCPFAvulso: TLabel
                Left = 681
                Top = 98
                Width = 95
                Height = 13
                Caption = 'CPF / CNPJ avulso:'
                Enabled = False
                FocusControl = EdCNPJCPFAvulso
              end
              object LaRazaoNomeAvulso: TLabel
                Left = 804
                Top = 98
                Width = 139
                Height = 13
                Caption = 'Raz'#227'o Social / Nome avulso:'
                Enabled = False
                FocusControl = EdRazaoNomeAvulso
              end
              object RG_idDest: TdmkRadioGroup
                Left = 725
                Top = 16
                Width = 330
                Height = 53
                Caption = ' Local de Destino da Opera'#231#227'o:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  '0 - N'#227'o definido'
                  '1 - Opera'#231#227'o interna'
                  '2 - Opera'#231#227'o interestadual'
                  '3 - Opera'#231#227'o com exterior')
                TabOrder = 3
                TabStop = True
                QryCampo = 'idDest'
                UpdCampo = 'idDest'
                UpdType = utYes
                OldValor = 0
              end
              object RG_indFinal: TdmkRadioGroup
                Left = 2
                Top = 16
                Width = 127
                Height = 53
                Caption = ' Oper. c/Consumidor: '
                ItemIndex = 1
                Items.Strings = (
                  '0-Normal'
                  '1-Consumidor final')
                TabOrder = 0
                QryCampo = 'indFinal'
                UpdCampo = 'indFinal'
                UpdType = utYes
                OldValor = 0
              end
              object EdindPres: TdmkEdit
                Left = 15
                Top = 115
                Width = 34
                Height = 23
                CharCase = ecUpperCase
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'indPres'
                UpdCampo = 'indPres'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdindPresChange
                OnKeyDown = EdindPresKeyDown
              end
              object EdindPres_TXT: TEdit
                Left = 50
                Top = 115
                Width = 465
                Height = 21
                TabStop = False
                ReadOnly = True
                TabOrder = 7
              end
              object Edide_finNFe_TXT: TdmkEdit
                Left = 541
                Top = 115
                Width = 136
                Height = 23
                TabStop = False
                ReadOnly = True
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_finNFe: TdmkEdit
                Left = 518
                Top = 115
                Width = 23
                Height = 23
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '4'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'finNFe'
                UpdCampo = 'finNFe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_finNFeChange
                OnKeyDown = Edide_finNFeKeyDown
              end
              object EdRegrFiscal: TdmkEditCB
                Left = 397
                Top = 73
                Width = 60
                Height = 22
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBRegrFiscal
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBRegrFiscal: TdmkDBLookupComboBox
                Left = 461
                Top = 73
                Width = 564
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsFisRegCad
                TabOrder = 5
                dmkEditCB = EdRegrFiscal
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object RGtpEmis: TdmkRadioGroup
                Left = 130
                Top = 16
                Width = 596
                Height = 53
                Caption = ' Tipo de Emiss'#227'o da NF-e : '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  '1=Emiss'#227'o normal (n'#227'o em conting'#234'ncia)'
                  '6=Conting'#234'ncia SVC-AN (SEFAZ Virtual de Cont. AN)'
                  '7=Conting'#234'ncia SVC-RS (SEFAZ Virtual de Cont. RS)')
                TabOrder = 1
                QryCampo = 'indFinal'
                UpdCampo = 'indFinal'
                UpdType = utYes
                OldValor = 0
              end
              object EdCNPJCPFAvulso: TdmkEdit
                Left = 681
                Top = 115
                Width = 120
                Height = 23
                Enabled = False
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtCPFJ
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRazaoNomeAvulso: TdmkEdit
                Left = 804
                Top = 115
                Width = 247
                Height = 23
                Enabled = False
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtCPFJ
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CkEditCabGA: TdmkCheckBox
                Left = 13
                Top = 77
                Width = 197
                Height = 18
                Caption = 'Autorizar emails para obter o XML.'
                TabOrder = 2
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
            object Panel12: TPanel
              Left = 0
              Top = 399
              Width = 1059
              Height = 97
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 4
              object StaticText1: TStaticText
                Left = 0
                Top = 0
                Width = 1059
                Height = 17
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Observa'#231#245'es adicionais NF-e [F4]'
                TabOrder = 0
              end
              object MeinfAdic_infCpl: TdmkMemo
                Left = 0
                Top = 17
                Width = 1059
                Height = 80
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 1
                OnKeyDown = MeinfAdic_infCplKeyDown
                UpdType = utYes
              end
            end
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 1059
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 5
              object LaSerieNF: TLabel
                Left = 11
                Top = 4
                Width = 44
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'S'#233'rie NF:'
              end
              object Label15: TLabel
                Left = 62
                Top = 4
                Width = 61
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = #218'lt. NF emit.:'
                FocusControl = DBEdit2
              end
              object Label16: TLabel
                Left = 142
                Top = 4
                Width = 45
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N'#186' Limite:'
                FocusControl = DBEdit3
              end
              object LaNumeroNF: TLabel
                Left = 223
                Top = 4
                Width = 71
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N'#186' Nota Fiscal:'
              end
              object Label18: TLabel
                Left = 303
                Top = 4
                Width = 115
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data e hora Emiss'#227'o NF'
              end
              object Label10: TLabel
                Left = 478
                Top = 4
                Width = 50
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'TZD UTC:'
              end
              object SpeedButton21: TSpeedButton
                Left = 534
                Top = 21
                Width = 23
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '?'
                OnClick = SpeedButton21Click
              end
              object Label26: TLabel
                Left = 562
                Top = 4
                Width = 67
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Hor'#225'rio ver'#227'o:'
              end
              object Label19: TLabel
                Left = 640
                Top = 4
                Width = 138
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data e hora Sa'#237'da / entrada:'
              end
              object Label25: TLabel
                Left = 818
                Top = 4
                Width = 50
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'TZD UTC:'
              end
              object Label27: TLabel
                Left = 876
                Top = 4
                Width = 67
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Hor'#225'rio ver'#227'o:'
              end
              object Label33: TLabel
                Left = 958
                Top = 4
                Width = 44
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Empresa:'
              end
              object EdSerieNF: TdmkEdit
                Left = 11
                Top = 21
                Width = 48
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object DBEdit2: TDBEdit
                Left = 62
                Top = 21
                Width = 77
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'Sequencial'
                DataSource = DsImprime
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 142
                Top = 21
                Width = 77
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'MaxSeqLib'
                DataSource = DsImprime
                TabOrder = 2
              end
              object EdNumeroNF: TdmkEdit
                Left = 223
                Top = 21
                Width = 77
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object TPDtEmissNF: TdmkEditDateTimePicker
                Left = 303
                Top = 21
                Width = 111
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44644.000000000000000000
                Time = 0.587303645843348900
                TabOrder = 4
                TabStop = False
                OnClick = TPDtEmissNFChange
                OnChange = TPDtEmissNFChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object EdHrEmi: TdmkEdit
                Left = 416
                Top = 21
                Width = 60
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                TabOrder = 5
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'HrEmi'
                UpdCampo = 'HrEmi'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EddhEmiTZD: TdmkEdit
                Left = 478
                Top = 21
                Width = 55
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfTZD_UTC
                Texto = '+00:00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EddhEmiVerao: TdmkEdit
                Left = 562
                Top = 21
                Width = 75
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object TPDtEntraSai: TdmkEditDateTimePicker
                Left = 640
                Top = 21
                Width = 111
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44644.000000000000000000
                Time = 0.587380277778720500
                TabOrder = 8
                TabStop = False
                OnClick = TPDtEmissNFChange
                OnChange = TPDtEmissNFChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object EdHrEntraSai: TdmkEdit
                Left = 753
                Top = 21
                Width = 60
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                TabOrder = 9
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'HrEntraSai'
                UpdCampo = 'HrEntraSai'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EddhSaiEntTZD: TdmkEdit
                Left = 818
                Top = 21
                Width = 56
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                ReadOnly = True
                TabOrder = 10
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfTZD_UTC
                Texto = '+00:00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EddhSaiEntVerao: TdmkEdit
                Left = 876
                Top = 21
                Width = 77
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdEmpresa: TdmkEdit
                Left = 958
                Top = 21
                Width = 48
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object Panel14: TPanel
              Left = 0
              Top = 372
              Width = 1059
              Height = 27
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 6
              object Panel15: TPanel
                Left = 0
                Top = 0
                Width = 108
                Height = 27
                Align = alLeft
                TabOrder = 0
                object Ckide_indIntermed: TdmkCheckBox
                  Left = 13
                  Top = 4
                  Width = 95
                  Height = 18
                  Caption = 'Intermediado:'
                  TabOrder = 0
                  OnClick = Ckide_indIntermedClick
                  UpdType = utYes
                  ValCheck = #0
                  ValUncheck = #0
                  OldValor = #0
                end
              end
              object PnIntermediador: TPanel
                Left = 108
                Top = 0
                Width = 951
                Height = 27
                Align = alClient
                TabOrder = 1
                Visible = False
                object Label37: TLabel
                  Left = 4
                  Top = 5
                  Width = 67
                  Height = 13
                  Caption = 'Intermediador:'
                end
                object SbInfIntermedEnti: TSpeedButton
                  Left = 640
                  Top = 2
                  Width = 22
                  Height = 23
                  Caption = '...'
                  OnClick = SbInfIntermedEntiClick
                end
                object EdInfIntermedEnti: TdmkEditCB
                  Left = 81
                  Top = 2
                  Width = 60
                  Height = 23
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'InfIntermedEnti'
                  UpdCampo = 'InfIntermedEnti'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBInfIntermedEnti
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBInfIntermedEnti: TdmkDBLookupComboBox
                  Left = 141
                  Top = 2
                  Width = 496
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NOMEENT'
                  TabOrder = 1
                  dmkEditCB = EdInfIntermedEnti
                  QryCampo = 'InfIntermedEnti'
                  UpdType = utNil
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Cabe'#231'alho e rodap'#233' da NF (2)'
          ImageIndex = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 1059
            Height = 517
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 1059
              Height = 48
              Align = alTop
              TabOrder = 0
              ExplicitWidth = 1058
              object Label22: TLabel
                Left = 702
                Top = 4
                Width = 90
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Vag'#227'o (transporte):'
              end
              object Label23: TLabel
                Left = 878
                Top = 4
                Width = 85
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Balsa (transporte):'
              end
              object Label24: TLabel
                Left = 6
                Top = 4
                Width = 103
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail do destinat'#225'rio:'
              end
              object EdVagao: TdmkEdit
                Left = 702
                Top = 21
                Width = 173
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdBalsa: TdmkEdit
                Left = 878
                Top = 21
                Width = 173
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_email: TdmkEdit
                Left = 6
                Top = 21
                Width = 351
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object Panel5: TPanel
              Left = 0
              Top = 48
              Width = 1059
              Height = 65
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitWidth = 1058
              object GroupBox10: TGroupBox
                Left = 0
                Top = 0
                Width = 679
                Height = 65
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 
                  ' Informa'#231#245'es de compras (Obs.: A nota de  empenho se refere a co' +
                  'mpras p'#250'blicas): '
                TabOrder = 0
                object Label183: TLabel
                  Left = 16
                  Top = 17
                  Width = 88
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Nota de empenho:'
                  FocusControl = EdCompra_XNEmp
                end
                object Label184: TLabel
                  Left = 112
                  Top = 17
                  Width = 36
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Pedido:'
                  FocusControl = EdCompra_XPed
                end
                object Label185: TLabel
                  Left = 396
                  Top = 17
                  Width = 96
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Contrato de compra:'
                  FocusControl = EdCompra_XCont
                end
                object EdCompra_XNEmp: TdmkEdit
                  Left = 16
                  Top = 34
                  Width = 92
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XNEmp'
                  UpdCampo = 'Compra_XNEmp'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XPed: TdmkEdit
                  Left = 112
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XPed'
                  UpdCampo = 'Compra_XPed'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XCont: TdmkEdit
                  Left = 396
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XCont'
                  UpdCampo = 'Compra_XCont'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox3: TGroupBox
                Left = 679
                Top = 0
                Width = 379
                Height = 65
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Exporta'#231#227'o: (NF-e: ZA - Informa'#231#245'es de com'#233'rcio exterior) '
                TabOrder = 1
                object Label20: TLabel
                  Left = 16
                  Top = 17
                  Width = 17
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'UF:'
                end
                object Label21: TLabel
                  Left = 58
                  Top = 17
                  Width = 224
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Local onde ocorrer'#225' o embarque dos produtos: '
                end
                object EdUFEmbarq: TdmkEdit
                  Left = 16
                  Top = 34
                  Width = 36
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 2
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdxLocEmbarq: TdmkEdit
                  Left = 58
                  Top = 34
                  Width = 279
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 113
              Width = 1059
              Height = 67
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Ve'#237'culo: '
              TabOrder = 2
              ExplicitWidth = 1058
              object Label11: TLabel
                Left = 16
                Top = 21
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Placa:'
              end
              object Label12: TLabel
                Left = 100
                Top = 21
                Width = 17
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'UF:'
              end
              object Label13: TLabel
                Left = 272
                Top = 21
                Width = 358
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Observa'#231#245'es: (Nome motorista, tel. doc., etc.) N'#227'o '#233' impresso na' +
                  ' NF/NF-e!!'
              end
              object RNTC: TLabel
                Left = 134
                Top = 21
                Width = 71
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'RNTC (ANTT):'
              end
              object EdPlacaNr: TdmkEdit
                Left = 16
                Top = 37
                Width = 79
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPlacaUF: TdmkEdit
                Left = 100
                Top = 37
                Width = 28
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdObservacao: TdmkEdit
                Left = 272
                Top = 37
                Width = 375
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRNTC: TdmkEdit
                Left = 134
                Top = 37
                Width = 134
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                MaxLength = 20
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object RGCRT: TdmkRadioGroup
              Left = 0
              Top = 180
              Width = 1059
              Height = 81
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' CRT (C'#243'digo de Regime Tribut'#225'rio): '
              Columns = 2
              Enabled = False
              ItemIndex = 0
              Items.Strings = (
                '0 - Nenhum'
                '1 - Simples Nacional'
                '2 - Simples Nacional - excesso de sublimite de receita bruta'
                '3 - Regime Normal')
              TabOrder = 3
              UpdType = utYes
              OldValor = 0
            end
          end
        end
      end
      object GBRodaPe: TGroupBox
        Left = 0
        Top = 545
        Width = 1067
        Height = 69
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1063
          Height = 52
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1a: TLabel
            Left = 401
            Top = 7
            Width = 465
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
              'DID-005 '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2a: TLabel
            Left = 402
            Top = 6
            Width = 465
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
              'DID-005 '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso1b: TLabel
            Left = 401
            Top = 29
            Width = 466
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
              'or item).'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2b: TLabel
            Left = 400
            Top = 28
            Width = 466
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
              'or item).'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -14
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PnSaiDesis: TPanel
            Left = 875
            Top = 0
            Width = 188
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida: TBitBtn
              Tag = 13
              Left = 2
              Top = 4
              Width = 128
              Height = 43
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Fechar'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtOk: TBitBtn
            Tag = 14
            Left = 27
            Top = 4
            Width = 128
            Height = 43
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&OK'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtOkClick
          end
          object CkNaoEnviar: TCheckBox
            Left = 183
            Top = 17
            Width = 189
            Height = 18
            Caption = 'N'#227'o enviar. Vou editar antes.'
            TabOrder = 2
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Itens da NF'
      ImageIndex = 1
      object GradeItens: TDBGrid
        Left = 0
        Top = 33
        Width = 1067
        Height = 516
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsStqMovValX
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GradeItensDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'CSOSN'
            PickList.Strings = (
              '101'
              '102'
              '103'
              '201'
              '202'
              '203'
              '300'
              '400'
              '500'
              '900')
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pCredSN'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD'
            Title.Caption = 'Nome do Produto'
            Width = 287
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tam.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_iTotTrib'
            Title.Caption = 'I'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vTotTrib'
            Title.Caption = '$ Tot.Trib.'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pTotTrib'
            Title.Caption = '% Trib'
            Width = 43
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 549
        Width = 1067
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object BtTodos: TBitBtn
          Tag = 127
          Left = 27
          Top = 5
          Width = 128
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtTodosClick
        end
        object BtSelecionados: TBitBtn
          Tag = 241
          Left = 159
          Top = 5
          Width = 128
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Selecionados'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtSelecionadosClick
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1067
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Label1: TLabel
          Left = 12
          Top = 2
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label2: TLabel
          Left = 11
          Top = 1
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1075
    Height = 51
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1020
      Top = 0
      Width = 55
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 9
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 55
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 55
      Top = 0
      Width = 509
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 468
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 564
      Top = 0
      Width = 456
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 452
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 17
          Top = 2
          Width = 120
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 16
          Top = 1
          Width = 120
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object QrPesqPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Preco '
      'FROM tabeprcgrg'
      'WHERE Nivel1=:P0')
    Left = 564
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqPrcPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 836
    Top = 56
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT smva.ID) Itens, smva.CFOP, '
      'frc.OrdCFOPGer, cfp.Nome, Descricao, Complementacao'
      'FROM stqmovvala smva'
      'LEFT JOIN fisregcfop frc ON frc.Codigo=1 AND frc.CFOP=smva.CFOP'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=smva.CFOP'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'GROUP BY smva.CFOP'
      'ORDER BY frc.OrdCFOPGer DESC')
    Left = 836
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCFOPItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 6
    end
    object QrCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrFatPedNFs: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP, '
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*'
      'FROM stqmovvala smva'
      'LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa'
      'LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa '
      '          AND smva.Tipo=:P0 '
      '          AND smna.OriCodi=:P1'
      'WHERE smva.Tipo=:P2'
      'AND smva.OriCodi=:P3'
      'AND ent.Codigo<>:P4'
      'ORDER BY ent.Filial')
    Left = 636
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrImprimeAfterOpen
    BeforeClose = QrImprimeBeforeClose
    SQL.Strings = (
      'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprimeempr ime'
      'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=ime.ParamsNFs'
      'WHERE ime.Empresa=:P0'
      'AND ime.Codigo=:P1')
    Left = 700
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
      Origin = 'imprime.SerieNF'
      Required = True
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
      Origin = 'paramsnfs.Sequencial'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'paramsnfs.IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Origin = 'paramsnfs.Controle'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Origin = 'paramsnfs.MaxSeqLib'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
      Origin = 'imprime.TipoImpressao'
      Required = True
    end
    object QrImprimeSerieNF_Normal: TIntegerField
      FieldName = 'SerieNF_Normal'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Associada, emp.AssocModNF, emp.CRT, emp.CSOSN,'
      
        'emp.pCredSNAlq, emp.pCredSNMez, emp.NFeInfCpl, cpl.Texto NFeInfC' +
        'pl_TXT'
      'FROM paramsemp emp'
      'LEFT JOIN nfeinfcpl cpl ON cpl.Codigo = emp.NFeInfCpl'
      'WHERE emp.Codigo=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
    end
    object QrParamsEmppCredSN_Cfg: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'pCredSN_Cfg'
      Calculated = True
    end
    object QrParamsEmpNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
    end
    object QrParamsEmpNFeInfCpl_TXT: TWideMemoField
      FieldName = 'NFeInfCpl_TXT'
      BlobType = ftWideMemo
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 700
    Top = 56
  end
  object QrNF_X: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2')
    Left = 240
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 288
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 340
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 388
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1 '
      'AND Empresa=:P2')
    Left = 444
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrSumXprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrSumXprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrSumXprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object DsStqMovValX: TDataSource
    DataSet = QrStqMovValX
    Left = 952
    Top = 56
  end
  object TbStqMovValX_: TMySQLTable
    Database = Dmod.MyDB
    AfterInsert = TbStqMovValX_AfterInsert
    TableName = 'stqmovvalx'
    Left = 564
    Top = 56
  end
  object QrEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CRT'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 892
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCRT: TSmallintField
      FieldName = 'CRT'
    end
  end
  object QrStqMovValX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 952
    Top = 8
    object QrStqMovValXID: TIntegerField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QrStqMovValXGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object QrStqMovValXCFOP: TWideStringField
      FieldName = 'CFOP'
      ReadOnly = True
      Size = 6
    end
    object QrStqMovValXQtde: TFloatField
      FieldName = 'Qtde'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.000;-#,###,###,###,##0.000; '
    end
    object QrStqMovValXPreco: TFloatField
      FieldName = 'Preco'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXTotal: TFloatField
      FieldName = 'Total'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrStqMovValXpCredSN: TFloatField
      FieldName = 'pCredSN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      ReadOnly = True
      Size = 50
    end
    object QrStqMovValXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      ReadOnly = True
      Size = 5
    end
    object QrStqMovValXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      ReadOnly = True
      Size = 30
    end
    object QrStqMovValXiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrStqMovValXvTotTrib: TFloatField
      FieldName = 'vTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovValXTXT_iTotTrib: TWideStringField
      FieldName = 'TXT_iTotTrib'
      Size = 1
    end
    object QrStqMovValXpTotTrib: TFloatField
      FieldName = 'pTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrCSOSN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM stqmovvala smva'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'AND smva.CSOSN=0')
    Left = 636
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCSOSNItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrStqMovNFsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqmovnfsa'
      'WHERE Tipo=1'
      'AND OriCodi>0'
      'AND Empresa=-11')
    Left = 324
    Top = 358
    object QrStqMovNFsAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrStqMovNFsATipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrStqMovNFsAOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Required = True
    end
    object QrStqMovNFsAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrStqMovNFsASerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Required = True
    end
    object QrStqMovNFsASerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Required = True
      Size = 5
    end
    object QrStqMovNFsANumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Required = True
    end
    object QrStqMovNFsAIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Required = True
    end
    object QrStqMovNFsAFreteVal: TFloatField
      FieldName = 'FreteVal'
      Required = True
    end
    object QrStqMovNFsASeguro: TFloatField
      FieldName = 'Seguro'
      Required = True
    end
    object QrStqMovNFsAOutros: TFloatField
      FieldName = 'Outros'
      Required = True
    end
    object QrStqMovNFsAPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Size = 2
    end
    object QrStqMovNFsAPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Size = 8
    end
    object QrStqMovNFsARNTC: TWideStringField
      FieldName = 'RNTC'
    end
    object QrStqMovNFsAQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Size = 30
    end
    object QrStqMovNFsAEspecie: TWideStringField
      FieldName = 'Especie'
      Size = 60
    end
    object QrStqMovNFsAMarca: TWideStringField
      FieldName = 'Marca'
      Size = 60
    end
    object QrStqMovNFsANumero: TWideStringField
      FieldName = 'Numero'
      Size = 60
    end
    object QrStqMovNFsAkgBruto: TFloatField
      FieldName = 'kgBruto'
      Required = True
    end
    object QrStqMovNFsAkgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Required = True
    end
    object QrStqMovNFsAObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrStqMovNFsACFOP1: TWideStringField
      FieldName = 'CFOP1'
      Size = 6
    end
    object QrStqMovNFsADtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Required = True
    end
    object QrStqMovNFsADtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Required = True
    end
    object QrStqMovNFsAHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Required = True
    end
    object QrStqMovNFsAinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrStqMovNFsAUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Size = 2
    end
    object QrStqMovNFsAxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Size = 60
    end
    object QrStqMovNFsAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrStqMovNFsAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrStqMovNFsAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Required = True
    end
    object QrStqMovNFsAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrStqMovNFsAvagao: TWideStringField
      FieldName = 'vagao'
    end
    object QrStqMovNFsAbalsa: TWideStringField
      FieldName = 'balsa'
    end
    object QrStqMovNFsACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrStqMovNFsACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrStqMovNFsACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrStqMovNFsAdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
      Required = True
    end
    object QrStqMovNFsAdhSaiEntTZD: TFloatField
      FieldName = 'dhSaiEntTZD'
      Required = True
    end
    object QrStqMovNFsAHrEmi: TTimeField
      FieldName = 'HrEmi'
      Required = True
    end
  end
  object QrFatPedNFs_B: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFs_BAfterScroll
    SQL.Strings = (
      
        'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, nfe.' +
        'infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.*'
      ''
      'ide_dhEmiTZD, ide_dhSaiEntTZD'
      ''
      ''
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 288
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFs_BFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFs_BIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFs_BTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFs_BOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFs_BEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFs_BNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFs_BIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFs_BAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFs_BAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFs_BSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFs_BSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFs_BCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFs_BDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFs_BPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFs_BEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFs_BMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFs_BNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFs_BkgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFs_BkgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFs_BQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFs_BObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFs_BCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFs_BDtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Origin = 'stqmovnfsa.DtEmissNF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Origin = 'stqmovnfsa.DtEntraSai'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrFatPedNFs_BinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrFatPedNFs_BinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      Origin = 'stqmovnfsa.infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFatPedNFs_BinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrFatPedNFs_BDTEMISSNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTEMISSNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BDTENTRASAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTENTRASAI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BRNTC: TWideStringField
      FieldName = 'RNTC'
      Origin = 'stqmovnfsa.RNTC'
    end
    object QrFatPedNFs_BUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Origin = 'stqmovnfsa.UFembarq'
      Size = 2
    end
    object QrFatPedNFs_BxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Origin = 'stqmovnfsa.xLocEmbarq'
      Size = 60
    end
    object QrFatPedNFs_Bide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrFatPedNFs_BHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Origin = 'stqmovnfsa.HrEntraSai'
    end
    object QrFatPedNFs_Bide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
      Origin = 'stqmovnfsa.ide_dhCont'
    end
    object QrFatPedNFs_Bide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Origin = 'stqmovnfsa.ide_xJust'
      Size = 255
    end
    object QrFatPedNFs_Bemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Origin = 'stqmovnfsa.emit_CRT'
    end
    object QrFatPedNFs_Bdest_email: TWideStringField
      FieldName = 'dest_email'
      Origin = 'stqmovnfsa.dest_email'
      Size = 60
    end
    object QrFatPedNFs_Bvagao: TWideStringField
      FieldName = 'vagao'
      Origin = 'stqmovnfsa.vagao'
    end
    object QrFatPedNFs_Bbalsa: TWideStringField
      FieldName = 'balsa'
      Origin = 'stqmovnfsa.balsa'
    end
    object QrFatPedNFs_BCabA_FatID: TIntegerField
      FieldName = 'CabA_FatID'
      Origin = 'nfecaba.FatID'
      Required = True
    end
    object QrFatPedNFs_BCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'stqmovnfsa.Compra_XNEmp'
      Size = 17
    end
    object QrFatPedNFs_BCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'stqmovnfsa.Compra_XPed'
      Size = 60
    end
    object QrFatPedNFs_BCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'stqmovnfsa.Compra_XCont'
      Size = 60
    end
    object QrFatPedNFs_BdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
    end
    object QrFatPedNFs_BdhSaiEntTZD: TFloatField
      FieldName = 'dhSaiEntTZD'
    end
    object QrFatPedNFs_BHrEmi: TTimeField
      FieldName = 'HrEmi'
    end
    object QrFatPedNFs_BLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou,'
      'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP,'
      'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI,'
      'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI,'
      'frc.ModeloNF, frc.infAdFisco, frc.Financeiro,'
      'frc.TipoMov tpNF, nfe.ICMSTot_vProd, pvd.*'
      'FROM fatpedcab fpc'
      
        'LEFT JOIN nfecaba nfe ON nfe.FatNum = fpc.Codigo AND  nfe.FatID ' +
        '= 1'
      'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg'
      'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF)'
      'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF)'
      'WHERE fpc.Encerrou > 0'
      'ORDER BY Encerrou DESC')
    Left = 288
    Top = 144
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
    end
    object QrFatPedCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrFatPedCabCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
    end
    object QrFatPedCabUF_TXT_emp: TWideStringField
      FieldName = 'UF_TXT_emp'
      Required = True
      Size = 2
    end
    object QrFatPedCabUF_TXT_cli: TWideStringField
      FieldName = 'UF_TXT_cli'
      Required = True
      Size = 2
    end
    object QrFatPedCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrFatPedCabinfAdFisco: TWideStringField
      FieldName = 'infAdFisco'
      Size = 255
    end
    object QrFatPedCabFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFatPedCabTotalFatur: TFloatField
      FieldName = 'TotalFatur'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFatPedCabtpNF: TSmallintField
      FieldName = 'tpNF'
    end
    object QrFatPedCabDTAEMISS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAEMISS_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAENTRA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAENTRA_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAINCLU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAINCLU_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAPREVI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAPREVI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
    end
    object QrFatPedCabDtaEntra: TDateField
      FieldName = 'DtaEntra'
    end
    object QrFatPedCabDtaInclu: TDateField
      FieldName = 'DtaInclu'
    end
    object QrFatPedCabDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
    end
    object QrFatPedCabFretePor: TIntegerField
      FieldName = 'FretePor'
    end
    object QrFatPedCabModeloNF: TFloatField
      FieldName = 'ModeloNF'
    end
    object QrFatPedCabRegrFiscal: TFloatField
      FieldName = 'RegrFiscal'
    end
    object QrFatPedCabCartEmis: TFloatField
      FieldName = 'CartEmis'
    end
    object QrFatPedCabTabelaPrc: TFloatField
      FieldName = 'TabelaPrc'
    end
    object QrFatPedCabCondicaoPG: TFloatField
      FieldName = 'CondicaoPG'
    end
    object QrFatPedCabTransporta: TFloatField
      FieldName = 'Transporta'
    end
    object QrFatPedCabEmpresa: TFloatField
      FieldName = 'Empresa'
    end
    object QrFatPedCabCliente: TFloatField
      FieldName = 'Cliente'
    end
    object QrFatPedCabidDest: TSmallintField
      FieldName = 'idDest'
    end
    object QrFatPedCabindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrFatPedCabindPres: TSmallintField
      FieldName = 'indPres'
    end
    object QrFatPedCabindSinc: TSmallintField
      FieldName = 'indSinc'
    end
    object QrFatPedCabTpCalcTrib: TSmallintField
      FieldName = 'TpCalcTrib'
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrFatPedCabfinNFe: TSmallintField
      FieldName = 'finNFe'
    end
    object QrFatPedCabId: TIntegerField
      FieldName = 'Id'
    end
    object QrFatPedCabRetiradaUsa: TSmallintField
      FieldName = 'RetiradaUsa'
    end
    object QrFatPedCabRetiradaEnti: TIntegerField
      FieldName = 'RetiradaEnti'
    end
    object QrFatPedCabEntregaUsa: TSmallintField
      FieldName = 'EntregaUsa'
    end
    object QrFatPedCabEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
    end
    object QrFatPedCabL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object QrFatPedCabInfIntermedEnti: TFloatField
      FieldName = 'InfIntermedEnti'
    end
  end
  object QrFatPedNFs_A: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFs_AAfterScroll
    SQL.Strings = (
      
        'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, nfe.' +
        'infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.*'
      ''
      'ide_dhEmiTZD, ide_dhSaiEntTZD'
      ''
      ''
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 232
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 344
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 297
    Top = 217
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 492
    Top = 8
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 492
    Top = 53
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 736
    Top = 228
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClienteIE: TWideStringField
      FieldName = 'IE'
    end
    object QrClienteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrClienteDOCENT: TWideStringField
      FieldName = 'DOCENT'
      Size = 18
    end
    object QrClienteindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrClienteTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    Left = 540
    Top = 313
    object QrPediVdaidDest: TSmallintField
      FieldName = 'idDest'
    end
    object QrPediVdaindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrPediVdaindPres: TSmallintField
      FieldName = 'indPres'
    end
    object QrPediVdaindSinc: TSmallintField
      FieldName = 'indSinc'
    end
    object QrPediVdafinNFe: TSmallintField
      FieldName = 'finNFe'
    end
  end
end
