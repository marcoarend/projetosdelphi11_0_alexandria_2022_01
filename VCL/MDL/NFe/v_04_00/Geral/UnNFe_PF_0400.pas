unit UnNFe_PF_0400;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
(*
  ExtCtrls, StdCtrls,
  Spin, Buttons, ComCtrls, OleCtrls, SHDocVw, ACBrMail,
  //ACBrPosPrinter, ACBrNFeDANFeESCPOS,
  ACBrNFeDANFEClass,
  //ACBrDANFCeFortesFr,
  ACBrDFeReport, ACBrDFeDANFeReport,
  //ACBrNFeDANFeRLClass,
  ACBrBase, ACBrDFe,
  ACBrNFe, ACBrUtil, ShellAPI, XMLIntf, XMLDoc, zlib, ACBrIntegrador,
  //ACBrDANFCeFortesFrA4;
  pcnConversaoNFe, pcnConversao,
  DmkGeral, UnDmkEnums, MyDBCheck,
  UnXXe_PF,
  dmkEdit;
*)
  UnDmkEnums;

type
  TUnNFe_PF_0400 = class(TObject)
  private
    { Private declarations }
{
    FverXML_vrsao: String;
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
    function  TextoArqDefinido(Texto: String): Boolean;
}
  public
    { Public declarations }
    function LerTextoEnvioLoteEvento(Empresa, Lote: Integer; TextoArq: String):
             Boolean;
  end;

var
  NFe_PF_0400: TUnNFe_PF_0400;

implementation

{ TUnNFe_PF_0400 }

uses ModuleGeral, NFe_PF_0400, MyDBCheck;

function TUnNFe_PF_0400.LerTextoEnvioLoteEvento(Empresa, Lote: Integer;
  FTextoArq: String): Boolean;
begin
  if DBCheck.CriaFm(TFmNFe_PF_0400, FmNFe_PF_0400, afmoLiberado) then
  begin
    FmNFe_PF_0400.LerTextoEnvioLoteEvento(Empresa, Lote, TextoArq);
    FmNFe_PF_0400.Destroy;
  end;
end;

end.
