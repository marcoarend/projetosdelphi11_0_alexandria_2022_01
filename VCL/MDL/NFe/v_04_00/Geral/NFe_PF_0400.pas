unit NFe_PF_0400;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,

  ExtCtrls, StdCtrls,
  Spin, Buttons, ComCtrls, OleCtrls, SHDocVw, ACBrMail,
  //ACBrPosPrinter, ACBrNFeDANFeESCPOS,
  ACBrNFeDANFEClass,
  //ACBrDANFCeFortesFr,
  ACBrDFeReport, ACBrDFeDANFeReport,
  //ACBrNFeDANFeRLClass,
  ACBrBase, ACBrDFe,
  ACBrNFe, ACBrUtil, ShellAPI, XMLIntf, XMLDoc, zlib, ACBrIntegrador,
  //ACBrDANFCeFortesFrA4;
  pcnConversaoNFe, pcnConversao,
  DmkGeral, UnDmkEnums, MyDBCheck,
  UnXXe_PF;

type
  TFmNFe_PF_0400 = class(TForm)
  private
    { Private declarations }
    FverXML_versao: String;
    //
    //FMsg: String;
    //FSiglas_WS: MyArrayLista;
    //FPathLoteNFe, FPathLoteEvento, FPathLoteDowNFeDes: String;
    xmlDoc: IXMLDocument;
    //xmlNode, xmlSub, xmlChild1: IXMLNode;
    //xmlList: IXMLNodeList;
    //FAmbiente_Int, FCodigoUF_Int: Integer;
    //FAmbiente_Txt, FCodigoUF_Txt: String;
    //FWSDL, FURL, FAvisoNSU: String;
    //FultNSU, FNSU: Int64;
    //
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
    function  TextoArqDefinido(Texto: String): Boolean;
    function  DefineXMLDoc(): Boolean;
  public
    { Public declarations }
    FTextoArq: String;
    //
    //function  LerTextoEnvioLoteEvento(Empresa, Lote: Integer; TextoArq: String): Boolean;
    procedure  ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
  end;

var
  FmNFe_PF_0400: TFmNFe_PF_0400;

implementation

uses ModuleGeral, ModuleNFe_0000;

{$R *.dfm}

{ TFmNFe_PF_0400 }

function TFmNFe_PF_0400.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  //Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Empresa n�o definida!');
  end;
end;

function TFmNFe_PF_0400.DefineLote(var Lote: Integer): Boolean;
begin
  //Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Lote n�o definido!');
  end;
end;

function TFmNFe_PF_0400.DefineXMLDoc: Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

procedure TFmNFe_PF_0400.ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteNFe) then
    begin
      Geral.MB_Erro('O lote "' + FPathLoteNFe + '" n�o foi localizado!');
      Exit;
    end;
    if dmkPF.CarregaArquivo(FPathLoteNFe, rtfDadosMsg) then
    begin
      if rtfDadosMsg = '' then
      begin
        Geral.MB_Erro('O lote de NFe "' + FPathLoteNFe +
        '" foi carregado mas est� vazio!');
        Exit;
      end;
      retWS :='';
      FTextoArq :='';
      Screen.Cursor := crHourGlass;
      try
        FTextoArq := FmNFeGeraXML_0400.WS_NFeRecepcaoLote(EdUF_Servico.Text,
        FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
        EdWebService, Sincronia, EdIde_mod.ValueVariant);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmNFe_0000.ReopenEmpresa(Empresa);
        LoteStr := FormatFloat('000000000', Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        Timer1.Enabled := Sincronia = TXXeIndSinc.nisAssincrono;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  if LerTextoEnvioLoteNFe() then ;
end;

{
function TFmNFe_PF_0400.LerTextoEnvioLoteEvento(Empresa, Codigo: Integer;
  FTextoArq: String): Boolean;
var
  Codigo,
  Controle, tpAmb, cOrgao, cStat: Integer;
  versao: Double;
  verAplic, xMotivo: String;

  Status, ret_tpAmb, ret_cOrgao, ret_cStat, ret_tpEvento,
  ret_nSeqEvento: Integer;
  ret_versao, ret_TZD_UTC: Double;
  ret_Id, ret_verAplic, ret_xMotivo, ret_chNFe, ret_xEvento,
  ret_CNPJDest, ret_CPFDest, ret_emailDest, ret_nProt: String;

  SubCtrl: Integer;

  eveMDe_tpAmb, eveMDe_cOrgao, eveMDe_cStat,
  eveMDe_tpEvento, eveMDe_nSeqEvento, cSitConf: Integer;
  eveMDe_Id, eveMDe_verAplic, eveMDe_xMotivo,
  eveMDe_chNFe, eveMDe_xEvento, eveMDe_CNPJDest,
  eveMDe_CPFDest, eveMDe_emailDest, eveMDe_nProt, Id, eveMDe_dhRegEvento: String;
  eveMDe_TZD_UTC: Double;

  //Empresa,
  cJust: Integer;
  //tMed, nRec
  idLote: String;
  infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chNFe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao, infEvento_xCorrecao: String;

  tpEvento, nSeqEvento: Integer;
  //cSitNFe: Integer;
  xJust: String;
  SQLType: TSQLType;

  CPF: String;
begin
  Codigo := Lote;
  FTextoArq := TextoArq;
  //
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerLotEve.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := Geral.DMV_Dot(LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False));
      //
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb));
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao));
      cStat    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cStat));
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
          begin
            //I := -1;
            while xmlList.Length > 0 do
            begin
              //I := I + 1;
              //testar
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao, False);
//{TODO :       Verificar vers�o e conte�do do XML_retEve!
              XML_retEve            := xmlList.Item[0].XML;
              //
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEvento/infEvento');
              infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              //
              infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
              infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              infEvento_chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
              infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
              infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
              infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
              infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
              infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
              infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
              infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
              infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
              //
              //infEvento_xCorrecao   := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
              //
              if infEvento_dhRegEvento = '' then
                infEvento_dhRegEvento := '0000-00-00 00:00:00'
              else
                XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status     := Geral.IMV(infEvento_cStat);
              tpEvento   := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmNFe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chNFe);
              //
              if Controle <> 0 then
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
                  'XML_retEve',
                  'Status', 'ret_versao', 'ret_Id',
                  'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                  'ret_cStat', 'ret_xMotivo', 'ret_chNFe',
                  'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                  'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                  'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                  (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                  Geral.WideStringToSQLString(XML_retEve), Status,
                  (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverret', '', 0);
                  DmNFe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeeverret', False, [
                    'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                    'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                    'ret_xMotivo', 'ret_chNFe', 'ret_tpEvento',
                    'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                    'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                    'ret_TZD_UTC', 'ret_nProt'], [
                    'SubCtrl'], [
                    Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                    (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                    (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                    (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                    (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                    (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                    (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                    (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                    (*ret_nProt*)infEvento_nProt], [
                    SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      NFe_CodEventoCCe: // Carta de corre��o.
                      //A princ�pio n�o faz nada!
                      // Implementado nos aplicativos Dermatek s� na NFe 3.10
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCCeNfeCabA(infEvento_chNFe,
                            verAplic, infEvento_chNFe, infEvento_dhRegEvento,
                            infEvento_nProt, nSeqEvento, cOrgao, tpAmb, tpEvento,
                            cStat, ret_TZD_UTC)
                          then
                            Exit;

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
                        end;
                      end;
                      NFe_CodEventoCan: // Cancelamento
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCanNfeCabA(
                            infEvento_chNFe, infEvento_tpAmb, infEvento_verAplic,
                            infEvento_dhRegEvento, infEvento_nProt, infEvento_versao,
                            ret_TZD_UTC)
                          then
                            Exit;
                        end;
                      end;
                      // Manifesta��o do destinat�rio
                      NFe_CodEventoMDeConfirmacao, // = 210200;
                      NFe_CodEventoMDeCiencia    , // = 210210;
                      NFe_CodEventoMDeDesconhece , // = 210220;
                      NFe_CodEventoMDeNaoRealizou: // = 210240;
                      begin
                        if infEvento_cStat = '135' then
                        begin
                          //  Realmente precisa localizar?
                          if not DmNFe_0000.LocalizaNFeInfoMDeEve(
                            infEvento_chNFe, Id, xJust, cJust, tpEvento)

                          then
                            Geral.MB_Erro('Falha ao localizar dados da manifesta��o');
                          //
                          Id                 := infEvento_chNFe;
                          //
                          eveMDe_Id          := infEvento_Id;
                          eveMDe_tpAmb       := Geral.IMV(infEvento_tpAmb);
                          eveMDe_verAplic    := infEvento_verAplic;
                          eveMDe_cOrgao      := Geral.IMV(infEvento_cOrgao);
                          eveMDe_cStat       := Geral.IMV(infEvento_cStat);
                          eveMDe_xMotivo     := infEvento_xMotivo;
                          eveMDe_chNFe       := infEvento_chNFe;
                          eveMDe_tpEvento    := Geral.IMV(infEvento_tpEvento);
                          eveMDe_xEvento     := infEvento_xEvento;
                          eveMDe_nSeqEvento  := Geral.IMV(infEvento_nSeqEvento);
                          eveMDe_CNPJDest    := infEvento_CNPJDest;
                          eveMDe_CPFDest     := infEvento_CPFDest;
                          eveMDe_emailDest   := infEvento_emailDest;
                          eveMDe_dhRegEvento := infEvento_dhRegEvento;
                          eveMDe_TZD_UTC     := ret_TZD_UTC;
                          eveMDe_nProt       := infEvento_nProt;
                          //
                          //cSitNFe  :=  N�o tem aqui!
                          cSitConf := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(eveMDe_tpEvento);
                          //
                          if eveMDe_dhRegEvento = '' then
                            eveMDe_dhRegEvento := '0000-00-00 00:00:00';
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                            'eveMDe_Id', 'eveMDe_tpAmb', 'eveMDe_verAplic',
                            'eveMDe_cOrgao', 'eveMDe_cStat', 'eveMDe_xMotivo',
                            'eveMDe_chNFe', 'eveMDe_tpEvento', 'eveMDe_xEvento',
                            'eveMDe_nSeqEvento', 'eveMDe_CNPJDest', 'eveMDe_CPFDest',
                            'eveMDe_emailDest', 'eveMDe_dhRegEvento', 'eveMDe_TZD_UTC',
                            'eveMDe_nProt', 'cSitConf'
                          ], ['Id'], [
                            eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
                            eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
                            eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
                            eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
                            eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
                            eveMDe_nProt, cSitConf
                          ], [Id], True);
                        end;
                      end;
                      else
                      begin
                        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' +
                        sLineBreak + 'TFmNFeSteps_0400.LerTextoEnvioLoteEvento()');
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [2]');
  end;
end;
}

function TFmNFe_PF_0400.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!  "TUnNFe_PF_0400.TextoArqDefinido()"');
end;

end.
