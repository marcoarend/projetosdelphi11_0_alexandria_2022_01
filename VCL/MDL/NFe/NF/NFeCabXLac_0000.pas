unit NFeCabXLac_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmNFeCabXLac_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    Label178: TLabel;
    EdnLacre: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FConta, FMaxRegCount: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabXLac_0000: TFmNFeCabXLac_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabXLac_0000.BtOKClick(Sender: TObject);
var
  nLacre: String;
  FatID, FatNum, Empresa, Controle, Conta: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  Conta          := FConta;
  nLacre         := EdnLacre.Text;
  //
  //Conta  := UMyMod.BPGS1I32('nfecabxlac', 'Conta', '', '', tsPos, SQLType, Conta);
  Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxlac', '', Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabxlac', False, [
  'nLacre'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nLacre], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    if (FMaxRegCount > 0) and (FQryIts.RecordCount >= FMaxRegCount) then
      Close
    else
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Item inclu�do com sucesso!');
      ImgTipo.SQLType := stIns;
      //
      EdnLacre.Text := '';
    end else
      Close;
  end;
end;

procedure TFmNFeCabXLac_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabXLac_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabXLac_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeCabXLac_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
