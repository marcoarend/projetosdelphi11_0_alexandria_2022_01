object FmNFeItsI_0000: TFmNFeItsI_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-005 :: Emiss'#227'o de NF-e'
  ClientHeight = 698
  ClientWidth = 1012
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel25: TPanel
    Left = 0
    Top = 48
    Width = 1012
    Height = 586
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1012
      Height = 46
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label231: TLabel
        Left = 160
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Item:'
      end
      object Label235: TLabel
        Left = 804
        Top = 4
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label236: TLabel
        Left = 872
        Top = 4
        Width = 37
        Height = 13
        Caption = 'EXTIPI:'
      end
      object Label237: TLabel
        Left = 920
        Top = 4
        Width = 38
        Height = 13
        Caption = 'Genero:'
      end
      object Label238: TLabel
        Left = 964
        Top = 4
        Width = 34
        Height = 13
        Caption = 'CFOP'#185':'
      end
      object Label13: TLabel
        Left = 112
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 44
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Fat.Num.:'
        Enabled = False
      end
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 29
        Height = 13
        Caption = 'FatID:'
        Enabled = False
      end
      object Label232: TLabel
        Left = 196
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label234: TLabel
        Left = 280
        Top = 4
        Width = 157
        Height = 13
        Caption = 'Descri'#231#227'o do produto ou servi'#231'o:'
      end
      object SpeedButton1: TSpeedButton
        Left = 254
        Top = 20
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label16: TLabel
        Left = 748
        Top = 4
        Width = 31
        Height = 13
        Caption = 'CEST:'
      end
      object EdnItem: TdmkEdit
        Left = 160
        Top = 20
        Width = 32
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        QryName = 'QrNFeItsI'
        QryCampo = 'nItem'
        UpdCampo = 'nItem'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edprod_NCM: TdmkEdit
        Left = 804
        Top = 20
        Width = 64
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 8
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000000'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_NCM'
        UpdCampo = 'prod_NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00000000'
        ValWarn = False
        OnChange = Edprod_NCMChange
      end
      object Edprod_EXTIPI: TdmkEdit
        Left = 872
        Top = 20
        Width = 43
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_EXTIPI'
        UpdCampo = 'prod_EXTIPI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
      end
      object Edprod_genero: TdmkEdit
        Left = 920
        Top = 20
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_genero'
        UpdCampo = 'prod_genero'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edprod_CFOP: TdmkEdit
        Left = 964
        Top = 20
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_CFOP'
        UpdCampo = 'prod_CFOP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = Edprod_CFOPKeyDown
      end
      object EdFatNum: TdmkEdit
        Left = 44
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'FatNum'
        UpdCampo = 'FatNum'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 112
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
      end
      object EdFatID: TdmkEdit
        Left = 8
        Top = 20
        Width = 33
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'FatID'
        UpdCampo = 'FatID'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edprod_cProd: TdmkEdit
        Left = 196
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_cProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnEnter = Edprod_cProdEnter
        OnExit = Edprod_cProdExit
      end
      object Edprod_xProd: TdmkEdit
        Left = 280
        Top = 20
        Width = 461
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_xProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edprod_CEST: TdmkEdit
        Left = 744
        Top = 20
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 7
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000000'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_CEST'
        UpdCampo = 'prod_CEST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = Edprod_NCMChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 46
      Width = 1012
      Height = 55
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox11: TGroupBox
        Left = 0
        Top = 0
        Width = 688
        Height = 55
        Align = alLeft
        Caption = ' Dados comerciais: '
        TabOrder = 0
        object Label242: TLabel
          Left = 316
          Top = 16
          Width = 66
          Height = 13
          Caption = 'Valor produto:'
          Enabled = False
        end
        object Label241: TLabel
          Left = 212
          Top = 16
          Width = 40
          Height = 13
          Caption = '$ Pre'#231'o:'
        end
        object Label240: TLabel
          Left = 136
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label239: TLabel
          Left = 88
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label233: TLabel
          Left = 4
          Top = 16
          Width = 78
          Height = 13
          Caption = 'GTIN, EAN, etc.'
        end
        object Label247: TLabel
          Left = 412
          Top = 16
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label248: TLabel
          Left = 480
          Top = 16
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label249: TLabel
          Left = 548
          Top = 16
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label6: TLabel
          Left = 616
          Top = 16
          Width = 67
          Height = 13
          Caption = '$ Desp. Aces:'
        end
        object Edprod_qCom: TdmkEdit
          Left = 136
          Top = 32
          Width = 73
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_qCom'
          UpdCampo = 'prod_qCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Edprod_qComChange
        end
        object Edprod_vProd: TdmkEdit
          Left = 316
          Top = 32
          Width = 93
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vProd'
          UpdCampo = 'prod_vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vUnCom: TdmkEdit
          Left = 212
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vUnCom'
          UpdCampo = 'prod_vUnCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Edprod_vUnComChange
        end
        object Edprod_uCom: TdmkEdit
          Left = 88
          Top = 32
          Width = 45
          Height = 21
          MaxLength = 6
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_uCom'
          UpdCampo = 'prod_uCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_uComChange
        end
        object Edprod_cEAN: TdmkEdit
          Left = 4
          Top = 32
          Width = 81
          Height = 21
          MaxLength = 14
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_cEAN'
          UpdCampo = 'prod_cEAN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_cEANChange
        end
        object Edprod_vFrete: TdmkEdit
          Left = 412
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vFrete'
          UpdCampo = 'prod_vFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vSeg: TdmkEdit
          Left = 480
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vSeg'
          UpdCampo = 'prod_vSeg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vDesc: TdmkEdit
          Left = 548
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vDesc'
          UpdCampo = 'prod_vDesc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vOutro: TdmkEdit
          Left = 616
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vOutro'
          UpdCampo = 'prod_vOutro'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox12: TGroupBox
        Left = 688
        Top = 0
        Width = 324
        Height = 55
        Align = alClient
        Caption = ' Dados tribut'#225'rios: '
        TabOrder = 1
        object Label001: TLabel
          Left = 4
          Top = 16
          Width = 78
          Height = 13
          Caption = 'GTIN, EAN, etc.'
        end
        object Label3: TLabel
          Left = 88
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label4: TLabel
          Left = 136
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label5: TLabel
          Left = 216
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
        end
        object Edprod_qTrib: TdmkEdit
          Left = 136
          Top = 32
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_qTrib'
          UpdCampo = 'prod_qTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Edprod_qTribChange
        end
        object Edprod_vUnTrib: TdmkEdit
          Left = 216
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_vUnTrib'
          UpdCampo = 'prod_vUnTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Edprod_vUnTribChange
          OnExit = Edprod_vUnTribExit
        end
        object Edprod_uTrib: TdmkEdit
          Left = 88
          Top = 32
          Width = 45
          Height = 21
          MaxLength = 6
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_uTrib'
          UpdCampo = 'prod_uTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Edprod_cEANTrib: TdmkEdit
          Left = 4
          Top = 32
          Width = 81
          Height = 21
          MaxLength = 14
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_cEANTrib'
          UpdCampo = 'prod_cEANTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object PageControl3: TPageControl
      Left = 0
      Top = 146
      Width = 1012
      Height = 383
      ActivePage = TabSheet18
      Align = alClient
      TabOrder = 2
      object TabSheet18: TTabSheet
        Caption = ' ICMS '
        ImageIndex = 2
        object Panel31: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox13: TGroupBox
            Left = 0
            Top = 0
            Width = 1004
            Height = 61
            Align = alTop
            Caption = 
              ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
              ': '
            TabOrder = 0
            object Panel38: TPanel
              Left = 2
              Top = 15
              Width = 1000
              Height = 44
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label244: TLabel
                Left = 8
                Top = 4
                Width = 156
                Height = 13
                Caption = 'N11 - Origem da mercadoria: [F3]'
              end
              object EdICMS_Orig: TdmkEdit
                Left = 8
                Top = 20
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrNFeItsN'
                QryCampo = 'ICMS_Orig'
                UpdCampo = 'ICMS_Orig'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdICMS_OrigChange
                OnKeyDown = EdICMS_OrigKeyDown
              end
              object EdTextoA: TdmkEdit
                Left = 56
                Top = 20
                Width = 681
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object PageControl1: TPageControl
            Left = 0
            Top = 61
            Width = 1004
            Height = 294
            ActivePage = TabSheet2
            Align = alClient
            TabOrder = 1
            object TabSheet2: TTabSheet
              Caption = 'ICMS Normal'
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 266
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox14: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 266
                  Align = alClient
                  Caption = ' ICMS Normal: '
                  TabOrder = 0
                  object Panel6: TPanel
                    Left = 2
                    Top = 15
                    Width = 992
                    Height = 249
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object RGICMS_modBC: TdmkRadioGroup
                      Left = 0
                      Top = 41
                      Width = 217
                      Height = 208
                      Align = alLeft
                      Caption = ' N13 - Modalid. de determ. da BC do ICMS: '
                      ItemIndex = 3
                      Items.Strings = (
                        'Margem valor agregado (%)'
                        'Pauta (valor)'
                        'Pre'#231'o tabelado m'#225'ximo (valor)'
                        'Valor da opera'#231#227'o')
                      TabOrder = 1
                      OnClick = RGICMS_modBCClick
                      QryName = 'QrNFeItsN'
                      QryCampo = 'ICMS_modBC'
                      UpdCampo = 'ICMS_modBC'
                      UpdType = utYes
                      OldValor = 0
                    end
                    object GroupBox8: TGroupBox
                      Left = 217
                      Top = 41
                      Width = 240
                      Height = 208
                      Align = alLeft
                      TabOrder = 2
                      object LaICMS_pRedBC: TLabel
                        Left = 8
                        Top = 24
                        Width = 83
                        Height = 13
                        Caption = 'N14: % Red. BC'#178':'
                      end
                      object LaICMS_VBC: TLabel
                        Left = 7
                        Top = 62
                        Width = 88
                        Height = 13
                        Caption = 'N15 - Valor da BC:'
                      end
                      object Label258: TLabel
                        Left = 8
                        Top = 145
                        Width = 85
                        Height = 13
                        Caption = 'N17 - Valor ICMS:'
                      end
                      object LaICMS_pICMS: TLabel
                        Left = 8
                        Top = 102
                        Width = 69
                        Height = 13
                        Caption = 'N16 - % ICMS:'
                      end
                      object Label31: TLabel
                        Left = 123
                        Top = 62
                        Width = 102
                        Height = 13
                        Caption = 'N17a - Valor BC FCP:'
                      end
                      object Label32: TLabel
                        Left = 124
                        Top = 101
                        Width = 69
                        Height = 13
                        Caption = 'N17b - % FCP:'
                      end
                      object Label33: TLabel
                        Left = 124
                        Top = 145
                        Width = 85
                        Height = 13
                        Caption = 'N17c - Valor FCP:'
                      end
                      object EdICMS_pRedBC: TdmkEdit
                        Left = 7
                        Top = 38
                        Width = 110
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_pRedBC'
                        UpdCampo = 'ICMS_pRedBC'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                        OnChange = EdICMS_pRedBCChange
                      end
                      object EdICMS_vBC: TdmkEdit
                        Left = 7
                        Top = 78
                        Width = 110
                        Height = 21
                        Alignment = taRightJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -12
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                        TabOrder = 1
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_vBC'
                        UpdCampo = 'ICMS_vBC'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                        OnChange = EdICMS_vBCChange
                      end
                      object EdICMS_vICMS: TdmkEdit
                        Left = 8
                        Top = 161
                        Width = 109
                        Height = 21
                        Alignment = taRightJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -12
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                        TabOrder = 3
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_vICMS'
                        UpdCampo = 'ICMS_vICMS'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                      object EdICMS_pICMS: TdmkEdit
                        Left = 8
                        Top = 118
                        Width = 109
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_pICMS'
                        UpdCampo = 'ICMS_pICMS'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                        OnChange = EdICMS_pICMSChange
                      end
                      object EdICMS_vBCFCP: TdmkEdit
                        Left = 123
                        Top = 78
                        Width = 110
                        Height = 21
                        Alignment = taRightJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -12
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 4
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_vBCFCP'
                        UpdCampo = 'ICMS_vBCFCP'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                      object EdICMS_pFCP: TdmkEdit
                        Left = 124
                        Top = 117
                        Width = 109
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 5
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 4
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,0000'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_pFCP'
                        UpdCampo = 'ICMS_pFCP'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                      object EdICMS_vFCP: TdmkEdit
                        Left = 124
                        Top = 161
                        Width = 109
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 6
                        FormatType = dmktfDouble
                        MskType = fmtNone
                        DecimalSize = 2
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0,00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_vFCP'
                        UpdCampo = 'ICMS_vFCP'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0.000000000000000000
                        ValWarn = False
                      end
                    end
                    object Panel17: TPanel
                      Left = 0
                      Top = 0
                      Width = 992
                      Height = 41
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label245: TLabel
                        Left = 8
                        Top = 0
                        Width = 202
                        Height = 13
                        Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                      end
                      object EdICMS_CST: TdmkEdit
                        Left = 8
                        Top = 17
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 2
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '0'
                        ValMax = '90'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '00'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_CST'
                        UpdCampo = 'ICMS_CST'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        OnChange = EdICMS_CSTChange
                        OnKeyDown = EdICMS_CSTKeyDown
                      end
                      object EdTextoB: TdmkEdit
                        Left = 56
                        Top = 17
                        Width = 681
                        Height = 21
                        ReadOnly = True
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object PageControl2: TPageControl
                      Left = 464
                      Top = 41
                      Width = 528
                      Height = 208
                      Margins.Left = 2
                      Margins.Top = 2
                      Margins.Right = 2
                      Margins.Bottom = 2
                      ActivePage = TabSheet7
                      Align = alRight
                      TabOrder = 3
                      object TabSheet6: TTabSheet
                        Margins.Left = 2
                        Margins.Top = 2
                        Margins.Right = 2
                        Margins.Bottom = 2
                        Caption = ' CST 51'
                        object Label21: TLabel
                          Left = 4
                          Top = 4
                          Width = 93
                          Height = 13
                          Caption = 'N16a - $ ICMS Op.:'
                        end
                        object Label24: TLabel
                          Left = 4
                          Top = 84
                          Width = 87
                          Height = 13
                          Caption = 'N16c - $ ICMS dif:'
                        end
                        object Label22: TLabel
                          Left = 4
                          Top = 44
                          Width = 65
                          Height = 13
                          Caption = 'N16b - % Dif.:'
                        end
                        object Label48: TLabel
                          Left = 174
                          Top = 4
                          Width = 156
                          Height = 13
                          Caption = 'N17d - % dif. ICMS relat. ao FCP:'
                        end
                        object Label49: TLabel
                          Left = 174
                          Top = 44
                          Width = 152
                          Height = 13
                          Caption = 'N17d - $ ICMS relativo FCP Dif.:'
                        end
                        object Label50: TLabel
                          Left = 174
                          Top = 84
                          Width = 169
                          Height = 13
                          Caption = 'N17f - $ efetivo de ICMS relat. FCP:'
                        end
                        object EdICMS_vICMSOp: TdmkEdit
                          Left = 4
                          Top = 20
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vICMSOp'
                          UpdCampo = 'ICMS_vICMSOp'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vICMSDif: TdmkEdit
                          Left = 4
                          Top = 100
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 2
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vICMSDif'
                          UpdCampo = 'ICMS_vICMSDif'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pDif: TdmkEdit
                          Left = 4
                          Top = 60
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pDif'
                          UpdCampo = 'ICMS_pDif'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pFCPDif: TdmkEdit
                          Left = 174
                          Top = 20
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 3
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pFCPDif'
                          UpdCampo = 'ICMS_pFCPDif'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vFCPDif: TdmkEdit
                          Left = 174
                          Top = 60
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 4
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vFCPDif'
                          UpdCampo = 'ICMS_vFCPDif'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vFCPEfet: TdmkEdit
                          Left = 174
                          Top = 100
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 5
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vFCPEfet'
                          UpdCampo = 'ICMS_vFCPEfet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                      end
                      object TabSheet7: TTabSheet
                        Margins.Left = 2
                        Margins.Top = 2
                        Margins.Right = 2
                        Margins.Bottom = 2
                        Caption = ' CST 60 / CSOSN 500: '
                        ImageIndex = 1
                        object Label25: TLabel
                          Left = 4
                          Top = 4
                          Width = 109
                          Height = 13
                          Caption = 'N26 - BC ICMSST Ret:'
                        end
                        object Label38: TLabel
                          Left = 4
                          Top = 44
                          Width = 114
                          Height = 13
                          Hint = 'Al'#237'quota suportada pelo Consumidor Final'
                          Caption = 'N26.a - Aliq. sup. p/ CF:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object Label27: TLabel
                          Left = 4
                          Top = 124
                          Width = 116
                          Height = 13
                          Caption = 'N27 - Val ICMS ST Ret.:'
                        end
                        object Label39: TLabel
                          Left = 174
                          Top = 4
                          Width = 173
                          Height = 13
                          Caption = 'N27a - Valor BC FCP ret. ant. p/ RT:'
                        end
                        object Label40: TLabel
                          Left = 174
                          Top = 44
                          Width = 142
                          Height = 13
                          Caption = 'N27.b - % FCP ret. ant. p/ ST:'
                        end
                        object Label41: TLabel
                          Left = 174
                          Top = 84
                          Width = 134
                          Height = 13
                          Caption = 'N27d - Valor FCP ret. p/ ST:'
                        end
                        object Label28: TLabel
                          Left = 4
                          Top = 84
                          Width = 168
                          Height = 13
                          Hint = 'Valor do ICMS pr'#243'prio do Substituto'
                          Caption = 'N26.b - $ ICMS pr'#243'p. do Substituto:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object Label43: TLabel
                          Left = 352
                          Top = 4
                          Width = 148
                          Height = 13
                          Hint = 'Percentual de redu'#231#227'o da base de c'#225'lculo efetiva'
                          Caption = 'N34 - % red. base c'#225'lc. efetiva:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object Label44: TLabel
                          Left = 352
                          Top = 44
                          Width = 154
                          Height = 13
                          Hint = 'Valor da base de c'#225'lculo efetiva'
                          Caption = 'N35 - Valor base c'#225'lculo efetiva:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object Label45: TLabel
                          Left = 352
                          Top = 84
                          Width = 151
                          Height = 13
                          Hint = 'Al'#237'quota do ICMS efetiva'
                          Caption = 'N36 - Al'#237'quota do ICMS efetiva:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object Label46: TLabel
                          Left = 352
                          Top = 124
                          Width = 135
                          Height = 13
                          Hint = 'Valor do ICMS efetivo'
                          Caption = 'N37 - Valor do ICMS efetivo:'
                          ParentShowHint = False
                          ShowHint = True
                        end
                        object EdICMS_vBCSTRet: TdmkEdit
                          Left = 4
                          Top = 20
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vBCSTRet'
                          UpdCampo = 'ICMS_vBCSTRet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pST: TdmkEdit
                          Left = 4
                          Top = 60
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pST'
                          UpdCampo = 'ICMS_pST'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vICMSSTRet: TdmkEdit
                          Left = 4
                          Top = 140
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 3
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vICMSSTRet'
                          UpdCampo = 'ICMS_vICMSSTRet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vBCFCPSTRet: TdmkEdit
                          Left = 174
                          Top = 20
                          Width = 172
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = []
                          ParentFont = False
                          TabOrder = 4
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vBCFCPSTRet'
                          UpdCampo = 'ICMS_vBCFCPSTRet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pFCPSTRet: TdmkEdit
                          Left = 174
                          Top = 60
                          Width = 172
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 5
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pFCPSTRet'
                          UpdCampo = 'ICMS_pFCPSTRet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vFCPSTRet: TdmkEdit
                          Left = 174
                          Top = 100
                          Width = 172
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = []
                          ParentFont = False
                          TabOrder = 6
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vFCPSTRet'
                          UpdCampo = 'ICMS_vFCPSTRet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vICMSSubstituto: TdmkEdit
                          Left = 4
                          Top = 100
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 2
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vICMSSubstituto'
                          UpdCampo = 'ICMS_vICMSSubstituto'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pRedBCEfet: TdmkEdit
                          Left = 352
                          Top = 20
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 7
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pRedBCEfet'
                          UpdCampo = 'ICMS_pRedBCEfet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vBCEfet: TdmkEdit
                          Left = 352
                          Top = 60
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 8
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vBCEfet'
                          UpdCampo = 'ICMS_vBCEfet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pICMSEfet: TdmkEdit
                          Left = 352
                          Top = 100
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 9
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_pICMSEfet'
                          UpdCampo = 'ICMS_pICMSEfet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_vICMSEfet: TdmkEdit
                          Left = 352
                          Top = 140
                          Width = 165
                          Height = 21
                          Alignment = taRightJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 10
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryName = 'QrNFeItsN'
                          QryCampo = 'ICMS_vICMSEfet'
                          UpdCampo = 'ICMS_vICMSEfet'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = 'ICMS ST'
              ImageIndex = 1
              object Panel15: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 266
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox15: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 170
                  Align = alTop
                  Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
                  TabOrder = 0
                  object LaICMS_pMVAST: TLabel
                    Left = 372
                    Top = 20
                    Width = 86
                    Height = 13
                    Caption = 'N19: % MVA. BC'#178':'
                  end
                  object LaICMS_pRedBCST: TLabel
                    Left = 488
                    Top = 20
                    Width = 83
                    Height = 13
                    Caption = 'N20: % Red. BC'#178':'
                  end
                  object Label259: TLabel
                    Left = 603
                    Top = 20
                    Width = 105
                    Height = 13
                    Caption = 'N21 - Valor da BC ST:'
                  end
                  object LaICMS_pICMSST: TLabel
                    Left = 718
                    Top = 20
                    Width = 86
                    Height = 13
                    Caption = 'N22 - % ICMS ST:'
                  end
                  object Label261: TLabel
                    Left = 834
                    Top = 20
                    Width = 102
                    Height = 13
                    Caption = 'N23 - Valor ICMS ST:'
                  end
                  object Label34: TLabel
                    Left = 372
                    Top = 62
                    Width = 102
                    Height = 13
                    Caption = 'N23a - Valor BC FCP:'
                  end
                  object Label35: TLabel
                    Left = 488
                    Top = 62
                    Width = 69
                    Height = 13
                    Caption = 'N23b - % FCP:'
                  end
                  object Label37: TLabel
                    Left = 603
                    Top = 62
                    Width = 85
                    Height = 13
                    Caption = 'N23d - Valor FCP:'
                  end
                  object Label51: TLabel
                    Left = 718
                    Top = 62
                    Width = 110
                    Height = 13
                    Caption = 'N33a - $ ICMS-ST des:'
                  end
                  object Label53: TLabel
                    Left = 834
                    Top = 62
                    Width = 148
                    Height = 13
                    Caption = 'N33b - Motivo deson ICMS-ST:'
                  end
                  object RGICMS_modBCST: TdmkRadioGroup
                    Left = 2
                    Top = 15
                    Width = 359
                    Height = 153
                    Align = alLeft
                    Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
                    ItemIndex = 0
                    Items.Strings = (
                      '0 = Pre'#231'o tabelado ou m'#225'ximo sugerido'
                      '1 = Lista Negativa (valor)'
                      '2 = Lista Positiva (valor);'
                      '3 = Lista Neutra (valor)'
                      '4 = Margem Valor Agregado (%)'
                      '5 = Pauta (valor)'
                      '6 = Valor da Opera'#231#227'o (NT 2019.001)')
                    TabOrder = 0
                    OnClick = RGICMS_modBCSTClick
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_modBCST'
                    UpdCampo = 'ICMS_modBCST'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object EdICMS_pRedBCST: TdmkEdit
                    Left = 488
                    Top = 36
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pRedBCST'
                    UpdCampo = 'ICMS_pRedBCST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_pRedBCSTChange
                  end
                  object EdICMS_pMVAST: TdmkEdit
                    Left = 372
                    Top = 36
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pMVAST'
                    UpdCampo = 'ICMS_pMVAST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_pMVASTChange
                  end
                  object EdICMS_vBCST: TdmkEdit
                    Left = 603
                    Top = 36
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vBCST'
                    UpdCampo = 'ICMS_vBCST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_vBCSTChange
                  end
                  object EdICMS_pICMSST: TdmkEdit
                    Left = 718
                    Top = 36
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pICMSST'
                    UpdCampo = 'ICMS_pICMSST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_pICMSSTChange
                  end
                  object EdICMS_vICMSST: TdmkEdit
                    Left = 834
                    Top = 36
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vICMSST'
                    UpdCampo = 'ICMS_vICMSST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = EdICMS_vICMSSTExit
                  end
                  object EdICMS_vBCFCPST: TdmkEdit
                    Left = 372
                    Top = 78
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 6
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vBCFCPST'
                    UpdCampo = 'ICMS_vBCFCPST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMS_pFCPST: TdmkEdit
                    Left = 488
                    Top = 78
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 7
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_pFCPST'
                    UpdCampo = 'ICMS_pFCPST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_pICMSChange
                  end
                  object EdICMS_vFCPST: TdmkEdit
                    Left = 603
                    Top = 78
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 8
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vFCPST'
                    UpdCampo = 'ICMS_vFCPST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_pICMSChange
                  end
                  object EdICMS_vICMSSTDeson: TdmkEdit
                    Left = 718
                    Top = 78
                    Width = 112
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 9
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vICMSSTDeson'
                    UpdCampo = 'ICMS_vICMSSTDeson'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMS_motDesICMSST: TdmkEdit
                    Left = 834
                    Top = 78
                    Width = 23
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 10
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_motDesICMSST'
                    UpdCampo = 'ICMS_motDesICMSST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMS_motDesICMSSTChange
                    OnKeyDown = EdICMS_motDesICMSSTKeyDown
                  end
                  object EdN33b_TXT: TdmkEdit
                    Left = 859
                    Top = 78
                    Width = 128
                    Height = 21
                    Enabled = False
                    ReadOnly = True
                    TabOrder = 11
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = 'ICMS Deson'
              ImageIndex = 2
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 266
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox6: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 266
                  Align = alClient
                  Caption = ' ICMS Desonerado: '
                  TabOrder = 0
                  object Label23: TLabel
                    Left = 860
                    Top = 17
                    Width = 123
                    Height = 13
                    Caption = 'N27a - Valor ICMS deson:'
                  end
                  object EdICMS_vICMSDeson: TdmkEdit
                    Left = 860
                    Top = 33
                    Width = 132
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrNFeItsN'
                    QryCampo = 'ICMS_vICMSDeson'
                    UpdCampo = 'ICMS_vICMSDeson'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object Panel5: TPanel
                    Left = 2
                    Top = 15
                    Width = 851
                    Height = 249
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 1
                    object RGICMS_motDesICMS: TdmkRadioGroup
                      Left = 0
                      Top = 0
                      Width = 851
                      Height = 249
                      Align = alClient
                      Caption = ' N28 - Motivo da desonera'#231#227'o do ICMS: '
                      Columns = 2
                      ItemIndex = 0
                      Items.Strings = (
                        '0=Indefinido'
                        '1=T'#225'xi;'
                        '2=Deficiente F'#237'sico(Revogado)'
                        '3=Uso na agropecu'#225'ria;'
                        '4=Frotista/Locadora;'
                        '5=Diplom'#225'tico/Consular;'
                        
                          '6=Utilit'#225'rios e Motocicletas da Amaz'#244'nia Ocidental e '#193'reas de Li' +
                          'vre Com'#233'rcio (Resolu'#231#227'o 714/88 e 790/94 '#8211' CONTRAN e suas altera'#231 +
                          #245'es);'
                        '7=SUFRAMA;'
                        '8=Venda a '#211'rg'#227'o P'#250'blico;'
                        '9=Outros;'
                        '10=Deficiente Condutor (Conv'#234'nio ICMS 38/12);'
                        '11=Deficiente N'#227'o Condutor (Conv'#234'nio ICMS 38/12).'
                        '12='#211'rg'#227'o de fomento e desenvolvimento agropecu'#225'rio.')
                      TabOrder = 0
                      OnClick = RGICMS_modBCClick
                      QryName = 'QrNFeItsN'
                      QryCampo = 'ICMS_motDesICMS'
                      UpdCampo = 'ICMS_motDesICMS'
                      UpdType = utYes
                      OldValor = 0
                    end
                  end
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = 'CSOSN'
              ImageIndex = 3
              object Panel18: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 266
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox10: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 266
                  Align = alClient
                  Caption = ' ICMS Grupo CRT=1: '
                  TabOrder = 0
                  object Panel19: TPanel
                    Left = 2
                    Top = 15
                    Width = 992
                    Height = 249
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object LapCredSN: TLabel
                      Left = 8
                      Top = 46
                      Width = 309
                      Height = 13
                      Caption = 
                        'N29 - Al'#237'quota aplic'#225'vel de c'#225'lculo do cr'#233'dito (Simples Nacional' +
                        '):'
                    end
                    object Label26: TLabel
                      Left = 8
                      Top = 70
                      Width = 450
                      Height = 13
                      Caption = 
                        'N30 - Valor cr'#233'dito do ICMS que pode ser aproveitado nos termos ' +
                        'do art.123 (simples nacional):'
                    end
                    object Panel20: TPanel
                      Left = 0
                      Top = 0
                      Width = 992
                      Height = 41
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label36: TLabel
                        Left = 8
                        Top = 0
                        Width = 362
                        Height = 13
                        Caption = 
                          'N12a - CSOSN (C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional' +
                          '): [F3]'
                      end
                      object EdICMS_CSOSN: TdmkEdit
                        Left = 8
                        Top = 17
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 3
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '0'
                        ValMax = '999'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '000'
                        QryName = 'QrNFeItsN'
                        QryCampo = 'ICMS_CSOSN'
                        UpdCampo = 'ICMS_CSOSN'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        OnChange = EdICMS_CSOSNChange
                        OnKeyDown = EdICMS_CSOSNKeyDown
                      end
                      object EdCSOSN_Txt: TdmkEdit
                        Left = 56
                        Top = 17
                        Width = 681
                        Height = 21
                        ReadOnly = True
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object EdICMS_pCredSN: TdmkEdit
                      Left = 691
                      Top = 42
                      Width = 46
                      Height = 21
                      TabStop = False
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      QryCampo = 'ICMS_pCredSN'
                      UpdCampo = 'ICMS_pCredSN'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdICMS_vCredICMSSN: TdmkEdit
                      Left = 637
                      Top = 66
                      Width = 100
                      Height = 21
                      TabStop = False
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      QryCampo = 'ICMS_vCredICMSSN'
                      UpdCampo = 'ICMS_vCredICMSSN'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
        end
      end
      object TabSheet19: TTabSheet
        Caption = ' IPI '
        ImageIndex = 3
        object Panel34: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel39: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 355
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox19: TGroupBox
              Left = 0
              Top = 227
              Width = 1004
              Height = 128
              Align = alBottom
              Caption = ' Outras informa'#231#245'es: '
              TabOrder = 1
              object Label265: TLabel
                Left = 12
                Top = 24
                Width = 299
                Height = 13
                Caption = 'O02: Classe de enquadramento de IPI para cigarros e bebidas: '
              end
              object Label266: TLabel
                Left = 12
                Top = 48
                Width = 327
                Height = 13
                Caption = 
                  'O03: CNPJ do produtor da mercadoria quando diferente do emitente' +
                  '. '
              end
              object Label267: TLabel
                Left = 12
                Top = 72
                Width = 177
                Height = 13
                Caption = 'O04: C'#243'digo do seleo de controle IPI:'
              end
              object Label268: TLabel
                Left = 12
                Top = 96
                Width = 177
                Height = 13
                Caption = 'O05: Quantidade de selo de controle:'
              end
              object Label273: TLabel
                Left = 452
                Top = 48
                Width = 248
                Height = 13
                Caption = 'Somente nos casos de exporta'#231#227'o direta ou indireta.'
              end
              object EdIPI_clEnq: TdmkEdit
                Left = 344
                Top = 21
                Width = 104
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_clEnq'
                UpdCampo = 'IPI_clEnq'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIPI_CNPJProd: TdmkEdit
                Left = 344
                Top = 45
                Width = 104
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_CNPJProd'
                UpdCampo = 'IPI_CNPJProd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIPI_cSelo: TdmkEdit
                Left = 344
                Top = 69
                Width = 104
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_cSelo'
                UpdCampo = 'IPI_cSelo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIPI_qSelo: TdmkEdit
                Left = 344
                Top = 93
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_qSelo'
                UpdCampo = 'IPI_qSelo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnExit = EdIPI_qSeloExit
              end
            end
            object Panel40: TPanel
              Left = 0
              Top = 147
              Width = 1004
              Height = 80
              Align = alBottom
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label251: TLabel
                Left = 332
                Top = 8
                Width = 158
                Height = 13
                Caption = 'O12: Valor por unidade tribut'#225'vel:'
              end
              object Label262: TLabel
                Left = 16
                Top = 56
                Width = 109
                Height = 13
                Caption = 'O13: % Aliquota de IPI:'
              end
              object Label263: TLabel
                Left = 16
                Top = 8
                Width = 119
                Height = 13
                Caption = 'O06: C'#243'd. enq. legal IPI'#185':'
              end
              object Label269: TLabel
                Left = 16
                Top = 32
                Width = 116
                Height = 13
                Caption = 'O10: Valor da BC do IPI:'
              end
              object Label270: TLabel
                Left = 332
                Top = 32
                Width = 273
                Height = 13
                Caption = 'O11: Quantidade total na unidade padr'#227'o para tributa'#231#227'o:'
              end
              object Label271: TLabel
                Left = 332
                Top = 56
                Width = 84
                Height = 13
                Caption = 'O14: Valor do IPI:'
              end
              object Label272: TLabel
                Left = 720
                Top = 32
                Width = 222
                Height = 13
                Caption = '(somente para produtos tributados por unidade)'
              end
              object EdIPI_cEnq: TdmkEdit
                Left = 180
                Top = 4
                Width = 104
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '999'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_cEnq'
                UpdCampo = 'IPI_cEnq'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '999'
                ValWarn = False
              end
              object EdIPI_pIPI: TdmkEdit
                Left = 180
                Top = 52
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_pIPI'
                UpdCampo = 'IPI_pIPI'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdIPI_pIPIChange
              end
              object EdIPI_vUnid: TdmkEdit
                Left = 612
                Top = 4
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_vUnid'
                UpdCampo = 'IPI_vUnid'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdIPI_vUnidChange
              end
              object EdIPI_vBC: TdmkEdit
                Left = 180
                Top = 28
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_vBC'
                UpdCampo = 'IPI_vBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdIPI_vBCChange
              end
              object EdIPI_qUnid: TdmkEdit
                Left = 612
                Top = 28
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_qUnid'
                UpdCampo = 'IPI_qUnid'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdIPI_qUnidChange
              end
              object EdIPI_vIPI: TdmkEdit
                Left = 612
                Top = 52
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsO'
                QryCampo = 'IPI_vIPI'
                UpdCampo = 'IPI_vIPI'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1004
              Height = 61
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 2
              object GroupBox18: TGroupBox
                Left = 321
                Top = 0
                Width = 683
                Height = 61
                Align = alClient
                Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
                TabOrder = 0
                object Label264: TLabel
                  Left = 8
                  Top = 16
                  Width = 199
                  Height = 13
                  Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object EdIPI_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '99'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryName = 'QrNFeItsO'
                  QryCampo = 'IPI_CST'
                  UpdCampo = 'IPI_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdIPI_CSTChange
                  OnKeyDown = EdIPI_CSTKeyDown
                end
                object EdTextoIPI_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 613
                  Height = 21
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox1: TGroupBox
                Left = 0
                Top = 0
                Width = 321
                Height = 61
                Align = alLeft
                Caption = ' Tag do IPI: '
                TabOrder = 1
                object CkTem_IPI: TdmkCheckBox
                  Left = 12
                  Top = 28
                  Width = 301
                  Height = 17
                  Caption = 'Informar dados do IPI mesmo se o valor do IPI for zero.'
                  TabOrder = 0
                  OnClick = CkTem_IPIClick
                  QryName = 'QrNFeItsI'
                  QryCampo = 'Tem_IPI'
                  UpdCampo = 'Tem_IPI'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
          end
        end
      end
      object TabSheet20: TTabSheet
        Caption = ' II '
        ImageIndex = 4
        object Panel33: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 65
            Width = 1004
            Height = 290
            Align = alClient
            Caption = ' Dados do II (Imposto de Importa'#231#227'o): '
            TabOrder = 0
            object Label301: TLabel
              Left = 8
              Top = 20
              Width = 87
              Height = 13
              Caption = 'P02 - Valor da BC:'
            end
            object Label302: TLabel
              Left = 8
              Top = 44
              Width = 173
              Height = 13
              Caption = 'P03 - Valor das depesas aduaneiras:'
            end
            object Label303: TLabel
              Left = 8
              Top = 68
              Width = 179
              Height = 13
              Caption = 'P04 - Valor do imposto de importa'#231#227'o:'
            end
            object Label304: TLabel
              Left = 8
              Top = 92
              Width = 90
              Height = 13
              Caption = 'P05 - Valor do IOF:'
            end
            object Label19: TLabel
              Left = 8
              Top = 116
              Width = 98
              Height = 13
              Caption = 'I70 - N'#250'mero da FCI:'
            end
            object Label20: TLabel
              Left = 548
              Top = 116
              Width = 361
              Height = 13
              Caption = 
                'FCI: Ficha de conteudo de importa'#231#227'o - Resolu'#231#227'o 13/2012 senado ' +
                'federal.'
            end
            object EdII_vBC: TdmkEdit
              Left = 204
              Top = 16
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vBC'
              UpdCampo = 'II_vBC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vDespAdu: TdmkEdit
              Left = 204
              Top = 40
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vDespAdu'
              UpdCampo = 'II_vDespAdu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vII: TdmkEdit
              Left = 204
              Top = 64
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vII'
              UpdCampo = 'II_vII'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_vIOF: TdmkEdit
              Left = 204
              Top = 88
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsP'
              QryCampo = 'II_vIOF'
              UpdCampo = 'II_vIOF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdII_vIOFExit
            end
            object Edprod_nFCI: TdmkEdit
              Left = 204
              Top = 112
              Width = 341
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 36
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsI'
              QryCampo = 'prod_nFCI'
              UpdCampo = 'prod_nFCI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 1004
            Height = 65
            Align = alTop
            Caption = ' Tag do II (Imposto de Importa'#231#227'o): '
            TabOrder = 1
            object CkTem_II: TdmkCheckBox
              Left = 12
              Top = 28
              Width = 277
              Height = 17
              Caption = 'Informar dados do II mesmo se o valor do II for zero.'
              TabOrder = 0
              QryName = 'QrNFeItsI'
              QryCampo = 'Tem_II'
              UpdCampo = 'Tem_II'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
        end
      end
      object TabSheet21: TTabSheet
        Caption = ' PIS '
        ImageIndex = 5
        object Panel32: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox20: TGroupBox
            Left = 0
            Top = 0
            Width = 1004
            Height = 129
            Align = alTop
            Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
            TabOrder = 0
            object Label274: TLabel
              Left = 8
              Top = 16
              Width = 199
              Height = 13
              Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
            end
            object Label281: TLabel
              Left = 560
              Top = 72
              Width = 88
              Height = 13
              Caption = 'Q09: Valor do PIS:'
            end
            object EdTextoPIS_CST: TdmkEdit
              Left = 56
              Top = 32
              Width = 613
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPIS_CST: TdmkEdit
              Left = 8
              Top = 32
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '01'
              QryName = 'QrNFeItsQ'
              QryCampo = 'PIS_CST'
              UpdCampo = 'PIS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
              OnChange = EdPIS_CSTChange
              OnKeyDown = EdPIS_CSTKeyDown
            end
            object GroupBox22: TGroupBox
              Left = 8
              Top = 56
              Width = 268
              Height = 65
              Caption = ' Por Al'#237'quota: '
              TabOrder = 2
              object Label279: TLabel
                Left = 8
                Top = 16
                Width = 85
                Height = 13
                Caption = 'Q07: Valor da BC:'
              end
              object Label276: TLabel
                Left = 136
                Top = 16
                Width = 86
                Height = 13
                Caption = 'Q08: Aliq. PIS (%):'
              end
              object EdPIS_vBC: TdmkEdit
                Left = 8
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_vBC'
                UpdCampo = 'PIS_vBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPIS_vBCChange
              end
              object EdPIS_pPIS: TdmkEdit
                Left = 136
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_pPIS'
                UpdCampo = 'PIS_pPIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPIS_pPISChange
              end
            end
            object GroupBox23: TGroupBox
              Left = 280
              Top = 56
              Width = 268
              Height = 65
              Caption = ' Por Unidade: '
              TabOrder = 3
              object Label275: TLabel
                Left = 136
                Top = 16
                Width = 95
                Height = 13
                Caption = 'Q11: % Aliq. PIS ($):'
              end
              object Label282: TLabel
                Left = 8
                Top = 16
                Width = 93
                Height = 13
                Caption = 'Q10: Qtde vendida:'
              end
              object EdPIS_qBCProd: TdmkEdit
                Left = 8
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_qBCProd'
                UpdCampo = 'PIS_qBCProd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPIS_qBCProdChange
              end
              object EdPIS_vAliqProd: TdmkEdit
                Left = 136
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsQ'
                QryCampo = 'PIS_vAliqProd'
                UpdCampo = 'PIS_vAliqProd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPIS_vAliqProdChange
              end
            end
            object EdPIS_vPIS: TdmkEdit
              Left = 560
              Top = 88
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsQ'
              QryCampo = 'PIS_vPIS'
              UpdCampo = 'PIS_vPIS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox21: TGroupBox
            Left = 0
            Top = 129
            Width = 1004
            Height = 226
            Align = alClient
            Caption = ' PIS ST: '
            TabOrder = 1
            object Label284: TLabel
              Left = 560
              Top = 32
              Width = 88
              Height = 13
              Caption = 'R06: Valor do PIS:'
            end
            object GroupBox24: TGroupBox
              Left = 8
              Top = 16
              Width = 268
              Height = 65
              Caption = ' Por Al'#237'quota: '
              TabOrder = 0
              object Label277: TLabel
                Left = 8
                Top = 16
                Width = 85
                Height = 13
                Caption = 'R02: Valor da BC:'
              end
              object Label278: TLabel
                Left = 136
                Top = 16
                Width = 86
                Height = 13
                Caption = 'R03: Aliq. PIS (%):'
              end
              object EdPISST_vBC: TdmkEdit
                Left = 8
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsR'
                QryCampo = 'PISST_vBC'
                UpdCampo = 'PISST_vBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPISST_vBCChange
              end
              object EdPISST_pPIS: TdmkEdit
                Left = 136
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsR'
                QryCampo = 'PISST_pPIS'
                UpdCampo = 'PISST_pPIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPISST_pPISChange
              end
            end
            object GroupBox25: TGroupBox
              Left = 280
              Top = 16
              Width = 268
              Height = 65
              Caption = ' Por Unidade: '
              TabOrder = 1
              object Label280: TLabel
                Left = 136
                Top = 16
                Width = 95
                Height = 13
                Caption = 'R05: % Aliq. PIS ($):'
              end
              object Label283: TLabel
                Left = 8
                Top = 16
                Width = 93
                Height = 13
                Caption = 'R04: Qtde vendida:'
              end
              object EdPISST_qBCProd: TdmkEdit
                Left = 8
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsR'
                QryCampo = 'PISST_qBCProd'
                UpdCampo = 'PISST_qBCProd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPISST_qBCProdChange
              end
              object EdPISST_vAliqProd: TdmkEdit
                Left = 136
                Top = 32
                Width = 124
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsR'
                QryCampo = 'PISST_vAliqProd'
                UpdCampo = 'PISST_vAliqProd'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPISST_vAliqProdChange
              end
            end
            object EdPISST_vPIS: TdmkEdit
              Left = 560
              Top = 48
              Width = 104
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsR'
              QryCampo = 'PISST_vPIS'
              UpdCampo = 'PISST_vPIS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object RGPISST_indSomaPISST: TdmkRadioGroup
              Left = 676
              Top = 28
              Width = 305
              Height = 41
              Caption = ' 0=Valor do PISST comp'#245'e o valor total da NF-e: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                '0=N'#227'o'
                '1=Sim'
                '?=N/D')
              TabOrder = 3
              OnClick = RGPISST_indSomaPISSTClick
              QryName = 'QrNFeItsR'
              QryCampo = 'PISST_indSomaPISST'
              UpdCampo = 'PISST_indSomaPISST'
              UpdType = utYes
              OldValor = 0
            end
          end
        end
      end
      object TabSheet22: TTabSheet
        Caption = ' COFINS '
        ImageIndex = 6
        object Panel35: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel41: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 355
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox26: TGroupBox
              Left = 0
              Top = 0
              Width = 1004
              Height = 129
              Align = alTop
              Caption = ' CST - COFINS (Programa de Integra'#231#227'o Social): '
              TabOrder = 0
              object Label285: TLabel
                Left = 8
                Top = 16
                Width = 198
                Height = 13
                Caption = 'S06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
              end
              object Label286: TLabel
                Left = 560
                Top = 72
                Width = 109
                Height = 13
                Caption = 'S09: Valor do COFINS:'
              end
              object EdTextoCOFINS_CST: TdmkEdit
                Left = 56
                Top = 32
                Width = 613
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryName = 'QrNFeItsS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCOFINS_CST: TdmkEdit
                Left = 8
                Top = 32
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '99'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '01'
                QryName = 'QrNFeItsS'
                QryCampo = 'COFINS_CST'
                UpdCampo = 'COFINS_CST'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnChange = EdCOFINS_CSTChange
                OnKeyDown = EdCOFINS_CSTKeyDown
              end
              object GroupBox27: TGroupBox
                Left = 8
                Top = 56
                Width = 268
                Height = 65
                Caption = ' Por Al'#237'quota: '
                TabOrder = 2
                object Label287: TLabel
                  Left = 8
                  Top = 16
                  Width = 84
                  Height = 13
                  Caption = 'S07: Valor da BC:'
                end
                object Label288: TLabel
                  Left = 136
                  Top = 16
                  Width = 107
                  Height = 13
                  Caption = 'S08: Aliq. COFINS (%):'
                end
                object EdCOFINS_vBC: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_vBC'
                  UpdCampo = 'COFINS_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINS_vBCChange
                end
                object EdCOFINS_pCOFINS: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_pCOFINS'
                  UpdCampo = 'COFINS_pCOFINS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINS_pCOFINSChange
                end
              end
              object GroupBox28: TGroupBox
                Left = 280
                Top = 56
                Width = 268
                Height = 65
                Caption = ' Por Unidade: '
                TabOrder = 3
                object Label289: TLabel
                  Left = 136
                  Top = 16
                  Width = 116
                  Height = 13
                  Caption = 'S11: % Aliq. COFINS ($):'
                end
                object Label290: TLabel
                  Left = 8
                  Top = 16
                  Width = 92
                  Height = 13
                  Caption = 'S10: Qtde vendida:'
                end
                object EdCOFINS_qBCProd: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_qBCProd'
                  UpdCampo = 'COFINS_qBCProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINS_qBCProdChange
                end
                object EdCOFINS_vAliqProd: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsS'
                  QryCampo = 'COFINS_vAliqProd'
                  UpdCampo = 'COFINS_vAliqProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINS_vAliqProdChange
                end
              end
              object EdCOFINS_vCOFINS: TdmkEdit
                Left = 560
                Top = 88
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsS'
                QryCampo = 'COFINS_vCOFINS'
                UpdCampo = 'COFINS_vCOFINS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object GroupBox29: TGroupBox
              Left = 0
              Top = 129
              Width = 1004
              Height = 226
              Align = alClient
              Caption = ' COFINS ST: '
              TabOrder = 1
              object Label291: TLabel
                Left = 560
                Top = 32
                Width = 109
                Height = 13
                Caption = 'T06: Valor do COFINS:'
              end
              object GroupBox30: TGroupBox
                Left = 8
                Top = 16
                Width = 268
                Height = 65
                Caption = ' Por Al'#237'quota: '
                TabOrder = 0
                object Label292: TLabel
                  Left = 8
                  Top = 16
                  Width = 84
                  Height = 13
                  Caption = 'T02: Valor da BC:'
                end
                object Label293: TLabel
                  Left = 136
                  Top = 16
                  Width = 107
                  Height = 13
                  Caption = 'T03: Aliq. COFINS (%):'
                end
                object EdCOFINSST_vBC: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsT'
                  QryCampo = 'COFINSST_vBC'
                  UpdCampo = 'COFINSST_vBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINSST_vBCChange
                end
                object EdCOFINSST_pCOFINS: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryName = 'QrNFeItsT'
                  QryCampo = 'COFINSST_pCOFINS'
                  UpdCampo = 'COFINSST_pCOFINS'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINSST_pCOFINSChange
                end
              end
              object GroupBox31: TGroupBox
                Left = 280
                Top = 16
                Width = 268
                Height = 65
                Caption = ' Por Unidade: '
                TabOrder = 1
                object Label294: TLabel
                  Left = 136
                  Top = 16
                  Width = 116
                  Height = 13
                  Caption = 'T05: % Aliq. COFINS ($):'
                end
                object Label295: TLabel
                  Left = 8
                  Top = 16
                  Width = 92
                  Height = 13
                  Caption = 'T04: Qtde vendida:'
                end
                object EdCOFINSST_qBCProd: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsT'
                  QryCampo = 'COFINSST_qBCProd'
                  UpdCampo = 'COFINSST_qBCProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINSST_qBCProdChange
                end
                object EdCOFINSST_vAliqProd: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 124
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryName = 'QrNFeItsT'
                  QryCampo = 'COFINSST_vAliqProd'
                  UpdCampo = 'COFINSST_vAliqProd'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdCOFINSST_vAliqProdChange
                end
              end
              object EdCOFINSST_vCOFINS: TdmkEdit
                Left = 560
                Top = 48
                Width = 104
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsT'
                QryCampo = 'COFINSST_vCOFINS'
                UpdCampo = 'COFINSST_vCOFINS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object RGCOFINSST_indSomaCOFINSST: TdmkRadioGroup
                Left = 676
                Top = 28
                Width = 305
                Height = 41
                Caption = ' 0=Valor do COFINSST comp'#245'e o valor total da NF-e: '
                Columns = 3
                ItemIndex = 2
                Items.Strings = (
                  '0=N'#227'o'
                  '1=Sim'
                  '?=N/D')
                TabOrder = 3
                OnClick = RGCOFINSST_indSomaCOFINSSTClick
                QryName = 'QrNFeItsT'
                QryCampo = 'COFINSST_indSomaCOFINSST'
                UpdCampo = 'COFINSST_indSomaCOFINSST'
                UpdType = utYes
                OldValor = 0
              end
            end
          end
        end
      end
      object TabSheet24: TTabSheet
        Caption = 'V01 -  Informa'#231#245'es adicionais do produto '
        ImageIndex = 8
        object Panel37: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 295
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object MeInfAdProd: TdmkMemo
            Left = 0
            Top = 17
            Width = 1006
            Height = 278
            Align = alClient
            TabOrder = 0
            OnKeyDown = MeInfAdProdKeyDown
            QryName = 'QrNFeItsV'
            QryCampo = 'InfAdProd'
            UpdCampo = 'InfAdProd'
            UpdType = utYes
          end
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1006
            Height = 17
            Align = alTop
            BevelOuter = bvNone
            Caption = 
              'Para iniciar nova linha mantenha pressionado a tecla Ctrl e tecl' +
              'e enter.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'ICMS Opera'#231#245'es Interestaduais Venda Consumidor Final'
        ImageIndex = 6
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 355
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox4: TGroupBox
            Left = 0
            Top = 0
            Width = 1004
            Height = 355
            Align = alClient
            TabOrder = 0
            object CkGeraGrupoNA: TCheckBox
              Left = 2
              Top = 15
              Width = 1000
              Height = 17
              Align = alTop
              Caption = 'Gera este grupo de informa'#231#245'es.'
              TabOrder = 0
              OnClick = CkGeraGrupoNAClick
            end
            object PnGrupoNA: TPanel
              Left = 2
              Top = 32
              Width = 1000
              Height = 321
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              Visible = False
              object Label9: TLabel
                Left = 4
                Top = 16
                Width = 187
                Height = 13
                Caption = 'Valor da BC do ICMS da UF de destino:'
              end
              object Label8: TLabel
                Left = 4
                Top = 40
                Width = 305
                Height = 13
                Caption = '% Adicional Fundo Combate Pobreza (FCP) UF destino (m'#225'x 2%):'
              end
              object Label1: TLabel
                Left = 4
                Top = 64
                Width = 170
                Height = 13
                Caption = '% Aliquota ICMS interno UF destino:'
              end
              object Label10: TLabel
                Left = 4
                Top = 88
                Width = 199
                Height = 13
                Caption = '% ICMS interestadual das UFs envolvidas:'
              end
              object LapICMSInterPart: TLabel
                Left = 5
                Top = 112
                Width = 155
                Height = 13
                Caption = '% ICMS partilha para UF destino:'
              end
              object Label11: TLabel
                Left = 4
                Top = 136
                Width = 261
                Height = 13
                Caption = 'Valor ICMS Fundo Combate Pobreza (FCP) UF Destino:'
              end
              object Label14: TLabel
                Left = 4
                Top = 160
                Width = 347
                Height = 13
                Caption = 
                  'Valor ICMS interestadual UF destino j'#225' considerado o valor do IC' +
                  'MS FCP:'
              end
              object Label15: TLabel
                Left = 4
                Top = 184
                Width = 186
                Height = 13
                Caption = 'Valor ICMS interestadual UF remetente:'
              end
              object Label42: TLabel
                Left = 4
                Top = 206
                Width = 187
                Height = 13
                Caption = 'Valor da BC do ICMS da UF de destino:'
              end
              object EdICMSUFDest_vBCUFDest: TdmkEdit
                Left = 364
                Top = 12
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_vBCUFDest'
                UpdCampo = 'ICMSUFDest_vBCUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdICMSUFDest_vBCUFDestChange
              end
              object EdICMSUFDest_pFCPUFDest: TdmkEdit
                Left = 364
                Top = 36
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_pFCPUFDest'
                UpdCampo = 'ICMSUFDest_pFCPUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdICMSUFDest_pFCPUFDestChange
              end
              object EdICMSUFDest_pICMSUFDest: TdmkEdit
                Left = 364
                Top = 60
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_pICMSUFDest'
                UpdCampo = 'ICMSUFDest_pICMSUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdICMSUFDest_pICMSUFDestChange
              end
              object EdICMSUFDest_pICMSInter: TdmkEdit
                Left = 364
                Top = 84
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_pICMSInter'
                UpdCampo = 'ICMSUFDest_pICMSInter'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdICMSUFDest_pICMSInterChange
              end
              object EdICMSUFDest_pICMSInterPart: TdmkEdit
                Left = 364
                Top = 108
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_pICMSInterPart'
                UpdCampo = 'ICMSUFDest_pICMSInterPart'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdICMSUFDest_pICMSInterPartChange
              end
              object EdICMSUFDest_vFCPUFDest: TdmkEdit
                Left = 364
                Top = 132
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_vFCPUFDest'
                UpdCampo = 'ICMSUFDest_vFCPUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMSUFDest_vICMSUFDest: TdmkEdit
                Left = 364
                Top = 156
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_vICMSUFDest'
                UpdCampo = 'ICMSUFDest_vICMSUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMSUFDest_vICMSUFRemet: TdmkEdit
                Left = 364
                Top = 180
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_vICMSUFRemet'
                UpdCampo = 'ICMSUFDest_vICMSUFRemet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMSUFDest_vBCFCPUFDest: TdmkEdit
                Left = 364
                Top = 204
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryName = 'QrNFeItsNA'
                QryCampo = 'ICMSUFDest_vBCFCPUFDest'
                UpdCampo = 'ICMSUFDest_vBCFCPUFDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 101
      Width = 1012
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label29: TLabel
        Left = 284
        Top = 5
        Width = 166
        Height = 13
        Caption = 'CNPJ do Fabricante da Mercadoria'
      end
      object Label30: TLabel
        Left = 207
        Top = 28
        Width = 239
        Height = 13
        Caption = 'C'#243'digo de Benef'#237'cio Fiscal na UF aplicado ao item'
      end
      object Label52: TLabel
        Left = 576
        Top = 4
        Width = 219
        Height = 13
        Caption = 'C'#243'd. Barras pr'#243'prio ou de terceiros (NFe I03a):'
      end
      object Label47: TLabel
        Left = 577
        Top = 26
        Width = 219
        Height = 13
        Caption = 'C'#243'd. Barras pr'#243'prio ou de terceiros (NFe I12a):'
      end
      object Ckprod_indTot: TdmkCheckBox
        Left = 5
        Top = 0
        Width = 275
        Height = 17
        Caption = 'O valor deste item comp'#245'e o valor total da NF-e.'
        TabOrder = 0
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_indTot'
        UpdCampo = 'prod_indTot'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object Ckprod_indEscala: TdmkCheckBox
        Left = 5
        Top = 20
        Width = 176
        Height = 17
        Caption = 'Indicador de Escala Relevante'
        TabOrder = 1
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_indEscala'
        UpdCampo = 'prod_indEscala'
        UpdType = utYes
        ValCheck = 'S'
        ValUncheck = 'N'
        OldValor = #0
      end
      object Edprod_CNPJFab: TdmkEdit
        Left = 464
        Top = 0
        Width = 104
        Height = 22
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtCPFJ_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_CNPJFab'
        UpdCampo = 'prod_CNPJFab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edprod_cBenef: TdmkEdit
        Left = 464
        Top = 23
        Width = 104
        Height = 22
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_cBenef'
        UpdCampo = 'prod_cBenef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edprod_cBarra: TdmkEdit
        Left = 816
        Top = 0
        Width = 193
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_cBarra'
        UpdCampo = 'prod_cBarra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edprod_cBarraTrib: TdmkEdit
        Left = 816
        Top = 22
        Width = 193
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_cBarraTrib'
        UpdCampo = 'prod_cBarraTrib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 529
      Width = 1012
      Height = 57
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 4
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 249
        Height = 57
        Align = alLeft
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 1
          Top = 1
          Width = 247
          Height = 55
          Align = alClient
          Caption = ' Pedido de compra: '
          TabOrder = 0
          object Panel13: TPanel
            Left = 2
            Top = 15
            Width = 243
            Height = 38
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label17: TLabel
              Left = 2
              Top = -2
              Width = 90
              Height = 13
              Caption = 'N'#250'mero do pedido:'
            end
            object Label18: TLabel
              Left = 158
              Top = -2
              Width = 73
              Height = 13
              Caption = 'Item do pedido:'
            end
            object Edprod_xPed: TdmkEdit
              Left = 2
              Top = 14
              Width = 151
              Height = 20
              MaxLength = 15
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsI'
              QryCampo = 'prod_xPed'
              UpdCampo = 'prod_xPed'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Edprod_nItemPed: TdmkEdit
              Left = 158
              Top = 14
              Width = 76
              Height = 20
              Alignment = taRightJustify
              MaxLength = 6
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryName = 'QrNFeItsI'
              QryCampo = 'prod_nItemPed'
              UpdCampo = 'prod_nItemPed'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
      object REWarning: TRichEdit
        Left = 249
        Top = 0
        Width = 763
        Height = 57
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          'ATEN'#199#195'O:'
          
            'Verifique os c'#225'lculos de impostos. Caso estajam errados contate ' +
            'seu contador e informe-os manualmente.'
          'Informe a DERMATEK a forma correta de calcul'#225'-los.!!!'
          
            'N26a - Aliq. sup. p/ CF = Al'#237'quota suportada pelo Consumidor Fin' +
            'al')
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1012
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 964
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 509
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 557
      Top = 0
      Width = 407
      Height = 48
      Align = alRight
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 403
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 634
    Width = 1012
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1008
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 520
        Top = 16
        Width = 107
        Height = 13
        Caption = #185': [F3] Pesquisa CFOP.'
      end
      object PnSaiDesis: TPanel
        Left = 864
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtCalcular: TBitBtn
        Tag = 254
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Calcular'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCalcularClick
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object CkCalcular: TCheckBox
        Left = 280
        Top = 16
        Width = 221
        Height = 17
        Caption = 'Informar campos calculados manualmente.'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = CkCalcularClick
      end
    end
  end
  object QrPrx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(nItem)  nItem'
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 20
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPrxnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  med.Sigla, ggx.Controle GraGruX, '
      '/*gg1.Nome NO_GG1, '
      'IF(gcc.PrintCor=1,gcc.Nome,"") NO_COR, '
      'IF(gtc.PrintTam=1,gti.Nome,"") NO_TAM, */'
      'CONCAT(gg1.Nome, '
      '  IF(gtc.PrintTam=1, CONCAT(" ", gti.Nome),""),'
      '  IF(gcc.PrintCor=1, CONCAT(" ", gcc.Nome),"")) NO_GGX,'
      'pgt.MadeBy, gg1.* '
      'FROM gragrux ggx'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruc'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      'ORDER BY NO_GGX')
    Left = 48
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
    end
    object QrGGXNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 67
    end
    object QrGGXNivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru1.Nivel3'
    end
    object QrGGXNivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru1.Nivel2'
    end
    object QrGGXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
    end
    object QrGGXCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
    end
    object QrGGXNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrGGXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrGGXGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Origin = 'gragru1.GraTamCad'
    end
    object QrGGXUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
    end
    object QrGGXCST_A: TSmallintField
      FieldName = 'CST_A'
      Origin = 'gragru1.CST_A'
    end
    object QrGGXCST_B: TSmallintField
      FieldName = 'CST_B'
      Origin = 'gragru1.CST_B'
    end
    object QrGGXNCM: TWideStringField
      FieldName = 'NCM'
      Origin = 'gragru1.NCM'
      Size = 10
    end
    object QrGGXPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'gragru1.Peso'
    end
    object QrGGXIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Origin = 'gragru1.IPI_Alq'
    end
    object QrGGXIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      Origin = 'gragru1.IPI_CST'
    end
    object QrGGXIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Origin = 'gragru1.IPI_cEnq'
      Size = 3
    end
    object QrGGXTipDimens: TSmallintField
      FieldName = 'TipDimens'
      Origin = 'gragru1.TipDimens'
    end
    object QrGGXPerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
      Origin = 'gragru1.PerCuztMin'
    end
    object QrGGXPerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
      Origin = 'gragru1.PerCuztMax'
    end
    object QrGGXMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
      Origin = 'gragru1.MedOrdem'
    end
    object QrGGXPartePrinc: TIntegerField
      FieldName = 'PartePrinc'
      Origin = 'gragru1.PartePrinc'
    end
    object QrGGXInfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Origin = 'gragru1.InfAdProd'
      Size = 255
    end
    object QrGGXSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'gragru1.SiglaCustm'
      Size = 15
    end
    object QrGGXLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gragru1.Lk'
    end
    object QrGGXDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gragru1.DataCad'
    end
    object QrGGXDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gragru1.DataAlt'
    end
    object QrGGXUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gragru1.UserCad'
    end
    object QrGGXUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gragru1.UserAlt'
    end
    object QrGGXAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gragru1.AlterWeb'
    end
    object QrGGXAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gragru1.Ativo'
    end
    object QrGGXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
      Origin = 'gragru1.HowBxaEstq'
    end
    object QrGGXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
      Origin = 'gragru1.GerBxaEstq'
    end
    object QrGGXPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Origin = 'gragru1.PIS_CST'
    end
    object QrGGXPIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
      Origin = 'gragru1.PIS_AlqP'
    end
    object QrGGXPIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
      Origin = 'gragru1.PIS_AlqV'
    end
    object QrGGXCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Origin = 'gragru1.COFINS_CST'
    end
    object QrGGXCOFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
      Origin = 'gragru1.COFINS_AlqP'
    end
    object QrGGXCOFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
      Origin = 'gragru1.COFINS_AlqV'
    end
    object QrGGXICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
      Origin = 'gragru1.ICMS_modBC'
    end
    object QrGGXICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
      Origin = 'gragru1.ICMS_modBCST'
    end
    object QrGGXICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      Origin = 'gragru1.ICMS_pRedBC'
    end
    object QrGGXICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      Origin = 'gragru1.ICMS_pRedBCST'
    end
    object QrGGXICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      Origin = 'gragru1.ICMS_pMVAST'
    end
    object QrGGXICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      Origin = 'gragru1.ICMS_pICMSST'
    end
    object QrGGXPISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
      Origin = 'gragru1.PISST_AlqP'
    end
    object QrGGXPISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
      Origin = 'gragru1.PISST_AlqV'
    end
    object QrGGXCOFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
      Origin = 'gragru1.COFINSST_AlqP'
    end
    object QrGGXCOFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
      Origin = 'gragru1.COFINSST_AlqV'
    end
    object QrGGXIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      Origin = 'gragru1.IPI_vUnid'
    end
    object QrGGXIPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
      Origin = 'gragru1.IPI_TpTrib'
    end
    object QrGGXICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
      Origin = 'gragru1.ICMS_Pauta'
    end
    object QrGGXICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
      Origin = 'gragru1.ICMS_MaxTab'
    end
    object QrGGXIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      Origin = 'gragru1.IPI_pIPI'
    end
    object QrGGXcGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Origin = 'gragru1.cGTIN_EAN'
      Size = 14
    end
    object QrGGXEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Origin = 'gragru1.EX_TIPI'
      Size = 3
    end
    object QrGGXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGGXMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrGGXUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
  end
  object QrLista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1 '
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 76
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
end
