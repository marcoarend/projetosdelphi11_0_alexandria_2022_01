unit NFeCabA_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGrid, Menus, dmkMemo, ComCtrls, dmkEditDateTimePicker, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, dmkImage, UnDmkEnums, dmkDBGridZTO,
  dmkCheckBox;

type
  TSubForm = (tsfNenhum=0, tsfDadosPagamento=1);
  TFmNFeCabA_0000 = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabALoteEnv: TIntegerField;
    QrNFeCabAversao: TFloatField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_cUF: TSmallintField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_indPag: TSmallintField;
    QrNFeCabAide_mod: TSmallintField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_dSaiEnt: TDateField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAide_cMunFG: TIntegerField;
    QrNFeCabAide_tpImp: TSmallintField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabAide_cDV: TSmallintField;
    QrNFeCabAide_tpAmb: TSmallintField;
    QrNFeCabAide_finNFe: TSmallintField;
    QrNFeCabAide_procEmi: TSmallintField;
    QrNFeCabAide_verProc: TWideStringField;
    QrNFeCabAemit_CNPJ: TWideStringField;
    QrNFeCabAemit_CPF: TWideStringField;
    QrNFeCabAemit_xNome: TWideStringField;
    QrNFeCabAemit_xFant: TWideStringField;
    QrNFeCabAemit_xLgr: TWideStringField;
    QrNFeCabAemit_nro: TWideStringField;
    QrNFeCabAemit_xCpl: TWideStringField;
    QrNFeCabAemit_xBairro: TWideStringField;
    QrNFeCabAemit_cMun: TIntegerField;
    QrNFeCabAemit_xMun: TWideStringField;
    QrNFeCabAemit_UF: TWideStringField;
    QrNFeCabAemit_CEP: TIntegerField;
    QrNFeCabAemit_cPais: TIntegerField;
    QrNFeCabAemit_xPais: TWideStringField;
    QrNFeCabAemit_fone: TWideStringField;
    QrNFeCabAemit_IE: TWideStringField;
    QrNFeCabAemit_IEST: TWideStringField;
    QrNFeCabAemit_IM: TWideStringField;
    QrNFeCabAemit_CNAE: TWideStringField;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    QrNFeCabAdest_xNome: TWideStringField;
    QrNFeCabAdest_xLgr: TWideStringField;
    QrNFeCabAdest_nro: TWideStringField;
    QrNFeCabAdest_xCpl: TWideStringField;
    QrNFeCabAdest_xBairro: TWideStringField;
    QrNFeCabAdest_cMun: TIntegerField;
    QrNFeCabAdest_xMun: TWideStringField;
    QrNFeCabAdest_UF: TWideStringField;
    QrNFeCabAdest_CEP: TWideStringField;
    QrNFeCabAdest_cPais: TIntegerField;
    QrNFeCabAdest_xPais: TWideStringField;
    QrNFeCabAdest_fone: TWideStringField;
    QrNFeCabAdest_IE: TWideStringField;
    QrNFeCabAdest_ISUF: TWideStringField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vBCST: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vII: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAISSQNtot_vServ: TFloatField;
    QrNFeCabAISSQNtot_vBC: TFloatField;
    QrNFeCabAISSQNtot_vISS: TFloatField;
    QrNFeCabAISSQNtot_vPIS: TFloatField;
    QrNFeCabAISSQNtot_vCOFINS: TFloatField;
    QrNFeCabARetTrib_vRetPIS: TFloatField;
    QrNFeCabARetTrib_vRetCOFINS: TFloatField;
    QrNFeCabARetTrib_vRetCSLL: TFloatField;
    QrNFeCabARetTrib_vBCIRRF: TFloatField;
    QrNFeCabARetTrib_vIRRF: TFloatField;
    QrNFeCabARetTrib_vBCRetPrev: TFloatField;
    QrNFeCabARetTrib_vRetPrev: TFloatField;
    QrNFeCabAModFrete: TSmallintField;
    QrNFeCabATransporta_CNPJ: TWideStringField;
    QrNFeCabATransporta_CPF: TWideStringField;
    QrNFeCabATransporta_XNome: TWideStringField;
    QrNFeCabATransporta_IE: TWideStringField;
    QrNFeCabATransporta_XEnder: TWideStringField;
    QrNFeCabATransporta_XMun: TWideStringField;
    QrNFeCabATransporta_UF: TWideStringField;
    QrNFeCabARetTransp_vServ: TFloatField;
    QrNFeCabARetTransp_vBCRet: TFloatField;
    QrNFeCabARetTransp_PICMSRet: TFloatField;
    QrNFeCabARetTransp_vICMSRet: TFloatField;
    QrNFeCabARetTransp_CFOP: TWideStringField;
    QrNFeCabARetTransp_CMunFG: TWideStringField;
    QrNFeCabAVeicTransp_Placa: TWideStringField;
    QrNFeCabAVeicTransp_UF: TWideStringField;
    QrNFeCabAVeicTransp_RNTC: TWideStringField;
    QrNFeCabACobr_Fat_nFat: TWideStringField;
    QrNFeCabACobr_Fat_vOrig: TFloatField;
    QrNFeCabACobr_Fat_vDesc: TFloatField;
    QrNFeCabACobr_Fat_vLiq: TFloatField;
    QrNFeCabAInfAdic_InfCpl: TWideMemoField;
    QrNFeCabAExporta_UFEmbarq: TWideStringField;
    QrNFeCabAExporta_XLocEmbarq: TWideStringField;
    QrNFeCabACompra_XNEmp: TWideStringField;
    QrNFeCabACompra_XPed: TWideStringField;
    QrNFeCabACompra_XCont: TWideStringField;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAinfProt_Id: TWideStringField;
    QrNFeCabAinfProt_tpAmb: TSmallintField;
    QrNFeCabAinfProt_verAplic: TWideStringField;
    QrNFeCabAinfProt_dhRecbto: TDateTimeField;
    QrNFeCabAinfProt_nProt: TWideStringField;
    QrNFeCabAinfProt_digVal: TWideStringField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_Id: TWideStringField;
    QrNFeCabAinfCanc_tpAmb: TSmallintField;
    QrNFeCabAinfCanc_verAplic: TWideStringField;
    QrNFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrNFeCabAinfCanc_nProt: TWideStringField;
    QrNFeCabAinfCanc_digVal: TWideStringField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfCanc_cJust: TIntegerField;
    QrNFeCabAinfCanc_xJust: TWideStringField;
    QrNFeCabA_Ativo_: TSmallintField;
    QrNFeCabALk: TIntegerField;
    QrNFeCabADataCad: TDateField;
    QrNFeCabADataAlt: TDateField;
    QrNFeCabAUserCad: TIntegerField;
    QrNFeCabAUserAlt: TIntegerField;
    QrNFeCabAAlterWeb: TSmallintField;
    QrNFeCabAAtivo: TSmallintField;
    QrNFeCabAide_indPag_TXT: TWideStringField;
    QrNFeCabAide_tpNF_TXT: TWideStringField;
    QrNFeCabAide_cMunFG_TXT: TWideStringField;
    QrNFeCabAide_tpImp_TXT: TWideStringField;
    QrNFeCabAide_tpEmis_TXT: TWideStringField;
    QrNFeCabAide_tpAmb_TXT: TWideStringField;
    QrNFeCabAide_finNFe_TXT: TWideStringField;
    QrNFeCabAide_procEmi_TXT: TWideStringField;
    QrNFeCabACNAE_TXT: TWideStringField;
    QrNFeCabAModFrete_TXT: TWideStringField;
    QrNFeCabAFisRegCad: TIntegerField;
    QrNFeCabANO_FisRegCad: TWideStringField;
    QrNFeCabANO_CARTEMISS: TWideStringField;
    QrNFeCabATabelaPrc: TIntegerField;
    QrNFeCabANO_TabelaPrc: TWideStringField;
    QrNFeCabACartEmiss: TIntegerField;
    QrNFeCabANO_CondicaoPG: TWideStringField;
    QrNFeCabACondicaoPg: TIntegerField;
    QrNFeCabATP_CART: TIntegerField;
    QrNFeCabAMedDDSimpl: TFloatField;
    QrNFeCabAMedDDReal: TFloatField;
    QrNFeCabAJurosMes: TFloatField;
    DsNFeCabA: TDataSource;
    QrNFeCabB: TmySQLQuery;
    QrNFeCabBFatID: TIntegerField;
    QrNFeCabBFatNum: TIntegerField;
    QrNFeCabBEmpresa: TIntegerField;
    QrNFeCabBControle: TIntegerField;
    QrNFeCabBrefNFe: TWideStringField;
    QrNFeCabBrefNF_cUF: TSmallintField;
    QrNFeCabBrefNF_AAMM: TIntegerField;
    QrNFeCabBrefNF_CNPJ: TWideStringField;
    QrNFeCabBrefNF_mod: TSmallintField;
    QrNFeCabBrefNF_serie: TIntegerField;
    QrNFeCabBrefNF_nNF: TIntegerField;
    DsNFeCabB: TDataSource;
    QrNFeCabF: TmySQLQuery;
    QrNFeCabFFatID: TIntegerField;
    QrNFeCabFFatNum: TIntegerField;
    QrNFeCabFEmpresa: TIntegerField;
    QrNFeCabFretirada_CNPJ: TWideStringField;
    QrNFeCabFretirada_xLgr: TWideStringField;
    QrNFeCabFretirada_nro: TWideStringField;
    QrNFeCabFretirada_xCpl: TWideStringField;
    QrNFeCabFretirada_xBairro: TWideStringField;
    QrNFeCabFretirada_cMun: TIntegerField;
    QrNFeCabFretirada_xMun: TWideStringField;
    QrNFeCabFretirada_UF: TWideStringField;
    DsNFeCabF: TDataSource;
    QrNFeCabG: TmySQLQuery;
    QrNFeCabGFatID: TIntegerField;
    QrNFeCabGFatNum: TIntegerField;
    QrNFeCabGEmpresa: TIntegerField;
    QrNFeCabGentrega_CNPJ: TWideStringField;
    QrNFeCabGentrega_xLgr: TWideStringField;
    QrNFeCabGentrega_nro: TWideStringField;
    QrNFeCabGentrega_xCpl: TWideStringField;
    QrNFeCabGentrega_xBairro: TWideStringField;
    QrNFeCabGentrega_cMun: TIntegerField;
    QrNFeCabGentrega_xMun: TWideStringField;
    QrNFeCabGentrega_UF: TWideStringField;
    DsNFeCabG: TDataSource;
    QrEntiDest: TmySQLQuery;
    QrEntiDestCodigo: TIntegerField;
    QrEntiDestNO_ENT: TWideStringField;
    DsEntiDest: TDataSource;
    QrNFeItsI: TmySQLQuery;
    QrNFeItsIFatID: TIntegerField;
    QrNFeItsIFatNum: TIntegerField;
    QrNFeItsIEmpresa: TIntegerField;
    QrNFeItsInItem: TIntegerField;
    QrNFeItsIprod_cProd: TWideStringField;
    QrNFeItsIprod_cEAN: TWideStringField;
    QrNFeItsIprod_xProd: TWideStringField;
    QrNFeItsIprod_NCM: TWideStringField;
    QrNFeItsIprod_EXTIPI: TWideStringField;
    QrNFeItsIprod_genero: TSmallintField;
    QrNFeItsIprod_CFOP: TIntegerField;
    QrNFeItsIprod_uCom: TWideStringField;
    QrNFeItsIprod_qCom: TFloatField;
    QrNFeItsIprod_vUnCom: TFloatField;
    QrNFeItsIprod_vProd: TFloatField;
    QrNFeItsIprod_cEANTrib: TWideStringField;
    QrNFeItsIprod_uTrib: TWideStringField;
    QrNFeItsIprod_qTrib: TFloatField;
    QrNFeItsIprod_vUnTrib: TFloatField;
    QrNFeItsIprod_vFrete: TFloatField;
    QrNFeItsIprod_vSeg: TFloatField;
    QrNFeItsIprod_vDesc: TFloatField;
    QrNFeItsITem_IPI: TSmallintField;
    QrNFeItsIInfAdCuztm: TIntegerField;
    QrNFeItsIEhServico: TIntegerField;
    QrNFeItsIEhServico_TXT: TWideStringField;
    DsNFeItsI: TDataSource;
    QrNfeItsIDI: TmySQLQuery;
    QrNfeItsIDIDI_nDI: TWideStringField;
    QrNfeItsIDIDI_dDI: TDateField;
    QrNfeItsIDIDI_xLocDesemb: TWideStringField;
    QrNfeItsIDIDI_UFDesemb: TWideStringField;
    QrNfeItsIDIDI_dDesemb: TDateField;
    QrNfeItsIDIDI_cExportador: TWideStringField;
    QrNfeItsIDIControle: TIntegerField;
    DsNfeItsIDI: TDataSource;
    QrNFeItsIDIa: TmySQLQuery;
    QrNFeItsIDIaAdi_nAdicao: TIntegerField;
    QrNFeItsIDIaAdi_cFabricante: TWideStringField;
    QrNFeItsIDIaAdi_vDescDI: TFloatField;
    QrNFeItsIDIaConta: TIntegerField;
    DsNfeItsIDIa: TDataSource;
    QrNFeItsN: TmySQLQuery;
    QrNFeItsNICMS_Orig: TSmallintField;
    QrNFeItsNICMS_CST: TSmallintField;
    QrNFeItsNICMS_modBC: TSmallintField;
    QrNFeItsNICMS_pRedBC: TFloatField;
    QrNFeItsNICMS_vBC: TFloatField;
    QrNFeItsNICMS_pICMS: TFloatField;
    QrNFeItsNICMS_vICMS: TFloatField;
    QrNFeItsNICMS_modBCST: TSmallintField;
    QrNFeItsNICMS_pMVAST: TFloatField;
    QrNFeItsNICMS_pRedBCST: TFloatField;
    QrNFeItsNICMS_vBCST: TFloatField;
    QrNFeItsNICMS_pICMSST: TFloatField;
    QrNFeItsNICMS_vICMSST: TFloatField;
    DsNFeItsN: TDataSource;
    QrNFeItsO: TmySQLQuery;
    QrNFeItsOIPI_clEnq: TWideStringField;
    QrNFeItsOIPI_CNPJProd: TWideStringField;
    QrNFeItsOIPI_cSelo: TWideStringField;
    QrNFeItsOIPI_qSelo: TFloatField;
    QrNFeItsOIPI_cEnq: TWideStringField;
    QrNFeItsOIPI_CST: TSmallintField;
    QrNFeItsOIPI_vBC: TFloatField;
    QrNFeItsOIPI_qUnid: TFloatField;
    QrNFeItsOIPI_vUnid: TFloatField;
    QrNFeItsOIPI_pIPI: TFloatField;
    QrNFeItsOIPI_vIPI: TFloatField;
    DsNFeItsO: TDataSource;
    QrNFeItsP: TmySQLQuery;
    QrNFeItsPII_vBC: TFloatField;
    QrNFeItsPII_vDespAdu: TFloatField;
    QrNFeItsPII_vII: TFloatField;
    QrNFeItsPII_vIOF: TFloatField;
    DsNFeItsP: TDataSource;
    QrNFeItsQ: TmySQLQuery;
    QrNFeItsQPIS_CST: TSmallintField;
    QrNFeItsQPIS_vBC: TFloatField;
    QrNFeItsQPIS_pPIS: TFloatField;
    QrNFeItsQPIS_vPIS: TFloatField;
    QrNFeItsQPIS_qBCProd: TFloatField;
    QrNFeItsQPIS_vAliqProd: TFloatField;
    DsNFeItsQ: TDataSource;
    QrNFeItsS: TmySQLQuery;
    QrNFeItsSCOFINS_CST: TSmallintField;
    QrNFeItsSCOFINS_vBC: TFloatField;
    QrNFeItsSCOFINS_pCOFINS: TFloatField;
    QrNFeItsSCOFINS_qBCProd: TFloatField;
    QrNFeItsSCOFINS_vAliqProd: TFloatField;
    QrNFeItsSCOFINS_vCOFINS: TFloatField;
    DsNFeItsS: TDataSource;
    QrNFeItsU: TmySQLQuery;
    QrNFeItsUISSQN_vBC: TFloatField;
    QrNFeItsUISSQN_vAliq: TFloatField;
    QrNFeItsUISSQN_vISSQN: TFloatField;
    QrNFeItsUISSQN_cMunFG: TIntegerField;
    QrNFeItsUNO_ISSQN_cMunFG: TWideStringField;
    QrNFeItsUNO_ISSQN_cListServ: TWideStringField;
    QrNFeItsV: TmySQLQuery;
    QrNFeItsVInfAdProd: TWideMemoField;
    QrNFeCabY: TmySQLQuery;
    QrNFeCabYControle: TIntegerField;
    QrNFeCabYnDup: TWideStringField;
    QrNFeCabYdVenc: TDateField;
    QrNFeCabYvDup: TFloatField;
    QrNFeCabYFatID: TIntegerField;
    QrNFeCabYFatNum: TIntegerField;
    QrNFeCabYEmpresa: TIntegerField;
    QrNFeCabYSub: TIntegerField;
    QrNFeCabYLancto: TIntegerField;
    PainelData: TPanel;
    Panel4: TPanel;
    PageControl1: TPageControl;
    TabSheet15: TTabSheet;
    Panel7: TPanel;
    Label01: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    Panel44: TPanel;
    Label321: TLabel;
    Label322: TLabel;
    Label323: TLabel;
    Label315: TLabel;
    DBEdit221: TDBEdit;
    DBEdit222: TDBEdit;
    DBEdit223: TDBEdit;
    DBEdit224: TDBEdit;
    DBEdit225: TDBEdit;
    DBEdit226: TDBEdit;
    DBEdit227: TDBEdit;
    DBEdit228: TDBEdit;
    DBEdit229: TDBEdit;
    Panel46: TPanel;
    TabSheet1: TTabSheet;
    Panel9: TPanel;
    Label45: TLabel;
    Label46: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label47: TLabel;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit55: TDBEdit;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label30: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label42: TLabel;
    Label37: TLabel;
    Label41: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label61: TLabel;
    DBEdit26: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit72: TDBEdit;
    TabSheet5: TTabSheet;
    Panel13: TPanel;
    TabSheet14: TTabSheet;
    DBGItsI: TDBGrid;
    PageControl3: TPageControl;
    TabSheet16: TTabSheet;
    Panel25: TPanel;
    Label231: TLabel;
    Label232: TLabel;
    Label234: TLabel;
    Label235: TLabel;
    Label236: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    Label249: TLabel;
    DBEdit142: TDBEdit;
    DBEdit143: TDBEdit;
    DBEdit145: TDBEdit;
    DBEdit146: TDBEdit;
    DBEdit147: TDBEdit;
    DBEdit148: TDBEdit;
    DBEdit149: TDBEdit;
    DBEdit158: TDBEdit;
    DBEdit159: TDBEdit;
    DBEdit160: TDBEdit;
    GroupBox11: TGroupBox;
    Label242: TLabel;
    Label241: TLabel;
    Label240: TLabel;
    Label239: TLabel;
    Label233: TLabel;
    DBEdit151: TDBEdit;
    DBEdit153: TDBEdit;
    DBEdit152: TDBEdit;
    DBEdit150: TDBEdit;
    DBEdit144: TDBEdit;
    GroupBox12: TGroupBox;
    Label253: TLabel;
    Label254: TLabel;
    Label255: TLabel;
    Label256: TLabel;
    DBEdit163: TDBEdit;
    DBEdit165: TDBEdit;
    DBEdit166: TDBEdit;
    DBEdit167: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    TabSheet17: TTabSheet;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    DBGNFeItsIDIa: TDBGrid;
    Panel29: TPanel;
    Panel30: TPanel;
    DBGNFeItsIDI: TDBGrid;
    TabSheet18: TTabSheet;
    Panel31: TPanel;
    GroupBox13: TGroupBox;
    Panel38: TPanel;
    Label244: TLabel;
    Label245: TLabel;
    EdCST_A: TDBEdit;
    EdTextoA: TdmkEdit;
    EdCST_B: TDBEdit;
    EdTextoB: TdmkEdit;
    TabSheet19: TTabSheet;
    Panel34: TPanel;
    Panel39: TPanel;
    GroupBox19: TGroupBox;
    Label265: TLabel;
    Label266: TLabel;
    Label267: TLabel;
    Label268: TLabel;
    Label273: TLabel;
    DBEdit173: TDBEdit;
    DBEdit174: TDBEdit;
    DBEdit175: TDBEdit;
    DBEdit176: TDBEdit;
    Panel40: TPanel;
    Label251: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    Label269: TLabel;
    Label270: TLabel;
    Label271: TLabel;
    Label272: TLabel;
    DBEdit171: TDBEdit;
    DBEdit157: TDBEdit;
    DBEdit172: TDBEdit;
    DBEdit177: TDBEdit;
    DBEdit178: TDBEdit;
    DBEdit179: TDBEdit;
    TabSheet20: TTabSheet;
    Panel33: TPanel;
    TabSheet21: TTabSheet;
    Panel32: TPanel;
    GroupBox20: TGroupBox;
    Label274: TLabel;
    Label281: TLabel;
    EdTextoPIS_CST: TdmkEdit;
    EdPIS_CST: TDBEdit;
    GroupBox22: TGroupBox;
    Label279: TLabel;
    Label276: TLabel;
    DBEdit184: TDBEdit;
    DBEdit180: TDBEdit;
    GroupBox23: TGroupBox;
    Label275: TLabel;
    Label282: TLabel;
    DBEdit187: TDBEdit;
    DBEdit181: TDBEdit;
    DBEdit186: TDBEdit;
    GroupBox21: TGroupBox;
    Label284: TLabel;
    GroupBox24: TGroupBox;
    Label277: TLabel;
    Label278: TLabel;
    DBEdit182: TDBEdit;
    DBEdit183: TDBEdit;
    GroupBox25: TGroupBox;
    Label280: TLabel;
    Label283: TLabel;
    DBEdit185: TDBEdit;
    DBEdit188: TDBEdit;
    DBEdit189: TDBEdit;
    TabSheet22: TTabSheet;
    Panel35: TPanel;
    Panel41: TPanel;
    GroupBox26: TGroupBox;
    Label285: TLabel;
    Label286: TLabel;
    EdTextoCOFINS_CST: TdmkEdit;
    EdCOFINS_CST: TDBEdit;
    GroupBox27: TGroupBox;
    Label287: TLabel;
    Label288: TLabel;
    DBEdit191: TDBEdit;
    DBEdit192: TDBEdit;
    GroupBox28: TGroupBox;
    Label289: TLabel;
    Label290: TLabel;
    DBEdit193: TDBEdit;
    DBEdit194: TDBEdit;
    DBEdit195: TDBEdit;
    GroupBox29: TGroupBox;
    Label291: TLabel;
    GroupBox30: TGroupBox;
    Label292: TLabel;
    Label293: TLabel;
    DBEdit196: TDBEdit;
    DBEdit197: TDBEdit;
    GroupBox31: TGroupBox;
    Label294: TLabel;
    Label295: TLabel;
    DBEdit198: TDBEdit;
    DBEdit199: TDBEdit;
    DBEdit200: TDBEdit;
    TabSheet23: TTabSheet;
    Panel36: TPanel;
    Label296: TLabel;
    Label297: TLabel;
    Label298: TLabel;
    Label299: TLabel;
    Label300: TLabel;
    DBEdit201: TDBEdit;
    DBEdit202: TDBEdit;
    DBEdit203: TDBEdit;
    DBEdit204: TDBEdit;
    DBEdit205: TDBEdit;
    DBEdit209: TDBEdit;
    DBEdit211: TDBEdit;
    TabSheet24: TTabSheet;
    Panel37: TPanel;
    DBMemo2: TDBMemo;
    TabSheet6: TTabSheet;
    Panel47: TPanel;
    Label102: TLabel;
    Label103: TLabel;
    GroupBox4: TGroupBox;
    Label104: TLabel;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    GroupBox5: TGroupBox;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    DBEdit117: TDBEdit;
    DBEdit118: TDBEdit;
    DBEdit119: TDBEdit;
    TabSheet11: TTabSheet;
    DBGNfeCabB: TDBGrid;
    TabSheet12: TTabSheet;
    StaticText2: TStaticText;
    Panel23: TPanel;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label218: TLabel;
    Label217: TLabel;
    Label219: TLabel;
    DBEdit120: TDBEdit;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    DBEdit123: TDBEdit;
    DBEdit124: TDBEdit;
    DBEdit125: TDBEdit;
    DBEdit126: TDBEdit;
    DBEdit127: TDBEdit;
    DBEdit128: TDBEdit;
    DBEdit129: TDBEdit;
    DBEdit130: TDBEdit;
    TabSheet13: TTabSheet;
    StaticText3: TStaticText;
    Panel24: TPanel;
    Label221: TLabel;
    Label222: TLabel;
    Label223: TLabel;
    Label224: TLabel;
    Label225: TLabel;
    Label226: TLabel;
    Label227: TLabel;
    DBEdit131: TDBEdit;
    DBEdit132: TDBEdit;
    DBEdit133: TDBEdit;
    DBEdit134: TDBEdit;
    DBEdit135: TDBEdit;
    DBEdit136: TDBEdit;
    DBEdit137: TDBEdit;
    DBEdit138: TDBEdit;
    TabSheet4: TTabSheet;
    Panel10: TPanel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    Panel12: TPanel;
    GroupBox1: TGroupBox;
    Label59: TLabel;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    GroupBox3: TGroupBox;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    DBEdit93: TDBEdit;
    DBEdit94: TDBEdit;
    DBEdit95: TDBEdit;
    Panel45: TPanel;
    Label325: TLabel;
    Label326: TLabel;
    Label327: TLabel;
    Label328: TLabel;
    Label329: TLabel;
    Label330: TLabel;
    Label331: TLabel;
    Label333: TLabel;
    Label324: TLabel;
    Label332: TLabel;
    DBEdit230: TDBEdit;
    DBEdit231: TDBEdit;
    DBEdit232: TDBEdit;
    DBEdit233: TDBEdit;
    DBEdit234: TDBEdit;
    DBEdit235: TDBEdit;
    DBEdit236: TDBEdit;
    DBEdit237: TDBEdit;
    DBEdit238: TDBEdit;
    DBEdit239: TDBEdit;
    TabSheet25: TTabSheet;
    Panel48: TPanel;
    Panel49: TPanel;
    Label308: TLabel;
    Label309: TLabel;
    Label310: TLabel;
    Label311: TLabel;
    DBEdit214: TDBEdit;
    DBEdit215: TDBEdit;
    DBEdit216: TDBEdit;
    DBEdit217: TDBEdit;
    DBGrid4: TDBGrid;
    Panel6: TPanel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label314: TLabel;
    Label3: TLabel;
    Label306: TLabel;
    Label307: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit210: TDBEdit;
    DBEdit212: TDBEdit;
    DBEdit213: TDBEdit;
    PainelEdit: TPanel;
    Panel14: TPanel;
    PageControl2: TPageControl;
    TabSheet26: TTabSheet;
    Panel43: TPanel;
    Label316: TLabel;
    Label318: TLabel;
    Label320: TLabel;
    SpeedButton13: TSpeedButton;
    SbRegrFiscal: TSpeedButton;
    BtTabelaPrc: TSpeedButton;
    BtCondicaoPG: TSpeedButton;
    Label317: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    Panel22: TPanel;
    Label193: TLabel;
    Label195: TLabel;
    Label196: TLabel;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    Label201: TLabel;
    Label202: TLabel;
    Label203: TLabel;
    Label204: TLabel;
    Label205: TLabel;
    Label206: TLabel;
    Label207: TLabel;
    Label208: TLabel;
    Label209: TLabel;
    Label210: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Edide_natOp: TdmkEdit;
    Edide_indPag: TdmkEdit;
    Edide_mod: TdmkEdit;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Edide_tpNF: TdmkEdit;
    Edide_cMunFG: TdmkEdit;
    Edide_tpImp: TdmkEdit;
    Edide_tpEmis: TdmkEdit;
    Edide_cDV: TdmkEdit;
    Edide_tpAmb: TdmkEdit;
    Edide_finNFe: TdmkEdit;
    Edide_procEmi: TdmkEdit;
    Edide_verProc: TdmkEdit;
    Edide_indPag_Txt: TdmkEdit;
    Edide_tpNF_TXT: TdmkEdit;
    Edide_cMunFG_TXT: TdmkEdit;
    Edide_TpImp_TXT: TdmkEdit;
    Edide_tpEmis_TXT: TdmkEdit;
    Edide_tpAmb_TXT: TdmkEdit;
    Edide_finNFe_TXT: TdmkEdit;
    Edide_procEmi_TXT: TdmkEdit;
    TPide_dEmi: TdmkEditDateTimePicker;
    TPide_dSaiEnt: TdmkEditDateTimePicker;
    Edide_CUF: TdmkEditCB;
    CBide_UF: TdmkDBLookupComboBox;
    Panel50: TPanel;
    TabSheet3: TTabSheet;
    Panel15: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label109: TLabel;
    Label110: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label119: TLabel;
    Eddest_CNPJ: TdmkEdit;
    Eddest_xNome: TdmkEdit;
    Eddest_xLgr: TdmkEdit;
    Eddest_nro: TdmkEdit;
    Eddest_xCpl: TdmkEdit;
    Eddest_xBairro: TdmkEdit;
    Eddest_cMun: TdmkEdit;
    Eddest_UF: TdmkEdit;
    Eddest_CEP: TdmkEdit;
    Eddest_cPais: TdmkEdit;
    Eddest_xMun: TdmkEdit;
    Eddest_CPF: TdmkEdit;
    Panel42: TPanel;
    Label305: TLabel;
    SbEntiDest: TSpeedButton;
    EdCodInfoDest: TdmkEditCB;
    CBCodInfoDest: TdmkDBLookupComboBox;
    TabSheet7: TTabSheet;
    Panel16: TPanel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    Edemit_CNPJ: TdmkEdit;
    Edemit_xNome: TdmkEdit;
    Edemit_xFant: TdmkEdit;
    Edemit_xLgr: TdmkEdit;
    Edemit_nro: TdmkEdit;
    Edemit_xCpl: TdmkEdit;
    Edemit_xBairro: TdmkEdit;
    Edemit_cMun: TdmkEdit;
    Edemit_UF: TdmkEdit;
    Edemit_CEP: TdmkEdit;
    Edemit_cPais: TdmkEdit;
    Edemit_fone: TdmkEdit;
    Edemit_IE: TdmkEdit;
    Edemit_IEST: TdmkEdit;
    Edemit_IM: TdmkEdit;
    Edemit_xMun: TdmkEdit;
    Edemit_xPais: TdmkEdit;
    Edemit_CPF: TdmkEdit;
    Edemit_CNAE: TdmkEditCB;
    CBemit_CNAE: TdmkDBLookupComboBox;
    TabSheet9: TTabSheet;
    Panel20: TPanel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    Label174: TLabel;
    Label175: TLabel;
    Label176: TLabel;
    Label177: TLabel;
    Label178: TLabel;
    SpeedButton8: TSpeedButton;
    EdModFrete: TdmkEdit;
    EdModFrete_TXT: TdmkEdit;
    Edtransporta_CNPJ: TdmkEdit;
    Edtransporta_CPF: TdmkEdit;
    Edtransporta_xNome: TdmkEdit;
    EdTransporta_IE: TdmkEdit;
    Transporta_XEnder: TdmkEdit;
    Transporta_XMun: TdmkEdit;
    EdTransporta_UF: TdmkEdit;
    EdRetTransp_vServ: TdmkEdit;
    EdRetTransp_vBCRet: TdmkEdit;
    EdRetTransp_PICMSRet: TdmkEdit;
    EdRetTransp_vICMSRet: TdmkEdit;
    EdRetTransp_CFOP: TdmkEdit;
    EdRetTransp_CMunFG: TdmkEdit;
    EdVeicTransp_Placa: TdmkEdit;
    EdVeicTransp_UF: TdmkEdit;
    EdVeicTransp_RNTC: TdmkEdit;
    EdRetTransp_CMunFG_TXT: TdmkEdit;
    TabSheet10: TTabSheet;
    Panel51: TPanel;
    Label179: TLabel;
    Label180: TLabel;
    GroupBox9: TGroupBox;
    Label181: TLabel;
    Label182: TLabel;
    dmkEdit115: TdmkEdit;
    dmkEdit116: TdmkEdit;
    GroupBox10: TGroupBox;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    EdCompra_XNEmp: TdmkEdit;
    EdCompra_XPed: TdmkEdit;
    EdCompra_XCont: TdmkEdit;
    TabSheet8: TTabSheet;
    Panel52: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    GroupBox6: TGroupBox;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    EdISSQNtot_vServ: TdmkEdit;
    EdISSQNtot_vBC: TdmkEdit;
    EdISSQNtot_vISS: TdmkEdit;
    EdISSQNtot_vPIS: TdmkEdit;
    EdISSQNtot_vCOFINS: TdmkEdit;
    Panel19: TPanel;
    GroupBox7: TGroupBox;
    Label142: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    Label146: TLabel;
    Label147: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    Label151: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label154: TLabel;
    Label155: TLabel;
    EdICMSTot_vBC: TdmkEdit;
    EdICMSTot_vICMS: TdmkEdit;
    EdICMSTot_vBCST: TdmkEdit;
    EdICMSTot_vST: TdmkEdit;
    EdICMSTot_vProd: TdmkEdit;
    EdICMSTot_vFrete: TdmkEdit;
    EdICMSTot_vSeg: TdmkEdit;
    EdICMSTot_vDesc: TdmkEdit;
    EdICMSTot_vII: TdmkEdit;
    EdICMSTot_vIPI: TdmkEdit;
    EdICMSTot_vPIS: TdmkEdit;
    EdICMSTot_vCOFINS: TdmkEdit;
    EdICMSTot_vOutro: TdmkEdit;
    EdICMSTot_vNF: TdmkEdit;
    GroupBox8: TGroupBox;
    Label156: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    dmkEdit89: TdmkEdit;
    dmkEdit90: TdmkEdit;
    dmkEdit91: TdmkEdit;
    dmkEdit92: TdmkEdit;
    dmkEdit93: TdmkEdit;
    dmkEdit94: TdmkEdit;
    dmkEdit95: TdmkEdit;
    Panel21: TPanel;
    Label186: TLabel;
    Label187: TLabel;
    Label188: TLabel;
    Label189: TLabel;
    Label190: TLabel;
    Label191: TLabel;
    Label192: TLabel;
    Label319: TLabel;
    Label194: TLabel;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdIDCtrl: TdmkEdit;
    EdLoteEnv: TdmkEdit;
    Edversao: TdmkEdit;
    EdNFe_Id: TdmkEdit;
    EdStatus: TdmkEdit;
    Edide_cNF: TdmkEdit;
    PMNFeCabA: TPopupMenu;
    IncluinovaNFe1: TMenuItem;
    AlteraNFeatual1: TMenuItem;
    ExcluiNFeatual1: TMenuItem;
    N1: TMenuItem;
    LocaldeRetirada1: TMenuItem;
    LocaldeEntrega1: TMenuItem;
    N4: TMenuItem;
    ReGeraarquivoXML1: TMenuItem;
    VuFisRegCad: TdmkValUsu;
    VuTabelaPrc: TdmkValUsu;
    VuCondicaoPG: TdmkValUsu;
    PMNFeCabB: TPopupMenu;
    IncluiNFreferenciada1: TMenuItem;
    Alterarefenciamentoatual1: TMenuItem;
    Excluireferenciamentos1: TMenuItem;
    PMNFeItsI_U: TPopupMenu;
    Incluinovoproduto1: TMenuItem;
    Alteraprodutoatual1: TMenuItem;
    N2: TMenuItem;
    IncluinovoServico1: TMenuItem;
    Alteraservicoatual1: TMenuItem;
    N3: TMenuItem;
    Excluiprodutoatual1: TMenuItem;
    PMNFeCabY: TPopupMenu;
    Incluinovacobrana1: TMenuItem;
    Alteracobranaatual1: TMenuItem;
    Excluicobranaatual1: TMenuItem;
    Incluialtera1: TMenuItem;
    Exclui1: TMenuItem;
    IncluiAltera2: TMenuItem;
    Exclui2: TMenuItem;
    REWarning: TRichEdit;
    DsNFeItsV: TDataSource;
    QrNFeCabAISS_Usa: TSmallintField;
    QrNFeCabAISS_Alq: TFloatField;
    DsNFeCabY: TDataSource;
    GroupBox32: TGroupBox;
    GroupBox33: TGroupBox;
    EdCOFINSRec_pRedBC: TdmkEdit;
    Label312: TLabel;
    Label313: TLabel;
    EdCOFINSRec_vBC: TdmkEdit;
    GroupBox34: TGroupBox;
    Label334: TLabel;
    Label335: TLabel;
    EdPISRec_pRedBC: TdmkEdit;
    EdPISRec_vBC: TdmkEdit;
    GroupBox35: TGroupBox;
    Label336: TLabel;
    Label337: TLabel;
    EdIPIRec_pRedBC: TdmkEdit;
    EdIPIRec_vBC: TdmkEdit;
    GroupBox36: TGroupBox;
    Label338: TLabel;
    Label339: TLabel;
    EdICMSRec_pRedBC: TdmkEdit;
    EdICMSRec_vBC: TdmkEdit;
    QrNFeCabAFreteExtra: TFloatField;
    QrNFeCabASegurExtra: TFloatField;
    QrNFeCabAICMSRec_pRedBC: TFloatField;
    QrNFeCabAICMSRec_vICMS: TFloatField;
    QrNFeCabAIPIRec_pRedBC: TFloatField;
    QrNFeCabAPISRec_pRedBC: TFloatField;
    QrNFeCabACOFINSRec_pRedBC: TFloatField;
    QrNFeCabAIPIRec_vIPI: TFloatField;
    QrNFeCabAPISRec_vPIS: TFloatField;
    QrNFeCabACOFINSRec_vCOFINS: TFloatField;
    EdICMSRec_vICMS: TdmkEdit;
    Label340: TLabel;
    EdIPIRec_vIPI: TdmkEdit;
    Label341: TLabel;
    Label342: TLabel;
    EdPISRec_vPIS: TdmkEdit;
    EdCOFINSRec_vCOFINS: TdmkEdit;
    Label343: TLabel;
    QrNFeCabAICMSRec_vBC: TFloatField;
    QrNFeCabAIPIRec_vBC: TFloatField;
    QrNFeCabAPISRec_vBC: TFloatField;
    QrNFeCabACOFINSRec_vBC: TFloatField;
    Label344: TLabel;
    EdICMSRec_pAliq: TdmkEdit;
    Label345: TLabel;
    EdIPIRec_pAliq: TdmkEdit;
    Label346: TLabel;
    EdPISRec_pAliq: TdmkEdit;
    Label347: TLabel;
    EdCOFINSRec_pAliq: TdmkEdit;
    QrNFeCabAICMSRec_pAliq: TFloatField;
    QrNFeCabAIPIRec_pAliq: TFloatField;
    QrNFeCabAPISRec_pAliq: TFloatField;
    QrNFeCabACOFINSRec_pAliq: TFloatField;
    TabSheet27: TTabSheet;
    GroupBox37: TGroupBox;
    Label348: TLabel;
    TPDataFiscal: TdmkEditDateTimePicker;
    TabSheet28: TTabSheet;
    Panel55: TPanel;
    QrNFeCabADataFiscal: TDateField;
    QrNFeCabASINTEGRA_ExpDeclNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpDeclDta: TDateField;
    QrNFeCabASINTEGRA_ExpNat: TWideStringField;
    QrNFeCabASINTEGRA_ExpRegNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpRegDta: TDateField;
    QrNFeCabASINTEGRA_ExpConhNum: TWideStringField;
    QrNFeCabASINTEGRA_ExpConhDta: TDateField;
    QrNFeCabASINTEGRA_ExpConhTip: TWideStringField;
    QrNFeCabASINTEGRA_ExpPais: TWideStringField;
    QrNFeCabASINTEGRA_ExpAverDta: TDateField;
    Panel56: TPanel;
    Label359: TLabel;
    SbEntiEmit: TSpeedButton;
    EdCodInfoEmit: TdmkEditCB;
    CBCodInfoEmit: TdmkDBLookupComboBox;
    DsEntiEmit: TDataSource;
    QrEntiEmit: TmySQLQuery;
    QrEntiEmitCodigo: TIntegerField;
    QrEntiEmitNO_ENT: TWideStringField;
    QrNFeCabACodInfoEmit: TIntegerField;
    QrNFeCabACodInfoDest: TIntegerField;
    QrNFeCabAprotNFe_versao: TFloatField;
    QrNFeCabAretCancNFe_versao: TFloatField;
    QrNFeCabAInfAdic_InfAdFisco: TWideMemoField;
    BtIncluiItsIDI: TBitBtn;
    BtAlteraItsIDI: TBitBtn;
    BtExcluiItsIDI: TBitBtn;
    BtIncluiItsIDIA: TBitBtn;
    BtAlteraItsIDIA: TBitBtn;
    BtExcluiItsIDIA: TBitBtn;
    QrNfeItsIDIFatID: TIntegerField;
    QrNfeItsIDIFatNum: TIntegerField;
    QrNfeItsIDIEmpresa: TIntegerField;
    QrNfeItsIDInItem: TIntegerField;
    QrNfeItsIDIaAdi_nSeqAdic: TIntegerField;
    Edide_hSaiEnt: TdmkEdit;
    Edemit_CRT: TdmkEdit;
    Label360: TLabel;
    Edemit_CRT_TXT: TdmkEdit;
    QrNFeCabAide_hSaiEnt: TTimeField;
    QrNFeCabAide_dhCont: TDateTimeField;
    QrNFeCabAide_xJust: TWideStringField;
    QrNFeCabAemit_CRT: TSmallintField;
    QrNFeCabAdest_email: TWideStringField;
    QrNFeCabAVagao: TWideStringField;
    QrNFeCabABalsa: TWideStringField;
    QrNFeCabACriAForca: TSmallintField;
    QrNFeCabACodInfoTrsp: TIntegerField;
    QrNFeCabAOrdemServ: TIntegerField;
    QrNFeCabASituacao: TSmallintField;
    Eddest_xPais: TdmkEdit;
    Label116: TLabel;
    Eddest_fone: TdmkEdit;
    Label117: TLabel;
    Eddest_IE: TdmkEdit;
    Label118: TLabel;
    Eddest_ISUF: TdmkEdit;
    Label361: TLabel;
    Eddest_email: TdmkEdit;
    QrNFeItsR: TmySQLQuery;
    DsNFeItsR: TDataSource;
    QrNFeItsT: TmySQLQuery;
    DsNFeItsT: TDataSource;
    QrNFeItsRPISST_vBC: TFloatField;
    QrNFeItsRPISST_pPIS: TFloatField;
    QrNFeItsRPISST_qBCProd: TFloatField;
    QrNFeItsRPISST_vAliqProd: TFloatField;
    QrNFeItsRPISST_vPIS: TFloatField;
    QrNFeItsRPISST_fatorBCST: TFloatField;
    QrNFeItsTCOFINSST_vBC: TFloatField;
    QrNFeItsTCOFINSST_pCOFINS: TFloatField;
    QrNFeItsTCOFINSST_qBCProd: TFloatField;
    QrNFeItsTCOFINSST_vAliqProd: TFloatField;
    QrNFeItsTCOFINSST_vCOFINS: TFloatField;
    QrNFeItsTCOFINSST_fatorBCST: TFloatField;
    QrNFeItsITem_II: TSmallintField;
    QrNFeItsIprod_vOutro: TFloatField;
    Panel57: TPanel;
    GroupBox18: TGroupBox;
    Label264: TLabel;
    EdIPI_CST: TDBEdit;
    EdTextoIPI_CST: TdmkEdit;
    GroupBox43: TGroupBox;
    DBCheckBox3: TDBCheckBox;
    GroupBox44: TGroupBox;
    Label301: TLabel;
    DBEdit190: TDBEdit;
    Label302: TLabel;
    DBEdit206: TDBEdit;
    Label303: TLabel;
    DBEdit207: TDBEdit;
    Label304: TLabel;
    DBEdit208: TDBEdit;
    GroupBox45: TGroupBox;
    DBCheckBox4: TDBCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel53: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtNFeCabA: TBitBtn;
    BtNFeCabB: TBitBtn;
    BtNFeItsI_U: TBitBtn;
    BtNFeCabY: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel58: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrNFeCabYFatParcela: TIntegerField;
    Exclui3: TMenuItem;
    Excluiitematualnoexcluifinanceiro1: TMenuItem;
    QrNFeItsUISSQN_cListServ: TWideStringField;
    QrNfeItsIDILk: TIntegerField;
    QrNfeItsIDIDataCad: TDateField;
    QrNfeItsIDIDataAlt: TDateField;
    QrNfeItsIDIUserCad: TIntegerField;
    QrNfeItsIDIUserAlt: TIntegerField;
    QrNfeItsIDIAlterWeb: TSmallintField;
    QrNfeItsIDIAtivo: TSmallintField;
    QrNfeItsIDIDI_tpViaTransp: TSmallintField;
    QrNfeItsIDIDI_vAFRMM: TFloatField;
    QrNfeItsIDIDI_tpIntermedio: TSmallintField;
    QrNfeItsIDIDI_CNPJ: TWideStringField;
    QrNfeItsIDIDI_UFTerceiro: TWideStringField;
    TabSheet29: TTabSheet;
    QrNFeItsINVE: TmySQLQuery;
    QrNFeItsINVEFatID: TIntegerField;
    QrNFeItsINVEFatNum: TIntegerField;
    QrNFeItsINVEEmpresa: TIntegerField;
    QrNFeItsINVEnItem: TIntegerField;
    QrNFeItsINVEControle: TIntegerField;
    QrNFeItsINVEImportacia: TIntegerField;
    QrNFeItsINVENVE: TWideStringField;
    QrNFeItsINVELk: TIntegerField;
    QrNFeItsINVEDataCad: TDateField;
    QrNFeItsINVEDataAlt: TDateField;
    QrNFeItsINVEUserCad: TIntegerField;
    QrNFeItsINVEUserAlt: TIntegerField;
    QrNFeItsINVEAlterWeb: TSmallintField;
    QrNFeItsINVEAtivo: TSmallintField;
    DsNFeItsINVE: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    BtiNVE_Inclui: TBitBtn;
    BtiNVE_Altera: TBitBtn;
    BtiNVE_Exclui: TBitBtn;
    QrNfeItsIDIaAdi_nDraw: TFloatField;
    TabSheet30: TTabSheet;
    Panel59: TPanel;
    BtNFeItsI03_Inclui: TBitBtn;
    BtNFeItsI03_Altera: TBitBtn;
    BtNFeItsI03_Exclui: TBitBtn;
    DBGNFeItsI03: TDBGrid;
    QrNFeItsI03: TmySQLQuery;
    QrNFeItsI03FatID: TIntegerField;
    QrNFeItsI03FatNum: TIntegerField;
    QrNFeItsI03Empresa: TIntegerField;
    QrNFeItsI03nItem: TIntegerField;
    QrNFeItsI03Controle: TIntegerField;
    QrNFeItsI03detExport_nDraw: TLargeintField;
    QrNFeItsI03detExport_nRE: TLargeintField;
    QrNFeItsI03detExport_chNFe: TWideStringField;
    QrNFeItsI03detExport_qExport: TFloatField;
    QrNFeItsI03Lk: TIntegerField;
    QrNFeItsI03DataCad: TDateField;
    QrNFeItsI03DataAlt: TDateField;
    QrNFeItsI03UserCad: TIntegerField;
    QrNFeItsI03UserAlt: TIntegerField;
    QrNFeItsI03AlterWeb: TSmallintField;
    QrNFeItsI03Ativo: TSmallintField;
    DsNFeItsI03: TDataSource;
    Edide_dhSaiEntTZD: TdmkEdit;
    Edide_dhEmiTZD: TdmkEdit;
    Edide_hEmi: TdmkEdit;
    Label1: TLabel;
    Edide_idDest: TdmkEdit;
    Edide_idDest_TXT: TdmkEdit;
    Label362: TLabel;
    Edide_indFinal: TdmkEdit;
    Edide_indFinal_TXT: TdmkEdit;
    Label363: TLabel;
    Edide_indPres: TdmkEdit;
    Edide_indPres_TXT: TdmkEdit;
    QrNFeCabAvTotTrib: TFloatField;
    QrNFeCabAide_dhEmi: TDateTimeField;
    QrNFeCabAide_dhEmiTZD: TFloatField;
    QrNFeCabAide_dhSaiEntTZD: TFloatField;
    QrNFeCabAide_dhSaiEnt: TDateTimeField;
    QrNFeCabAide_idDest: TSmallintField;
    QrNFeCabAide_indFinal: TSmallintField;
    QrNFeCabAide_indPres: TSmallintField;
    QrNFeCabAide_dhContTZD: TFloatField;
    QrNFeCabAdest_idEstrangeiro: TWideStringField;
    QrNFeCabAdest_indIEDest: TSmallintField;
    QrNFeCabAdest_IM: TWideStringField;
    QrNFeCabAICMSTot_vICMSDeson: TFloatField;
    QrNFeCabAEstrangDef: TSmallintField;
    QrNFeCabAide_hEmi: TTimeField;
    Eddest_idEstrangeiro: TdmkEdit;
    Label364: TLabel;
    Label365: TLabel;
    Eddest_indIEDest: TdmkEdit;
    Eddest_indIEDest_TXT: TdmkEdit;
    Panel54: TPanel;
    GroupBox38: TGroupBox;
    Label349: TLabel;
    Label350: TLabel;
    EdSINTEGRA_ExpDeclNum: TdmkEdit;
    TPSINTEGRA_ExpDeclDta: TdmkEditDateTimePicker;
    GroupBox39: TGroupBox;
    Label351: TLabel;
    Label352: TLabel;
    EdSINTEGRA_ExpRegNum: TdmkEdit;
    TPSINTEGRA_ExpRegDta: TdmkEditDateTimePicker;
    GroupBox40: TGroupBox;
    Label353: TLabel;
    Label354: TLabel;
    Label356: TLabel;
    EdSINTEGRA_ExpConhNum: TdmkEdit;
    TPSINTEGRA_ExpConhDta: TdmkEditDateTimePicker;
    EdSINTEGRA_ExpConhTip_TXT: TdmkEdit;
    EdSINTEGRA_ExpConhTip: TdmkEdit;
    GroupBox41: TGroupBox;
    Label355: TLabel;
    EdSINTEGRA_ExpNat: TdmkEdit;
    EdSINTEGRA_ExpNat_TXT: TdmkEdit;
    GroupBox42: TGroupBox;
    Label357: TLabel;
    Label358: TLabel;
    EdSINTEGRA_ExpPais: TdmkEdit;
    EdSINTEGRA_ExpPais_TXT: TdmkEdit;
    TPSINTEGRA_ExpAverDta: TdmkEditDateTimePicker;
    QrNFeCabGA: TmySQLQuery;
    QrNFeCabGAFatID: TIntegerField;
    QrNFeCabGAFatNum: TIntegerField;
    QrNFeCabGAEmpresa: TIntegerField;
    QrNFeCabGAControle: TIntegerField;
    QrNFeCabGAAddForma: TSmallintField;
    QrNFeCabGAAddIDCad: TSmallintField;
    QrNFeCabGATipo: TSmallintField;
    QrNFeCabGAautXML_CNPJ: TWideStringField;
    QrNFeCabGAautXML_CPF: TWideStringField;
    QrNFeCabGALk: TIntegerField;
    QrNFeCabGADataCad: TDateField;
    QrNFeCabGADataAlt: TDateField;
    QrNFeCabGAUserCad: TIntegerField;
    QrNFeCabGAUserAlt: TIntegerField;
    QrNFeCabGAAlterWeb: TSmallintField;
    QrNFeCabGAAtivo: TSmallintField;
    QrNFeCabGACNPJ_CPF: TWideStringField;
    QrNFeCabGAITEM: TIntegerField;
    QrNFeCabGATpDOC: TWideStringField;
    QrNFeCabGANO_AddForma: TWideStringField;
    QrNFeCabGANO_ENT_ECO: TWideStringField;
    DsNFeCabGA: TDataSource;
    TabSheet31: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel60: TPanel;
    BtNFeCabGA: TBitBtn;
    N5: TMenuItem;
    AlteraTributostotaisaproximados1: TMenuItem;
    QrNFeItsM: TmySQLQuery;
    DsNFeItsM: TDataSource;
    QrNFeItsMFatID: TIntegerField;
    QrNFeItsMFatNum: TIntegerField;
    QrNFeItsMEmpresa: TIntegerField;
    QrNFeItsMnItem: TIntegerField;
    QrNFeItsMiTotTrib: TSmallintField;
    QrNFeItsMvTotTrib: TFloatField;
    QrNFeItsMLk: TIntegerField;
    QrNFeItsMDataCad: TDateField;
    QrNFeItsMDataAlt: TDateField;
    QrNFeItsMUserCad: TIntegerField;
    QrNFeItsMUserAlt: TIntegerField;
    QrNFeItsMAlterWeb: TSmallintField;
    QrNFeItsMAtivo: TSmallintField;
    QrNFeItsMpTotTrib: TFloatField;
    QrNFeItsMtabTotTrib: TSmallintField;
    QrNFeItsMverTotTrib: TWideStringField;
    QrNFeItsMtpAliqTotTrib: TSmallintField;
    QrNFeItsMfontTotTrib: TSmallintField;
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrNFeCabYGenero: TIntegerField;
    QrNFeCabYDescricao: TWideStringField;
    CkEstrangDef: TdmkCheckBox;
    QrNFeItsNA: TmySQLQuery;
    QrNFeItsNAFatID: TIntegerField;
    QrNFeItsNAFatNum: TIntegerField;
    QrNFeItsNAEmpresa: TIntegerField;
    QrNFeItsNAnItem: TIntegerField;
    QrNFeItsNAICMSUFDest_vBCUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pFCPUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSInter: TFloatField;
    QrNFeItsNAICMSUFDest_pICMSInterPart: TFloatField;
    QrNFeItsNAICMSUFDest_vFCPUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_vICMSUFDest: TFloatField;
    QrNFeItsNAICMSUFDest_vICMSUFRemet: TFloatField;
    DsNFeItsNA: TDataSource;
    TabSheet32: TTabSheet;
    PnGrupoNA: TPanel;
    Label366: TLabel;
    Label367: TLabel;
    Label368: TLabel;
    Label369: TLabel;
    LapICMSInterPart: TLabel;
    Label370: TLabel;
    Label371: TLabel;
    Label372: TLabel;
    DBEdit218: TDBEdit;
    DBEdit219: TDBEdit;
    DBEdit220: TDBEdit;
    DBEdit240: TDBEdit;
    DBEdit241: TDBEdit;
    DBEdit242: TDBEdit;
    DBEdit243: TDBEdit;
    DBEdit244: TDBEdit;
    GroupBox46: TGroupBox;
    Label373: TLabel;
    Label374: TLabel;
    Label375: TLabel;
    EdICMSTot_vFCPUFDest: TdmkEdit;
    EdICMSTot_vICMSUFDest: TdmkEdit;
    EdICMSTot_vICMSUFRemet: TdmkEdit;
    QrNFeCabAICMSTot_vFCPUFDest: TFloatField;
    QrNFeCabAICMSTot_vICMSUFDest: TFloatField;
    QrNFeCabAICMSTot_vICMSUFRemet: TFloatField;
    EdDBide_indFinal: TDBEdit;
    EdDBide_indPres: TDBEdit;
    GroupBox47: TGroupBox;
    DBEdit247: TDBEdit;
    DBEdit248: TDBEdit;
    DBEdit249: TDBEdit;
    Label376: TLabel;
    Label377: TLabel;
    Label381: TLabel;
    EdDBide_idDest: TDBEdit;
    EdDBide_idDest_TXT: TdmkEdit;
    EdDBide_indFinal_TXT: TdmkEdit;
    EdDBide_indPres_TXT: TdmkEdit;
    QrDTB_UFs: TmySQLQuery;
    QrDTB_UFsDTB: TWideStringField;
    QrDTB_UFsNome: TWideStringField;
    DsDTB_UFs: TDataSource;
    QrNFeCabBLk: TIntegerField;
    QrNFeCabBDataCad: TDateField;
    QrNFeCabBDataAlt: TDateField;
    QrNFeCabBUserCad: TIntegerField;
    QrNFeCabBUserAlt: TIntegerField;
    QrNFeCabBAlterWeb: TSmallintField;
    QrNFeCabBAtivo: TSmallintField;
    QrNFeCabBrefNFP_cUF: TSmallintField;
    QrNFeCabBrefNFP_AAMM: TSmallintField;
    QrNFeCabBrefNFP_CNPJ: TWideStringField;
    QrNFeCabBrefNFP_CPF: TWideStringField;
    QrNFeCabBrefNFP_IE: TWideStringField;
    QrNFeCabBrefNFP_mod: TSmallintField;
    QrNFeCabBrefNFP_serie: TSmallintField;
    QrNFeCabBrefNFP_nNF: TIntegerField;
    QrNFeCabBrefCTe: TWideStringField;
    QrNFeCabBrefECF_mod: TWideStringField;
    QrNFeCabBrefECF_nECF: TSmallintField;
    QrNFeCabBrefECF_nCOO: TIntegerField;
    QrNFeCabBQualNFref: TSmallintField;
    Eddest_IM: TdmkEdit;
    Label382: TLabel;
    Label383: TLabel;
    Label384: TLabel;
    Label385: TLabel;
    Label386: TLabel;
    DBEdit245: TDBEdit;
    QrNFeCabFretirada_CPF: TWideStringField;
    QrNFeCabGentrega_CPF: TWideStringField;
    DBEdit246: TDBEdit;
    Label56: TLabel;
    Label57: TLabel;
    DBEdit250: TDBEdit;
    QrNFeItsIprod_indTot: TSmallintField;
    QrNFeItsIprod_CEST: TIntegerField;
    QrNFeItsIprod_xPed: TWideStringField;
    QrNFeItsIprod_nItemPed: TIntegerField;
    QrNFeItsIprod_nFCI: TWideStringField;
    Label58: TLabel;
    DBEdit251: TDBEdit;
    QrNFeItsNICMS_motDesICMS: TSmallintField;
    QrNFeItsNICMS_vICMSDeson: TFloatField;
    PageControl4: TPageControl;
    TabSheet33: TTabSheet;
    Panel61: TPanel;
    GroupBox14: TGroupBox;
    DBRadioGroup2: TDBRadioGroup;
    TabSheet34: TTabSheet;
    Panel63: TPanel;
    GroupBox15: TGroupBox;
    Label246: TLabel;
    Label250: TLabel;
    DBRadioGroup3: TDBRadioGroup;
    DBEdit155: TDBEdit;
    DBEdit156: TDBEdit;
    TabSheet35: TTabSheet;
    Panel64: TPanel;
    Label259: TLabel;
    DBEdit168: TDBEdit;
    Label260: TLabel;
    DBEdit169: TDBEdit;
    Label261: TLabel;
    DBEdit170: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    Label387: TLabel;
    DBEdit252: TDBEdit;
    QrNFeItsNICMS_vICMSSTRet: TFloatField;
    QrNFeItsNICMS_vBCSTRet: TFloatField;
    QrNFeItsNICMS_vICMSOp: TFloatField;
    QrNFeItsNICMS_pDif: TFloatField;
    QrNFeItsNICMS_vICMSDif: TFloatField;
    GroupBox16: TGroupBox;
    LaICMS_pRedBC: TLabel;
    LaICMS_VBC: TLabel;
    Label388: TLabel;
    LaICMS_pICMS: TLabel;
    DBEdit154: TDBEdit;
    DBEdit162: TDBEdit;
    DBEdit161: TDBEdit;
    DBEdit164: TDBEdit;
    GroupBox17: TGroupBox;
    Label389: TLabel;
    Label390: TLabel;
    Label391: TLabel;
    GroupBox48: TGroupBox;
    Label392: TLabel;
    Label393: TLabel;
    DBEdit253: TDBEdit;
    DBEdit254: TDBEdit;
    DBEdit255: TDBEdit;
    DBEdit256: TDBEdit;
    DBEdit257: TDBEdit;
    QrNFeItsNICMS_pCredSN: TFloatField;
    QrNFeItsNICMS_vCredICMSSN: TFloatField;
    QrNFeItsNICMS_CSOSN: TIntegerField;
    EdICMSTot_vICMSDeson: TdmkEdit;
    Label243: TLabel;
    EdIvTotTrib: TdmkEdit;
    Label252: TLabel;
    DBEdit258: TDBEdit;
    Label257: TLabel;
    Label258: TLabel;
    DBEdit259: TDBEdit;
    Label378: TLabel;
    Label379: TLabel;
    Label380: TLabel;
    Panel62: TPanel;
    Label86: TLabel;
    DBEdit96: TDBEdit;
    DBEdit97: TDBEdit;
    Label87: TLabel;
    DBEdit98: TDBEdit;
    DBEdit99: TDBEdit;
    Label89: TLabel;
    DBEdit100: TDBEdit;
    Label88: TLabel;
    DBEdit101: TDBEdit;
    Label90: TLabel;
    DBEdit102: TDBEdit;
    Label91: TLabel;
    DBEdit103: TDBEdit;
    Label92: TLabel;
    DBEdit104: TDBEdit;
    Label93: TLabel;
    Label94: TLabel;
    DBEdit105: TDBEdit;
    DBEdit106: TDBEdit;
    Label97: TLabel;
    DBEdit109: TDBEdit;
    DBEdit111: TDBEdit;
    Label99: TLabel;
    Label95: TLabel;
    DBEdit107: TDBEdit;
    Label100: TLabel;
    DBEdit112: TDBEdit;
    DBEdit113: TDBEdit;
    Label101: TLabel;
    DBEdit108: TDBEdit;
    Label96: TLabel;
    Label98: TLabel;
    DBEdit110: TDBEdit;
    Panel65: TPanel;
    GroupBox49: TGroupBox;
    Panel66: TPanel;
    BtNFeCabXReb: TBitBtn;
    DBGNFeCabXReb: TdmkDBGridZTO;
    GroupBox50: TGroupBox;
    Panel67: TPanel;
    BtNFeCabXVol: TBitBtn;
    DBGNFeCabXVol: TdmkDBGridZTO;
    GroupBox51: TGroupBox;
    Panel68: TPanel;
    BtNFeCabXLac: TBitBtn;
    DBGNFeCabXLac: TdmkDBGridZTO;
    Label394: TLabel;
    EdVagao: TdmkEdit;
    Label166: TLabel;
    EdBalsa: TdmkEdit;
    Label395: TLabel;
    DBEdit261: TDBEdit;
    DBEdit260: TDBEdit;
    Label396: TLabel;
    Label397: TLabel;
    QrNFeCabXReb: TmySQLQuery;
    QrNFeCabXRebplaca: TWideStringField;
    QrNFeCabXRebUF: TWideStringField;
    QrNFeCabXRebRNTC: TWideStringField;
    QrNFeCabXLac: TmySQLQuery;
    QrNFeCabXLacnLacre: TWideStringField;
    QrNFeCabXVol: TmySQLQuery;
    QrNFeCabXVolqVol: TFloatField;
    QrNFeCabXVolesp: TWideStringField;
    QrNFeCabXVolmarca: TWideStringField;
    QrNFeCabXVolnVol: TWideStringField;
    QrNFeCabXVolpesoL: TFloatField;
    QrNFeCabXVolpesoB: TFloatField;
    QrNFeCabXVolControle: TIntegerField;
    DsNFeCabXReb: TDataSource;
    DsNFeCabXLac: TDataSource;
    DsNFeCabXVol: TDataSource;
    PMNFeCabXReb: TPopupMenu;
    IncluiReboque1: TMenuItem;
    AlteraReboque1: TMenuItem;
    ExcluiReboque1: TMenuItem;
    QrNFeCabXRebFatID: TIntegerField;
    QrNFeCabXRebFatNum: TIntegerField;
    QrNFeCabXRebEmpresa: TIntegerField;
    QrNFeCabXRebControle: TIntegerField;
    QrNFeCabXLacFatID: TIntegerField;
    QrNFeCabXLacFatNum: TIntegerField;
    QrNFeCabXLacEmpresa: TIntegerField;
    QrNFeCabXLacControle: TIntegerField;
    QrNFeCabXLacConta: TIntegerField;
    QrNFeCabXVolFatID: TIntegerField;
    QrNFeCabXVolFatNum: TIntegerField;
    QrNFeCabXVolEmpresa: TIntegerField;
    QrNFeCabXVolSeqInArq: TIntegerField;
    PMNFeCabXVol: TPopupMenu;
    IncluiVolume1: TMenuItem;
    AlteraVolume1: TMenuItem;
    ExcluiVolume1: TMenuItem;
    PMNFeCabXLac: TPopupMenu;
    IncluiLacre1: TMenuItem;
    AlteraLacre1: TMenuItem;
    ExcluiLacre1: TMenuItem;
    TabSheet36: TTabSheet;
    Panel69: TPanel;
    Panel70: TPanel;
    Label398: TLabel;
    Label399: TLabel;
    Label400: TLabel;
    Label401: TLabel;
    EdCobr_Fat_nFat: TdmkEdit;
    EdCobr_Fat_vOrig: TdmkEdit;
    EdCobr_Fat_vLiq: TdmkEdit;
    EdCobr_Fat_vDesc: TdmkEdit;
    Panel71: TPanel;
    Panel72: TPanel;
    QrNFeCabZFis: TmySQLQuery;
    DsNFeCabZFis: TDataSource;
    PmNFeCabZCon: TPopupMenu;
    IncluiCampoLivre1: TMenuItem;
    AlteraCampoLivre1: TMenuItem;
    ExcluiCampoLivre1: TMenuItem;
    QrNFeCabZFisFatID: TIntegerField;
    QrNFeCabZFisFatNum: TIntegerField;
    QrNFeCabZFisEmpresa: TIntegerField;
    QrNFeCabZFisControle: TIntegerField;
    QrNFeCabZFisxCampo: TWideStringField;
    QrNFeCabZFisxTexto: TWideStringField;
    GroupBox53: TGroupBox;
    Panel74: TPanel;
    BtNFeCabZPro: TBitBtn;
    DBGNFeCabZPro: TDBGrid;
    Splitter1: TSplitter;
    GroupBox52: TGroupBox;
    Panel73: TPanel;
    BtNFeCabZCon: TBitBtn;
    DBGNFeCabZCon: TDBGrid;
    Splitter2: TSplitter;
    GroupBox54: TGroupBox;
    Panel75: TPanel;
    BtNFeCabZFis: TBitBtn;
    DBGNFeCabZFis: TDBGrid;
    PmNFeCabZFis: TPopupMenu;
    IncluiCampoLivre2: TMenuItem;
    AlteraCampoLivre2: TMenuItem;
    ExcluiCampoLivre2: TMenuItem;
    PmNFeCabZPro: TPopupMenu;
    IncluiProcessoouatoconcessrio1: TMenuItem;
    AlteraProcessoouatoconcessrio1: TMenuItem;
    ExcluiProcessoouatoconcessrio1: TMenuItem;
    QrNFeCabZCon: TmySQLQuery;
    QrNFeCabZConFatID: TIntegerField;
    QrNFeCabZConFatNum: TIntegerField;
    QrNFeCabZConEmpresa: TIntegerField;
    QrNFeCabZConControle: TIntegerField;
    QrNFeCabZConxCampo: TWideStringField;
    QrNFeCabZConxTexto: TWideStringField;
    DsNFeCabZCon: TDataSource;
    QrNFeCabZPro: TmySQLQuery;
    DsNFeCabZPro: TDataSource;
    QrNFeCabZProFatID: TIntegerField;
    QrNFeCabZProFatNum: TIntegerField;
    QrNFeCabZProEmpresa: TIntegerField;
    QrNFeCabZProControle: TIntegerField;
    QrNFeCabZPronProc: TWideStringField;
    QrNFeCabZProindProc: TSmallintField;
    Label402: TLabel;
    EdExporta_XLocDespacho: TdmkEdit;
    QrNFeCabAExporta_XLocDespacho: TWideStringField;
    DBEdit114: TDBEdit;
    Label105: TLabel;
    Label403: TLabel;
    N6: TMenuItem;
    Gerenciar1: TMenuItem;
    QrNFeItsIStqMovValA: TIntegerField;
    QrSUMNFeCabY: TmySQLQuery;
    QrSUMNFeCabYvDup: TFloatField;
    Label404: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    QrTransportas: TmySQLQuery;
    QrTransportasCodigo: TIntegerField;
    QrTransportasNOMEENT: TWideStringField;
    QrTransportasCIDADE: TWideStringField;
    QrTransportasNOMEUF: TWideStringField;
    DsTransportas: TDataSource;
    EdICMSTot_vFCP: TdmkEdit;
    Label405: TLabel;
    Label406: TLabel;
    DBEdit262: TDBEdit;
    QrNFeCabAICMSTot_vFCP: TFloatField;
    Label407: TLabel;
    EdICMSTot_vFCPST: TdmkEdit;
    QrNFeCabAICMSTot_vFCPST: TFloatField;
    Label408: TLabel;
    DBEdit263: TDBEdit;
    QrNFeCabAICMSTot_vFCPSTRet: TFloatField;
    Label409: TLabel;
    DBEdit264: TDBEdit;
    EdICMSTot_vFCPSTRet: TdmkEdit;
    Label410: TLabel;
    EdICMSTot_vIPIDevol: TdmkEdit;
    Label411: TLabel;
    Label412: TLabel;
    DBEdit265: TDBEdit;
    QrNFeCabAICMSTot_vIPIDevol: TFloatField;
    QrNFeItsNICMS_vBCFCP: TFloatField;
    QrNFeItsNICMS_pFCP: TFloatField;
    QrNFeItsNICMS_vFCP: TFloatField;
    QrNFeItsNICMS_pST: TFloatField;
    QrNFeItsNICMS_vBCFCPSTRet: TFloatField;
    QrNFeItsNICMS_pFCPSTRet: TFloatField;
    QrNFeItsNICMS_vFCPSTRet: TFloatField;
    QrNFeItsNICMS_vBCFCPST: TFloatField;
    QrNFeItsNICMS_pFCPST: TFloatField;
    QrNFeItsNICMS_vFCPST: TFloatField;
    QrNFeItsNAICMSUFDest_vBCFCPUFDest: TFloatField;
    QrNFeItsIprod_indEscala: TWideStringField;
    QrNFeItsIprod_CNPJFab: TWideStringField;
    QrNFeItsIprod_cBenef: TWideStringField;
    TabSheet37: TTabSheet;
    Panel76: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    DBGNFeCabYA: TDBGrid;
    QrNFeCabYA: TmySQLQuery;
    DsNFeCabYA: TDataSource;
    QrNFeCabYAFatID: TIntegerField;
    QrNFeCabYAFatNum: TIntegerField;
    QrNFeCabYAEmpresa: TIntegerField;
    QrNFeCabYAControle: TIntegerField;
    QrNFeCabYAtPag: TSmallintField;
    QrNFeCabYAvPag: TFloatField;
    QrNFeCabYAtpIntegra: TSmallintField;
    QrNFeCabYACNPJ: TWideStringField;
    QrNFeCabYAtBand: TSmallintField;
    QrNFeCabYAcAut: TWideStringField;
    QrNFeCabYAvTroco: TFloatField;
    QrNFeCabYAtpIntegra_TXT: TWideStringField;
    QrNFeCabYAtPag_TXT: TWideStringField;
    QrNFeCabYAtBand_TXT: TWideStringField;
    QrNFeCabYACNPJ_TXT: TWideStringField;
    QrNFeCabFretirada_xNome: TWideStringField;
    QrNFeCabFretirada_CEP: TIntegerField;
    QrNFeCabFretirada_cPais: TIntegerField;
    QrNFeCabFretirada_xPais: TWideStringField;
    QrNFeCabFretirada_fone: TWideStringField;
    QrNFeCabFretirada_email: TWideStringField;
    QrNFeCabFretirada_IE: TWideStringField;
    QrNFeCabGentrega_xNome: TWideStringField;
    QrNFeCabGentrega_CEP: TIntegerField;
    QrNFeCabGentrega_cPais: TIntegerField;
    QrNFeCabGentrega_xPais: TWideStringField;
    QrNFeCabGentrega_fone: TWideStringField;
    QrNFeCabGentrega_email: TWideStringField;
    QrNFeCabGentrega_IE: TWideStringField;
    DBEdit266: TDBEdit;
    Label413: TLabel;
    DBEdit267: TDBEdit;
    Label414: TLabel;
    Label415: TLabel;
    DBEdit268: TDBEdit;
    DBEdit269: TDBEdit;
    Label416: TLabel;
    Label220: TLabel;
    DBEdit139: TDBEdit;
    Label228: TLabel;
    DBEdit140: TDBEdit;
    Label229: TLabel;
    DBEdit141: TDBEdit;
    DBEdit270: TDBEdit;
    DBEdit271: TDBEdit;
    Label230: TLabel;
    Label417: TLabel;
    DBEdit272: TDBEdit;
    DBEdit273: TDBEdit;
    Label418: TLabel;
    MeDBInfAdic_InfAdFisco: TMemo;
    MeDBInfAdic_InfCpl: TMemo;
    MeInfAdic_InfCpl: TdmkMemo;
    MeInfAdic_InfAdFisco: TdmkMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeCabABeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure Incluinovoitemdeatributo1Click(Sender: TObject);
    procedure Alteraitemdeatributoatual1Click(Sender: TObject);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure BtNFeCabAClick(Sender: TObject);
    procedure IncluinovaNFe1Click(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure SbEntiDestClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Edide_cMunFGChange(Sender: TObject);
    procedure Edide_indPagChange(Sender: TObject);
    procedure Edide_tpNFChange(Sender: TObject);
    procedure Edide_tpImpChange(Sender: TObject);
    procedure Edide_tpAmbChange(Sender: TObject);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_procEmiChange(Sender: TObject);
    procedure Edide_tpEmisChange(Sender: TObject);
    procedure EdModFreteChange(Sender: TObject);
    procedure Eddest_UFChange(Sender: TObject);
    procedure Edemit_UFChange(Sender: TObject);
    procedure Edide_finNFe_TXTExit(Sender: TObject);
    procedure Eddest_ISUFExit(Sender: TObject);
    procedure EdRetTransp_CMunFG_TXTExit(Sender: TObject);
    procedure EdCompra_XContExit(Sender: TObject);
    procedure Edtransporta_CNPJChange(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure Edtransporta_CPFChange(Sender: TObject);
    procedure EdTransporta_UFChange(Sender: TObject);
    procedure Eddest_cMunChange(Sender: TObject);
    procedure Edemit_cMunChange(Sender: TObject);
    procedure Edemit_cPaisChange(Sender: TObject);
    procedure Eddest_cPaisChange(Sender: TObject);
    procedure BtNFeCabBClick(Sender: TObject);
    procedure IncluiNFreferenciada1Click(Sender: TObject);
    procedure BtNFeItsI_UClick(Sender: TObject);
    procedure BtNFeCabYClick(Sender: TObject);
    procedure AlteraNFeatual1Click(Sender: TObject);
    procedure PMNFeCabAPopup(Sender: TObject);
    procedure Edide_modChange(Sender: TObject);
    procedure Alterarefenciamentoatual1Click(Sender: TObject);
    procedure Excluireferenciamentos1Click(Sender: TObject);
    procedure PMNFeCabBPopup(Sender: TObject);
    procedure Incluialtera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure IncluiAltera2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure Incluinovoproduto1Click(Sender: TObject);
    procedure ExcluiNFeatual1Click(Sender: TObject);
    procedure Alteraprodutoatual1Click(Sender: TObject);
    procedure PMNFeItsI_UPopup(Sender: TObject);
    procedure Excluiprodutoatual1Click(Sender: TObject);
    procedure QrNFeItsIAfterScroll(DataSet: TDataSet);
    procedure QrNFeItsIBeforeClose(DataSet: TDataSet);
    procedure IncluinovoServico1Click(Sender: TObject);
    procedure Alteraservicoatual1Click(Sender: TObject);
    procedure Incluinovacobrana1Click(Sender: TObject);
    procedure Excluicobranaatual1Click(Sender: TObject);
    procedure ReGeraarquivoXML1Click(Sender: TObject);
    procedure EdICMSTot_vBCChange(Sender: TObject);
    procedure EdSINTEGRA_ExpNatChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdSINTEGRA_ExpConhTipChange(Sender: TObject);
    procedure EdSINTEGRA_ExpPaisChange(Sender: TObject);
    procedure EdCodInfoDestKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SbEntiEmitClick(Sender: TObject);
    procedure EdCodInfoEmitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdversaoChange(Sender: TObject);
    procedure BtIncluiItsIDIClick(Sender: TObject);
    procedure BtAlteraItsIDIClick(Sender: TObject);
    procedure BtExcluiItsIDIClick(Sender: TObject);
    procedure QrNFeItsIDIafterScroll(DataSet: TDataSet);
    procedure QrNFeItsIAfterOpen(DataSet: TDataSet);
    procedure QrNFeItsIDIafterOpen(DataSet: TDataSet);
    procedure QrNfeItsIDIBeforeClose(DataSet: TDataSet);
    procedure QrNFeItsIDIaBeforeClose(DataSet: TDataSet);
    procedure QrNFeItsIDIaAfterOpen(DataSet: TDataSet);
    procedure BtExcluiItsIDIAClick(Sender: TObject);
    procedure BtIncluiItsIDIAClick(Sender: TObject);
    procedure BtAlteraItsIDIAClick(Sender: TObject);
    procedure Edemit_CRTChange(Sender: TObject);
    procedure Edemit_CRTExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Alteracobranaatual1Click(Sender: TObject);
    procedure PMNFeCabYPopup(Sender: TObject);
    procedure Excluiitematualnoexcluifinanceiro1Click(Sender: TObject);
    procedure BtiNVE_IncluiClick(Sender: TObject);
    procedure QrNFeItsINVEAfterOpen(DataSet: TDataSet);
    procedure BtiNVE_ExcluiClick(Sender: TObject);
    procedure BtNFeItsI03_IncluiClick(Sender: TObject);
    procedure BtNFeItsI03_AlteraClick(Sender: TObject);
    procedure BtNFeItsI03_ExcluiClick(Sender: TObject);
    procedure QrNFeItsI03AfterOpen(DataSet: TDataSet);
    procedure Edide_idDestChange(Sender: TObject);
    procedure Edide_indFinalChange(Sender: TObject);
    procedure Edide_indPresChange(Sender: TObject);
    procedure Eddest_indIEDestChange(Sender: TObject);
    procedure QrNFeCabGACalcFields(DataSet: TDataSet);
    procedure BtNFeCabGAClick(Sender: TObject);
    procedure AlteraTributostotaisaproximados1Click(Sender: TObject);
    procedure Edide_indPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_indPagKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_tpNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_tpEmisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_tpAmbKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_idDestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_indFinalKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_AChange(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure EdDBide_idDestChange(Sender: TObject);
    procedure EdDBide_indFinalChange(Sender: TObject);
    procedure EdDBide_indPresChange(Sender: TObject);
    procedure QrNFeCabXVolBeforeClose(DataSet: TDataSet);
    procedure QrNFeCabXVolAfterScroll(DataSet: TDataSet);
    procedure BtNFeCabXRebClick(Sender: TObject);
    procedure IncluiReboque1Click(Sender: TObject);
    procedure AlteraReboque1Click(Sender: TObject);
    procedure ExcluiReboque1Click(Sender: TObject);
    procedure PMNFeCabXRebPopup(Sender: TObject);
    procedure IncluiVolume1Click(Sender: TObject);
    procedure AlteraVolume1Click(Sender: TObject);
    procedure ExcluiVolume1Click(Sender: TObject);
    procedure PMNFeCabXVolPopup(Sender: TObject);
    procedure BtNFeCabXVolClick(Sender: TObject);
    procedure BtNFeCabXLacClick(Sender: TObject);
    procedure IncluiLacre1Click(Sender: TObject);
    procedure AlteraLacre1Click(Sender: TObject);
    procedure ExcluiLacre1Click(Sender: TObject);
    procedure BtNFeCabZConClick(Sender: TObject);
    procedure PmNFeCabZConPopup(Sender: TObject);
    procedure IncluiCampoLivre1Click(Sender: TObject);
    procedure AlteraCampoLivre1Click(Sender: TObject);
    procedure IncluiCampoLivre2Click(Sender: TObject);
    procedure AlteraCampoLivre2Click(Sender: TObject);
    procedure ExcluiCampoLivre2Click(Sender: TObject);
    procedure ExcluiCampoLivre1Click(Sender: TObject);
    procedure PmNFeCabZFisPopup(Sender: TObject);
    procedure IncluiProcessoouatoconcessrio1Click(Sender: TObject);
    procedure AlteraProcessoouatoconcessrio1Click(Sender: TObject);
    procedure ExcluiProcessoouatoconcessrio1Click(Sender: TObject);
    procedure PmNFeCabZProPopup(Sender: TObject);
    procedure BtNFeCabZFisClick(Sender: TObject);
    procedure BtNFeCabZProClick(Sender: TObject);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure Gerenciar1Click(Sender: TObject);
    procedure QrNFeCabYACalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    F_indPag, F_tpNF, F_tpEmis, F_indPres, F_tpAmb, F_idDest, F_indFinal,
    F_finNFe, F_tPag, F_tpIntegra, F_tBand: MyArrayLista;
    FSoPermiteAlterarPelaSenhaAdmin: Boolean;
    FSoPermiteIncluirPelaSenhaAdmin: Boolean;
    FSINTEGRA_ExpNat, FSINTEGRA_ExpConhTip: MyArrayLista;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ObtemDadosDeCNPJCFP(DOC: String);
    procedure ObtemDadosDeTransportadora(Entidade: Integer; CopyDoc: Boolean);
    procedure CalculaRecuperacaoImpostos();
    procedure BuscaDadosDest();
    procedure BuscaDadosEmit();
    procedure GerenciaNFeCabY();
    procedure MostraSubForm();
    procedure IncluiPagto();
    procedure AlteraPagto();
    procedure ExcluiPagto();
  public
    { Public declarations }
    FForm: String;
    FMostraSubForm: TSubForm;
    function LocCod(Atual, Codigo: Integer): Boolean;
    procedure ReopenNFeCabB(Controle: Integer);
    procedure ReopenNFeCabF(Controle: Integer);
    procedure ReopenNFeCabG(Controle: Integer);
    procedure ReopenNFeCabGA(Controle: Integer);
    procedure ReopenNFeCabXLac(Conta: Integer);
    procedure ReopenNFeCabXReb(Controle: Integer);
    procedure ReopenNFeCabXVol(Controle: Integer);
    procedure ReopenNFeCabY(Controle: Integer);
    procedure ReopenNFeCabYA(Controle: Integer);
    procedure ReopenNFeCabZCon(Controle: Integer);
    procedure ReopenNFeCabZFis(Controle: Integer);
    procedure ReopenNFeCabZPro(Controle: Integer);
    procedure ReopenNFeItsI(Controle: Integer);
    procedure ReopenNFeItsINVE(Controle: Integer);
    procedure ReopenNFeItsIDI(Controle: Integer);
    procedure ReopenNFeItsIDIa(Conta: Integer);
    procedure ReopenNFeItsI03(Controle: Integer);
    procedure ReopenNFeItsN(nItem: Integer);
    procedure ReopenNFeItsNA(nItem: Integer);
    procedure ReopenNFeItsM(nItem: Integer);
    procedure ReopenNFeItsO(nItem: Integer);
    procedure ReopenNFeItsP(nItem: Integer);
    procedure ReopenNFeItsQ(nItem: Integer);
    procedure ReopenNFeItsR(nItem: Integer);
    procedure ReopenNFeItsS(nItem: Integer);
    procedure ReopenNFeItsT(nItem: Integer);
    procedure ReopenNFeItsU(nItem: Integer);
    procedure ReopenNFeItsV(nItem: Integer);
    procedure CalculaFaturasNFs();
  end;

var
  FmNFeCabA_0000: TFmNFeCabA_0000;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraAtrIts, MyDBCheck, ModuleGeral, ModPediVda, UnGrade_Jan,
  UnGFat_Jan, UnFinanceiroJan, UnPraz_PF, ModuleNFe_0000, CreateNFe,
  //{$IfNDef SemPediPrzCab2}PediPrzCab2, {$EndIf}
  NFeCabB_0000, NfeCabF_0000, NfeCabG_0000, NFeItsI_0000, NFeItsU_0000,
  NfeCabY_0000, NfeCabYA_0000, {$IFNDef semNFe_v0200} NFeGeraXML_0200, {$EndIF}
  (*NFeItsIDI_0000,*) NFeItsIDIa_0000, NFeCabA_0000_Pesq, UnFinanceiro,
  (*CashTabs,*) NFe_PF, NFeItsM_0310, NfeCabYAll_0000;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
function TFmNFeCabA_0000.LocCod(Atual, Codigo: Integer): Boolean;
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
  Result := QrNFeCabAIDCtrl.Value = Codigo;
end;

procedure TFmNFeCabA_0000.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeCabAIDCtrl.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeCabA_0000.DefParams;
begin
  VAR_GOTOTABELA := 'NFeCabA';
  VAR_GOTOMYSQLTABLE := QrNFeCabA;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'IDCtrl';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mun1.Nome ide_cMunFG_TXT,  cnan.Nome CNAE_TXT,');
  VAR_SQLx.Add('frc.Nome NO_FisRegCad, car.Nome NO_CARTEMISS,');
  VAR_SQLx.Add('car.Tipo TP_CART, tpc.Nome NO_TabelaPrc,');
  VAR_SQLx.Add('ppc.Nome NO_CondicaoPG, ppc.JurosMes,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal,');
  VAR_SQLx.Add('frc.ISS_Usa, frc.ISS_Alq, nfea.*');
  VAR_SQLx.Add('FROM nfecaba nfea');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21cad cnan ON cnan.CodAlf=nfea.emit_CNAE');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun1 ON mun1.Codigo=nfea.ide_cMunFG');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=nfea.CartEmiss');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=nfea.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=nfea.CondicaoPg');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE nfea.IDCtrl > -1000');
  VAR_SQLx.Add('AND nfea.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND nfea.IDCtrl=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmNFeCabA_0000.EdCompra_XContExit(Sender: TObject);
begin
  Pagecontrol2.ActivePageIndex := 5;
  EdICMSTot_vBC.SetFocus;
end;

procedure TFmNFeCabA_0000.Eddest_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Eddest_cMun.ValueVariant, []) then
    Eddest_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Eddest_xMun.Text := '';
end;

procedure TFmNFeCabA_0000.Eddest_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Eddest_cPais.ValueVariant, []) then
    Eddest_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value
  else
    Eddest_xPais.Text := '';
end;

procedure TFmNFeCabA_0000.Eddest_ISUFExit(Sender: TObject);
begin
  Pagecontrol2.ActivePageIndex := 3;
  EdModFrete.SetFocus;
end;

procedure TFmNFeCabA_0000.Eddest_UFChange(Sender: TObject);
begin
  Eddest_IE.LinkMsk := Eddest_UF.Text;
end;

procedure TFmNFeCabA_0000.Edemit_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edemit_cMun.ValueVariant, []) then
    Edemit_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edemit_xMun.Text := '';
end;

procedure TFmNFeCabA_0000.Edemit_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Edemit_cPais.ValueVariant, []) then
    Edemit_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value
  else
    Edemit_xPais.Text := '';
end;

procedure TFmNFeCabA_0000.Edemit_CRTChange(Sender: TObject);
begin
  Edemit_CRT_TXT.Text := UFinanceiro.CRT_Get(Edemit_CRT.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edemit_CRTExit(Sender: TObject);
begin
  Pagecontrol2.ActivePageIndex := 2;
  EdCodInfoDest.SetFocus;
end;

procedure TFmNFeCabA_0000.Edemit_UFChange(Sender: TObject);
begin
  Edemit_IE.LinkMsk := Edemit_UF.Text;
  Edemit_IEST.LinkMsk := Edemit_UF.Text;
end;

procedure TFmNFeCabA_0000.EdCodInfoDestKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BuscaDadosDest();
end;

procedure TFmNFeCabA_0000.EdCodInfoEmitKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BuscaDadosEmit();
end;

procedure TFmNFeCabA_0000.EdCOFINS_CSTChange(Sender: TObject);
begin
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(Geral.IMV(EdCOFINS_CST.Text));
end;

procedure TFmNFeCabA_0000.EdCST_AChange(Sender: TObject);
begin
  EdTextoA.Text := UFinanceiro.CST_A_Get(Geral.IMV(EdCST_A.Text));
end;

procedure TFmNFeCabA_0000.EdCST_BChange(Sender: TObject);
begin
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
end;

procedure TFmNFeCabA_0000.EdSINTEGRA_ExpConhTipChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdSINTEGRA_ExpConhTip.Text, FSINTEGRA_ExpConhTip, Texto, 0, 1);
  EdSINTEGRA_ExpConhTip_TXT.Text := Texto;
end;

procedure TFmNFeCabA_0000.EdSINTEGRA_ExpNatChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdSINTEGRA_ExpNat.Text, FSINTEGRA_ExpNat, Texto, 0, 1);
  EdSINTEGRA_ExpNat_TXT.Text := Texto;
end;

procedure TFmNFeCabA_0000.EdSINTEGRA_ExpPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', EdSINTEGRA_ExpPais.ValueVariant, []) then
    EdSINTEGRA_ExpPais_TXT.Text := DmNFe_0000.QrBACEN_PaisNome.Value
  else
    EdSINTEGRA_ExpPais_TXT.Text := '';
end;

procedure TFmNFeCabA_0000.EdICMSTot_vBCChange(Sender: TObject);
begin
  CalculaRecuperacaoImpostos();
end;

procedure TFmNFeCabA_0000.Edide_idDestChange(Sender: TObject);
begin
  Edide_idDest_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_idDest, Edide_idDest.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_idDestKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_idDest.Text := Geral.SelecionaItem(F_idDest, 0,
      'SEL-LISTA-000 :: Identifica��o do local de destino da opera��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_cMunFGChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edide_cMunFG.ValueVariant, []) then
    Edide_cMunFG_TXT.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edide_cMunFG_TXT.Text := '';
end;

procedure TFmNFeCabA_0000.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text  :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_finNFe_TXTExit(Sender: TObject);
begin
  Pagecontrol2.ActivePageIndex := 1;
  EdCodInfoEmit.SetFocus;
end;

procedure TFmNFeCabA_0000.Edide_indFinalChange(Sender: TObject);
begin
  Edide_indFinal_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indFinal, Edide_indFinal.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_indFinalKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_indFinal.Text := Geral.SelecionaItem(F_indFinal, 0,
      'SEL-LISTA-000 :: Opera��o com consumidor final',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_indPagChange(Sender: TObject);
begin
  Edide_indPag_TXT.Text  :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPag,  Edide_indPag.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_indPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_indPag.Text := Geral.SelecionaItem(F_indPag, 0,
      'SEL-LISTA-000 :: Indicador de forma de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_indPresChange(Sender: TObject);
begin
  Edide_indPres_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPres, Edide_indPres.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_indPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_indPres.Text := Geral.SelecionaItem(F_indPres, 0,
      'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_modChange(Sender: TObject);
begin
  if Edide_mod.ValueVariant = 0 then
    Edide_mod.ValueVariant := '55';
end;

procedure TFmNFeCabA_0000.Edide_procEmiChange(Sender: TObject);
begin
  Edide_procEmi_TXT.Text :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_procEmi, Edide_procEmi.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_tpAmbChange(Sender: TObject);
begin
  Edide_tpAmb_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, Edide_tpAmb.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_tpAmbKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_tpAmb.Text := Geral.SelecionaItem(F_tpAmb, 0,
      'SEL-LISTA-000 :: Identifica��o do ambiente',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_tpEmisChange(Sender: TObject);
begin
  Edide_tpEmis_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpEmis, Edide_tpEmis.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_tpEmisKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_tpEmis.Text := Geral.SelecionaItem(F_tpEmis, 0,
      'SEL-LISTA-000 :: Tipo de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.Edide_tpImpChange(Sender: TObject);
begin
  Edide_tpImp_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpImp, Edide_tpImp.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_tpNFChange(Sender: TObject);
begin
  Edide_tpNF_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF, Edide_tpNF.ValueVariant);
end;

procedure TFmNFeCabA_0000.Edide_tpNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_tpNF.Text := Geral.SelecionaItem(F_tpNF, 0,
      'SEL-LISTA-000 :: Tipo do doc. fiscal',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabA_0000.EdIPI_CSTChange(Sender: TObject);
begin
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(Geral.IMV(EdIPI_CST.Text));
end;

procedure TFmNFeCabA_0000.Eddest_indIEDestChange(Sender: TObject);
begin
  Eddest_indIEDest_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTdest_indIEDest, Eddest_indIEDest.ValueVariant);
end;

procedure TFmNFeCabA_0000.EdModFreteChange(Sender: TObject);
begin
  EdModFrete_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete, EdModFrete.ValueVariant);
end;

procedure TFmNFeCabA_0000.EdPIS_CSTChange(Sender: TObject);
begin
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(Geral.IMV(EdPIS_CST.Text));
end;

procedure TFmNFeCabA_0000.EdRetTransp_CMunFG_TXTExit(Sender: TObject);
begin
  Pagecontrol2.ActivePageIndex := 4;
  MeInfAdic_InfAdFisco.SetFocus;
end;

procedure TFmNFeCabA_0000.Edtransporta_CNPJChange(Sender: TObject);
begin
  ObtemDadosDeCNPJCFP(Edtransporta_CNPJ.Text);
end;

procedure TFmNFeCabA_0000.Edtransporta_CPFChange(Sender: TObject);
begin
  ObtemDadosDeCNPJCFP(Edtransporta_CPF.Text);
end;

procedure TFmNFeCabA_0000.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    {
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    }
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeCabA_0000.MostraSubForm;
begin
  case FMostraSubForm of
    tsfNenhum:
      ;
    tsfDadosPagamento:
    begin
      PageControl1.ActivePageIndex := 12;
      //
      if (QrNFeCabYA.State = dsInactive) or (QrNFeCabYA.RecordCount = 0) then
        IncluiPagto();
    end;
  end;
end;

procedure TFmNFeCabA_0000.ObtemDadosDeCNPJCFP(DOC: String);
var
  //Txt:String;
  //T,
  Entidade: Integer;
begin
  if DModG.ObtemEntidadeDeCNPJCFP(DOC, Entidade) then
    ObtemDadosDeTransportadora(Entidade, False);
end;

procedure TFmNFeCabA_0000.ObtemDadosDeTransportadora(Entidade: Integer; CopyDoc: Boolean);
begin
  DmNFe_0000.ReopenTransporta(Entidade);
  if CopyDoc then
  begin
    Edtransporta_CNPJ    .ValueVariant := DmNFe_0000.QrTransportaCNPJ.Value;
    Edtransporta_CPF     .ValueVariant := DmNFe_0000.QrTransportaCPF.Value;
  end;
  Edtransporta_xNome     .ValueVariant := DmNFe_0000.QrTransportaNO_ENT.Value;
  Transporta_XEnder      .ValueVariant := DmNFe_0000.QrTransportaENDERECO.Value;
  Transporta_XMun        .ValueVariant := DmNFe_0000.QrTransportaNO_Munici.Value;
  EdTransporta_UF        .ValueVariant := DmNFe_0000.QrTransportaNO_UF.Value;
  EdRetTransp_CMunFG     .ValueVariant := DmNFe_0000.QrTransportaCodMunici.Value;
  EdRetTransp_CMunFG_TXT .ValueVariant := DmNFe_0000.QrTransportaNO_Munici.Value;
  // Deve ser depois da UF
  EdTransporta_IE        .ValueVariant := DmNFe_0000.QrTransportaIE.Value;
end;

procedure TFmNFeCabA_0000.PMAtributosPopup(Sender: TObject);
begin
{
  Alteraatributoatual1.Enabled :=
    (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
}
end;

procedure TFmNFeCabA_0000.PMItensPopup(Sender: TObject);
begin
{
  Incluinovoitemdeatributo1.Enabled := (QrNFeCabA.State <> dsInactive) and
    (QrNFeCabA.RecordCount >0);
  Alteraitemdeatributoatual1.Enabled := (QrGraAtrIts.State <> dsInactive) and
    (QrGraAtrIts.RecordCount >0);
}
end;

procedure TFmNFeCabA_0000.PMNFeCabAPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
    and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluinovaNFe1.Enabled    := False; //N�o permite criar novas NFe's
  AlteraNFeatual1.Enabled   := Habilita;
  ExcluiNFeatual1.Enabled   := False; //N�o permite excluir por aqui
  LocaldeRetirada1.Enabled  := Habilita;
  LocaldeEntrega1.Enabled   := Habilita;
  ReGeraarquivoXML1.Enabled := Habilita and (QrNFeCabAStatus.Value < DmNFe_0000.stepNFeAdedLote());
end;

procedure TFmNFeCabA_0000.PMNFeCabBPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabB.State <> dsInactive) and (QrNFeCabB.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiNFreferenciada1.Enabled     := Habilita;
  Alterarefenciamentoatual1.Enabled := Habilita and Habilita2;
  Excluireferenciamentos1.Enabled   := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PMNFeCabXVolPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabXVol.State <> dsInactive) and (QrNFeCabXVol.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiVolume1.Enabled := Habilita and (QrNFeCabXVol.RecordCount > 0);
  AlteraVolume1.Enabled := Habilita and Habilita2;
  ExcluiVolume1.Enabled := Habilita and Habilita2 and (QrNFeCabXLac.RecordCount = 0);
end;

procedure TFmNFeCabA_0000.PMNFeCabXRebPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabXReb.State <> dsInactive) and (QrNFeCabXReb.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiReboque1.Enabled := Habilita;
  AlteraReboque1.Enabled := Habilita and Habilita2;
  ExcluiReboque1.Enabled := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PMNFeCabYPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabY.State <> dsInactive) and (QrNFeCabY.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  Incluinovacobrana1.Enabled  := Habilita;
  Alteracobranaatual1.Enabled := Habilita and Habilita2;
  Exclui3.Enabled             := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PmNFeCabZConPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabZCon.State <> dsInactive) and (QrNFeCabZCon.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiCampoLivre1.Enabled := Habilita;
  AlteraCampoLivre1.Enabled := Habilita and Habilita2;
  ExcluiCampoLivre1.Enabled := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PmNFeCabZFisPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabZFis.State <> dsInactive) and (QrNFeCabZFis.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiCampoLivre2.Enabled := Habilita;
  AlteraCampoLivre2.Enabled := Habilita and Habilita2;
  ExcluiCampoLivre2.Enabled := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PmNFeCabZProPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeCabZPro.State <> dsInactive) and (QrNFeCabZPro.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  IncluiProcessoouatoconcessrio1.Enabled := Habilita;
  AlteraProcessoouatoconcessrio1.Enabled := Habilita and Habilita2;
  ExcluiProcessoouatoconcessrio1.Enabled := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.PMNFeItsI_UPopup(Sender: TObject);
var
  Habilita, Habilita2: Boolean;
begin
  Habilita  := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0)
                 and (DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value));
  Habilita2 := (QrNFeItsI.State <> dsInactive) and (QrNFeItsI.RecordCount > 0);
  //
  FSoPermiteAlterarPelaSenhaAdmin := not Habilita;
  //
  Incluinovoproduto1.Enabled               := Habilita;
  Alteraprodutoatual1.Enabled              := Habilita and Habilita2;
  //N�o implementado
  IncluinovoServico1.Enabled               := False;
  Alteraservicoatual1.Enabled              := False;
  //
  Excluiprodutoatual1.Enabled              := Habilita and Habilita2;
  AlteraTributostotaisaproximados1.Enabled := Habilita and Habilita2;
end;

procedure TFmNFeCabA_0000.CalculaFaturasNFs;
const
  Con_MsgErro = 'Falha ao atualizar as faturas!' + sLineBreak +
                  'As faturas dever�o ser atualizadas manualmente!';
var
  Empresa, Filial, FatID, FatNum, EMP_CtaFaturas, Associada, ASS_CtaFaturas,
  ASS_FILIAL, Financeiro, CartEmis, CondicaoPG, AFP_Sit, EMP_FaturaNum,
  EMP_IDDuplicata, NumeroNF, EMP_FaturaSeq, TIPOCART, NF_Emp_Serie,
  Represen, ASS_IDDuplicata, ASS_FaturaSeq, Cliente: Integer;
  ASS_FaturaSep, ASS_TpDuplicata, ASS_TxtFaturas, EMP_TpDuplicata,
  EMP_FaturaSep, EMP_TxtFaturas, SQL_FAT_ITS, SQL_FAT_TOT: String;
  DataFat: TDateTime;
  AFP_Per, FreteVal, Seguro, Desconto, Outros, Cobr_Fat_vLiq: Double;
begin
  if QrNFeCabACobr_Fat_vOrig.Value <> QrSUMNFeCabYvDup.Value then
  begin
    Geral.MB_Aviso('Os valores / quantidades dos produtos foram alterados!' +
      sLineBreak + 'com isso as faturas ser�o recriadas e todas as altera��es ser�o perdidas!');
    //
    Screen.Cursor := crHourGlass;
    try
      FatNum  := QrNFeCabAFatNum.Value;
      //
      DmPediVda.ReopenFatPedCab(FatNum, False);
      //
      Empresa         := DmPediVda.QrFatPedCabEmpresa.Value;
      Filial          := DModG.ObtemFilialDeEntidade(Empresa);
      FatID           := QrNFeCabAFatID.Value;
      EMP_CtaFaturas  := DmPediVda.QrFatPedCabEMP_CtaProdVen.Value;
      Associada       := DmPediVda.QrFatPedCabAssociada.Value;
      ASS_CtaFaturas  := DmPediVda.QrFatPedCabASS_CtaProdVen.Value;
      ASS_FILIAL      := DmPediVda.QrFatPedCabASS_FILIAL.Value;
      Financeiro      := DmPediVda.QrFatPedCabFinanceiro.Value;
      CartEmis        := DmPediVda.QrFatPedCabCartEmis.Value;
      CondicaoPG      := DmPediVda.QrFatPedCabCondicaoPG.Value;
      AFP_Sit         := DmPediVda.QrFatPedCabAFP_Sit.Value;
      EMP_FaturaNum   := DmPediVda.QrFatPedCabEMP_FaturaNum .Value;
      EMP_IDDuplicata := DmPediVda.QrFatPedCabCodUsu.Value;
      NumeroNF        := QrNFeCabAide_nNF.Value;
      EMP_FaturaSeq   := DmPediVda.QrFatPedCabEMP_FaturaSeq.Value;
      TIPOCART        := DmPediVda.QrFatPedCabTIPOCART.Value;
      NF_Emp_Serie    := QrNFeCabAide_serie.Value;
      ASS_FaturaSep   := DmPediVda.QrFatPedCabASS_FaturaSep .Value;
      ASS_TpDuplicata := DmPediVda.QrFatPedCabASS_DupProdVen.Value;
      ASS_TxtFaturas  := DmPediVda.QrFatPedCabASS_TxtProdVen.Value;
      DataFat         := QrNFeCabAide_dEmi.Value;
      Represen        := DmPediVda.QrFatPedCabRepresen.Value;
      ASS_IDDuplicata := DmPediVda.QrFatPedCabCodUsu.Value;
      ASS_FaturaSeq   := DmPediVda.QrFatPedCabASS_FaturaSeq.Value;
      Cliente         := DmPediVda.QrFatPedCabCliente.Value;
      AFP_Per         := DmPediVda.QrFatPedCabAFP_Per.Value;
      FreteVal        := QrNFeCabAICMSTot_vFrete.Value;
      Seguro          := QrNFeCabAICMSTot_vSeg.Value;
      Desconto        := QrNFeCabAICMSTot_vDesc.Value;
      Outros          := QrNFeCabAICMSTot_vOutro.Value;
      EMP_TpDuplicata := DmPediVda.QrFatPedCabEMP_DupProdVen.Value;
      EMP_FaturaSep   := DmPediVda.QrFatPedCabEMP_FaturaSep .Value;
      EMP_TxtFaturas  := DmPediVda.QrFatPedCabEMP_TxtProdVen.Value;
      //
      if not UnNFe_PF.InsUpdFaturasNFe(Filial, Empresa, FatID, VAR_FATID_0001,
        FatNum, EMP_CtaFaturas, Associada, ASS_CtaFaturas, ASS_FILIAL, Financeiro,
        CartEmis, CondicaoPG, AFP_Sit, EMP_FaturaNum, EMP_IDDuplicata, NumeroNF,
        EMP_FaturaSeq, TIPOCART, Represen, ASS_IDDuplicata, ASS_FaturaSeq,
        Cliente, AFP_Per, FreteVal, Seguro, Desconto, Outros, EMP_TpDuplicata,
        EMP_FaturaSep, EMP_TxtFaturas, ASS_FaturaSep, ASS_TpDuplicata,
        ASS_TxtFaturas, DataFat, QrPrzT, QrSumT, QrPrzX, QrSumX, QrNF_X, True) then
      begin
        Geral.MB_Erro(Con_MsgErro);
      end else
      begin
        SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(VAR_FATID_0001, FatNum, Empresa);
        //
        if SQL_FAT_ITS = '' then
        begin
          Geral.MB_Erro(Con_MsgErro);
          Exit;
        end;
        //Exclui todos lan�amentos para atualizar a tabela
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM nfecaby WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
        Dmod.QrUpd.Params[00].AsInteger := FatID;
        Dmod.QrUpd.Params[01].AsInteger := FatNum;
        Dmod.QrUpd.Params[02].AsInteger := Empresa;
        Dmod.QrUpd.ExecSQL;
        //
        if not DmNFe_0000.InsUpdNFeCabY(FatID, FatNum,
          Empresa, SQL_FAT_ITS, LaAviso1, LaAviso2, REWarning) then
        begin
          Geral.MB_Erro(Con_MsgErro);
        end else
          ReopenNFeCabY(0);
        //
        //Atualizar Cobr_Fat_vLiq pois os lan�amentos s�o recriados acima
        SQL_FAT_TOT := DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(FatID, FatNum, Empresa);
        DmNFe_0000.QrFatYTot.Close;
        DmNFe_0000.QrFatYTot.SQL.Text := SQL_FAT_TOT;
        UMyMod.AbreQuery(DmNFe_0000.QrFatYTot, Dmod.MyDB, 'TFmNFeCabA_0000.CalculaFaturasNFs()');
        //
        Cobr_Fat_vLiq := DmNFe_0000.QrFatYTotValor.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False,
          ['Cobr_Fat_vLiq'], ['FatID', 'FatNum', 'Empresa'],
          [Cobr_Fat_vLiq], [FatID, FatNum, Empresa], True);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value);
  end;
  ReopenNFeCabY(0);
end;

procedure TFmNFeCabA_0000.CalculaRecuperacaoImpostos();
var
  BC_T, Fator, BC_I: Double;
begin
  BC_T := EdICMSTot_vBC.ValueVariant;
  //
  Fator := (100 - EdICMSRec_pRedBC.ValueVariant) / 100;
  BC_I := BC_T * Fator;
  EdICMSRec_vBC.ValueVariant := BC_I;
  EdICMSRec_vICMS.ValueVariant := BC_I * EdICMSRec_pAliq.ValueVariant / 100;
  //
  Fator := (100 - EdIPIRec_pRedBC.ValueVariant) / 100;
  BC_I := BC_T * Fator;
  EdIPIRec_vBC.ValueVariant := BC_I;
  EdIPIRec_vIPI.ValueVariant := BC_I * EdIPIRec_pAliq.ValueVariant / 100;
  //
  Fator := (100 - EdPISRec_pRedBC.ValueVariant) / 100;
  BC_I := BC_T * Fator;
  EdPISRec_vBC.ValueVariant := BC_I;
  EdPISRec_vPIS.ValueVariant := BC_I * EdPISRec_pAliq.ValueVariant / 100;
  //
  Fator := (100 - EdCOFINSRec_pRedBC.ValueVariant) / 100;
  BC_I := BC_T * Fator;
  EdCOFINSRec_vBC.ValueVariant := BC_I;
  EdCOFINSRec_vCOFINS.ValueVariant := BC_I * EdCOFINSRec_pAliq.ValueVariant / 100;
  //
end;

procedure TFmNFeCabA_0000.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeCabA_0000.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeCabA_0000.EdDBide_idDestChange(Sender: TObject);
begin
  EdDBide_idDest_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_idDest, Geral.IMV(EdDBide_idDest.Text));
end;

procedure TFmNFeCabA_0000.EdDBide_indFinalChange(Sender: TObject);
begin
  EdDBide_indFinal_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indFinal, Geral.IMV(EdDBide_indFinal.Text));
end;

procedure TFmNFeCabA_0000.EdDBide_indPresChange(Sender: TObject);
begin
  EdDBide_indPres_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPres, Geral.IMV(EdDBide_indPres.Text));
end;

procedure TFmNFeCabA_0000.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeCabA_0000.SpeedButton13Click(Sender: TObject);
var
  CartEmis: Integer;
begin
  VAR_CADASTRO := 0;
  CartEmis     := EdCartEmis.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartEmis);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdCartEmis, CBCartEmis, DmPedivda.QrCartEmis,
      VAR_CADASTRO, 'Codigo', 'Codigo');
end;

procedure TFmNFeCabA_0000.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeCabA_0000.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeCabA_0000.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeCabA_0000.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeCabA_0000.SpeedButton5Click(Sender: TObject);
var
  Pesq: Integer;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'dtb_munici', DModG.AllID_DB, '', True);
  if Pesq <> 0 then
    Edide_cMunFG.ValueVariant := VAR_CODIGO;
end;

procedure TFmNFeCabA_0000.SpeedButton6Click(Sender: TObject);
var
  Pesq: String;
begin
  Pesq := CuringaLoc.CriaForm4('Codigo', 'Nome', 'cfop2003', Dmod.MyDB, '');
  if Pesq <> '' then
    Edide_natOp.Text := VAR_NOME_X;
end;

procedure TFmNFeCabA_0000.SbEntiDestClick(Sender: TObject);
var
  CodInfoDest: Integer;
begin
  VAR_CADASTRO := 0;
  CodInfoDest  := EdCodInfoDest.ValueVariant;
  //
  DmodG.CadastroDeEntidade(CodInfoDest, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdCodInfoDest, CBCodInfoDest, QrEntiDest,
      VAR_CADASTRO, 'Codigo', 'Codigo');
end;

procedure TFmNFeCabA_0000.SpeedButton8Click(Sender: TObject);
var
  Transporta: Integer;
begin
  VAR_CADASTRO := 0;
  Transporta   := EdTransporta.ValueVariant;
  //
  DmodG.CadastroDeEntidade(Transporta, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdTransporta, CBTransporta, QrTransportas,
      VAR_CADASTRO, 'Codigo', 'Codigo');
    ObtemDadosDeTransportadora(VAR_CADASTRO, True);
  end;
end;

procedure TFmNFeCabA_0000.SbEntiEmitClick(Sender: TObject);
var
  EntiEmit: Integer;
begin
  VAR_CADASTRO := 0;
  EntiEmit     := EdCodInfoEmit.ValueVariant;
  //
  DmodG.CadastroDeEntidade(EntiEmit, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdCodInfoEmit, CBCodInfoEmit, QrEntiEmit,
      VAR_CADASTRO, 'Codigo', 'Codigo');
end;

procedure TFmNFeCabA_0000.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeCabA_0000.EdTransporta_UFChange(Sender: TObject);
begin
  EdTransporta_IE.LinkMsk := EdTransporta_UF.Text;
end;

procedure TFmNFeCabA_0000.EdversaoChange(Sender: TObject);
begin
  case Trunc((Edversao.ValueVariant + 0.009) * 100) of
    110:
    begin
      MeInfAdic_InfAdFisco.MaxLength := 255;
    end;
    200, 310, 400:
    begin
      MeInfAdic_InfAdFisco.MaxLength := 2000;
    end;
    else begin
      MeInfAdic_InfAdFisco.MaxLength := 2000;
      Geral.MB_Aviso('Vers�o do XML n�o implementada: ' +
      FloatToStr(Edversao.ValueVariant) + sLineBreak +
      'FmNFeCabA_0000.EdversaoChange()');
    end;
  end;
end;

procedure TFmNFeCabA_0000.Exclui1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabF, nil, 'nfecabf',
    ['FatID', 'FatNum', 'Empresa'],
    ['FatID', 'FatNum', 'Empresa'], istAtual, '');
end;

procedure TFmNFeCabA_0000.Exclui2Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabG, nil, 'nfecabg',
    ['FatID', 'FatNum', 'Empresa'],
    ['FatID', 'FatNum', 'Empresa'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.ExcluiCampoLivre1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabZCon,
  TDBGrid(DBGNfeCabZCon), 'nfecabzcon',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.ExcluiCampoLivre2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabZFis,
  TDBGrid(DBGNfeCabZFis), 'nfecabzfis',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.Excluicobranaatual1Click(Sender: TObject);
  procedure DeleteNFeCabY(FatID, FatNum, Empresa: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecaby ');
    Dmod.QrUpd.SQL.Add('WHERE FatId=:P0 ');
    Dmod.QrUpd.SQL.Add('AND FatNum=:P1 ');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 ');
    Dmod.QrUpd.SQL.Add('AND FatID<>0 ');
    Dmod.QrUpd.SQL.Add('AND FatNum<>0 ');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
  end;
  procedure DeleteLancto(FatID, FatNum, Empresa: Integer);
  var
    TabLctA: String;
  {$IFDEF DEFINE_VARLCT}
    Filial: Integer;
  {$ENDIF}
  begin
  {$IFDEF DEFINE_VARLCT}
    Filial := DModG.ObtemFilialDeEntidade(Empresa);
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum, Empresa, 0,
    dmkPF.MotivDel_ValidaCodigo(311), False, TabLcta);
  {$ELSE}
    TabLctA := VAR_LCT;
    UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FatID, FatNum, Empresa, 0, False);
  {$ENDIF}
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ' + TabLctA);
    Dmod.QrUpd.SQL.Add('WHERE Fat_Id=:P0 ');
    Dmod.QrUpd.SQL.Add('AND Fat_Num=:P1 ');
    Dmod.QrUpd.SQL.Add('AND CliInt=:P2 ');
    Dmod.QrUpd.SQL.Add('AND Fat_ID<>0 ');
    Dmod.QrUpd.SQL.Add('AND Fat_Num<>0 ');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
}
  end;
var
  FatID, FatNum, Empresa: Integer;
begin
//  ERRO GRAVE! est� excluindo os lan�amento financeiros do PQ pois ambos tem FATID=1!
//  ARRUMAR!
  if Geral.MensagemBox('Confirma a exclus�o da parcela de cobran�a selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    FatID    := QrNFeCabYFatId.Value;
    FatNum   := QrNFeCabYFatNum.Value;
    //FatParc  := QrNFeCabYFatParcela.Value;
    Empresa  := QrNFeCabYEmpresa.Value;
    //
    DeleteLancto(FatID, FatNum, Empresa);
    //UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FatID, FatNum, Empresa, 0, False);
    DeleteNFeCabY(FatID, FatNum, Empresa);
    //
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, LaAviso1, LaAviso2, REWarning);
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
    //LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value);
  end;
end;

procedure TFmNFeCabA_0000.Excluiitematualnoexcluifinanceiro1Click(
  Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item de faturamento atual?' +
  sLineBreak + 'CUIDADO! O lan�amento financeiro correspondente n�o ser� exclu�do!')
  = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM nfecaby ',
    'WHERE FatID=' + Geral.FF0(QrNFeCabYFatID.Value),
    'AND FatNum=' + Geral.FF0(QrNFeCabYFatNum.Value),
    'AND FatParcela=' + Geral.FF0(QrNFeCabYFatParcela.Value),
    'AND Empresa=' + Geral.FF0(QrNFeCabYEmpresa.Value),
    'AND FatID<>0 ',
    'AND FatNum<>0 ',
    '']);
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
    //
    QrNfeCabY.Next;
    ReopenNfeCabY(QrNfeCabYControle.Value);
  end;
end;

procedure TFmNFeCabA_0000.ExcluiLacre1Click(Sender: TObject);
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabXLac,
  TDBGrid(DBGNFeCabXLac), 'nfecabxlac',
    ['FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'],
    ['FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.ExcluiNFeatual1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
// Parei aqui
end;

procedure TFmNFeCabA_0000.ExcluiPagto;
begin
  PageControl1.ActivePageIndex := 12;
  //
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabYA, TDBGrid(DBGNFeCabYA),
    'nfecabya', ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.ExcluiProcessoouatoconcessrio1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabZPro,
  TDBGrid(DBGNfeCabZPro), 'nfecabzpro',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.Excluiprodutoatual1Click(Sender: TObject);

  procedure DeleteItem(Tabela:String; FatID, FatNum, Empresa, nItem: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + Tabela);
    Dmod.QrUpd.SQL.Add('WHERE FatId=:P0 ');
    Dmod.QrUpd.SQL.Add('AND FatNum=:P1 ');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 ');
    Dmod.QrUpd.SQL.Add('AND nItem=:P3 ');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
  end;

var
  Qry: TmySQLQuery;
  FatID, FatNum, Empresa, nItem, IDCtrl: Integer;
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if Geral.MensagemBox('Confirma a exclus�o do item selecionado?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    FatID    := QrNFeItsIFatId.Value;
    FatNum   := QrNFeItsIFatNum.Value;
    Empresa  := QrNFeItsIEmpresa.Value;
    nItem    := QrNFeItsInItem.Value;
    //
    DeleteItem('nfeitsv', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsu', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitst', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitss', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsr', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsq', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsp', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitso', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsn', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsm', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsidia', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsidi', FatID, FatNum, Empresa, nItem);
    DeleteItem('nfeitsi', FatID, FatNum, Empresa, nItem);
    //
    if QrNFeItsIStqMovValA.Value <> 0 then
    begin
      Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT IDCtrl ',
          'FROM stqmovvala ',
          'WHERE ID=' + Geral.FF0(QrNFeItsIStqMovValA.Value),
          '']);
        //
        IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
        //
        if IDCtrl <> 0 then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
            DELETE_FROM + ' stqmovvala ',
            'WHERE ID=' + Geral.FF0(QrNFeItsIStqMovValA.Value),
            '']);
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
            DELETE_FROM + ' stqmovitsa ',
            'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
            '']);
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
            DELETE_FROM + ' stqmovitsb ',
            'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
            '']);
        end;
      finally
        Qry.Free;
      end;
    end;
    //
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, LaAviso1, LaAviso2, REWarning);
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
    //
    LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value);
  end;
end;

procedure TFmNFeCabA_0000.ExcluiReboque1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabXReb,
  TDBGrid(DBGNfeCabXReb), 'nfecabxreb',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.Excluireferenciamentos1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabB, DBGNfeCabB, 'nfecabb',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.ExcluiVolume1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeCabXVol,
  TDBGrid(DBGNFeCabXVol), 'nfecabxvol',
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'], istPergunta, '');
end;

procedure TFmNFeCabA_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeCabAIDCtrl.Value;
  Close;
end;

procedure TFmNFeCabA_0000.BtTabelaPrcClick(Sender: TObject);
var
  TabelaPrc: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdTabelaPrc, TabelaPrc, '', 'Codigo', 'CodUsu') then
    Exit;
  //
  GFat_Jan.MostraFormTabePrcCab(TabelaPrc);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdTabelaPrc, CBTabelaPrc, DmPedivda.QrTabePrcCab,
      VAR_CADASTRO);
end;

procedure TFmNFeCabA_0000.BuscaDadosDest();
var
  Lograd_Rua: String;
begin
  DmNFe_0000.ReopenDest(EdCodInfoDest.ValueVariant);
  if DmNFe_0000.QrDestNO_LOGRAD.Value <> '' then
    Lograd_Rua := DmNFe_0000.QrDestNO_LOGRAD.Value + ' ';
  Lograd_Rua := Lograd_Rua + DmNFe_0000.QrDestRUA.Value;
  //
  Eddest_IE     .ValueVariant := '';
  Eddest_ISUF   .ValueVariant := '';
  Eddest_CNPJ   .ValueVariant := '';
  Eddest_CPF    .ValueVariant := '';
  Eddest_xNome  .ValueVariant := '';
  Eddest_xLgr   .ValueVariant := '';
  Eddest_nro    .ValueVariant := '';
  Eddest_xCpl   .ValueVariant := '';
  Eddest_xBairro.ValueVariant := '';
  Eddest_cMun   .ValueVariant := 0;
  Eddest_xMun   .ValueVariant := '';
  Eddest_CEP    .ValueVariant := '';
  Eddest_cPais  .ValueVariant := 0;
  Eddest_xPais  .ValueVariant := '';
  Eddest_fone   .ValueVariant := '';
  Eddest_UF     .ValueVariant := '';
  Eddest_idEstrangeiro.ValueVariant := '';
  //
  Eddest_CNPJ   .ValueVariant := Geral.FormataCNPJ_TT(DmNFe_0000.QrDestCNPJ.Value);
  Eddest_CPF    .ValueVariant := Geral.FormataCNPJ_TT(DmNFe_0000.QrDestCPF.Value);
  Eddest_xNome  .ValueVariant := DmNFe_0000.QrDestNO_ENT.Value;
  Eddest_xLgr   .ValueVariant := Lograd_Rua;
  Eddest_nro    .ValueVariant := DmNFe_0000.QrDestNumero.Value;
  Eddest_xCpl   .ValueVariant := DmNFe_0000.QrDestCOMPL.Value;
  Eddest_xBairro.ValueVariant := DmNFe_0000.QrDestBAIRRO.Value;
  Eddest_cMun   .ValueVariant := DmNFe_0000.QrDestCodMunici.Value;
  Eddest_xMun   .ValueVariant := DmNFe_0000.QrDestNO_Munici.Value;
  Eddest_UF     .ValueVariant := DmNFe_0000.QrDestNO_UF.Value;
  Eddest_CEP    .ValueVariant := DmNFe_0000.QrDestCEP.Value;
  Eddest_cPais  .ValueVariant := DmNFe_0000.QrDestCodiPais.Value;
  Eddest_xPais  .ValueVariant := DmNFe_0000.QrDestNO_Pais.Value;
  Eddest_fone   .ValueVariant := DmNFe_0000.QrDestTe1.Value;
  Eddest_IE     .ValueVariant := DmNFe_0000.QrDestIE.Value;
  Eddest_ISUF   .ValueVariant := DmNFe_0000.QrDestSUFRAMA.Value;
  Eddest_idEstrangeiro.ValueVariant := DmNFe_0000.QrDestEstrangNum.Value;
end;

procedure TFmNFeCabA_0000.BuscaDadosEmit;
begin
  DmNFe_0000.ReopenEmit(EdCodInfoEmit.ValueVariant);
  Edemit_CNPJ    .ValueVariant := DmNFe_0000.QrEmitCNPJ.Value;
  Edemit_CPF     .ValueVariant := DmNFe_0000.QrEmitCPF.Value;
  Edemit_xNome   .ValueVariant := DmNFe_0000.QrEmitNO_ENT.Value;
  Edemit_xFant   .ValueVariant := DmNFe_0000.QrEmitFANTASIA.Value;
  Edemit_xLgr    .ValueVariant := DmNFe_0000.QrEmitNO_LOGRAD.Value;
  Edemit_nro     .ValueVariant := DmNFe_0000.QrEmitNumero.Value;
  Edemit_xCpl    .ValueVariant := DmNFe_0000.QrEmitCOMPL.Value;
  Edemit_xBairro .ValueVariant := DmNFe_0000.QrEmitBAIRRO.Value;
  Edemit_cMun    .ValueVariant := DmNFe_0000.QrEmitCodMunici.Value;
  Edemit_xMun    .ValueVariant := DmNFe_0000.QrEmitNO_Munici.Value;
  Edemit_UF      .ValueVariant := DmNFe_0000.QrEmitNO_UF.Value;
  Edemit_CEP     .ValueVariant := DmNFe_0000.QrEmitCEP.Value;
  Edemit_cPais   .ValueVariant := DmNFe_0000.QrEmitCodiPais.Value;
  Edemit_xPais   .ValueVariant := DmNFe_0000.QrEmitNO_Pais.Value;
  Edemit_fone    .ValueVariant := DmNFe_0000.QrEmitTe1.Value;
  Edemit_IE      .ValueVariant := DmNFe_0000.QrEmitIE.Value;
  Edemit_IEST    .ValueVariant := DmNFe_0000.QrEmitIEST.Value;
  Edemit_IM      .ValueVariant := DmNFe_0000.QrEmitNIRE.Value;
  Edemit_CNAE    .ValueVariant := DmNFe_0000.QrEmitCNAE.Value;
  CBemit_CNAE    .KeyValue     := DmNFe_0000.QrEmitCNAE.Value;
end;

procedure TFmNFeCabA_0000.AlteraCampoLivre1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZCon(stUpd, QrNFeCabA, QrNFeCabZCon, 10);
end;

procedure TFmNFeCabA_0000.AlteraCampoLivre2Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZFis(stUpd, QrNFeCabA, QrNFeCabZFis, 10);
end;

procedure TFmNFeCabA_0000.Alteracobranaatual1Click(Sender: TObject);
var
  //Controle,
  FatID, FatNum, Empresa, FatParcela: Integer;
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if DBCheck.CriaFm(TFmNfeCabY_0000, FmNfeCabY_0000, afmoNegarComAviso) then
  begin
    FmNfeCabY_0000.ImgTipo.SQLType := stUpd;
    FmNfeCabY_0000.EdControle.ValueVariant := QrNFeCabYControle.Value;
    FatID      := QrNFeCabYFatID.Value;
    FatNum     := QrNFeCabYFatNum.Value;
    // N�o tem como!!
    //FatParcela := QrNFeCabYFatParcela.Value;
    FatParcela := QrNFeCabYFatParcela.Value;
    Empresa    := QrNFeCabYEmpresa.Value;
    FmNfeCabY_0000.EdFatID.ValueVariant      := FatID;
    FmNfeCabY_0000.EdFatNum.ValueVariant     := FatNum;
    FmNfeCabY_0000.EdFatParcela.ValueVariant := FatParcela;
    FmNfeCabY_0000.EdEmpresa.ValueVariant    := Empresa;
    // N�o tem como! N�o existe equivalente ao FatParcela no NfeCabY!
    //DmNFe_0000.LocLctCabY(FatID, FatNum, FatParcela, Empresa, Controle);
    FmNfeCabY_0000.EdLancto.ValueVariant     := QrNFeCabYLancto.Value;
    FmNfeCabY_0000.EdSub.ValueVariant        := QrNFeCabYSub.Value;
    FmNfeCabY_0000.EdnDup.ValueVariant       := QrNFeCabYnDup.Value;
    FmNfeCabY_0000.TPdVenc.Date              := QrNFeCabYdVenc.Value;
    FmNfeCabY_0000.EdvDup.ValueVariant       := QrNFeCabYvDup.Value;
    FmNfeCabY_0000.EdGenero.ValueVariant     := QrNFeCabYGenero.Value;
    FmNfeCabY_0000.CBGenero.KeyValue         := QrNFeCabYGenero.Value;
    FmNfeCabY_0000.EdDescricao.ValueVariant  := QrNFeCabYDescricao.Value;
    //
    FmNfeCabY_0000.ShowModal;
    FmNfeCabY_0000.Destroy;
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
  end;
end;

procedure TFmNFeCabA_0000.Alteraitemdeatributoatual1Click(Sender: TObject);
begin
{
  UmyMod.FormInsUpd_Show(TFmGraAtrIts, FmGraAtrIts, afmoNegarComAviso,
    QrGraAtrIts, stUpd);
}
end;

procedure TFmNFeCabA_0000.AlteraLacre1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXLac(stUpd, QrNFeCabXVol, QrNFeCabXLac, 5000);
end;

procedure TFmNFeCabA_0000.AlteraNFeatual1Click(Sender: TObject);
var
  CompoToFocus: TWinControl;
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  PageControl2.ActivePageIndex := PageControl1.ActivePageIndex;
  case PageControl2.ActivePageIndex of
    0: CompoToFocus := EdRegrFiscal;
    1: CompoToFocus := EdCodInfoEmit;
    2: CompoToFocus := EdCodInfoDest;
    3: CompoToFocus := EdModFrete;
    4: CompoToFocus := MeInfAdic_InfAdFisco;
    5: CompoToFocus := EdICMSTot_vBC;
    else CompoToFocus := nil;
  end;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdit, QrNFeCabA, [PainelDados],
    [PainelEdita], CompoToFocus, ImgTipo, 'nfecaba');
{
    EdFatID.ValueVariant := 999999999;
    EdFatNum.ValueVariant := 0;
    EdEmpresa.ValueVariant := DModG.QrFiliLogCodigo.Value;
    Edide_verProc.Text :=  DBCheck.Obtem_verProc;
    Edide_natOp.Text := '';
  end;
}

end;

procedure TFmNFeCabA_0000.AlteraPagto();
var
  FatID, FatNum, Empresa, Controle: Integer;
begin
  PageControl1.ActivePageIndex := 12;
  //
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  FatID    := QrNFeCabYAFatID.Value;
  FatNum   := QrNFeCabYAFatNum.Value;
  Empresa  := QrNFeCabYAEmpresa.Value;
  Controle := QrNFeCabYAControle.Value;
  //
  if DBCheck.CriaFm(TFmNfeCabYA_0000, FmNfeCabYA_0000, afmoNegarComAviso) then
  begin
    FmNfeCabYA_0000.ImgTipo.SQLType         := stUpd;
    FmNfeCabYA_0000.EdControle.ValueVariant := Controle;
    FmNfeCabYA_0000.EdFatID.ValueVariant    := FatID;
    FmNfeCabYA_0000.EdFatNum.ValueVariant   := FatNum;
    FmNfeCabYA_0000.EdEmpresa.ValueVariant  := Empresa;
    //
    FmNfeCabYA_0000.EdtPag.ValueVariant          := QrNFeCabYAtPag.Value;
    FmNfeCabYA_0000.EdtPag_TXT.ValueVariant      := QrNFeCabYAtPag_TXT.Value;
    FmNFeCabYA_0000.EdvPag.ValueVariant          := QrNFeCabYAvPag.Value;
    FmNFeCabYA_0000.FvPag                        := QrNFeCabYAvPag.Value;
    FmNFeCabYA_0000.EdvTroco.ValueVariant        := QrNFeCabYAvTroco.Value;
    FmNFeCabYA_0000.EdtpIntegra.ValueVariant     := QrNFeCabYAtpIntegra.Value;
    FmNFeCabYA_0000.EdtpIntegra_TXT.ValueVariant := QrNFeCabYAtpIntegra_TXT.Value;
    FmNFeCabYA_0000.EdCNPJ.ValueVariant          := QrNFeCabYACNPJ.Value;
    FmNFeCabYA_0000.EdtBand.ValueVariant         := QrNFeCabYAtBand.Value;
    FmNFeCabYA_0000.EdtBand_TXT.ValueVariant     := QrNFeCabYAtBand_TXT.Value;
    FmNFeCabYA_0000.EdcAut.ValueVariant          := QrNFeCabYAcAut.Value;
    //
    FmNfeCabYA_0000.ShowModal;
    FmNfeCabYA_0000.Destroy;
  end;
end;

procedure TFmNFeCabA_0000.AlteraProcessoouatoconcessrio1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZPro(stUpd, QrNFeCabA, QrNFeCabZPro, 100);
end;

procedure TFmNFeCabA_0000.Alteraprodutoatual1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if UmyMod.FormInsUpd_Mul_Cria(TFmNFeItsI_0000, FmNFeItsI_0000, afmoNegarComAviso,
    [QrNFeItsI, QrNFeItsN, QrNFeItsNA, QrNFeItsO, QrNFeItsP, QrNFeItsQ, QrNFeItsR,
    QrNFeItsS, QrNFeItsT, QrNFeItsV], stUpd) then
  begin
    FmNFeItsI_0000.FAbaAtu               := PageControl3.ActivePageIndex;
    FmNFeItsI_0000.FForm                 := 'FmNFeCabA_0000';
    FmNFeItsI_0000.FStqMovValA           := QrNFeItsIStqMovValA.Value;
    FmNFeItsI_0000.CkGeraGrupoNA.Checked := QrNFeItsNA.recordCount > 0;
    FmNFeItsI_0000.ShowModal;
    FmNFeItsI_0000.Destroy;
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
  end;
end;

procedure TFmNFeCabA_0000.AlteraReboque1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXReb(stUpd, QrNFeCabA, QrNFeCabXReb, 5);
end;

procedure TFmNFeCabA_0000.Alterarefenciamentoatual1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  UmyMod.FormInsUpd_Cria(TFmNFeCabB_0000, FmNFeCabB_0000, afmoNegarComAviso,
  QrNFeCabB, stUpd);
  FmNFeCabB_0000.FFatID    := QrNFeCabAFatID.Value;
  FmNFeCabB_0000.FFatNum   := QrNFeCabAFatNum.Value;
  FmNFeCabB_0000.FEmpresa  := QrNFeCabAEmpresa.Value;
  FmNFeCabB_0000.FControle := QrNFeCabBControle.Value;
  FmNFeCabB_0000.FQryIts   := QrNFeCabB;
  //
  FmNFeCabB_0000.ShowModal;
  FmNFeCabB_0000.Destroy;
end;

procedure TFmNFeCabA_0000.Alteraservicoatual1Click(Sender: TObject);
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  UmyMod.FormInsUpd_Mul_Show(TFmNFeItsU_0000, FmNFeItsU_0000, afmoNegarComAviso,
    [QrNFeItsI, QrNFeItsU, QrNFeItsV], stUpd);
end;

procedure TFmNFeCabA_0000.AlteraTributostotaisaproximados1Click(
  Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
  if MyObjects.FIC(QrNFeItsM.RecordCount = 0, nil,
  'N�o existe registro de Tributos Totais Aproximados para o item selecionado!')
  then
    Exit;
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if DBCheck.CriaFm(TFmNFeItsM_0310, FmNFeItsM_0310, afmoNegarComAviso) then
  begin
    FmNFeItsM_0310.ImgTipo.SQLType := stUpd;
    FatID    := QrNFeCabAFatId.Value;
    FatNum   := QrNFeCabAFatNum.Value;
    Empresa  := QrNFeCabAEmpresa.Value;
    nItem    := QrNFeItsMnItem.Value;
    //
    FmNFeItsM_0310.EdFatID   .ValueVariant := FatID;
    FmNFeItsM_0310.EdFatNum  .ValueVariant := FatNum;
    FmNFeItsM_0310.EdEmpresa .ValueVariant := Empresa;
    FmNFeItsM_0310.EdnItem   .ValueVariant := nItem;
    FmNFeItsM_0310.Edprod_xProd.Text       := QrNFEItsIprod_cProd.Value;
    FmNFeItsM_0310.Edprod_xProd.Text       := QrNFEItsIprod_xProd.Value;
    //
    //FmNFeItsM_0310.EdiTotTrib.ValueVariant      := 1; // Alteracao Manual
    FmNFeItsM_0310.F_iTotTrib                   := 1; // Alteracao Manual
    FmNFeItsM_0310.EdvTotTrib.ValueVariant      := QrNFeItsMvTotTrib.Value;
    FmNFeItsM_0310.EdpTotTrib.ValueVariant      := QrNFeItsMptotTrib.Value;
    //FmNFeItsM_0310.EdtabTotTrib.ValueVariant    := 0; // Alterado
    FmNFeItsM_0310.F_tabTotTrib                 := 0; // Alterado
    //FmNFeItsM_0310.EdverTotTrib.ValueVariant    := ''; // Alterado
    FmNFeItsM_0310.F_verTotTrib                 := ''; // Alterado
    //FmNFeItsM_0310.EDtpAliqTotTrib.ValueVariant := ''; // Alterado
    FmNFeItsM_0310.F_tpAliqTotTrib              := 0; // Alterado
    FmNFeItsM_0310.F_fontTotTrib                := 0; // Alterado
    //
    FmNFeItsM_0310.ShowModal;
    FmNFeItsM_0310.Destroy;
    //
    ReopenNFeItsM(0);
  end;
end;

procedure TFmNFeCabA_0000.AlteraVolume1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXVol(stUpd, QrNFeCabA, QrNFeCabXVol, 5000);
end;

procedure TFmNFeCabA_0000.BtNFeCabXLacClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabXLac, BtNFeCabXLac);
end;

procedure TFmNFeCabA_0000.BtNFeCabXRebClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabXReb, BtNFeCabXReb);
end;

procedure TFmNFeCabA_0000.BtNFeCabXVolClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabXVol, BtNFeCabXVol);
end;

procedure TFmNFeCabA_0000.BtAlteraClick(Sender: TObject);
begin
  AlteraPagto();
end;

procedure TFmNFeCabA_0000.BtAlteraItsIDIAClick(Sender: TObject);
begin
  //UmyMod.FormInsUpd_Show(TFmNFeItsIDIa_0000, FmNFeItsIDIa_0000, afmoNegarComAviso,
  //  QrNFeItsIDIa, stUpd);
  UnNFe_PF.MostraFormNFeItsIDIa(QrNFeItsIDIa, stUpd);
end;

procedure TFmNFeCabA_0000.BtAlteraItsIDIClick(Sender: TObject);
begin
  //UmyMod.FormInsUpd_Show(TFmNFeItsIDI_0000, FmNFeItsIDI_0000, afmoNegarComAviso,
  //  QrNFeItsIDI, stUpd);
  UnNFe_PF.MostraFormNFeItsIDI(QrNFeItsIDI, stUpd);
end;

procedure TFmNFeCabA_0000.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
  UMyMod.ObtemCodigoDeCodUsu(EdCondicaoPG, CondicaoPG, '', 'Codigo', 'CodUsu');
  //
  Praz_PF.MostraFormPediPrzCab1(CondicaoPG);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdCondicaoPg, CBCondicaoPg, DmPedivda.QrPediPrzCab,
    VAR_CADASTRO);
end;

procedure TFmNFeCabA_0000.BtConfirmaClick(Sender: TObject);
var
  IDCtrl, ide_cUF, ide_serie, ide_nNF, ide_Mod, ide_cNF: Integer;
  emit_CNPJ, emit_CPF, ide_cDV, Id: String;
  ide_dEmi: TDateTime;
  cNF_Atual, FatID, ide_tpEmis, ide_tpNF: Integer;
  versao: Double;
begin
  //ualizar NFeCabA, etc...
  DmNFe_0000.ReopenOpcoesNFe(EdCodInfoEmit.ValueVariant, True);
  ide_cUF    := Edide_CUF.ValueVariant;
  ide_dEmi   := Int(TPide_dEmi.Date);
  emit_CNPJ  := Geral.SoNumero_TT(Edemit_CNPJ.Text);
  emit_CPF   := Geral.SoNumero_TT(Edemit_CPF.Text);
  ide_Mod    := Edide_Mod.ValueVariant;
  ide_serie  := Edide_serie.ValueVariant;
  ide_nNF    := Edide_nNF.ValueVariant;
  versao     := Edversao.ValueVariant;
  ide_tpEmis := Edide_tpEmis.ValueVariant;
  //
  if EdFatID.ValueVariant = 999999999 then
  begin
    if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
      'Informe a regra fiscal!') then Exit;
    if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
      'Informe a carteira para emiss�o dos lan�amentos!') then Exit;
    if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
      'Informe a tabela de pre�os!') then Exit;
    if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
      'Informe a condi��o de pagamento!') then Exit;
  end;
  FatID := EdFatID.ValueVariant;
  if not FatID in ([113(*entrada de couro NF normal*)]) then
  begin
    if MyObjects.FIC(Edide_CUF.ValueVariant = 0, Edide_CUF,
      'Informe a UF (Geral)!') then Exit;
    if MyObjects.FIC(Length(Edide_natOp.Text) < 1, Edide_natOp,
      'Informe a natureza da opera��o)!') then Exit;
    if MyObjects.FIC(Edide_cMunFG.ValueVariant = 0, Edide_cMunFG,
      'Informe o c�digo do munic�pio do fato gerador!') then Exit;
    if MyObjects.FIC(Edide_cMunFG_TXT.ValueVariant = '', Edide_cMunFG_TXT,
      'Informe o nome do munic�pio do fato gerador!') then Exit;
    if MyObjects.FIC(Edide_finNFe.ValueVariant = 0, Edide_finNFe,
      'Informe a finalidade da emiss�o da NF-e!') then Exit;
    //
    cNF_Atual := Edide_cNF.ValueVariant;
    if not DmNFe_0000.MontaChaveDeAcesso(ide_cUF, ide_dEmi, emit_CNPJ, ide_Mod,
      ide_serie, ide_nNF, ide_cNF, ide_cDV, Id, cNF_Atual, ide_tpEmis, versao) then Exit;
    Edide_cNF.ValueVariant := ide_cNF;
    Edide_cDv.ValueVariant := ide_cDV;
    EdNFe_Id.Text := Id;
    //
  end;
  if MyObjects.FIC(Edide_nNF.ValueVariant = 0, Edide_nNF,
    'Informe o n�mero da nota fiscal!') then Exit;
  ide_tpNF := Edide_tpNF.ValueVariant;
  if MyObjects.FIC(not (ide_tpNF in ([0, 1])), Edide_tpNF,
    'Informe o tipo de documento fiscal!') then Exit;
  if MyObjects.FIC(Edide_tpEmis.ValueVariant = 0, Edide_tpEmis,
    'Informe a forma de impress�o da NF-e!') then Exit;
  if MyObjects.FIC(Edide_tpAmb.ValueVariant = 0, Edide_tpAmb,
    'Informe a identifica��o do ambiente!') then Exit;
  if MyObjects.FIC((emit_CNPJ = '') and (emit_CPF = ''), Edemit_CNPJ,
    'Informe o CNPJ / CPF do Emitente!') then Exit;
  //
  if MyObjects.FIC(Edide_idDest.ValueVariant <= 0, Edide_idDest,
    'Informe a identifica��o do local de destino da opera��o!') then Exit;
  if MyObjects.FIC(Edide_indFinal.ValueVariant < 0, Edide_indFinal,
    'Informe a opera��o com consumidor final!') then Exit;
  if MyObjects.FIC(Edide_indPres.ValueVariant < 0, Edide_indPres,
    'Informe o indicador de presen�a do comprador no estabelecimento no momento da opera��o!') then Exit;
  //
  if (ImgTipo.SQLType = stIns) then
  begin
    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    EdIDCtrl.ValueVariant := IDCtrl;
    EdFatNum.ValueVariant := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeavulsa', '', EdFatNum.ValueVariant);
  end else begin
    IDCtrl := EdIDCtrl.ValueVariant;
  end;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeCabA_0000, PainelEdit,
    'NFeCabA', IDCtrl, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    UnNFe_PF.Atualiza_DtEmissNF_StqMovNfsa(IDCtrl, TPide_dEmi.Date);
    //
    if ImgTipo.SQLType = stIns then
    begin
      if FForm = 'FmPQE' then
      begin

      end;
    end;

    LocCod(IDCtrl, IDCtrl);
    MostraEdicao(0, stLok, 0);
    {if (FForm = 'FmMPIn')
    or (FForm = 'FmPQE')}
    if FForm <> '' then
      Close;
  end;
end;

procedure TFmNFeCabA_0000.BtDesisteClick(Sender: TObject);
var
  IDCtrl : Integer;
begin
  IDCtrl := Geral.IMV(EdIDCtrl.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFeCabA', IDCtrl);
  UMyMod.UpdUnlockY(IDCtrl, Dmod.MyDB, 'NFeCabA', 'IDCtrl');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(IDCtrl, Dmod.MyDB, 'NFeCabA', 'IDCtrl');
end;

procedure TFmNFeCabA_0000.BtExcluiClick(Sender: TObject);
begin
  ExcluiPagto();
end;

procedure TFmNFeCabA_0000.BtExcluiItsIDIaClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeItsIDIa, DBGNFeItsIDIa,
    'nfeitsidia', ['Conta'], ['Conta'], istAtual, '');
end;

procedure TFmNFeCabA_0000.BtExcluiItsIDIClick(Sender: TObject);
begin
  if QrNFeItsIDIa.RecordCount > 0 then
  begin
    Geral.MensagemBox('Esta declara��o de importa��o s� poder� ser ' +
    sLineBreak + 'excluida quando seus itens tamb�m forem!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeItsIDI, DBGNFeItsIDI,
    'nfeitsidi', ['Controle'], ['Controle'], istAtual, '');
end;

procedure TFmNFeCabA_0000.BtIncluiClick(Sender: TObject);
begin
  IncluiPagto();
end;

procedure TFmNFeCabA_0000.BtIncluiItsIDIAClick(Sender: TObject);
begin
  //UmyMod.FormInsUpd_Show(TFmNFeItsIDIa_0000, FmNFeItsIDIa_0000, afmoNegarComAviso,
  //  QrNFeItsIDIa, stIns);
  UnNFe_PF.MostraFormNFeItsIDIa(QrNFeItsIDIa, stIns);
end;

procedure TFmNFeCabA_0000.BtIncluiItsIDIClick(Sender: TObject);
begin
//  UmyMod.FormInsUpd_Show(TFmNFeItsIDI_0000, FmNFeItsIDI_0000, afmoNegarComAviso,
//    QrNFeItsIDI, stIns);
  UnNFe_PF.MostraFormNFeItsIDI(QrNFeItsIDI, stIns);
end;

procedure TFmNFeCabA_0000.BtiNVE_ExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do NVE selecionado?',
  'nfeitsinve', 'Controle', QrNFeItsINVEControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrNFeItsINVE,
      QrNFeItsINVEControle, QrNFeItsINVEControle.Value);
    ReopenNFeItsINVE(Controle);
  end;
end;

procedure TFmNFeCabA_0000.BtiNVE_IncluiClick(Sender: TObject);
const
  Importacia = 1;
var
  NVE: String;
  FatID, FatNum, Empresa, nItem, Controle: Integer;
begin
  if QrNFeItsINVE.RecordCount < 8 then
  begin
    NVE := '';
    if InputQuery('Codifica��o NVE', 'Informe o NVE:', NVE) then
    begin
      (*if*) Geral.ValidaNVE(NVE) (*then*);
      begin
        FatID     := QrNFeItsIFatID.Value;
        FatNum    := QrNFeItsIFatNum.Value;
        Empresa   := QrNFeItsIEmpresa.Value;
        nItem     := QrNFeItsInItem.Value;
        Controle := UMyMod.BPGS1I32('nfeitsinve', 'Controle', '', '', tsPos, stIns, Controle);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsinve', False, [
        'Importacia', 'NVE'], [
        'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle'], [
        Importacia, NVE], [
        FatID, FatNum, Empresa, nItem, Controle], True) then
          ReopenNFeItsINVE(Controle);
      end;
    end;
  end else
    Geral.MB_Aviso('Quantidade m�xima de itens NVE (8) j� foi atingida!');
end;

procedure TFmNFeCabA_0000.BtNFeCabBClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 6;
  MyObjects.MostraPopUpDeBotao(PMNFeCabB, BtNFeCabB);
end;

procedure TFmNFeCabA_0000.BtNFeCabGAClick(Sender: TObject);
var
  FatID, FatNum, Empresa, CodInfoDest: Integer;
begin
  FatID        := QrNFeCabAFatID.Value;
  FatNum       := QrNFeCabAFatNum.Value;
  Empresa      := QrNFeCabAEmpresa.Value;
  CodInfoDest  := QrNFeCabACodInfoDest.Value;
  //
  UnNFe_PF.MostraFormNFeCabGA(FatID, FatNum, Empresa, CodInfoDest, False);
  //
  ReopenNFeCabGA(0);
end;

procedure TFmNFeCabA_0000.BtNFeCabAClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMNFeCabA, BtNFeCabA);
end;

procedure TFmNFeCabA_0000.BtNFeCabYClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 11;
  (*
  MyObjects.MostraPopUpDeBotao(PMNFeCabY, BtNFeCabY);
  *)
  GerenciaNFeCabY();
end;

procedure TFmNFeCabA_0000.BtNFeCabZConClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabZCon, BtNFeCabZCon);
end;

procedure TFmNFeCabA_0000.BtNFeCabZFisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabZFis, BtNFeCabZFis);
end;

procedure TFmNFeCabA_0000.BtNFeCabZProClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFeCabZPro, BtNFeCabZPro);
end;

procedure TFmNFeCabA_0000.BtNFeItsI03_AlteraClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeItsI03(QrNFeItsI03, stUpd);
end;

procedure TFmNFeCabA_0000.BtNFeItsI03_ExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrNFeItsI03, DBGNFeItsI03,
    'nfeitsi03', ['Controle'], ['Controle'], istAtual, '');
end;

procedure TFmNFeCabA_0000.BtNFeItsI03_IncluiClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeItsI03(QrNFeItsI03, stIns);
end;

procedure TFmNFeCabA_0000.BtNFeItsI_UClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 4;
  MyObjects.MostraPopUpDeBotao(PMNFeItsI_U, BtNFeItsI_U);
end;

procedure TFmNFeCabA_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FForm := '';
  TPide_dEmi.Date              := Date;
  TPide_dSaiEnt.Date           := Date;
  TPDataFiscal.Date            := Date;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PainelEdit.Align             := alClient;
  PainelData.Align             := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrEntiEmit, Dmod.MyDB);
  UMyMod.AbreQuery(QrEntiDest, Dmod.MyDB);
  UMyMod.AbreQuery(QrTransportas, Dmod.MyDB);
  //UMyMod.AbreQuery(DmNFe_0000.QrDTB_UFs, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrDTB_UFs, Dmod.MyDB);
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_Munici, DModG.AllID_DB);
  UMyMod.AbreQuery(DmNFe_0000.QrBACEN_Pais, DModG.AllID_DB);
  DmPediVda.ReopenTabelasPedido();
  //
  UMyMod.AbreQuery(DmPedivda.QrCartEmis, Dmod.MyDB);
  //
  FSINTEGRA_ExpNat     := Geral.SINTEGRA_ExpNat();
  FSINTEGRA_ExpConhTip := Geral.SINTEGRA_ExpConhTip();
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
{$IfNDef SemNFe_0000}
  F_indPag   := UnNFe_PF.ListaIndicadorDeFormaDePagamento();
  F_tpNF     := UnNFe_PF.ListaTipoDocFiscal();
  F_tpEmis   := UnNFe_PF.ListaTipoDeEmiss�oDaNFe();
  F_indPres  := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_tpAmb    := UnNFe_PF.ListaIdentificacaoDoAmbiente();
  F_finNFe   := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe();
  F_indFinal := UnNFe_PF.ListaOperacaoComConsumidorFinal();
  F_idDest   := UnNFe_PF.ListaIdentificacaoDoLocalDeDestinoDaOperacao();
  //
  F_tPag      := UnNFe_PF.ListaMeiosDePagamento();
  F_tpIntegra := UnNFe_PF.ListaTiposDeIntegracaoParaPagamento();
  F_tBand     := UnNFe_PF.ListaBandeirasDaOperadoraDeCartao();
{$Else}
  F_indPag   := 0;
  F_tpNF     := 0;
  F_tpEmis   := 0;
  F_indPres  := 0;
  F_tpAmb    := 0;
  F_finNFe   := 0;
  F_indFinal := 0;
  F_idDest   := 0;
  //
  F_tPag      := 0;
  F_tpIntegra := 0;
  F_tBand     := 0;
{$EndIf}
  CBRegrFiscal.ListSource := DmPediVda.DsFisRegCad;
  CBCartEmis.ListSource := DmPediVda.DsCartEmis;
  CBTabelaPrc.ListSource := DmPediVda.DsTabePrcCab;
  CBCondicaoPG.ListSource := DmPediVda.DsPediPrzCab;
end;

procedure TFmNFeCabA_0000.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    //
    if EdSINTEGRA_ExpNat.Focused or EdSINTEGRA_ExpNat_TXT.Focused then
      EdSINTEGRA_ExpNat.Text := Geral.SelecionaItem(FSINTEGRA_ExpNat, 0,
        'SEL-LISTA-000 :: Sele��o da Natureza de Exporta��o', TitCols, Screen.Width);
    //
    if EdSINTEGRA_ExpConhTip.Focused or EdSINTEGRA_ExpConhTip_TXT.Focused then
      EdSINTEGRA_ExpConhTip.Text := Geral.SelecionaItem(FSINTEGRA_ExpConhTip, 0,
        'SEL-LISTA-000 :: Sele��o do Tipo de Conhecimento de Transporte', TitCols, Screen.Width);
    // parei aqui! Fazer os outros
  end;
end;

procedure TFmNFeCabA_0000.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeCabAIDCtrl.Value, LaRegistro.Caption);
end;

procedure TFmNFeCabA_0000.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeCabA_0000.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNFeCabACodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeCabA_0000.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeCabA_0000.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeCabA_0000.QrNFeCabAAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
  Texto: String;
begin
  Habilita := DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value);
  FSoPermiteIncluirPelaSenhaAdmin := not Habilita;
  //BtNFeCabB.Enabled := Habilita;
  //BtNFeItsI_U.Enabled := Habilita;
  //BtNFeCabY.Enabled := Habilita;
  //
  ReopenNFeCabB(0);
  ReopenNFeCabF(0);
  ReopenNFeCabG(0);
  ReopenNFeCabGA(0);
  ReopenNFeCabY(0);
  ReopenNFeCabYA(0);
  ReopenNFeCabXReb(0);
  ReopenNFeCabXVol(0);
  ReopenNFeCabZCon(0);
  ReopenNFeCabZFis(0);
  ReopenNFeCabZPro(0);
  ReopenNFeItsI(0);
  //
  //
(*
  MeDBInfAdic_InfAdFisco.Text := QrNFeCabAInfAdic_InfAdFisco.Value;
  MeDBInfAdic_InfCpl.Text := QrNFeCabAInfAdic_InfCpl.Value;
*)
  MeDBInfAdic_InfAdFisco.Text := DMod.MyDB.SelectStringDef(
  'SELECT InfAdic_InfAdFisco FROM nfecaba ' +
  ' WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value) +
  ' AND FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value) +
  ' AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
  '');
  MeDBInfAdic_InfCpl.Text := DMod.MyDB.SelectStringDef(
  'SELECT InfAdic_InfCpl FROM nfecaba ' +
  ' WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value) +
  ' AND FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value) +
  ' AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
  '');
end;

procedure TFmNFeCabA_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Edide_tpNF_TXT.ValueVariant    :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF,    Edide_tpNF.ValueVariant);
  Edide_indPag_TXT.ValueVariant  :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPag,  Edide_indPag.ValueVariant);
  Edide_procEmi_TXT.ValueVariant :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_procEmi, Edide_procEmi.ValueVariant);
  Edide_finNFe_TXT.ValueVariant  :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
  Edide_tpAmb_TXT.ValueVariant   :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,   Edide_tpAmb.ValueVariant);
  Edide_tpEmis_TXT.ValueVariant  :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpEmis,  Edide_tpEmis.ValueVariant);
  Edide_tpImp_TXT.ValueVariant   :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpImp,   Edide_tpImp.ValueVariant);
  EdModFrete_TXT.ValueVariant    :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete,    EdModFrete.ValueVariant);
  //
  MostraSubForm();
end;

procedure TFmNFeCabA_0000.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrNFeCabAIDCtrl.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeCabA', Dmod.MyDB, CO_VAZIO));
}
  MyObjects.FormShow(TFmNFeCabA_0000_Pesq, FmNFeCabA_0000_Pesq);
end;

procedure TFmNFeCabA_0000.SbRegrFiscalClick(Sender: TObject);
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  FisRegCad    := EdRegrFiscal.ValueVariant;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdRegrFiscal, CBRegrFiscal, DmPedivda.QrFisRegCad,
      VAR_CADASTRO);
end;

procedure TFmNFeCabA_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabA_0000.GerenciaNFeCabY;
var
  TmpTable, nDup, dVenc, Descricao: String;
  Controle, FatID, FatNum, FatParcela, Empresa, Lancto, Sub, Genero: Integer;
  vDup: Double;
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  TmpTable := UnCreateNFe.RecriaTempTableNovo(ntrttNFeCabY, DmodG.QrUpdPID1, False);
  //
  if TmpTable = '' then
  begin
    Geral.MB_Erro('A��o abortada!' + sLineBreak + 'Falha ao criar tabela tempor�ria!');
    Exit;
  end;
  try
    QrNFeCabY.DisableControls;
    //
    QrNFeCabY.First;
    //
    while not QrNFeCabY.Eof do
    begin
      nDup       := QrNFeCabYnDup.Value;
      dVenc      := Geral.FDT(QrNFeCabYdVenc.Value, 01);
      vDup       := QrNFeCabYvDup.Value;
      Lancto     := QrNFeCabYLancto.Value;
      Sub        := QrNFeCabYSub.Value;
      FatID      := QrNFeCabYFatID.Value;
      FatNum     := QrNFeCabYFatNum.Value;
      FatParcela := QrNFeCabYFatParcela.Value;
      Empresa    := QrNFeCabYEmpresa.Value;
      Controle   := QrNFeCabYControle.Value;
      Genero     := QrNFeCabYGenero.Value;
      Descricao  := QrNFeCabYDescricao.Value;
      //
      if not UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, TmpTable, False,
        ['nDup', 'dVenc', 'vDup', 'Lancto', 'Sub', 'Genero', 'Descricao'],
        ['FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'],
        [nDup, dVenc, vDup, Lancto, Sub, Genero, Descricao],
        [FatID, FatNum, FatParcela, Empresa, Controle], False) then
      begin
        Geral.MB_Erro('A��o abortada!' + sLineBreak + 'Falha ao carregar faturas!');
        Exit;
      end;
      //
      QrNFeCabY.Next;
    end;
  finally
    FatNum := QrNFeCabAFatNum.Value;
    //
    DmPediVda.ReopenFatPedCab(FatNum, False);
    //
    QrNFeCabY.EnableControls;
  end;
  if DBCheck.CriaFm(TFmNfeCabYAll_0000, FmNfeCabYAll_0000, afmoNegarComAviso) then
  begin
    FmNfeCabYAll_0000.FTmpTable   := TmpTable;
    FmNfeCabYAll_0000.FFatID      := QrNFeCabAFatID.Value;
    FmNfeCabYAll_0000.FFatNum     := QrNFeCabAFatNum.Value;
    FmNfeCabYAll_0000.FEmpresa    := QrNFeCabAEmpresa.Value;
    FmNfeCabYAll_0000.FNFeVal     := QrNFeCabACobr_Fat_vOrig.Value;
    FmNfeCabYAll_0000.FFinanceiro := DmPediVda.QrFatPedCabFinanceiro.Value;
    //
    FmNfeCabYAll_0000.FJurosMes   := QrNFeCabAJurosMes.Value;
    FmNfeCabYAll_0000.FDataFat    := QrNFeCabAide_dEmi.Value;
    FmNfeCabYAll_0000.FTipoCart   := QrNFeCabATP_CART.Value;
    FmNfeCabYAll_0000.FCarteira   := QrNFeCabACartEmiss.Value;
    FmNfeCabYAll_0000.FNotaFiscal := QrNFeCabAide_nNF.Value;
    FmNfeCabYAll_0000.FRepresen   := DmPediVda.QrFatPedCabRepresen.Value;
    FmNfeCabYAll_0000.FCliente    := DmPediVda.QrFatPedCabCliente.Value;
    FmNfeCabYAll_0000.FSerieNF    := QrNFeCabAide_serie.Value;
    //
    FmNfeCabYAll_0000.ShowModal;
    FmNfeCabYAll_0000.Destroy;
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
  end;
end;

procedure TFmNFeCabA_0000.Gerenciar1Click(Sender: TObject);
begin
  GerenciaNFeCabY();
end;

procedure TFmNFeCabA_0000.Incluialtera1Click(Sender: TObject);
var
  SQLType: TSQLType;
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  PageControl1.ActivePageIndex := 7;
  if QrNFeCabF.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  UmyMod.FormInsUpd_Show(TFmNFeCabF_0000, FmNFeCabF_0000, afmoNegarComAviso,
    QrNFeCabF, SQLType);
end;

procedure TFmNFeCabA_0000.IncluiAltera2Click(Sender: TObject);
var
  SQLType: TSQLType;
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  PageControl1.ActivePageIndex := 8;
  if QrNFeCabG.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  UmyMod.FormInsUpd_Show(TFmNfeCabG_0000, FmNfeCabG_0000, afmoNegarComAviso,
    QrNFeCabG, SQLType);
end;

procedure TFmNFeCabA_0000.IncluiCampoLivre1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZCon(stIns, QrNFeCabA, QrNFeCabZCon, 10);
end;

procedure TFmNFeCabA_0000.IncluiCampoLivre2Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZFis(stIns, QrNFeCabA, QrNFeCabZFis, 10);
end;

procedure TFmNFeCabA_0000.IncluiLacre1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXLac(stIns, QrNFeCabXVol, QrNFeCabXLac, 5000);
end;

procedure TFmNFeCabA_0000.IncluiNFreferenciada1Click(Sender: TObject);
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  UmyMod.FormInsUpd_Cria(TFmNFeCabB_0000, FmNFeCabB_0000, afmoNegarComAviso,
    QrNFeCabB, stIns);
  FmNFeCabB_0000.FFatID    := QrNFeCabAFatID.Value;
  FmNFeCabB_0000.FFatNum   := QrNFeCabAFatNum.Value;
  FmNFeCabB_0000.FEmpresa  := QrNFeCabAEmpresa.Value;
  FmNFeCabB_0000.FControle := QrNFeCabBControle.Value;
  FmNFeCabB_0000.FQryIts   := QrNFeCabB;
  //
  FmNFeCabB_0000.ShowModal;
  FmNFeCabB_0000.Destroy;
end;

procedure TFmNFeCabA_0000.Incluinovacobrana1Click(Sender: TObject);
var
  FatID, FatNum, Empresa, Parcela: Integer;
  nDup: String;
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if DBCheck.CriaFm(TFmNfeCabY_0000, FmNfeCabY_0000, afmoNegarComAviso) then
  begin
    FmNfeCabY_0000.ImgTipo.SQLType := stIns;
    FatID   := QrNFeCabAFatId.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    FmNfeCabY_0000.EdFatID   .ValueVariant := FatID;
    FmNfeCabY_0000.EdFatNum  .ValueVariant := FatNum;
    FmNfeCabY_0000.EdEmpresa .ValueVariant := Empresa;
    //
    DmNFe_0000.QrParcela.Close;
    DmNFe_0000.QrParcela.Params[00].AsInteger := FatID;
    DmNFe_0000.QrParcela.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrParcela.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrParcela.Open;
    Parcela := DmNFe_0000.QrParcelaFatParcela.Value + 1;
    //Parcela := QrNFeCabY.RecordCount + 1;
    //
    FmNfeCabY_0000.EdFatParcela.ValueVariant := Parcela;
    nDup := 'FA-' + FormatFloat('000000', FatNum) + '/' + FormatFloat('000', Parcela);
    FmNfeCabY_0000.EdnDup.ValueVariant := nDup;
    //
    FmNfeCabY_0000.ShowModal;
    FmNfeCabY_0000.Destroy;
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
  end;
end;

procedure TFmNFeCabA_0000.IncluinovaNFe1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  if DmodG.QrFiliLog.RecordCount <> 1 then
    Geral.MensagemBox(
      'Para inclus�o de NF-e � necess�rio estar logado a uma empresa!',
      'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    DModG.ReopenParamsEmp(DModG.QrFiliLogCodigo.Value);
    UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeCabA, [PainelDados],
      [PainelEdita], EdRegrFiscal, ImgTipo, 'nfecaba');
    EdFatID.ValueVariant   := 999999999;
    EdFatNum.ValueVariant  := 0;
    EdEmpresa.ValueVariant := DModG.QrFiliLogCodigo.Value;
    Edide_verProc.Text     := DBCheck.Obtem_verProc;
    Edide_natOp.Text       := '';
    Edide_mod.Text         := '55';
    Edversao.ValueVariant  := DModG.QrPrmsEmpNFeversao.Value;
  end;
end;

procedure TFmNFeCabA_0000.Incluinovoitemdeatributo1Click(Sender: TObject);
begin
{
  UmyMod.FormInsUpd_Show(TFmGraAtrIts, FmGraAtrIts, afmoNegarComAviso,
    QrGraAtrIts, stIns);
}
end;

procedure TFmNFeCabA_0000.Incluinovoproduto1Click(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  if DBCheck.CriaFm(TFmNFeItsI_0000, FmNFeItsI_0000, afmoNegarComAviso) then
  begin
    FmNFeItsI_0000.ImgTipo.SQLType := stIns;
    FatID   := QrNFeCabAFatId.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    nItem := DModG.BuscaProximoInteiro('nfeitsI', 'nItem', 'FatID', FatID,
    'FatNum', FatNum, 'Empresa', Empresa);
    FmNFeItsI_0000.EdFatID   .ValueVariant := FatID;
    FmNFeItsI_0000.EdFatNum  .ValueVariant := FatNum;
    FmNFeItsI_0000.EdEmpresa .ValueVariant := Empresa;
    FmNFeItsI_0000.EdnItem   .ValueVariant := nItem;
    //
    FmNFeItsI_0000.FAbaAtu                 := PageControl3.ActivePageIndex;
    FmNFeItsI_0000.FForm                   := 'NFeCabA_0000';
    FmNFeItsI_0000.Fdest_CNPJ              := QrNFeCabAdest_CNPJ.Value;
    FmNFeItsI_0000.Fdest_CPF               := QrNFeCabAdest_CPF.Value;
    FmNFeItsI_0000.FEmpresa                := QrNFeCabAEmpresa.Value;
    FmNFeItsI_0000.FFisRegCad              := QrNFeCabAFisRegCad.Value;
    FmNFeItsI_0000.FTabelaPrc              := QrNFeCabATabelaPrc.Value;
    FmNFeItsI_0000.FCondicaoPg             := QrNFeCabACondicaoPG.Value;
    FmNFeItsI_0000.FMedDDSimpl             := QrNFeCabAMedDDSimpl.Value;
    FmNFeItsI_0000.FMedDDReal              := QrNFeCabAMedDDReal.Value;
    //
    FmNFeItsI_0000.ShowModal;
    FmNFeItsI_0000.Destroy;
    //
    if UnNFe_PF.VersaoNFeEmUso >= 310 then
      CalculaFaturasNFs;
  end;
end;

procedure TFmNFeCabA_0000.IncluinovoServico1Click(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if DBCheck.CriaFm(TFmNFeItsU_0000, FmNFeItsU_0000, afmoNegarComAviso) then
  begin
    FmNFeItsU_0000.ImgTipo.SQLType := stIns;
    FatID   := QrNFeCabAFatId.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    nItem := DModG.BuscaProximoInteiro('nfeItsU', 'nItem', 'FatID', FatID,
    'FatNum', FatNum, 'Empresa', Empresa);
    FmNFeItsU_0000.EdFatID   .ValueVariant := FatID;
    FmNFeItsU_0000.EdFatNum  .ValueVariant := FatNum;
    FmNFeItsU_0000.EdEmpresa .ValueVariant := Empresa;
    FmNFeItsU_0000.EdnItem   .ValueVariant := nItem;
    //
    FmNFeItsU_0000.ShowModal;
    FmNFeItsU_0000.Destroy;
  end;
end;

procedure TFmNFeCabA_0000.IncluiPagto;
var
  FatID, FatNum, Empresa: Integer;
begin
  PageControl1.ActivePageIndex := 12;
  //
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  FatID   := QrNFeCabAFatId.Value;
  FatNum  := QrNFeCabAFatNum.Value;
  Empresa := QrNFeCabAEmpresa.Value;
  //
  if DBCheck.CriaFm(TFmNfeCabYA_0000, FmNfeCabYA_0000, afmoNegarComAviso) then
  begin
    FmNfeCabYA_0000.ImgTipo.SQLType        := stIns;
    FmNfeCabYA_0000.EdFatID.ValueVariant   := FatID;
    FmNfeCabYA_0000.EdFatNum.ValueVariant  := FatNum;
    FmNfeCabYA_0000.EdEmpresa.ValueVariant := Empresa;
    //
    FmNfeCabYA_0000.EdtPag.ValueVariant          := '';
    FmNfeCabYA_0000.EdtPag_TXT.ValueVariant      := '';
    FmNFeCabYA_0000.EdvPag.ValueVariant          := QrNFeCabAICMSTot_vNF.Value;
    FmNFeCabYA_0000.FvPag                        := QrNFeCabAICMSTot_vNF.Value;
    FmNFeCabYA_0000.EdvTroco.ValueVariant        := 0;
    FmNFeCabYA_0000.EdtpIntegra.ValueVariant     := 2;
    FmNFeCabYA_0000.EdtpIntegra_TXT.ValueVariant := '';
    FmNFeCabYA_0000.EdCNPJ.ValueVariant          := '';
    FmNFeCabYA_0000.EdtBand.ValueVariant         := '';
    FmNFeCabYA_0000.EdtBand_TXT.ValueVariant     := '';
    FmNFeCabYA_0000.EdcAut.ValueVariant          := '';
    //
    FmNfeCabYA_0000.ShowModal;
    FmNfeCabYA_0000.Destroy;
  end;
end;

procedure TFmNFeCabA_0000.IncluiProcessoouatoconcessrio1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabZPro(stIns, QrNFeCabA, QrNFeCabZPro, 100);
end;

procedure TFmNFeCabA_0000.IncluiReboque1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXReb(stIns, QrNFeCabA, QrNFeCabXReb, 5);
end;

procedure TFmNFeCabA_0000.IncluiVolume1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabXVol(stIns, QrNFeCabA, QrNFeCabXVol, 5000);
end;

procedure TFmNFeCabA_0000.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
//  QrGraAtrIts.Close;
  QrNFeCabB.Close;
  QrNFeCabF.Close;
  QrNFeCabG.Close;
  QrNFeCabGA.Close;
  QrNFeCabXReb.Close;
  QrNFeCabXVol.Close;
  QrNFeCabY.Close;
  QrNFeCabYA.Close;
  QrSUMNFeCabY.Close;
  QrNFeItsI.Close;
  QrNFeCabZCon.Close;
  QrNFeCabZFis.Close;
  QrNFeCabZPro.Close;
  //
  MeDBInfAdic_InfAdFisco.Text := '';
  MeDBInfAdic_InfCpl.Text := '';
end;

procedure TFmNFeCabA_0000.QrNFeCabABeforeOpen(DataSet: TDataSet);
begin
  QrNFeCabAIDCtrl.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeCabA_0000.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  QrNFeCabAide_tpNF_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF, QrNFeCabAide_tpNF.Value);
  QrNFeCabAide_indPag_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPag, QrNFeCabAide_indPag.Value);
  QrNFeCabAide_procEmi_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_procEmi, QrNFeCabAide_procEmi.Value);
  QrNFeCabAide_finNFe_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe, QrNFeCabAide_finNFe.Value);
  QrNFeCabAide_tpAmb_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, QrNFeCabAide_tpAmb.Value);
  QrNFeCabAide_tpEmis_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpEmis, QrNFeCabAide_tpEmis.Value);
  QrNFeCabAide_tpImp_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpImp, QrNFeCabAide_tpImp.Value);
  QrNFeCabAModFrete_TXT.Value :=  UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete, QrNFeCabAModFrete.Value);

{
  case QrNFeCabAide_tpNF.Value of
    0: QrNFeCabAide_tpNF_TXT.Value := 'Entrada';
    1: QrNFeCabAide_tpNF_TXT.Value := 'Sa�da';
    else QrNFeCabAide_tpNF_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_indPag.Value of
    0: QrNFeCabAide_indPag_TXT.Value := 'Pagamento � vista';
    1: QrNFeCabAide_indPag_TXT.Value := 'Pagamento � prazo';
    2: QrNFeCabAide_indPag_TXT.Value := 'Outros';
    else QrNFeCabAide_indPag_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_procEmi.Value of
    0: QrNFeCabAide_procEmi_TXT.Value := 'Emiss�o de NF-e com aplicativo do contribuinte.';
    1: QrNFeCabAide_procEmi_TXT.Value := 'Emiss�o de NF-e avulsa pelo Fisco.';
    2: QrNFeCabAide_procEmi_TXT.Value := 'Emiss�o de NF-e avulsa, pelo contribuinte com seu certificado digital, atrav�s do site do Fisco.';
    3: QrNFeCabAide_procEmi_TXT.Value := 'Emiss�o NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.';
    else QrNFeCabAide_procEmi_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_finNFe.Value of
    1: QrNFeCabAide_finNFe_TXT.Value := 'NF-e normal';
    2: QrNFeCabAide_finNFe_TXT.Value := 'NF-e complementar';
    3: QrNFeCabAide_finNFe_TXT.Value := 'NF-e de ajuste';
    else QrNFeCabAide_finNFe_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_tpAmb.Value of
    1: QrNFeCabAide_tpAmb_TXT.Value := 'Produ��o';
    2: QrNFeCabAide_tpAmb_TXT.Value := 'Homologa��o';
    else QrNFeCabAide_tpAmb_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_tpEmis.Value of
    1: QrNFeCabAide_tpEmis_TXT.Value := 'Normal � emiss�o normal';
    2: QrNFeCabAide_tpEmis_TXT.Value := 'Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a';
    3: QrNFeCabAide_tpEmis_TXT.Value := 'Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN';
    4: QrNFeCabAide_tpEmis_TXT.Value := 'Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC';
    5: QrNFeCabAide_tpEmis_TXT.Value := 'Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA)';
    else QrNFeCabAide_tpEmis_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAide_tpImp.Value of
    1: QrNFeCabAide_tpImp_TXT.Value := 'Retrato';
    2: QrNFeCabAide_tpImp_TXT.Value := 'Paisagem';
    else QrNFeCabAide_tpImp_TXT.Value := '? ? ?';
  end;
  case QrNFeCabAModFrete.Value of
    0: QrNFeCabAModFrete_TXT.Value := 'Por conta do emitente';
    1: QrNFeCabAModFrete_TXT.Value := 'Por conta do destinat�rio';
    else QrNFeCabAModFrete_TXT.Value := '? ? ?';
  end;
}
end;

procedure TFmNFeCabA_0000.QrNFeCabGACalcFields(DataSet: TDataSet);
begin
  QrNFeCabGAITEM.Value := QrNFeCabGA.RecNo;
end;

procedure TFmNFeCabA_0000.QrNFeCabXVolAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeCabXLac(0);
end;

procedure TFmNFeCabA_0000.QrNFeCabXVolBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabXLac.Close;
end;

procedure TFmNFeCabA_0000.QrNFeCabYACalcFields(DataSet: TDataSet);
var
  tPag, tpIntegra, tBand, CNPJ: String;
begin
  Geral.DescricaoDeArrStrStr(Geral.FF0(QrNFeCabYAtPag.Value), F_tPag, tPag, 0, 1);
  Geral.DescricaoDeArrStrStr(Geral.FF0(QrNFeCabYAtpIntegra.Value), F_tpIntegra, tpIntegra, 0, 1);
  Geral.DescricaoDeArrStrStr(Geral.FF0(QrNFeCabYAtBand.Value), F_tBand, tBand, 0, 1);
  //
  CNPJ := Geral.FormataCNPJ_TT(QrNFeCabYACNPJ.Value);
  //
  QrNFeCabYAtPag_TXT.Value      := tPag;
  QrNFeCabYAtpIntegra_TXT.Value := tpIntegra;
  QrNFeCabYAtBand_TXT.Value     := tBand;
  QrNFeCabYACNPJ_TXT.Value      := CNPJ;
end;

procedure TFmNFeCabA_0000.QrNFeItsI03AfterOpen(DataSet: TDataSet);
begin
  BtNFeItsI03_Altera.Enabled := QrNFeItsI03.RecordCount > 0;
  BtNFeItsI03_Exclui.Enabled := QrNFeItsI03.RecordCount > 0;
end;

procedure TFmNFeCabA_0000.QrNFeItsIAfterOpen(DataSet: TDataSet);
begin
  BtIncluiItsIDI.Enabled     := QrNFeItsI.RecordCount > 0;
  BtiNVE_Inclui.Enabled      := QrNFeItsI.RecordCount > 0;
  BtNFeItsI03_Inclui.Enabled := QrNFeItsI.RecordCount > 0;
end;

procedure TFmNFeCabA_0000.QrNFeItsIAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeItsM(0);
  ReopenNFeItsN(0);
  ReopenNFeItsNA(0);
  ReopenNFeItsO(0);
  ReopenNFeItsP(0);
  ReopenNFeItsQ(0);
  ReopenNFeItsR(0);
  ReopenNFeItsS(0);
  ReopenNFeItsT(0);
  ReopenNFeItsU(0);
  ReopenNFeItsV(0);
  ReopenNFeItsIDI(0);
  ReopenNFeItsINVE(0);
  ReopenNFeItsI03(0);
end;

procedure TFmNFeCabA_0000.QrNFeItsIBeforeClose(DataSet: TDataSet);
begin
  QrNFeItsM.Close;
  QrNFeItsN.Close;
  QrNFeItsNA.Close;
  QrNFeItsO.Close;
  QrNFeItsP.Close;
  QrNFeItsQ.Close;
  QrNFeItsR.Close;
  QrNFeItsS.Close;
  QrNFeItsT.Close;
  QrNFeItsU.Close;
  QrNFeItsV.Close;
  //
  QrNFeItsIDI.Close;
  QrNFeItsINVE.Close;
  QrNFeItsI03.Close;
  //
  BtIncluiItsIDI.Enabled := False;
end;

procedure TFmNFeCabA_0000.QrNFeItsIDIaAfterOpen(DataSet: TDataSet);
begin
  BtAlteraItsIDIa.Enabled := QrNFeItsIDIa.RecordCount > 0;
  BtExcluiItsIDIa.Enabled := QrNFeItsIDIa.RecordCount > 0;
end;

procedure TFmNFeCabA_0000.QrNFeItsIDIaBeforeClose(DataSet: TDataSet);
begin
  BtAlteraItsIDIa.Enabled := False;
  BtExcluiItsIDIa.Enabled := False;
end;

procedure TFmNFeCabA_0000.QrNFeItsIDIAfterOpen(DataSet: TDataSet);
begin
  BtIncluiItsIDIa.Enabled := QrNfeItsIDI.RecordCount > 0;
  //
  BtAlteraItsIDI.Enabled := QrNfeItsIDI.RecordCount > 0;
  BtExcluiItsIDI.Enabled := QrNfeItsIDI.RecordCount > 0;
end;

procedure TFmNFeCabA_0000.QrNFeItsIDIAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeItsIDIa(0);
end;

procedure TFmNFeCabA_0000.QrNfeItsIDIBeforeClose(DataSet: TDataSet);
begin
  BtIncluiItsIDIa.Enabled := False;
  BtAlteraItsIDIa.Enabled := False;
  BtExcluiItsIDIa.Enabled := False;
end;

procedure TFmNFeCabA_0000.QrNFeItsINVEAfterOpen(DataSet: TDataSet);
begin
  BtiNVE_Altera.Enabled := QrNFeItsINVE.RecordCount > 0;
  BtiNVE_Exclui.Enabled := QrNFeItsINVE.RecordCount > 0;
end;

procedure TFmNFeCabA_0000.ReGeraarquivoXML1Click(Sender: TObject);
const
  GravaCampos = ID_NO;
var
  Status, FatID, FatNum, Empresa, VersaoNFe: Integer;
  XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir: String;
  Continua: Boolean;
begin
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando chave de acesso');
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inciando processo');
    //
    FatID      := QrNFeCabAFatID.Value;
    FatNum     := QrNFeCabAFatNum.Value;
    Empresa    := QrNFeCabAEmpresa.Value;
    //IDCtrl     := QrNFeCabAIDCtrl.Value;
    {
    FreteVal   := QrNFeCabAFreteVal.Value;
    Seguro     := QrNFeCabASeguro.Value;
    Outros     := QrNFeCabAOutros.Value;
    RegrFiscal := QrNFeCabARegrFiscal.Value;
    }
    XMLGerado_Arq   := '';
    XMLGerado_Dir   := '';
    XMLAssinado_Dir := '';
    //Continua        := False;
    //
    Status := QrNFeCabAStatus.Value;
    //if Continua or (Status = DmNFe_0000.stepNFeDados) then
    //begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Montando arquivo XML');

    VersaoNFe := Trunc((QrNFeCabAversao.Value + 0.009) * 100);
    //
    Continua := UnNFe_PF.CriarDocuNFe(VersaoNFe, FatID, FatNum, Empresa,
             XMLGerado_Arq, XMLGerado_Dir, LaAviso1, LaAviso2, GravaCampos);
    if Continua then
      Continua := DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
        DmNFe_0000.stepNFeGerada, LaAviso1, LaAviso2);
    //end;
    if Continua or (Status = DmNFe_0000.stepNFeGerada) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados para assinatura do arquivo XML');
      // Assinar NF-e
      //Continua := False;
      if XMLGerado_Arq = '' then
      begin
        Status := DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
        if Status = DmNFe_0000.stepNFeGerada then
        begin
          DmNFe_0000.ReopenEmpresa(Empresa);
          //
          XMLGerado_Arq := QrNFECabAId.Value + NFE_EXT_NFE_XML;
          XMLGerado_Dir := DmNFe_0000.QrFilialDirNFeGer.Value;
        end;
      end;
      if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
      if XMLGerado_Arq = '' then
        Geral.MensagemBox('Nome do arquivo da ' +
        'NF-e gerada indefinido!', 'Aviso', MB_OK+MB_ICONWARNING)
      else
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML');
        Continua := DmNFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir,
          Empresa, 0, QrNFECabAIDCtrl.Value, XMLAssinado_Dir);
      if Continua then
        //Continua :=
        DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
          DmNFe_0000.stepNFeAssinada, LaAviso1, LaAviso2);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeCabA_0000.ReopenNFeCabB(Controle: Integer);
begin
  QrNFeCabB.Close;
  QrNFeCabB.Database := DMod.MyDB;
  QrNFeCabB.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeCabB.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeCabB.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeCabB.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeCabF(Controle: Integer);
begin
  QrNFeCabF.Close;
  QrNFeCabF.Database := DMod.MyDB;
  QrNFeCabF.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeCabF.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeCabF.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeCabF.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeCabG(Controle: Integer);
begin
  QrNFeCabG.Close;
  QrNFeCabG.Database := DMod.MyDB;
  QrNFeCabG.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeCabG.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeCabG.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeCabG.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeCabGA(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabGA, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, autXML_CNPJ, autXML_CPF) CNPJ_CPF ',
  'FROM nfecabga cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
  'AND cga.FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND cga.Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabGA.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabXLac(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXLac, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxlac ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeCabXVolControle.Value),
  '']);
  //
  if Conta <> 0 then
    QrNFeCabXLac.Locate('Conta', Conta, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabXReb(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXReb, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxreb ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabXReb.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabXVol(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabXVol, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabxvol ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabXVol.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabYA(Controle: Integer);
var
  FatID, FatNum, Empresa: Integer;
begin
  FatID   := QrNFeCabAFatID.Value;
  FatNum  := QrNFeCabAFatNum.Value;
  Empresa := QrNFeCabAEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabYA, Dmod.MyDB, [
    'SELECT * ',
    'FROM nfecabya ',
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
  //
  if Controle <> 0 then
    QrNFeCabYA.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabY(Controle: Integer);
  var
{$IFDEF DEFINE_VARLCT}
    Empresa, Filial: Integer;
{$ENDIF}
    TabLctA: String;
begin
{$IFDEF DEFINE_VARLCT}
  Empresa := QrNFeCabAEmpresa.Value;
  Filial  := DModG.ObtemFilialDeEntidade(Empresa);
  TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
{$ELSE}
  TabLctA := VAR_LCT;
{$ENDIF}
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabY, Dmod.MyDB, [
    'SELECT nfey.*, lct.Genero, lct.Descricao ',
    'FROM nfecaby nfey ',
    'LEFT JOIN ' + TabLctA + ' lct ON ',
    '( ',
    'lct.Controle = nfey.Lancto ',
    'AND lct.FatID = nfey.FatID ',
    'AND lct.FatNum = nfey.FatNum ',
    'AND lct.CliInt = nfey.Empresa) ',
    'WHERE nfey.FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
    'AND nfey.FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
    'AND nfey.Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
    'ORDER BY nfey.dVenc, nfey.nDup ',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSUMNFeCabY, Dmod.MyDB, [
    'SELECT SUM(vDup) vDup ',
    'FROM nfecaby nfey ',
    'LEFT JOIN ' + TabLctA + ' lct ON ',
    '( ',
    'lct.Controle = nfey.Lancto ',
    'AND lct.FatID = nfey.FatID ',
    'AND lct.FatNum = nfey.FatNum ',
    'AND lct.CliInt = nfey.Empresa) ',
    'WHERE nfey.FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
    'AND nfey.FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
    'AND nfey.Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
    'ORDER BY nfey.dVenc, nfey.nDup ',
    '']);
  if Controle <> 0 then
    QrNFeCabY.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabZCon(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZCon, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzcon ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabZCon.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabZFis(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZFis, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzfis ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabZCon.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeCabZPro(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabZPro, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecabzpro ',
  'WHERE FatID='  + Geral.FF0(QrNFeCabAFatID.Value),
  'AND FatNum='   + Geral.FF0(QrNFeCabAFatNum.Value),
  'AND Empresa='  + Geral.FF0(QrNFeCabAEmpresa.Value),
  '']);
  //
  if Controle <> 0 then
    QrNFeCabZCon.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsI(Controle: Integer);
begin
  QrNFeItsI.Close;
  QrNFeItsI.Database := DMod.MyDB;
  QrNFeItsI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeItsI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeItsI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeItsI.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsI03(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI03, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsi03 ',
  'WHERE FatID=' + Geral.FF0(QrNFeItsIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeItsIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeItsIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrNFeItsInItem.Value),
  '']);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsIDI(Controle: Integer);
begin
  QrNFeItsIDI.Close;
  QrNFeItsIDI.Database := DMod.MyDB;
  QrNFeItsIDI.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsIDI.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsIDI.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsIDI.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsIDI.Open;
  //
  QrNFeItsIDI.Locate('Controle', Controle, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsIDIa(Conta: Integer);
begin
(*

  QrNFeItsIDIa.Close;
  QrNFeItsIDIa.Params[00].AsInteger := QrNFeItsIDIFatID.Value;
  QrNFeItsIDIa.Params[01].AsInteger := QrNFeItsIDIFatNum.Value;
  QrNFeItsIDIa.Params[02].AsInteger := QrNFeItsIDIEmpresa.Value;
  QrNFeItsIDIa.Params[03].AsInteger := QrNFeItsIDInItem.Value;
  QrNFeItsIDIa.Params[04].AsInteger := QrNFeItsIDIControle.Value;
  QrNFeItsIDIa.Open;
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsIDIa, Dmod.MyDB, [
  'SELECT nfedia.Adi_nDraw + 0.000 Adi_nDraw, nfedia.* ',
  'FROM nfeitsidia nfedia ',
  'WHERE nfedia.FatID=' + Geral.FF0(QrNFeItsIDIFatID.Value),
  'AND nfedia.FatNum=' + Geral.FF0(QrNFeItsIDIFatNum.Value),
  'AND nfedia.Empresa=' + Geral.FF0(QrNFeItsIDIEmpresa.Value),
  'AND nfedia.nItem=' + Geral.FF0(QrNFeItsIDInItem.Value),
  'AND nfedia.Controle=' + Geral.FF0(QrNFeItsIDIControle.Value),
  '']);
  QrNFeItsIDIa.Locate('Conta', Conta, []);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsINVE(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsINVE, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsinve ',
  'WHERE FatID=' + Geral.FF0(QrNFeItsIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeItsIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeItsIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrNFeItsInItem.Value),
  '']);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsM(nItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsM, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsm ',
  'WHERE FatID=' + Geral.FF0(QrNFeItsIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeItsIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeItsIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrNFeItsInItem.Value),
  '']);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsN(nItem: Integer);
begin
  QrNFeItsN.Close;
  QrNFeItsN.Database := DMod.MyDB;
  QrNFeItsN.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsN.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsN.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsN.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsN.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsNA(nItem: Integer);
begin
  QrNFeItsNA.Close;
  QrNFeItsNA.Database := DMod.MyDB;
  QrNFeItsNA.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsNA.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsNA.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsNA.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsNA.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsO(nItem: Integer);
begin
  QrNFeItsO.Close;
  QrNFeItsO.Database := DMod.MyDB;
  QrNFeItsO.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsO.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsO.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsO.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsO.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsP(nItem: Integer);
begin
  QrNFeItsP.Close;
  QrNFeItsP.Database := DMod.MyDB;
  QrNFeItsP.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsP.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsP.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsP.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsP.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsQ(nItem: Integer);
begin
  QrNFeItsQ.Close;
  QrNFeItsQ.Database := DMod.MyDB;
  QrNFeItsQ.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsQ.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsQ.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsQ.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsQ.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsR(nItem: Integer);
begin
  QrNFeItsR.Close;
  QrNFeItsR.Database := DMod.MyDB;
  QrNFeItsR.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsR.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsR.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsR.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsR.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsS(nItem: Integer);
begin
  QrNFeItsS.Close;
  QrNFeItsS.Database := DMod.MyDB;
  QrNFeItsS.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsS.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsS.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsS.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsS.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsT(nItem: Integer);
begin
  QrNFeItsT.Close;
  QrNFeItsT.Database := DMod.MyDB;
  QrNFeItsT.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsT.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsT.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsT.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsT.Open;
end;

procedure TFmNFeCabA_0000.ReopenNFeItsU(nItem: Integer);
begin
{
  QrNFeItsU.Close;
  QrNFeItsU.Params[00].AsInteger := ;
  QrNFeItsU.Params[01].AsInteger := ;
  QrNFeItsU.Params[02].AsInteger := ;
  QrNFeItsU.Params[03].AsInteger := ;
  QrNFeItsU.Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsU, DMod.MyDB, [
  'SELECT muni.Nome NO_ISSQN_cMunFG, ',
  'cnae.Nome  NO_ISSQN_cListServ, nfeu.* ',
  'FROM nfeitsu nfeu ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici muni ON muni.Codigo=nfeu.ISSQN_cMunFG ',
{ TODO : CNAE ou ListServ????? }  
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21Cad cnae ON cnae.CodAlf=nfeu.ISSQN_cListServ ',
  'WHERE FatID=' + Geral.FF0(QrNFeItsIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeItsIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeItsIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrNFeItsInItem.Value),
  '']);
end;

procedure TFmNFeCabA_0000.ReopenNFeItsV(nItem: Integer);
begin
  QrNFeItsV.Close;
  QrNFeItsV.Database := DMod.MyDB;
  QrNFeItsV.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsV.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsV.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsV.Params[03].AsInteger := QrNFeItsInItem.Value;
  QrNFeItsV.Open;
end;

(*
object QrNFeCabAICMSTot_vFCPUFDest: TFloatField
  FieldName = 'ICMSTot_vFCPUFDest'
end
object QrNFeCabAICMSTot_vICMSUFDest: TFloatField
  FieldName = 'ICMSTot_vICMSUFDest'
end
object QrNFeCabAICMSTot_vICMSUFRemet: TFloatField
  FieldName = 'ICMSTot_vICMSUFRemet'
end
*)

end.

