unit NFeItsI_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DB,
  mySQLDbTables, dmkMemo, dmkRadioGroup, ComCtrls, dmkEdit, dmkCheckBox,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmNFeItsI_0000 = class(TForm)
    Panel25: TPanel;
    Panel3: TPanel;
    Label231: TLabel;
    Label235: TLabel;
    Label236: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label232: TLabel;
    Label234: TLabel;
    SpeedButton1: TSpeedButton;
    EdnItem: TdmkEdit;
    Edprod_NCM: TdmkEdit;
    Edprod_EXTIPI: TdmkEdit;
    Edprod_genero: TdmkEdit;
    Edprod_CFOP: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdFatID: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    Panel4: TPanel;
    GroupBox11: TGroupBox;
    Label242: TLabel;
    Label241: TLabel;
    Label240: TLabel;
    Label239: TLabel;
    Label233: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    Label249: TLabel;
    Edprod_qCom: TdmkEdit;
    Edprod_vProd: TdmkEdit;
    Edprod_vUnCom: TdmkEdit;
    Edprod_uCom: TdmkEdit;
    Edprod_cEAN: TdmkEdit;
    Edprod_vFrete: TdmkEdit;
    Edprod_vSeg: TdmkEdit;
    Edprod_vDesc: TdmkEdit;
    GroupBox12: TGroupBox;
    Label001: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edprod_qTrib: TdmkEdit;
    Edprod_vUnTrib: TdmkEdit;
    Edprod_uTrib: TdmkEdit;
    Edprod_cEANTrib: TdmkEdit;
    PageControl3: TPageControl;
    TabSheet18: TTabSheet;
    TabSheet19: TTabSheet;
    Panel34: TPanel;
    Panel39: TPanel;
    GroupBox19: TGroupBox;
    Label265: TLabel;
    Label266: TLabel;
    Label267: TLabel;
    Label268: TLabel;
    Label273: TLabel;
    EdIPI_clEnq: TdmkEdit;
    EdIPI_CNPJProd: TdmkEdit;
    EdIPI_cSelo: TdmkEdit;
    EdIPI_qSelo: TdmkEdit;
    Panel40: TPanel;
    Label251: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    Label269: TLabel;
    Label270: TLabel;
    Label271: TLabel;
    Label272: TLabel;
    EdIPI_cEnq: TdmkEdit;
    EdIPI_pIPI: TdmkEdit;
    EdIPI_vUnid: TdmkEdit;
    EdIPI_vBC: TdmkEdit;
    EdIPI_qUnid: TdmkEdit;
    EdIPI_vIPI: TdmkEdit;
    TabSheet20: TTabSheet;
    Panel33: TPanel;
    TabSheet21: TTabSheet;
    Panel32: TPanel;
    GroupBox20: TGroupBox;
    Label274: TLabel;
    Label281: TLabel;
    EdTextoPIS_CST: TdmkEdit;
    EdPIS_CST: TdmkEdit;
    GroupBox22: TGroupBox;
    Label279: TLabel;
    Label276: TLabel;
    EdPIS_vBC: TdmkEdit;
    EdPIS_pPIS: TdmkEdit;
    GroupBox23: TGroupBox;
    Label275: TLabel;
    Label282: TLabel;
    EdPIS_qBCProd: TdmkEdit;
    EdPIS_vAliqProd: TdmkEdit;
    EdPIS_vPIS: TdmkEdit;
    GroupBox21: TGroupBox;
    Label284: TLabel;
    GroupBox24: TGroupBox;
    Label277: TLabel;
    Label278: TLabel;
    EdPISST_vBC: TdmkEdit;
    EdPISST_pPIS: TdmkEdit;
    GroupBox25: TGroupBox;
    Label280: TLabel;
    Label283: TLabel;
    EdPISST_qBCProd: TdmkEdit;
    EdPISST_vAliqProd: TdmkEdit;
    EdPISST_vPIS: TdmkEdit;
    TabSheet22: TTabSheet;
    Panel35: TPanel;
    Panel41: TPanel;
    GroupBox26: TGroupBox;
    Label285: TLabel;
    Label286: TLabel;
    EdTextoCOFINS_CST: TdmkEdit;
    EdCOFINS_CST: TdmkEdit;
    GroupBox27: TGroupBox;
    Label287: TLabel;
    Label288: TLabel;
    EdCOFINS_vBC: TdmkEdit;
    EdCOFINS_pCOFINS: TdmkEdit;
    GroupBox28: TGroupBox;
    Label289: TLabel;
    Label290: TLabel;
    EdCOFINS_qBCProd: TdmkEdit;
    EdCOFINS_vAliqProd: TdmkEdit;
    EdCOFINS_vCOFINS: TdmkEdit;
    GroupBox29: TGroupBox;
    Label291: TLabel;
    GroupBox30: TGroupBox;
    Label292: TLabel;
    Label293: TLabel;
    EdCOFINSST_vBC: TdmkEdit;
    EdCOFINSST_pCOFINS: TdmkEdit;
    GroupBox31: TGroupBox;
    Label294: TLabel;
    Label295: TLabel;
    EdCOFINSST_qBCProd: TdmkEdit;
    EdCOFINSST_vAliqProd: TdmkEdit;
    EdCOFINSST_vCOFINS: TdmkEdit;
    TabSheet24: TTabSheet;
    Panel37: TPanel;
    MeInfAdProd: TdmkMemo;
    QrPrx: TmySQLQuery;
    QrPrxnItem: TIntegerField;
    QrGGX: TmySQLQuery;
    QrGGXGraGruX: TIntegerField;
    QrGGXNO_GGX: TWideStringField;
    QrGGXNivel3: TIntegerField;
    QrGGXNivel2: TIntegerField;
    QrGGXNivel1: TIntegerField;
    QrGGXCodUsu: TIntegerField;
    QrGGXNome: TWideStringField;
    QrGGXPrdGrupTip: TIntegerField;
    QrGGXGraTamCad: TIntegerField;
    QrGGXUnidMed: TIntegerField;
    QrGGXCST_A: TSmallintField;
    QrGGXCST_B: TSmallintField;
    QrGGXNCM: TWideStringField;
    QrGGXPeso: TFloatField;
    QrGGXIPI_Alq: TFloatField;
    QrGGXIPI_CST: TSmallintField;
    QrGGXIPI_cEnq: TWideStringField;
    QrGGXTipDimens: TSmallintField;
    QrGGXPerCuztMin: TFloatField;
    QrGGXPerCuztMax: TFloatField;
    QrGGXMedOrdem: TIntegerField;
    QrGGXPartePrinc: TIntegerField;
    QrGGXInfAdProd: TWideStringField;
    QrGGXSiglaCustm: TWideStringField;
    QrGGXLk: TIntegerField;
    QrGGXDataCad: TDateField;
    QrGGXDataAlt: TDateField;
    QrGGXUserCad: TIntegerField;
    QrGGXUserAlt: TIntegerField;
    QrGGXAlterWeb: TSmallintField;
    QrGGXAtivo: TSmallintField;
    QrGGXHowBxaEstq: TSmallintField;
    QrGGXGerBxaEstq: TSmallintField;
    QrGGXPIS_CST: TSmallintField;
    QrGGXPIS_AlqP: TFloatField;
    QrGGXPIS_AlqV: TFloatField;
    QrGGXCOFINS_CST: TSmallintField;
    QrGGXCOFINS_AlqP: TFloatField;
    QrGGXCOFINS_AlqV: TFloatField;
    QrGGXICMS_modBC: TSmallintField;
    QrGGXICMS_modBCST: TSmallintField;
    QrGGXICMS_pRedBC: TFloatField;
    QrGGXICMS_pRedBCST: TFloatField;
    QrGGXICMS_pMVAST: TFloatField;
    QrGGXICMS_pICMSST: TFloatField;
    QrGGXPISST_AlqP: TFloatField;
    QrGGXPISST_AlqV: TFloatField;
    QrGGXCOFINSST_AlqP: TFloatField;
    QrGGXCOFINSST_AlqV: TFloatField;
    QrGGXIPI_vUnid: TFloatField;
    QrGGXIPI_TpTrib: TSmallintField;
    QrGGXICMS_Pauta: TFloatField;
    QrGGXICMS_MaxTab: TFloatField;
    QrGGXIPI_pIPI: TFloatField;
    QrGGXcGTIN_EAN: TWideStringField;
    QrGGXEX_TIPI: TWideStringField;
    QrGGXSigla: TWideStringField;
    QrGGXMadeBy: TSmallintField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    Panel1: TPanel;
    Panel7: TPanel;
    GroupBox18: TGroupBox;
    Label264: TLabel;
    EdIPI_CST: TdmkEdit;
    EdTextoIPI_CST: TdmkEdit;
    GroupBox1: TGroupBox;
    CkTem_IPI: TdmkCheckBox;
    GroupBox2: TGroupBox;
    Label301: TLabel;
    EdII_vBC: TdmkEdit;
    Label302: TLabel;
    EdII_vDespAdu: TdmkEdit;
    Label303: TLabel;
    EdII_vII: TdmkEdit;
    Label304: TLabel;
    EdII_vIOF: TdmkEdit;
    GroupBox3: TGroupBox;
    CkTem_II: TdmkCheckBox;
    Label6: TLabel;
    Edprod_vOutro: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtCalcular: TBitBtn;
    BtOK: TBitBtn;
    CkCalcular: TCheckBox;
    Label7: TLabel;
    BtSaida: TBitBtn;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    GroupBox4: TGroupBox;
    CkGeraGrupoNA: TCheckBox;
    PnGrupoNA: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    Label10: TLabel;
    LapICMSInterPart: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdICMSUFDest_vBCUFDest: TdmkEdit;
    EdICMSUFDest_pFCPUFDest: TdmkEdit;
    EdICMSUFDest_pICMSUFDest: TdmkEdit;
    EdICMSUFDest_pICMSInter: TdmkEdit;
    EdICMSUFDest_pICMSInterPart: TdmkEdit;
    EdICMSUFDest_vFCPUFDest: TdmkEdit;
    EdICMSUFDest_vICMSUFDest: TdmkEdit;
    EdICMSUFDest_vICMSUFRemet: TdmkEdit;
    Panel10: TPanel;
    Ckprod_indTot: TdmkCheckBox;
    Edprod_CEST: TdmkEdit;
    Label16: TLabel;
    Panel11: TPanel;
    Panel12: TPanel;
    REWarning: TRichEdit;
    GroupBox5: TGroupBox;
    Panel13: TPanel;
    Label17: TLabel;
    Edprod_xPed: TdmkEdit;
    Label18: TLabel;
    Edprod_nItemPed: TdmkEdit;
    Edprod_nFCI: TdmkEdit;
    Label19: TLabel;
    Label20: TLabel;
    Panel31: TPanel;
    GroupBox13: TGroupBox;
    Panel38: TPanel;
    Label244: TLabel;
    EdICMS_Orig: TdmkEdit;
    EdTextoA: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel14: TPanel;
    GroupBox14: TGroupBox;
    Panel6: TPanel;
    RGICMS_modBC: TdmkRadioGroup;
    TabSheet3: TTabSheet;
    Panel15: TPanel;
    GroupBox15: TGroupBox;
    LaICMS_pMVAST: TLabel;
    LaICMS_pRedBCST: TLabel;
    RGICMS_modBCST: TdmkRadioGroup;
    EdICMS_pRedBCST: TdmkEdit;
    EdICMS_pMVAST: TdmkEdit;
    TabSheet4: TTabSheet;
    Panel16: TPanel;
    Label259: TLabel;
    EdICMS_vBCST: TdmkEdit;
    EdICMS_pICMSST: TdmkEdit;
    LaICMS_pICMSST: TLabel;
    EdICMS_vICMSST: TdmkEdit;
    Label261: TLabel;
    GroupBox6: TGroupBox;
    Label23: TLabel;
    EdICMS_vICMSDeson: TdmkEdit;
    GroupBox8: TGroupBox;
    LaICMS_pRedBC: TLabel;
    EdICMS_pRedBC: TdmkEdit;
    LaICMS_VBC: TLabel;
    EdICMS_vBC: TdmkEdit;
    EdICMS_vICMS: TdmkEdit;
    Label258: TLabel;
    EdICMS_pICMS: TdmkEdit;
    LaICMS_pICMS: TLabel;
    Panel17: TPanel;
    EdICMS_CST: TdmkEdit;
    EdTextoB: TdmkEdit;
    Label245: TLabel;
    TabSheet5: TTabSheet;
    Panel18: TPanel;
    GroupBox10: TGroupBox;
    Panel19: TPanel;
    Panel20: TPanel;
    Label36: TLabel;
    EdICMS_CSOSN: TdmkEdit;
    EdCSOSN_Txt: TdmkEdit;
    EdICMS_pCredSN: TdmkEdit;
    LapCredSN: TLabel;
    Label26: TLabel;
    EdICMS_vCredICMSSN: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Ckprod_indEscala: TdmkCheckBox;
    Label29: TLabel;
    Edprod_CNPJFab: TdmkEdit;
    Edprod_cBenef: TdmkEdit;
    Label30: TLabel;
    EdICMS_vBCFCP: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    EdICMS_pFCP: TdmkEdit;
    Label33: TLabel;
    EdICMS_vFCP: TdmkEdit;
    Label34: TLabel;
    EdICMS_vBCFCPST: TdmkEdit;
    EdICMS_pFCPST: TdmkEdit;
    Label35: TLabel;
    Label37: TLabel;
    EdICMS_vFCPST: TdmkEdit;
    Panel5: TPanel;
    RGICMS_motDesICMS: TdmkRadioGroup;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Label21: TLabel;
    EdICMS_vICMSOp: TdmkEdit;
    EdICMS_vICMSDif: TdmkEdit;
    Label24: TLabel;
    Label22: TLabel;
    EdICMS_pDif: TdmkEdit;
    Label25: TLabel;
    EdICMS_vBCSTRet: TdmkEdit;
    EdICMS_pST: TdmkEdit;
    Label38: TLabel;
    Label27: TLabel;
    EdICMS_vICMSSTRet: TdmkEdit;
    Label39: TLabel;
    EdICMS_vBCFCPSTRet: TdmkEdit;
    Label40: TLabel;
    EdICMS_pFCPSTRet: TdmkEdit;
    Label41: TLabel;
    EdICMS_vFCPSTRet: TdmkEdit;
    Label42: TLabel;
    EdICMSUFDest_vBCFCPUFDest: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edprod_NCMChange(Sender: TObject);
    procedure Edprod_cEANChange(Sender: TObject);
    procedure Edprod_uComChange(Sender: TObject);
    procedure Edprod_qComChange(Sender: TObject);
    procedure Edprod_vUnComChange(Sender: TObject);
    procedure EdICMS_OrigChange(Sender: TObject);
    procedure EdICMS_OrigKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_CSTChange(Sender: TObject);
    procedure EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGICMS_modBCClick(Sender: TObject);
    procedure EdICMS_pRedBCChange(Sender: TObject);
    procedure EdICMS_pMVASTChange(Sender: TObject);
    procedure EdICMS_pRedBCSTChange(Sender: TObject);
    procedure EdICMS_pICMSChange(Sender: TObject);
    procedure EdICMS_pICMSSTChange(Sender: TObject);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIPI_vBCChange(Sender: TObject);
    procedure EdIPI_pIPIChange(Sender: TObject);
    procedure EdIPI_vUnidChange(Sender: TObject);
    procedure EdIPI_qUnidChange(Sender: TObject);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPIS_vBCChange(Sender: TObject);
    procedure EdPIS_pPISChange(Sender: TObject);
    procedure EdPISST_vBCChange(Sender: TObject);
    procedure EdPISST_pPISChange(Sender: TObject);
    procedure EdPIS_qBCProdChange(Sender: TObject);
    procedure EdPIS_vAliqProdChange(Sender: TObject);
    procedure EdPISST_qBCProdChange(Sender: TObject);
    procedure EdPISST_vAliqProdChange(Sender: TObject);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure EdCOFINS_vBCChange(Sender: TObject);
    procedure EdCOFINS_pCOFINSChange(Sender: TObject);
    procedure EdCOFINSST_vBCChange(Sender: TObject);
    procedure EdCOFINSST_pCOFINSChange(Sender: TObject);
    procedure EdCOFINS_qBCProdChange(Sender: TObject);
    procedure EdCOFINS_vAliqProdChange(Sender: TObject);
    procedure EdCOFINSST_qBCProdChange(Sender: TObject);
    procedure EdCOFINSST_vAliqProdChange(Sender: TObject);
    procedure Edprod_cProdEnter(Sender: TObject);
    procedure Edprod_cProdExit(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure Edprod_qTribChange(Sender: TObject);
    procedure Edprod_vUnTribChange(Sender: TObject);
    procedure RGICMS_modBCSTClick(Sender: TObject);
    procedure EdICMS_vBCChange(Sender: TObject);
    procedure EdICMS_vBCSTChange(Sender: TObject);
    procedure EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_vUnTribExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdICMS_vICMSSTExit(Sender: TObject);
    procedure EdIPI_qSeloExit(Sender: TObject);
    procedure EdII_vIOFExit(Sender: TObject);
    procedure EdPISST_vPISExit(Sender: TObject);
    procedure EdCOFINSST_vCOFINSExit(Sender: TObject);
    procedure MeInfAdProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtCalcularClick(Sender: TObject);
    procedure CkCalcularClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkGeraGrupoNAClick(Sender: TObject);
    procedure EdICMSUFDest_vBCUFDestChange(Sender: TObject);
    procedure EdICMSUFDest_pFCPUFDestChange(Sender: TObject);
    procedure EdICMSUFDest_pICMSUFDestChange(Sender: TObject);
    procedure EdICMSUFDest_pICMSInterChange(Sender: TObject);
    procedure EdICMSUFDest_pICMSInterPartChange(Sender: TObject);
    procedure EdICMS_CSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_CSOSNChange(Sender: TObject);
    procedure CkTem_IPIClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FGraGruX: Integer;
    FBC_Geral: Double;
    FCalculoAutomatico: Boolean;
    //
    procedure ConfiguraAba();
    //
    procedure CalculaBCs();
    procedure CalculaBC_ICMS();
    procedure CalculaBC_ICMSST();
    procedure CalculaICMS_OIVCF();
    procedure CalculaBC_IPI();
    procedure CalculaBC_PIS();
    procedure CalculaBC_PISST();
    procedure CalculaBC_COFINS();
    procedure CalculaBC_COFINSST();
    procedure CalculaICMS();
    procedure CalculaICMSST();
    procedure CalculaIPI();
    procedure CalculaPIS();
    procedure CalculaPISST();
    procedure CalculaCOFINS();
    procedure CalculaCOFINSST();
    procedure CalculaValorCom();
  public
    { Public declarations }
    FForm: String;// FmNFeCabA_0000
    Fdest_CNPJ: String; //FmNFeCabA_0000.QrNFeCabAdest_CNPJ.Value
    Fdest_CPF: String; //FmNFeCabA_0000.QrNFeCabAdest_CPF.Value
    FEmpresa: Integer; //FmNFeCabA_0000.QrNFeCabAEmpresa.Value
    FFisRegCad: Integer; //FmNFeCabA_0000.QrNFeCabAFisRegCad.Value
    FTabelaPrc: Integer; // FmNFeCabA_0000.QrNFeCabATabelaPrc.Value;
    FCondicaoPg: Integer; // FmNFeCabA_0000.QrNFeCabACondicaoPG.Value;
    FMedDDSimpl: Double; // FmNFeCabA_0000.QrNFeCabAMedDDSimpl.Value;
    FMedDDReal: Double; //FmNFeCabA_0000.QrNFeCabAMedDDReal.Value;
    FAbaAtu, FStqMovValA: Integer;
  end;

  var
  FmNFeItsI_0000: TFmNFeItsI_0000;

implementation

uses UnMyObjects, GraGruPesq1, MyDBCheck, ModuleNFe_0000, NFeCabA_0000,
ModuleGeral, ModProd, UnFinanceiro, dmkGeral, UMySQLModule, Module,
{$IFNDef semEntradaNFe}EntradaCab, {$EndIf} UnInternalConsts, UnMySQLCuringa,
NFe_PF;

{$R *.DFM}

procedure TFmNFeItsI_0000.BtCalcularClick(Sender: TObject);
begin
  CalculaBCs();
  CalculaBC_ICMS();
  CalculaBC_ICMSST();
  CalculaBC_IPI();
  CalculaBC_PIS();
  CalculaBC_PISST();
  CalculaBC_COFINS();
  CalculaBC_COFINSST();
  CalculaICMS();
  CalculaICMSST();
  CalculaIPI();
  CalculaPIS();
  CalculaPISST();
  CalculaCOFINS();
  CalculaCOFINSST();
  CalculaValorCom();
end;

procedure TFmNFeItsI_0000.BtOKClick(Sender: TObject);
  procedure ExcluiItem(Tabela:String; FatID, FatNum, Empresa, nItem: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + Tabela  + ' WHERE ');
    Dmod.QrUpd.SQL.Add('FatID=' + FormatFloat('0', FatID));
    Dmod.QrUpd.SQL.Add('AND FatNum=' + FormatFloat('0', FatNum));
    Dmod.QrUpd.SQL.Add('AND Empresa=' + FormatFloat('0', Empresa));
    Dmod.QrUpd.SQL.Add('AND nItem=' + FormatFloat('0', nItem));
    Dmod.QrUpd.ExecSQL;
  end;

  procedure AtualizaCamposFaturamento(StqMovValA: Integer; Qtde, Preco,
    Total: Double);
  var
    IDCtrl: Integer;
    Qry: TmySQLQuery;
  begin
    if StqMovValA <> 0 then
    begin
      Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT IDCtrl ',
          'FROM stqmovvala ',
          'WHERE ID=' + Geral.FF0(StqMovValA),
          '']);
        //
        IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False,
          ['Qtde', 'Preco', 'Total'], ['ID'],
          [Qtde, Preco, Total], [StqMovValA], False) then
        begin
          if IDCtrl <> 0 then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False,
              ['Qtde'], ['IDCtrl'],
              [Qtde], [IDCtrl], False);
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;

var
  FatID, FatNum, Empresa, nItem: Integer;
  //
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EXTIPI, prod_uCom,
  prod_cEANTrib, prod_uTrib: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib,
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  prod_genero, prod_CFOP, Tem_IPI, Tem_II, _Ativo_, InfAdCuztm, EhServico: Integer;
  //
  ICMS_Orig, ICMS_CST, ICMS_modBC, ICMS_CSOSN, ICMS_modBCST: Integer;
  ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_vBCFCP, ICMS_pFCP, ICMS_vFCP, ICMS_pMVAST,
  ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST, ICMS_vBCFCPST, ICMS_pFCPST, ICMS_vFCPST: Double;
  //
  IPI_clEnq, IPI_CNPJProd, IPI_cSelo, IPI_cEnq, IPI_CST: String;
  IPI_qSelo, IPI_vBC, IPI_qUnid, IPI_vUnid, IPI_pIPI, IPI_vIPI: Double;
  //
  II_vBC, II_vDespAdu, II_vII, II_vIOF: Double;
  //
  PIS_CST: Integer;
  PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
  PISST_vBC, PISST_pPIS, PISST_qBCProd, PISST_vAliqProd, PISST_vPIS: Double;
  //
  COFINS_CST: Integer;
  COFINS_vBC, COFINS_pCOFINS, COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd,
  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd, COFINSST_vAliqProd,
  COFINSST_vCOFINS: Double;
  //
  InfAdProd: String;
  //
  FreteVal, Seguro: Double;
  //
  ICMSUFDest_vBCUFDest, ICMSUFDest_vBCFCPUFDest, ICMSUFDest_pFCPUFDest,
  ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter, ICMSUFDest_pICMSInterPart,
  ICMSUFDest_vFCPUFDest, ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet: Double;
  prod_indTot, prod_CEST: Integer;
  prod_xPed, prod_nFCI, prod_indEscala, prod_CNPJFab, prod_cBenef: String;
  prod_nItemPed: Variant;
  ICMS_vICMSDeson, ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif, ICMS_vBCSTRet, ICMS_pST,
  ICMS_vICMSSTRet, ICMS_vBCFCPSTRet, ICMS_pFCPSTRet, ICMS_vFCPSTRet,
  ICMS_pCredSN, ICMS_vCredICMSSN: Double;
  ICMS_motDesICMS: Integer;
begin
  FatID   := EdFatID.valueVariant;
  FatNum  := EdFatNum.valueVariant;
  Empresa := EdEmpresa.valueVariant;
  //
  prod_cProd    := Geral.SoNumero_TT(Edprod_cProd   .ValueVariant);
  prod_cEAN     := Geral.SoNumero_TT(Edprod_cEAN    .ValueVariant);
  prod_xProd    := Edprod_xProd   .ValueVariant;
  prod_NCM      := Geral.SoNumero_TT(Edprod_NCM     .ValueVariant);
  prod_EXTIPI   := Geral.SoNumero_TT(Edprod_EXTIPI  .ValueVariant);
  prod_genero   := Edprod_genero  .ValueVariant;
  prod_CFOP     := Edprod_CFOP    .ValueVariant;
  prod_uCom     := Edprod_uCom    .ValueVariant;
  prod_qCom     := Edprod_qCom    .ValueVariant;
  prod_vUnCom   := Edprod_vUnCom  .ValueVariant;
  prod_vProd    := Edprod_vProd   .ValueVariant;
  prod_cEANTrib := Geral.SoNumero_TT(Edprod_cEANTrib.ValueVariant);
  prod_uTrib    := Edprod_uTrib   .ValueVariant;
  prod_qTrib    := Edprod_qTrib   .ValueVariant;
  prod_vUnTrib  := Edprod_vUnTrib .ValueVariant;
  prod_vFrete   := Edprod_vFrete  .ValueVariant;
  prod_vSeg     := Edprod_vSeg    .ValueVariant;
  prod_vDesc    := Edprod_vDesc   .ValueVariant;
  prod_vOutro   := Edprod_vOutro  .ValueVariant;
  //Tem_IPI       := MLAGeral.BTI(EdIPI_pIPI.ValueVariant > 0);
  Tem_IPI       := Geral.BoolToInt(CkTem_IPI.Checked);
  Tem_II        := Geral.BoolToInt(CkTem_II.Checked);

{ TODO 5 : est� totalizando o II? }

  _Ativo_       := 1;
  InfAdCuztm    := 0;
  EhServico     := 0;
  //
  if ImgTipo.SQLType = stIns then
  begin
    QrPrx.Close;
    QrPrx.Params[00].AsInteger := FatID;
    QrPrx.Params[01].AsInteger := FatNum;
    QrPrx.Params[02].AsInteger := Empresa;
    QrPrx.Open;
    EdnItem.ValueVariant := QrPrxnItem.Value + 1;
  end;
  nItem := EdnItem.ValueVariant;
  //
  prod_indTot   := Geral.BoolToInt(Ckprod_indTot.Checked);
  prod_CEST     := Edprod_CEST.ValueVariant;
  prod_CNPJFab  := Edprod_CNPJFab.ValueVariant;
  prod_cBenef   := Edprod_cBenef.ValueVariant;
  prod_xPed     := Edprod_xPed.Text;
  prod_nItemPed := Edprod_nItemPed.ValueVariant;
  prod_nFCI     := Edprod_nFCI.Text;
  //
  if Ckprod_indEscala.Checked = True then
    prod_indEscala := Ckprod_indEscala.ValCheck
  else
    prod_indEscala := Ckprod_indEscala.ValUncheck;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsi', False, [
  'prod_cProd', 'prod_cEAN', 'prod_xProd',
  'prod_NCM', 'prod_EXTIPI', 'prod_genero',
  'prod_CFOP', 'prod_uCom', 'prod_qCom',
  'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
  'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
  'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
  'prod_vOutro', 'Tem_IPI', 'Tem_II',
  'prod_indTot', 'prod_CEST', 'prod_nFCI',
  'prod_xPed', 'prod_nItemPed',
  '_Ativo_', 'InfAdCuztm', 'EhServico',
  'prod_indEscala', 'prod_CNPJFab', 'prod_cBenef'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  prod_cProd, prod_cEAN, prod_xProd,
  prod_NCM, prod_EXTIPI, prod_genero,
  prod_CFOP, prod_uCom, prod_qCom,
  prod_vUnCom, prod_vProd, prod_cEANTrib,
  prod_uTrib, prod_qTrib, prod_vUnTrib,
  prod_vFrete, prod_vSeg, prod_vDesc,
  prod_vOutro, Tem_IPI, Tem_II,
  prod_indTot, prod_CEST, prod_nFCI,
  prod_xPed, prod_nItemPed,
  _Ativo_, InfAdCuztm, EhServico,
  prod_indEscala, prod_CNPJFab, prod_cBenef], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
(*    if UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
    'SELECT ICMS_CSOSN ',
    'FROM nfeitsn ',
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND nItem=' + Geral.FF0(nItem),
    '']) then
    //
    ICMS_CSOSN := DModG.QrAux.FieldByName('ICMS_CSOSN').AsInteger;
    //
*)
    if FStqMovValA <> 0 then
      AtualizaCamposFaturamento(FStqMovValA, prod_qCom, prod_vUnCom, prod_vProd);
    //
    ExcluiItem('nfeitsn', FatID, FatNum, Empresa, nItem);
    //
    ICMS_Orig        := EdICMS_Orig    .ValueVariant;
    ICMS_CST         := EdICMS_CST     .ValueVariant;
    ICMS_modBC       := RGICMS_modBC   .ItemIndex;
    ICMS_modBCST     := RgICMS_modBCST .ItemIndex;
    ICMS_pRedBC      := EdICMS_pRedBC  .ValueVariant;
    ICMS_vBC         := EdICMS_vBC     .ValueVariant;
    ICMS_pICMS       := EdICMS_pICMS   .ValueVariant;
    ICMS_vICMS       := EdICMS_vICMS   .ValueVariant;
    ICMS_vBCFCP      := EdICMS_vBCFCP  .ValueVariant;
    ICMS_pFCP        := EdICMS_pFCP    .ValueVariant;
    ICMS_vFCP        := EdICMS_vFCP    .ValueVariant;
    ICMS_pMVAST      := EdICMS_pMVAST  .ValueVariant;
    ICMS_pRedBCST    := EdICMS_pRedBCST.ValueVariant;
    ICMS_vBCST       := EdICMS_vBCST   .ValueVariant;
    ICMS_pICMSST     := EdICMS_pICMSST .ValueVariant;
    ICMS_vICMSST     := EdICMS_vICMSST .ValueVariant;
    ICMS_vBCFCPST    := EdICMS_vBCFCPST.ValueVariant;
    ICMS_pFCPST      := EdICMS_pFCPST  .ValueVariant;
    ICMS_vFCPST      := EdICMS_vFCPST  .ValueVariant;
    //
    ICMS_vICMSDeson  := EdICMS_vICMSDeson.ValueVariant;
    ICMS_motDesICMS  := RGICMS_motDesICMS.ItemIndex;
    ICMS_vICMSOp     := EdICMS_vICMSOp.ValueVariant;
    ICMS_pDif        := EdICMS_pDif.ValueVariant;
    ICMS_vICMSDif    := EdICMS_vICMSDif.ValueVariant;
    ICMS_vBCSTRet    := EdICMS_vBCSTRet.ValueVariant;
    ICMS_pST         := EdICMS_pST.ValueVariant;
    ICMS_vICMSSTRet  := EdICMS_vICMSSTRet.ValueVariant;
    ICMS_vBCFCPSTRet := EdICMS_vBCFCPSTRet.ValueVariant;
    ICMS_pFCPSTRet   := EdICMS_pFCPSTRet.ValueVariant;
    ICMS_vFCPSTRet   := EdICMS_vFCPSTRet.ValueVariant;
    ICMS_CSOSN       := EdICMS_CSOSN.ValueVariant;
    ICMS_pCredSN     := EdICMS_pCredSN.ValueVariant;
    ICMS_vCredICMSSN := EdICMS_vCredICMSSN.ValueVariant;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
    'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
    'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
    'ICMS_vICMS', 'ICMS_vBCFCP', 'ICMS_pFCP', 'ICMS_vFCP', 'ICMS_modBCST', 'ICMS_pMVAST',
    'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
    'ICMS_vICMSST', 'ICMS_vBCFCPST', 'ICMS_pFCPST', 'ICMS_vFCPST', 'ICMS_CSOSN',
    'ICMS_vICMSDeson', 'ICMS_motDesICMS',
    'ICMS_vICMSOp', 'ICMS_pDif', 'ICMS_vICMSDif',
    'ICMS_vBCSTRet', 'ICMS_pST', 'ICMS_vICMSSTRet',
    'ICMS_vBCFCPSTRet', 'ICMS_pFCPSTRet', 'ICMS_vFCPSTRet',
    'ICMS_pCredSN', 'ICMS_vCredICMSSN',
    '_Ativo_'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    ICMS_Orig, ICMS_CST, ICMS_modBC,
    ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
    ICMS_vICMS, ICMS_vBCFCP, ICMS_pFCP, ICMS_vFCP, ICMS_modBCST, ICMS_pMVAST,
    ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
    ICMS_vICMSST, ICMS_vBCFCPST, ICMS_pFCPST, ICMS_vFCPST, ICMS_CSOSN,
    ICMS_vICMSDeson, ICMS_motDesICMS,
    ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif,
    ICMS_vBCSTRet, ICMS_pST, ICMS_vICMSSTRet,
    ICMS_vBCFCPSTRet, ICMS_pFCPSTRet, ICMS_vFCPSTRet,
    ICMS_pCredSN, ICMS_vCredICMSSN,
    _Ativo_], [
    FatID, FatNum, Empresa, nItem], True) then
    begin
      //
      ExcluiItem('nfeitsna', FatID, FatNum, Empresa, nItem);
      if CkGeraGrupoNA.Checked then
      begin
(*        ICMSUFDest_vBCUFDest,
        ICMSUFDest_pFCPUFDest,
        ICMSUFDest_pICMSUFDest,
        ICMSUFDest_pICMSInter,
        ICMSUFDest_pICMSInterPart,
        ICMSUFDest_vFCPUFDest,
        ICMSUFDest_vICMSUFDest,
        ICMSUFDest_vICMSUFRemet,*)
        ICMSUFDest_vBCUFDest      := EdICMSUFDest_vBCUFDest.ValueVariant;
        ICMSUFDest_pFCPUFDest     := EdICMSUFDest_pFCPUFDest.ValueVariant;
        ICMSUFDest_pICMSUFDest    := EdICMSUFDest_pICMSUFDest.ValueVariant;
        ICMSUFDest_pICMSInter     := EdICMSUFDest_pICMSInter.ValueVariant;
        ICMSUFDest_pICMSInterPart := EdICMSUFDest_pICMSInterPart.ValueVariant;
        ICMSUFDest_vFCPUFDest     := EdICMSUFDest_vFCPUFDest.ValueVariant;
        ICMSUFDest_vICMSUFDest    := EdICMSUFDest_vICMSUFDest.ValueVariant;
        ICMSUFDest_vICMSUFRemet   := EdICMSUFDest_vICMSUFRemet.ValueVariant;
        ICMSUFDest_vBCFCPUFDest   := EdICMSUFDest_vBCFCPUFDest.ValueVariant;

        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsna', False, [
        'ICMSUFDest_vBCUFDest', 'ICMSUFDest_pFCPUFDest',
        'ICMSUFDest_pICMSUFDest', 'ICMSUFDest_pICMSInter',
        'ICMSUFDest_pICMSInterPart', 'ICMSUFDest_vFCPUFDest',
        'ICMSUFDest_vICMSUFDest', 'ICMSUFDest_vICMSUFRemet',
        'ICMSUFDest_vBCFCPUFDest'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        ICMSUFDest_vBCUFDest, ICMSUFDest_pFCPUFDest,
        ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
        ICMSUFDest_pICMSInterPart, ICMSUFDest_vFCPUFDest,
        ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet,
        ICMSUFDest_vBCFCPUFDest], [
        FatID, FatNum, Empresa, nItem], True);
      end;
      ExcluiItem('nfeitso', FatID, FatNum, Empresa, nItem);
      //
      IPI_vIPI      := EdIPI_vIPI    .ValueVariant;
      if (IPI_vIPI > 0) or CkTem_IPI.Checked then
      begin
        IPI_clEnq     := EdIPI_clEnq   .ValueVariant;
        IPI_CNPJProd  := Geral.SoNumero_TT(EdIPI_CNPJProd.ValueVariant);
        IPI_cSelo     := EdIPI_cSelo   .ValueVariant;
        IPI_cEnq      := EdIPI_cEnq    .ValueVariant;
        IPI_CST       := Geral.SoNumero_TT(EdIPI_CST     .ValueVariant);
        IPI_qSelo     := EdIPI_qSelo   .ValueVariant;
        IPI_vBC       := EdIPI_vBC     .ValueVariant;
        IPI_qUnid     := EdIPI_qUnid   .ValueVariant;
        IPI_vUnid     := EdIPI_vUnid   .ValueVariant;
        IPI_pIPI      := EdIPI_pIPI    .ValueVariant;
        //

        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
        'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
        'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
        'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',
        'IPI_pIPI', 'IPI_vIPI', '_Ativo_'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
        IPI_qSelo, IPI_cEnq, IPI_CST,
        IPI_vBC, IPI_qUnid, IPI_vUnid,
        IPI_pIPI, IPI_vIPI, _Ativo_], [
        FatID, FatNum, Empresa, nItem], True);
      end;

      //
      ExcluiItem('nfeitsp', FatID, FatNum, Empresa, nItem);
      //
      II_vBC      := EdII_vBC     .ValueVariant;
      if (II_vII > 0) or (CkTem_II.Checked) then
      begin
        II_vII      := EdII_vII     .ValueVariant;
        II_vBC      := EdII_vBC     .ValueVariant;
        II_vDespAdu := EdII_vDespAdu.ValueVariant;
        II_vIOF     := EdII_vIOF    .ValueVariant;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsp', False, [
        'II_vBC', 'II_vDespAdu', 'II_vII',
        'II_vIOF', '_Ativo_'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        II_vBC, II_vDespAdu, II_vII,
        II_vIOF, _Ativo_], [
        FatID, FatNum, Empresa, nItem], True);
      end;

      //
      ExcluiItem('nfeitsq', FatID, FatNum, Empresa, nItem);
      //
      PIS_CST         := EdPIS_CST        .ValueVariant;
      PIS_vBC         := EdPIS_vBC        .ValueVariant;
      PIS_pPIS        := EdPIS_pPIS       .ValueVariant;
      PIS_vPIS        := EdPIS_vPIS       .ValueVariant;
      PIS_qBCProd     := EdPIS_qBCProd    .ValueVariant;
      PIS_vAliqProd   := EdPIS_vAliqProd  .ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsq', False, [
      'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
      'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
      '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      PIS_CST, PIS_vBC, PIS_pPIS,
      PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
      _Ativo_], [
      FatID, FatNum, Empresa, nItem], True);

      //
      ExcluiItem('nfeitsr', FatID, FatNum, Empresa, nItem);
      //
      PISST_vBC       := EdPISST_vBC      .ValueVariant;
      PISST_pPIS      := EdPISST_pPIS     .ValueVariant;
      PISST_qBCProd   := EdPISST_qBCProd  .ValueVariant;
      PISST_vAliqProd := EdPISST_vAliqProd.ValueVariant;
      PISST_vPIS      := EdPISST_vPIS     .ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsr', False, [
      'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
      'PISST_vAliqProd', 'PISST_vPIS', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      PISST_vBC, PISST_pPIS, PISST_qBCProd,
      PISST_vAliqProd, PISST_vPIS, _Ativo_], [
      FatID, FatNum, Empresa, nItem], True);

      //
      ExcluiItem('nfeitss', FatID, FatNum, Empresa, nItem);
      //
      COFINS_CST         := EdCOFINS_CST        .ValueVariant;
      COFINS_vBC         := EdCOFINS_vBC        .ValueVariant;
      COFINS_pCOFINS     := EdCOFINS_pCOFINS    .ValueVariant;
      COFINS_vCOFINS     := EdCOFINS_vCOFINS    .ValueVariant;
      COFINS_qBCProd     := EdCOFINS_qBCProd    .ValueVariant;
      COFINS_vAliqProd   := EdCOFINS_vAliqProd  .ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitss', False, [
      'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
      'COFINS_vCOFINS', 'COFINS_qBCProd', 'COFINS_vAliqProd',
      '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
      COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd,
      _Ativo_], [
      FatID, FatNum, Empresa, nItem], True);

      //
      ExcluiItem('nfeitst', FatID, FatNum, Empresa, nItem);
      //
      COFINSST_vBC       := EdCOFINSST_vBC      .ValueVariant;
      COFINSST_pCOFINS   := EdCOFINSST_pCOFINS  .ValueVariant;
      COFINSST_qBCProd   := EdCOFINSST_qBCProd  .ValueVariant;
      COFINSST_vAliqProd := EdCOFINSST_vAliqProd.ValueVariant;
      COFINSST_vCOFINS   := EdCOFINSST_vCOFINS  .ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitst', False, [
      'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
      'COFINSST_vAliqProd', 'COFINSST_vCOFINS', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
      COFINSST_vAliqProd, COFINSST_vCOFINS, _Ativo_], [
      FatID, FatNum, Empresa, nItem], True);
      //
      ExcluiItem('nfeitsv', FatID, FatNum, Empresa, nItem);
      //
      InfAdProd := MeInfAdProd.Text;
      if Trim(InfAdProd) <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsv', False, [
        'InfAdProd', '_Ativo_'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        InfAdProd, _Ativo_], [
        FatID, FatNum, Empresa, nItem], True);
      end;
    end;
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, LaAviso1, LaAviso2, REWarning);
    //
    if FForm = 'FmNFeCabA_0000' then
    begin
      FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value, FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
      FmNFeCabA_0000.QrNFeItsI.Locate('nItem', nItem, []);
    end else
    if FForm = 'FmEntradaCab' then
    begin
      {$IFNDef semEntradaNFe}
      FmEntradaCab.LocCod(FmEntradaCab.QrNFeCabAIDCtrl.Value,
      FmEntradaCab.QrNFeCabAIDCtrl.Value);
      FmEntradaCab.QrNFeItsI.Locate('nItem', nItem, []);
      {$EndIf}
    end;
    Close;
  end;
end;

procedure TFmNFeItsI_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsI_0000.CalculaBCs;
var
  Qtd, Prc:Double;
begin
  Qtd := Edprod_qTrib.ValueVariant;
  Prc := Edprod_vUnTrib.ValueVariant;
  FBC_Geral := (Round(Qtd * Prc * 100)) / 100;
  //
  CalculaBC_ICMS();
  CalculaBC_ICMSST();
  CalculaBC_IPI();
  CalculaBC_PIS();
  CalculaBC_PISST();
  CalculaBC_COFINS();
  CalculaBC_COFINSST();
end;

procedure TFmNFeItsI_0000.CalculaBC_COFINS;
begin
  EdCOFINS_vBC.ValueVariant :=  FBC_Geral;
end;

procedure TFmNFeItsI_0000.CalculaBC_COFINSST;
begin
  // Manual?
end;

procedure TFmNFeItsI_0000.CalculaBC_ICMS();
var
  Red, Tot: Double;
begin
  Red := EdICMS_pRedBC.ValueVariant;
  Tot := (Round(FBC_Geral * (1 - (Red / 100)) * 100)) / 100;
  //
  EdICMS_vBC.ValueVariant := Tot;
end;

procedure TFmNFeItsI_0000.CalculaBC_ICMSST;
begin
  // Manual?
end;

procedure TFmNFeItsI_0000.CalculaBC_IPI;
begin
  EdIPI_vBC.ValueVariant :=  FBC_Geral;
end;

procedure TFmNFeItsI_0000.CalculaBC_PIS;
begin
  EdPIS_vBC.ValueVariant :=  FBC_Geral;
end;

procedure TFmNFeItsI_0000.CalculaBC_PISST;
begin
  // Manual?
end;

procedure TFmNFeItsI_0000.CalculaCOFINS;
var
  Bas, Per, Prc, Qtd, Val: Double;
begin
  Bas := EdCOFINS_vBC.ValueVariant;
  Per := EdCOFINS_pCOFINS.ValueVariant;
  //
  Prc := EdCOFINS_vAliqProd.ValueVariant;
  Qtd := EdCOFINS_qBCProd.ValueVariant;
  //
  if (Prc <> 0) and (Qtd <> 0) then
    Val := Round(Prc * Qtd * 100) / 100
  else
    Val := Round(Bas * Per) / 100;
  //
  EdCOFINS_vCOFINS.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaCOFINSST;
var
  Bas, Per, Prc, Qtd, Val: Double;
begin
  Bas := EdCOFINSST_vBC.ValueVariant;
  Per := EdCOFINSST_pCOFINS.ValueVariant;
  //
  Prc := EdCOFINSST_vAliqProd.ValueVariant;
  Qtd := EdCOFINSST_qBCProd.ValueVariant;
  //
  if (Prc <> 0) and (Qtd <> 0) then
    Val := Round(Prc * Qtd * 100) / 100
  else
    Val := Round(Bas * Per) / 100;
  //
  Val := Val - EdCOFINS_vCOFINS.ValueVariant;
  if Val < 0 then
    Val := 0;
  EdCOFINSST_vCOFINS.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaICMS();
var
  Bas, Per, Val: Double;
begin
  Bas := EdICMS_vBC.ValueVariant;
  Per := EdICMS_pICMS.ValueVariant;
  Val := Round(Bas * Per) / 100;
  //
  EdICMS_vICMS.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaICMSST;
begin
  //???? Varia de caso a caso !!!! ????
end;

procedure TFmNFeItsI_0000.CalculaICMS_OIVCF();
var
  ICMSUFDest_pFCPUFDest,
  ICMSUFDest_vBCUFDest, ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
  ICMSUFDest_pICMSInterPart: Double;
  ICMSUFDest_vFCPUFDest,
  ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet: Double;
begin
  // Mudar quando estiver definido pelo CONCAT!
  ICMSUFDest_vBCUFDest      := EdICMSUFDest_vBCUFDest.ValueVariant;
  ICMSUFDest_pFCPUFDest     := EdICMSUFDest_pFCPUFDest.ValueVariant;
  ICMSUFDest_pICMSUFDest    := EdICMSUFDest_pICMSUFDest.ValueVariant;
  ICMSUFDest_pICMSInter     := EdICMSUFDest_pICMSInter.ValueVariant;
  ICMSUFDest_pICMSInterPart := EdICMSUFDest_pICMSInterPart.ValueVariant;
  //
  UnNFe_PF.CalculaValoresICMS_OIVCF(ICMSUFDest_pFCPUFDest,
  ICMSUFDest_vBCUFDest, ICMSUFDest_pICMSUFDest, ICMSUFDest_pICMSInter,
  ICMSUFDest_pICMSInterPart, ICMSUFDest_vFCPUFDest,
  ICMSUFDest_vICMSUFDest, ICMSUFDest_vICMSUFRemet);
  //
  EdICMSUFDest_vFCPUFDest.ValueVariant   := ICMSUFDest_vFCPUFDest;
  EdICMSUFDest_vICMSUFDest.ValueVariant  := ICMSUFDest_vICMSUFDest;
  EdICMSUFDest_vICMSUFRemet.ValueVariant := ICMSUFDest_vICMSUFRemet;
end;

procedure TFmNFeItsI_0000.CalculaIPI;
var
  Bas, Per, Prc, Qtd, Val: Double;
begin
  Bas := EdIPI_vBC.ValueVariant;
  Per := EdIPI_pIPI.ValueVariant;
  //
  Prc := EdIPI_vUnid.ValueVariant;
  Qtd := EdIPI_qUnid.ValueVariant;
  //
  if (Prc <> 0) and (Qtd <> 0) then
    Val := Round(Prc * Qtd * 100) / 100
  else
    Val := Round(Bas * Per) / 100;
  //
  EdIPI_vIPI.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaPIS;
var
  Bas, Per, Prc, Qtd, Val: Double;
begin
  Bas := EdPIS_vBC.ValueVariant;
  Per := EdPIS_pPIS.ValueVariant;
  //
  Prc := EdPIS_vAliqProd.ValueVariant;
  Qtd := EdPIS_qBCProd.ValueVariant;
  //
  if (Prc <> 0) and (Qtd <> 0) then
    Val := Round(Prc * Qtd * 100) / 100
  else
    Val := Round(Bas * Per) / 100;
  //
  EdPIS_vPIS.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaPISST;
var
  Bas, Per, Prc, Qtd, Val: Double;
begin
  Bas := EdPISST_vBC.ValueVariant;
  Per := EdPISST_pPIS.ValueVariant;
  //
  Prc := EdPISST_vAliqProd.ValueVariant;
  Qtd := EdPISST_qBCProd.ValueVariant;
  //
  if (Prc <> 0) and (Qtd <> 0) then
    Val := Round(Prc * Qtd * 100) / 100
  else
    Val := Round(Bas * Per) / 100;
  //
  Val := Val - EdPIS_vPIS.ValueVariant;
  if Val < 0 then
    Val := 0;
  EdPISST_vPIS.ValueVariant := Val;
end;

procedure TFmNFeItsI_0000.CalculaValorCom;
var
  Qtd, Prc, Tot: Double;
begin
  Qtd :=  Edprod_qCom.ValueVariant;
  Prc :=  Edprod_vUnCom.ValueVariant;
  Tot :=  (Round(Qtd * Prc * 100)) / 100;
  Edprod_vProd.ValueVariant := Tot;
end;

procedure TFmNFeItsI_0000.CkCalcularClick(Sender: TObject);
begin
  BtCalcular.Enabled := CkCalcular.Checked;
  FCalculoAutomatico := CkCalcular.Checked = False;
end;

procedure TFmNFeItsI_0000.CkGeraGrupoNAClick(Sender: TObject);
begin
  PnGrupoNA.Visible := CkGeraGrupoNA.Checked;
end;

procedure TFmNFeItsI_0000.CkTem_IPIClick(Sender: TObject);
begin
  if CkTem_IPI.Checked then
    EdIPI_cEnq.ValueVariant := 999;
end;

procedure TFmNFeItsI_0000.EdICMS_CSOSNChange(Sender: TObject);
begin
  EdCSOSN_Txt.Text := UFinanceiro.CSOSN_Get(1, EdICMS_CSOSN.ValueVariant);
end;

procedure TFmNFeItsI_0000.EdICMS_CSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdICMS_CSOSN.Text := UFinanceiro.ListaDeCSOSN();
end;

procedure TFmNFeItsI_0000.ConfiguraAba;
var
  Aba: Integer;
begin
  case FAbaAtu of
    2: //ICMS
      Aba := 0;
    3://IPI
      Aba := 1;
    4://II
      Aba := 2;
    5://PIS
      Aba := 3;
    6://COFINS
      Aba := 4;
    8://Informa��es adicionais do produto
      Aba := 5;
    else
      Aba := 0;
  end;
  PageControl3.ActivePageIndex := Aba;
end;

procedure TFmNFeItsI_0000.EdCOFINSST_pCOFINSChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINSST();
end;

procedure TFmNFeItsI_0000.EdCOFINSST_qBCProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINSST();
end;

procedure TFmNFeItsI_0000.EdCOFINSST_vAliqProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINSST();
end;

procedure TFmNFeItsI_0000.EdCOFINSST_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINSST();
end;

procedure TFmNFeItsI_0000.EdCOFINSST_vCOFINSExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 5;
  MeInfAdProd.SetFocus;
end;

procedure TFmNFeItsI_0000.EdCOFINS_CSTChange(Sender: TObject);
begin
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(Geral.IMV(EdCOFINS_CST.Text));
  //
  if FCalculoAutomatico then
    CalculaBC_COFINS();
end;

procedure TFmNFeItsI_0000.EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmNFeItsI_0000.EdCOFINS_pCOFINSChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINS();
end;

procedure TFmNFeItsI_0000.EdCOFINS_qBCProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINS();
end;

procedure TFmNFeItsI_0000.EdCOFINS_vAliqProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINS();
end;

procedure TFmNFeItsI_0000.EdCOFINS_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaCOFINS();
end;

procedure TFmNFeItsI_0000.EdEmpresaChange(Sender: TObject);
begin
  DModG.ReopenParamsEmp(EdEmpresa.ValueVariant);
end;

procedure TFmNFeItsI_0000.EdICMSUFDest_pFCPUFDestChange(Sender: TObject);
begin
  if FCalculoAutomatico then
    CalculaICMS_OIVCF();
end;

procedure TFmNFeItsI_0000.EdICMSUFDest_pICMSInterChange(Sender: TObject);
begin
  if FCalculoAutomatico then
    CalculaICMS_OIVCF();
end;

procedure TFmNFeItsI_0000.EdICMSUFDest_pICMSInterPartChange(Sender: TObject);
begin
  if FCalculoAutomatico then
    CalculaICMS_OIVCF();
end;

procedure TFmNFeItsI_0000.EdICMSUFDest_pICMSUFDestChange(Sender: TObject);
begin
  if FCalculoAutomatico then
    CalculaICMS_OIVCF();
end;

procedure TFmNFeItsI_0000.EdICMSUFDest_vBCUFDestChange(Sender: TObject);
begin
  if FCalculoAutomatico then
    CalculaICMS_OIVCF();
end;

procedure TFmNFeItsI_0000.EdICMS_CSTChange(Sender: TObject);
begin
  EdTextoB.Text := UFinanceiro.CST_B_Get(EdICMS_CST.ValueVariant);
  //
  if FCalculoAutomatico then
    CalculaBCs();
end;

procedure TFmNFeItsI_0000.EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdICMS_CST.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
end;

procedure TFmNFeItsI_0000.EdICMS_OrigChange(Sender: TObject);
begin
  EdTextoA.Text := UFinanceiro.CST_A_Get(EdICMS_Orig.ValueVariant);
  //
  if FCalculoAutomatico then
    CalculaBC_ICMS();
end;

procedure TFmNFeItsI_0000.EdICMS_OrigKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdICMS_Orig.Text := UFinanceiro.ListaDeSituacaoTributaria();
end;

procedure TFmNFeItsI_0000.EdICMS_pICMSChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaICMS();
end;

procedure TFmNFeItsI_0000.EdICMS_pICMSSTChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaICMSST();
end;

procedure TFmNFeItsI_0000.EdICMS_pMVASTChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBC_ICMSST();
end;

procedure TFmNFeItsI_0000.EdICMS_pRedBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBC_ICMS();
end;

procedure TFmNFeItsI_0000.EdICMS_pRedBCSTChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBC_ICMSST();
end;

procedure TFmNFeItsI_0000.EdICMS_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaICMS();
end;

procedure TFmNFeItsI_0000.EdICMS_vBCSTChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaICMSST();
end;

procedure TFmNFeItsI_0000.EdICMS_vICMSSTExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 1;
  EdIPI_CST.SetFocus;
end;

procedure TFmNFeItsI_0000.EdII_vIOFExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 3;
  EdPIS_CST.SetFocus;
end;

procedure TFmNFeItsI_0000.EdIPI_CSTChange(Sender: TObject);
begin
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(Geral.IMV(EdIPI_CST.Text));
  //
  if FCalculoAutomatico then
    CalculaBC_IPI();
end;

procedure TFmNFeItsI_0000.EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdIPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmNFeItsI_0000.EdIPI_pIPIChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaIPI();
end;

procedure TFmNFeItsI_0000.EdIPI_qSeloExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex :=2;
  EdII_vBC.SetFocus;
end;

procedure TFmNFeItsI_0000.EdIPI_qUnidChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaIPI();
end;

procedure TFmNFeItsI_0000.EdIPI_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaIPI();
end;

procedure TFmNFeItsI_0000.EdIPI_vUnidChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaIPI();
end;

procedure TFmNFeItsI_0000.EdPISST_pPISChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPISST();
end;

procedure TFmNFeItsI_0000.EdPISST_qBCProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPISST();
end;

procedure TFmNFeItsI_0000.EdPISST_vAliqProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPISST();
end;

procedure TFmNFeItsI_0000.EdPISST_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPISST();
end;

procedure TFmNFeItsI_0000.EdPISST_vPISExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 4;
  EdCOFINS_CST.SetFocus;
end;

procedure TFmNFeItsI_0000.EdPIS_CSTChange(Sender: TObject);
begin
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(Geral.IMV(EdPIS_CST.Text));
  //
  if FCalculoAutomatico then
    CalculaBC_PIS();
end;

procedure TFmNFeItsI_0000.EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdPis_CST.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmNFeItsI_0000.EdPIS_pPISChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPIS();
end;

procedure TFmNFeItsI_0000.EdPIS_qBCProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPIS();
end;

procedure TFmNFeItsI_0000.EdPIS_vAliqProdChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPIS();
end;

procedure TFmNFeItsI_0000.EdPIS_vBCChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaPIS();
end;

procedure TFmNFeItsI_0000.Edprod_cEANChange(Sender: TObject);
begin
  Edprod_cEANTrib.ValueVariant := Edprod_cEAN.ValueVariant;
end;

procedure TFmNFeItsI_0000.Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pesq: String;
begin
  if Key = VK_F3 then
  begin
    Pesq := CuringaLoc.CriaForm4('Codigo', 'Nome', 'cfop2003', Dmod.MyDB, '');
    if Pesq <> '' then
      Edprod_CFOP.Text := VAR_NOME_X;
  end;
end;

procedure TFmNFeItsI_0000.Edprod_cProdEnter(Sender: TObject);
begin
  FGraGruX := Edprod_cProd.ValueVariant;
end;

procedure TFmNFeItsI_0000.Edprod_cProdExit(Sender: TObject);
const
  EhServico = False;
var
  Qry: TmySQLQuery;
  CFOP, DOC: String;
  TabelaPrc, CondicaoPg, Empresa, Cliente, Item_MadeBy, RegrFiscal,
  GraGruX: Integer;
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, IDCtrl: Integer;
  MedDDSimpl, MedDDReal, PrecoO, PrecoR, ICMS_Aliq: Double;
  Continua: Boolean;
begin
  GraGruX := Edprod_cProd.ValueVariant;
  //
  if FGraGruX <> GraGruX then
  begin
    QrGGX.Close;
    QrGGX.Params[0].AsInteger := GraGruX;
    QrGGX.Open;
    if QrGGX.RecordCount > 0 then
    begin
      Edprod_xProd.Text          := QrGGXNO_GGX.Value;
      Edprod_xProd.Text          := QrGGXNO_GGX.Value;
      Edprod_NCM.Text            := QrGGXNCM.Value;
      Edprod_EXTIPI.Text         := QrGGXEX_TIPI.Value;
      Edprod_genero.ValueVariant := Geral.IMV(Copy(QrGGXNCM.Value, 1, 2));
      Edprod_cEAN.Text           := QrGGXcGTIN_EAN.Value;
      Edprod_uCom.Text           := QrGGXSigla.Value;
      // IPI
      EdIPI_pIPI.ValueVariant    := QrGGXIPI_Alq.Value;

      // CFOP, ICMS, PIS, COFINS
      if Length(Fdest_CNPJ) = 14 then
        DOC := Fdest_CNPJ
      else
        DOC := Fdest_CPF;
      if not DmodG.ObtemEntidadeDeCNPJCFP(DOC, Cliente) then
        Exit;
      Empresa     := FEmpresa;
      Item_MadeBy := QrGGXMadeBy.Value;
      RegrFiscal  := FFisRegCad;
      //
      if (ImgTipo.SQLType = stIns) or (FStqMovValA = 0) then
      begin
        Continua := DmNFe_0000.ObtemNumeroCFOP('', [0], Empresa,
                      Cliente, Item_MadeBy, RegrFiscal, EhServico, CFOP,
                      ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio);
      end else
      begin
        Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT IDCtrl ',
            'FROM stqmovvala ',
            'WHERE ID=' + Geral.FF0(FStqMovValA),
            '']);
          //
          IDCtrl := Qry.FieldByName('IDCtrl').AsInteger;
          //
          Continua := DmNFe_0000.ObtemNumeroCFOP('fatpedfis', [IDCtrl], Empresa,
                        Cliente, Item_MadeBy, RegrFiscal, EhServico, CFOP,
                        ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio);
        finally
          Qry.Free;
        end;
      end;
      if Continua = True then
      begin
        Edprod_CFOP.ValueVariant       := Geral.IMV(Geral.SoNumero_TT(CFOP));
        EdICMS_pICMS.ValueVariant      := ICMS_Aliq;
        {
        EdPIS_pPIS.ValueVariant        := PIS_Aliq;
        EdCOFINS_pCOFINS.ValueVariant  := COFINS_Aliq;
        }
      end;
      //Pre�o
      TabelaPrc  := FTabelaPrc;
      CondicaoPg := FCondicaoPG;
      MedDDSimpl := FMedDDSimpl;
      MedDDReal  := FMedDDReal;
      DmProd.ObtemPrecoDeLista(Empresa, GraGruX, TabelaPrc, CondicaoPG,
        MedDDSimpl, MedDDReal, PrecoO, PrecoR);
      Edprod_vUnCom.ValueVariant := PrecoR; // pre�o com juros
    end;
  end;
end;

procedure TFmNFeItsI_0000.Edprod_NCMChange(Sender: TObject);
var
  NCM: String;
begin
  NCM := Geral.SoNumero_TT(Edprod_NCM.Text);
  if Length(NCM) > 1 then
    Edprod_genero.Text := Copy(NCM, 1, 2);
end;

procedure TFmNFeItsI_0000.Edprod_qComChange(Sender: TObject);
begin
  Edprod_qTrib.ValueVariant := Edprod_qCom.ValueVariant;
  if FCalculoAutomatico then
  CalculaValorCom();
end;

procedure TFmNFeItsI_0000.Edprod_qTribChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBCs();
end;

procedure TFmNFeItsI_0000.Edprod_uComChange(Sender: TObject);
begin
  Edprod_uTrib.ValueVariant := Edprod_uCom.ValueVariant;
end;

procedure TFmNFeItsI_0000.Edprod_vUnComChange(Sender: TObject);
begin
  Edprod_vUnTrib.ValueVariant := Edprod_vUnCom.ValueVariant;
  if FCalculoAutomatico then
  CalculaValorCom();
end;

procedure TFmNFeItsI_0000.Edprod_vUnTribChange(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBCs();
end;

procedure TFmNFeItsI_0000.Edprod_vUnTribExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  EdICMS_Orig.SetFocus;
end;

procedure TFmNFeItsI_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if Edprod_cProd.Visible and Edprod_cProd.Enabled then
    Edprod_cProd.SetFocus;
  //
  EdTextoA.Text          := UFinanceiro.CST_A_Get(EdICMS_Orig.ValueVariant);
  EdTextoB.Text          := UFinanceiro.CST_B_Get(EdICMS_CST.ValueVariant);
  EdTextoIPI_CST.Text    := UFinanceiro.CST_IPI_Get(Geral.IMV(EdIPI_CST.Text));
  EdTextoPIS_CST.Text    := UFinanceiro.CST_PIS_Get(Geral.IMV(EdPIS_CST.Text));
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(Geral.IMV(EdCOFINS_CST.Text));
end;

procedure TFmNFeItsI_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FAbaAtu     := 0;
  FStqMovValA := 0;
  //
  FCalculoAutomatico := CkCalcular.Checked = False;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
end;

procedure TFmNFeItsI_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeItsI_0000.FormShow(Sender: TObject);
begin
  ConfiguraAba();
end;

procedure TFmNFeItsI_0000.MeInfAdProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = 13 then
 begin
   if (ssCtrl in Shift) then
     //
   else
     BtOK.SetFocus;
 end;
end;

procedure TFmNFeItsI_0000.RGICMS_modBCClick(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBC_ICMS();
end;

procedure TFmNFeItsI_0000.RGICMS_modBCSTClick(Sender: TObject);
begin
  if FCalculoAutomatico then
  CalculaBC_ICMSST();
end;

procedure TFmNFeItsI_0000.SpeedButton1Click(Sender: TObject);
var
  Query: TmySQLQuery;
  Nivel1: Integer;
begin
  if FGraGruX <> 0 then
  begin
    Query := TmySQLQuery.Create(Dmod.MyDB);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT gg1.CodUsu ',
        'FROM gragrux ggx ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = ggx.GraGru1 ',
        'WHERE ggx.Controle=' + Geral.FF0(FGraGruX),
        '']);
      if Query.RecordCount > 0 then
        Nivel1 := Query.FieldByName('CodUsu').AsInteger
      else
        Nivel1 := 0;
    finally
      Query.Free;
    end;
  end else
    Nivel1 := 0;
  //
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    FmGraGruPesq1.QrGraGru1.Close;
    FmGraGruPesq1.QrGraGru1.SQL.Clear;
    FmGraGruPesq1.QrGraGru1.SQL.Add('SELECT gg1.CodUsu, gg1.Nivel1, ');
    FmGraGruPesq1.QrGraGru1.SQL.Add('gg1.GraTamCad, gg1.Nome');
    FmGraGruPesq1.QrGraGru1.SQL.Add('FROM gragru1 gg1');
    FmGraGruPesq1.QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    //FmGraGruPesq1.QrGraGru1.SQL.Add('WHERE pgt.TipPrd=2');
    FmGraGruPesq1.QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
    FmGraGruPesq1.QrGraGru1.Open;
    //
    FmGraGruPesq1.EdGraGru1.ValueVariant := Nivel1;
    FmGraGruPesq1.CBGraGru1.KeyValue     := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    if FmGraGruPesq1.FGraGruX <> '0' then
      Edprod_cProd.ValueVariant := FmGraGruPesq1.FGraGruX;
    //
    FmGraGruPesq1.Destroy;
    // Forcar dados
    Edprod_xprod.SetFocus;
    Edprod_cprod.SetFocus;
  end;
end;
{ TODO : Fazer recupera��o de impostos como no Entradacab }
end.
