object FmNFeCabA_0000: TFmNFeCabA_0000
  Left = 368
  Top = 194
  Caption = 'NFe-EMISS-001 :: Emiss'#227'o de NF-e'
  ClientHeight = 633
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 537
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 445
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel14: TPanel
        Left = 0
        Top = 48
        Width = 1008
        Height = 397
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 1008
          Height = 397
          ActivePage = TabSheet10
          Align = alClient
          TabOrder = 0
          object TabSheet26: TTabSheet
            Caption = 'Geral'
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel50: TPanel
              Left = 0
              Top = 299
              Width = 1000
              Height = 70
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object GroupBox37: TGroupBox
                Left = 0
                Top = 0
                Width = 1000
                Height = 60
                Align = alTop
                Caption = ' Fiscal: '
                TabOrder = 0
                object Label348: TLabel
                  Left = 8
                  Top = 16
                  Width = 248
                  Height = 13
                  Caption = 'Data fiscal (SINTEGRA) (Data de entrada ou sa'#237'da):'
                end
                object TPDataFiscal: TdmkEditDateTimePicker
                  Left = 8
                  Top = 32
                  Width = 116
                  Height = 21
                  Date = 40048.941292650460000000
                  Time = 40048.941292650460000000
                  TabOrder = 0
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'DataFiscal'
                  UpdCampo = 'DataFiscal'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
            end
            object Panel43: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 89
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label316: TLabel
                Left = 8
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Regra fiscal:'
              end
              object Label318: TLabel
                Left = 8
                Top = 44
                Width = 86
                Height = 13
                Caption = 'Tabela de pre'#231'os:'
              end
              object Label320: TLabel
                Left = 564
                Top = 4
                Width = 187
                Height = 13
                Caption = 'Carteira para emiss'#227'o dos lan'#231'amentos:'
              end
              object SpeedButton13: TSpeedButton
                Left = 956
                Top = 19
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton13Click
              end
              object SbRegrFiscal: TSpeedButton
                Left = 536
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbRegrFiscalClick
              end
              object BtTabelaPrc: TSpeedButton
                Left = 564
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = BtTabelaPrcClick
              end
              object BtCondicaoPG: TSpeedButton
                Left = 956
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = BtCondicaoPGClick
              end
              object Label317: TLabel
                Left = 588
                Top = 44
                Width = 119
                Height = 13
                Caption = 'Condi'#231#227'o de pagamento:'
              end
              object EdRegrFiscal: TdmkEditCB
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'FisRegCad'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBRegrFiscal
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBRegrFiscal: TdmkDBLookupComboBox
                Left = 68
                Top = 20
                Width = 465
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 1
                dmkEditCB = EdRegrFiscal
                QryCampo = 'FisRegCad'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdCartEmis: TdmkEditCB
                Left = 566
                Top = 19
                Width = 44
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CartEmiss'
                UpdCampo = 'CartEmiss'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCartEmis
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCartEmis: TdmkDBLookupComboBox
                Left = 612
                Top = 19
                Width = 341
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                TabOrder = 3
                dmkEditCB = EdCartEmis
                QryCampo = 'CartEmiss'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdTabelaPrc: TdmkEditCB
                Left = 8
                Top = 60
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'TabelaPrc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBTabelaPrc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBTabelaPrc: TdmkDBLookupComboBox
                Left = 68
                Top = 60
                Width = 493
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 5
                dmkEditCB = EdTabelaPrc
                QryCampo = 'TabelaPrc'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdCondicaoPG: TdmkEditCB
                Left = 588
                Top = 60
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CondicaoPg'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBCondicaoPG
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCondicaoPG: TdmkDBLookupComboBox
                Left = 648
                Top = 60
                Width = 305
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 7
                dmkEditCB = EdCondicaoPG
                QryCampo = 'CondicaoPg'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
            end
            object Panel22: TPanel
              Left = 0
              Top = 89
              Width = 1000
              Height = 210
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label193: TLabel
                Left = 4
                Top = 4
                Width = 48
                Height = 13
                Caption = 'UF (DTB):'
              end
              object Label195: TLabel
                Left = 96
                Top = 4
                Width = 109
                Height = 13
                Caption = 'Natureza da opera'#231#227'o:'
                FocusControl = Edide_natOp
              end
              object Label196: TLabel
                Left = 804
                Top = 4
                Width = 165
                Height = 13
                Caption = 'Indicador de forma de pagamento: '
                FocusControl = Edide_indPag
              end
              object Label197: TLabel
                Left = 4
                Top = 44
                Width = 27
                Height = 13
                Caption = 'Mod.:'
                Enabled = False
                FocusControl = Edide_mod
              end
              object Label198: TLabel
                Left = 36
                Top = 44
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
                FocusControl = Edide_serie
              end
              object Label199: TLabel
                Left = 72
                Top = 44
                Width = 32
                Height = 13
                Caption = 'N'#186' NF:'
                FocusControl = Edide_nNF
              end
              object Label200: TLabel
                Left = 156
                Top = 44
                Width = 150
                Height = 13
                Caption = 'Data / Hora e TZD da emiss'#227'o:'
              end
              object Label201: TLabel
                Left = 372
                Top = 44
                Width = 183
                Height = 13
                Caption = 'Data / hora e TZD da sa'#237'sa / entrada:'
              end
              object Label202: TLabel
                Left = 592
                Top = 44
                Width = 90
                Height = 13
                Caption = 'Tipo do doc. fiscal:'
                FocusControl = Edide_tpNF
              end
              object Label203: TLabel
                Left = 692
                Top = 44
                Width = 178
                Height = 13
                Caption = 'C'#243'digo do munic'#237'pio do fato gerador: '
                FocusControl = Edide_cMunFG
              end
              object Label204: TLabel
                Left = 3
                Top = 84
                Width = 89
                Height = 13
                Caption = 'Tipo de impress'#227'o:'
                Enabled = False
                FocusControl = Edide_tpImp
              end
              object Label205: TLabel
                Left = 268
                Top = 84
                Width = 121
                Height = 13
                Caption = 'Tipo de emiss'#227'o da NF-e:'
                FocusControl = Edide_tpEmis
              end
              object Label206: TLabel
                Left = 4
                Top = 124
                Width = 18
                Height = 13
                Caption = 'DV:'
                Enabled = False
                FocusControl = Edide_cDV
              end
              object Label207: TLabel
                Left = 28
                Top = 124
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
                FocusControl = Edide_tpAmb
              end
              object Label208: TLabel
                Left = 160
                Top = 124
                Width = 148
                Height = 13
                Caption = 'Finalidade de emiss'#227'o da NF-e:'
                FocusControl = Edide_finNFe
              end
              object Label209: TLabel
                Left = 312
                Top = 124
                Width = 144
                Height = 13
                Caption = 'Processo de emiss'#227'o da NF-e:'
                Enabled = False
                FocusControl = Edide_procEmi
              end
              object Label210: TLabel
                Left = 830
                Top = 124
                Width = 153
                Height = 13
                Caption = 'Vers'#227'o do processo de emiss'#227'o:'
                Enabled = False
                FocusControl = Edide_verProc
              end
              object SpeedButton5: TSpeedButton
                Left = 968
                Top = 60
                Width = 21
                Height = 21
                Caption = '?'
                OnClick = SpeedButton5Click
              end
              object SpeedButton6: TSpeedButton
                Left = 776
                Top = 20
                Width = 23
                Height = 22
                Caption = '?'
                OnClick = SpeedButton6Click
              end
              object Label1: TLabel
                Left = 4
                Top = 164
                Width = 222
                Height = 13
                Caption = 'Identifica'#231#227'o do local de destino da opera'#231#227'o: '
                FocusControl = Edide_idDest
              end
              object Label362: TLabel
                Left = 228
                Top = 164
                Width = 152
                Height = 13
                Caption = 'Opera'#231#227'o com consumidor final:'
                FocusControl = Edide_indFinal
              end
              object Label363: TLabel
                Left = 384
                Top = 164
                Width = 396
                Height = 13
                Caption = 
                  'Indicador de presen'#231'a do comprador no estabelecimento no momento' +
                  ' da opera'#231#227'o:'
                FocusControl = Edide_indPres
              end
              object Edide_natOp: TdmkEdit
                Left = 96
                Top = 20
                Width = 677
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ide_natOp'
                UpdCampo = 'ide_natOp'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_indPag: TdmkEdit
                Left = 804
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '2'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_indPag'
                UpdCampo = 'ide_indPag'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_indPagChange
                OnKeyDown = Edide_indPagKeyDown
              end
              object Edide_mod: TdmkEdit
                Left = 4
                Top = 60
                Width = 29
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '55'
                QryCampo = 'ide_mod'
                UpdCampo = 'ide_mod'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 55
                ValWarn = False
                OnChange = Edide_modChange
              end
              object Edide_serie: TdmkEdit
                Left = 36
                Top = 60
                Width = 33
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_serie'
                UpdCampo = 'ide_serie'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Edide_nNF: TdmkEdit
                Left = 72
                Top = 60
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_nNF'
                UpdCampo = 'ide_nNF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Edide_tpNF: TdmkEdit
                Left = 592
                Top = 60
                Width = 21
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_tpNF'
                UpdCampo = 'ide_tpNF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_tpNFChange
                OnKeyDown = Edide_tpNFKeyDown
              end
              object Edide_cMunFG: TdmkEdit
                Left = 692
                Top = 60
                Width = 50
                Height = 21
                Alignment = taRightJustify
                MaxLength = 20
                TabOrder = 16
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_cMunFG'
                UpdCampo = 'ide_cMunFG'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_cMunFGChange
              end
              object Edide_tpImp: TdmkEdit
                Left = 3
                Top = 100
                Width = 25
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 18
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMax = '5'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_tpImp'
                UpdCampo = 'ide_tpImp'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_tpImpChange
              end
              object Edide_tpEmis: TdmkEdit
                Left = 268
                Top = 100
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 20
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                QryCampo = 'ide_tpEmis'
                UpdCampo = 'ide_tpEmis'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnChange = Edide_tpEmisChange
                OnKeyDown = Edide_tpEmisKeyDown
              end
              object Edide_cDV: TdmkEdit
                Left = 4
                Top = 140
                Width = 21
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 22
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_cDV'
                UpdCampo = 'ide_cDV'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Edide_tpAmb: TdmkEdit
                Left = 28
                Top = 140
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 23
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '2'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                QryCampo = 'ide_tpAmb'
                UpdCampo = 'ide_tpAmb'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnChange = Edide_tpAmbChange
                OnKeyDown = Edide_tpAmbKeyDown
              end
              object Edide_finNFe: TdmkEdit
                Left = 160
                Top = 140
                Width = 21
                Height = 21
                Alignment = taRightJustify
                TabOrder = 25
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '4'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                QryCampo = 'ide_finNFe'
                UpdCampo = 'ide_finNFe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnChange = Edide_finNFeChange
                OnKeyDown = Edide_finNFeKeyDown
              end
              object Edide_procEmi: TdmkEdit
                Left = 312
                Top = 140
                Width = 25
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                TabOrder = 27
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_procEmi'
                UpdCampo = 'ide_procEmi'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_procEmiChange
              end
              object Edide_verProc: TdmkEdit
                Left = 830
                Top = 140
                Width = 157
                Height = 21
                Enabled = False
                TabOrder = 29
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ide_verProc'
                UpdCampo = 'ide_verProc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_indPag_Txt: TdmkEdit
                Left = 832
                Top = 20
                Width = 157
                Height = 21
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_tpNF_TXT: TdmkEdit
                Left = 612
                Top = 60
                Width = 74
                Height = 21
                ReadOnly = True
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_cMunFG_TXT: TdmkEdit
                Left = 740
                Top = 60
                Width = 228
                Height = 21
                ReadOnly = True
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_TpImp_TXT: TdmkEdit
                Left = 31
                Top = 100
                Width = 234
                Height = 21
                Enabled = False
                ReadOnly = True
                TabOrder = 19
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_tpEmis_TXT: TdmkEdit
                Left = 296
                Top = 100
                Width = 693
                Height = 21
                ReadOnly = True
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_tpAmb_TXT: TdmkEdit
                Left = 56
                Top = 140
                Width = 101
                Height = 21
                ReadOnly = True
                TabOrder = 24
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_finNFe_TXT: TdmkEdit
                Left = 184
                Top = 140
                Width = 125
                Height = 21
                ReadOnly = True
                TabOrder = 26
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = Edide_finNFe_TXTExit
              end
              object Edide_procEmi_TXT: TdmkEdit
                Left = 340
                Top = 140
                Width = 485
                Height = 21
                Enabled = False
                TabOrder = 28
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object TPide_dEmi: TdmkEditDateTimePicker
                Left = 156
                Top = 60
                Width = 116
                Height = 21
                Date = 40048.941292650460000000
                Time = 40048.941292650460000000
                TabOrder = 8
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'ide_dEmi'
                UpdCampo = 'ide_dEmi'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPide_dSaiEnt: TdmkEditDateTimePicker
                Left = 372
                Top = 60
                Width = 116
                Height = 21
                Date = 40048.941462858800000000
                Time = 40048.941462858800000000
                TabOrder = 11
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'ide_dSaiEnt'
                UpdCampo = 'ide_dSaiEnt'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object Edide_CUF: TdmkEditCB
                Left = 4
                Top = 20
                Width = 33
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_CUF'
                UpdCampo = 'ide_CUF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '0'
                ValWarn = False
                DBLookupComboBox = CBide_UF
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBide_UF: TdmkDBLookupComboBox
                Left = 40
                Top = 20
                Width = 53
                Height = 21
                KeyField = 'DTB'
                ListField = 'Nome'
                ListSource = DsDTB_UFs
                TabOrder = 1
                dmkEditCB = Edide_CUF
                QryCampo = 'ide_CUF'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object Edide_hSaiEnt: TdmkEdit
                Left = 488
                Top = 60
                Width = 49
                Height = 21
                TabOrder = 12
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'ide_hSaiEnt'
                UpdCampo = 'ide_hSaiEnt'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object Edide_dhSaiEntTZD: TdmkEdit
                Left = 536
                Top = 60
                Width = 48
                Height = 21
                TabOrder = 13
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfTZD_UTC
                Texto = '+00:00'
                QryCampo = 'ide_dhSaiEntTZD'
                UpdCampo = 'ide_dhSaiEntTZD'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object Edide_dhEmiTZD: TdmkEdit
                Left = 320
                Top = 60
                Width = 48
                Height = 21
                TabOrder = 10
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfTZD_UTC
                Texto = '+00:00'
                QryCampo = 'ide_dhEmiTZD'
                UpdCampo = 'ide_dhEmiTZD'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object Edide_hEmi: TdmkEdit
                Left = 276
                Top = 60
                Width = 49
                Height = 21
                TabOrder = 9
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'ide_hEmi'
                UpdCampo = 'ide_hEmi'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object Edide_idDest: TdmkEdit
                Left = 4
                Top = 180
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 30
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '3'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_idDest'
                UpdCampo = 'ide_idDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_idDestChange
                OnKeyDown = Edide_idDestKeyDown
              end
              object Edide_idDest_TXT: TdmkEdit
                Left = 32
                Top = 180
                Width = 193
                Height = 21
                ReadOnly = True
                TabOrder = 31
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_indFinal: TdmkEdit
                Left = 228
                Top = 180
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 32
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_indFinal'
                UpdCampo = 'ide_indFinal'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_indFinalChange
                OnKeyDown = Edide_indFinalKeyDown
              end
              object Edide_indFinal_TXT: TdmkEdit
                Left = 256
                Top = 180
                Width = 125
                Height = 21
                ReadOnly = True
                TabOrder = 33
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edide_indPres: TdmkEdit
                Left = 384
                Top = 180
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 34
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ide_indPres'
                UpdCampo = 'ide_indPres'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edide_indPresChange
                OnKeyDown = Edide_indPresKeyDown
              end
              object Edide_indPres_TXT: TdmkEdit
                Left = 412
                Top = 180
                Width = 573
                Height = 21
                ReadOnly = True
                TabOrder = 35
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Emitente '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel16: TPanel
              Left = 0
              Top = 49
              Width = 1000
              Height = 320
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              ExplicitWidth = 1002
              ExplicitHeight = 323
              object Label120: TLabel
                Left = 4
                Top = 4
                Width = 42
                Height = 13
                Caption = 'CNPJ ...:'
                FocusControl = Edemit_CNPJ
              end
              object Label121: TLabel
                Left = 236
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Edemit_xNome
              end
              object Label122: TLabel
                Left = 752
                Top = 4
                Width = 74
                Height = 13
                Caption = 'Nome Fantasia:'
                FocusControl = Edemit_xFant
              end
              object Label123: TLabel
                Left = 4
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = Edemit_xLgr
              end
              object Label124: TLabel
                Left = 428
                Top = 44
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = Edemit_nro
              end
              object Label125: TLabel
                Left = 480
                Top = 44
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = Edemit_xCpl
              end
              object Label126: TLabel
                Left = 4
                Top = 84
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = Edemit_xBairro
              end
              object Label127: TLabel
                Left = 428
                Top = 84
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = Edemit_cMun
              end
              object Label128: TLabel
                Left = 4
                Top = 124
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = Edemit_UF
              end
              object Label129: TLabel
                Left = 36
                Top = 124
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = Edemit_CEP
              end
              object Label130: TLabel
                Left = 100
                Top = 124
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = Edemit_cPais
              end
              object Label131: TLabel
                Left = 844
                Top = 124
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Edemit_fone
              end
              object Label132: TLabel
                Left = 4
                Top = 164
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = Edemit_IE
              end
              object Label133: TLabel
                Left = 140
                Top = 164
                Width = 127
                Height = 13
                Caption = 'I.E. do Substituto tribut'#225'rio:'
                FocusControl = Edemit_IEST
              end
              object Label134: TLabel
                Left = 276
                Top = 164
                Width = 94
                Height = 13
                Caption = 'Inscri'#231#227'o Municipal:'
                FocusControl = Edemit_IM
              end
              object Label135: TLabel
                Left = 412
                Top = 164
                Width = 62
                Height = 13
                Caption = 'CNAE Fiscal:'
              end
              object Label136: TLabel
                Left = 120
                Top = 4
                Width = 50
                Height = 13
                Caption = '... ou CPF:'
                FocusControl = Edemit_CPF
              end
              object Label360: TLabel
                Left = 748
                Top = 164
                Width = 25
                Height = 13
                Caption = 'CRT:'
              end
              object Edemit_CNPJ: TdmkEdit
                Left = 4
                Top = 20
                Width = 112
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_CNPJ'
                UpdCampo = 'emit_CNPJ'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xNome: TdmkEdit
                Left = 236
                Top = 20
                Width = 513
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xNome'
                UpdCampo = 'emit_xNome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xFant: TdmkEdit
                Left = 752
                Top = 20
                Width = 229
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xFant'
                UpdCampo = 'emit_xFant'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xLgr: TdmkEdit
                Left = 4
                Top = 60
                Width = 420
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xLgr'
                UpdCampo = 'emit_xLgr'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_nro: TdmkEdit
                Left = 428
                Top = 60
                Width = 48
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_nro'
                UpdCampo = 'emit_nro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xCpl: TdmkEdit
                Left = 480
                Top = 60
                Width = 501
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xCpl'
                UpdCampo = 'emit_xCpl'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xBairro: TdmkEdit
                Left = 4
                Top = 100
                Width = 420
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xBairro'
                UpdCampo = 'emit_xBairro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_cMun: TdmkEdit
                Left = 428
                Top = 100
                Width = 56
                Height = 21
                Alignment = taRightJustify
                MaxLength = 7
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'emit_cMun'
                UpdCampo = 'emit_cMun'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edemit_cMunChange
              end
              object Edemit_UF: TdmkEdit
                Left = 4
                Top = 140
                Width = 30
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_UF'
                UpdCampo = 'emit_UF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = Edemit_UFChange
              end
              object Edemit_CEP: TdmkEdit
                Left = 36
                Top = 140
                Width = 61
                Height = 21
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtCEP_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_CEP'
                UpdCampo = 'emit_CEP'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_cPais: TdmkEdit
                Left = 100
                Top = 140
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'emit_cPais'
                UpdCampo = 'emit_cPais'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edemit_cPaisChange
              end
              object Edemit_fone: TdmkEdit
                Left = 843
                Top = 140
                Width = 138
                Height = 21
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtTel_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_fone'
                UpdCampo = 'emit_fone'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_IE: TdmkEdit
                Left = 4
                Top = 180
                Width = 132
                Height = 21
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtIE_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_IE'
                UpdCampo = 'emit_IE'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_IEST: TdmkEdit
                Left = 140
                Top = 180
                Width = 132
                Height = 21
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtIE_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_IEST'
                UpdCampo = 'emit_IEST'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_IM: TdmkEdit
                Left = 276
                Top = 180
                Width = 132
                Height = 21
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_IM'
                UpdCampo = 'emit_IM'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xMun: TdmkEdit
                Left = 484
                Top = 100
                Width = 497
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xMun'
                UpdCampo = 'emit_xMun'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_xPais: TdmkEdit
                Left = 148
                Top = 140
                Width = 693
                Height = 21
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_xPais'
                UpdCampo = 'emit_xPais'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_CPF: TdmkEdit
                Left = 120
                Top = 20
                Width = 112
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_CPF'
                UpdCampo = 'emit_CPF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edemit_CNAE: TdmkEditCB
                Left = 412
                Top = 180
                Width = 52
                Height = 21
                TabOrder = 18
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'emit_CNAE'
                UpdCampo = 'emit_CNAE'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                DBLookupComboBox = CBemit_CNAE
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBemit_CNAE: TdmkDBLookupComboBox
                Left = 464
                Top = 180
                Width = 281
                Height = 21
                KeyField = 'CodTxt'
                ListField = 'Nome'
                TabOrder = 19
                dmkEditCB = Edemit_CNAE
                QryCampo = 'emit_CNAE'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object Edemit_CRT: TdmkEdit
                Left = 748
                Top = 180
                Width = 24
                Height = 21
                Alignment = taRightJustify
                TabOrder = 20
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'emit_CRT'
                UpdCampo = 'emit_CRT'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Edemit_CRTChange
                OnExit = Edemit_CRTExit
              end
              object Edemit_CRT_TXT: TdmkEdit
                Left = 772
                Top = 180
                Width = 209
                Height = 21
                ReadOnly = True
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object Panel56: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1002
              object Label359: TLabel
                Left = 4
                Top = 4
                Width = 129
                Height = 13
                Caption = 'Emitente [F5 busca dados]:'
              end
              object SbEntiEmit: TSpeedButton
                Left = 960
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbEntiEmitClick
              end
              object EdCodInfoEmit: TdmkEditCB
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CodInfoEmit'
                UpdCampo = 'CodInfoEmit'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnKeyDown = EdCodInfoEmitKeyDown
                DBLookupComboBox = CBCodInfoEmit
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCodInfoEmit: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 889
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NO_ENT'
                ListSource = DsEntiEmit
                TabOrder = 1
                OnKeyDown = EdCodInfoEmitKeyDown
                dmkEditCB = EdCodInfoEmit
                QryCampo = 'CodInfoEmit'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' Destinat'#225'rio '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel15: TPanel
              Left = 0
              Top = 49
              Width = 1000
              Height = 320
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              ExplicitWidth = 1002
              ExplicitHeight = 323
              object Label7: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'CNPJ...:'
                FocusControl = Eddest_CNPJ
              end
              object Label8: TLabel
                Left = 372
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Eddest_xNome
              end
              object Label9: TLabel
                Left = 4
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = Eddest_xLgr
              end
              object Label109: TLabel
                Left = 428
                Top = 44
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = Eddest_nro
              end
              object Label110: TLabel
                Left = 480
                Top = 44
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = Eddest_xCpl
              end
              object Label111: TLabel
                Left = 4
                Top = 84
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = Eddest_xBairro
              end
              object Label112: TLabel
                Left = 428
                Top = 84
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = Eddest_cMun
              end
              object Label113: TLabel
                Left = 4
                Top = 124
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = Eddest_UF
              end
              object Label114: TLabel
                Left = 36
                Top = 124
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = Eddest_CEP
              end
              object Label115: TLabel
                Left = 100
                Top = 124
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = Eddest_cPais
              end
              object Label119: TLabel
                Left = 120
                Top = 4
                Width = 59
                Height = 13
                Caption = '... ou CPF...:'
                FocusControl = Eddest_CPF
              end
              object Label116: TLabel
                Left = 536
                Top = 124
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Eddest_fone
              end
              object Label117: TLabel
                Left = 632
                Top = 124
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = Eddest_IE
              end
              object Label118: TLabel
                Left = 756
                Top = 124
                Width = 116
                Height = 13
                Caption = 'Inscri'#231#227'o na SUFRAMA:'
                FocusControl = Eddest_ISUF
              end
              object Label361: TLabel
                Left = 4
                Top = 164
                Width = 31
                Height = 13
                Caption = 'E-mail:'
                FocusControl = Eddest_email
              end
              object Label364: TLabel
                Left = 236
                Top = 4
                Width = 82
                Height = 13
                Caption = '... ou estrangeiro:'
                FocusControl = Eddest_idEstrangeiro
              end
              object Label365: TLabel
                Left = 4
                Top = 204
                Width = 147
                Height = 13
                Caption = 'Indicador da IE do destinat'#225'rio:'
                FocusControl = Eddest_indIEDest
              end
              object Label382: TLabel
                Left = 880
                Top = 124
                Width = 94
                Height = 13
                Caption = 'Inscri'#231#227'o Municipal:'
                FocusControl = Eddest_IM
              end
              object Eddest_CNPJ: TdmkEdit
                Left = 4
                Top = 20
                Width = 113
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_CNPJ'
                UpdCampo = 'dest_CNPJ'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xNome: TdmkEdit
                Left = 372
                Top = 20
                Width = 609
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xNome'
                UpdCampo = 'dest_xNome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xLgr: TdmkEdit
                Left = 4
                Top = 60
                Width = 420
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xLgr'
                UpdCampo = 'dest_xLgr'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_nro: TdmkEdit
                Left = 428
                Top = 60
                Width = 48
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_nro'
                UpdCampo = 'dest_nro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xCpl: TdmkEdit
                Left = 480
                Top = 60
                Width = 501
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xCpl'
                UpdCampo = 'dest_xCpl'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xBairro: TdmkEdit
                Left = 4
                Top = 100
                Width = 420
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xBairro'
                UpdCampo = 'dest_xBairro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_cMun: TdmkEdit
                Left = 428
                Top = 100
                Width = 56
                Height = 21
                Alignment = taRightJustify
                MaxLength = 7
                TabOrder = 7
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'dest_cMun'
                UpdCampo = 'dest_cMun'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Eddest_cMunChange
              end
              object Eddest_UF: TdmkEdit
                Left = 4
                Top = 140
                Width = 30
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_UF'
                UpdCampo = 'dest_UF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = Eddest_UFChange
              end
              object Eddest_CEP: TdmkEdit
                Left = 36
                Top = 140
                Width = 61
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtCEP_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_CEP'
                UpdCampo = 'dest_CEP'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_cPais: TdmkEdit
                Left = 100
                Top = 140
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 11
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'dest_cPais'
                UpdCampo = 'dest_cPais'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Eddest_cPaisChange
              end
              object Eddest_xMun: TdmkEdit
                Left = 484
                Top = 100
                Width = 497
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xMun'
                UpdCampo = 'dest_xMun'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_CPF: TdmkEdit
                Left = 120
                Top = 20
                Width = 113
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_CPF'
                UpdCampo = 'dest_CPF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xPais: TdmkEdit
                Left = 148
                Top = 140
                Width = 385
                Height = 21
                TabOrder = 12
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xPais'
                UpdCampo = 'dest_xPais'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_fone: TdmkEdit
                Left = 535
                Top = 140
                Width = 92
                Height = 21
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtTel_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_fone'
                UpdCampo = 'dest_fone'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_IE: TdmkEdit
                Left = 632
                Top = 140
                Width = 120
                Height = 21
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtIE_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_IE'
                UpdCampo = 'dest_IE'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_ISUF: TdmkEdit
                Left = 756
                Top = 140
                Width = 120
                Height = 21
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_ISUF'
                UpdCampo = 'dest_ISUF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_email: TdmkEdit
                Left = 4
                Top = 180
                Width = 977
                Height = 21
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_email'
                UpdCampo = 'dest_email'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_idEstrangeiro: TdmkEdit
                Left = 236
                Top = 20
                Width = 132
                Height = 21
                TabOrder = 18
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_idEstrangeiro'
                UpdCampo = 'dest_idEstrangeiro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_indIEDest: TdmkEdit
                Left = 4
                Top = 220
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 19
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'dest_indIEDest'
                UpdCampo = 'dest_indIEDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = Eddest_indIEDestChange
              end
              object Eddest_indIEDest_TXT: TdmkEdit
                Left = 32
                Top = 220
                Width = 949
                Height = 21
                ReadOnly = True
                TabOrder = 20
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CkEstrangDef: TdmkCheckBox
                Left = 4
                Top = 247
                Width = 97
                Height = 17
                Caption = 'Estrangeiro.'
                TabOrder = 21
                QryCampo = 'EstrangDef'
                UpdCampo = 'EstrangDef'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object Eddest_IM: TdmkEdit
                Left = 880
                Top = 140
                Width = 101
                Height = 21
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_IM'
                UpdCampo = 'dest_IM'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object Panel42: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1002
              object Label305: TLabel
                Left = 4
                Top = 4
                Width = 144
                Height = 13
                Caption = 'Destinat'#225'rio [F5 busca dados]:'
              end
              object SbEntiDest: TSpeedButton
                Left = 960
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbEntiDestClick
              end
              object EdCodInfoDest: TdmkEditCB
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CodInfoDest'
                UpdCampo = 'CodInfoDest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnKeyDown = EdCodInfoDestKeyDown
                DBLookupComboBox = CBCodInfoDest
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCodInfoDest: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 889
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NO_ENT'
                ListSource = DsEntiDest
                TabOrder = 1
                OnKeyDown = EdCodInfoDestKeyDown
                dmkEditCB = EdCodInfoDest
                QryCampo = 'CodInfoDest'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet9: TTabSheet
            Caption = ' Transporte '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel20: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1002
              ExplicitHeight = 372
              object Label163: TLabel
                Left = 8
                Top = 56
                Width = 97
                Height = 13
                Caption = 'Modalidade do frete:'
                FocusControl = EdModFrete
              end
              object Label164: TLabel
                Left = 240
                Top = 56
                Width = 157
                Height = 13
                Caption = 'CNPJ da transportadora, ou CPF:'
                FocusControl = Edtransporta_CNPJ
              end
              object Label165: TLabel
                Left = 8
                Top = 96
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Edtransporta_xNome
              end
              object Label167: TLabel
                Left = 8
                Top = 136
                Width = 49
                Height = 13
                Caption = 'Endere'#231'o:'
                FocusControl = Transporta_XEnder
              end
              object Label168: TLabel
                Left = 8
                Top = 176
                Width = 96
                Height = 13
                Caption = 'Nome do Munic'#237'pio:'
                FocusControl = Transporta_XMun
              end
              object Label169: TLabel
                Left = 364
                Top = 176
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdTransporta_UF
              end
              object Label170: TLabel
                Left = 8
                Top = 220
                Width = 79
                Height = 13
                Caption = 'Valor do servi'#231'o:'
                FocusControl = EdRetTransp_vServ
              end
              object Label171: TLabel
                Left = 8
                Top = 240
                Width = 121
                Height = 13
                Caption = 'BC da reten'#231#227'o do ICMS:'
                FocusControl = EdRetTransp_vBCRet
              end
              object Label172: TLabel
                Left = 8
                Top = 264
                Width = 108
                Height = 13
                Caption = 'Al'#237'quota da Reten'#231#227'o:'
                FocusControl = EdRetTransp_PICMSRet
              end
              object Label173: TLabel
                Left = 8
                Top = 292
                Width = 100
                Height = 13
                Caption = 'Valor do ICMS retido:'
                FocusControl = EdRetTransp_vICMSRet
              end
              object Label174: TLabel
                Left = 228
                Top = 220
                Width = 31
                Height = 13
                Caption = 'CFOP:'
                FocusControl = EdRetTransp_CFOP
              end
              object Label175: TLabel
                Left = 8
                Top = 316
                Width = 352
                Height = 13
                Caption = 
                  'C'#243'digo do munic'#237'pio de ocorr'#234'ncia do fato gerador do ICMS do tra' +
                  'nsporte:'
                FocusControl = EdRetTransp_CMunFG
              end
              object Label176: TLabel
                Left = 228
                Top = 244
                Width = 30
                Height = 13
                Caption = 'Placa:'
                FocusControl = EdVeicTransp_Placa
              end
              object Label177: TLabel
                Left = 228
                Top = 268
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdVeicTransp_UF
              end
              object Label178: TLabel
                Left = 228
                Top = 292
                Width = 33
                Height = 13
                Caption = 'RNTC:'
                FocusControl = EdVeicTransp_RNTC
              end
              object SpeedButton8: TSpeedButton
                Left = 447
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton8Click
              end
              object Label394: TLabel
                Left = 368
                Top = 96
                Width = 19
                Height = 13
                Caption = 'I.E.:'
                FocusControl = DBEdit101
              end
              object Label166: TLabel
                Left = 8
                Top = 340
                Width = 34
                Height = 13
                Caption = 'Vag'#227'o:'
                FocusControl = EdVagao
              end
              object Label395: TLabel
                Left = 276
                Top = 340
                Width = 29
                Height = 13
                Caption = 'Balsa:'
                FocusControl = EdBalsa
              end
              object Label404: TLabel
                Left = 8
                Top = 4
                Width = 69
                Height = 13
                Caption = 'Transportador:'
              end
              object EdModFrete: TdmkEdit
                Left = 8
                Top = 72
                Width = 29
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'ModFrete'
                UpdCampo = 'ModFrete'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdModFreteChange
              end
              object EdModFrete_TXT: TdmkEdit
                Left = 40
                Top = 72
                Width = 197
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Edtransporta_CNPJ: TdmkEdit
                Left = 240
                Top = 72
                Width = 134
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_CNPJ'
                UpdCampo = 'Transporta_CNPJ'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = Edtransporta_CNPJChange
              end
              object Edtransporta_CPF: TdmkEdit
                Left = 376
                Top = 72
                Width = 92
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_CPF'
                UpdCampo = 'Transporta_CPF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = Edtransporta_CPFChange
              end
              object Edtransporta_xNome: TdmkEdit
                Left = 8
                Top = 112
                Width = 357
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_XNome'
                UpdCampo = 'Transporta_XNome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTransporta_IE: TdmkEdit
                Left = 368
                Top = 112
                Width = 113
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtIE_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_IE'
                UpdCampo = 'Transporta_IE'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Transporta_XEnder: TdmkEdit
                Left = 8
                Top = 152
                Width = 473
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_XEnder'
                UpdCampo = 'Transporta_XEnder'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Transporta_XMun: TdmkEdit
                Left = 8
                Top = 192
                Width = 353
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_XMun'
                UpdCampo = 'Transporta_XMun'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTransporta_UF: TdmkEdit
                Left = 364
                Top = 192
                Width = 30
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Transporta_UF'
                UpdCampo = 'Transporta_UF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdTransporta_UFChange
              end
              object EdRetTransp_vServ: TdmkEdit
                Left = 140
                Top = 216
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 11
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'RetTransp_vServ'
                UpdCampo = 'RetTransp_vServ'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdRetTransp_vBCRet: TdmkEdit
                Left = 140
                Top = 240
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'RetTransp_vBCRet'
                UpdCampo = 'RetTransp_vBCRet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdRetTransp_PICMSRet: TdmkEdit
                Left = 140
                Top = 264
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 13
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'RetTransp_PICMSRet'
                UpdCampo = 'RetTransp_PICMSRet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdRetTransp_vICMSRet: TdmkEdit
                Left = 140
                Top = 288
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'RetTransp_vICMSRet'
                UpdCampo = 'RetTransp_vICMSRet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdRetTransp_CFOP: TdmkEdit
                Left = 268
                Top = 216
                Width = 56
                Height = 21
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'RetTransp_CFOP'
                UpdCampo = 'RetTransp_CFOP'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRetTransp_CMunFG: TdmkEdit
                Left = 368
                Top = 312
                Width = 56
                Height = 21
                MaxLength = 7
                TabOrder = 19
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'RetTransp_CMunFG'
                UpdCampo = 'RetTransp_CMunFG'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdVeicTransp_Placa: TdmkEdit
                Left = 268
                Top = 240
                Width = 108
                Height = 21
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'VeicTransp_Placa'
                UpdCampo = 'VeicTransp_Placa'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdVeicTransp_UF: TdmkEdit
                Left = 268
                Top = 264
                Width = 30
                Height = 21
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'VeicTransp_UF'
                UpdCampo = 'VeicTransp_UF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdVeicTransp_RNTC: TdmkEdit
                Left = 268
                Top = 288
                Width = 264
                Height = 21
                TabOrder = 18
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'VeicTransp_RNTC'
                UpdCampo = 'VeicTransp_RNTC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRetTransp_CMunFG_TXT: TdmkEdit
                Left = 428
                Top = 312
                Width = 557
                Height = 21
                ReadOnly = True
                TabOrder = 20
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdRetTransp_CMunFG_TXTExit
              end
              object EdVagao: TdmkEdit
                Left = 48
                Top = 336
                Width = 224
                Height = 21
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Vagao'
                UpdCampo = 'Vagao'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdBalsa: TdmkEdit
                Left = 308
                Top = 336
                Width = 224
                Height = 21
                TabOrder = 22
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Balsa'
                UpdCampo = 'Balsa'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTransporta: TdmkEditCB
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CodInfoTrsp'
                UpdCampo = 'CodInfoTrsp'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnKeyDown = EdCodInfoDestKeyDown
                DBLookupComboBox = CBTransporta
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBTransporta: TdmkDBLookupComboBox
                Left = 68
                Top = 20
                Width = 374
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsTransportas
                TabOrder = 1
                OnKeyDown = EdCodInfoDestKeyDown
                dmkEditCB = EdTransporta
                QryCampo = 'CodInfoTrsp'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = ' Informa'#231#245'es '
            ImageIndex = 5
            object Panel51: TPanel
              Left = 0
              Top = 0
              Width = 752
              Height = 369
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label179: TLabel
                Left = 4
                Top = 0
                Width = 211
                Height = 13
                Caption = 'Informa'#231#245'es adicionais de interesse do fisco:'
              end
              object Label180: TLabel
                Left = 4
                Top = 104
                Width = 274
                Height = 13
                Caption = 'Informa'#231#245'es complementares de interesse do contribuinte:'
              end
              object GroupBox9: TGroupBox
                Left = 4
                Top = 240
                Width = 744
                Height = 61
                Caption = ' Com'#233'rcio exterior ( informa'#231#245'es de embarque): '
                TabOrder = 0
                object Label181: TLabel
                  Left = 12
                  Top = 16
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = dmkEdit115
                end
                object Label182: TLabel
                  Left = 44
                  Top = 16
                  Width = 243
                  Height = 13
                  Caption = 'Local de embarque ou de transposi'#231#227'o de fronteira:'
                  FocusControl = dmkEdit116
                end
                object Label402: TLabel
                  Left = 392
                  Top = 16
                  Width = 264
                  Height = 13
                  Caption = 'Descri'#231#227'o do local de despacho (Recinto Alfandeg'#225'rio):'
                  FocusControl = EdExporta_XLocDespacho
                end
                object dmkEdit115: TdmkEdit
                  Left = 12
                  Top = 32
                  Width = 30
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Exporta_UFEmbarq'
                  UpdCampo = 'Exporta_UFEmbarq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEdit116: TdmkEdit
                  Left = 44
                  Top = 32
                  Width = 344
                  Height = 21
                  MaxLength = 60
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Exporta_XLocEmbarq'
                  UpdCampo = 'Exporta_XLocEmbarq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdExporta_XLocDespacho: TdmkEdit
                  Left = 392
                  Top = 32
                  Width = 344
                  Height = 21
                  MaxLength = 60
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Exporta_XLocDespacho'
                  UpdCampo = 'Exporta_XLocDespacho'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox10: TGroupBox
                Left = 4
                Top = 304
                Width = 744
                Height = 61
                Caption = 
                  ' Informa'#231#245'es de compras (Obs.: A nota de  empenho se refere a co' +
                  'mpras p'#250'blicas): '
                TabOrder = 1
                object Label183: TLabel
                  Left = 12
                  Top = 16
                  Width = 88
                  Height = 13
                  Caption = 'Nota de empenho:'
                  FocusControl = EdCompra_XNEmp
                end
                object Label184: TLabel
                  Left = 132
                  Top = 16
                  Width = 36
                  Height = 13
                  Caption = 'Pedido:'
                  FocusControl = EdCompra_XPed
                end
                object Label185: TLabel
                  Left = 436
                  Top = 16
                  Width = 96
                  Height = 13
                  Caption = 'Contrato de compra:'
                  FocusControl = EdCompra_XCont
                end
                object EdCompra_XNEmp: TdmkEdit
                  Left = 12
                  Top = 32
                  Width = 116
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XNEmp'
                  UpdCampo = 'Compra_XNEmp'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XPed: TdmkEdit
                  Left = 132
                  Top = 32
                  Width = 300
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XPed'
                  UpdCampo = 'Compra_XPed'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompra_XCont: TdmkEdit
                  Left = 436
                  Top = 32
                  Width = 300
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Compra_XCont'
                  UpdCampo = 'Compra_XCont'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = EdCompra_XContExit
                end
              end
              object MeInfAdic_InfCpl: TdmkMemo
                Left = 4
                Top = 120
                Width = 744
                Height = 117
                MaxLength = 5000
                TabOrder = 2
                QryCampo = 'InfAdic_InfCpl'
                UpdCampo = 'InfAdic_InfCpl'
                UpdType = utYes
              end
              object MeInfAdic_InfAdFisco: TdmkMemo
                Left = 4
                Top = 16
                Width = 744
                Height = 85
                MaxLength = 2000
                TabOrder = 3
                QryCampo = 'InfAdic_InfAdFisco'
                UpdCampo = 'InfAdic_InfAdFisco'
                UpdType = utYes
              end
            end
            object Panel71: TPanel
              Left = 752
              Top = 0
              Width = 248
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' Totais de impostos '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel52: TPanel
              Left = 0
              Top = 199
              Width = 1002
              Height = 173
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object GroupBox32: TGroupBox
                Left = 0
                Top = 0
                Width = 1002
                Height = 173
                Align = alClient
                Caption = ' Recupera'#231#227'o de impostos: '
                TabOrder = 0
                object GroupBox33: TGroupBox
                  Left = 721
                  Top = 15
                  Width = 248
                  Height = 156
                  Align = alLeft
                  Caption = ' COFINS: '
                  TabOrder = 3
                  object Label312: TLabel
                    Left = 8
                    Top = 20
                    Width = 51
                    Height = 13
                    Caption = '% Red BC:'
                    FocusControl = EdCOFINSRec_pRedBC
                  end
                  object Label313: TLabel
                    Left = 68
                    Top = 20
                    Width = 26
                    Height = 13
                    Caption = '$ BC:'
                    FocusControl = EdCOFINSRec_vBC
                  end
                  object Label343: TLabel
                    Left = 68
                    Top = 60
                    Width = 27
                    Height = 13
                    Caption = 'Valor:'
                    FocusControl = EdCOFINSRec_vCOFINS
                  end
                  object Label347: TLabel
                    Left = 8
                    Top = 60
                    Width = 53
                    Height = 13
                    Caption = '% COFINS:'
                    FocusControl = EdCOFINSRec_pAliq
                  end
                  object EdCOFINSRec_pRedBC: TdmkEdit
                    Left = 8
                    Top = 36
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'COFINSRec_pRedBC'
                    UpdCampo = 'COFINSRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdCOFINSRec_vBC: TdmkEdit
                    Left = 68
                    Top = 36
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'COFINSRec_vBC'
                    UpdCampo = 'COFINSRec_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdCOFINSRec_vCOFINS: TdmkEdit
                    Left = 68
                    Top = 76
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'COFINSRec_vCOFINS'
                    UpdCampo = 'COFINSRec_vCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCOFINSRec_pAliq: TdmkEdit
                    Left = 8
                    Top = 76
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '7,60'
                    QryCampo = 'COFINSRec_pAliq'
                    UpdCampo = 'COFINSRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 7.600000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                end
                object GroupBox34: TGroupBox
                  Left = 481
                  Top = 15
                  Width = 240
                  Height = 156
                  Align = alLeft
                  Caption = ' PIS: '
                  TabOrder = 2
                  object Label334: TLabel
                    Left = 8
                    Top = 20
                    Width = 51
                    Height = 13
                    Caption = '% Red BC:'
                    FocusControl = EdPISRec_pRedBC
                  end
                  object Label335: TLabel
                    Left = 68
                    Top = 20
                    Width = 26
                    Height = 13
                    Caption = '$ BC:'
                    FocusControl = EdPISRec_vBC
                  end
                  object Label342: TLabel
                    Left = 68
                    Top = 60
                    Width = 27
                    Height = 13
                    Caption = 'Valor:'
                    FocusControl = EdPISRec_vPIS
                  end
                  object Label346: TLabel
                    Left = 8
                    Top = 60
                    Width = 31
                    Height = 13
                    Caption = '% PIS:'
                    FocusControl = EdPISRec_pAliq
                  end
                  object EdPISRec_pRedBC: TdmkEdit
                    Left = 8
                    Top = 36
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'PISRec_pRedBC'
                    UpdCampo = 'PISRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdPISRec_vBC: TdmkEdit
                    Left = 68
                    Top = 36
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'PISRec_vBC'
                    UpdCampo = 'PISRec_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdPISRec_vPIS: TdmkEdit
                    Left = 68
                    Top = 76
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'PISRec_vPIS'
                    UpdCampo = 'PISRec_vPIS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdPISRec_pAliq: TdmkEdit
                    Left = 8
                    Top = 76
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,65'
                    QryCampo = 'PISRec_pAliq'
                    UpdCampo = 'PISRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.650000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                end
                object GroupBox35: TGroupBox
                  Left = 241
                  Top = 15
                  Width = 240
                  Height = 156
                  Align = alLeft
                  Caption = ' IPI: '
                  TabOrder = 1
                  object Label336: TLabel
                    Left = 8
                    Top = 20
                    Width = 51
                    Height = 13
                    Caption = '% Red BC:'
                    FocusControl = EdIPIRec_pRedBC
                  end
                  object Label337: TLabel
                    Left = 68
                    Top = 20
                    Width = 26
                    Height = 13
                    Caption = '$ BC:'
                    FocusControl = EdIPIRec_vBC
                  end
                  object Label341: TLabel
                    Left = 68
                    Top = 60
                    Width = 27
                    Height = 13
                    Caption = 'Valor:'
                    FocusControl = EdIPIRec_vIPI
                  end
                  object Label345: TLabel
                    Left = 8
                    Top = 60
                    Width = 27
                    Height = 13
                    Caption = '% IPI:'
                    FocusControl = EdIPIRec_pAliq
                  end
                  object EdIPIRec_pRedBC: TdmkEdit
                    Left = 8
                    Top = 36
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'IPIRec_pRedBC'
                    UpdCampo = 'IPIRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdIPIRec_vBC: TdmkEdit
                    Left = 68
                    Top = 36
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'IPIRec_vBC'
                    UpdCampo = 'IPIRec_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdIPIRec_vIPI: TdmkEdit
                    Left = 68
                    Top = 76
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'IPIRec_vIPI'
                    UpdCampo = 'IPIRec_vIPI'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdIPIRec_pAliq: TdmkEdit
                    Left = 8
                    Top = 76
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'IPIRec_pAliq'
                    UpdCampo = 'IPIRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                end
                object GroupBox36: TGroupBox
                  Left = 2
                  Top = 15
                  Width = 239
                  Height = 156
                  Align = alLeft
                  Caption = ' ICMS: '
                  TabOrder = 0
                  object Label338: TLabel
                    Left = 8
                    Top = 20
                    Width = 51
                    Height = 13
                    Caption = '% Red BC:'
                    FocusControl = EdICMSRec_pRedBC
                  end
                  object Label339: TLabel
                    Left = 68
                    Top = 20
                    Width = 26
                    Height = 13
                    Caption = '$ BC:'
                    FocusControl = EdICMSRec_vBC
                  end
                  object Label340: TLabel
                    Left = 68
                    Top = 60
                    Width = 27
                    Height = 13
                    Caption = 'Valor:'
                    FocusControl = EdICMSRec_vICMS
                  end
                  object Label344: TLabel
                    Left = 8
                    Top = 60
                    Width = 40
                    Height = 13
                    Caption = '% ICMS:'
                    FocusControl = EdICMSRec_pAliq
                  end
                  object EdICMSRec_pRedBC: TdmkEdit
                    Left = 8
                    Top = 36
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'ICMSRec_pRedBC'
                    UpdCampo = 'ICMSRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdICMSRec_vBC: TdmkEdit
                    Left = 68
                    Top = 36
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSRec_vBC'
                    UpdCampo = 'ICMSRec_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdICMSRec_vICMS: TdmkEdit
                    Left = 68
                    Top = 76
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSRec_vICMS'
                    UpdCampo = 'ICMSRec_vICMS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSRec_pAliq: TdmkEdit
                    Left = 8
                    Top = 76
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '7,00'
                    QryCampo = 'ICMSRec_pAliq'
                    UpdCampo = 'ICMSRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 7.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                end
              end
            end
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 199
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Panel18: TPanel
                Left = 684
                Top = 0
                Width = 318
                Height = 199
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object GroupBox6: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 318
                  Height = 109
                  Align = alTop
                  Caption = ' Valores totais referentes ao ISSQN: '
                  TabOrder = 0
                  object Label137: TLabel
                    Left = 8
                    Top = 16
                    Width = 69
                    Height = 13
                    Caption = 'Valor servi'#231'os:'
                    FocusControl = EdISSQNtot_vServ
                  end
                  object Label138: TLabel
                    Left = 92
                    Top = 16
                    Width = 52
                    Height = 13
                    Caption = 'BC do ISS:'
                    FocusControl = EdISSQNtot_vBC
                  end
                  object Label139: TLabel
                    Left = 176
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total do ISS:'
                    FocusControl = EdISSQNtot_vISS
                  end
                  object Label140: TLabel
                    Left = 8
                    Top = 56
                    Width = 62
                    Height = 13
                    Caption = 'Valor do PIS:'
                    FocusControl = EdISSQNtot_vPIS
                  end
                  object Label141: TLabel
                    Left = 92
                    Top = 56
                    Width = 69
                    Height = 13
                    Caption = 'Valor COFINS:'
                    FocusControl = EdISSQNtot_vCOFINS
                  end
                  object EdISSQNtot_vServ: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ISSQNtot_vServ'
                    UpdCampo = 'ISSQNtot_vServ'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdISSQNtot_vBC: TdmkEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ISSQNtot_vBC'
                    UpdCampo = 'ISSQNtot_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdISSQNtot_vISS: TdmkEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ISSQNtot_vISS'
                    UpdCampo = 'ISSQNtot_vISS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdISSQNtot_vPIS: TdmkEdit
                    Left = 8
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ISSQNtot_vPIS'
                    UpdCampo = 'ISSQNtot_vPIS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdISSQNtot_vCOFINS: TdmkEdit
                    Left = 92
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ISSQNtot_vCOFINS'
                    UpdCampo = 'ISSQNtot_vCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object GroupBox46: TGroupBox
                  Left = 0
                  Top = 109
                  Width = 318
                  Height = 57
                  Align = alTop
                  Caption = ' Val. tot. ref. ICMS em Oper. Interest. Consum. Final: '
                  TabOrder = 1
                  object Label373: TLabel
                    Left = 8
                    Top = 16
                    Width = 50
                    Height = 13
                    Caption = 'Total FCP:'
                    FocusControl = EdICMSTot_vFCPUFDest
                  end
                  object Label374: TLabel
                    Left = 92
                    Top = 16
                    Width = 79
                    Height = 13
                    Caption = 'Total ICMS dest:'
                    FocusControl = EdICMSTot_vICMSUFDest
                  end
                  object Label375: TLabel
                    Left = 176
                    Top = 16
                    Width = 85
                    Height = 13
                    Caption = 'Total ICMS remet:'
                    FocusControl = EdICMSTot_vICMSUFRemet
                  end
                  object EdICMSTot_vFCPUFDest: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vFCPUFDest'
                    UpdCampo = 'ICMSTot_vFCPUFDest'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vICMSUFDest: TdmkEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vICMSUFDest'
                    UpdCampo = 'ICMSTot_vICMSUFDest'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vICMSUFRemet: TdmkEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vICMSUFRemet'
                    UpdCampo = 'ICMSTot_vICMSUFRemet'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object Panel19: TPanel
                Left = 0
                Top = 0
                Width = 684
                Height = 199
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox7: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 684
                  Height = 138
                  Align = alTop
                  Caption = ' Valores totais referentes ao ICMS: '
                  TabOrder = 0
                  object Label142: TLabel
                    Left = 8
                    Top = 16
                    Width = 61
                    Height = 13
                    Caption = 'BC do ICMS:'
                    FocusControl = EdICMSTot_vBC
                  end
                  object Label143: TLabel
                    Left = 92
                    Top = 16
                    Width = 71
                    Height = 13
                    Caption = 'Valor do ICMS:'
                    FocusControl = EdICMSTot_vICMS
                  end
                  object Label144: TLabel
                    Left = 260
                    Top = 16
                    Width = 78
                    Height = 13
                    Caption = 'BC do ICMS ST:'
                    FocusControl = EdICMSTot_vBCST
                  end
                  object Label145: TLabel
                    Left = 344
                    Top = 16
                    Width = 73
                    Height = 13
                    Caption = 'Valor ICMS ST:'
                    FocusControl = EdICMSTot_vST
                  end
                  object Label146: TLabel
                    Left = 428
                    Top = 16
                    Width = 81
                    Height = 13
                    Caption = 'Tot. prod e serv.:'
                    FocusControl = EdICMSTot_vProd
                  end
                  object Label147: TLabel
                    Left = 512
                    Top = 16
                    Width = 51
                    Height = 13
                    Caption = 'Total frete:'
                    FocusControl = EdICMSTot_vFrete
                  end
                  object Label148: TLabel
                    Left = 596
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total seguro:'
                    FocusControl = EdICMSTot_vSeg
                  end
                  object Label149: TLabel
                    Left = 8
                    Top = 56
                    Width = 76
                    Height = 13
                    Caption = 'Total Desconto:'
                    FocusControl = EdICMSTot_vDesc
                  end
                  object Label150: TLabel
                    Left = 92
                    Top = 56
                    Width = 51
                    Height = 13
                    Caption = 'Total do II:'
                    FocusControl = EdICMSTot_vII
                  end
                  object Label151: TLabel
                    Left = 176
                    Top = 56
                    Width = 58
                    Height = 13
                    Caption = 'Total do IPI:'
                    FocusControl = EdICMSTot_vIPI
                  end
                  object Label152: TLabel
                    Left = 260
                    Top = 56
                    Width = 62
                    Height = 13
                    Caption = 'Total do PIS:'
                    FocusControl = EdICMSTot_vPIS
                  end
                  object Label153: TLabel
                    Left = 344
                    Top = 56
                    Width = 69
                    Height = 13
                    Caption = 'Total COFINS:'
                    FocusControl = EdICMSTot_vCOFINS
                  end
                  object Label154: TLabel
                    Left = 428
                    Top = 56
                    Width = 61
                    Height = 13
                    Caption = 'Total Outros:'
                    FocusControl = EdICMSTot_vOutro
                  end
                  object Label155: TLabel
                    Left = 512
                    Top = 56
                    Width = 53
                    Height = 13
                    Caption = 'Total NF-e:'
                    FocusControl = EdICMSTot_vNF
                  end
                  object Label243: TLabel
                    Left = 176
                    Top = 16
                    Width = 79
                    Height = 13
                    Caption = 'Vlr. ICMS deson:'
                    FocusControl = EdICMSTot_vICMSDeson
                  end
                  object Label252: TLabel
                    Left = 596
                    Top = 56
                    Width = 64
                    Height = 13
                    Caption = 'Vlr. Tot. Trib.:'
                    FocusControl = EdIvTotTrib
                  end
                  object Label405: TLabel
                    Left = 8
                    Top = 97
                    Width = 50
                    Height = 13
                    Caption = 'Total FCP:'
                    FocusControl = EdICMSTot_vFCP
                  end
                  object Label407: TLabel
                    Left = 92
                    Top = 97
                    Width = 80
                    Height = 13
                    Caption = 'Tot. FCP ret. ST:'
                    FocusControl = EdICMSTot_vFCPST
                  end
                  object Label410: TLabel
                    Left = 176
                    Top = 98
                    Width = 80
                    Height = 13
                    Caption = 'T. FCP r. an. ST:'
                    FocusControl = EdICMSTot_vFCPSTRet
                  end
                  object Label411: TLabel
                    Left = 260
                    Top = 98
                    Width = 62
                    Height = 13
                    Caption = 'Tot. IPI dev.:'
                    FocusControl = EdICMSTot_vIPIDevol
                  end
                  object EdICMSTot_vBC: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vBC'
                    UpdCampo = 'ICMSTot_vBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdICMSTot_vBCChange
                  end
                  object EdICMSTot_vICMS: TdmkEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vICMS'
                    UpdCampo = 'ICMSTot_vICMS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vBCST: TdmkEdit
                    Left = 260
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vBCST'
                    UpdCampo = 'ICMSTot_vBCST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vST: TdmkEdit
                    Left = 344
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vST'
                    UpdCampo = 'ICMSTot_vST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vProd: TdmkEdit
                    Left = 428
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vProd'
                    UpdCampo = 'ICMSTot_vProd'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vFrete: TdmkEdit
                    Left = 512
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 6
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vFrete'
                    UpdCampo = 'ICMSTot_vFrete'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vSeg: TdmkEdit
                    Left = 596
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 7
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vSeg'
                    UpdCampo = 'ICMSTot_vSeg'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vDesc: TdmkEdit
                    Left = 8
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 8
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vDesc'
                    UpdCampo = 'ICMSTot_vDesc'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vII: TdmkEdit
                    Left = 92
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 9
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vII'
                    UpdCampo = 'ICMSTot_vII'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vIPI: TdmkEdit
                    Left = 176
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 10
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vIPI'
                    UpdCampo = 'ICMSTot_vIPI'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vPIS: TdmkEdit
                    Left = 260
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 11
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vPIS'
                    UpdCampo = 'ICMSTot_vPIS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vCOFINS: TdmkEdit
                    Left = 344
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 12
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vCOFINS'
                    UpdCampo = 'ICMSTot_vCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vOutro: TdmkEdit
                    Left = 428
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 13
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vOutro'
                    UpdCampo = 'ICMSTot_vOutro'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vNF: TdmkEdit
                    Left = 512
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 14
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vNF'
                    UpdCampo = 'ICMSTot_vNF'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vICMSDeson: TdmkEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vICMSDeson'
                    UpdCampo = 'ICMSTot_vICMSDeson'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdIvTotTrib: TdmkEdit
                    Left = 596
                    Top = 72
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 15
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'vTotTrib'
                    UpdCampo = 'vTotTrib'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vFCP: TdmkEdit
                    Left = 8
                    Top = 112
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 16
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vFCP'
                    UpdCampo = 'ICMSTot_vFCP'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vFCPST: TdmkEdit
                    Left = 92
                    Top = 112
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 17
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vFCPST'
                    UpdCampo = 'ICMSTot_vFCPST'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vFCPSTRet: TdmkEdit
                    Left = 176
                    Top = 112
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 18
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vFCPSTRet'
                    UpdCampo = 'ICMSTot_vFCPSTRet'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdICMSTot_vIPIDevol: TdmkEdit
                    Left = 260
                    Top = 112
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 19
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ICMSTot_vIPIDevol'
                    UpdCampo = 'ICMSTot_vIPIDevol'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object GroupBox8: TGroupBox
                  Left = 0
                  Top = 138
                  Width = 684
                  Height = 61
                  Align = alClient
                  Caption = ' Reten'#231#227'o de Tributos (em valores - $): '
                  TabOrder = 1
                  object Label156: TLabel
                    Left = 8
                    Top = 15
                    Width = 20
                    Height = 13
                    Caption = 'PIS:'
                    FocusControl = dmkEdit89
                  end
                  object Label157: TLabel
                    Left = 92
                    Top = 15
                    Width = 42
                    Height = 13
                    Caption = 'COFINS:'
                    FocusControl = dmkEdit90
                  end
                  object Label158: TLabel
                    Left = 176
                    Top = 15
                    Width = 29
                    Height = 13
                    Caption = 'CSLL:'
                    FocusControl = dmkEdit91
                  end
                  object Label159: TLabel
                    Left = 260
                    Top = 15
                    Width = 60
                    Height = 13
                    Caption = 'BC do IRRF:'
                    FocusControl = dmkEdit92
                  end
                  object Label160: TLabel
                    Left = 344
                    Top = 15
                    Width = 28
                    Height = 13
                    Caption = 'IRRF:'
                    FocusControl = dmkEdit93
                  end
                  object Label161: TLabel
                    Left = 428
                    Top = 15
                    Width = 75
                    Height = 13
                    Caption = 'BC Prev. social:'
                    FocusControl = dmkEdit94
                  end
                  object Label162: TLabel
                    Left = 512
                    Top = 15
                    Width = 58
                    Height = 13
                    Caption = 'Prev. social:'
                    FocusControl = dmkEdit95
                  end
                  object dmkEdit89: TdmkEdit
                    Left = 8
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vRetPIS'
                    UpdCampo = 'RetTrib_vRetPIS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit90: TdmkEdit
                    Left = 92
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vRetCOFINS'
                    UpdCampo = 'RetTrib_vRetCOFINS'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit91: TdmkEdit
                    Left = 176
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vRetCSLL'
                    UpdCampo = 'RetTrib_vRetCSLL'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit92: TdmkEdit
                    Left = 260
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vBCIRRF'
                    UpdCampo = 'RetTrib_vBCIRRF'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit93: TdmkEdit
                    Left = 344
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vIRRF'
                    UpdCampo = 'RetTrib_vIRRF'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit94: TdmkEdit
                    Left = 428
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vBCRetPrev'
                    UpdCampo = 'RetTrib_vBCRetPrev'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdit95: TdmkEdit
                    Left = 512
                    Top = 30
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 6
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'RetTrib_vRetPrev'
                    UpdCampo = 'RetTrib_vRetPrev'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
            end
          end
          object TabSheet36: TTabSheet
            Caption = ' Dados da cobran'#231'a '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel69: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel70: TPanel
                Left = 0
                Top = 0
                Width = 1000
                Height = 121
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label398: TLabel
                  Left = 4
                  Top = 4
                  Width = 85
                  Height = 13
                  Caption = 'N'#250'mero da fatura:'
                end
                object Label399: TLabel
                  Left = 144
                  Top = 4
                  Width = 61
                  Height = 13
                  Caption = 'Valor orignal:'
                end
                object Label400: TLabel
                  Left = 284
                  Top = 4
                  Width = 49
                  Height = 13
                  Caption = 'Desconto:'
                end
                object Label401: TLabel
                  Left = 424
                  Top = 4
                  Width = 62
                  Height = 13
                  Caption = 'Valor l'#237'quido:'
                end
                object EdCobr_Fat_nFat: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 133
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Cobr_Fat_nFat'
                  UpdCampo = 'Cobr_Fat_nFat'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCobr_Fat_vOrig: TdmkEdit
                  Left = 144
                  Top = 20
                  Width = 133
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Cobr_Fat_vOrig'
                  UpdCampo = 'Cobr_Fat_vOrig'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCobr_Fat_vLiq: TdmkEdit
                  Left = 424
                  Top = 20
                  Width = 133
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Cobr_Fat_vLiq'
                  UpdCampo = 'Cobr_Fat_vLiq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCobr_Fat_vDesc: TdmkEdit
                  Left = 284
                  Top = 20
                  Width = 133
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Cobr_Fat_vDesc'
                  UpdCampo = 'Cobr_Fat_vDesc'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet27: TTabSheet
            Caption = ' SINTEGRA '
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel54: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 369
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1002
              ExplicitHeight = 372
              object GroupBox38: TGroupBox
                Left = 4
                Top = 0
                Width = 349
                Height = 61
                Caption = 
                  ' Declara'#231#227'o de Exporta'#231#227'o / Declara'#231#227'o Simplificada de exporta'#231#227 +
                  'o: '
                TabOrder = 0
                object Label349: TLabel
                  Left = 12
                  Top = 16
                  Width = 86
                  Height = 13
                  Caption = 'N'#186' da declara'#231#227'o:'
                  FocusControl = EdSINTEGRA_ExpDeclNum
                end
                object Label350: TLabel
                  Left = 120
                  Top = 16
                  Width = 97
                  Height = 13
                  Caption = 'Data da declara'#231#227'o:'
                end
                object EdSINTEGRA_ExpDeclNum: TdmkEdit
                  Left = 11
                  Top = 32
                  Width = 104
                  Height = 21
                  MaxLength = 11
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpDeclNum'
                  UpdCampo = 'SINTEGRA_ExpDeclNum'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPSINTEGRA_ExpDeclDta: TdmkEditDateTimePicker
                  Left = 120
                  Top = 32
                  Width = 112
                  Height = 21
                  Date = 40048.941292650460000000
                  Time = 40048.941292650460000000
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'SINTEGRA_ExpDeclDta'
                  UpdCampo = 'SINTEGRA_ExpDeclDta'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object GroupBox39: TGroupBox
                Left = 4
                Top = 64
                Width = 349
                Height = 61
                Caption = ' Registro de exporta'#231#227'o: '
                TabOrder = 2
                object Label351: TLabel
                  Left = 12
                  Top = 16
                  Width = 67
                  Height = 13
                  Caption = 'N'#186' do registro:'
                  FocusControl = EdSINTEGRA_ExpRegNum
                end
                object Label352: TLabel
                  Left = 120
                  Top = 16
                  Width = 78
                  Height = 13
                  Caption = 'Data do registro:'
                end
                object EdSINTEGRA_ExpRegNum: TdmkEdit
                  Left = 11
                  Top = 32
                  Width = 104
                  Height = 21
                  MaxLength = 12
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpRegNum'
                  UpdCampo = 'SINTEGRA_ExpRegNum'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPSINTEGRA_ExpRegDta: TdmkEditDateTimePicker
                  Left = 120
                  Top = 32
                  Width = 112
                  Height = 21
                  Date = 40048.941292650460000000
                  Time = 40048.941292650460000000
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'SINTEGRA_ExpRegDta'
                  UpdCampo = 'SINTEGRA_ExpRegDta'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
              object GroupBox40: TGroupBox
                Left = 356
                Top = 64
                Width = 633
                Height = 61
                Caption = ' Conhecimento de embarque: '
                TabOrder = 3
                object Label353: TLabel
                  Left = 12
                  Top = 16
                  Width = 100
                  Height = 13
                  Caption = 'N'#186' do conhecimento:'
                  FocusControl = EdSINTEGRA_ExpConhNum
                end
                object Label354: TLabel
                  Left = 120
                  Top = 16
                  Width = 111
                  Height = 13
                  Caption = 'Data do conhecimento:'
                end
                object Label356: TLabel
                  Left = 236
                  Top = 16
                  Width = 130
                  Height = 13
                  Caption = 'Tipo do conhecimento [F4]:'
                end
                object EdSINTEGRA_ExpConhNum: TdmkEdit
                  Left = 11
                  Top = 32
                  Width = 104
                  Height = 21
                  MaxLength = 16
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpConhNum'
                  UpdCampo = 'SINTEGRA_ExpConhNum'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPSINTEGRA_ExpConhDta: TdmkEditDateTimePicker
                  Left = 120
                  Top = 32
                  Width = 112
                  Height = 21
                  Date = 40048.941292650460000000
                  Time = 40048.941292650460000000
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'SINTEGRA_ExpConhDta'
                  UpdCampo = 'SINTEGRA_ExpConhDta'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdSINTEGRA_ExpConhTip_TXT: TdmkEdit
                  Left = 293
                  Top = 32
                  Width = 332
                  Height = 21
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdSINTEGRA_ExpConhTip: TdmkEdit
                  Left = 236
                  Top = 32
                  Width = 56
                  Height = 21
                  MaxLength = 2
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpConhTip'
                  UpdCampo = 'SINTEGRA_ExpConhTip'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdSINTEGRA_ExpConhTipChange
                end
              end
              object GroupBox41: TGroupBox
                Left = 356
                Top = 0
                Width = 633
                Height = 61
                Caption = ' Natureza da Exporta'#231#227'o: '
                TabOrder = 1
                object Label355: TLabel
                  Left = 12
                  Top = 16
                  Width = 139
                  Height = 13
                  Caption = 'Natureza da Exporta'#231#227'o [F4]:'
                end
                object EdSINTEGRA_ExpNat: TdmkEdit
                  Left = 12
                  Top = 32
                  Width = 56
                  Height = 21
                  MaxLength = 1
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpNat'
                  UpdCampo = 'SINTEGRA_ExpNat'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdSINTEGRA_ExpNatChange
                end
                object EdSINTEGRA_ExpNat_TXT: TdmkEdit
                  Left = 69
                  Top = 32
                  Width = 556
                  Height = 21
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox42: TGroupBox
                Left = 4
                Top = 128
                Width = 985
                Height = 61
                TabOrder = 4
                object Label357: TLabel
                  Left = 16
                  Top = 16
                  Width = 125
                  Height = 13
                  Caption = 'Pa'#237's (Tabela SISCOMEX):'
                end
                object Label358: TLabel
                  Left = 864
                  Top = 16
                  Width = 95
                  Height = 13
                  Caption = 'Data da averba'#231#227'o:'
                end
                object EdSINTEGRA_ExpPais: TdmkEdit
                  Left = 16
                  Top = 32
                  Width = 56
                  Height = 21
                  MaxLength = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SINTEGRA_ExpPais'
                  UpdCampo = 'SINTEGRA_ExpPais'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdSINTEGRA_ExpPaisChange
                end
                object EdSINTEGRA_ExpPais_TXT: TdmkEdit
                  Left = 73
                  Top = 32
                  Width = 788
                  Height = 21
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPSINTEGRA_ExpAverDta: TdmkEditDateTimePicker
                  Left = 864
                  Top = 32
                  Width = 112
                  Height = 21
                  Date = 40048.941292650460000000
                  Time = 40048.941292650460000000
                  TabOrder = 2
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'SINTEGRA_ExpAverDta'
                  UpdCampo = 'SINTEGRA_ExpAverDta'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
            end
          end
        end
      end
      object Panel21: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label186: TLabel
          Left = 8
          Top = 4
          Width = 29
          Height = 13
          Caption = 'FatID:'
          FocusControl = EdFatID
        end
        object Label187: TLabel
          Left = 76
          Top = 4
          Width = 46
          Height = 13
          Caption = 'Fat.Num.:'
          FocusControl = EdFatNum
        end
        object Label188: TLabel
          Left = 144
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = EdEmpresa
        end
        object Label189: TLabel
          Left = 192
          Top = 4
          Width = 26
          Height = 13
          Caption = 'IDCtrl'
          FocusControl = EdIDCtrl
        end
        object Label190: TLabel
          Left = 260
          Top = 4
          Width = 24
          Height = 13
          Caption = 'Lote:'
          FocusControl = EdLoteEnv
        end
        object Label191: TLabel
          Left = 400
          Top = 4
          Width = 36
          Height = 13
          Caption = 'Vers'#227'o:'
          FocusControl = Edversao
        end
        object Label192: TLabel
          Left = 512
          Top = 4
          Width = 264
          Height = 13
          Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
          FocusControl = EdNFe_Id
        end
        object Label319: TLabel
          Left = 788
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Status:'
          FocusControl = EdStatus
        end
        object Label194: TLabel
          Left = 444
          Top = 4
          Width = 58
          Height = 13
          Caption = 'C'#243'd. chave:'
          Enabled = False
          FocusControl = Edide_cNF
        end
        object EdFatID: TdmkEdit
          Left = 8
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatID'
          UpdCampo = 'FatID'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatNum: TdmkEdit
          Left = 76
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatNum'
          UpdCampo = 'FatNum'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 144
          Top = 20
          Width = 45
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdIDCtrl: TdmkEdit
          Left = 192
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'IDCtrl'
          UpdCampo = 'IDCtrl'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdLoteEnv: TdmkEdit
          Left = 260
          Top = 20
          Width = 134
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'LoteEnv'
          UpdCampo = 'LoteEnv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edversao: TdmkEdit
          Left = 400
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'versao'
          UpdCampo = 'versao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdversaoChange
        end
        object EdNFe_Id: TdmkEdit
          Left = 512
          Top = 20
          Width = 271
          Height = 21
          Enabled = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Id'
          UpdCampo = 'Id'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdStatus: TdmkEdit
          Left = 788
          Top = 20
          Width = 33
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Status'
          UpdCampo = 'Status'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edide_cNF: TdmkEdit
          Left = 444
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ide_cNF'
          UpdCampo = 'ide_cNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 475
      Width = 1008
      Height = 62
      Align = alBottom
      TabOrder = 1
      object Panel58: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 45
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 45
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 537
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 475
      Width = 1008
      Height = 62
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 45
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 186
        Height = 45
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 360
        Top = 15
        Width = 646
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 513
          Top = 0
          Width = 133
          Height = 45
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtNFeCabA: TBitBtn
          Tag = 441
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'NF-&e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtNFeCabAClick
        end
        object BtNFeCabB: TBitBtn
          Tag = 456
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'NFs &ref.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeCabBClick
        end
        object BtNFeItsI_U: TBitBtn
          Tag = 503
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Prod/Serv.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtNFeItsI_UClick
        end
        object BtNFeCabY: TBitBtn
          Tag = 187
          Left = 364
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cobran'#231'a'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtNFeCabYClick
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 445
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 44
        Width = 1008
        Height = 401
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 1008
          Height = 401
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet15: TTabSheet
            Caption = ' Geral '
            ImageIndex = 9
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel7: TPanel
              Left = 0
              Top = 85
              Width = 1000
              Height = 208
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label01: TLabel
                Left = 4
                Top = 4
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit1
              end
              object Label4: TLabel
                Left = 32
                Top = 4
                Width = 109
                Height = 13
                Caption = 'Natureza da opera'#231#227'o:'
                FocusControl = DBEdit3
              end
              object Label5: TLabel
                Left = 804
                Top = 4
                Width = 165
                Height = 13
                Caption = 'Indicador de forma de pagamento: '
                FocusControl = DBEdit4
              end
              object Label6: TLabel
                Left = 4
                Top = 44
                Width = 27
                Height = 13
                Caption = 'Mod.:'
                FocusControl = DBEdit5
              end
              object Label10: TLabel
                Left = 36
                Top = 44
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
                FocusControl = DBEdit6
              end
              object Label11: TLabel
                Left = 72
                Top = 44
                Width = 32
                Height = 13
                Caption = 'N'#186' NF:'
                FocusControl = DBEdit14
              end
              object Label18: TLabel
                Left = 156
                Top = 44
                Width = 82
                Height = 13
                Caption = 'Data da emiss'#227'o:'
                FocusControl = DBEdit15
              end
              object Label19: TLabel
                Left = 272
                Top = 44
                Width = 117
                Height = 13
                Caption = 'Data da sa'#237'sa / entrada:'
                FocusControl = DBEdit16
              end
              object Label20: TLabel
                Left = 408
                Top = 44
                Width = 90
                Height = 13
                Caption = 'Tipo do doc. fiscal:'
                FocusControl = DBEdit17
              end
              object Label21: TLabel
                Left = 504
                Top = 44
                Width = 178
                Height = 13
                Caption = 'C'#243'digo do munic'#237'pio do fato gerador: '
                FocusControl = DBEdit18
              end
              object Label22: TLabel
                Left = 879
                Top = 44
                Width = 89
                Height = 13
                Caption = 'Tipo de impress'#227'o:'
                FocusControl = DBEdit19
              end
              object Label23: TLabel
                Left = 4
                Top = 84
                Width = 138
                Height = 13
                Caption = 'Forma de impress'#227'o da NF-e:'
                FocusControl = DBEdit20
              end
              object Label24: TLabel
                Left = 4
                Top = 124
                Width = 18
                Height = 13
                Caption = 'DV:'
                FocusControl = DBEdit21
              end
              object Label25: TLabel
                Left = 28
                Top = 124
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
                FocusControl = DBEdit22
              end
              object Label26: TLabel
                Left = 160
                Top = 124
                Width = 148
                Height = 13
                Caption = 'Finalidade de emiss'#227'o da NF-e:'
                FocusControl = DBEdit23
              end
              object Label27: TLabel
                Left = 312
                Top = 124
                Width = 144
                Height = 13
                Caption = 'Processo de emiss'#227'o da NF-e:'
                FocusControl = DBEdit24
              end
              object Label28: TLabel
                Left = 830
                Top = 124
                Width = 153
                Height = 13
                Caption = 'Vers'#227'o do processo de emiss'#227'o:'
                FocusControl = DBEdit25
              end
              object Label376: TLabel
                Left = 4
                Top = 164
                Width = 222
                Height = 13
                Caption = 'Identifica'#231#227'o do local de destino da opera'#231#227'o: '
                FocusControl = Edide_idDest
              end
              object Label377: TLabel
                Left = 228
                Top = 164
                Width = 152
                Height = 13
                Caption = 'Opera'#231#227'o com consumidor final:'
                FocusControl = Edide_indFinal
              end
              object Label381: TLabel
                Left = 384
                Top = 164
                Width = 396
                Height = 13
                Caption = 
                  'Indicador de presen'#231'a do comprador no estabelecimento no momento' +
                  ' da opera'#231#227'o:'
                FocusControl = Edide_indPres
              end
              object DBEdit1: TDBEdit
                Left = 4
                Top = 20
                Width = 25
                Height = 21
                DataField = 'ide_cUF'
                DataSource = DsNFeCabA
                TabOrder = 0
              end
              object DBEdit3: TDBEdit
                Left = 32
                Top = 20
                Width = 769
                Height = 21
                DataField = 'ide_natOp'
                DataSource = DsNFeCabA
                TabOrder = 1
              end
              object DBEdit4: TDBEdit
                Left = 804
                Top = 20
                Width = 25
                Height = 21
                DataField = 'ide_indPag'
                DataSource = DsNFeCabA
                TabOrder = 2
              end
              object DBEdit5: TDBEdit
                Left = 4
                Top = 60
                Width = 29
                Height = 21
                DataField = 'ide_mod'
                DataSource = DsNFeCabA
                TabOrder = 3
              end
              object DBEdit6: TDBEdit
                Left = 36
                Top = 60
                Width = 33
                Height = 21
                DataField = 'ide_serie'
                DataSource = DsNFeCabA
                TabOrder = 4
              end
              object DBEdit14: TDBEdit
                Left = 72
                Top = 60
                Width = 80
                Height = 21
                DataField = 'ide_nNF'
                DataSource = DsNFeCabA
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 5
              end
              object DBEdit15: TDBEdit
                Left = 156
                Top = 60
                Width = 112
                Height = 21
                DataField = 'ide_dEmi'
                DataSource = DsNFeCabA
                TabOrder = 6
              end
              object DBEdit16: TDBEdit
                Left = 272
                Top = 60
                Width = 134
                Height = 21
                DataField = 'ide_dSaiEnt'
                DataSource = DsNFeCabA
                TabOrder = 7
              end
              object DBEdit17: TDBEdit
                Left = 408
                Top = 60
                Width = 21
                Height = 21
                DataField = 'ide_tpNF'
                DataSource = DsNFeCabA
                TabOrder = 8
              end
              object DBEdit18: TDBEdit
                Left = 504
                Top = 60
                Width = 50
                Height = 21
                DataField = 'ide_cMunFG'
                DataSource = DsNFeCabA
                TabOrder = 9
              end
              object DBEdit19: TDBEdit
                Left = 879
                Top = 60
                Width = 25
                Height = 21
                DataField = 'ide_tpImp'
                DataSource = DsNFeCabA
                TabOrder = 10
              end
              object DBEdit20: TDBEdit
                Left = 4
                Top = 100
                Width = 25
                Height = 21
                DataField = 'ide_tpEmis'
                DataSource = DsNFeCabA
                TabOrder = 11
              end
              object DBEdit21: TDBEdit
                Left = 4
                Top = 140
                Width = 21
                Height = 21
                DataField = 'ide_cDV'
                DataSource = DsNFeCabA
                TabOrder = 12
              end
              object DBEdit22: TDBEdit
                Left = 28
                Top = 140
                Width = 25
                Height = 21
                DataField = 'ide_tpAmb'
                DataSource = DsNFeCabA
                TabOrder = 13
              end
              object DBEdit23: TDBEdit
                Left = 160
                Top = 140
                Width = 21
                Height = 21
                DataField = 'ide_finNFe'
                DataSource = DsNFeCabA
                TabOrder = 14
              end
              object DBEdit24: TDBEdit
                Left = 312
                Top = 140
                Width = 25
                Height = 21
                DataField = 'ide_procEmi'
                DataSource = DsNFeCabA
                TabOrder = 15
              end
              object DBEdit25: TDBEdit
                Left = 830
                Top = 140
                Width = 157
                Height = 21
                DataField = 'ide_verProc'
                DataSource = DsNFeCabA
                TabOrder = 16
              end
              object DBEdit30: TDBEdit
                Left = 828
                Top = 20
                Width = 160
                Height = 21
                DataField = 'ide_indPag_TXT'
                DataSource = DsNFeCabA
                TabOrder = 17
              end
              object DBEdit31: TDBEdit
                Left = 428
                Top = 60
                Width = 74
                Height = 21
                DataField = 'ide_tpNF_TXT'
                DataSource = DsNFeCabA
                TabOrder = 18
              end
              object DBEdit32: TDBEdit
                Left = 556
                Top = 60
                Width = 321
                Height = 21
                DataField = 'ide_cMunFG_TXT'
                DataSource = DsNFeCabA
                TabOrder = 19
              end
              object DBEdit33: TDBEdit
                Left = 907
                Top = 60
                Width = 81
                Height = 21
                DataField = 'ide_tpImp_TXT'
                DataSource = DsNFeCabA
                TabOrder = 20
              end
              object DBEdit34: TDBEdit
                Left = 32
                Top = 100
                Width = 956
                Height = 21
                DataField = 'ide_tpEmis_TXT'
                DataSource = DsNFeCabA
                TabOrder = 21
              end
              object DBEdit35: TDBEdit
                Left = 56
                Top = 140
                Width = 101
                Height = 21
                DataField = 'ide_tpAmb_TXT'
                DataSource = DsNFeCabA
                TabOrder = 22
              end
              object DBEdit36: TDBEdit
                Left = 184
                Top = 140
                Width = 125
                Height = 21
                DataField = 'ide_finNFe_TXT'
                DataSource = DsNFeCabA
                TabOrder = 23
              end
              object DBEdit37: TDBEdit
                Left = 340
                Top = 140
                Width = 485
                Height = 21
                DataField = 'ide_procEmi_TXT'
                DataSource = DsNFeCabA
                TabOrder = 24
              end
              object EdDBide_indFinal: TDBEdit
                Left = 228
                Top = 180
                Width = 25
                Height = 21
                DataField = 'ide_indFinal'
                DataSource = DsNFeCabA
                TabOrder = 25
                OnChange = EdDBide_indFinalChange
              end
              object EdDBide_indPres: TDBEdit
                Left = 384
                Top = 180
                Width = 25
                Height = 21
                DataField = 'ide_indPres'
                DataSource = DsNFeCabA
                TabOrder = 26
                OnChange = EdDBide_indPresChange
              end
              object EdDBide_idDest: TDBEdit
                Left = 4
                Top = 180
                Width = 25
                Height = 21
                DataField = 'ide_idDest'
                DataSource = DsNFeCabA
                TabOrder = 27
                OnChange = EdDBide_idDestChange
              end
              object EdDBide_idDest_TXT: TdmkEdit
                Left = 32
                Top = 180
                Width = 193
                Height = 21
                ReadOnly = True
                TabOrder = 28
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDBide_indFinal_TXT: TdmkEdit
                Left = 256
                Top = 180
                Width = 125
                Height = 21
                ReadOnly = True
                TabOrder = 29
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDBide_indPres_TXT: TdmkEdit
                Left = 412
                Top = 180
                Width = 573
                Height = 21
                ReadOnly = True
                TabOrder = 30
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object Panel44: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 85
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label321: TLabel
                Left = 8
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Regra fiscal:'
              end
              object Label322: TLabel
                Left = 8
                Top = 44
                Width = 86
                Height = 13
                Caption = 'Tabela de pre'#231'os:'
              end
              object Label323: TLabel
                Left = 564
                Top = 4
                Width = 187
                Height = 13
                Caption = 'Carteira para emiss'#227'o dos lan'#231'amentos:'
              end
              object Label315: TLabel
                Left = 588
                Top = 44
                Width = 119
                Height = 13
                Caption = 'Condi'#231#227'o de pagamento:'
                FocusControl = DBEdit227
              end
              object DBEdit221: TDBEdit
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                DataField = 'FisRegCad'
                DataSource = DsNFeCabA
                TabOrder = 0
              end
              object DBEdit222: TDBEdit
                Left = 68
                Top = 20
                Width = 493
                Height = 21
                DataField = 'NO_FisRegCad'
                DataSource = DsNFeCabA
                TabOrder = 1
              end
              object DBEdit223: TDBEdit
                Left = 564
                Top = 20
                Width = 56
                Height = 21
                DataField = 'CartEmiss'
                DataSource = DsNFeCabA
                TabOrder = 2
              end
              object DBEdit224: TDBEdit
                Left = 624
                Top = 20
                Width = 353
                Height = 21
                DataField = 'NO_CARTEMISS'
                DataSource = DsNFeCabA
                TabOrder = 3
              end
              object DBEdit225: TDBEdit
                Left = 8
                Top = 60
                Width = 56
                Height = 21
                DataField = 'TabelaPrc'
                DataSource = DsNFeCabA
                TabOrder = 4
              end
              object DBEdit226: TDBEdit
                Left = 68
                Top = 60
                Width = 517
                Height = 21
                DataField = 'NO_TabelaPrc'
                DataSource = DsNFeCabA
                TabOrder = 5
              end
              object DBEdit227: TDBEdit
                Left = 588
                Top = 60
                Width = 56
                Height = 21
                DataField = 'CondicaoPg'
                DataSource = DsNFeCabA
                TabOrder = 6
              end
              object DBEdit228: TDBEdit
                Left = 908
                Top = 148
                Width = 654
                Height = 21
                DataField = 'NO_CondicaoPG'
                DataSource = DsNFeCabA
                TabOrder = 7
              end
              object DBEdit229: TDBEdit
                Left = 648
                Top = 60
                Width = 329
                Height = 21
                DataField = 'NO_CondicaoPG'
                DataSource = DsNFeCabA
                TabOrder = 8
              end
            end
            object Panel46: TPanel
              Left = 0
              Top = 293
              Width = 1000
              Height = 80
              Align = alClient
              ParentBackground = False
              TabOrder = 2
              object REWarning: TRichEdit
                Left = 1
                Top = 1
                Width = 998
                Height = 78
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = 346859
                Font.Height = -12
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
                Zoom = 100
              end
            end
          end
          object TabSheet1: TTabSheet
            Caption = ' Destinat'#225'rio '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 376
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label45: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'CNPJ...:'
                FocusControl = DBEdit53
              end
              object Label46: TLabel
                Left = 236
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = DBEdit54
              end
              object Label48: TLabel
                Left = 4
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = DBEdit56
              end
              object Label49: TLabel
                Left = 428
                Top = 44
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = DBEdit57
              end
              object Label50: TLabel
                Left = 480
                Top = 44
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = DBEdit58
              end
              object Label51: TLabel
                Left = 4
                Top = 84
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = DBEdit59
              end
              object Label52: TLabel
                Left = 428
                Top = 84
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = DBEdit60
              end
              object Label53: TLabel
                Left = 4
                Top = 124
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit61
              end
              object Label54: TLabel
                Left = 36
                Top = 124
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = DBEdit62
              end
              object Label55: TLabel
                Left = 96
                Top = 124
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = DBEdit63
              end
              object Label47: TLabel
                Left = 120
                Top = 4
                Width = 50
                Height = 13
                Caption = '... ou CPF:'
                FocusControl = DBEdit55
              end
              object Label383: TLabel
                Left = 632
                Top = 124
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = Eddest_IE
              end
              object Label384: TLabel
                Left = 756
                Top = 124
                Width = 116
                Height = 13
                Caption = 'Inscri'#231#227'o na SUFRAMA:'
                FocusControl = Eddest_ISUF
              end
              object Label385: TLabel
                Left = 880
                Top = 124
                Width = 94
                Height = 13
                Caption = 'Inscri'#231#227'o Municipal:'
                FocusControl = Eddest_IM
              end
              object Label386: TLabel
                Left = 536
                Top = 124
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Eddest_fone
              end
              object DBEdit53: TDBEdit
                Left = 4
                Top = 20
                Width = 113
                Height = 24
                DataField = 'dest_CNPJ'
                DataSource = DsNFeCabA
                TabOrder = 0
              end
              object DBEdit54: TDBEdit
                Left = 236
                Top = 20
                Width = 745
                Height = 24
                DataField = 'dest_xNome'
                DataSource = DsNFeCabA
                TabOrder = 1
              end
              object DBEdit56: TDBEdit
                Left = 4
                Top = 60
                Width = 420
                Height = 24
                DataField = 'dest_xLgr'
                DataSource = DsNFeCabA
                TabOrder = 2
              end
              object DBEdit57: TDBEdit
                Left = 428
                Top = 60
                Width = 48
                Height = 24
                DataField = 'dest_nro'
                DataSource = DsNFeCabA
                TabOrder = 3
              end
              object DBEdit58: TDBEdit
                Left = 480
                Top = 60
                Width = 501
                Height = 24
                DataField = 'dest_xCpl'
                DataSource = DsNFeCabA
                TabOrder = 4
              end
              object DBEdit59: TDBEdit
                Left = 4
                Top = 100
                Width = 420
                Height = 24
                DataField = 'dest_xBairro'
                DataSource = DsNFeCabA
                TabOrder = 5
              end
              object DBEdit60: TDBEdit
                Left = 428
                Top = 100
                Width = 56
                Height = 24
                DataField = 'dest_cMun'
                DataSource = DsNFeCabA
                TabOrder = 6
              end
              object DBEdit61: TDBEdit
                Left = 4
                Top = 140
                Width = 30
                Height = 24
                DataField = 'dest_UF'
                DataSource = DsNFeCabA
                TabOrder = 7
              end
              object DBEdit62: TDBEdit
                Left = 36
                Top = 140
                Width = 56
                Height = 24
                DataField = 'dest_CEP'
                DataSource = DsNFeCabA
                TabOrder = 8
              end
              object DBEdit63: TDBEdit
                Left = 96
                Top = 140
                Width = 48
                Height = 24
                DataField = 'dest_cPais'
                DataSource = DsNFeCabA
                TabOrder = 9
              end
              object DBEdit64: TDBEdit
                Left = 537
                Top = 140
                Width = 92
                Height = 24
                DataField = 'dest_fone'
                DataSource = DsNFeCabA
                TabOrder = 10
              end
              object DBEdit65: TDBEdit
                Left = 632
                Top = 140
                Width = 121
                Height = 24
                DataField = 'dest_IE'
                DataSource = DsNFeCabA
                TabOrder = 11
              end
              object DBEdit66: TDBEdit
                Left = 756
                Top = 140
                Width = 121
                Height = 24
                DataField = 'dest_ISUF'
                DataSource = DsNFeCabA
                TabOrder = 12
              end
              object DBEdit70: TDBEdit
                Left = 484
                Top = 100
                Width = 497
                Height = 24
                DataField = 'dest_xMun'
                DataSource = DsNFeCabA
                TabOrder = 13
              end
              object DBEdit71: TDBEdit
                Left = 148
                Top = 140
                Width = 385
                Height = 24
                DataField = 'dest_xPais'
                DataSource = DsNFeCabA
                TabOrder = 14
              end
              object DBEdit55: TDBEdit
                Left = 120
                Top = 20
                Width = 113
                Height = 24
                DataField = 'dest_CPF'
                DataSource = DsNFeCabA
                TabOrder = 15
              end
              object DBEdit245: TDBEdit
                Left = 881
                Top = 140
                Width = 100
                Height = 24
                DataField = 'dest_IM'
                DataSource = DsNFeCabA
                TabOrder = 16
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' Empresa '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 376
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label29: TLabel
                Left = 4
                Top = 4
                Width = 42
                Height = 13
                Caption = 'CNPJ ...:'
                FocusControl = DBEdit26
              end
              object Label31: TLabel
                Left = 236
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = DBEdit28
              end
              object Label32: TLabel
                Left = 752
                Top = 4
                Width = 74
                Height = 13
                Caption = 'Nome Fantasia:'
                FocusControl = DBEdit29
              end
              object Label33: TLabel
                Left = 4
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = DBEdit38
              end
              object Label34: TLabel
                Left = 428
                Top = 44
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = DBEdit39
              end
              object Label35: TLabel
                Left = 480
                Top = 44
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = DBEdit40
              end
              object Label36: TLabel
                Left = 4
                Top = 84
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = DBEdit41
              end
              object Label30: TLabel
                Left = 428
                Top = 84
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = DBEdit27
              end
              object Label38: TLabel
                Left = 4
                Top = 124
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit43
              end
              object Label39: TLabel
                Left = 36
                Top = 124
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = DBEdit44
              end
              object Label40: TLabel
                Left = 96
                Top = 124
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = DBEdit45
              end
              object Label42: TLabel
                Left = 844
                Top = 124
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = DBEdit47
              end
              object Label37: TLabel
                Left = 4
                Top = 164
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = DBEdit48
              end
              object Label41: TLabel
                Left = 196
                Top = 164
                Width = 127
                Height = 13
                Caption = 'I.E. do Substituto tribut'#225'rio:'
                FocusControl = DBEdit49
              end
              object Label43: TLabel
                Left = 384
                Top = 164
                Width = 94
                Height = 13
                Caption = 'Inscri'#231#227'o Municipal:'
                FocusControl = DBEdit50
              end
              object Label44: TLabel
                Left = 588
                Top = 164
                Width = 62
                Height = 13
                Caption = 'CNAE Fiscal:'
                FocusControl = DBEdit51
              end
              object Label61: TLabel
                Left = 120
                Top = 4
                Width = 50
                Height = 13
                Caption = '... ou CPF:'
                FocusControl = DBEdit72
              end
              object DBEdit26: TDBEdit
                Left = 4
                Top = 20
                Width = 112
                Height = 24
                DataField = 'emit_CNPJ'
                DataSource = DsNFeCabA
                TabOrder = 0
              end
              object DBEdit28: TDBEdit
                Left = 236
                Top = 20
                Width = 513
                Height = 24
                DataField = 'emit_xNome'
                DataSource = DsNFeCabA
                TabOrder = 1
              end
              object DBEdit29: TDBEdit
                Left = 752
                Top = 20
                Width = 229
                Height = 24
                DataField = 'emit_xFant'
                DataSource = DsNFeCabA
                TabOrder = 2
              end
              object DBEdit38: TDBEdit
                Left = 4
                Top = 60
                Width = 420
                Height = 24
                DataField = 'emit_xLgr'
                DataSource = DsNFeCabA
                TabOrder = 3
              end
              object DBEdit39: TDBEdit
                Left = 428
                Top = 60
                Width = 48
                Height = 24
                DataField = 'emit_nro'
                DataSource = DsNFeCabA
                TabOrder = 4
              end
              object DBEdit40: TDBEdit
                Left = 480
                Top = 60
                Width = 501
                Height = 24
                DataField = 'emit_xCpl'
                DataSource = DsNFeCabA
                TabOrder = 5
              end
              object DBEdit41: TDBEdit
                Left = 4
                Top = 100
                Width = 420
                Height = 24
                DataField = 'emit_xBairro'
                DataSource = DsNFeCabA
                TabOrder = 6
              end
              object DBEdit27: TDBEdit
                Left = 428
                Top = 100
                Width = 56
                Height = 24
                DataField = 'emit_cMun'
                DataSource = DsNFeCabA
                TabOrder = 7
              end
              object DBEdit43: TDBEdit
                Left = 4
                Top = 140
                Width = 30
                Height = 24
                DataField = 'emit_UF'
                DataSource = DsNFeCabA
                TabOrder = 8
              end
              object DBEdit44: TDBEdit
                Left = 36
                Top = 140
                Width = 56
                Height = 24
                DataField = 'emit_CEP'
                DataSource = DsNFeCabA
                TabOrder = 9
              end
              object DBEdit45: TDBEdit
                Left = 96
                Top = 140
                Width = 48
                Height = 24
                DataField = 'emit_cPais'
                DataSource = DsNFeCabA
                TabOrder = 10
              end
              object DBEdit47: TDBEdit
                Left = 843
                Top = 140
                Width = 138
                Height = 24
                DataField = 'emit_fone'
                DataSource = DsNFeCabA
                TabOrder = 11
              end
              object DBEdit48: TDBEdit
                Left = 4
                Top = 180
                Width = 186
                Height = 24
                DataField = 'emit_IE'
                DataSource = DsNFeCabA
                TabOrder = 12
              end
              object DBEdit49: TDBEdit
                Left = 196
                Top = 180
                Width = 186
                Height = 24
                DataField = 'emit_IEST'
                DataSource = DsNFeCabA
                TabOrder = 13
              end
              object DBEdit50: TDBEdit
                Left = 384
                Top = 180
                Width = 199
                Height = 24
                DataField = 'emit_IM'
                DataSource = DsNFeCabA
                TabOrder = 14
              end
              object DBEdit51: TDBEdit
                Left = 588
                Top = 180
                Width = 95
                Height = 24
                DataField = 'emit_CNAE'
                DataSource = DsNFeCabA
                TabOrder = 15
              end
              object DBEdit52: TDBEdit
                Left = 684
                Top = 180
                Width = 297
                Height = 24
                DataField = 'CNAE_TXT'
                DataSource = DsNFeCabA
                TabOrder = 16
              end
              object DBEdit42: TDBEdit
                Left = 484
                Top = 100
                Width = 497
                Height = 24
                DataField = 'emit_xMun'
                DataSource = DsNFeCabA
                TabOrder = 17
              end
              object DBEdit46: TDBEdit
                Left = 144
                Top = 140
                Width = 697
                Height = 24
                DataField = 'emit_xPais'
                DataSource = DsNFeCabA
                TabOrder = 18
              end
              object DBEdit72: TDBEdit
                Left = 120
                Top = 20
                Width = 112
                Height = 24
                DataField = 'emit_CPF'
                DataSource = DsNFeCabA
                TabOrder = 19
              end
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Transporte '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 376
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel62: TPanel
                Left = 0
                Top = 0
                Width = 489
                Height = 376
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Label86: TLabel
                  Left = 8
                  Top = 4
                  Width = 97
                  Height = 13
                  Caption = 'Modalidade do frete:'
                  FocusControl = DBEdit96
                end
                object Label87: TLabel
                  Left = 252
                  Top = 4
                  Width = 157
                  Height = 13
                  Caption = 'CNPJ da transportadora, ou CPF:'
                  FocusControl = DBEdit98
                end
                object Label89: TLabel
                  Left = 8
                  Top = 44
                  Width = 112
                  Height = 13
                  Caption = 'Raz'#227'o Social ou Nome:'
                  FocusControl = DBEdit100
                end
                object Label88: TLabel
                  Left = 368
                  Top = 44
                  Width = 19
                  Height = 13
                  Caption = 'I.E.:'
                  FocusControl = DBEdit101
                end
                object Label90: TLabel
                  Left = 8
                  Top = 84
                  Width = 49
                  Height = 13
                  Caption = 'Endere'#231'o:'
                  FocusControl = DBEdit102
                end
                object Label91: TLabel
                  Left = 8
                  Top = 124
                  Width = 96
                  Height = 13
                  Caption = 'Nome do Munic'#237'pio:'
                  FocusControl = DBEdit103
                end
                object Label92: TLabel
                  Left = 448
                  Top = 124
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = DBEdit104
                end
                object Label93: TLabel
                  Left = 8
                  Top = 168
                  Width = 79
                  Height = 13
                  Caption = 'Valor do servi'#231'o:'
                  FocusControl = DBEdit105
                end
                object Label94: TLabel
                  Left = 8
                  Top = 192
                  Width = 121
                  Height = 13
                  Caption = 'BC da reten'#231#227'o do ICMS:'
                  FocusControl = DBEdit106
                end
                object Label97: TLabel
                  Left = 228
                  Top = 168
                  Width = 31
                  Height = 13
                  Caption = 'CFOP:'
                  FocusControl = DBEdit109
                end
                object Label99: TLabel
                  Left = 228
                  Top = 192
                  Width = 30
                  Height = 13
                  Caption = 'Placa:'
                  FocusControl = DBEdit111
                end
                object Label95: TLabel
                  Left = 8
                  Top = 216
                  Width = 108
                  Height = 13
                  Caption = 'Al'#237'quota da Reten'#231#227'o:'
                  FocusControl = DBEdit107
                end
                object Label100: TLabel
                  Left = 228
                  Top = 216
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = DBEdit112
                end
                object Label101: TLabel
                  Left = 228
                  Top = 240
                  Width = 33
                  Height = 13
                  Caption = 'RNTC:'
                  FocusControl = DBEdit113
                end
                object Label96: TLabel
                  Left = 8
                  Top = 240
                  Width = 100
                  Height = 13
                  Caption = 'Valor do ICMS retido:'
                  FocusControl = DBEdit108
                end
                object Label98: TLabel
                  Left = 8
                  Top = 264
                  Width = 352
                  Height = 13
                  Caption = 
                    'C'#243'digo do munic'#237'pio de ocorr'#234'ncia do fato gerador do ICMS do tra' +
                    'nsporte:'
                  FocusControl = DBEdit110
                end
                object Label396: TLabel
                  Left = 8
                  Top = 288
                  Width = 34
                  Height = 13
                  Caption = 'Vag'#227'o:'
                  FocusControl = EdVagao
                end
                object Label397: TLabel
                  Left = 248
                  Top = 288
                  Width = 29
                  Height = 13
                  Caption = 'Balsa:'
                  FocusControl = EdBalsa
                end
                object DBEdit96: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 29
                  Height = 24
                  DataField = 'ModFrete'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit97: TDBEdit
                  Left = 40
                  Top = 20
                  Width = 209
                  Height = 24
                  DataField = 'ModFrete_TXT'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit98: TDBEdit
                  Left = 252
                  Top = 20
                  Width = 113
                  Height = 24
                  DataField = 'Transporta_CNPJ'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
                object DBEdit99: TDBEdit
                  Left = 368
                  Top = 20
                  Width = 112
                  Height = 24
                  DataField = 'Transporta_CPF'
                  DataSource = DsNFeCabA
                  TabOrder = 3
                end
                object DBEdit100: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 357
                  Height = 24
                  DataField = 'Transporta_XNome'
                  DataSource = DsNFeCabA
                  TabOrder = 4
                end
                object DBEdit101: TDBEdit
                  Left = 368
                  Top = 60
                  Width = 113
                  Height = 24
                  DataField = 'Transporta_IE'
                  DataSource = DsNFeCabA
                  TabOrder = 5
                end
                object DBEdit102: TDBEdit
                  Left = 8
                  Top = 100
                  Width = 473
                  Height = 24
                  DataField = 'Transporta_XEnder'
                  DataSource = DsNFeCabA
                  TabOrder = 6
                end
                object DBEdit103: TDBEdit
                  Left = 8
                  Top = 140
                  Width = 437
                  Height = 24
                  DataField = 'Transporta_XMun'
                  DataSource = DsNFeCabA
                  TabOrder = 7
                end
                object DBEdit104: TDBEdit
                  Left = 448
                  Top = 140
                  Width = 30
                  Height = 24
                  DataField = 'Transporta_UF'
                  DataSource = DsNFeCabA
                  TabOrder = 8
                end
                object DBEdit105: TDBEdit
                  Left = 140
                  Top = 164
                  Width = 80
                  Height = 24
                  DataField = 'RetTransp_vServ'
                  DataSource = DsNFeCabA
                  TabOrder = 9
                end
                object DBEdit106: TDBEdit
                  Left = 140
                  Top = 188
                  Width = 80
                  Height = 24
                  DataField = 'RetTransp_vBCRet'
                  DataSource = DsNFeCabA
                  TabOrder = 10
                end
                object DBEdit109: TDBEdit
                  Left = 268
                  Top = 164
                  Width = 56
                  Height = 24
                  DataField = 'RetTransp_CFOP'
                  DataSource = DsNFeCabA
                  TabOrder = 11
                end
                object DBEdit111: TDBEdit
                  Left = 268
                  Top = 188
                  Width = 108
                  Height = 24
                  DataField = 'VeicTransp_Placa'
                  DataSource = DsNFeCabA
                  TabOrder = 12
                end
                object DBEdit107: TDBEdit
                  Left = 140
                  Top = 212
                  Width = 80
                  Height = 24
                  DataField = 'RetTransp_PICMSRet'
                  DataSource = DsNFeCabA
                  TabOrder = 13
                end
                object DBEdit112: TDBEdit
                  Left = 268
                  Top = 212
                  Width = 30
                  Height = 24
                  DataField = 'VeicTransp_UF'
                  DataSource = DsNFeCabA
                  TabOrder = 14
                end
                object DBEdit113: TDBEdit
                  Left = 268
                  Top = 236
                  Width = 213
                  Height = 24
                  DataField = 'VeicTransp_RNTC'
                  DataSource = DsNFeCabA
                  TabOrder = 15
                end
                object DBEdit108: TDBEdit
                  Left = 140
                  Top = 236
                  Width = 80
                  Height = 24
                  DataField = 'RetTransp_vICMSRet'
                  DataSource = DsNFeCabA
                  TabOrder = 16
                end
                object DBEdit110: TDBEdit
                  Left = 364
                  Top = 260
                  Width = 56
                  Height = 24
                  DataField = 'RetTransp_CMunFG'
                  DataSource = DsNFeCabA
                  TabOrder = 17
                end
                object DBEdit261: TDBEdit
                  Left = 280
                  Top = 284
                  Width = 200
                  Height = 24
                  DataField = 'Balsa'
                  DataSource = DsNFeCabA
                  TabOrder = 18
                end
                object DBEdit260: TDBEdit
                  Left = 44
                  Top = 284
                  Width = 200
                  Height = 24
                  DataField = 'Vagao'
                  DataSource = DsNFeCabA
                  TabOrder = 19
                end
              end
              object Panel65: TPanel
                Left = 489
                Top = 0
                Width = 513
                Height = 376
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object GroupBox49: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 513
                  Height = 116
                  Align = alTop
                  Caption = ' Reboques: '
                  TabOrder = 0
                  object Panel66: TPanel
                    Left = 2
                    Top = 15
                    Width = 97
                    Height = 100
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object BtNFeCabXReb: TBitBtn
                      Tag = 11
                      Left = 3
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Gerencia'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtNFeCabXRebClick
                    end
                  end
                  object DBGNFeCabXReb: TdmkDBGridZTO
                    Left = 99
                    Top = 15
                    Width = 412
                    Height = 100
                    Align = alClient
                    DataSource = DsNFeCabXReb
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'placa'
                        Title.Caption = 'Placa'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'UF'
                        Width = 26
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'RNTC'
                        Width = 132
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'ID'
                        Visible = True
                      end>
                  end
                end
                object GroupBox50: TGroupBox
                  Left = 0
                  Top = 116
                  Width = 513
                  Height = 116
                  Align = alTop
                  Caption = ' Volumes: '
                  TabOrder = 1
                  object Panel67: TPanel
                    Left = 2
                    Top = 15
                    Width = 97
                    Height = 100
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object BtNFeCabXVol: TBitBtn
                      Tag = 11
                      Left = 3
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Gerencia'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtNFeCabXVolClick
                    end
                  end
                  object DBGNFeCabXVol: TdmkDBGridZTO
                    Left = 99
                    Top = 15
                    Width = 412
                    Height = 100
                    Align = alClient
                    DataSource = DsNFeCabXVol
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'qVol'
                        Title.Caption = 'Qtde Vol.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'esp'
                        Title.Caption = 'Esp'#233'cie dos volumes'
                        Width = 80
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'marca'
                        Title.Caption = 'Marca dos volumes'
                        Width = 80
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'nVol'
                        Title.Caption = 'Numera'#231#227'o dos volumes'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'pesoL'
                        Title.Caption = 'Peso Liq.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'pesoB'
                        Title.Caption = 'Peso Bruto'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'ID'
                        Width = 56
                        Visible = True
                      end>
                  end
                end
                object GroupBox51: TGroupBox
                  Left = 0
                  Top = 232
                  Width = 513
                  Height = 144
                  Align = alClient
                  Caption = ' Lacres: '
                  TabOrder = 2
                  object Panel68: TPanel
                    Left = 2
                    Top = 15
                    Width = 97
                    Height = 127
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object BtNFeCabXLac: TBitBtn
                      Tag = 11
                      Left = 3
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Gerencia'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtNFeCabXLacClick
                    end
                  end
                  object DBGNFeCabXLac: TdmkDBGridZTO
                    Left = 99
                    Top = 15
                    Width = 412
                    Height = 127
                    Align = alClient
                    DataSource = DsNFeCabXLac
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'nLacre'
                        Title.Caption = 'N'#250'mero do Lacre'
                        Width = 300
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Conta'
                        Title.Caption = 'ID'
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Produtos e servi'#231'os '
            ImageIndex = 8
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGItsI: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 88
              Align = alClient
              DataSource = DsNFeItsI
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'nItem'
                  Title.Caption = 'Item'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_cProd'
                  Title.Caption = 'C'#243'digo'
                  Width = 74
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_xProd'
                  Title.Caption = 'Descri'#231#227'o do produto ou servi'#231'o'
                  Width = 269
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_CEST'
                  Title.Caption = 'CEST'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_CFOP'
                  Title.Caption = 'CFOP'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_NCM'
                  Title.Caption = 'NCM'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_uCom'
                  Title.Caption = 'Unidade'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_qCom'
                  Title.Caption = 'Quantidade'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vUnCom'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vProd'
                  Title.Caption = 'Valor'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vFrete'
                  Title.Caption = 'Frete'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vSeg'
                  Title.Caption = 'Seguro'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vDesc'
                  Title.Caption = 'Desconto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EhServico_TXT'
                  Title.Caption = 'P/S'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_xPed'
                  Title.Caption = 'Num pedido'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_nItemPed'
                  Title.Caption = 'It.ped.'
                  Visible = True
                end>
            end
            object PageControl3: TPageControl
              Left = 0
              Top = 88
              Width = 1000
              Height = 285
              ActivePage = TabSheet16
              Align = alBottom
              TabOrder = 1
              ExplicitTop = 85
              object TabSheet16: TTabSheet
                Caption = ' Dados do produto / servi'#231'o'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel25: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label231: TLabel
                    Left = 8
                    Top = 4
                    Width = 23
                    Height = 13
                    Caption = 'Item:'
                    FocusControl = DBEdit142
                  end
                  object Label232: TLabel
                    Left = 44
                    Top = 4
                    Width = 36
                    Height = 13
                    Caption = 'C'#243'digo:'
                    FocusControl = DBEdit143
                  end
                  object Label234: TLabel
                    Left = 112
                    Top = 4
                    Width = 157
                    Height = 13
                    Caption = 'Descri'#231#227'o do produto ou servi'#231'o:'
                    FocusControl = DBEdit145
                  end
                  object Label235: TLabel
                    Left = 796
                    Top = 4
                    Width = 27
                    Height = 13
                    Caption = 'NCM:'
                    FocusControl = DBEdit146
                  end
                  object Label236: TLabel
                    Left = 856
                    Top = 4
                    Width = 37
                    Height = 13
                    Caption = 'EXTIPI:'
                    FocusControl = DBEdit147
                  end
                  object Label237: TLabel
                    Left = 904
                    Top = 4
                    Width = 38
                    Height = 13
                    Caption = 'Genero:'
                    FocusControl = DBEdit148
                  end
                  object Label238: TLabel
                    Left = 948
                    Top = 4
                    Width = 31
                    Height = 13
                    Caption = 'CFOP:'
                    FocusControl = DBEdit149
                  end
                  object Label247: TLabel
                    Left = 8
                    Top = 108
                    Width = 27
                    Height = 13
                    Caption = 'Frete:'
                    FocusControl = DBEdit158
                  end
                  object Label248: TLabel
                    Left = 116
                    Top = 108
                    Width = 37
                    Height = 13
                    Caption = 'Seguro:'
                    FocusControl = DBEdit159
                  end
                  object Label249: TLabel
                    Left = 224
                    Top = 108
                    Width = 49
                    Height = 13
                    Caption = 'Desconto:'
                    FocusControl = DBEdit160
                  end
                  object DBEdit142: TDBEdit
                    Left = 8
                    Top = 20
                    Width = 32
                    Height = 24
                    DataField = 'nItem'
                    DataSource = DsNFeItsI
                    TabOrder = 0
                  end
                  object DBEdit143: TDBEdit
                    Left = 44
                    Top = 20
                    Width = 64
                    Height = 24
                    DataField = 'prod_cProd'
                    DataSource = DsNFeItsI
                    TabOrder = 1
                  end
                  object DBEdit145: TDBEdit
                    Left = 112
                    Top = 20
                    Width = 681
                    Height = 24
                    DataField = 'prod_xProd'
                    DataSource = DsNFeItsI
                    TabOrder = 2
                  end
                  object DBEdit146: TDBEdit
                    Left = 796
                    Top = 20
                    Width = 56
                    Height = 24
                    DataField = 'prod_NCM'
                    DataSource = DsNFeItsI
                    TabOrder = 3
                  end
                  object DBEdit147: TDBEdit
                    Left = 856
                    Top = 20
                    Width = 43
                    Height = 24
                    DataField = 'prod_EXTIPI'
                    DataSource = DsNFeItsI
                    TabOrder = 4
                  end
                  object DBEdit148: TDBEdit
                    Left = 904
                    Top = 20
                    Width = 41
                    Height = 24
                    DataField = 'prod_genero'
                    DataSource = DsNFeItsI
                    TabOrder = 5
                  end
                  object DBEdit149: TDBEdit
                    Left = 948
                    Top = 20
                    Width = 32
                    Height = 24
                    DataField = 'prod_CFOP'
                    DataSource = DsNFeItsI
                    TabOrder = 6
                  end
                  object DBEdit158: TDBEdit
                    Left = 8
                    Top = 124
                    Width = 104
                    Height = 24
                    DataField = 'prod_vFrete'
                    DataSource = DsNFeItsI
                    TabOrder = 7
                  end
                  object DBEdit159: TDBEdit
                    Left = 116
                    Top = 124
                    Width = 104
                    Height = 24
                    DataField = 'prod_vSeg'
                    DataSource = DsNFeItsI
                    TabOrder = 8
                  end
                  object DBEdit160: TDBEdit
                    Left = 224
                    Top = 124
                    Width = 104
                    Height = 24
                    DataField = 'prod_vDesc'
                    DataSource = DsNFeItsI
                    TabOrder = 9
                  end
                  object GroupBox11: TGroupBox
                    Left = 8
                    Top = 44
                    Width = 545
                    Height = 61
                    Caption = ' Dados comerciais: '
                    TabOrder = 10
                    object Label242: TLabel
                      Left = 420
                      Top = 16
                      Width = 54
                      Height = 13
                      Caption = 'Valor bruto:'
                      FocusControl = DBEdit153
                    end
                    object Label241: TLabel
                      Left = 308
                      Top = 16
                      Width = 31
                      Height = 13
                      Caption = 'Pre'#231'o:'
                      FocusControl = DBEdit152
                    end
                    object Label240: TLabel
                      Left = 220
                      Top = 16
                      Width = 58
                      Height = 13
                      Caption = 'Quantidade:'
                      FocusControl = DBEdit151
                    end
                    object Label239: TLabel
                      Left = 164
                      Top = 16
                      Width = 43
                      Height = 13
                      Caption = 'Unidade:'
                      FocusControl = DBEdit150
                    end
                    object Label233: TLabel
                      Left = 12
                      Top = 16
                      Width = 145
                      Height = 13
                      Caption = 'GTIN / EAN / UPC / DUN:  ['#185']'
                      FocusControl = DBEdit144
                    end
                    object DBEdit151: TDBEdit
                      Left = 220
                      Top = 32
                      Width = 86
                      Height = 24
                      DataField = 'prod_qCom'
                      DataSource = DsNFeItsI
                      TabOrder = 0
                    end
                    object DBEdit153: TDBEdit
                      Left = 420
                      Top = 32
                      Width = 108
                      Height = 24
                      DataField = 'prod_vProd'
                      DataSource = DsNFeItsI
                      TabOrder = 1
                    end
                    object DBEdit152: TDBEdit
                      Left = 308
                      Top = 32
                      Width = 108
                      Height = 24
                      DataField = 'prod_vUnCom'
                      DataSource = DsNFeItsI
                      TabOrder = 2
                    end
                    object DBEdit150: TDBEdit
                      Left = 164
                      Top = 32
                      Width = 54
                      Height = 24
                      DataField = 'prod_uCom'
                      DataSource = DsNFeItsI
                      TabOrder = 3
                    end
                    object DBEdit144: TDBEdit
                      Left = 12
                      Top = 32
                      Width = 149
                      Height = 24
                      DataField = 'prod_cEAN'
                      DataSource = DsNFeItsI
                      TabOrder = 4
                    end
                  end
                  object GroupBox12: TGroupBox
                    Left = 556
                    Top = 44
                    Width = 433
                    Height = 61
                    Caption = ' Dados tribut'#225'rios: '
                    TabOrder = 11
                    object Label253: TLabel
                      Left = 308
                      Top = 16
                      Width = 31
                      Height = 13
                      Caption = 'Pre'#231'o:'
                      FocusControl = DBEdit165
                    end
                    object Label254: TLabel
                      Left = 220
                      Top = 16
                      Width = 58
                      Height = 13
                      Caption = 'Quantidade:'
                      FocusControl = DBEdit163
                    end
                    object Label255: TLabel
                      Left = 164
                      Top = 16
                      Width = 43
                      Height = 13
                      Caption = 'Unidade:'
                      FocusControl = DBEdit166
                    end
                    object Label256: TLabel
                      Left = 12
                      Top = 16
                      Width = 145
                      Height = 13
                      Caption = 'GTIN / EAN / UPC / DUN:  ['#185']'
                      FocusControl = DBEdit167
                    end
                    object DBEdit163: TDBEdit
                      Left = 220
                      Top = 32
                      Width = 86
                      Height = 24
                      DataField = 'prod_qTrib'
                      DataSource = DsNFeItsI
                      TabOrder = 0
                    end
                    object DBEdit165: TDBEdit
                      Left = 308
                      Top = 32
                      Width = 108
                      Height = 24
                      DataField = 'prod_vUnTrib'
                      DataSource = DsNFeItsI
                      TabOrder = 1
                    end
                    object DBEdit166: TDBEdit
                      Left = 164
                      Top = 32
                      Width = 54
                      Height = 24
                      DataField = 'prod_uTrib'
                      DataSource = DsNFeItsI
                      TabOrder = 2
                    end
                    object DBEdit167: TDBEdit
                      Left = 12
                      Top = 32
                      Width = 149
                      Height = 24
                      DataField = 'prod_cEANTrib'
                      DataSource = DsNFeItsI
                      TabOrder = 3
                    end
                  end
                  object DBCheckBox1: TDBCheckBox
                    Left = 660
                    Top = 128
                    Width = 61
                    Height = 17
                    Caption = 'Tem IPI.'
                    DataField = 'Tem_IPI'
                    DataSource = DsNFeItsI
                    TabOrder = 12
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                  object DBCheckBox2: TDBCheckBox
                    Left = 732
                    Top = 128
                    Width = 258
                    Height = 17
                    Caption = 'Tem informa'#231#245'es adicionais de customiza'#231#227'o.'
                    DataField = 'InfAdCuztm'
                    DataSource = DsNFeItsI
                    TabOrder = 13
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                end
              end
              object TabSheet17: TTabSheet
                Caption = ' Declara'#231#227'o de importa'#231#227'o '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel26: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel27: TPanel
                    Left = 548
                    Top = 0
                    Width = 508
                    Height = 260
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Panel28: TPanel
                      Left = 0
                      Top = 0
                      Width = 509
                      Height = 48
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object BtIncluiItsIDIA: TBitBtn
                        Tag = 10
                        Left = 4
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Inclui'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtIncluiItsIDIAClick
                      end
                      object BtAlteraItsIDIA: TBitBtn
                        Tag = 11
                        Left = 96
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Altera'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 1
                        OnClick = BtAlteraItsIDIAClick
                      end
                      object BtExcluiItsIDIA: TBitBtn
                        Tag = 12
                        Left = 188
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Exclui'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 2
                        OnClick = BtExcluiItsIDIAClick
                      end
                    end
                    object DBGNFeItsIDIa: TDBGrid
                      Left = 0
                      Top = 48
                      Width = 509
                      Height = 212
                      Align = alClient
                      DataSource = DsNfeItsIDIa
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Adi_nAdicao'
                          Title.Caption = 'N'#186' adi'#231#227'o'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_nSeqAdic'
                          Title.Caption = 'Seq. adi'#231#227'o'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_cFabricante'
                          Title.Caption = 'C'#243'd. Fabricante'
                          Width = 84
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_vDescDI'
                          Title.Caption = 'Desconto item DI'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_nDraw'
                          Title.Caption = 'N'#176' do ato concess'#243'rio de Drawback:'
                          Visible = True
                        end>
                    end
                  end
                  object Panel29: TPanel
                    Left = 0
                    Top = 0
                    Width = 548
                    Height = 260
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object Panel30: TPanel
                      Left = 0
                      Top = 0
                      Width = 548
                      Height = 48
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object BtIncluiItsIDI: TBitBtn
                        Tag = 10
                        Left = 4
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Inclui'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtIncluiItsIDIClick
                      end
                      object BtAlteraItsIDI: TBitBtn
                        Tag = 11
                        Left = 96
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Altera'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 1
                        OnClick = BtAlteraItsIDIClick
                      end
                      object BtExcluiItsIDI: TBitBtn
                        Tag = 12
                        Left = 188
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Exclui'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 2
                        OnClick = BtExcluiItsIDIClick
                      end
                    end
                    object DBGNFeItsIDI: TDBGrid
                      Left = 0
                      Top = 48
                      Width = 548
                      Height = 212
                      Align = alClient
                      DataSource = DsNfeItsIDI
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DI_nDI'
                          Title.Caption = 'N'#186' DI/DSI/DA'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_dDI'
                          Title.Caption = 'Data DI/DSI/DA'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_xLocDesemb'
                          Title.Caption = 'Local de desembara'#231'o'
                          Width = 219
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_UFDesemb'
                          Title.Caption = 'UF desembara'#231'o'
                          Width = 21
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_dDesemb'
                          Title.Caption = 'Data desembara'#231'o'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_cExportador'
                          Title.Caption = 'C'#243'd. exportador'
                          Width = 84
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_tpViaTransp'
                          Title.Caption = 'VT'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_vAFRMM'
                          Title.Caption = 'AFRMM'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_tpIntermedio'
                          Title.Caption = 'TI'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_CNPJ'
                          Title.Caption = 'CNPJ'
                          Width = 112
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_UFTerceiro'
                          Title.Caption = 'UF'
                          Width = 32
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object TabSheet18: TTabSheet
                Caption = ' ICMS '
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel31: TPanel
                  Left = 0
                  Top = 0
                  Width = 992
                  Height = 101
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  ExplicitWidth = 995
                  object GroupBox13: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 101
                    Align = alClient
                    Caption = 
                      ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
                      ': '
                    TabOrder = 0
                    object Panel38: TPanel
                      Left = 2
                      Top = 15
                      Width = 992
                      Height = 86
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label244: TLabel
                        Left = 8
                        Top = 4
                        Width = 156
                        Height = 13
                        Caption = 'N11 - Origem da mercadoria: [F3]'
                      end
                      object Label245: TLabel
                        Left = 8
                        Top = 44
                        Width = 202
                        Height = 13
                        Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                      end
                      object EdCST_A: TDBEdit
                        Left = 8
                        Top = 20
                        Width = 45
                        Height = 24
                        DataField = 'ICMS_Orig'
                        DataSource = DsNFeItsN
                        TabOrder = 0
                        OnChange = EdCST_AChange
                      end
                      object EdTextoA: TdmkEdit
                        Left = 56
                        Top = 20
                        Width = 681
                        Height = 21
                        ReadOnly = True
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdCST_B: TDBEdit
                        Left = 8
                        Top = 61
                        Width = 45
                        Height = 24
                        DataField = 'ICMS_CST'
                        DataSource = DsNFeItsN
                        TabOrder = 2
                        OnChange = EdCST_BChange
                      end
                      object EdTextoB: TdmkEdit
                        Left = 56
                        Top = 61
                        Width = 681
                        Height = 21
                        ReadOnly = True
                        TabOrder = 3
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                end
                object PageControl4: TPageControl
                  Left = 0
                  Top = 101
                  Width = 992
                  Height = 156
                  ActivePage = TabSheet33
                  Align = alClient
                  TabOrder = 1
                  ExplicitHeight = 153
                  object TabSheet33: TTabSheet
                    Caption = 'ICMS Normal'
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel61: TPanel
                      Left = 0
                      Top = 0
                      Width = 989
                      Height = 134
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object GroupBox14: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 989
                        Height = 134
                        Align = alClient
                        Caption = ' ICMS Normal: '
                        TabOrder = 0
                        object DBRadioGroup2: TDBRadioGroup
                          Left = 2
                          Top = 15
                          Width = 359
                          Height = 117
                          Align = alLeft
                          Caption = ' N13 - Modalidade de determina'#231#227'o da BC do ICMS: '
                          Columns = 2
                          DataField = 'ICMS_modBC'
                          DataSource = DsNFeItsN
                          Items.Strings = (
                            'Margem valor agregado (%)'
                            'Pauta (valor)'
                            'Pre'#231'o tabelado m'#225'ximo (valor)'
                            'Valor da opera'#231#227'o')
                          TabOrder = 0
                          Values.Strings = (
                            '0'
                            '1'
                            '2'
                            '3')
                        end
                        object GroupBox16: TGroupBox
                          Left = 361
                          Top = 15
                          Width = 204
                          Height = 117
                          Align = alLeft
                          TabOrder = 1
                          object LaICMS_pRedBC: TLabel
                            Left = 8
                            Top = 24
                            Width = 83
                            Height = 13
                            Caption = 'N14: % Red. BC'#178':'
                          end
                          object LaICMS_VBC: TLabel
                            Left = 8
                            Top = 62
                            Width = 88
                            Height = 13
                            Caption = 'N15 - Valor da BC:'
                          end
                          object Label388: TLabel
                            Left = 104
                            Top = 62
                            Width = 85
                            Height = 13
                            Caption = 'N17 - Valor ICMS:'
                          end
                          object LaICMS_pICMS: TLabel
                            Left = 104
                            Top = 22
                            Width = 69
                            Height = 13
                            Caption = 'N16 - % ICMS:'
                          end
                          object DBEdit154: TDBEdit
                            Left = 8
                            Top = 40
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_pRedBC'
                            DataSource = DsNFeItsN
                            TabOrder = 0
                          end
                          object DBEdit162: TDBEdit
                            Left = 104
                            Top = 40
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_pICMS'
                            DataSource = DsNFeItsN
                            TabOrder = 1
                          end
                          object DBEdit161: TDBEdit
                            Left = 8
                            Top = 80
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_vBC'
                            DataSource = DsNFeItsN
                            TabOrder = 2
                          end
                          object DBEdit164: TDBEdit
                            Left = 104
                            Top = 80
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_vICMS'
                            DataSource = DsNFeItsN
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -12
                            Font.Name = 'MS Sans Serif'
                            Font.Style = [fsBold]
                            ParentFont = False
                            TabOrder = 3
                          end
                        end
                        object GroupBox17: TGroupBox
                          Left = 565
                          Top = 15
                          Width = 200
                          Height = 117
                          Align = alLeft
                          Caption = ' CST 51: '
                          TabOrder = 2
                          object Label389: TLabel
                            Left = 4
                            Top = 22
                            Width = 93
                            Height = 13
                            Caption = 'N16a - $ ICMS Op.:'
                          end
                          object Label390: TLabel
                            Left = 4
                            Top = 66
                            Width = 65
                            Height = 13
                            Caption = 'N16b - % Dif.:'
                          end
                          object Label391: TLabel
                            Left = 104
                            Top = 22
                            Width = 87
                            Height = 13
                            Caption = 'N16c - $ ICMS dif:'
                          end
                          object DBEdit253: TDBEdit
                            Left = 4
                            Top = 36
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_vICMSOp'
                            DataSource = DsNFeItsN
                            TabOrder = 0
                          end
                          object DBEdit254: TDBEdit
                            Left = 4
                            Top = 80
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_pDif'
                            DataSource = DsNFeItsN
                            TabOrder = 1
                          end
                          object DBEdit255: TDBEdit
                            Left = 104
                            Top = 36
                            Width = 92
                            Height = 24
                            DataField = 'ICMS_vICMSDif'
                            DataSource = DsNFeItsN
                            TabOrder = 2
                          end
                        end
                        object GroupBox48: TGroupBox
                          Left = 765
                          Top = 15
                          Width = 132
                          Height = 117
                          Align = alLeft
                          Caption = ' CST 60: '
                          TabOrder = 3
                          object Label392: TLabel
                            Left = 4
                            Top = 22
                            Width = 109
                            Height = 13
                            Caption = 'N26 - BC ICMSST Ret:'
                          end
                          object Label393: TLabel
                            Left = 4
                            Top = 66
                            Width = 116
                            Height = 13
                            Caption = 'N27 - Val ICMS ST Ret.:'
                          end
                          object DBEdit256: TDBEdit
                            Left = 4
                            Top = 36
                            Width = 120
                            Height = 24
                            DataField = 'ICMS_vBCSTRet'
                            DataSource = DsNFeItsN
                            TabOrder = 0
                          end
                          object DBEdit257: TDBEdit
                            Left = 4
                            Top = 80
                            Width = 120
                            Height = 24
                            DataField = 'ICMS_vICMSSTRet'
                            DataSource = DsNFeItsN
                            TabOrder = 1
                          end
                        end
                      end
                    end
                  end
                  object TabSheet34: TTabSheet
                    Caption = 'ICMS ST'
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel63: TPanel
                      Left = 0
                      Top = 0
                      Width = 989
                      Height = 134
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object GroupBox15: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 989
                        Height = 134
                        Align = alClient
                        Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
                        TabOrder = 0
                        object Label246: TLabel
                          Left = 372
                          Top = 20
                          Width = 86
                          Height = 13
                          Caption = 'N19: % MVA. BC'#178':'
                        end
                        object Label250: TLabel
                          Left = 460
                          Top = 20
                          Width = 83
                          Height = 13
                          Caption = 'N20: % Red. BC'#178':'
                        end
                        object Label259: TLabel
                          Left = 548
                          Top = 20
                          Width = 76
                          Height = 13
                          Caption = 'Valor da BC ST:'
                          FocusControl = DBEdit168
                        end
                        object Label260: TLabel
                          Left = 656
                          Top = 20
                          Width = 57
                          Height = 13
                          Caption = '% ICMS ST:'
                          FocusControl = DBEdit169
                        end
                        object Label261: TLabel
                          Left = 764
                          Top = 20
                          Width = 73
                          Height = 13
                          Caption = 'Valor ICMS ST:'
                          FocusControl = DBEdit170
                        end
                        object DBRadioGroup3: TDBRadioGroup
                          Left = 2
                          Top = 15
                          Width = 359
                          Height = 117
                          Align = alLeft
                          Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
                          Columns = 2
                          DataField = 'ICMS_modBCST'
                          DataSource = DsNFeItsN
                          Items.Strings = (
                            'Margem valor agregado (%)'
                            'Pauta (valor)'
                            'Pre'#231'o tabelado m'#225'ximo (valor)'
                            'Valor da opera'#231#227'o')
                          TabOrder = 0
                          Values.Strings = (
                            '0'
                            '1'
                            '2'
                            '3')
                        end
                        object DBEdit155: TDBEdit
                          Left = 372
                          Top = 36
                          Width = 84
                          Height = 24
                          DataField = 'ICMS_pRedBCST'
                          DataSource = DsNFeItsN
                          TabOrder = 1
                        end
                        object DBEdit156: TDBEdit
                          Left = 460
                          Top = 36
                          Width = 84
                          Height = 24
                          DataField = 'ICMS_pMVAST'
                          DataSource = DsNFeItsN
                          TabOrder = 2
                        end
                        object DBEdit168: TDBEdit
                          Left = 548
                          Top = 36
                          Width = 104
                          Height = 24
                          DataField = 'ICMS_vBCST'
                          DataSource = DsNFeItsN
                          TabOrder = 3
                        end
                        object DBEdit169: TDBEdit
                          Left = 656
                          Top = 36
                          Width = 104
                          Height = 24
                          DataField = 'ICMS_pICMSST'
                          DataSource = DsNFeItsN
                          TabOrder = 4
                        end
                        object DBEdit170: TDBEdit
                          Left = 764
                          Top = 36
                          Width = 104
                          Height = 24
                          DataField = 'ICMS_vICMSST'
                          DataSource = DsNFeItsN
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                          TabOrder = 5
                        end
                      end
                    end
                  end
                  object TabSheet35: TTabSheet
                    Caption = 'ICMS Deson'
                    ImageIndex = 2
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel64: TPanel
                      Left = 0
                      Top = 0
                      Width = 989
                      Height = 134
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label387: TLabel
                        Left = 856
                        Top = 17
                        Width = 123
                        Height = 13
                        Caption = 'N27a - Valor ICMS deson:'
                      end
                      object DBRadioGroup1: TDBRadioGroup
                        Left = 0
                        Top = 0
                        Width = 851
                        Height = 134
                        Align = alLeft
                        Caption = ' N28 - Motivo da desonera'#231#227'o do ICMS: '
                        Columns = 2
                        DataField = 'ICMS_motDesICMS'
                        DataSource = DsNFeItsN
                        Items.Strings = (
                          '0=Indefinido'
                          '1=T'#225'xi;'
                          '2=Deficiente F'#237'sico(Revogado)'
                          '3=Uso na agropecu'#225'ria;'
                          '4=Frotista/Locadora;'
                          '5=Diplom'#225'tico/Consular;'
                          
                            '6=Utilit'#225'rios e Motocicletas da Amaz'#244'nia Ocidental e '#193'reas de Li' +
                            'vre Com.'
                          '7=SUFRAMA;'
                          '8=Venda a '#211'rg'#227'o P'#250'blico;'
                          '9=Outros;'
                          '10=Deficiente Condutor (Conv'#234'nio ICMS 38/12);'
                          '11=Deficiente N'#227'o Condutor (Conv'#234'nio ICMS 38/12).'
                          '12='#211'rg'#227'o de fomento e desenvolvimento agropecu'#225'rio.')
                        TabOrder = 0
                        Values.Strings = (
                          '0'
                          '1'
                          '2'
                          '3'
                          '4'
                          '5'
                          '6'
                          '7'
                          '8'
                          '9'
                          '10'
                          '11'
                          '12'
                          '13'
                          '14'
                          '15'
                          '16'
                          '17'
                          '18'
                          '19'
                          '20')
                      end
                      object DBEdit252: TDBEdit
                        Left = 856
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'ICMS_vICMSDeson'
                        DataSource = DsNFeItsN
                        TabOrder = 1
                      end
                    end
                  end
                end
              end
              object TabSheet19: TTabSheet
                Caption = ' IPI '
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel34: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Panel39: TPanel
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 260
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object GroupBox19: TGroupBox
                      Left = 0
                      Top = 140
                      Width = 995
                      Height = 120
                      Align = alClient
                      Caption = ' Outras informa'#231#245'es: '
                      Color = clBtnFace
                      ParentBackground = False
                      ParentColor = False
                      TabOrder = 1
                      object Label265: TLabel
                        Left = 12
                        Top = 24
                        Width = 299
                        Height = 13
                        Caption = 'O02: Classe de enquadramento de IPI para cigarros e bebidas: '
                      end
                      object Label266: TLabel
                        Left = 12
                        Top = 48
                        Width = 339
                        Height = 13
                        Caption = 
                          'O03: CNPJ do produtor da mercadoria quando diferente do emitente' +
                          ' ('#179'). '
                      end
                      object Label267: TLabel
                        Left = 516
                        Top = 24
                        Width = 177
                        Height = 13
                        Caption = 'O04: C'#243'digo do seleo de controle IPI:'
                      end
                      object Label268: TLabel
                        Left = 516
                        Top = 48
                        Width = 177
                        Height = 13
                        Caption = 'O05: Quantidade de selo de controle:'
                      end
                      object Label273: TLabel
                        Left = 40
                        Top = 68
                        Width = 263
                        Height = 13
                        Caption = '('#179'): Somente nos casos de exporta'#231#227'o direta ou indireta.'
                      end
                      object DBEdit173: TDBEdit
                        Left = 352
                        Top = 21
                        Width = 104
                        Height = 24
                        DataField = 'IPI_clEnq'
                        DataSource = DsNFeItsO
                        TabOrder = 0
                      end
                      object DBEdit174: TDBEdit
                        Left = 352
                        Top = 45
                        Width = 104
                        Height = 24
                        DataField = 'IPI_CNPJProd'
                        DataSource = DsNFeItsO
                        TabOrder = 1
                      end
                      object DBEdit175: TDBEdit
                        Left = 704
                        Top = 21
                        Width = 104
                        Height = 24
                        DataField = 'IPI_cSelo'
                        DataSource = DsNFeItsO
                        TabOrder = 2
                      end
                      object DBEdit176: TDBEdit
                        Left = 704
                        Top = 45
                        Width = 104
                        Height = 24
                        DataField = 'IPI_qSelo'
                        DataSource = DsNFeItsO
                        TabOrder = 3
                      end
                    end
                    object Panel40: TPanel
                      Left = 0
                      Top = 61
                      Width = 995
                      Height = 79
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 2
                      object Label251: TLabel
                        Left = 332
                        Top = 8
                        Width = 158
                        Height = 13
                        Caption = 'O12: Valor por unidade tribut'#225'vel:'
                      end
                      object Label262: TLabel
                        Left = 16
                        Top = 32
                        Width = 109
                        Height = 13
                        Caption = 'O13: % Aliquota de IPI:'
                      end
                      object Label263: TLabel
                        Left = 332
                        Top = 56
                        Width = 119
                        Height = 13
                        Caption = 'O06: C'#243'd. enq. legal IPI'#185':'
                      end
                      object Label269: TLabel
                        Left = 16
                        Top = 8
                        Width = 116
                        Height = 13
                        Caption = 'O10: Valor da BC do IPI:'
                        FocusControl = DBEdit177
                      end
                      object Label270: TLabel
                        Left = 332
                        Top = 32
                        Width = 273
                        Height = 13
                        Caption = 'O11: Quantidade total na unidade padr'#227'o para tributa'#231#227'o:'
                        FocusControl = DBEdit178
                      end
                      object Label271: TLabel
                        Left = 16
                        Top = 56
                        Width = 84
                        Height = 13
                        Caption = 'O14: Valor do IPI:'
                        FocusControl = DBEdit179
                      end
                      object Label272: TLabel
                        Left = 720
                        Top = 32
                        Width = 222
                        Height = 13
                        Caption = '(somente para produtos tributados por unidade)'
                        FocusControl = DBEdit178
                      end
                      object DBEdit171: TDBEdit
                        Left = 612
                        Top = 53
                        Width = 104
                        Height = 24
                        DataField = 'IPI_cEnq'
                        DataSource = DsNFeItsO
                        TabOrder = 0
                      end
                      object DBEdit157: TDBEdit
                        Left = 180
                        Top = 28
                        Width = 104
                        Height = 24
                        DataField = 'IPI_pIPI'
                        DataSource = DsNFeItsO
                        TabOrder = 1
                      end
                      object DBEdit172: TDBEdit
                        Left = 612
                        Top = 4
                        Width = 104
                        Height = 24
                        DataField = 'IPI_vUnid'
                        DataSource = DsNFeItsO
                        TabOrder = 2
                      end
                      object DBEdit177: TDBEdit
                        Left = 180
                        Top = 4
                        Width = 104
                        Height = 24
                        DataField = 'IPI_vBC'
                        DataSource = DsNFeItsO
                        TabOrder = 3
                      end
                      object DBEdit178: TDBEdit
                        Left = 612
                        Top = 28
                        Width = 104
                        Height = 24
                        DataField = 'IPI_qUnid'
                        DataSource = DsNFeItsO
                        TabOrder = 4
                      end
                      object DBEdit179: TDBEdit
                        Left = 180
                        Top = 52
                        Width = 104
                        Height = 24
                        DataField = 'IPI_vIPI'
                        DataSource = DsNFeItsO
                        TabOrder = 5
                      end
                    end
                    object Panel57: TPanel
                      Left = 0
                      Top = 0
                      Width = 995
                      Height = 61
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object GroupBox18: TGroupBox
                        Left = 297
                        Top = 0
                        Width = 698
                        Height = 61
                        Align = alClient
                        Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
                        TabOrder = 0
                        object Label264: TLabel
                          Left = 8
                          Top = 16
                          Width = 199
                          Height = 13
                          Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                        end
                        object EdIPI_CST: TDBEdit
                          Left = 8
                          Top = 32
                          Width = 45
                          Height = 24
                          DataField = 'IPI_CST'
                          DataSource = DsNFeItsO
                          TabOrder = 0
                          OnChange = EdIPI_CSTChange
                        end
                        object EdTextoIPI_CST: TdmkEdit
                          Left = 56
                          Top = 32
                          Width = 613
                          Height = 21
                          ReadOnly = True
                          TabOrder = 1
                          FormatType = dmktfString
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = ''
                          ValWarn = False
                        end
                      end
                      object GroupBox43: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 297
                        Height = 61
                        Align = alLeft
                        Caption = ' Tag do IPI: '
                        TabOrder = 1
                        object DBCheckBox3: TDBCheckBox
                          Left = 12
                          Top = 24
                          Width = 281
                          Height = 17
                          Caption = 'Informar dados do IPI mesmo se o valor do IPI for zero.'
                          DataField = 'Tem_IPI'
                          DataSource = DsNFeItsI
                          TabOrder = 0
                          ValueChecked = '1'
                          ValueUnchecked = '0'
                        end
                      end
                    end
                  end
                end
              end
              object TabSheet20: TTabSheet
                Caption = ' II '
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel33: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox44: TGroupBox
                    Left = 0
                    Top = 65
                    Width = 995
                    Height = 195
                    Align = alClient
                    Caption = ' Dados do II (Imposto de Importa'#231#227'o): '
                    TabOrder = 0
                    object Label301: TLabel
                      Left = 8
                      Top = 20
                      Width = 59
                      Height = 13
                      Caption = 'Valor da BC:'
                      FocusControl = DBEdit190
                    end
                    object Label302: TLabel
                      Left = 8
                      Top = 44
                      Width = 145
                      Height = 13
                      Caption = 'Valor das depesas aduaneiras:'
                      FocusControl = DBEdit206
                    end
                    object Label303: TLabel
                      Left = 8
                      Top = 68
                      Width = 151
                      Height = 13
                      Caption = 'Valor do imposto de importa'#231#227'o:'
                      FocusControl = DBEdit207
                    end
                    object Label304: TLabel
                      Left = 8
                      Top = 92
                      Width = 62
                      Height = 13
                      Caption = 'Valor do IOF:'
                      FocusControl = DBEdit208
                    end
                    object Label58: TLabel
                      Left = 8
                      Top = 116
                      Width = 74
                      Height = 13
                      Caption = 'N'#250'mero da FCI:'
                    end
                    object DBEdit190: TDBEdit
                      Left = 176
                      Top = 16
                      Width = 104
                      Height = 24
                      DataField = 'II_vBC'
                      DataSource = DsNFeItsP
                      TabOrder = 0
                    end
                    object DBEdit206: TDBEdit
                      Left = 176
                      Top = 40
                      Width = 104
                      Height = 24
                      DataField = 'II_vDespAdu'
                      DataSource = DsNFeItsP
                      TabOrder = 1
                    end
                    object DBEdit207: TDBEdit
                      Left = 176
                      Top = 64
                      Width = 104
                      Height = 24
                      DataField = 'II_vII'
                      DataSource = DsNFeItsP
                      TabOrder = 2
                    end
                    object DBEdit208: TDBEdit
                      Left = 176
                      Top = 88
                      Width = 104
                      Height = 24
                      DataField = 'II_vIOF'
                      DataSource = DsNFeItsP
                      TabOrder = 3
                    end
                    object DBEdit251: TDBEdit
                      Left = 176
                      Top = 112
                      Width = 341
                      Height = 24
                      DataField = 'prod_nFCI'
                      DataSource = DsNFeItsI
                      TabOrder = 4
                    end
                  end
                  object GroupBox45: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 65
                    Align = alTop
                    Caption = ' Tag do II (Imposto de Importa'#231#227'o): '
                    TabOrder = 1
                    object DBCheckBox4: TDBCheckBox
                      Left = 12
                      Top = 24
                      Width = 281
                      Height = 17
                      Caption = 'Informar dados do II mesmo se o valor do II for zero.'
                      DataField = 'Tem_II'
                      DataSource = DsNFeItsI
                      TabOrder = 0
                      ValueChecked = '1'
                      ValueUnchecked = '0'
                    end
                  end
                end
              end
              object TabSheet21: TTabSheet
                Caption = ' PIS '
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel32: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox20: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 129
                    Align = alTop
                    Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 0
                    object Label274: TLabel
                      Left = 8
                      Top = 16
                      Width = 199
                      Height = 13
                      Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                    end
                    object Label281: TLabel
                      Left = 560
                      Top = 72
                      Width = 88
                      Height = 13
                      Caption = 'Q09: Valor do PIS:'
                      FocusControl = DBEdit186
                    end
                    object EdTextoPIS_CST: TdmkEdit
                      Left = 56
                      Top = 32
                      Width = 613
                      Height = 21
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPIS_CST: TDBEdit
                      Left = 8
                      Top = 32
                      Width = 45
                      Height = 24
                      DataField = 'PIS_CST'
                      DataSource = DsNFeItsQ
                      TabOrder = 1
                      OnChange = EdPIS_CSTChange
                    end
                    object GroupBox22: TGroupBox
                      Left = 8
                      Top = 56
                      Width = 268
                      Height = 65
                      Caption = ' Por Al'#237'quota: '
                      TabOrder = 2
                      object Label279: TLabel
                        Left = 8
                        Top = 16
                        Width = 85
                        Height = 13
                        Caption = 'Q07: Valor da BC:'
                        FocusControl = DBEdit184
                      end
                      object Label276: TLabel
                        Left = 136
                        Top = 16
                        Width = 86
                        Height = 13
                        Caption = 'Q08: Aliq. PIS (%):'
                      end
                      object DBEdit184: TDBEdit
                        Left = 8
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PIS_vBC'
                        DataSource = DsNFeItsQ
                        TabOrder = 0
                      end
                      object DBEdit180: TDBEdit
                        Left = 136
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PIS_pPIS'
                        DataSource = DsNFeItsQ
                        TabOrder = 1
                      end
                    end
                    object GroupBox23: TGroupBox
                      Left = 280
                      Top = 56
                      Width = 268
                      Height = 65
                      Caption = ' Por Unidade: '
                      TabOrder = 3
                      object Label275: TLabel
                        Left = 136
                        Top = 16
                        Width = 95
                        Height = 13
                        Caption = 'Q11: % Aliq. PIS ($):'
                      end
                      object Label282: TLabel
                        Left = 8
                        Top = 16
                        Width = 93
                        Height = 13
                        Caption = 'Q10: Qtde vendida:'
                        FocusControl = DBEdit187
                      end
                      object DBEdit187: TDBEdit
                        Left = 8
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PIS_qBCProd'
                        DataSource = DsNFeItsQ
                        TabOrder = 0
                      end
                      object DBEdit181: TDBEdit
                        Left = 136
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PIS_vAliqProd'
                        DataSource = DsNFeItsQ
                        TabOrder = 1
                      end
                    end
                    object DBEdit186: TDBEdit
                      Left = 560
                      Top = 88
                      Width = 104
                      Height = 24
                      DataField = 'PIS_vPIS'
                      DataSource = DsNFeItsQ
                      TabOrder = 4
                    end
                  end
                  object GroupBox21: TGroupBox
                    Left = 0
                    Top = 129
                    Width = 995
                    Height = 131
                    Align = alClient
                    Caption = ' PIS ST: '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 1
                    object Label284: TLabel
                      Left = 560
                      Top = 32
                      Width = 88
                      Height = 13
                      Caption = 'R06: Valor do PIS:'
                      FocusControl = DBEdit189
                    end
                    object GroupBox24: TGroupBox
                      Left = 8
                      Top = 16
                      Width = 268
                      Height = 65
                      Caption = ' Por Al'#237'quota: '
                      TabOrder = 0
                      object Label277: TLabel
                        Left = 8
                        Top = 16
                        Width = 85
                        Height = 13
                        Caption = 'R02: Valor da BC:'
                        FocusControl = DBEdit182
                      end
                      object Label278: TLabel
                        Left = 136
                        Top = 16
                        Width = 86
                        Height = 13
                        Caption = 'R03: Aliq. PIS (%):'
                      end
                      object DBEdit182: TDBEdit
                        Left = 8
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PISST_vBC'
                        DataSource = DsNFeItsR
                        TabOrder = 0
                      end
                      object DBEdit183: TDBEdit
                        Left = 136
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PISST_pPIS'
                        DataSource = DsNFeItsR
                        TabOrder = 1
                      end
                    end
                    object GroupBox25: TGroupBox
                      Left = 280
                      Top = 16
                      Width = 268
                      Height = 65
                      Caption = ' Por Unidade: '
                      TabOrder = 1
                      object Label280: TLabel
                        Left = 136
                        Top = 16
                        Width = 95
                        Height = 13
                        Caption = 'R05: % Aliq. PIS ($):'
                      end
                      object Label283: TLabel
                        Left = 8
                        Top = 16
                        Width = 93
                        Height = 13
                        Caption = 'R04: Qtde vendida:'
                        FocusControl = DBEdit185
                      end
                      object DBEdit185: TDBEdit
                        Left = 8
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PISST_qBCProd'
                        DataSource = DsNFeItsR
                        TabOrder = 0
                      end
                      object DBEdit188: TDBEdit
                        Left = 136
                        Top = 32
                        Width = 124
                        Height = 24
                        DataField = 'PISST_vAliqProd'
                        DataSource = DsNFeItsR
                        TabOrder = 1
                      end
                    end
                    object DBEdit189: TDBEdit
                      Left = 560
                      Top = 48
                      Width = 104
                      Height = 24
                      DataField = 'PISST_vPIS'
                      DataSource = DsNFeItsR
                      TabOrder = 2
                    end
                  end
                end
              end
              object TabSheet22: TTabSheet
                Caption = ' COFINS '
                ImageIndex = 6
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel35: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Panel41: TPanel
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 260
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object GroupBox26: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 995
                      Height = 129
                      Align = alTop
                      Caption = ' CST - COFINS (Programa de Integra'#231#227'o Social): '
                      TabOrder = 0
                      object Label285: TLabel
                        Left = 8
                        Top = 16
                        Width = 199
                        Height = 13
                        Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                      end
                      object Label286: TLabel
                        Left = 560
                        Top = 72
                        Width = 110
                        Height = 13
                        Caption = 'Q09: Valor do COFINS:'
                        FocusControl = DBEdit195
                      end
                      object EdTextoCOFINS_CST: TdmkEdit
                        Left = 56
                        Top = 32
                        Width = 613
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdCOFINS_CST: TDBEdit
                        Left = 8
                        Top = 32
                        Width = 45
                        Height = 24
                        DataField = 'COFINS_CST'
                        DataSource = DsNFeItsS
                        TabOrder = 1
                        OnChange = EdCOFINS_CSTChange
                      end
                      object GroupBox27: TGroupBox
                        Left = 8
                        Top = 56
                        Width = 268
                        Height = 65
                        Caption = ' Por Al'#237'quota: '
                        TabOrder = 2
                        object Label287: TLabel
                          Left = 8
                          Top = 16
                          Width = 85
                          Height = 13
                          Caption = 'Q07: Valor da BC:'
                          FocusControl = DBEdit191
                        end
                        object Label288: TLabel
                          Left = 136
                          Top = 16
                          Width = 108
                          Height = 13
                          Caption = 'Q08: Aliq. COFINS (%):'
                        end
                        object DBEdit191: TDBEdit
                          Left = 8
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINS_vBC'
                          DataSource = DsNFeItsS
                          TabOrder = 0
                        end
                        object DBEdit192: TDBEdit
                          Left = 136
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINS_pCOFINS'
                          DataSource = DsNFeItsS
                          TabOrder = 1
                        end
                      end
                      object GroupBox28: TGroupBox
                        Left = 280
                        Top = 56
                        Width = 268
                        Height = 65
                        Caption = ' Por Unidade: '
                        TabOrder = 3
                        object Label289: TLabel
                          Left = 136
                          Top = 16
                          Width = 117
                          Height = 13
                          Caption = 'Q11: % Aliq. COFINS ($):'
                        end
                        object Label290: TLabel
                          Left = 8
                          Top = 16
                          Width = 93
                          Height = 13
                          Caption = 'Q10: Qtde vendida:'
                          FocusControl = DBEdit193
                        end
                        object DBEdit193: TDBEdit
                          Left = 8
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINS_qBCProd'
                          DataSource = DsNFeItsS
                          TabOrder = 0
                        end
                        object DBEdit194: TDBEdit
                          Left = 136
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINS_vAliqProd'
                          DataSource = DsNFeItsS
                          TabOrder = 1
                        end
                      end
                      object DBEdit195: TDBEdit
                        Left = 560
                        Top = 88
                        Width = 104
                        Height = 24
                        DataField = 'COFINS_vCOFINS'
                        DataSource = DsNFeItsS
                        TabOrder = 4
                      end
                    end
                    object GroupBox29: TGroupBox
                      Left = 0
                      Top = 129
                      Width = 995
                      Height = 131
                      Align = alClient
                      Caption = ' COFINS ST: '
                      TabOrder = 1
                      object Label291: TLabel
                        Left = 560
                        Top = 32
                        Width = 110
                        Height = 13
                        Caption = 'R06: Valor do COFINS:'
                        FocusControl = DBEdit200
                      end
                      object GroupBox30: TGroupBox
                        Left = 8
                        Top = 16
                        Width = 268
                        Height = 65
                        Caption = ' Por Al'#237'quota: '
                        TabOrder = 0
                        object Label292: TLabel
                          Left = 8
                          Top = 16
                          Width = 85
                          Height = 13
                          Caption = 'R02: Valor da BC:'
                          FocusControl = DBEdit196
                        end
                        object Label293: TLabel
                          Left = 136
                          Top = 16
                          Width = 108
                          Height = 13
                          Caption = 'R03: Aliq. COFINS (%):'
                        end
                        object DBEdit196: TDBEdit
                          Left = 8
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINSST_vBC'
                          DataSource = DsNFeItsT
                          TabOrder = 0
                        end
                        object DBEdit197: TDBEdit
                          Left = 136
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINSST_pCOFINS'
                          DataSource = DsNFeItsT
                          TabOrder = 1
                        end
                      end
                      object GroupBox31: TGroupBox
                        Left = 280
                        Top = 16
                        Width = 268
                        Height = 65
                        Caption = ' Por Unidade: '
                        TabOrder = 1
                        object Label294: TLabel
                          Left = 136
                          Top = 16
                          Width = 117
                          Height = 13
                          Caption = 'R05: % Aliq. COFINS ($):'
                        end
                        object Label295: TLabel
                          Left = 8
                          Top = 16
                          Width = 93
                          Height = 13
                          Caption = 'R04: Qtde vendida:'
                          FocusControl = DBEdit198
                        end
                        object DBEdit198: TDBEdit
                          Left = 8
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINSST_qBCProd'
                          DataSource = DsNFeItsT
                          TabOrder = 0
                        end
                        object DBEdit199: TDBEdit
                          Left = 136
                          Top = 32
                          Width = 124
                          Height = 24
                          DataField = 'COFINSST_vAliqProd'
                          DataSource = DsNFeItsT
                          TabOrder = 1
                        end
                      end
                      object DBEdit200: TDBEdit
                        Left = 560
                        Top = 48
                        Width = 104
                        Height = 24
                        DataField = 'COFINSST_vCOFINS'
                        DataSource = DsNFeItsT
                        TabOrder = 2
                      end
                    end
                  end
                end
              end
              object TabSheet23: TTabSheet
                Caption = ' ISS '
                ImageIndex = 7
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel36: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label296: TLabel
                    Left = 8
                    Top = 8
                    Width = 59
                    Height = 13
                    Caption = 'Valor da BC:'
                    FocusControl = DBEdit201
                  end
                  object Label297: TLabel
                    Left = 148
                    Top = 8
                    Width = 60
                    Height = 13
                    Caption = 'Al'#237'quota (%):'
                    FocusControl = DBEdit202
                  end
                  object Label298: TLabel
                    Left = 288
                    Top = 8
                    Width = 78
                    Height = 13
                    Caption = 'Valor do ISSQN:'
                    FocusControl = DBEdit203
                  end
                  object Label299: TLabel
                    Left = 428
                    Top = 8
                    Width = 193
                    Height = 13
                    Caption = 'Munic'#237'pio de ocorr'#234'ncia do fato gerador:'
                    FocusControl = DBEdit204
                  end
                  object Label300: TLabel
                    Left = 8
                    Top = 48
                    Width = 327
                    Height = 13
                    Caption = 
                      'C'#243'digo da lista de servi'#231'os LC 116/03 em que se classifica o ser' +
                      'vi'#231'o:'
                    FocusControl = DBEdit205
                  end
                  object DBEdit201: TDBEdit
                    Left = 8
                    Top = 24
                    Width = 134
                    Height = 24
                    DataField = 'ISSQN_vBC'
                    TabOrder = 0
                  end
                  object DBEdit202: TDBEdit
                    Left = 148
                    Top = 24
                    Width = 134
                    Height = 24
                    DataField = 'ISSQN_vAliq'
                    TabOrder = 1
                  end
                  object DBEdit203: TDBEdit
                    Left = 288
                    Top = 24
                    Width = 134
                    Height = 24
                    DataField = 'ISSQN_vISSQN'
                    TabOrder = 2
                  end
                  object DBEdit204: TDBEdit
                    Left = 428
                    Top = 24
                    Width = 56
                    Height = 24
                    DataField = 'ISSQN_cMunFG'
                    TabOrder = 3
                  end
                  object DBEdit205: TDBEdit
                    Left = 8
                    Top = 64
                    Width = 56
                    Height = 24
                    DataField = 'ISSQN_cListServ'
                    TabOrder = 4
                  end
                  object DBEdit209: TDBEdit
                    Left = 488
                    Top = 24
                    Width = 493
                    Height = 24
                    DataField = 'NO_ISSQN_cMunFG'
                    TabOrder = 5
                  end
                  object DBEdit211: TDBEdit
                    Left = 68
                    Top = 64
                    Width = 913
                    Height = 24
                    DataField = 'NO_ISSQN_cListServ'
                    TabOrder = 6
                  end
                end
              end
              object TabSheet24: TTabSheet
                Caption = ' Informa'#231#245'es adicionais do produto '
                ImageIndex = 8
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel37: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object DBMemo2: TDBMemo
                    Left = 0
                    Top = 0
                    Width = 995
                    Height = 260
                    Align = alClient
                    DataField = 'InfAdProd'
                    DataSource = DsNFeItsV
                    TabOrder = 0
                  end
                end
              end
              object TabSheet29: TTabSheet
                Caption = 'Codifica'#231#227'o NVE'
                ImageIndex = 9
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGrid1: TDBGrid
                  Left = 101
                  Top = 0
                  Width = 894
                  Height = 260
                  Align = alClient
                  DataSource = DsNFeItsINVE
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Importacia'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NVE'
                      Width = 70
                      Visible = True
                    end>
                end
                object Panel1: TPanel
                  Left = 0
                  Top = 0
                  Width = 101
                  Height = 260
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object BtiNVE_Inclui: TBitBtn
                    Tag = 10
                    Left = 4
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Inclui novo banco'
                    Caption = '&Inclui'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtiNVE_IncluiClick
                  end
                  object BtiNVE_Altera: TBitBtn
                    Tag = 11
                    Left = 4
                    Top = 44
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Altera banco atual'
                    Caption = '&Altera'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    Visible = False
                  end
                  object BtiNVE_Exclui: TBitBtn
                    Tag = 12
                    Left = 4
                    Top = 84
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Exclui banco atual'
                    Caption = '&Exclui'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtiNVE_ExcluiClick
                  end
                end
              end
              object TabSheet30: TTabSheet
                Caption = 'Informa'#231#245'es de exporta'#231#227'o '
                ImageIndex = 10
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel59: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 48
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtNFeItsI03_Inclui: TBitBtn
                    Tag = 10
                    Left = 4
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Inclui'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtNFeItsI03_IncluiClick
                  end
                  object BtNFeItsI03_Altera: TBitBtn
                    Tag = 11
                    Left = 96
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Altera'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtNFeItsI03_AlteraClick
                  end
                  object BtNFeItsI03_Exclui: TBitBtn
                    Tag = 12
                    Left = 188
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Exclui'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtNFeItsI03_ExcluiClick
                  end
                end
                object DBGNFeItsI03: TDBGrid
                  Left = 0
                  Top = 48
                  Width = 995
                  Height = 212
                  Align = alClient
                  DataSource = DsNFeItsI03
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'detExport_nDraw'
                      Title.Caption = 'N'#176' do ato concess'#243'rio de Drawback'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'detExport_nRE'
                      Title.Caption = 'N'#176' do registro de exporta'#231#227'o'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'detExport_chNFe'
                      Title.Caption = 'NFe recebida para exporta'#231#227'o'
                      Width = 284
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'detExport_qExport'
                      Title.Caption = 'Qtde real exportado do item'
                      Visible = True
                    end>
                end
              end
              object TabSheet32: TTabSheet
                Caption = 'ICMS Opera'#231#245'es Interestaduais Venda Consumidor Final'
                ImageIndex = 11
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object PnGrupoNA: TPanel
                  Left = 0
                  Top = 0
                  Width = 995
                  Height = 260
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label366: TLabel
                    Left = 4
                    Top = 16
                    Width = 187
                    Height = 13
                    Caption = 'Valor da BC do ICMS da UF de destino:'
                  end
                  object Label367: TLabel
                    Left = 4
                    Top = 40
                    Width = 305
                    Height = 13
                    Caption = '% Adicional Fundo Combate Pobreza (FCP) UF destino (m'#225'x 2%):'
                  end
                  object Label368: TLabel
                    Left = 4
                    Top = 64
                    Width = 170
                    Height = 13
                    Caption = '% Aliquota ICMS interno UF destino:'
                  end
                  object Label369: TLabel
                    Left = 4
                    Top = 88
                    Width = 199
                    Height = 13
                    Caption = '% ICMS interestadual das UFs envolvidas:'
                  end
                  object LapICMSInterPart: TLabel
                    Left = 5
                    Top = 112
                    Width = 155
                    Height = 13
                    Caption = '% ICMS partilha para UF destino:'
                  end
                  object Label370: TLabel
                    Left = 4
                    Top = 136
                    Width = 261
                    Height = 13
                    Caption = 'Valor ICMS Fundo Combate Pobreza (FCP) UF Destino:'
                  end
                  object Label371: TLabel
                    Left = 4
                    Top = 160
                    Width = 347
                    Height = 13
                    Caption = 
                      'Valor ICMS interestadual UF destino j'#225' considerado o valor do IC' +
                      'MS FCP:'
                  end
                  object Label372: TLabel
                    Left = 4
                    Top = 184
                    Width = 186
                    Height = 13
                    Caption = 'Valor ICMS interestadual UF remetente:'
                  end
                  object DBEdit218: TDBEdit
                    Left = 364
                    Top = 12
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_vBCUFDest'
                    DataSource = DsNFeItsNA
                    TabOrder = 0
                  end
                  object DBEdit219: TDBEdit
                    Left = 364
                    Top = 36
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_pFCPUFDest'
                    DataSource = DsNFeItsNA
                    TabOrder = 1
                  end
                  object DBEdit220: TDBEdit
                    Left = 364
                    Top = 60
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_pICMSUFDest'
                    DataSource = DsNFeItsNA
                    TabOrder = 2
                  end
                  object DBEdit240: TDBEdit
                    Left = 364
                    Top = 84
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_pICMSInter'
                    DataSource = DsNFeItsNA
                    TabOrder = 3
                  end
                  object DBEdit241: TDBEdit
                    Left = 364
                    Top = 108
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_pICMSInterPart'
                    DataSource = DsNFeItsNA
                    TabOrder = 4
                  end
                  object DBEdit242: TDBEdit
                    Left = 364
                    Top = 132
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_vFCPUFDest'
                    DataSource = DsNFeItsNA
                    TabOrder = 5
                  end
                  object DBEdit243: TDBEdit
                    Left = 364
                    Top = 156
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_vICMSUFDest'
                    DataSource = DsNFeItsNA
                    TabOrder = 6
                  end
                  object DBEdit244: TDBEdit
                    Left = 364
                    Top = 180
                    Width = 100
                    Height = 24
                    DataField = 'ICMSUFDest_vICMSUFRemet'
                    DataSource = DsNFeItsNA
                    TabOrder = 7
                  end
                end
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Informa'#231#245'es '
            ImageIndex = 5
            object Panel47: TPanel
              Left = 0
              Top = 0
              Width = 752
              Height = 373
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label102: TLabel
                Left = 4
                Top = 0
                Width = 211
                Height = 13
                Caption = 'Informa'#231#245'es adicionais de interesse do fisco:'
              end
              object Label103: TLabel
                Left = 4
                Top = 104
                Width = 274
                Height = 13
                Caption = 'Informa'#231#245'es complementares de interesse do contribuinte:'
              end
              object GroupBox4: TGroupBox
                Left = 4
                Top = 240
                Width = 744
                Height = 61
                Caption = ' Com'#233'rcio exterior ( informa'#231#245'es de embarque): '
                TabOrder = 0
                object Label104: TLabel
                  Left = 12
                  Top = 16
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = DBEdit115
                end
                object Label105: TLabel
                  Left = 44
                  Top = 16
                  Width = 243
                  Height = 13
                  Caption = 'Local de embarque ou de transposi'#231#227'o de fronteira:'
                  FocusControl = dmkEdit116
                end
                object Label403: TLabel
                  Left = 392
                  Top = 16
                  Width = 264
                  Height = 13
                  Caption = 'Descri'#231#227'o do local de despacho (Recinto Alfandeg'#225'rio):'
                  FocusControl = EdExporta_XLocDespacho
                end
                object DBEdit115: TDBEdit
                  Left = 12
                  Top = 32
                  Width = 30
                  Height = 21
                  DataField = 'Exporta_UFEmbarq'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit116: TDBEdit
                  Left = 44
                  Top = 32
                  Width = 344
                  Height = 21
                  DataField = 'Exporta_XLocEmbarq'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit114: TDBEdit
                  Left = 392
                  Top = 32
                  Width = 344
                  Height = 21
                  DataField = 'Exporta_XLocDespacho'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object GroupBox5: TGroupBox
                Left = 4
                Top = 304
                Width = 744
                Height = 61
                Caption = 
                  ' Informa'#231#245'es de compras (Obs.: A nota de  empenho se refere a co' +
                  'mpras p'#250'blicas): '
                TabOrder = 1
                object Label106: TLabel
                  Left = 12
                  Top = 16
                  Width = 88
                  Height = 13
                  Caption = 'Nota de empenho:'
                  FocusControl = DBEdit117
                end
                object Label107: TLabel
                  Left = 132
                  Top = 16
                  Width = 36
                  Height = 13
                  Caption = 'Pedido:'
                  FocusControl = DBEdit118
                end
                object Label108: TLabel
                  Left = 436
                  Top = 16
                  Width = 96
                  Height = 13
                  Caption = 'Contrato de compra:'
                  FocusControl = DBEdit119
                end
                object DBEdit117: TDBEdit
                  Left = 12
                  Top = 32
                  Width = 116
                  Height = 21
                  DataField = 'Compra_XNEmp'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit118: TDBEdit
                  Left = 132
                  Top = 32
                  Width = 300
                  Height = 21
                  DataField = 'Compra_XPed'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit119: TDBEdit
                  Left = 436
                  Top = 32
                  Width = 300
                  Height = 21
                  DataField = 'Compra_XCont'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object MeDBInfAdic_InfAdFisco: TMemo
                Left = 4
                Top = 16
                Width = 741
                Height = 85
                ReadOnly = True
                TabOrder = 2
              end
              object MeDBInfAdic_InfCpl: TMemo
                Left = 3
                Top = 120
                Width = 741
                Height = 117
                ReadOnly = True
                TabOrder = 3
              end
            end
            object Panel72: TPanel
              Left = 752
              Top = 0
              Width = 248
              Height = 373
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Splitter1: TSplitter
                Left = 0
                Top = 123
                Width = 248
                Height = 3
                Cursor = crVSplit
                Align = alBottom
                ExplicitTop = 126
                ExplicitWidth = 249
              end
              object Splitter2: TSplitter
                Left = 0
                Top = 248
                Width = 248
                Height = 3
                Cursor = crVSplit
                Align = alBottom
                ExplicitTop = 251
                ExplicitWidth = 249
              end
              object GroupBox53: TGroupBox
                Left = 0
                Top = 251
                Width = 248
                Height = 122
                Align = alBottom
                Caption = ' Identificador de processo ou ato concess'#243'rio: '
                TabOrder = 0
                object Panel74: TPanel
                  Left = 2
                  Top = 15
                  Width = 244
                  Height = 46
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtNFeCabZPro: TBitBtn
                    Tag = 11
                    Left = 3
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Gerencia'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtNFeCabZProClick
                  end
                end
                object DBGNFeCabZPro: TDBGrid
                  Left = 2
                  Top = 61
                  Width = 244
                  Height = 59
                  Align = alClient
                  DataSource = DsNFeCabZPro
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'indProc'
                      Width = 18
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'nProc'
                      Title.Caption = 'Processo ou ato concess'#243'rio'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Visible = True
                    end>
                end
              end
              object GroupBox52: TGroupBox
                Left = 0
                Top = 0
                Width = 248
                Height = 123
                Align = alClient
                Caption = ' Campo livre do contribuinte: '
                TabOrder = 1
                object Panel73: TPanel
                  Left = 2
                  Top = 15
                  Width = 244
                  Height = 46
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtNFeCabZCon: TBitBtn
                    Tag = 11
                    Left = 3
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Gerencia'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtNFeCabZConClick
                  end
                end
                object DBGNFeCabZCon: TDBGrid
                  Left = 2
                  Top = 61
                  Width = 244
                  Height = 60
                  Align = alClient
                  DataSource = DsNFeCabZCon
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'xCampo'
                      Title.Caption = 'Campo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'xTexto'
                      Title.Caption = 'Texto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Visible = True
                    end>
                end
              end
              object GroupBox54: TGroupBox
                Left = 0
                Top = 126
                Width = 248
                Height = 122
                Align = alBottom
                Caption = ' Campo livre do FISCO!!!: '
                TabOrder = 2
                object Panel75: TPanel
                  Left = 2
                  Top = 15
                  Width = 244
                  Height = 46
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtNFeCabZFis: TBitBtn
                    Tag = 11
                    Left = 3
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Gerencia'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtNFeCabZFisClick
                  end
                end
                object DBGNFeCabZFis: TDBGrid
                  Left = 2
                  Top = 61
                  Width = 244
                  Height = 59
                  Align = alClient
                  DataSource = DsNFeCabZFis
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'xCampo'
                      Title.Caption = 'Campo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'xTexto'
                      Title.Caption = 'Texto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet11: TTabSheet
            Caption = ' NFs Referenciadas '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGNfeCabB: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 373
              Align = alClient
              DataSource = DsNFeCabB
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'refNFe'
                  Title.Caption = 'Chave de acesso da NF-e'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_cUF'
                  Title.Caption = 'UF do emitente'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_AAMM'
                  Title.Caption = 'Ano e m'#234's da emiss'#227'o da NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_CNPJ'
                  Title.Caption = 'CNPJ'
                  Width = 115
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_mod'
                  Title.Caption = 'Modelo da NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_serie'
                  Title.Caption = 'S'#233'rie da NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_nNF'
                  Title.Caption = 'N'#250'mero da NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_cUF'
                  Title.Caption = 'NFP UF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNF_AAMM'
                  Title.Caption = 'NFP AAMM'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_CNPJ'
                  Title.Caption = 'NFP CNPJ'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_CPF'
                  Title.Caption = 'NFP CPF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_IE'
                  Title.Caption = 'NFP IE'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_mod'
                  Title.Caption = 'NFP Mod.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_serie'
                  Title.Caption = 'NFP Serie'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refNFP_nNF'
                  Title.Caption = 'NFP n.NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refCTe'
                  Title.Caption = 'NFP CTe'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refECF_mod'
                  Title.Caption = 'ECF Mod.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refECF_nECF'
                  Title.Caption = 'Num ECF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'refECF_nCOO'
                  Title.Caption = 'CCO ECF'
                  Visible = True
                end>
            end
          end
          object TabSheet12: TTabSheet
            Caption = ' Local de retirada '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object StaticText2: TStaticText
              Left = 0
              Top = 0
              Width = 542
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 
                'Informe os dados de retirada somente quando o endere'#231'o for difer' +
                'ente do ender'#231'o do emitente'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object Panel23: TPanel
              Left = 0
              Top = 17
              Width = 1000
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label211: TLabel
                Left = 8
                Top = 8
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = DBEdit120
              end
              object Label212: TLabel
                Left = 8
                Top = 48
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = DBEdit121
              end
              object Label213: TLabel
                Left = 404
                Top = 48
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = DBEdit122
              end
              object Label214: TLabel
                Left = 532
                Top = 48
                Width = 67
                Height = 13
                Caption = 'Complemento:'
                FocusControl = DBEdit123
              end
              object Label215: TLabel
                Left = 8
                Top = 88
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = DBEdit124
              end
              object Label216: TLabel
                Left = 432
                Top = 88
                Width = 50
                Height = 13
                Caption = 'Munic'#237'pio:'
                FocusControl = DBEdit125
              end
              object Label218: TLabel
                Left = 946
                Top = 88
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit127
              end
              object Label217: TLabel
                Left = 8
                Top = 128
                Width = 24
                Height = 13
                Caption = 'CEP:'
              end
              object Label219: TLabel
                Left = 108
                Top = 128
                Width = 25
                Height = 13
                Caption = 'Pa'#237's:'
              end
              object Label56: TLabel
                Left = 124
                Top = 8
                Width = 23
                Height = 13
                Caption = 'CPF:'
                FocusControl = DBEdit246
              end
              object Label413: TLabel
                Left = 240
                Top = 8
                Width = 108
                Height = 13
                Caption = 'Raz'#227'o Social / Nome :'
                FocusControl = DBEdit266
              end
              object Label414: TLabel
                Left = 396
                Top = 128
                Width = 87
                Height = 13
                Caption = 'Telefone principal:'
              end
              object Label415: TLabel
                Left = 536
                Top = 128
                Width = 28
                Height = 13
                Caption = 'Email:'
              end
              object Label416: TLabel
                Left = 840
                Top = 128
                Width = 19
                Height = 13
                Caption = 'I.E.:'
              end
              object DBEdit120: TDBEdit
                Left = 8
                Top = 24
                Width = 113
                Height = 21
                DataField = 'retirada_CNPJ'
                DataSource = DsNFeCabF
                TabOrder = 0
              end
              object DBEdit121: TDBEdit
                Left = 8
                Top = 64
                Width = 393
                Height = 21
                DataField = 'retirada_xLgr'
                DataSource = DsNFeCabF
                TabOrder = 1
              end
              object DBEdit122: TDBEdit
                Left = 404
                Top = 64
                Width = 125
                Height = 21
                DataField = 'retirada_nro'
                DataSource = DsNFeCabF
                TabOrder = 2
              end
              object DBEdit123: TDBEdit
                Left = 532
                Top = 64
                Width = 445
                Height = 21
                DataField = 'retirada_xCpl'
                DataSource = DsNFeCabF
                TabOrder = 3
              end
              object DBEdit124: TDBEdit
                Left = 8
                Top = 104
                Width = 420
                Height = 21
                DataField = 'retirada_xBairro'
                DataSource = DsNFeCabF
                TabOrder = 4
              end
              object DBEdit125: TDBEdit
                Left = 432
                Top = 104
                Width = 58
                Height = 21
                DataField = 'retirada_cMun'
                DataSource = DsNFeCabF
                TabOrder = 5
              end
              object DBEdit126: TDBEdit
                Left = 492
                Top = 104
                Width = 449
                Height = 21
                DataField = 'retirada_xMun'
                DataSource = DsNFeCabF
                TabOrder = 6
              end
              object DBEdit127: TDBEdit
                Left = 946
                Top = 104
                Width = 30
                Height = 21
                DataField = 'retirada_UF'
                DataSource = DsNFeCabF
                TabOrder = 7
              end
              object DBEdit128: TDBEdit
                Left = 8
                Top = 144
                Width = 96
                Height = 21
                DataField = 'retirada_CEP'
                DataSource = DsNFeCabF
                TabOrder = 8
              end
              object DBEdit129: TDBEdit
                Left = 108
                Top = 144
                Width = 56
                Height = 21
                DataField = 'retirada_cPais'
                DataSource = DsNFeCabF
                TabOrder = 9
              end
              object DBEdit130: TDBEdit
                Left = 168
                Top = 144
                Width = 220
                Height = 21
                DataField = 'retirada_xPais'
                DataSource = DsNFeCabF
                TabOrder = 10
              end
              object DBEdit246: TDBEdit
                Left = 124
                Top = 24
                Width = 113
                Height = 21
                DataField = 'retirada_CPF'
                DataSource = DsNFeCabF
                TabOrder = 11
              end
              object DBEdit266: TDBEdit
                Left = 240
                Top = 24
                Width = 737
                Height = 21
                DataField = 'retirada_xNome'
                DataSource = DsNFeCabF
                TabOrder = 12
              end
              object DBEdit267: TDBEdit
                Left = 396
                Top = 144
                Width = 134
                Height = 21
                DataField = 'retirada_fone'
                DataSource = DsNFeCabF
                TabOrder = 13
              end
              object DBEdit268: TDBEdit
                Left = 536
                Top = 144
                Width = 300
                Height = 21
                DataField = 'retirada_email'
                DataSource = DsNFeCabF
                TabOrder = 14
              end
              object DBEdit269: TDBEdit
                Left = 840
                Top = 144
                Width = 136
                Height = 21
                DataField = 'retirada_IE'
                DataSource = DsNFeCabF
                TabOrder = 15
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = 'Local de Entrega'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object StaticText3: TStaticText
              Left = 0
              Top = 0
              Width = 560
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 
                'Informe os dados de entrega somente quando o endere'#231'o for difere' +
                'nte do ender'#231'o do destinat'#225'rio'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object Panel24: TPanel
              Left = 0
              Top = 17
              Width = 1000
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label221: TLabel
                Left = 8
                Top = 8
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = DBEdit131
              end
              object Label222: TLabel
                Left = 8
                Top = 48
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = DBEdit132
              end
              object Label223: TLabel
                Left = 404
                Top = 48
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = DBEdit133
              end
              object Label224: TLabel
                Left = 532
                Top = 48
                Width = 67
                Height = 13
                Caption = 'Complemento:'
                FocusControl = DBEdit134
              end
              object Label225: TLabel
                Left = 8
                Top = 88
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = DBEdit135
              end
              object Label226: TLabel
                Left = 432
                Top = 88
                Width = 50
                Height = 13
                Caption = 'Munic'#237'pio:'
                FocusControl = DBEdit136
              end
              object Label227: TLabel
                Left = 946
                Top = 88
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit138
              end
              object Label57: TLabel
                Left = 124
                Top = 8
                Width = 23
                Height = 13
                Caption = 'CPF:'
                FocusControl = DBEdit250
              end
              object Label220: TLabel
                Left = 240
                Top = 8
                Width = 108
                Height = 13
                Caption = 'Raz'#227'o Social / Nome :'
                FocusControl = DBEdit139
              end
              object Label228: TLabel
                Left = 12
                Top = 128
                Width = 24
                Height = 13
                Caption = 'CEP:'
              end
              object Label229: TLabel
                Left = 108
                Top = 128
                Width = 25
                Height = 13
                Caption = 'Pa'#237's:'
              end
              object Label230: TLabel
                Left = 396
                Top = 128
                Width = 87
                Height = 13
                Caption = 'Telefone principal:'
              end
              object Label417: TLabel
                Left = 536
                Top = 128
                Width = 28
                Height = 13
                Caption = 'Email:'
              end
              object Label418: TLabel
                Left = 840
                Top = 128
                Width = 19
                Height = 13
                Caption = 'I.E.:'
              end
              object DBEdit131: TDBEdit
                Left = 8
                Top = 24
                Width = 113
                Height = 21
                DataField = 'entrega_CNPJ'
                DataSource = DsNFeCabG
                TabOrder = 0
              end
              object DBEdit132: TDBEdit
                Left = 8
                Top = 64
                Width = 393
                Height = 21
                DataField = 'entrega_xLgr'
                DataSource = DsNFeCabG
                TabOrder = 1
              end
              object DBEdit133: TDBEdit
                Left = 404
                Top = 64
                Width = 125
                Height = 21
                DataField = 'entrega_nro'
                DataSource = DsNFeCabG
                TabOrder = 2
              end
              object DBEdit134: TDBEdit
                Left = 532
                Top = 64
                Width = 445
                Height = 21
                DataField = 'entrega_xCpl'
                DataSource = DsNFeCabG
                TabOrder = 3
              end
              object DBEdit135: TDBEdit
                Left = 8
                Top = 104
                Width = 420
                Height = 21
                DataField = 'entrega_xBairro'
                DataSource = DsNFeCabG
                TabOrder = 4
              end
              object DBEdit136: TDBEdit
                Left = 432
                Top = 104
                Width = 58
                Height = 21
                DataField = 'entrega_cMun'
                DataSource = DsNFeCabG
                TabOrder = 5
              end
              object DBEdit137: TDBEdit
                Left = 492
                Top = 104
                Width = 449
                Height = 21
                DataField = 'entrega_xMun'
                DataSource = DsNFeCabG
                TabOrder = 6
              end
              object DBEdit138: TDBEdit
                Left = 946
                Top = 104
                Width = 30
                Height = 21
                DataField = 'entrega_UF'
                DataSource = DsNFeCabG
                TabOrder = 7
              end
              object DBEdit250: TDBEdit
                Left = 124
                Top = 24
                Width = 113
                Height = 21
                DataField = 'entrega_CPF'
                DataSource = DsNFeCabG
                TabOrder = 8
              end
              object DBEdit139: TDBEdit
                Left = 240
                Top = 24
                Width = 737
                Height = 21
                DataField = 'entrega_xNome'
                DataSource = DsNFeCabG
                TabOrder = 9
              end
              object DBEdit140: TDBEdit
                Left = 8
                Top = 144
                Width = 96
                Height = 21
                DataField = 'entrega_CEP'
                DataSource = DsNFeCabG
                TabOrder = 10
              end
              object DBEdit141: TDBEdit
                Left = 108
                Top = 144
                Width = 56
                Height = 21
                DataField = 'entrega_cPais'
                DataSource = DsNFeCabG
                TabOrder = 11
              end
              object DBEdit270: TDBEdit
                Left = 168
                Top = 144
                Width = 220
                Height = 21
                DataField = 'entrega_xPais'
                DataSource = DsNFeCabG
                TabOrder = 12
              end
              object DBEdit271: TDBEdit
                Left = 396
                Top = 144
                Width = 134
                Height = 21
                DataField = 'entrega_fone'
                DataSource = DsNFeCabG
                TabOrder = 13
              end
              object DBEdit272: TDBEdit
                Left = 536
                Top = 144
                Width = 300
                Height = 21
                DataField = 'entrega_email'
                DataSource = DsNFeCabG
                TabOrder = 14
              end
              object DBEdit273: TDBEdit
                Left = 840
                Top = 144
                Width = 136
                Height = 21
                DataField = 'entrega_IE'
                DataSource = DsNFeCabG
                TabOrder = 15
              end
            end
          end
          object TabSheet31: TTabSheet
            Caption = 'Autorizar obter XML'
            ImageIndex = 12
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 97
              Top = 0
              Width = 903
              Height = 373
              Align = alClient
              DataSource = DsNFeCabGA
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'ITEM'
                  ReadOnly = True
                  Title.Caption = 'Item'
                  Width = 27
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TpDOC'
                  Title.Caption = 'Tipo'
                  Width = 49
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNPJ_CPF'
                  Title.Caption = 'CNPJ / CPF'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AddForma'
                  Title.Caption = 'Forma adi'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT_ECO'
                  Title.Caption = 'Nome entidade / contato'
                  Width = 593
                  Visible = True
                end>
            end
            object Panel60: TPanel
              Left = 0
              Top = 0
              Width = 97
              Height = 373
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtNFeCabGA: TBitBtn
                Tag = 11
                Left = 3
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Gerencia'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtNFeCabGAClick
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Totais de impostos '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel45: TPanel
              Left = 0
              Top = 201
              Width = 1000
              Height = 172
              Align = alClient
              BevelOuter = bvLowered
              ParentBackground = False
              TabOrder = 1
              object Label325: TLabel
                Left = 4
                Top = 48
                Width = 91
                Height = 13
                Caption = 'Valor retido do PIS:'
                FocusControl = DBEdit231
              end
              object Label326: TLabel
                Left = 192
                Top = 48
                Width = 113
                Height = 13
                Caption = 'Valor retido do COFINS:'
                FocusControl = DBEdit232
              end
              object Label327: TLabel
                Left = 192
                Top = 4
                Width = 136
                Height = 13
                Caption = 'Valor do PIS sobre servi'#231'os: '
                FocusControl = DBEdit233
              end
              object Label328: TLabel
                Left = 380
                Top = 4
                Width = 155
                Height = 13
                Caption = 'Valor da COFINS sobre servi'#231'os:'
                FocusControl = DBEdit234
              end
              object Label329: TLabel
                Left = 380
                Top = 48
                Width = 100
                Height = 13
                Caption = 'Valor retido do CSLL:'
                FocusControl = DBEdit235
              end
              object Label330: TLabel
                Left = 584
                Top = 48
                Width = 122
                Height = 13
                Caption = 'Base de c'#225'lculo do IRRF:'
                FocusControl = DBEdit236
              end
              object Label331: TLabel
                Left = 584
                Top = 4
                Width = 183
                Height = 13
                Caption = 'BC da reten'#231#227'o da Previd'#234'ncia Social:'
                FocusControl = DBEdit237
              end
              object Label333: TLabel
                Left = 788
                Top = 48
                Width = 99
                Height = 13
                Caption = 'Valor retido do IRRF:'
                FocusControl = DBEdit239
              end
              object Label324: TLabel
                Left = 4
                Top = 4
                Width = 135
                Height = 13
                Caption = 'Outras despesas acess'#243'rias:'
              end
              object Label332: TLabel
                Left = 788
                Top = 4
                Width = 193
                Height = 13
                Caption = 'Valor da reten'#231#227'o da Previd'#234'ncia Social:'
              end
              object DBEdit230: TDBEdit
                Left = 4
                Top = 20
                Width = 184
                Height = 21
                DataField = 'ICMSTot_vOutro'
                DataSource = DsNFeCabA
                TabOrder = 0
              end
              object DBEdit231: TDBEdit
                Left = 4
                Top = 64
                Width = 184
                Height = 21
                DataField = 'RetTrib_vRetPIS'
                DataSource = DsNFeCabA
                TabOrder = 1
              end
              object DBEdit232: TDBEdit
                Left = 192
                Top = 64
                Width = 184
                Height = 21
                DataField = 'RetTrib_vRetCOFINS'
                DataSource = DsNFeCabA
                TabOrder = 2
              end
              object DBEdit233: TDBEdit
                Left = 192
                Top = 20
                Width = 184
                Height = 21
                DataField = 'ISSQNtot_vPIS'
                DataSource = DsNFeCabA
                TabOrder = 3
              end
              object DBEdit234: TDBEdit
                Left = 380
                Top = 20
                Width = 200
                Height = 21
                DataField = 'ISSQNtot_vCOFINS'
                DataSource = DsNFeCabA
                TabOrder = 4
              end
              object DBEdit235: TDBEdit
                Left = 380
                Top = 64
                Width = 200
                Height = 21
                DataField = 'RetTrib_vRetCSLL'
                DataSource = DsNFeCabA
                TabOrder = 5
              end
              object DBEdit236: TDBEdit
                Left = 584
                Top = 64
                Width = 200
                Height = 21
                DataField = 'RetTrib_vBCIRRF'
                DataSource = DsNFeCabA
                TabOrder = 6
              end
              object DBEdit237: TDBEdit
                Left = 584
                Top = 20
                Width = 200
                Height = 21
                DataField = 'RetTrib_vBCRetPrev'
                DataSource = DsNFeCabA
                TabOrder = 7
              end
              object DBEdit238: TDBEdit
                Left = 788
                Top = 20
                Width = 200
                Height = 21
                DataField = 'RetTrib_vRetPrev'
                DataSource = DsNFeCabA
                TabOrder = 8
              end
              object DBEdit239: TDBEdit
                Left = 788
                Top = 64
                Width = 200
                Height = 21
                DataField = 'RetTrib_vIRRF'
                DataSource = DsNFeCabA
                TabOrder = 9
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 201
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel11: TPanel
                Left = 684
                Top = 0
                Width = 316
                Height = 201
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 316
                  Height = 101
                  Align = alTop
                  Caption = ' Valores totais referentes ao ISSQN: '
                  TabOrder = 0
                  object Label74: TLabel
                    Left = 8
                    Top = 16
                    Width = 69
                    Height = 13
                    Caption = 'Valor servi'#231'os:'
                    FocusControl = DBEdit84
                  end
                  object Label75: TLabel
                    Left = 92
                    Top = 16
                    Width = 52
                    Height = 13
                    Caption = 'BC do ISS:'
                    FocusControl = DBEdit85
                  end
                  object Label76: TLabel
                    Left = 176
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total do ISS:'
                    FocusControl = DBEdit86
                  end
                  object Label77: TLabel
                    Left = 8
                    Top = 56
                    Width = 62
                    Height = 13
                    Caption = 'Valor do PIS:'
                    FocusControl = DBEdit87
                  end
                  object Label78: TLabel
                    Left = 92
                    Top = 56
                    Width = 69
                    Height = 13
                    Caption = 'Valor COFINS:'
                    FocusControl = DBEdit88
                  end
                  object DBEdit84: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ISSQNtot_vServ'
                    DataSource = DsNFeCabA
                    TabOrder = 0
                  end
                  object DBEdit85: TDBEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ISSQNtot_vBC'
                    DataSource = DsNFeCabA
                    TabOrder = 1
                  end
                  object DBEdit86: TDBEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ISSQNtot_vISS'
                    DataSource = DsNFeCabA
                    TabOrder = 2
                  end
                  object DBEdit87: TDBEdit
                    Left = 8
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ISSQNtot_vPIS'
                    DataSource = DsNFeCabA
                    TabOrder = 3
                  end
                  object DBEdit88: TDBEdit
                    Left = 92
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ISSQNtot_vCOFINS'
                    DataSource = DsNFeCabA
                    TabOrder = 4
                  end
                end
                object GroupBox47: TGroupBox
                  Left = 0
                  Top = 101
                  Width = 316
                  Height = 57
                  Align = alTop
                  Caption = ' Val. tot. ref. ICMS em Oper. Interest. Consum. Final: '
                  TabOrder = 1
                  object Label378: TLabel
                    Left = 8
                    Top = 16
                    Width = 50
                    Height = 13
                    Caption = 'Total FCP:'
                    FocusControl = EdICMSTot_vFCPUFDest
                  end
                  object Label379: TLabel
                    Left = 92
                    Top = 16
                    Width = 79
                    Height = 13
                    Caption = 'Total ICMS dest:'
                    FocusControl = EdICMSTot_vICMSUFDest
                  end
                  object Label380: TLabel
                    Left = 176
                    Top = 16
                    Width = 85
                    Height = 13
                    Caption = 'Total ICMS remet:'
                    FocusControl = EdICMSTot_vICMSUFRemet
                  end
                  object DBEdit247: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vFCPUFDest'
                    DataSource = DsNFeCabA
                    TabOrder = 0
                  end
                  object DBEdit248: TDBEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vICMSUFDest'
                    DataSource = DsNFeCabA
                    TabOrder = 1
                  end
                  object DBEdit249: TDBEdit
                    Left = 180
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vICMSUFRemet'
                    DataSource = DsNFeCabA
                    TabOrder = 2
                  end
                end
              end
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 684
                Height = 201
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 684
                  Height = 137
                  Align = alTop
                  Caption = ' Valores totais referentes ao ICMS: '
                  TabOrder = 0
                  object Label59: TLabel
                    Left = 8
                    Top = 16
                    Width = 61
                    Height = 13
                    Caption = 'BC do ICMS:'
                    FocusControl = DBEdit67
                  end
                  object Label60: TLabel
                    Left = 92
                    Top = 16
                    Width = 71
                    Height = 13
                    Caption = 'Valor do ICMS:'
                    FocusControl = DBEdit68
                  end
                  object Label62: TLabel
                    Left = 260
                    Top = 16
                    Width = 78
                    Height = 13
                    Caption = 'BC do ICMS ST:'
                    FocusControl = DBEdit69
                  end
                  object Label63: TLabel
                    Left = 344
                    Top = 16
                    Width = 73
                    Height = 13
                    Caption = 'Valor ICMS ST:'
                    FocusControl = DBEdit73
                  end
                  object Label64: TLabel
                    Left = 428
                    Top = 16
                    Width = 81
                    Height = 13
                    Caption = 'Tot. prod e serv.:'
                    FocusControl = DBEdit74
                  end
                  object Label65: TLabel
                    Left = 512
                    Top = 16
                    Width = 51
                    Height = 13
                    Caption = 'Total frete:'
                    FocusControl = DBEdit75
                  end
                  object Label66: TLabel
                    Left = 596
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total seguro:'
                    FocusControl = DBEdit76
                  end
                  object Label67: TLabel
                    Left = 8
                    Top = 56
                    Width = 76
                    Height = 13
                    Caption = 'Total Desconto:'
                    FocusControl = DBEdit77
                  end
                  object Label68: TLabel
                    Left = 92
                    Top = 56
                    Width = 51
                    Height = 13
                    Caption = 'Total do II:'
                    FocusControl = DBEdit78
                  end
                  object Label69: TLabel
                    Left = 176
                    Top = 56
                    Width = 58
                    Height = 13
                    Caption = 'Total do IPI:'
                    FocusControl = DBEdit79
                  end
                  object Label70: TLabel
                    Left = 260
                    Top = 56
                    Width = 62
                    Height = 13
                    Caption = 'Total do PIS:'
                    FocusControl = DBEdit80
                  end
                  object Label71: TLabel
                    Left = 344
                    Top = 56
                    Width = 69
                    Height = 13
                    Caption = 'Total COFINS:'
                    FocusControl = DBEdit81
                  end
                  object Label72: TLabel
                    Left = 428
                    Top = 56
                    Width = 61
                    Height = 13
                    Caption = 'Total Outros:'
                    FocusControl = DBEdit82
                  end
                  object Label73: TLabel
                    Left = 512
                    Top = 56
                    Width = 53
                    Height = 13
                    Caption = 'Total NF-e:'
                    FocusControl = DBEdit83
                  end
                  object Label257: TLabel
                    Left = 176
                    Top = 16
                    Width = 79
                    Height = 13
                    Caption = 'Vlr. ICMS deson:'
                    FocusControl = EdICMSTot_vICMSDeson
                  end
                  object Label258: TLabel
                    Left = 596
                    Top = 56
                    Width = 64
                    Height = 13
                    Caption = 'Vlr. Tot. Trib.:'
                    FocusControl = EdIvTotTrib
                  end
                  object Label406: TLabel
                    Left = 8
                    Top = 95
                    Width = 50
                    Height = 13
                    Caption = 'Total FCP:'
                    FocusControl = DBEdit262
                  end
                  object Label408: TLabel
                    Left = 92
                    Top = 95
                    Width = 80
                    Height = 13
                    Caption = 'Tot. FCP ret. ST:'
                    FocusControl = DBEdit263
                  end
                  object Label409: TLabel
                    Left = 176
                    Top = 95
                    Width = 80
                    Height = 13
                    Caption = 'T. FCP r. an. ST:'
                    FocusControl = DBEdit264
                  end
                  object Label412: TLabel
                    Left = 260
                    Top = 95
                    Width = 62
                    Height = 13
                    Caption = 'Tot. IPI dev.:'
                    FocusControl = DBEdit265
                  end
                  object DBEdit67: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vBC'
                    DataSource = DsNFeCabA
                    TabOrder = 0
                  end
                  object DBEdit68: TDBEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vICMS'
                    DataSource = DsNFeCabA
                    TabOrder = 1
                  end
                  object DBEdit69: TDBEdit
                    Left = 260
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vBCST'
                    DataSource = DsNFeCabA
                    TabOrder = 2
                  end
                  object DBEdit73: TDBEdit
                    Left = 344
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vST'
                    DataSource = DsNFeCabA
                    TabOrder = 3
                  end
                  object DBEdit74: TDBEdit
                    Left = 428
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vProd'
                    DataSource = DsNFeCabA
                    TabOrder = 4
                  end
                  object DBEdit75: TDBEdit
                    Left = 512
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vFrete'
                    DataSource = DsNFeCabA
                    TabOrder = 5
                  end
                  object DBEdit76: TDBEdit
                    Left = 596
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vSeg'
                    DataSource = DsNFeCabA
                    TabOrder = 6
                  end
                  object DBEdit77: TDBEdit
                    Left = 8
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vDesc'
                    DataSource = DsNFeCabA
                    TabOrder = 7
                  end
                  object DBEdit78: TDBEdit
                    Left = 92
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vII'
                    DataSource = DsNFeCabA
                    TabOrder = 8
                  end
                  object DBEdit79: TDBEdit
                    Left = 176
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vIPI'
                    DataSource = DsNFeCabA
                    TabOrder = 9
                  end
                  object DBEdit80: TDBEdit
                    Left = 260
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vPIS'
                    DataSource = DsNFeCabA
                    TabOrder = 10
                  end
                  object DBEdit81: TDBEdit
                    Left = 344
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vCOFINS'
                    DataSource = DsNFeCabA
                    TabOrder = 11
                  end
                  object DBEdit82: TDBEdit
                    Left = 428
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vOutro'
                    DataSource = DsNFeCabA
                    TabOrder = 12
                  end
                  object DBEdit83: TDBEdit
                    Left = 512
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vNF'
                    DataSource = DsNFeCabA
                    TabOrder = 13
                  end
                  object DBEdit258: TDBEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vICMSDeson'
                    DataSource = DsNFeCabA
                    TabOrder = 14
                  end
                  object DBEdit259: TDBEdit
                    Left = 596
                    Top = 72
                    Width = 80
                    Height = 21
                    DataField = 'vTotTrib'
                    DataSource = DsNFeCabA
                    TabOrder = 15
                  end
                  object DBEdit262: TDBEdit
                    Left = 8
                    Top = 111
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vFCP'
                    DataSource = DsNFeCabA
                    TabOrder = 16
                  end
                  object DBEdit263: TDBEdit
                    Left = 92
                    Top = 111
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vFCPST'
                    DataSource = DsNFeCabA
                    TabOrder = 17
                  end
                  object DBEdit264: TDBEdit
                    Left = 176
                    Top = 111
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vFCPSTRet'
                    DataSource = DsNFeCabA
                    TabOrder = 18
                  end
                  object DBEdit265: TDBEdit
                    Left = 260
                    Top = 111
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vIPIDevol'
                    DataSource = DsNFeCabA
                    TabOrder = 19
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 0
                  Top = 137
                  Width = 684
                  Height = 64
                  Align = alClient
                  Caption = ' Reten'#231#227'o de Tributos (em valores - $): '
                  TabOrder = 1
                  object Label79: TLabel
                    Left = 8
                    Top = 16
                    Width = 20
                    Height = 13
                    Caption = 'PIS:'
                    FocusControl = DBEdit89
                  end
                  object Label80: TLabel
                    Left = 92
                    Top = 16
                    Width = 42
                    Height = 13
                    Caption = 'COFINS:'
                    FocusControl = DBEdit90
                  end
                  object Label81: TLabel
                    Left = 176
                    Top = 16
                    Width = 29
                    Height = 13
                    Caption = 'CSLL:'
                    FocusControl = DBEdit91
                  end
                  object Label82: TLabel
                    Left = 260
                    Top = 16
                    Width = 60
                    Height = 13
                    Caption = 'BC do IRRF:'
                    FocusControl = DBEdit92
                  end
                  object Label83: TLabel
                    Left = 344
                    Top = 16
                    Width = 28
                    Height = 13
                    Caption = 'IRRF:'
                    FocusControl = DBEdit93
                  end
                  object Label84: TLabel
                    Left = 428
                    Top = 16
                    Width = 75
                    Height = 13
                    Caption = 'BC Prev. social:'
                    FocusControl = DBEdit94
                  end
                  object Label85: TLabel
                    Left = 512
                    Top = 16
                    Width = 58
                    Height = 13
                    Caption = 'Prev. social:'
                    FocusControl = DBEdit95
                  end
                  object DBEdit89: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vRetPIS'
                    DataSource = DsNFeCabA
                    TabOrder = 0
                  end
                  object DBEdit90: TDBEdit
                    Left = 92
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vRetCOFINS'
                    DataSource = DsNFeCabA
                    TabOrder = 1
                  end
                  object DBEdit91: TDBEdit
                    Left = 176
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vRetCSLL'
                    DataSource = DsNFeCabA
                    TabOrder = 2
                  end
                  object DBEdit92: TDBEdit
                    Left = 260
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vBCIRRF'
                    DataSource = DsNFeCabA
                    TabOrder = 3
                  end
                  object DBEdit93: TDBEdit
                    Left = 344
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vIRRF'
                    DataSource = DsNFeCabA
                    TabOrder = 4
                  end
                  object DBEdit94: TDBEdit
                    Left = 428
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vBCRetPrev'
                    DataSource = DsNFeCabA
                    TabOrder = 5
                  end
                  object DBEdit95: TDBEdit
                    Left = 512
                    Top = 32
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vRetPrev'
                    DataSource = DsNFeCabA
                    TabOrder = 6
                  end
                end
              end
            end
          end
          object TabSheet25: TTabSheet
            Caption = ' Dados da cobran'#231'a '
            ImageIndex = 10
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 373
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel49: TPanel
                Left = 0
                Top = 0
                Width = 1000
                Height = 45
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label308: TLabel
                  Left = 4
                  Top = 4
                  Width = 85
                  Height = 13
                  Caption = 'N'#250'mero da fatura:'
                  FocusControl = DBEdit214
                end
                object Label309: TLabel
                  Left = 144
                  Top = 4
                  Width = 61
                  Height = 13
                  Caption = 'Valor orignal:'
                  FocusControl = DBEdit215
                end
                object Label310: TLabel
                  Left = 284
                  Top = 4
                  Width = 49
                  Height = 13
                  Caption = 'Desconto:'
                  FocusControl = DBEdit216
                end
                object Label311: TLabel
                  Left = 424
                  Top = 4
                  Width = 62
                  Height = 13
                  Caption = 'Valor l'#237'quido:'
                  FocusControl = DBEdit217
                end
                object DBEdit214: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 134
                  Height = 21
                  DataField = 'Cobr_Fat_nFat'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit215: TDBEdit
                  Left = 144
                  Top = 20
                  Width = 134
                  Height = 21
                  DataField = 'Cobr_Fat_vOrig'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit216: TDBEdit
                  Left = 284
                  Top = 20
                  Width = 134
                  Height = 21
                  DataField = 'Cobr_Fat_vDesc'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
                object DBEdit217: TDBEdit
                  Left = 424
                  Top = 20
                  Width = 134
                  Height = 21
                  DataField = 'Cobr_Fat_vLiq'
                  DataSource = DsNFeCabA
                  TabOrder = 3
                end
              end
              object DBGrid4: TDBGrid
                Left = 0
                Top = 45
                Width = 1000
                Height = 328
                Align = alClient
                DataSource = DsNFeCabY
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'nDup'
                    Title.Caption = 'N'#250'mero duplicata'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'dVenc'
                    Title.Caption = 'Vencimento'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'vDup'
                    Title.Caption = 'Valor'
                    Width = 88
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Lancto'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet37: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = 'Informa'#231#245'es de Pagamento'
            ImageIndex = 13
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel76: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtExclui: TBitBtn
                Tag = 12
                Left = 188
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Exclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtExcluiClick
              end
              object BtAltera: TBitBtn
                Tag = 11
                Left = 96
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtAlteraClick
              end
              object BtInclui: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Inclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtIncluiClick
              end
            end
            object DBGNFeCabYA: TDBGrid
              Left = 0
              Top = 48
              Width = 1000
              Height = 325
              Align = alClient
              DataSource = DsNFeCabYA
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tPag_TXT'
                  Title.Caption = 'Meio de pagamento'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vPag'
                  Title.Caption = 'Valor do Pgto'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vTroco'
                  Title.Caption = 'Valor do troco'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tpIntegra_TXT'
                  Title.Caption = 'Tipo de Itegra'#231#227'o para pagamento'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNPJ_TXT'
                  Title.Caption = 'CNPJ da Credenciadora de cart'#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tBand_TXT'
                  Title.Caption = 'Bandeira da operadora de cart'#227'o'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'cAut'
                  Title.Caption = 'N'#186' de autoriz. da opera'#231#227'o cart'#227'o'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet28: TTabSheet
            Caption = ' SINTEGRA '
            ImageIndex = 11
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel55: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 376
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 4
          Width = 29
          Height = 13
          Caption = 'FatID:'
          FocusControl = DBEdit7
        end
        object Label12: TLabel
          Left = 72
          Top = 4
          Width = 46
          Height = 13
          Caption = 'Fat.Num.:'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 140
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit9
        end
        object Label14: TLabel
          Left = 188
          Top = 4
          Width = 26
          Height = 13
          Caption = 'IDCtrl'
          FocusControl = DBEdit10
        end
        object Label15: TLabel
          Left = 256
          Top = 4
          Width = 24
          Height = 13
          Caption = 'Lote:'
          FocusControl = DBEdit11
        end
        object Label16: TLabel
          Left = 396
          Top = 4
          Width = 36
          Height = 13
          Caption = 'Vers'#227'o:'
          FocusControl = DBEdit12
        end
        object Label17: TLabel
          Left = 508
          Top = 4
          Width = 264
          Height = 13
          Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
          FocusControl = DBEdit13
        end
        object Label314: TLabel
          Left = 784
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Status:'
        end
        object Label3: TLabel
          Left = 440
          Top = 4
          Width = 58
          Height = 13
          Caption = 'C'#243'd. chave:'
          FocusControl = DBEdit2
        end
        object Label306: TLabel
          Left = 824
          Top = 4
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
          FocusControl = DBEdit212
        end
        object Label307: TLabel
          Left = 860
          Top = 4
          Width = 32
          Height = 13
          Caption = 'N'#186' NF:'
          FocusControl = DBEdit213
        end
        object DBEdit7: TDBEdit
          Left = 8
          Top = 20
          Width = 64
          Height = 21
          DataField = 'FatID'
          DataSource = DsNFeCabA
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 72
          Top = 20
          Width = 64
          Height = 21
          DataField = 'FatNum'
          DataSource = DsNFeCabA
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 140
          Top = 20
          Width = 45
          Height = 21
          DataField = 'Empresa'
          DataSource = DsNFeCabA
          TabOrder = 2
        end
        object DBEdit10: TDBEdit
          Left = 188
          Top = 20
          Width = 64
          Height = 21
          DataField = 'IDCtrl'
          DataSource = DsNFeCabA
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 256
          Top = 20
          Width = 134
          Height = 21
          DataField = 'LoteEnv'
          DataSource = DsNFeCabA
          TabOrder = 4
        end
        object DBEdit12: TDBEdit
          Left = 396
          Top = 20
          Width = 41
          Height = 21
          DataField = 'versao'
          DataSource = DsNFeCabA
          TabOrder = 5
        end
        object DBEdit13: TDBEdit
          Left = 508
          Top = 20
          Width = 271
          Height = 21
          DataField = 'Id'
          DataSource = DsNFeCabA
          TabOrder = 6
        end
        object DBEdit2: TDBEdit
          Left = 440
          Top = 20
          Width = 64
          Height = 21
          DataField = 'ide_cNF'
          DataSource = DsNFeCabA
          TabOrder = 7
        end
        object DBEdit210: TDBEdit
          Left = 784
          Top = 20
          Width = 33
          Height = 21
          DataField = 'Status'
          DataSource = DsNFeCabA
          TabOrder = 8
        end
        object DBEdit212: TDBEdit
          Left = 820
          Top = 20
          Width = 33
          Height = 21
          DataField = 'ide_serie'
          DataSource = DsNFeCabA
          TabOrder = 9
        end
        object DBEdit213: TDBEdit
          Left = 856
          Top = 20
          Width = 80
          Height = 21
          DataField = 'ide_nNF'
          DataSource = DsNFeCabA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel53: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 767
        Height = 17
        Caption = 
          'Nos campos que possuem campo num'#233'rico e campo texto ao lado prec' +
          'ione a tecla F4 para mostrar a lista de op'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 767
        Height = 17
        Caption = 
          'Nos campos que possuem campo num'#233'rico e campo texto ao lado prec' +
          'ione a tecla F4 para mostrar a lista de op'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object QrNFeCabA: TMySQLQuery
    BeforeOpen = QrNFeCabABeforeOpen
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT mun1.Nome ide_cMunFG_TXT,  cnan.Nome CNAE_TXT,'
      'frc.Nome NO_FisRegCad, car.Nome NO_CARTEMISS,'
      'car.Tipo TP_CART, tpc.Nome NO_TabelaPrc,'
      'ppc.Nome NO_CondicaoPG, ppc.JurosMes,'
      'ppc.MedDDSimpl, ppc.MedDDReal,'
      'frc.ISS_Usa, frc.ISS_Alq, nfea.*'
      'FROM nfecaba nfea'
      
        'LEFT JOIN locbdermall.cnae21Cad cnan ON cnan.CodAlf=nfea.emit_CN' +
        'AE'
      
        'LEFT JOIN locbdermall.dtb_munici mun1 ON mun1.Codigo=nfea.ide_cM' +
        'unFG'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad'
      'LEFT JOIN carteiras car ON car.Codigo=nfea.CartEmiss'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=nfea.TabelaPrc'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=nfea.CondicaoPg')
    Left = 12
    Top = 12
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfecaba.FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfecaba.FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfecaba.Empresa'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfecaba.IDCtrl'
    end
    object QrNFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
      Origin = 'nfecaba.LoteEnv'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfecaba.Id'
      Size = 44
    end
    object QrNFeCabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
      Origin = 'nfecaba.ide_cUF'
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
      Origin = 'nfecaba.ide_cNF'
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'nfecaba.ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_indPag: TSmallintField
      FieldName = 'ide_indPag'
      Origin = 'nfecaba.ide_indPag'
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'nfecaba.ide_mod'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'nfecaba.ide_serie'
    end
    object QrNFeCabAversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfecaba.versao'
      DisplayFormat = '0.00'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Origin = 'nfecaba.ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'nfecaba.ide_dEmi'
    end
    object QrNFeCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      Origin = 'nfecaba.ide_dSaiEnt'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrNFeCabAide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
      Origin = 'nfecaba.ide_cMunFG'
    end
    object QrNFeCabAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'nfecaba.ide_tpImp'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'nfecaba.ide_tpEmis'
    end
    object QrNFeCabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
      Origin = 'nfecaba.ide_cDV'
    end
    object QrNFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'nfecaba.ide_tpAmb'
    end
    object QrNFeCabAide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
      Origin = 'nfecaba.ide_finNFe'
    end
    object QrNFeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
      Origin = 'nfecaba.ide_procEmi'
    end
    object QrNFeCabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
      Origin = 'nfecaba.ide_verProc'
    end
    object QrNFeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Origin = 'nfecaba.emit_CNPJ'
      Size = 14
    end
    object QrNFeCabAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Origin = 'nfecaba.emit_CPF'
      Size = 11
    end
    object QrNFeCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Origin = 'nfecaba.emit_xNome'
      Size = 60
    end
    object QrNFeCabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Origin = 'nfecaba.emit_xFant'
      Size = 60
    end
    object QrNFeCabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Origin = 'nfecaba.emit_xLgr'
      Size = 60
    end
    object QrNFeCabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Origin = 'nfecaba.emit_nro'
      Size = 60
    end
    object QrNFeCabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Origin = 'nfecaba.emit_xCpl'
      Size = 60
    end
    object QrNFeCabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Origin = 'nfecaba.emit_xBairro'
      Size = 60
    end
    object QrNFeCabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
      Origin = 'nfecaba.emit_cMun'
    end
    object QrNFeCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Origin = 'nfecaba.emit_xMun'
      Size = 60
    end
    object QrNFeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Origin = 'nfecaba.emit_UF'
      Size = 2
    end
    object QrNFeCabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
      Origin = 'nfecaba.emit_CEP'
    end
    object QrNFeCabAemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
      Origin = 'nfecaba.emit_cPais'
    end
    object QrNFeCabAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Origin = 'nfecaba.emit_xPais'
      Size = 60
    end
    object QrNFeCabAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Origin = 'nfecaba.emit_fone'
      Size = 14
    end
    object QrNFeCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Origin = 'nfecaba.emit_IE'
      Size = 14
    end
    object QrNFeCabAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Origin = 'nfecaba.emit_IEST'
      Size = 14
    end
    object QrNFeCabAemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Origin = 'nfecaba.emit_IM'
      Size = 15
    end
    object QrNFeCabAemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Origin = 'nfecaba.emit_CNAE'
      Size = 7
    end
    object QrNFeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Origin = 'nfecaba.dest_CNPJ'
      Size = 14
    end
    object QrNFeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Origin = 'nfecaba.dest_CPF'
      Size = 11
    end
    object QrNFeCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Origin = 'nfecaba.dest_xNome'
      Size = 60
    end
    object QrNFeCabAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Origin = 'nfecaba.dest_xLgr'
      Size = 60
    end
    object QrNFeCabAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Origin = 'nfecaba.dest_nro'
      Size = 60
    end
    object QrNFeCabAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Origin = 'nfecaba.dest_xCpl'
      Size = 60
    end
    object QrNFeCabAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Origin = 'nfecaba.dest_xBairro'
      Size = 60
    end
    object QrNFeCabAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
      Origin = 'nfecaba.dest_cMun'
    end
    object QrNFeCabAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Origin = 'nfecaba.dest_xMun'
      Size = 60
    end
    object QrNFeCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Origin = 'nfecaba.dest_UF'
      Size = 2
    end
    object QrNFeCabAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Origin = 'nfecaba.dest_CEP'
      Size = 8
    end
    object QrNFeCabAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
      Origin = 'nfecaba.dest_cPais'
    end
    object QrNFeCabAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Origin = 'nfecaba.dest_xPais'
      Size = 60
    end
    object QrNFeCabAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Origin = 'nfecaba.dest_fone'
      Size = 14
    end
    object QrNFeCabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Origin = 'nfecaba.dest_IE'
      Size = 14
    end
    object QrNFeCabAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Origin = 'nfecaba.dest_ISUF'
      Size = 9
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Origin = 'nfecaba.ICMSTot_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Origin = 'nfecaba.ICMSTot_vICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Origin = 'nfecaba.ICMSTot_vBCST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Origin = 'nfecaba.ICMSTot_vST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Origin = 'nfecaba.ICMSTot_vProd'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Origin = 'nfecaba.ICMSTot_vFrete'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Origin = 'nfecaba.ICMSTot_vSeg'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Origin = 'nfecaba.ICMSTot_vDesc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Origin = 'nfecaba.ICMSTot_vII'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Origin = 'nfecaba.ICMSTot_vIPI'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Origin = 'nfecaba.ICMSTot_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Origin = 'nfecaba.ICMSTot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Origin = 'nfecaba.ICMSTot_vOutro'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Origin = 'nfecaba.ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
      Origin = 'nfecaba.ISSQNtot_vServ'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
      Origin = 'nfecaba.ISSQNtot_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
      Origin = 'nfecaba.ISSQNtot_vISS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
      Origin = 'nfecaba.ISSQNtot_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
      Origin = 'nfecaba.ISSQNtot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
      Origin = 'nfecaba.RetTrib_vRetPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
      Origin = 'nfecaba.RetTrib_vRetCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
      Origin = 'nfecaba.RetTrib_vRetCSLL'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
      Origin = 'nfecaba.RetTrib_vBCIRRF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
      Origin = 'nfecaba.RetTrib_vIRRF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
      Origin = 'nfecaba.RetTrib_vBCRetPrev'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabARetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
      Origin = 'nfecaba.RetTrib_vRetPrev'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Origin = 'nfecaba.ModFrete'
    end
    object QrNFeCabATransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Origin = 'nfecaba.Transporta_CNPJ'
      Size = 14
    end
    object QrNFeCabATransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Origin = 'nfecaba.Transporta_CPF'
      Size = 11
    end
    object QrNFeCabATransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Origin = 'nfecaba.Transporta_XNome'
      Size = 60
    end
    object QrNFeCabATransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Origin = 'nfecaba.Transporta_IE'
      Size = 14
    end
    object QrNFeCabATransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Origin = 'nfecaba.Transporta_XEnder'
      Size = 60
    end
    object QrNFeCabATransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Origin = 'nfecaba.Transporta_XMun'
      Size = 60
    end
    object QrNFeCabATransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Origin = 'nfecaba.Transporta_UF'
      Size = 2
    end
    object QrNFeCabARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
      Origin = 'nfecaba.RetTransp_vServ'
    end
    object QrNFeCabARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
      Origin = 'nfecaba.RetTransp_vBCRet'
    end
    object QrNFeCabARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
      Origin = 'nfecaba.RetTransp_PICMSRet'
    end
    object QrNFeCabARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
      Origin = 'nfecaba.RetTransp_vICMSRet'
    end
    object QrNFeCabARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Origin = 'nfecaba.RetTransp_CFOP'
      Size = 4
    end
    object QrNFeCabARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Origin = 'nfecaba.RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeCabAVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Origin = 'nfecaba.VeicTransp_Placa'
      Size = 8
    end
    object QrNFeCabAVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Origin = 'nfecaba.VeicTransp_UF'
      Size = 2
    end
    object QrNFeCabAVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
      Origin = 'nfecaba.VeicTransp_RNTC'
    end
    object QrNFeCabACobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Origin = 'nfecaba.Cobr_Fat_nFat'
      Size = 60
    end
    object QrNFeCabACobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
      Origin = 'nfecaba.Cobr_Fat_vOrig'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabACobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
      Origin = 'nfecaba.Cobr_Fat_vDesc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabACobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
      Origin = 'nfecaba.Cobr_Fat_vLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeCabAInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      Origin = 'nfecaba.InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeCabAExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Origin = 'nfecaba.Exporta_UFEmbarq'
      Size = 2
    end
    object QrNFeCabAExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Origin = 'nfecaba.Exporta_XLocEmbarq'
      Size = 60
    end
    object QrNFeCabACompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'nfecaba.Compra_XNEmp'
      Size = 17
    end
    object QrNFeCabACompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'nfecaba.Compra_XPed'
      Size = 60
    end
    object QrNFeCabACompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'nfecaba.Compra_XCont'
      Size = 60
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrNFeCabAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Origin = 'nfecaba.infProt_Id'
      Size = 30
    end
    object QrNFeCabAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
      Origin = 'nfecaba.infProt_tpAmb'
    end
    object QrNFeCabAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Origin = 'nfecaba.infProt_verAplic'
      Size = 30
    end
    object QrNFeCabAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
      Origin = 'nfecaba.infProt_dhRecbto'
    end
    object QrNFeCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Origin = 'nfecaba.infProt_nProt'
      Size = 15
    end
    object QrNFeCabAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Origin = 'nfecaba.infProt_digVal'
      Size = 28
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Origin = 'nfecaba.infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Origin = 'nfecaba.infCanc_Id'
      Size = 30
    end
    object QrNFeCabAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
      Origin = 'nfecaba.infCanc_tpAmb'
    end
    object QrNFeCabAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Origin = 'nfecaba.infCanc_verAplic'
      Size = 30
    end
    object QrNFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
      Origin = 'nfecaba.infCanc_dhRecbto'
    end
    object QrNFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Origin = 'nfecaba.infCanc_nProt'
      Size = 15
    end
    object QrNFeCabAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Origin = 'nfecaba.infCanc_digVal'
      Size = 28
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Origin = 'nfecaba.infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
      Origin = 'nfecaba.infCanc_cJust'
    end
    object QrNFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Origin = 'nfecaba.infCanc_xJust'
      Size = 255
    end
    object QrNFeCabA_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Origin = 'nfecaba._Ativo_'
    end
    object QrNFeCabALk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfecaba.Lk'
    end
    object QrNFeCabADataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfecaba.DataCad'
    end
    object QrNFeCabADataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfecaba.DataAlt'
    end
    object QrNFeCabAUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfecaba.UserCad'
    end
    object QrNFeCabAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfecaba.UserAlt'
    end
    object QrNFeCabAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfecaba.AlterWeb'
    end
    object QrNFeCabAAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfecaba.Ativo'
    end
    object QrNFeCabAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Origin = 'nfecaba.FisRegCad'
    end
    object QrNFeCabATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'nfecaba.TabelaPrc'
    end
    object QrNFeCabACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'nfecaba.CartEmiss'
    end
    object QrNFeCabACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Origin = 'nfecaba.CondicaoPg'
    end
    object QrNFeCabAMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrNFeCabAMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrNFeCabAJurosMes: TFloatField
      FieldName = 'JurosMes'
      Origin = 'pediprzcab.JurosMes'
    end
    object QrNFeCabAide_cMunFG_TXT: TWideStringField
      FieldName = 'ide_cMunFG_TXT'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrNFeCabACNAE_TXT: TWideStringField
      FieldName = 'CNAE_TXT'
      Size = 255
    end
    object QrNFeCabANO_CARTEMISS: TWideStringField
      FieldName = 'NO_CARTEMISS'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrNFeCabANO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Origin = 'fisregcad.Nome'
      Size = 50
    end
    object QrNFeCabANO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Size = 50
    end
    object QrNFeCabANO_CondicaoPG: TWideStringField
      FieldName = 'NO_CondicaoPG'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrNFeCabATP_CART: TIntegerField
      FieldName = 'TP_CART'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrNFeCabAide_tpNF_TXT: TWideStringField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'ide_tpNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrNFeCabAide_indPag_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_indPag_TXT'
      Calculated = True
    end
    object QrNFeCabAide_procEmi_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_procEmi_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_finNFe_TXT: TWideStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'ide_finNFe_TXT'
      Calculated = True
    end
    object QrNFeCabAide_tpAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_tpAmb_TXT'
      Size = 12
      Calculated = True
    end
    object QrNFeCabAide_tpEmis_TXT: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'ide_tpEmis_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_tpImp_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_tpImp_TXT'
      Size = 10
      Calculated = True
    end
    object QrNFeCabAModFrete_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ModFrete_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeCabAISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
      Origin = 'fisregcad.ISS_Usa'
    end
    object QrNFeCabAISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
      Origin = 'fisregcad.ISS_Alq'
    end
    object QrNFeCabAFreteExtra: TFloatField
      FieldName = 'FreteExtra'
      Origin = 'nfecaba.FreteExtra'
    end
    object QrNFeCabASegurExtra: TFloatField
      FieldName = 'SegurExtra'
      Origin = 'nfecaba.SegurExtra'
    end
    object QrNFeCabAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Origin = 'nfecaba.ICMSRec_pRedBC'
    end
    object QrNFeCabAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Origin = 'nfecaba.ICMSRec_vICMS'
    end
    object QrNFeCabAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Origin = 'nfecaba.IPIRec_pRedBC'
    end
    object QrNFeCabAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Origin = 'nfecaba.PISRec_pRedBC'
    end
    object QrNFeCabACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Origin = 'nfecaba.COFINSRec_pRedBC'
    end
    object QrNFeCabAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Origin = 'nfecaba.IPIRec_vIPI'
    end
    object QrNFeCabAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Origin = 'nfecaba.PISRec_vPIS'
    end
    object QrNFeCabACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Origin = 'nfecaba.COFINSRec_vCOFINS'
    end
    object QrNFeCabAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Origin = 'nfecaba.ICMSRec_vBC'
    end
    object QrNFeCabAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Origin = 'nfecaba.IPIRec_vBC'
    end
    object QrNFeCabAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Origin = 'nfecaba.PISRec_vBC'
    end
    object QrNFeCabACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Origin = 'nfecaba.COFINSRec_vBC'
    end
    object QrNFeCabAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNFeCabAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNFeCabAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNFeCabACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNFeCabADataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrNFeCabASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrNFeCabASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrNFeCabASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrNFeCabASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrNFeCabASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrNFeCabASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrNFeCabASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrNFeCabASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrNFeCabASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrNFeCabASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrNFeCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrNFeCabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrNFeCabAprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrNFeCabAretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
    end
    object QrNFeCabAInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeCabAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNFeCabAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNFeCabAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrNFeCabAemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNFeCabAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNFeCabAVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNFeCabABalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrNFeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrNFeCabACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
    end
    object QrNFeCabAOrdemServ: TIntegerField
      FieldName = 'OrdemServ'
    end
    object QrNFeCabAvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFeCabAide_dhEmi: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhEmi'
      Calculated = True
    end
    object QrNFeCabAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrNFeCabAide_dhSaiEntTZD: TFloatField
      FieldName = 'ide_dhSaiEntTZD'
    end
    object QrNFeCabAide_dhSaiEnt: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ide_dhSaiEnt'
      Calculated = True
    end
    object QrNFeCabAide_idDest: TSmallintField
      FieldName = 'ide_idDest'
    end
    object QrNFeCabAide_indFinal: TSmallintField
      FieldName = 'ide_indFinal'
    end
    object QrNFeCabAide_indPres: TSmallintField
      FieldName = 'ide_indPres'
    end
    object QrNFeCabAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
    end
    object QrNFeCabAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrNFeCabAdest_indIEDest: TSmallintField
      FieldName = 'dest_indIEDest'
    end
    object QrNFeCabAdest_IM: TWideStringField
      FieldName = 'dest_IM'
      Size = 15
    end
    object QrNFeCabAICMSTot_vICMSDeson: TFloatField
      FieldName = 'ICMSTot_vICMSDeson'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrNFeCabASituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrNFeCabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrNFeCabAICMSTot_vFCPUFDest: TFloatField
      FieldName = 'ICMSTot_vFCPUFDest'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMSUFDest: TFloatField
      FieldName = 'ICMSTot_vICMSUFDest'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAICMSTot_vICMSUFRemet: TFloatField
      FieldName = 'ICMSTot_vICMSUFRemet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAExporta_XLocDespacho: TWideStringField
      FieldName = 'Exporta_XLocDespacho'
      Size = 60
    end
    object QrNFeCabAICMSTot_vFCP: TFloatField
      FieldName = 'ICMSTot_vFCP'
    end
    object QrNFeCabAICMSTot_vFCPST: TFloatField
      FieldName = 'ICMSTot_vFCPST'
    end
    object QrNFeCabAICMSTot_vFCPSTRet: TFloatField
      FieldName = 'ICMSTot_vFCPSTRet'
    end
    object QrNFeCabAICMSTot_vIPIDevol: TFloatField
      FieldName = 'ICMSTot_vIPIDevol'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 40
    Top = 12
  end
  object QrNFeCabB: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabb nfeb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabBFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabBFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabBEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabBControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabBrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrNFeCabBrefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
    end
    object QrNFeCabBrefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
      DisplayFormat = '0000'
    end
    object QrNFeCabBrefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Size = 14
    end
    object QrNFeCabBrefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
      DisplayFormat = '0'
    end
    object QrNFeCabBrefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabBrefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
      DisplayFormat = '000000000'
    end
    object QrNFeCabBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeCabBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeCabBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeCabBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeCabBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeCabBAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeCabBAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeCabBrefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
    end
    object QrNFeCabBrefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
    end
    object QrNFeCabBrefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Size = 14
    end
    object QrNFeCabBrefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Size = 11
    end
    object QrNFeCabBrefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Size = 14
    end
    object QrNFeCabBrefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
    end
    object QrNFeCabBrefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
    end
    object QrNFeCabBrefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
    end
    object QrNFeCabBrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrNFeCabBrefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Size = 2
    end
    object QrNFeCabBrefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
    end
    object QrNFeCabBrefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
    end
    object QrNFeCabBQualNFref: TSmallintField
      FieldName = 'QualNFref'
    end
  end
  object DsNFeCabB: TDataSource
    DataSet = QrNFeCabB
    Left = 124
    Top = 12
  end
  object QrNFeCabF: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabf nfef'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 156
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabFFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabFEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabFretirada_CNPJ: TWideStringField
      FieldName = 'retirada_CNPJ'
      Size = 14
    end
    object QrNFeCabFretirada_xLgr: TWideStringField
      FieldName = 'retirada_xLgr'
      Size = 60
    end
    object QrNFeCabFretirada_nro: TWideStringField
      FieldName = 'retirada_nro'
      Size = 60
    end
    object QrNFeCabFretirada_xCpl: TWideStringField
      FieldName = 'retirada_xCpl'
      Size = 60
    end
    object QrNFeCabFretirada_xBairro: TWideStringField
      FieldName = 'retirada_xBairro'
      Size = 60
    end
    object QrNFeCabFretirada_cMun: TIntegerField
      FieldName = 'retirada_cMun'
    end
    object QrNFeCabFretirada_xMun: TWideStringField
      FieldName = 'retirada_xMun'
      Size = 60
    end
    object QrNFeCabFretirada_UF: TWideStringField
      FieldName = 'retirada_UF'
      Size = 2
    end
    object QrNFeCabFretirada_CPF: TWideStringField
      FieldName = 'retirada_CPF'
      Size = 11
    end
    object QrNFeCabFretirada_xNome: TWideStringField
      FieldName = 'retirada_xNome'
      Size = 60
    end
    object QrNFeCabFretirada_CEP: TIntegerField
      FieldName = 'retirada_CEP'
    end
    object QrNFeCabFretirada_cPais: TIntegerField
      FieldName = 'retirada_cPais'
    end
    object QrNFeCabFretirada_xPais: TWideStringField
      FieldName = 'retirada_xPais'
      Size = 60
    end
    object QrNFeCabFretirada_fone: TWideStringField
      FieldName = 'retirada_fone'
      Size = 14
    end
    object QrNFeCabFretirada_email: TWideStringField
      FieldName = 'retirada_email'
      Size = 60
    end
    object QrNFeCabFretirada_IE: TWideStringField
      FieldName = 'retirada_IE'
      Size = 14
    end
  end
  object DsNFeCabF: TDataSource
    DataSet = QrNFeCabF
    Left = 184
    Top = 12
  end
  object QrNFeCabG: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfecabg nfeg'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 212
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabGFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabGFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabGEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabGentrega_CNPJ: TWideStringField
      FieldName = 'entrega_CNPJ'
      Size = 14
    end
    object QrNFeCabGentrega_xLgr: TWideStringField
      FieldName = 'entrega_xLgr'
      Size = 60
    end
    object QrNFeCabGentrega_nro: TWideStringField
      FieldName = 'entrega_nro'
      Size = 60
    end
    object QrNFeCabGentrega_xCpl: TWideStringField
      FieldName = 'entrega_xCpl'
      Size = 60
    end
    object QrNFeCabGentrega_xBairro: TWideStringField
      FieldName = 'entrega_xBairro'
      Size = 60
    end
    object QrNFeCabGentrega_cMun: TIntegerField
      FieldName = 'entrega_cMun'
    end
    object QrNFeCabGentrega_xMun: TWideStringField
      FieldName = 'entrega_xMun'
      Size = 60
    end
    object QrNFeCabGentrega_UF: TWideStringField
      FieldName = 'entrega_UF'
      Size = 2
    end
    object QrNFeCabGentrega_CPF: TWideStringField
      FieldName = 'entrega_CPF'
      Size = 11
    end
    object QrNFeCabGentrega_xNome: TWideStringField
      FieldName = 'entrega_xNome'
      Size = 60
    end
    object QrNFeCabGentrega_CEP: TIntegerField
      FieldName = 'entrega_CEP'
    end
    object QrNFeCabGentrega_cPais: TIntegerField
      FieldName = 'entrega_cPais'
    end
    object QrNFeCabGentrega_xPais: TWideStringField
      FieldName = 'entrega_xPais'
      Size = 60
    end
    object QrNFeCabGentrega_fone: TWideStringField
      FieldName = 'entrega_fone'
      Size = 14
    end
    object QrNFeCabGentrega_email: TWideStringField
      FieldName = 'entrega_email'
      Size = 60
    end
    object QrNFeCabGentrega_IE: TWideStringField
      FieldName = 'entrega_IE'
      Size = 14
    end
  end
  object DsNFeCabG: TDataSource
    DataSet = QrNFeCabG
    Left = 240
    Top = 12
  end
  object QrEntiDest: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 740
    Top = 660
    object QrEntiDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiDestNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsEntiDest: TDataSource
    DataSet = QrEntiDest
    Left = 740
    Top = 704
  end
  object QrNFeItsI: TMySQLQuery
    AfterOpen = QrNFeItsIAfterOpen
    BeforeClose = QrNFeItsIBeforeClose
    AfterScroll = QrNFeItsIAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsi nfei'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 492
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeItsIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfeitsi.FatID'
    end
    object QrNFeItsIFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfeitsi.FatNum'
    end
    object QrNFeItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeitsi.Empresa'
    end
    object QrNFeItsInItem: TIntegerField
      FieldName = 'nItem'
      Origin = 'nfeitsi.nItem'
    end
    object QrNFeItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Origin = 'nfeitsi.prod_cProd'
      Size = 60
    end
    object QrNFeItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Origin = 'nfeitsi.prod_cEAN'
      Size = 14
    end
    object QrNFeItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Origin = 'nfeitsi.prod_xProd'
      Size = 120
    end
    object QrNFeItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Origin = 'nfeitsi.prod_NCM'
      Size = 8
    end
    object QrNFeItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Origin = 'nfeitsi.prod_EXTIPI'
      Size = 3
    end
    object QrNFeItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
      Origin = 'nfeitsi.prod_genero'
    end
    object QrNFeItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Origin = 'nfeitsi.prod_CFOP'
    end
    object QrNFeItsIprod_uCom: TWideStringField
      DisplayWidth = 6
      FieldName = 'prod_uCom'
      Origin = 'nfeitsi.prod_uCom'
      Size = 6
    end
    object QrNFeItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Origin = 'nfeitsi.prod_qCom'
      DisplayFormat = '###,###,##0.0000'
    end
    object QrNFeItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Origin = 'nfeitsi.prod_vUnCom'
      DisplayFormat = '###,###,##0.0000000000'
    end
    object QrNFeItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Origin = 'nfeitsi.prod_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Origin = 'nfeitsi.prod_cEANTrib'
      Size = 14
    end
    object QrNFeItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Origin = 'nfeitsi.prod_uTrib'
      Size = 6
    end
    object QrNFeItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Origin = 'nfeitsi.prod_qTrib'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Origin = 'nfeitsi.prod_vUnTrib'
      DisplayFormat = '#,###,##0.0000000000'
    end
    object QrNFeItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Origin = 'nfeitsi.prod_vFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Origin = 'nfeitsi.prod_vSeg'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Origin = 'nfeitsi.prod_vDesc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Origin = 'nfeitsi.Tem_IPI'
    end
    object QrNFeItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Origin = 'nfeitsi.InfAdCuztm'
    end
    object QrNFeItsIEhServico: TIntegerField
      FieldName = 'EhServico'
      Origin = 'nfeitsi.EhServico'
    end
    object QrNFeItsIEhServico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EhServico_TXT'
      Size = 1
      Calculated = True
    end
    object QrNFeItsITem_II: TSmallintField
      FieldName = 'Tem_II'
      Origin = 'nfeitsi.Tem_II'
    end
    object QrNFeItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFeItsIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrNFeItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeItsIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrNFeItsIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrNFeItsIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
    end
    object QrNFeItsIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrNFeItsIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
  end
  object DsNFeItsI: TDataSource
    DataSet = QrNFeItsI
    Left = 520
    Top = 12
  end
  object QrNfeItsIDI: TMySQLQuery
    AfterOpen = QrNfeItsIDIAfterOpen
    BeforeClose = QrNfeItsIDIBeforeClose
    AfterScroll = QrNfeItsIDIAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsidi nfedi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 548
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNfeItsIDIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfeitsidi.FatID'
    end
    object QrNfeItsIDIFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfeitsidi.FatNum'
    end
    object QrNfeItsIDIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeitsidi.Empresa'
    end
    object QrNfeItsIDInItem: TIntegerField
      FieldName = 'nItem'
      Origin = 'nfeitsidi.nItem'
    end
    object QrNfeItsIDIDI_nDI: TWideStringField
      DisplayWidth = 12
      FieldName = 'DI_nDI'
      Origin = 'nfeitsidi.DI_nDI'
      Size = 12
    end
    object QrNfeItsIDIDI_dDI: TDateField
      FieldName = 'DI_dDI'
      Origin = 'nfeitsidi.DI_dDI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNfeItsIDIDI_xLocDesemb: TWideStringField
      FieldName = 'DI_xLocDesemb'
      Origin = 'nfeitsidi.DI_xLocDesemb'
      Size = 60
    end
    object QrNfeItsIDIDI_UFDesemb: TWideStringField
      FieldName = 'DI_UFDesemb'
      Origin = 'nfeitsidi.DI_UFDesemb'
      Size = 2
    end
    object QrNfeItsIDIDI_dDesemb: TDateField
      FieldName = 'DI_dDesemb'
      Origin = 'nfeitsidi.DI_dDesemb'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNfeItsIDIDI_cExportador: TWideStringField
      FieldName = 'DI_cExportador'
      Origin = 'nfeitsidi.DI_cExportador'
      Size = 60
    end
    object QrNfeItsIDIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'nfeitsidi.Controle'
    end
    object QrNfeItsIDILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNfeItsIDIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNfeItsIDIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNfeItsIDIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNfeItsIDIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNfeItsIDIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNfeItsIDIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNfeItsIDIDI_tpViaTransp: TSmallintField
      FieldName = 'DI_tpViaTransp'
    end
    object QrNfeItsIDIDI_vAFRMM: TFloatField
      FieldName = 'DI_vAFRMM'
    end
    object QrNfeItsIDIDI_tpIntermedio: TSmallintField
      FieldName = 'DI_tpIntermedio'
    end
    object QrNfeItsIDIDI_CNPJ: TWideStringField
      FieldName = 'DI_CNPJ'
      Size = 14
    end
    object QrNfeItsIDIDI_UFTerceiro: TWideStringField
      FieldName = 'DI_UFTerceiro'
      Size = 2
    end
  end
  object DsNfeItsIDI: TDataSource
    DataSet = QrNfeItsIDI
    Left = 576
    Top = 12
  end
  object QrNfeItsIDIa: TMySQLQuery
    AfterOpen = QrNfeItsIDIaAfterOpen
    BeforeClose = QrNfeItsIDIaBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsidia nfedia'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      'AND Controle=:P4'
      '')
    Left = 604
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrNfeItsIDIaAdi_nAdicao: TIntegerField
      FieldName = 'Adi_nAdicao'
      Origin = 'nfeitsidia.Adi_nAdicao'
    end
    object QrNfeItsIDIaAdi_nSeqAdic: TIntegerField
      FieldName = 'Adi_nSeqAdic'
      Origin = 'nfeitsidia.Adi_nSeqAdic'
    end
    object QrNfeItsIDIaAdi_cFabricante: TWideStringField
      FieldName = 'Adi_cFabricante'
      Origin = 'nfeitsidia.Adi_cFabricante'
      Size = 60
    end
    object QrNfeItsIDIaAdi_vDescDI: TFloatField
      FieldName = 'Adi_vDescDI'
      Origin = 'nfeitsidia.Adi_vDescDI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNfeItsIDIaConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'nfeitsidia.Conta'
    end
    object QrNfeItsIDIaAdi_nDraw: TFloatField
      FieldName = 'Adi_nDraw'
    end
  end
  object DsNfeItsIDIa: TDataSource
    DataSet = QrNfeItsIDIa
    Left = 632
    Top = 12
  end
  object QrNFeItsN: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsn nfen'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 660
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFeItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNFeItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNFeItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNFeItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrNFeItsNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vICMSOp: TFloatField
      FieldName = 'ICMS_vICMSOp'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_vICMSDif: TFloatField
      FieldName = 'ICMS_vICMSDif'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrNFeItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNICMS_vBCFCP: TFloatField
      FieldName = 'ICMS_vBCFCP'
    end
    object QrNFeItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pFCP: TFloatField
      FieldName = 'ICMS_pFCP'
    end
    object QrNFeItsNICMS_vFCP: TFloatField
      FieldName = 'ICMS_vFCP'
    end
    object QrNFeItsNICMS_pST: TFloatField
      FieldName = 'ICMS_pST'
    end
    object QrNFeItsNICMS_vBCFCPSTRet: TFloatField
      FieldName = 'ICMS_vBCFCPSTRet'
    end
    object QrNFeItsNICMS_pFCPSTRet: TFloatField
      FieldName = 'ICMS_pFCPSTRet'
    end
    object QrNFeItsNICMS_vFCPSTRet: TFloatField
      FieldName = 'ICMS_vFCPSTRet'
    end
    object QrNFeItsNICMS_vBCFCPST: TFloatField
      FieldName = 'ICMS_vBCFCPST'
    end
    object QrNFeItsNICMS_pFCPST: TFloatField
      FieldName = 'ICMS_pFCPST'
    end
    object QrNFeItsNICMS_vFCPST: TFloatField
      FieldName = 'ICMS_vFCPST'
    end
  end
  object DsNFeItsN: TDataSource
    DataSet = QrNFeItsN
    Left = 688
    Top = 12
  end
  object QrNFeItsO: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitso nfeo'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 716
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrNFeItsOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrNFeItsOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrNFeItsOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrNFeItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrNFeItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrNFeItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNFeItsO: TDataSource
    DataSet = QrNFeItsO
    Left = 744
    Top = 12
  end
  object QrNFeItsP: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsp nfep'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 764
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsPII_vBC: TFloatField
      FieldName = 'II_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vDespAdu: TFloatField
      FieldName = 'II_vDespAdu'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vII: TFloatField
      FieldName = 'II_vII'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vIOF: TFloatField
      FieldName = 'II_vIOF'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNFeItsP: TDataSource
    DataSet = QrNFeItsP
    Left = 796
    Top = 12
  end
  object QrNFeItsQ: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsq nfeq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 824
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrNFeItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrNFeItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,###,##0.0000'
    end
  end
  object DsNFeItsQ: TDataSource
    DataSet = QrNFeItsQ
    Left = 856
    Top = 12
  end
  object QrNFeItsS: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitss nfes'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 884
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrNFeItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNFeItsS: TDataSource
    DataSet = QrNFeItsS
    Left = 912
    Top = 12
  end
  object QrNFeItsU: TMySQLQuery
    SQL.Strings = (
      'SELECT muni.Nome NO_ISSQN_cMunFG, '
      'cnae.Nome  NO_ISSQN_cListServ, nfeu.*'
      'FROM nfeitsu nfeu'
      'LEFT JOIN dtb_munici muni ON muni.Codigo=nfeu.ISSQN_cMunFG'
      'LEFT JOIN cnae21Cad cnae ON cnae.CodAlf=nfeu.ISSQN_cListServ'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 940
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsUISSQN_vBC: TFloatField
      FieldName = 'ISSQN_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsUISSQN_vAliq: TFloatField
      FieldName = 'ISSQN_vAliq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsUISSQN_vISSQN: TFloatField
      FieldName = 'ISSQN_vISSQN'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsUISSQN_cMunFG: TIntegerField
      FieldName = 'ISSQN_cMunFG'
    end
    object QrNFeItsUNO_ISSQN_cMunFG: TWideStringField
      FieldName = 'NO_ISSQN_cMunFG'
      Size = 100
    end
    object QrNFeItsUNO_ISSQN_cListServ: TWideStringField
      FieldName = 'NO_ISSQN_cListServ'
      Size = 255
    end
    object QrNFeItsUISSQN_cListServ: TWideStringField
      FieldName = 'ISSQN_cListServ'
      Size = 5
    end
  end
  object QrNFeItsV: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsv nfev'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 936
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrNFeCabY: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfecaby nfey'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY dVenc, nDup')
    Left = 936
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabYControle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000'
    end
    object QrNFeCabYnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrNFeCabYdVenc: TDateField
      FieldName = 'dVenc'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeCabYvDup: TFloatField
      FieldName = 'vDup'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabYFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabYFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabYEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabYSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrNFeCabYLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrNFeCabYFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrNFeCabYGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrNFeCabYDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object PMNFeCabA: TPopupMenu
    OnPopup = PMNFeCabAPopup
    Left = 896
    Top = 652
    object IncluinovaNFe1: TMenuItem
      Caption = '&Inclui nova NF-e'
      OnClick = IncluinovaNFe1Click
    end
    object AlteraNFeatual1: TMenuItem
      Caption = '&Altera NF-e atual'
      OnClick = AlteraNFeatual1Click
    end
    object ExcluiNFeatual1: TMenuItem
      Caption = '&Exclui NF-e atual'
      OnClick = ExcluiNFeatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object LocaldeRetirada1: TMenuItem
      Caption = 'Local de &Retirada'
      object Incluialtera1: TMenuItem
        Caption = '&Inclui / altera'
        OnClick = Incluialtera1Click
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui1Click
      end
    end
    object LocaldeEntrega1: TMenuItem
      Caption = 'Local de E&ntrega'
      object IncluiAltera2: TMenuItem
        Caption = '&Inclui / Altera'
        OnClick = IncluiAltera2Click
      end
      object Exclui2: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui2Click
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ReGeraarquivoXML1: TMenuItem
      Caption = 'Re&Gera arquivo XML'
      OnClick = ReGeraarquivoXML1Click
    end
  end
  object VuFisRegCad: TdmkValUsu
    dmkEditCB = EdRegrFiscal
    Panel = PainelEdita
    QryCampo = 'FisRegCad'
    UpdCampo = 'FisRegCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 900
    Top = 68
  end
  object VuTabelaPrc: TdmkValUsu
    dmkEditCB = EdTabelaPrc
    Panel = PainelEdita
    QryCampo = 'TabelaPrc'
    UpdCampo = 'TabelaPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 872
    Top = 68
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PainelEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 844
    Top = 68
  end
  object PMNFeCabB: TPopupMenu
    OnPopup = PMNFeCabBPopup
    Left = 984
    Top = 652
    object IncluiNFreferenciada1: TMenuItem
      Caption = '&Inclui NF referenciada'
      OnClick = IncluiNFreferenciada1Click
    end
    object Alterarefenciamentoatual1: TMenuItem
      Caption = '&Altera refenciamento atual'
      OnClick = Alterarefenciamentoatual1Click
    end
    object Excluireferenciamentos1: TMenuItem
      Caption = '&Exclui referenciamento(s)'
      OnClick = Excluireferenciamentos1Click
    end
  end
  object PMNFeItsI_U: TPopupMenu
    OnPopup = PMNFeItsI_UPopup
    Left = 1080
    Top = 652
    object Incluinovoproduto1: TMenuItem
      Caption = '&Inclui novo &Produto'
      OnClick = Incluinovoproduto1Click
    end
    object Alteraprodutoatual1: TMenuItem
      Caption = '&1. Altera produto atual'
      OnClick = Alteraprodutoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object IncluinovoServico1: TMenuItem
      Caption = 'Inclui novo &Servi'#231'o'
      OnClick = IncluinovoServico1Click
    end
    object Alteraservicoatual1: TMenuItem
      Caption = '&2. Altera servi'#231'o atual '
      OnClick = Alteraservicoatual1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Excluiprodutoatual1: TMenuItem
      Caption = '&Exclui o item selecionado'
      OnClick = Excluiprodutoatual1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object AlteraTributostotaisaproximados1: TMenuItem
      Caption = '&Altera Tributos totais aproximados'
      OnClick = AlteraTributostotaisaproximados1Click
    end
  end
  object PMNFeCabY: TPopupMenu
    OnPopup = PMNFeCabYPopup
    Left = 1168
    Top = 652
    object Incluinovacobrana1: TMenuItem
      Caption = '&Inclui nova cobran'#231'a'
      OnClick = Incluinovacobrana1Click
    end
    object Alteracobranaatual1: TMenuItem
      Caption = '&Altera cobran'#231'a atual'
      OnClick = Alteracobranaatual1Click
    end
    object Exclui3: TMenuItem
      Caption = '&Exclui'
      object Excluicobranaatual1: TMenuItem
        Caption = '&Exclui TODA cobran'#231'a atual'
        OnClick = Excluicobranaatual1Click
      end
      object Excluiitematualnoexcluifinanceiro1: TMenuItem
        Caption = 'Exclui item atual (n'#227'o exclui financeiro)'
        OnClick = Excluiitematualnoexcluifinanceiro1Click
      end
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Gerenciar1: TMenuItem
      Caption = '&Gerenciar'
      OnClick = Gerenciar1Click
    end
  end
  object DsNFeItsV: TDataSource
    DataSet = QrNFeItsV
    Left = 964
    Top = 40
  end
  object DsNFeCabY: TDataSource
    DataSet = QrNFeCabY
    Left = 964
    Top = 68
  end
  object DsEntiEmit: TDataSource
    DataSet = QrEntiEmit
    Left = 804
    Top = 704
  end
  object QrEntiEmit: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 804
    Top = 660
    object QrEntiEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiEmitNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object QrNFeItsR: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsr nfer'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 824
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrNFeItsRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrNFeItsRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrNFeItsRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrNFeItsRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrNFeItsRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
  end
  object DsNFeItsR: TDataSource
    DataSet = QrNFeItsR
    Left = 856
    Top = 40
  end
  object QrNFeItsT: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitst nfet'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 884
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrNFeItsTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrNFeItsTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrNFeItsTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrNFeItsTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrNFeItsTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
  end
  object DsNFeItsT: TDataSource
    DataSet = QrNFeItsT
    Left = 912
    Top = 40
  end
  object QrNFeItsINVE: TMySQLQuery
    AfterOpen = QrNFeItsINVEAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsinve'
      'WHERE FatID=:P0'
      'AND FAtNum=:P1'
      'AND EMpresa=:P2'
      'AND nItem=:P3')
    Left = 184
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsINVEFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsINVEFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsINVEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsINVEnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsINVEControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeItsINVEImportacia: TIntegerField
      FieldName = 'Importacia'
    end
    object QrNFeItsINVENVE: TWideStringField
      FieldName = 'NVE'
      Size = 6
    end
    object QrNFeItsINVELk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsINVEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsINVEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsINVEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsINVEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsINVEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsINVEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeItsINVE: TDataSource
    DataSet = QrNFeItsINVE
    Left = 184
    Top = 620
  end
  object QrNFeItsI03: TMySQLQuery
    AfterOpen = QrNFeItsI03AfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi03'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 256
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsI03FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsI03FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsI03Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsI03nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsI03Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeItsI03detExport_nDraw: TLargeintField
      FieldName = 'detExport_nDraw'
    end
    object QrNFeItsI03detExport_nRE: TLargeintField
      FieldName = 'detExport_nRE'
    end
    object QrNFeItsI03detExport_chNFe: TWideStringField
      FieldName = 'detExport_chNFe'
      Required = True
      Size = 44
    end
    object QrNFeItsI03detExport_qExport: TFloatField
      FieldName = 'detExport_qExport'
    end
    object QrNFeItsI03Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsI03DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsI03DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsI03UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsI03UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsI03AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsI03Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeItsI03: TDataSource
    DataSet = QrNFeItsI03
    Left = 256
    Top = 620
  end
  object QrNFeCabGA: TMySQLQuery
    OnCalcFields = QrNFeCabGACalcFields
    SQL.Strings = (
      'SELECT cga.*, '
      'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,'
      'IF(cga.Tipo=0, autXML_CNPJ, autXML_CPF) CNPJ_CPF, '
      'ELT(cga.AddForma + 1, "a", "b", "c", "d", "e") NO_AddForma'
      'FROM nfecabga cga '
      'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad')
    Left = 328
    Top = 572
    object QrNFeCabGAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabGAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabGAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabGAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabGAAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrNFeCabGAAddIDCad: TSmallintField
      FieldName = 'AddIDCad'
    end
    object QrNFeCabGATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrNFeCabGAautXML_CNPJ: TWideStringField
      FieldName = 'autXML_CNPJ'
      Size = 14
    end
    object QrNFeCabGAautXML_CPF: TWideStringField
      FieldName = 'autXML_CPF'
      Size = 11
    end
    object QrNFeCabGALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeCabGADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeCabGADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeCabGAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeCabGAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeCabGAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeCabGAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeCabGACNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrNFeCabGAITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrNFeCabGATpDOC: TWideStringField
      FieldName = 'TpDOC'
    end
    object QrNFeCabGANO_AddForma: TWideStringField
      FieldName = 'NO_AddForma'
      Size = 1
    end
    object QrNFeCabGANO_ENT_ECO: TWideStringField
      FieldName = 'NO_ENT_ECO'
      Size = 100
    end
  end
  object DsNFeCabGA: TDataSource
    DataSet = QrNFeCabGA
    Left = 328
    Top = 620
  end
  object QrNFeItsM: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 392
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsMFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsMEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsMnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsMiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrNFeItsMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
    object QrNFeItsMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsMAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsMAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeItsMpTotTrib: TFloatField
      FieldName = 'pTotTrib'
    end
    object QrNFeItsMtabTotTrib: TSmallintField
      FieldName = 'tabTotTrib'
    end
    object QrNFeItsMverTotTrib: TWideStringField
      FieldName = 'verTotTrib'
      Required = True
      Size = 30
    end
    object QrNFeItsMtpAliqTotTrib: TSmallintField
      FieldName = 'tpAliqTotTrib'
    end
    object QrNFeItsMfontTotTrib: TSmallintField
      FieldName = 'fontTotTrib'
    end
  end
  object DsNFeItsM: TDataSource
    DataSet = QrNFeItsM
    Left = 392
    Top = 620
  end
  object QrNF_X: TMySQLQuery
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2')
    Left = 664
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TMySQLQuery
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 712
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TMySQLQuery
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 764
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TMySQLQuery
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 812
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TMySQLQuery
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1 '
      'AND Empresa=:P2')
    Left = 868
    Top = 572
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrNFeItsNA: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsna'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 44
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsNAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsNAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsNAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsNAnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsNAICMSUFDest_vBCUFDest: TFloatField
      FieldName = 'ICMSUFDest_vBCUFDest'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsNAICMSUFDest_pFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_pFCPUFDest'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNAICMSUFDest_pICMSUFDest: TFloatField
      FieldName = 'ICMSUFDest_pICMSUFDest'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNAICMSUFDest_pICMSInter: TFloatField
      FieldName = 'ICMSUFDest_pICMSInter'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNAICMSUFDest_pICMSInterPart: TFloatField
      FieldName = 'ICMSUFDest_pICMSInterPart'
      DisplayFormat = '0.0000'
    end
    object QrNFeItsNAICMSUFDest_vFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_vFCPUFDest'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsNAICMSUFDest_vICMSUFDest: TFloatField
      FieldName = 'ICMSUFDest_vICMSUFDest'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsNAICMSUFDest_vICMSUFRemet: TFloatField
      FieldName = 'ICMSUFDest_vICMSUFRemet'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsNAICMSUFDest_vBCFCPUFDest: TFloatField
      FieldName = 'ICMSUFDest_vBCFCPUFDest'
    end
  end
  object DsNFeItsNA: TDataSource
    DataSet = QrNFeItsNA
    Left = 44
    Top = 624
  end
  object QrDTB_UFs: TMySQLQuery
    SQL.Strings = (
      'SELECT DTB, Nome'
      'FROM ufs'
      'ORDER BY Nome')
    Left = 116
    Top = 576
    object QrDTB_UFsDTB: TWideStringField
      FieldName = 'DTB'
      Size = 2
    end
    object QrDTB_UFsNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
  end
  object DsDTB_UFs: TDataSource
    DataSet = QrDTB_UFs
    Left = 116
    Top = 624
  end
  object QrNFeCabXReb: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxreb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 504
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabXRebplaca: TWideStringField
      FieldName = 'placa'
      Size = 8
    end
    object QrNFeCabXRebUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNFeCabXRebRNTC: TWideStringField
      FieldName = 'RNTC'
    end
    object QrNFeCabXRebFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabXRebFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabXRebEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabXRebControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrNFeCabXLac: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxlac'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3')
    Left = 588
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabXLacnLacre: TWideStringField
      FieldName = 'nLacre'
      Size = 60
    end
    object QrNFeCabXLacFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabXLacFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabXLacEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabXLacControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabXLacConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrNFeCabXVol: TMySQLQuery
    BeforeClose = QrNFeCabXVolBeforeClose
    AfterScroll = QrNFeCabXVolAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 656
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrNFeCabXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFeCabXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFeCabXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFeCabXVolpesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrNFeCabXVolpesoB: TFloatField
      FieldName = 'pesoB'
    end
    object QrNFeCabXVolControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabXVolFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabXVolFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabXVolEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabXVolSeqInArq: TIntegerField
      FieldName = 'SeqInArq'
    end
  end
  object DsNFeCabXReb: TDataSource
    DataSet = QrNFeCabXReb
    Left = 504
    Top = 704
  end
  object DsNFeCabXLac: TDataSource
    DataSet = QrNFeCabXLac
    Left = 588
    Top = 704
  end
  object DsNFeCabXVol: TDataSource
    DataSet = QrNFeCabXVol
    Left = 660
    Top = 704
  end
  object PMNFeCabXReb: TPopupMenu
    OnPopup = PMNFeCabXRebPopup
    Left = 1118
    Top = 32
    object IncluiReboque1: TMenuItem
      Caption = '&Inclui Reboque'
      OnClick = IncluiReboque1Click
    end
    object AlteraReboque1: TMenuItem
      Caption = '&Altera Reboque'
      OnClick = AlteraReboque1Click
    end
    object ExcluiReboque1: TMenuItem
      Caption = '&Exclui Reboque'
      OnClick = ExcluiReboque1Click
    end
  end
  object PMNFeCabXVol: TPopupMenu
    OnPopup = PMNFeCabXVolPopup
    Left = 1110
    Top = 80
    object IncluiVolume1: TMenuItem
      Caption = '&Inclui Volume'
      OnClick = IncluiVolume1Click
    end
    object AlteraVolume1: TMenuItem
      Caption = '&Altera Volume'
      OnClick = AlteraVolume1Click
    end
    object ExcluiVolume1: TMenuItem
      Caption = '&Exclui Volume'
      OnClick = ExcluiVolume1Click
    end
  end
  object PMNFeCabXLac: TPopupMenu
    OnPopup = PMNFeCabXVolPopup
    Left = 1110
    Top = 128
    object IncluiLacre1: TMenuItem
      Caption = '&Inclui Lacre'
      OnClick = IncluiLacre1Click
    end
    object AlteraLacre1: TMenuItem
      Caption = '&Altera Lacre'
      OnClick = AlteraLacre1Click
    end
    object ExcluiLacre1: TMenuItem
      Caption = '&Exclui Lacre'
      OnClick = ExcluiLacre1Click
    end
  end
  object QrNFeCabZFis: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 332
    Top = 664
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZFisFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZFisFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZFisEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZFisControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZFisxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeCabZFisxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object DsNFeCabZFis: TDataSource
    DataSet = QrNFeCabZFis
    Left = 332
    Top = 712
  end
  object PmNFeCabZCon: TPopupMenu
    OnPopup = PmNFeCabZConPopup
    Left = 536
    Top = 460
    object IncluiCampoLivre1: TMenuItem
      Caption = '&Inclui Campo Livre'
      OnClick = IncluiCampoLivre1Click
    end
    object AlteraCampoLivre1: TMenuItem
      Caption = '&Altera Campo Livre'
      OnClick = AlteraCampoLivre1Click
    end
    object ExcluiCampoLivre1: TMenuItem
      Caption = '&Exclui Campo Livre'
      OnClick = ExcluiCampoLivre1Click
    end
  end
  object PmNFeCabZFis: TPopupMenu
    OnPopup = PmNFeCabZFisPopup
    Left = 536
    Top = 508
    object IncluiCampoLivre2: TMenuItem
      Caption = '&Inclui Campo Livre'
      OnClick = IncluiCampoLivre2Click
    end
    object AlteraCampoLivre2: TMenuItem
      Caption = '&Altera Campo Livre'
      OnClick = AlteraCampoLivre2Click
    end
    object ExcluiCampoLivre2: TMenuItem
      Caption = '&Exclui Campo Livre'
      OnClick = ExcluiCampoLivre2Click
    end
  end
  object PmNFeCabZPro: TPopupMenu
    OnPopup = PmNFeCabZProPopup
    Left = 536
    Top = 560
    object IncluiProcessoouatoconcessrio1: TMenuItem
      Caption = '&Inclui Processo ou ato concess'#243'rio'
      OnClick = IncluiProcessoouatoconcessrio1Click
    end
    object AlteraProcessoouatoconcessrio1: TMenuItem
      Caption = '&Altera Processo ou ato concess'#243'rio'
      OnClick = AlteraProcessoouatoconcessrio1Click
    end
    object ExcluiProcessoouatoconcessrio1: TMenuItem
      Caption = '&Exclui Processo ou ato concess'#243'rio'
      OnClick = ExcluiProcessoouatoconcessrio1Click
    end
  end
  object QrNFeCabZCon: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 248
    Top = 664
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZConFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZConFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZConEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZConControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZConxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeCabZConxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object DsNFeCabZCon: TDataSource
    DataSet = QrNFeCabZCon
    Left = 248
    Top = 712
  end
  object QrNFeCabZPro: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabzcon'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 416
    Top = 664
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabZProFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabZProFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabZProEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabZProControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabZPronProc: TWideStringField
      FieldName = 'nProc'
      Size = 60
    end
    object QrNFeCabZProindProc: TSmallintField
      FieldName = 'indProc'
    end
  end
  object DsNFeCabZPro: TDataSource
    DataSet = QrNFeCabZPro
    Left = 416
    Top = 712
  end
  object QrSUMNFeCabY: TMySQLQuery
    Left = 1112
    Top = 436
    object QrSUMNFeCabYvDup: TFloatField
      FieldName = 'vDup'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrTransportas: TMySQLQuery
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'#13
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Fornece2="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 952
    Top = 444
    object QrTransportasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportasNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrTransportasCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportasNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
  end
  object DsTransportas: TDataSource
    DataSet = QrTransportas
    Left = 981
    Top = 444
  end
  object QrNFeCabYA: TMySQLQuery
    OnCalcFields = QrNFeCabYACalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM nfecaby nfey'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY dVenc, nDup')
    Left = 992
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabYAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabYAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabYAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabYAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCabYAtPag: TSmallintField
      FieldName = 'tPag'
    end
    object QrNFeCabYAvPag: TFloatField
      FieldName = 'vPag'
    end
    object QrNFeCabYAtpIntegra: TSmallintField
      FieldName = 'tpIntegra'
    end
    object QrNFeCabYACNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFeCabYAtBand: TSmallintField
      FieldName = 'tBand'
    end
    object QrNFeCabYAcAut: TWideStringField
      FieldName = 'cAut'
    end
    object QrNFeCabYAvTroco: TFloatField
      FieldName = 'vTroco'
    end
    object QrNFeCabYAtPag_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tPag_TXT'
      Size = 100
      Calculated = True
    end
    object QrNFeCabYAtpIntegra_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tpIntegra_TXT'
      Size = 100
      Calculated = True
    end
    object QrNFeCabYAtBand_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tBand_TXT'
      Size = 100
      Calculated = True
    end
    object QrNFeCabYACNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 18
      Calculated = True
    end
  end
  object DsNFeCabYA: TDataSource
    DataSet = QrNFeCabYA
    Left = 1020
    Top = 68
  end
end
