unit NFeCabB_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup,
  dmkCheckBox;

type
  TFmNFeCabB_0000 = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label2: TLabel;
    EdUF: TdmkEditCB;
    CBUF: TdmkDBLookupComboBox;
    EdAAMM: TdmkEdit;
    Label4: TLabel;
    Label3: TLabel;
    EdCNPJ: TdmkEdit;
    EdmodNF: TdmkEdit;
    Label16: TLabel;
    Label1: TLabel;
    EdSerie: TdmkEdit;
    EdnNF: TdmkEdit;
    Label10: TLabel;
    Panel4: TPanel;
    EdrefNFe: TdmkEdit;
    Label15: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    SpeedButton1: TSpeedButton;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel2: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    EdrefNFP_cUF: TdmkEditCB;
    CBrefNFP_cUF: TdmkDBLookupComboBox;
    EdrefNFP_AAMM: TdmkEdit;
    EdrefNFP_CNPJ: TdmkEdit;
    EdrefNFP_mod: TdmkEdit;
    EdrefNFP_serie: TdmkEdit;
    EdrefNFP_nNF: TdmkEdit;
    EdrefNFP_CPF: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    EdrefNFP_IE: TdmkEdit;
    Label14: TLabel;
    EdrefCTe: TdmkEdit;
    Label17: TLabel;
    Panel7: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    EdrefECF_mod: TdmkEdit;
    EdrefECF_nECF: TdmkEdit;
    EdrefECF_nCOO: TdmkEdit;
    RGQualNFref: TdmkRadioGroup;
    DsDTB_UFs1: TDataSource;
    QrDTB_UFs1: TmySQLQuery;
    QrDTB_UFs1DTB: TWideStringField;
    QrDTB_UFs1Nome: TWideStringField;
    DsDTB_UFs2: TDataSource;
    QrDTB_UFs2: TmySQLQuery;
    QrDTB_UFs2DTB: TWideStringField;
    QrDTB_UFs2Nome: TWideStringField;
    CkSigilo: TdmkCheckBox;
    PnSigilo: TPanel;
    Label18: TLabel;
    EdrefNFeSig: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdrefNFeExit(Sender: TObject);
    procedure EdAAMMExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure RGQualNFrefClick(Sender: TObject);
    procedure CkSigiloClick(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaChaveSigilosa();
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabB_0000: TFmNFeCabB_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral, (*NFeCabA_0000,*)
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF, UnXXe_PF;

{$R *.DFM}

procedure TFmNFeCabB_0000.BtOKClick(Sender: TObject);
var
  refNFe, refNFeSig, refNF_CNPJ, refNFP_CNPJ, refNFP_CPF, refNFP_IE, refCTe,
  refECF_mod, refNF_cUF, refNFP_cUF: String;
  FatID, FatNum, Empresa, Controle, refNF_AAMM, refNF_mod,
  refNF_serie, refNF_nNF, refNFP_AAMM, refNFP_mod, refNFP_serie,
  refNFP_nNF, refECF_nECF, refECF_nCOO, QualNFref: Integer;
begin
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  //
  refNFe         := '';
  refNFeSig      := '';
  //
  refNF_cUF      := '0';
  refNF_AAMM     := 0;
  refNF_CNPJ     := '';
  refNF_mod      := 0;
  refNF_serie    := 0;
  refNF_nNF      := 0;
  //
  refNFP_cUF     := '0';
  refNFP_AAMM    := 0;
  refNFP_CNPJ    := '';
  refNFP_CPF     := '';
  refNFP_IE      := '';
  refNFP_mod     := 0;
  refNFP_serie   := 0;
  refNFP_nNF     := 0;
  refCTe         := '';
  //
  refECF_mod     := '';
  refECF_nECF    := 0;
  refECF_nCOO    := 0;
  //
  QualNFref      := RGQualNFref.ItemIndex;
  case QualNFref of
    0:
    begin
      refNFe         := EdrefNFe.Text;
      if CkSigilo.Checked then
        refNFeSig      := EdrefNFeSig.Text;
    end;
    1:
    begin
      refNF_cUF      := EdUF.ValueVariant;
      refNF_AAMM     := EdAAMM.ValueVariant;
      refNF_CNPJ     := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
      refNF_mod      := EdmodNF.ValueVariant;
      refNF_serie    := EdSerie.ValueVariant;
      refNF_nNF      := EdnNF.ValueVariant;
    end;
    2:
    begin
      refNFP_cUF     := EdrefNFP_cUF.ValueVariant;
      refNFP_AAMM    := EdrefNFP_AAMM.ValueVariant;
      refNFP_CNPJ    := Geral.SoNumero_TT(EdrefNFP_CNPJ.ValueVariant);
      refNFP_CPF     := Geral.SoNumero_TT(EdrefNFP_CPF.ValueVariant);
      refNFP_IE      := Geral.SoNumero_TT(EdrefNFP_IE.ValueVariant);
      refNFP_mod     := EdrefNFP_mod.ValueVariant;
      refNFP_serie   := EdrefNFP_serie.ValueVariant;
      refNFP_nNF     := EdrefNFP_nNF.ValueVariant;
      refCTe         := EdrefCTe.ValueVariant;
      //
      if Length(refCTe) <> 44 then
        Geral.MB_Aviso('CUIDADO!! "Chave CT-e" n�o est� com 44 n�meros!"');
    end;
    3:
    begin
      refECF_mod     := EdrefECF_mod.ValueVariant;
      refECF_nECF    := EdrefECF_nECF.ValueVariant;
      refECF_nCOO    := EdrefECF_nCOO.ValueVariant;
    end;
  end;
  //
  if ImgTipo.SQLType = stIns then
    Controle := DModG.BuscaProximoInteiro('nfecabb', 'Controle', '', 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecabb', False, [
  'refNFe',
  'refNFeSig', // 2023-06-09
  'refNF_cUF', 'refNF_AAMM',
  'refNF_CNPJ', 'refNF_mod', 'refNF_serie',
  'refNF_nNF', 'refNFP_cUF', 'refNFP_AAMM',
  'refNFP_CNPJ', 'refNFP_CPF', 'refNFP_IE',
  'refNFP_mod', 'refNFP_serie', 'refNFP_nNF',
  'refCTe', 'refECF_mod', 'refECF_nECF',
  'refECF_nCOO', 'QualNFref'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  refNFe,
  refNFeSig, // 2023-06-09
  refNF_cUF, refNF_AAMM,
  refNF_CNPJ, refNF_mod, refNF_serie,
  refNF_nNF, refNFP_cUF, refNFP_AAMM,
  refNFP_CNPJ, refNFP_CPF, refNFP_IE,
  refNFP_mod, refNFP_serie, refNFP_nNF,
  refCTe, refECF_mod, refECF_nECF,
  refECF_nCOO, QualNFref], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    //FmNFeCabA_0000.ReopenNFeCabB(Controle);
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    Geral.MB_Info('NF referenciada inclu�da com sucesso!');
    ImgTipo.SQLType        := stIns;
    case PageControl1.ActivePageIndex of
      0:
      begin
        EdrefNFe.ValueVariant := '';
        EdrefNFeSig.ValueVariant := '';
      end;
      1: EdnNF.ValueVariant := 0;
    end;
  end;
end;

procedure TFmNFeCabB_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabB_0000.CkSigiloClick(Sender: TObject);
begin
  PnSigilo.Visible := CkSigilo.Checked;
  VerificaChaveSigilosa();
end;

procedure TFmNFeCabB_0000.RGQualNFrefClick(Sender: TObject);
begin
  PageControl1.Visible := False;
  TabSheet1.TabVisible := True;
  TabSheet2.TabVisible := True;
  TabSheet3.TabVisible := True;
  TabSheet4.TabVisible := True;
  PageControl1.ActivePageIndex := RGQualNFref.ItemIndex;
  //
  PageControl1.Visible := True;
  TabSheet1.TabVisible := PageControl1.ActivePageIndex = 0;
  TabSheet2.TabVisible := PageControl1.ActivePageIndex = 1;
  TabSheet3.TabVisible := PageControl1.ActivePageIndex = 2;
  TabSheet4.TabVisible := PageControl1.ActivePageIndex = 3;
  //
end;

procedure TFmNFeCabB_0000.EdAAMMExit(Sender: TObject);
var
  T, Ano, Mes: Integer;
  Teste: TDateTime;
  Erro: Boolean;
  AAMM: String;
begin
  Erro := False;
  AAMM := EdAAMM.Text;
  T := Length(AAMM);
  if T = 3 then
    AAMM := '0' + AAMM;
  if T <> 4 then
  begin
    Erro := True;
  end else begin
    Ano := Geral.IMV(Copy(EdAAMM.Text, 1, 2));
    Mes := Geral.IMV(Copy(EdAAMM.Text, 3));
    try
      Teste := EncodeDate(2000 + Ano, Mes, 1);
        if Teste <> 0 then
        begin
          AAMM := Copy(FormatFloat('0000', Ano), 3, 2) + FormatFloat('00', Mes);
          EdAAMM.Text := AAMM;
        end;
    except
      Erro := True;
    end;
  end;
  if Erro then
  begin
    Geral.MensagemBox('Ano/m�s inv�lido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdAAMM.SetFocus;
  end;
end;

procedure TFmNFeCabB_0000.EdrefNFeExit(Sender: TObject);
begin
  if not DmNFe_0000.VerificaChavedeAcesso(EdrefNFe.Text, True) then
    EdrefNFe.SetFocus;
  VerificaChaveSigilosa();
end;

procedure TFmNFeCabB_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabB_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TabSheet1.Visible := False;
  TabSheet2.Visible := False;
  TabSheet3.Visible := False;
  TabSheet4.Visible := False;
  UMyMod.AbreQuery(QrDTB_UFs1, Dmod.MyDB);
  UMyMod.AbreQuery(QrDTB_UFs2, Dmod.MyDB);
end;

procedure TFmNFeCabB_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabB_0000.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTROX := '';
  //
  UnNFe_PF.MostraFormNFePesq(False, nil, nil, 0);
  //
  EdrefNFe.ValueVariant := VAR_CADASTROX;
end;

procedure TFmNFeCabB_0000.VerificaChaveSigilosa;
var
  ChaveAtual, ChaveNova: String;
begin
  ChaveAtual := EdrefNFe.Text;
  if XXe_PF.ZeraCodSegChaveDeAcesso(ChaveAtual, ChaveNova) then
  begin
    EdrefNFeSig.ValueVariant := ChaveNova;
  end;
end;

end.
