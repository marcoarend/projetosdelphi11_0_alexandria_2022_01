object FmNFeItsVA_0000: TFmNFeItsVA_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-022 :: NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
  ClientHeight = 386
  ClientWidth = 705
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 705
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 757
    object GB_R: TGroupBox
      Left = 657
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 709
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 609
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 661
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 272
    Width = 705
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 506
    ExplicitWidth = 757
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 701
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 753
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 316
    Width = 705
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 550
    ExplicitWidth = 757
    object PnSaiDesis: TPanel
      Left = 559
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 611
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 557
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 609
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 705
    Height = 65
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 3
    ExplicitWidth = 757
    object Label5: TLabel
      Left = 168
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Item:'
      FocusControl = EdnItem
    end
    object Label3: TLabel
      Left = 224
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label4: TLabel
      Left = 304
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = EdFatID
    end
    object Label10: TLabel
      Left = 40
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = EdFatNum
    end
    object Label13: TLabel
      Left = 120
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = EdEmpresa
    end
    object EdnItem: TdmkEdit
      Left = 168
      Top = 32
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFatID: TdmkEdit
      Left = 8
      Top = 32
      Width = 28
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'FatID'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFatNum: TdmkEdit
      Left = 40
      Top = 32
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'FatNum'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEmpresa: TdmkEdit
      Left = 120
      Top = 32
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Edprod_cProd: TdmkEdit
      Left = 224
      Top = 32
      Width = 77
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Edprod_xProd: TdmkEdit
      Left = 304
      Top = 32
      Width = 389
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 113
    Width = 705
    Height = 159
    Align = alClient
    Caption = ' Grupo VA. Observa'#231#245'es de uso livre: '
    TabOrder = 4
    ExplicitLeft = 12
    ExplicitTop = 48
    ExplicitWidth = 501
    ExplicitHeight = 157
    object GroupBox3: TGroupBox
      Left = 2
      Top = 15
      Width = 701
      Height = 70
      Align = alTop
      Caption = 'VA02. Observa'#231#245'es para uso livre do contribuinte: '
      TabOrder = 0
      ExplicitTop = 19
      ExplicitWidth = 497
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 114
        Height = 13
        Caption = 'Identifica'#231#227'o do campo:'
      end
      object Label14: TLabel
        Left = 132
        Top = 20
        Width = 99
        Height = 13
        Caption = 'Conte'#250'do do campo:'
      end
      object EdobsCont_xCampo: TdmkEdit
        Left = 8
        Top = 36
        Width = 120
        Height = 21
        MaxLength = 20
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdobsCont_xTexto: TdmkEdit
        Left = 132
        Top = 36
        Width = 360
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 85
      Width = 701
      Height = 66
      Align = alTop
      Caption = 'VA02. Observa'#231#245'es para uso livre do fisco: '
      TabOrder = 1
      ExplicitTop = 89
      ExplicitWidth = 497
      object Label15: TLabel
        Left = 8
        Top = 20
        Width = 114
        Height = 13
        Caption = 'Identifica'#231#227'o do campo:'
      end
      object Label16: TLabel
        Left = 132
        Top = 20
        Width = 99
        Height = 13
        Caption = 'Conte'#250'do do campo:'
      end
      object EdobsFisco_xCampo: TdmkEdit
        Left = 8
        Top = 36
        Width = 120
        Height = 21
        MaxLength = 20
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdobsFisco_xTexto: TdmkEdit
        Left = 132
        Top = 36
        Width = 360
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
end
