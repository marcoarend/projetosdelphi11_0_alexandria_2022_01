unit NfeCabYA_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkEditDateTimePicker,
  dmkEdit, dmkLabel, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox, dmkEditCB,
  dmkImage, UnDmkEnums, dmkGeral;

type
  TFmNFeCabYA_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PnKeys: TPanel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    EdFatID: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label7: TLabel;
    Panel8: TPanel;
    Label3: TLabel;
    Label14: TLabel;
    Label208: TLabel;
    EdvPag: TdmkEdit;
    EdvTroco: TdmkEdit;
    EdtPag: TdmkEdit;
    EdtPag_TXT: TdmkEdit;
    PnCard: TPanel;
    Label29: TLabel;
    Label17: TLabel;
    Label28: TLabel;
    EdCNPJ: TdmkEdit;
    EdtpIntegra: TdmkEdit;
    EdtpIntegra_TXT: TdmkEdit;
    EdtBand: TdmkEdit;
    EdtBand_TXT: TdmkEdit;
    EdcAut: TdmkEdit;
    REWarning: TRichEdit;
    Label30: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdtPagChange(Sender: TObject);
    procedure EdtPagKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdtpIntegraChange(Sender: TObject);
    procedure EdtpIntegraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBandChange(Sender: TObject);
    procedure EdtBandKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    F_tPag, F_tpIntegra, F_tBand: MyArrayLista;
  public
    { Public declarations }
    FvPag: Double;
  end;

  var
  FmNFeCabYA_0000: TFmNFeCabYA_0000;

implementation

{$R *.DFM}

uses UnMyObjects, UnFinanceiro, NFeCabA_0000, UMySQLModule, Module, ModuleGeral,
  UnInternalConsts, MyDBCheck, Contas, ModuleNFe_0000, NFe_PF;

procedure TFmNFeCabYA_0000.BtOKClick(Sender: TObject);
var
  FatID, FatNum, Empresa, Controle, tPag, tpIntegra, tBand: Integer;
  vPag, vTroco: Double;
  CNPJ, cAut: String;
begin
  FatID    := EdFatID.ValueVariant;
  FatNum   := EdFatNum.ValueVariant;
  Empresa  := EdEmpresa.ValueVariant;
  Controle := EdControle.ValueVariant;
  //
  tPag      := Geral.IMV(EdtPag.ValueVariant);
  vPag      := EdvPag.ValueVariant;
  vTroco    := EdvTroco.ValueVariant;
  tpIntegra := Geral.IMV(EdtpIntegra.ValueVariant);
  CNPJ      := EdCNPJ.ValueVariant;
  tBand     := Geral.IMV(EdtBand.ValueVariant);
  cAut      := EdcAut.ValueVariant;
  //
  if MyObjects.FIC(FatID = 0, nil, 'FatID n�o informado!') then Exit;
  if MyObjects.FIC(FatNum = 0, nil, 'FatNum n�o informado!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o informada!') then Exit;
  if MyObjects.FIC(tPag = 0, EdtPag, 'Informe o meio de pagamento!') then Exit;
  if MyObjects.FIC(tpIntegra = 0,  EdtpIntegra, 'Tipo de Integra��o para pagamento!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    EdControle.ValueVariant := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
  //
  Controle := EdControle.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecabya', False,
    ['tPag', 'vPag', 'vTroco', 'tpIntegra', 'CNPJ', 'tBand', 'cAut'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    [tPag, vPag, vTroco, tpIntegra, CNPJ, tBand, cAut],
    [FatID, FatNum, Empresa, Controle], True) then
  begin
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, FmNFeCabA_0000.LaAviso1,
      FmNFeCabA_0000.LaAviso2, FmNFeCabA_0000.REWarning);
    FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value,
      FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
    //
    Close;
  end;
end;

procedure TFmNFeCabYA_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabYA_0000.EdtBandChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtBand.Text, F_tBand, Texto, 0, 1);
  EdtBand_TXT.Text := Texto;
end;

procedure TFmNFeCabYA_0000.EdtBandKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtBand.Text := Geral.SelecionaItem(F_tBand, 0,
    'SEL-LISTA-000 :: Bandeira da operadora de cart�o',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabYA_0000.EdtPagChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtPag.Text, F_tPag, Texto, 0, 1);
  EdtPag_TXT.Text := Texto;
  //
  if EdtPag.Text = '90' then
    EdvPag.ValueVariant := 0
  else
    EdvPag.ValueVariant := FvPag;
end;

procedure TFmNFeCabYA_0000.EdtPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtPag.Text := Geral.SelecionaItem(F_tPag, 0,
    'SEL-LISTA-000 :: Meio de Pagamento',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabYA_0000.EdtpIntegraChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtpIntegra.Text, F_tpIntegra, Texto, 0, 1);
  EdtpIntegra_TXT.Text := Texto;
end;

procedure TFmNFeCabYA_0000.EdtpIntegraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtpIntegra.Text := Geral.SelecionaItem(F_tpIntegra, 0,
    'SEL-LISTA-000 :: Tipo de Integra��o para pagamento',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFeCabYA_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if EdtpIntegra.ValueVariant <> '' then
    EdtpIntegraChange(EdtpIntegra);
end;

procedure TFmNFeCabYA_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  F_tPag      := UnNFe_PF.ListaMeiosDePagamento();
  F_tpIntegra := UnNFe_PF.ListaTiposDeIntegracaoParaPagamento();
  F_tBand     := UnNFe_PF.ListaBandeirasDaOperadoraDeCartao();
  FvPag       := 0;
end;

procedure TFmNFeCabYA_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
