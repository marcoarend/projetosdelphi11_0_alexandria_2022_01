object FmNFeItsU_0000: TFmNFeItsU_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-006 :: NF-e - Servi'#231'os'
  ClientHeight = 513
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 359
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel25: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 359
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 993
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label231: TLabel
          Left = 158
          Top = 4
          Width = 23
          Height = 13
          Caption = 'Item:'
        end
        object Label232: TLabel
          Left = 193
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label234: TLabel
          Left = 280
          Top = 4
          Width = 157
          Height = 13
          Caption = 'Descri'#231#227'o do produto ou servi'#231'o:'
        end
        object Label235: TLabel
          Left = 792
          Top = 4
          Width = 27
          Height = 13
          Caption = 'NCM:'
          Enabled = False
        end
        object Label236: TLabel
          Left = 858
          Top = 4
          Width = 37
          Height = 13
          Caption = 'EXTIPI:'
          Enabled = False
        end
        object Label237: TLabel
          Left = 906
          Top = 4
          Width = 38
          Height = 13
          Caption = 'Genero:'
          Enabled = False
        end
        object Label238: TLabel
          Left = 949
          Top = 4
          Width = 31
          Height = 13
          Caption = 'CFOP:'
        end
        object Label13: TLabel
          Left = 110
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          Enabled = False
        end
        object Label12: TLabel
          Left = 43
          Top = 4
          Width = 46
          Height = 13
          Caption = 'Fat.Num.:'
          Enabled = False
        end
        object Label2: TLabel
          Left = 8
          Top = 4
          Width = 29
          Height = 13
          Caption = 'FatID:'
          Enabled = False
        end
        object SpeedButton1: TSpeedButton
          Left = 252
          Top = 20
          Width = 22
          Height = 22
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdnItem: TdmkEdit
          Left = 162
          Top = 20
          Width = 31
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          QryName = 'QrNFeItsI'
          QryCampo = 'nItem'
          UpdCampo = 'nItem'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_NCM: TdmkEdit
          Left = 792
          Top = 20
          Width = 62
          Height = 20
          Enabled = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 8
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_NCM'
          UpdCampo = 'prod_NCM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_NCMChange
        end
        object Edprod_EXTIPI: TdmkEdit
          Left = 858
          Top = 20
          Width = 43
          Height = 20
          Enabled = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_EXTIPI'
          UpdCampo = 'prod_EXTIPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_EXTIPIChange
        end
        object Edprod_genero: TdmkEdit
          Left = 906
          Top = 20
          Width = 40
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_genero'
          UpdCampo = 'prod_genero'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = Edprod_generoChange
        end
        object Edprod_CFOP: TdmkEdit
          Left = 949
          Top = 20
          Width = 32
          Height = 20
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_CFOP'
          UpdCampo = 'prod_CFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = Edprod_CFOPChange
        end
        object EdFatNum: TdmkEdit
          Left = 43
          Top = 20
          Width = 63
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'FatNum'
          UpdCampo = 'FatNum'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 110
          Top = 20
          Width = 44
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatID: TdmkEdit
          Left = 8
          Top = 20
          Width = 32
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'FatID'
          UpdCampo = 'FatID'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_cProd: TdmkEdit
          Left = 193
          Top = 20
          Width = 55
          Height = 20
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_cProd'
          UpdCampo = 'prod_cProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = Edprod_cProdChange
        end
        object Edprod_xProd: TdmkEdit
          Left = 280
          Top = 20
          Width = 509
          Height = 20
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_xProd'
          UpdCampo = 'prod_xProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 46
        Width = 993
        Height = 60
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox11: TGroupBox
          Left = 0
          Top = 0
          Width = 654
          Height = 60
          Align = alLeft
          Caption = ' Dados comerciais: '
          TabOrder = 0
          object Label242: TLabel
            Left = 319
            Top = 16
            Width = 54
            Height = 13
            Caption = 'Valor bruto:'
            Enabled = False
          end
          object Label241: TLabel
            Left = 236
            Top = 16
            Width = 40
            Height = 13
            Caption = '$ Pre'#231'o:'
          end
          object Label240: TLabel
            Left = 154
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label239: TLabel
            Left = 98
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Unidade:'
          end
          object Label233: TLabel
            Left = 4
            Top = 16
            Width = 78
            Height = 13
            Caption = 'GTIN, EAN, etc.'
            Enabled = False
          end
          object Label247: TLabel
            Left = 426
            Top = 16
            Width = 36
            Height = 13
            Caption = '$ Frete:'
          end
          object Label248: TLabel
            Left = 500
            Top = 16
            Width = 46
            Height = 13
            Caption = '$ Seguro:'
          end
          object Label249: TLabel
            Left = 575
            Top = 16
            Width = 58
            Height = 13
            Caption = '$ Desconto:'
          end
          object Edprod_qCom: TdmkEdit
            Left = 154
            Top = 31
            Width = 78
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_qCom'
            UpdCampo = 'prod_qCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = Edprod_qComChange
          end
          object Edprod_vProd: TdmkEdit
            Left = 319
            Top = 31
            Width = 103
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vProd'
            UpdCampo = 'prod_vProd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vUnCom: TdmkEdit
            Left = 236
            Top = 31
            Width = 79
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vUnCom'
            UpdCampo = 'prod_vUnCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = Edprod_vUnComChange
          end
          object Edprod_uCom: TdmkEdit
            Left = 98
            Top = 31
            Width = 54
            Height = 21
            MaxLength = 6
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_uCom'
            UpdCampo = 'prod_uCom'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = Edprod_uComChange
          end
          object Edprod_cEAN: TdmkEdit
            Left = 4
            Top = 31
            Width = 90
            Height = 21
            Enabled = False
            MaxLength = 14
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_cEAN'
            UpdCampo = 'prod_cEAN'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = Edprod_cEANChange
          end
          object Edprod_vFrete: TdmkEdit
            Left = 426
            Top = 31
            Width = 70
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vFrete'
            UpdCampo = 'prod_vFrete'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vSeg: TdmkEdit
            Left = 500
            Top = 31
            Width = 71
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vSeg'
            UpdCampo = 'prod_vSeg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Edprod_vDesc: TdmkEdit
            Left = 575
            Top = 31
            Width = 71
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vDesc'
            UpdCampo = 'prod_vDesc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox12: TGroupBox
          Left = 654
          Top = 0
          Width = 339
          Height = 60
          Align = alClient
          Caption = ' Dados tribut'#225'rios: '
          TabOrder = 1
          object Label1: TLabel
            Left = 4
            Top = 16
            Width = 78
            Height = 13
            Caption = 'GTIN, EAN, etc.'
            Enabled = False
          end
          object Label3: TLabel
            Left = 98
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Unidade:'
          end
          object Label4: TLabel
            Left = 154
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label5: TLabel
            Left = 236
            Top = 16
            Width = 31
            Height = 13
            Caption = 'Pre'#231'o:'
          end
          object Edprod_qTrib: TdmkEdit
            Left = 154
            Top = 31
            Width = 78
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_qTrib'
            UpdCampo = 'prod_qTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = Edprod_qTribChange
          end
          object Edprod_vUnTrib: TdmkEdit
            Left = 236
            Top = 31
            Width = 79
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_vUnTrib'
            UpdCampo = 'prod_vUnTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = Edprod_vUnTribChange
            OnExit = Edprod_vUnTribExit
          end
          object Edprod_uTrib: TdmkEdit
            Left = 98
            Top = 31
            Width = 54
            Height = 21
            MaxLength = 6
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_uTrib'
            UpdCampo = 'prod_uTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Edprod_cEANTrib: TdmkEdit
            Left = 4
            Top = 31
            Width = 90
            Height = 21
            Enabled = False
            MaxLength = 14
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsI'
            QryCampo = 'prod_cEANTrib'
            UpdCampo = 'prod_cEANTrib'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = Edprod_cEANTribChange
          end
        end
      end
      object PageControl3: TPageControl
        Left = 0
        Top = 106
        Width = 993
        Height = 190
        ActivePage = TabSheet23
        Align = alClient
        TabOrder = 2
        object TabSheet23: TTabSheet
          Caption = ' ISS '
          ImageIndex = 7
          object Panel36: TPanel
            Left = 0
            Top = 0
            Width = 985
            Height = 162
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label296: TLabel
              Left = 8
              Top = 8
              Width = 59
              Height = 13
              Caption = 'Valor da BC:'
            end
            object Label297: TLabel
              Left = 146
              Top = 8
              Width = 60
              Height = 13
              Caption = 'Al'#237'quota (%):'
            end
            object Label298: TLabel
              Left = 283
              Top = 8
              Width = 78
              Height = 13
              Caption = 'Valor do ISSQN:'
              Enabled = False
            end
            object Label299: TLabel
              Left = 422
              Top = 8
              Width = 193
              Height = 13
              Caption = 'Munic'#237'pio de ocorr'#234'ncia do fato gerador:'
            end
            object Label300: TLabel
              Left = 8
              Top = 47
              Width = 327
              Height = 13
              Caption = 
                'C'#243'digo da lista de servi'#231'os LC 116/03 em que se classifica o ser' +
                'vi'#231'o:'
            end
            object SpeedButton2: TSpeedButton
              Left = 953
              Top = 63
              Width = 21
              Height = 21
              Caption = '?'
              OnClick = SpeedButton2Click
            end
            object SpeedButton3: TSpeedButton
              Left = 953
              Top = 24
              Width = 21
              Height = 20
              Caption = '?'
              OnClick = SpeedButton3Click
            end
            object EdISSQN_vBC: TdmkEdit
              Left = 8
              Top = 24
              Width = 132
              Height = 20
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_vBC'
              UpdCampo = 'ISSQN_vBC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdISSQN_vBCChange
            end
            object EdISSQN_vAliq: TdmkEdit
              Left = 146
              Top = 24
              Width = 132
              Height = 20
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_vAliq'
              UpdCampo = 'ISSQN_vAliq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdISSQN_vAliqChange
            end
            object EdISSQN_vISSQN: TdmkEdit
              Left = 283
              Top = 24
              Width = 132
              Height = 20
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_vISSQN'
              UpdCampo = 'ISSQN_vISSQN'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CBISSQN_cListServ: TdmkDBLookupComboBox
              Left = 67
              Top = 63
              Width = 883
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DmNFe_0000.DsListServ
              TabOrder = 6
              OnExit = CBISSQN_cListServExit
              dmkEditCB = EdISSQN_cListServ
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_cListServ'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBISSQN_cMunFG: TdmkDBLookupComboBox
              Left = 481
              Top = 24
              Width = 469
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DmNFe_0000.DsDTB_Munici
              TabOrder = 4
              dmkEditCB = EdISSQN_cMunFG
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_cMunFG'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdISSQN_cMunFG: TdmkEditCB
              Left = 422
              Top = 24
              Width = 55
              Height = 20
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_cMunFG'
              UpdCampo = 'ISSQN_cMunFG'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBISSQN_cMunFG
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdISSQN_cListServ: TdmkEditCB
              Left = 8
              Top = 63
              Width = 55
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsU'
              QryCampo = 'ISSQN_cListServ'
              UpdCampo = 'ISSQN_cListServ'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBISSQN_cListServ
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
        end
        object TabSheet1: TTabSheet
          Caption = ' Informa'#231#245'es adicionais do servi'#231'o '
          ImageIndex = 1
          object MeInfAdProd: TdmkMemo
            Left = 0
            Top = 0
            Width = 985
            Height = 162
            Align = alClient
            TabOrder = 0
            QryName = 'QrNFeItsV'
            QryCampo = 'InfAdProd'
            UpdCampo = 'InfAdProd'
            UpdType = utYes
          end
        end
      end
      object REWarning: TRichEdit
        Left = 0
        Top = 296
        Width = 993
        Height = 63
        Align = alBottom
        BevelOuter = bvNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          'ATEN'#199#195'O:'
          
            'Verifique os c'#225'lculos de impostos. Caso estajam errados contate ' +
            'seu contador e informe-os manualmente.'
          'Informe a DERMATEK a forma correta de calcul'#225'-los.!!!')
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 180
        Height = 31
        Caption = 'NF-e - Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 180
        Height = 31
        Caption = 'NF-e - Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 180
        Height = 31
        Caption = 'NF-e - Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 406
    Width = 993
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 450
    Width = 993
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 846
        Top = 0
        Width = 143
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 88
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 88
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrGraSrv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla, med.Grandeza, gs1.* '
      'FROM grasrv1 gs1'
      'LEFT JOIN unidmed med ON med.Codigo=gs1.UnidMed')
    Left = 44
    Top = 8
    object QrGraSrv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraSrv1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraSrv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraSrv1infAdProd: TWideStringField
      FieldName = 'infAdProd'
      Size = 255
    end
    object QrGraSrv1cListServ: TIntegerField
      FieldName = 'cListServ'
    end
    object QrGraSrv1PrecoPc: TFloatField
      FieldName = 'PrecoPc'
    end
    object QrGraSrv1PrecoM2: TFloatField
      FieldName = 'PrecoM2'
    end
    object QrGraSrv1Precokg: TFloatField
      FieldName = 'Precokg'
    end
    object QrGraSrv1Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraSrv1UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraSrv1Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object QrPrx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(nItem)  nItem'
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 12
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPrxnItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
  end
end
