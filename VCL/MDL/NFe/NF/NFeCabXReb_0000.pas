unit NFeCabXReb_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmNFeCabXReb_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    Label176: TLabel;
    EdPlaca: TdmkEdit;
    Label177: TLabel;
    EdUF: TdmkEdit;
    Label178: TLabel;
    EdRNTC: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPlacaRedefinido(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FMaxRegCount: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabXReb_0000: TFmNFeCabXReb_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabXReb_0000.BtOKClick(Sender: TObject);
var
  placa, UF, RNTC: String;
  FatID, FatNum, Empresa, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  placa          := EdPlaca.Text;
  UF             := EdUF.Text;
  RNTC           := EdRNTC.Text;
  //
  //Controle  := UMyMod.BPGS1I32('nfecabxreb', 'Controle', '', '', tsPos, SQLType, Controle);
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxreb', '', Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabxreb', False, [
  'placa', 'UF', 'RNTC'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  placa, UF, RNTC], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    if (FMaxRegCount > 0) and (FQryIts.RecordCount >= FMaxRegCount) then
      Close
    else
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Item inclu�do com sucesso!');
      ImgTipo.SQLType := stIns;
      //
      EdPlaca.Text := '';
      EdUF.Text    := '';
      EdRNTC.Text  := '';
    end else
      Close;
  end;
end;

procedure TFmNFeCabXReb_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabXReb_0000.EdPlacaRedefinido(Sender: TObject);
var
  Placa: String;
begin
  Placa := Geral.SoNumeroELetra_TT(EdPlaca.Text);
  if Placa <> EdPlaca.Text then
    EdPlaca.Text := Placa;
end;

procedure TFmNFeCabXReb_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabXReb_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeCabXReb_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
