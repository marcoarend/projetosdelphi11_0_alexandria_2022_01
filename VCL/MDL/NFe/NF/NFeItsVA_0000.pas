unit NFeItsVA_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, ShellAPI, dmkCheckBox;

type
  TFmNFeItsVA_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdnItem: TdmkEdit;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label14: TLabel;
    EdobsCont_xCampo: TdmkEdit;
    EdobsCont_xTexto: TdmkEdit;
    GroupBox4: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    EdobsFisco_xCampo: TdmkEdit;
    EdobsFisco_xTexto: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure LinkLabel1LinkClick(Sender: TObject; const Link: string;
      LinkType: TSysLinkType);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmNFeItsVA_0000: TFmNFeItsVA_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeItsVA_0000.BtOKClick(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String;
  SQLType: TSQLType;
begin
  SQLType                      := ImgTipo.SQLType;
  FatID                        := EdFatID.ValueVariant;
  FatNum                       := EdFatNum.ValueVariant;
  Empresa                      := EdEmpresa.ValueVariant;
  nItem                        := EdnItem.ValueVariant;
  //
  obsCont_xCampo  := EdobsCont_xCampo.ValueVariant;
  obsCont_xTexto  := EdobsCont_xTexto.ValueVariant;
  obsFisco_xCampo := EdobsFisco_xCampo.ValueVariant;
  obsFisco_xTexto := EdobsFisco_xTexto.ValueVariant;

  if Trim(obsCont_xCampo) + Trim(obsCont_xTexto) <> EmptyStr then
  begin
    if MyObjects.FIC(Trim(obsCont_xCampo) = EmptyStr, EdobsCont_xCampo,
    'Informe a identifica��o do campo do uso livre do contribuinte!') then Exit;

    if MyObjects.FIC(Trim(obsCont_xTexto) = EmptyStr, EdobsCont_xTexto,
    'Informe o conte�do do campo do uso livre do contribuinte!') then Exit;
  end;

  if Trim(obsFisco_xCampo) + Trim(obsFisco_xTexto) <> EmptyStr then
  begin
    if MyObjects.FIC(Trim(obsFisco_xCampo) = EmptyStr, EdobsFisco_xCampo,
    'Informe a identifica��o do campo do uso livre do fisco!') then Exit;

    if MyObjects.FIC(Trim(obsFisco_xTexto) = EmptyStr, EdobsFisco_xTexto,
    'Informe o conte�do do campo do uso livre do fisco!') then Exit;
  end;

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeitsva', False, [
  'obsCont_xCampo', 'obsCont_xTexto',
  'obsFisco_xCampo', 'obsFisco_xTexto'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  obsCont_xCampo, obsCont_xTexto,
  obsFisco_xCampo, obsFisco_xTexto], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
    //
    Close;
  end;
end;

procedure TFmNFeItsVA_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsVA_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsVA_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeItsVA_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeItsVA_0000.LinkLabel1LinkClick(Sender: TObject;
  const Link: string; LinkType: TSysLinkType);
begin
  ShellExecute(0, nil, PChar(Link), nil, nil, 1);
end;

procedure TFmNFeItsVA_0000.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
end;

end.
