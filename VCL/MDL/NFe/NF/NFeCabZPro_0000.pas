unit NFeCabZPro_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmNFeCabZPro_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    Label178: TLabel;
    EdnProc: TdmkEdit;
    Panel2: TPanel;
    RGindProc: TdmkRadioGroup;
    RGtpAto: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGindProcClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FMaxRegCount: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabZPro_0000: TFmNFeCabZPro_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabZPro_0000.BtOKClick(Sender: TObject);
var
  nProc: String;
  FatID, FatNum, Empresa, Controle, indProc, tpAto: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  nProc          := EdnProc.Text;
  indProc        := RGindProc.ItemIndex;
  //
  if MyObjects.FIC(indProc < 0 , nil,
  'Informe o Indicador da Origem do processo!') then
    Exit
  else
    indProc := Geral.IMV(RGindProc.Items[RGindProc.ItemIndex][1]);
  //
  if indProc = 0 then
  begin
    if MyObjects.FIC(tpAto < 0 , nil,
    'Informe o Indicador da Origem do processo!') then
      Exit
    else
      tpAto := Geral.IMV(Copy(RGtpAto.Items[RGtpAto.ItemIndex], 1, 2));
  end else
    tpAto := -1;
  //
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabzpro', '', Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabzpro', False, [
  'nProc', 'indProc', 'tpAto'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  nProc, indProc, tpAto], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    if (FMaxRegCount > 0) and (FQryIts.RecordCount >= FMaxRegCount) then
      Close
    else
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Item inclu�do com sucesso!');
      ImgTipo.SQLType := stIns;
      //
      EdnProc.Text := '';
    end else
      Close;
  end;
end;

procedure TFmNFeCabZPro_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabZPro_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabZPro_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeCabZPro_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabZPro_0000.RGindProcClick(Sender: TObject);
begin
  RGTpAto.Enabled := RGindProc.ItemIndex = 0;
end;

end.
