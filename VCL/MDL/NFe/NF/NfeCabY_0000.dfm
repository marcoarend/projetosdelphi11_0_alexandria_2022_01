object FmNFeCabY_0000: TFmNFeCabY_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-007 :: NF-e - Duplicata de Cobran'#231'a'
  ClientHeight = 332
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 176
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 84
      Top = 8
      Width = 62
      Height = 13
      Caption = 'ID Duplicata:'
      Enabled = False
    end
    object Label2: TLabel
      Left = 84
      Top = 52
      Width = 88
      Height = 13
      Caption = 'N'#250'mero Duplicata:'
    end
    object Label3: TLabel
      Left = 228
      Top = 52
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
    end
    object Label4: TLabel
      Left = 352
      Top = 52
      Width = 73
      Height = 13
      Caption = 'Valor duplicata:'
    end
    object Label5: TLabel
      Left = 152
      Top = 8
      Width = 47
      Height = 13
      Caption = 'ID Fatura:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 220
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Num. Fatura:'
      Enabled = False
    end
    object Label7: TLabel
      Left = 288
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Empresa: '
      Enabled = False
    end
    object Label8: TLabel
      Left = 344
      Top = 8
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
      Enabled = False
    end
    object Label9: TLabel
      Left = 448
      Top = 8
      Width = 22
      Height = 13
      Caption = 'Sub:'
      Enabled = False
    end
    object Label14: TLabel
      Left = 84
      Top = 92
      Width = 116
      Height = 13
      Caption = 'Conta (plano de contas):'
    end
    object Label20: TLabel
      Left = 138
      Top = 132
      Width = 176
      Height = 13
      Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
    end
    object SbProdVen: TSpeedButton
      Left = 478
      Top = 108
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbProdVenClick
    end
    object Label10: TLabel
      Left = 84
      Top = 132
      Width = 39
      Height = 13
      Caption = 'Parcela:'
    end
    object EdControle: TdmkEdit
      Left = 84
      Top = 24
      Width = 64
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdnDup: TdmkEdit
      Left = 84
      Top = 68
      Width = 141
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'nDup'
      UpdCampo = 'nDup'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdvDup: TdmkEdit
      Left = 352
      Top = 68
      Width = 121
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'vDup'
      UpdCampo = 'vDup'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdFatID: TdmkEdit
      Left = 152
      Top = 24
      Width = 64
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'FatID'
      UpdCampo = 'FatID'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFatNum: TdmkEdit
      Left = 220
      Top = 24
      Width = 64
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'FatNum'
      UpdCampo = 'FatNum'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEmpresa: TdmkEdit
      Left = 288
      Top = 24
      Width = 53
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object TPdVenc: TdmkEditDateTimePicker
      Left = 228
      Top = 68
      Width = 121
      Height = 21
      Date = 40053.000000000000000000
      Time = 0.984072835650295000
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'dVenc'
      UpdCampo = 'dVenc'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdLancto: TdmkEdit
      Left = 344
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Lancto'
      UpdCampo = 'Lancto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSub: TdmkEdit
      Left = 448
      Top = 24
      Width = 25
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Sub'
      UpdCampo = 'Sub'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdGenero: TdmkEditCB
      Left = 84
      Top = 108
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Genero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGenero
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGenero: TdmkDBLookupComboBox
      Left = 140
      Top = 108
      Width = 333
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 10
      dmkEditCB = EdGenero
      QryCampo = 'Genero'
      UpdType = utNil
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdDescricao: TdmkEdit
      Left = 138
      Top = 148
      Width = 335
      Height = 21
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'FA'
      QryCampo = 'Descricao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'FA'
      ValWarn = False
    end
    object EdFatParcela: TdmkEdit
      Left = 84
      Top = 148
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'FatParcela'
      UpdCampo = 'Sub'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 506
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 458
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 363
        Height = 32
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 363
        Height = 32
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 363
        Height = 32
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 224
    Width = 554
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 520
        Height = 16
        Caption = 
          'CUIDADO!! A Parcela deve ser sequencial!!  -  Lan'#231'amentos financ' +
          'eiros n'#227'o s'#227'o afetados!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 520
        Height = 16
        Caption = 
          'CUIDADO!! A Parcela deve ser sequencial!!  -  Lan'#231'amentos financ' +
          'eiros n'#227'o s'#227'o afetados!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 268
    Width = 554
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 406
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 36
    Top = 8
  end
end
