unit NfeCabY_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     ComCtrls,
  dmkEditDateTimePicker, dmkEdit, dmkLabel, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmNFeCabY_0000 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdnDup: TdmkEdit;
    EdvDup: TdmkEdit;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    TPdVenc: TdmkEditDateTimePicker;
    EdLancto: TdmkEdit;
    EdSub: TdmkEdit;
    Label14: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    EdDescricao: TdmkEdit;
    Label20: TLabel;
    SbProdVen: TSpeedButton;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    EdFatParcela: TdmkEdit;
    Label10: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbProdVenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function IncluiLancto(Valor, JurosMes: Double; FatID, FatNum, Empresa,
              Cliente, Controle, Sub: Integer; Data, Vencto: TDateTime;
              Duplicata, Descricao: String; TipoCart, Carteira, Genero, Parcela,
              NotaFiscal, Account, SerieNF: Integer; VerificaCliInt: Boolean):
              Boolean;
  public
    { Public declarations }
  end;

  var
  FmNFeCabY_0000: TFmNFeCabY_0000;

implementation

{$R *.DFM}

uses UnMyObjects, UnFinanceiro, NFeCabA_0000, UMySQLModule, Module, ModuleGeral,
UnInternalConsts, MyDBCheck, Contas, dmkGeral, ModuleNFe_0000, DmkDAC_PF;

function TFmNFeCabY_0000.IncluiLancto(Valor, JurosMes: Double; FatID, FatNum, Empresa,
  Cliente, Controle, Sub: Integer; Data, Vencto: TDateTime; Duplicata,
  Descricao: String; TipoCart, Carteira, Genero, Parcela,
  NotaFiscal, Account, SerieNF: Integer; VerificaCliInt: Boolean): Boolean;
begin
  Result := True;
  Geral.MensagemBox('Os lan�amentos financeiros n�o s�o inclu�dos/alterados ' +
  'na inclus�o manual de cobran�a!' + sLineBreak +
  'Inclua / altere manualmente estes lan�amentos financeiros!',
  'Informa��o', MB_OK+MB_ICONINFORMATION);
{

  o problema � o  U n F i n a n c e i r o . I n s e r e L a n c a m e n t o ()


  Result := False;
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_VERIFICA_CLIINT := VerificaCliInt;
  FLAN_Data       := Geral.FDT(Data, 1);
  FLAN_Tipo       := TipoCart;
  FLAN_Documento  := 0;
  FLAN_Credito    := Valor;
  FLAN_MoraDia    := JurosMes;
  FLAN_Multa      := 0;//
  FLAN_Carteira   := Carteira;
  F L A N _ G e n e r o     := Genero;
  FLAN_Cliente    := Cliente;
  FLAN_CliInt     := Empresa;
  //FLAN_Depto      := QrBolacarAApto.Value;
  //FLAN_ForneceI   := QrBolacarAPropriet.Value;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
  FLAN_FatID      := FatID;
  FLAN_FatNum     := FatNum;
  FLAN_FatParcela := Parcela;
  FLAN_Descricao  := Descricao;
  FLAN_Duplicata  := Duplicata;
  FLAN_SerieNF    := FormatFloat('0', SerieNF);
  FLAN_NotaFiscal := NotaFiscal;
  FLAN_Account    := Account;
  FLAN_Sub        := Sub;
  if ImgTipo.SQLType = stUpd then
  begin
    EXIT;
    //
    (*
    UFinanceiro.ExcluiLct_Unico(True, DMod.MyDB,
      FmNfeCabA_0000.QrNFeCabYData.Value,
      FmNfeCabA_0000.QrNFeCabYTipo.Value,
      FmNfeCabA_0000.QrNFeCabYCarteira.Value,
      FmNfeCabA_0000.QrNFeCabYControle.Value,
      FmNfeCabA_0000.QrNFeCabYSub.Value, False);
    *)
  end else
    FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      VAR_LCT, VAR_LCT, 'Controle');
  if U n F i n a n c e i r o . I n s e r e L a n c a m e n t o () then
  begin
    Result := True;
    // nada
  end;
}
end;

procedure TFmNFeCabY_0000.SbProdVenClick(Sender: TObject);
begin
  VAR_CONTA := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    if VAR_CONTA <> 0 then
    begin
      QrContas.Close;
      UMyMod.AbreQuery(QrContas, Dmod.MyDB, 'TFmNFeCabY.SbProdVenClick()');
      //
      EdGenero.ValueVariant := VAR_CONTA;
      CBGenero.KeyValue := VAR_CONTA;
    end;
  end;
end;

procedure TFmNFeCabY_0000.BtOKClick(Sender: TObject);
const
  VerificaCliInt = True;
var
  vDup, JurosMes: Double;
  FatID, FatNum, Empresa, Cliente, LANCTO, Sub, SerieNF, Controle,
  TipoCart, Carteira, Genero, FatParcela, NotaFiscal, Account: Integer;
  Data, dVenc: TDateTime;
  nDup, Descricao: String;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  DmodG.ReopenParamsEmp(Empresa);
  //
  vDup       := EdvDup.ValueVariant;
  dVenc      := TPdVenc.Date;
  nDup       := EdnDup.ValueVariant;
  JurosMes   := FmNFeCabA_0000.QrNFeCabAJurosMes.Value;
  FatID      := EdFatID.ValueVariant;
  FatNum     := EdFatNum.ValueVariant;
  Cliente    := FmNFeCabA_0000.QrNFeCabACodInfoEmit.Value;
  Data       := FmNFeCabA_0000.QrNFeCabAide_dEmi.Value;
  Descricao  := EdDescricao.Text;
  SerieNF    := FmNFeCabA_0000.QrNFeCabAide_serie.Value;
  TipoCart   := FmNFeCabA_0000.QrNFeCabATP_CART.Value;
  Carteira   := FmNFeCabA_0000.QrNFeCabACartEmiss.Value;
  Genero     := EdGenero.ValueVariant;
  FatParcela := EdFatParcela.ValueVariant;
  NotaFiscal := FmNFeCabA_0000.QrNFeCabAide_nNF.Value;
  Account    := 0; // Parei Aqui!! Fazer
  LANCTO     := EdLancto.ValueVariant;
  Sub        := EdSub.ValueVariant;
  //
  if ImgTipo.SQLType = stIns then
    // Mudado 2013-02-12
    //EdControle.ValueVariant := DModG.BuscaProximoInteiro('nfecaby', 'Controle', '', 0);
    EdControle.ValueVariant := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
    // FIM Mudado 2013-02-12
  Controle := EdControle.ValueVariant;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecaby', False, [
    'nDup', 'dVenc', 'vDup', 'Lancto', 'Sub'
  ], ['FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'], [
    nDup, dVenc, vDup, Lancto, Sub
  ], [FatID, FatNum, FatParcela, Empresa, Controle], True) then
  begin
    if
    IncluiLancto(vDup, JurosMes, FatID, FatNum, Empresa,
      Cliente, LANCTO, Sub, Data, dVenc, nDup,
      Descricao, TipoCart, Carteira, Genero, FatParcela,
      NotaFiscal, Account, SerieNF, VerificaCliInt) then
    begin
      DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, FmNFeCabA_0000.LaAviso1,
        FmNFeCabA_0000.LaAviso2, FmNFeCabA_0000.REWarning);
      FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value,
        FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
      //
      Close;
    end;
  end;
end;

procedure TFmNFeCabY_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabY_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabY_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  // ini 2022-02-20
  //QrContas. O p e n ;
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  // fim 2022-02-20
  TPdVenc.Date := Date;
end;

procedure TFmNFeCabY_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
