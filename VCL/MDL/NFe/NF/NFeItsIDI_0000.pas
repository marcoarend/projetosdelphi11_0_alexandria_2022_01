unit NFeItsIDI_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums;

type
  TFmNFeItsIDI_0000 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    EdControle: TdmkEdit;
    EdDI_nDI: TdmkEdit;
    Label5: TLabel;
    DBEdnItem: TdmkDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    TPDI_dDI: TdmkEditDateTimePicker;
    Label6: TLabel;
    EdDI_xLocDesemb: TdmkEdit;
    Label7: TLabel;
    EdDI_UFDesemb: TdmkEdit;
    Label9: TLabel;
    Label11: TLabel;
    TPDI_dDesemb: TdmkEditDateTimePicker;
    EdDI_cExportador: TdmkEdit;
    Label12: TLabel;
    Label1: TLabel;
    DBEdFatID: TdmkDBEdit;
    Label10: TLabel;
    DBEdFatNum: TdmkDBEdit;
    Label13: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeItsIDI_0000: TFmNFeItsIDI_0000;

implementation

uses UnMyObjects, Module, NFeCabA_0000, UMySQLModule, UnInternalConsts, MyDBCheck,
  ModuleGeral;

{$R *.DFM}

procedure TFmNFeItsIDI_0000.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
{
  if (Ini = 0) or (Fim = 0) or (Ini > Fim) or (Fim < Ini)
  or ((Atu < Ini) and (Atu > 0)) or ((Atu > Fim) and (Atu > 0)) then
  begin
    Geral.MensagemBox('Erro de numera��o!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
}
  //Controle := UMyMod.BuscaEmLivreY_Def('nfeitsidi', 'nfectrl', ImgTipo.SQLType, EdControle.ValueVariant);
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeitsidi', '', EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(FmNFeItsIDI_0000, ImgTipo.SQLType, 'nfeitsidi',
  Controle, Dmod.QrUpd) then
  begin
    FmNFeCabA_0000.ReopenNFeItsIDI(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Declara��o inclu�da / alterada!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EdDI_nDI.SetFocus;
    end else Close;
  end;
end;

procedure TFmNFeItsIDI_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsIDI_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsIDI_0000.FormCreate(Sender: TObject);
begin
  TPDI_dDesemb.Date := Date;
  TPDI_dDI.Date     := Date;
end;

procedure TFmNFeItsIDI_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
