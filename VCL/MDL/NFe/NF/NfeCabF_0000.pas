unit NfeCabF_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, dmkLabel, dmkEdit, DB,
  mySQLDbTables, dmkEditCB, DBCtrls, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmNFeCabF_0000 = class(TForm)
    Panel1: TPanel;
    Panel23: TPanel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label218: TLabel;
    Edretirada_xLgr: TdmkEdit;
    Edretirada_nro: TdmkEdit;
    Edretirada_xCpl: TdmkEdit;
    Edretirada_xBairro: TdmkEdit;
    Edretirada_xMun: TdmkEdit;
    Edretirada_UF: TdmkEdit;
    Edretirada_cMun: TdmkEditCB;
    Edretirada_CNPJ: TdmkEdit;
    SbMunicipio: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label120: TLabel;
    Label136: TLabel;
    Edretirada_CPF: TdmkEdit;
    Label1: TLabel;
    Edretirada_xNome: TdmkEdit;
    Edretirada_CEP: TdmkEdit;
    Label94: TLabel;
    BtCEP_L: TBitBtn;
    Edretirada_cPais: TdmkEditCB;
    Label2: TLabel;
    Edretirada_xPais: TdmkEdit;
    SbPais: TSpeedButton;
    Edretirada_fone: TdmkEdit;
    Label175: TLabel;
    Label167: TLabel;
    Edretirada_email: TdmkEdit;
    Edretirada_IE: TdmkEdit;
    Label168: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbMunicipioClick(Sender: TObject);
    procedure SbPaisClick(Sender: TObject);
    procedure Edretirada_cMunChange(Sender: TObject);
    procedure Edretirada_cPaisChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeCabF_0000: TFmNFeCabF_0000;

implementation

uses UnMyObjects, ModuleNFe_0000, Module, UnMySQLCuringa, UnInternalConsts,
  UMySQLModule, NFeCabA_0000, ModuleGeral;

{$R *.DFM}

procedure TFmNFeCabF_0000.BtOKClick(Sender: TObject);
var
  retirada_CNPJ, retirada_xLgr, retirada_nro, retirada_xCpl, retirada_xBairro,
  retirada_xMun, retirada_UF, retirada_CPF, retirada_xNome, retirada_xPais,
  retirada_fone, retirada_email, retirada_IE: String;
  FatID, FatNum, Empresa, retirada_CEP, retirada_cMun, retirada_cPais: Integer;
  SQLType: TSQLType;
begin
  SQLType          := ImgTipo.SQLType;
  FatID            := FmNFeCabA_0000.QrNFeCabAFatID.Value;
  FatNum           := FmNFeCabA_0000.QrNFeCabAFatNum.Value;
  Empresa          := FmNFeCabA_0000.QrNFeCabAEmpresa.Value;
  retirada_CNPJ    := Geral.SoNumero_TT(Edretirada_CNPJ.ValueVariant);
  retirada_CPF     := Geral.SoNumero_TT(Edretirada_CPF.ValueVariant);
  retirada_xNome   := Edretirada_xNome.ValueVariant;
  retirada_xLgr    := Edretirada_xLgr.ValueVariant;
  retirada_nro     := Edretirada_nro.ValueVariant;
  retirada_xCpl    := Edretirada_xCpl.ValueVariant;
  retirada_xBairro := Edretirada_xBairro.ValueVariant;
  retirada_cMun    := Edretirada_cMun.ValueVariant;
  retirada_xMun    := Edretirada_xMun.ValueVariant;
  retirada_UF      := Edretirada_UF.ValueVariant;
  //retirada_CEP     := Geral.SoNumero_TT(Edretirada_CEP.ValueVariant);
  retirada_CEP     := Edretirada_CEP.ValueVariant;
  retirada_cPais   := Edretirada_cPais.ValueVariant;
  retirada_xPais   := Edretirada_xPais.ValueVariant;
  retirada_fone    := Edretirada_fone.ValueVariant;
  retirada_email   := Edretirada_email.ValueVariant;
  retirada_IE      := Edretirada_IE.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabf', False, [
  'retirada_CNPJ', 'retirada_xLgr', 'retirada_nro',
  'retirada_xCpl', 'retirada_xBairro', 'retirada_cMun',
  'retirada_xMun', 'retirada_UF', 'retirada_CPF',
  'retirada_xNome', 'retirada_CEP', 'retirada_cPais',
  'retirada_xPais', 'retirada_fone', 'retirada_email',
  'retirada_IE'], [
  'FatID', 'FatNum', 'Empresa'], [
  retirada_CNPJ, retirada_xLgr, retirada_nro,
  retirada_xCpl, retirada_xBairro, retirada_cMun,
  retirada_xMun, retirada_UF, retirada_CPF,
  retirada_xNome, retirada_CEP, retirada_cPais,
  retirada_xPais, retirada_fone, retirada_email,
  retirada_IE], [
  FatID, FatNum, Empresa], True) then
  begin
    FmNFeCabA_0000.ReopenNFeCabF(0);
    Close;
  end;
end;

procedure TFmNFeCabF_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabF_0000.Edretirada_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edretirada_cMun.ValueVariant, []) then
    Edretirada_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
end;

procedure TFmNFeCabF_0000.Edretirada_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Edretirada_cPais.ValueVariant, []) then
    Edretirada_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value;
end;

procedure TFmNFeCabF_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabF_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeCabF_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabF_0000.SbPaisClick(Sender: TObject);
var
  Pesq: Integer;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'bacen_pais', DModG.AllID_DB, '', False);
  if Pesq <> 0 then
    Edretirada_cPais.ValueVariant := VAR_CODIGO;
end;

procedure TFmNFeCabF_0000.SbMunicipioClick(Sender: TObject);
var
  Pesq: Integer;
  UF: String;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'dtb_munici', DModG.AllID_DB, '', False);
  if Pesq <> 0 then
  begin
    Edretirada_cMun.ValueVariant := VAR_CODIGO;
    UF := DmNFe_0000.ObtemSiglaUFdeDTBdeCidade(VAR_CODIGO);
    if UF <> '' then
      Edretirada_UF.Text := UF;
  end;
end;

end.
