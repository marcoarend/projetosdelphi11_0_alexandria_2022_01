object FmNFeCabYAll_0000: TFmNFeCabYAll_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-020 :: NF-e - Duplicata de Cobran'#231'a'
  ClientHeight = 619
  ClientWidth = 888
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 888
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 841
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 794
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 31
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 31
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 31
        Caption = 'NF-e - Duplicata de Cobran'#231'a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 888
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 21
      Width = 888
      Height = 439
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 888
        Height = 439
        Align = alClient
        TabOrder = 0
        object DBGTmpNFeCabY: TDBGrid
          Left = 2
          Top = 96
          Width = 884
          Height = 341
          Align = alClient
          DataSource = DsTmpNFeCabY
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGTmpNFeCabYKeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'nDup'
              Title.Caption = 'N'#250'mero Duplicata'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dVenc'
              Title.Caption = 'Vencimento'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'vDup'
              Title.Caption = 'Valor duplicata'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero_TXT'
              Title.Caption = 'Conta (plano de contas) [F7]'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'Parcela'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Texto para descri'#231#227'o no lan'#231'amento'
              Width = 200
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 884
          Height = 81
          Align = alTop
          TabOrder = 1
          object LaTotNFe: TLabel
            Left = 12
            Top = 2
            Width = 860
            Height = 24
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Total NF-e: 0.000.000,000'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object LaTotFatu: TLabel
            Left = 12
            Top = 29
            Width = 860
            Height = 24
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Total faturas: 0.000.000,000'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object LaDiferenca: TLabel
            Left = 12
            Top = 54
            Width = 860
            Height = 24
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Diferen'#231'a: 0.000.000,000'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
      end
    end
    object PB1: TProgressBar
      Left = 0
      Top = 0
      Width = 888
      Height = 21
      Align = alTop
      TabOrder = 1
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 507
    Width = 888
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 884
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 622
        Height = 16
        Caption = 
          'Ctrl + Del para excluir // Selecione a '#250'ltima linha em seguida s' +
          'eta para baixo para incluir nova linha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 622
        Height = 16
        Caption = 
          'Ctrl + Del para excluir // Selecione a '#250'ltima linha em seguida s' +
          'eta para baixo para incluir nova linha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 888
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 744
      Top = 15
      Width = 142
      Height = 52
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 742
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object TbTmpNFeCabY: TMySQLTable
    BeforePost = TbTmpNFeCabYBeforePost
    AfterPost = TbTmpNFeCabYAfterPost
    AfterDelete = TbTmpNFeCabYAfterDelete
    OnNewRecord = TbTmpNFeCabYNewRecord
    SortFieldNames = 'FatParcela'
    IndexFieldNames = 'Controle'
    Left = 180
    Top = 288
    object TbTmpNFeCabYControle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000'
    end
    object TbTmpNFeCabYnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object TbTmpNFeCabYdVenc: TDateField
      FieldName = 'dVenc'
      OnSetText = TbTmpNFeCabYdVencSetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbTmpNFeCabYvDup: TFloatField
      FieldName = 'vDup'
      OnSetText = TbTmpNFeCabYvDupSetText
      DisplayFormat = '####,##0.00'
    end
    object TbTmpNFeCabYFatID: TIntegerField
      FieldName = 'FatID'
    end
    object TbTmpNFeCabYFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object TbTmpNFeCabYEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object TbTmpNFeCabYSub: TIntegerField
      FieldName = 'Sub'
    end
    object TbTmpNFeCabYLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object TbTmpNFeCabYFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object TbTmpNFeCabYDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbTmpNFeCabYGenero: TIntegerField
      FieldName = 'Genero'
    end
    object TbTmpNFeCabYGenero_TXT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Genero_TXT'
      LookupDataSet = QrContas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Genero'
      Size = 50
      Lookup = True
    end
  end
  object DsTmpNFeCabY: TDataSource
    DataSet = TbTmpNFeCabY
    Left = 208
    Top = 288
  end
  object QrTmpNFeCabYTot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT EhGGX, COUNT(*) Itens'
      'FROM formulasits'
      'GROUP BY EhGGX'
      'ORDER BY EhGGX')
    Left = 364
    Top = 284
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 236
    Top = 287
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 264
    Top = 287
  end
end
