unit NFeCabA_0000_Pesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkEdit,
  DBCtrls, dmkDBLookupComboBox, dmkEditCB, DB, mySQLDbTables, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmNFeCabA_0000_Pesq = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdFatID: TdmkEdit;
    Label2: TLabel;
    EdFatNum: TdmkEdit;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqFatID: TIntegerField;
    QrPesqFatNum: TIntegerField;
    QrPesqEmpresa: TIntegerField;
    QrPesqIDCtrl: TIntegerField;
    QrPesqLoteEnv: TIntegerField;
    QrPesqversao: TFloatField;
    QrPesqId: TWideStringField;
    QrPesqide_cUF: TSmallintField;
    QrPesqide_cNF: TIntegerField;
    QrPesqide_natOp: TWideStringField;
    QrPesqide_indPag: TSmallintField;
    QrPesqide_mod: TSmallintField;
    QrPesqide_serie: TIntegerField;
    QrPesqide_nNF: TIntegerField;
    QrPesqide_dEmi: TDateField;
    QrPesqide_dSaiEnt: TDateField;
    QrPesqide_tpNF: TSmallintField;
    QrPesqide_cMunFG: TIntegerField;
    QrPesqide_tpImp: TSmallintField;
    QrPesqide_tpEmis: TSmallintField;
    QrPesqide_cDV: TSmallintField;
    QrPesqide_tpAmb: TSmallintField;
    QrPesqide_finNFe: TSmallintField;
    QrPesqide_procEmi: TSmallintField;
    QrPesqide_verProc: TWideStringField;
    QrPesqemit_CNPJ: TWideStringField;
    QrPesqemit_CPF: TWideStringField;
    QrPesqemit_xNome: TWideStringField;
    QrPesqemit_xFant: TWideStringField;
    QrPesqemit_xLgr: TWideStringField;
    QrPesqemit_nro: TWideStringField;
    QrPesqemit_xCpl: TWideStringField;
    QrPesqemit_xBairro: TWideStringField;
    QrPesqemit_cMun: TIntegerField;
    QrPesqemit_xMun: TWideStringField;
    QrPesqemit_UF: TWideStringField;
    QrPesqemit_CEP: TIntegerField;
    QrPesqemit_cPais: TIntegerField;
    QrPesqemit_xPais: TWideStringField;
    QrPesqemit_fone: TWideStringField;
    QrPesqemit_IE: TWideStringField;
    QrPesqemit_IEST: TWideStringField;
    QrPesqemit_IM: TWideStringField;
    QrPesqemit_CNAE: TWideStringField;
    QrPesqdest_CNPJ: TWideStringField;
    QrPesqdest_CPF: TWideStringField;
    QrPesqdest_xNome: TWideStringField;
    QrPesqdest_xLgr: TWideStringField;
    QrPesqdest_nro: TWideStringField;
    QrPesqdest_xCpl: TWideStringField;
    QrPesqdest_xBairro: TWideStringField;
    QrPesqdest_cMun: TIntegerField;
    QrPesqdest_xMun: TWideStringField;
    QrPesqdest_UF: TWideStringField;
    QrPesqdest_CEP: TWideStringField;
    QrPesqdest_cPais: TIntegerField;
    QrPesqdest_xPais: TWideStringField;
    QrPesqdest_fone: TWideStringField;
    QrPesqdest_IE: TWideStringField;
    QrPesqdest_ISUF: TWideStringField;
    QrPesqICMSTot_vBC: TFloatField;
    QrPesqICMSTot_vICMS: TFloatField;
    QrPesqICMSTot_vBCST: TFloatField;
    QrPesqICMSTot_vST: TFloatField;
    QrPesqICMSTot_vProd: TFloatField;
    QrPesqICMSTot_vFrete: TFloatField;
    QrPesqICMSTot_vSeg: TFloatField;
    QrPesqICMSTot_vDesc: TFloatField;
    QrPesqICMSTot_vII: TFloatField;
    QrPesqICMSTot_vIPI: TFloatField;
    QrPesqICMSTot_vPIS: TFloatField;
    QrPesqICMSTot_vCOFINS: TFloatField;
    QrPesqICMSTot_vOutro: TFloatField;
    QrPesqICMSTot_vNF: TFloatField;
    QrPesqISSQNtot_vServ: TFloatField;
    QrPesqISSQNtot_vBC: TFloatField;
    QrPesqISSQNtot_vISS: TFloatField;
    QrPesqISSQNtot_vPIS: TFloatField;
    QrPesqISSQNtot_vCOFINS: TFloatField;
    QrPesqRetTrib_vRetPIS: TFloatField;
    QrPesqRetTrib_vRetCOFINS: TFloatField;
    QrPesqRetTrib_vRetCSLL: TFloatField;
    QrPesqRetTrib_vBCIRRF: TFloatField;
    QrPesqRetTrib_vIRRF: TFloatField;
    QrPesqRetTrib_vBCRetPrev: TFloatField;
    QrPesqRetTrib_vRetPrev: TFloatField;
    QrPesqModFrete: TSmallintField;
    QrPesqTransporta_CNPJ: TWideStringField;
    QrPesqTransporta_CPF: TWideStringField;
    QrPesqTransporta_XNome: TWideStringField;
    QrPesqTransporta_IE: TWideStringField;
    QrPesqTransporta_XEnder: TWideStringField;
    QrPesqTransporta_XMun: TWideStringField;
    QrPesqTransporta_UF: TWideStringField;
    QrPesqRetTransp_vServ: TFloatField;
    QrPesqRetTransp_vBCRet: TFloatField;
    QrPesqRetTransp_PICMSRet: TFloatField;
    QrPesqRetTransp_vICMSRet: TFloatField;
    QrPesqRetTransp_CFOP: TWideStringField;
    QrPesqRetTransp_CMunFG: TWideStringField;
    QrPesqVeicTransp_Placa: TWideStringField;
    QrPesqVeicTransp_UF: TWideStringField;
    QrPesqVeicTransp_RNTC: TWideStringField;
    QrPesqCobr_Fat_nFat: TWideStringField;
    QrPesqCobr_Fat_vOrig: TFloatField;
    QrPesqCobr_Fat_vDesc: TFloatField;
    QrPesqCobr_Fat_vLiq: TFloatField;
    QrPesqInfAdic_InfAdFisco: TWideMemoField;
    QrPesqInfAdic_InfCpl: TWideMemoField;
    QrPesqExporta_UFEmbarq: TWideStringField;
    QrPesqExporta_XLocEmbarq: TWideStringField;
    QrPesqCompra_XNEmp: TWideStringField;
    QrPesqCompra_XPed: TWideStringField;
    QrPesqCompra_XCont: TWideStringField;
    QrPesqStatus: TIntegerField;
    QrPesqinfProt_Id: TWideStringField;
    QrPesqinfProt_tpAmb: TSmallintField;
    QrPesqinfProt_verAplic: TWideStringField;
    QrPesqinfProt_dhRecbto: TDateTimeField;
    QrPesqinfProt_nProt: TWideStringField;
    QrPesqinfProt_digVal: TWideStringField;
    QrPesqinfProt_cStat: TIntegerField;
    QrPesqinfProt_xMotivo: TWideStringField;
    QrPesqinfCanc_Id: TWideStringField;
    QrPesqinfCanc_tpAmb: TSmallintField;
    QrPesqinfCanc_verAplic: TWideStringField;
    QrPesqinfCanc_dhRecbto: TDateTimeField;
    QrPesqinfCanc_nProt: TWideStringField;
    QrPesqinfCanc_digVal: TWideStringField;
    QrPesqinfCanc_cStat: TIntegerField;
    QrPesqinfCanc_xMotivo: TWideStringField;
    QrPesqinfCanc_cJust: TIntegerField;
    QrPesqinfCanc_xJust: TWideStringField;
    QrPesq_Ativo_: TSmallintField;
    QrPesqFisRegCad: TIntegerField;
    QrPesqCartEmiss: TIntegerField;
    QrPesqTabelaPrc: TIntegerField;
    QrPesqCondicaoPg: TIntegerField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    QrPesqFreteExtra: TFloatField;
    QrPesqSegurExtra: TFloatField;
    QrPesqICMSRec_pRedBC: TFloatField;
    QrPesqICMSRec_vBC: TFloatField;
    QrPesqICMSRec_pAliq: TFloatField;
    QrPesqICMSRec_vICMS: TFloatField;
    QrPesqIPIRec_pRedBC: TFloatField;
    QrPesqIPIRec_vBC: TFloatField;
    QrPesqIPIRec_pAliq: TFloatField;
    QrPesqIPIRec_vIPI: TFloatField;
    QrPesqPISRec_pRedBC: TFloatField;
    QrPesqPISRec_vBC: TFloatField;
    QrPesqPISRec_pAliq: TFloatField;
    QrPesqPISRec_vPIS: TFloatField;
    QrPesqCOFINSRec_pRedBC: TFloatField;
    QrPesqCOFINSRec_vBC: TFloatField;
    QrPesqCOFINSRec_pAliq: TFloatField;
    QrPesqCOFINSRec_vCOFINS: TFloatField;
    QrPesqDataFiscal: TDateField;
    QrPesqprotNFe_versao: TFloatField;
    QrPesqretCancNFe_versao: TFloatField;
    QrPesqSINTEGRA_ExpDeclNum: TWideStringField;
    QrPesqSINTEGRA_ExpDeclDta: TDateField;
    QrPesqSINTEGRA_ExpNat: TWideStringField;
    QrPesqSINTEGRA_ExpRegNum: TWideStringField;
    QrPesqSINTEGRA_ExpRegDta: TDateField;
    QrPesqSINTEGRA_ExpConhNum: TWideStringField;
    QrPesqSINTEGRA_ExpConhDta: TDateField;
    QrPesqSINTEGRA_ExpConhTip: TWideStringField;
    QrPesqSINTEGRA_ExpPais: TWideStringField;
    QrPesqSINTEGRA_ExpAverDta: TDateField;
    QrPesqCodInfoEmit: TIntegerField;
    QrPesqCodInfoDest: TIntegerField;
    QrPesqCriAForca: TSmallintField;
    QrPesqide_hSaiEnt: TTimeField;
    QrPesqide_dhCont: TDateTimeField;
    QrPesqide_xJust: TWideStringField;
    QrPesqemit_CRT: TSmallintField;
    QrPesqdest_email: TWideStringField;
    QrPesqVagao: TWideStringField;
    QrPesqBalsa: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtLocaliza: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeCabA_0000_Pesq: TFmNFeCabA_0000_Pesq;

implementation

uses UnMyObjects, Module, ModuleGeral, NFeCabA_0000, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabA_0000_Pesq.BtLocalizaClick(Sender: TObject);
begin
  FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value, QrPesqIDCtrl.Value);
end;

procedure TFmNFeCabA_0000_Pesq.BtPesquisaClick(Sender: TObject);
var
  Filial: Integer;
  Empresa, FatID, FatNum: String;
begin
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa') then Exit;
  Empresa := FormatFloat('0', DmodG.QrEmpresasCodigo.Value);
  FatID   := FormatFloat('0', EdFatID.ValueVariant);
  FatNum  := FormatFloat('0', EdFatNum.ValueVariant);
  //
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT * FROM nfecaba');
  QrPesq.SQL.Add('WHERE Empresa=' + Empresa);
  if FatID <> '0' then
    QrPesq.SQL.Add('AND FatID = ' + FatID);
  if FatNum <> '0' then
    QrPesq.SQL.Add('AND FatNum = ' + FatNum);
  UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmNFeCabA_0000_Pesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabA_0000_Pesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabA_0000_Pesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmNFeCabA_0000_Pesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabA_0000_Pesq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtLocaliza.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmNFeCabA_0000_Pesq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtLocaliza.Enabled := False;
end;

end.
