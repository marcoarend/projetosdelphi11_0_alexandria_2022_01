unit NFeItsU_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkEditCB, dmkMemo, dmkImage, UnDmkEnums;

type
  TFmNFeItsU_0000 = class(TForm)
    Panel1: TPanel;
    Panel25: TPanel;
    Panel3: TPanel;
    Label231: TLabel;
    Label232: TLabel;
    Label234: TLabel;
    Label235: TLabel;
    Label236: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    EdnItem: TdmkEdit;
    Edprod_NCM: TdmkEdit;
    Edprod_EXTIPI: TdmkEdit;
    Edprod_genero: TdmkEdit;
    Edprod_CFOP: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdFatID: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    Panel4: TPanel;
    GroupBox11: TGroupBox;
    Label242: TLabel;
    Label241: TLabel;
    Label240: TLabel;
    Label239: TLabel;
    Label233: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    Label249: TLabel;
    Edprod_qCom: TdmkEdit;
    Edprod_vProd: TdmkEdit;
    Edprod_vUnCom: TdmkEdit;
    Edprod_uCom: TdmkEdit;
    Edprod_cEAN: TdmkEdit;
    Edprod_vFrete: TdmkEdit;
    Edprod_vSeg: TdmkEdit;
    Edprod_vDesc: TdmkEdit;
    GroupBox12: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edprod_qTrib: TdmkEdit;
    Edprod_vUnTrib: TdmkEdit;
    Edprod_uTrib: TdmkEdit;
    Edprod_cEANTrib: TdmkEdit;
    PageControl3: TPageControl;
    TabSheet23: TTabSheet;
    Panel36: TPanel;
    Label296: TLabel;
    Label297: TLabel;
    Label298: TLabel;
    Label299: TLabel;
    Label300: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    EdISSQN_vBC: TdmkEdit;
    EdISSQN_vAliq: TdmkEdit;
    EdISSQN_vISSQN: TdmkEdit;
    TabSheet1: TTabSheet;
    QrGraSrv1: TmySQLQuery;
    QrGraSrv1Codigo: TIntegerField;
    QrGraSrv1CodUsu: TIntegerField;
    QrGraSrv1Nome: TWideStringField;
    QrGraSrv1infAdProd: TWideStringField;
    QrGraSrv1cListServ: TIntegerField;
    QrGraSrv1PrecoPc: TFloatField;
    QrGraSrv1PrecoM2: TFloatField;
    QrGraSrv1Precokg: TFloatField;
    CBISSQN_cListServ: TdmkDBLookupComboBox;
    CBISSQN_cMunFG: TdmkDBLookupComboBox;
    EdISSQN_cMunFG: TdmkEditCB;
    EdISSQN_cListServ: TdmkEditCB;
    MeInfAdProd: TdmkMemo;
    QrPrx: TmySQLQuery;
    QrPrxnItem: TIntegerField;
    REWarning: TRichEdit;
    QrGraSrv1Sigla: TWideStringField;
    QrGraSrv1UnidMed: TIntegerField;
    QrGraSrv1Grandeza: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edprod_cProdChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edprod_NCMChange(Sender: TObject);
    procedure Edprod_EXTIPIChange(Sender: TObject);
    procedure Edprod_generoChange(Sender: TObject);
    procedure Edprod_CFOPChange(Sender: TObject);
    procedure Edprod_cEANChange(Sender: TObject);
    procedure Edprod_uComChange(Sender: TObject);
    procedure Edprod_qComChange(Sender: TObject);
    procedure Edprod_vUnComChange(Sender: TObject);
    procedure Edprod_cEANTribChange(Sender: TObject);
    procedure Edprod_qTribChange(Sender: TObject);
    procedure Edprod_vUnTribChange(Sender: TObject);
    procedure Edprod_vUnTribExit(Sender: TObject);
    procedure EdISSQN_vBCChange(Sender: TObject);
    procedure EdISSQN_vAliqChange(Sender: TObject);
    procedure EdISSQN_cMunFGChange(Sender: TObject);
    procedure EdISSQN_cListServChange(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CBISSQN_cListServExit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValorBruto();
    procedure CalculaBaseCalculo();
    procedure CalculaISSQN();

  public
    { Public declarations }
  end;

  var
  FmNFeItsU_0000: TFmNFeItsU_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, GraAtrCad, UnInternalConsts, dmkGeral,
  GraSrv1, ModuleNFe_0000, UnMySQLCuringa, NFeCabA_0000, ModuleGeral,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeItsU_0000.BtOKClick(Sender: TObject);
  procedure ExcluiItem(Tabela:String; FatID, FatNum, Empresa, nItem: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + Tabela  + ' WHERE ');
    Dmod.QrUpd.SQL.Add('FatID=' + FormatFloat('0', FatID));
    Dmod.QrUpd.SQL.Add('AND FatNum=' + FormatFloat('0', FatNum));
    Dmod.QrUpd.SQL.Add('AND Empresa=' + FormatFloat('0', Empresa));
    Dmod.QrUpd.SQL.Add('AND nItem=' + FormatFloat('0', nItem));
    Dmod.QrUpd.ExecSQL;
  end;
var
  FatID, FatNum, Empresa, nItem: Integer;
  //
  prod_cProd, prod_cEAN, prod_xProd, prod_NCM, prod_EXTIPI, prod_uCom,
  prod_cEANTrib, prod_uTrib: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib,
  prod_vFrete, prod_vSeg, prod_vDesc: Double;
  prod_genero, prod_CFOP, Tem_IPI, _Ativo_, InfAdCuztm, EhServico, UsaSubsTrib:
  Integer;
  //
  ISSQN_vBC, ISSQN_vAliq, ISSQN_vISSQN: Double;
  ISSQN_cMunFG, ISSQN_cListServ: Integer;
  //
  InfAdProd: String;
begin
  FatID   := EdFatID.valueVariant;
  FatNum  := EdFatNum.valueVariant;
  Empresa := EdEmpresa.valueVariant;
  //
  prod_cProd    := Geral.SoNumero_TT(Edprod_cProd   .ValueVariant);
  prod_cEAN     := Geral.SoNumero_TT(Edprod_cEAN    .ValueVariant);
  prod_xProd    := Edprod_xProd   .ValueVariant;
  prod_NCM      := Geral.SoNumero_TT(Edprod_NCM     .ValueVariant);
  prod_EXTIPI   := Geral.SoNumero_TT(Edprod_EXTIPI  .ValueVariant);
  prod_genero   := Edprod_genero  .ValueVariant;
  prod_CFOP     := Edprod_CFOP    .ValueVariant;
  prod_uCom     := Edprod_uCom    .ValueVariant;
  prod_qCom     := Edprod_qCom    .ValueVariant;
  prod_vUnCom   := Edprod_vUnCom  .ValueVariant;
  prod_vProd    := Edprod_vProd   .ValueVariant;
  prod_cEANTrib := Geral.SoNumero_TT(Edprod_cEANTrib.ValueVariant);
  prod_uTrib    := Edprod_uTrib   .ValueVariant;
  prod_qTrib    := Edprod_qTrib   .ValueVariant;
  prod_vUnTrib  := Edprod_vUnTrib .ValueVariant;
  prod_vFrete   := Edprod_vFrete  .ValueVariant;
  prod_vSeg     := Edprod_vSeg    .ValueVariant;
  prod_vDesc    := Edprod_vDesc   .ValueVariant;
  //
  _Ativo_       := 1;
  InfAdCuztm    := 0;
  EhServico     := 1;
  UsaSubsTrib   := 0;
  Tem_IPI       := 0;
  //
  if ImgTipo.SQLType = stIns then
  begin
    QrPrx.Close;
    QrPrx.Params[00].AsInteger := FatID;
    QrPrx.Params[01].AsInteger := FatNum;
    QrPrx.Params[02].AsInteger := Empresa;
    // ini 2022-02-20
    //QrPrx. O p e n ;
    UnDmkDAC_PF.AbreQuery(QrPrx, Dmod.MyDB);
    // fim 2022-02-20
    EdnItem.ValueVariant := QrPrxnItem.Value + 1;
  end;
  nItem := EdnItem.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsi', False, [
  'prod_cProd', 'prod_cEAN', 'prod_xProd',
  'prod_NCM', 'prod_EXTIPI', 'prod_genero',
  'prod_CFOP', 'prod_uCom', 'prod_qCom',
  'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
  'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
  'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
  'Tem_IPI', '_Ativo_', 'InfAdCuztm',
  'EhServico', 'UsaSubsTrib'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  prod_cProd, prod_cEAN, prod_xProd,
  prod_NCM, prod_EXTIPI, prod_genero,
  prod_CFOP, prod_uCom, prod_qCom,
  prod_vUnCom, prod_vProd, prod_cEANTrib,
  prod_uTrib, prod_qTrib, prod_vUnTrib,
  prod_vFrete, prod_vSeg, prod_vDesc,
  Tem_IPI, _Ativo_, InfAdCuztm,
  EhServico, UsaSubsTrib], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
    //
    ExcluiItem('nfeitsu', FatID, FatNum, Empresa, nItem);
    //
    ISSQN_vBC       := EdISSQN_vBC      .ValueVariant;
    ISSQN_vAliq     := EdISSQN_vAliq    .ValueVariant;
    ISSQN_vISSQN    := EdISSQN_vISSQN   .ValueVariant;
    ISSQN_cMunFG    := EdISSQN_cMunFG   .ValueVariant;
    ISSQN_cListServ := EdISSQN_cListServ.ValueVariant;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsu', False, [
    'ISSQN_vBC', 'ISSQN_vAliq', 'ISSQN_vISSQN',
    'ISSQN_cMunFG', 'ISSQN_cListServ', '_Ativo_'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    ISSQN_vBC, ISSQN_vAliq, ISSQN_vISSQN,
    ISSQN_cMunFG, ISSQN_cListServ, _Ativo_], [
    FatID, FatNum, Empresa, nItem], True);

    //
    ExcluiItem('nfeitsv', FatID, FatNum, Empresa, nItem);
    //
    InfAdProd := MeInfAdProd.Text;
    if Trim(InfAdProd) <> '' then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsv', False, [
      'InfAdProd', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      InfAdProd, _Ativo_], [
      FatID, FatNum, Empresa, nItem], True);
    end;
    //  Parei aqui!! 2023-06-14
    ExcluiItem('nfeitsva', FatID, FatNum, Empresa, nItem);
    //
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, LaAviso1, LaAviso2, REWarning);
    FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value, FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
    Close;
  end;
end;

procedure TFmNFeItsU_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsU_0000.CalculaBaseCalculo;
var
  Prc, Qtd: Double;
begin
  Prc := Edprod_vUnTrib.ValueVariant;
  Qtd := Edprod_qTrib.ValueVariant;
  //
  EdISSQN_vBC.ValueVariant := Round(Prc * Qtd * 100) / 100;
end;

procedure TFmNFeItsU_0000.CalculaISSQN;
var
  Bas, Per: Double;
begin
  Bas := EdISSQN_vBC.ValueVariant;
  Per := EdISSQN_vAliq.ValueVariant;
  //
  EdISSQN_vISSQN.ValueVariant := Round(Bas * Per) / 100;
end;

procedure TFmNFeItsU_0000.CalculaValorBruto();
var
  Prc, Qtd: Double;
begin
  Prc := Edprod_vUnCom.ValueVariant;
  Qtd := Edprod_qCom.ValueVariant;
  //
  Edprod_vProd.ValueVariant := Round(Prc * Qtd * 100) / 100;
end;

procedure TFmNFeItsU_0000.CBISSQN_cListServExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 1;
  MeInfAdProd.SetFocus;
end;

procedure TFmNFeItsU_0000.EdISSQN_cListServChange(Sender: TObject);
begin
{
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', EdISSQN_cMunFG.ValueVariant, []) then
    EdISSQN_xMunFG.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    EdISSQN_xMunFG.Text := '';
}
end;

procedure TFmNFeItsU_0000.EdISSQN_cMunFGChange(Sender: TObject);
begin
{
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', EdISSQN_cMunFG.ValueVariant, []) then
    EdISSQN_xMunFG.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    EdISSQN_xMunFG.Text := '';
}
end;

procedure TFmNFeItsU_0000.EdISSQN_vAliqChange(Sender: TObject);
begin
  CalculaISSQN();
end;

procedure TFmNFeItsU_0000.EdISSQN_vBCChange(Sender: TObject);
begin
  CalculaISSQN();
end;

procedure TFmNFeItsU_0000.Edprod_cEANChange(Sender: TObject);
begin
  if Edprod_cEAN.Text <> '' then
    Edprod_cEAN.Text := '';
end;

procedure TFmNFeItsU_0000.Edprod_cEANTribChange(Sender: TObject);
begin
  if Edprod_cEANTrib.Text <> '' then
    Edprod_cEANTrib.Text := '';
end;

procedure TFmNFeItsU_0000.Edprod_CFOPChange(Sender: TObject);
begin
//??
end;

procedure TFmNFeItsU_0000.Edprod_cProdChange(Sender: TObject);
const
  EhServico  = True;
  EhSubsTrib = False;
  Item_MadeBy = 0;
var
  CodUsu, Empresa, Cliente, RegrFiscal,
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  DOC, CFOP: String;
  ICMS_Aliq: Double;
begin
  CodUsu := Geral.IMV(Edprod_cProd.Text);
  if CodUsu <> 0 then
  begin
    if QrGraSrv1.Locate('CodUsu', CodUsu, []) then
    begin
      Edprod_cProd.ValueVariant      := QrGraSrv1CodUsu.Value;
      Edprod_xProd.ValueVariant      := QrGraSrv1Nome.Value;
      Edprod_uCom.ValueVariant       := QrGraSrv1Sigla.Value;
      EdISSQN_cListServ.ValueVariant := QrGraSrv1cListServ.Value;
      case QrGraSrv1Grandeza.Value of
        0: Edprod_vUnCom.ValueVariant     := QrGraSrv1PrecoPc.Value;
        1: Edprod_vUnCom.ValueVariant     := QrGraSrv1PrecoM2.Value;
        2: Edprod_vUnCom.ValueVariant     := QrGraSrv1Precokg.Value;
      end;

      // Parei aqui
      // CFOP ??
      // CFOP, ICMS, PIS, COFINS
      if Length(FmNFeCabA_0000.QrNFeCabAdest_CNPJ.Value) = 14 then
        DOC := FmNFeCabA_0000.QrNFeCabAdest_CNPJ.Value
      else
        DOC := FmNFeCabA_0000.QrNFeCabAdest_CPF.Value;
      if not DmodG.ObtemEntidadeDeCNPJCFP(DOC, Cliente) then
        Exit;
      Empresa    := FmNFeCabA_0000.QrNFeCabAEmpresa.Value;
      RegrFiscal := FmNFeCabA_0000.QrNFeCabAFisRegCad.Value;
      //
      if DmNFe_0000.ObtemNumeroCFOP_Emissao('', [0], Empresa, Cliente, Item_MadeBy,
        RegrFiscal, EhServico, EhSubsTrib, CFOP, ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF,
        CFOP_Proprio, CFOP_SubsTrib) then
      begin
        Edprod_CFOP.ValueVariant       := Geral.IMV(Geral.SoNumero_TT(CFOP));
        if FmNFeCabA_0000.QrNFeCabAISS_Usa.Value = 1 then
          EdISSQN_vAliq.ValueVariant := FmNFeCabA_0000.QrNFeCabAISS_Alq.Value
        else
          EdISSQN_vAliq.ValueVariant := 0;
      end;
    end;
  end;
end;

procedure TFmNFeItsU_0000.Edprod_EXTIPIChange(Sender: TObject);
begin
  if Edprod_EXTIPI.Text <> '' then
    Edprod_EXTIPI.Text := '';
end;

procedure TFmNFeItsU_0000.Edprod_generoChange(Sender: TObject);
begin
  if Edprod_genero.ValueVariant <> 0 then
    Edprod_genero.ValueVariant := 0;
end;

procedure TFmNFeItsU_0000.Edprod_NCMChange(Sender: TObject);
begin
  if Edprod_NCM.Text <> '' then
    Edprod_NCM.Text := '';
end;

procedure TFmNFeItsU_0000.Edprod_qComChange(Sender: TObject);
begin
  Edprod_qTrib.ValueVariant := Edprod_qCom.ValueVariant;
  CalculaValorBruto();
end;

procedure TFmNFeItsU_0000.Edprod_qTribChange(Sender: TObject);
begin
  CalculaBaseCalculo();
end;

procedure TFmNFeItsU_0000.Edprod_uComChange(Sender: TObject);
begin
  Edprod_uTrib.Text := Edprod_uCom.Text;
end;

procedure TFmNFeItsU_0000.Edprod_vUnComChange(Sender: TObject);
begin
  Edprod_vUnTrib.ValueVariant := Edprod_vUnCom.ValueVariant;
  CalculaValorBruto();
end;

procedure TFmNFeItsU_0000.Edprod_vUnTribChange(Sender: TObject);
begin
  CalculaBaseCalculo();
end;

procedure TFmNFeItsU_0000.Edprod_vUnTribExit(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  EdISSQN_vBC.SetFocus;
end;

procedure TFmNFeItsU_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsU_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  // ini 2022-02-20
  //QrGraSrv1. O p e n ;
  UnDmkDAC_PF.AbreQuery(QrGraSrv1, Dmod.MyDB);
  // fim 2022-02-20
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_Munici, DModG.AllID_DB);
  UMyMod.AbreQuery(DmNFe_0000.QrListServ, DModG.AllID_DB);
end;

procedure TFmNFeItsU_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeItsU_0000.SpeedButton1Click(Sender: TObject);
begin
  //ver como vai fazer o dia que usar
  Geral.MB_Aviso('Em desenvolvimento! Contate a Dermatek!');
  EXIT;
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraSrv1, FmGraSrv1, afmoNegarComAviso) then
  begin
    FmGraSrv1.ShowModal;
    FmGraSrv1.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      if QrGraSrv1.Locate('Codigo', VAR_CADASTRO, []) then
        Edprod_cProd.ValueVariant := QrGraSrv1CodUsu.Value;
    end;
  end;
end;

procedure TFmNFeItsU_0000.SpeedButton2Click(Sender: TObject);
var
  Pesq: Integer;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'listserv', Dmod.MyDB, '', True);
  if Pesq <> 0 then
    EdISSQN_cListServ.ValueVariant := VAR_CODIGO;
end;

procedure TFmNFeItsU_0000.SpeedButton3Click(Sender: TObject);
var
  Pesq: Integer;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'dtb_munici', DModG.AllID_DB, '', True);
  if Pesq <> 0 then
    EdISSQN_cMunFG.ValueVariant := VAR_CODIGO;
end;

end.
