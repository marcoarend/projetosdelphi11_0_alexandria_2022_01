object FmNFeCabXVol_0000: TFmNFeCabXVol_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-015 :: Volume'
  ClientHeight = 375
  ClientWidth = 439
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 439
    Height = 219
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 456
    ExplicitHeight = 64
    object Label4: TLabel
      Left = 12
      Top = 8
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label5: TLabel
      Left = 12
      Top = 48
      Width = 41
      Height = 13
      Caption = 'Esp'#233'cie:'
    end
    object Label6: TLabel
      Left = 12
      Top = 88
      Width = 33
      Height = 13
      Caption = 'Marca:'
    end
    object Label7: TLabel
      Left = 12
      Top = 128
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label8: TLabel
      Left = 12
      Top = 168
      Width = 51
      Height = 13
      Caption = 'Peso buto:'
    end
    object Label9: TLabel
      Left = 92
      Top = 168
      Width = 62
      Height = 13
      Caption = 'Peso l'#237'quido:'
    end
    object EdqVol: TdmkEdit
      Left = 12
      Top = 24
      Width = 86
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'qVol'
      UpdCampo = 'qVol'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEsp: TdmkEdit
      Left = 12
      Top = 64
      Width = 236
      Height = 21
      MaxLength = 60
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'esp'
      UpdCampo = 'esp'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdMarca: TdmkEdit
      Left = 12
      Top = 104
      Width = 236
      Height = 21
      MaxLength = 60
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'marca'
      UpdCampo = 'marca'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdnVol: TdmkEdit
      Left = 12
      Top = 144
      Width = 236
      Height = 21
      MaxLength = 60
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'nVol'
      UpdCampo = 'nVol'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdpesoB: TdmkEdit
      Left = 12
      Top = 184
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'pesoB'
      UpdCampo = 'pesoB'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdpesoL: TdmkEdit
      Left = 92
      Top = 184
      Width = 81
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'pesoL'
      UpdCampo = 'pesoL'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 439
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 456
    object GB_R: TGroupBox
      Left = 391
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 408
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 343
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 360
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 89
        Height = 32
        Caption = 'Volume'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 89
        Height = 32
        Caption = 'Volume'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 89
        Height = 32
        Caption = 'Volume'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 267
    Width = 439
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 112
    ExplicitWidth = 456
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 435
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 452
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 311
    Width = 439
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 156
    ExplicitWidth = 456
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 435
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 452
      object PnSaiDesis: TPanel
        Left = 291
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 308
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 128
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 8
        Top = 15
        Width = 113
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 2
      end
    end
  end
end
