unit NFeCabYAll_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables;

type
  THackDBGrid = class(TDBGrid);
  TFmNFeCabYAll_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    TbTmpNFeCabY: TmySQLTable;
    DsTmpNFeCabY: TDataSource;
    DBGTmpNFeCabY: TDBGrid;
    TbTmpNFeCabYControle: TIntegerField;
    TbTmpNFeCabYnDup: TWideStringField;
    TbTmpNFeCabYdVenc: TDateField;
    TbTmpNFeCabYvDup: TFloatField;
    TbTmpNFeCabYFatID: TIntegerField;
    TbTmpNFeCabYFatNum: TIntegerField;
    TbTmpNFeCabYEmpresa: TIntegerField;
    TbTmpNFeCabYSub: TIntegerField;
    TbTmpNFeCabYLancto: TIntegerField;
    TbTmpNFeCabYFatParcela: TIntegerField;
    TbTmpNFeCabYDescricao: TWideStringField;
    Panel5: TPanel;
    LaTotNFe: TLabel;
    LaTotFatu: TLabel;
    QrTmpNFeCabYTot: TmySQLQuery;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    TbTmpNFeCabYGenero: TIntegerField;
    TbTmpNFeCabYGenero_TXT: TWideStringField;
    LaDiferenca: TLabel;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGTmpNFeCabYKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbTmpNFeCabYBeforePost(DataSet: TDataSet);
    procedure TbTmpNFeCabYAfterPost(DataSet: TDataSet);
    procedure TbTmpNFeCabYvDupSetText(Sender: TField; const Text: string);
    procedure TbTmpNFeCabYdVencSetText(Sender: TField; const Text: string);
    procedure TbTmpNFeCabYNewRecord(DataSet: TDataSet);
    procedure TbTmpNFeCabYAfterDelete(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    FFechar: Boolean;
    procedure ReopenTmpNFeCabY(ReabreTable: Boolean);
    procedure ReopenGenero();
  public
    { Public declarations }
    FTmpTable: String;
    FFatID, FFatNum, FEmpresa, FFinanceiro, FTipoCart, FCarteira, FSerieNF,
    FNotaFiscal, FRepresen, FCliente: Integer;
    FNFeVal, FNFeFatu, FJurosMes: Double;
    FDataFat: TDateTime;
  end;

  var
  FmNFeCabYAll_0000: TFmNFeCabYAll_0000;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, MeuDBUses, UnDmkProcFunc,
  ModuleNFe_0000, UMySQLModule, UnFinanceiro, NFe_PF;

{$R *.DFM}

procedure TFmNFeCabYAll_0000.BtOKClick(Sender: TObject);

  function AtualizaNFeCabY(): Integer;
  var
    nDup, dVenc: String;
    vDup: Double;
    FatParcela, Controle: Integer;
  begin
    Result := 0;
    //
    Controle   := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
    nDup       := TbTmpNFeCabYnDup.Value;
    dVenc      := Geral.FDT(TbTmpNFeCabYdVenc.Value, 01);
    vDup       := TbTmpNFeCabYvDup.Value;
    FatParcela := TbTmpNFeCabYFatParcela.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaby', False,
      ['nDup', 'dVenc', 'vDup'],
      ['FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'],
      [nDup, dVenc, vDup],
      [FFatID, FFatNum, FatParcela, FEmpresa, Controle], True)
    then
      Result := Controle;
  end;

const
  VerificaCliInt = True;
var
  Lancto, Controle, FatParcela: Integer;
{$IfDef DEFINE_VARLCT}
  Filial: Integer;
  TabLctA: String;
{$EndIf}
  GenCtbD, GenCtbC: Integer;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) (*and (Financeiro > 0)*) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  //
  { ini 2022-12-13
  if (FNFeFatu = 0) or (FNFeVal = 0) or (FNFeFatu <> FNFeVal) then
  begin
    Geral.MB_Aviso('O total das faturas deve ser igual ao total da NF-e e diferente de zero!');
    Exit;
  end;
  fim 2022-12-13
  }
  if (TbTmpNFeCabY.State <> dsInactive) and (TbTmpNFeCabY.RecordCount > 0) then
  begin
    try
      TbTmpNFeCabY.DisableControls;
      //
      TbTmpNFeCabY.First;
      //
      while not TbTmpNFeCabY.Eof do
      begin
        FatParcela := TbTmpNFeCabYFatParcela.Value;
        //
        if MyObjects.FIC(TbTmpNFeCabYnDup.Value = '', nil,
          'N�mero da duplicata n�o informada para a parcela '+ Geral.FF0(FatParcela) +'!')
        then
          Exit;
        if MyObjects.FIC(TbTmpNFeCabYdVenc.Value < 2, nil,
          'Vencimento n�o informado para a parcela ' + Geral.FF0(FatParcela) + '!')
        then
          Exit;
        if MyObjects.FIC(TbTmpNFeCabYvDup.Value = 0, nil,
          'Valor da duplicata n�o informado para a parcela ' + Geral.FF0(FatParcela) + '!')
        then
          Exit;
        if MyObjects.FIC(TbTmpNFeCabYGenero.Value = 0, nil,
          'Conta (plano de contas) n�o foi informada para a parcela ' + Geral.FF0(FatParcela) + '!')
        then
          Exit;
        if MyObjects.FIC(TbTmpNFeCabYDescricao.Value = '', nil,
          'Descri��o n�o foi informada para a parcela ' + Geral.FF0(FatParcela) + '!')
        then
          Exit;
        //
        TbTmpNFeCabY.Next;
      end;
    finally
      TbTmpNFeCabY.EnableControls;
    end;
    try
      TbTmpNFeCabY.DisableControls;
      //
      //Exclui todos lan�amentos para atualizar a tabela
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfecaby WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FFatID;
      Dmod.QrUpd.Params[01].AsInteger := FFatNum;
      Dmod.QrUpd.Params[02].AsInteger := FEmpresa;
      Dmod.QrUpd.ExecSQL;
      //
      Filial := DModG.ObtemFilialDeEntidade(FEmpresa);
      Lancto := 0;
      //
      if (FFatID <> 0) and (FFatNum <> 0) then
      begin
{$IfDef DEFINE_VARLCT}
        TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
        //
        UFinanceiro.ExcluiLct_FatNum(nil, FFatID, FFatNum,
          FEmpresa, 0, dmkPF.MotivDel_ValidaCodigo(311), False, TabLctA);
{$Else}
        UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FatID, OriCodi, Empresa, 0, False);
{$EndIf}
      end;
      //
      TbTmpNFeCabY.First;
      //
      PB1.Position := 0;
      PB1.Max      := TbTmpNFeCabY.RecordCount;
      //
      while not TbTmpNFeCabY.Eof do
      begin
        Controle := AtualizaNFeCabY;
        //
        if (Controle <> 0) and (FFinanceiro in [1, 2]) then
        begin
          Lancto := UnNFe_PF.IncluiLanctoFaturasNFe(TbTmpNFeCabYvDup.Value,
                      FJurosMes, FDataFat, TbTmpNFeCabYdVenc.Value,
                      TbTmpNFeCabYnDup.Value, TbTmpNFeCabYDescricao.Value,
                      FTipoCart, FCarteira, TbTmpNFeCabYGenero.Value, (*GenCtb*)0, FEmpresa,
                      TbTmpNFeCabYFatParcela.Value, FNotaFiscal, FRepresen,
                      FFinanceiro, GenCtbD, GenCtbC, Filial, FCliente, FFatNum, Geral.FF0(FSerieNF),
                      VerificaCliInt);
          //
          if Lancto <> 0 then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaby', False,
              ['Lancto'],
              ['FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'],
              [Lancto],
              [FFatID, FFatNum, TbTmpNFeCabYFatParcela.Value, FEmpresa, Controle], True)
          end else
          begin
            Geral.MB_Erro('Falha ao criar lan�amento financeiro!');
            Exit;
          end;
        end else
        if Controle = 0 then
        begin
          Geral.MB_Erro('Falha ao atualizar faturas!');
          Exit;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.UPdate;
        Application.ProcessMessages;
        //
        TbTmpNFeCabY.Next;
      end;
    finally
      PB1.Position := 0;
      //
      TbTmpNFeCabY.EnableControls;
    end;
  end;
  FFechar := True;
  Close;
end;

procedure TFmNFeCabYAll_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabYAll_0000.DBGTmpNFeCabYKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Campo: String;
begin
  if key = VK_F7 then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Genero_TXT') then
    begin
      FmMeuDBUses.PesquisaNome('contas', 'Nome', 'Codigo', '');
      //
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          TbTmpNFeCabY.Edit;
          TbTmpNFeCabYGenero.Value := VAR_CADASTRO;
          TbTmpNFeCabY.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmNFeCabYAll_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabYAll_0000.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if (CanClose = True) and (FFechar = False) then
  begin
    if Geral.MB_Pergunta('Deseja sair sem salvar?' + sLineBreak +
      'ATEN��O: Todas as modifica��es ser�o perdidas!') <> ID_YES
    then
      CanClose := False;
  end;
end;

procedure TFmNFeCabYAll_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGTmpNFeCabY.DataSource := DsTmpNFeCabY;
  //
  FFechar  := False;
  FNFeVal  := 0;
  FNFeFatu := 0;
  FFatID   := 0;
  FFatNum  := 0;
  FEmpresa := 0;
end;

procedure TFmNFeCabYAll_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabYAll_0000.FormShow(Sender: TObject);
begin
  ReopenGenero();
  ReopenTmpNFeCabY(True);
end;

procedure TFmNFeCabYAll_0000.ReopenGenero;
var
  SQL_Compl: String;
begin
  if FFinanceiro = 1 then
    SQL_Compl := 'AND Credito="V" '
  else
    SQL_Compl := 'AND Debito="V" ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM contas ',
    'WHERE Codigo > 0 ',
    SQL_Compl,
    'AND Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmNFeCabYAll_0000.ReopenTmpNFeCabY(ReabreTable: Boolean);
const
  CO_TotNFe_Txt = 'Total NF-e: ';
  CO_TotFatu_Txt = 'Total faturas: ';
  CO_Diferenca_Txt = 'Diferen�a: ';
var
  Cor: TColor;
begin
  if ReabreTable then
  begin
    TbTmpNFeCabY.Filtered  := True;
    TbTmpNFeCabY.Filter    := '(FatID=' + Geral.FF0(FFatID) + ') AND (FatNum=' +
                              Geral.FF0(FFatNum) + ') AND (Empresa=' +
                              Geral.FF0(FEmpresa) + ')';
    TbTmpNFeCabY.TableName := FTmpTable;
    UnDmkDAC_PF.AbreTable(TbTmpNFeCabY, DModG.MyPID_DB);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTmpNFeCabYTot, DModG.MyPID_DB, [
    'SELECT SUM(vDup) vDup ',
    'FROM ' + FTmpTable,
    '']);
  //
  FNFeFatu := QrTmpNFeCabYTot.FieldByName('vDup').AsFloat;
  //
  if FNFeFatu = FNFeVal then
    Cor := clGreen
  else
    Cor := clRed;
  //
  LaTotNFe.Caption    := CO_TotNFe_Txt + Geral.FFT(FNFeVal, 2, siPositivo);
  LaTotFatu.Caption   := CO_TotFatu_Txt + Geral.FFT(FNFeFatu, 2, siPositivo);
  LaDiferenca.Caption := CO_Diferenca_Txt + Geral.FFT(FNFeVal - FNFeFatu, 2, siNegativo);
  //
  LaTotNFe.Font.Color    := Cor;
  LaTotFatu.Font.Color   := Cor;
  LaDiferenca.Font.Color := Cor;
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYAfterDelete(DataSet: TDataSet);
begin
  ReopenTmpNFeCabY(False);
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYAfterPost(DataSet: TDataSet);
begin
  ReopenTmpNFeCabY(False);
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYBeforePost(DataSet: TDataSet);
var
  Qry: TMySQLQuery;
  FatParcela, Genero: Integer;
  nDup, Descricao: String;
  vDup: Double;
begin
  FatParcela := TbTmpNFeCabYFatParcela.Value;
  nDup       := TbTmpNFeCabYnDup.Value;
  vDup       := TbTmpNFeCabYvDup.Value;
  Genero     := TbTmpNFeCabYGenero.Value;
  Descricao  := TbTmpNFeCabYDescricao.Value;
  //
  if MyObjects.FIC(FatParcela = 0, nil, 'Informe o n�mero da parcela!') then
  begin
    Abort;
    Exit;
  end;
  if MyObjects.FIC(nDup = '', nil, 'Informe o n�mero do duplicata!') then
  begin
    Abort;
    Exit;
  end;
  if MyObjects.FIC(Genero = 0, nil, 'Informe a conta do plano de contas!') then
  begin
    Abort;
    Exit;
  end;
  if MyObjects.FIC(Descricao = '', nil, 'Informe um texto para descri��o!') then
  begin
    Abort;
    Exit;
  end;
  //
  Qry := TmySQLQuery.Create(DModG.MyPID_DB.Owner);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'SELECT FatParcela ',
      'FROM ' + FTmpTable,
      'WHERE FatParcela=' + Geral.FF0(FatParcela),
      'AND Controle<>' + Geral.FF0(TbTmpNFeCabYControle.Value),
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este n�mero de parcela j� foi informado!');
      Abort;
    end;
  finally
    Qry.Free;
  end;
  //
  TbTmpNFeCabYFatID.Value    := FFatID;
  TbTmpNFeCabYFatNum.Value   := FFatNum;
  TbTmpNFeCabYEmpresa.Value  := FEmpresa;
  TbTmpNFeCabYControle.Value := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYdVencSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYNewRecord(DataSet: TDataSet);
var
  Parcela: Integer;
  nDup: String;
begin
  DmNFe_0000.QrParcela.Close;
  DmNFe_0000.QrParcela.Params[00].AsInteger := FFatID;
  DmNFe_0000.QrParcela.Params[01].AsInteger := FFatNum;
  DmNFe_0000.QrParcela.Params[02].AsInteger := FEmpresa;
  // ini 2022-02-20
  //DmNFe_0000.QrParcela. O p e n ;
  UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrParcela, Dmod.MyDB);
  // fim 2022-02-20
  //
  Parcela := DmNFe_0000.QrParcelaFatParcela.Value + 1;
  nDup    := 'FA-' + FormatFloat('000000', FFatNum) + '/' + FormatFloat('000', Parcela);
  //
  TbTmpNFeCabYFatParcela.Value := Parcela;
  TbTmpNFeCabYnDup.Value       := nDup;
end;

procedure TFmNFeCabYAll_0000.TbTmpNFeCabYvDupSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

end.
