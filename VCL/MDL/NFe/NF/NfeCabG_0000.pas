unit NfeCabG_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, dmkLabel, dmkEdit, DB,
  mySQLDbTables, dmkEditCB, DBCtrls, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmNFeCabG_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Panel23: TPanel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label218: TLabel;
    SbMunicipio: TSpeedButton;
    Label120: TLabel;
    Label136: TLabel;
    Label1: TLabel;
    Label94: TLabel;
    Label2: TLabel;
    SbPais: TSpeedButton;
    Label175: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Edentrega_xLgr: TdmkEdit;
    Edentrega_nro: TdmkEdit;
    Edentrega_xCpl: TdmkEdit;
    Edentrega_xBairro: TdmkEdit;
    Edentrega_xMun: TdmkEdit;
    Edentrega_UF: TdmkEdit;
    Edentrega_cMun: TdmkEditCB;
    Edentrega_CNPJ: TdmkEdit;
    Edentrega_CPF: TdmkEdit;
    Edentrega_xNome: TdmkEdit;
    Edentrega_CEP: TdmkEdit;
    BtCEP_L: TBitBtn;
    Edentrega_cPais: TdmkEditCB;
    Edentrega_xPais: TdmkEdit;
    Edentrega_fone: TdmkEdit;
    Edentrega_email: TdmkEdit;
    Edentrega_IE: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbMunicipioClick(Sender: TObject);
    procedure SbPaisClick(Sender: TObject);
    procedure Edentrega_cPaisChange(Sender: TObject);
    procedure Edentrega_cMunChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeCabG_0000: TFmNFeCabG_0000;

implementation

uses UnMyObjects, ModuleNFe_0000, Module, UnMySQLCuringa, UnInternalConsts,
UMySQLModule, NFeCabA_0000, ModuleGeral;

{$R *.DFM}

procedure TFmNFeCabG_0000.BtOKClick(Sender: TObject);
var
  entrega_CNPJ, entrega_xLgr, entrega_nro, entrega_xCpl, entrega_xBairro,
  entrega_xMun, entrega_UF, entrega_CPF, entrega_xNome, entrega_xPais,
  entrega_fone, entrega_email, entrega_IE: String;
  FatID, FatNum, Empresa, entrega_CEP, entrega_cMun, entrega_cPais: Integer;
  SQLType: TSQLType;
begin
  SQLType          := ImgTipo.SQLType;
  FatID            := FmNFeCabA_0000.QrNFeCabAFatID.Value;
  FatNum           := FmNFeCabA_0000.QrNFeCabAFatNum.Value;
  Empresa          := FmNFeCabA_0000.QrNFeCabAEmpresa.Value;
  entrega_CNPJ    := Geral.SoNumero_TT(Edentrega_CNPJ.ValueVariant);
  entrega_CPF     := Geral.SoNumero_TT(Edentrega_CPF.ValueVariant);
  entrega_xNome   := Edentrega_xNome.ValueVariant;
  entrega_xLgr    := Edentrega_xLgr.ValueVariant;
  entrega_nro     := Edentrega_nro.ValueVariant;
  entrega_xCpl    := Edentrega_xCpl.ValueVariant;
  entrega_xBairro := Edentrega_xBairro.ValueVariant;
  entrega_cMun    := Edentrega_cMun.ValueVariant;
  entrega_xMun    := Edentrega_xMun.ValueVariant;
  entrega_UF      := Edentrega_UF.ValueVariant;
  //entrega_CEP     := Geral.SoNumero_TT(Edentrega_CEP.ValueVariant);
  entrega_CEP     := Edentrega_CEP.ValueVariant;
  entrega_cPais   := Edentrega_cPais.ValueVariant;
  entrega_xPais   := Edentrega_xPais.ValueVariant;
  entrega_fone    := Edentrega_fone.ValueVariant;
  entrega_email   := Edentrega_email.ValueVariant;
  entrega_IE      := Edentrega_IE.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabg', False, [
  'entrega_CNPJ', 'entrega_xLgr', 'entrega_nro',
  'entrega_xCpl', 'entrega_xBairro', 'entrega_cMun',
  'entrega_xMun', 'entrega_UF', 'entrega_CPF',
  'entrega_xNome', 'entrega_CEP', 'entrega_cPais',
  'entrega_xPais', 'entrega_fone', 'entrega_email',
  'entrega_IE'], [
  'FatID', 'FatNum', 'Empresa'], [
  entrega_CNPJ, entrega_xLgr, entrega_nro,
  entrega_xCpl, entrega_xBairro, entrega_cMun,
  entrega_xMun, entrega_UF, entrega_CPF,
  entrega_xNome, entrega_CEP, entrega_cPais,
  entrega_xPais, entrega_fone, entrega_email,
  entrega_IE], [
  FatID, FatNum, Empresa], True) then
  begin
    FmNFeCabA_0000.ReopenNFeCabG(0);
    Close;
  end;
end;

procedure TFmNFeCabG_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabG_0000.Edentrega_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edentrega_cMun.ValueVariant, []) then
    Edentrega_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
end;

procedure TFmNFeCabG_0000.Edentrega_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Edentrega_cPais.ValueVariant, []) then
    Edentrega_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value;
end;

procedure TFmNFeCabG_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabG_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeCabG_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCabG_0000.SbMunicipioClick(Sender: TObject);
var
  Pesq: Integer;
  UF: String;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'dtb_munici', DModG.AllID_DB, '', False);
  if Pesq <> 0 then
  begin
    Edentrega_cMun.ValueVariant := VAR_CODIGO;
    UF := DmNFe_0000.ObtemSiglaUFdeDTBdeCidade(VAR_CODIGO);
    if UF <> '' then
      Edentrega_UF.Text := UF;
  end;
end;

procedure TFmNFeCabG_0000.SbPaisClick(Sender: TObject);
var
  Pesq: Integer;
begin
  Pesq := CuringaLoc.CriaForm('Codigo', 'Nome', 'bacen_pais', DModG.AllID_DB, '', False);
  if Pesq <> 0 then
    Edentrega_cPais.ValueVariant := VAR_CODIGO;
end;

end.
