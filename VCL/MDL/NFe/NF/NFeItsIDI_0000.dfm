object FmNFeItsIDI_0000: TFmNFeItsIDI_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-008 :: Declara'#231#227'o de Importa'#231#227'o'
  ClientHeight = 403
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 258
    Width = 547
    Height = 37
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 16
    ExplicitTop = 262
    object CkContinuar: TCheckBox
      Left = 4
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 197
    Width = 547
    Height = 61
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 168
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Item:'
      FocusControl = DBEdnItem
    end
    object Label3: TLabel
      Left = 224
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 304
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdit3
    end
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdFatID
    end
    object Label10: TLabel
      Left = 40
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdFatNum
    end
    object Label13: TLabel
      Left = 120
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object DBEdnItem: TdmkDBEdit
      Left = 168
      Top = 32
      Width = 53
      Height = 21
      DataField = 'nItem'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 0
      UpdCampo = 'nItem'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit2: TDBEdit
      Left = 224
      Top = 32
      Width = 76
      Height = 21
      DataField = 'prod_cProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 304
      Top = 32
      Width = 232
      Height = 21
      DataField = 'prod_xProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 2
    end
    object DBEdFatID: TdmkDBEdit
      Left = 8
      Top = 32
      Width = 28
      Height = 21
      DataField = 'FatID'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 3
      UpdCampo = 'FatID'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdFatNum: TdmkDBEdit
      Left = 40
      Top = 32
      Width = 76
      Height = 21
      DataField = 'FatNum'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 4
      UpdCampo = 'FatNum'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 120
      Top = 32
      Width = 45
      Height = 21
      DataField = 'Empresa'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 5
      UpdCampo = 'Empresa'
      UpdType = utYes
      Alignment = taLeftJustify
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 48
    Width = 547
    Height = 149
    Align = alTop
    Caption = ' Dados da declara'#231#227'o da importa'#231#227'o:'
    TabOrder = 1
    object Label2: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label8: TLabel
      Left = 96
      Top = 20
      Width = 72
      Height = 13
      Caption = 'N'#186' DI/DSI/DA:'
    end
    object Label6: TLabel
      Left = 204
      Top = 20
      Width = 120
      Height = 13
      Caption = 'Data registro DI/DSI/DA:'
    end
    object Label7: TLabel
      Left = 12
      Top = 60
      Width = 111
      Height = 13
      Caption = 'Local de desembara'#231'o:'
    end
    object Label9: TLabel
      Left = 504
      Top = 60
      Width = 21
      Height = 13
      Caption = 'UF*:'
    end
    object Label11: TLabel
      Left = 12
      Top = 100
      Width = 108
      Height = 13
      Caption = 'Data do desembara'#231'o:'
    end
    object Label12: TLabel
      Left = 128
      Top = 100
      Width = 366
      Height = 13
      Caption = 
        'C'#243'digo do exportador (usar o c'#243'digo do cadastro de entidade de p' +
        'refer'#234'ncia):'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDI_nDI: TdmkEdit
      Left = 96
      Top = 36
      Width = 105
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 12
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_nDI'
      UpdCampo = 'DI_nDI'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPDI_dDI: TdmkEditDateTimePicker
      Left = 204
      Top = 36
      Width = 124
      Height = 21
      Date = 40440.871393865740000000
      Time = 40440.871393865740000000
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DI_dDI'
      UpdCampo = 'DI_dDI'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdDI_xLocDesemb: TdmkEdit
      Left = 12
      Top = 76
      Width = 488
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_xLocDesemb'
      UpdCampo = 'DI_xLocDesemb'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdDI_UFDesemb: TdmkEdit
      Left = 504
      Top = 76
      Width = 28
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_UFDesemb'
      UpdCampo = 'DI_UFDesemb'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPDI_dDesemb: TdmkEditDateTimePicker
      Left = 12
      Top = 116
      Width = 112
      Height = 21
      Date = 40440.871393865740000000
      Time = 40440.871393865740000000
      TabOrder = 5
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DI_dDesemb'
      UpdCampo = 'DI_dDesemb'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdDI_cExportador: TdmkEdit
      Left = 128
      Top = 116
      Width = 405
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DI_cExportador'
      UpdCampo = 'DI_cExportador'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 547
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 499
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 451
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 320
        Height = 32
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 320
        Height = 32
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 320
        Height = 32
        Caption = 'Declara'#231#227'o de Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 295
    Width = 547
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 543
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 339
    Width = 547
    Height = 64
    Align = alBottom
    TabOrder = 5
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 543
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 26
      object PnSaiDesis: TPanel
        Left = 399
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
