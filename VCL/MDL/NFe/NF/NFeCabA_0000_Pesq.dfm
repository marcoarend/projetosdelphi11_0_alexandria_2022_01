object FmNFeCabA_0000_Pesq: TFmNFeCabA_0000_Pesq
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-010 :: Pesquisa de NF-e'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 111
    ExplicitHeight = 398
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 29
        Height = 13
        Caption = 'FatID:'
      end
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 40
        Height = 13
        Caption = 'FatNum:'
      end
      object Label4: TLabel
        Left = 176
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdFatID: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFatNum: TdmkEdit
        Left = 92
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 176
        Top = 20
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 228
        Top = 20
        Width = 545
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 48
      Width = 784
      Height = 290
      Align = alClient
      DataSource = DsPesq
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 217
        Height = 32
        Caption = 'Pesquisa de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 20
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtLocaliza: TBitBtn
        Tag = 18
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtLocalizaClick
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE FatID>0'
      'AND fatNum>0'
      '')
    Left = 96
    Top = 120
    object QrPesqFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPesqFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPesqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPesqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrPesqLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrPesqversao: TFloatField
      FieldName = 'versao'
    end
    object QrPesqId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrPesqide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrPesqide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrPesqide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrPesqide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrPesqide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrPesqide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrPesqide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrPesqide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrPesqide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrPesqide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrPesqide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrPesqide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrPesqide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrPesqide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrPesqide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrPesqide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrPesqide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrPesqide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrPesqemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrPesqemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrPesqemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrPesqemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrPesqemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrPesqemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrPesqemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrPesqemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrPesqemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrPesqemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrPesqemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrPesqemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrPesqemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrPesqemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrPesqemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrPesqemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrPesqemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrPesqemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrPesqemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrPesqdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrPesqdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrPesqdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrPesqdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrPesqdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrPesqdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrPesqdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrPesqdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrPesqdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrPesqdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrPesqdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrPesqdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrPesqdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrPesqdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrPesqdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrPesqdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrPesqICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrPesqICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrPesqICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrPesqICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrPesqICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrPesqICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrPesqICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrPesqICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrPesqICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrPesqICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrPesqICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrPesqICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrPesqICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrPesqICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrPesqISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrPesqISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrPesqISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrPesqISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrPesqISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrPesqRetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrPesqRetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrPesqRetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrPesqRetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrPesqRetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrPesqRetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrPesqRetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrPesqModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrPesqTransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrPesqTransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrPesqTransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrPesqTransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrPesqTransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrPesqTransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrPesqTransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrPesqRetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrPesqRetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrPesqRetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrPesqRetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrPesqRetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrPesqRetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrPesqVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrPesqVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrPesqVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrPesqCobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrPesqCobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrPesqCobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrPesqCobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrPesqInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPesqExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrPesqExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrPesqCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrPesqCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrPesqCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrPesqStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrPesqinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrPesqinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrPesqinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrPesqinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrPesqinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrPesqinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrPesqinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrPesqinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrPesqinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrPesqinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrPesqinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrPesqinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrPesqinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrPesqinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrPesqinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrPesqinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrPesqinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrPesqinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrPesq_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrPesqFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrPesqCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrPesqTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrPesqCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrPesqSegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrPesqICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrPesqICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrPesqICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrPesqICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrPesqIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrPesqIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrPesqIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrPesqIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrPesqPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrPesqPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrPesqPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrPesqPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrPesqCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrPesqCOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrPesqCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrPesqCOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrPesqDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrPesqprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrPesqretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
    end
    object QrPesqSINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrPesqSINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrPesqSINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrPesqSINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrPesqSINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrPesqSINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrPesqSINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrPesqSINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrPesqSINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrPesqSINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrPesqCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrPesqCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrPesqCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrPesqide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrPesqide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrPesqide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrPesqemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrPesqdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrPesqVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrPesqBalsa: TWideStringField
      FieldName = 'Balsa'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 124
    Top = 120
  end
end
