unit NFeCabXVol_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmNFeCabXVol_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    EdqVol: TdmkEdit;
    Label4: TLabel;
    EdEsp: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdMarca: TdmkEdit;
    EdnVol: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdpesoB: TdmkEdit;
    EdpesoL: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FMaxRegCount: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabXVol_0000: TFmNFeCabXVol_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabXVol_0000.BtOKClick(Sender: TObject);
const
  SeqInArq = 0;
var
  esp, marca, nVol: String;
  FatID, FatNum, Empresa, Controle(*, SeqInArq*): Integer;
  qVol, pesoL, pesoB: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  qVol           := EdqVol.ValueVariant;
  esp            := Edesp.ValueVariant;
  marca          := Edmarca.ValueVariant;
  nVol           := EdnVol.ValueVariant;
  pesoL          := EdpesoL.ValueVariant;
  pesoB          := EdpesoB.ValueVariant;
  //SeqInArq       := Ed.ValueVariant; ????   Importacao XML!

  //
  //Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxvol', '', Controle);
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxvol', '', Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabxvol', False, [
  'qVol', 'esp', 'marca',
  'nVol', 'pesoL', 'pesoB',
  'SeqInArq'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  qVol, esp, marca,
  nVol, pesoL, pesoB,
  SeqInArq], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    if (FMaxRegCount > 0) and (FQryIts.RecordCount >= FMaxRegCount) then
      Close
    else
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Item inclu�do com sucesso!');
      ImgTipo.SQLType := stIns;
      //
      EdqVol.Text           := '';
      Edesp.Text            := '';
      Edmarca.Text          := '';
      EdnVol.Text           := '';
      EdpesoL.ValueVariant  := 0.000;
      EdpesoB.ValueVariant  := 0.000;
    end else
      Close;
  end;
end;

procedure TFmNFeCabXVol_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabXVol_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabXVol_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeCabXVol_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
