unit NFeCabZFis_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmNFeCabZFis_0000 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    BtSaida: TBitBtn;
    Label178: TLabel;
    EdxCampo: TdmkEdit;
    EdxTexto: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FMaxRegCount: Integer;
    FQryIts: TmySQLQuery;
  end;

  var
  FmNFeCabZFis_0000: TFmNFeCabZFis_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, dmkGeral,
  ModuleGeral, ModuleNFe_0000, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeCabZFis_0000.BtOKClick(Sender: TObject);
var
  xCampo, xTexto: String;
  FatID, FatNum, Empresa, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  Controle       := FControle;
  xCampo         := EdxCampo.Text;
  xTexto         := EdxTexto.Text;
  //
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabzfis', '', Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabzfis', False, [
  'xCampo', 'xTexto'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  xCampo, xTexto], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQryIts, Dmod.MyDB);
    if (FMaxRegCount > 0) and (FQryIts.RecordCount >= FMaxRegCount) then
      Close
    else
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Item inclu�do com sucesso!');
      ImgTipo.SQLType := stIns;
      //
      EdxCampo.Text := '';
      EdxTexto.Text := '';
    end else
      Close;
  end;
end;

procedure TFmNFeCabZFis_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCabZFis_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCabZFis_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeCabZFis_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
