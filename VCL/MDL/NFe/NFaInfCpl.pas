unit NFaInfCpl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums, dmkMemo;

type
  TFmNFaInfCpl = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    EdFiltro: TEdit;
    QrNFeInfCpl: TmySQLQuery;
    DsNFeInfCpl: TDataSource;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    SBInfCpl: TSpeedButton;
    QrNFeInfCplCodigo: TIntegerField;
    QrNFeInfCplCodUsu: TIntegerField;
    QrNFeInfCplNome: TWideStringField;
    QrNFeInfCplTexto: TWideMemoField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdFiltroChange(Sender: TObject);
    procedure SBInfCplClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNFeInfCpl(CodUsu: Integer);
  public
    { Public declarations }
    FMemoinfAdic_infCpl: TdmkMemo;
  end;

  var
  FmNFaInfCpl: TFmNFaInfCpl;

implementation

uses UnMyObjects, Module, MyDBCheck, NFe_PF, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFaInfCpl.BtOKClick(Sender: TObject);
begin
  if FMemoinfAdic_infCpl <> nil then
    UnNFe_PF.InsereTextoObserv(QrNFeInfCplTexto.Value, FMemoinfAdic_infCpl)
  else
    Geral.MB_Erro('FMemoinfAdic_infCpl n�o definido!' + sLineBreak + 'Avise a Dermatek!');
end;

procedure TFmNFaInfCpl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaInfCpl.EdFiltroChange(Sender: TObject);
begin
  ReopenNFeInfCpl(0);
end;

procedure TFmNFaInfCpl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFaInfCpl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FMemoinfAdic_infCpl := nil;
  //
  ReopenNFeInfCpl(0);
end;

procedure TFmNFaInfCpl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaInfCpl.ReopenNFeInfCpl(CodUsu: Integer);
var
  CU: Integer;
begin
  if CodUsu = 0 then
  begin
    if QrNFeInfCpl.State <> dsInactive then
      CU := QrNFeInfCplCodUsu.Value
    else
      CU := 0;
  end else CU := CodUsu;
  //
  QrNFeInfCpl.Close;
  QrNFeInfCpl.SQL.Clear;
  QrNFeInfCpl.SQL.Add('SELECT *');
  QrNFeInfCpl.SQL.Add('FROM nfeinfcpl');
  QrNFeInfCpl.SQL.Add('WHERE Nome LIKE "%' + EdFiltro.Text + '%"');
  UnDmkDAC_PF.AbreQuery(QrNFeInfCpl, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  QrNFeInfCpl.Locate('CodUsu', CU, []);
end;

procedure TFmNFaInfCpl.SBInfCplClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNfeInfCpl();
  ReopenNFeInfCpl(-1);
end;

end.
