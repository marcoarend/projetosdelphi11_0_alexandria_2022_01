// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRecepcao.wsdl
//  >Import : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRecepcao.wsdl:0
// Encoding : utf-8
// Version  : 1.0
// (23/11/2009 09:02:11 - - $Rev: 10138 $)
// ************************************************************************ //

unit NFeRecepcao;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]



  // ************************************************************************ //
  // Namespace : http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao
  // soapAction: http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao/nfeRecepcaoLote
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : NfeRecepcaoSoap
  // service   : NfeRecepcao
  // port      : NfeRecepcaoSoap
  // URL       : https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferecepcao/nferecepcao.asmx
  // ************************************************************************ //
  NfeRecepcaoSoap = interface(IInvokable)
  ['{284137FB-09FD-99DA-E2CB-B42E304E1C3D}']
    function  nfeRecepcaoLote(const nfeCabecMsg: WideString; const nfeDadosMsg: WideString): WideString; stdcall;
  end;

function GetNfeRecepcaoSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): NfeRecepcaoSoap;


implementation
  uses SysUtils;

function GetNfeRecepcaoSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): NfeRecepcaoSoap;
const
  defWSDL = 'C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRecepcao.wsdl';
  defURL  = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferecepcao/nferecepcao.asmx';
  defSvc  = 'NfeRecepcao';
  defPrt  = 'NfeRecepcaoSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as NfeRecepcaoSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(NfeRecepcaoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(NfeRecepcaoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao/nfeRecepcaoLote');
  InvRegistry.RegisterInvokeOptions(TypeInfo(NfeRecepcaoSoap), ioDocument);

end.