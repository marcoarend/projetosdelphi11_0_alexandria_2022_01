// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeCancelamento.wsdl
//  >Import : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeCancelamento.wsdl:0
// Encoding : utf-8
// Version  : 1.0
// (25/11/2009 22:48:53 - - $Rev: 10138 $)
// ************************************************************************ //

unit NFeCancelamento;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]



  // ************************************************************************ //
  // Namespace : http://www.portalfiscal.inf.br/nfe/wsdl/NfeCancelamento
  // soapAction: http://www.portalfiscal.inf.br/nfe/wsdl/NfeCancelamento/nfeCancelamentoNF
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : NfeCancelamentoSoap
  // service   : NfeCancelamento
  // port      : NfeCancelamentoSoap
  // URL       : https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfeCANCELAMENTO/nfecancelamento.asmx
  // ************************************************************************ //
  NfeCancelamentoSoap = interface(IInvokable)
  ['{97A7C1B8-C94D-0003-2A0F-E2CDC6F3399C}']
    function  nfeCancelamentoNF(const nfeCabecMsg: WideString; const nfeDadosMsg: WideString): WideString; stdcall;
  end;

function GetNfeCancelamentoSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): NfeCancelamentoSoap;


implementation
  uses SysUtils;

function GetNfeCancelamentoSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): NfeCancelamentoSoap;
const
  defWSDL = 'C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeCancelamento.wsdl';
  defURL  = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfeCANCELAMENTO/nfecancelamento.asmx';
  defSvc  = 'NfeCancelamento';
  defPrt  = 'NfeCancelamentoSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as NfeCancelamentoSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(NfeCancelamentoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeCancelamento', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(NfeCancelamentoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeCancelamento/nfeCancelamentoNF');
  InvRegistry.RegisterInvokeOptions(TypeInfo(NfeCancelamentoSoap), ioDocument);

end.