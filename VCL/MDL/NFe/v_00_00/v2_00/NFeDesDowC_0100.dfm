object FmNFeDesDowC_0100: TFmNFeDesDowC_0100
  Left = 368
  Top = 194
  Caption = 'NFe-DESTI-002 :: Download NF-es Destinadas 1.00'
  ClientHeight = 759
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 641
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 113
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 10
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label9: TLabel
        Left = 84
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 10
        Top = 59
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 84
        Top = 25
        Width = 1030
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 10
        Top = 79
        Width = 65
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 80
        Top = 79
        Width = 1034
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 562
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 18
        Width = 1236
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 1058
          Top = 0
          Width = 178
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 9
            Top = 2
            Width = 147
            Height = 50
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 641
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 134
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1241
        Height = 55
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 10
          Top = 5
          Width = 16
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 84
          Top = 5
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label5: TLabel
          Left = 625
          Top = 5
          Width = 28
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdit2: TDBEdit
          Left = 625
          Top = 25
          Width = 69
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Filial'
          DataSource = DsNFeDescDowC
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 694
          Top = 25
          Width = 537
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_Empresa'
          DataSource = DsNFeDescDowC
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 84
          Top = 25
          Width = 536
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Nome'
          DataSource = DsNFeDescDowC
          TabOrder = 2
        end
        object DBEdCodigo: TDBEdit
          Left = 10
          Top = 25
          Width = 70
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsNFeDescDowC
          TabOrder = 3
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 55
        Width = 1241
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Mensagem de retorno: '
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 18
          Width = 1236
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 5
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit1
          end
          object Label6: TLabel
            Left = 59
            Top = 5
            Width = 129
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o da aplica'#231#227'o:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 295
            Top = 5
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ret_tpAmb'
            FocusControl = DBEdit5
          end
          object Label10: TLabel
            Left = 645
            Top = 5
            Width = 94
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Status e Motivo:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 482
            Top = 5
            Width = 151
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data e hora da resposta: '
            FocusControl = DBEdit8
          end
          object DBEdit1: TDBEdit
            Left = 10
            Top = 25
            Width = 44
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_versao'
            DataSource = DsNFeDescDowC
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 59
            Top = 25
            Width = 231
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_verAplic'
            DataSource = DsNFeDescDowC
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 295
            Top = 25
            Width = 20
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_tpAmb'
            DataSource = DsNFeDescDowC
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 645
            Top = 25
            Width = 34
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_cStat'
            DataSource = DsNFeDescDowC
            TabOrder = 3
          end
          object DBEdit7: TDBEdit
            Left = 679
            Top = 25
            Width = 548
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_xMotivo'
            DataSource = DsNFeDescDowC
            TabOrder = 4
          end
          object DBEdit8: TDBEdit
            Left = 482
            Top = 25
            Width = 158
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ret_dhResp'
            DataSource = DsNFeDescDowC
            TabOrder = 5
          end
          object DBEdit9: TDBEdit
            Left = 315
            Top = 25
            Width = 163
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_ret_tpAmb'
            DataSource = DsNFeDescDowC
            TabOrder = 6
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 134
      Width = 1241
      Height = 389
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Informa'#231#245'es das notas do lote '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 1231
          Height = 354
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Chaves das NF-es: '
          TabOrder = 0
          object DBGrid2: TDBGrid
            Left = 2
            Top = 18
            Width = 1226
            Height = 334
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsNFeDesDowI
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'chNFe'
                Title.Caption = 'Chave da NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ret_cStat'
                Title.Caption = 'Status'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ret_xMotivo'
                Title.Caption = 'Motivo do Status'
                Width = 620
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1231
          Height = 354
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          ControlData = {
            4C0000005A670000C41D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 562
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 532
        Top = 18
        Width = 706
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 543
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Tag = 526
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtNFe: TBitBtn
          Tag = 456
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&NFe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 481
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Download NF-es Destinadas 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 481
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Download NF-es Destinadas 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 481
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Download NF-es Destinadas 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeDescDowC: TDataSource
    DataSet = QrNFeDesDowC
    Left = 40
    Top = 65524
  end
  object QrNFeDesDowC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeDesDowCBeforeOpen
    AfterOpen = QrNFeDesDowCAfterOpen
    AfterScroll = QrNFeDesDowCAfterScroll
    OnCalcFields = QrNFeDesDowCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfedesdowc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 65524
    object QrNFeDesDowCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDesDowCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDesDowCNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrNFeDesDowCversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDesDowCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDesDowCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeDesDowCFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFeDesDowCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeDesDowCret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrNFeDesDowCret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrNFeDesDowCret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrNFeDesDowCret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeDesDowCret_dhResp: TDateTimeField
      FieldName = 'ret_dhResp'
    end
    object QrNFeDesDowCret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrNFeDesDowCNO_ret_tpAmb: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ret_tpAmb'
      Size = 15
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtNFe
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 452
    Top = 536
    object Incluinovolote1: TMenuItem
      Caption = '&Nova consulta'
      OnClick = Incluinovolote1Click
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      OnClick = Envialoteaofisco1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui consulta atual'
      Enabled = False
      Visible = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo retorno'
      OnClick = Lerarquivo1Click
    end
  end
  object QrNFeDesDowI: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeDesDowICalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM nfedesdowi'
      'WHERE Codigo=:P0')
    Left = 404
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeDesDowICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDesDowIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeDesDowIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDesDowIchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeDesDowIret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeDesDowIret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
  end
  object DsNFeDesDowI: TDataSource
    DataSet = QrNFeDesDowI
    Left = 432
    Top = 52
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 580
    Top = 536
    object IncluiChave1: TMenuItem
      Caption = '&Inclui Chave NFe ao lote'
      OnClick = IncluiChave1Click
    end
    object Retirachave1: TMenuItem
      Caption = '&Retira Chave NF-e do lote'
      OnClick = Retirachave1Click
    end
  end
  object QrFatID_0052: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id'
      'FROM nfecaba'
      'WHERE FatID=52'
      'AND Empresa=-11'
      'AND ide_tpAmb=2')
    Left = 128
    Top = 65524
    object QrFatID_0052Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
end
