unit NFeDesDowI_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  NFe_Tabs, DBCtrls, ComCtrls, UnInternalConsts, dmkMemo, dmkRadioGroup,
  mySQLDbTables, dmkDBGridDAC, UnDmkEnums;

type
  TFmNFeDesDowI_0100 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    RGnCondUso: TdmkRadioGroup;
    MeCondicao: TMemo;
    GroupBox3: TGroupBox;
    CkCondicao: TCheckBox;
    QrNFeChaves: TmySQLQuery;
    DsNFeChaves: TDataSource;
    QrNFeChavesChaveNFe: TWideStringField;
    QrNFeChavesUF_IBGE: TWideStringField;
    QrNFeChavesAAMM: TWideStringField;
    QrNFeChavesCNPJ: TWideStringField;
    QrNFeChavesModNFe: TWideStringField;
    QrNFeChavesSerNFe: TWideStringField;
    QrNFeChavesNumNFe: TWideStringField;
    QrNFeChavestpEmis: TWideStringField;
    QrNFeChavesAleNFe: TWideStringField;
    QrNFeChavesemit_UF: TWideStringField;
    QrNFeChavesAtivo: TSmallintField;
    QrNFeChavesCNPJ_TXT: TWideStringField;
    DBGChaves: TdmkDBGridDAC;
    QrNFeChavesChaveNFe_TXT: TWideStringField;
    QrSel: TmySQLQuery;
    QrSelChaveNFe: TWideStringField;
    QrSelUF_IBGE: TWideStringField;
    QrSelAAMM: TWideStringField;
    QrSelCNPJ: TWideStringField;
    QrSelModNFe: TWideStringField;
    QrSelSerNFe: TWideStringField;
    QrSelNumNFe: TWideStringField;
    QrSeltpEmis: TWideStringField;
    QrSelAleNFe: TWideStringField;
    QrSelemit_UF: TWideStringField;
    QrSelAtivo: TSmallintField;
    QrDup: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkCondicaoClick(Sender: TObject);
    procedure RGnCondUsoClick(Sender: TObject);
    procedure MexCorrecaoEnter(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrNFeChavesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure HabilitaOK();
  public
    { Public declarations }
    F_nfe_chaves: String;
    MsgMemoDesfeito: Boolean;
    //
    procedure DesfazMsgMemo();
    procedure ReopenNFeChaves(Chave: String);
  end;

  var
  FmNFeDesDowI_0100: TFmNFeDesDowI_0100;

implementation

uses UnMyObjects, Module, ModuleGeral, DMkDAC_PF, NFeDesDowC_0100, UMySQLModule;

{$R *.DFM}

procedure TFmNFeDesDowI_0100.BtOKClick(Sender: TObject);
var
  chNFe: String;
  Codigo, Controle, Empresa: Integer;
  //
  procedure AdicionaEventoAoLote();
  const
    SQLType = stIns;
  begin
    Codigo         := FmNFeDesDowC_0100.QrNFeDesDowCCodigo.Value;
    Controle       := FmNFeDesDowC_0100.QrNFeDesDowIControle.Value;
    Empresa        := FmNFeDesDowC_0100.QrNFeDesDowCCodigo.Value;;
    chNFe          := QrSelChaveNFe.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDup, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM nfedesdowi ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND chNFe="' + chNFe + '" ',
    '']);
    if QrDup.RecordCount = 0 then
    begin
      Controle := UMyMod.BPGS1I32('nfedesdowi', 'Controle', '', '', tsPos, SQLType, Controle);
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesdowi', False, [
      'Codigo', 'Empresa', 'chNFe'], [
      'Controle'], [
      Codigo, Empresa, chNFe], [
      Controle], True);
    end else
      Geral.MB_Aviso('A chave ' + chNFe + ' j� havia sido adicionado ao lote!');
  end;
var
  N: Integer;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MyPID_DB, [
  'SELECT * FROM _nfe_chaves ',
  'WHERE Ativo=1 ',
  '']);
  if QrSel.RecordCount > 0 then
  begin
    {
    N := DBGChaves.SelectedRows.Count;
    if N + FmNFeDesDowC_0100.QrNFeDesDowC.RecordCount > 20 then
    begin
      Geral.MensagemBox('Quantidade extrapola o m�ximo de 20 eventos permitidos por lote!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if N > 1 then
    begin
      if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
      ' NF-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
      ID_YES then
      begin
        with DBGChaves.DataSource.DataSet do
        for I := 0 to N - 1 do
        begin
          GotoBookmark(DBGChaves.SelectedRows.Items[I]);
          AdicionaEventoAoLote();
        end;
      end;
    end else AdicionaEventoAoLote();
    }
    N := QrSel.RecordCount;
    if N + FmNFeDesDowC_0100.QrNFeDesDowC.RecordCount > 20 then
    begin
      Geral.MensagemBox('Quantidade extrapola o m�ximo de 20 eventos permitidos por lote!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    QrSel.First;
    while not QrSel.Eof do
    begin
      AdicionaEventoAoLote();
      QrSel.Next;
    end;

    //
    
    FmNFeDesDowC_0100.ReopenNFeDesDowI(0);
    Close;
  end else
    Geral.MB_Aviso('Nenhuma chave foi selecionada!');
end;

procedure TFmNFeDesDowI_0100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeDesDowI_0100.CkCondicaoClick(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmNFeDesDowI_0100.DesfazMsgMemo();
begin
  if not MsgMemoDesfeito then
  begin
    {
    MexCorrecao.Lines.Clear;
    MexCorrecao.Font.Color := clWindowText;
    }
    MsgMemoDesfeito := True;
  end;
  HabilitaOK();
end;

procedure TFmNFeDesDowI_0100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeDesDowI_0100.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MsgMemoDesfeito := False;
end;

procedure TFmNFeDesDowI_0100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeDesDowI_0100.HabilitaOK();
begin
  //N := Length(Geral.SoNumeroELetra_TT(MexCorrecao.Text));
  BtOK.Enabled := (*MsgMemoDesfeito and
    (N >= 15) and*)
    CkCondicao.Checked and
    (RGnCondUso.ItemIndex > 0);
end;

procedure TFmNFeDesDowI_0100.MexCorrecaoEnter(Sender: TObject);
begin
  DesfazMsgMemo();
end;

procedure TFmNFeDesDowI_0100.QrNFeChavesCalcFields(DataSet: TDataSet);
begin
  QrNFeChavesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrNFeChavesCNPJ.Value);
  //
  QrNFeChavesChaveNFe_TXT.Value := Geral.FormataChaveNFe(
    QrNFeChavesChaveNFe.Value, cfcnFrendly);
end;

procedure TFmNFeDesDowI_0100.ReopenNFeChaves(Chave: String);
begin
  DBGChaves.SQLTable := F_nfe_chaves;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrNFeChaves, DModG.MyPID_DB, [
  'SELECT * FROM ' + F_nfe_chaves,
  '']);
end;

procedure TFmNFeDesDowI_0100.RGnCondUsoClick(Sender: TObject);
begin
  MeCondicao.Text := CO_CONDI_USO_DOW_NFE_CON_DES_COM[RGnCondUso.ItemIndex];
  HabilitaOK();
end;

end.
