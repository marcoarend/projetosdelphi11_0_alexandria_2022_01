object FmNFeCnfDowI_0100: TFmNFeCnfDowI_0100
  Left = 339
  Top = 185
  Caption = 'NFe-DESTI-003 :: Chaves de NF-es para Download'
  ClientHeight = 629
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 402
        Height = 32
        Caption = 'Chaves de NF-es para Download'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 402
        Height = 32
        Caption = 'Chaves de NF-es para Download'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 402
        Height = 32
        Caption = 'Chaves de NF-es para Download'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 450
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 780
            Height = 221
            Align = alTop
            TabOrder = 0
            object RGnCondUso: TdmkRadioGroup
              Left = 2
              Top = 15
              Width = 111
              Height = 204
              Align = alLeft
              Caption = ' Condi'#231#227'o de uso: '
              ItemIndex = 0
              Items.Strings = (
                'Sem condi'#231#227'o'
                'Modelo 1')
              TabOrder = 0
              OnClick = RGnCondUsoClick
              QryCampo = 'nCondUso'
              UpdCampo = 'nCondUso'
              UpdType = utYes
              OldValor = 0
            end
            object MeCondicao: TMemo
              Left = 113
              Top = 15
              Width = 665
              Height = 204
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 1
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 221
            Width = 780
            Height = 229
            Align = alClient
            Caption = ' Chaves de NF-e a serem baixadas do Web Service (m'#225'ximo 10): '
            TabOrder = 1
            object DBGChaves: TDBGrid
              Left = 2
              Top = 15
              Width = 776
              Height = 212
              Align = alClient
              DataSource = DsNFeChaves
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'ide_dEmi'
                  Title.Caption = 'Emiss'#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ChaveNFe'
                  Title.Caption = 'Chave da NF-e'
                  Width = 336
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SerNFe'
                  Title.Caption = 'S'#233'rie'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NumNFe'
                  Title.Caption = 'Numero NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNPJ_TXT'
                  Title.Caption = 'CNPJ do emitente'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'emit_UF'
                  Title.Caption = 'UF'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AAMM'
                  Title.Caption = 'AA+MM'
                  Width = 44
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkCondicao: TCheckBox
        Left = 144
        Top = 16
        Width = 413
        Height = 17
        Caption = 
          'Li as condi'#231#245'es de uso e os avisos e estou ciente do conte'#250'do e ' +
          'das implica'#231#245'es.'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        OnClick = CkCondicaoClick
      end
    end
  end
  object QrNFeChaves: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeChavesCalcFields
    SQL.Strings = (
      'SELECT * FROM _nfe_chaves')
    Left = 324
    Top = 128
    object QrNFeChavesChaveNFe: TWideStringField
      FieldName = 'ChaveNFe'
      Size = 44
    end
    object QrNFeChavesUF_IBGE: TWideStringField
      FieldName = 'UF_IBGE'
      Size = 2
    end
    object QrNFeChavesAAMM: TWideStringField
      FieldName = 'AAMM'
      Size = 4
    end
    object QrNFeChavesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeChavesModNFe: TWideStringField
      FieldName = 'ModNFe'
      Size = 2
    end
    object QrNFeChavesSerNFe: TWideStringField
      FieldName = 'SerNFe'
      Size = 3
    end
    object QrNFeChavesNumNFe: TWideStringField
      FieldName = 'NumNFe'
      Size = 11
    end
    object QrNFeChavestpEmis: TWideStringField
      FieldName = 'tpEmis'
      Size = 1
    end
    object QrNFeChavesAleNFe: TWideStringField
      FieldName = 'AleNFe'
      Size = 11
    end
    object QrNFeChavesemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNFeChavesAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
    object QrNFeChavesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeChavesChaveNFe_TXT: TWideStringField
      DisplayWidth = 80
      FieldKind = fkCalculated
      FieldName = 'ChaveNFe_TXT'
      Size = 80
      Calculated = True
    end
    object QrNFeChaveside_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeChavesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
  end
  object DsNFeChaves: TDataSource
    DataSet = QrNFeChaves
    Left = 352
    Top = 128
  end
  object QrSel: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeChavesCalcFields
    SQL.Strings = (
      'SELECT * FROM _nfe_chaves'
      'WHERE Ativo=1')
    Left = 168
    Top = 60
    object QrSelChaveNFe: TWideStringField
      FieldName = 'ChaveNFe'
      Size = 44
    end
    object QrSelUF_IBGE: TWideStringField
      FieldName = 'UF_IBGE'
      Size = 2
    end
    object QrSelAAMM: TWideStringField
      FieldName = 'AAMM'
      Size = 4
    end
    object QrSelCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrSelModNFe: TWideStringField
      FieldName = 'ModNFe'
      Size = 2
    end
    object QrSelSerNFe: TWideStringField
      FieldName = 'SerNFe'
      Size = 3
    end
    object QrSelNumNFe: TWideStringField
      FieldName = 'NumNFe'
      Size = 11
    end
    object QrSeltpEmis: TWideStringField
      FieldName = 'tpEmis'
      Size = 1
    end
    object QrSelAleNFe: TWideStringField
      FieldName = 'AleNFe'
      Size = 11
    end
    object QrSelemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrSelAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
  end
  object QrDup: TMySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 60
  end
end
