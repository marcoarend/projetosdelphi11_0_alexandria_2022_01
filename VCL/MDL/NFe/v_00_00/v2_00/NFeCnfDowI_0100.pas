unit NFeCnfDowI_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  NFe_Tabs, DBCtrls, ComCtrls, UnInternalConsts, dmkMemo, dmkRadioGroup,
  mySQLDbTables, dmkDBGridDAC, UnDmkEnums;

type
  TFmNFeCnfDowI_0100 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    RGnCondUso: TdmkRadioGroup;
    MeCondicao: TMemo;
    GroupBox3: TGroupBox;
    CkCondicao: TCheckBox;
    QrNFeChaves: TmySQLQuery;
    DsNFeChaves: TDataSource;
    QrNFeChavesChaveNFe: TWideStringField;
    QrNFeChavesUF_IBGE: TWideStringField;
    QrNFeChavesAAMM: TWideStringField;
    QrNFeChavesCNPJ: TWideStringField;
    QrNFeChavesModNFe: TWideStringField;
    QrNFeChavesSerNFe: TWideStringField;
    QrNFeChavesNumNFe: TWideStringField;
    QrNFeChavestpEmis: TWideStringField;
    QrNFeChavesAleNFe: TWideStringField;
    QrNFeChavesemit_UF: TWideStringField;
    QrNFeChavesAtivo: TSmallintField;
    QrNFeChavesCNPJ_TXT: TWideStringField;
    DBGChaves: TDBGrid;
    QrNFeChavesChaveNFe_TXT: TWideStringField;
    QrSel: TmySQLQuery;
    QrSelChaveNFe: TWideStringField;
    QrSelUF_IBGE: TWideStringField;
    QrSelAAMM: TWideStringField;
    QrSelCNPJ: TWideStringField;
    QrSelModNFe: TWideStringField;
    QrSelSerNFe: TWideStringField;
    QrSelNumNFe: TWideStringField;
    QrSeltpEmis: TWideStringField;
    QrSelAleNFe: TWideStringField;
    QrSelemit_UF: TWideStringField;
    QrSelAtivo: TSmallintField;
    QrDup: TmySQLQuery;
    QrNFeChaveside_dEmi: TDateField;
    QrNFeChavesRazaoSocial: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkCondicaoClick(Sender: TObject);
    procedure RGnCondUsoClick(Sender: TObject);
    procedure MexCorrecaoEnter(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrNFeChavesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure HabilitaOK();
  public
    { Public declarations }
    F_nfe_chaves: String;
    MsgMemoDesfeito: Boolean;
    //
    procedure DesfazMsgMemo();
    procedure ReopenNFeChaves(Chave: String);
  end;

  var
  FmNFeCnfDowI_0100: TFmNFeCnfDowI_0100;

implementation

uses UnMyObjects, Module, ModuleGeral, DMkDAC_PF, NFeCnfDowC_0100, UMySQLModule;

{$R *.DFM}

procedure TFmNFeCnfDowI_0100.BtOKClick(Sender: TObject);
var
  chNFe: String;
  Codigo, Controle, Empresa: Integer;
  //
  procedure AdicionaEventoAoLote();
  const
    SQLType = stIns;
  begin
    Codigo         := FmNFeCnfDowC_0100.QrNFeCnfDowCCodigo.Value;
    Controle       := FmNFeCnfDowC_0100.QrNFeCnfDowIControle.Value;
    Empresa        := FmNFeCnfDowC_0100.QrNFeCnfDowCCodigo.Value;;
    chNFe          := QrSelChaveNFe.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDup, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM nfecnfdowi ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND chNFe="' + chNFe + '" ',
    '']);
    if QrDup.RecordCount = 0 then
    begin
      Controle := UMyMod.BPGS1I32('nfecnfdowi', 'Controle', '', '', tsPos, SQLType, Controle);
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecnfdowi', False, [
      'Codigo', 'Empresa', 'chNFe'], [
      'Controle'], [
      Codigo, Empresa, chNFe], [
      Controle], True);
    end else
      Geral.MB_Aviso('A chave ' + chNFe + ' j� havia sido adicionado ao lote!');
  end;
var
  N: Integer;
begin
{
  UnDMkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MyPID_DB, [
  'SELECT * FROM _nfe_chaves ',
  'WHERE Ativo=1 ',
  '']);
  if QrSel.RecordCount > 0 then
  begin
    N := QrSel.RecordCount;
    if N + FmNFeCnfDowC_0100.QrNFeCnfDowC.RecordCount > 10 then
    begin
      Geral.MensagemBox('Quantidade extrapola o m�ximo de 10 eventos permitidos por lote!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    QrSel.First;
    while not QrSel.Eof do
    begin
      AdicionaEventoAoLote();
      QrSel.Next;
    end;

    //

    FmNFeCnfDowC_0100.ReopenNFeCnfDowI(0);
    Close;
  end else
    Geral.MB_Aviso('Nenhuma chave foi selecionada!');
}
  if DBGChaves.SelectedRows.Count > 0 then
  begin
    with DBGChaves.DataSource.DataSet do
    for n := 0 to DBGChaves.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGrid1.SelectedRows.Items[n]);
      GotoBookmark(DBGChaves.SelectedRows.Items[n]);
      AdicionaEventoAoLote();
    end;
  end;
end;

procedure TFmNFeCnfDowI_0100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeCnfDowI_0100.CkCondicaoClick(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmNFeCnfDowI_0100.DesfazMsgMemo();
begin
  if not MsgMemoDesfeito then
  begin
    {
    MexCorrecao.Lines.Clear;
    MexCorrecao.Font.Color := clWindowText;
    }
    MsgMemoDesfeito := True;
  end;
  HabilitaOK();
end;

procedure TFmNFeCnfDowI_0100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCnfDowI_0100.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MsgMemoDesfeito := False;
end;

procedure TFmNFeCnfDowI_0100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCnfDowI_0100.HabilitaOK();
begin
  //N := Length(Geral.SoNumeroELetra_TT(MexCorrecao.Text));
  BtOK.Enabled := (*MsgMemoDesfeito and
    (N >= 15) and*)
    CkCondicao.Checked and
    (RGnCondUso.ItemIndex > 0);
end;

procedure TFmNFeCnfDowI_0100.MexCorrecaoEnter(Sender: TObject);
begin
  DesfazMsgMemo();
end;

procedure TFmNFeCnfDowI_0100.QrNFeChavesCalcFields(DataSet: TDataSet);
begin
  QrNFeChavesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrNFeChavesCNPJ.Value);
  //
  QrNFeChavesChaveNFe_TXT.Value := Geral.FormataChaveNFe(
    QrNFeChavesChaveNFe.Value, cfcnFrendly);
end;

procedure TFmNFeCnfDowI_0100.ReopenNFeChaves(Chave: String);
begin
  //DBGChaves.SQLTable := F_nfe_chaves;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrNFeChaves, DModG.MyPID_DB, [
  'SELECT * FROM ' + F_nfe_chaves,
  '']);
end;

procedure TFmNFeCnfDowI_0100.RGnCondUsoClick(Sender: TObject);
begin
  MeCondicao.Text := CO_CONDI_USO_DOW_NFE_CON_DES_COM[RGnCondUso.ItemIndex];
  HabilitaOK();
end;

{
object DBGChaves: TdmkDBGridDAC
  Left = 2
  Top = 15
  Width = 776
  Height = 212
  SQLFieldsToChange.Strings = (
    'Ativo')
  SQLIndexesOnUpdate.Strings = (
    'ChaveNFe')
  Align = alClient
  Columns = <
    item
      Expanded = False
      FieldName = 'Ativo'
      Title.Caption = 'ok'
      Width = 17
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'ide_dEmi'
      Title.Caption = 'Emiss'#227'o'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'ChaveNFe'
      Title.Caption = 'Chave da NF-e'
      Width = 336
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'SerNFe'
      Title.Caption = 'S'#233'rie'
      Width = 32
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'NumNFe'
      Title.Caption = 'Numero NF'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'CNPJ_TXT'
      Title.Caption = 'CNPJ do emitente'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'emit_UF'
      Title.Caption = 'UF'
      Width = 24
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'AAMM'
      Title.Caption = 'AA+MM'
      Width = 44
      Visible = True
    end>
  Color = clWindow
  DataSource = DsNFeChaves
  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
  TabOrder = 0
  TitleFont.Charset = ANSI_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -11
  TitleFont.Name = 'MS Sans Serif'
  TitleFont.Style = []
  EditForceNextYear = False
  Columns = <
    item
      Expanded = False
      FieldName = 'Ativo'
      Title.Caption = 'ok'
      Width = 17
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'ide_dEmi'
      Title.Caption = 'Emiss'#227'o'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'ChaveNFe'
      Title.Caption = 'Chave da NF-e'
      Width = 336
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'SerNFe'
      Title.Caption = 'S'#233'rie'
      Width = 32
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'NumNFe'
      Title.Caption = 'Numero NF'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'CNPJ_TXT'
      Title.Caption = 'CNPJ do emitente'
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'emit_UF'
      Title.Caption = 'UF'
      Width = 24
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'AAMM'
      Title.Caption = 'AA+MM'
      Width = 44
      Visible = True
    end>
end
}
end.
