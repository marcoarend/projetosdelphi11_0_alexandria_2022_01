object FmNFeCnfDowC_0100: TFmNFeCnfDowC_0100
  Left = 368
  Top = 194
  Caption = 'NFe-DESTI-002 :: Download NF-es Confirmadas'
  ClientHeight = 617
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label9: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 837
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 840
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 860
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 109
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 45
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 68
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label5: TLabel
          Left = 508
          Top = 4
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdit2: TDBEdit
          Left = 508
          Top = 20
          Width = 56
          Height = 21
          DataField = 'Filial'
          DataSource = DsNFeCnfDowC
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 564
          Top = 20
          Width = 436
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsNFeCnfDowC
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 68
          Top = 20
          Width = 436
          Height = 21
          DataField = 'Nome'
          DataSource = DsNFeCnfDowC
          TabOrder = 2
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNFeCnfDowC
          TabOrder = 3
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 45
        Width = 1008
        Height = 64
        Align = alClient
        Caption = ' Mensagem de retorno: '
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit1
          end
          object Label6: TLabel
            Left = 48
            Top = 4
            Width = 100
            Height = 13
            Caption = 'Vers'#227'o da aplica'#231#227'o:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 240
            Top = 4
            Width = 48
            Height = 13
            Caption = 'ret_tpAmb'
            FocusControl = DBEdit5
          end
          object Label10: TLabel
            Left = 524
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Status e Motivo:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 392
            Top = 4
            Width = 120
            Height = 13
            Caption = 'Data e hora da resposta: '
            FocusControl = DBEdit8
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 20
            Width = 36
            Height = 21
            DataField = 'ret_versao'
            DataSource = DsNFeCnfDowC
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 48
            Top = 20
            Width = 188
            Height = 21
            DataField = 'ret_verAplic'
            DataSource = DsNFeCnfDowC
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 240
            Top = 20
            Width = 16
            Height = 21
            DataField = 'ret_tpAmb'
            DataSource = DsNFeCnfDowC
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 524
            Top = 20
            Width = 28
            Height = 21
            DataField = 'ret_cStat'
            DataSource = DsNFeCnfDowC
            TabOrder = 3
          end
          object DBEdit7: TDBEdit
            Left = 552
            Top = 20
            Width = 445
            Height = 21
            DataField = 'ret_xMotivo'
            DataSource = DsNFeCnfDowC
            TabOrder = 4
          end
          object DBEdit8: TDBEdit
            Left = 392
            Top = 20
            Width = 128
            Height = 21
            DataField = 'ret_dhResp'
            DataSource = DsNFeCnfDowC
            TabOrder = 5
          end
          object DBEdit9: TDBEdit
            Left = 256
            Top = 20
            Width = 132
            Height = 21
            DataField = 'NO_ret_tpAmb'
            DataSource = DsNFeCnfDowC
            TabOrder = 6
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 109
      Width = 1008
      Height = 316
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 288
          Align = alClient
          Caption = ' Chaves das NF-es: '
          TabOrder = 0
          object DBGrid2: TDBGrid
            Left = 2
            Top = 15
            Width = 996
            Height = 271
            Align = alClient
            DataSource = DsNFeCnfDowI
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'chNFe'
                Title.Caption = 'Chave da NFe'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ret_cStat'
                Title.Caption = 'Status'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ret_xMotivo'
                Title.Caption = 'Motivo do Status'
                Width = 620
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 288
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 165
          ControlData = {
            4C0000005A670000C41D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 258
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 432
        Top = 15
        Width = 574
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 441
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Tag = 526
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtNFe: TBitBtn
          Tag = 456
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 363
        Height = 32
        Caption = 'Download NF-es Confirmadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 363
        Height = 32
        Caption = 'Download NF-es Confirmadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 363
        Height = 32
        Caption = 'Download NF-es Confirmadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeCnfDowC: TDataSource
    DataSet = QrNFeCnfDowC
    Left = 48
    Top = 332
  end
  object QrNFeCnfDowC: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeCnfDowCBeforeOpen
    AfterOpen = QrNFeCnfDowCAfterOpen
    AfterScroll = QrNFeCnfDowCAfterScroll
    OnCalcFields = QrNFeCnfDowCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfedesdowc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 48
    Top = 284
    object QrNFeCnfDowCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeCnfDowCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCnfDowCNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrNFeCnfDowCversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeCnfDowCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeCnfDowCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeCnfDowCFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFeCnfDowCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeCnfDowCret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrNFeCnfDowCret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrNFeCnfDowCret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrNFeCnfDowCret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeCnfDowCret_dhResp: TDateTimeField
      FieldName = 'ret_dhResp'
    end
    object QrNFeCnfDowCret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrNFeCnfDowCNO_ret_tpAmb: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ret_tpAmb'
      Size = 15
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtNFe
    Left = 32
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 116
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 452
    Top = 536
    object Incluinovolote1: TMenuItem
      Caption = '&Nova consulta'
      OnClick = Incluinovolote1Click
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      OnClick = Envialoteaofisco1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui consulta atual'
      Enabled = False
      Visible = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo retorno'
      OnClick = Lerarquivo1Click
    end
  end
  object QrNFeCnfDowI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeCnfDowICalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM nfedesdowi'
      'WHERE Codigo=:P0')
    Left = 156
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeCnfDowICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeCnfDowIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeCnfDowIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCnfDowIchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeCnfDowIret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeCnfDowIret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
  end
  object DsNFeCnfDowI: TDataSource
    DataSet = QrNFeCnfDowI
    Left = 156
    Top = 332
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 580
    Top = 536
    object IncluiChave1: TMenuItem
      Caption = '&Inclui Chave NFe ao lote'
      OnClick = IncluiChave1Click
    end
    object Retirachave1: TMenuItem
      Caption = '&Retira Chave NF-e do lote'
      OnClick = Retirachave1Click
    end
  end
  object QrFatID_0053: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id'
      'FROM nfecaba'
      'WHERE FatID=52'
      'AND Empresa=-11'
      'AND ide_tpAmb=2')
    Left = 176
    Top = 40
    object QrFatID_0053Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrFatID_0053ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrFatID_0053NO_Terceiro: TWideStringField
      FieldName = 'NO_Terceiro'
      Size = 100
    end
  end
end
