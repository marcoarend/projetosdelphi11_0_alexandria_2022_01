unit NFeCnfDowC_0100;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmNFeCnfDowC_0100 = class(TForm)
    PainelDados: TPanel;
    DsNFeCnfDowC: TDataSource;
    QrNFeCnfDowC: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    QrNFeCnfDowI: TmySQLQuery;
    DsNFeCnfDowI: TDataSource;
    N1: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_XML: TWebBrowser;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    DBGrid2: TDBGrid;
    PMNFe: TPopupMenu;
    BtNFe: TBitBtn;
    QrNFeCnfDowCCodigo: TIntegerField;
    QrNFeCnfDowCEmpresa: TIntegerField;
    QrNFeCnfDowCNome: TWideStringField;
    QrNFeCnfDowCversao: TFloatField;
    QrNFeCnfDowCtpAmb: TSmallintField;
    QrNFeCnfDowCCNPJ: TWideStringField;
    QrNFeCnfDowCFilial: TIntegerField;
    QrNFeCnfDowCNO_Empresa: TWideStringField;
    QrNFeCnfDowICodigo: TIntegerField;
    QrNFeCnfDowIControle: TIntegerField;
    QrNFeCnfDowIEmpresa: TIntegerField;
    QrNFeCnfDowIchNFe: TWideStringField;
    IncluiChave1: TMenuItem;
    QrFatID_0053: TmySQLQuery;
    QrFatID_0053Id: TWideStringField;
    Envialoteaofisco1: TMenuItem;
    QrNFeCnfDowCret_versao: TFloatField;
    QrNFeCnfDowCret_tpAmb: TSmallintField;
    QrNFeCnfDowCret_verAplic: TWideStringField;
    QrNFeCnfDowCret_cStat: TIntegerField;
    QrNFeCnfDowCret_dhResp: TDateTimeField;
    QrNFeCnfDowCret_xMotivo: TWideStringField;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrNFeCnfDowCNO_ret_tpAmb: TWideStringField;
    QrNFeCnfDowIret_cStat: TIntegerField;
    QrNFeCnfDowIret_xMotivo: TWideStringField;
    Retirachave1: TMenuItem;
    QrFatID_0053ide_dEmi: TDateField;
    QrFatID_0053NO_Terceiro: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeCnfDowCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeCnfDowCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeCnfDowCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure QrNFeCnfDowCAfterScroll(DataSet: TDataSet);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFeCnfDowICalcFields(DataSet: TDataSet);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure BtNFeClick(Sender: TObject);
    procedure IncluiChave1Click(Sender: TObject);
    procedure Retirachave1Click(Sender: TObject);
    procedure Envialoteaofisco1Click(Sender: TObject);
  private
    F_nfe_chaves: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    (* N�o usa
    procedure AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad: Integer;
              ReopenQry: Boolean);
    *)
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeCnfDowI(Controle: Integer);
  end;

var
  FmNFeCnfDowC_0100: TFmNFeCnfDowC_0100;
const
  FFormatFloat = '00000';
  verDowNFeCnft_Int =  1.00;

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleNFe_0000, CreateNFE,
DmkDAC_PF, NFeXMLGerencia, NFeCnfDowI_0100, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeCnfDowC_0100.Lerarquivo1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_DownloadNFeConfirmadas(
    QrNFeCnfDowCCodigo.Value, QrNFeCnfDowCEmpresa.Value, QrNFeCnfDowI, True);
end;

procedure TFmNFeCnfDowC_0100.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeCnfDowC_0100.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeCnfDowCCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeCnfDowC_0100.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeCnfDowC_0100.DefParams;
begin
  VAR_GOTOTABELA := 'NFeCnfDowC';
  VAR_GOTOMYSQLTABLE := QrNFeCnfDowC;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfecnfdowc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmNFeCnfDowC_0100.Envialoteaofisco1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_DownloadNFeConfirmadas(
    QrNFeCnfDowCCodigo.Value, QrNFeCnfDowCEmpresa.Value, QrNFeCnfDowI, False);
end;

procedure TFmNFeCnfDowC_0100.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeCnfDowC_0100.PMLotePopup(Sender: TObject);
var
  HabilitaA: Boolean;
begin
  HabilitaA := (QrNFeCnfDowC.State = dsBrowse) and (QrNFeCnfDowC.RecordCount > 0);
  //Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  //
end;

procedure TFmNFeCnfDowC_0100.PMNFePopup(Sender: TObject);
begin
(*
  Habilita := (QrNFeCnfRIts.RecordCount > 0) and (QrNFeCnfRItscSitConf.Value = 0);
  RegistraChaveNFeparaManifestar1.Enabled := Habilita;
*)
end;

procedure TFmNFeCnfDowC_0100.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeCnfDowC_0100.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeCnfDowC_0100.ReopenNFeCnfDowI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCnfDowI, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfecnfdowi ',
  'WHERE Codigo=' + Geral.FF0(QrNFeCnfDowCCodigo.Value),
  '']);
end;

procedure TFmNFeCnfDowC_0100.Retirachave1Click(Sender: TObject);
begin
  if QrNFeCnfDowIret_cStat.Value = 140 then
    Geral.MB_Aviso('Retirada cancelada!  O Status n�o permite!')
  else
  begin
    UMyMod.ExcluiRegistroInt1('Confirma a retiada da chave NF-e?', 'nfecnfdowi',
      'Controle', QrNFeCnfDowIControle.Value, Dmod.MyDB);
    ReopenNFeCnfDowI(QrNFeCnfDowIControle.Value);
  end;
end;

procedure TFmNFeCnfDowC_0100.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeCnfDowC_0100.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeCnfDowC_0100.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeCnfDowC_0100.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeCnfDowC_0100.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeCnfDowC_0100.Alteraloteatual1Click(Sender: TObject);
begin
{
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeCnfDowC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfecnfdowc');
}
end;

procedure TFmNFeCnfDowC_0100.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeCnfDowCCodigo.Value;
  Close;
end;

(* N�o usa
procedure TFmNFeCnfDowC_0100.AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad: Integer;
ReopenQry: Boolean);
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecnfrits', False, [
  'NFa_IDCtrl'], ['IDCad'], [
  NFa_IDCtrl], [IDCad], True) then
    if ReopenQry then
      ReopenNFeCnfRIts(IDCad);
end;
*)

procedure TFmNFeCnfDowC_0100.BtConfirmaClick(Sender: TObject);
const
  _Hora = 1/24;
var
  Nome, (*xServ,*) CNPJ: String;
  Codigo, Empresa, tpAmb: Integer;
  versao: Double;
  //Qry: TmySQLQuery;
  //Antes, Agora, dhDif: TDateTime;
begin
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa') then
    Exit;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  DmNFe_0000.ReopenOpcoesNFe(Empresa, True);
  Nome           := EdNome.Text;
  versao         := verDowNFeCnft_Int;
  tpAmb          := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
  //xServ          := CO_DOWNLOAD_NFE;
  CNPJ           := Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value);
  //
  {
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(dhResp) dhResp ',
    'FROM nfecnfrcab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND tpAmb=' + Geral.FF0(tpAmb),
    ' ']);
    //
    Antes := Qry.FieldByName('dhResp').AsDateTime;
    Agora := DModG.ObtemAgora();
    dhDif := Agora - Antes;
    if dhDif < _Hora then
    begin
      Geral.MB_Aviso('O tempo m�nimo de espera entre consultas � de 1 hora.' +
      'Faltam ' + Geral.FDT(_Hora - dhDif, 110));
      Exit;
    end;
  finally
    Qry.Free;
  end;
  }
  Codigo := UMyMod.BPGS1I32('nfecnfdowc', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecnfdowc', False, [
  'Empresa', 'Nome', 'versao',
  'tpAmb', (*'xServ',*) 'CNPJ'(*,
  'XML'*)], [
  'Codigo'], [
  Empresa, Nome, versao,
  tpAmb, (*xServ,*) CNPJ(*,
  XML*)], [
  Codigo], True) then
  begin
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeCnfDowC_0100.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeCnfDowC', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeCnfDowC_0100.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeCnfDowC_0100.BtNFeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeCnfDowC_0100.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmNFeCnfDowC_0100.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeCnfDowCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeCnfDowC_0100.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeCnfDowC_0100.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeCnfDowC_0100.SbNovoClick(Sender: TObject);
begin
{:::
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeCnfDowCCodUsu.Value, LaRegistro.Caption);
}
end;

procedure TFmNFeCnfDowC_0100.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeCnfDowC_0100.QrNFeCnfDowCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeCnfDowC_0100.QrNFeCnfDowCAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeCnfDowI(0);
end;

procedure TFmNFeCnfDowC_0100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeCnfDowC_0100.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeCnfDowCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeCnfDowC', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeCnfDowC_0100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeCnfDowC_0100.IncluiChave1Click(Sender: TObject);
const
  Ativo = 0;
var
  VersaoNFe: Double;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe, ide_dEmi: String;
  ChaveNFe, emit_UF, NO_Terceiro: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando FatID = 53');
  F_nfe_chaves :=
    UnCreateNFE.RecriaTempTableNovo(ntrttNFeChaves, DmodG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatID_0053, Dmod.MyDB, [
  'SELECT cab.Id, cab.ide_dEmi,  ',
  'IF(cab.FatID=' + Geral.FF0(VAR_FATID_0053) + ', ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome),',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro ',
  'FROM nfecaba cab ',
  'LEFT JOIN entidades cli ON cli.Codigo=cab.CodInfoDest ',
  'LEFT JOIN entidades emi ON emi.Codigo=cab.CodInfoEmit ',
  'WHERE cab.FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND cab.Empresa=' + Geral.FF0(QrNFeCnfDowCEmpresa.Value),
  'AND cab.ide_tpAmb=' + Geral.FF0(QrNFeCnfDowCtpAmb.Value),
  'AND cab.Status=100',
  '']);
  if QrFatID_0053.RecordCount > 0 then
  begin
    QrFatID_0053.First;
    while not QrFatID_0053.Eof do
    begin
      ChaveNFe    := QrFatID_0053Id.Value;
      ide_dEmi    := Geral.FDT(QrFatID_0053ide_dEmi.Value, 1);
      NO_Terceiro := QrFatID_0053NO_Terceiro.Value;
      // N�o presisa saber! (at� quando?)
      VersaoNFe := 2.00;

      NFeXMLGeren.DesmontaChaveDeAcesso(ChaveNFe, VersaoNFe,
        UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
      emit_UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(UF_IBGE));
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_nfe_chaves, False, [
      'UF_IBGE', 'AAMM', 'CNPJ',
      'ModNFe', 'SerNFe', 'NumNFe',
      'tpEmis', 'AleNFe', 'emit_UF',
      'ide_dEmi', 'RazaoSocial'], [
      'ChaveNFe', 'Ativo'], [
      UF_IBGE, AAMM, CNPJ,
      ModNFe, SerNFe, NumNFe,
      tpEmis, AleNFe, emit_UF,
      ide_dEmi, NO_Terceiro], [
      ChaveNFe, Ativo], False);
      //
      QrFatID_0053.Next;
    end;
    if DBCheck.CriaFm(TFmNFeCnfDowI_0100, FmNFeCnfDowI_0100, afmoNegarComAviso) then
    begin
      FmNFeCnfDowI_0100.F_nfe_chaves := F_nfe_chaves;
      FmNFeCnfDowI_0100.ReopenNFeChaves('');
      FmNFeCnfDowI_0100.ShowModal;
      FmNFeCnfDowI_0100.Destroy;
    end;
    ReopenNFeCnfDowI(0);
  end else
    Geral.MB_Aviso('N�o foi localizada nenhuma chave para download!');
  // ...
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmNFeCnfDowC_0100.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeCnfDowC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfecnfdowc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFeCnfDowC_0100.QrNFeCnfDowCBeforeOpen(DataSet: TDataSet);
begin
  QrNFeCnfDowCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeCnfDowC_0100.QrNFeCnfDowCCalcFields(DataSet: TDataSet);
begin
  QrNFeCnfDowCNO_ret_tpAmb.Value :=
    UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,   QrNFeCnfDowCret_tpAmb.Value);
end;

procedure TFmNFeCnfDowC_0100.QrNFeCnfDowICalcFields(DataSet: TDataSet);
begin
{
  QrNFeCnfRCabDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeCnfRCabdhResp.Value, 0);
  QrNFeCnfRCabNO_Ambiente.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,
    QrNFeCnfRCabtpAmb.Value);
  QrNFeCnfRCabNO_indCont.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indCont,
    QrNFeCnfRCabindCont.Value);
}
end;

end.

