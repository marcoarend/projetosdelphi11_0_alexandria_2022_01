object FmNFeWebServicesGer_0000: TFmNFeWebServicesGer_0000
  Left = 339
  Top = 185
  Caption = 'NFe-WEBSV-002 :: Gerenciamento de Listas de Web Services da NF-e'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 625
        Height = 32
        Caption = 'Gerenciamento de Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 625
        Height = 32
        Caption = 'Gerenciamento de Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 625
        Height = 32
        Caption = 'Gerenciamento de Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object PNPesq: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 110
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 4
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label1: TLabel
            Left = 64
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Servi'#231'o:'
          end
          object Label2: TLabel
            Left = 220
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object Label8: TLabel
            Left = 8
            Top = 52
            Width = 25
            Height = 13
            Caption = 'URL:'
          end
          object EdUF: TdmkEdit
            Left = 8
            Top = 23
            Width = 50
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '%%'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '%%'
            ValWarn = False
            OnChange = EdUFChange
          end
          object RGtpAmb: TdmkRadioGroup
            Left = 275
            Top = 4
            Width = 461
            Height = 40
            Caption = ' Identifica'#231#227'o do Ambiente: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              '0 - Nenhum'
              '1 - Produ'#231#227'o'
              '2 - Homologa'#231#227'o')
            TabOrder = 3
            OnClick = RGtpAmbClick
            UpdType = utYes
            OldValor = 0
          end
          object EdServico: TdmkEdit
            Left = 64
            Top = 23
            Width = 150
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '%%'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '%%'
            ValWarn = False
            OnChange = EdServicoChange
          end
          object EdVersao: TdmkEdit
            Left = 220
            Top = 23
            Width = 50
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '4,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 4.000000000000000000
            ValWarn = False
            OnChange = EdVersaoChange
          end
          object EdURL: TdmkEdit
            Left = 8
            Top = 71
            Width = 600
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '%%'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '%%'
            ValWarn = False
          end
          object BtPesquisa: TBitBtn
            Tag = 22
            Left = 616
            Top = 62
            Width = 120
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 5
            OnClick = BtPesquisaClick
          end
        end
        object DBGrade: TdmkDBGrid
          Left = 2
          Top = 125
          Width = 808
          Height = 340
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Servico'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Versao'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpAmb_TXT'
              Title.Caption = 'Ambiente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'URL'
              Width = 400
              Visible = True
            end>
          Color = clWindow
          DataSource = DsNFe_WS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Servico'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Versao'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpAmb_TXT'
              Title.Caption = 'Ambiente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'URL'
              Width = 400
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 263
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
      object BtImportar: TBitBtn
        Tag = 19
        Left = 389
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Importa'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtImportarClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 136
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtAlteraClick
      end
    end
  end
  object QrNFe_WS: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM nfe_ws')
    Left = 256
    Top = 232
    object QrNFe_WSUF: TWideStringField
      FieldName = 'UF'
      Size = 10
    end
    object QrNFe_WSServico: TWideStringField
      FieldName = 'Servico'
      Size = 50
    end
    object QrNFe_WSURL: TWideStringField
      FieldName = 'URL'
      Size = 255
    end
    object QrNFe_WStpAmb: TSmallintField
      FieldName = 'tpAmb'
      MaxValue = 2
    end
    object QrNFe_WStpAmb_TXT: TWideStringField
      FieldName = 'tpAmb_TXT'
      Size = 11
    end
    object QrNFe_WSVersao: TFloatField
      FieldName = 'Versao'
      DisplayFormat = '0.00'
    end
  end
  object DsNFe_WS: TDataSource
    DataSet = QrNFe_WS
    Left = 284
    Top = 232
  end
  object frxNFe_WEBSV_002_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 42033.694879050920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 477
    Top = 253
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsNFe_WS
        DataSetName = 'frxDsNFe_WS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.125996460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213049999999600000
          Height = 18.897635350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Rela'#231#227'o de Servi'#231'os Web')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 510.236549999999600000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'URL:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Servi'#231'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vers'#227'o:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 196.535560000000000000
        Width = 699.213050000000000000
        DataSet = frxDsNFe_WS
        DataSetName = 'frxDsNFe_WS'
        RowCount = 0
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 510.236549999999600000
          Height = 13.228346460000000000
          DataField = 'URL'
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_WS."URL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Width = 132.283525590000000000
          Height = 13.228346460000000000
          DataField = 'Servico'
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_WS."Servico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 56.692925590000000000
          Height = 13.228346460000000000
          DataField = 'Versao'
          DataSet = frxDsNFe_WS
          DataSetName = 'frxDsNFe_WS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_WS."Versao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 113.385900000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsNFe_WS."tpAmb_TXT"'
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 661.417749999999600000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Ambiente: [frxDsNFe_WS."tpAmb_TXT"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 154.960730000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsNFe_WS."UF"'
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 646.299629999999600000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF: [frxDsNFe_WS."UF"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 272.126160000000000000
        Width = 699.213050000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 234.330860000000000000
        Width = 699.213050000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.677180000000000000
        Top = 347.716760000000000000
        Width = 699.213050000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 535.693260000000000000
          Top = 15.118119999999980000
          Width = 164.000000000000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 434.645950000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.net.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 264.567099999999600000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsNFe_WS: TfrxDBDataset
    UserName = 'frxDsNFe_WS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'UF=UF'
      'Servico=Servico'
      'URL=URL'
      'tpAmb=tpAmb'
      'tpAmb_TXT=tpAmb_TXT'
      'Versao=Versao')
    DataSet = QrNFe_WS
    BCDToCurrency = False
    DataSetOptions = []
    Left = 505
    Top = 253
  end
end
