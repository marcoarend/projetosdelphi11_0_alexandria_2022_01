unit NFeWebServices_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkRadioGroup,
  mySQLDbTables, System.Variants;

type
  TFmNFeWebServices_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel9: TPanel;
    QrNFW: TMySQLQuery;
    PCWS: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Label27: TLabel;
    SbNFe: TSpeedButton;
    EdCamNFe: TdmkEdit;
    RGtpAmb: TdmkRadioGroup;
    PCNFe: TPageControl;
    TabSheet2: TTabSheet;
    SGNFe: TStringGrid;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    TabSheet4: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    SbQrCode: TSpeedButton;
    EdCamQrCode: TdmkEdit;
    PCQrCode: TPageControl;
    TabSheet5: TTabSheet;
    SGQrCode: TStringGrid;
    TabSheet6: TTabSheet;
    Memo2: TMemo;
    Panel5: TPanel;
    CkExluirQrCode: TCheckBox;
    TabSheet7: TTabSheet;
    Panel6: TPanel;
    RGAmbiente: TdmkRadioGroup;
    Label20: TLabel;
    EdVersao: TdmkEdit;
    EdUF: TdmkEdit;
    Label2: TLabel;
    EdServico: TdmkEdit;
    Label3: TLabel;
    QrNFe_WS: TMySQLQuery;
    DsNFe_WS: TDataSource;
    DBGrid1: TDBGrid;
    QrNFe_WSUF: TWideStringField;
    QrNFe_WSServico: TWideStringField;
    QrNFe_WSVersao: TFloatField;
    QrNFe_WSURL: TWideStringField;
    QrNFe_WStpAmb: TSmallintField;
    PnLoad: TPanel;
    BtAbre: TBitBtn;
    BtSalvar: TBitBtn;
    BtBaixar: TBitBtn;
    PnEdit: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure SbQrCodeClick(Sender: TObject);
    procedure SbNFeClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure EdUFRedefinido(Sender: TObject);
    procedure EdServicoRedefinido(Sender: TObject);
    procedure PCWSChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNFW();
    procedure ReopenNFe_WS();
    procedure ObtemDados(const Linha: String; var UF, Versoes, URL: String);
    procedure ListaLinha(UF, Versoes, URL: String);
    procedure CarregaXLS_WS_NFe();
    //procedure CarregaXLS_WS_QrCode();
    procedure SalvarNFe();
    procedure SalvarQrCode();
  public
    { Public declarations }
  end;

  var
  FmNFeWebServices_0000: TFmNFeWebServices_0000;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

//const
  //FDestFile = 'C:\_Compilers\Delphi_XE2\NFe\Web Services 2014_10_28_Homologacao_CSV.csv';

procedure TFmNFeWebServices_0000.BtAbreClick(Sender: TObject);
var
  I: Integer;
  Arquivo, Linha, UF, Versoes, URL: String;
  Versao: Double;
  lstArq: TStringList;
begin
  case PCWS.ActivePageIndex of
    0: // NF-e
    begin
      Memo2.Lines.Clear;
      if pos('http', LowerCase(EdCamNFe.ValueVariant)) > 0 then
      begin
        Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
        Exit;
      end;
      //
      if MyObjects.Xls_To_StringGrid(SGNFe, EdCamNFe.Text, PB1, LaAviso1, LaAviso2, 1) then
      begin
        //BtCarrega.Enabled := True;
        CarregaXLS_WS_NFe();
        PCNFe.ActivePageIndex := 1;
        //
        BtSalvar.Visible := True;
      end;
    end;
    1: // QR Code
    begin
      Memo2.Lines.Clear;
      if pos('http', LowerCase(EdCamQrCode.ValueVariant)) > 0 then
      begin
        Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
        Exit;
      end;
      //
      if MyObjects.Xls_To_StringGrid(SGQrCode, EdCamQrCode.Text, PB1, LaAviso1, LaAviso2, 1) then
      begin
        //BtCarrega.Enabled := True;
        //CarregaXLS_WS_NFe();
        PCQrCode.ActivePageIndex := 0;
        //
        BtSalvar.Visible := True;
      end;
    end;
  end;
end;

procedure TFmNFeWebServices_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeWebServices_0000.BtSalvarClick(Sender: TObject);
begin
  case PCWS.ActivePageIndex of
    0: SalvarNFe();
    1: SalvarQrCode();
  end;
end;

procedure TFmNFeWebServices_0000.SalvarNFe();
var
  I, J, TotVersoes, tpAmb: Integer;
  UF, Servico, Versoes, URL: String;
  Versao: Double;
  Res: Boolean;
begin
  Res          := False;
  tpAmb        := RGtpAmb.ItemIndex;
  PB1.Max      := SGNFe.RowCount -1;
  PB1.Position := 0;

  UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrAllUpd, DModG.AllID_DB, [
    'DELETE FROM nfe_ws WHERE tpAmb=' + Geral.FF0(tpAmb),
    '']);

  if MyObjects.FIC(tpAmb = 0, RGtpAmb, 'Defina a identifica��o do ambiente!') then Exit;

  ReopenNFW();

  for I := 1 to SGNFe.RowCount -1 do
  begin
    UF := Trim(Geral.Substitui(SGNFe.Cells[01, I], #9, ' '));

    if UF <> '' then
    begin
      Versoes    := Trim(Geral.Substitui(SGNFe.Cells[03, I], #9, ' '));
      Versoes    := Geral.SoNumero_TT(Versoes);
      TotVersoes := Round(Length(Versoes) / 4);

      for J := 1 to TotVersoes do
      begin
        if J = 1 then
          Versao := Geral.DMV(Copy(Versoes, 1, 3)) / 100
        else
          Versao := Geral.DMV(Copy(Versoes, ((J - 1) * 3) + 1, 3))  / 100;

        Servico := Trim(Geral.Substitui(SGNFe.Cells[02, I], #9, ' '));
        URL     := Trim(Geral.Substitui(SGNFe.Cells[04, I], #9, ' '));

        if not QrNFW.Locate('UF; Servico; Versao; URL; tpAmb',
          VarArrayOf([UF, Servico, Versao, URL, tpAmb]), []) then
        begin
          if not UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'nfe_ws', False,
            ['UF', 'Servico', 'Versao', 'URL', 'tpAmb'], [],
            [UF, Servico, Versao, URL, tpAmb], [], True) then
          begin
            Geral.MB_Erro('Falha ao carregar arquivo!');
            Exit;
          end else
            Res := True;
        end;
      end;
    end;
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
  end;

  if Res then
    Geral.MB_Aviso('Dados importados com sucesso!');

  PB1.Position := 0;
end;

procedure TFmNFeWebServices_0000.SalvarQrCode;
var
  I: Integer;
var
  UF, URL, Servico: String;
  tpAmb: Integer;
  Versao: Double;
  SQLType: TSQLType;
begin
  PB1.Max      := SGQrCode.RowCount -1;
  PB1.Position := 0;
  //
  for I := 1 to SGQrCode.RowCount - 1 do
  begin
    MyObjects.UpdPB(PB1, LaAViso1, LaAviso2);
    //
    SQLType        := stIns;
    UF             := SGQrCode.Cells[1, I];
    Servico        := SGQrCode.Cells[2, I];
    Versao         := Geral.DMV(SGQrCode.Cells[3, I]);
    URL            := Trim(SGQrCode.Cells[4, I]);
    tpAmb          := Geral.IMV(SGQrCode.Cells[5, I]);
    //
    DModG.QrAllUpd.DataBAse := DModG.AllID_DB;
    //
    if UF <> '' then
    begin
      if CkExluirQrCode.Checked then
      begin
        DModG.AllID_DB.Execute('DELETE FROM qrcode_ws WHERE UF="' + UF +
        '" AND Servico="' + Servico + '" AND Versao=' + Geral.FFT_Dot(Versao, 2,
        siPositivo) + ' AND tpAmb=' + Geral.FF0(tpAmb));
      end;
      if UMyMod.SQLInsUpd(DModG.QrAllUpd, SQLType, 'qrcode_ws', False, [], [
      'UF', 'Servico', 'Versao', 'URL', 'tpAmb'], [], [
      UF, Servico, Versao, URL, tpAmb], True) then
      begin
        //
      end;
    end;
  end;
end;

procedure TFmNFeWebServices_0000.CarregaXLS_WS_NFe();
const
  c1 = 'if x = Uppercase(''';
  c2 = ''') then FURL := ''';
  c3 = ''' else';
var
  Linha: String;
var
  //LeftZeros, InfoVazio: Byte;
  R, K(*, P, L, N*): Integer;
  //Grupo, Nome, ID, Campo, Descricao, Elemento, Pai, Tipo, Tamanho, Ocorrencia,
  //Codigo, Observacao, Casas, TamVar, CodTxt, MinTxt, MaxTxt: String;
  //OcorMin, OcorMax, TamMin, TamMax, DeciCasas, Ordem: Integer;
  UF, Servico, Versoes, Versao, URL: String;
begin
  for R := 1 to SGNFe.RowCount -1 do
  begin
    PB1.Position := PB1.Position + 1;
    UF      := Trim(Geral.Substitui(SGNFe.Cells[01, R], #9, ' '));
    Servico := Trim(Geral.Substitui(SGNFe.Cells[02, R], #9, ' '));
    Versoes := Trim(Geral.Substitui(SGNFe.Cells[03, R], #9, ' '));
    URL     := Trim(Geral.Substitui(SGNFe.Cells[04, R], #9, ' '));
    if UF <> '' then
    begin
      K := pos('/', Versoes);
      while K > 0 do
      begin
        Versao := Trim(Copy(Versoes, 1, K - 1));
        if Versao <> '' then
          Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3;
        Versoes := Trim(Copy(Versoes, K + 1));
        //
        K := pos('/', Versoes);
      end;
      Versao := Versoes;
      if Versao <> '' then
        Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3
    end
    else
      Linha := '//' + UF + ' ' + Servico + ' ' + Versoes + ' ' + URL;
    //
    Memo2.Lines.Add(Linha);
  end;
end;

procedure TFmNFeWebServices_0000.EdServicoRedefinido(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServices_0000.EdUFRedefinido(Sender: TObject);
begin
  ReopenNFe_WS();
end;

{
procedure TFmNFeWebServices_0310.CarregaXLS_WS_QrCode();
const
  c1 = 'if x = Uppercase(''';
  c2 = ''') then FURL := ''';
  c3 = ''' else';
var
  Linha: String;
var
  //LeftZeros, InfoVazio: Byte;
  R, K(*, P, L, N*): Integer;
  //Grupo, Nome, ID, Campo, Descricao, Elemento, Pai, Tipo, Tamanho, Ocorrencia,
  //Codigo, Observacao, Casas, TamVar, CodTxt, MinTxt, MaxTxt: String;
  //OcorMin, OcorMax, TamMin, TamMax, DeciCasas, Ordem: Integer;
  UF, Servico, Versoes, Versao, URL: String;
begin
  for R := 1 to SGQrCode.RowCount -1 do
  begin
    PB1.Position := PB1.Position + 1;
    UF      := Trim(Geral.Substitui(SGQrCode.Cells[01, R], #9, ' '));
    Servico := Trim(Geral.Substitui(SGQrCode.Cells[02, R], #9, ' '));
    Versoes := Trim(Geral.Substitui(SGQrCode.Cells[03, R], #9, ' '));
    URL     := Trim(Geral.Substitui(SGQrCode.Cells[04, R], #9, ' '));
    if UF <> '' then
    begin
      K := pos('/', Versoes);
      while K > 0 do
      begin
        Versao := Trim(Copy(Versoes, 1, K - 1));
        if Versao <> '' then
          Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3;
        Versoes := Trim(Copy(Versoes, K + 1));
        //
        K := pos('/', Versoes);
      end;
      Versao := Versoes;
      if Versao <> '' then
        Linha := c1 + Trim(Uppercase(UF)) + ' ' + Servico + ' ' + Versao + c2 + URL + c3
    end
    else
      Linha := '//' + UF + ' ' + Servico + ' ' + Versoes + ' ' + URL;
    //
    Memo1.Lines.Add(Linha);
  end;
end;
}

procedure TFmNFeWebServices_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeWebServices_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PCWS.ActivePageIndex := 0;
  PnLoad.Visible := True;
  PnEdit.Visible := False;
  BtSalvar.Visible  := False;
  RGtpAmb.ItemIndex := 0;
end;

procedure TFmNFeWebServices_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeWebServices_0000.ListaLinha(UF, Versoes, URL: String);
const
  c1 = 'if x = Uppercase(''';
  c2 = ''') then FURL := ''';
  c3 = '''';
var
  Linha: String;
begin
  if UF <> '' then
    Linha := c1 + Trim(Uppercase(UF)) + ' ' + Versoes + ' ' + c2 + URL + c3
  else
    Linha := '//' + UF + ' ' + Versoes + ' ' + URL;
  //
  Memo1.Lines.Add(Linha);
end;

procedure TFmNFeWebServices_0000.ObtemDados(const Linha: String; var UF, Versoes,
  URL: String);
var
  lstLin: TStringList;
  P: Integer;
begin
  UF      := '';
  Versoes := '';
  URL     := '';
  //
  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      //
      if lstLin.Count > 0 then
        UF    := lstLin[00];
      if lstLin.Count > 1 then
        Versoes := lstLin[01];
      if lstLin.Count > 2 then
        URL := lstLin[02];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
  end;
end;

procedure TFmNFeWebServices_0000.PCWSChange(Sender: TObject);
begin
  case PCWS.ActivePageIndex of
    0,1:
    begin
      PnLoad.Visible := True;
      PnEdit.Visible := False;
    end;
    2:
    begin
      PnLoad.Visible := False;
      PnEdit.Visible := True;
    end;
  end;
end;

procedure TFmNFeWebServices_0000.ReopenNFe_WS();
var
  Ambiente: Integer;
  UF, Servico: String;
  Versao: Double;
  SQL_Ambiente, SQL_UF, SQL_Servico, SQL_Versao: String;
begin
  SQL_UF := EmptyStr;
  SQL_Servico := EmptyStr;
  //
  Ambiente := RGAmbiente.ItemIndex;
  Versao := EdVersao.ValueVariant;
  UF := EdUF.Text;
  Servico := EdServico.Text;
  //
  SQL_Ambiente := 'WHERE tpAmb=' + Geral.FF0(Ambiente);
  SQL_Versao := 'AND Versao=' + Geral.FFT_Dot(Versao, 2, siPositivo);
  if UF <> EmptyStr then
    SQL_UF := 'AND UF = "' + UF + '"';
  if Servico <> EmptyStr then
    SQL_Servico := 'AND Servico = "' + Servico + '"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_WS, DModG.AllID_DB, [
  'SELECT * ',
  'FROM nfe_ws ',
  SQL_Ambiente,
  SQL_Versao,
  SQL_UF,
  SQL_Servico,
  '']);
end;

procedure TFmNFeWebServices_0000.ReopenNFW();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFW, DModG.AllID_DB, [
  'SELECT * ',
  'FROM nfe_ws ',
  '']);
end;

procedure TFmNFeWebServices_0000.RGAmbienteClick(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServices_0000.SbNFeClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir  := ExtractFileDir(EdCamNFe.Text);
  Arquivo := ExtractFileName(EdCamNFe.Text);
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo', '', [], Arquivo) then
    EdCamNFe.Text := Arquivo;
end;

procedure TFmNFeWebServices_0000.SbQrCodeClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir  := ExtractFileDir(EdCamQrCode.Text);
  Arquivo := ExtractFileName(EdCamQrCode.Text);
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo', '', [], Arquivo) then
    EdCamQrCode.Text := Arquivo;
end;

end.
