unit NFeWebServicesGer_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkRadioGroup, dmkEdit,
  dmkDBGrid, mySQLDbTables, System.Variants, frxClass, frxDBSet;

type
  TFmNFeWebServicesGer_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PNPesq: TPanel;
    Label27: TLabel;
    EdUF: TdmkEdit;
    RGtpAmb: TdmkRadioGroup;
    Label1: TLabel;
    EdServico: TdmkEdit;
    Label2: TLabel;
    EdVersao: TdmkEdit;
    Label8: TLabel;
    EdURL: TdmkEdit;
    BtPesquisa: TBitBtn;
    DBGrade: TdmkDBGrid;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    QrNFe_WS: TmySQLQuery;
    QrNFe_WSUF: TWideStringField;
    QrNFe_WSServico: TWideStringField;
    QrNFe_WSURL: TWideStringField;
    QrNFe_WStpAmb: TSmallintField;
    DsNFe_WS: TDataSource;
    QrNFe_WStpAmb_TXT: TWideStringField;
    BtImportar: TBitBtn;
    SbImprime: TBitBtn;
    frxNFe_WEBSV_002_001: TfrxReport;
    frxDsNFe_WS: TfrxDBDataset;
    QrNFe_WSVersao: TFloatField;
    BtAltera: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure EdUFChange(Sender: TObject);
    procedure EdServicoChange(Sender: TObject);
    procedure EdVersaoChange(Sender: TObject);
    procedure RGtpAmbClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormNFeWebServicesItm(SQLType: TSQLType);
  public
    { Public declarations }
    procedure ReopenNFe_WS();
  end;

  var
  FmNFeWebServicesGer_0000: TFmNFeWebServicesGer_0000;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, MyDBCheck,
  NFeWebServices_0000, NFeWebServicesItm_0000;

{$R *.DFM}

procedure TFmNFeWebServicesGer_0000.BtAlteraClick(Sender: TObject);
begin
  MostraFormNFeWebServicesItm(stUpd);
end;

procedure TFmNFeWebServicesGer_0000.BtExcluiClick(Sender: TObject);
var
  UF, Servico, Versao, URL: String;
  tpAmb: Integer;
begin
  if (QrNFe_WS.State <> dsInactive) and (QrNFe_WS.RecordCount > 0) then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do registro selecionado?') = ID_YES then
    begin
      UF      := QrNFe_WSUF.Value;
      Servico := QrNFe_WSServico.Value;
      Versao  := Geral.FFT_Dot(QrNFe_WSVersao.Value, 2, siPositivo);
      URL     := QrNFe_WSURL.Value;
      tpAmb   := QrNFe_WStpAmb.Value;
      //
      if UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllUpd, DModG.AllID_DB, [
        'DELETE FROM nfe_ws WHERE tpAmb=' + Geral.FF0(tpAmb),
        'AND UF = "' + UF + '"',
        'AND Servico = "' + Servico + '"',
        'AND Versao = "' + Versao + '"',
        'AND URL = "' + URL + '"',
        ''])
      then
        ReopenNFe_WS;
    end;
  end;
end;

procedure TFmNFeWebServicesGer_0000.BtImportarClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeWebServices_0000, FmNFeWebServices_0000, afmoNegarComAviso) then
  begin
    FmNFeWebServices_0000.ShowModal;
    FmNFeWebServices_0000.Destroy;
    //
    ReopenNFe_WS();
  end;
end;

procedure TFmNFeWebServicesGer_0000.BtIncluiClick(Sender: TObject);
begin
  MostraFormNFeWebServicesItm(stIns);
end;

procedure TFmNFeWebServicesGer_0000.BtPesquisaClick(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeWebServicesGer_0000.EdServicoChange(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.EdUFChange(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.EdVersaoChange(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeWebServicesGer_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeWebServicesGer_0000.MostraFormNFeWebServicesItm(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmNFeWebServicesItm_0000, FmNFeWebServicesItm_0000, afmoNegarComAviso) then
  begin
    FmNFeWebServicesItm_0000.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
      //
    else
    begin
      FmNFeWebServicesItm_0000.FUF := QrNFe_WSUF.Value;
      FmNFeWebServicesItm_0000.FServico := QrNFe_WSServico.Value;
      FmNFeWebServicesItm_0000.FVersao := QrNFe_WSVersao.Value;
      FmNFeWebServicesItm_0000.FtpAmb := QrNFe_WStpAmb.Value;
      //
      FmNFeWebServicesItm_0000.EdUF.ValueVariant := QrNFe_WSUF.Value;
      FmNFeWebServicesItm_0000.EdServico.ValueVariant := QrNFe_WSServico.Value;
      FmNFeWebServicesItm_0000.EdVersao.ValueVariant := QrNFe_WSVersao.Value;
      FmNFeWebServicesItm_0000.RGtpAmb.ItemIndex := QrNFe_WStpAmb.Value;
      FmNFeWebServicesItm_0000.EdURL.ValueVariant := QrNFe_WSURL.Value;
    end;
    FmNFeWebServicesItm_0000.ShowModal;
    FmNFeWebServicesItm_0000.Destroy;
  end;
end;

procedure TFmNFeWebServicesGer_0000.ReopenNFe_WS();
var
  UF, Servico, Versao, Url, SQLUf, SQLServico, SQLVersao, SQLUrl: String;
  tpAmb: Integer;
begin
  UF      := EdUF.ValueVariant;
  Servico := EdServico.ValueVariant;
  Versao  := Geral.FFT_Dot(EdVersao.ValueVariant, 2, siPositivo);
  tpAmb   := RGtpAmb.ItemIndex;
  Url     := EdURL.ValueVariant;
  //
  if UF <> '' then
    SQLUf := 'AND UF LIKE "' + UF + '" ';
  if Servico <> '' then
    SQLServico := 'AND Servico LIKE "' + Servico + '"';
  if Versao <> '0.00' then
    SQLVersao := 'AND Versao = "' + Versao + '"';
  if Url <> '' then
    SQLUrl := 'AND URL LIKE "' + Url + '"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_WS, DModG.AllID_DB, [
    'SELECT *, ',
    'CASE tpAmb ',
    'WHEN 0 THEN "Nenhum" ',
    'WHEN 1 THEN "Produ��o" ',
    'WHEN 2 THEN "Homologa��o" END tpAmb_TXT ',
    'FROM nfe_ws ',
    'WHERE tpAmb = ' + Geral.FF0(tpAmb),
    SQLUf,
    SQLServico,
    SQLVersao,
    SQLUrl,
    'ORDER BY tpAmb, UF, Servico, Versao ',
    '']);
  //Geral.MB_Teste(QrNFe_WS.SQL.Text);
end;

procedure TFmNFeWebServicesGer_0000.RGtpAmbClick(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0000.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxNFe_WEBSV_002_001, [
    DModG.frxDsMaster,
    frxDsNFe_WS
    ]);
  MyObjects.frxMostra(frxNFe_WEBSV_002_001, 'Rela��o de Servi�os Web');
end;

end.
