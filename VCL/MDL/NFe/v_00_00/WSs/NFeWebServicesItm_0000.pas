unit NFeWebServicesItm_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkRadioGroup, Variants;

type
  TFmNFeWebServicesItm_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnCad: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdUF: TdmkEdit;
    RGtpAmb: TdmkRadioGroup;
    EdServico: TdmkEdit;
    EdVersao: TdmkEdit;
    EdURL: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FUF, FServico: String;
    FVersao: Double;
    FtpAmb: Integer;
  end;

  var
  FmNFeWebServicesItm_0000: TFmNFeWebServicesItm_0000;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, NFeWebServicesGer_0000,
  ModuleGeral;

{$R *.DFM}

procedure TFmNFeWebServicesItm_0000.BtOKClick(Sender: TObject);
var
  UF, Servico, URL: String;
  tpAmb: Integer;
  Versao: Double;
  SQLType: TSQLType;
  Continua: Boolean;
begin
  SQLType := ImgTipo.SQLType;
  UF      := EdUF.ValueVariant;
  Servico := EdServico.ValueVariant;
  Versao  := EdVersao.ValueVariant;
  URL     := EdURL.ValueVariant;
  tpAmb   := RGtpAmb.ItemIndex;
  //
  if MyObjects.FIC(UF = '', EdUF, 'Defina o UF!') then Exit;
  if MyObjects.FIC(Servico = '', EdServico, 'Defina o servi�o!') then Exit;
  if MyObjects.FIC(Versao = 0, EdVersao, 'Defina a vers�o!') then Exit;
  if MyObjects.FIC(tpAmb = 0, RGtpAmb, 'Defina a idenditifa��o do ambiente!') then Exit;
  if MyObjects.FIC(URL = '', EdURL, 'Defina o URL!') then Exit;
  //
  Continua := False;
  if SQLType = stIns then
  begin
    Continua := UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'nfe_ws', False,
    ['UF', 'Servico', 'Versao', 'tpAmb', 'URL'], [],
    [UF, Servico, Versao, tpAmb, URL], [], True);
  end else
  begin
    Continua := UMyMod.SQLInsUpd(DModG.QrAllUpd, stUpd, 'nfe_ws', False, [
    'UF', 'Servico', 'Versao', 'tpAmb', 'URL'], [
    'UF', 'Servico', 'Versao', 'tpAmb'], [
    UF, Servico, Versao, tpAmb, URL], [
    FUF, FServico, FVersao, FtpAmb], True);
  end;
  if Continua then
  begin
    FmNFeWebServicesGer_0000.ReopenNFe_WS();
    FmNFeWebServicesGer_0000.QrNFe_WS.Locate('UF; Servico; Versao; URL; tpAmb',
      VarArrayOf([UF, Servico, Versao, URL, tpAmb]), []);
    Close;
  end;
end;

procedure TFmNFeWebServicesItm_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeWebServicesItm_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeWebServicesItm_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FUF := EmptyStr;
  FServico := EmptyStr;
  FVersao := 0.00;
  FtpAmb := 0;
end;

procedure TFmNFeWebServicesItm_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
