object FmNFeWebServices_0000: TFmNFeWebServices_0000
  Left = 339
  Top = 185
  Caption = 'NFe-WEBSV-001 :: Listas de Web Services da NF-e'
  ClientHeight = 629
  ClientWidth = 1006
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1006
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 958
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 910
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 394
        Height = 32
        Caption = 'Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 394
        Height = 32
        Caption = 'Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 394
        Height = 32
        Caption = 'Listas de Web Services da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1006
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 860
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 858
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnLoad: TPanel
        Left = 0
        Top = 0
        Width = 393
        Height = 53
        Align = alLeft
        TabOrder = 0
        object BtAbre: TBitBtn
          Tag = 28
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Carregar'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtAbreClick
        end
        object BtSalvar: TBitBtn
          Tag = 24
          Left = 138
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Salvar'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtSalvarClick
        end
        object BtBaixar: TBitBtn
          Tag = 19
          Left = 264
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Baixar'
          NumGlyphs = 2
          TabOrder = 2
          Visible = False
        end
      end
      object PnEdit: TPanel
        Left = 393
        Top = 0
        Width = 393
        Height = 53
        Align = alLeft
        TabOrder = 1
        Visible = False
        object BitBtn1: TBitBtn
          Tag = 10
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Inclui'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtAbreClick
        end
        object BitBtn2: TBitBtn
          Tag = 11
          Left = 138
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Altera'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtSalvarClick
        end
        object BitBtn3: TBitBtn
          Tag = 12
          Left = 264
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Exclui'
          NumGlyphs = 2
          TabOrder = 2
          Visible = False
        end
      end
    end
  end
  object PCWS: TPageControl
    Left = 0
    Top = 48
    Width = 1006
    Height = 453
    ActivePage = TabSheet4
    Align = alTop
    TabOrder = 2
    OnChange = PCWSChange
    object TabSheet1: TTabSheet
      Caption = 'NF-e'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 998
        Height = 425
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 52
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 7
            Width = 116
            Height = 13
            Caption = 'Arquivo a ser carregado:'
          end
          object SbNFe: TSpeedButton
            Left = 505
            Top = 23
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbNFeClick
          end
          object EdCamNFe: TdmkEdit
            Left = 8
            Top = 23
            Width = 490
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 
              'C:\_Compilers\Delphi_XE2\NFe\Web Services 2014_10_28_Homologacao' +
              '.xlsx'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 
              'C:\_Compilers\Delphi_XE2\NFe\Web Services 2014_10_28_Homologacao' +
              '.xlsx'
            ValWarn = False
          end
          object RGtpAmb: TdmkRadioGroup
            Left = 531
            Top = 7
            Width = 461
            Height = 45
            Caption = ' Identifica'#231#227'o do Ambiente: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              '0 - Nenhum'
              '1 - Produ'#231#227'o'
              '2 - Homologa'#231#227'o')
            TabOrder = 1
            UpdType = utYes
            OldValor = 0
          end
        end
        object PCNFe: TPageControl
          Left = 0
          Top = 52
          Width = 998
          Height = 373
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 1
          object TabSheet2: TTabSheet
            Caption = 'Arquivo carregado'
            object SGNFe: TStringGrid
              Left = 0
              Top = 0
              Width = 990
              Height = 345
              Align = alClient
              TabOrder = 0
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Lista de Webservices'
            ImageIndex = 1
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 345
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              WantReturns = False
              WordWrap = False
              ExplicitHeight = 288
            end
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'QR Code'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 998
        Height = 425
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 52
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 7
            Width = 116
            Height = 13
            Caption = 'Arquivo a ser carregado:'
          end
          object SbQrCode: TSpeedButton
            Left = 505
            Top = 23
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbQrCodeClick
          end
          object EdCamQrCode: TdmkEdit
            Left = 8
            Top = 23
            Width = 490
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\_Sincro\_MLArend\NFe\URLsQrCode.csv'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\_Sincro\_MLArend\NFe\URLsQrCode.csv'
            ValWarn = False
          end
        end
        object PCQrCode: TPageControl
          Left = 0
          Top = 52
          Width = 998
          Height = 373
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 1
          object TabSheet5: TTabSheet
            Caption = 'Arquivo carregado'
            object SGQrCode: TStringGrid
              Left = 0
              Top = 41
              Width = 990
              Height = 304
              Align = alClient
              TabOrder = 0
            end
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 990
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object CkExluirQrCode: TCheckBox
                Left = 8
                Top = 12
                Width = 285
                Height = 17
                Caption = 'Excluir registro atual'
                TabOrder = 0
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Lista de Webservices'
            ImageIndex = 1
            object Memo2: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 345
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              WantReturns = False
              WordWrap = False
            end
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = ' Dados no BD'
      ImageIndex = 2
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 998
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label20: TLabel
          Left = 328
          Top = 4
          Width = 36
          Height = 13
          Caption = 'Vers'#227'o:'
        end
        object Label2: TLabel
          Left = 404
          Top = 4
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object Label3: TLabel
          Left = 464
          Top = 4
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
        end
        object RGAmbiente: TdmkRadioGroup
          Left = 3
          Top = 3
          Width = 318
          Height = 45
          Caption = ' Identifica'#231#227'o do Ambiente: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '0 - Nenhum'
            '1 - Produ'#231#227'o'
            '2 - Homologa'#231#227'o')
          TabOrder = 0
          OnClick = RGAmbienteClick
          UpdType = utYes
          OldValor = 0
        end
        object EdVersao: TdmkEdit
          Left = 328
          Top = 20
          Width = 69
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '4,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 4.000000000000000000
          ValWarn = False
        end
        object EdUF: TdmkEdit
          Left = 404
          Top = 20
          Width = 53
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdUFRedefinido
        end
        object EdServico: TdmkEdit
          Left = 464
          Top = 20
          Width = 257
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdServicoRedefinido
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 52
        Width = 998
        Height = 373
        Align = alClient
        DataSource = DsNFe_WS
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'tpAmb'
            Title.Caption = 'Ambiente'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Versao'
            Title.Caption = 'Vers'#227'o'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Servico'
            Title.Caption = 'Servi'#231'o'
            Width = 129
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'URL'
            Width = 656
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 502
    Width = 1006
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1002
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1002
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object QrNFW: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfe_ws')
    Left = 280
    Top = 280
  end
  object QrNFe_WS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfe_ws')
    Left = 348
    Top = 280
    object QrNFe_WSUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 10
    end
    object QrNFe_WSServico: TWideStringField
      FieldName = 'Servico'
      Required = True
      Size = 50
    end
    object QrNFe_WSVersao: TFloatField
      FieldName = 'Versao'
      Required = True
    end
    object QrNFe_WSURL: TWideStringField
      FieldName = 'URL'
      Required = True
      Size = 255
    end
    object QrNFe_WStpAmb: TSmallintField
      FieldName = 'tpAmb'
      Required = True
    end
  end
  object DsNFe_WS: TDataSource
    DataSet = QrNFe_WS
    Left = 348
    Top = 332
  end
end
