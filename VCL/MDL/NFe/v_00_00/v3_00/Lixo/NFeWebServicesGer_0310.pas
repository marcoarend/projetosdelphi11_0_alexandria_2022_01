unit NFeWebServicesGer_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkRadioGroup, dmkEdit,
  dmkDBGrid, mySQLDbTables, System.Variants, frxClass, frxDBSet;

type
  TFmNFeWebServicesGer_0310 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PNPesq: TPanel;
    Label27: TLabel;
    EdUF: TdmkEdit;
    RGtpAmb: TdmkRadioGroup;
    Label1: TLabel;
    EdServico: TdmkEdit;
    Label2: TLabel;
    EdVersao: TdmkEdit;
    Label8: TLabel;
    EdURL: TdmkEdit;
    BtPesquisa: TBitBtn;
    PnCad: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdUFCad: TdmkEdit;
    RGtpAmbCad: TdmkRadioGroup;
    EdServicoCad: TdmkEdit;
    EdVersaoCad: TdmkEdit;
    EdURLCad: TdmkEdit;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    Panel10: TPanel;
    BtDesiste2: TBitBtn;
    DBGrade: TdmkDBGrid;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    QrNFe_WS: TmySQLQuery;
    QrNFe_WSUF: TWideStringField;
    QrNFe_WSServico: TWideStringField;
    QrNFe_WSURL: TWideStringField;
    QrNFe_WStpAmb: TSmallintField;
    DsNFe_WS: TDataSource;
    QrNFe_WStpAmb_TXT: TWideStringField;
    BtImportar: TBitBtn;
    SbImprime: TBitBtn;
    frxNFe_WEBSV_002_001: TfrxReport;
    frxDsNFe_WS: TfrxDBDataset;
    QrNFe_WSVersao: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Insere: Boolean);
    procedure ReopenNFe_WS();
  public
    { Public declarations }
  end;

  var
  FmNFeWebServicesGer_0310: TFmNFeWebServicesGer_0310;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, MyDBCheck,
  NFeWebServices_0310;

{$R *.DFM}

procedure TFmNFeWebServicesGer_0310.BtConfirma2Click(Sender: TObject);
var
  UF, Servico, URL: String;
  tpAmb: Integer;
  Versao: Double;
begin
  UF      := EdUFCad.ValueVariant;
  Servico := EdServicoCad.ValueVariant;
  Versao  := EdVersaoCad.ValueVariant;
  URL     := EdURLCad.ValueVariant;
  tpAmb   := RGtpAmbCad.ItemIndex;
  //
  if MyObjects.FIC(UF = '', EdUFCad, 'Defina o UF!') then Exit;
  if MyObjects.FIC(Servico = '', EdServicoCad, 'Defina o servi�o!') then Exit;
  if MyObjects.FIC(Versao = 0, EdVersaoCad, 'Defina a vers�o!') then Exit;
  if MyObjects.FIC(tpAmb = 0, RGtpAmbCad, 'Defina a idenditifa��o do ambiente!') then Exit;
  if MyObjects.FIC(URL = '', EdURLCad, 'Defina o URL!') then Exit;
  //
  if UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'nfe_ws', False,
    ['UF', 'Servico', 'Versao', 'URL', 'tpAmb'], [],
    [UF, Servico, Versao, URL, tpAmb], [], True) then
  begin
    ReopenNFe_WS();
    MostraEdicao(False);
    //
    QrNFe_WS.Locate('UF; Servico; Versao; URL; tpAmb',
      VarArrayOf([UF, Servico, Versao, URL, tpAmb]), []);
  end;
end;

procedure TFmNFeWebServicesGer_0310.BtDesiste2Click(Sender: TObject);
begin
  MostraEdicao(False);
end;

procedure TFmNFeWebServicesGer_0310.BtExcluiClick(Sender: TObject);
var
  UF, Servico, Versao, URL: String;
  tpAmb: Integer;
begin
  if (QrNFe_WS.State <> dsInactive) and (QrNFe_WS.RecordCount > 0) then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do registro selecionado?') = ID_YES then
    begin
      UF      := QrNFe_WSUF.Value;
      Servico := QrNFe_WSServico.Value;
      Versao  := Geral.FFT_Dot(QrNFe_WSVersao.Value, 2, siPositivo);
      URL     := QrNFe_WSURL.Value;
      tpAmb   := QrNFe_WStpAmb.Value;
      //
      if UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllUpd, DModG.AllID_DB, [
        'DELETE FROM nfe_ws WHERE tpAmb=' + Geral.FF0(tpAmb),
        'AND UF = "' + UF + '"',
        'AND Servico = "' + Servico + '"',
        'AND Versao = "' + Versao + '"',
        'AND URL = "' + URL + '"',
        ''])
      then
        ReopenNFe_WS;
    end;
  end;
end;

procedure TFmNFeWebServicesGer_0310.BtImportarClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeWebServices_0310, FmNFeWebServices_0310, afmoNegarComAviso) then
  begin
    FmNFeWebServices_0310.ShowModal;
    FmNFeWebServices_0310.Destroy;
    //
    ReopenNFe_WS();
  end;
end;

procedure TFmNFeWebServicesGer_0310.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True);
end;

procedure TFmNFeWebServicesGer_0310.BtPesquisaClick(Sender: TObject);
begin
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeWebServicesGer_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeWebServicesGer_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao(False);
  ReopenNFe_WS();
end;

procedure TFmNFeWebServicesGer_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeWebServicesGer_0310.MostraEdicao(Insere: Boolean);
begin
  if Insere then
  begin
    PnCad.Visible   := True;
    PNPesq.Visible  := False;
    DBGrade.Enabled := False;
    //
    EdUFCad.ValueVariant      := '';
    EdServicoCad.ValueVariant := '';
    EdVersaoCad.ValueVariant  := '';
    RGtpAmbCad.ItemIndex      := 0;
    EdURLCad.ValueVariant     := '';
  end else
  begin
    PnCad.Visible   := False;
    PNPesq.Visible  := True;
    DBGrade.Enabled := True;
    //
    EdUF.ValueVariant      := '';
    EdServico.ValueVariant := '';
    EdVersao.ValueVariant  := '';
    RGtpAmb.ItemIndex      := 1;
    EdServico.ValueVariant := '';
  end;
end;

procedure TFmNFeWebServicesGer_0310.ReopenNFe_WS();
var
  UF, Servico, Versao, Url, SQLUf, SQLServico, SQLVersao, SQLUrl: String;
  tpAmb: Integer;
begin
  UF      := EdUF.ValueVariant;
  Servico := EdServico.ValueVariant;
  Versao  := EdVersao.ValueVariant;
  tpAmb   := RGtpAmb.ItemIndex;
  Url     := EdURL.ValueVariant;
  //
  if UF <> '' then
    SQLUf := 'AND UF LIKE "%' + UF + '%" ';
  if Servico <> '' then
    SQLServico := 'AND Servico LIKE "%' + Servico + '%"';
  if Versao <> '' then
    SQLVersao := 'AND Versao LIKE "%' + Versao + '%"';
  if Url <> '' then
    SQLUrl := 'AND URL = "%' + Url + '%"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFe_WS, DModG.AllID_DB, [
    'SELECT *, ',
    'CASE tpAmb ',
    'WHEN 0 THEN "Nenhum" ',
    'WHEN 1 THEN "Produ��o" ',
    'WHEN 2 THEN "Homologa��o" END tpAmb_TXT ',
    'FROM nfe_ws ',
    'WHERE tpAmb = ' + Geral.FF0(tpAmb),
    SQLUf,
    SQLServico,
    SQLVersao,
    SQLUrl,
    'ORDER BY tpAmb, UF, Servico, Versao ',
    '']);
end;

procedure TFmNFeWebServicesGer_0310.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxNFe_WEBSV_002_001, [
    DModG.frxDsMaster,
    frxDsNFe_WS
    ]);
  MyObjects.frxMostra(frxNFe_WEBSV_002_001, 'Rela��o de Servi�os Web');
end;

end.
