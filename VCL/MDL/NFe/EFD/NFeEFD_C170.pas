unit NFeEFD_C170;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkRadioGroup, dmkMemo,
  SPED_Listas, Vcl.Mask, Vcl.Menus;

type
  TFmNFeEFD_C170 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNivel1: TIntegerField;
    DsGraGruX: TDataSource;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    EdCST_ICMS: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdCFOP: TdmkEdit;
    Label8: TLabel;
    EdCOD_NAT: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdALIQ_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    Label11: TLabel;
    EdVL_BC_ICMS_ST: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    EdALIQ_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    Label14: TLabel;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdCST_IPI: TdmkEdit;
    EdVL_BC_IPI: TdmkEdit;
    EdALIQ_IPI: TdmkEdit;
    EdVL_IPI: TdmkEdit;
    EdIND_APUR: TdmkEdit;
    Label24: TLabel;
    EdIND_APUR_TXT: TdmkEdit;
    EdCOD_ENQ: TdmkEdit;
    Label25: TLabel;
    Panel7: TPanel;
    Label26: TLabel;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label16: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    EdCST_PIS: TdmkEdit;
    EdVL_BC_PIS: TdmkEdit;
    EdALIQ_PIS_p: TdmkEdit;
    EdQUANT_BC_PIS: TdmkEdit;
    EdALIQ_PIS_r: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    Panel9: TPanel;
    Label4: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdQTD: TdmkEdit;
    Label2: TLabel;
    EdUNID: TdmkEdit;
    EdVL_ITEM: TdmkEdit;
    Label3: TLabel;
    Label5: TLabel;
    EdVL_DESC: TdmkEdit;
    RGIND_MOV: TdmkRadioGroup;
    GroupBox5: TGroupBox;
    Panel10: TPanel;
    Label17: TLabel;
    Label21: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    EdCST_COFINS: TdmkEdit;
    EdVL_BC_COFINS: TdmkEdit;
    EdALIQ_COFINS_p: TdmkEdit;
    EdQUANT_BC_COFINS: TdmkEdit;
    EdALIQ_COFINS_r: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    MeDESCR_COMPL: TdmkMemo;
    Label34: TLabel;
    EdCOD_CTA: TdmkEdit;
    EdCOD_ITEM: TdmkEdit;
    Label35: TLabel;
    EdCST_ICMS_TXT: TdmkEdit;
    EdCFOP_TXT: TdmkEdit;
    QrTbSPEDEFD30: TmySQLQuery;
    QrTbSPEDEFD30CodTxt: TWideStringField;
    QrTbSPEDEFD30Codigo: TIntegerField;
    QrTbSPEDEFD30Nome: TWideStringField;
    EdCST_IPI_TXT: TdmkEdit;
    EdCST_PIS_TXT: TdmkEdit;
    EdCST_COFINS_TXT: TdmkEdit;
    Panel11: TPanel;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_mod: TSmallintField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAKIND_DOC: TWideStringField;
    QrNFeCabACNPJ_CPF: TWideStringField;
    QrNFeCabAemit_xNome: TWideStringField;
    QrNFeCabAemit_xMun: TWideStringField;
    QrNFeCabAemit_UF: TWideStringField;
    Label36: TLabel;
    DBEdit1: TDBEdit;
    DsNFeCabA: TDataSource;
    Label37: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label38: TLabel;
    DBEdit5: TDBEdit;
    Label39: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    QrNFeCabAide_serie: TSmallintField;
    DBEdit8: TDBEdit;
    Label40: TLabel;
    DBEdit9: TDBEdit;
    Label41: TLabel;
    DBEdit10: TDBEdit;
    Label42: TLabel;
    DBEdit11: TDBEdit;
    SbCFOP: TSpeedButton;
    PMCFOP: TPopupMenu;
    CadastrarCombinaoCFOPEmpresaReduzido1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdIND_APURRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdCST_ICMSRedefinido(Sender: TObject);
    procedure EdCFOPRedefinido(Sender: TObject);
    procedure EdCST_IPIRedefinido(Sender: TObject);
    procedure EdCST_PISRedefinido(Sender: TObject);
    procedure EdCST_COFINSRedefinido(Sender: TObject);
    procedure SbCFOPClick(Sender: TObject);
    procedure CadastrarCombinaoCFOPEmpresaReduzido1Click(Sender: TObject);
    procedure PMCFOPPopup(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FnItem, FCFOP_OUT: Integer;
  end;

  var
  FmNFeEFD_C170: TFmNFeEFD_C170;

implementation

uses UnMyObjects, NFe_PF, Module, UMySQLModule, DmkDAC_PF, ModProd, UnSPED_Geral;

{$R *.DFM}

procedure TFmNFeEFD_C170.BtOKClick(Sender: TObject);
(*
05 QTD Quantidade do item N - 05 O O
06 UNID Unidade do item (Campo 02 do registro 0190) C 006 - O O
07 VL_ITEM Valor total do item (mercadorias ou servi�os) N - 02 O O
08 VL_DESC Valor do desconto comercial N - 02 OC OC
09 IND_MOV Movimenta��o f�sica do ITEM/PRODUTO: 0. SIM 1. N�O C 001* - O O
10 CST_ICMS C�digo da Situa��o Tribut�ria referente ao ICMS, conforme a Tabela indicada no item 4.3.1  N 003* - O O
11 CFOP C�digo Fiscal de Opera��o e Presta��o N 004* - O O
12 COD_NAT C�digo da natureza da opera��o (campo 02 do Registro 0400) C 010 - OC OC
13 VL_BC_ICMS Valor da base de c�lculo do ICMS N - 02 OC OC
14 ALIQ_ICMS Al�quota do ICMS N 006 02 OC OC
15 VL_ICMS Valor do ICMS creditado/debitado N - 02 OC OC
16 VL_BC_ICMS_ST Valor da base de c�lculo referente � substitui��o tribut�ria N - 02 OC OC
17 ALIQ_ST Al�quota do ICMS da substitui��o tribut�ria na unidade da federa��o de destino N - 02 OC OC
18 VL_ICMS_ST Valor do ICMS referente � substitui��o tribut�ria N - 02 OC OC
19 IND_APUR Indicador de per�odo de apura��o do IPI: 0 - Mensal; 1 - Decendial C 001* - OC OC
20 CST_IPI C�digo da Situa��o Tribut�ria referente ao IPI, conforme a Tabela indicada no item 4.3.2. C 002* - OC OC
21 COD_ENQ C�digo de enquadramento legal do IPI, conforme tabela indicada no item 4.5.3. C 003* - OC OC
22 VL_BC_IPI Valor da base de c�lculo do IPI N - 02 OC OC
23 ALIQ_IPI Al�quota do IPI N 006 02 OC OC
24 VL_IPI Valor do IPI creditado/debitado N - 02 OC OC
25 CST_PIS C�digo da Situa��o Tribut�ria referente ao PIS. N 002* - OC OC
26 VL_BC_PIS Valor da base de c�lculo do PIS N 02 OC OC
27 ALIQ_PIS Al�quota do PIS (em percentual) N 008 04 OC OC
28 QUANT_BC_PIS Quantidade � Base de c�lculo PIS N 03 OC OC
29 ALIQ_PIS Al�quota do PIS (em reais) N 04 OC OC
30 VL_PIS Valor do PIS N - 02 OC OC
31 CST_COFINS C�digo da Situa��o Tribut�ria referente ao N 002* - OC OC
32 VL_BC_COFINS Valor da base de c�lculo da COFINS N 02 OC OC
33 ALIQ_COFINS Al�quota do COFINS (em percentual) N 008 04 OC OC
34 QUANT_BC_COFINS Quantidade � Base de c�lculo COFINS N 03 OC OC
35 ALIQ_COFINS Al�quota da COFINS (em reais) N 04 OC OC
36 VL_COFINS Valor da COFINS N - 02 OC OC
37 COD_CTA C�digo da conta anal�tica cont�bil debitada/creditada C - - OC OC
*)
var
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  COD_CTA: String;
  FatID, FatNum, Empresa, nItem, GraGruX, CST_ICMS, CFOP, CST_PIS,
  CST_COFINS, SPED_EFD_ID_0200: Integer;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r, VL_COFINS: Double;
  SQLType: TSQLType;
  IndMovOK: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  FatID          := FFatID;
  FatNum         := FFatNum;
  Empresa        := FEmpresa;
  nItem          := FnItem;
  GraGruX        := EdGraGruX.ValueVariant;
  COD_ITEM       := EdCOD_ITEM.Text;
  DESCR_COMPL    := MeDESCR_COMPL.Text;
  QTD            := EdQTD.ValueVariant;
  UNID           := EdUNID.Text;
  VL_ITEM        := EdVL_ITEM.ValueVariant;
  VL_DESC        := EdVL_DESC.ValueVariant;
  IND_MOV        := Trim(RGIND_MOV.Items[RGIND_MOV.ItemIndex][1]);
  CST_ICMS       := EdCST_ICMS.ValueVariant;
  CFOP           := EdCFOP.ValueVariant;
  COD_NAT        := EdCOD_NAT.Text;
  VL_BC_ICMS     := EdVL_BC_ICMS.ValueVariant;
  ALIQ_ICMS      := EdALIQ_ICMS.ValueVariant;
  VL_ICMS        := EdVL_ICMS.ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  ALIQ_ST        := EdALIQ_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST.ValueVariant;
  IND_APUR       := EdIND_APUR.Text;
  CST_IPI        := EdCST_IPI.Text;
  COD_ENQ        := EdCOD_ENQ.Text;
  VL_BC_IPI      := EdVL_BC_IPI.ValueVariant;
  ALIQ_IPI       := EdALIQ_IPI.ValueVariant;
  VL_IPI         := EdVL_IPI.ValueVariant;
  CST_PIS        := EdCST_PIS.ValueVariant;
  VL_BC_PIS      := EdVL_BC_PIS.ValueVariant;
  ALIQ_PIS_p     := EdALIQ_PIS_p.ValueVariant;
  QUANT_BC_PIS   := EdQUANT_BC_PIS.ValueVariant;
  ALIQ_PIS_r     := EdALIQ_PIS_r.ValueVariant;
  VL_PIS         := EdVL_PIS.ValueVariant;
  CST_COFINS     := EdCST_COFINS.ValueVariant;
  VL_BC_COFINS   := EdVL_BC_COFINS.ValueVariant;
  ALIQ_COFINS_p  := EdALIQ_COFINS_p.ValueVariant;
  QUANT_BC_COFINS:= EdQUANT_BC_COFINS.ValueVariant;
  ALIQ_COFINS_r  := EdALIQ_COFINS_r.ValueVariant;
  VL_COFINS      := EdVL_COFINS.ValueVariant;
  COD_CTA        := EdCOD_CTA.Text;
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX,
    '"Informe o reduzido!') then Exit;
  if MyObjects.FIC(Trim(COD_ITEM) = '', EdGraGruX,
    '"COD_ITEM" indefinido para o reduzido selecionado!') then Exit;
   IndMovOK := (IND_MOV = '0') or (IND_MOV = '1');
  if MyObjects.FIC(not IndMovOK, RGIND_MOV,
    '"Informe a Movimenta��o f�sica do ITEM/PRODUTO!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeefd_c170', False, [
  'GraGruX', 'COD_ITEM', 'DESCR_COMPL',
  'QTD', 'UNID', 'VL_ITEM',
  'VL_DESC', 'IND_MOV', 'CST_ICMS',
  'CFOP', 'COD_NAT', 'VL_BC_ICMS',
  'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
  'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
  'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
  'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
  'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
  'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
  'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
  'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  GraGruX, COD_ITEM, DESCR_COMPL,
  QTD, UNID, VL_ITEM,
  VL_DESC, IND_MOV, CST_ICMS,
  CFOP, COD_NAT, VL_BC_ICMS,
  ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, IND_APUR,
  CST_IPI, COD_ENQ, VL_BC_IPI,
  ALIQ_IPI, VL_IPI, CST_PIS,
  VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  ALIQ_PIS_r, VL_PIS, CST_COFINS,
  VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
  ALIQ_COFINS_r, VL_COFINS, COD_CTA], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
    Close;
  end;
end;

procedure TFmNFeEFD_C170.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEFD_C170.CadastrarCombinaoCFOPEmpresaReduzido1Click(
  Sender: TObject);
var
  CFOP_INN, CFOP_OUT, Nome: String;
  Codigo, Empresa, GraGruX: Integer;
  SQLType: TSQLType;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    SQLType        := stIns;
    Codigo         := 0;
    CFOP_INN       := Geral.FormataCFOP(EdCFOP.Text);
    CFOP_OUT       := Geral.FormataCFOP(Geral.FF0(FCFOP_OUT));
    Nome           := EdCFOP_TXT.Text;
    Empresa        := FEmpresa;
    GraGruX        := EdGraGruX.ValueVariant;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM cfopcfop ',
    'WHERE REPLACE(CFOP_INN, ".", "")="' + Geral.SoNumero_TT(CFOP_INN) + '"',
    'AND REPLACE(CFOP_OUT, ".", "")="' + Geral.SoNumero_TT(CFOP_OUT) + '"',
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    if Qry.RecordCount = 0 then
    begin
      Codigo := UMyMod.BPGS1I32('cfopcfop', 'Codigo', '', '', tsPos, SQLType, Codigo);
      if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, SQLType, 'cfopcfop', False, [
      'CFOP_INN', 'CFOP_OUT', 'Nome',
      'Empresa', 'GraGruX'], [
      'Codigo'], [
      CFOP_INN, CFOP_OUT, Nome,
      Empresa, GraGruX], [
      Codigo], True) then
        Geral.MB_Info(
        'Combina��o de CFOP/Empresa/Reduzido cadastrada com sucesso!');
    end else
        Geral.MB_Aviso(
        'A��o abortada! Combina��o CFOP/Empresa/Reduzido j� est� cadastrada!');
  finally
    Qry.Free;
  end;
end;

procedure TFmNFeEFD_C170.EdCFOPRedefinido(Sender: TObject);
begin
  EdCFOP_TXT.Text := SPED_Geral.ObtemTextoDeCodTxtTbSPED(
    CO_NOME_TbSPEDEFD_CFOP, EdCFOP.Text);
end;

procedure TFmNFeEFD_C170.EdCST_COFINSRedefinido(Sender: TObject);
begin
  EdCST_COFINS_TXT.Text := SPED_Geral.ObtemTextoDeCodTxtTbSPED(
    CO_NOME_TbSPEDEFD_COFINS_CST, EdCST_COFINS.Text);
end;

procedure TFmNFeEFD_C170.EdCST_ICMSRedefinido(Sender: TObject);
begin
  EdCST_ICMS_TXT.Text := SPED_Geral.ObtemTextoDeCodTxtTbSPED(
    CO_NOME_TbSPEDEFD_ICMS_CST, EdCST_ICMS.Text);
end;

procedure TFmNFeEFD_C170.EdCST_IPIRedefinido(Sender: TObject);
begin
  EdCST_IPI_TXT.Text := SPED_Geral.ObtemTextoDeCodTxtTbSPED(
    CO_NOME_TbSPEDEFD_IPI_CST, EdCST_IPI.Text);
end;

procedure TFmNFeEFD_C170.EdCST_PISRedefinido(Sender: TObject);
begin
  EdCST_PIS_TXT.Text := SPED_Geral.ObtemTextoDeCodTxtTbSPED(
    CO_NOME_TbSPEDEFD_PIS_CST, EdCST_PIS.Text);
end;

procedure TFmNFeEFD_C170.EdGraGruXRedefinido(Sender: TObject);
var
  GraGruX, SPED_EFD_ID_0200: Integer;
  COD_ITEM, COD_CTA: String;
begin
  GraGruX := EdGraGruX.ValueVariant;
  SPED_EFD_ID_0200 := UnNFe_PF.ObtemSPEDEFD_SPED_EFD_ID_0XXX(200, FEmpresa);
  UnNFe_PF.ObtemSPEDEFD_COD_ITEM(GraGruX, SPED_EFD_ID_0200, COD_ITEM);
  EdCOD_ITEM.Text := COD_ITEM;
  //
  COD_CTA := DmProd.ObtemCTB_peloGGX(GraGruX);
  EdCOD_CTA.Text := COD_CTA;
end;

procedure TFmNFeEFD_C170.EdIND_APURRedefinido(Sender: TObject);
begin
  EdIND_APUR_TXT.Text := sEFD_IND_APUR[Geral.IMV(EdIND_APUR.Text)];
end;

procedure TFmNFeEFD_C170.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEFD_C170.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCFOP_OUT := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, " ",  ',
  '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
  '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM,  ',
  'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR,  ',
  'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI ',
  'FROM      gragrux    ggx  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'WHERE ggx.Controle>0 ',
  'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle ',
  '']);
end;

procedure TFmNFeEFD_C170.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEFD_C170.PMCFOPPopup(Sender: TObject);
begin
  CadastrarCombinaoCFOPEmpresaReduzido1.Enabled := FCFOP_OUT > 0;
end;

procedure TFmNFeEFD_C170.SbCFOPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCFOP, SbCFOP);
end;

(*
SELECT c170.CST_ICMS, CFOP, itsi.prod_CFOP,
itsn.ICMS_Orig, itsn.ICMS_CST,
COUNT(c170.CST_ICMS) Itens,
SUM(c170.VL_ITEM) VL_ITEM,
SUM(c170.VL_ICMS) VL_ICMS
FROM nfeefd_c170 c170
LEFT JOIN nfecaba caba
  ON caba.FatID=c170.FatID
  AND caba.FatNum=c170.FatNum
  AND caba.Empresa=c170.Empresa
LEFT JOIN nfeitsi itsi
  ON itsi.FatID=c170.FatID
  AND itsi.FatNum=c170.FatNum
  AND itsi.Empresa=c170.Empresa
  AND itsi.nItem=c170.nItem
LEFT JOIN nfeitsn itsn
  ON itsn.FatID=c170.FatID
  AND itsn.FatNum=c170.FatNum
  AND itsn.Empresa=c170.Empresa
  AND itsn.nItem=c170.nItem
WHERE caba.DataFiscal
  BETWEEN "2017-01-01" AND "2017-01-31"
GROUP BY CST_ICMS, CFOP, prod_CFOP,
ICMS_Orig, ICMS_CST
ORDER BY CST_ICMS, CFOP, prod_CFOP,
ICMS_Orig, ICMS_CST
*)

end.
