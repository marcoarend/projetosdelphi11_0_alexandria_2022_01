object FmNFeEFD_C170: TFmNFeEFD_C170
  Left = 339
  Top = 185
  Caption = 'NFe-EFD_C-170 :: Item de NF de Terceiros Importado de XML'
  ClientHeight = 680
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 984
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 936
      Top = 0
      Width = 48
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 889
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 493
        Height = 31
        Caption = 'Item de NF de Terceiros Importado de XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 493
        Height = 31
        Caption = 'Item de NF de Terceiros Importado de XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 493
        Height = 31
        Caption = 'Item de NF de Terceiros Importado de XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 984
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 984
      Height = 521
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 82
        Width = 984
        Height = 439
        Align = alBottom
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 100
          Width = 980
          Height = 103
          Align = alTop
          Caption = ' ICMS: '
          TabOrder = 1
          ExplicitTop = 99
          object Panel5: TPanel
            Left = 2
            Top = 14
            Width = 976
            Height = 87
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label6: TLabel
              Left = 8
              Top = 4
              Width = 24
              Height = 13
              Caption = 'CST:'
            end
            object Label7: TLabel
              Left = 8
              Top = 27
              Width = 31
              Height = 13
              Caption = 'CFOP:'
            end
            object Label8: TLabel
              Left = 8
              Top = 47
              Width = 98
              Height = 13
              Caption = 'C'#243'd. Nat. Opera'#231#227'o:'
            end
            object Label9: TLabel
              Left = 110
              Top = 47
              Width = 55
              Height = 13
              Caption = '$ BC ICMS:'
            end
            object Label10: TLabel
              Left = 213
              Top = 47
              Width = 63
              Height = 13
              Caption = '% Aliq. ICMS:'
            end
            object Label11: TLabel
              Left = 280
              Top = 47
              Width = 38
              Height = 13
              Caption = '$ ICMS:'
            end
            object Label12: TLabel
              Left = 382
              Top = 47
              Width = 72
              Height = 13
              Caption = '$ BC ICMS ST:'
            end
            object Label13: TLabel
              Left = 485
              Top = 47
              Width = 80
              Height = 13
              Caption = '% Aliq. ICMS ST:'
            end
            object Label14: TLabel
              Left = 567
              Top = 47
              Width = 55
              Height = 13
              Caption = '$ ICMS ST:'
            end
            object SbCFOP: TSpeedButton
              Left = 949
              Top = 24
              Width = 21
              Height = 20
              Caption = '...'
              OnClick = SbCFOPClick
            end
            object EdCST_ICMS: TdmkEdit
              Left = 39
              Top = 0
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -1
              ValWarn = False
              OnRedefinido = EdCST_ICMSRedefinido
            end
            object EdCFOP: TdmkEdit
              Left = 47
              Top = 24
              Width = 55
              Height = 20
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-0001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -1
              ValWarn = False
              OnRedefinido = EdCFOPRedefinido
            end
            object EdCOD_NAT: TdmkEdit
              Left = 8
              Top = 63
              Width = 98
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVL_BC_ICMS: TdmkEdit
              Left = 110
              Top = 63
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_ICMS: TdmkEdit
              Left = 213
              Top = 63
              Width = 64
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_ICMS: TdmkEdit
              Left = 280
              Top = 63
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_BC_ICMS_ST: TdmkEdit
              Left = 382
              Top = 63
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_ST: TdmkEdit
              Left = 485
              Top = 63
              Width = 79
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_ICMS_ST: TdmkEdit
              Left = 567
              Top = 63
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCST_ICMS_TXT: TdmkEdit
              Left = 78
              Top = 0
              Width = 892
              Height = 21
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCFOP_TXT: TdmkEdit
              Left = 106
              Top = 24
              Width = 840
              Height = 20
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 203
          Width = 980
          Height = 60
          Align = alTop
          Caption = ' IPI: '
          TabOrder = 2
          ExplicitTop = 202
          object Panel6: TPanel
            Left = 2
            Top = 14
            Width = 976
            Height = 44
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label15: TLabel
              Left = 209
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CST:'
            end
            object Label18: TLabel
              Left = 701
              Top = 0
              Width = 42
              Height = 13
              Caption = '$ BC IPI:'
            end
            object Label19: TLabel
              Left = 803
              Top = 0
              Width = 50
              Height = 13
              Caption = '% Aliq. IPI:'
            end
            object Label20: TLabel
              Left = 870
              Top = 0
              Width = 25
              Height = 13
              Caption = '$ IPI:'
            end
            object Label24: TLabel
              Left = 8
              Top = 0
              Width = 196
              Height = 13
              Caption = 'Indicador do per'#237'odo de apura'#231#227'o do IPI:'
            end
            object Label25: TLabel
              Left = 662
              Top = 0
              Width = 30
              Height = 13
              Caption = 'CEL*'#185':'
            end
            object EdCST_IPI: TdmkEdit
              Left = 209
              Top = 16
              Width = 36
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdCST_IPIRedefinido
            end
            object EdVL_BC_IPI: TdmkEdit
              Left = 701
              Top = 16
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_IPI: TdmkEdit
              Left = 803
              Top = 16
              Width = 64
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_IPI: TdmkEdit
              Left = 870
              Top = 16
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdIND_APUR: TdmkEdit
              Left = 8
              Top = 16
              Width = 29
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdIND_APURRedefinido
            end
            object EdIND_APUR_TXT: TdmkEdit
              Left = 39
              Top = 16
              Width = 167
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCOD_ENQ: TdmkEdit
              Left = 662
              Top = 16
              Width = 36
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCST_IPI_TXT: TdmkEdit
              Left = 248
              Top = 16
              Width = 410
              Height = 21
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 383
          Width = 980
          Height = 20
          Align = alTop
          TabOrder = 5
          ExplicitTop = 382
          object Label26: TLabel
            Left = 8
            Top = 4
            Width = 217
            Height = 13
            Caption = 'CEL*'#185': C'#243'digo do enquadramento legal do IPI.'
          end
        end
        object GroupBox4: TGroupBox
          Left = 2
          Top = 263
          Width = 980
          Height = 60
          Align = alTop
          Caption = ' PIS: '
          TabOrder = 3
          ExplicitTop = 262
          object Panel8: TPanel
            Left = 2
            Top = 14
            Width = 976
            Height = 44
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label16: TLabel
              Left = 8
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CST:'
            end
            object Label22: TLabel
              Left = 473
              Top = 0
              Width = 46
              Height = 13
              Caption = '$ BC PIS:'
            end
            object Label23: TLabel
              Left = 575
              Top = 0
              Width = 54
              Height = 13
              Caption = '% Aliq. PIS:'
            end
            object Label27: TLabel
              Left = 650
              Top = 0
              Width = 89
              Height = 13
              Caption = 'Qtd base calc PIS:'
            end
            object Label28: TLabel
              Left = 768
              Top = 0
              Width = 52
              Height = 13
              Caption = '$ Aliq. PIS:'
            end
            object Label30: TLabel
              Left = 870
              Top = 0
              Width = 29
              Height = 13
              Caption = '$ PIS:'
            end
            object EdCST_PIS: TdmkEdit
              Left = 8
              Top = 16
              Width = 36
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-01'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -1
              ValWarn = False
              OnRedefinido = EdCST_PISRedefinido
            end
            object EdVL_BC_PIS: TdmkEdit
              Left = 473
              Top = 16
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_PIS_p: TdmkEdit
              Left = 575
              Top = 16
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdQUANT_BC_PIS: TdmkEdit
              Left = 650
              Top = 16
              Width = 114
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_PIS_r: TdmkEdit
              Left = 768
              Top = 16
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_PIS: TdmkEdit
              Left = 870
              Top = 16
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCST_PIS_TXT: TdmkEdit
              Left = 47
              Top = 16
              Width = 423
              Height = 21
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 980
          Height = 85
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 14
          object Label4: TLabel
            Left = 12
            Top = 4
            Width = 89
            Height = 13
            Caption = 'Produto (reduzido):'
          end
          object Label1: TLabel
            Left = 12
            Top = 43
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label2: TLabel
            Left = 126
            Top = 43
            Width = 43
            Height = 13
            Caption = 'Unidade:'
          end
          object Label3: TLabel
            Left = 209
            Top = 43
            Width = 49
            Height = 13
            Caption = 'Valor item:'
          end
          object Label5: TLabel
            Left = 311
            Top = 43
            Width = 74
            Height = 13
            Caption = 'Valor desconto:'
          end
          object Label34: TLabel
            Left = 414
            Top = 43
            Width = 258
            Height = 13
            Caption = 'C'#243'digo da conta anal'#237'tica cont'#225'bil debitada/creditada:'
          end
          object Label35: TLabel
            Left = 496
            Top = 4
            Width = 73
            Height = 13
            Caption = 'C'#243'digo do item:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 12
            Top = 20
            Width = 55
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'gragrux'
            QryCampo = 'Controle'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdGraGruXRedefinido
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 67
            Top = 20
            Width = 427
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 1
            dmkEditCB = EdGraGruX
            UpdType = utYes
            LocF7CodiFldName = 'Controle'
            LocF7NameFldName = 'NO_PRD_TAM_COR'
            LocF7TableName = 'gragrux'
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdQTD: TdmkEdit
            Left = 12
            Top = 59
            Width = 110
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 5
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdUNID: TdmkEdit
            Left = 126
            Top = 59
            Width = 79
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdVL_ITEM: TdmkEdit
            Left = 209
            Top = 59
            Width = 98
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdVL_DESC: TdmkEdit
            Left = 311
            Top = 59
            Width = 99
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGIND_MOV: TdmkRadioGroup
            Left = 752
            Top = 4
            Width = 226
            Height = 76
            Caption = 'Movimenta'#231#227'o f'#237'sica do ITEM/PRODUTO:'
            Columns = 3
            ItemIndex = 2
            Items.Strings = (
              '0. SIM'
              '1. N'#195'O'
              ' . ???')
            TabOrder = 8
            UpdType = utYes
            OldValor = 0
          end
          object EdCOD_CTA: TdmkEdit
            Left = 414
            Top = 59
            Width = 332
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCOD_ITEM: TdmkEdit
            Left = 496
            Top = 20
            Width = 250
            Height = 20
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox5: TGroupBox
          Left = 2
          Top = 323
          Width = 980
          Height = 60
          Align = alTop
          Caption = ' COFINS: '
          TabOrder = 4
          ExplicitTop = 322
          object Panel10: TPanel
            Left = 2
            Top = 14
            Width = 976
            Height = 44
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label17: TLabel
              Left = 8
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CST:'
            end
            object Label21: TLabel
              Left = 473
              Top = 0
              Width = 68
              Height = 13
              Caption = '$ BC COFINS:'
            end
            object Label29: TLabel
              Left = 575
              Top = 0
              Width = 71
              Height = 13
              Caption = '% Alq COFINS:'
            end
            object Label31: TLabel
              Left = 650
              Top = 0
              Width = 111
              Height = 13
              Caption = 'Qtd base calc COFINS:'
            end
            object Label32: TLabel
              Left = 768
              Top = 0
              Width = 74
              Height = 13
              Caption = '$ Aliq. COFINS:'
            end
            object Label33: TLabel
              Left = 870
              Top = 0
              Width = 51
              Height = 13
              Caption = '$ COFINS:'
            end
            object EdCST_COFINS: TdmkEdit
              Left = 8
              Top = 16
              Width = 36
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-01'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -1
              ValWarn = False
              OnRedefinido = EdCST_COFINSRedefinido
            end
            object EdVL_BC_COFINS: TdmkEdit
              Left = 473
              Top = 16
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_COFINS_p: TdmkEdit
              Left = 575
              Top = 16
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdQUANT_BC_COFINS: TdmkEdit
              Left = 650
              Top = 16
              Width = 114
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_COFINS_r: TdmkEdit
              Left = 768
              Top = 16
              Width = 98
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdVL_COFINS: TdmkEdit
              Left = 870
              Top = 16
              Width = 99
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCST_COFINS_TXT: TdmkEdit
              Left = 47
              Top = 16
              Width = 423
              Height = 21
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object MeDESCR_COMPL: TdmkMemo
          Left = 2
          Top = 403
          Width = 980
          Height = 34
          Align = alClient
          ReadOnly = True
          TabOrder = 6
          WantReturns = False
          UpdType = utYes
          ExplicitTop = 402
          ExplicitHeight = 35
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 984
        Height = 80
        Align = alTop
        TabOrder = 1
        object Label36: TLabel
          Left = 654
          Top = 8
          Width = 12
          Height = 13
          Caption = 'Id:'
          FocusControl = DBEdit1
        end
        object Label37: TLabel
          Left = 8
          Top = 8
          Width = 41
          Height = 13
          Caption = 'Emitente'
          FocusControl = DBEdit2
        end
        object Label38: TLabel
          Left = 8
          Top = 27
          Width = 34
          Height = 13
          Caption = 'NatOp:'
          FocusControl = DBEdit5
        end
        object Label39: TLabel
          Left = 8
          Top = 55
          Width = 127
          Height = 13
          Caption = 'Modelo / s'#233'rie / n'#186' da NFe'
          FocusControl = DBEdit6
        end
        object Label40: TLabel
          Left = 260
          Top = 55
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
          FocusControl = DBEdit9
        end
        object Label41: TLabel
          Left = 441
          Top = 55
          Width = 50
          Height = 13
          Caption = 'Munic'#237'pio:'
          FocusControl = DBEdit10
        end
        object Label42: TLabel
          Left = 926
          Top = 55
          Width = 17
          Height = 13
          Caption = 'UF:'
          FocusControl = DBEdit11
        end
        object DBEdit1: TDBEdit
          Left = 674
          Top = 4
          Width = 303
          Height = 21
          DataField = 'Id'
          DataSource = DsNFeCabA
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 205
          Top = 4
          Width = 442
          Height = 21
          DataField = 'emit_xNome'
          DataSource = DsNFeCabA
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 59
          Top = 4
          Width = 36
          Height = 21
          DataField = 'KIND_DOC'
          DataSource = DsNFeCabA
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 94
          Top = 4
          Width = 111
          Height = 21
          DataField = 'CNPJ_CPF'
          DataSource = DsNFeCabA
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 59
          Top = 27
          Width = 919
          Height = 21
          DataField = 'ide_natOp'
          DataSource = DsNFeCabA
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 138
          Top = 51
          Width = 28
          Height = 21
          DataField = 'ide_mod'
          DataSource = DsNFeCabA
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 193
          Top = 51
          Width = 61
          Height = 21
          DataField = 'ide_nNF'
          DataSource = DsNFeCabA
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 166
          Top = 51
          Width = 28
          Height = 21
          DataField = 'ide_serie'
          DataSource = DsNFeCabA
          TabOrder = 7
        end
        object DBEdit9: TDBEdit
          Left = 307
          Top = 51
          Width = 132
          Height = 21
          DataField = 'ide_dEmi'
          DataSource = DsNFeCabA
          TabOrder = 8
        end
        object DBEdit10: TDBEdit
          Left = 496
          Top = 51
          Width = 422
          Height = 21
          DataField = 'emit_xMun'
          DataSource = DsNFeCabA
          TabOrder = 9
        end
        object DBEdit11: TDBEdit
          Left = 946
          Top = 51
          Width = 28
          Height = 21
          DataField = 'emit_UF'
          DataSource = DsNFeCabA
          TabOrder = 10
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 568
    Width = 984
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 980
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 611
    Width = 984
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 840
      Top = 14
      Width = 142
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 838
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 620
    Top = 7
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, " ", '
      '  IF(gti.PrintTam=1, gti.Nome, ""), " ", '
      '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR,'
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM, '
      'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR, '
      'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI'
      'FROM      gragrux    ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle>0'
      'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle'
      '')
    Left = 188
    Top = 396
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 87
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 188
    Top = 448
  end
  object QrTbSPEDEFD30: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM tbspedefd130'
      'ORDER BY Nome')
    Left = 780
    Top = 316
    object QrTbSPEDEFD30CodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrTbSPEDEFD30Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTbSPEDEFD30Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id, ide_natOp,  ide_mod, ide_nNF, ide_dEmi,  '
      'IF(emit_CNPJ <> "", "CNPJ", "CPF") KIND_DOC, '
      'IF(emit_CNPJ <> "", emit_CNPJ, emit_CPF) CNPJ_CPF, '
      'emit_xNome, emit_xMun, emit_UF '
      'FROM nfecaba '
      'WHERE FatID=51 '
      'AND FatNum>0 '
      'AND Empresa=-11 '
      ' ')
    Left = 100
    Top = 396
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeCabAKIND_DOC: TWideStringField
      FieldName = 'KIND_DOC'
      Required = True
      Size = 4
    end
    object QrNFeCabACNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrNFeCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrNFeCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrNFeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNFeCabAide_serie: TSmallintField
      FieldName = 'ide_serie'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 100
    Top = 444
  end
  object PMCFOP: TPopupMenu
    OnPopup = PMCFOPPopup
    Left = 892
    Top = 296
    object CadastrarCombinaoCFOPEmpresaReduzido1: TMenuItem
      Caption = 'Cadastrar Combina'#231#227'o CFOP/Empresa/Reduzido'
      OnClick = CadastrarCombinaoCFOPEmpresaReduzido1Click
    end
  end
end
