unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral,
  ABSMain;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrLivreY_D: TmySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSelX: TMySQLQuery;
    QrSelXLk: TIntegerField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrSomaM: TMySQLQuery;
    QrSomaMValor: TFloatField;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrLocDataY: TmySQLQuery;
    QrLocDataYRecord: TDateField;
    QrLivreY_DCodigo: TLargeintField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrMasterLogo2: TBlobField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ChDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrControleTravaCidade: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TIntegerField;
    QrControleEntraSemValor: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrControleMyPagTip: TIntegerField;
    QrControleMyPagCar: TIntegerField;
    QrControleDono: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrEnderecoRespons1: TWideStringField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoNF: TWideStringField;
    QrMasterENumero: TFloatField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrLanctos: TmySQLQuery;
    QrLanctosData: TDateField;
    QrLanctosTipo: TSmallintField;
    QrLanctosCarteira: TIntegerField;
    QrLanctosAutorizacao: TIntegerField;
    QrLanctosGenero: TIntegerField;
    QrLanctosDescricao: TWideStringField;
    QrLanctosNotaFiscal: TIntegerField;
    QrLanctosDebito: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosCompensado: TDateField;
    QrLanctosDocumento: TFloatField;
    QrLanctosSit: TIntegerField;
    QrLanctosVencimento: TDateField;
    QrLanctosLk: TIntegerField;
    QrLanctosFatID: TIntegerField;
    QrLanctosFatNum: TIntegerField;
    QrLanctosFatParcela: TIntegerField;
    QrLanctosCONTA: TIntegerField;
    QrLanctosNOMECONTA: TWideStringField;
    QrLanctosNOMEEMPRESA: TWideStringField;
    QrLanctosNOMESUBGRUPO: TWideStringField;
    QrLanctosNOMEGRUPO: TWideStringField;
    QrLanctosNOMECONJUNTO: TWideStringField;
    QrLanctosNOMESIT: TWideStringField;
    QrLanctosAno: TFloatField;
    QrLanctosMENSAL: TWideStringField;
    QrLanctosMENSAL2: TWideStringField;
    QrLanctosBanco: TIntegerField;
    QrLanctosLocal: TIntegerField;
    QrLanctosFatura: TWideStringField;
    QrLanctosSub: TSmallintField;
    QrLanctosCartao: TIntegerField;
    QrLanctosLinha: TIntegerField;
    QrLanctosPago: TFloatField;
    QrLanctosSALDO: TFloatField;
    QrLanctosID_Sub: TSmallintField;
    QrLanctosMez: TIntegerField;
    QrLanctosFornecedor: TIntegerField;
    QrLanctoscliente: TIntegerField;
    QrLanctosMoraDia: TFloatField;
    QrLanctosNOMECLIENTE: TWideStringField;
    QrLanctosNOMEFORNECEDOR: TWideStringField;
    QrLanctosTIPOEM: TWideStringField;
    QrLanctosNOMERELACIONADO: TWideStringField;
    QrLanctosOperCount: TIntegerField;
    QrLanctosLancto: TIntegerField;
    QrLanctosMulta: TFloatField;
    QrLanctosATRASO: TFloatField;
    QrLanctosJUROS: TFloatField;
    QrLanctosDataDoc: TDateField;
    QrLanctosNivel: TIntegerField;
    QrLanctosVendedor: TIntegerField;
    QrLanctosAccount: TIntegerField;
    QrLanctosMes2: TLargeintField;
    QrLanctosProtesto: TDateField;
    QrLanctosDataCad: TDateField;
    QrLanctosDataAlt: TDateField;
    QrLanctosUserCad: TSmallintField;
    QrLanctosUserAlt: TSmallintField;
    QrLanctosControle: TIntegerField;
    QrLanctosID_Pgto: TIntegerField;
    QrLanctosCtrlIni: TIntegerField;
    QrLanctosFatID_Sub: TIntegerField;
    QrLanctosICMS_P: TFloatField;
    QrLanctosICMS_V: TFloatField;
    QrLanctosDuplicata: TWideStringField;
    QrLanctosCOMPENSADO_TXT: TWideStringField;
    QrLanctosCliInt: TIntegerField;
    QrLanctosDepto: TIntegerField;
    QrLanctosDescoPor: TIntegerField;
    QrLanctosForneceI: TIntegerField;
    QrLanctosQtde: TFloatField;
    DsLanctos: TDataSource;
    QrEstqR: TmySQLQuery;
    QrEstqRQtd: TFloatField;
    QrEstqRVal: TFloatField;
    QrEstqP: TmySQLQuery;
    QrEstqPQtd: TFloatField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleFPPontoHor: TIntegerField;
    QrControleFPHrExt: TIntegerField;
    QrControleVerSalTabs: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleFPFunciFer: TIntegerField;
    QrControleFPFunciHor: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleFPFunciEve: TIntegerField;
    QrControleFPRacas: TIntegerField;
    QrControleFPCivil: TIntegerField;
    QrControleFPNacio: TIntegerField;
    QrControleFPFolhaCal: TIntegerField;
    QrControleFPCateg: TIntegerField;
    QrControleFPEvent: TIntegerField;
    QrControleFPVaria: TIntegerField;
    QrControleFPVincu: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TSmallintField;
    QrControleModeloNF: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleEntiGrupos: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleLastBco: TIntegerField;
    QrUpdW: TmySQLQuery;
    frxDsDono: TfrxDBDataset;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoLograd: TLargeintField;
    QrDonoTipo: TSmallintField;
    QrDonoCEP: TLargeintField;
    QrDonoTE1: TWideStringField;
    QrDonoTE2: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoTE2_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrDonoFONES: TWideStringField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    frxDsMaster: TfrxDBDataset;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrSenhasIP_Default: TWideStringField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FProdDelete: Integer;
    FStrFmtPrc: String;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    function ObtemIP(Retorno: Integer): String;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    function BuscaProximoMovix: Integer;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TABSQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, UnLock_MLA, Servidor, VerifiDB, MyDBCheck,
  InstallMySQL41, MyListas, ModuleGeral, NFe_Dmk, Entidades;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  //ODBC: String;
  DataBase, IP: String;
begin
  if MyDB.Connected then
    Application.MessageBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  {
  if MyDBCashier.Connected then
    Application.MessageBox('MyDBCashier est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  }
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  //
  VAR_BDSENHA := 'wkljweryhvbirt';
  if not GOTOy.OMySQLEstaInstalado(
  FmNFe_Dmk.StaticText1, FmNFe_Dmk.ProgressBar1) then
  begin
    raise EAbort.Create('');
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////
  {
  MyDBCashier.UserPassword := VAR_BDSENHA;
  MyDB.UserPassword := VAR_BDSENHA;
  MyLocDataBase.UserPassword := VAR_BDSENHA;
  //
  MyDBCashier.Connected := False;
  MyDBCashier.DataBaseName := '';
  }
  //
  MyDB.Connected := False;
  MyDB.DataBaseName := '';
  //
  MyLocDataBase.Connected := False;
  MyLocDataBase.DataBaseName := '';
  //
  //DBMLocal.AliasName := '';
  //DBMMoney.AliasName := '';
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
        ShowMessage('M�quina cliente sem rede. O aplicativo ser� finalizado.');
        Application.Terminate;
      end;
    end;
  except
    ShowMessage('Teste');
    Application.Terminate;
    Exit;
  end;
  //
  try
    Database := Geral.ReadAppKey('Cashier_Database', 'Dermatek', ktString,
    '', HKEY_LOCAL_MACHINE);
    {
    if Database = 'BlueSkin'  then ivTabPerfis := 'WPerfis' else ivTabPerfis := 'Perfis';
    }
    //FmNFe_Dmk.Memo.Lines.Add('Database: '+Database);
    if Database = '' then //ShowMessage('Database n�o defenido');
    begin
      //FmNFe_Dmk.Memo.Lines.Add('Database do Cashier n�o foi definido');
      if InputQuery('Database para conex�o','Informe o database',Database) then
        Geral.WriteAppKey('Cashier_Database', 'Dermatek', Database, ktString,
        HKEY_LOCAL_MACHINE)
      else begin
        //FmNFe_Dmk.Memo.Lines.Add('Conex�o abortada pelo usu�rio!');
        Application.MessageBox('Conex�o abortada pelo usu�rio!', 'Aviso',
        MB_OK+MB_ICONEXCLAMATION);
        Application.Terminate;
        Exit;
      end;
    end;
    //FmNFe_Dmk.Memo.Lines.Add('Databse: '+Database);
    if Database = '' then //ShowMessage('Database n�o defenido');
    begin
      Application.MessageBox('Database n�o definido ou inexistente! O aplicativo ser� encerrado',
      'Erro', MB_OK+MB_ICONERROR);
      Application.Terminate;
      Exit;
    end;
    TMeuDB := DataBase;
    //FmNFe_Dmk.Memo.Lines.Add('DBMMoney.AliasName = '+VAR_BDCASHIER);
  except
    ShowMessage('Imposs�vel conectar database - Verifique a configura��o da ODBC.');
    Application.Terminate;
    Exit;
  end;
  //
  try
    Database := Geral.ReadAppKey('CashLoc_Database', 'Dermatek', ktString,
    '', HKEY_LOCAL_MACHINE);
    //FmNFe_Dmk.Memo.Lines.Add('Database Local = '+Database);
    if Database = '' then
    begin
      if InputQuery('Database para conex�o','Informe o database',Database) then
        Geral.WriteAppKey('CashLoc_Database', 'Dermatek', Database, ktString,
        HKEY_LOCAL_MACHINE)
      else begin
        Application.MessageBox('Conex�o abortada pelo usu�rio!', 'Aviso',
        MB_OK+MB_ICONEXCLAMATION);
        Application.Terminate;
        Exit;
      end;
    end;
    //ODBC := 'ODBC\ODBC.INI\'+Database;
    //Database := Geral.ReadAppKey('Database', ODBC, ktString, '', HKEY_CURRENT_USER);
    //if Database = '' then
    //FmNFe_Dmk.Memo.Lines.Add('Database local n�o definido.');
    TLocDB := DataBase;
    //FmNFe_Dmk.Memo.Lines.Add('DBMMoney.AliasName = '+VAR_BDCASHLOC);
(*    DBMLocal.AliasName := Database;
    try
      DBMLocal.Connected := True;
      FmNFe_Dmk.Memo.Lines.Add('Database local conectado com sucesso.');
    except
      FmNFe_Dmk.Memo.Lines.Add('N�o foi poss�vel conectar database local.');
    end;*)
  except
    ShowMessage('Imposs�vel conectar database local - Verifique a configura��o da ODBC.');
    Application.Terminate;
    Exit;
  end;
  //
(*  try
    DBMLocal.Connected := False;
    DBMLocal.AliasName := VAR_BDCASHLOC;
    DBMLocal.Connected := True;
    FmNFe_Dmk.Memo.Lines.Add('DBMLocal conectado com sucesso.');
  except
    ShowMessage('O alias "'+VAR_BDCASHLOC+'" n�o existe, ou n�o est� cadastrado no ODBC.');
    Application.Terminate;
    Exit;
  end;
  try
    DBMMoney.Connected := False;
    DBMMoney.AliasName := VAR_BDCASHIER;
    DBMMoney.Connected := True;
    FmNFe_Dmk.Memo.Lines.Add('DBMMoney conectado com sucesso.');
  except
    ShowMessage('O alias "'+VAR_BDCASHIER+'" n�o existe, ou n�o est� cadastrado no ODBC.');
    Application.Terminate;
    Exit;
  end;*)

  //AtzRecovery.AtualizaVersao;

  Geral.DefineFormatacoes;
  (*VAR_FORMATDATE  := 'yyyy/mm/dd';
  VAR_FORMATDATE2 := 'dd/mm/yyyy';
  VAR_FORMATDATE3 := 'dd/mm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE4 := 'yyyymmdd';
  VAR_FORMATTIMESTAMP4 := 'mm/yy';
  VAR_FORMATTIME := 'hh:nn:ss';
  VAR_FORMATTIME2 := 'hh:nn';*)
  IP := Geral.ReadAppKey('IPServer', Application.Title, ktString,
    CO_VAZIO, HKEY_LOCAL_MACHINE);
  if IP = CO_VAZIO then
  begin
    IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  VAR_IP := IP;
  {
  MyDBCashier.DataBaseName := TMeuDB;
  MyDBCashier.Host := VAR_IP;
  MyDBCashier.Connected := True;
  }
  MyDB.DataBaseName := TMeuDB;
  MyDB.Host := VAR_IP;
  MyLocDataBase.DataBaseName := TLocDB;
  MyDB.Connected := True;
  //FmNFe_Dmk.DBLocal.DatabaseName := VAR_BDCASHLOC;
  UMyMod.AbreQuery(QrMaster, 'TDmod.DataModuleCreate()');
  VAR_EMPRESANOME := QrMasterEm.Value;
  if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  {
  CriaQv_GOTO;
  }
  DefParams();
  GOTOy.VerificaTerminal;
  if not VAR_APPTERMINATE then
  begin
    Application.CreateForm(TFmEntidades, FmEntidades);
    FmEntidades.Destroy;
  end;
  //
  {
  FmPrincipal.FDModCriado := True;
  FmPrincipal.IniciaAplicativo;
  }
  //
  MLAGeral.ConfiguracoesIniciais(1, MyDB.DatabaseName);


  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados Geral'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

{
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica: Boolean;
begin
  if MyDB.Connected then
    Application.MessageBox('MyDB est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  VAR_BDSENHA := 'wkljweryhvbirt';
  if not GOTOy.OMySQLEstaInstalado(
  FmNFe_Dmk.StaticText1, FmNFe_Dmk.ProgressBar1) then
  begin
    raise EAbort.Create('');
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ChDB.UserPassword := VAR_BDSENHA;
  try
    ChDB.Connected := True;
  except
    ChDB.UserPassword := '852456';
    try
      ChDB.Connected := True;
      QrUpd.Database := ChDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Application.MessageBox(PChar('M�quina cliente sem rede.'), 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.MessageBox(PChar('Teste'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'), 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'),
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Application.MessageBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  MLAGeral.ConfiguracoesIniciais(1, MyDB.DatabaseName);

  //

end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
}
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
var
  s: String;
begin
  s := Uppercase(Senha);
  if (s = VAR_BOSS) or (s = CO_MASTER)
  or ((s = VAR_ADMIN) and (VAR_ADMIN <> ''))
  then VAR_SENHARESULT := 2 else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  QrControle.Close;
  QrControle.Open;
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDmod.ObtemIP(Retorno: Integer): String;
var
  p: PHostEnt;
  s: array[0..128] of char;
  p2: PChar;
begin
  GetHostName(@s, 128);
  p := GetHostByName(@s);
  p2 := iNet_ntoa(PInAddr(p^.h_addr_list^)^);
  case Retorno of
    0: Result := p^.h_Name;
    1: Result := p2;
    else Result := '';
  end;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then Natal := QrEnderecoENatal.Value
  else Natal := QrEnderecoPNatal.Value;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.DefParams;
begin
  {
  FmPrincipal.DefParams;
  }
end;

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
  {
  FmPrincipal.LocCod(Atual, Codigo);
  }
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FStrFmtPrc := MLAGeral.StrFmt_Double(Dmod.QrControleCasasProd.Value);
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(Trunc(QrDonoNumero.Value + 0.1), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TABSQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Application.MessageBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

end.

