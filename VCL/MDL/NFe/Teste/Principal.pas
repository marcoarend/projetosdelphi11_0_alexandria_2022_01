unit Principal;

{DEFINE FmImprime}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     UnMLAGeral, UMySQLModule,
    LMDButtonBar, ExtCtrls, Menus,
  Grids, DBGrids, Db, (*DBTables,*) TypInfo, StdCtrls, LMDCustomComponent, ZCF2,
  LMDSysInfo, ToolWin, ComCtrls,   UnInternalConsts,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, 
  LMDGraphicControl, LMDBaseImage, LMDCustomNImage, LMDNImage, LMDGraph, Mask,
  DBCtrls, Tabs, DockTabSet, AdvToolBar, AdvGlowButton, AdvMenus, dmkGeral,
  AdvToolBarStylers, frxpngimage, AdvShapeButton, AdvPreviewMenu, AdvOfficeHint,
  dmkDBGrid, dmkEdit,
  CAPICOM_TLB, MSXML2_TLB, JwaWinCrypt, wininet;

type
    Base = class
      x : Integer;
    end;

  TcpCalc = (cpJurosMes, cpMulta);
  TFmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Sair1: TMenuItem;
    LMDSysInfo1: TLMDSysInfo;
    Logoffde1: TMenuItem;
    Cadastros1: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    Ferramentas1: TMenuItem;
    Imagemdefundo1: TMenuItem;
    Entidades1: TMenuItem;
    BD1: TMenuItem;
    Verifica1: TMenuItem;
    VCLSkin1: TMenuItem;
    Escolher1: TMenuItem;
    Timer1: TTimer;
    Dump1: TMenuItem;
    Backup1: TMenuItem;
    Diversos1: TMenuItem;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    Padro1: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Seleciona1: TMenuItem;
    Limpa1: TMenuItem;
    N2: TMenuItem;
    Estilo1: TMenuItem;
    Automtico1: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    Opes2: TMenuItem;
    PMPlaCtas: TPopupMenu;
    Saldoinicialdeconta1: TMenuItem;
    Listagem2: TMenuItem;
    Simples2: TMenuItem;
    Transfernciaentrecontas1: TMenuItem;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasNOMEBANCO: TWideStringField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    DsCarteiras: TDataSource;
    QrCartSum: TmySQLQuery;
    QrCartSumSALDO: TFloatField;
    QrCartSumFuturoC: TFloatField;
    QrCartSumFuturoD: TFloatField;
    QrCartSumFuturoS: TFloatField;
    QrCartSumEmCaixa: TFloatField;
    QrCartSumDifere: TFloatField;
    QrCartSumSDO_FUT: TFloatField;
    DsCartSum: TDataSource;
    Verificanovaverso1: TMenuItem;
    N8: TMenuItem;
    TimerIdle: TTimer;
    OpenDialog1: TOpenDialog;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager11: TAdvPage;
    AdvToolBarPager12: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvPMSkin: TAdvPopupMenu;
    Escolher2: TMenuItem;
    Padro2: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Estilo2: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvToolBarButton2: TAdvToolBarButton;
    AdvToolBarButton3: TAdvToolBarButton;
    AdvToolBarButton4: TAdvToolBarButton;
    AdvPreviewMenu1: TAdvPreviewMenu;
    Prprio1: TMenuItem;
    Web1: TMenuItem;
    AdvPMVerifiDB: TAdvPopupMenu;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AGMBSkin: TAdvGlowMenuButton;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AGMBVerifiBD: TAdvGlowMenuButton;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    AdvOfficeHint1: TAdvOfficeHint;
    AdvGlowButton58: TAdvGlowButton;
    Matriz1: TMenuItem;
    Timer2: TTimer;
    Memo3: TMemo;
    TrayIcon1: TTrayIcon;
    PMTrayIcon: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    ImgPrincipal: TLMDNImage;
    AGMBBackup: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton80: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton7: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton10: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    procedure Sair1Click(Sender: TObject);
    procedure Logoffde1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Entidades1Click(Sender: TObject);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Escolher1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Diversos1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Padro1Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Seleciona1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Automtico1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure Padro2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvToolBarButton4Click(Sender: TObject);
    procedure AdvToolBarButton2Click(Sender: TObject);
    procedure Prprio1Click(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure AGMBVerifiBDpClick(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure Matriz1Click(Sender: TObject);
    procedure PMTrayIconPopup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AdvGlowButton80Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    //
    procedure MostraBackup3;
    procedure MostraVerifiDB;
    procedure MostraOpcoes;
    procedure MostraLogoff;
    procedure MostraMatriz();
    procedure SkinMenu(Index: integer);
    //
  public
    { Public declarations }
    FDuplicata, FCheque, FVerArqSCX, FEntInt, FTipoNovoEnti, FCliInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    FImportPath, FCript: String;

    procedure AcoesIniciaisDoAplicativo;
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;

    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
    procedure CadastroDeEntidades(Cliente: Integer);

    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure AjustaEstiloImagem(Index: integer);
    procedure MostraPediPrzCab();
    function MostraUnidMed: Boolean;
    procedure MostraFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda: Integer);

    // Cashier
    {
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function VerificaNovasVersoes(): Integer;
    procedure CadastroDeContasNiv();
    procedure ReopenBackDir(Codigo: Integer);
    procedure CadastroIGPM();
    }
    // Compatibilidade
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
             Associada, RegrFiscal, GraGruX: Integer; NO_tablaPrc: String;
             StqCenCad, FatSemEstq, AFP_Sit: Integer; AFP_Per, Qtde: Double;
             Cli_Tipo: Integer; Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL,
             ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer; Item_IPI_ALq: Double;
             Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
             Preco_MedidaA, Preco_MedidaE: Double; Preco_MedOrdem: Integer;
             SQLType: TSQLType; PediVda, OriPart, InfAdCuztm,
             TipoNF, modNF, Serie, nNF, SitDevolu, Servico: Integer;
             refNFe: String; TotalPreCalc: Double; var OriCtrl: Integer;
             Pecas, AreaM2, AreaP2, Peso: Double): Boolean;

    // Financeiros
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure CadastroDeContasSdoLista();
    procedure CadastroDeCarteira;
    procedure CadastroDeContas;
    procedure CadastroDeSubGrupos;
    procedure CadastroDeGrupos;
    procedure CadastroDeConjuntos();
    procedure CadastroDePlano();
    procedure CadastroDeNiveisPlano();
    procedure CadastroDeContasNiv();
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure SaldoDeContas;
    procedure AtzSdoContas;
    procedure ImpressaoDoPlanoDeContas;
    procedure CadastroCFOP2003();

  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses MyListas, Module, MyDBCheck, NFeSteps2, Entidade2, MyGlyfs, Matriz,
MyVCLSkin, NFe_Dmk, VerifiDB, Opcoes, BackUp3, ModuleGeral, CapicomListas,
  ParamsEmp, ModuleNFe2, OpcoesNFe, XML_Testes, XML_Testes2, XML_Testes3,
  NFeInut2, NFeGeraXML, CalcPercent, PediPrzCab2, PediPrzCab1, UnidMed,
  FatPedNFs2, NFeCabA2, NFeLEnC2, NFeValidaXML, Megasena, NFe_Pesq2;

const
  FAltLin = CO_POLEGADA / 2.16;
  XML_Str =
    '<?xml version="1.0" encoding="UTF-8" ?>' +
    '<inutNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07">' +
    '  <infInut Id="ID410902595943000130559000000001000000001">' +
    '    <tpAmb>1</tpAmb>' +
    '    <xServ>INUTILIZAR</xServ>' +
    '    <cUF>41</cUF>' +
    '    <ano>09</ano>' +
    '    <CNPJ>02595943000130</CNPJ>' +
    '    <mod>55</mod>' +
    '    <serie>9</serie>' +
    '    <nNFIni>1</nNFIni>' +
    '    <nNFFin>1</nNFFin>' +
    '    <xJust>0000000002 - INUTILIZADA PARA TESTE DE HOMOLOGACAO</xJust>' +
    '  </infInut>' +
{
    '  <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">' +
    '    <SignedInfo>' +
    '      <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />' +
    '      <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />' +
    '      <Reference URI="#ID2109060176240001065599000000000000000001">' +
    '        <Transforms>' +
    '          <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />' +
    '          <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />' +
    '        </Transforms>' +
    '        <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />' +
    '        <DigestValue></DigestValue>' +
    '      </Reference>' +
    '    </SignedInfo>' +
    '      <SignatureValue></SignatureValue>' +
    '    <KeyInfo>' +
    '      <X509Data>' +
    '        <X509Certificate></X509Certificate>' +
    '      </X509Data>' +
    '    </KeyInfo>' +
    '  </Signature>' +
}
    '</inutNFe>';
{$R *.DFM}

procedure TFmPrincipal.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.Logoffde1Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  TrayIcon1.Visible := False;
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem(*, Avisa*): String;
begin
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_DOTIMP := 250/90;
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then
  begin
    ImgPrincipal.Bitmap.LoadFromFile(Imagem);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MLAGeral.CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  AjustaEstiloImagem(-1);
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MLAGeral.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  TrayIcon1.Visible := True;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Bitmap.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    (*DmDados.TbMaster.Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
      (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
       (Time < DmDados.TbMasterLH.Value)  then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy;*)
  TrayIcon1.Visible := False;
  Application.Terminate;
end;

procedure TFmPrincipal.Entidades1Click(Sender: TObject);
begin
  CadastroDeEntidades(0);
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM Ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

procedure TFmPrincipal.Prprio1Click(Sender: TObject);
begin
  MostraVerifiDB;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLEnC2, FmNFeLEnC2, afmoNegarComAviso) then
  begin
    FmNFeLEnC2.ShowModal;
    FmNFeLEnC2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeValidaXML, FmNFeValidaXML, afmoNegarComAviso) then
  begin
    FmNFeValidaXML.ShowModal;
    FmNFeValidaXML.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMegaSena, FmMegaSena, AFMOPROVISORIO) then
  begin
    FmMegaSena.ShowModal;
    FmMegaSena.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  Geral.MensagemBox('Teste Linha1' + #13#10 + 'Teste linha Comprida! Teste linha Comprida! Teste linha Comprida! ',
  'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFe_Pesq2, FmNFe_Pesq2, afmoNegarComAviso) then
  begin
    FmNFe_Pesq2.ShowModal;
    FmNFe_Pesq2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  CadastroDeEntidades(0);
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  if VAR_LIB_EMPRESA_SEL = 0 then
  begin
    Geral.MensagemBox('A��o cancelada! Existe mais de uma empresa logada!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DmNFe2.ReopenOpcoesNFe(True);
  if DBCheck.CriaFm(TFmNFeSteps2, FmNFeSteps2, afmoSoMaster) then
  begin
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
    // Serial number do certificado digital     //0863F49605983A88E35F6B0EE5ED7366 O Casulo Feliz
    //FmNFeSteps2.EdchNFe.Text                := '41090902595943000130550000000000476518091056';
    FmNFeSteps2.EdEmpresa.ValueVariant      := VAR_LIB_EMPRESA_SEL;
    FmNFeSteps2.EdEmitCNPJ.Text             := Geral.SoNumero_TT(DModG.QrParamsEmpCNPJ_CPF.Value);
    FmNFeSteps2.EdchNFe.ReadOnly            := False;
    FmNFeSteps2.PnInutiliza.Enabled         := True;
    //
    FmNFeSteps2.EdSerialNumber.ValueVariant := DModG.QrParamsEmpNFeSerNum.Value;
    FmNFeSteps2.CBUF.Text                   := DModG.QrParamsEmpUF_WebServ.Value;
    FmNFeSteps2.EdUF_Servico.Text           := DModG.QrParamsEmpUF_Servico.Value;
    FmNFeSteps2.RGAmbiente.ItemIndex        := DmNFe2.QrOpcoesNFeide_tpAmb.Value;
    FmNFeSteps2.PnLoteEnv.Visible           := True;
    FmNFeSteps2.RGAcao.Enabled              := True;
    FmNFeSteps2.PnAbrirXML.Visible          := True;
    FmNFeSteps2.PnAbrirXML.Enabled          := True;
    FmNFeSteps2.CkSoLer.Enabled             := True;
    FmNFeSteps2.PnConfirma.Visible          := True;
    FmNFeSteps2.ShowModal;
    FmNFeSteps2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoAdmin) then
  begin
    FmCapicomListas.ShowModal;
    FmCapicomListas.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoAdmin) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmXML_Testes, FmXML_Testes, afmoSoMaster) then
  begin
    //FmXML_Testes.PnLoteEnv.Visible := True;
    FmXML_Testes.ShowModal;
    FmXML_Testes.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmXML_Testes2, FmXML_Testes2, afmoSoMaster) then
  begin
    //FmXML_Testes.PnLoteEnv.Visible := True;
    FmXML_Testes2.ShowModal;
    FmXML_Testes2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmXML_Testes3, FmXML_Testes3, afmoSoMaster) then
  begin
    //FmXML_Testes.PnLoteEnv.Visible := True;
    FmXML_Testes3.ShowModal;
    FmXML_Testes3.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton80Click(Sender: TObject);
begin
  DmNFe2.ReopenOpcoesNFe(True);
  UMyMod.FormInsUpd_Show(TFmOpcoesNFe, FmOpcoesNFe, afmoSoAdmin,
    DmNFe2.QrOpcoesNFe, stUpd);
  DmNFe2.ReopenOpcoesNFe(True);
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeInut2, FmNFeInut2, afmoNegarComAviso) then
  begin
    FmNFeInut2.ShowModal;
    FmNFeInut2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeCabA2, FmNFeCabA2, afmoNegarComAviso) then
  begin
    FmNFeCabA2.ShowModal;
    FmNFeCabA2.Destroy;
  end;
end;

procedure TFmPrincipal.MostraMatriz();
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoAdmin) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AdvToolBarButton2Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AdvToolBarButton4Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AGMBVerifiBDpClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(TPopUpMenu(AdvPMVerifiDB), TButton(AGMBVerifiBD));
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  CadastroDeEntidades(0);
end;

procedure TFmPrincipal.CadastroDeEntidades(Cliente: Integer);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeGrupos;
begin

end;

procedure TFmPrincipal.CadastroDeNiveisPlano;
begin

end;

procedure TFmPrincipal.CadastroDePlano;
begin

end;

procedure TFmPrincipal.CadastroDeSubGrupos;
begin

end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.CriaImpressaoDiversos(Indice: Integer);
begin

end;

procedure TFmPrincipal.Escolher1Click(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinEscolhe(FmMyGlyfs.Dialog1, FmPrincipal.sd1);
end;

procedure TFmPrincipal.Escolher2Click(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinEscolhe(FmMyGlyfs.Dialog1, FmPrincipal.sd1);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmNFe_Dmk.Show;
  Enabled := False;
  FmNFe_Dmk.Refresh;
  FmNFe_Dmk.EdSenha.Text := FmNFe_Dmk.EdSenha.Text+'*';
  FmNFe_Dmk.EdSenha.Refresh;
  FmNFe_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmNFe_Dmk.EdSenha.Text := FmNFe_Dmk.EdSenha.Text+'*';
  FmNFe_Dmk.EdSenha.Refresh;
  FmNFe_Dmk.ReloadSkin;
  FmNFe_Dmk.EdLogin.Text := '';
  FmNFe_Dmk.EdLogin.PasswordChar := 'l';
  FmNFe_Dmk.EdSenha.Text := '';
  FmNFe_Dmk.EdSenha.Refresh;
  FmNFe_Dmk.EdLogin.ReadOnly := False;
  FmNFe_Dmk.EdSenha.ReadOnly := False;
  FmNFe_Dmk.EdLogin.SetFocus;
  //FmNFe_Dmk.ReloadSkin;
  FmNFe_Dmk.Refresh;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.ImpressaoDoPlanoDeContas;
begin

end;

function TFmPrincipal.InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
  Associada, RegrFiscal, GraGruX: Integer; NO_tablaPrc: String; StqCenCad,
  FatSemEstq, AFP_Sit: Integer; AFP_Per, Qtde: Double; Cli_Tipo: Integer;
  Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL, ASS_CO_UF, ASS_FILIAL,
  Item_MadeBy: Integer; Item_IPI_ALq, Preco_PrecoF, Preco_PercCustom,
  Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE: Double;
  Preco_MedOrdem: Integer; SQLType: TSQLType; PediVda, OriPart, InfAdCuztm,
  TipoNF, modNF, Serie, nNF, SitDevolu, Servico: Integer; refNFe: String;
  TotalPreCalc: Double; var OriCtrl: Integer;
  Pecas, AreaM2, AreaP2, Peso: Double): Boolean;
begin
  Result := DmNFe2.InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
    Associada, RegrFiscal, GraGruX, NO_tablaPrc,
    StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde,
    Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
    ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
    Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
    Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
    SQLType, PediVda, OriPart, InfAdCuztm,
    TipoNF, modNF, Serie, nNF, SitDevolu, Servico,
    refNFe, TotalPreCalc, OriCtrl,
    Pecas, AreaM2, AreaP2, Peso);
end;

procedure TFmPrincipal.Diversos1Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.CadastroCFOP2003;
begin

end;

procedure TFmPrincipal.CadastroDeCarteira;
begin

end;

procedure TFmPrincipal.CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
begin
  // Compatibilidade
end;

procedure TFmPrincipal.CadastroDeConjuntos;
begin

end;

procedure TFmPrincipal.CadastroDeContas;
begin

end;

procedure TFmPrincipal.CadastroDeContasNiv;
begin

end;

procedure TFmPrincipal.CadastroDeContasSdoLista;
begin

end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade,
  Conta: Integer): Boolean;
begin

end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmNFe_Dmk.Show;
  FmNFe_Dmk.EdLogin.Text   := '';
  FmNFe_Dmk.EdSenha.Text   := '';
  FmNFe_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPediPrzCab();
begin
  if VAR_LIB_ARRAY_EMPRESAS_CONTA > 1 then
  begin
    if DBCheck.CriaFm(TFmPediPrzCab2, FmPediPrzCab2, afmoNegarComAviso) then
    begin
      FmPediPrzCab2.ShowModal;
      FmPediPrzCab2.Destroy;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
    begin
      FmPediPrzCab1.ShowModal;
      FmPediPrzCab1.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.WindowState := wsNormal;
  FmPrincipal.Visible := True;
end;

function TFmPrincipal.MostraUnidMed: Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    Result := True;
  end;
end;

procedure TFmPrincipal.MostraVerifiDB;
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  {
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
  }
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer);
begin
 // Nada
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo;
begin

end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  //Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.RetornoCNAB;
begin

end;

procedure TFmPrincipal.Padro1Click(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title+'\VCLSkins';
  MLAGeral.DelAppKey(Cam, HKEY_LOCAL_MACHINE);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.Padro2Click(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title+'\VCLSkins';
  MLAGeral.DelAppKey(Cam, HKEY_LOCAL_MACHINE);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
//  MLAGeral.ReabrirtabelasFormAtivo;
end;

procedure TFmPrincipal.SaldoDeContas;
begin

end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.Seleciona1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.Limpa1Click(Sender: TObject);
begin
  ImgPrincipal.Bitmap := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmPrincipal.AjustaEstiloImagem(Index: integer);
var
 Indice: Integer;
begin
  if Index >= 0 then Indice := Index else
  Indice := Geral.ReadAppKey('Estio_ImgPrincipal', Application.Title,
    ktInteger, 1, HKEY_LOCAL_MACHINE);
  case Indice of
    00: ImgPrincipal.Style := sbAutosize;
    01: ImgPrincipal.Style := sbCenter;
    02: ImgPrincipal.Style := sbKeepAspRatio;
    03: ImgPrincipal.Style := sbKeepHeight;
    04: ImgPrincipal.Style := sbKeepWidth;
    05: ImgPrincipal.Style := sbNone;
    06: ImgPrincipal.Style := sbStretch;
    07: ImgPrincipal.Style := sbTile;
    else ImgPrincipal.Style := sbCenter;
  end;
  ImgPrincipal.Tag       := Indice;
  Automtico1.Checked     := Indice = 00;
  Centralizado1.Checked  := Indice = 01;
  ManterProporo1.Checked := Indice = 02;
  ManterAltura1.Checked  := Indice = 03;
  ManterLargura1.Checked := Indice = 04;
  Nenhum1.Checked        := Indice = 05;
  Espichar1.Checked      := Indice = 06;
  Repetido1.Checked      := Indice = 07;
  //
  ImgPrincipal.Invalidate;
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.AtzSdoContas;
begin

end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.Matriz1Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  MostraVerifiDB;
end;

procedure TFmPrincipal.MostraBackup3;
begin
  if DBCheck.CriaFm(TFmBackUp3, FmBackUp3, afmoNegarComAviso) then
  begin
    FmBackUp3.ShowModal;
    FmBackUp3.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFatPedNFs(EMP_FILIAL, Cliente,
  CU_PediVda: Integer);
begin
  if DBCheck.CriaFm(TFmFatPedNFs2, FmFatPedNFs2, afmoNegarComAviso) then
  begin
    FmFatPedNFs2.EdFilial.ValueVariant  := EMP_FILIAL;
    FmFatPedNFs2.CBFilial.KeyValue      := EMP_FILIAL;
    FmFatPedNFs2.EdCliente.ValueVariant := Cliente;
    FmFatPedNFs2.CBCliente.KeyValue     := Cliente;
    FmFatPedNFs2.EdPedido.ValueVariant  := CU_PediVda;
    FmFatPedNFs2.ShowModal;
    FmFatPedNFs2.Destroy;
  end;
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKey('MenuStyle', Application.Title,
      ktInteger, 1, HKEY_LOCAL_MACHINE);}
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.Automtico1Click(Sender: TObject);
begin
  Geral.WriteAppKey('Estio_ImgPrincipal', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  AjustaEstiloImagem(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.FormResize(Sender: TObject);
begin
  AjustaEstiloImagem(ImgPrincipal.Tag);
end;

{
function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0;//FmFaturas.QrFaturasCodigo.Value;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';//FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;
}

procedure TFmPrincipal.Backup1Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.PMTrayIconPopup(Sender: TObject);
begin
  Mostrar1.Enabled := not FmPrincipal.Visible;
end;

end.
