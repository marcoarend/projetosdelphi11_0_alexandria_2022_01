object FmNFe_Dmk: TFmNFe_Dmk
  Left = 403
  Top = 236
  Caption = 'Planning'
  ClientHeight = 238
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 349
    Height = 85
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 152
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label3: TLabel
      Left = 200
      Top = 4
      Width = 43
      Height = 13
      Caption = 'Terminal:'
      Visible = False
    end
    object Label4: TLabel
      Left = 296
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object EdLogin: TEdit
      Left = 8
      Top = 24
      Width = 140
      Height = 21
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 152
      Top = 24
      Width = 140
      Height = 21
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdSenhaKeyDown
    end
    object EdTerminal: TEdit
      Left = 200
      Top = 20
      Width = 65
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      Visible = False
      OnKeyDown = EdLoginKeyDown
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 67
      Width = 10
      Height = 17
      Align = alBottom
      Caption = '  '
      TabOrder = 3
    end
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 50
      Width = 347
      Height = 17
      Align = alBottom
      TabOrder = 4
    end
    object EdEmpresa: TEdit
      Left = 296
      Top = 24
      Width = 45
      Height = 21
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 5
      OnKeyDown = EdSenhaKeyDown
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 190
    Width = 349
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 28
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel3: TPanel
      Left = 203
      Top = 1
      Width = 145
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Left = 28
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Fechar'
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
  end
  object Memo: TRichEdit
    Left = 0
    Top = 85
    Width = 349
    Height = 105
    TabStop = False
    Align = alClient
    BorderStyle = bsNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -7
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
  object PopupMenu1: TPopupMenu
    MenuAnimation = [maLeftToRight]
    OwnerDraw = True
    Left = 64
    Top = 4
    object Close1: TMenuItem
      Caption = '&Close'
      OnClick = Close1Click
    end
  end
end
