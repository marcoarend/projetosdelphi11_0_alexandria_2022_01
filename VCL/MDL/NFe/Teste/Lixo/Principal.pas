unit Principal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, GIFImg, ExtCtrls, InvokeRegistry, StdCtrls, Rio,
  SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt, WinInet, Buttons,
  dmkRadioGroup, WinSkinData, WinSkinStore;

type
  TFmPrincipal = class(TForm)
    HTTPRIO1: THTTPRIO;
    Panel1: TPanel;
    CBUF: TComboBox;
    RGAmbiente: TdmkRadioGroup;
    BtOK: TBitBtn;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    procedure BtOKClick(Sender: TObject);
    procedure HTTPRIO1HTTPWebNode1BeforePost(const HTTPReqResp: THTTPReqResp;
      Data: Pointer);
    procedure RGAmbienteClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FAmbiente_Int, FCodigoUF_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt: String;
    FWSDL, FURL: String;
    FResp: WideString;
  public
    { Public declarations }
    procedure AcoesIniciaisDoAplicativo();
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure CadastroDeEntidades(Cliente: Integer);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses dmkGeral, NFeStatusServico, ModuleGeral, Module, UnInternalConsts,
MyDBCheck, NFeSteps2;

{$R *.dfm}

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  DModG.MyPID_DB_Cria();
  DModG.AtualizaCotacoes();
  if (VAR_USUARIO  = -1) or (VAR_USUARIO = -3) then
  begin
    DModG.QrFiliaisSP.Database := Dmod.MyDB;
    DModG.QrFiliaisSP.Close;
    DModG.QrFiliaisSP.Open;
    if DModG.QrFiliaisSP.RecordCount > 0 then
      {
      MostraParamsEmp();
      }
  end;
  {
  AtzPed();
  }
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeSteps2, FmNFeSteps2, afmoSoMaster) then
  begin
    // Serial number do certificado digital
    FmNFeSteps2.EdSerialNumber.ValueVariant := '0863F49605983A88E35F6B0EE5ED7366'; // O Casulo Feliz
    FmNFeSteps2.PnLoteEnv.Visible  := True;
    FmNFeSteps2.RGAcao.Enabled     := True;
    FmNFeSteps2.PnAbrirXML.Visible := True;
    FmNFeSteps2.PnAbrirXML.Enabled := True;
    FmNFeSteps2.CkSoLer.Enabled    := True;
    FmNFeSteps2.ShowModal;
    FmNFeSteps2.Destroy;
  end;
end;

procedure TFmPrincipal.BtOKClick(Sender: TObject);
const
  strVersaoDados = '1.07';
var
 IStatusServico: NfeStatusServicoSoap;
begin
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MensagemBox('Defina o ambiente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  case FAmbiente_Int of
    1:
    begin
      case FCodigoUF_Int of
        (*PR*)41:
        begin
          FWSDL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF.xml';
          FURL  := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF';
        end;
        (*AN*)else // SVAN (SEFAZ Virtual Ambiente Nacional)
        begin
          FWSDL := 'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico/NFeStatusServico.xml';
          FURL  := 'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx';
        end;
      end;
    end;
    2:
    begin
      case FCodigoUF_Int of
        (*PR*)41:
        begin
          FWSDL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF.xml';
          FURL  := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF';
        end;
        (*RS*)43: // Virtual !!!
        begin
          FWSDL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfestatusservico/nfestatusservico.wsdl';
          FURL  := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfestatusservico/nfestatusservico.asmx';
        end;
        (*AN*)else // SVAN (SEFAZ Virtual Ambiente Nacional)
        begin
          FWSDL := 'https://hom.nfe.fazenda.gov.br/NFeStatusServico/NFeStatusServico.wsdl';
          FURL  := 'https://hom.nfe.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx';
        end;
      end;
    end;
  end;
  //
  IStatusServico := (HTTPRIO1 as NfeStatusServicoSoap);
  HTTPRIO1.WSDLLocation := FWSDL;
  HTTPRIO1.URL          := FURL;
  //HTTPRIO1.Service := srv;
  //HTTPRIO1.Port := prt;
  //
  if FWSDL = '' then
  begin
    Geral.MensagemBox(PChar('Servi�o n�o definido ou UF n�o implementada!' +
    #13#10 + 'AVISE A DERMATEK'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    FResp := IStatusServico.nfeStatusServicoNF(
      '<?xml version="1.0" encoding="UTF-8" ?>'+
      '<cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02">'+
      '<versaoDados>' + strVersaoDados + '</versaoDados></cabecMsg>',
      '<?xml version="1.0" encoding="UTF-8" ?>'+
      '<consStatServ xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07">'+
      '<tpAmb>'+ FAmbiente_Txt +'</tpAmb>'+
      '<cUF>' + FCodigoUF_Txt + '</cUF><xServ>STATUS</xServ></consStatServ>');
    {'<nfeCabecMsg>
    <?xml version="1.0" encoding="UTF-8"?><cabecMsg xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.02"><versaoDados>1.07</versaoDados></cabecMsg></nfeCabecMsg>',
    '<consStatServ versao="1.07" xmlns="http://www.portalfiscal.inf.br/nfe">' +
    '<tpAmb>' + FtAmb +'</tpAmb><cUF>'+ FcodUF + '</cUF><xServ>STATUS</xServ></consStatServ>');}
    Memo1.Text := FResp;
  end;
end;

procedure TFmPrincipal.CadastroDeEntidades(Cliente: Integer);
begin
// Parei Aqui ! Fazer!
end;

procedure TFmPrincipal.HTTPRIO1HTTPWebNode1BeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  SerialNumber: String;
begin
  // Serial number do certificado digital
  SerialNumber := '0863F49605983A88E35F6B0EE5ED7366'; // O Casulo Feliz
  //
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(SerialNumber) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        if not (InternetSetOption( Data, 84,
            PCertContext, Sizeof( CERT_CONTEXT ) )) then
        begin
          Geral.MensagemBox('Falha ao selecionar o certificado.', 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;

procedure TFmPrincipal.RGAmbienteClick(Sender: TObject);
begin
  BtOK.Enabled := RGAmbiente.ItemIndex > 0;
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  //Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

end.
