unit LoadXML;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkEdit,
  dmkGeral, UrlMon, InvokeRegistry, Rio,
  SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, (*20230320 JwaWinCrypt,*) WinInet,
  XMLDoc, xmldom, XMLIntf, msxmldom, dmkImage(*, MSXML2_TLB*), UnDmkEnums;

type
  TFmLoadXML = class(TForm)
    Panel1: TPanel;
    Label39: TLabel;
    EdArqXML: TdmkEdit;
    SBDirPedCan: TSpeedButton;
    Memo1: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Button1: TButton;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBDirPedCanClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTextoArq: WideString;
    function AbreArquivoSelecionado(Arquivo: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmLoadXML: TFmLoadXML;

implementation

uses UnMyObjects, retConsStatServ_v107;

{$R *.DFM}

function TFmLoadXML.AbreArquivoSelecionado(Arquivo: String): Boolean;
begin
  FTextoArq := MLAGeral.LoadFileToText(Arquivo);
  Memo1.Text := FTextoArq;
{
  MostraTextoRetorno(FTextoArq);
  if FTextoArq <> '' then
    HabilitaBotoes();
}
  Result := True;
end;

procedure TFmLoadXML.BitBtn1Click(Sender: TObject);
var
  cXML: IXMLTRetConsStatServ;
  arqXML: TXMLDocument;
begin
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := EdArqXML.Text;
  //cXML := LoadretConsStatServ(arqXML.FileName);
  cXML := LoadretConsStatServ(EdArqXML.Text);
  {
  cXML := IXMLTConsStatServ;
  cXML := LoadconsStatServ(FTextoArq);
  }
  ShowMessage('Vers�o = ' + cXML.Versao);
  //
end;

procedure TFmLoadXML.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoadXML.Button1Click(Sender: TObject);
var 
   doc: IXMLDocument; 
   EnvNode: IXMLNode; 
   I: Integer; 
   nc: IXMLNodeCollection; 
begin 
   doc := LoadXMLData( 
     '<Envelope>'+ 
       '<letter name="1"/>'+ 
       '<noletter name="2"/>'+ 
       '<letter name="3"/>'+ 
       '<letter name="4"/>'+ 
     '</Envelope>'); 


   EnvNode := doc.Node.ChildNodes[0]; 
   nc := (EnvNode as IXMLNodeAccess).CreateCollection( 
     TXMLNodeCollection, IXMLNode, 'letter'); 


   for I := 0 to EnvNode.ChildNodes.Count - 1 do 
     if Assigned(EnvNode.ChildNodes[I].Collection) then 
       ShowMessage(IntToStr(EnvNode.ChildNodes[I].Collection.Count)) 
     else 
       ShowMessage('No collection found'); 
end; 

procedure TFmLoadXML.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoadXML.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmLoadXML.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoadXML.SBDirPedCanClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
  begin
    EdArqXML.Text := Arquivo;
    AbreArquivoSelecionado(Arquivo);
  end;
end;

end.
