unit EveGeraXMLCan_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math, TypInfo,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, (*20230320 JwaWinCrypt,*)
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  NFeXMLGerencia,
  // Cancelamento
  eventoCancNFe_v100,
  UnDmkEnums, UnGrl_Vars,
  ACBrNFeWebServices, ACBrDFeXsLibXml2, ACBrDFeSSL;

const
 { TODO : Colocar aqui os stat da Can }
{
  NFe_AllModelos      = '55';
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
}
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/nfe"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosCan_Layout        = '1.00'; // Evento - Cancelamento da NFe - Vers�o do layout
  verEventosCan_Evento        = '1.00'; // Evento - Cancelamento da NFe - Vers�o do Evento
  verEventosCan_Cancel        = '1.00'; // Evento - Cancelamento da NFe - Vers�o do Cancelamento

type
  TEveGeraXMLCan_0100 = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoCan(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chNFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, nProt, xJust: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnEveGeraXMLCan_0100: TEveGeraXMLCan_0100;

implementation

uses ModuleNFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  CanXML: IXMLTEvento;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLCan_0100 }

function AssinarACBr(ConteudoXML: String): String;
var
  docElement, infElement: String;
  SignatureNode, SelectionNamespaces, IdSignature, IdAttr: String;
  s: String;
  //DFeSSLXmlSignLibXml2: TDFeSSLXmlSignLibXml2;
  ADFeSSL: TDFeSSL;
begin
////////////////////////////////////////////////////////////////////////////////
/// Na verdade n�o precisa assinar. � assinado pelo componente ACBrNFe.
/// Mas se precisar, aqui consegui assinar diretamente pelo componenete TDFeSSL!
///
///
  Result := '';
  DmNFe_0000.ConfigurarComponenteNFe();
  //
  ADFeSSL := TDFeSSL.Create;
  try
    with ADFeSSL do
    begin
      URLPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.URLPFX;
      ArquivoPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.ArquivoPFX;
      DadosPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.DadosPFX;
      NameSpaceURI := DmNFe_0000.ACBrNFe1.GetNameSpaceURI;
      NumeroSerie := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.NumeroSerie;
      Senha := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.Senha;

      ProxyHost := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyHost;
      ProxyPass := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyPass;
      ProxyPort := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyPort;
      ProxyUser := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyUser;
      TimeOut := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.TimeOut;
      TimeOutPorThread := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.TimeOutPorThread;

      SSLCryptLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLCryptLib;
      SSLHttpLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLHttpLib;
      SSLXmlSignLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib;
    end;
    //DFeSSLXmlSignLibXml2 := TDFeSSLXmlSignLibXml2(ADFeSSL);
    //
    docElement := 'evento';
    infElement := 'infEvento';

    //s := DFeSSLXmlSignLibXml2.Assinar(ConteudoXML, docElement, infElement);
    Result := ADFeSSL.Assinar(ConteudoXML, docElement, infElement, SignatureNode, SelectionNamespaces, IdSignature, IdAttr);
    //Geral.MB_info(Result);
  finally
    FreeAndNil(ADFeSSL);
  end;
  //Halt(0);
////////////////////////////////////////////////////////////////////////////////
//// fim N�o Precisa!!!???
////////////////////////////////////////////////////////////////////////////////
end;


function TEveGeraXMLCan_0100.CriarDocumentoCan(const FatID, FatNum,
  Empresa: Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
  const CNPJ, CPF, chNFe: String; const dhEvento: TDateTime;
  const TZD_UTC: Double; const tpEvento, nSeqEvento: Integer; verEvento: Double;
  const versao: Double; descEvento, nProt, xJust: String;
  var XMLAssinado: String; const LaAviso1, LaAviso2: TLabel): Boolean;
const
  sProcName = 'TEveGeraXMLCan_0100.CriarDocumentoCan()';
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, Msg: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  CanXML := Getevento(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)

  CanXML.versao := verEventosCan_Layout;
  CanXML.InfEvento.Id := Id;
  CanXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
  CanXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
  if Tipo = 0 then
    CanXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ)
  else
    CanXML.InfEvento.CPF := Geral.SoNumero_TT(CPF);
  CanXML.InfEvento.ChNFe := Geral.SoNumero_TT(chNFe);
  CanXML.InfEvento.DhEvento := dmkPF.FDT_NFe_UTC(dhEvento, TZD_UTC); 
  CanXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
  CanXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
  CanXML.InfEvento.VerEvento := verEventosCan_Evento;
  CanXML.InfEvento.DetEvento.Versao := verEventosCan_Cancel;
  CanXML.InfEvento.DetEvento.DescEvento := descEvento;
  CanXML.InfEvento.DetEvento.NProt := nProt;
  CanXML.InfEvento.DetEvento.XJust := xJust;
  //

  Texto := CanXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  // Assinar!
  // ini 2022-02-15
  NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
    Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);

  // TFormaAssinaXXe = (faxxeCAPICOM=0, faxxeDotNetFrameWork=1, faxxeACBr=2);
  case TFormaAssinaXXe(VAR_AssDigMode) of
    (*0*)faxxeCAPICOM:
    begin
      NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
      if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
        Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);
    end;
    //(*1*)faxxeDotNetFrameWork:
    (*2*)faxxeACBr:
    begin
      XMLAssinado := AssinarACBr(Texto);
    end;
    (*?*)else
    begin
      Msg := GetEnumName(TypeInfo(TFormaAssinaXXe), Integer(VarType(VAR_AssDigMode)));
      Geral.MB_Erro('Forma de assinatura (' + Msg + ') n�o definida em ' + sProcName);
    end;
  end;


  // ini 2022-02-15
  arqXML := nil;
end;

end.
