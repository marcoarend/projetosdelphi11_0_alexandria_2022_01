unit NFeEveRCan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, NFeXMLGerencia, UnDmkEnums, UnXXe_PF;

type
  TFmNFeEveRCan = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrJust: TmySQLQuery;
    QrJustCodigo: TIntegerField;
    QrJustNome: TWideStringField;
    QrJustCodUsu: TIntegerField;
    QrJustAplicacao: TIntegerField;
    DsJust: TDataSource;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdJust: TdmkEditCB;
    CBJust: TdmkDBLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nNF: TdmkEdit;
    Label3: TLabel;
    EdnProt: TdmkEdit;
    Memo1: TMemo;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeEveRCan: TFmNFeEveRCan;

implementation

uses UnMyObjects, Module, NFe_Tabs, NFeEveRCab, ModuleGeral, UMySQLModule,
  EveGeraXMLCan_0100, ModuleNFe_0000, NFe_PF;

{$R *.DFM}

procedure TFmNFeEveRCan.BtOKClick(Sender: TObject);
var
  Destino, Dir, nProt, xJust: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, nJust: Integer;
  // XML
  XML_Eve: String;
begin
  if not DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_CAN_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmNFeEveRCab.IncluiNFeEveRCab(0, eveCan, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevercan', '', 0);
  end else begin
    FatID    := FmNFeEveRCab.QrNFeEveRCabFatID.Value;
    FatNum   := FmNFeEveRCab.QrNFeEveRCabFatNum.Value;
    Empresa  := FmNFeEveRCab.QrNFeEveRCabEmpresa.Value;
    Controle := FmNFeEveRCab.QrNFeEveRCabControle.Value;
    Conta    := FmNFeEveRCab.QrNFeEveRCanConta.Value;
  end;
  //
  nProt := EdnProt.Text;
  nJust := EdJust.ValueVariant;
  xJust :=  Trim(XXe_PF.ValidaTexto_XML(QrJustNome.Value, 'eve.can.xjust', 'eve.can.xjust'));
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeevercan', False, [
    'nProt', 'nJust', 'xJust'], [
    'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
    nProt, nJust, xJust], [
    FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnEveGeraXMLCan_0100.CriarDocumentoCan(FatID, FatNum, Empresa,
      FmNFeEveRCab.QrERCId.Value, FmNFeEveRCab.QrERCcOrgao.Value,
      FmNFeEveRCab.QrERCtpAmb.Value, FmNFeEveRCab.QrERCTipoEnt.Value,
      FmNFeEveRCab.QrERCCNPJ.Value, FmNFeEveRCab.QrERCCPF.Value,
      FmNFeEveRCab.QrERCchNFe.Value, FmNFeEveRCab.QrERCdhEvento.Value,
      FmNFeEveRCab.QrERCTZD_UTC.Value, FmNFeEveRCab.QrERCtpEvento.Value,
      FmNFeEveRCab.QrERCnSeqEvento.Value, FmNFeEveRCab.QrERCverEvento.Value,
      FmNFeEveRCab.QrERCversao.Value, FmNFeEveRCab.QrERCdescEvento.Value,
      nProt, xJust,
      XML_Eve, LaAviso1, LaAviso2) then
      begin
        Destino := DmNFe_0000.SalvaXML(NFE_EXT_EVE_ENV_CAN_XML,
        FmNFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
        if FileExists(Destino) then
        begin
            Status := DmNFe_0000.stepNFeAssinada();
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
            'XML_Eve', 'Status'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            Geral.WideStringToSQLString(XML_Eve), Status], [
            FatID, FatNum, Empresa, Controle], True) then
              FmNFeEveRCab.ReopenNFeEveRCab(Controle);
        end;
      end;

    //
    FmNFeEveRCab.ReopenNFeEveRCan(0);
    Close;
  end;
end;

procedure TFmNFeEveRCan.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveRCan.EdJustChange(Sender: TObject);
begin
  BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmNFeEveRCan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveRCan.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UMyMod.AbreQuery(QrJust, Dmod.MyDB);
end;

procedure TFmNFeEveRCan.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRCan.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnNFe_PF.MostraFormNFeJust(EdJust.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdJust, CBJust, QrJust, VAR_CADASTRO);
    //
    EdJust.SetFocus;
  end;
end;

end.
