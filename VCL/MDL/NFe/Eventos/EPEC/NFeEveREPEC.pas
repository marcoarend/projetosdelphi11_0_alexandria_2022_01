unit NFeEveREPEC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, NFeXMLGerencia, UnDmkEnums, UnXXe_PF;

type
  TFmNFeEveREPEC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrNFeCabA: TMySQLQuery;
    DsNFeCabA: TDataSource;
    GroupBox2: TGroupBox;
    PnEPEC: TPanel;
    Panel2: TPanel;
    EdcOrgaoAutor: TdmkEdit;
    Label3: TLabel;
    EdtpAutor: TdmkEdit;
    Label4: TLabel;
    EdtpAutor_TXT: TdmkEdit;
    EdverAplic: TdmkEdit;
    Label5: TLabel;
    LadhEmi: TLabel;
    TPdEmi: TdmkEditDateTimePicker;
    EdhEmi: TdmkEdit;
    EdhEmiTZD: TdmkEdit;
    EdtpNF: TdmkEdit;
    Label202: TLabel;
    EdtpNF_TXT: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    Label7: TLabel;
    Eddest_CNPJ: TdmkEdit;
    Label119: TLabel;
    Eddest_CPF: TdmkEdit;
    Label364: TLabel;
    Eddest_idEstrangeiro: TdmkEdit;
    Label8: TLabel;
    Eddest_xNome: TdmkEdit;
    Label117: TLabel;
    Eddest_IE: TdmkEdit;
    Label113: TLabel;
    Eddest_UF: TdmkEdit;
    Label142: TLabel;
    EdvNF: TdmkEdit;
    EdvICMS: TdmkEdit;
    Label143: TLabel;
    Label243: TLabel;
    EdvST: TdmkEdit;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_hEmi: TTimeField;
    QrNFeCabAemit_IE: TWideStringField;
    QrNFeCabAemit_UF: TWideStringField;
    QrNFeCabAdest_UF: TWideStringField;
    QrNFeCabAdest_CNPJ: TWideStringField;
    QrNFeCabAdest_CPF: TWideStringField;
    QrNFeCabAdest_idEstrangeiro: TWideStringField;
    QrNFeCabAdest_IE: TWideStringField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    Memo1: TMemo;
    Edide_serie: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edide_nNF: TdmkEdit;
    EdIE: TdmkEdit;
    Label132: TLabel;
    Label6: TLabel;
    Edide_ID: TdmkEdit;
    SbDataHora: TSpeedButton;
    EdtpEmis_TXT: TdmkEdit;
    EdtpEmis: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdtpNFChange(Sender: TObject);
    procedure SbDataHoraClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeEveREPEC: TFmNFeEveREPEC;

implementation

uses UnMyObjects, Module, NFe_Tabs, NFeEveRCab, ModuleGeral, UMySQLModule,
  EveGeraXMLEPEC_0100, ModuleNFe_0000, NFe_PF, DmkDAC_PF, MyDBCheck;

{$R *.DFM}

procedure TFmNFeEveREPEC.BtOKClick(Sender: TObject);
var
  verAplic, dhEmi, IE, dest_UF, dest_CNPJ, dest_CPF, dest_idEstrangeiro, dest_IE,
  Dir, XML_Eve, Destino: String;
  FatID, FatNum, Empresa, Controle, Conta, cOrgaoAutor, tpAutor, tpNF, Status:
  Integer;
  dhEmiTZD, vNF, vICMS, vST: Double;
  SQLType: TSQLType;
  versao, descEvento, ide_dEmi, ide_hEmi, Id: String;
  ide_dhEmiTZD: Double;
  ide_tpEmis: Integer;
begin
  SQLType := ImgTipo.SQLType;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
  'SELECT ide_dEmi, ide_hEmi, emit_IE, emit_UF, dest_UF, ',
  'dest_CNPJ, dest_CPF, dest_idEstrangeiro, dest_IE,',
  'ICMSTot_vNF, ICMSTot_vICMS, ICMSTot_vST',
  'FROM nfecaba ',
  'WHERE Id="' + Edide_ID.Text + '"',
  '']);
  if MyObjects.FIC(QrNFeCabA.RecordCount = 0 , nil,
    'N�o foi poss�vel obter as informa��es da chave NF-e ' + Edide_ID.Text) then
      Exit;
  if not DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_EPEC_XML, Dir, True) then
    Exit;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if not FmNFeEveRCab.IncluiNFeEveRCab(0, eveEPEC, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverepec', '', 0);
  end else begin
    FatID    := FmNFeEveRCab.QrNFeEveRCabFatID.Value;
    FatNum   := FmNFeEveRCab.QrNFeEveRCabFatNum.Value;
    Empresa  := FmNFeEveRCab.QrNFeEveRCabEmpresa.Value;
    Controle := FmNFeEveRCab.QrNFeEveRCabControle.Value;
    Conta    := FmNFeEveRCab.QrNFeEveREPECConta.Value;
  end;
  //
  versao             := verEventosEPEC_EPEC;
  descEvento         := 'EPEC';
  cOrgaoAutor        := EdcOrgaoAutor.ValueVariant;
  tpAutor            := EdtpAutor.ValueVariant;
  verAplic           := EdverAplic.ValueVariant;
  // N�o inserir o "T" aqui, diferente do dhEmi para XML!!!
  dhEmi              := Geral.FDT(TPdEmi.Date, 1) + ' ' + EdhEmi.Text;
  dhEmiTZD           := EdhEmiTZD.ValueVariant;
  tpNF               := EdtpNF.ValueVariant;
  IE                 := EdIE.ValueVariant;
  dest_UF            := Eddest_UF.ValueVariant;
  dest_CNPJ          := Eddest_CNPJ.ValueVariant;
  dest_CPF           := Eddest_CPF.ValueVariant;
  dest_idEstrangeiro := Eddest_idEstrangeiro.ValueVariant;
  dest_IE            := Eddest_IE.ValueVariant;
  vNF                := EdvNF.ValueVariant;
  vICMS              := EdvICMS.ValueVariant;
  vST                := EdvST.ValueVariant;
  //
  Conta := UMyMod.BPGS1I32('nfeeverepec', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeeverepec', False, [
  'versao', 'descEvento',
  'cOrgaoAutor', 'tpAutor', 'verAplic',
  'dhEmi', 'dhEmiTZD', 'tpNF',
  'IE', 'dest_UF', 'dest_CNPJ',
  'dest_CPF', 'dest_idEstrangeiro', 'dest_IE',
  'vNF', 'vICMS', 'vST'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  versao, descEvento,
  cOrgaoAutor, tpAutor, verAplic,
  dhEmi, dhEmiTZD, tpNF,
  IE, dest_UF, dest_CNPJ,
  dest_CPF, dest_idEstrangeiro, dest_IE,
  vNF, vICMS, vST], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
(*
                MOC 7.0 p�gina 39/150
Como os dados do EPEC s�o obtidos a partir da NF-e que n�o conseguiu ser transmitida por
problemas t�cnicos, quando for transmitida, esta NF-e dever� possuir os mesmos dados do EPEC
autorizado anteriormente.
*)
    Id           := Geral.SoNumero_TT(Edide_ID.ValueVariant);
    ide_dEmi     := Geral.FDT(TPdEmi.Date, 1);
    ide_hEmi     := EdhEmi.Text;
    ide_dhEmiTZD := EdhEmiTZD.ValueVariant;
    ide_tpEmis   := EdtpEmis.ValueVariant; // 4
    // Inserir o "T" aqui, diferente do dhEmi para SQL
    dhEmi        := Geral.FDT(TPdEmi.Date, 1) + 'T' + EdhEmi.Text;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'ide_dEmi', 'ide_hEmi', 'ide_dhEmiTZD',
    'ide_tpEmis'], ['Id'
    ], [
    ide_dEmi, ide_hEmi, ide_dhEmiTZD,
    ide_tpEmis], [Id
    ], True) then
    begin
      if UnEveGeraXMLEPEC_0100.CriarDocumentoEPEC(FatID, FatNum, Empresa,
      FmNFeEveRCab.QrERCId.Value, FmNFeEveRCab.QrERCcOrgao.Value,
      FmNFeEveRCab.QrERCtpAmb.Value, FmNFeEveRCab.QrERCTipoEnt.Value,
      FmNFeEveRCab.QrERCCNPJ.Value, FmNFeEveRCab.QrERCCPF.Value,
      FmNFeEveRCab.QrERCchNFe.Value, FmNFeEveRCab.QrERCdhEvento.Value,
      FmNFeEveRCab.QrERCTZD_UTC.Value, FmNFeEveRCab.QrERCtpEvento.Value,
      FmNFeEveRCab.QrERCnSeqEvento.Value, FmNFeEveRCab.QrERCverEvento.Value,
      FmNFeEveRCab.QrERCversao.Value, FmNFeEveRCab.QrERCdescEvento.Value,
      verAplic, dhEmi, IE, dest_UF, dest_CNPJ, dest_CPF, dest_idEstrangeiro,
      dest_IE, cOrgaoAutor, tpAutor, tpNF, dhEmiTZD, vNF, vICMS, vST, XML_Eve,
      LaAviso1, LaAviso2) then
      begin
        Destino := DmNFe_0000.SalvaXML(NFE_EXT_EVE_ENV_EPEC_XML,
        FmNFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
        if FileExists(Destino) then
        begin
            Status := DmNFe_0000.stepNFeAssinada();
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
            'XML_Eve', 'Status'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            Geral.WideStringToSQLString(XML_Eve), Status], [
            FatID, FatNum, Empresa, Controle], True) then
              FmNFeEveRCab.ReopenNFeEveRCab(Controle);
        end;
      end;
    end;
    //
    FmNFeEveRCab.ReopenNFeEveREPEC(0);
    Close;
  end;
end;

procedure TFmNFeEveREPEC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveREPEC.EdtpNFChange(Sender: TObject);
begin
  EdtpNF_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF, EdtpNF.ValueVariant);
end;

procedure TFmNFeEveREPEC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveREPEC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFeEveREPEC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveREPEC.SbDataHoraClick(Sender: TObject);
(*
const
  MinData = 0;
  HrDefault = 0;
  HabilitaHora = True;
var
  Data: TDateTime;
  Qry: TmySQLQuery;
  Serie: Integer;
begin
  Data := DModG.ObtemAgora() - 90;
  DBCheck.ObtemData(Data, Data, MinData, HrDefault, HabilitaHora, 'Data/hora da NFe');
*)
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    LadhEmi.Enabled := True;
    TPdEmi.Enabled := True;
    EdhEmi.Enabled := True;
  end;
end;

end.
