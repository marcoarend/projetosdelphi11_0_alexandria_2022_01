object FmNFeEveREPEC: TFmNFeEveREPEC
  Left = 339
  Top = 185
  Caption = 'NFe-EVENT-005 :: EPEC - Evento Pr'#233'vio de Emiss'#227'o em Conting'#234'ncia'
  ClientHeight = 420
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 529
        Height = 32
        Caption = 'Evento Pr'#233'vio de Emiss'#227'o em Conting'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 529
        Height = 32
        Caption = 'Evento Pr'#233'vio de Emiss'#227'o em Conting'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 529
        Height = 32
        Caption = 'Evento Pr'#233'vio de Emiss'#227'o em Conting'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 258
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 258
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 73
        Align = alTop
        Caption = ' Dados da NF-e: '
        Enabled = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 543
          Height = 56
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label2: TLabel
            Left = 48
            Top = 8
            Width = 32
            Height = 13
            Caption = 'N'#186' NF:'
          end
          object Label132: TLabel
            Left = 132
            Top = 7
            Width = 90
            Height = 13
            Caption = 'Inscri'#231#227'o Estadual:'
            FocusControl = EdIE
          end
          object Label6: TLabel
            Left = 268
            Top = 7
            Width = 75
            Height = 13
            Caption = 'Chave da NF-e:'
            FocusControl = Edide_ID
          end
          object Edide_serie: TdmkEdit
            Left = 8
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object Edide_nNF: TdmkEdit
            Left = 48
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdIE: TdmkEdit
            Left = 132
            Top = 23
            Width = 132
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'emit_IE'
            UpdCampo = 'emit_IE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Edide_ID: TdmkEdit
            Left = 267
            Top = 23
            Width = 272
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'emit_IE'
            UpdCampo = 'emit_IE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 545
          Top = 15
          Width = 237
          Height = 56
          Align = alClient
          ReadOnly = True
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 73
        Width = 784
        Height = 185
        Align = alClient
        Caption = ' EPEC - Evento Pr'#233'vio de Emiss'#227'o em Conting'#234'ncia : '
        TabOrder = 1
        object PnEPEC: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 168
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 4
            Width = 28
            Height = 13
            Caption = 'Autor:'
            Enabled = False
          end
          object Label4: TLabel
            Left = 44
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Tipo de Autor:'
            Enabled = False
          end
          object Label5: TLabel
            Left = 256
            Top = 4
            Width = 99
            Height = 13
            Caption = 'Vers'#227'o do aplicativo:'
            Enabled = False
          end
          object LadhEmi: TLabel
            Left = 424
            Top = 4
            Width = 183
            Height = 13
            Caption = 'Data / hora e TZD da sa'#237'sa / entrada:'
            Enabled = False
          end
          object Label202: TLabel
            Left = 660
            Top = 4
            Width = 90
            Height = 13
            Caption = 'Tipo do doc. fiscal:'
            Enabled = False
            FocusControl = EdtpNF
          end
          object Label142: TLabel
            Left = 16
            Top = 120
            Width = 77
            Height = 13
            Caption = '$ Total da NF-e:'
            Enabled = False
            FocusControl = EdvNF
          end
          object Label143: TLabel
            Left = 100
            Top = 120
            Width = 65
            Height = 13
            Caption = '$ Total ICMS:'
            Enabled = False
            FocusControl = EdvICMS
          end
          object Label243: TLabel
            Left = 184
            Top = 120
            Width = 53
            Height = 13
            Caption = '$ Total ST:'
            Enabled = False
            FocusControl = EdvST
          end
          object SbDataHora: TSpeedButton
            Left = 636
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbDataHoraClick
          end
          object Label9: TLabel
            Left = 268
            Top = 120
            Width = 80
            Height = 13
            Caption = 'Tipo de emiss'#227'o:'
            Enabled = False
            FocusControl = EdtpEmis
          end
          object EdcOrgaoAutor: TdmkEdit
            Left = 12
            Top = 20
            Width = 29
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdtpAutor: TdmkEdit
            Left = 44
            Top = 20
            Width = 25
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdtpAutor_TXT: TdmkEdit
            Left = 72
            Top = 20
            Width = 181
            Height = 21
            Enabled = False
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Empresa Emitente/Pessoa F'#205'sica'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Empresa Emitente/Pessoa F'#205'sica'
            ValWarn = False
          end
          object EdverAplic: TdmkEdit
            Left = 256
            Top = 20
            Width = 165
            Height = 21
            Enabled = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPdEmi: TdmkEditDateTimePicker
            Left = 424
            Top = 20
            Width = 112
            Height = 21
            Date = 40048.000000000000000000
            Time = 0.941462858798331600
            Enabled = False
            TabOrder = 4
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdhEmi: TdmkEdit
            Left = 536
            Top = 20
            Width = 53
            Height = 21
            Enabled = False
            TabOrder = 5
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdhEmiTZD: TdmkEdit
            Left = 588
            Top = 20
            Width = 48
            Height = 21
            Enabled = False
            TabOrder = 6
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfTZD_UTC
            Texto = '+00:00'
            QryCampo = 'ide_dhSaiEntTZD'
            UpdCampo = 'ide_dhSaiEntTZD'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdtpNF: TdmkEdit
            Left = 660
            Top = 20
            Width = 21
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '1'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ide_tpNF'
            UpdCampo = 'ide_tpNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdtpNFChange
          end
          object EdtpNF_TXT: TdmkEdit
            Left = 680
            Top = 20
            Width = 74
            Height = 21
            Enabled = False
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object GroupBox3: TGroupBox
            Left = 12
            Top = 48
            Width = 745
            Height = 65
            Caption = ' Destinat'#225'rio: '
            TabOrder = 9
            object Panel5: TPanel
              Left = 2
              Top = 15
              Width = 741
              Height = 48
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label7: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'CNPJ...:'
                Enabled = False
                FocusControl = Eddest_CNPJ
              end
              object Label119: TLabel
                Left = 120
                Top = 4
                Width = 59
                Height = 13
                Caption = '... ou CPF...:'
                Enabled = False
                FocusControl = Eddest_CPF
              end
              object Label364: TLabel
                Left = 236
                Top = 4
                Width = 82
                Height = 13
                Caption = '... ou estrangeiro:'
                Enabled = False
                FocusControl = Eddest_idEstrangeiro
              end
              object Label8: TLabel
                Left = 372
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                Enabled = False
                FocusControl = Eddest_xNome
              end
              object Label117: TLabel
                Left = 580
                Top = 4
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                Enabled = False
                FocusControl = Eddest_IE
              end
              object Label113: TLabel
                Left = 704
                Top = 4
                Width = 17
                Height = 13
                Caption = 'UF:'
                Enabled = False
                FocusControl = Eddest_UF
              end
              object Eddest_CNPJ: TdmkEdit
                Left = 4
                Top = 20
                Width = 113
                Height = 21
                Enabled = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_CNPJ'
                UpdCampo = 'dest_CNPJ'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_CPF: TdmkEdit
                Left = 120
                Top = 20
                Width = 113
                Height = 21
                Enabled = False
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtCPFJ_NFe
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_CPF'
                UpdCampo = 'dest_CPF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_idEstrangeiro: TdmkEdit
                Left = 236
                Top = 20
                Width = 132
                Height = 21
                Enabled = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_idEstrangeiro'
                UpdCampo = 'dest_idEstrangeiro'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_xNome: TdmkEdit
                Left = 372
                Top = 20
                Width = 205
                Height = 21
                Enabled = False
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_xNome'
                UpdCampo = 'dest_xNome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_IE: TdmkEdit
                Left = 580
                Top = 20
                Width = 121
                Height = 21
                Enabled = False
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_IE'
                UpdCampo = 'dest_IE'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Eddest_UF: TdmkEdit
                Left = 704
                Top = 20
                Width = 30
                Height = 21
                Enabled = False
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'dest_UF'
                UpdCampo = 'dest_UF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object EdvNF: TdmkEdit
            Left = 16
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMSTot_vBC'
            UpdCampo = 'ICMSTot_vBC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdvICMS: TdmkEdit
            Left = 100
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMSTot_vICMS'
            UpdCampo = 'ICMSTot_vICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdvST: TdmkEdit
            Left = 184
            Top = 136
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMSTot_vICMSDeson'
            UpdCampo = 'ICMSTot_vICMSDeson'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdtpEmis_TXT: TdmkEdit
            Left = 288
            Top = 136
            Width = 469
            Height = 21
            Enabled = False
            ReadOnly = True
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Conting'#234'ncia EPEC (Evento Pr'#233'vio da Emiss'#227'o em Conting'#234'ncia);'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Conting'#234'ncia EPEC (Evento Pr'#233'vio da Emiss'#227'o em Conting'#234'ncia);'
            ValWarn = False
          end
          object EdtpEmis: TdmkEdit
            Left = 268
            Top = 136
            Width = 21
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 14
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '4'
            QryCampo = 'ide_tpEmiss'
            UpdCampo = 'ide_tpEmiss'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 4
            ValWarn = False
            OnChange = EdtpNFChange
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 306
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 350
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ide_dEmi, ide_hEmi, emit_IE, emit_UF, dest_UF, '
      'dest_CNPJ, dest_CPF, dest_idEstrangeiro, dest_IE,'
      'ICMSTot_vNF, ICMSTot_vICMS, ICMSTot_vST'
      'FROM nfecaba'
      'WHERE Id=""')
    Left = 344
    Top = 136
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeCabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrNFeCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrNFeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNFeCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrNFeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNFeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNFeCabAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
    object QrNFeCabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 344
    Top = 184
  end
end
