unit EveGeraXMLEPEC_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math, TypInfo,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, (*20230320 JwaWinCrypt,*)
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  NFeXMLGerencia,
  // EPEC
  //EPEC_v100,
  UnDmkEnums, UnGrl_Vars, UnXXe_PF,
  ACBrNFeWebServices, ACBrDFeXsLibXml2(*, ACBrDFeSSL*);

const
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/nfe"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosEPEC_Layout        = '1.00'; // Evento - Evento Pr�vio de Emiss�o em Conting�ncia - Vers�o do layout
  verEventosEPEC_Evento        = '1.00'; // Evento - Evento Pr�vio de Emiss�o em Conting�ncia - Vers�o do Evento
  verEventosEPEC_EPEC          = '1.00'; // Evento - Evento Pr�vio de Emiss�o em Conting�ncia - Vers�o da EPEC

type
  TEveGeraXMLEPEC_0100 = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoEPEC(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chNFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, verAplic, dhEmi, IE, dest_UF,
              dest_CNPJ, dest_CPF, dest_idEstrangeiro, dest_IE: String;
              cOrgaoAutor, tpAutor, tpNF: Integer; dhEmiTZD, vNF, vICMS, vST:
              Double; var XMLAssinado: String; const LaAviso1, LaAviso2:
              TLabel): Boolean;
  end;

var
  UnEveGeraXMLEPEC_0100: TEveGeraXMLEPEC_0100;

implementation

uses ModuleNFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  //EPECXML: IXMLTEvento;
  //arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLEPEC_0100 }

function TEveGeraXMLEPEC_0100.CriarDocumentoEPEC(const FatID, FatNum,
  Empresa: Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
  const CNPJ, CPF, chNFe: String; const dhEvento: TDateTime;
  const TZD_UTC: Double; const tpEvento, nSeqEvento: Integer; verEvento: Double;
  const versao: Double; descEvento, verAplic, dhEmi, IE, dest_UF, dest_CNPJ,
  dest_CPF, dest_idEstrangeiro, dest_IE: String; cOrgaoAutor, tpAutor, tpNF:
  Integer; dhEmiTZD, vNF, vICMS, vST: Double; var XMLAssinado: String;
  const LaAviso1, LaAviso2: TLabel): Boolean;
const
  sProcName = 'TEveGeraXMLEPEC_0100.CriarDocumentoEPEC()';
var
  NumeroSerial: String;
  Texto, Msg, DH_TZD, DocEmpresa, DocCliente, IEEmpresa, IECliente: String;
begin
  // Desenvolvendo Aqui!
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
  { Servidor AN n�o aceita tags fora de ordem como o XML est� sendo constru�do pelo  EPECXML
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  EPECXML := Getevento(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)

  EPECXML.versao := verEventosEPEC_Layout;
  EPECXML.InfEvento.Id := Id;
  EPECXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
  EPECXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
  if Tipo = 0 then
    EPECXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ)
  else
    EPECXML.InfEvento.CPF := Geral.SoNumero_TT(CPF);
  EPECXML.InfEvento.ChNFe := Geral.SoNumero_TT(chNFe);
  EPECXML.InfEvento.DhEvento := dmkPF.FDT_NFe_UTC(dhEvento, TZD_UTC);
  EPECXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
  EPECXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
  EPECXML.InfEvento.VerEvento := verEventosEPEC_Evento;
  EPECXML.InfEvento.DetEvento.Versao := verEventosEPEC_EPEC;
  EPECXML.InfEvento.DetEvento.DescEvento := descEvento;
  EPECXML.InfEvento.DetEvento.COrgaoAutor := Geral.FF0(cOrgaoAutor);
  EPECXML.InfEvento.DetEvento.TpAutor := Geral.FF0(tpAutor);
  EPECXML.InfEvento.DetEvento.VerAplic := verAplic;
  //DH_TZD := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', dhEmi) + dmkPF.TZD_UTC_FloatToSignedStr(dhEmiTZD);
  EPECXML.InfEvento.DetEvento.DhEmi := dhEmi + dmkPF.TZD_UTC_FloatToSignedStr(dhEmiTZD);
  EPECXML.InfEvento.DetEvento.TpNF := Geral.FF0(tpNF);
  EPECXML.InfEvento.DetEvento.IE := IE;
  EPECXML.InfEvento.DetEvento.Dest.UF := dest_UF;
  if dest_CNPJ <> EmptyStr then
    EPECXML.InfEvento.DetEvento.Dest.CNPJ := dest_CNPJ
  else
  if dest_CPF <> EmptyStr then
    EPECXML.InfEvento.DetEvento.Dest.CPF := dest_CPF
  else
  if dest_idEstrangeiro <> EmptyStr then
    EPECXML.InfEvento.DetEvento.Dest.IdEstrangeiro := dest_idEstrangeiro;
  EPECXML.InfEvento.DetEvento.Dest.IE := IE;
  // erro XSD? Valores NF no Dest!!!
  EPECXML.InfEvento.DetEvento.Dest.VNF := Geral.FFT_Dot(vNF, 2, siPositivo);
  EPECXML.InfEvento.DetEvento.Dest.VICMS := Geral.FFT_Dot(vICMS, 2, siPositivo);
  EPECXML.InfEvento.DetEvento.Dest.VST := Geral.FFT_Dot(vST, 2, siPositivo);
  //
  Texto := EPECXML.XML;
}
  if Tipo = 0 then
    DocEmpresa := '<CNPJ>' + Geral.SoNumero_TT(CNPJ) + '</CNPJ>'
  else
    DocEmpresa := '<CPF>' + Geral.SoNumero_TT(CPF)+ '</CPF>';
  //
  if dest_CNPJ <> EmptyStr then
    DocCliente := '<CNPJ>'+ Geral.SoNumero_TT(dest_CNPJ) + '</CNPJ>'
  else
  if dest_CPF <> EmptyStr then
    DocCliente := '<CPF>' + Geral.SoNumero_TT(dest_CPF) + '</CPF>'
  else
  if dest_idEstrangeiro <> EmptyStr then
    DocCliente := '<idEstrangeiro>' + dest_idEstrangeiro + '</idEstrangeiro>';
  IEEmpresa := '<IE>' + IE + '</IE>';
  IECliente := Geral.SoNumero_TT(dest_IE);
  if IECliente <> EmptyStr then
    IECliente := '<IE>' + IECliente + '</IE>';
  //DH_TZD := dhEmi + dmkPF.TZD_UTC_FloatToSignedStr(dhEmiTZD);
  //
(*
verEventosEPEC_Layout
verEventosEPEC_Evento
verEventosEPEC_EPEC
*)
  Texto :=
  '<evento xmlns="http://www.portalfiscal.inf.br/nfe" ' +
    'versao="' + verEventosEPEC_Evento +'">' +
  '<infEvento Id="' + Id + '">' +
  '<cOrgao>' + Geral.FFN(cOrgao, 2) + '</cOrgao>' +
  '<tpAmb>' + Geral.FF0(tpAmb) + '</tpAmb>' +
  DocEmpresa +
  '<chNFe>' + chNFe + '</chNFe>' +
  '<dhEvento>' + dmkPF.FDT_NFe_UTC(dhEvento, TZD_UTC) + '</dhEvento>' +
  '<tpEvento>' + Geral.FF0(tpEvento) + '</tpEvento>' +
  '<nSeqEvento>' + Geral.FF0(nSeqEvento) + '</nSeqEvento>' +
  '<verEvento>' + verEventosEPEC_Evento + '</verEvento>' +
  '<detEvento versao="' + verEventosEPEC_EPEC + '">' +
    '<descEvento>' + descEvento + '</descEvento>' +
    '<cOrgaoAutor>' + Geral.FF0(cOrgaoAutor) + '</cOrgaoAutor>' +
    '<tpAutor>' + Geral.FF0(tpAutor) + '</tpAutor>' +
    '<verAplic>' + verAplic + '</verAplic>' +
    '<dhEmi>' + dhEmi + dmkPF.TZD_UTC_FloatToSignedStr(dhEmiTZD) + '</dhEmi>' +
    '<tpNF>' + Geral.FF0(tpNF) + '</tpNF>' +
    IEEmpresa +
    '<dest>' +
      '<UF>' + dest_UF + '</UF>' +
      DocCliente +
      IECliente +
      '<vNF>' + Geral.FFT_Dot(vNF, 2, siPositivo) + '</vNF>' +
      '<vICMS>' + Geral.FFT_Dot(vICMS, 2, siPositivo) + '</vICMS>' +
      '<vST>' + Geral.FFT_Dot(vST, 2, siPositivo) + '</vST>' +
    '</dest>' +
  '</detEvento>' +
  '</infEvento>' +
  '</evento>';
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  // Assinar!
  // ini 2022-02-15
  NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
    Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);

  // TFormaAssinaXXe = (faxxeCAPICOM=0, faxxeDotNetFrameWork=1, faxxeACBr=2);
  case TFormaAssinaXXe(VAR_AssDigMode) of
    (*0*)faxxeCAPICOM:
    begin
      NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
      if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
        Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);
    end;
    //(*1*)faxxeDotNetFrameWork:
    (*2*)faxxeACBr:
    begin
      XMLAssinado := XXe_PF.AssinarACBr(Texto);
    end;
    (*?*)else
    begin
      Msg := GetEnumName(TypeInfo(TFormaAssinaXXe), Integer(VarType(VAR_AssDigMode)));
      Geral.MB_Erro('Forma de assinatura (' + Msg + ') n�o definida em ' + sProcName);
    end;
  end;
  //arqXML := nil;
end;

end.
