unit EveGeraXMLMDe_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, (*20230320 JwaWinCrypt,*)
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  NFeXMLGerencia,
  // Manifestação do Destinatário
  confRecebto_v100;

const
 { TODO : Colocar aqui os stat da MDe }
{
  NFe_AllModelos      = '55';
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
}
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/nfe"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosMDe_Layout        = '1.00'; // Evento - Manifestação do Destinatário - Versão do layout
  verEventosMDe_Evento        = '1.00'; // Evento - Manifestação do Destinatário - Versão do Evento
  verEventosMDe_ManDes        = '1.00'; // Evento - Manifestação do Destinatário - Versão da Manifestação do Destinatário

type
  TEveGeraXMLMDe_0100 = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoMDe(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chNFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, xJust: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnEveGeraXMLMDe_0100: TEveGeraXMLMDe_0100;

implementation

uses ModuleNFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  MDeXML: IXMLTEvento;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLMDe_0100 }

function TEveGeraXMLMDe_0100.CriarDocumentoMDe(const FatID, FatNum,
  Empresa: Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
  const CNPJ, CPF, chNFe: String; const dhEvento: TDateTime;
  const TZD_UTC: Double; const tpEvento, nSeqEvento: Integer; verEvento: Double;
  const versao: Double; descEvento, xJust: String; var XMLAssinado: String;
  const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
(* Criando o Documento XML e Gravando cabeçalho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  MDeXML := GetEvento(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)

  MDeXML.versao := verEventosMDe_Layout;
  MDeXML.InfEvento.Id := Id;
  MDeXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
  MDeXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
  if Tipo = 0 then
    MDeXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ)
  else
    MDeXML.InfEvento.CPF := Geral.SoNumero_TT(CPF);
  MDeXML.InfEvento.ChNFe := Geral.SoNumero_TT(chNFe);
  MDeXML.InfEvento.DhEvento := dmkPF.FDT_NFe_UTC(dhEvento, TZD_UTC);
  MDeXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
  MDeXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
  MDeXML.InfEvento.VerEvento := verEventosMDe_Evento;
  MDeXML.InfEvento.DetEvento.Versao := verEventosMDe_ManDes;
  MDeXML.InfEvento.DetEvento.DescEvento := descEvento;
  if xJust <> '' then
    MDeXML.InfEvento.DetEvento.XJust := xJust;
  //

  Texto := MDeXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, '<Evento', '<evento', [rfReplaceAll] );
  Texto := StringReplace(Texto, '</Evento', '</evento', [rfReplaceAll] );

  // Assinar!
  NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
    Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);
  arqXML := nil;
end;

end.
