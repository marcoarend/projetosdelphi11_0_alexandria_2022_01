unit NFeEveRMDe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkRadioGroup,
  dmkDBLookupComboBox, mySQLDbTables, dmkEditCB, UnDmkEnums, UnXXe_PF;

type
  TFmNFeEveRMDe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    EdCodInfoEmit: TdmkEdit;
    Label4: TLabel;
    EdEMIT_CNPJ_CPF_TXT: TdmkEdit;
    Edemit_xNome: TdmkEdit;
    RGcSitConf: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdJust: TdmkEditCB;
    QrJust: TmySQLQuery;
    QrJustCodigo: TIntegerField;
    QrJustNome: TWideStringField;
    QrJustCodUsu: TIntegerField;
    QrJustAplicacao: TIntegerField;
    DsJust: TDataSource;
    CBJust: TdmkDBLookupComboBox;
    Memo1: TMemo;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGcSitConfClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeEveRMDe: TFmNFeEveRMDe;

implementation

uses UnMyObjects, Module, ModuleNFe_0000, NFeEveRCab, ModuleGeral, UMySQLModule,
NFeXMLGerencia, EveGeraXMLMDe_0100, NFe_PF;

{$R *.DFM}

procedure TFmNFeEveRMDe.BtOKClick(Sender: TObject);
var
  Destino, Dir, xJust: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, cSitConf, nJust, tpEvento: Integer;
  NeedJust: Boolean;
  // XML
  XML_Eve: String;
begin
  if not DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_MDE_XML, Dir, True) then
    Exit;
  cSitConf := RGcSitConf.ItemIndex;
  if MyObjects.FIC(cSitConf < 1, RGcSitConf, 'Informe o c�digo do evento!') then
    Exit;
  tpEvento := NFeXMLGeren.Obtem_DeManifestacao_de_cSitConf_tpEvento(cSitConf);
  if MyObjects.FIC(tpEvento = 0, RGcSitConf,
  'N�o foi poss�vel definir o tpEvento a partir do c�digo do evento!') then
    Exit;

  case tpEvento of
    NFe_CodEventoMDeConfirmacao:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeConfirmacao,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Confirma��o criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeCiencia:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeCiencia,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Ci�ncia criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeDesconhece:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeDesconhece,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Desconhecimento criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeNaoRealizou:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeNaoRealizou,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo N�o realizada criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
  end;

  if ImgTipo.SQLType = stIns then
  begin
    if not FmNFeEveRCab.IncluiNFeEveRCab(cSitConf, eveMDe, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevermde', '', 0);
  end else begin
    FatID    := FmNFeEveRCab.QrNFeEveRCabFatID.Value;
    FatNum   := FmNFeEveRCab.QrNFeEveRCabFatNum.Value;
    Empresa  := FmNFeEveRCab.QrNFeEveRCabEmpresa.Value;
    Controle := FmNFeEveRCab.QrNFeEveRCabControle.Value;
    Conta    := FmNFeEveRCab.QrNFeEveRMDeConta.Value;
  end;
  nJust := EdJust.ValueVariant;
  if nJust <> 0 then
    xJust := QrJustNome.Value
  else
    xJust := '';
  //
  xJust :=  Trim(XXe_PF.ValidaTexto_XML(xJust, 'eve.mde.xjust', 'eve.mde.xjust'));
  NeedJust := cSitConf = NFe_CodEventoSitConfMDeNaoRealizada;
  if NeedJust and ((nJust = 0) or (xJust = '')) then
  begin
    Geral.MB_Aviso('O "c�digo do evento" selecionado exige uma justificativa!');
    Exit;
  end else
  if not NeedJust and ((nJust <> 0) or (xJust <> '')) then
  begin
    Geral.MB_Aviso('O "c�digo do evento" selecionado n�o permite uma justificativa!');
    Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeevermde', False, [
  'cSitConf', 'tpEvento', 'nJust',
  'xJust'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  cSitConf, tpEvento, nJust,
  xJust], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnEveGeraXMLMDe_0100.CriarDocumentoMDe(FatID, FatNum, Empresa,
      FmNFeEveRCab.QrERCId.Value, FmNFeEveRCab.QrERCcOrgao.Value,
      FmNFeEveRCab.QrERCtpAmb.Value, FmNFeEveRCab.QrERCTipoEnt.Value,
      FmNFeEveRCab.QrERCCNPJ.Value, FmNFeEveRCab.QrERCCPF.Value,
      FmNFeEveRCab.QrERCchNFe.Value, FmNFeEveRCab.QrERCdhEvento.Value,
      FmNFeEveRCab.QrERCTZD_UTC.Value, FmNFeEveRCab.QrERCtpEvento.Value,
      FmNFeEveRCab.QrERCnSeqEvento.Value, FmNFeEveRCab.QrERCverEvento.Value,
      FmNFeEveRCab.QrERCversao.Value, FmNFeEveRCab.QrERCdescEvento.Value,
      xJust, XML_Eve, LaAviso1, LaAviso2) then
      begin
        Destino := DmNFe_0000.SalvaXML(NFE_EXT_EVE_ENV_MDE_XML,
        FmNFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
        if FileExists(Destino) then
        begin
            Status := DmNFe_0000.stepNFeAssinada();
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
            'XML_Eve', 'Status'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            Geral.WideStringToSQLString(XML_Eve), Status], [
            FatID, FatNum, Empresa, Controle], True) then
              FmNFeEveRCab.ReopenNFeEveRCab(Controle);
        end;
      end;
    //
    FmNFeEveRCab.ReopenNFeEveRMDe(0);
    Close;
  end;
end;

(*Backup 160506 => Erro na verifica��o de Manifesta��o de destinat�rio
procedure TFmNFeEveRMDe.BtOKClick(Sender: TObject);
var
  Destino, Dir, xJust: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, cSitConf, nJust, tpEvento: Integer;
  NeedJust: Boolean;
  // XML
  XML_Eve: String;
begin
  if not DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_MDE_XML, Dir, True) then
    Exit;
  cSitConf := RGcSitConf.ItemIndex;
  if MyObjects.FIC(cSitConf < 1, RGcSitConf, 'Informe o c�digo do evento!') then
    Exit;
  tpEvento := NFeXMLGeren.Obtem_DeManifestacao_de_cSitConf_tpEvento(cSitConf);
  if MyObjects.FIC(tpEvento = 0, RGcSitConf,
  'N�o foi poss�vel definir o tpEvento a partir do c�digo do evento!') then
    Exit;



  if ImgTipo.SQLType = stIns then
  begin
    if not FmNFeEveRCab.IncluiNFeEveRCab(cSitConf, eveMDe, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevermde', '', 0);
  end else begin
    FatID    := FmNFeEveRCab.QrNFeEveRCabFatID.Value;
    FatNum   := FmNFeEveRCab.QrNFeEveRCabFatNum.Value;
    Empresa  := FmNFeEveRCab.QrNFeEveRCabEmpresa.Value;
    Controle := FmNFeEveRCab.QrNFeEveRCabControle.Value;
    Conta    := FmNFeEveRCab.QrNFeEveRMDeConta.Value;
  end;
  nJust := EdJust.ValueVariant;
  if nJust <> 0 then
    xJust := QrJustNome.Value
  else
    xJust := '';
  //
  xJust :=  Trim(XXe_PF.ValidaTexto_XML(xJust, 'eve.mde.xjust', 'eve.mde.xjust'));
  NeedJust := cSitConf = NFe_CodEventoSitConfMDeNaoRealizada;
  if NeedJust and ((nJust = 0) or (xJust = '')) then
  begin
    Geral.MB_Aviso('O "c�digo do evento" selecionado exige uma justificativa!');
    Exit;
  end else
  if not NeedJust and ((nJust <> 0) or (xJust <> '')) then
  begin
    Geral.MB_Aviso('O "c�digo do evento" selecionado n�o permite uma justificativa!');
    Exit;
  end;
  //
  case tpEvento of
    NFe_CodEventoMDeConfirmacao:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeConfirmacao,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Confirma��o criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeCiencia:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeCiencia,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Ci�ncia criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeDesconhece:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeDesconhece,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo Desconhecimento criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
    NFe_CodEventoMDeNaoRealizou:
    begin
      if (not FmNFeEveRCab.PermiteCriacaoDeEvento(NFe_CodEventoMDeNaoRealizou,
        FatID, FatNum, Empresa)) and (ImgTipo.SQLType = stIns) then
      begin
        Geral.MB_Aviso('Voc� deve primeiro enviar a manifesta��o do tipo N�o realizada criada anteriormente ao FISCO antes de gerar uma nova!');
        Exit;
      end;
    end;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeevermde', False, [
  'cSitConf', 'tpEvento', 'nJust',
  'xJust'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  cSitConf, tpEvento, nJust,
  xJust], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnEveGeraXMLMDe_0100.CriarDocumentoMDe(FatID, FatNum, Empresa,
      FmNFeEveRCab.QrERCId.Value, FmNFeEveRCab.QrERCcOrgao.Value,
      FmNFeEveRCab.QrERCtpAmb.Value, FmNFeEveRCab.QrERCTipoEnt.Value,
      FmNFeEveRCab.QrERCCNPJ.Value, FmNFeEveRCab.QrERCCPF.Value,
      FmNFeEveRCab.QrERCchNFe.Value, FmNFeEveRCab.QrERCdhEvento.Value,
      FmNFeEveRCab.QrERCTZD_UTC.Value, FmNFeEveRCab.QrERCtpEvento.Value,
      FmNFeEveRCab.QrERCnSeqEvento.Value, FmNFeEveRCab.QrERCverEvento.Value,
      FmNFeEveRCab.QrERCversao.Value, FmNFeEveRCab.QrERCdescEvento.Value,
      xJust, XML_Eve, LaAviso1, LaAviso2) then
      begin
        Destino := DmNFe_0000.SalvaXML(NFE_EXT_EVE_ENV_MDE_XML,
        FmNFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
        if FileExists(Destino) then
        begin
            Status := DmNFe_0000.stepNFeAssinada();
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
            'XML_Eve', 'Status'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            Geral.WideStringToSQLString(XML_Eve), Status], [
            FatID, FatNum, Empresa, Controle], True) then
              FmNFeEveRCab.ReopenNFeEveRCab(Controle);
        end;
      end;
    //
    FmNFeEveRCab.ReopenNFeEveRMDe(0);
    Close;
  end;
end;
*)

procedure TFmNFeEveRMDe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveRMDe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveRMDe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrJust, Dmod.MyDB);
end;

procedure TFmNFeEveRMDe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRMDe.RGcSitConfClick(Sender: TObject);
begin
  case RGcSitConf.ItemIndex of
    0: MyObjects.Informa2(LaAviso1, LaAviso2, False,
       'Selecione um "C�digo do evento v�lido!"');
    4: MyObjects.Informa2(LaAviso1, LaAviso2, False,
       'ATEN��O: A ci�ncia da opera��o n�o � definitiva!!!');
    else
       MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmNFeEveRMDe.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnNFe_PF.MostraFormNFeJust(EdJust.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdJust, CBJust, QrJust, VAR_CADASTRO);
    //
    EdJust.SetFocus;
  end;
end;

end.
