object FmNFeEveRMDe: TFmNFeEveRMDe
  Left = 339
  Top = 185
  Caption = 'NFe-EVENT-004 :: Manifesta'#231#227'o do Destinat'#225'rio'
  ClientHeight = 440
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 354
        Height = 32
        Caption = 'Manifesta'#231#227'o do Destinat'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 354
        Height = 32
        Caption = 'Manifesta'#231#227'o do Destinat'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 354
        Height = 32
        Caption = 'Manifesta'#231#227'o do Destinat'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 278
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 278
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 213
        Align = alTop
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 808
          Height = 118
          Align = alTop
          Caption = ' Dados da NF-e: '
          Enabled = False
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 56
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 8
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
            end
            object Label2: TLabel
              Left = 48
              Top = 8
              Width = 32
              Height = 13
              Caption = 'N'#186' NF:'
            end
            object Label4: TLabel
              Left = 132
              Top = 8
              Width = 184
              Height = 13
              Caption = 'Emitente (C'#243'digo, CNPJ/CPF e Nome):'
            end
            object Edide_serie: TdmkEdit
              Left = 8
              Top = 24
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object Edide_nNF: TdmkEdit
              Left = 48
              Top = 24
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdCodInfoEmit: TdmkEdit
              Left = 132
              Top = 24
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdEMIT_CNPJ_CPF_TXT: TdmkEdit
              Left = 188
              Top = 24
              Width = 112
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Edemit_xNome: TdmkEdit
              Left = 300
              Top = 24
              Width = 497
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Memo1: TMemo
            Left = 2
            Top = 71
            Width = 804
            Height = 45
            Align = alClient
            ReadOnly = True
            TabOrder = 1
          end
        end
        object RGcSitConf: TdmkRadioGroup
          Left = 12
          Top = 136
          Width = 789
          Height = 69
          Caption = ' C'#243'digo do evento: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '0 = Sem manifesta'#231#227'o do destinat'#225'rio'
            '1 = Confirmo a opera'#231#227'o'
            '2 = Desconhe'#231'o a opera'#231#227'o'
            '3 = A opera'#231#227'o n'#227'o foi realizada'
            '4 = Apenas estou ciente da opera'#231#227'o')
          TabOrder = 1
          OnClick = RGcSitConfClick
          UpdType = utYes
          OldValor = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 213
        Width = 812
        Height = 65
        Align = alClient
        Caption = ' Campo obrigat'#243'rio para OPERA'#199#195'O N'#195'O REALIZADA: '
        TabOrder = 1
        object PnJustificativa: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 169
            Height = 13
            Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
          end
          object SpeedButton1: TSpeedButton
            Left = 775
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdJust: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBJust
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBJust: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 712
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsJust
            TabOrder = 1
            dmkEditCB = EdJust
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 326
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 226
        Height = 16
        Caption = 'Selecione um "C'#243'digo do evento v'#225'lido!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 226
        Height = 16
        Caption = 'Selecione um "C'#243'digo do evento v'#225'lido!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 370
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrJust: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE 4 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 484
    Top = 12
    object QrJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsJust: TDataSource
    DataSet = QrJust
    Left = 512
    Top = 12
  end
end
