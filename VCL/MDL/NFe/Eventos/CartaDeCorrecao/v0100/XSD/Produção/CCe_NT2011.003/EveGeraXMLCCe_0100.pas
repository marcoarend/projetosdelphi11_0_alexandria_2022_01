unit EveGeraXMLCCe_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, (*20230320JwaWinCrypt,*)
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  NFeXMLGerencia,
  // Carta de corre��o
  CCe_v100;

const
 { TODO : Colocar aqui os stat da CCe }
{
  NFe_AllModelos      = '55';
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
}
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/nfe"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosCCe_Layout        = '1.00'; // Evento - Carta de corre��o - Vers�o do layout
  verEventosCCe_Evento        = '1.00'; // Evento - Carta de corre��o - Vers�o do Evento
  verEventosCCe_Carta         = '1.00'; // Evento - Carta de corre��o - Vers�o da Carta de corre��o

type
  TEveGeraXMLCCe_0100 = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoCCe(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chNFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, xCorrecao, xCondUso: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnEveGeraXMLCCe_0100: TEveGeraXMLCCe_0100;

implementation

uses ModuleNFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  CCeXML: IXMLTEvento;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLCCe_0100 }

function TEveGeraXMLCCe_0100.CriarDocumentoCCe(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chNFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, xCorrecao, xCondUso: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  CCeXML := Getevento(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)

  CCeXML.versao := verEventosCCe_Layout;
  CCeXML.InfEvento.Id := Id;
  CCeXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
  CCeXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
  if Tipo = 0 then
    CCeXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ)
  else
    CCeXML.InfEvento.CPF := Geral.SoNumero_TT(CPF);
  CCeXML.InfEvento.ChNFe := Geral.SoNumero_TT(chNFe);
  CCeXML.InfEvento.DhEvento := dmkPF.FDT_NFe_UTC(dhEvento, TZD_UTC); 
  CCeXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
  CCeXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
  CCeXML.InfEvento.VerEvento := verEventosCCe_Evento;
  CCeXML.InfEvento.DetEvento.Versao := verEventosCCe_Carta;
  CCeXML.InfEvento.DetEvento.DescEvento := descEvento;
  CCeXML.InfEvento.DetEvento.XCorrecao := xCorrecao;
  CCeXML.InfEvento.DetEvento.XCondUso := xCondUso;
  //

  Texto := CCeXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  // Assinar!
  NumeroSerial := DmNFe_0000.QrFilialNFeSerNum.Value;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
    Result := NFeXMLGeren.AssinarMSXML(Texto, Cert, XMLAssinado);
  arqXML := nil;
end;

end.
