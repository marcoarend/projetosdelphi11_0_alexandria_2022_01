object FmNFeEveRLoEAdd: TFmNFeEveRLoEAdd
  Left = 339
  Top = 185
  Caption = 'NFe-EVENT-102 :: Adi'#231#227'o de Evento a Lote'
  ClientHeight = 406
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 247
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 1006
      Height = 245
      Align = alClient
      DataSource = DsNFeEveRCab
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'chNFe'
          Title.Caption = 'Chave da NFe'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'chNFe_NF_SER'
          Title.Caption = 'S'#233'rie'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'chNFe_NF_NUM'
          Title.Caption = 'N'#186' NFe'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'tpEvento'
          Title.Caption = 'Evento'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'descEvento'
          Title.Caption = 'Descri'#231#227'o do Evento'
          Width = 321
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dhEvento'
          Title.Caption = 'Data / hota evento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'tpAmb'
          Title.Caption = 'Amb'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 297
        Height = 32
        Caption = 'Adi'#231#227'o de Evento a Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 32
        Caption = 'Adi'#231#227'o de Evento a Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 297
        Height = 32
        Caption = 'Adi'#231#227'o de Evento a Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 343
    Width = 1008
    Height = 63
    Align = alBottom
    TabOrder = 2
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 17
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel2: TPanel
      Left = 898
      Top = 15
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrNFeEveRCab: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeEveRCabCalcFields
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento'
      'FROM nfeevercab'
      'WHERE Empresa=:P0'
      'AND Status=:P1')
    Left = 8
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabchNFe_NF_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object QrNFeEveRCabchNFe_NF_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
  end
  object DsNFeEveRCab: TDataSource
    DataSet = QrNFeEveRCab
    Left = 36
    Top = 12
  end
end
