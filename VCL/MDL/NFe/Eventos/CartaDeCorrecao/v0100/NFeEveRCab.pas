unit NFeEveRCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Menus, NFeXMLGerencia, DmkDAC_PF, EveGeraXMLCCe_0100,
  EveGeraXMLCan_0100, frxClass, frxDBSet, UnDmkProcFunc, frxBarcode, UnDmkEnums,
  UnGrl_Consts;

type
  TFmNFeEveRcab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtCCe: TBitBtn;
    LaTitulo1C: TLabel;
    QrA: TmySQLQuery;
    QrANO_Cli: TWideStringField;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAId: TWideStringField;
    QrAide_natOp: TWideStringField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAnProt: TWideStringField;
    QrAxMotivo: TWideStringField;
    QrAdhRecbto: TWideStringField;
    QrAIDCtrl: TIntegerField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAversao: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAide_tpEmis: TSmallintField;
    QrANOME_tpEmis: TWideStringField;
    QrANOME_tpNF: TWideStringField;
    QrAinfCanc_xJust: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    DsA: TDataSource;
    DBGrid1: TDBGrid;
    PMCCe: TPopupMenu;
    Crianovacorreo1: TMenuItem;
    Alteracorreo1: TMenuItem;
    Excluicorreo1: TMenuItem;
    QrNFeEveRCab: TmySQLQuery;
    DsNFeEveRCab: TDataSource;
    QrNFeEveRCabFatID: TIntegerField;
    QrNFeEveRCabFatNum: TIntegerField;
    QrNFeEveRCabEmpresa: TIntegerField;
    QrNFeEveRCabControle: TIntegerField;
    QrNFeEveRCabEventoLote: TIntegerField;
    QrNFeEveRCabId: TWideStringField;
    QrNFeEveRCabcOrgao: TSmallintField;
    QrNFeEveRCabtpAmb: TSmallintField;
    QrNFeEveRCabTipoEnt: TSmallintField;
    QrNFeEveRCabCNPJ: TWideStringField;
    QrNFeEveRCabCPF: TWideStringField;
    QrNFeEveRCabchNFe: TWideStringField;
    QrNFeEveRCabdhEvento: TDateTimeField;
    QrNFeEveRCabverEvento: TFloatField;
    QrNFeEveRCabtpEvento: TIntegerField;
    QrNFeEveRCabnSeqEvento: TSmallintField;
    QrNFeEveRCabversao: TFloatField;
    QrNFeEveRCabdescEvento: TWideStringField;
    QrNFeEveRCabLk: TIntegerField;
    QrNFeEveRCabDataCad: TDateField;
    QrNFeEveRCabDataAlt: TDateField;
    QrNFeEveRCabUserCad: TIntegerField;
    QrNFeEveRCabUserAlt: TIntegerField;
    QrNFeEveRCabAlterWeb: TSmallintField;
    QrNFeEveRCabAtivo: TSmallintField;
    QrNFeEveRCCe: TmySQLQuery;
    DsNFeEveRCCe: TDataSource;
    QrNFeEveRCCeFatID: TIntegerField;
    QrNFeEveRCCeFatNum: TIntegerField;
    QrNFeEveRCCeEmpresa: TIntegerField;
    QrNFeEveRCCeControle: TIntegerField;
    QrNFeEveRCCeConta: TIntegerField;
    QrNFeEveRCCexCorrecao: TWideMemoField;
    QrNFeEveRCCenCondUso: TSmallintField;
    QrNFeEveRCCeLk: TIntegerField;
    QrNFeEveRCCeDataCad: TDateField;
    QrNFeEveRCCeDataAlt: TDateField;
    QrNFeEveRCCeUserCad: TIntegerField;
    QrNFeEveRCCeUserAlt: TIntegerField;
    QrNFeEveRCCeAlterWeb: TSmallintField;
    QrNFeEveRCCeAtivo: TSmallintField;
    DBGrid2: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid4: TDBGrid;
    QrERC: TmySQLQuery;
    QrERCFatID: TIntegerField;
    QrERCFatNum: TIntegerField;
    QrERCEmpresa: TIntegerField;
    QrERCControle: TIntegerField;
    QrERCEventoLote: TIntegerField;
    QrERCId: TWideStringField;
    QrERCcOrgao: TSmallintField;
    QrERCtpAmb: TSmallintField;
    QrERCTipoEnt: TSmallintField;
    QrERCCNPJ: TWideStringField;
    QrERCCPF: TWideStringField;
    QrERCchNFe: TWideStringField;
    QrERCdhEvento: TDateTimeField;
    QrERCverEvento: TFloatField;
    QrERCtpEvento: TIntegerField;
    QrERCnSeqEvento: TSmallintField;
    QrERCversao: TFloatField;
    QrERCdescEvento: TWideStringField;
    QrNFeEveRCabTZD_UTC: TFloatField;
    QrERCTZD_UTC: TFloatField;
    DBMemo1: TDBMemo;
    Splitter1: TSplitter;
    QrNFeEveRCabtpAmb_TXT: TWideStringField;
    QrNFeEveRCabStatus: TIntegerField;
    BtCan: TBitBtn;
    PMCan: TPopupMenu;
    IncluicancelamentodaNFe1: TMenuItem;
    ExcluicancelamentodaNFe1: TMenuItem;
    QrNFeEveRCan: TmySQLQuery;
    DsNFeEveRCan: TDataSource;
    QrNFeEveRCanFatID: TIntegerField;
    QrNFeEveRCanFatNum: TIntegerField;
    QrNFeEveRCanEmpresa: TIntegerField;
    QrNFeEveRCanControle: TIntegerField;
    QrNFeEveRCanConta: TIntegerField;
    QrNFeEveRCannProt: TLargeintField;
    QrNFeEveRCannJust: TIntegerField;
    QrNFeEveRCanxJust: TWideStringField;
    QrNFeEveRCanLk: TIntegerField;
    QrAcStat: TFloatField;
    N1: TMenuItem;
    Imprime1: TMenuItem;
    frxEveCCe: TfrxReport;
    frxDsA: TfrxDBDataset;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_CNPJ_CPF_TXT: TWideStringField;
    QrAID_TXT: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAEMIT_IEST_TXT: TWideStringField;
    QrADEST_ENDERECO: TWideStringField;
    QrADEST_CEP_TXT: TWideStringField;
    QrADEST_FONE_TXT: TWideStringField;
    QrADEST_IE_TXT: TWideStringField;
    QrADEST_CNPJ_CPF_TXT: TWideStringField;
    QrAMODFRETE_TXT: TWideStringField;
    QrADEST_XMUN_TXT: TWideStringField;
    QrAdest_xMun: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrATRANSPORTA_IE_TXT: TWideStringField;
    QrAtransporta_CNPJ: TWideStringField;
    QrAtransporta_CPF: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    QrAtransporta_IE: TWideStringField;
    QrAtransporta_UF: TWideStringField;
    QrAide_tpAmb: TSmallintField;
    QrAide_DSaiEnt_Txt: TWideStringField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAide_DSaiEnt: TDateField;
    QrAModFrete: TSmallintField;
    QrAemit_xNome: TWideStringField;
    frxDsNFeEveRCCe: TfrxDBDataset;
    frxDsNFeEveRCab: TfrxDBDataset;
    QrAdest_xBairro: TWideStringField;
    BtMDe: TBitBtn;
    PMMDe: TPopupMenu;
    Incluimanifestaodedestinatrio1: TMenuItem;
    Excluimanifestaodedestinatrio1: TMenuItem;
    QrAemit_CPF: TWideStringField;
    QrNFeEveRMDe: TMySQLQuery;
    DsNFeEveRMDe: TDataSource;
    QrACodInfoEmit: TIntegerField;
    QrNFeEveRMDeFatID: TIntegerField;
    QrNFeEveRMDeFatNum: TIntegerField;
    QrNFeEveRMDeEmpresa: TIntegerField;
    QrNFeEveRMDeControle: TIntegerField;
    QrNFeEveRMDeConta: TIntegerField;
    QrNFeEveRMDecSitConf: TSmallintField;
    QrNFeEveRMDetpEvento: TIntegerField;
    QrNFeEveRMDenJust: TIntegerField;
    QrNFeEveRMDexJust: TWideStringField;
    TabSheet3: TTabSheet;
    QrACodInfoDest: TIntegerField;
    QrNFeEveRCabXML_Eve: TWideMemoField;
    QrNFeEveRCabXML_retEve: TWideMemoField;
    QrNFeEveRCabret_versao: TFloatField;
    QrNFeEveRCabret_Id: TWideStringField;
    QrNFeEveRCabret_tpAmb: TSmallintField;
    QrNFeEveRCabret_verAplic: TWideStringField;
    QrNFeEveRCabret_cOrgao: TSmallintField;
    QrNFeEveRCabret_cStat: TIntegerField;
    QrNFeEveRCabret_xMotivo: TWideStringField;
    QrNFeEveRCabret_chNFe: TWideStringField;
    QrNFeEveRCabret_tpEvento: TIntegerField;
    QrNFeEveRCabret_xEvento: TWideStringField;
    QrNFeEveRCabret_nSeqEvento: TIntegerField;
    QrNFeEveRCabret_CNPJDest: TWideStringField;
    QrNFeEveRCabret_CPFDest: TWideStringField;
    QrNFeEveRCabret_emailDest: TWideStringField;
    QrNFeEveRCabret_dhRegEvento: TDateTimeField;
    QrNFeEveRCabret_TZD_UTC: TFloatField;
    QrNFeEveRCabret_nProt: TWideStringField;
    BtLote: TBitBtn;
    QrAide_mod: TSmallintField;
    AtorinteressadoTransportadora1: TMenuItem;
    BtEPEC: TBitBtn;
    PmEPEC: TPopupMenu;
    IncluinovoEPEC1: TMenuItem;
    TabSheet4: TTabSheet;
    DsNFeEveREPEC: TDataSource;
    QrNFeEveREPEC: TMySQLQuery;
    QrNFeEveREPECFatID: TIntegerField;
    QrNFeEveREPECFatNum: TIntegerField;
    QrNFeEveREPECEmpresa: TIntegerField;
    QrNFeEveREPECControle: TIntegerField;
    QrNFeEveREPECConta: TIntegerField;
    QrNFeEveREPECcOrgaoAutor: TSmallintField;
    QrNFeEveREPECtpAutor: TSmallintField;
    QrNFeEveREPECverAplic: TWideStringField;
    QrNFeEveREPECdhEmi: TDateTimeField;
    QrNFeEveREPECdhEmiTZD: TFloatField;
    QrNFeEveREPECtpNF: TSmallintField;
    QrNFeEveREPECIE: TWideStringField;
    QrNFeEveREPECdest_UF: TWideStringField;
    QrNFeEveREPECdest_CNPJ: TWideStringField;
    QrNFeEveREPECdest_CPF: TWideStringField;
    QrNFeEveREPECdest_idEstrangeiro: TWideStringField;
    QrNFeEveREPECdest_IE: TWideStringField;
    QrNFeEveREPECvNF: TFloatField;
    QrNFeEveREPECvICMS: TFloatField;
    QrNFeEveREPECvST: TFloatField;
    QrAide_verProc: TWideStringField;
    QrAide_hEmi: TTimeField;
    QrAide_dhEmiTZD: TFloatField;
    QrAdest_idEstrangeiro: TWideStringField;
    AlteraEPEC1: TMenuItem;
    ExcluiEPEC1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtCCeClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Crianovacorreo1Click(Sender: TObject);
    procedure QrAAfterOpen(DataSet: TDataSet);
    procedure QrAAfterScroll(DataSet: TDataSet);
    procedure QrABeforeClose(DataSet: TDataSet);
    procedure QrNFeEveRCabBeforeClose(DataSet: TDataSet);
    procedure QrNFeEveRCabAfterScroll(DataSet: TDataSet);
    procedure QrNFeEveRCabCalcFields(DataSet: TDataSet);
    procedure PMCCePopup(Sender: TObject);
    procedure Alteracorreo1Click(Sender: TObject);
    procedure BtCanClick(Sender: TObject);
    procedure IncluicancelamentodaNFe1Click(Sender: TObject);
    procedure ExcluicancelamentodaNFe1Click(Sender: TObject);
    procedure Excluicorreo1Click(Sender: TObject);
    procedure PMCanPopup(Sender: TObject);
    procedure Imprime1Click(Sender: TObject);
    procedure frxEveCCeGetValue(const VarName: string; var Value: Variant);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure BtMDeClick(Sender: TObject);
    procedure Incluimanifestaodedestinatrio1Click(Sender: TObject);
    procedure Excluimanifestaodedestinatrio1Click(Sender: TObject);
    procedure PMMDePopup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLoteClick(Sender: TObject);
    procedure AtorinteressadoTransportadora1Click(Sender: TObject);
    procedure BtEPECClick(Sender: TObject);
    procedure IncluinovoEPEC1Click(Sender: TObject);
    procedure ExcluiEPEC1Click(Sender: TObject);
    procedure AlteraEPEC1Click(Sender: TObject);
    procedure PmEPECPopup(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormCartaCorrecao(SQLType: TSQLType);
    procedure MostraFormCancelamentoNFe(SQLType: TSQLType);
    procedure MostraFormManifestacaoDestinatario(SQLType: TSQLType);
    procedure MostraFormEmissaoContingenciaEPEC(SQLType: TSQLType);
  public
    { Public declarations }
    function  PermiteCriacaoDeEvento(tpEvento, FatID, FatNum, Empresa: Integer): Boolean;
    procedure ReopenNFeCabA(FatID, FatNum, Empresa: Integer);
    function  IncluiNFeEveRCab(const EvePreDef: Integer; const EventoNFe:
              TEventosNFe; var FatID, FatNum, Empresa, Controle: Integer):
              Boolean;
    procedure AbreDadosParaXML(Controle: Integer);
    procedure ReopenNFeEveRCab(Controle: Integer);
    procedure ReopenNFeEveRCCe(Conta: Integer);
    procedure ReopenNFeEveRCan(Conta: Integer);
    procedure ReopenNFeEveRMDe(Conta: Integer);
    procedure ReopenNFeEveREPEC(Conta: Integer);
  end;

  var
  FmNFeEveRcab: TFmNFeEveRcab;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, NFeEveRCCe, ModuleNFe_0000,
  NFe_Tabs, ModuleGeral,
  //{$IFNDef semNFe_v 0 2 0 0} NFeGeraXML_0200, {$EndIF}
  NFeEveRCan, NFeEveRMDe, NFeEveREPEC, NFe_PF;

{$R *.DFM}

procedure TFmNFeEveRcab.AbreDadosParaXML(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrERC, Dmod.MyDB, [
  'SELECT * FROM nfeevercab ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TFmNFeEveRcab.Alteracorreo1Click(Sender: TObject);
begin
  MostraFormCartaCorrecao(stUpd);
end;

procedure TFmNFeEveRcab.AlteraEPEC1Click(Sender: TObject);
begin
  MostraFormEmissaoContingenciaEPEC(stUpd);
end;

procedure TFmNFeEveRcab.AtorinteressadoTransportadora1Click(Sender: TObject);
begin
//
end;

procedure TFmNFeEveRcab.BtCanClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMCan, BtCan);
end;

procedure TFmNFeEveRcab.BtCCeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  if QrAStatus.Value = CO_100_NFeAutorizada then
    MyObjects.MostraPopUpDeBotao(PMCCe, BtCCe)
  else
    Geral.MB_Aviso(
    'A NFe n�o pode ter carta de corre��o porque seu status n�o � 100 - "Uso autorizado"');
end;

procedure TFmNFeEveRcab.BtEPECClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  //
  MyObjects.MostraPopUpDeBotao(PMEPEC, BtEPEC);
end;

procedure TFmNFeEveRcab.BtLoteClick(Sender: TObject);
var
  LoteEnv, Controle: Integer;
begin
  if (QrNFeEveRCab.State <> dsInactive) and (QrNFeEveRCab.RecordCount > 0) then
  begin
    Controle := QrNFeEveRCabControle.Value;
    LoteEnv  := QrNFeEveRCabEventoLote.Value;
  end else
  begin
    Controle := 0;
    LoteEnv  := 0;
  end;
  //
  UnNFe_PF.MostraFormNFeEveRLoE(LoteEnv);
  //
  if Controle <> 0 then
    ReopenNFeEveRCab(Controle);
end;

procedure TFmNFeEveRcab.BtMDeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMMDe, BtMDe);
end;

procedure TFmNFeEveRcab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveRcab.Crianovacorreo1Click(Sender: TObject);
begin
  MostraFormCartaCorrecao(stIns);
end;

procedure TFmNFeEveRcab.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFe_0000.ColoreStatusNFeEmDBGrid(DBGrid1, Rect, DataCol, Column,
    State, Trunc(QrAcStat.Value), QrAide_mod.Value);
end;

procedure TFmNFeEveRcab.ExcluicancelamentodaNFe1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do cancelamento da NFe?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCan, 'nfeevercan',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCab, 'nfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenNFeEveRCab(0);
  end;
end;

procedure TFmNFeEveRcab.Excluicorreo1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da carta de corre��o selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCCe, 'nfeevercce',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCab, 'nfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenNFeEveRCab(0);
  end;
end;

procedure TFmNFeEveRcab.ExcluiEPEC1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do EPEC da NFe?') = ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveREPEC, 'nfeeverepec',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCab, 'nfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenNFeEveRCab(0);
  end;
end;

procedure TFmNFeEveRcab.Excluimanifestaodedestinatrio1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da manifesta��o?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRMDe, 'nfeevermde',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrNfeEveRCab, 'nfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenNFeEveRCab(0);
  end;
end;

procedure TFmNFeEveRcab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFeEveRcab.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmNFeEveRcab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRcab.frxEveCCeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'LogoNFeExiste' then Value := FileExists(DModG.QrPrmsEmpNFePathLogoNF.Value) else
  if VarName = 'LogoNFePath' then Value := DModG.QrPrmsEmpNFePathLogoNF.Value else
  if VarName = 'NFeItsLin' then
  begin
    case DModG.QrPrmsEmpNFeNFeItsLin.Value of
      1: Value := 12.47244;    // 1 linha
      2: Value := 21.54331;    // 2 linhas
      else Value := 32.12598;  // 3 linhas
    end;
  end;
  if VarName = 'NFeFTRazao' then Value := DModG.QrPrmsEmpNFeNFeFTRazao.Value else
  if VarName = 'NFeFTEnder' then Value := DModG.QrPrmsEmpNFeNFeFTEnder.Value else
  if VarName = 'NFeFTFones' then Value := DModG.QrPrmsEmpNFeNFeFTFones.Value else
  if VarName = 'xCondUso' then Value := CO_CONDI_USO_CCE_COM[QrNFeEveRCCenCondUso.Value];
end;

procedure TFmNFeEveRcab.Imprime1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM nfeevercab ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    'AND tpEvento=' + Geral.FF0(NFe_CodEventoCCe), // 110110
    'ORDER BY nSeqEvento DESC ',
    '']);
    if Qry.FieldByName('Controle').AsInteger <> QrNFeEveRCabControle.Value then
    begin
      //135 - NFeXMLGerencia.Texto_StatusNFe(
      Geral.MB_Aviso('Impress�o de Carta de Corre��o cancelada!' + slineBreak +
      'Sequencia de carta criada inv�lida!' + sLineBreak +
      'Sequencia correta (�ltima): ' +
      Geral.FF0(Qry.FieldByName('Controle').AsInteger) + sLineBreak +
      'Sequencia  desta carta: ' + Geral.FF0(QrNFeEveRCabControle.Value));
      //
      Exit;
    end;
    if QrNFeEveRCabStatus.Value <> 135 then
    begin
      //135 - NFeXMLGerencia.Texto_StatusNFe(
      Geral.MB_Aviso('Impress�o de Carta de Corre��o cancelada!' + slineBreak +
      'Status inv�lido!' + sLineBreak +
      'Status correto: 135 = ' + NFeXMLGeren.Texto_StatusNFe(135, 0) + slineBreak +
      'Status atual: ' + Geral.FF0(QrNFeEveRCabStatus.Value) + ' = ' +
      NFeXMLGeren.Texto_StatusNFe(QrNFeEveRCabStatus.Value, 0));
      //
      Exit;
    end;
    DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
    MyObjects.frxDefineDataSets(
      frxEveCCe, [frxDsA, frxDsNFeEveRCab, frxDsNFeEveRCCe]);
    MyObjects.frxMostra(frxEveCCe, 'Carta de Corre��o Eletr�nica');
  finally
    Qry.Free;
  end;
end;

procedure TFmNFeEveRcab.IncluicancelamentodaNFe1Click(Sender: TObject);
begin
  if QrAStatus.Value = CO_100_NFeAutorizada then
    MostraFormCancelamentoNFe(stIns)
  else
    Geral.MB_Aviso(
    'A NFe n�o pode ser cancelada porque seu status n�o � 100 - "Uso autorizado"');
end;

procedure TFmNFeEveRcab.Incluimanifestaodedestinatrio1Click(Sender: TObject);
begin
  MostraFormManifestacaoDestinatario(stIns);
end;

function TFmNFeEveRcab.IncluiNFeEveRCab(const EvePreDef: Integer;
const EventoNFe: TEventosNFe; var
FatID, FatNum, Empresa, Controle: Integer): Boolean;
var
  Id, CNPJ, CPF, chNFe, dhEvento, descEvento, verEvento: String;
  //EventoLote,
  cOrgao, tpAmb, TipoEnt, tpEvento, nSeqEvento: Integer;
  TZD_UTC, versao: Double;
begin
  Result := False;
  FatID          := QrAFatID.Value;
  FatNum         := QrAFatNum.Value;
  Empresa        := QrAEmpresa.Value;

  DmNFe_0000.ReopenEmpresa(Empresa);
  if DmNFe_0000.QrEmpresaCodigo.Value <> Empresa then
    Exit;
  if not DmNFe_0000.ReopenOpcoesNFe(Empresa, True) then
    Exit;
  //
  if EventoNFe = eveMDe then
    cOrgao         := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(
                      DmNFe_0000.QrOpcoesNFeUF_MDeMDe.Value) // 91 = Ambiente Nacional
  else
  if EventoNFe = eveEPEC then
  begin
    cOrgao         := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(
                      DmNFe_0000.QrOpcoesNFeNFeUF_EPEC.Value); // 91 = Ambiente Nacional
  end else
    cOrgao         := Geral.IMV(DmNFe_0000.QrEmpresaDTB_UF.Value);
  if ((EventoNFe = eveMDe ) or (EventoNFe = eveEPEC)) and (cOrgao <> 91) then
    Geral.MB_Aviso(
    'Aten��o!!! cOrg�o difere de 91! Ver campo Web Service espec�fico nos par�metros da filial!');
  tpAmb          := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
  TipoEnt        := DmNFe_0000.QrEmpresaTipo.Value;
  if TipoEnt = 0 then
  begin
    CNPJ           := Geral.SoNumero_TT(DmNFe_0000.QrEmpresaCNPJ.Value);
    CPF            := '';
  end else
  begin
    CNPJ           := '';
    CPF            := Geral.SoNumero_TT(DmNFe_0000.QrEmpresaCPF.Value);
  end;
  chNFe          := QrAId.Value;

  dhEvento       := Geral.FDT(DmodG.ObtemAgora(), 109);
  //TZD_UTC        := DModG.UTC_ObtemDiferenca();
  //TZD_UTC        := DmodG.QrParamsEmpTZD_UTC.Value;
  TZD_UTC        := DmodG.ObtemHorarioTZD_JahCorrigido(Empresa);
  verEvento      := verEventosCCe_Evento;
  NFeXMLGeren.Evento_Obtem_DadosEspecificos(EvePreDef, EventoNFe, tpEvento, versao,
    descEvento);
  nSeqEvento     := DmNFe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento);
  Id             := NFeXMLGeren.Evento_MontaId(tpEvento, QrAId.Value, nSeqEvento);
  //
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevercab', '', 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeevercab', False, [
  (*'EventoLote',*) 'Id', 'cOrgao',
  'tpAmb', 'TipoEnt', 'CNPJ',
  'CPF', 'chNFe', 'dhEvento',
  'TZD_UTC', 'verEvento', 'tpEvento',
  'nSeqEvento', 'versao', 'descEvento'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  (*EventoLote,*) Id, cOrgao,
  tpAmb, TipoEnt, CNPJ,
  CPF, chNFe, dhEvento,
  TZD_UTC, verEvento, tpEvento,
  nSeqEvento, versao, descEvento], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    AbreDadosParaXML(Controle);
    Result := True;
  end;
end;

procedure TFmNFeEveRcab.IncluinovoEPEC1Click(Sender: TObject);
begin
  MostraFormEmissaoContingenciaEPEC(stIns);
end;

procedure TFmNFeEveRcab.MostraFormCancelamentoNFe(SQLType: TSQLType);
begin
  if (PermiteCriacaoDeEvento(NFe_CodEventoCan, QrAFatID.Value,
    QrAFatNum.Value, QrAEmpresa.Value) = False) and (SQLType = stIns) then
  begin
    Geral.MB_Aviso('Voc� deve primeiro enviar o cancelamento criado anteriormente ao FISCO antes de gerar um novo!');
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmNFeEveRCan, FmNFeEveRCan, afmoNegarComAviso) then
  begin
    FmNFeEveRCan.ImgTipo.SQLType := SQLType;
    FmNFeEveRCan.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmNFeEveRCan.Edide_nNF.ValueVariant := QrAide_nNF.Value;
    FmNFeEveRCan.EdnProt.Text := QrAnProt.Value;
    if SQLType = stUpd then
    begin
      // N�o tem upd
    end;
    FmNFeEveRCan.ShowModal;
    FmNFeEveRCan.Destroy;
  end;
end;

procedure TFmNFeEveRcab.MostraFormCartaCorrecao(SQLType: TSQLType);
begin
  if (PermiteCriacaoDeEvento(Nfe_CodEventoCCe, QrAFatID.Value,
    QrAFatNum.Value, QrAEmpresa.Value) = False) and (SQLType = stIns) then
  begin
    Geral.MB_Aviso('Voc� deve primeiro enviar a carta criada anteriormente ao FISCO antes de gerar uma nova!');
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmNFeEveRCCe, FmNFeEveRCCe, afmoNegarComAviso) then
  begin
    FmNFeEveRCCe.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmNFeEveRCCe.DesfazMsgMemo();
      FmNFeEveRCCe.RGnCondUso.ItemIndex := QrNFeEveRCCenCondUso.Value;
      FmNFeEveRCCe.MexCorrecao.Text := QrNFeEveRCCexCorrecao.Value;
    end;
    FmNFeEveRCCe.ShowModal;
    FmNFeEveRCCe.Destroy;
  end;
end;

procedure TFmNFeEveRcab.MostraFormEmissaoContingenciaEPEC(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmNFeEveREPEC, FmNFeEveREPEC, afmoNegarComAviso) then
  begin
    FmNFeEveREPEC.ImgTipo.SQLType := SQLType;
    FmNFeEveREPEC.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmNFeEveREPEC.Edide_nNF.ValueVariant := QrAide_nNF.Value;
    FmNFeEveREPEC.EdIE.ValueVariant := QrAemit_IE.Value;
    FmNFeEveREPEC.Edide_ID.ValueVariant := QrAId.Value;
    //
    FmNFeEveREPEC.EdcOrgaoAutor.ValueVariant := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmNFe_0000.QrFilialUF_WebServ.Value);
    FmNFeEveREPEC.EdtpAutor.ValueVariant := 1; // Empresa Emitente/PessoaFisica
    FmNFeEveREPEC.EdverAplic.ValueVariant := QrAide_verProc.Value;
    FmNFeEveREPEC.TPdEmi.Date := QrAide_dEmi.Value;
    FmNFeEveREPEC.EdhEmi.ValueVariant := QrAide_hEmi.Value;
    FmNFeEveREPEC.EdhEmiTZD.ValueVariant := QrAide_dhEmiTZD.Value;
    FmNFeEveREPEC.EdtpNF.ValueVariant := QrAide_tpNF.Value; // ver o que fazer!
    FmNFeEveREPEC.Eddest_CNPJ.ValueVariant := QrAdest_CNPJ.Value;
    FmNFeEveREPEC.Eddest_CPF.ValueVariant := QrAdest_CPF.Value;
    FmNFeEveREPEC.Eddest_idEstrangeiro.ValueVariant := QrAdest_idEstrangeiro.Value;
    FmNFeEveREPEC.Eddest_xNome.ValueVariant := QrAdest_xNome.Value;
    FmNFeEveREPEC.Eddest_IE.ValueVariant := QrAdest_IE.Value;
    FmNFeEveREPEC.Eddest_UF.ValueVariant := QrAdest_UF.Value;
    FmNFeEveREPEC.EdvNF.ValueVariant := QrAICMSTot_vNF.Value;
    FmNFeEveREPEC.EdvICMS.ValueVariant := QrAICMSTot_vICMS.Value;
    FmNFeEveREPEC.EdvST.ValueVariant := QrAICMSTot_vST.Value;
    if SQLType = stUpd then
    begin
      // Fazer aqui? Ins e Upd s�o iguais?
    end;
    FmNFeEveREPEC.ShowModal;
    FmNFeEveREPEC.Destroy;
  end;
end;

procedure TFmNFeEveRcab.MostraFormManifestacaoDestinatario(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmNFeEveRMDe, FmNFeEveRMDe, afmoNegarComAviso) then
  begin
    FmNFeEveRMDe.ImgTipo.SQLType := SQLType;
    FmNFeEveRMDe.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmNFeEveRMDe.Edide_nNF.ValueVariant := QrAide_nNF.Value;
    FmNFeEveRMDe.EdCodInfoEmit.ValueVariant := QrACodInfoEmit.Value;
    FmNFeEveRMDe.EdEMIT_CNPJ_CPF_TXT.Text := QrAEMIT_CNPJ_CPF_TXT.Value;
    FmNFeEveRMDe.Edemit_xNome.Text := QrAemit_xNome.Value;
    if SQLType = stUpd then
    begin
      // N�o tem upd
    end;
    FmNFeEveRMDe.ShowModal;
    FmNFeEveRMDe.Destroy;
  end;
end;

function TFmNFeEveRcab.PermiteCriacaoDeEvento(tpEvento, FatID, FatNum,
  Empresa: Integer): Boolean;
begin
  //True  => Permite
  //False => N�o permite
  //Descri��o: N�o permitir criar mais de um evento sem envi�-lo para o fisco
  //para n�o gerar erros no aplicativo e evitar cria��o de registros a toa
  //
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM nfeevercab ',
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa='+ Geral.FF0(Empresa),
    'AND tpEvento=' + Geral.FF0(tpEvento),
    // 2014-11-20
    //'AND Status <= 30 ',
    'AND (Status BETWEEN 10 AND 50 OR Status = 0) ',
    // FIM 2014-11-20
    '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := False;
end;

procedure TFmNFeEveRcab.PMCanPopup(Sender: TObject);
begin
  ExcluicancelamentodaNFe1.Enabled :=
    (QrAStatus.Value = CO_100_NFeAutorizada) and
    (QrNFeEveRCabtpEvento.Value = Nfe_CodEventoCan)
    and (QrNFeEveRCab.RecNo = 1) and
    ((QrNFeEveRCabStatus.Value <= 30) or
    (QrNFeEveRCabStatus.Value > 200));
end;

procedure TFmNFeEveRcab.PMCCePopup(Sender: TObject);
begin
  Alteracorreo1.Enabled :=
    (QrAStatus.Value = CO_100_NFeAutorizada) and
    (QrNFeEveRCabtpEvento.Value = Nfe_CodEventoCCe)
    and (QrNFeEveRCab.RecNo = 1) and
    ((QrNFeEveRCabStatus.Value <= 30) or
    (QrNFeEveRCabStatus.Value > 200));
  Excluicorreo1.Enabled := Alteracorreo1.Enabled;
  //
  Imprime1.Enabled := (QrNFeEveRCCe.State <> dsInactive) and (QrNFeEveRCCe.RecordCount > 0);
end;

procedure TFmNFeEveRcab.PmEPECPopup(Sender: TObject);
var
  Existe: Boolean;
begin
  Existe := (QrNFeEveREPEC.State <> dsInactive) and ( QrNFeEveREPEC.RecordCount > 0);
  //
  IncluinovoEPEC1.Enabled := Existe = False;
  AlteraEPEC1.Enabled := Existe;
  ExcluiEPEC1.Enabled := Existe;
end;

procedure TFmNFeEveRcab.PMMDePopup(Sender: TObject);
var
  EhMDe: Boolean;
begin
  EhMDe :=
    (QrNFeEveRCabtpEvento.Value = NFe_CodEventoMDeConfirmacao) or
    (QrNFeEveRCabtpEvento.Value = NFe_CodEventoMDeCiencia) or
    (QrNFeEveRCabtpEvento.Value = NFe_CodEventoMDeDesconhece) or
    (QrNFeEveRCabtpEvento.Value = NFe_CodEventoMDeNaoRealizou);
  //
  Excluimanifestaodedestinatrio1.Enabled := EhMde and
    ((QrNFeEveRCabStatus.Value <= 30) or
    (QrNFeEveRCabStatus.Value > 200));
end;

procedure TFmNFeEveRcab.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := DmNFe_0000.FormataID_NFe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + #13#10 +
    QrAemit_xBairro.Value + #13#10 + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + #13#10 +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value)) + '  ' + QrAemit_xPais.Value;

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  if QrAemit_CNPJ.Value <> '' then
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value)
  else
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CPF.Value);
  //
  //
  ////// DESTINAT�RIO
  //
  //
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrPrmsEmpNFeNFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + #13#10 +
      DModG.QrPrmsEmpNFeNFeShowURL.Value;
  if DModG.QrPrmsEmpNFeNFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
  //
  ////// TRANSPORTADORA - REBOQUE???
  //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'NF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
(* p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
*)
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
end;

procedure TFmNFeEveRcab.QrAAfterOpen(DataSet: TDataSet);
begin
  DmNFe_0000.ReopenEmpresa(QrAEmpresa.Value);
end;

procedure TFmNFeEveRcab.QrAAfterScroll(DataSet: TDataSet);
var
  Emit, Dest, Status100, Status0: Boolean;
begin
(*  ini 2023-06-28
  BtCCe.Enabled := QrAEmpresa.Value = QrACodInfoEmit.Value;
  BtCan.Enabled := QrAEmpresa.Value = QrACodInfoEmit.Value;
  BtMDe.Enabled := QrAEmpresa.Value = QrACodInfoDest.Value;
*)
  Emit := QrAEmpresa.Value = QrACodInfoEmit.Value;
  Dest := QrAEmpresa.Value = QrACodInfoDest.Value;
  Status100 := QrAcStat.Value = 100;
  Status0 := QrAcStat.Value = 0;
  //
  BtCCe.Enabled := Emit and Status100;
  BtCan.Enabled := Emit and Status100;
  BtMDe.Enabled := Dest and Status100;
  BtEPEC.Enabled := Emit and Status0;
  // fim 2023-06-28
  ReopenNFeEveRCab(0);
end;

procedure TFmNFeEveRcab.QrABeforeClose(DataSet: TDataSet);
begin
  QrNFeEveRCab.Close;
end;

procedure TFmNFeEveRcab.QrNFeEveRCabAfterScroll(DataSet: TDataSet);
begin
  QrNFeEveRCCe.Close;
  QrNFeEveRCan.Close;
  QrNFeEveRMDe.Close;
  QrNFeEveREPEC.Close;
  //
  case QrNFeEveRCabtpEvento.Value of
    110110: //Carta de Correcao
    begin
      PageControl1.ActivePageIndex := 0;
  ReopenNFeEveRCCe(0);
    end;
    110111: //Cancelamento
    begin
      PageControl1.ActivePageIndex := 1;
      ReopenNFeEveRCan(0);
    end;
    110140: // EPEC - Evento Pr�vio de Emiss�o em Conting�ncia
    begin
      PageControl1.ActivePageIndex := 3;
      ReopenNFeEveREPEC(0);
    end;
    0, 210200, 210210, 210220, 210240: // Manifesta��o do Destinat�rio
    begin
      PageControl1.ActivePageIndex := 2;
      ReopenNFeEveRMDe(0);
    end;
    else
      PageControl1.ActivePageIndex := 0;
  end;
end;

procedure TFmNFeEveRcab.QrNFeEveRCabBeforeClose(DataSet: TDataSet);
begin
  QrNFeEveRCCe.Close;
end;

procedure TFmNFeEveRcab.QrNFeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrNFeEveRCabtpAmb_TXT.Value := UnNFe_PF.TextoDeCodigoNFe(
    nfeCTide_tpAmb, QrNFeEveRCabtpAmb.Value);
end;

procedure TFmNFeEveRcab.ReopenNFeCabA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrA, Dmod.MyDB, [
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli, ',
  'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id, ',
  'ide_mod, ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF, ',
  'ide_verProc, ide_hEmi, ide_dhEmiTZD, ', // 2023-06-28
  'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF, ',
  'dest_xNome, ',
  // 2013-05-05
  'emit_xNome, emit_CPF, CodInfoEmit, CodInfoDest, ',
  // Fim 2013-05-05
  // 2012-09-07
  'emit_xLgr, emit_nro, emit_xCpl, emit_xBairro, ',
  'emit_xMun, emit_UF, emit_CEP, emit_xPais, dest_xBairro, ',
  'emit_IE, emit_IEST, emit_CNPJ, emit_fone, ',
  'dest_xLgr, dest_nro, dest_xCpl, dest_xMun, ',
  'dest_CEP, dest_fone, dest_IE, dest_UF, ',
  'dest_idEstrangeiro, ', // 2023-06-28
  'transporta_CNPJ, transporta_CPF, ',
  'transporta_IE, transporta_UF, ',
  'ide_tpAmb, infProt_nProt, infProt_dhRecbto, ',
  'ide_DSaiEnt, ModFrete, ',
  // Fim 2012-09-07
  'IF(Importado>0, Status, ',
  'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) + 0.000 cStat, ',
  'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
  'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
  // 2012-08-31
  //'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
  'IF(infCanc_cStat>0, ' +
  'DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ' +
  'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,',
  // Fim 2012-08-31
  'IDCtrl, versao, ',
  'ide_tpEmis, infCanc_xJust, Status, ',
  'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg, ',
  'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc, ',
  'ICMSTot_vBC, ICMSTot_vICMS, ',
  'infCanc_dhRecbto, infCanc_nProt ',
  'FROM nfecaba nfa ',
  'LEFT JOIN entidades cli ON ',
  'cli.Codigo=nfa.CodInfoDest ',
  'WHERE nfa.FatID=' + Geral.FF0(FatID),
  'AND nfa.FatNum=' + Geral.FF0(FatNum),
  'AND nfa.Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TFmNFeEveRcab.ReopenNFeEveRCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCab, Dmod.MyDB, [
  'SELECT * FROM nfeevercab ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'ORDER BY Controle DESC ',
  '']);
  //
  if (Controle <> 0) then
    QrNFeEveRCab.Locate('Controle', Controle, []);
end;

procedure TFmNFeEveRcab.ReopenNFeEveRCan(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCan, Dmod.MyDB, [
  'SELECT * FROM nfeevercan ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrNFeEveRCan.Locate('Conta', Conta, []);
end;

procedure TFmNFeEveRcab.ReopenNFeEveRCCe(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCCe, Dmod.MyDB, [
  'SELECT * FROM nfeevercce ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrNFeEveRCCe.Locate('Conta', Conta, []);
end;

procedure TFmNFeEveRcab.ReopenNFeEveREPEC(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveREPEC, Dmod.MyDB, [
  'SELECT * FROM nfeeverepec ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrNFeEveREPEC.Locate('Conta', Conta, []);
end;

procedure TFmNFeEveRcab.ReopenNFeEveRMDe(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRMDe, Dmod.MyDB, [
  'SELECT * FROM nfeevermde ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrNFeEveRMDe.Locate('Conta', Conta, []);
end;

// TODO :      ver retirada de evento de lote }
{ TODO :      mostrar status do evento processado }

end.
