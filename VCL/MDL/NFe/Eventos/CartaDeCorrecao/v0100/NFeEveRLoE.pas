unit NFeEveRLoE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, NFeXMLGerencia, DmkDAC_PF,
  UnDmkEnums, UnGrl_Consts;

type
  TFmNFeEveRLoE = class(TForm)
    PainelDados: TPanel;
    DsNFeEveRLoE: TDataSource;
    QrNFeEveRLoE: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    VuEmpresa: TdmkValUsu;
    QrNFeEveRLoEDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMEvento: TPopupMenu;
    DsNFeEveRCab: TDataSource;
    QrNFeEveRCab: TmySQLQuery;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrNFeEveRLoENO_Ambiente: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_XML: TWebBrowser;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNFEnonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdita: TGroupBox;
    Panel6: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtEvento: TBitBtn;
    BtXML: TBitBtn;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox3: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrNFeEveRLoECodigo: TIntegerField;
    QrNFeEveRLoECodUsu: TIntegerField;
    QrNFeEveRLoENome: TWideStringField;
    QrNFeEveRLoEEmpresa: TIntegerField;
    QrNFeEveRLoEversao: TFloatField;
    QrNFeEveRLoEtpAmb: TSmallintField;
    QrNFeEveRLoEverAplic: TWideStringField;
    QrNFeEveRLoEcOrgao: TSmallintField;
    QrNFeEveRLoEcStat: TSmallintField;
    QrNFeEveRLoExMotivo: TWideStringField;
    QrNFeEveRLoELk: TIntegerField;
    QrNFeEveRLoEDataCad: TDateField;
    QrNFeEveRLoEDataAlt: TDateField;
    QrNFeEveRLoEUserCad: TIntegerField;
    QrNFeEveRLoEUserAlt: TIntegerField;
    QrNFeEveRLoEAlterWeb: TSmallintField;
    QrNFeEveRLoEAtivo: TSmallintField;
    QrNFeEveRLoEFilial: TIntegerField;
    QrNFeEveRLoENO_Empresa: TWideStringField;
    IncluiEventoaoloteatual1: TMenuItem;
    RemoveEventosselecionados1: TMenuItem;
    DBGrid2: TDBGrid;
    QrNFeEveRCabFatID: TIntegerField;
    QrNFeEveRCabFatNum: TIntegerField;
    QrNFeEveRCabEmpresa: TIntegerField;
    QrNFeEveRCabControle: TIntegerField;
    QrNFeEveRCabtpAmb: TSmallintField;
    QrNFeEveRCabchNFe: TWideStringField;
    QrNFeEveRCabdhEvento: TDateTimeField;
    QrNFeEveRCabtpEvento: TIntegerField;
    QrNFeEveRCabdescEvento: TWideStringField;
    QrNFeEveRCabchNFe_NF_SER: TWideStringField;
    QrNFeEveRCabchNFe_NF_NUM: TWideStringField;
    QrNFeEveRCabret_cStat: TIntegerField;
    QrNFeEveRCabStatus: TIntegerField;
    BtEnvia: TBitBtn;
    QrProt: TmySQLQuery;
    QrCabA: TmySQLQuery;
    QrNFeEveRCabret_nProt: TWideStringField;
    QrNFeEveRCabret_dhRegEvento: TDateTimeField;
    QrProtItens: TLargeintField;
    QrNFeEveRCabXML_Eve: TWideMemoField;
    QrNFeEveRCabXML_retEve: TWideMemoField;
    QrNFeEveRCabSTATUS_TXT: TWideStringField;
    QrNFeEveRCabversao: TFloatField;
    QrNFeEveRCabcOrgao: TSmallintField;
    QrNFeEveRCabUF_Nome: TWideStringField;
    QrNFeEveRCabCNPJ_CPF: TWideStringField;
    QrNFeEveRCabnSeqEvento: TSmallintField;
    QrNFeEveRCabverEvento: TFloatField;
    QrNFeEveRCCe: TmySQLQuery;
    QrNFeEveRCCeConta: TIntegerField;
    QrNFeEveRCCexCorrecao: TWideMemoField;
    QrNFeEveRCCenCondUso: TSmallintField;
    RGide_mod: TdmkRadioGroup;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    QrNFeEveRLoEide_mod: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeEveRLoEAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeEveRLoEBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeEveRLoECalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMEventoPopup(Sender: TObject);
    procedure QrNFeEveRLoEAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure IncluiEventoaoloteatual1Click(Sender: TObject);
    procedure RemoveEventosselecionados1Click(Sender: TObject);
    procedure QrNFeEveRCabCalcFields(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
  private
    FDirNFeXML: String;
    FPreEmail: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    function  ImpedePelaMisturaDeWebServices(var UF_Servico: String): Boolean;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeEveRCab();
  end;

var
  FmNFeEveRLoE: TFmNFeEveRLoE;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleNFe_0000,
  NFeEveRLoEAdd, UnMailEnv, NFe_Tabs, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeEveRLoE.Lerarquivo1Click(Sender: TObject);
const
  SohLer = True;
var
  CodsEve: String;
  EventoLote, Empresa, ide_mod: Integer;
begin
  CodsEve    := NFe_CodsEveWbserverUserDef;
  EventoLote := QrNFeEveRLoECodigo.Value;
  Empresa    := QrNFeEveRLoEEmpresa.Value;
  ide_mod    := QrNFeEveRLoEide_mod.Value;
  //
  UnNFe_PF.MostraFormStepsNFe_EnvioLoteEvento(SohLer, CodsEve, EventoLote,
    Empresa, ide_mod);
  LocCod(EventoLote, EventoLote);
end;

procedure TFmNFeEveRLoE.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeEveRLoE.Lote1Click(Sender: TObject);
//var
  //LoteStr: String;
begin
{
  LoteStr := FormatFloat('000000000', QrNFeEveRLoECodigo.Value);
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeEveRLoEEmpresa.Value, NFE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
}
end;

procedure TFmNFeEveRLoE.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeEveRLoECodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeEveRLoE.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeEveRLoE.DefParams;
begin
  VAR_GOTOTABELA := 'nfeeverloe';
  VAR_GOTOMYSQLTABLE := QrNFeEveRLoE;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfeeverloe lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  VAR_SQLx.Add('AND lot.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Empresa IN (' + VAR_LIB_EMPRESAS + ')';
end;

procedure TFmNFeEveRLoE.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfeeverloe', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeEveRLoE.Envialoteaofisco1Click(Sender: TObject);
const
  SohLer = False;
var
  CodsEve: String;
  EventoLote, Empresa, ide_mod: Integer;
begin
  CodsEve    := NFe_CodsEveWbserverUserDef;
  EventoLote := QrNFeEveRLoECodigo.Value;
  Empresa    := QrNFeEveRLoEEmpresa.Value;
  ide_mod    := QrNFeEveRLoEide_mod.Value;
  //
  UnNFe_PF.MostraFormStepsNFe_EnvioLoteEvento(SohLer, CodsEve, EventoLote,
    Empresa, ide_mod);
  LocCod(EventoLote, EventoLote);
  ReopenNFeEveRCab();
end;

procedure TFmNFeEveRLoE.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeEveRLoE.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB, HabilitaC, HabilitaD, HabilitaE, HabilitaF: Boolean;
begin
  HabilitaA := (QrNFeEveRLoE.State = dsBrowse) and (QrNFeEveRLoE.RecordCount > 0);
  HabilitaC := (QrNFeEveRCab.State = dsBrowse) and (QrNFeEveRCab.RecordCount > 0);
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  //Enviodelote1.Enabled     := HabilitaA;
  //Consultadelote1.Enabled  := HabilitaA;//? and (QrNFeEveRLoE.);
  //
  HabilitaB := HabilitaA and (QrNFeEveRLoE.RecordCount = 0);
  Alteraloteatual1.Enabled := HabilitaB;
  //
  HabilitaF := not (QrNFeEveRLoEcStat.Value in [103,104,105]);
  HabilitaE := not (QrNFeEveRCabStatus.Value in [135, 136]);
  HabilitaD := HabilitaF and HabilitaE;
  HabilitaB := HabilitaA and HabilitaC and HabilitaD;

  Envialoteaofisco1.Enabled := HabilitaB;
  //HabilitaB := HabilitaA and (QrNFeEveRLoEcStat.Value > 102);
  //Verificalotenofisco1.Enabled := HabilitaB;

  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmNFeEveRLoE.PMEventoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeEveRLoE.RecordCount > 0) and (QrNFeEveRLoEcStat.Value <> 103);
  Alteraloteatual1.Enabled := Habilita;
end;

procedure TFmNFeEveRLoE.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNFeEveRLoE.AlteraRegistro;
var
  NFeEveRLoE : Integer;
begin
  NFeEveRLoE := QrNFeEveRLoECodigo.Value;
  if QrNFeEveRLoECodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NFeEveRLoE, Dmod.MyDB, 'nfeeverloe', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFeEveRLoE, Dmod.MyDB, 'nfeeverloe', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFeEveRLoE.IncluiRegistro;
var
  Cursor : TCursor;
  NFeEveRLoE : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NFeEveRLoE := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'nfeeverloe', 'nfeeverloe', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NFeEveRLoE))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, NFeEveRLoE);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNFeEveRLoE.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeEveRLoE.RemoveEventosselecionados1Click(Sender: TObject);
var
  Status: Integer;
begin
  if (QrNFeEveRLoEcStat.Value < 100) or (not (QrNFeEveRCabret_cStat.Value in ([128,129])))
  or ((QrNFeEveRLoEcStat.Value in ([130])) and (QrNFeEveRCabret_cStat.Value > 200)) then
  begin
    if Geral.MB_Pergunta('Deseja realmente tirar o evento deste lote?') = ID_YES then
    begin
      if (QrNFeEveRCabStatus.Value = 50)
      or ((QrNFeEveRLoEcStat.Value = 130) and (QrNFeEveRCabret_cStat.Value > 200)) then
        Status := 30
      else
        Status := QrNFeEveRCabStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeevercab SET EventoLote = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 ');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P3 AND Controle=:P4');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrNFeEveRCabFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrNFeEveRCabFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrNFeEveRCabEmpresa.Value;
      Dmod.QrUpd.Params[04].AsInteger := QrNFeEveRCabControle.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenNFeEveRCab();
    end;
  end else
    Geral.MB_Aviso('A��o n�o permitida!');
end;

procedure TFmNFeEveRLoE.ReopenNFeEveRCab();
begin
(*
SELECT FatID, FatNum, Empresa, Controle, 
tpAmb, chNFe, dhEvento, tpEvento, descEvento, 
Status, ret_cStat, ret_nProt, ret_dhRegEvento, 
XML_Eve, XML_retEve, versao 
FROM nfeevercab
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCab, Dmod.MyDB, [
    'SELECT nec.FatID, nec.FatNum, nec.Empresa, nec.Controle, nec.versao,',
    'nec.tpAmb, nec.chNFe, nec.dhEvento, nec.tpEvento, nec.descEvento,',
    'nec.Status, nec.ret_cStat, nec.ret_nProt, nec.ret_dhRegEvento, nec.verEvento,',
    'nec.XML_Eve, nec.XML_retEve, nec.versao, duf.Nome UF_Nome, nec.cOrgao,',
    'IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF, nec.nSeqEvento',
    'FROM nfeevercab nec',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_ufs duf ON duf.Codigo=nec.cOrgao',
    'WHERE nec.EventoLote=' + Geral.FF0(QrNFeEveRLoECodigo.Value),
    '']);
end;

procedure TFmNFeEveRLoE.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeEveRLoE.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeEveRLoE.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeEveRLoE.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeEveRLoE.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeEveRLoE.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeEveRLoE, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfeeverloe');
end;

procedure TFmNFeEveRLoE.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeEveRLoECodigo.Value;
  Close;
end;

procedure TFmNFeEveRLoE.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmNFeEveRLoE.Assina1Click(Sender: TObject);
begin
{
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeEveRLoEEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeEveRCabid.Value, True);
}
end;

procedure TFmNFeEveRLoE.Assinada1Click(Sender: TObject);
begin
{
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeEveRLoEEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeEveRCabid.Value, True, WB_XML);
}
end;

procedure TFmNFeEveRLoE.BtConfirmaClick(Sender: TObject);
var
  Codigo, ide_mod: Integer;
begin
  ide_mod := 0;
  case RGide_mod.ItemIndex of
    1: ide_mod := CO_MODELO_NFE_55;
    2: ide_mod := CO_MODELO_NFE_65;
  end;
  if MyObjects.FIC(ide_mod = 0, RGide_mod, 'Selecione o modelo da NF!') then Exit;
  //
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverloe', '', 0, 999999999, '');
  EdCodigo.ValueVariant := Codigo;
  EdCodUsu.ValueVariant := Codigo;

  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PainelEdita,
    'nfeeverloe', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeeverloe', False, [
    'ide_mod'], ['Codigo'], [
    ide_mod], [Codigo], True);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeEveRLoE.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfeeverloe', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeEveRLoE.BtEnviaClick(Sender: TObject);

  procedure ExportaXMLCCE(var DiretXML: String);
  var
    Continua: Boolean;
    Protocolo, ArqXML, XML_Distribuicao: String;
  begin
    Continua := False;
    DiretXML := '';
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Verificando exist�ncia do arquivo XML da Carta de Corre��o');
    //
    Protocolo := Geral.SoNumero_TT(QrNFeEveRCabret_nProt.Value);
    ArqXML    := DmNFe_0000.QrFilialDirEveProcCCe.Value;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
    //
    ArqXML := ArqXML + Protocolo + NFE_EXT_EVE_PRO_CCE_XML;
    //
    if not FileExists(ArqXML) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da Carta de Corre��o');
      //
      XML_Distribuicao := '';
      //
      if NFeXMLGeren.XML_DistribuiCartaDeCorrecao(verProcCCeNFe_Versao,
        QrNFeEveRCabXML_Eve.Value, QrNFeEveRCabXML_retEve.Value, XML_Distribuicao)
      then
        Continua := NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
    end else
      Continua := True;
    if Continua then
      DiretXML := ArqXML;
  end;

  procedure ExportaXMLCan(var DiretXML: String);
  var
    Continua: Boolean;
    Protocolo, ArqXML, XML_Distribuicao: String;
  begin
    Continua := False;
    DiretXML := '';
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Verificando exist�ncia do arquivo XML do Cancelamento');
    //
    Protocolo := Geral.SoNumero_TT(QrNFeEveRCabret_nProt.Value);
    ArqXML    := DmNFe_0000.QrFilialDirEveRetCan.Value;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
    //
    ArqXML := ArqXML + Protocolo + '-procEventoNFe.xml';
    //
    if not FileExists(ArqXML) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML do Cancelamento');
      //
      XML_Distribuicao := '';
      //
      if NFeXMLGeren.XML_DistribuiEvento(QrNFeEveRCabchNFe.Value,
        QrNFeEveRCabret_cStat.Value, QrNFeEveRCabXML_Eve.Value,
        QrNFeEveRCabXML_retEve.Value, False, XML_Distribuicao,
        QrNFeEveRCabversao.Value, 'na base de dados')
      then
        Continua := NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
    end else
      Continua := True;
    if Continua then
      DiretXML := ArqXML;
  end;

var
  Msg, XML, Orgao, Ambiente, CNPJ_CPF, CHAVE_ACESSO: String;
  Entidade: Integer;
begin
  PageControl1.ActivePageIndex := 0;
  Msg := '';
  DmNFe_0000.ReopenEmpresa(QrNFeEveRLoEEmpresa.Value);
  DmNFe_0000.ReopenOpcoesNFe(DModG.QrEmpresasCodigo.Value, True);
  ReopenNFeEveRCab();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProt, Dmod.MyDB, [
    'SELECT COUNT(*) Itens ',
    'FROM nfeevercab ',
    'WHERE EventoLote=' + Geral.FF0(QrNFeEveRLoECodigo.Value),
    'AND Status in (135,136)',
    '']);
  if QrProt.RecordCount = 0 then
    Msg := 'N�o h� item protocolado para envio!'
  else
  if QrProtItens.Value < QrNFeEveRCab.RecordCount then
  begin
    Msg := 'Somente os itens protocolados ser�o enviados!'
  end;
  if Msg <> '' then
    Geral.MB_Aviso(Msg);
  if QrProt.RecordCount = 0 then
    Exit;
  //
  case TNFeEventos(QrNFeEveRCabtpEvento.Value) of
    nfeeveCCe: //Carta de corre��o
    begin
      FDirNFeXML := DmNFe_0000.QrFilialDirEveProcCCe.Value;
      XML        := '';

      if FDirNFeXML = '' then
      begin
        Geral.MB_Aviso(
        'Diret�rio do processamento de carta de corre��o n�o definido!');
        Exit;
      end;
      Geral.VerificaDir(FDirNFeXML, '\', '', True);
      FPreEmail := DmNFe_0000.QrFilialPreMailEveCCe.Value;

      QrNFeEveRCab.DisableControls;
      try
        QrNFeEveRCab.First;
        while not QrNFeEveRCab.Eof do
        begin
          if QrNFeEveRCabStatus.Value in ([135,136]) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
              'SELECT Id, ide_serie, ide_nNF, ',
              'versao, IDCtrl, ',
              'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat, ',
              'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
              'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
              'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
              'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF, ',
              'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF, ',
              'Transporta_CNPJ, Transporta_CPF, Transporta_xNome, ',
              'Transporta_IE, Transporta_UF ',
              'FROM nfecaba ',
              'WHERE Id="' + QrNFeEveRCabchNFe.Value + '"',
              '']);
            //
            ExportaXMLCCE(XML);
            //
            if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' +
              sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCCe, Dmod.MyDB, [
              'SELECT Conta, xCorrecao, nCondUso',
              'FROM nfeevercce',
              'WHERE FatID=' + Geral.FF0(QrNFeEveRCabFatID.Value),
              'AND FatNum=' + Geral.FF0(QrNFeEveRCabFatNum.Value),
              'AND Empresa=' + Geral.FF0(QrNFeEveRCabEmpresa.Value),
              'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
              '']);
            if QrNFeEveRCCe.RecordCount <> 1 then
            begin
              Geral.MB_Erro(
                'Quantidade de registros de carta de corre��o diferente de um.' + sLineBreak +
                'Quantidade encontrada: ' + Geral.FF0(QrNFeEveRCCe.RecordCount));
                Exit;
            end;
            if QrCabA.FieldByName('dest_CNPJ').AsString <> '' then
              CNPJ_CPF := QrCabA.FieldByName('dest_CNPJ').AsString
            else
              CNPJ_CPF := QrCabA.FieldByName('dest_CPF').AsString;
            //
            DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
            //
            Orgao        := Geral.FF0(QrNFeEveRCabcOrgao.Value) + ' - ' + QrNFeEveRCabUF_Nome.Value;
            Ambiente     := Geral.FF0(QrNFeEveRCabtpAmb.Value) + ' - ' + NFeXMLGeren.TextoAmbiente(QrNFeEveRCabtpAmb.Value);
            CNPJ_CPF     := Geral.FormataCNPJ_TT(QrNFeEveRCabCNPJ_CPF.Value);
            CHAVE_ACESSO := Geral.FormataChaveNFe(QrNFeEveRCabchNFe.Value, cfcnFrendly);
            //
            if UMailEnv.Monta_e_Envia_Mail([Entidade,
              DModG.QrEmpresasCodigo.Value, QrNFeEveRCabStatus.Value],
              meNFeEveCCE, [XML], [QrCabA.FieldByName('dest_xNome').AsString,
              QrCabA. FieldByName('Id').AsString,
              Geral.FormataVersaoNFe(QrNFeEveRCabversao.Value), Orgao, Ambiente,
              CNPJ_CPF, CHAVE_ACESSO, Geral.FDT(QrNFeEveRCabdhEvento.Value, 0),
              Geral.FF0(QrNFeEveRCabtpEvento.Value), Geral.FF0(QrNFeEveRCabnSeqEvento.Value),
              Geral.FormataVersaoNFe(QrNFeEveRCabverEvento.Value), QrNFeEveRCabdescEvento.Value,
              QrNFeEveRCCexCorrecao.Value, CO_CONDI_USO_CCE_COM[QrNFeEveRCCenCondUso.Value],
              Geral.FF0(QrCabA.FieldByName('ide_serie').AsInteger),
              Geral.FF0(QrCabA.FieldByName('ide_nNF').AsInteger)], True)
            then
              Geral.MB_Aviso('E-mail enviado com sucesso!');
          end;
          //
          QrNFeEveRCab.Next;
        end;
      finally
        QrNFeEveRCab.EnableControls;
      end;
    end;
    nfeeveCan: //Cancelamento
    begin
      FDirNFeXML := DmNFe_0000.QrFilialDirEveRetCan.Value;
      XML        := '';

      if FDirNFeXML = '' then
      begin
        Geral.MB_Aviso('Diret�rio das respostas de cancelamento de NF-e como evento da NF-e (-can.xml) n�o definido!');
        Exit;
      end;
      Geral.VerificaDir(FDirNFeXML, '\', '', True);
      FPreEmail := DmNFe_0000.QrFilialPreMailEveCCe.Value;

      QrNFeEveRCab.DisableControls;
      try
        QrNFeEveRCab.First;
        while not QrNFeEveRCab.Eof do
        begin
          if QrNFeEveRCabStatus.Value in ([135,136]) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
              'SELECT Id, ide_serie, ide_nNF, ',
              'versao, IDCtrl, ',
              'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat, ',
              'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
              'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
              'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
              'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF, ',
              'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF, ',
              'Transporta_CNPJ, Transporta_CPF, Transporta_xNome, ',
              'Transporta_IE, Transporta_UF ',
              'FROM nfecaba ',
              'WHERE Id="' + QrNFeEveRCabchNFe.Value + '"',
              '']);
            //
            ExportaXMLCan(XML);
            //
            if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' +
              sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
            //
            if QrCabA.FieldByName('dest_CNPJ').AsString <> '' then
              CNPJ_CPF := QrCabA.FieldByName('dest_CNPJ').AsString
            else
              CNPJ_CPF := QrCabA.FieldByName('dest_CPF').AsString;
            //
            DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
            //
            Orgao        := Geral.FF0(QrNFeEveRCabcOrgao.Value) + ' - ' + QrNFeEveRCabUF_Nome.Value;
            Ambiente     := Geral.FF0(QrNFeEveRCabtpAmb.Value) + ' - ' + NFeXMLGeren.TextoAmbiente(QrNFeEveRCabtpAmb.Value);
            CNPJ_CPF     := Geral.FormataCNPJ_TT(QrNFeEveRCabCNPJ_CPF.Value);
            CHAVE_ACESSO := Geral.FormataChaveNFe(QrNFeEveRCabchNFe.Value, cfcnFrendly);
            //
            if UMailEnv.Monta_e_Envia_Mail([Entidade,
              DModG.QrEmpresasCodigo.Value, QrNFeEveRCabStatus.Value],
              meNFeEveCan, [XML], [QrCabA.FieldByName('dest_xNome').AsString,
              QrCabA.FieldByName('Id').AsString,
              Geral.FF0(QrCabA.FieldByName('ide_serie').AsInteger),
              Geral.FF0(QrCabA.FieldByName('ide_nNF').AsInteger)], True)
            then
              Geral.MB_Aviso('E-mail enviado com sucesso!');
          end;
          //
          QrNFeEveRCab.Next;
        end;
      finally
        QrNFeEveRCab.EnableControls;
      end;
    end;
    else
      Geral.MB_Aviso('Tipo de devento n�o implementado!');
  end;
end;

procedure TFmNFeEveRLoE.BtEventoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMEvento, BtEvento);
end;

procedure TFmNFeEveRLoE.BtLoteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeEveRLoE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //N�o implementado
  BtXML.Visible:= False;
end;

procedure TFmNFeEveRLoE.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeEveRLoECodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeEveRLoE.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeEveRLoE.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeEveRLoE.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeEveRLoECodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeEveRLoE.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeEveRLoE.QrNFeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrNFeEveRCabchNFe_NF_SER.Value := Copy(QrNFeEveRCabchNFe.Value,
    DEMONTA_CHV_INI_SerNFe,  DEMONTA_CHV_TAM_SerNFe);
  QrNFeEveRCabchNFe_NF_NUM.Value := Copy(QrNFeEveRCabchNFe.Value,
    DEMONTA_CHV_INI_NumNFe,  DEMONTA_CHV_TAM_NumNFe);
  QrNFeEveRCabSTATUS_TXT.Value :=
    NFeXMLGeren.Texto_StatusNFe(QrNFeEveRCabStatus.Value, 0);

end;

procedure TFmNFeEveRLoE.QrNFeEveRLoEAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeEveRLoE.QrNFeEveRLoEAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeEveRCab();
end;

procedure TFmNFeEveRLoE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveRLoE.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeEveRLoECodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfeeverloe', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeEveRLoE.SelecionaroXML1Click(Sender: TObject);
//var
  //Arquivo: String;
begin
{
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\NFE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
}
end;

procedure TFmNFeEveRLoE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRLoE.Gerada1Click(Sender: TObject);
begin
{
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeEveRLoEEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeEveRCabid.Value, False);
}
end;

procedure TFmNFeEveRLoE.Gerada2Click(Sender: TObject);
begin
{
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeEveRLoEEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeEveRCabid.Value, False, WB_XML);
}
end;

function TFmNFeEveRLoE.ImpedePelaMisturaDeWebServices(var UF_Servico: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  UF_Servico := '??';
  if DModG.QrPrmsEmpNFeUF_MDeMDe.Value <> DModG.QrPrmsEmpNFeUF_Servico.Value then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    'SUM(IF(tpEvento IN (' + NFe_CodsEveWbserverUserDef + '), 1, 0)) UsuDef, ',
    'SUM(IF(tpEvento IN (' + NFe_CodsEveWbserverUserDef + '), 0, 1)) PreDef ',
    'FROM nfeevercab ',
    'WHERE EventoLote=' + Geral.FF0(QrNFeEveRLoECodigo.Value),
    '']);
    Result := (Qry.FieldByName('UsuDef').AsFloat >= 1) and
              (Qry.FieldByName('PreDef').AsFloat >= 1);
    if Result then Geral.MB_Aviso('Envio cancelado!' + sLineBreak +
    'Existe mistura de web services nos eventos inclu�dos neste lote!')
    else
    if Qry.FieldByName('UsuDef').AsFloat >= 1 then
      UF_Servico := DModG.QrPrmsEmpNFeUF_MDeMDe.Value
    else
      UF_Servico := DModG.QrPrmsEmpNFeUF_Servico.Value;
  end else
    Result := False;
end;

procedure TFmNFeEveRLoE.IncluiEventoaoloteatual1Click(Sender: TObject);
var
  FatID: Integer;
begin
  if DBCheck.CriaFm(TFmNFeEveRLoEAdd, FmNFeEveRLoEAdd, afmoNegarComAviso) then
  begin
{   ini 2020-11-02
    FmNFeEveRLoEAdd.QrNFeEveRCab.Close;
    FmNFeEveRLoEAdd.QrNFeEveRCab.Params[00].AsInteger := QrNFeEveRLoEEmpresa.Value;
    FmNFeEveRLoEAdd.QrNFeEveRCab.Params[01].AsInteger := DmNFe_0000.stepNFeAssinada();
    UnDmkDAC_PF.AbreQuery(FmNFeEveRLoEAdd.QrNFeEveRCab, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
}
    if not Geral.ide_mod_to_FatID(QrNFeEveRLoEide_mod.Value, FatID) then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmNFeEveRLoEAdd.QrNFeEveRCab, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, Controle,  ',
    'tpAmb, chNFe, dhEvento, tpEvento, descEvento ',
    'FROM nfeevercab ',
    'WHERE Empresa=' + Geral.FF0(QrNFeEveRLoEEmpresa.Value),
    'AND Status=' + Geral.FF0(DmNFe_0000.stepNFeAssinada()),
    'AND FatID=' + Geral.FF0(FatID),
    '']);
    // fim 2020-11-02
    FmNFeEveRLoEAdd.ShowModal;
    FmNFeEveRLoEAdd.Destroy;
    //
  end;
end;

procedure TFmNFeEveRLoE.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrNFeEveRLoE, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfeeverloe');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
  RGide_mod.ItemIndex := 0;
end;

procedure TFmNFeEveRLoE.QrNFeEveRLoEBeforeOpen(DataSet: TDataSet);
begin
  QrNFeEveRLoECodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeEveRLoE.QrNFeEveRLoECalcFields(DataSet: TDataSet);
begin
  //QrNFeEveRLoEDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeEveRLoEdhRecbto.Value, 0);
  case QrNFeEveRLoEtpAmb.Value of
    1: QrNFeEveRLoENO_Ambiente.Value := 'Produ��o';
    2: QrNFeEveRLoENO_Ambiente.Value := 'Homologa��o';
    else QrNFeEveRLoENO_Ambiente.Value := '';
  end;
end;

end.

