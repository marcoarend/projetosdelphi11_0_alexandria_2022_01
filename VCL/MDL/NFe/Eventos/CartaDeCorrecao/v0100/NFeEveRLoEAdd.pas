unit NFeEveRLoEAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkImage, NFeXMLGerencia, UnDmkEnums;

type
  TFmNFeEveRLoEAdd = class(TForm)
    Panel1: TPanel;
    QrNFeEveRCab: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsNFeEveRCab: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrNFeEveRCabControle: TIntegerField;
    QrNFeEveRCabtpAmb: TSmallintField;
    QrNFeEveRCabchNFe: TWideStringField;
    QrNFeEveRCabdhEvento: TDateTimeField;
    QrNFeEveRCabtpEvento: TIntegerField;
    QrNFeEveRCabdescEvento: TWideStringField;
    QrNFeEveRCabchNFe_NF_SER: TWideStringField;
    QrNFeEveRCabchNFe_NF_NUM: TWideStringField;
    QrNFeEveRCabFatID: TIntegerField;
    QrNFeEveRCabEmpresa: TIntegerField;
    QrNFeEveRCabFatNum: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure QrNFeEveRCabCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeEveRLoEAdd: TFmNFeEveRLoEAdd;

implementation

uses UnMyObjects, NFeEveRLoE, Module, UMySQLModule, dmkGeral, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeEveRLoEAdd.BtConfirmaClick(Sender: TObject);
  procedure AdicionaEventoAoLote();
  var
    EventoLote, FatID, FatNum, Empresa, Status, Controle, ret_cStat: Integer;
    ret_xMotivo: String;
  begin
    EventoLote := FmNFeEveRLoE.QrNFeEveRLoECodigo.Value;
    FatID    := QrNFeEveRCabFatID.Value;
    FatNum   := QrNFeEveRCabFatNum.Value;
    Empresa  := QrNFeEveRCabEmpresa.Value;
    Controle := QrNFeEveRCabControle.Value;
    Status   := DmNFe_0000.stepNFeAdedLote();
    ret_cStat   := 0;
    ret_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
    'EventoLote', 'Status', 'ret_cStat', 'ret_xMotivo'], [
    'FatID', 'FatNum', 'Empresa', 'Controle'], [
    EventoLote, Status, ret_cStat, ret_xMotivo], [
    FatID, FatNum, Empresa, Controle], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmNFeEveRLoE.QrNFeEveRCab.RecordCount > 20 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 20 eventos permitidos por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
    ' NF-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        //GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        GotoBookmark(DBGrid1.SelectedRows.Items[I]);
        AdicionaEventoAoLote();
      end;
    end;
  end else AdicionaEventoAoLote();
  FmNFeEveRLoE.ReopenNFeEveRCab();
  Close;
end;

procedure TFmNFeEveRLoEAdd.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveRLoEAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveRLoEAdd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmNFeEveRLoE.LocCod(FmNFeEveRLoE.QrNFeEveRLoECodigo.Value,
    FmNFeEveRLoE.QrNFeEveRLoECodigo.Value);
end;

procedure TFmNFeEveRLoEAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeEveRLoEAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRLoEAdd.QrNFeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrNFeEveRCabchNFe_NF_SER.Value := Copy(QrNFeEveRCabchNFe.Value,
    DEMONTA_CHV_INI_SerNFe,  DEMONTA_CHV_TAM_SerNFe);
  QrNFeEveRCabchNFe_NF_NUM.Value := Copy(QrNFeEveRCabchNFe.Value,
    DEMONTA_CHV_INI_NumNFe,  DEMONTA_CHV_TAM_NumNFe);
end;

end.
