unit NFeEveRCCe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, NFeXMLGerencia, UnDmkEnums, UnXXe_PF;

type
  TFmNFeEveRCCe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    MexCorrecao: TdmkMemo;
    RGnCondUso: TdmkRadioGroup;
    MeCondicao: TMemo;
    CkCondicao: TCheckBox;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure MexCorrecaoEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGnCondUsoClick(Sender: TObject);
    procedure CkCondicaoClick(Sender: TObject);
    procedure MexCorrecaoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure MexCorrecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeCondicaoChange(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaOK();
  public
    { Public declarations }
    MsgMemoDesfeito: Boolean;
    //
    procedure DesfazMsgMemo();
  end;

  var
  FmNFeEveRCCe: TFmNFeEveRCCe;

implementation

uses UnMyObjects, Module, NFe_Tabs, NFeEveRCab, ModuleGeral, UMySQLModule,
  EveGeraXMLCCe_0100, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeEveRCCe.BtOKClick(Sender: TObject);
var
  xCorrecao, Destino, Dir: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, nCondUso: Integer;
  // XML
  XML_Eve: String;
begin
  if not DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_CCE_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmNFeEveRCab.IncluiNFeEveRCab(0, eveCCe, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeevercce', '', 0);
  end else begin
    FatID := FmNFeEveRCab.QrNFeEveRCabFatID.Value;
    FatNum := FmNFeEveRCab.QrNFeEveRCabFatNum.Value;
    Empresa := FmNFeEveRCab.QrNFeEveRCabEmpresa.Value;
    Controle := FmNFeEveRCab.QrNFeEveRCabControle.Value;
    Conta := FmNFeEveRCab.QrNFeEveRCCeConta.Value;
  end;
  //
  xCorrecao      := Trim(XXe_PF.ValidaTexto_XML(MexCorrecao.Text, 'xCorrecao', 'xCorrecao'));
  nCondUso       := RGnCondUso.ItemIndex;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeevercce', False, [
  'xCorrecao', 'nCondUso'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  xCorrecao, nCondUso], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnEveGeraXMLCCe_0100.CriarDocumentoCCe(FatID, FatNum, Empresa,
      FmNFeEveRCab.QrERCId.Value, FmNFeEveRCab.QrERCcOrgao.Value,
      FmNFeEveRCab.QrERCtpAmb.Value, FmNFeEveRCab.QrERCTipoEnt.Value,
      FmNFeEveRCab.QrERCCNPJ.Value, FmNFeEveRCab.QrERCCPF.Value,
      FmNFeEveRCab.QrERCchNFe.Value, FmNFeEveRCab.QrERCdhEvento.Value,
      FmNFeEveRCab.QrERCTZD_UTC.Value, FmNFeEveRCab.QrERCtpEvento.Value,
      FmNFeEveRCab.QrERCnSeqEvento.Value, FmNFeEveRCab.QrERCverEvento.Value,
      FmNFeEveRCab.QrERCversao.Value, FmNFeEveRCab.QrERCdescEvento.Value,
      xCorrecao, CO_CONDI_USO_CCE_SEM[nCondUso],
      XML_Eve, LaAviso1, LaAviso2) then
      begin
        Destino := DmNFe_0000.SalvaXML(NFE_EXT_EVE_ENV_CCE_XML,
        FmNFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
        if FileExists(Destino) then
        begin
            Status := DmNFe_0000.stepNFeAssinada();
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
            'XML_Eve', 'Status'], [
            'FatID', 'FatNum', 'Empresa', 'Controle'], [
            Geral.WideStringToSQLString(XML_Eve), Status], [
            FatID, FatNum, Empresa, Controle], True) then
              FmNFeEveRCab.ReopenNFeEveRCab(Controle);
        end;
      end;

    //
    FmNFeEveRCab.ReopenNFeEveRCCe(0);
    Close;
  end;
end;

procedure TFmNFeEveRCCe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveRCCe.CkCondicaoClick(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmNFeEveRCCe.DesfazMsgMemo();
begin
  if not MsgMemoDesfeito then
  begin
    MexCorrecao.Lines.Clear;
    MexCorrecao.Font.Color := clWindowText;
    MsgMemoDesfeito := True;
  end;
  HabilitaOK();
end;

procedure TFmNFeEveRCCe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeEveRCCe.FormCreate(Sender: TObject);
begin
  MsgMemoDesfeito := False;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFeEveRCCe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveRCCe.HabilitaOK();
var
  N: Integer;
begin
  N := Length(Geral.SoNumeroELetra_TT(MexCorrecao.Text));
  BtOK.Enabled := MsgMemoDesfeito and
    (N >= 15) and
    CkCondicao.Checked and
    (RGnCondUso.ItemIndex > 0);
end;

procedure TFmNFeEveRCCe.MeCondicaoChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmNFeEveRCCe.MexCorrecaoChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmNFeEveRCCe.MexCorrecaoEnter(Sender: TObject);
begin
  DesfazMsgMemo();
end;

procedure TFmNFeEveRCCe.MexCorrecaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key in ([9, 10, 13]) then
   Key := 0;
end;

procedure TFmNFeEveRCCe.RGnCondUsoClick(Sender: TObject);
begin
  MeCondicao.Text := CO_CONDI_USO_CCE_COM[RGnCondUso.ItemIndex];
  HabilitaOK();
end;

end.
