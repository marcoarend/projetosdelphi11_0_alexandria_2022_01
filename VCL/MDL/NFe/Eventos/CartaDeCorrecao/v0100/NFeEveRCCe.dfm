object FmNFeEveRCCe: TFmNFeEveRCCe
  Left = 339
  Top = 185
  Caption = 'NFe-EVENT-002 :: Carta de Corre'#231#227'o NF-e'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 32
        Caption = 'Carta de Corre'#231#227'o NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 32
        Caption = 'Carta de Corre'#231#227'o NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 32
        Caption = 'Carta de Corre'#231#227'o NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 105
        Align = alTop
        TabOrder = 0
        object RGnCondUso: TdmkRadioGroup
          Left = 2
          Top = 15
          Width = 111
          Height = 88
          Align = alLeft
          Caption = ' Condi'#231#227'o de uso: '
          ItemIndex = 0
          Items.Strings = (
            'Sem condi'#231#227'o'
            'Modelo 1')
          TabOrder = 0
          OnClick = RGnCondUsoClick
          QryCampo = 'nCondUso'
          UpdCampo = 'nCondUso'
          UpdType = utYes
          OldValor = 0
        end
        object MeCondicao: TMemo
          Left = 113
          Top = 15
          Width = 669
          Height = 88
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 1
          OnChange = MeCondicaoChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 105
        Width = 784
        Height = 225
        Align = alClient
        Caption = 
          ' Texto da Carta de Corre'#231#227'o: (DEVE CONTER PELO MENOS 15 CARACTER' +
          'ES!) '
        TabOrder = 1
        object MexCorrecao: TdmkMemo
          Left = 2
          Top = 15
          Width = 542
          Height = 208
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clGray
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Lines.Strings = (
            'Descreva aqui a corre'#231#227'o a ser considerada. '
            'Lembre-se que a corre'#231#227'o mais recente substituir'#225' as anteriores!')
          ParentFont = False
          TabOrder = 0
          WantReturns = False
          OnChange = MexCorrecaoChange
          OnEnter = MexCorrecaoEnter
          OnKeyDown = MexCorrecaoKeyDown
          QryCampo = 'xCorrecao'
          UpdCampo = 'xCorrecao'
          UpdType = utYes
        end
        object Memo1: TMemo
          Left = 544
          Top = 15
          Width = 238
          Height = 208
          Align = alRight
          ReadOnly = True
          TabOrder = 1
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 609
        Height = 32
        Caption = 
          'Descreva a corre'#231#227'o a ser considerada. Lembre-se que a corre'#231#227'o ' +
          'mais recente substituir'#225' as anteriores!'#13#10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 609
        Height = 32
        Caption = 
          'Descreva a corre'#231#227'o a ser considerada. Lembre-se que a corre'#231#227'o ' +
          'mais recente substituir'#225' as anteriores!'#13#10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkCondicao: TCheckBox
        Left = 144
        Top = 16
        Width = 413
        Height = 17
        Caption = 
          'Li as condi'#231#245'es de uso e os avisos e estou ciente do conte'#250'do e ' +
          'das implica'#231#245'es.'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        OnClick = CkCondicaoClick
      end
    end
  end
end
