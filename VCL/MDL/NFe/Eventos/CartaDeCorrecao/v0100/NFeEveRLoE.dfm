object FmNFeEveRLoE: TFmNFeEveRLoE
  Left = 368
  Top = 194
  Caption = 'NFe-EVENT-101 :: Lotes de Eventos da NF-e'
  ClientHeight = 625
  ClientWidth = 878
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 106
    Width = 878
    Height = 519
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 878
      Height = 213
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 874
        Height = 196
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 10
          Top = 4
          Width = 14
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 64
          Top = 4
          Width = 57
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo: [F4]'
        end
        object Label9: TLabel
          Left = 143
          Top = 4
          Width = 51
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label4: TLabel
          Left = 10
          Top = 44
          Width = 44
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
        end
        object EdCodigo: TdmkEdit
          Left = 10
          Top = 20
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 64
          Top = 20
          Width = 74
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdNome: TdmkEdit
          Left = 143
          Top = 20
          Width = 584
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 10
          Top = 60
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 60
          Width = 665
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 4
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGide_mod: TdmkRadioGroup
          Left = 12
          Top = 84
          Width = 717
          Height = 45
          Caption = '  Modelo da NF: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '00 - Indefinido'
            '55 - NF-e'
            '65 - NFC-e')
          TabOrder = 5
          QryCampo = 'ide_mod'
          UpdCampo = 'ide_mod'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 442
      Width = 878
      Height = 77
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 743
        Top = 15
        Width = 133
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 106
    Width = 878
    Height = 519
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 176
    ExplicitTop = 98
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 878
      Height = 213
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 741
        Height = 213
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Dados do Lote: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 737
          Height = 196
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 10
            Top = 4
            Width = 14
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID:'
          end
          object Label2: TLabel
            Left = 139
            Top = 4
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object Label3: TLabel
            Left = 64
            Top = 4
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
            FocusControl = DBEdNome
          end
          object Label5: TLabel
            Left = 10
            Top = 44
            Width = 20
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Filial'
            FocusControl = DBEdit2
          end
          object Label6: TLabel
            Left = 680
            Top = 4
            Width = 38
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modelo:'
            FocusControl = DBEdit1
          end
          object DBEdNome: TDBEdit
            Left = 64
            Top = 20
            Width = 74
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CodUsu'
            DataSource = DsNFeEveRLoE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 10
            Top = 60
            Width = 52
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Filial'
            DataSource = DsNFeEveRLoE
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 64
            Top = 60
            Width = 669
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_Empresa'
            DataSource = DsNFeEveRLoE
            TabOrder = 2
          end
          object GroupBox2: TGroupBox
            Left = 10
            Top = 84
            Width = 455
            Height = 105
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Resposta do envio do XML: '
            TabOrder = 3
            object Label11: TLabel
              Left = 10
              Top = 16
              Width = 85
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vers'#227'o do leiaute:'
              FocusControl = DBEdit6
            end
            object Label12: TLabel
              Left = 101
              Top = 16
              Width = 85
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de ambiente:'
              FocusControl = DBEdit7
            end
            object Label13: TLabel
              Left = 240
              Top = 16
              Width = 171
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vers'#227'o do aplic. que recebeu o lote:'
              FocusControl = DBEdit8
            end
            object Label14: TLabel
              Left = 10
              Top = 56
              Width = 48
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Resposta:'
              FocusControl = DBEdit9
            end
            object Label15: TLabel
              Left = 421
              Top = 16
              Width = 17
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'UF:'
              FocusControl = DBEdit12
            end
            object DBEdit6: TDBEdit
              Left = 10
              Top = 32
              Width = 89
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'versao'
              DataSource = DsNFeEveRLoE
              TabOrder = 0
            end
            object DBEdit7: TDBEdit
              Left = 101
              Top = 32
              Width = 20
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'tpAmb'
              DataSource = DsNFeEveRLoE
              TabOrder = 1
            end
            object DBEdit8: TDBEdit
              Left = 240
              Top = 32
              Width = 177
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'verAplic'
              DataSource = DsNFeEveRLoE
              TabOrder = 2
            end
            object DBEdit9: TDBEdit
              Left = 10
              Top = 74
              Width = 30
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'cStat'
              DataSource = DsNFeEveRLoE
              TabOrder = 3
            end
            object DBEdit10: TDBEdit
              Left = 42
              Top = 74
              Width = 407
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'xMotivo'
              DataSource = DsNFeEveRLoE
              TabOrder = 4
            end
            object DBEdit11: TDBEdit
              Left = 122
              Top = 32
              Width = 115
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_Ambiente'
              DataSource = DsNFeEveRLoE
              TabOrder = 5
            end
            object DBEdit12: TDBEdit
              Left = 421
              Top = 32
              Width = 27
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'cOrgao'
              DataSource = DsNFeEveRLoE
              TabOrder = 6
            end
          end
          object GroupBox3: TGroupBox
            Left = 464
            Top = 84
            Width = 269
            Height = 105
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Recibo de aceite do lote: '
            Enabled = False
            TabOrder = 4
            object Label16: TLabel
              Left = 10
              Top = 16
              Width = 87
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#250'mero do recibo:'
              Enabled = False
              FocusControl = DBEdit13
            end
            object Label10: TLabel
              Left = 10
              Top = 56
              Width = 58
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data / hora:'
              Enabled = False
              FocusControl = DBEdit5
            end
            object Label17: TLabel
              Left = 174
              Top = 56
              Width = 84
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 't m'#233'd. resp. (seg):'
              Enabled = False
              FocusControl = DBEdit14
            end
            object DBEdit13: TDBEdit
              Left = 10
              Top = 32
              Width = 245
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataSource = DsNFeEveRLoE
              Enabled = False
              TabOrder = 0
            end
            object DBEdit5: TDBEdit
              Left = 10
              Top = 73
              Width = 160
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataSource = DsNFeEveRLoE
              Enabled = False
              TabOrder = 1
            end
            object DBEdit14: TDBEdit
              Left = 174
              Top = 73
              Width = 83
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataSource = DsNFeEveRLoE
              Enabled = False
              TabOrder = 2
            end
          end
          object DBEdit4: TDBEdit
            Left = 139
            Top = 20
            Width = 538
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Nome'
            DataSource = DsNFeEveRLoE
            TabOrder = 5
          end
          object DBEdCodigo: TDBEdit
            Left = 10
            Top = 20
            Width = 52
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Codigo'
            DataSource = DsNFeEveRLoE
            TabOrder = 6
          end
          object DBEdit1: TDBEdit
            Left = 680
            Top = 20
            Width = 52
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ide_mod'
            DataSource = DsNFeEveRLoE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 7
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 213
      Width = 878
      Height = 200
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Informa'#231#245'es dos eventos do lote'
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 870
          Height = 172
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsNFeEveRCab
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe'
              Title.Caption = 'Chave da NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe_NF_SER'
              Title.Caption = 'S'#233'rie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe_NF_NUM'
              Title.Caption = 'N'#186' NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpEvento'
              Title.Caption = 'Evento'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'descEvento'
              Title.Caption = 'Descri'#231#227'o do Evento'
              Width = 252
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dhEvento'
              Title.Caption = 'Data / hora evento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpAmb'
              Title.Caption = 'Amb'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS_TXT'
              Title.Caption = 'Descri'#231#227'o do Status'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 870
          Height = 172
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 986
          ExplicitHeight = 166
          ControlData = {
            4C000000EB590000C71100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 451
      Width = 878
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 179
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 130
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 46
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 181
        Top = 15
        Width = 54
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 235
        Top = 15
        Width = 641
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 507
          Top = 0
          Width = 134
          Height = 51
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Tag = 570
          Left = 5
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtEvento: TBitBtn
          Tag = 441
          Left = 126
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Evento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEventoClick
        end
        object BtXML: TBitBtn
          Left = 369
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
        end
        object BtEnvia: TBitBtn
          Tag = 244
          Left = 247
          Top = 5
          Width = 120
          Height = 40
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'E&Mail'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtEnviaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 878
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 826
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 221
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 221
      Top = 0
      Width = 605
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lotes de Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 878
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 874
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeEveRLoE: TDataSource
    DataSet = QrNFeEveRLoE
    Left = 40
    Top = 12
  end
  object QrNFeEveRLoE: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeEveRLoEBeforeOpen
    AfterOpen = QrNFeEveRLoEAfterOpen
    AfterScroll = QrNFeEveRLoEAfterScroll
    OnCalcFields = QrNFeEveRLoECalcFields
    SQL.Strings = (
      'SELECT lot.*, ent.Filial, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa'
      'FROM nfeeverlot lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa'
      ''
      'WHERE lot.Codigo>-1000'
      '')
    Left = 12
    Top = 12
    object QrNFeEveRLoEDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrNFeEveRLoENO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
    object QrNFeEveRLoECodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfeeverlot.Codigo'
    end
    object QrNFeEveRLoECodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'nfeeverlot.CodUsu'
    end
    object QrNFeEveRLoENome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfeeverlot.Nome'
      Size = 50
    end
    object QrNFeEveRLoEEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeeverlot.Empresa'
    end
    object QrNFeEveRLoEversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfeeverlot.versao'
    end
    object QrNFeEveRLoEtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfeeverlot.tpAmb'
    end
    object QrNFeEveRLoEverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfeeverlot.verAplic'
    end
    object QrNFeEveRLoEcOrgao: TSmallintField
      FieldName = 'cOrgao'
      Origin = 'nfeeverlot.cOrgao'
    end
    object QrNFeEveRLoEcStat: TSmallintField
      FieldName = 'cStat'
      Origin = 'nfeeverlot.cStat'
    end
    object QrNFeEveRLoExMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfeeverlot.xMotivo'
      Size = 255
    end
    object QrNFeEveRLoELk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfeeverlot.Lk'
    end
    object QrNFeEveRLoEDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfeeverlot.DataCad'
    end
    object QrNFeEveRLoEDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfeeverlot.DataAlt'
    end
    object QrNFeEveRLoEUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfeeverlot.UserCad'
    end
    object QrNFeEveRLoEUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfeeverlot.UserAlt'
    end
    object QrNFeEveRLoEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfeeverlot.AlterWeb'
    end
    object QrNFeEveRLoEAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfeeverlot.Ativo'
    end
    object QrNFeEveRLoEFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeEveRLoENO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeEveRLoEide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtXML
    CanUpd01 = BtEvento
    Left = 68
    Top = 12
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 12
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 292
    Top = 528
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      OnClick = Lerarquivo1Click
    end
  end
  object PMEvento: TPopupMenu
    OnPopup = PMEventoPopup
    Left = 400
    Top = 540
    object IncluiEventoaoloteatual1: TMenuItem
      Caption = '&Inclui Evento ao lote atual'
      OnClick = IncluiEventoaoloteatual1Click
    end
    object RemoveEventosselecionados1: TMenuItem
      Caption = '&Remove Evento(s) selecionado(s)'
      OnClick = RemoveEventosselecionados1Click
    end
  end
  object DsNFeEveRCab: TDataSource
    DataSet = QrNFeEveRCab
    Left = 160
    Top = 12
  end
  object QrNFeEveRCab: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeEveRCabCalcFields
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento, '
      'Status, ret_cStat, ret_nProt, ret_dhRegEvento, '
      'XML_Eve, XML_retEve, versao, duf.Nome UF_Nome, '
      'cOrgao, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF,'
      'nSeqEvento, verEvento'
      'FROM nfeevercab'
      'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao ')
    Left = 132
    Top = 12
    object QrNFeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrNFeEveRCabCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrNFeEveRCabUF_Nome: TWideStringField
      FieldName = 'UF_Nome'
    end
    object QrNFeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabchNFe_NF_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object QrNFeEveRCabchNFe_NF_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object QrNFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrNFeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrNFeEveRCabSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object PMXML: TPopupMenu
    Left = 628
    Top = 524
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote'
        OnClick = Lote1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaNFEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML da NFE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assina1Click
      end
    end
  end
  object QrProt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens '
      'FROM nfeevercab '
      'WHERE EventoLote=:P0'
      'AND Status in (135,136)')
    Left = 420
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id, ide_serie, ide_nNF, '
      'versao, IDCtrl,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF,'
      'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF,'
      'Transporta_CNPJ, Transporta_CPF, Transporta_xNome,  '
      'Transporta_IE, Transporta_UF '
      'FROM nfecaba'
      'WHERE Id=:P0')
    Left = 448
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrNFeEveRCCe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, xCorrecao, nCondUso'
      'FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      '')
    Left = 476
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCCexCorrecao: TWideMemoField
      FieldName = 'xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
  end
end
