object FmNFeEveRcab: TFmNFeEveRcab
  Tag = 526
  Left = 339
  Top = 185
  Caption = 'NFe-EVENT-001 :: Eventos da NF-e'
  ClientHeight = 606
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 48
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 913
      Top = 0
      Width = 52
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 53
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtLote: TBitBtn
        Tag = 570
        Left = 6
        Top = 4
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLoteClick
      end
    end
    object GB_M: TGroupBox
      Left = 53
      Top = 0
      Width = 860
      Height = 48
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 7
        Width = 208
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 10
        Width = 208
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 8
        Width = 208
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Eventos da NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 965
    Height = 434
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 434
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 222
        Width = 965
        Height = 6
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 965
        Height = 74
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        DataSource = DsA
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'ide_serie'
            Title.Caption = 'S'#233'rie'
            Width = 29
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nNF'
            Title.Caption = 'N'#186' NF'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cStat'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xMotivo'
            Title.Caption = 'Motivo do status'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhRecbto'
            Title.Caption = 'Data/hora fisco'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Cli'
            Title.Caption = 'Cliente'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMSTot_vNF'
            Title.Caption = 'R$ NF'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMSTot_vProd'
            Title.Caption = 'R$ Prod.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Id'
            Title.Caption = 'Chave de acesso da NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dEmi'
            Title.Caption = 'Emiss'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_natOp'
            Title.Caption = 'Natureza da opera'#231#227'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nProt'
            Title.Caption = 'N'#186' do protocolo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_tpNF'
            Title.Caption = 'tpNF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LoteEnv'
            Title.Caption = 'Lote NFe'
            Width = 51
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 74
        Width = 965
        Height = 148
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        DataSource = DsNFeEveRCab
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cOrgao'
            Title.Caption = 'UF'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EventoLote'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descEvento'
            Title.Caption = 'Tipo de evento'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhEvento'
            Title.Caption = 'Data / hora gera'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TZD_UTC'
            Title.Caption = 'Dif UTC'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nSeqEvento'
            Title.Caption = 'nSeq'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tpAmb'
            Title.Caption = 'Amb'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Status'
            Width = 38
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 228
        Width = 965
        Height = 206
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 2
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Carta de Corre'#231#227'o'
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 957
            Height = 178
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataField = 'xCorrecao'
            DataSource = DsNFeEveRCCe
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cancelamento'
          ImageIndex = 1
          object DBGrid4: TDBGrid
            Left = 0
            Top = 0
            Width = 957
            Height = 178
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsNFeEveRCan
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Manifesta'#231#227'o do Destinat'#225'rio'
          ImageIndex = 2
        end
        object TabSheet4: TTabSheet
          Caption = ' EPEC '
          ImageIndex = 3
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 482
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 961
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 536
    Width = 965
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 816
      Top = 15
      Width = 147
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 814
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCCe: TBitBtn
        Tag = 10091
        Left = 4
        Top = 5
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Carta corr.'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCCeClick
      end
      object BtCan: TBitBtn
        Tag = 455
        Left = 126
        Top = 5
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Cancelar NF&-e.'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtCanClick
      end
      object BtMDe: TBitBtn
        Tag = 560
        Left = 248
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Manifestar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtMDeClick
      end
      object BtEPEC: TBitBtn
        Left = 370
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'E&PEC'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtEPECClick
      end
    end
  end
  object QrA: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAAfterOpen
    BeforeClose = QrABeforeClose
    AfterScroll = QrAAfterScroll
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ide_verProc, ide_hEmi, ide_dhEmiTZD,'
      'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF,'
      'dest_xNome,'
      ''
      'emit_xNome, emit_CPF, CodInfoEmit, CodInfoDest,'
      'emit_xLgr, emit_nro, emit_xCpl, emit_xBairro,'
      'emit_xMun, emit_UF, emit_CEP, emit_xPais,'
      'emit_IE, emit_IEST, emit_CNPJ, emit_fone,'
      'dest_xLgr, dest_nro, dest_xCpl, dest_xBairro, dest_xMun,'
      'dest_CEP, dest_fone, dest_IE, dest_UF,'
      'dest_idEstrangeiro,'
      'transporta_CNPJ, transporta_CPF,'
      'transporta_IE, transporta_UF,'
      'ide_tpAmb, infProt_nProt, infProt_dhRecbto,'
      'ide_DSaiEnt, ModFrete,'
      ''
      'IF(Importado>0, Status,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao,'
      'ide_tpEmis, infCanc_xJust, Status,'
      'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg,'
      'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc,'
      'ICMSTot_vBC, ICMSTot_vICMS,'
      'infCanc_dhRecbto, infCanc_nProt'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON'
      'cli.Codigo=nfa.CodInfoDest'
      'WHERE nfa.FatID=1'
      'AND nfa.FatNum=1883'
      'AND nfa.Empresa=-11'
      '')
    Left = 56
    Top = 140
    object QrANO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrANOME_tpNF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpNF'
      Size = 1
      Calculated = True
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAcStat: TFloatField
      FieldName = 'cStat'
    end
    object QrAID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrAEMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrATRANSPORTA_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrAEMIT_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_CPF_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IEST_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IEST_TXT'
      Size = 100
      Calculated = True
    end
    object QrADEST_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrADEST_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CEP_TXT'
      Calculated = True
    end
    object QrADEST_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_FONE_TXT'
      Size = 50
      Calculated = True
    end
    object QrADEST_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TXT'
      Size = 100
      Calculated = True
    end
    object QrADEST_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrAMODFRETE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MODFRETE_TXT'
      Calculated = True
    end
    object QrADEST_XMUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_XMUN_TXT'
      Size = 60
      Calculated = True
    end
    object QrADOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrAide_DSaiEnt_Txt: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_DSaiEnt_Txt'
      Size = 10
      Calculated = True
    end
    object QrADOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrAdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrAtransporta_CNPJ: TWideStringField
      FieldName = 'transporta_CNPJ'
      Size = 14
    end
    object QrAtransporta_CPF: TWideStringField
      FieldName = 'transporta_CPF'
      Size = 11
    end
    object QrAtransporta_IE: TWideStringField
      FieldName = 'transporta_IE'
    end
    object QrAtransporta_UF: TWideStringField
      FieldName = 'transporta_UF'
      Size = 2
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAide_DSaiEnt: TDateField
      FieldName = 'ide_DSaiEnt'
    end
    object QrAModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrAemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
      Required = True
    end
    object QrAdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 56
    Top = 188
  end
  object PMCCe: TPopupMenu
    OnPopup = PMCCePopup
    Left = 44
    Top = 392
    object Crianovacorreo1: TMenuItem
      Caption = '&Cria nova corre'#231#227'o'
      OnClick = Crianovacorreo1Click
    end
    object Alteracorreo1: TMenuItem
      Caption = '&Altera corre'#231#227'o'
      Enabled = False
      OnClick = Alteracorreo1Click
    end
    object Excluicorreo1: TMenuItem
      Caption = '&Exclui corre'#231#227'o'
      Enabled = False
      OnClick = Excluicorreo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Imprime1: TMenuItem
      Caption = '&Imprime'
      OnClick = Imprime1Click
    end
  end
  object QrNFeEveRCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeEveRCabBeforeClose
    AfterScroll = QrNFeEveRCabAfterScroll
    OnCalcFields = QrNFeEveRCabCalcFields
    SQL.Strings = (
      'SELECT * FROM nfeevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 224
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrNFeEveRCabId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrNFeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrNFeEveRCabCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrNFeEveRCabCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrNFeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEveRCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEveRCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEveRCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEveRCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEveRCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEveRCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeEveRCabTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrNFeEveRCabtpAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tpAmb_TXT'
      Size = 15
      Calculated = True
    end
    object QrNFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrNFeEveRCabret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrNFeEveRCabret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrNFeEveRCabret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrNFeEveRCabret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrNFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCabret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrNFeEveRCabret_chNFe: TWideStringField
      FieldName = 'ret_chNFe'
      Size = 44
    end
    object QrNFeEveRCabret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrNFeEveRCabret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrNFeEveRCabret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrNFeEveRCabret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrNFeEveRCabret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrNFeEveRCabret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrNFeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRCabret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrNFeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
  end
  object DsNFeEveRCab: TDataSource
    DataSet = QrNFeEveRCab
    Left = 224
    Top = 188
  end
  object QrNFeEveRCCe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 308
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCCeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCCeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCCeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCCeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCCexCorrecao: TWideMemoField
      FieldName = 'xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
    object QrNFeEveRCCeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEveRCCeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEveRCCeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEveRCCeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEveRCCeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEveRCCeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEveRCCeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeEveRCCe: TDataSource
    DataSet = QrNFeEveRCCe
    Left = 308
    Top = 188
  end
  object QrERC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercab '
      'WHERE Controle=:P0')
    Left = 160
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrERCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrERCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrERCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrERCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrERCEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrERCId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrERCcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrERCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrERCTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrERCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrERCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrERCchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrERCdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrERCTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrERCverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrERCtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrERCnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrERCversao: TFloatField
      FieldName = 'versao'
    end
    object QrERCdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
  end
  object PMCan: TPopupMenu
    OnPopup = PMCanPopup
    Left = 164
    Top = 396
    object IncluicancelamentodaNFe1: TMenuItem
      Caption = '&Inclui cancelamento da NFe'
      OnClick = IncluicancelamentodaNFe1Click
    end
    object ExcluicancelamentodaNFe1: TMenuItem
      Caption = '&Exclui cancelamento da NFe'
      OnClick = ExcluicancelamentodaNFe1Click
    end
  end
  object QrNFeEveRCan: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercan'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 388
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCanFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCanFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCanEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCanControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCanConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCannProt: TLargeintField
      FieldName = 'nProt'
    end
    object QrNFeEveRCannJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrNFeEveRCanxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrNFeEveRCanLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object DsNFeEveRCan: TDataSource
    DataSet = QrNFeEveRCan
    Left = 388
    Top = 188
  end
  object frxEveCCe: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 40927.443534282400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Altura: Extended;                                      '
      'begin'
      '  if <LogoNFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      'end.')
    OnGetValue = frxEveCCeGetValue
    Left = 108
    Top = 188
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsNFeEveRCab
        DataSetName = 'frxDsNFeEveRCab'
      end
      item
        DataSet = frxDsNFeEveRCCe
        DataSetName = 'frxDsNFeEveRCCe'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795275590000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 173.858267720000000000
        Top = 79.370130000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 177.637817240000000000
          Height = 75.590558500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322737010000000000
          Width = 124.724409450000000000
          Height = 75.590558500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 37.795275590551200000
          Width = 427.086675200000000000
          Height = 136.062992130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          AllowVectorExport = True
          Left = 35.905511810000000000
          Top = 41.574810470000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          Frame.Typ = []
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 457.322834650000000000
          Top = 75.590548739999990000
          Width = 302.362204720000000000
          Height = 60.472443390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 171.968621100000000000
          Top = 41.574830000000010000
          Width = 287.244280000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 171.968621100000000000
          Top = 79.370130000000000000
          Width = 283.464750000000000000
          Height = 77.480314960000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 171.968621100000000000
          Top = 156.850401020000000000
          Width = 283.464750000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 154.960629920000000000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 457.322805350000000000
          Top = 136.062992130000000000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 159.385736460000000000
          Width = 221.480290550000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 480.755881100000000000
          Top = 137.708556460000000000
          Width = 274.393710550000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 582.047375900000000000
          Top = 53.669181499999990000
          Width = 64.251966060000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Folha:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 646.299188190000000000
          Top = 53.669181499999990000
          Width = 113.385851180000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCanhotoRec: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 427.086497010000000000
          Height = 37.795280470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CARTA DE CORRE'#199#195'O ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 177.637910000000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Evento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 582.157861100000000000
          Top = 37.795309759999990000
          Width = 64.251966060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora:')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 22.677180000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sequencia:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 457.433371100000000000
          Top = 37.795309759999990000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 495.228671100000000000
          Top = 37.795309759999990000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 52.913400470000010000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 52.913400470000010000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 22.677180000000010000
          Width = 22.677180000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Top = 22.677180000000010000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 124.724490000000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299188190000000000
          Top = 22.677180000000010000
          Width = 113.385858500000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."nSeqEvento"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299188190000000000
          Top = 37.795300000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."dhEvento"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 75.590600000000010000
          Width = 302.362400000000000000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Registro no Fisco')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 98.267780000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 98.267780000000000000
          Width = 241.889920000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."ret_nProt"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 113.385900000000000000
          Width = 68.031540000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000100000
          Top = 113.385900000000000000
          Width = 234.330860000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."ret_dhRegEvento"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 820.157497400000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 529.133929060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 525.354399060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 461.102432990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 457.322902990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 491.338653460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 491.338653460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 676.535503860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 676.535503860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 378.330779450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 374.551249450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 408.567019450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 408.567019450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 531.779598340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 531.779598340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 109.606370000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DA CORRE'#199#195'O:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 124.724490000000000000
          Width = 733.606599450000000000
          Height = 491.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCCe."xCorrecao"]')
          ParentFont = False
          WordBreak = True
        end
        object Memo12: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 616.063390000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CONDI'#199#195'O DE USO ACEITA:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 631.181509999999900000
          Width = 733.606599450000000000
          Height = 188.976377950000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[xCondUso]')
          ParentFont = False
          WordBreak = True
        end
      end
    end
  end
  object frxDsA: TfrxDBDataset
    UserName = 'frxDsA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Cli=NO_Cli'
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'Id=Id'
      'ide_natOp=ide_natOp'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_tpNF=ide_tpNF'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vNF=ICMSTot_vNF'
      'nProt=nProt'
      'xMotivo=xMotivo'
      'dhRecbto=dhRecbto'
      'IDCtrl=IDCtrl'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'versao=versao'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ide_tpEmis=ide_tpEmis'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'infCanc_xJust=infCanc_xJust'
      'Status=Status'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'cStat=cStat'
      'ID_TXT=ID_TXT'
      'EMIT_ENDERECO=EMIT_ENDERECO'
      'EMIT_FONE_TXT=EMIT_FONE_TXT'
      'EMIT_IE_TXT=EMIT_IE_TXT'
      'TRANSPORTA_CNPJ_CPF_TXT=TRANSPORTA_CNPJ_CPF_TXT'
      'TRANSPORTA_IE_TXT=TRANSPORTA_IE_TXT'
      'EMIT_CNPJ_CPF_TXT=EMIT_CNPJ_CPF_TXT'
      'EMIT_IEST_TXT=EMIT_IEST_TXT'
      'DEST_ENDERECO=DEST_ENDERECO'
      'DEST_CEP_TXT=DEST_CEP_TXT'
      'DEST_FONE_TXT=DEST_FONE_TXT'
      'DEST_CNPJ_CPF_TXT=DEST_CNPJ_CPF_TXT'
      'DEST_IE_TXT=DEST_IE_TXT'
      'MODFRETE_TXT=MODFRETE_TXT'
      'DEST_XMUN_TXT=DEST_XMUN_TXT'
      'DOC_SEM_VLR_JUR=DOC_SEM_VLR_JUR'
      'ide_DSaiEnt_Txt=ide_DSaiEnt_Txt'
      'DOC_SEM_VLR_FIS=DOC_SEM_VLR_FIS'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_xMun=emit_xMun'
      'emit_UF=emit_UF'
      'emit_CEP=emit_CEP'
      'emit_xPais=emit_xPais'
      'emit_IE=emit_IE'
      'emit_IEST=emit_IEST'
      'emit_CNPJ=emit_CNPJ'
      'emit_fone=emit_fone'
      'dest_xLgr=dest_xLgr'
      'dest_nro=dest_nro'
      'dest_xCpl=dest_xCpl'
      'dest_xMun=dest_xMun'
      'dest_CEP=dest_CEP'
      'dest_fone=dest_fone'
      'dest_IE=dest_IE'
      'dest_UF=dest_UF'
      'transporta_CNPJ=transporta_CNPJ'
      'transporta_CPF=transporta_CPF'
      'transporta_IE=transporta_IE'
      'transporta_UF=transporta_UF'
      'ide_tpAmb=ide_tpAmb'
      'infProt_nProt=infProt_nProt'
      'infProt_dhRecbto=infProt_dhRecbto'
      'ide_DSaiEnt=ide_DSaiEnt'
      'ModFrete=ModFrete'
      'emit_xNome=emit_xNome'
      'dest_xBairro=dest_xBairro')
    DataSet = QrA
    BCDToCurrency = False
    DataSetOptions = []
    Left = 108
    Top = 140
  end
  object frxDsNFeEveRCCe: TfrxDBDataset
    UserName = 'frxDsNFeEveRCCe'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'Conta=Conta'
      'xCorrecao=xCorrecao'
      'nCondUso=nCondUso'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrNFeEveRCCe
    BCDToCurrency = False
    DataSetOptions = []
    Left = 312
    Top = 236
  end
  object frxDsNFeEveRCab: TfrxDBDataset
    UserName = 'frxDsNFeEveRCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'EventoLote=EventoLote'
      'Id=Id'
      'cOrgao=cOrgao'
      'tpAmb=tpAmb'
      'TipoEnt=TipoEnt'
      'CNPJ=CNPJ'
      'CPF=CPF'
      'chNFe=chNFe'
      'dhEvento=dhEvento'
      'verEvento=verEvento'
      'tpEvento=tpEvento'
      'nSeqEvento=nSeqEvento'
      'versao=versao'
      'descEvento=descEvento'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'TZD_UTC=TZD_UTC'
      'tpAmb_TXT=tpAmb_TXT'
      'Status=Status'
      'XML_Eve=XML_Eve'
      'XML_retEve=XML_retEve'
      'ret_versao=ret_versao'
      'ret_Id=ret_Id'
      'ret_tpAmb=ret_tpAmb'
      'ret_verAplic=ret_verAplic'
      'ret_cOrgao=ret_cOrgao'
      'ret_cStat=ret_cStat'
      'ret_xMotivo=ret_xMotivo'
      'ret_chNFe=ret_chNFe'
      'ret_tpEvento=ret_tpEvento'
      'ret_xEvento=ret_xEvento'
      'ret_nSeqEvento=ret_nSeqEvento'
      'ret_CNPJDest=ret_CNPJDest'
      'ret_CPFDest=ret_CPFDest'
      'ret_emailDest=ret_emailDest'
      'ret_dhRegEvento=ret_dhRegEvento'
      'ret_TZD_UTC=ret_TZD_UTC'
      'ret_nProt=ret_nProt')
    DataSet = QrNFeEveRCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 224
    Top = 236
  end
  object PMMDe: TPopupMenu
    OnPopup = PMMDePopup
    Left = 284
    Top = 396
    object Incluimanifestaodedestinatrio1: TMenuItem
      Caption = '&Inclui manifesta'#231#227'o de destinat'#225'rio'
      OnClick = Incluimanifestaodedestinatrio1Click
    end
    object Excluimanifestaodedestinatrio1: TMenuItem
      Caption = '&Exclui manifesta'#231#227'o de destinat'#225'rio'
      OnClick = Excluimanifestaodedestinatrio1Click
    end
    object AtorinteressadoTransportadora1: TMenuItem
      Caption = 'Ator interessado: Transportadora'
      OnClick = AtorinteressadoTransportadora1Click
    end
  end
  object QrNFeEveRMDe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevermde'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 472
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRMDeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRMDeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRMDeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRMDeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRMDeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRMDecSitConf: TSmallintField
      FieldName = 'cSitConf'
    end
    object QrNFeEveRMDetpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRMDenJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrNFeEveRMDexJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
  end
  object DsNFeEveRMDe: TDataSource
    DataSet = QrNFeEveRMDe
    Left = 472
    Top = 188
  end
  object PmEPEC: TPopupMenu
    OnPopup = PmEPECPopup
    Left = 420
    Top = 400
    object IncluinovoEPEC1: TMenuItem
      Caption = 'Inclui novo EPEC'
      OnClick = IncluinovoEPEC1Click
    end
    object AlteraEPEC1: TMenuItem
      Caption = '&Altera EPEC'
      OnClick = AlteraEPEC1Click
    end
    object ExcluiEPEC1: TMenuItem
      Caption = '&Exclui EPEC'
      OnClick = ExcluiEPEC1Click
    end
  end
  object DsNFeEveREPEC: TDataSource
    DataSet = QrNFeEveREPEC
    Left = 568
    Top = 188
  end
  object QrNFeEveREPEC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeeverepec'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 568
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveREPECFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrNFeEveREPECFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFeEveREPECEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrNFeEveREPECControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNFeEveREPECConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrNFeEveREPECcOrgaoAutor: TSmallintField
      FieldName = 'cOrgaoAutor'
      Required = True
    end
    object QrNFeEveREPECtpAutor: TSmallintField
      FieldName = 'tpAutor'
      Required = True
    end
    object QrNFeEveREPECverAplic: TWideStringField
      FieldName = 'verAplic'
      Required = True
    end
    object QrNFeEveREPECdhEmi: TDateTimeField
      FieldName = 'dhEmi'
      Required = True
    end
    object QrNFeEveREPECdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
      Required = True
    end
    object QrNFeEveREPECtpNF: TSmallintField
      FieldName = 'tpNF'
      Required = True
    end
    object QrNFeEveREPECIE: TWideStringField
      FieldName = 'IE'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Required = True
      Size = 2
    end
    object QrNFeEveREPECdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Required = True
      Size = 11
    end
    object QrNFeEveREPECdest_idEstrangeiro: TWideStringField
      FieldName = 'dest_idEstrangeiro'
      Required = True
    end
    object QrNFeEveREPECdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Required = True
      Size = 14
    end
    object QrNFeEveREPECvNF: TFloatField
      FieldName = 'vNF'
      Required = True
    end
    object QrNFeEveREPECvICMS: TFloatField
      FieldName = 'vICMS'
      Required = True
    end
    object QrNFeEveREPECvST: TFloatField
      FieldName = 'vST'
      Required = True
    end
  end
end
