object FmNFeEveMail: TFmNFeEveMail
  Left = 339
  Top = 185
  Caption = 'NFe-EMAIL-002 :: Envio de Evento de NF-e'
  ClientHeight = 632
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 106
      Width = 784
      Height = 364
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 281
        Width = 784
        Height = 83
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 4
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 135
          Height = 13
          Caption = 'Caminho do XML do evento:'
        end
        object SpeedButton2: TSpeedButton
          Left = 752
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object Label7: TLabel
          Left = 4
          Top = 44
          Width = 46
          Height = 13
          Caption = 'Pr'#233'-email:'
        end
        object EdNFeXML: TdmkEdit
          Left = 4
          Top = 20
          Width = 749
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdPreEmail: TdmkEditCB
          Left = 4
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdPreEmailChange
          DBLookupComboBox = CBPreEmail
          IgnoraDBLookupComboBox = False
        end
        object CBPreEmail: TdmkDBLookupComboBox
          Left = 60
          Top = 60
          Width = 712
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPre
          TabOrder = 2
          dmkEditCB = EdPreEmail
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 57
        Align = alTop
        Caption = ' Dados da NF-e: '
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 0
            Width = 72
            Height = 13
            Caption = 'Chave da NFe:'
          end
          object Label6: TLabel
            Left = 312
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label10: TLabel
            Left = 348
            Top = 0
            Width = 38
            Height = 13
            Caption = 'N'#186' NFe:'
          end
          object Label2: TLabel
            Left = 420
            Top = 0
            Width = 33
            Height = 13
            Caption = 'Status:'
          end
          object Label9: TLabel
            Left = 648
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object Label3: TLabel
            Left = 696
            Top = 0
            Width = 56
            Height = 13
            Caption = 'ID Controle:'
          end
          object EdNFeId: TdmkEdit
            Left = 4
            Top = 16
            Width = 304
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtChaveNFe
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSerie: TdmkEdit
            Left = 312
            Top = 16
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdNumeroNFe: TdmkEdit
            Left = 348
            Top = 16
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdNFeStatus: TdmkEdit
            Left = 420
            Top = 16
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdNFeStatus_TXT: TEdit
            Left = 468
            Top = 16
            Width = 177
            Height = 21
            ReadOnly = True
            TabOrder = 4
          end
          object EdVersaoNFe: TdmkEdit
            Left = 648
            Top = 16
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdNFeIDCtrl: TdmkEdit
            Left = 696
            Top = 16
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 57
        Width = 784
        Height = 56
        Align = alTop
        Caption = ' Emitente: '
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 0
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
          end
          object Label11: TLabel
            Left = 132
            Top = 0
            Width = 105
            Height = 13
            Caption = 'Nome / Raz'#227'o Social:'
          end
          object Label12: TLabel
            Left = 612
            Top = 0
            Width = 19
            Height = 13
            Caption = 'I.E.:'
          end
          object Label13: TLabel
            Left = 740
            Top = 0
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object EdemitCNPJ_CPF: TdmkEdit
            Left = 4
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object Edemit_xNome: TdmkEdit
            Left = 132
            Top = 16
            Width = 477
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdemitIE: TdmkEdit
            Left = 612
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdemitUF: TdmkEdit
            Left = 740
            Top = 16
            Width = 32
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 113
        Width = 784
        Height = 56
        Align = alTop
        Caption = ' Destinat'#225'rio: '
        TabOrder = 2
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label14: TLabel
            Left = 4
            Top = 0
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
          end
          object Label15: TLabel
            Left = 132
            Top = 0
            Width = 105
            Height = 13
            Caption = 'Nome / Raz'#227'o Social:'
          end
          object Label16: TLabel
            Left = 612
            Top = 0
            Width = 19
            Height = 13
            Caption = 'I.E.:'
          end
          object Label17: TLabel
            Left = 740
            Top = 0
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object EddestCNPJ_CPF: TdmkEdit
            Left = 4
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object Eddest_xNome: TdmkEdit
            Left = 132
            Top = 16
            Width = 477
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EddestIE: TdmkEdit
            Left = 612
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EddestUF: TdmkEdit
            Left = 740
            Top = 16
            Width = 32
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 225
        Width = 784
        Height = 56
        Align = alTop
        Caption = ' Evento: '
        TabOrder = 3
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label18: TLabel
            Left = 4
            Top = 0
            Width = 55
            Height = 13
            Caption = 'Ocorr'#234'ncia:'
          end
          object Label20: TLabel
            Left = 536
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
          end
          object Label8: TLabel
            Left = 412
            Top = 0
            Width = 100
            Height = 13
            Caption = 'Data / hora gera'#231#227'o:'
          end
          object Label19: TLabel
            Left = 644
            Top = 0
            Width = 105
            Height = 13
            Caption = 'Data / hora protocolo:'
          end
          object EddescEvento: TdmkEdit
            Left = 64
            Top = 16
            Width = 344
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object Edret_nProt: TdmkEdit
            Left = 536
            Top = 16
            Width = 104
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdtpEvento: TdmkEdit
            Left = 4
            Top = 16
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EddhEvento: TdmkEdit
            Left = 412
            Top = 16
            Width = 120
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object Edret_dhregEvento: TdmkEdit
            Left = 644
            Top = 16
            Width = 128
            Height = 21
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 169
        Width = 784
        Height = 56
        Align = alTop
        Caption = ' Transportador: '
        TabOrder = 5
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label21: TLabel
            Left = 4
            Top = 0
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
          end
          object Label22: TLabel
            Left = 132
            Top = 0
            Width = 105
            Height = 13
            Caption = 'Nome / Raz'#227'o Social:'
          end
          object Label23: TLabel
            Left = 612
            Top = 0
            Width = 19
            Height = 13
            Caption = 'I.E.:'
          end
          object Label24: TLabel
            Left = 740
            Top = 0
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object EdtrspCNPJ_CPF: TdmkEdit
            Left = 4
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object Edtrsp_xNome: TdmkEdit
            Left = 132
            Top = 16
            Width = 477
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdtrspIE: TdmkEdit
            Left = 612
            Top = 16
            Width = 124
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdtrspUF: TdmkEdit
            Left = 740
            Top = 16
            Width = 32
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 784
      Height = 106
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Emails de destino '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade: TStringGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 78
          Align = alClient
          ColCount = 4
          DefaultColWidth = 24
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          ColWidths = (
            24
            147
            190
            394)
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Mensagem texto '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGMsg: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 237
          Height = 78
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipo'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Nome'
              Width = 90
              Visible = True
            end>
          Color = clWindow
          DataSource = DModG.DsPreEmMsg
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipo'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Nome'
              Width = 90
              Visible = True
            end>
        end
        object MeMsg: TDBMemo
          Left = 237
          Top = 0
          Width = 539
          Height = 78
          Align = alClient
          DataField = 'Texto'
          DataSource = DModG.DsPreEmMsg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Codifica'#231#227'o de imagens '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object MeEncode: TMemo
          Left = 165
          Top = 0
          Width = 611
          Height = 78
          Align = alClient
          TabOrder = 0
          ExplicitLeft = 0
          ExplicitWidth = 776
        end
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 165
          Height = 78
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object CkRecriarImagens: TCheckBox
            Left = 8
            Top = 5
            Width = 157
            Height = 17
            Caption = 'For'#231'ar recria'#231#227'o de imagens.'
            TabOrder = 0
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 302
        Height = 32
        Caption = 'Envio de Evento de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 302
        Height = 32
        Caption = 'Envio de Evento de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 302
        Height = 32
        Caption = 'Envio de Evento de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 518
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 562
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 145
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtEnvia: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtEnviaClick
        end
      end
      object GroupBox6: TGroupBox
        Left = 145
        Top = 0
        Width = 491
        Height = 53
        Align = alClient
        TabOrder = 1
        object LaEvento1: TLabel
          Left = 9
          Top = 11
          Width = 248
          Height = 32
          Caption = 'Controle do Evento: '
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LaEvento2: TLabel
          Left = 8
          Top = 10
          Width = 248
          Height = 32
          Caption = 'Controle do Evento: '
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
  end
  object QrEmails: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome, etm.EMail'
      'FROM entimail etm'
      'LEFT JOIN enticontat ent ON ent.Controle=etm.Controle'
      'WHERE etm.Codigo=:P0'
      'AND etm.EntiTipCto=:P1')
    Left = 8
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmailsEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
    object QrEmailsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 312
    Top = 244
  end
  object QrPre: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pem.Codigo, pem.Nome'
      'FROM preemail pem'
      'ORDER BY pem.Nome')
    Left = 256
    Top = 244
    object QrPreCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPre: TDataSource
    DataSet = QrPre
    Left = 284
    Top = 244
  end
  object IdSMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 340
    Top = 244
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 368
    Top = 244
  end
  object QrNFeEveRCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nec.FatID, nec.FatNum, nec.Empresa, nec.Controle,  '
      
        'nec.tpAmb, nec.chNFe, nec.dhEvento, nec.tpEvento, nec.descEvento' +
        ',  '
      'nec.Status, nec.ret_cStat, nec.ret_nProt, nec.ret_dhRegEvento,  '
      'nec.XML_Eve, nec.XML_retEve, nec.versao, nec.cOrgao, '
      'nec.verEvento, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF, '
      'duf.Nome UF_Nome, nSeqEvento  '
      'FROM nfeevercab nec '
      'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao ')
    Left = 380
    Top = 8
    object QrNFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeEveRCabchNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrNFeEveRCabchNFe_NF_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_SER'
      Size = 3
      Calculated = True
    end
    object QrNFeEveRCabchNFe_NF_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chNFe_NF_NUM'
      Size = 9
      Calculated = True
    end
    object QrNFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrNFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrNFeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrNFeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCabSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
    object QrNFeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeEveRCabUF_Nome: TWideStringField
      FieldName = 'UF_Nome'
    end
    object QrNFeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeEveRCabCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrNFeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrNFeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
  end
  object QrNFeEveRCCe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, xCorrecao, nCondUso'
      'FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      '')
    Left = 408
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeEveRCCexCorrecao: TWideMemoField
      FieldName = 'xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
  end
end
