unit NFeEveMail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkEdit, DB,
  mySQLDbTables, frxClass, frxExportPDF, ShellAPI, UnDmkProcFunc, DmkDAC_PF,
  // Indy
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, IdText,
  IdAttachment, IdBaseComponent, IdMessage, ActiveX, ComObj, IdAttachmentFile,
  IdExplicitTLSClientServerBase, IdSMTPBase, DBCtrls, dmkDBGrid, ComCtrls,
  dmkDBLookupComboBox, dmkEditCB, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  // Fim Indy
  dmkImage, UnInternalConsts, UnDmkEnums;
type
  TFmNFeEveMail = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrEmails: TmySQLQuery;
    QrEmailsEMail: TWideStringField;
    frxPDFExport: TfrxPDFExport;
    QrPre: TmySQLQuery;
    DsPre: TDataSource;
    IdSMTP1: TIdSMTP;
    QrEmailsNome: TWideStringField;
    QrPreCodigo: TIntegerField;
    QrPreNome: TWideStringField;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    Panel5: TPanel;
    Label4: TLabel;
    EdNFeXML: TdmkEdit;
    SpeedButton2: TSpeedButton;
    Label7: TLabel;
    EdPreEmail: TdmkEditCB;
    CBPreEmail: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    EdNFeId: TdmkEdit;
    EdSerie: TdmkEdit;
    Label6: TLabel;
    EdNumeroNFe: TdmkEdit;
    Label10: TLabel;
    Label2: TLabel;
    EdNFeStatus: TdmkEdit;
    EdNFeStatus_TXT: TEdit;
    EdVersaoNFe: TdmkEdit;
    Label9: TLabel;
    Label3: TLabel;
    EdNFeIDCtrl: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label5: TLabel;
    EdemitCNPJ_CPF: TdmkEdit;
    Edemit_xNome: TdmkEdit;
    Label11: TLabel;
    EdemitIE: TdmkEdit;
    Label12: TLabel;
    EdemitUF: TdmkEdit;
    Label13: TLabel;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EddestCNPJ_CPF: TdmkEdit;
    Eddest_xNome: TdmkEdit;
    EddestIE: TdmkEdit;
    EddestUF: TdmkEdit;
    GroupBox4: TGroupBox;
    Panel9: TPanel;
    Label18: TLabel;
    Label20: TLabel;
    EddescEvento: TdmkEdit;
    Edret_nProt: TdmkEdit;
    EdtpEvento: TdmkEdit;
    Label8: TLabel;
    EddhEvento: TdmkEdit;
    Label19: TLabel;
    Edret_dhregEvento: TdmkEdit;
    GroupBox5: TGroupBox;
    Panel10: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdtrspCNPJ_CPF: TdmkEdit;
    Edtrsp_xNome: TdmkEdit;
    EdtrspIE: TdmkEdit;
    EdtrspUF: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade: TStringGrid;
    TabSheet2: TTabSheet;
    DBGMsg: TdmkDBGrid;
    MeMsg: TDBMemo;
    TabSheet3: TTabSheet;
    MeEncode: TMemo;
    Panel11: TPanel;
    BtEnvia: TBitBtn;
    GroupBox6: TGroupBox;
    LaEvento1: TLabel;
    LaEvento2: TLabel;
    Panel12: TPanel;
    CkRecriarImagens: TCheckBox;
    QrNFeEveRCab: TmySQLQuery;
    QrNFeEveRCabFatID: TIntegerField;
    QrNFeEveRCabFatNum: TIntegerField;
    QrNFeEveRCabEmpresa: TIntegerField;
    QrNFeEveRCabControle: TIntegerField;
    QrNFeEveRCabtpAmb: TSmallintField;
    QrNFeEveRCabchNFe: TWideStringField;
    QrNFeEveRCabdhEvento: TDateTimeField;
    QrNFeEveRCabtpEvento: TIntegerField;
    QrNFeEveRCabdescEvento: TWideStringField;
    QrNFeEveRCabchNFe_NF_SER: TWideStringField;
    QrNFeEveRCabchNFe_NF_NUM: TWideStringField;
    QrNFeEveRCabret_cStat: TIntegerField;
    QrNFeEveRCabStatus: TIntegerField;
    QrNFeEveRCabret_nProt: TWideStringField;
    QrNFeEveRCabret_dhRegEvento: TDateTimeField;
    QrNFeEveRCabXML_Eve: TWideMemoField;
    QrNFeEveRCabXML_retEve: TWideMemoField;
    QrNFeEveRCabSTATUS_TXT: TWideStringField;
    QrNFeEveRCabversao: TFloatField;
    QrNFeEveRCabUF_Nome: TWideStringField;
    QrNFeEveRCabcOrgao: TSmallintField;
    QrNFeEveRCabCNPJ_CPF: TWideStringField;
    QrNFeEveRCabverEvento: TFloatField;
    QrNFeEveRCabnSeqEvento: TSmallintField;
    QrNFeEveRCCe: TmySQLQuery;
    QrNFeEveRCCeConta: TIntegerField;
    QrNFeEveRCCexCorrecao: TWideMemoField;
    QrNFeEveRCCenCondUso: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure EdPreEmailChange(Sender: TObject);
  private
    { Private declarations }
    FAutorizada, FAtivou: Boolean;
    FIdMessage: TIdMessage;
    function EnviaEmeio(ArqPDF, ArqXML: String; AnexaArq: Boolean): Boolean;
    function SubstituiVariaveis(Texto: String): String;
    procedure Info(Msg: String);
  public
    { Public declarations }
    FXML_Evento, FXML_Protocolo: String;
    //
    procedure ReopenNFeEveRCab(EventoLote: Integer);
  end;

  var
  FmNFeEveMail: TFmNFeEveMail;

implementation

uses UnMyObjects, dmkGeral, ModuleGeral, NFeXMLGerencia, ModuleNFe_0000,
  UMySQLModule, NFe_Tabs, Module;

const
  FCaminhoEMail = 'C:\Dermatek\NFe\Emails\';
  FIniPath      = 'C:\Dermatek\Base64\';
  FBase64       = 'C:\Dermatek\Base64\Base64.exe';
  SInterno      = 'Interno';

{$R *.DFM}

procedure TFmNFeEveMail.BtEnviaClick(Sender: TObject);
var
  Protocolo, ArqXML: String;
  Continua: Boolean;
  //XML_Evento, XML_Protocolo,
  XML_Distribuicao: String;
begin
  //
  // XML
  //
  Info('Verificando exist�ncia do arquivo XML da Carta de Corre��o');
  Protocolo := Geral.SoNumero_TT(Edret_nProt.Text);
  ArqXML := EdNFeXML.Text;
  Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de NFe', True);
  ArqXML := ArqXML + Protocolo + NFE_EXT_EVE_PRO_CCE_XML;
  if not FileExists(ArqXML) then
  begin
    Info('Gerando arquivo XML da Carta de Corre��o');
    //
    Continua := False;
    XML_Distribuicao := '';
    if NFeXMLGeren.XML_DistribuiCartaDeCorrecao(verProcCCeNFe_Versao,
    FXML_Evento, FXML_Protocolo, XML_Distribuicao) then
      Continua := NFeXMLGeren.SalvaXML(ArqXML, XML_Distribuicao);
  end else Continua := True;
  if not Continua then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  EnviaEmeio('', ArqXML, True);
end;

function TFmNFeEveMail.EnviaEmeio(ArqPDF, ArqXML: String; AnexaArq: Boolean): Boolean;
  procedure AnexaPDF();
  begin
    if ArqPDF <> '' then
    begin
      with TIdAttachmentFile.Create(FIdMessage.MessageParts, ArqPDF) do
      begin
        ContentType        := 'application/pdf';
        ContentDisposition := 'attachment';
        ContentTransfer    := 'base64';
      end;
    end;
  end;

  //
  procedure AnexaXML();
  begin
    with TIdAttachmentFile.Create(FIdMessage.MessageParts, ArqXML) do
    begin
      ContentType        := 'application/xml';
      ContentDisposition := 'attachment';
      ContentTransfer    := 'base64';
    end;
  end;

  //

  procedure AnexaTextoPlano;
  var
    Texto: String;
  begin
    if DmodG.QrPEM_0.RecordCount > 0 then
    begin
      Texto := '';
      DmodG.QrPEM_0.First;
      while not DmodG.QrPEM_0.Eof do
      begin
        Texto := Texto + #13+#10 + SubstituiVariaveis(DmodG.QrPEM_0Texto.Value);
        DmodG.QrPEM_0.Next;
      end;
    // � obrigat�rio mandar um texto plano
    end else Texto := 'E-mail sem texto plano!!!!';
    with TIdText.Create(FIdMessage.MessageParts) do
    begin
      Body.Text       := Texto;
      ContentTransfer := '7bit';
      ContentType     := 'text/plain';
      ParentPart      := 1;
    end;
  end;

  //

  procedure AnexaHTMLs;
  var
    TextoHTML: TIdText;
  begin
    if DmodG.QrPEM_1.RecordCount > 0 then
    begin
      DmodG.QrPEM_1.First;
      TextoHTML := TIdText.Create(FIdMessage.MessageParts);
      with TextoHTML do
      begin
        ContentTransfer := '7bit';
        ContentType     := 'text/html';
        ParentPart      := 1;
        Body.Text       := '';
        while not DmodG.QrPEM_1.Eof do
        begin
          Body.Text := Body.Text +'<br/>' + SubstituiVariaveis(DmodG.QrPEM_1Texto.Value);
          DmodG.QrPEM_1.Next;
        end;
      end;
    end;
  end;

  //

  procedure AnexaImagensCorpo;
  var
    ImgPath, TxtPath: String;
    ArqExiste: Boolean;
    Comando: PChar;
  begin
    if DmodG.QrPEM_2.RecordCount > 0 then
    begin
      DmodG.QrPEM_2.First;
      while not DmodG.QrPEM_2.Eof do
      begin
        ImgPath := FIniPath + FormatFloat('00000000', DmodG.QrPEM_2Controle.Value) +
        '\' + DmodG.QrPEM_2Descricao.Value;
        TxtPath := dmkPF.MudaExtensaoDeArquivo(ImgPath, 'txt');

        //
        ArqExiste := FileExists(ImgPath);
        if not ArqExiste or CkRecriarImagens.Checked then
        begin
          if FileExists(FBase64) then
          begin
            //if FileExist(TxtPath) then
              //DeleteFile(TxtPath);
            Geral.SalvaTextoEmArquivo(TxtPath, SubstituiVariaveis(DmodG.QrPEM_2Texto.Value), True);
            Comando := PChar(FBase64 + ' -d ' + TxtPath + ' ' + ImgPath);
            //"C:\base64 -e bg2.jpg bg2.txt"
            dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
            ArqExiste := FileExists(ImgPath);
          end else Application.MessageBox(PChar('O aplicativo "' + FBase64 +
          '" n�o existe para criar a imagem "' + ImgPath + '".'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;
        if ArqExiste then
        begin
          with TIdAttachmentFile.Create(FIdMessage.MessageParts, ImgPath) do
          begin
            ContentType        := 'image/jpg';//AcharContentType(ExtractFileExt(ImgPath));
            ContentDisposition := 'inline';
            ContentTransfer    := 'base64';
            ContentID          := '<' + DmodG.QrPEM_2CidID_Img.Value + '>';
            ParentPart         := 0;
          end;
        end;
        DmodG.QrPEM_2.Next;
      end;
    end;
  end;

  //
var
  ImageMsg, AnexoArq: Boolean;
  I: Integer;
//  Link, Msg: String;
begin
  try
    Screen.Cursor      := crHourGlass;
    Result := False;
    Info('Conectando');
    //
    if DModG.QrPreEmail.RecordCount = 0 then
    begin
      Application.MessageBox(
      'Envio cancelado! Nenhum pr�-emeio foi selecionado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //

    IdSMTP1.Disconnect(True);
    IdSMTP1.Host     := DmodG.QrPreEmailSMTPServer.Value;
    IdSMTP1.Username := DmodG.QrPreEmailLogi_Name.Value;
    IdSMTP1.Password := DmodG.QrPreEmailLogi_Pass.Value;;
    if DModG.QrPreEmailPorta_Mail.Value > 0 then
      IdSMTP1.Port := DModG.QrPreEmailPorta_Mail.Value
    else
      IdSMTP1.Port :=  25;//StrToInt(edtPorta.Text);
    if DModG.QrPreEmailLogi_SSL.Value = 1 then
    begin
      //In�cio - Verifica se DLL's existem
      if not FileExists(ExtractFilePath(Application.ExeName) + 'libeay32.dll') then
      begin
        if Geral.MensagemBox('DLL libeay32.dll n�o localizada!' + #13#10 +
         'Deseja fazer o download? ' + #13#10 +
         'Salve o arquivo no diret�rio: "' + ExtractFilePath(Application.ExeName)
         + '"', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          ShellExecute(Application.Handle, nil,
          PChar('http://www.dermatek.com.br/DLLs/libeay32.dll'), nil, nil, sw_hide);
          Info('Erro no envio do email!');
          Exit;
        end else
        begin
          Info('Erro no envio do email!');
          Exit;
        end;
      end;
      if not FileExists(ExtractFilePath(Application.ExeName) + 'ssleay32.dll') then
      begin
        if Geral.MensagemBox('DLL ssleay32.dll n�o localizada!' + #13#10 +
         'Deseja fazer o download? ' + #13#10 +
         'Salve o arquivo no diret�rio: "' + ExtractFilePath(Application.ExeName)
         + '"', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          ShellExecute(Application.Handle, nil,
          PChar('http://www.dermatek.com.br/DLLs/ssleay32.dll'), nil, nil, sw_hide);
          Info('Erro no envio do email!');
          Exit;
        end else
        begin
          Info('Erro no envio do email!');
          Exit;
        end;
      end;
      //Fim - Verifica se DLL's existem
      //
      IdSMTP1.IOHandler := IdSSL1;
      IdSMTP1.UseTLS    := utUseExplicitTLS;
      //
      IdSSL1.Host              := DModG.QrPreEmailSMTPServer.Value;
      IdSSL1.Port              := DModG.QrPreEmailPorta_Mail.Value;
      IdSSL1.SSLOptions.Mode   := sslmUnassigned;
      IdSSL1.SSLOptions.Method := sslvTLSv1;
    end;
    //
    if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
      raise Exception.Create('SMTP com dados incompletos...');

    try
      IdSMTP1.Connect;
    except
      ShowMessage('Erro na Conex�o...');
    end;

    if IdSMTP1.Connected then
    begin
      Info('Configurando email');
      //PlainMsg := DmodG.QrPEM_0.RecordCount > 0;
      //HTML_Msg := DmodG.QrPEM_1.RecordCount > 0;
      ImageMsg := DmodG.QrPEM_2.RecordCount > 0;
      AnexoArq := (FileExists(ArqPDF) or FileExists(ArqXML)) and AnexaArq;
      //
      (*Set up the message...*)
      FIdMessage              := TIdMessage.Create(nil);
      FIdMessage.Subject      := SubstituiVariaveis(DmodG.QrPreEmailMail_Titu.Value);
      FIdMessage.From.Address := DmodG.QrPreEmailSend_Mail.Value;//edSenderEMail.Text;
      FIdMessage.From.Name    := DmodG.QrPreEmailSend_Name.Value;//edSenderName.Text;
      FIdMessage.Encoding     := meMIME;
      FIdMessage.CharSet      := 'iso-8859-1';
      for I := 1 to Grade.RowCount -1 do
      begin
        if Grade.Cells[1,I] = SInterno then
        begin
          with FIdMessage.CCList.Add do
          begin
            Name    := Grade.Cells[2,I];
            Address := Grade.Cells[3,I];
          end;
        end else begin
          with FIdMessage.Recipients.Add do
          begin
            Name    := Grade.Cells[2,I];
            Address := Grade.Cells[3,I];
          end;
        end;
      end;
      if not ImageMsg and not AnexoArq then
      begin
        // S� e texto puro e em HMTL
        if AnexaArq then
          Application.MessageBox('N�o h� DANFE ou XML para anexar � mensagem!',
          'Aviso', MB_OK+MB_ICONWARNING);
        FIdMessage.ContentType := 'multipart/alternative';
        //AnexaTextoPlano; n�o colocar!! Erro quando n�o tem bloqueto anexado
        AnexaHTMLs;
        // Final s� e texto puro e em HMTL
      end
      else if AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML, Figura Inline e Anexo(s)
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo;
        if AnexaArq then
        begin
          AnexaXML;
          AnexaPDF;
        end;
        // Final Texto puro, HTML, Figura Inline e Anexo(s)
      end
      else if not AnexoArq and ImageMsg then
      begin
        // Texto puro, HTML e a figura Inline
        FIdMessage.ContentType := 'multipart/related';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
        end;

        AnexaTextoPlano;
        AnexaHTMLs;
        AnexaImagensCorpo
        // Final texto puro, HTML e a figura Inline
      end
      else if AnexoArq and not ImageMsg then
      begin
        // Texto puro, HTML e anexos
        FIdMessage.ContentType := 'multipart/mixed';

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/related';
        end;

        with  TIdText.Create(FIdMessage.MessageParts) do
        begin
          ContentType := 'multipart/alternative';
          ParentPart  := 0;
        end;


        AnexaTextoPlano;
        AnexaHTMLs;
        if AnexaArq then
        begin
          AnexaPDF;
          AnexaXML;
        end;
        // Final texto puro, HTML e anexos
      end;
      try
        Info('Enviando email');
        IdSMTP1.Send(FIdMessage);
        Result := True;
        Info('Email enviado com sucesso!');
      except
        begin
          Info('Erro no envio do email!');
          Application.MessageBox('Erro no envio do Email descrito no status',
            'Aviso', MB_OK+MB_ICONERROR);
        end;
      end;
    end;
  finally
    IdSMTP1.Disconnect;
    FIdMessage.Free;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmNFeEveMail.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeEveMail.EdPreEmailChange(Sender: TObject);
begin
  DModG.ReopenPreEmail(Geral.IMV(EdPreEmail.Text));
end;

procedure TFmNFeEveMail.FormActivate(Sender: TObject);
var
  CNPJ_CPF, Emails, Email: String;
  I, Entidade, EntiTipCto, EntiTipCt1: Integer;
begin
  MyObjects.CorIniComponente();
  if not FAtivou then
  begin
    FAtivou := True;
    Emails := DmNFe_0000.QrOpcoesNFeMyEmailNFe.Value;
    I := 0;
    MyObjects.LimpaGrade(Grade, 1, 1, True);
    Grade.Cells[00,0] := 'Seq.';
    Grade.Cells[01,0] := 'Tipo';
    Grade.Cells[02,0] := 'Nome';
    Grade.Cells[03,0] := 'Email';
    while Emails <> '' do
    begin
      Geral.SeparaPrimeiraOcorrenciaDeTexto(';', Emails, Email, Emails);
      I := I + 1;
      Grade.RowCount := I + 1;
      Grade.Cells[00,I] := FormatFloat('00', I);
      Grade.Cells[01,I] := SInterno;
      Grade.Cells[02,I] := 'Envio paralelo';
      Grade.Cells[03,I] := Email;
      //
    end;
    //
    // Emails do destinat�rio
    CNPJ_CPF := Geral.SoNumero_TT(EddestCNPJ_CPF.Text);
    DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
    EntiTipCto := DmNFe_0000.QrOpcoesNFeEntiTipCto.Value;
    if EntiTipCto <> 0 then
    begin
      QrEmails.Close;
      QrEmails.Params[00].AsInteger := Entidade;
      QrEmails.Params[01].AsInteger := EntiTipCto;
      QrEmails.Open;
      //
      if QrEmails.RecordCount > 0 then
      begin
        BtEnvia.Enabled := True;
        while not QrEmails.Eof do
        begin
          I := I + 1;
          Grade.RowCount := I + 1;
          Grade.Cells[00,I] := FormatFloat('00', I);
          Grade.Cells[01,I] := 'Terceiro';
          Grade.Cells[02,I] := QrEmailsNome.Value;
          Grade.Cells[03,I] := QrEmailsEMail.Value;
          //
          QrEmails.Next;
        end;
      end else Geral.MensagemBox(
      'Cliente sem email definido para envio do XML!' + #13#10 +
      'Tipo de email: ' + DmNFe_0000.QrOpcoesNFeNO_ETC_0.Value,
      'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox(
    '"Tipo de email para envio de NFe e Carta de Corre��o" n�o definido nas op��es de NF-e!',
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    // Emails da transportadora
    CNPJ_CPF := Geral.SoNumero_TT(EdtrspCNPJ_CPF.Text);
    DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
    EntiTipCt1 := DmNFe_0000.QrOpcoesNFeEntiTipCt1.Value;
    if EntiTipCt1 <> 0 then
    begin
      QrEmails.Close;
      QrEmails.Params[00].AsInteger := Entidade;
      QrEmails.Params[01].AsInteger := EntiTipCt1;
      QrEmails.Open;
      //
      if QrEmails.RecordCount > 0 then
      begin
        BtEnvia.Enabled := True;
        while not QrEmails.Eof do
        begin
          I := I + 1;
          Grade.RowCount := I + 1;
          Grade.Cells[00,I] := FormatFloat('00', I);
          Grade.Cells[01,I] := 'Transportador';
          Grade.Cells[02,I] := QrEmailsNome.Value;
          Grade.Cells[03,I] := QrEmailsEMail.Value;
          //
          QrEmails.Next;
        end;
      end else Geral.MensagemBox(
      'Transportador sem email definido para envio do XML!' + #13#10 +
      'Tipo de email: ' + DmNFe_0000.QrOpcoesNFeNO_ETC_1.Value,
      'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox(
    '"Tipo de email para envio espec�fico de Carta de Corre��o" n�o definido nas op��es de NF-e!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmNFeEveMail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGMsg.DataSource := DModG.DsPreEmMsg;
  MeMsg.DataSource  := DModG.DsPreEmMsg;
  FAutorizada := False;
  FAtivou := False;
  QrPre.Close;
  QrPre.Open;
end;

procedure TFmNFeEveMail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeEveMail.Info(Msg: String);
var
  Aguarde: Boolean;
begin
  Aguarde := Ord(Msg[Length(Msg)]) in ([048..122]);
  MyObjects.Informa2(LaAviso1, LaAviso2, Aguarde, Msg);
end;

procedure TFmNFeEveMail.ReopenNFeEveRCab(EventoLote: Integer);
begin
(*
SELECT FatID, FatNum, Empresa, Controle,
tpAmb, chNFe, dhEvento, tpEvento, descEvento,
Status, ret_cStat, ret_nProt, ret_dhRegEvento,
XML_Eve, XML_retEve, versao
FROM nfeevercab
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCab, Dmod.MyDB, [
  'SELECT nec.FatID, nec.FatNum, nec.Empresa, nec.Controle,   ',
  'nec.tpAmb, nec.chNFe, nec.dhEvento, nec.tpEvento, nec.descEvento,   ',
  'nec.Status, nec.ret_cStat, nec.ret_nProt, nec.ret_dhRegEvento,   ',
  'nec.XML_Eve, nec.XML_retEve, nec.versao, nec.cOrgao,  ',
  'nec.verEvento, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF,  ',
  'nec.nSeqEvento, duf.Nome UF_Nome',
  'FROM nfeevercab nec  ',
  'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao  ',
  'WHERE nec.EventoLote=' + Geral.FF0(EventoLote),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEveRCCe, Dmod.MyDB, [
  'SELECT Conta, xCorrecao, nCondUso',
  'FROM nfeevercce',
  'WHERE FatID=' + Geral.FF0(QrNFeEveRCabFatID.Value),
  'AND FatNum=' + Geral.FF0(QrNFeEveRCabFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrNFeEveRCabEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrNFeEveRCabControle.Value),
  '']);
  if QrNFeEveRCCe.RecordCount <> 1 then
  begin
    Geral.MensagemBox(
    'Quantidade de registros de carta de corre��o diferente de um.' + #13#10 +
    'Quantidade encontrada: ' + Geral.FF0(QrNFeEveRCCe.RecordCount),
    'ERRO', MB_OK+MB_ICONERROR);
    Close;
  end;
end;

procedure TFmNFeEveMail.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdNFeXML);
end;

function TFmNFeEveMail.SubstituiVariaveis(Texto: String): String;
var
  //ID_Protocolo,
  versao, Orgao, Ambiente, CNPJ_CPF, CHAVE_ACESSO, DATA, verEvento,
  CODIGO_EVENTO, SEQUENCIAL_EVENTO: String;
begin
  Result := Texto;
  {
  ID_Protocolo := FormatFloat('0', QrUnidCondApto.Value) + '#' +
                  FormatFloat('0', QrUnidCondProtoco.Value) + '#' +
                  FormatFloat('0', QrUnidCondBloquet.Value) + '#' +
                  FormatFloat('0', QrUnidCondIDEmeio.Value);
  Randomize;
  ID_Protocolo := MLAGeral.PWDExEncode(ID_Protocolo, Dmod.QrControleSecuritStr.Value);
  ID_Protocolo := Dmod.QrControleWeb_MyURL.Value +
    '/index.php?page=recebibloqueto&protocolo=' + ID_Protocolo;
  }
  //
  versao        := Geral.FormataVersaoNFe(QrNFeEveRCabversao.Value);
  Orgao         := Geral.FF0(QrNFeEveRCabcOrgao.Value) + ' - ' +
                  QrNFeEveRCabUF_Nome.Value;
  Ambiente      := Geral.FF0(QrNFeEveRCabtpAmb.Value) + ' - ' +
                  NFeXMLGeren.TextoAmbiente(QrNFeEveRCabtpAmb.Value);
  CNPJ_CPF      := Geral.FormataCNPJ_TT(QrNFeEveRCabCNPJ_CPF.Value);
  CHAVE_ACESSO  := Geral.FormataChaveNFe(QrNFeEveRCabchNFe.Value, cfcnFrendly);
  DATA          := Geral.FDT(QrNFeEveRCabdhEvento.Value, 0);
  verEvento     := Geral.FormataVersaoNFe(QrNFeEveRCabverEvento.Value);
  CODIGO_EVENTO := Geral.FF0(QrNFeEveRCabtpEvento.Value);
  SEQUENCIAL_EVENTO := Geral.FF0(QrNFeEveRCabnSeqEvento.Value);
  //
  Result := Geral.Substitui(Result, '[Empresa]'       , DModG.ObtemNomeFantasiaEmpresa());
  Result := Geral.Substitui(Result, '[ChaveNFe]'      , EdNFeId.Text);
  Result := Geral.Substitui(Result, '[Saudacao]'      , DModG.QrPreEmailSaudacao.Value);
  Result := Geral.Substitui(Result, '[Cliente]'       , Eddest_xNome.Text);
  //Result := Geral.Substitui(Result, '[?]'      , ?);
  Result := Geral.Substitui(Result, '[versao]'        , versao); //1.0
  Result := Geral.Substitui(Result, '[ORGAO]'         , Orgao); //15 - PAR�
  Result := Geral.Substitui(Result, '[AMBIENTE]'      , Ambiente); //2 - Homologa��o
  Result := Geral.Substitui(Result, '[CNPJ_CPF]'      , CNPJ_CPF); //04.333.952/0001-88
  Result := Geral.Substitui(Result, '[CHAVE_ACESSO]'  , CHAVE_ACESSO); // 15-1203-04.333.952/0001-88-55-009-000.000.202-176.112.641-4);
  Result := Geral.Substitui(Result, '[DATA]'          , DATA); //24/03/2012 13:04:57);
  Result := Geral.Substitui(Result, '[CODIGO_EVENTO]' , CODIGO_EVENTO); //110110
  Result := Geral.Substitui(Result, '[SEQUENCIAL_EVENTO]', SEQUENCIAL_EVENTO);
  Result := Geral.Substitui(Result, '[verEvento]'        , verEvento); //1.0
  Result := Geral.Substitui(Result, '[descEvento]'       , QrNFeEveRCabdescEvento.Value); //'Carta de Correcao';
  Result := Geral.Substitui(Result, '[xCorrecao]'        , QrNFeEveRCCexCorrecao.Value);
  Result := Geral.Substitui(Result, '[xCondUso]'         , CO_CONDI_USO_CCE_COM[QrNFeEveRCCenCondUso.Value]);

  // Parei Aqui
  //[LinkConfirma]
  //mudar status
end;

end.
