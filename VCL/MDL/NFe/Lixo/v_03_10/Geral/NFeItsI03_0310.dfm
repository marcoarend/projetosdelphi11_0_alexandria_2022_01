object FmNFeItsI03_0310: TFmNFeItsI03_0310
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-012 :: Informa'#231#245'es de Exporta'#231#227'o'
  ClientHeight = 431
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 286
    Width = 544
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 4
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 544
    Height = 61
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 168
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Item:'
      FocusControl = DBEdnItem
    end
    object Label3: TLabel
      Left = 224
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 304
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdit3
    end
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdFatID
    end
    object Label10: TLabel
      Left = 40
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdFatNum
    end
    object Label13: TLabel
      Left = 120
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object DBEdnItem: TdmkDBEdit
      Left = 168
      Top = 32
      Width = 53
      Height = 21
      DataField = 'nItem'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 0
      UpdCampo = 'nItem'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit2: TDBEdit
      Left = 224
      Top = 32
      Width = 76
      Height = 21
      DataField = 'prod_cProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 304
      Top = 32
      Width = 232
      Height = 21
      DataField = 'prod_xProd'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 2
    end
    object DBEdFatID: TdmkDBEdit
      Left = 8
      Top = 32
      Width = 28
      Height = 21
      DataField = 'FatID'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 3
      UpdCampo = 'FatID'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdFatNum: TdmkDBEdit
      Left = 40
      Top = 32
      Width = 76
      Height = 21
      DataField = 'FatNum'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 4
      UpdCampo = 'FatNum'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 120
      Top = 32
      Width = 45
      Height = 21
      DataField = 'Empresa'
      DataSource = FmNFeCabA_0000.DsNFeItsI
      TabOrder = 5
      UpdCampo = 'Empresa'
      UpdType = utYes
      Alignment = taLeftJustify
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 109
    Width = 544
    Height = 177
    Align = alClient
    Caption = ' Dados da declara'#231#227'o da importa'#231#227'o:'
    TabOrder = 1
    object Label2: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label17: TLabel
      Left = 360
      Top = 20
      Width = 175
      Height = 13
      Caption = 'N'#176' do ato concess'#243'rio de Drawback:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 36
      Width = 68
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EddetExport_nDraw: TdmkEdit
      Left = 360
      Top = 36
      Width = 173
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 11
      TabOrder = 1
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMax = '99999999999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'detExport_nDraw'
      UpdCampo = 'detExport_nDraw'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object GroupBox2: TGroupBox
      Left = 12
      Top = 60
      Width = 521
      Height = 109
      Caption = ' Exporta'#231#227'o Indireta: '
      TabOrder = 2
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 517
        Height = 92
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label18: TLabel
          Left = 12
          Top = 8
          Width = 139
          Height = 13
          Caption = 'N'#176' do registro de Exporta'#231#227'o:'
        end
        object Label7: TLabel
          Left = 12
          Top = 48
          Width = 251
          Height = 13
          Caption = 'Chave de acesso da NF-e recebida para exporta'#231#227'o:'
        end
        object Label14: TLabel
          Left = 340
          Top = 8
          Width = 162
          Height = 13
          Caption = 'Qtde do item realmente exportado:'
        end
        object EddetExport_nRE: TdmkEdit
          Left = 12
          Top = 24
          Width = 173
          Height = 21
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 12
          TabOrder = 0
          FormatType = dmktfInt64
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMax = '999999999999'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'detExport_nRE'
          UpdCampo = 'detExport_nRE'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EddetExport_chNFe: TdmkEdit
          Left = 12
          Top = 64
          Width = 493
          Height = 21
          MaxLength = 44
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'detExport_chNFe'
          UpdCampo = 'detExport_chNFe'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EddetExport_qExport: TdmkEdit
          Left = 340
          Top = 24
          Width = 165
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'detExport_qExport'
          UpdCampo = 'detExport_qExport'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 496
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 448
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 331
        Height = 32
        Caption = 'Informa'#231#245'es de Exporta'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 331
        Height = 32
        Caption = 'Informa'#231#245'es de Exporta'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 331
        Height = 32
        Caption = 'Informa'#231#245'es de Exporta'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 323
    Width = 544
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 367
    Width = 544
    Height = 64
    Align = alBottom
    TabOrder = 5
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 396
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
