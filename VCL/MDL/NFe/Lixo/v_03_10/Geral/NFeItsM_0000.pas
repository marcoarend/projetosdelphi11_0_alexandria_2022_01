unit NFeItsM_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls;

type
  TFmNFeItsM_0000 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdnItem: TdmkEdit;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    EdvTotTrib: TdmkEdit;
    Label1: TLabel;
    Label7: TLabel;
    EdpTotTrib: TdmkEdit;
    REWarning: TRichEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    F_iTotTrib, F_tabTotTrib, F_tpAliqTotTrib, F_fontTotTrib: Integer;
    F_verTotTrib: String;
  end;

  var
  FmNFeItsM_0000: TFmNFeItsM_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeItsM_0000.BtOKClick(Sender: TObject);
var
  verTotTrib: String;
  FatID, FatNum, Empresa, nItem, iTotTrib, tabTotTrib, tpAliqTotTrib,
  fontTotTrib: Integer;
  vTotTrib, pTotTrib: Double;
begin
  FatID          := EdFatID.ValueVariant;
  FatNum         := EdFatNum.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  nItem          := EdnItem.ValueVariant;
  iTotTrib       := F_iTotTrib;
  vTotTrib       := EdvTotTrib.ValueVariant;
  pTotTrib       := EdpTotTrib.ValueVariant;
  tabTotTrib     := F_tabTotTrib;
  verTotTrib     := F_verTotTrib;
  tpAliqTotTrib  := F_tpAliqTotTrib;
  fontTotTrib    := F_fontTotTrib;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsm', False, [
  'iTotTrib', 'vTotTrib', 'pTotTrib',
  'tabTotTrib', 'verTotTrib', 'tpAliqTotTrib',
  'fontTotTrib'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  iTotTrib, vTotTrib, pTotTrib,
  tabTotTrib, verTotTrib, tpAliqTotTrib,
  fontTotTrib], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
    if DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, LaAviso1, LaAviso2,
    REWarning) then
      Close;
  end;
end;

procedure TFmNFeItsM_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsM_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsM_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeItsM_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeItsM_0000.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
end;

end.
