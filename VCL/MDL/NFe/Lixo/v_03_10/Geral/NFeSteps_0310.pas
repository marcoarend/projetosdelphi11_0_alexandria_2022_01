unit NFeSteps_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, ComCtrls, dmkEdit,
  DB, DBClient, OmniXML, OmniXMLUtils, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, Variants, dmkGeral, OleCtrls, SHDocVw,
  UnDmkProcFunc, NFeXMLGerencia, dmkImage, UnDmkEnums, dmkRadioGroup,
  NFeImporta_0310, NFe_PF, UnXXE_PF, UnProjGroup_Consts;

type
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chNFe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nNFIni            ,
     ttx_nNFFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento       ,
     ttx_dhResp            ,
     ttx_indCont           ,
     ttx_ultNSU            ,
     ttx_maxNSU            ,
     ttx_NSU               ,
     ttx_CPF               ,
     ttx_xNome             ,
     ttx_IE                ,
     ttx_dEmi              ,
     ttx_tpNF              ,
     ttx_vNF               ,
     ttx_cSitNFe           ,
     ttx_cSitConf          ,
     ttx_dhEvento          ,
     ttx_descEvento        ,
     ttx_xCorrecao         ,
     ttx_cJust             ,
     ttx_xJust             ,
     ttx_verEvento         ,
     ttx_schema            ,
     ttx_docZip            ,
     ttx_dhEmi             ,
     //
     ttx_
     );

  TFmNFeSteps_0310 = class(TForm)
    QrNFeCabA1: TmySQLQuery;
    QrNFeCabA1FatID: TIntegerField;
    QrNFeCabA1FatNum: TIntegerField;
    QrNFeCabA1Empresa: TIntegerField;
    QrNFeJust: TmySQLQuery;
    DsNFeJust: TDataSource;
    QrNFeJustCodigo: TIntegerField;
    QrNFeJustNome: TWideStringField;
    QrNFeJustCodUsu: TIntegerField;
    QrNFeJustAplicacao: TIntegerField;
    QrNFeCabA2: TmySQLQuery;
    QrNFeCabA2FatID: TIntegerField;
    QrNFeCabA2FatNum: TIntegerField;
    QrNFeCabA2Empresa: TIntegerField;
    Panel3: TPanel;
    PnLoteEnv: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    REWarning: TRichEdit;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdNFeJust: TdmkEditCB;
    CBNFeJust: TdmkDBLookupComboBox;
    PnCancInutiliza: TPanel;
    PnChaveNFe: TPanel;
    Label8: TLabel;
    EdchNFe: TEdit;
    Panel8: TPanel;
    PnRecibo: TPanel;
    Label6: TLabel;
    EdRecibo: TdmkEdit;
    PnProtocolo: TPanel;
    Label9: TLabel;
    EdnProt: TEdit;
    PnIDCtrl: TPanel;
    Label16: TLabel;
    EdIDCtrl: TdmkEdit;
    PnInutiliza: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdnNFIni: TdmkEdit;
    EdnNFFim: TdmkEdit;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    EdModelo: TdmkEdit;
    EdSerie: TdmkEdit;
    EdEmitCNPJ: TdmkEdit;
    EdAno: TdmkEdit;
    PnConfig1: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdSerialNumber: TdmkEdit;
    EdUF_Servico: TdmkEdit;
    CBUF: TComboBox;
    Panel7: TPanel;
    RGAmbiente: TRadioGroup;
    PnLote: TPanel;
    Label4: TLabel;
    EdLote: TdmkEdit;
    Panel15: TPanel;
    RGAcao: TRadioGroup;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    LaWait: TLabel;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    QrCabAinfProt_nProt: TWideStringField;
    Timer1: TTimer;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaExpiraCertDigital: TLabel;
    PnCadastroContribuinte: TPanel;
    Label21: TLabel;
    EdContribuinte_CNPJ: TdmkEdit;
    EdContribuinte_UF: TdmkEdit;
    Label22: TLabel;
    SpeedButton2: TSpeedButton;
    EdEmpresa: TdmkEdit;
    PnDesConC: TPanel;
    Label1: TLabel;
    EddestCNPJ: TdmkEdit;
    EdIndNFe: TdmkEdit;
    Label24: TLabel;
    EdindEmi: TdmkEdit;
    Label25: TLabel;
    EdultNSU: TdmkEdit;
    Label26: TLabel;
    TabSheet6: TTabSheet;
    MeChaves: TMemo;
    Panel10: TPanel;
    CkSoLer: TCheckBox;
    RGIndSinc: TdmkRadioGroup;
    QrNFeCabA1IDCtrl: TIntegerField;
    QrNFeCabA2Id: TWideStringField;
    EdNSU: TdmkEdit;
    Label23: TLabel;
    QrDFe: TmySQLQuery;
    RGFrmaCnslt: TdmkRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RETxtEnvioSelectionChange(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure EdVersaoAcaoChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdultNSUKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //FVersaoAcao: String;
    FMsg: String;
    FSiglas_WS: MyArrayLista;
    FPathLoteNFe, FPathLoteEvento, FPathLoteDowNFeDes: String;
    xmlDoc: IXMLDocument;
    xmlNode, xmlSub, xmlChild1: IXMLNode;
    xmlList(*, xmlSubs*): IXMLNodeList;
    {
    FNomeCertificado, FSiglaUF: String;
    FtpAmb: Integer;
    }
    FAmbiente_Int, FCodigoUF_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt: String;
    FWSDL, FURL, FAvisoNSU: String;
    //FindCont: Byte;
    FultNSU, FNSU: Int64;

    //function TotaisNFe(FatID, FatNum, Empresa: Integer): Boolean;
    function  DefX(Codigo, ID: String; Texto: String): String;
    function  DefI(Codigo, ID: String; ValMin, ValMax, Numero: Integer): Integer;
    function  DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefinechNFe(var chNFe: String): Boolean;
    function  DefinenProt(var nProt: String): Boolean;
    function  DefineModelo(var Modelo: String): Boolean;
    function  DefineSerie(var Serie: String): Boolean;
    function  DefinenNFIni(var nNFIni: String): Boolean;
    function  DefinenNFFin(var nNFFim: String): Boolean;
    function  DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
    function  DefineDestCNPJ(var DestCNPJ: String): Boolean;
    function  DefineIntCNPJ(var IntCNPJ: String): Boolean;
    function  DefineIDCtrl(var IDCtrl: Integer): Boolean;
    function  DefineXMLDoc(): Boolean;
    function  Define_indNFe(var indNFe: String): Boolean;
    function  Define_indEmi(var indEmi: String): Boolean;
    function  Define_ultNSU(var ultNSU: String): Boolean;
    function  Define_NSU(var NSU: String): Boolean;
    //function  DefineVersaoAcao(): Boolean;

    //function DefineMeuID_NFe(var Controle: Integer): Boolean;
    procedure UpdateCursorPos(Memo: TMemo);
    {
    procedure GetXMLData(ANode: TxmlNode; Control: TWinControl);
    procedure SetXMLData(ANode: TxmlNode; Control: TWinControl);
    }
    procedure HabilitaBotoes(Visivel: Boolean = True);
    procedure ReopenNFeJust(Aplicacao: Byte);
    procedure MostraTextoRetorno(Texto: String);
    function TextoArqDefinido(Texto: String): Boolean;
    function LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML; AvisaVersao: Boolean = True): String;
    //function LeNoXML_2(No: IXMLNode; Tipo: TTipoNoXML; Grupo: String; Tag: TTipoTagXML): String;
    function ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    procedure VerificaCertificadoDigital(Empresa: Integer);
    procedure ExecutaConsultaLoteNFe();
    procedure LerTextoConsultaLoteNFe();
    procedure LerTextoConsultaNFeDest(ultNSU: String);
    function LerTextoEnvioLoteNFe((*Sincronia: TNFeIndSinc*)): Boolean;
    //
    function  Configura_versao(NFeversao: Double; VersaoNFe: Integer;
              var Msg: String): Double;
    function  Configura_ide_cUF(DTB_UF: String; var Msg: String): Integer;
    function  Configura_ide_mod(ide_mod: Integer): Integer;
    function  Configura_emit_Doc(Tipo: Integer; CNPJ, CPF: String;
              var Msg: String): String;
    function  AtualizaXML_No_BD_NFeConfirmada(XML_NFe: String; NFeCabA: TNFeCabA): Boolean;
  public
    { Public declarations }
    Node: TxmlNode;
    FFormChamou: String;
    FXML_LoteNFe, FXML_LoteEvento, FXML_LoteDowNFeDes: String;
    FSegundos, FSecWait: Integer;
    FTextoArq: String;
    FNaoExecutaLeitura: Boolean;
    //
    function  AbreArquivoSelecionado(Arquivo: String): Boolean;
    function  AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
    function  CriaNFeNormal(Recria: Boolean; NFeStatus, FatID, FatNum,
              Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta,
              ide_indPag, FisRegCad, CartEmiss, TabelaPrc, CondicaoPg: Integer;
              FreteVal, Seguro, Outros: Double;
              ide_Serie: Variant; ide_nNF: Integer; ide_dEmi, ide_dSaiEnt: TDateTime;
              ide_tpNF, ide_tpEmis: Integer; infAdic_infAdFisco, infAdic_infCpl,
              VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
              Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT,
              SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String;
              UF_Emit, UF_Dest: String; GravaCampos, cNF_Atual,
              Financeiro: Integer; ide_hSaiEnt: String; ide_dhCont: TDateTime;
              ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
              Compra_XNEmp, Compra_XPed, Compra_XCont: String;
              // NFe 3.10
              idDest: Integer; ide_hEmi: String; ide_dhEmiTZD, ide_dhSaiEntTZD:
              Double; indFinal, indPres, finNFe: Integer;
              ApenasCriaXML: Boolean; CalculaAutomatico: Boolean): Boolean;
              //
    function  InsUpdNFeCab(Status: Integer; SQLType: TSQLType;
              Empresa, Cliente, FisRegCad, CartEmiss, TabelaPrc, CondicaoPg,
              ModFrete, Transporta: Integer; ide_natOp: String;
              ide_indPag: Integer; ide_serie: Variant; ide_nNF: Integer;
              ide_dEmi, ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis, ide_finNFe,
              Retirada, Entrega, FatID, FatNum, IDCtrl: Integer;
              infAdic_infAdFisco, infAdic_infCpl,
              VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
              Exporta_UFEmbarq, Exporta_xLocEmbarq,
              SQL_FAT_ITS: String; FreteExtra, SegurExtra, DespAcess: Double;
              cNF_Atual: Integer; ide_hSaiEnt: String; ide_dhCont:
              TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email,
              Vagao, Balsa, Compra_XNEmp, Compra_XPed, Compra_XCont: String;
              // NFe 3.10
              ide_idDest: Integer; ide_hEmi: String; ide_dhEmiTZD,
              ide_dhSaiEntTZD: Double; ide_indFinal, ide_indPres: Integer;
              //
              VersaoNFe: Integer; NFeConjugada: Boolean;
              var dest_indIEDest: Integer): Boolean;
    function  MontaNomeArqNSU(Lote: Integer): String;
    procedure PreparaStepGenerico(Empresa: Integer);
    function  PreparaEnvioDeLoteNFe(Lote, Empresa: Integer; Sincronia: TXXeIndSinc): Boolean;
    procedure PreparaVerificacaoStatusServico(Empresa: Integer);
    procedure PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
    procedure PreparaCancelamentoDeNFe(Lote, Empresa: Integer; ChaveNFe, Protocolo: String);
    procedure PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo, Serie, nNFIni, nNFFim, Justif: Integer);
    procedure PreparaConsultaNFe(Empresa, IDCtrl: Integer; ChaveNFe: String);
    function  PreparaEnvioDeLoteEvento(var UF_Servico: String;
              Lote, Empresa: Integer): Boolean;
    procedure PreparaConsultaCadastro(Empresa: Integer);
    procedure PreparaConsultaDeNFesDestinadas(Empresa, Lote: Integer; ultNSU: Int64);
    procedure PreparaDownloadDeNFeDestinadas(Lote, Empresa: Integer);
    procedure PreparaDownloadDeNFeConfirmadas(Lote, Empresa: Integer);
    procedure PreparaConsultaDistribuicaoDFeInteresse(Empresa, Lote: Integer;
              ultNSU, NSU: Int64; FrmaCnslt: Integer);

    procedure VerificaStatusServico();
    procedure ConsultaCadastroContribuinte();

    procedure ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
    procedure ExecutaEnvioDeLoteEvento();
    procedure ExecutaInutilizaNumerosNF();
    procedure ExecutaConsultaNFe();
    procedure ExecutaConsultaNFDest();
    procedure ExecutaDownloadNFDestinadas();
    procedure ExecutaDownloadNFeConfirmadas();
    procedure ExecutaConsultaDistribuicaoDFeInteresse();

    procedure ImportaDadosNFeDeXML(XML_NFe: String; IDCtrl, FatID, FatNum,
              Empresa: Integer; NFeCabA: TNFeCabA);

    procedure LerTextoStatusServico();
    function  LerTextoEnvioLoteEvento(): Boolean;
    procedure LerTextoInutilizaNumerosNF();
    procedure LerTextoConsultaNFe();
    procedure LerTextoDownloadNFeDestinadas();
    procedure LerTextoDownloadNFeConfirmadas();
    procedure LerTextoConsultaDistribuicaoDFeInteresse();

    procedure LerXML_procEventoNFe(Lista: IXMLNodeList);

  end;

  var
  FmNFeSteps_0310: TFmNFeSteps_0310;

implementation

uses RichEdit, ReInit, UnInternalConsts, UnMyObjects, NFeStatusServico,
  ModuleGeral, Module, UMySQLModule, ModuleNFe_0000, NFeInut_0000, MyDBCheck,
  Entidade2, NFeLEnC_0310, NFeLEnU_0310, NFeJust, DmkDAC_PF, NFeGeraXML_0310,
  {$IFNDef semNFe_v0200}
  NFeCnfDowC_0100,
  {$EndIF}
  //NFeDesConC_0101, NFeDesDowC_0100,
  NFeDistDFeInt_0100;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  CO_Texto_Opt_Sel = 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!';
  CO_Texto_Clk_Sel = 'Configure a forma de consulta e clique em "OK"!';
  CO_Texto_Env_Sel = 'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!';
  FVersaoNFe = 310;
var
  FverXML_versao: String;

function TFmNFeSteps_0310.AtualizaXML_No_BD_NFeConfirmada(XML_NFe: String;
  NFeCabA: TNFeCabA): Boolean;
var
  SQLType: TSQLType;
  IDCtrl, FatID, FatNum, Empresa: Integer;
begin
  Result  := False;
  IDCtrl  := NFeCabA.IDCtrl;
  FatID   := NFeCabA.FatID;
  FatNum  := NFeCabA.FatNum;
  Empresa := NFeCabA.Empresa;
  //
  if IDCtrl = 0 then
  begin
    (* N�o mostrar mensagem
    Geral.MB_Erro('"IDCtrl" n�o definido em "TFmNFeSteps_0310.AtualizaXML_No_BD_NFeConfirmada()"');
    *)
    Exit;
  end;
  DmNFe_0000.ReopenNfeArq(IDCtrl);
  //
  FatID   := DmNFe_0000.QrNFeArqFatID.Value;
  FatNum  := DmNFe_0000.QrNFeArqFatNum.Value;
  Empresa := DmNFe_0000.QrNFeArqEmpresa.Value;
  //
  if DmNFe_0000.QrNFeArqIDCtrl_Arq.Value = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfearq WHERE IDCtrl=:P0');
    Dmod.QrUpd.Params[00].AsInteger := IDCtrl;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfearq ');
    Dmod.QrUpd.SQL.Add('WHERE FatID=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.ExecSQL;
    //
    SQLType := stIns;
  end else begin
    SQLType := stUpd;
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfearq', False, [
    'IDCtrl', 'XML_NFe'], ['FatID', 'FatNum', 'Empresa'], [
    IDCtrl, XML_NFe ], [ FatID, FatNum, Empresa ], True);
  //
  Result := True;
end;

procedure TFmNFeSteps_0310.BtOKClick(Sender: TObject);
begin
  REWarning.Text  := '';
  RETxtEnvio.Text := '';
  MeInfo.Text     := '';
  //
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 0;
  Update;
  Application.ProcessMessages;
  //
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MB_Aviso('Defina o ambiente!');
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MB_Aviso('Defina a UF!');
    Exit;
  end;
  //
  BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    0: VerificaStatusServico();
    1:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteNFe((*nisSincrono*))
      else
        ExecutaEnvioDeLoteNFe(TXXeIndSinc(RGIndSinc.ItemIndex));
    end;
    2:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaLoteNFe()
      else
        ExecutaConsultaLoteNFe();
    end;
    3:
    begin
      Geral.MB_Erro('Cancelamento de NF-e somente via "Evento de NF-e"!');
      (*
      if CkSoLer.Checked then
        LerTextoCancelaNFe()
      else
        ExecutaCancelaNFe();
      *)
    end;
    4:
    begin
      if CkSoLer.Checked then
        LerTextoInutilizaNumerosNF()
      else
        ExecutaInutilizaNumerosNF();
    end;
    5:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNFe()
      else
        ExecutaConsultaNFe();
    end;
    6:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteEvento()
      else
        ExecutaEnvioDeLoteEvento();
    end;
    7: ConsultaCadastroContribuinte();
    //8: Consulta situa��o NFe
    9:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNFeDest(EdultNSU.Text)
      else
        ExecutaConsultaNFDest();
    end;
    10:
    begin
(*  Extinto !!!???
      if CkSoLer.Checked then
        LerTextoDownloadNFeDestinadas()
      else
        ExecutaDownloadNFDestinadas();
*) // Fim extinto !!!???
      if CkSoLer.Checked then
        LerTextoDownloadNFeConfirmadas()
      else
        ExecutaDownloadNFeConfirmadas();
    end;
    11:
    begin
(*
      if CkSoLer.Checked then
        LerTextoEnvioLoteNFe()
      else
        ExecutaEnvioDeLoteNFe(TXXeIndSinc(RGIndSinc.ItemIndex));
*)
      if CkSoLer.Checked then
        LerTextoConsultaDistribuicaoDFeInteresse()
      else
        ExecutaConsultaDistribuicaoDFeInteresse();
    end;
    else
      Geral.MB_Aviso('A a��o "' + RGAcao.Items[RGAcao.ItemIndex] +
        '" n�o est� implementada! AVISE A DERMATEK!');
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeTexto_Permanente(REWarning.Text, 'Aviso do Web Service');
end;

procedure TFmNFeSteps_0310.Button1Click(Sender: TObject);
begin
  dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
end;

procedure TFmNFeSteps_0310.ExecutaEnvioDeLoteEvento();
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    //
    if not FileExists(FPathLoteEvento) then
    begin
      //Continua := False;
      Geral.MB_Erro('O lote "' + FPathLoteEvento + '" n�o foi localizado!');
      Exit;
    end;
    rtfDadosMsg := MLAGeral.LoadFileToText(FPathLoteEvento);
    //RETxtEnvio.Text := rtfDadosMsg;
    if rtfDadosMsg = '' then
    begin
      //Continua := False;
      Geral.MB_Erro('O lote de eventos "' + FPathLoteEvento +
      '" foi carregado mas est� vazio!');
      Exit;
    end;
    retWS :='';
    FTextoArq :='';
    Screen.Cursor := crHourGlass;
    try
      FTextoArq := FmNFeGeraXML_0310.WS_RecepcaoEvento(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService);
      //Geral.MB_(FTextoArq, 'Texto resultante', MB_OK+MB_ICONWARNING);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2,False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      //
      DmNFe_0000.ReopenEmpresa(Empresa);
      LoteStr := FormatFloat('000000000', Lote);
      DmNFe_0000.SalvaXML(NFE_EXT_EVE_RET_LOT_XML, LoteStr, FTextoArq, RETxtRetorno, False);
      //
      if LerTextoEnvioLoteEvento() then ;//Close;
      //
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        Geral.MB_Erro('Erro na chamada do WS...' + sLineBreak + FTextoArq);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
      end else
        Close;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFeSteps_0310.ExecutaEnvioDeLoteNFe(Sincronia: TXXeIndSinc);
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteNFe) then
    begin
      //Continua := False;
      Geral.MB_Erro('O lote "' + FPathLoteNFe + '" n�o foi localizado!');
      Exit;
    end;
    //rtfDadosMsg := MLAGeral.LoadFileToText(FPathLoteNFe);      / erro
    if dmkPF.CarregaArquivo(FPathLoteNFe, rtfDadosMsg) then
    begin
      //RETxtEnvio.Text := rtfDadosMsg;
      if rtfDadosMsg = '' then
      begin
        //Continua := False;
        Geral.MB_Erro('O lote de NFe "' + FPathLoteNFe +
        '" foi carregado mas est� vazio!');
        Exit;
      end;
      retWS :='';
      FTextoArq :='';
      Screen.Cursor := crHourGlass;
      try
        FTextoArq := FmNFeGeraXML_0310.WS_NFeRecepcaoLote(EdUF_Servico.Text,
        FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
        EdWebService, Sincronia);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmNFe_0000.ReopenEmpresa(Empresa);
        LoteStr := FormatFloat('000000000', Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        Timer1.Enabled := Sincronia = TXXeIndSinc.nisAssincrono;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  if LerTextoEnvioLoteNFe((*Sincronia*)) then ;//Close;
end;

procedure TFmNFeSteps_0310.ExecutaInutilizaNumerosNF();
var
  Id, xJust, Modelo, Serie, nNFIni, nNFFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  if not DefineModelo(Modelo) then Exit;
  if not DefineSerie(Serie) then Exit;
  if not DefinenNFIni(nNFIni) then Exit;
  if not DefinenNFFin(nNFFin) then Exit;
  if not DefineEmitCNPJ(EmitCNPJ) then Exit;
  //
  xJust := Trim(XXe_PF.ValidaTexto_XML(CBNFeJust.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MB_Aviso('A justificativa deve ter pelo menos 15 caracteres!' +
    sLineBreak + 'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.');
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrNFeJustCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmNFeGeraXML_0310.WS_NFeInutilizacaoNFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdAno.ValueVariant, Id, emitCNPJ, Modelo,
      Serie, nNFIni, nNFFin, XJust, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    //cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FSiglaUF));
    DmNFe_0000.MontaID_Inutilizacao(FCodigoUF_Txt, EdAno.Text, emitCNPJ, Modelo, Serie,
      nNFIni, nNFFin, Id);
    //
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    DmNFe_0000.SalvaXML(NFE_EXT_INU_XML, LoteStr, FTextoArq, RETxtRetorno, False);
    //
    LerTextoInutilizaNumerosNF();
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFeSteps_0310.LerTextoEnvioLoteNFe((*Sincronia: TXXeIndSinc*)): Boolean;
  function CorrigeDataStringVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
var
  Status, Codigo, Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed: String;
  //
  FatID, FatNum: Integer;
  infProt_Id, infProt_chNFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  infProt_dhRecbtoTZD, dhRecbtoTZD: Double;
  Sincronia: TXXeIndSinc;
  Dir, Aviso: String;
  IDCtrl: Integer;
begin
  //FverXML_versao := verEnviNFeNovo_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
(*
- <retEnviNFe versao="3.10" xmlns="http://www.portalfiscal.inf.br/nfe">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <cStat>104</cStat>
  <xMotivo>Lote processado</xMotivo>
  <cUF>41</cUF>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
*)
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
(*==============================================================================
      NFe 3.10  - NFe Sincrona
================================================================================
- <protNFe versao="3.10">
- <infProt Id="ID141140001620054">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <chNFe>41141002717861000110550010000045811360413650</chNFe>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
  <nProt>141140001620054</nProt>
  <digVal>gzIavxRcIKOO6WbOu2M+HwEH0Cg=</digVal>
  <cStat>100</cStat>
  <xMotivo>Autorizado o uso da NF-e</xMotivo>
  </infProt>
  </protNFe>
  </retEnviNFe>
*)
      xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/protNFe');
      if assigned(xmlNode) then
      begin
        Sincronia := nisSincrono;
        xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/protNFe/infProt');
        if assigned(xmlNode) then
        begin
          infProt_Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infProt_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infProt_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          infProt_chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          infProt_dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          infProt_nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          infProt_digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
          infProt_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          infProt_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
            'Status', 'infProt_Id', 'infProt_tpAmb',
            'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
            'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
            'protNFe_versao', 'infProt_dhRecbtoTZD'
          ], ['ID', 'LoteEnv'], [
            infProt_cStat, infProt_Id, infProt_tpAmb,
            infProt_verAplic, infProt_dhRecbto, infProt_nProt,
            infProt_digVal, infProt_cStat, infProt_xMotivo,
            versao, infProt_dhRecbtoTZD
          ], [infProt_chNFe, Codigo], True) then
          begin
            // hist�rico da NF
            QrNFeCabA1.Close;
            QrNFeCabA1.Params[00].AsString  := infProt_chNFe;
            QrNFeCabA1.Params[01].AsInteger := Codigo;
            QrNFeCabA1.Open;
            if QrNFeCabA1.RecordCount > 0 then
            begin
              FatID   := QrNFeCabA1FatID.Value;
              FatNum  := QrNFeCabA1FatNum.Value;
              Empresa := QrNFeCabA1Empresa.Value;
              //
              Controle := DModG.BuscaProximoCodigoInt(
                'nfectrl', 'nfecabamsg', '', 0);
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
              'FatID', 'FatNum', 'Empresa', 'Solicit',
              'Id', 'tpAmb', 'verAplic',
              'dhRecbto', 'nProt', 'digVal',
              'cStat', 'xMotivo', 'dhRecbtoTZD',
              '_Ativo_'], [
              'Controle'], [
              FatID, FatNum, Empresa, 100(*autoriza��o*),
              infProt_Id, infProt_tpAmb, infProt_verAplic,
              infProt_dhRecbto, infProt_nProt, infProt_digVal,
              infProt_cStat, infProt_xMotivo, infProt_dhRecbtoTZD,
              1], [
              Controle], True);
            end else Geral.MB_Aviso('A Nota Fiscal de chave "' +
            infProt_chNFe +
            '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
            //
            Dir := DModG.QrPrmsEmpNFeDirRec.Value;
            Aviso := '';
            IDCtrl := QrNFeCabA1IDCtrl.Value;
            //DmNFE_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_Id, Dir, Aviso);
            DmNFE_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_chNFe, Dir, Aviso);
            //
            //DmNFE_0000.AtualizaXML_No_BD_NFe(QrNFeCabA1IDCtrl.Value, Dir, Aviso);
            //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
            //
           if Aviso <> '' then Geral.MB_Aviso(
            'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
          end;
          // NFeLEnM
          // Conforme Manual NFe 3.10:
          // C. Processamento S�ncrono
          //No caso de processamento s�ncrono do Lote de NF-e, as valida��es da
          //NF-e ser�o feitas na sequ�ncia, sem a gera��o de um N�mero de Recibo.
          nRec      := 'NFe s�ncrona';
          dhRecbto := infProt_dhRecbto;
          tMed      := '0';
          //
        end else Geral.MB_Erro('Protocolo de NFe sem inform��es!');
      end else
      begin
        Sincronia := nisAssincrono;
(*==============================================================================
      FIM  NFe 3.10  - NFe Sincrona
==============================================================================*)
        xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/infRec');
        if assigned(xmlNode) then
        begin
          nRec      := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
          dhRecbto  := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto));
          tMed      := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
        end else begin
          nRec      := '';
          dhRecbto  := CorrigeDataStringVazio('');
          tMed      := '0';
        end;
      end;
      //
      XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
      //
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        DmNFe_0000.stepLoteEnvEnviado(), dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          // CUIDADO!!!!  Somente se for lote Assincrono!!
          // Se fizer no sincrono a NFe mesmo autorizada fimca Status = 40 !!!
          if Sincronia = nisAssincrono then
          begin
            Status := Geral.IMV(cStat);
            //
            if Status <> 103 then
              Status := DmNFe_0000.stepLoteRejeitado;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
              'Status'], ['LoteEnv', 'Empresa'], [
              Status], [Codigo, Empresa], True);
          end;
        end;
      end;
      if FFormChamou = 'FmNFeLEnc_0310' then
        FmNFeLEnc_0310.LocCod(Codigo, Codigo);
      if FFormChamou = 'FmNFeLEnU_0310' then
        FmNFeLEnU_0310.ReabreNFeLEnc(Codigo);
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado!');
  end;
  Result := True;
end;

function TFmNFeSteps_0310.LerTextoEnvioLoteEvento(): Boolean;
var
  Codigo, Controle, tpAmb, cOrgao, cStat: Integer;
  versao: Double;
  verAplic, xMotivo: String;

  Status, ret_tpAmb, ret_cOrgao, ret_cStat, ret_tpEvento,
  ret_nSeqEvento: Integer;
  ret_versao, ret_TZD_UTC: Double;
  ret_Id, ret_verAplic, ret_xMotivo, ret_chNFe, ret_xEvento,
  ret_CNPJDest, ret_CPFDest, ret_emailDest, ret_nProt: String;

  SubCtrl: Integer;

  eveMDe_tpAmb, eveMDe_cOrgao, eveMDe_cStat,
  eveMDe_tpEvento, eveMDe_nSeqEvento, cSitConf: Integer;
  eveMDe_Id, eveMDe_verAplic, eveMDe_xMotivo,
  eveMDe_chNFe, eveMDe_xEvento, eveMDe_CNPJDest,
  eveMDe_CPFDest, eveMDe_emailDest, eveMDe_nProt, Id, eveMDe_dhRegEvento: String;
  eveMDe_TZD_UTC: Double;

  Empresa, cJust: Integer;
  //tMed, nRec
  idLote: String;
  infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chNFe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao, infEvento_xCorrecao: String;

  tpEvento, nSeqEvento: Integer;
  //cSitNFe: Integer;
  xJust: String;
  SQLType: TSQLType;

  CPF: String;
begin
  //FverXML_versao := verEnviEvento_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerLotEve.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := Geral.DMV_Dot(LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False));
      //
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb));
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao));
      cStat    := Geral.IMV(LeNoXML(xmlNode, tnxTextStr, ttx_cStat));
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
          begin
            //I := -1;
            while xmlList.Length > 0 do
            begin
              //I := I + 1;
              //testar
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao, False);
//{TODO :       Verificar vers�o e conte�do do XML_retEve!
              XML_retEve            := xmlList.Item[0].XML;
              //
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEvento/infEvento');
              infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              //
              infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
              infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              infEvento_chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
              infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
              infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
              infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
              infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
              infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
              infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
              infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
              infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
              //
              //infEvento_xCorrecao   := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
              //
              if infEvento_dhRegEvento = '' then
                infEvento_dhRegEvento := '0000-00-00 00:00:00'
              else
                XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status     := Geral.IMV(infEvento_cStat);
              tpEvento   := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmNFe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chNFe);
              //
              if Controle <> 0 then
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
                  'XML_retEve',
                  'Status', 'ret_versao', 'ret_Id',
                  'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                  'ret_cStat', 'ret_xMotivo', 'ret_chNFe',
                  'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                  'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                  'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                  (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                  Geral.WideStringToSQLString(XML_retEve), Status,
                  (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverret', '', 0);
                  DmNFe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeeverret', False, [
                    'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                    'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                    'ret_xMotivo', 'ret_chNFe', 'ret_tpEvento',
                    'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                    'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                    'ret_TZD_UTC', 'ret_nProt'], [
                    'SubCtrl'], [
                    Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                    (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                    (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                    (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                    (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                    (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                    (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                    (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                    (*ret_nProt*)infEvento_nProt], [
                    SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      NFe_CodEventoCCe: // Carta de corre��o.
                      //A princ�pio n�o faz nada!
                      // Implementado nos aplicativos Dermatek s� na NFe 3.10
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCCeNfeCabA(infEvento_chNFe,
                            verAplic, infEvento_chNFe, infEvento_dhRegEvento,
                            infEvento_nProt, nSeqEvento, cOrgao, tpAmb, tpEvento,
                            cStat, ret_TZD_UTC)
                          then
                            Exit;

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
                        end;
                      end;
                      NFe_CodEventoCan: // Cancelamento
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not UnNFe_PF.AtualizaDadosCanNfeCabA(
                            infEvento_chNFe, infEvento_tpAmb, infEvento_verAplic,
                            infEvento_dhRegEvento, infEvento_nProt, infEvento_versao,
                            ret_TZD_UTC)
                          then
                            Exit;
                        end;
                      end;
                      // Manifesta��o do destinat�rio
                      NFe_CodEventoMDeConfirmacao, // = 210200;
                      NFe_CodEventoMDeCiencia    , // = 210210;
                      NFe_CodEventoMDeDesconhece , // = 210220;
                      NFe_CodEventoMDeNaoRealizou: // = 210240;
                      begin
                        if infEvento_cStat = '135' then
                        begin
                          //  Realmente precisa localizar?
                          if not DmNFe_0000.LocalizaNFeInfoMDeEve(
                            infEvento_chNFe, Id, xJust, cJust, tpEvento)

                          then
                            Geral.MB_Erro('Falha ao localizar dados da manifesta��o');
                          //
                          Id                 := infEvento_chNFe;
                          //
                          eveMDe_Id          := infEvento_Id;
                          eveMDe_tpAmb       := Geral.IMV(infEvento_tpAmb);
                          eveMDe_verAplic    := infEvento_verAplic;
                          eveMDe_cOrgao      := Geral.IMV(infEvento_cOrgao);
                          eveMDe_cStat       := Geral.IMV(infEvento_cStat);
                          eveMDe_xMotivo     := infEvento_xMotivo;
                          eveMDe_chNFe       := infEvento_chNFe;
                          eveMDe_tpEvento    := Geral.IMV(infEvento_tpEvento);
                          eveMDe_xEvento     := infEvento_xEvento;
                          eveMDe_nSeqEvento  := Geral.IMV(infEvento_nSeqEvento);
                          eveMDe_CNPJDest    := infEvento_CNPJDest;
                          eveMDe_CPFDest     := infEvento_CPFDest;
                          eveMDe_emailDest   := infEvento_emailDest;
                          eveMDe_dhRegEvento := infEvento_dhRegEvento;
                          eveMDe_TZD_UTC     := ret_TZD_UTC;
                          eveMDe_nProt       := infEvento_nProt;
                          //
                          //cSitNFe  :=  N�o tem aqui!
                          cSitConf := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(eveMDe_tpEvento);
                          //
                          if eveMDe_dhRegEvento = '' then
                            eveMDe_dhRegEvento := '0000-00-00 00:00:00';
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                            'eveMDe_Id', 'eveMDe_tpAmb', 'eveMDe_verAplic',
                            'eveMDe_cOrgao', 'eveMDe_cStat', 'eveMDe_xMotivo',
                            'eveMDe_chNFe', 'eveMDe_tpEvento', 'eveMDe_xEvento',
                            'eveMDe_nSeqEvento', 'eveMDe_CNPJDest', 'eveMDe_CPFDest',
                            'eveMDe_emailDest', 'eveMDe_dhRegEvento', 'eveMDe_TZD_UTC',
                            'eveMDe_nProt', 'cSitConf'
                          ], ['Id'], [
                            eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
                            eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
                            eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
                            eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
                            eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
                            eveMDe_nProt, cSitConf
                          ], [Id], True);
                        end;
                      end;
                      else
                      begin
                        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' +
                        sLineBreak + 'TFmNFeSteps_0310.LerTextoEnvioLoteEvento()');
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [2]');
  end;
end;

procedure TFmNFeSteps_0310.LerTextoInutilizaNumerosNF;
var
  Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nNFIni, nNFFim: String;
  //
  Lote: Integer;
  dhRecbtoTZD: Double;
begin
  //FverXML_versao := verNFeInutNFe_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retInutNFe');
    if assigned(xmlNode) then
    begin
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      xmlNode := xmlDoc.SelectSingleNode('/retInutNFe/infInut');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefineLote(Lote) then
        begin
          FmNFeGeraXML_0310.DesmontaID_Inutilizacao(
            Id, cUF, Ano, CNPJ, Modelo, Serie, nNFIni, nNFFim);
          DmNFe_0000.QrNFeInut.Close;
          DmNFe_0000.QrNFeInut.Params[00].AsInteger := Empresa;
          DmNFe_0000.QrNFeInut.Params[01].AsString := cUF;
          DmNFe_0000.QrNFeInut.Params[02].AsString := ano;
          DmNFe_0000.QrNFeInut.Params[03].AsString := CNPJ;
          DmNFe_0000.QrNFeInut.Params[04].AsString := modelo;
          DmNFe_0000.QrNFeInut.Params[05].AsString := Serie;
          DmNFe_0000.QrNFeInut.Params[06].AsString := nNFIni;
          DmNFe_0000.QrNFeInut.Params[07].AsString := nNFFim;
          DmNFe_0000.QrNFeInut.Open;
          Lote := DmNFe_0000.QrNFeInutCodigo.Value;
          if Lote = 0 then
          begin
            Geral.MB_Erro('N�o foi poss�vel descobrir o Lote pelo ID!');
            //
            Exit;
          end else
            Geral.MB_Aviso('O Lote foi encontrado pelo ID!');
        end;
        if cStat = '102' then
        begin
          ano      := LeNoXML(xmlNode, tnxTextStr, ttx_ano);
          CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          modelo   := LeNoXML(xmlNode, tnxTextStr, ttx_mod);
          serie    := LeNoXML(xmlNode, tnxTextStr, ttx_serie);
          nNFIni   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFIni);
          nNFFim   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFFin);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeinut', False, [
          'versao', 'Id', 'tpAmb',
          'cUF', 'ano', 'CNPJ',
          //ver aqui o que fazer
          'modelo', 'Serie',
          'nNFIni', 'nNFFim', 'xJust',
          'cStat', 'xMotivo', 'dhRecbto',
          'nProt', 'dhRecbtoTZD'], ['Codigo'], [
          versao, Id, tpAmb,
          cUF, ano, CNPJ,
          modelo, Serie,
          nNFIni, nNFFim, xJust,
          cStat, xMotivo, dhRecbto,
          nProt, dhRecbtoTZD], [Lote], True);
        end;
        Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinutmsg', '', 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinutmsg', False, [
        'Codigo', 'versao', 'Id',
        'tpAmb', 'verAplic', 'cStat',
        'xMotivo', 'cUF', '_Ativo_'], [
        'Controle'], [
        Lote, versao, Id,
        tpAmb, verAplic, cStat,
        xMotivo, cUF, 1], [
        Controle], True);
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de NF-e!');
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [3]');
    //
    DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmNFeInut_0000.LocCod(Lote, Lote);
  //Close;
end;

procedure TFmNFeSteps_0310.LerTextoStatusServico();
begin
  //FverXML_versao := verConsStatServ_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � status de servi�o
    xmlNode := xmlDoc.SelectSingleNode('/retConsStatServ');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
      LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      //
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [4]');
  end;
end;

procedure TFmNFeSteps_0310.LerXML_procEventoNFe(Lista: IXMLNodeList);
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chNFe, digVal, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, xCorrecao, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_xCorrecao, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chNFe, infCCe_dhEvento, infEvento_versao: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso, cJust, infCanc_cStat: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF, _xCorreca: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
begin
  if Lista.Length > 0 then
  begin
    while Lista.Length > 0 do
    begin
      cOrgao     := '';
      tpAmb      := '';
      CNPJ       := '';
      chNFe      := '';
      dhEvento   := '';
      tpEvento   := '';
      nSeqEvento := '';
      verEvento  := '';
      xCorrecao  := '';  // Carta de correcao
      nProt      := '';  // Cancelamento
      xJust      := '';  // Cancelamento
      //
      //xmlSub  := Lista.Item[0].FirstChild;
      xmlSub  := nil;
      xmlSub  := Lista.Item[0];
      // Dados do envio ao Fisco...
      // ... dados gerais
      // Erro Aqui! Pega sempre o primeiro!
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento');
      // Nao pega nada!
      //xmlNode := xmlSub.SelectSingleNode('procEventoNFe/infEvento');
      xmlNode := xmlSub.SelectSingleNode('evento/infEvento');
      if assigned(xmlNode) then
      begin
        infEvento_versao := LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False);
        cOrgao           := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
        tpAmb            := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        CNPJ             := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
        CPF              := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
        chNFe            := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
        dhEvento         := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
        tpEvento         := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
        nSeqEvento       := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
        verEvento        := LeNoXML(xmlNode, tnxTextStr, ttx_verEvento);
        //
        XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, dhEventoTZD);
        //
      end;
(*
//Cartade correcao
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-24T19:04:22-02:00</dhEvento>
<tpEvento>110110</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
//
// Cancelamento
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-25T14:45:22-02:00</dhEvento>
<tpEvento>110111</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
*)
      // ... dados especificos de cada evento
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento/detEvento');
      xmlChild1 := xmlNode.SelectSingleNode('detEvento');

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
      if assigned(xmlChild1) then
      begin
        xCorrecao := LeNoXML(xmlChild1, tnxTextStr, ttx_xCorrecao);
        //xCondUso := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
// Fim Carta de correcao

(*
<descEvento>Cancelamento</descEvento>
<nProt>141140001628294</nProt>
<xJust>POR ESTAR EM DESACORDO COM O PEDIDO</xJust>
</detEvento>
</infEvento>
*)
        nProt     := LeNoXML(xmlChild1, tnxTextStr, ttx_nProt);
        xJust     := LeNoXML(xmlChild1, tnxTextStr, ttx_xJust);
// Fim cancelamento
      //
      //
      end;
      // Dados do retorno da consulta no Fisco
      // erro aqui
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/retEvento/infEvento');
      xmlNode := xmlSub.SelectSingleNode('retEvento/infEvento');
      if assigned(xmlNode) then
      begin
        tpEvento := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
        _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        Evento := Geral.IMV(tpEvento);
        if (Geral.IMV(_Stat) in ([135, 136])) then
        begin
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cOrgao    := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
          xMotivo    := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          xEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
          nSeqEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
          dhRegEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          //
          case Evento of
            NFe_CodEventoCCe: // = 110110;
            begin
              XXe_PF.Ajusta_dh_XXe_UTC(dhRegEvento, infCCe_dhRegEventoTZD);
              //
              if not UnNFe_PF.AtualizaDadosCCeNfeCabA(chNFe, verAplic, chNFe,
                dhRegEvento, nProt, Geral.IMV(nSeqEvento), Geral.IMV(cOrgao),
                Geral.IMV(tpAmb), Geral.IMV(tpEvento), Geral.IMV(_Stat),
                infCCe_dhRegEventoTZD)
              then
                Exit;
            end;
            NFe_CodEventoCan: // = 110111;
            begin
              XXe_PF.Ajusta_dh_XXe_UTC(dhRegEvento, infCanc_dhRecbtoTZD);
              //
              if not UnNFe_PF.AtualizaDadosCanNfeCabA(chNFe, tpAmb, verAplic,
                dhRegEvento, nProt, infEvento_versao, infCanc_dhRecbtoTZD)
              then
               Exit;
            end;
            else Geral.MB_Erro('Evento: ' + tpEvento +
            ' n�o implementado em "TFmNFeSteps_0310.LerTextoConsultaNFe()"');
          end;
        end else
          Geral.MB_Erro('C�digo de retorno de evento ' + _Stat + ' - ' +
          NFeXMLGeren.Texto_StatusNFe(Geral.IMV(_Stat), 0) + sLineBreak +
          'N�o esperado para c�digo de retorno de consulta ' + cStat +
          NFeXMLGeren.Texto_StatusNFe(Geral.IMV(cStat), 0));
      end;
      Lista.Remove(Lista.Item[0]);
    end;
  end;
end;

function TFmNFeSteps_0310.MontaNomeArqNSU(Lote: Integer): String;
var
  LoteStr, MeuNSU: String;
begin
  LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
  case RGFrmaCnslt.ItemIndex of
    1: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdultNSU.ValueVariant);
    2: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdultNSU.ValueVariant); // Zero ???
    3: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdNSU.ValueVariant);
    else
    begin
       MeuNSU := DmNFe_0000.FormataNSU_NFe(0);
       Geral.MB_Erro('Forma de consulta n�o definida!');
    end;
  end;
  Result := LoteStr + '_NSU_' + MeuNSU;
end;

procedure TFmNFeSteps_0310.MostraTextoRetorno(Texto: String);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmNFeSteps_0310.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chNFe       : Result := 'Chave NF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo NF'             ;
    ttx_serie       : Result := 'S�rie NF'              ;
    ttx_nNFIni      : Result := 'N�mero inicial NF'     ;
    ttx_nNFFin      : Result := 'N�mero final NF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    ttx_tpNF        : Result := 'Tipo de opera��o NF-e' ;
    ttx_vNF         : Result := 'Valor total da NF-e'   ;
    ttx_cSitNFe     : Result := 'Situa��o da NF-e'      ;
    ttx_cSitConf    : Result := 'Sit. Manifes. Destinat';
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    ttx_xCorrecao   : Result := 'xCorrecao'             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';

    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmNFeSteps_0310.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chNFe       : Result := 'chNFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nNFIni      : Result := 'nNFIni'            ;
    ttx_nNFFin      : Result := 'nNFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    ttx_tpNF        : Result := 'tpNF'              ;
    ttx_vNF         : Result := 'vNF'               ;
    ttx_cSitNFe     : Result := 'cSitNFe'           ;
    ttx_cSitConf    : Result := 'cSitConf'          ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    ttx_xCorrecao   : Result := 'xCorrecao'         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    //
    else           Result :=      '???';
  end;
end;

function TFmNFeSteps_0310.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag:
  TTipoTagXML; AvisaVersao: Boolean): String;
var
  Texto: String;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    if AvisaVersao then
      Geral.MB_Aviso('Vers�o do XML difere do esperado!' +
      sLineBreak + 'Vers�o informada: ' + Result + sLineBreak +
      'Verifique a vers�o do servi�o no cadastro das op��es da filial para a vers�o: '
      + Result);
  if (Tag = ttx_dhRecbto) then XXe_PF.Ajusta_dh_XXe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + XXe_PF.ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmNFeSteps_0310.LerTextoConsultaDistribuicaoDFeInteresse();
var
  Lote, Codigo: Integer;
  Ini, Fim, Tam: Integer;
  //
  N: Integer;
  Dir, Msg, Chaves, Arq, Tit: String;

  //

  retDistDFeInt_versao, retDistDFeInt_tpAmb, retDistDFeInt_cStat,
  retDistDFeInt_xMotivo, retDistDFeInt_dhResp, retDistDFeInt_verAplic,
  retDistDFeInt_ultNSU, retDistDFeInt_maxNSU: String;
  //FrmaCnslt,
  Item: Integer;
  //NSU,
  Controle: Int64;
  retDistDFeInt_dhRespTZD: Double;

  docZIP_NSU, docZIP_schema, docZIP_base64gZip: String;
  DFeSchema: Integer;
  SQLType: TSQLType;
  P: Integer;
  NoVerSchema, XML: String;
  NFeDFeSchema: TNFeDFeSchemas;

  chNFe, CNPJ, CPF, xNome, IE, dhEmi, digVal, dhRecbto, nProt, versao, vNF,
  tpAmb, tpNF, cSitNFe, cStat: String;
  dhEmiTZD, dhRecbtoTZD: Double;
  Conta: Int64;
  xmlB: IXMLDocument;
  Lista: IXMLNodeList;
  CodEvento, EventoLote, TipoEnt: Integer;
  Id, verEvento, descEvento, xCorrecao, xJust, ret_tpAmb, ret_verAplic,
  ret_cOrgao, ret_cStat, ret_xMotivo, ret_chNFe, ret_tpEvento, ret_xEvento,
  ret_nSeqEvento, ret_dhRegEvento, ret_nProt, Status, XML_Eve, XML_RetEve,
  ret_versao, ret_Id, ret_CNPJDest, ret_CPFDest, ret_emailDest: String;
  TZD_UTC, ret_TZD_UTC: Double;

  dhEvento, xEvento, cOrgao, tpEvento, nSeqEvento: String;
  dhEventoTZD: Double;
  //
  IDCtrl, FatID, FatNum, Empresa: Integer;
  XML_NFe: String;
  NFeCabA: TNFeCabA;
begin
  //FverXML_versao := '3.10';
  MyObjects.Informa(LaWait, True, 'Lendo texto de resposta do web service');
  N := 0;
  //FverXML_versao := verDowNFeDest_Versao;
  if not DefineLote(Lote) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retDistDFeInt');
    if assigned(xmlNode) then
    begin
      FverXML_versao       := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);
      retDistDFeInt_versao := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      retDistDFeInt_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      retDistDFeInt_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      retDistDFeInt_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      retDistDFeInt_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      retDistDFeInt_dhResp   := LeNoXML(xmlNode, tnxTextStr, ttx_dhResp);
      XXe_PF.Ajusta_dh_XXe(retDistDFeInt_dhResp);
      // Nao tem TZD !!!!
      retDistDFeInt_dhRespTZD := 0;
      //
      Codigo := Lote;
      //
      //FrmaCnslt      := ; <== Ver como fazer!!
      retDistDFeInt_ultNSU   := LeNoXML(xmlNode, tnxTextStr, ttx_ultNSU);
      retDistDFeInt_maxNSU   := LeNoXML(xmlNode, tnxTextStr, ttx_maxNSU);
      //
      MyObjects.Informa(LaWait, True, retDistDFeInt_xMotivo);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedfeicab', False, [
      (*'Empresa', 'Nome', 'distDFeInt_versao',
      'distDFeInt_tpAmb', 'distDFeInt_cUFAutor', 'distDFeInt_CNPJ',
      'distDFeInt_CPF', 'distDFeInt_ultNSU', 'distDFeInt_NSU',*)
      (*'FrmaCnslt',*) 'retDistDFeInt_versao', 'retDistDFeInt_tpAmb',
      'retDistDFeInt_verAplic', 'retDistDFeInt_cStat', 'retDistDFeInt_xMotivo',
      'retDistDFeInt_dhResp', 'retDistDFeInt_dhRespTZD', 'retDistDFeInt_ultNSU',
      'retDistDFeInt_maxNSU'], [
      'Codigo'], [
      (*Empresa, Nome, distDFeInt_versao,
      distDFeInt_tpAmb, distDFeInt_cUFAutor, distDFeInt_CNPJ,
      distDFeInt_CPF, distDFeInt_ultNSU, distDFeInt_NSU,*)
      (*FrmaCnslt,*) retDistDFeInt_versao, retDistDFeInt_tpAmb,
      retDistDFeInt_verAplic, retDistDFeInt_cStat, retDistDFeInt_xMotivo,
      retDistDFeInt_dhResp, retDistDFeInt_dhRespTZD, retDistDFeInt_ultNSU,
      retDistDFeInt_maxNSU], [
      Codigo], True);
      xmlList := xmlDoc.SelectNodes('/retDistDFeInt/loteDistDFeInt/docZip');
      if xmlList.Length > 0 then
      begin
        MyObjects.Informa(LaExpiraCertDigital, False, 'Encontrados ' +
        Geral.FF0(xmlList.Length) + ' registros');
        xmlB := TXMLDocument.Create;
        //
        Item := 0;
        while xmlList.Length > 0 do
        begin
          Item := Item + 1;
          MyObjects.Informa(LaWait, True, 'Verificando tipo de xml do item ' +
          Geral.FF0(Item));
          //
          docZIP_NSU        := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_NSU);
          docZIP_schema     := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_schema);
          docZIP_base64gZip := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_docZip);
          //
          P := pos('_v', docZIP_schema);
          NoVerSchema := Lowercase(Copy(docZIP_schema, 1, P - 1));
          if NoVerSchema = 'resnfe' then
            NFeDFeSchema := nfedfeschResNFe else
          if NoVerSchema = 'procnfe' then
            NFeDFeSchema := nfedfeschProcNFe else
          if NoVerSchema = 'resevento' then
            NFeDFeSchema := nfedfeschResEvento else
          if NoVerSchema = 'proceventonfe' then
            NFeDFeSchema := nfedfeschProcEvento else
          begin
            NFeDFeSchema := nfedfeschUnknown;
            Geral.MB_Erro(
            'Schema n�o implementado na distribui��o de DFe de Interesse!' +
            sLineBreak + sLineBreak + docZIP_schema + sLineBreak +
            sLineBreak + 'Abra um chamado para a DERMATEK!');
          end;
          DFeSchema := Integer(NFeDFeSchema);
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrDFe, Dmod.MyDB, [
            'SELECT Controle ',
            'FROM nfedfeiits ',
            'WHERE Codigo=' + Geral.FF0(Codigo),
            'AND docZIP_NSU=' + docZIP_NSU,
            '']);
          if QrDFe.RecordCount > 0 then
          begin
            SQLType  := stUpd;
            Controle := QrDFe.FieldByName('Controle').AsLargeInt;
          end else
          begin
            SQLType  := stIns;
            Controle := 0;
          end;
          //
          Controle := UMyMod.BPGS1I64('nfedfeiits', 'Controle', '', '', tsPos,
            SQLType, Controle);
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedfeiits', False, [
            'Codigo', 'DFeSchema', 'docZIP_NSU',
            'docZIP_schema', 'docZIP_base64gZip'], [
            'Controle'], [
            Codigo, DFeSchema, docZIP_NSU,
            docZIP_schema, docZIP_base64gZip], [
            Controle], True);
          //
          XML := UnNFe_PF.DescompactaXML(docZIP_base64gZip, MeChaves);
          //Geral.MB_Info(XML);
          MyObjects.Informa(LaWait, True, 'Lendo xml do item ' +
          Geral.FF0(Item));
          case NFeDFeSchema of
            //nfedfeschUnknown: ; // Nada!
            (*1*)nfedfeschResNFe:
            begin
              if not XMLLoadFromAnsiString(xmlB, XML) then
                dmkPF.LeTexto_Permanente(XML, FXML_Load_Failure)
              else begin
                xmlNode := xmlB.SelectSingleNode('/resNFe');
                if assigned(xmlNode) then
                begin
                  Conta       := 0;
                  versao      := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
                  tpAmb       := retDistDFeInt_tpAmb;
                  chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                  CNPJ        := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
                  CPF         := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
                  xNome       := LeNoXML(xmlNode, tnxTextStr, ttx_xNome);
                  IE          := LeNoXML(xmlNode, tnxTextStr, ttx_IE);
                  dhEmi       := LeNoXML(xmlNode, tnxTextStr, ttx_dhEmi);
                  dhEmiTZD    := 0;
                  tpNF        := LeNoXML(xmlNode, tnxTextStr, ttx_tpNF);
                  vNF         := LeNoXML(xmlNode, tnxTextStr, ttx_vNF);
                  digVal      := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                  dhRecbto    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                  dhRecbtoTZD := 0;
                  nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                  cSitNFe     := LeNoXML(xmlNode, tnxTextStr, ttx_cSitNFe);
                  //
                  UnDmkDAC_PF.AbreMySQLQuery0(QrDFe, Dmod.MyDB, [
                  'SELECT Conta ',
                  'FROM nfedfeinfe ',
                  'WHERE Controle=' + Geral.FF0(Controle),
                  'AND chNFe="' + chNFe + '"',
                  '']);
                  if QrDFe.RecordCount > 0 then
                  begin
                    SQLType := stUpd;
                    Conta   := QrDFe.FieldByName('Conta').AsLargeInt;
                  end else
                  begin
                    SQLType := stIns;
                    Conta   := 0;
                  end;
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEmi, dhEmiTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                  //
                  if dhRecbto = '' then
                    dhRecbto := '0000-00-00 00:00:00';
                  //
                  Conta := UMyMod.BPGS1I32('nfedfeinfe', 'Conta', '', '', tsPos, SQLType, Conta);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedfeinfe', False, [
                    'Codigo', 'Controle', 'versao',
                    'tpAmb', 'chNFe', 'CNPJ',
                    'CPF', 'xNome', 'IE',
                    'dhEmi', 'dhEmiTZD', 'tpNF',
                    'vNF', 'digVal', 'dhRecbto',
                    'dhRecbtoTZD', 'nProt', 'cSitNFe'], [
                    'Conta'], [
                    Codigo, Controle, versao,
                    tpAmb, chNFe, CNPJ,
                    CPF, xNome, IE,
                    dhEmi, dhEmiTZD, tpNF,
                    vNF, digVal, dhRecbto,
                    dhRecbtoTZD, nProt, cSitNFe], [
                    Conta], True) then
                  begin
                    UnNFe_PF.RegistraChaveNFeParaManifestar(chNFe, Conta);
                  end;
                end;
              end;
            end;
            (*2*)nfedfeschProcNFe:
            begin
              if not XMLLoadFromAnsiString(xmlB, XML) then
                dmkPF.LeTexto_Permanente(XML, FXML_Load_Failure)
              else begin
                xmlNode := xmlB.SelectSingleNode('/nfeProc/NFe/infNFe');
                if assigned(xmlNode) then
                begin
                  versao      := LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False);
                  //
                  xmlNode := xmlB.SelectSingleNode('/nfeProc/NFe/infNFe/ide');
                  if assigned(xmlNode) then
                  begin
                    dhEmi       := LeNoXML(xmlNode, tnxTextStr, ttx_dhEmi);
                    dhEmiTZD    := 0;
                    tpNF        := LeNoXML(xmlNode, tnxTextStr, ttx_tpNF);
                  end;
                  xmlNode := xmlB.SelectSingleNode('/nfeProc/NFe/infNFe/emit');
                  if assigned(xmlNode) then
                  begin
                    CNPJ        := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
                    CPF         := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
                    xNome       := LeNoXML(xmlNode, tnxTextStr, ttx_xNome);
                    IE          := LeNoXML(xmlNode, tnxTextStr, ttx_IE);
                  end;
                  xmlNode := xmlB.SelectSingleNode('/nfeProc/NFe/infNFe/total/ICMSTot');
                  if assigned(xmlNode) then
                  begin
                    vNF         := LeNoXML(xmlNode, tnxTextStr, ttx_vNF);
                  end;
                  xmlNode := xmlB.SelectSingleNode('/nfeProc/protNFe/infProt');
                  if assigned(xmlNode) then
                  begin
                    Conta       := 0;
                    tpAmb       := retDistDFeInt_tpAmb;
                    chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                    digVal      := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                    dhRecbto    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                    dhRecbtoTZD := 0;
                    nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                    cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                    //
                    UnDmkDAC_PF.AbreMySQLQuery0(QrDFe, Dmod.MyDB, [
                      'SELECT Conta ',
                      'FROM nfedfeinfe ',
                      'WHERE Controle=' + Geral.FF0(Controle),
                      'AND chNFe="' + chNFe + '"',
                      '']);
                    if QrDFe.RecordCount > 0 then
                    begin
                      SQLType := stUpd;
                      Conta   := QrDFe.FieldByName('Conta').AsLargeInt;
                    end else
                    begin
                      SQLType := stIns;
                      Conta   := 0;
                    end;
                    //
                    XXe_PF.Ajusta_dh_XXe_UTC(dhEmi, dhEmiTZD);
                    XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                    cSitNFe     := Geral.FF0(NFeXMLGeren.Obtem_cSitNFe_De_cStat(Geral.IMV(cStat)));
                    //
                    if dhRecbto = '' then
                      dhRecbto := '0000-00-00 00:00:00';
                    //
                    Conta := UMyMod.BPGS1I32('nfedfeinfe', 'Conta', '', '', tsPos, SQLType, Conta);
                    //
                    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedfeinfe', False, [
                      'Codigo', 'Controle', 'versao',
                      'tpAmb', 'chNFe', 'CNPJ',
                      'CPF', 'xNome', 'IE',
                      'dhEmi', 'dhEmiTZD', 'tpNF',
                      'vNF', 'digVal', 'dhRecbto',
                      'dhRecbtoTZD', 'nProt', 'cSitNFe'], [
                      'Conta'], [
                      Codigo, Controle, versao,
                      tpAmb, chNFe, CNPJ,
                      CPF, xNome, IE,
                      dhEmi, dhEmiTZD, tpNF,
                      vNF, digVal, dhRecbto,
                      dhRecbtoTZD, nProt, cSitNFe], [
                      Conta], True) then
                    begin
                      // Nao presisa? Ja foi manifestado?
                      //UnNFe_PF.RegistraChaveNFeParaManifestar(chNFe, Conta);
                      NFeCabA := TNFeCabA.Create;
                      try
                        xmlNode := xmlB.SelectSingleNode('/nfeProc/NFe');
                        if assigned(xmlNode) then
                        begin
                          XML_NFe := xmlNode.XML;
                          IDCtrl  := 0;
                          FatID   := 0;
                          FatNum  := 0;
                          Empresa := 0;
                          //
                          if NFeCabA.ObtemDadosNFeCabA(Dmod.MyDB, chNFe) then
                          begin
                            AtualizaXML_No_BD_NFeConfirmada(XML_NFe, NFeCabA);
                            //
                            IDCtrl  := NFeCabA.IDCtrl;
                            FatID   := NFeCabA.FatID;
                            FatNum  := NFeCabA.FatNum;
                            Empresa := NFeCabA.Empresa;
                          end;
                          //Arq :=
                          DmNFe_0000.SalvaXML(NFE_EXT_RET_CNF_NFE_NFE_XML, chNFe,
                            NFeXMLGeren.TipoXML(False) + XML_NFe, nil, False);
                        end;
                        xmlNode := xmlB.SelectSingleNode('/nfeProc/protNFe');
                        //
                        if assigned(xmlNode) then
                          DmNFe_0000.AtualizaXML_No_BD_AutConfirmada(chNFe, xmlNode.XML);
                        //Atualizar campos das tabelas NFeCab... NfeIts...
                        //
                        if IDCtrl = 0 then
                          IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                        if FatID = 0 then
                          FatID := VAR_FATID_0053;
                        if FatNum = 0 then
                          FatNum := Conta;
                        //
                        if (XML_NFe <> '') and (IDCtrl <> 0) then
                          ImportaDadosNFeDeXML(XML_NFe, IDCtrl, FatID, FatNum, Empresa, NFeCabA);
                      finally
                        NFeCabA.Free;
                      end;
                    end;
                  end;
                end else
                  Geral.MB_Erro('XML parcial N�o � NF-e!');
              end;
            end;
            (*3*)nfedfeschResEvento:
            begin
              if not XMLLoadFromAnsiString(xmlB, XML) then
                dmkPF.LeTexto_Permanente(XML, FXML_Load_Failure)
              else begin
                xmlNode := xmlB.SelectSingleNode('/resEvento');
                if assigned(xmlNode) then
                begin
                  Conta       := 0;
                  versao      := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
                  tpAmb       := retDistDFeInt_tpAmb;
                  cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                  CNPJ        := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
                  CPF         := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
                  chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                  dhEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
                  dhEventoTZD := 0;
                  tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                  nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                  xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
                  dhRecbto    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                  dhRecbtoTZD := 0;
                  nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, dhEventoTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                  //
                  UnDmkDAC_PF.AbreMySQLQuery0(QrDFe, Dmod.MyDB, [
                  'SELECT Conta ',
                  'FROM nfedfeieve ',
                  'WHERE Controle=' + Geral.FF0(Controle),
                  'AND chNFe="' + chNFe + '"',
                  '']);
                  if QrDFe.RecordCount > 0 then
                  begin
                    SQLType := stUpd;
                    Conta   := QrDFe.FieldByName('Conta').AsLargeInt;
                  end else
                  begin
                    SQLType := stIns;
                    Conta   := 0;
                  end;
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEmi, dhEmiTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                  //
                  Conta := UMyMod.BPGS1I32('nfedfeieve', 'Conta', '', '', tsPos, SQLType, Conta);
                  //
                  if dhRecbto = '' then
                    dhRecbto := '0000-00-00 00:00:00';
                  //
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedfeieve', False, [
                    'Codigo', 'Controle', 'versao',
                    'tpAmb', 'cOrgao', 'CNPJ',
                    'CPF', 'chNFe', 'dhEvento',
                    'dhEventoTZD', 'tpEvento', 'nSeqEvento',
                    'xEvento', 'dhRecbto', 'dhRecbtoTZD',
                    'nProt'], [
                    'Conta'], [
                    Codigo, Controle, versao,
                    tpAmb, cOrgao, CNPJ,
                    CPF, chNFe, dhEvento,
                    dhEventoTZD, tpEvento, nSeqEvento,
                    xEvento, dhRecbto, dhRecbtoTZD,
                    nProt], [
                    Conta], True) then
                  begin
                  //
                  end;
                end;
              end;
            end;
            (*4*)nfedfeschProcEvento:
            begin
              if not XMLLoadFromAnsiString(xmlB, XML) then
                dmkPF.LeTexto_Permanente(XML, FXML_Load_Failure)
              else begin
                Lista := xmlDoc.SelectNodes('/procEventoNFe');
                LerXML_procEventoNFe(Lista);

                xmlNode := xmlB.SelectSingleNode('/procEventoNFe/evento');
                if assigned(xmlNode) then
                begin
                  XML_Eve  := xmlNode.XML;
                  //Geral.MB_Info(XML_Eve);
                  versao      := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
                  //
                  xmlNode := xmlB.SelectSingleNode('/procEventoNFe/evento/infEvento');
                  if assigned(xmlNode) then
                  begin
                    Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                    cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                    tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                    CNPJ        := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
                    CPF         := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
                    chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                    dhEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
                    dhEventoTZD := 0;
                    tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                    nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                    verEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_verEvento);
                    CodEvento   := Geral.IMV(tpEvento);
                    xmlNode := xmlB.SelectSingleNode('/procEventoNFe/evento/infEvento/detEvento');
                    if assigned(xmlNode) then
                    begin
                      case TNFeEventos(CodEvento) of
                        nfeeveCCe:(*110110*)
                        begin
                          descEvento := LeNoXML(xmlNode, tnxTextStr, ttx_descEvento);
                          xCorrecao  := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
                        end;
                        nfeeveCan:(*110111*)
                        begin
                          descEvento := LeNoXML(xmlNode, tnxTextStr, ttx_descEvento);
                          nProt      := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                          xJust      := LeNoXML(xmlNode, tnxTextStr, ttx_xJust);
                        end;
                        nfeeveMDeConfirmacao(*=210200*),
                        nfeeveMDeCiencia(*=210210*),
                        nfeeveMDeDesconhece(*=210220*),
                        nfeeveMDeNaoRealizou(*=210240*):
                        begin
                          descEvento := LeNoXML(xmlNode, tnxTextStr, ttx_descEvento);
                        end;
                        else
                        (*nfeeveEPEC=110140);*)
                        begin
                          Geral.MB_Erro('"LerTextoConsultaDistribuicaoDFeInteresse"' +
                            sLineBreak + 'Evento n�o implementado:' + sLineBreak +
                            Geral.FF0(CodEvento) + sLineBreak + xmlNode.XML);
                        end;
                      end;
                    end;
                  end;
                  xmlNode := xmlB.SelectSingleNode('/procEventoNFe/retEvento');
                  if assigned(xmlNode) then
                  begin
                    FverXML_versao  := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);;
                    ret_versao      := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
                  end;
                  xmlNode := xmlB.SelectSingleNode('/procEventoNFe/retEvento/infEvento');
                  //
                  if assigned(xmlNode) then
                  begin
                    ret_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                    ret_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                    ret_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                    ret_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                    ret_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                    ret_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                    ret_chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                    ret_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                    ret_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
                    ret_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                    ret_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
                    ret_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                    ret_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
                    ret_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
                    ret_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
                    //
                    Status          := ret_cStat;
                  end;
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, dhEventoTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                  //
                  UnDmkDAC_PF.AbreMySQLQuery0(QrDFe, Dmod.MyDB, [
                  'SELECT Conta ',
                  'FROM nfedfeieve ',
                  'WHERE Controle=' + Geral.FF0(Controle),
                  'AND chNFe="' + chNFe + '"',
                  '']);
                  if QrDFe.RecordCount > 0 then
                  begin
                    SQLType := stUpd;
                    Conta   := QrDFe.FieldByName('Conta').AsLargeInt;
                  end else
                  begin
                    SQLType := stIns;
                    Conta   := 0;
                  end;
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEmi, dhEmiTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
                  //
                  Conta := UMyMod.BPGS1I32('nfedfeieve', 'Conta', '', '', tsPos, SQLType, Conta);
                  //
                  if dhRecbto = '' then
                    dhRecbto := '0000-00-00 00:00:00';
                  //
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedfeieve', False, [
                    'Codigo', 'Controle', 'versao',
                    'tpAmb', 'cOrgao', 'CNPJ',
                    'CPF', 'chNFe', 'dhEvento',
                    'dhEventoTZD', 'tpEvento', 'nSeqEvento',
                    'xEvento', 'dhRecbto', 'dhRecbtoTZD',
                    'nProt'], [
                    'Conta'], [
                    Codigo, Controle, versao,
                    tpAmb, cOrgao, CNPJ,
                    CPF, chNFe, dhEvento,
                    dhEventoTZD, tpEvento, nSeqEvento,
                    xEvento, dhRecbto, dhRecbtoTZD,
                    nProt], [
                    Conta], True) then
                  begin
                    xmlNode := xmlB.SelectSingleNode('/procEventoNFe/retEvento');
                    if assigned(xmlNode) then
                    begin
                      XML_RetEve  := xmlNode.XML;
                      //Geral.MB_Info(XML_RetEve);
                    end else
                      XML_RetEve := '';
                    //
                    XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, TZD_UTC);
                    XXe_PF.Ajusta_dh_XXe_UTC(ret_dhRegEvento, ret_TZD_UTC);
                    //
                    if CNPJ <> '' then
                      TipoEnt := 0
                    else
                      TipoEnt := 1;
                    EventoLote := 0;
                    //
                    DmNFe_0000.AtualizaXML_No_BD_EveConfirmado(
                      EventoLote, Id, cOrgao,
                      tpAmb, TipoEnt, CNPJ,
                      CPF, chNFe, dhEvento,
                      TZD_UTC, verEvento, tpEvento,
                      nSeqEvento, versao, descEvento,
                      XML_Eve, XML_retEve, Status,
                      ret_versao, ret_Id, ret_tpAmb,
                      ret_verAplic, ret_cOrgao, ret_cStat,
                      ret_xMotivo, ret_chNFe, ret_tpEvento,
                      ret_xEvento, ret_nSeqEvento, ret_CNPJDest,
                      ret_CPFDest, ret_emailDest, ret_dhRegEvento,
                      ret_TZD_UTC, ret_nProt);
                  end;
                end;
              end;
            end;
            else Geral.MB_Erro('Leitura de XML n�o implementado:' +
            sLineBreak + 'NFeDFeSchema: ' + Geral.FF0(Integer(NFeDFeSchema)));
          end;
          //base64
          (*
          Ini := pos('<nfeProc', xmlList.Item[0].XML);
          Fim := pos('</nfeProc>', xmlList.Item[0].XML);
          Tam := Fim - Ini + 10;
          XML_NFe := Copy(xmlList.Item[0].XML, Ini, Tam);
          XML_NFe := NFeXMLGeren.TipoXML(False) + XML_NFe;
          //
          Arq := DmNFe_0000.SalvaXML(
            NFE_EXT_RET_DOW_NFE_NFE_XML, chNFe, XML_NFe, nil, False);
          N := N + 1;
          Chaves := Chaves + sLineBreak + Arq;
          //
          &*)
          xmlList.Remove(xmlList.Item[0]);
          //
        end;
      end;
      (*&
      case N of
        0: Msg := 'Nenhuma NF-e foi salva!';
        1: Msg := 'Uma NF-e foi salva no diret�rio:';
        2: Msg := Geral.FF0(N) + ' NF-e foram salvas no diret�rio:';
      end;
      case N of
        0: Tit := '';
        1: Tit := 'Arquivo salvo:';
        2: Tit := 'Arquivos salvos:';
      end;
      if N > 0 then
      begin
        DmNFe_0000.ObtemDirXML(NFE_EXT_RET_DOW_NFE_NFE_XML,  Dir, False);
        Msg := Trim(Msg) + sLineBreak + sLineBreak + Dir + sLineBreak + sLineBreak +
        Tit + sLineBreak + Chaves;
        //
        Geral.MB_Info(Msg);
      end;
      &*)
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [9]');
    //
    //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmNFeDistDFeInt_0100.LocCod(Lote, Lote);
  //Close;
(*&*)
end;

procedure TFmNFeSteps_0310.LerTextoConsultaLoteNFe();
var
  Codigo, Controle, FatID, FatNum, Empresa, Status: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed,
  infProt_Id, infProt_chNFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  dhRecbtoTZD, infProt_dhRecbtoTZD: Double;
begin
  //FverXML_versao := verConsReciNFe_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);
  dhRecbto       := '0000-00-00';
  dhRecbtoTZD    := 0;
  tMed           := '0';
  //
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de lote de envio
    xmlNode := xmlDoc.SelectSingleNode('/retConsReciNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      nRec     := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      Status := DmNFe_0000.stepLoteEnvConsulta();
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Status, dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed',
          'dhRecbtoTZD'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed,
          dhRecbtoTZD
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsReciNFe/protNFe/infProt');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              //
              infProt_Id       := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_Id);
              infProt_tpAmb    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_tpAmb);
              infProt_verAplic := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_verAplic);
              infProt_chNFe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chNFe);
              infProt_dhRecbto := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dhRecbto);
              infProt_nProt    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nProt);
              infProt_digVal   := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_digVal);
              infProt_cStat    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
              infProt_xMotivo  := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'Status', 'infProt_Id', 'infProt_tpAmb',
                'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                'protNFe_versao', 'infProt_dhRecbtoTZD'
              ], ['ID', 'LoteEnv'], [
                infProt_cStat, infProt_Id, infProt_tpAmb,
                infProt_verAplic, infProt_dhRecbto, infProt_nProt,
                infProt_digVal, infProt_cStat, infProt_xMotivo,
                versao, infProt_dhRecbtoTZD
              ], [infProt_chNFe, Codigo], True) then
              begin
                // hist�rico da NF
                QrNFeCabA1.Close;
                QrNFeCabA1.Params[00].AsString  := infProt_chNFe;
                QrNFeCabA1.Params[01].AsInteger := Codigo;
                QrNFeCabA1.Open;
                if QrNFeCabA1.RecordCount > 0 then
                begin
                  FatID       := QrNFeCabA1FatID.Value;
                  FatNum      := QrNFeCabA1FatNum.Value;
                  Empresa     := QrNFeCabA1Empresa.Value;
                  //
                  Controle := DModG.BuscaProximoCodigoInt(
                    'nfectrl', 'nfecabamsg', '', 0);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
                  'FatID', 'FatNum', 'Empresa', 'Solicit',
                  'Id', 'tpAmb', 'verAplic',
                  'dhRecbto', 'nProt', 'digVal',
                  'cStat', 'xMotivo', '_Ativo_',
                  'dhRecbtoTZD'], [
                  'Controle'], [
                  FatID, FatNum, Empresa, 100(*autoriza��o*),
                  infProt_Id, infProt_tpAmb, infProt_verAplic,
                  infProt_dhRecbto, infProt_nProt, infProt_digVal,
                  infProt_cStat, infProt_xMotivo, 1,
                  infProt_dhRecbtoTZD], [
                  Controle], True);
                end else Geral.MB_Aviso('A Nota Fiscal de chave "' +
                infProt_chNFe +
                '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      //
      DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [6]');
  end;
end;

procedure TFmNFeSteps_0310.LerTextoConsultaNFe();
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chNFe, digVal, cJust, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, xCorrecao, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_xCorrecao, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chNFe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF, _xCorreca: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
begin
  //FverXML_versao := FVersaoAcao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConNFe.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    if DefineIDCtrl(IDCtrl) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA2, Dmod.MyDB, [
      'SELECT * ',
      'FROM nfecaba ',
      //'WHERE Id=:P0 AND IDCtrl=:P1'
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
      if QrNFeCabA2.RecordCount > 0 then
      begin
        // Verifica se � recibo de consulta de NFe
        xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe');
        if not assigned(xmlNode) then
          Geral.MB_Aviso(
          'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de NF-e!')
        else
        begin
          tpAmb    := '';
          verAplic := '';
          cStat    := '';
          xMotivo  := '';
          cUF      := '';
          chNFe    := '';
          dhRecbto := '';
          nProt    := '';
          digVal   := '';
          cJust    := '';
          xJust    := '';
          //
          PageControl1.ActivePageIndex := 4;
          //Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
          chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          //
          if (Geral.IMV(cStat) in ([100, 101, 110])) then
          begin
            if (chNFe <> QrNFeCabA2Id.Value) then
            begin
              Geral.MB_Erro('Chave da NF-e n�o confere: ' + sLineBreak +
              'No XML: ' + chNFe + sLineBreak +
              'No BD: ' + QrNFeCabA2Id.Value);
              //
              Exit;
            end else
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'Status'], ['ID'], [cStat], [chNFe], True);
            end;
            LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/protNFe/infProt');
            // 2014-10-27
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 100
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 100 then
              begin
                //tMed     := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
                chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'infProt_Id', 'infProt_tpAmb',
                  'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                  'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                  'infProt_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb,
                  verAplic, dhRecbto, nProt,
                  digVal, _Stat, _Motivo,
                  infProt_dhRecbtoTZD
                ], [chNFe], True);
              end;
              //
            end;
            // 2012-03-29
            // Cancelamento
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/retCancNFe/infCanc');
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 101
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 101 then
              begin
                //tMed     := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
                chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                cJust    := LeNoXML(xmlNode, tnxTextStr, ttx_cJust);
                xJust    := LeNoXML(xmlNode, tnxTextStr, ttx_xJust);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'nfCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'nfCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'nfCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
                  'nfCanc_xJust', 'infCanc_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb, verAplic,
                  dhRecbto, nProt, digVal,
                  _Stat, _Motivo, cJust,
                  xJust, infProt_dhRecbtoTZD
                ], [chNFe], True);
              end;
            end;

////////////////////////////////////////////////////////////////////////////////
            // E V E N T O S
(*
            xmlList := xmlDoc.SelectNodes('/retConsSitNFe/procEventoNFe/retEvento');
            if xmlList.Length > 0 then
            begin
              Geral.MB_Aviso('Nohs: ' + Geral.FF0(xmlList.Length));
              while xmlList.Length > 0 do
              begin
                xmlNode := xmlList.Item[0].FirstChild;
                if assigned(xmlNode) then
                begin
                  tpEvento := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                  _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
*)

            //xmlList := xmlDoc.SelectNodes('/retConsSitNFe/procEventoNFe/retEvento');
            xmlList := xmlDoc.SelectNodes('/retConsSitNFe/procEventoNFe');
            LerXML_procEventoNFe(xmlList);
            {&
            if xmlList.Length > 0 then
            begin
              //Geral.MB_Aviso('Nohs: ' + Geral.FF0(xmlList.Length));
              while xmlList.Length > 0 do
              begin
                cOrgao     := '';
                tpAmb      := '';
                CNPJ       := '';
                chNFe      := '';
                dhEvento   := '';
                tpEvento   := '';
                nSeqEvento := '';
                verEvento  := '';
                xCorrecao  := '';  // Carta de correcao
                nProt      := '';  // Cancelamento
                xJust      := '';  // Cancelamento
                //
                //xmlSub  := xmlList.Item[0].FirstChild;
                xmlSub  := nil;
                xmlSub  := xmlList.Item[0];
                // Dados do envio ao Fisco...
                // ... dados gerais
                // Erro Aqui! Pega sempre o primeiro!
                //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento');
                // Nao pega nada!
                //xmlNode := xmlSub.SelectSingleNode('procEventoNFe/infEvento');
                xmlNode := xmlSub.SelectSingleNode('evento/infEvento');
                if assigned(xmlNode) then
                begin
                  cOrgao     := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                  tpAmb      := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                  CNPJ       := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
                  CPF        := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
                  chNFe      := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                  dhEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
                  tpEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                  nSeqEvento := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                  verEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_verEvento);
                  //
                  XXe_PF.Ajusta_dh_XXe_UTC(dhEvento, dhEventoTZD);
                  //
                end;

(*
//Cartade correcao
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-24T19:04:22-02:00</dhEvento>
<tpEvento>110110</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
//
// Cancelamento
<cOrgao>41</cOrgao>
<tpAmb>2</tpAmb>
<CNPJ>02717861000110</CNPJ>
<chNFe>41141002717861000110550010000046201568000375</chNFe>
<dhEvento>2014-10-25T14:45:22-02:00</dhEvento>
<tpEvento>110111</tpEvento>
<nSeqEvento>1</nSeqEvento>
<verEvento>1.00</verEvento>
<detEvento versao="1.00">
*)
                // ... dados especificos de cada evento
                //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/evento/infEvento/detEvento');
                xmlChild1 := xmlNode.SelectSingleNode('detEvento');

(*
// carta de correcao
<descEvento>Carta de Correcao</descEvento>
<xCorrecao>QWF EF EWEW EW WE EWWER</xCorrecao>
<xCondUso>
A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
</xCondUso>
</detEvento>
*)
                if assigned(xmlChild1) then
                begin
                  xCorrecao := LeNoXML(xmlChild1, tnxTextStr, ttx_xCorrecao);
                  //xCondUso := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao);
// Fim Carta de correcao

(*
<descEvento>Cancelamento</descEvento>
<nProt>141140001628294</nProt>
<xJust>POR ESTAR EM DESACORDO COM O PEDIDO</xJust>
</detEvento>
</infEvento>
*)
                  nProt     := LeNoXML(xmlChild1, tnxTextStr, ttx_nProt);
                  xJust     := LeNoXML(xmlChild1, tnxTextStr, ttx_xJust);
// Fim cancelamento
                //
                //
                end;
                // Dados do retorno da consulta no Fisco
                // erro aqui
                //xmlNode := xmlSub.SelectSingleNode('/retConsSitNFe/procEventoNFe/retEvento/infEvento');
                xmlNode := xmlSub.SelectSingleNode('retEvento/infEvento');
                if assigned(xmlNode) then
                begin
                  tpEvento := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                  _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                  Evento := Geral.IMV(tpEvento);
                  if (Geral.IMV(_Stat) in ([135, 136])) then
                  begin
                    tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                    verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                    cOrgao    := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                    xMotivo    := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                    chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
                    tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                    xEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
                    nSeqEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                    dhRegEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
                    nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                    //
                    //
                    case Evento of
                      NFe_CodEventoCCe: // = 110110;
                      begin
                        infCCe_verAplic       := verAplic;
                        infCCe_cOrgao         := Geral.IMV(cOrgao);
                        infCCe_tpAmb          := Geral.IMV(tpAmb);
                        infCCe_CNPJ           := CNPJ;
                        infCCe_CPF            := CPF;
                        infCCe_chNFe          := chNFe;
                        infCCe_dhEvento       := dhEvento;
                        infCCe_dhEventoTZD    := dhEventoTZD;
                        infCCe_tpEvento       := Geral.IMV(tpEvento);
                        infCCe_nSeqEvento     := Geral.IMV(nSeqEvento);
                        infCCe_verEvento      := Geral.DMV_Dot(verEvento);
                        infCCe_xCorrecao      := xCorrecao;
                        infCCe_cStat          := Geral.IMV(_Stat);
                        infCCe_dhRegEvento    := dhRegEvento;
                        //infCCe_dhRegEventoTZD := dhRegEventoTZD;
                        infCCe_nProt          := nProt;

                        DmNFe_0000.LocalizaNFeInfoCCe(ChNFe, infCCe_nSeqEvento,
                          _CNPJ, _CPF, _dhEvento, _dhEventoTZD, _verEvento,
                          nCondUso, _xCorreca);
                        infCCe_nCondUso       := nCondUso;
                        //
                        //XXe_PF.Ajusta_dh_XXe_UTC(infCCe_dhEvento, infCCe_dhEventoTZD);
                        XXe_PF.Ajusta_dh_XXe_UTC(infCCe_dhRegEvento, infCCe_dhRegEventoTZD);
                        //
                        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                        'infCCe_verAplic', 'infCCe_cOrgao',
                        'infCCe_tpAmb', 'infCCe_CNPJ', 'infCCe_CPF', 'infCCe_chNFe',
                        'infCCe_dhEvento', 'infCCe_dhEventoTZD', 'infCCe_tpEvento',
                        'infCCe_nSeqEvento', 'infCCe_verEvento', 'infCCe_xCorrecao',
                        'infCCe_cStat', 'infCCe_dhRegEvento', 'infCCe_dhRegEventoTZD',
                        'infCCe_nProt', 'infCCe_nCondUso'], [
                        'ID'], [
                        infCCe_verAplic, infCCe_cOrgao,
                        infCCe_tpAmb, infCCe_CNPJ, infCCe_CPF, infCCe_chNFe,
                        infCCe_dhEvento, infCCe_dhEventoTZD, infCCe_tpEvento,
                        infCCe_nSeqEvento, infCCe_verEvento, infCCe_xCorrecao,
                        infCCe_cStat, infCCe_dhRegEvento, infCCe_dhRegEventoTZD,
                        infCCe_nProt, infCCe_nCondUso], [
                        chNFe], True);
                      end;
                      NFe_CodEventoCan: // = 110111;
                      begin
                        //
                        XXe_PF.Ajusta_dh_XXe_UTC(dhRegEvento, infCanc_dhRecbtoTZD);
                        //
                        // cJust -> Nao informa no retorno!!!
                        // xMotivo vem como carta > "Evento registrado e vinculado a NF-e"
                        xMotivo := 'Cancelamento de NF-e homologado';
                        //
                        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                          'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                          'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                          'infCanc_cStat', 'infCanc_xMotivo', (*'infCanc_cJust',*)
                          'infCanc_xJust', 'infCanc_dhRecbtoTZD'
                        ], ['ID'], [
                          Id, tpAmb, verAplic,
                          dhRecbto, nProt, digVal,
                          cStat, xMotivo, (*cJust,*)
                          xJust, infProt_dhRecbtoTZD
                        ], [chNFe], True);
                      end;
                      else Geral.MB_Erro('Evento: ' + tpEvento +
                      ' n�o implementado em "TFmNFeSteps_0310.LerTextoConsultaNFe()"');
                    end;
                  end else
                    Geral.MB_Erro('C�digo de retorno de evento ' + _Stat + ' - ' +
                    NFeXMLGeren.Texto_StatusNFe(Geral.IMV(_Stat), 0) + sLineBreak +
                    'N�o esperado para c�digo de retorno de consulta ' + cStat +
                    NFeXMLGeren.Texto_StatusNFe(Geral.IMV(cStat), 0));
                end;
                xmlList.Remove(xmlList.Item[0]);
              end;
            end;
            }
          end else
            Geral.MB_Erro('C�digo de retorno ' + cStat + ' - ' +
            NFeXMLGeren.Texto_StatusNFe(Geral.IMV(cStat), 0) + sLineBreak +
            'N�o esperado na consulta!');
        end;
      end else Geral.MB_Aviso('A Nota Fiscal de chave "' + chNFe +
      '" n�o foi localizada e ficar� sem defini��o de Autoriza��o ou Cancelamento DESTA CONSULTA!');
      if Geral.IMV(cStat) > 0 then
      begin
        if DefineIDCtrl(IDCtrl) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA2, Dmod.MyDB, [
          'SELECT * ',
          'FROM nfecaba ',
          //'WHERE Id=:P0
          //'AND IDCtrl=:P1
          'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
          '']);
            //
          if QrNFeCabA2.RecordCount > 0 then
          begin
            // hist�rico da NF
            FatID   := QrNFeCabA2FatID.Value;
            FatNum  := QrNFeCabA2FatNum.Value;
            Empresa := QrNFeCabA2Empresa.Value;
            dhRecbto    := '0000-00-00 00:00:00';
            dhRecbtoTZD := infProt_dhRecbtoTZD;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'nfectrl', 'nfecabamsg', '', 0);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_',
            'dhRecbtoTZD'], [
            'Controle'], [
            FatID, FatNum, Empresa, 100(*homologa��o*),
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1,
            dhRecbtoTZD], [
            Controle], True) then
            begin
              // Mostrar
              //Para evitar erros quando aberto em aba FmNFe_Pesq_0000.PageControl1.ActivePageIndex := 1;
            end;
          end;
          //end;
        end else Geral.MB_Aviso(
        'O Status retornou zerado e a Nota Fiscal de chave "' + chNFe +
        '" ficar� sem o hist�rico desta consulta!');
        //
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de NF-e!');
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [7]');
  end;
  try
    (*
    Para evitar erros quando aberto em aba
    if IDCtrl <> 0 then
      FmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl, False);
    *)
  except
    //
  end;
  //Close;
end;

procedure TFmNFeSteps_0310.LerTextoConsultaNFeDest(ultNSU: String);
var
  Qry: TmySQLQuery;
  //
  procedure ObtemIDCadNFe(const chNFe: String; var ID: Integer; var Tip: TSQLType);
  begin
    if chNFe = '' then
    begin
      ID := 0;
      Tip := stNil;
    end else
    begin
      Qry.Database := Dmod.MyDB;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IDCad ',
      'FROM nfedesrits ',
      'WHERE NFe_chNFe="' + chNFe + '"',
      'OR Canc_chNFe="' + chNFe + '"',
      'or CCe_chNFe="' + chNFe + '"',
      '']);
      //
      ID := Qry.FieldByName('IDCad').AsInteger;
      if ID = 0 then
      begin
        Tip := stIns;
        ID := UMyMod.BPGS1I32('nfedesrits', 'IDCad', '', '', tsPos, stIns, 0);
      end else
        Tip := stUpd;
    end;
  end;
  //
  procedure AtualizaNFe(IDCtrl: Integer; cSitNFe, cSitConf: String);
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'cSitNFe', 'cSitConf'
    ], [
    'IDCtrl'
    ], [
    Geral.IMV(cSitNFe), Geral.IMV(cSitConf)
    ], [
    IDCtrl], True);
  end;
var
  Codigo, Empresa, Controle, Conta, IDCad, NFa_IDCtrl: Integer;
  //
  verAplic, xMotivo, dhResp,
  tpAmb, cStat, indCont,
  versao: String;
  //XML: String;
  //
  NFe_chNFe, NFe_CNPJ, NFe_CPF, NFe_xNome, NFe_IE, NFe_dEmi, NFe_digVal,
  NFe_dhRecbto, Canc_chNFe, Canc_CNPJ, Canc_CPF, Canc_xNome, Canc_IE, Canc_dEmi,
  Canc_digVal, Canc_dhRecbto, CCe_chNFe, CCe_dhEvento, CCe_descEvento,
  CCe_xCorrecao, CCe_dhRecbto: String;
  //
  NFe_tpNF, NFe_cSitNFe, NFe_cSitConf, Canc_tpNF, Canc_cSitNFe, Canc_cSitConf,
  CCe_tpEvento, CCe_nSeqEvento, CCe_tpNF: String;//Integer;
  //
  NFe_NSU, NFe_vNF, Canc_NSU, Canc_vNF, CCe_NSU: String;//Double;
  //
  SQLType: TSQLType;
begin
{
TZD???
  Qry := TmySQLQuery.Create(Dmod);
  try
    //FverXML_versao := verConsNFeDest_Versao;
    FverXML_versao := ?;
    if not DefineLote(Codigo) then Exit;
    if not DefineEmpresa(Empresa) then Exit;
    if not TextoArqDefinido(FTextoArq) then Exit;
    if DefineXMLDoc() then
    begin
      // Verifica se � retorno de NF-es destinadas
      xmlNode := xmlDoc.SelectSingleNode('/retConsNFeDest');
      if assigned(xmlNode) then
      begin
        Pagecontrol1.ActivePageIndex := 4;
        //
        versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
        //
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        dhResp   := LeNoXML(xmlNode, tnxTextStr, ttx_dhResp);
        indCont  := LeNoXML(xmlNode, tnxTextStr, ttx_indCont);
        ultNSU   := LeNoXML(xmlNode, tnxTextStr, ttx_ultNSU);
        //
        FindCont := Geral.IMV(indCont);
        FultNSU  := Geral.I64(ultNSU);
        //
        XXe_PF.Ajusta_dh_XXe_UTC(dhResp, ?);
        //

        Qry.Database := Dmod.MyDB;
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM nfedesrcab ',
        'WHERE Codigo =' + Geral.FF0(Codigo),
        'AND ultNSU =' + ultNSU,
        '']);
        Controle := Qry.FieldByName('Controle').AsInteger;
        if Controle = 0 then
          SQLType := stIns
        else
          SQLType := stUpd;
        Controle := UMyMod.BPGS1I32('nfedesrcab', 'Controle', '', '', tsPos, SQLType, Controle);

        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrcab', False, [
        'Codigo', 'Empresa', 'versao',
        'tpAmb', 'verAplic', 'cStat',
        'dhResp', 'indCont', 'ultNSU',
        ?], [
        'Controle'], [
        Codigo, Empresa, versao,
        tpAmb, verAplic, cStat,
        dhResp, indCont, ultNSU,
        ?], [
        Controle], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsNFeDest/ret');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              NFe_NSU        := '0';
              NFe_chNFe      := '';
              NFe_CNPJ       := '';
              NFe_CPF        := '';
              NFe_xNome      := '';
              NFe_IE         := '';
              NFe_dEmi       := '0000-00-00 00';
              NFe_tpNF       := '-1';
              NFe_vNF        := '0';
              NFe_digVal     := '';
              NFe_dhRecbto   := '0000-00-00';
              NFe_cSitNFe    := '0';
              NFe_cSitConf   := '-1';
              Canc_NSU       := '-1';
              Canc_chNFe     := '';
              Canc_CNPJ      := '';
              Canc_CPF       := '';
              Canc_xNome     := '';
              Canc_IE        := '';
              Canc_dEmi      := '0000-00-00';
              Canc_tpNF      := '-1';
              Canc_vNF       := '0';
              Canc_digVal    := '';
              Canc_dhRecbto  := '0000-00-00';
              Canc_cSitNFe   := '0';
              Canc_cSitConf  := '-1';
              CCe_NSU        := '0';
              CCe_chNFe      := '';
              CCe_dhEvento   := '0000-00-00';
              CCe_tpEvento   := '-1';
              CCe_nSeqEvento := '-1';
              CCe_descEvento := '';
              CCe_xCorrecao  := '';
              CCe_tpNF       := '-1';
              CCe_dhRecbto   := '0000-00-00';
              //
              xmlNode := xmlList.Item[0].FirstChild;
              //
              while Assigned(xmlNode) do
              begin
                (*
                xmlNode  := xmlNode.SelectSingleNode('/retConsNFeDest/ret/resNFe');
                if assigned(xmlNode) then
                *)
                if xmlNode.NodeName = 'resNFe' then
                begin
                  NFe_NSU        := LeNoXML(xmlNode, tnxAttrStr, ttx_NSU     );
                  NFe_chNFe      := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe   );
                  NFe_CNPJ       := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ    );
                  NFe_CPF        := LeNoXML(xmlNode, tnxTextStr, ttx_CPF     );
                  NFe_xNome      := LeNoXML(xmlNode, tnxTextStr, ttx_xNome   );
                  NFe_IE         := LeNoXML(xmlNode, tnxTextStr, ttx_IE      );
                  NFe_dEmi       := LeNoXML(xmlNode, tnxTextStr, ttx_dEmi    );
                  NFe_tpNF       := LeNoXML(xmlNode, tnxTextStr, ttx_tpNF    );
                  NFe_vNF        := LeNoXML(xmlNode, tnxTextStr, ttx_vNF     );
                  NFe_digVal     := LeNoXML(xmlNode, tnxTextStr, ttx_digVal  );
                  NFe_dhRecbto   := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                  NFe_cSitNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_cSitNFe );
                  NFe_cSitConf   := LeNoXML(xmlNode, tnxTextStr, ttx_cSitConf);
                  //
                  NFa_IDCtrl     := DmNFE_0000.ObtemIDCtrlDeChaveNFe(NFe_chNFe);
                  //
                  ObtemIDCadNFe(NFe_chNFe, IDCad, SQLType);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrits', False, [
                  'Empresa', 'NFe_NSU', 'NFe_chNFe',
                  'NFe_CNPJ', 'NFe_CPF', 'NFe_xNome',
                  'NFe_IE', 'NFe_dEmi', 'NFe_tpNF',
                  'NFe_vNF', 'NFe_digVal', 'NFe_dhRecbto',
                  'NFe_cSitNFe', 'NFe_cSitConf', 'NFa_IDCtrl'
                  ], [
                  'IDCad'], [
                  Empresa, NFe_NSU, NFe_chNFe,
                  NFe_CNPJ, NFe_CPF, NFe_xNome,
                  NFe_IE, NFe_dEmi, NFe_tpNF,
                  NFe_vNF, NFe_digVal, NFe_dhRecbto,
                  NFe_cSitNFe, NFe_cSitConf, NFa_IDCtrl
                  ], [
                  IDCad], True) then
                  begin
                    Qry.Database := Dmod.MyDB;
                    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
                    'SELECT Conta ',
                    'FROM nfedesrnfe ',
                    'WHERE Controle=' + Geral.FF0(Controle),
                    'AND NFe_NSU=' + NFe_NSU,
                    '']);
                    //
                    Conta := Qry.FieldByName('Conta').AsInteger;
                    if Conta = 0 then
                    begin
                      SQLType := stIns;
                      Conta := UMyMod.BPGS1I32('nfedesrnfe', 'Conta', '', '', tsPos, stIns, 0);
                    end else
                      SQLType := stUpd;
                    Conta := UMyMod.BPGS1I32('nfedesrnfe', 'Conta', '', '', tsPos, SQLType, Conta);
                    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrnfe', False, [
                    'Codigo', 'Controle', 'IDCad',
                    'NFe_NSU', 'NFe_cSitNFe', 'NFe_cSitConf'], [
                    'Conta'], [
                    Codigo, Controle, IDCad,
                    NFe_NSU, NFe_cSitNFe, NFe_cSitConf], [
                    Conta], True);
                    //
                    AtualizaNFe(NFa_IDCtrl, NFe_cSitNFe, NFe_cSitConf);
                  end;
                end;
                //
                (*
                xmlNode  := xmlNode.SelectSingleNode('/retConsNFeDest/ret/resCanc');
                if assigned(xmlNode) then
                *)
                if xmlNode.NodeName = 'resCanc' then
                begin
                  Canc_NSU       := LeNoXML(xmlNode, tnxAttrStr, ttx_NSU      );
                  Canc_chNFe     := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe    );
                  Canc_CNPJ      := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ     );
                  Canc_CPF       := LeNoXML(xmlNode, tnxTextStr, ttx_CPF      );
                  Canc_xNome     := LeNoXML(xmlNode, tnxTextStr, ttx_xNome    );
                  Canc_IE        := LeNoXML(xmlNode, tnxTextStr, ttx_IE       );
                  Canc_dEmi      := LeNoXML(xmlNode, tnxTextStr, ttx_dEmi     );
                  Canc_tpNF      := LeNoXML(xmlNode, tnxTextStr, ttx_tpNF     );
                  Canc_vNF       := LeNoXML(xmlNode, tnxTextStr, ttx_vNF      );
                  Canc_digVal    := LeNoXML(xmlNode, tnxTextStr, ttx_digVal   );
                  Canc_dhRecbto  := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                  Canc_cSitNFe   := LeNoXML(xmlNode, tnxTextStr, ttx_cSitNFe  );
                  Canc_cSitConf  := LeNoXML(xmlNode, tnxTextStr, ttx_cSitConf );
                  //
                  NFa_IDCtrl     := DmNFE_0000.ObtemIDCtrlDeChaveNFe(Canc_chNFe);
                  //
                  ObtemIDCadNFe(Canc_chNFe, IDCad, SQLType);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrits', False, [
                  'Empresa',
                  'Canc_NSU',
                  'Canc_chNFe', 'Canc_CNPJ', 'Canc_CPF',
                  'Canc_xNome', 'Canc_IE', 'Canc_dEmi',
                  'Canc_tpNF', 'Canc_vNF', 'Canc_digVal',
                  'Canc_dhRecbto', 'Canc_cSitNFe', 'Canc_cSitConf'], [
                  'IDCad'], [
                  Empresa,
                  Canc_NSU,
                  Canc_chNFe, Canc_CNPJ, Canc_CPF,
                  Canc_xNome, Canc_IE, Canc_dEmi,
                  Canc_tpNF, Canc_vNF, Canc_digVal,
                  Canc_dhRecbto, Canc_cSitNFe, Canc_cSitConf], [
                  IDCad], True) then
                  begin
                    Qry.Database := Dmod.MyDB;
                    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
                    'SELECT Conta ',
                    'FROM nfedesrcan ',
                    'WHERE Controle=' + Geral.FF0(Controle),
                    'AND Canc_NSU=' + Canc_NSU,
                    '']);
                    //
                    Conta := Qry.FieldByName('Conta').AsInteger;
                    if Conta = 0 then
                    begin
                      SQLType := stIns;
                      Conta := UMyMod.BPGS1I32('nfedesrcan', 'Conta', '', '', tsPos, stIns, 0);
                    end else
                      SQLType := stUpd;
                    Conta := UMyMod.BPGS1I32('nfedesrcan', 'Conta', '', '', tsPos, SQLType, Conta);
                    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrcan', False, [
                    'Codigo', 'Controle', 'IDCad',
                    'Canc_NSU', 'Canc_cSitNFe', 'Canc_cSitConf'], [
                    'Conta'], [
                    Codigo, Controle, IDCad,
                    Canc_NSU, Canc_cSitNFe, Canc_cSitConf], [
                    Conta], True);
                    AtualizaNFe(NFa_IDCtrl, Canc_cSitNFe, Canc_cSitConf);
                  end;
                end;
                //
                (*
                xmlNode  := xmlNode.SelectSingleNode('/retConsNFeDest/ret/resCCe');
                if assigned(xmlNode) then
                *)
                if xmlNode.NodeName = 'resCCe' then
                begin
                  CCe_NSU        := LeNoXML(xmlNode, tnxAttrStr, ttx_NSU       );
                  CCe_chNFe      := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe     );
                  CCe_dhEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento  );
                  CCe_tpEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento  );
                  CCe_nSeqEvento := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                  CCe_descEvento := LeNoXML(xmlNode, tnxTextStr, ttx_descEvento);
                  CCe_xCorrecao  := LeNoXML(xmlNode, tnxTextStr, ttx_xCorrecao );
                  CCe_tpNF       := LeNoXML(xmlNode, tnxTextStr, ttx_tpNF      );
                  CCe_dhRecbto   := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto  );
                  //
                  ObtemIDCadNFe(CCe_chNFe, IDCad, SQLType);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrits', False, [
                  'Empresa',
                  'CCe_NSU', 'CCe_chNFe', 'CCe_dhEvento',
                  'CCe_tpEvento', 'CCe_nSeqEvento', 'CCe_descEvento',
                  'CCe_xCorrecao', 'CCe_tpNF', 'CCe_dhRecbto'], [
                  'IDCad'], [
                  Empresa,
                  CCe_NSU, CCe_chNFe, CCe_dhEvento,
                  CCe_tpEvento, CCe_nSeqEvento, CCe_descEvento,
                  CCe_xCorrecao, CCe_tpNF, CCe_dhRecbto], [
                  IDCad], True) then
                  begin
                    Qry.Database := Dmod.MyDB;
                    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
                    'SELECT Conta ',
                    'FROM nfedesrcce ',
                    'WHERE Controle=' + Geral.FF0(Controle),
                    'AND CCe_NSU=' + CCe_NSU,
                    '']);
                    //
                    Conta := Qry.FieldByName('Conta').AsInteger;
                    if Conta = 0 then
                    begin
                      SQLType := stIns;
                      Conta := UMyMod.BPGS1I32('nfedesrcce', 'Conta', '', '', tsPos, stIns, 0);
                    end else
                      SQLType := stUpd;
                    Conta := UMyMod.BPGS1I32('nfedesrcce', 'Conta', '', '', tsPos, SQLType, Conta);
                    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfedesrcce', False, [
                    'Codigo', 'Controle', 'IDCad',
                    'CCe_NSU', 'CCe_dhEvento', 'CCe_tpEvento',
                    'CCe_nSeqEvento', 'CCe_descEvento', 'CCe_xCorrecao',
                    'CCe_tpNF', 'CCe_dhRecbto'], [
                    'Conta'], [
                    Codigo, Controle, IDCad,
                    CCe_NSU, CCe_dhEvento, CCe_tpEvento,
                    CCe_nSeqEvento, CCe_descEvento, CCe_xCorrecao,
                    CCe_tpNF, CCe_dhRecbto], [
                    Conta], True);
                  end;
                end;

                //

                xmlNode := xmlNode.NextSibling;
              end;
              xmlList.Remove(xmlList.Item[0]);
            end;
          end
        end;
        //
        //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
        //
      end else Geral.MB_('Arquivo XML n�o conhecido ou n�o implementado! [8]',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TFmNFeSteps_0310.LerTextoDownloadNFeConfirmadas;
var
  Lote, Codigo: Integer;
  ret_versao, ret_tpAmb, ret_verAplic, ret_cStat, ret_xMotivo, ret_dhResp,
  chNFe
  //
{
  , xMotivo, cStat, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nNFIni, nNFFim}: String;

  Ini, Fim, Tam: Integer;
  XML_NFe: String;
  //
  N: Integer;
  Dir, Msg, Chaves, Arq, Tit, ChavesNao: String;
  ret_dhRespTZD: Double;
begin
  N := 0;
  ChavesNao := '';
  //FverXML_versao := verDowNFeDest_Versao;
  FverXML_versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDowNFe.Value, 2, siNegativo);;
  if not DefineLote(Lote) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retDownloadNFe');
    if assigned(xmlNode) then
    begin
      ret_versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      ret_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      ret_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      ret_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      ret_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      ret_dhResp   := LeNoXML(xmlNode, tnxTextStr, ttx_dhResp);
      XXe_PF.Ajusta_dh_XXe_UTC(ret_dhResp, ret_dhRespTZD);
      //
      Codigo := Lote;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecnfdowc', False, [
      'ret_versao', 'ret_xMotivo',
      'ret_tpAmb', 'ret_verAplic', 'ret_cStat',
      'ret_dhResp', 'ret_dhRespTZD'], [
      'Codigo'], [
      ret_versao, ret_xMotivo,
      ret_tpAmb, ret_verAplic, ret_cStat,
      ret_dhResp, ret_dhRespTZD], [
      Codigo], True);
      //
      xmlList := xmlDoc.SelectNodes('/retDownloadNFe/retNFe');
      if xmlList.Length > 0 then
      begin
        while xmlList.Length > 0 do
        begin
          chNFe            := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chNFe);
          ret_cStat        := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
          ret_xMotivo      := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);

          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecnfdowi', False, [
          'ret_cStat', 'ret_xMotivo'
          ], [
          'Codigo', 'chNFe'], [
          ret_cStat, ret_xMotivo
          ], [
          Codigo, chNFe], True);
          //

          Ini := pos('<nfeProc', xmlList.Item[0].XML);
          Fim := pos('</nfeProc>', xmlList.Item[0].XML);
          if Fim > 0 then
          begin
            Tam := Fim - Ini + 10;
            XML_NFe := Copy(xmlList.Item[0].XML, Ini, Tam);
            XML_NFe := NFeXMLGeren.TipoXML(False) + XML_NFe;
            //
            Arq := DmNFe_0000.SalvaXML(
              NFE_EXT_RET_DOW_NFE_NFE_XML, chNFe, XML_NFe, nil, False);
            N := N + 1;
            Chaves := Chaves + sLineBreak + Arq;
          end else
            ChavesNao := ChavesNao + sLineBreak + chNFe;
          //
          xmlList.Remove(xmlList.Item[0]);
        end;
      end;
      case N of
        0: Msg := 'Nenhuma NF-e foi salva!';
        1: Msg := 'Uma NF-e foi salva no diret�rio:';
        2: Msg := Geral.FF0(N) + ' NF-e foram salvas no diret�rio:';
      end;
      case N of
        0: Tit := '';
        1: Tit := 'Arquivo salvo:';
        2: Tit := 'Arquivos salvos:';
      end;
      if N > 0 then
      begin
        DmNFe_0000.ObtemDirXML(NFE_EXT_RET_DOW_NFE_NFE_XML,  Dir, False);
        Msg := Trim(Msg) + sLineBreak + sLineBreak + Dir + sLineBreak + sLineBreak +
        Tit + sLineBreak + Chaves;
        //
        Geral.MB_Info(Msg);
      end;
    end else
      Geral.MB_Erro('Arquivo XML n�o conhecido ou n�o implementado! [9]');
    if ChavesNao <> '' then
      Geral.MB_Aviso(
      'A(s) NF-e(s) abaixo n�o foi(ram) salva(s) no banco de dados:' +
      sLineBreak + ChavesNao);
    //
    //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end else
    Geral.MB_Aviso('Documento XML n�o definido! [9]');
  {$IFNDef semNFe_v0200}
  FmNFeCnfDowC_0100.LocCod(Lote, Lote);
  {$EndIF}
  //Close;
end;

procedure TFmNFeSteps_0310.LerTextoDownloadNFeDestinadas();
var
  Lote, Codigo: Integer;
  ret_versao, ret_tpAmb, ret_verAplic, ret_cStat, ret_xMotivo, ret_dhResp,
  chNFe
  //
{
  , xMotivo, cStat, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nNFIni, nNFFim}: String;

  Ini, Fim, Tam: Integer;
  XML_NFe: String;
  //
  N: Integer;
  Dir, Msg, Chaves, Arq, Tit: String;
begin
{
TZD???
  N := 0;
  //FverXML_versao := verDowNFeDest_Versao;
  FverXML_versao := ?;
  if not DefineLote(Lote) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retDownloadNFe');
    if assigned(xmlNode) then
    begin
      ret_versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      ret_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      ret_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      ret_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      ret_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      ret_dhResp   := LeNoXML(xmlNode, tnxTextStr, ttx_dhResp);
      XXe_PF.Ajusta_dh_XXe_UTC(ret_dhResp, ?);
      //
      Codigo := Lote;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedesdowc', False, [
      'ret_versao', 'ret_xMotivo',
      'ret_tpAmb', 'ret_verAplic', 'ret_cStat',
      'ret_dhResp', ?], [
      'Codigo'], [
      ret_versao, ret_xMotivo,
      ret_tpAmb, ret_verAplic, ret_cStat,
      ret_dhResp, ?], [
      Codigo], True);
      //
      xmlList := xmlDoc.SelectNodes('/retDownloadNFe/retNFe');
      if xmlList.Length > 0 then
      begin
        while xmlList.Length > 0 do
        begin
          chNFe            := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chNFe);
          ret_cStat        := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
          ret_xMotivo      := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);

          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedesdowi', False, [
          'ret_cStat', 'ret_xMotivo'
          ], [
          'Codigo', 'chNFe'], [
          ret_cStat, ret_xMotivo
          ], [
          Codigo, chNFe], True);
          //

          Ini := pos('<nfeProc', xmlList.Item[0].XML);
          Fim := pos('</nfeProc>', xmlList.Item[0].XML);
          Tam := Fim - Ini + 10;
          XML_NFe := Copy(xmlList.Item[0].XML, Ini, Tam);
          XML_NFe := NFeXMLGeren.TipoXML(False) + XML_NFe;
          //
          Arq := DmNFe_0000.SalvaXML(
            NFE_EXT_RET_DOW_NFE_NFE_XML, chNFe, XML_NFe, nil, False);
          N := N + 1;
          Chaves := Chaves + sLineBreak + Arq;
          //
          xmlList.Remove(xmlList.Item[0]);
          //
        end;
      end;
      case N of
        0: Msg := 'Nenhuma NF-e foi salva!';
        1: Msg := 'Uma NF-e foi salva no diret�rio:';
        2: Msg := Geral.FF0(N) + ' NF-e foram salvas no diret�rio:';
      end;
      case N of
        0: Tit := '';
        1: Tit := 'Arquivo salvo:';
        2: Tit := 'Arquivos salvos:';
      end;
      if N > 0 then
      begin
        DmNFe_0000.ObtemDirXML(NFE_EXT_RET_DOW_NFE_NFE_XML,  Dir, False);
        Msg := Trim(Msg) + sLineBreak + sLineBreak + Dir + sLineBreak + sLineBreak +
        Tit + sLineBreak + Chaves;
        //
        Geral.MB_Info(Msg);
      end;
    end else Geral.MB_(
    'Arquivo XML n�o conhecido ou n�o implementado! [9]',
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmNFeDesDowC_0100.LocCod(Lote, Lote);
  //Close;
}
end;

procedure TFmNFeSteps_0310.BtAbrirClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    AbreArquivoSelecionado(Arquivo);
end;

function TFmNFeSteps_0310.AbreArquivoSelecionado(Arquivo: String): Boolean;
begin
(*
var
  Lista: TStringList;
begin
  // cria uma lista de strings para armazenar o conte�do em log
  Lista := TStringList.Create;
  try
    try
      // se o log ja existe carrega ele
      if FileExists(Arquivo) then
        Lista.LoadFromFile(Arquivo);
      // adiciona a nova string ao log
      Lista.Add(Geral.FDT(Now(), 109) + ' > ' + Msg);
    except
      on e: exception do
        Lista.Add(Geral.FDT(Now(), 109) + ' >  Erro ' + E.Message);
    end;
  finally
    // atualiza o log
    Lista.SaveToFile(Arquivo);
    // libera a lista
    Lista.Free;
  end;
end;
*)
  //FTextoArq := MLAGeral.LoadFileToText(Arquivo);     / erro
  Result := False;
  if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
  begin
    MostraTextoRetorno(FTextoArq);
    if FTextoArq <> '' then
      HabilitaBotoes();
    Result := true;
  end;
end;

function TFmNFeSteps_0310.AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
var
  Dir, Arquivo: String;
begin
  Result := False;
  FTextoArq := '';
  if not DmNFe_0000.ObtemDirXML(Ext, Dir, Assinado) then
    Exit;
  Arquivo := Dir + Arq + Ext;
  if FileExists(Arquivo) then
  begin
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      Result := FTextoArq <> '';
      if Result then HabilitaBotoes();
    end;
  end else Geral.MB_Erro('Arquivo n�o localizado "' + Arquivo + '"!');
end;

procedure TFmNFeSteps_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
  //Destroy;
end;

procedure TFmNFeSteps_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeSteps_0310.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MeChaves.Lines.Clear;
  {
  if FFormChamou = 'FmNFeLEnc' then
    FmNFeLEnc.LocCod(EdLote.ValueVariant, EdLote.ValueVariant);
  }
end;

procedure TFmNFeSteps_0310.FormCreate(Sender: TObject);
begin
  //FindCont := 1;
  FultNSU  := 0;
  FNSU     := 0;
  //
  Self.Height := 730;
  ImgTipo.SQLType := stLok;
  //
  FTextoArq :='';
  FNaoExecutaLeitura := False;
  FSecWait := 15;
  Timer1.Enabled := False;
  FSegundos := 0;
  //
  PageControl1.ActivePageIndex := 0;
  FSiglas_WS  := Geral.SiglasWebService();
  FFormChamou := '';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando conforme solicita��o');
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdLote.Enabled     := True;
    EdEmpresa.Enabled  := True;
    EdRecibo.Enabled   := True;
    //
    BtAbrir.Enabled    := True;
  end;
  //DefineVersaoAcao();
  (*
  Grade.Cells[00,00] := 'Linha';
  Grade.Cells[00,01] := '#';
  Grade.Cells[00,02] := 'ID';
  Grade.Cells[00,03] := 'Campo';
  Grade.Cells[00,04] := 'Descri��o';
  Grade.Cells[00,05] := 'Valor';
  Grade.Cells[00,06] := 'Adic.';
  Grade.Cells[00,07] := 'Observa��o';
  //
  Grade.ColWidths[00] := 40;
  Grade.ColWidths[01] := 40;
  Grade.ColWidths[02] := 40;
  Grade.ColWidths[03] := 80;
  Grade.ColWidths[04] := 300;
  Grade.ColWidths[05] := 300;
  Grade.ColWidths[06] := 28;
  Grade.ColWidths[07] := 600;
  *)
  //
  LaWait.Visible := True;
  LaWait.Font.Color := clBlue;
  MyObjects.Informa(LaWait, False, '...');
end;

procedure TFmNFeSteps_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeSteps_0310.HabilitaBotoes(Visivel: Boolean = True);
begin
  PnConfirma.Visible := Visivel;
end;

procedure TFmNFeSteps_0310.ImportaDadosNFeDeXML(XML_NFe: String; IDCtrl, FatID,
  FatNum, Empresa: Integer; NFeCabA: TNFeCabA);
var
  Terceiro, Transporta, cab_qVol: Integer;
  cab_PesoB, cab_PesoL: Double;
  DataFiscal: TDateTime;
begin
  if UnNFeImporta_0310.ImportaDadosNFeDeXML(XML_NFe, Self, FatID, FatNum,
    Empresa, IDCtrl, Terceiro, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
    DataFiscal, NFeCabA) then
  begin
    // Parei aqui!  2014-11-26
  end;
end;

function TFmNFeSteps_0310.InsUpdNFeCab(Status: Integer; SQLType: TSQLType;
  Empresa, Cliente, FisRegCad, CartEmiss, TabelaPrc, CondicaoPg, ModFrete,
  Transporta: Integer; ide_natOp: String;
  ide_indPag: Integer; ide_serie: Variant; ide_nNF: Integer; ide_dEmi,
  ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis, ide_finNFe, Retirada, Entrega,
  FatID, FatNum, IDCtrl: Integer; infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC, Exporta_UFEmbarq,
  Exporta_xLocEmbarq, SQL_FAT_ITS: String; FreteExtra, SegurExtra,
  DespAcess: Double; cNF_Atual: Integer; ide_hSaiEnt: String; ide_dhCont:
  TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  ide_idDest: Integer; ide_hEmi: String; ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  ide_indFinal, ide_indPres: Integer;
  //
  VersaoNFe: Integer; NFeConjugada: Boolean; var dest_indIEDest: Integer): Boolean;
var
  NFe_Id: String;
  ide_cNF, ide_cUF, ide_mod: Integer;
  versao: Double;
  //
  ide_tpAmb, ide_cMunFG, ide_tpImp, emit_cMun, emit_cPais, dest_cMun,
  dest_cPais, retirada_cMun, entrega_cMun, _Ativo_: Integer;
  ide_verProc, emit_CNPJ, emit_CPF, emit_Doc, emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro, emit_xMun, emit_UF, emit_CEP, emit_xPais,
  emit_fone, emit_IE, emit_IEST, emit_IM, emit_CNAE, dest_CNPJ, dest_CPF,
  dest_idEstrangeiro, dest_xNome,
  dest_xLgr, dest_nro, dest_xCpl, dest_xBairro, dest_xMun, dest_UF, dest_CEP,
  dest_xPais, dest_fone, dest_IE, dest_ISUF, retirada_CNPJ, retirada_xLgr,
  retirada_nro, retirada_xCpl, retirada_xBairro, retirada_xMun, retirada_UF,
  entrega_CNPJ, entrega_xLgr, entrega_nro, entrega_xCpl, entrega_xBairro,
  entrega_xMun, entrega_UF, ide_cDV, DataFiscal: String;
  retirada_, entrega_: Boolean;
  //
  Transporta_CNPJ, Transporta_CPF, Transporta_XNome, Transporta_IE,
  Transporta_XEnder, Transporta_XMun, Transporta_UF: String;
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet,
  RetTransp_vICMSRet: Double;
  RetTransp_CFOP, RetTransp_CMunFG,
  //VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Cobr_Fat_NFat: String;
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq: Double;
  CodInfoEmit, CodInfoDest, NFeNT2013_003LTT: Integer;
  // NFe 3.10
  EstrangDef: Integer;
begin
  dest_indIEDest := -1;
  DmNFe_0000.ReopenNFeLayI();
  Result := False;
  if not DmNFe_0000.ExcluiNfe(Status, FatID, FatNum, Empresa, False, False) then Exit;
  //
  FMsg := '';
  if not DmNFe_0000.ReopenOpcoesNFe(Empresa, True) then Exit;
  NFeNT2013_003LTT := DmNFe_0000.QrOpcoesNFeNFeNT2013_003LTT.Value;
  // 2017-01-30 In�cio
  if not UnNFe_PF.VerificaSeCalculaTributos(NFeNT2013_003LTT,
    DModG.QrPrmsEmpNFeNFe_indFinalCpl.Value, ide_indFinal) then
  begin
    NFeNT2013_003LTT := 0;
  end;
  // 2017-01-30 Fim
  //
  DmNFe_0000.ReopenDest(Cliente);
  if DmNFe_0000.QrDestCodigo.Value <> Cliente then
    FMsg := 'Destinat�rio n�o localizado!';
  dest_indIEDest := DmNFe_0000.QrDestindIEDest.Value;
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  if DmNFe_0000.QrEmpresaCodigo.Value <> Empresa then
    FMsg := 'Empresa n�o localizada!';
  // S� simples federal! - Parei aqui
  if DmNFe_0000.QrFilialSimplesFed.Value = 0 then
    Geral.MB_Aviso('C�lculo de impostos em fase de implementa��o!' + sLineBreak +
    'Verifique a exatid�o dos valores e comunique � DERMATEK qualquer erro!');
  //
  if Transporta <> 0 then
  begin
    DmNFe_0000.ReopenTransporta(Transporta);
    if DmNFe_0000.QrTransportaCodigo.Value <> Transporta then
      FMsg := 'Transportadora n�o localizada!';
  end;
  //
  if Empresa = 0 then
    FMsg := 'Empresa inv�lida!';
  //
  if Cliente = 0 then
    FMsg := 'Cliente inv�lido!';
  //
  versao := Configura_versao(DmNFe_0000.QrOpcoesNFeversao.Value, VersaoNFe, FMsg);
  //
  ide_cUF := Configura_ide_cUF(DmNFe_0000.QrEmpresaDTB_UF.Value, FMsg);
  ide_mod := Configura_ide_mod(DmNFe_0000.QrOpcoesNFeide_mod.Value);
  {  Errado!
  if ide_tpNF = 0 then // entrada
    ide_cMunFG  := DefI('16', 'B12', 1, 9999999, QrClienteCodMunici.Value)
  else // saida
  }
    ide_cMunFG  := DefI('16', 'B12', 1, 9999999, Trunc(DmNFe_0000.QrEmpresaCodMunici.Value));
  //
  ide_tpImp   := DefI('25', 'B21', 1, 2, DmNFe_0000.QrOpcoesNFeide_tpImp.Value);
  ide_tpAmb   := DefI('28', 'B24', 1, 2, DmNFe_0000.QrOpcoesNFeide_tpAmb.Value);
  ide_verProc := DefX('29b', 'B27', DBCheck.Obtem_verProc);
  //
  //evitar cpf no cnpj
  emit_Doc    := Configura_emit_Doc(DmNFe_0000.QrEmpresaTipo.Value,
                 DmNFe_0000.QrEmpresaCNPJ.Value, DmNFe_0000.QrEmpresaCPF.Value, FMsg);
  emit_xNome  := DefX('32', 'C03', DmNFe_0000.QrEmpresaNO_ENT.Value);
  emit_xFant  := DefX('33', 'C04', DmNFe_0000.QrEmpresaFANTASIA.Value);
  emit_xLgr   := DefX('35', 'C06', Trim(DmNFe_0000.QrEmpresaNO_LOGRAD.Value + ' ' + DmNFe_0000.QrEmpresaRua.Value));
  emit_nro    := DefX('36', 'C07', Geral.FormataNumeroDeRua(
                 DmNFe_0000.QrEmpresaRua.Value, FormatFloat('0',
                 DmNFe_0000.QrEmpresaNumero.Value), False));
  emit_xCpl   := DefX('37', 'C08', DmNFe_0000.QrEmpresaCOMPL.Value);
  emit_xBairro:= DefX('38', 'C09', DmNFe_0000.QrEmpresaBAIRRO.Value);
  emit_cMun   := DefI('39', 'C10', 1, 9999999, Trunc(DmNFe_0000.QrEmpresaCodMunici.Value));
  emit_xMun   := DefX('40', 'C11', DmNFe_0000.QrEmpresaNO_Munici.Value);
  emit_UF     := DefX('41', 'C12', DmNFe_0000.QrEmpresaNO_UF.Value);
  emit_CEP    := DefX('42', 'C13', Geral.FormataCEP_TT(Geral.FFT(DmNFe_0000.QrEmpresaCEP.Value, 0, siPositivo), '', '00000000'));
  emit_cPais  := DefI('43', 'C14', 1, 9999, Trunc(DmNFe_0000.QrEmpresaCodiPais.Value));
  emit_xPais  := DefX('44', 'C15', DmNFe_0000.QrEmpresaNO_Pais.Value);
  emit_fone   := DefX('45', 'C16', Geral.FormataTelefone_TT_NFe(DmNFe_0000.QrEmpresaTe1.Value));
  emit_IE     := DefX('46', 'C17', Trim(Uppercase(DmNFe_0000.QrEmpresaIE.Value)));
  // evitar erro no XML
  if (DmNFe_0000.QrEmpresaTipo.Value = 0) and (Trim(emit_IE) = '') then
    FMsg := 'Para pessoa jur�dica deve ser informada a sua Inscri��o Estadual. '
    + 'Caso seja isenta, informe a palavra "ISENTO" no cadastro da empresa ' +
    emit_Doc + ' - ' + emit_xNome + '.';
  emit_IEST   := DefX('47', 'C18', Trim(Uppercase(DmNFe_0000.QrEmpresaIEST.Value)));
  // 2012-09-16
  if NFeConjugada then
  begin
    emit_IM     := DefX('48', 'C19', DmNFe_0000.QrEmpresaNIRE.Value);
    emit_CNAE   := DefX('49', 'C20', DmNFe_0000.QrEmpresaCNAE.Value);
  end else
  begin
    emit_IM     := '';
    emit_CNAE   := '';
  end;
  // FIM 2012-09-16
  //
  // verificar se o municipio pertence � UF
  if (emit_cMun div 100000) <> Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(emit_UF) then
    FMsg := 'O munic�pio ' + FormatFloat('0000000', emit_cMun) +
    ' n�o pertence � UF ' + emit_UF + ' (Munic�pio do emitente)!';
  //
  //if MyObjects.FIC(DmNFe_0000.QrDestindIEDest.Value = 0, nil,
  //'Entidade sem defini��o do "Indicador de I.E." !') then
  //  Exit;
  dest_CNPJ          := '';
  dest_CPF           := '';
  dest_idEstrangeiro := '';
  EstrangDef         := DmNFe_0000.QrDestEstrangDef.Value ;
  if EstrangDef = 1 then
  begin
    dest_idEstrangeiro := DmNFe_0000.QrDestEstrangNum.Value;
    if not (Length(dest_idEstrangeiro)) in [0, 5, 20] then
      FMsg := 'Documento de estrangeiro "' + DmNFe_0000.QrDestCNPJ.Value +
      '" com tamanho inv�lido para o cliente (deve ser 0, 5, ou 20)!';
  end else
  begin
    //evitar cpf no cnpj
    if DmNFe_0000.QrDestTipo.Value = 0 then
    begin
      dest_CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrDestCNPJ.Value);
      if Length(dest_CNPJ) < 14 then
        FMsg := 'CNPJ "' + DmNFe_0000.QrDestCNPJ.Value + '" com tamanho inv�lido para o cliente!';
      dest_CNPJ := Geral.CompletaString(dest_CNPJ, '0', 14, taRightJustify, False);
    end else
    begin
      dest_CPF := Geral.SoNumero_TT(DmNFe_0000.QrDestCPF.Value);
      if Length(dest_CPF) < 11 then
        FMsg := 'CPF "' + DmNFe_0000.QrDestCPF.Value + '" com tamanho inv�lido para o cliente!';
      dest_CPF := Geral.CompletaString(dest_CPF, '0', 11, taRightJustify, False);
    end;
  end;
  dest_xNome  := DefX('65', 'E04', DmNFe_0000.QrDestNO_ENT.Value);
  dest_xLgr   := DefX('67', 'E06', Trim(DmNFe_0000.QrDestNO_LOGRAD.Value + ' ' + DmNFe_0000.QrDestRua.Value));
  dest_nro    := DefX('68', 'E07', Geral.FormataNumeroDeRua(
                 DmNFe_0000.QrDestRua.Value, FormatFloat('0', DmNFe_0000.QrDestNumero.Value), False));
  dest_xCpl   := DefX('69', 'E08', DmNFe_0000.QrDestCOMPL.Value);
  dest_xBairro:= DefX('70', 'E09', DmNFe_0000.QrDestBAIRRO.Value);
  dest_cMun   := DefI('71', 'E10', 1, 9999999, Trunc(DmNFe_0000.QrDestCodMunici.Value));
  dest_xMun   := DefX('72', 'E11', DmNFe_0000.QrDestNO_Munici.Value);
  dest_UF     := DefX('73', 'E12', DmNFe_0000.QrDestNO_UF.Value);
  dest_CEP    := DefX('74', 'E13', Geral.FormataCEP_TT(IntToStr(Trunc(DmNFe_0000.QrDestCEP.Value)), '', '00000000'));
  dest_cPais  := DefI('75', 'E14', 1, 9999, Trunc(DmNFe_0000.QrDestCodiPais.Value));//1058; //
  dest_xPais  := DefX('76', 'E15', DmNFe_0000.QrDestNO_Pais.Value);//'BRASIL'; //
  dest_fone   := DefX('77', 'E16', Geral.FormataTelefone_TT_NFe(DmNFe_0000.QrDestTe1.Value));
  dest_IE     := DefX('78', 'E17', Trim(Uppercase(DmNFe_0000.QrDestIE.Value)));
  // NFe 3.10
(*
  // evitar erro no XML
  if (DmNFe_0000.QrDestTipo.Value = 0) and (Trim(dest_IE) = '') then
    FMsg := 'Para pessoa jur�dica deve ser informada a sua Inscri��o Estadual. '
    + 'Caso seja isenta, informe a palavra "ISENTO" no cadastro da empresa ' +
    dest_CNPJ + ' - ' + dest_xNome + '.';
*)
  //'77a', 'E16a'
  //dest_indIEDest := DmNFe_0000.QrDestindIEDest.Value;
  case dest_indIEDest of
    1:
    begin
      if UpperCase(dest_IE) <> 'ISENTO' then
        dest_IE := Geral.SoNumero_TT(dest_IE);
      if Length(dest_IE) = 0 then
        FMsg := 'IE deve ser informado para entidade contribuinte do ICMS!)';

    end;
    2:
    begin
      //if dest_IE <> '' then
      if UpperCase(dest_IE) <> 'ISENTO' then
        dest_IE := Geral.SoNumero_TT(dest_IE);
      if (Length(dest_IE) = 0) and (ide_indFinal = 0)  then
        FMsg := 'IE n�o deve ser informada para entidade remetente / destinat�ria isenta de inscri��o no cadastro de contribuinte do ICMS!)';
    end;
    9:
    begin
      //if dest_IE <> '' then
      if (UpperCase(dest_IE) <> 'ISENTO') and (Trim(dest_CNPJ) <> '') then
      begin
        dest_IE := Geral.SoNumero_TT(dest_IE);
        if Length(dest_IE) = 0 then
          FMsg := 'IE do remetente / destinat�rio n�o deve ser informada em opera��es com o exterior!)';
      end;
    end
    else FMsg :=
      '"Indicador da IE do Destinat�rio" n�o informado no cadastro do cliente!';
  end;
  // Fim NFe 3.10
  dest_ISUF   := DefX('79', 'E18', Trim(Uppercase(DmNFe_0000.QrDestSUFRAMA.Value)));
  //
  // verificar se o municipio pertence � UF
  if (dest_cMun div 100000) <> Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(dest_UF) then
    FMsg := 'O munic�pio ' + FormatFloat('0000000', dest_cMun) +
    ' n�o pertence � UF ' + dest_UF + ' (Munic�pio do destinat�rio)!';

  retirada_ := (Retirada <> 0) and (Retirada <> Empresa);
  if retirada_ then
  begin
    retirada_CNPJ     := '';  // Parei Aqui
    retirada_xLgr     := '';  // ...
    retirada_nro      := '';  // ...
    retirada_xCpl     := '';  // ...
    retirada_xBairro  := '';  // ...
    retirada_cMun     := 0;   // ...
    retirada_xMun     := '';  // ...
    retirada_UF       := '';  // ...
  end else begin
    retirada_CNPJ     := '';  // ...
    retirada_xLgr     := '';  // ...
    retirada_nro      := '';  // ...
    retirada_xCpl     := '';  // ...
    retirada_xBairro  := '';  // ...
    retirada_cMun     := 0;   // ...
    retirada_xMun     := '';  // ...
    retirada_UF       := '';  // ...
  end;
  //
  entrega_ := (Entrega <> 0) and (Entrega <> Cliente);
  if entrega_ or (DmNFe_0000.QrDestL_Ativo.Value = 1) then
  begin
    entrega_CNPJ      := Geral.SoNumero_TT(DmNFe_0000.QrDestL_CNPJ.Value);
    entrega_xLgr      := DefX('91', 'G03', Trim(DmNFe_0000.QrDestNO_LLOGRAD.Value + ' ' + DmNFe_0000.QrDestLRua.Value));
    entrega_nro       := DefX('92', 'G04', Geral.FormataNumeroDeRua(
                         DmNFe_0000.QrDestRua.Value, FormatFloat('0', DmNFe_0000.QrDestLNumero.Value), False));
    entrega_xCpl      := DefX('93', 'G05', DmNFe_0000.QrDestLCompl.Value);
    entrega_xBairro   := DefX('94', 'G06', DmNFe_0000.QrDestLBairro.Value);
    entrega_cMun      := DefI('95', 'G07', 1, 9999999, DmNFe_0000.QrDestLCodMunici.Value);
    entrega_xMun      := DefX('96', 'G08', DmNFe_0000.QrDestNO_LMunici.Value);
    entrega_UF        := DefX('97', 'G09', DmNFe_0000.QrDestNO_LUF.Value);
    if Length(entrega_CNPJ) < 14 then
      FMsg := 'CNPJ "' + DmNFe_0000.QrDestCNPJ.Value + '" com tamanho inv�lido para o endere�o de entrega!';
  end else begin
    entrega_CNPJ     := '';
    entrega_xLgr     := '';
    entrega_nro      := '';
    entrega_xCpl     := '';
    entrega_xBairro  := '';
    entrega_cMun     := 0;
    entrega_xMun     := '';
    entrega_UF       := '';
  end;
  //
  //
  //  TOTAIS da NFe somente ap�s inclus�o dos itens
  //
  //evitar cpf no cnpj
  if Transporta <> 0 then
  begin
    if DmNFe_0000.QrTransportaTipo.Value = 0 then
    begin
      transporta_CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrTransportaCNPJ.Value);
      if Length(transporta_CNPJ) < 14 then
        FMsg := 'CNPJ "' + DmNFe_0000.QrTransportaCNPJ.Value +
        '" com tamanho inv�lido para a transportadora "' +
        DmNFe_0000.QrTransportaNO_ENT.Value + '"!';
      transporta_CNPJ := Geral.CompletaString(transporta_CNPJ, '0', 14, taRightJustify, False);
    end else
    begin
      transporta_CPF := Geral.SoNumero_TT(DmNFe_0000.QrTransportaCPF.Value);
      if Length(transporta_CPF) < 11 then
        FMsg := 'CPF "' + DmNFe_0000.QrTransportaCPF.Value +
        '" com tamanho inv�lido para a transportadora "' +
        DmNFe_0000.QrTransportaNO_ENT.Value + '"!';
      transporta_CNPJ := Geral.CompletaString(transporta_CNPJ, '0', 11, taRightJustify, False);
    end;
    transporta_xNome  := DmNFe_0000.QrTransportaNO_ENT.Value;
    transporta_IE     := Uppercase(DmNFe_0000.QrTransportaIE.Value);
    if transporta_IE <> 'ISENTO' then
      transporta_IE := Geral.SoNumero_TT(transporta_IE);
    transporta_xEnder := DmNFe_0000.QrTransportaENDERECO.Value;
    transporta_xMun   := DmNFe_0000.QrTransportaNO_Munici.Value;
    transporta_UF     := DmNFe_0000.QrTransportaNO_UF.Value;
    //
    RetTransp_vServ     := 0.00;  // Parei aqui
    RetTransp_vBCRet    := 0.00;  // ...
    RetTransp_pICMSRet  := 0.00;  // ...
    RetTransp_vICMSRet  := 0.00;  // ...
    RetTransp_CFOP      := '';    // ...
    RetTransp_CMunFG    := '';    // ...
  end else
  begin
    transporta_CNPJ     := '';
    transporta_CPF      := '';
    transporta_xNome    := '';
    transporta_IE       := '';
    transporta_xEnder   := '';
    transporta_xMun     := '';
    transporta_UF       := '';
    //
    RetTransp_vServ     := 0.00;
    RetTransp_vBCRet    := 0.00;
    RetTransp_pICMSRet  := 0.00;
    RetTransp_vICMSRet  := 0.00;
    RetTransp_CFOP      := '';
    RetTransp_CMunFG    := '';
  end;
  // Fatura: Deixar assim (� acertado depois nos totais!) -> TDmNFe_0000.TotaisNFe(...
  Cobr_Fat_NFat       := '';
  Cobr_Fat_vOrig      := 0.00;
  Cobr_Fat_vDesc      := 0.00;
  Cobr_Fat_vLiq       := 0.00;
  // Fim Fatura

  Exporta_UFEmbarq    := Exporta_UFEmbarq;
  Exporta_XLocEmbarq  := Exporta_xLocEmbarq;
  //
  _Ativo_           := 1;
  //
  if (emit_Doc = '') then
    FMsg := 'Emitente sem CNPJ e sem CPF!';
  if (dest_CNPJ = '') and (dest_CPF = '')
  // NFe 3.10
  and (EstrangDef = 0) then
    FMsg := 'Cliente nacional sem CNPJ e sem CPF!';
  if (emit_Doc = '') and (DmNFe_0000.QrEmpresaTipo.Value = 0) then
    FMsg := 'Emitente jur�dico sem CNPJ!';
  if (emit_Doc = '') and (DmNFe_0000.QrEmpresaTipo.Value = 1) then
    FMsg := 'Emitente pessoa f�sica sem CPF!';
  // NFe 3.10
  if EstrangDef = 0 then
  // N�o � estrangeiro
  begin
    if (dest_CNPJ = '') and (DmNFe_0000.QrDestTipo.Value = 0) then
      FMsg := 'Cliente jur�dico sem CNPJ!';
    if (dest_CPF = '') and (DmNFe_0000.QrDestTipo.Value = 1) then
      FMsg := 'Cliente pessoa f�sica sem CPF!';
  end;
  if emit_IE <> 'ISENTO' then
    emit_IE := Geral.SoNumero_TT(emit_IE);
  if Length(emit_IE) = 0 then
    FMsg := 'IE deve ser informado para entidade contribuinte do ICMS!)';
  if (Length(emit_fone) > 0) and (Length(emit_fone) < 10) then
    FMsg := 'Telefone deve ter pelo menos 10 n�meros: ' + emit_fone;
  //
  if not DmNFe_0000.MontaChaveDeAcesso(ide_cUF, ide_dEmi, emit_Doc, ide_mod,
    ide_Serie, ide_nNF, ide_cNF, ide_cDV, NFe_Id, cNF_Atual, ide_tpEmis,
    versao)
  then
    Exit;
  //
  if FMsg <> '' then
  begin
    if FMsg <> 'Exit' then
      Geral.MB_Aviso(FMsg);
  end else
  begin
    // Natureza da opera��o (txt)
    if ide_tpAmb = 2 then // Homologa��o
    begin
      ide_natOp := 'TESTE EMISSAO NOTA FISCAL ELETRONICA';
      dest_xNome := 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';

      // pediu no SCAN !!!!
      if ide_Serie >= 900 then
      begin
        // pediu no SCAN
        // Rejeicao: NF-e emitida em ambiente de homologacao com CNPJ do destinatario diferente de 99999999000191
        // pediu no SCAN
        //
        // como fazer na NFe 3.10 ???
        //
        dest_CPF := '';
        dest_CNPJ := '99999999000191';
      end;
    end;
    //
(*    Mudar futuramente aqui! ver pelo QrParamsEmpSPED_EFD_DtFiscal.Value !!!
    if ide_dSaiEnt >= ide_dEmi then
      DataFiscal := Geral.FDT(ide_dSaiEnt, 1)
    else
*)
      DataFiscal := Geral.FDT(ide_dEmi, 1);
    //
    CodInfoEmit := Empresa;
    CodInfoDest := Cliente;
    //
    if DmNFe_0000.QrEmitTipo.Value = 0 then //CNPJ
    begin
      emit_CNPJ := emit_Doc;
      emit_CPF  := '';
    end else
    begin
      emit_CNPJ := '';
      emit_CPF  := emit_Doc;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecaba', False, [
    'versao', 'Id', 'ide_cUF', 'FisRegCad',
    'CartEmiss', 'TabelaPrc', 'CondicaoPg',
    'ide_cNF', 'ide_natOp', 'ide_indPag',
    'ide_mod', 'ide_serie', 'ide_nNF',
    'ide_dEmi', 'ide_dSaiEnt', 'ide_tpNF',
    'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
    'ide_tpAmb', 'ide_finNFe', 'ide_verProc', 'emit_CNPJ',
    'emit_CPF', 'emit_xNome', 'emit_xFant',
    'emit_xLgr', 'emit_nro', 'emit_xCpl',
    'emit_xBairro', 'emit_cMun', 'emit_xMun',
    'emit_UF', 'emit_CEP', 'emit_cPais',
    'emit_xPais', 'emit_fone', 'emit_IE',
    'emit_IEST', 'emit_IM', 'emit_CNAE',
    'dest_CNPJ', 'dest_CPF',
    'dest_xNome',
    'dest_xLgr', 'dest_nro', 'dest_xCpl',
    'dest_xBairro', 'dest_cMun', 'dest_xMun',
    'dest_UF', 'dest_CEP', 'dest_cPais',
    'dest_xPais', 'dest_fone', 'dest_IE',
    'dest_ISUF',
    'ModFrete', 'Transporta_CNPJ', 'Transporta_CPF',
    'Transporta_XNome', 'Transporta_IE', 'Transporta_XEnder',
    'Transporta_XMun', 'Transporta_UF', 'RetTransp_vServ',
    'RetTransp_vBCRet', 'RetTransp_PICMSRet', 'RetTransp_vICMSRet',
    'RetTransp_CFOP', 'RetTransp_CMunFG', 'VeicTransp_Placa',
    'VeicTransp_UF', 'VeicTransp_RNTC', 'Cobr_Fat_NFat',
    'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
    'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
    'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
    'Compra_XCont', 'IDCtrl', 'FreteExtra', 'SegurExtra',
    'DataFiscal', 'CodInfoEmit', 'CodInfoDest', 'ICMSTot_vOutro',
    // NFe 2.00
    'ide_hSaiEnt', 'ide_dhCont', 'ide_xJust',
    'emit_CRT', 'dest_email', 'Vagao', 'Balsa',
    // fim NFe 2.00
    // Lei da transparencia
    'NFeNT2013_003LTT',
    // NFe 3.10
    'ide_idDest', 'ide_hEmi', 'ide_dhEmiTZD',
    'ide_dhSaiEntTZD',
    'ide_indFinal', 'ide_indPres',
    'EstrangDef', 'dest_idEstrangeiro', 'dest_indIEDest',
    //
    '_Ativo_'], ['FatID', 'FatNum', 'Empresa'], [
    versao, NFe_Id, ide_cUF, FisRegCad,
    CartEmiss, TabelaPrc, CondicaoPg,
    ide_cNF, ide_natOp, ide_indPag,
    ide_mod, ide_serie, ide_nNF,
    ide_dEmi, ide_dSaiEnt, ide_tpNF,
    ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV,
    ide_tpAmb, ide_finNFe, ide_verProc, emit_CNPJ,
    emit_CPF, emit_xNome, emit_xFant,
    emit_xLgr, emit_nro, emit_xCpl,
    emit_xBairro, emit_cMun, emit_xMun,
    emit_UF, emit_CEP, emit_cPais,
    emit_xPais, emit_fone, emit_IE,
    emit_IEST, emit_IM, emit_CNAE,
    dest_CNPJ, dest_CPF, dest_xNome,
    dest_xLgr, dest_nro, dest_xCpl,
    dest_xBairro, dest_cMun, dest_xMun,
    dest_UF, dest_CEP, dest_cPais,
    dest_xPais, dest_fone, dest_IE,
    dest_ISUF,
    ModFrete, Transporta_CNPJ, Transporta_CPF,
    Transporta_XNome, Transporta_IE, Transporta_XEnder,
    Transporta_XMun, Transporta_UF, RetTransp_vServ,
    RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet,
    RetTransp_CFOP, RetTransp_CMunFG, VeicTransp_Placa,
    VeicTransp_UF, VeicTransp_RNTC, Cobr_Fat_NFat,
    Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq,
    InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq,
    Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed,
    Compra_XCont, IDCtrl, FreteExtra, SegurExtra,
    DataFiscal, CodInfoEmit, CodInfoDest, DespAcess,
    // NFe 2.00
    ide_hSaiEnt, ide_dhCont, ide_xJust,
    emit_CRT, dest_email, Vagao, Balsa,
    // fim NFe 2.00
    // Lei da transparencia
    NFeNT2013_003LTT,
    // NFe 3.10
    ide_idDest, ide_hEmi, ide_dhEmiTZD,
    ide_dhSaiEntTZD, ide_indFinal, ide_indPres,
    EstrangDef, dest_idEstrangeiro, dest_indIEDest,
    //
    _Ativo_], [FatID, FatNum, Empresa], True);
    //
    if retirada_ then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabf', False, [
      'retirada_CNPJ', 'retirada_xLgr',
      'retirada_nro', 'retirada_xCpl', 'retirada_xBairro',
      'retirada_cMun', 'retirada_xMun', 'retirada_UF', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa'], [
      retirada_CNPJ, retirada_xLgr,
      retirada_nro, retirada_xCpl, retirada_xBairro,
      retirada_cMun, retirada_xMun, retirada_UF,_Ativo_], [
      FatID, FatNum, Empresa], True);
    end;
    //
    if entrega_ or (DmNFe_0000.QrDestL_Ativo.Value = 1) then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabg', False, [
      'entrega_CNPJ', 'entrega_xLgr', 'entrega_nro',
      'entrega_xCpl', 'entrega_xBairro', 'entrega_cMun',
      'entrega_xMun', 'entrega_UF', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa'], [
      entrega_CNPJ, entrega_xLgr, entrega_nro,
      entrega_xCpl, entrega_xBairro, entrega_cMun,
      entrega_xMun, entrega_UF, _Ativo_], [
      FatID, FatNum, Empresa], True);
    end;
    //
    UnNFe_PF.MostraFormNFeCabGA(FatID, FatNum, Empresa, CodInfoDest, True);
  end;
end;

function TFmNFeSteps_0310.Configura_emit_Doc(Tipo: Integer; CNPJ, CPF: String;
  var Msg: String): String;
var
  emit_CNPJ, emit_CPF: String;
begin
  Result := '';
  //
  if Tipo = 0 then
  begin
    emit_CNPJ := Geral.SoNumero_TT(CNPJ);
    //
    if Length(emit_CNPJ) < 14 then
      Msg := 'CNPJ com tamanho inv�lido para o emitente!';
    //
    emit_CNPJ := Geral.CompletaString(emit_CNPJ, '0', 14, taRightJustify, False);
    Result    := emit_CNPJ;
  end else
  begin
    emit_CPF := Geral.SoNumero_TT(CPF);
    //
    if Length(emit_CPF) < 11 then
      Msg := 'CPF com tamanho inv�lido para o emitente!';
    //
    emit_CPF := Geral.CompletaString(emit_CPF, '0', 11, taRightJustify, False);
    Result   := emit_CPF;
  end;
end;

function TFmNFeSteps_0310.Configura_ide_cUF(DTB_UF: String;
  var Msg: String): Integer;
var
  ide_cUF: Integer;
begin
  ide_cUF := Geral.IMV(DTB_UF);
  //
  if (ide_cUF < FMinUF_IBGE) or (ide_cUF > FMaxUF_IBGE) then
    Msg := 'C�digo IBGE da UF inv�lido: ' + DTB_UF;
  //
  Result := ide_cUF;
end;

function TFmNFeSteps_0310.Configura_ide_mod(ide_mod: Integer): Integer;
begin
  Result := DefI('10', 'B06', 55, 55, ide_mod);
end;

function TFmNFeSteps_0310.Configura_versao(NFeversao: Double;
  VersaoNFe: Integer; var Msg: String): Double;
begin
  Result := NFeversao;
  //
  if Trunc((NFeversao * 100) + 0.5) <> VersaoNFe then
    Msg := 'Vers�o NF-e n�o suportada neste aplicativo: ' + FloatToStr(NFeversao);
end;

procedure TFmNFeSteps_0310.ConsultaCadastroContribuinte();
begin
  Screen.Cursor := crHourGlass;
  try
    FTextoArq := FmNFeGeraXML_0310.WS_NFeConsultaCadastro(
      EdUF_Servico.Text, EdContribuinte_UF.Text,
      EdContribuinte_CNPJ.Text, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    ///  Parei aqui!
    //LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFeSteps_0310.CriaNFeNormal(Recria: Boolean; NFeStatus, FatID, FatNum,
  Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
  FisRegCad, CartEmiss, TabelaPrc, CondicaoPg: Integer; FreteVal, Seguro,
  Outros: Double; ide_Serie: Variant; ide_nNF: Integer; ide_dEmi,
  ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis: Integer; infAdic_infAdFisco,
  infAdic_infCpl, VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS,
  SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String; UF_Emit, UF_Dest: String;
  GravaCampos, cNF_Atual, Financeiro: Integer; ide_hSaiEnt: String; ide_dhCont:
  TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  idDest: Integer; ide_hEmi: String; ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  indFinal, indPres, finNFe: Integer;
  //
  ApenasCriaXML: Boolean; CalculaAutomatico: Boolean): Boolean;
(*
const
  ide_finNFe = 1; //1- NF-e normal/ 2-NF-e complementar / 3 � NF-e de ajuste
*)
var
  ide_cUF, ide_mod, ide_cNF,
  Retirada, Entrega, Status: Integer;
  //
  emit_Doc, ide_cDV, NFe_Id,
  XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir, ide_natOp: String;
  //
  Continua: Boolean;
  ICMS_Usa, PIS_Usa, COFINS_Usa, ISS_Usa, IPI_Usa, II_Usa, dest_indIEDest: Integer;
  versao,
  ISS_Alq: Double;
begin
  Result := False;
  // FretePor > N�o usa. Por qu�? Parei Aqui!
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inciando processo');
  //
  Status := 0;
  try
    if not ApenasCriaXML then   // 2010-10-22
    begin
      DmNFe_0000.QrFisRegCad.Close;
      DmNFe_0000.QrFisRegCad.Params[0].AsInteger := FisRegCad;
      DmNFe_0000.QrFisRegCad.Open;
      //
      ide_natOp  := DmNFe_0000.QrFisRegCadide_natOP .Value;
      ICMS_Usa   := DmNFe_0000.QrFisRegCadICMS_Usa  .Value;
      PIS_Usa    := DmNFe_0000.QrFisRegCadPIS_Usa   .Value;
      COFINS_Usa := DmNFe_0000.QrFisRegCadCOFINS_Usa.Value;
      ISS_Usa    := DmNFe_0000.QrFisRegCadISS_Usa   .Value;
      ISS_Alq    := DmNFe_0000.QrFisRegCadISS_Alq   .Value;
      IPI_Usa    := DmNFe_0000.QrFisRegCadIPI_Usa   .Value;
      II_Usa     := DmNFe_0000.QrFisRegCadII_Usa    .Value;
      //
      XMLGerado_Arq   := '';
      XMLGerado_Dir   := '';
      XMLAssinado_Dir := '';
      Retirada        := 0;
      Entrega         := 0;
      Continua        := False;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo status da NFe');
      //
      Status := DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
      //
      if (Status <= 0) or Recria then
      begin
        if Recria then Status := 0;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo registro de cabe�alho da NFe no banco de dados');
        //
        if InsUpdNFeCab(NFeStatus, stIns, Empresa, Cliente, FisRegCad,
          CartEmiss, TabelaPrc, CondicaoPg, modFrete, Transporta, ide_natOp,
          ide_indPag, ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,
          ide_tpEmis, finNFe, Retirada, Entrega, FatID, FatNum, IDCtrl,
          infAdic_infAdFisco, infAdic_infCpl, VeicTransp_Placa, VeicTransp_UF,
          VeicTransp_RNTC, Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_FAT_TOT,
          FreteVal, Seguro, Outros, cNF_Atual, ide_hSaiEnt, ide_dhCont,
          ide_xJust, emit_CRT, dest_email, Vagao, Balsa, Compra_XNEmp,
          Compra_XPed, Compra_XCont,
          // NFe 3.10
          idDest, ide_hEmi, ide_dhEmiTZD, ide_dhSaiEntTZD, indFinal, indPres,
          //
          FVersaoNFe, False, dest_indIEDest) then
        begin
          //if Continua then
          if Financeiro > 0 then
            Continua := DmNFe_0000.InsUpdNFeCabY(FatID, FatNum, 
              Empresa, SQL_FAT_ITS, LaAviso1, LaAviso2, REWarning)
            else
              Continua := True;
          if Continua then
            Continua := DmNFe_0000.InsUpdNFeCabXVol(FatID, FatNum, Empresa,
              SQL_VOLUMES, LaAviso1, LaAviso2, REWarning);
          if Continua then
          begin
            Continua := DmNFe_0000.InsUpdNFeIts(FatID, FatNum, Empresa, ICMS_Usa,
                          IPI_Usa, II_Usa, PIS_Usa, COFINS_Usa, ISS_Usa, ISS_Alq,
                          UF_Emit, UF_Dest, FisRegCad, SQL_ITS_ITS, SQL_ITS_TOT,
                          SQL_CUSTOMZ, LaAviso1, LaAviso2, emit_CRT,
                          CalculaAutomatico, idDest, indFinal, dest_indIEDest,
                          ide_dEmi);
          end;
          if Continua then
            Continua := DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa,
              LaAviso1, LaAviso2, REWarning);
          if Continua then
            Continua := DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
            DmNFe_0000.stepNFeDados, LaAviso1, LaAviso2);
        end;
      end else
        Continua := True;
      //
    end else
    begin
      if not DmNFe_0000.ReopenOpcoesNFe(Empresa, True) then
        Exit;
      //
      DmNFe_0000.ReopenEmpresa(Empresa);
      DmNFe_0000.ReopenNFeLayI();
      //
      FMsg     := '';
      ide_cUF  := Configura_ide_cUF(DmNFe_0000.QrEmpresaDTB_UF.Value, FMsg);
      emit_Doc := Configura_emit_Doc(DmNFe_0000.QrEmpresaTipo.Value,
                  DmNFe_0000.QrEmpresaCNPJ.Value, DmNFe_0000.QrEmpresaCPF.Value, FMsg);
      ide_mod  := Configura_ide_mod(DmNFe_0000.QrOpcoesNFeide_mod.Value);
      versao   := Configura_versao(DmNFe_0000.QrOpcoesNFeversao.Value, FVersaoNFe, FMsg);
      //
      if DmNFe_0000.MontaChaveDeAcesso(ide_cUF, ide_dEmi, emit_Doc, ide_mod, ide_Serie,
        ide_nNF, ide_cNF, ide_cDV, NFe_Id, cNF_Atual, ide_tpEmis, versao, False) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False,
          ['Id', 'ide_cNF', 'ide_cDV'], ['FatID', 'FatNum', 'Empresa'],
          [NFe_Id, ide_cNF, ide_cDV], [FatID, FatNum, Empresa], True) then
        begin
          //DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
          //
          Continua := True; // 2010-10-22
        end;
      end else
      begin
        if FMsg <> '' then
          Geral.MB_Aviso(FMsg);
      end;
    end;
    //
    if Continua or (Status = DmNFe_0000.stepNFeDados) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Montando arquivo XML');
      //
      Continua := FmNFeGeraXML_0310.CriarDocumentoNFe(FatID, FatNum, Empresa,
        XMLGerado_Arq, XMLGerado_Dir, LaAviso1, LaAviso2, GravaCampos);
      //
      if Continua then
        Continua := DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
          DmNFe_0000.stepNFeGerada, LaAviso1, LaAviso2);
    end;
    if Continua or (Status = DmNFe_0000.stepNFeGerada) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados para assinatura do arquivo XML');
      // Assinar NF-e
      //Continua := False;
      if XMLGerado_Arq = '' then
      begin
        Status := DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
        //
        if Status = DmNFe_0000.stepNFeGerada then
        begin
          DmNFe_0000.ReopenEmpresa(Empresa);
          //
          XMLGerado_Arq := DmNFe_0000.QrNFECabAId.Value + NFE_EXT_NFE_XML;
          XMLGerado_Dir := DmNFe_0000.QrFilialDirNFeGer.Value;
        end;
      end;
      if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
      //
      if XMLGerado_Arq = '' then
        Geral.MB_Aviso('Nome do arquivo da NF-e gerada indefinido!')
      else
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML');
      //
      Continua := DmNFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir,
                    Empresa, 0, DmNFe_0000.QrNFECabAIDCtrl.Value, XMLAssinado_Dir);
      //
      if Continua then
      begin
        //Continua :=
        DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
          DmNFe_0000.stepNFeAssinada, LaAviso1, LaAviso2);
        Result := True;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFeSteps_0310.DefI(Codigo, ID: String; ValMin, ValMax,
  Numero: Integer): Integer;
  function Txt: String;
  begin
    Result := IntToStr(Numero);
  end;
begin
  Result := Numero;
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) and (Numero < ValMin) then
    begin
      if FMsg = '' then
        FMsg := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer abaixo do m�nimo!', 4);
    end else
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) and (Numero > ValMax) then
    begin
      if FMsg = '' then
        FMsg := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer acima do m�ximo!', 5);
    end;
  end else FMsg := 'N�o foi poss�vel definir o item #' + Codigo + '- ID = ' +
    ID + ' pelo layout da NF-e!';
end;

function TFmNFeSteps_0310.DefinechNFe(var chNFe: String): Boolean;
var
  K: Integer;
begin
  Result := False;
  chNFe := EdchNFe.Text;
  K := Length(chNFe);
  if K <> 44 then
    Geral.MB_Erro('Tamanho da chave difere de 44: tamanho = ' + IntToStr(K))
  else if Geral.SoNumero1a9_TT(chNFe) = '' then
    Geral.MB_Erro('Chave n�o definida!')
  else
    Result := True;
end;

function TFmNFeSteps_0310.DefineDestCNPJ(var DestCNPJ: String): Boolean;
var
  K: Integer;
begin
  DestCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(DestCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa destinat�ria com tamanho incorreto!');
  end;
end;

function TFmNFeSteps_0310.DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
var
  K: Integer;
begin
  EmitCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(EmitCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa emitente com tamanho incorreto!');
  end;
end;

function TFmNFeSteps_0310.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Empresa n�o definida!');
  end;
end;

function TFmNFeSteps_0310.DefineIDCtrl(var IDCtrl: Integer): Boolean;
begin
  IDCtrl := EdIDCtrl.ValueVariant;
  if IDCtrl <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro(
    'IDCtrl n�o definido! A��o/consulta n�o ser� inclu�da no hist�rico da NF!');
  end;
end;

function TFmNFeSteps_0310.DefineIntCNPJ(var IntCNPJ: String): Boolean;
var
  K: Integer;
begin
  IntCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(IntCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa interessada com tamanho incorreto!');
  end;
end;

function TFmNFeSteps_0310.DefineLote(var lote: Integer): Boolean;
begin
  Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Lote n�o definido!');
  end;
end;

function TFmNFeSteps_0310.DefineModelo(var Modelo: String): Boolean;
begin
  Modelo := FormatFloat('00', EdModelo.ValueVariant);
  if Modelo <> '55' then
  begin
    Result := False;
    Geral.MB_Erro('Modelo de NF-e n�o implementado: ' + Modelo);
  end else Result := True;
end;

function TFmNFeSteps_0310.DefinenNFFin(var nNFFim: String): Boolean;
begin
  if (EdnNFFim.ValueVariant = null) or (EdnNFFim.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o final de NF-e inv�lida!');
  end else begin
    nNFFim := FormatFloat('0', EdnNFFim.ValueVariant);
    Result := True;
  end;
end;

function TFmNFeSteps_0310.DefinenNFIni(var nNFIni: String): Boolean;
begin
  if (EdnNFIni.ValueVariant = null) or (EdnNFIni.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o inicial de NF-e inv�lida!');
  end else begin
    nNFIni := FormatFloat('0', EdnNFIni.ValueVariant);
    Result := True;
  end;
end;

function TFmNFeSteps_0310.DefinenProt(var nProt: String): Boolean;
var
  K: Integer;
begin
  Result := False;
  nProt := EdnProt.Text;
  K := Length(nProt);
  if K <> 15 then
    Geral.MB_Erro('Tamanho do protocolo difere de 15: tamanho = ' +
    IntToStr(K))
  else if Geral.SoNumero1a9_TT(nProt) = '' then
    Geral.MB_Erro('Protocolo n�o definido!')
  else
    Result := True;
end;

function TFmNFeSteps_0310.DefineSerie(var Serie: String): Boolean;
begin
  if (EdSerie.ValueVariant = null) or
  (EdSerie.ValueVariant < 0) or
  (EdSerie.ValueVariant > 899) then
  begin
    Result := False;
    Geral.MB_Aviso('N�mero de s�rie inv�lido!');
  end else
  begin
    Serie  := FormatFloat('0', EdSerie.ValueVariant);
    Result := True;
  end;
end;

(*
function TFmNFeSteps_0310.DefineVersaoAcao(): Boolean;
begin
  FVersaoAcao := Geral.FFT_Dot(EdVersaoAcao.ValueVariant, 2, siPositivo);
  Result := Geral.DMV_Dot(FVersaoAcao) >= NFE_EMI_VERSAO_MIN_USO;
  if not Result then
    Geral.MB_Erro('Vers�o pode estar equivocada!');
end;
*)

function TFmNFeSteps_0310.DefineXMLDoc(): Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

function TFmNFeSteps_0310.Define_indEmi(var indEmi: String): Boolean;
begin
  if (not (Integer(EdindEmi.ValueVariant)) in [0,1]) then
  begin
    Result := False;
    Geral.MB_Aviso('Indicador da emiss�o da NFe inv�lido!');
  end else
  begin
    indEmi  := FormatFloat('0', EdindEmi.ValueVariant);
    Result := True;
  end;
end;

function TFmNFeSteps_0310.Define_indNFe(var indNFe: String): Boolean;
begin
  if (not (Integer(EdindNFe.ValueVariant)) in [0,1]) then
  begin
    Result := False;
    Geral.MB_Aviso('Indicador de NF-e consultada inv�lido!');
  end else
  begin
    indNFe  := FormatFloat('0', EdindNFe.ValueVariant);
    Result := True;
  end;
end;

function TFmNFeSteps_0310.Define_NSU(var NSU: String): Boolean;
begin
  NSU  := Geral.SoNumero_TT(EdNSU.Text);
  Result := True;
end;

function TFmNFeSteps_0310.Define_ultNSU(var ultNSU: String): Boolean;
begin
  {
  if (not (EdultNSU.ValueVariant)) = Null) then
  begin
    Result := False;
    Geral.MB_('�ltimo NSU inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end else
  begin
  }
    ultNSU  := Geral.SoNumero_TT(EdultNSU.Text);
    Result := True;
  //end;
end;

function TFmNFeSteps_0310.DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
  procedure AlteraDest;
  var
    Dest: Integer;
  begin
    //
    if DmNFe_0000.QrDest.State <> dsInactive then
      Dest := DmNFe_0000.QrDestCodigo.Value
    else
      Dest := 0;
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Dest <> 0 then
        FmEntidade2.LocCod(Dest, Dest);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    if DmNFe_0000.QrDest.State <> dsInactive then
    begin
      DmNFe_0000.QrDest.Close;
      DmNFe_0000.QrDest.Open;
    end;
  end;
var
  Msg1, Msg2: String;
  Cod: String;
begin
  Msg1 := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Valor, '[Extra] ' + MsgExtra, 6);
  if FMsg = '' then FMsg := Msg1;

  //  Redirecionamento
  if Uppercase(ID[1]) = 'E' then // Emitente
  begin
    Cod := IntToStr(DmNFe_0000.QrDestCodigo.Value);
    Msg2 := 'Desejo alterar agora o cadastro da entidade n� ' + Cod;
    MLAGeral.MessageDlgCheck(Msg1, mtConfirmation, [mbOK], 0, mrOK,
      True, True, Msg2, @AlteraDest);
    FMsg := 'Exit';
  end;
  //
  Result := True;
end;

function TFmNFeSteps_0310.DefX(Codigo, ID, Texto: String): String;
var
  t: Integer;
  OK: Boolean;
begin
  //Result := ValidaTexto_XML(Texto, Codigo, ID);
  Result := Texto;
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    t := Length(Result);
    OK := False;
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) or (t > 0) then
    begin
      if DmNFe_0000.QrNFeLayITamVar.Value <> '' then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT ' + FormatFloat('0', T) + ' in (' +
        DmNFe_0000.QrNFeLayITamVar.Value + ') Tem');
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB, 'TDmNFe_0000.DefX()');
        OK := Dmod.QrAux.FieldByName('Tem').AsInteger = 1;
      end;
      if not OK then
      begin
        if t < DmNFe_0000.QrNFeLayITamMin.Value then
          DefMsg(Codigo, ID, 'Tamanho do texto abaixo do m�nimo!', Result);
        if t > DmNFe_0000.QrNFeLayITamMax.Value then
        begin
          if DmNFe_0000.QrNFeLayITamMin.Value = DmNFe_0000.QrNFeLayITamMax.Value then
            DefMsg(Codigo, ID, 'Tamanho do texto difere do esperado!', Result)
          else
            Result := Copy(Result, 1, DmNFe_0000.QrNFeLayITamMax.Value);
        end;
      end;
    end;
  end else DefMsg(Codigo, ID, 'Item n�o localizado na tabela "NFeLayI"', Texto);
end;

function TFmNFeSteps_0310.PreparaEnvioDeLoteEvento(var UF_Servico: String;
Lote, Empresa: Integer): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteEvento := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  EdUF_Servico.Text := UF_Servico;
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0310.GerarLoteEvento(Lote, Empresa, FPathLoteEvento, FXML_LoteEvento, LaAviso1, LaAviso2);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Env_Sel);
    end else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, NFE_EXT_EVE_RET_LOT_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  Result := True;
end;

function TFmNFeSteps_0310.PreparaEnvioDeLoteNFe(Lote, Empresa: Integer;
  Sincronia: TXXeIndSinc): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteNFe := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    //Continua := FmNFeGeraXML_0310.GerarLoteNFe(Lote, Empresa, FPathLoteNFe, FXML_LoteNFe, LaAviso1, LaAviso2);
    Continua := FmNFeGeraXML_0310.GerarLoteNFeNovo(Lote, Empresa, FPathLoteNFe, FXML_LoteNFe, LaAviso1, LaAviso2, Sincronia);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  Result := True;
end;

procedure TFmNFeSteps_0310.PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo, Serie,
  nNFIni, nNFFim, Justif: Integer);
var
  LoteStr, cUF, Id: String;
begin
  EdEmpresa.ValueVariant   := Empresa;
  EdLote.ValueVariant      := Lote;
  EdAno.ValueVariant       := Ano;
  EdModelo.ValueVariant    := Modelo;
  EdSerie.ValueVariant     := Serie;
  EdnNFIni.ValueVariant    := nNFIni;
  EdnNFFim.ValueVariant    := nNFFim;
  EdNFeJust.ValueVariant   := Justif;
  CBNFeJust.KeyValue       := Justif;
  //
  PnJustificativa.Enabled  := False;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmNFe_0000.QrEmpresaNO_UF.Value));
    DmNFe_0000.MontaID_Inutilizacao(cUF, EdAno.Text, EdEmitCNPJ.Text,
      FormatFloat('00', EdModelo.ValueVariant),
      FormatFloat('00', EdSerie.ValueVariant),
      FormatFloat('000000000', nNFIni),
      FormatFloat('000000000', nNFFim), Id);
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, NFE_EXT_INU_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmNFeSteps_0310.PreparaStepGenerico(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaVerificacaoStatusServico(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaCancelamentoDeNFe(Lote, Empresa: Integer;
  ChaveNFe, Protocolo: String);
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdChNFe.Text             := ChaveNFe;
  EdnProt.Text             := Protocolo;
  //
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    if AbreArquivoXML(ChaveNFe, NFE_EXT_CAN_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmNFeSteps_0310.PreparaConsultaNFe(Empresa, IDCtrl: Integer; ChaveNFe: String);
begin
  EdEmpresa.ValueVariant := Empresa;
  EdchNFe.Text           := ChaveNFe;
  EdIDCtrl.ValueVariant  := IDCtrl;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaDownloadDeNFeConfirmadas(Lote,
  Empresa: Integer);
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteDowNFeDes := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0310.GerarLoteDownloadNFeConfirmadas(Lote, Empresa,
      FPathLoteDowNFeDes, FXML_LoteDowNFeDes, LaAviso1, LaAviso2);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_RET_DOW_NFE_CNF_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  EdUF_Servico.Text := DmNFe_0000.QrOpcoesNFeUF_MDeNFe.Value; //'AN' = Ambiente Nacional
end;

procedure TFmNFeSteps_0310.PreparaDownloadDeNFeDestinadas(Lote,
  Empresa: Integer);
var
  LoteStr: String;
  Continua: Boolean;
begin
{
  FPathLoteDowNFeDes := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0310.GerarLoteDownloadNFeDestinadas(Lote, Empresa, FPathLoteDowNFeDes, FXML_LoteDowNFeDes, LaAviso1, LaAviso2);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_RET_DOW_NFE_DES_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  EdUF_Servico.Text := DmNFe_0000.QrOpcoesNFeUF_MDeNFe.Value; //'AN' = Ambiente Nacional
}
end;

procedure TFmNFeSteps_0310.PreparaConsultaCadastro(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaConsultaDeNFesDestinadas(Empresa, Lote:
Integer; ultNSU: Int64);
var
  LoteStr, ItemStr: String;
begin
  EdEmpresa.ValueVariant := Empresa;
  EdLote.ValueVariant := Lote;
  EdultNSU.ValueVariant := ultNSU;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  DmNFe_0000.ReopenOpcoesNFe(EdEmpresa.ValueVariant, True);
  EdUF_Servico.Text := DmNFe_0000.QrOpcoesNFeUF_MDeDes.Value; //'AN' = Ambiente Nacional
  //
  if CkSoLer.Checked then
  begin
    LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
    ItemStr := LoteStr + '_NSU_' + DmNFe_0000.FormataNSU_NFe(EdultNSU.ValueVariant);
    if AbreArquivoXML(ItemStr, NFE_EXT_RET_NFE_DES_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaConsultaDistribuicaoDFeInteresse(Empresa,
  Lote: Integer; ultNSU, NSU: Int64; FrmaCnslt: Integer);
var
  //LoteStr,
  ItemStr: String;
  MeuNSU: String;
begin
  EdEmpresa.ValueVariant := Empresa;
  EdLote.ValueVariant := Lote;
  EdultNSU.ValueVariant := ultNSU;
  EdNSU.ValueVariant := NSU;
  RGFrmaCnslt.ItemIndex := FrmaCnslt;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  DmNFe_0000.ReopenOpcoesNFe(EdEmpresa.ValueVariant, True);
  EdUF_Servico.Text := DmNFe_0000.QrOpcoesNFeUF_DistDFeInt.Value; //'AN' = Ambiente Nacional
  //
  if CkSoLer.Checked then
  begin
(*
    LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
    case RGFrmaCnslt.ItemIndex of
      1: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdultNSU.ValueVariant);
      2: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdultNSU.ValueVariant); // Zero ???
      3: MeuNSU := DmNFe_0000.FormataNSU_NFe(EdNSU.ValueVariant);
      else Geral.MB_Erro('Forma de consulta n�o definida!');
    end;
    ItemStr := LoteStr + '_NSU_' + MeuNSU;
*)
    ItemStr := MontaNomeArqNSU(Lote);
    if AbreArquivoXML(ItemStr, NFE_EXT_RET_DFE_DIS_INT_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
var
  LoteStr: String;
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdRecibo.ValueVariant    := Recibo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  if CkSoLer.Checked then
  begin
    LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_PRO_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmNFeSteps_0310.EdEmpresaChange(Sender: TObject);
begin
  {
  DmNFe_0000.ReopenEmpresa(Geral.IMV(EdEmpresa.Text));
  //
  CBUF.Text := DmNFe_0000.QrFilialUF_WebServ.Value;
  }
  DmNFe_0000.ReopenOpcoesNFe(EdEmpresa.ValueVariant, True);
  RGAmbiente.ItemIndex := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
end;

procedure TFmNFeSteps_0310.EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    //
    EdUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmNFeSteps_0310.EdultNSUKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Codigo: String;
  CodVal: Int64;
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
    Codigo := '0';
    if InputQuery('NSU - N�mero Sequencial �nico', 'Informe o NSU anterior:',
    Codigo) then
    begin
      CodVal := Geral.I64(Codigo);
      EdultNSU.ValueVariant := CodVal;
    end;
  end;
end;

procedure TFmNFeSteps_0310.EdVersaoAcaoChange(Sender: TObject);
begin
  //DefineVersaoAcao();
end;

procedure TFmNFeSteps_0310.ExecutaConsultaDistribuicaoDFeInteresse();
var
  Empresa, Lote: Integer;
  //LoteStr,
  ItemStr, ultNSU, NSU, CNPJ, CPF, Certificado, xUltNSU, xNSU: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  //
  if not DefineIntCNPJ(CNPJ) then Exit;
  if not Define_ultNSU(ultNSU) then Exit;
  if not Define_NSU(NSU) then Exit;
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  FTextoArq :='';
  Screen.Cursor := CrHourGlass;
  try
    Certificado := EdSerialNumber.Text;
    FultNSU     := Geral.I64(ultNSU);
    FNSU        := Geral.I64(NSU);
    //
    if NSU = '0' then
      NSU := '';
    //while FindCont = 1 do
    begin
      xUltNSU := Geral.FI64(FultNSU);
      xNSU := Geral.FI64(FNSU);
      FAvisoNSU := 'NSU ' + xUltNSU + '. ';
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Consultando');

(*
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        ItemStr := LoteStr + '_NSU_' + DmNFe_0000.FormataNSU_NFe(FultNSU);
*)
        ItemStr := MontaNomeArqNSU(Lote);

      FTextoArq := FmNFeGeraXML_0310.WS_NFeConsultaDistDFeInt(EdUF_Servico.Text,
        FCodigoUF_Txt, FAmbiente_Int, CNPJ, CPF, UltNSU, NSU,
         Certificado, LaAviso1, LaAviso2, RETxtEnvio, EdWebService, ItemStr);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, False, FAvisoNSU + 'O fisco acusou erros na resposta!');
        Geral.MB_Erro('Resposta recebida com Erros!');
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
(*
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        ItemStr := LoteStr + '_NSU_' + DmNFe_0000.FormataNSU_NFe(FultNSU);
*)
        ItemStr := MontaNomeArqNSU(Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_RET_DFE_DIS_INT_XML, ItemStr, FTextoArq, RETxtRetorno, False);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Gravando dados recebidos no banco de dados!');
        LerTextoConsultaDistribuicaoDFeInteresse();
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Pesquisa finalizada!');
        try
          if FFormChamou = 'FmNFeDistDFeInt_0100' then
            FmNFeDistDFeInt_0100.LocCod(Lote, Lote);
        except
          Geral.MB_Erro('N�o foi poss�vel localizar o lote de NF-e(s) n�mero ' +
          EdLote.Text + '!');
        end;
      end;
    end;
    //
    FmNFeDistDFeInt_0100.LocCod(Lote, Lote);

  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeSteps_0310.ExecutaConsultaLoteNFe();
var
  Recibo, LoteStr: String;
  Empresa, Lote: Integer;
begin
  Recibo := EdRecibo.Text;
  if (Recibo <> '')  then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
    if not DefineEmpresa(Empresa) then Exit;
    if not DefineLote(Lote) then Exit;
    DmNFe_0000.ReopenEmpresa(Empresa);
    FTextoArq :='';
    Screen.Cursor := CrHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando o servidor do fisco');
      //
      FTextoArq := FmNFeGeraXML_0310.WS_NFeRetRecepcao(EdUF_Servico.Text, FAmbiente_Int,
        FCodigoUF_Int, EdRecibo.Text, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2,False, 'O fisco acusou erros na resposta!');
        Geral.MB_Erro('Resposta recebida com Erros!');
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_PRO_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        LerTextoConsultaLoteNFe();
        try
          if FFormChamou = 'FmNFeLEnc_0310' then
            FmNFeLEnc_0310.LocCod(Lote, Lote);
          if FFormChamou = 'FmNFeLEnU_0310' then
            FmNFeLEnU_0310.ReabreNFeLEnc(Lote);
        except
          Geral.MB_Aviso('N�o foi poss�vel localizar o lote de NF-e(s) n�mero ' +
          EdLote.Text + '!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    Geral.MB_Aviso('Recibo n�o informado para consulta...');
end;

procedure TFmNFeSteps_0310.ExecutaConsultaNFDest();
var
  Empresa, Lote: Integer;
  LoteStr, ItemStr, indNFe, indEmi, ultNSU, CNPJ, Certificado, xNSU: String;
begin
{
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  //
  if not DefineDestCNPJ(CNPJ) then Exit;
  if not Define_indNFe(indNFe) then Exit;
  if not Define_indEmi(indEmi) then Exit;
  if not Define_ultNSU(ultNSU) then Exit;
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  FTextoArq :='';
  Screen.Cursor := CrHourGlass;
  try
    //
    Certificado := EdSerialNumber.Text;
    //
    FultNSU := Geral.I64(ultNSU);
    while FindCont = 1 do
    begin
      xNSU := Geral.FI64(FultNSU);
      FAvisoNSU := 'NSU ' + xNSU + '. ';
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Consultando');
      FTextoArq := FmNFeGeraXML_0310.WS_NFeConsultaNFDest(EdUF_Servico.Text, CBUF.Text,
        FVersaoAcao, FAmbiente_Int, CNPJ, indNFe, indEmi, xNSU,
         Certificado, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, False, FAvisoNSU + 'O fisco acusou erros na resposta!');
        Geral.MB_('Resposta recebida com Erros!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        ItemStr := LoteStr + '_NSU_' + DmNFe_0000.FormataNSU_NFe(FultNSU);
        DmNFe_0000.SalvaXML(NFE_EXT_RET_NFE_DES_XML, ItemStr, FTextoArq, RETxtRetorno, False);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Gravando dados recebidos no banco de dados!');
        LerTextoConsultaNFeDest(xNSU);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Pesquisa finalizada!');

        (*
        try
          if FFormChamou = 'FmNFeLEnc_0310' then
            FmNFeLEnc_0310.LocCod(Lote, Lote);
          if FFormChamou = 'FmNFeLEnU_0310' then
            FmNFeLEnU_0310.ReabreNFeLEnc(Lote);
        except
          Geral.MB_(
          'N�o foi poss�vel localizar o lote de NF-e(s) n�mero ' + EdLote.Text +
          '!', 'Aviso', MB_OK+MB_ICONWARNING);
        end;
        *)
      end;
    end;
    //
    FmNFeDesConC_0101.LocCod(Lote, Lote);

  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmNFeSteps_0310.ExecutaConsultaNFe();
var
  Empresa, IDCtrl: Integer;
  chNFe: String;
  Id, Dir, Aviso: String;
begin
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FTextoArq := FmNFeGeraXML_0310.WS_NFeConsultaNF(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, chNFe, LaAviso1, LaAviso2,
      RETxtEnvio, EdWebService);
    MostraTextoRetorno(FTextoArq);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Resposta recebida com Sucesso!');
    //
    DmNFe_0000.SalvaXML(NFE_EXT_SIT_XML, chNFe, FTextoArq, RETxtRetorno, False);
    //
    // 2012-03-29
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros!');
    end;
    if not FNaoExecutaLeitura then
    begin
    // fim 2012-03-29
      LerTextoConsultaNFe();
      //
      // 2010-10-18
      QrCabA.Close;
      QrCabA.Params[00].AsInteger := EdEmpresa.ValueVariant;
      QrCabA.Params[01].AsString  := chNFe;
      QrCabA.Open;
      IDCtrl := QrCabAIDCtrl.Value;
      if IDCtrl > 0 then
      begin
        // 2011-06-08
        Id    := QrCabAinfProt_ID.Value;
        if Trim(Id) = '' then
          Id := QrCabAinfProt_nProt.Value;
        // Fim2011-06-08

        Dir   := DModG.QrPrmsEmpNFeDirSit.Value;
        Aviso := '';
        DmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(chNFe, Id, IDCtrl, Dir, Aviso);
        if Aviso <> '' then Geral.MB_Aviso(
        'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
      end;
    end else
    begin
      // 2014-10-27
      //Geral.MB_Erro(FTextoArq);
      LerTextoConsultaNFe();
      // FIM 2014-10-27
      Close;
    end;
    // Fim 2010-10-18
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeSteps_0310.ExecutaDownloadNFDestinadas();
var
  Empresa, Lote: Integer;
  LoteStr, CNPJ, Certificado: String;
  //xNSU: String;
begin
(*&
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  //
  if not DefineDestCNPJ(CNPJ) then Exit;
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  FTextoArq :='';
  Screen.Cursor := CrHourGlass;
  try
    //
    Certificado := EdSerialNumber.Text;
    //
    //for I := 1 to MeChaves.Lines.Count do
    begin
      FTextoArq := FmNFeGeraXML_0310.WS_NFeDownloadNFeDestinadas(
        EdUF_Servico.Text, CBUF.Text,
        FVersaoAcao, FAmbiente_Int, CNPJ, MeChaves.Lines,
         Certificado, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, False, FAvisoNSU + 'O fisco acusou erros na resposta!');
        Geral.MB_('Resposta recebida com Erros!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_RET_DOW_NFE_DES_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Gravando dados recebidos no banco de dados!');
        LerTextoDownloadNFeDestinadas();
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Pesquisa finalizada!');
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
&*)
end;

procedure TFmNFeSteps_0310.ExecutaDownloadNFeConfirmadas();
var
  Empresa, Lote: Integer;
  LoteStr, CNPJ, Certificado: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando envio de solicita��o');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  //
  if not DefineDestCNPJ(CNPJ) then Exit;
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  FTextoArq :='';
  Screen.Cursor := CrHourGlass;
  try
    //
    Certificado := EdSerialNumber.Text;
    //
    FTextoArq := FmNFeGeraXML_0310.WS_NFeDownloadNFeConfirmadas(
      EdUF_Servico.Text, CBUF.Text, FAmbiente_Int, CNPJ, MeChaves.Lines,
       Certificado, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Resposta recebida com Sucesso!');
    MostraTextoRetorno(FTextoArq);
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, FAvisoNSU + 'O fisco acusou erros na resposta!');
      Geral.MB_Erro('Resposta recebida com Erros!');
    end else
    begin
      DmNFe_0000.ReopenEmpresa(Empresa);
      //
      LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
      DmNFe_0000.SalvaXML(NFE_EXT_RET_DOW_NFE_CNF_XML, LoteStr, FTextoArq, RETxtRetorno, False);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, FAvisoNSU + 'Gravando dados recebidos no banco de dados!');

      LerTextoDownloadNFeConfirmadas();
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Pesquisa finalizada!');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeSteps_0310.RGAcaoClick(Sender: TObject);
begin
  PnLote.Visible               := False;
  PnRecibo.Visible             := False;
  PnJustificativa.Visible      := False;
  PnInutiliza.Visible          := False;
  PnChaveNFe.Visible           := False;
  PnProtocolo.Visible          := False;
  //
  case RGAcao.ItemIndex of
    0: (*Nada*);
    1: 
    begin
      PnLote.Visible               := True;
    end;
    2:
    begin
      PnLote.Visible               := True;
      PnRecibo.Visible             := True;
    end;
    3:
    begin
      ReopenNFeJust(1);
      PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnChaveNFe.Visible           := True;
      PnProtocolo.Visible          := True;
      PnJustificativa.Visible      := True;
    end;
    4: (*Ainda n�o fiz*)
    begin
      ReopenNFeJust(2);
      PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnInutiliza.Visible          := True;
      PnJustificativa.Visible      := True;
    end;
    5:
    begin
      PnChaveNFe.Visible           := True;
    end;
    6:
    begin
      PnLote.Visible               := True;
    end;
    9:
    begin
      PnLote.Visible               := True;
    end;

  end;
  PnCancInutiliza.Visible := PnRecibo.Visible or PnChaveNFe.Visible or PnProtocolo.Visible;
end;

procedure TFmNFeSteps_0310.SpeedButton2Click(Sender: TObject);
var
  NFeJust: Integer;
begin
  VAR_CADASTRO := 0;
  NFeJust      := EdNFeJust.ValueVariant;
  //
  if DBCheck.CriaFm(TFmNFeJust, FmNFeJust, afmoNegarComAviso) then
  begin
    if NFeJust <> 0 then
      FmNFeJust.LocCod(NFeJust, NFeJust);
    FmNFeJust.ShowModal;
    FmNFeJust.Destroy;
  end;
  //
  if VAR_CADASTRO > 0 then
  begin
    //Para n�o perder a SQL pois varia no reopen de acordo com a aplica��o
    QrNFeJust.Close;
    QrNFeJust.Open;
    //
    EdNFeJust.ValueVariant := VAR_CADASTRO;
    CBNFeJust.KeyValue     := VAR_CADASTRO;
  end;
end;

function TFmNFeSteps_0310.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!');
end;

procedure TFmNFeSteps_0310.Timer1Timer(Sender: TObject);
begin
  FSegundos := FSegundos + 1;
  LaWait.Visible := True;
  MyObjects.Informa(LaWait, True, 'Aguarde o tempo m�nimo de resposta ' +
    FormatFloat('0', FSegundos) + ' de ' + FormatFloat('0', FSecWait));
  //
  if LaWait.Font.Color = clGreen then
    LaWait.Font.Color := clBlue
  else
    LaWait.Font.Color := clGreen;
  //
  if FSegundos = FSecWait then
    Close;
end;

procedure TFmNFeSteps_0310.RETxtEnvioChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmNFeSteps_0310.RETxtEnvioSelectionChange(Sender: TObject);
begin
  UpdateCursorPos(RETxtEnvio);
end;

procedure TFmNFeSteps_0310.RETxtRetornoChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmNFeSteps_0310.ReopenNFeJust(Aplicacao: Byte);
begin
  QrNFeJust.Close;
  QrNFeJust.Params[0].AsInteger := Aplicacao;
  QrNFeJust.Open;
  //
end;

procedure TFmNFeSteps_0310.UpdateCursorPos(Memo: TMemo);
var
  CharPos: TPoint;
begin
  CharPos.Y := SendMessage(Memo.Handle, EM_EXLINEFROMCHAR, 0, Memo.SelStart);
  CharPos.X := (Memo.SelStart - SendMessage(Memo.Handle, EM_LINEINDEX, CharPos.Y, 0));
  Inc(CharPos.Y);
  Inc(CharPos.X);
  //StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X]);
  dmkEdit1.ValueVariant := CharPos.Y;
  dmkEdit2.ValueVariant := CharPos.X;
  dmkEdit3.ValueVariant := Memo.SelStart;
end;

procedure TFmNFeSteps_0310.VerificaCertificadoDigital(Empresa: Integer);
begin
  DmNFe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmNFe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmNFe_0000.QrFilialUF_WebServ.Value;
  //2011-08-25
  //EdUF_Servico.Text        := DmNFe_0000.QrFilialUF_Servico.Value;
  case DmNFe_0000.QrFilialNFetpEmis.Value of
    3(*SCAN*): EdUF_Servico.Text := 'SCAN';
    else EdUF_Servico.Text := DmNFe_0000.QrFilialUF_Servico.Value;
  end;
  // Fim 2011-08-25
  EdSerialNumber.Text := DmNFe_0000.QrFilialNFeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmNFe_0000.QrFilialNFeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmNFe_0000.QrFilialNFeSerVal.Value < Int(Date) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expirou!'
  else
  if DmNFe_0000.QrFilialNFeSerVal.Value <= (Int(Date) + DmNFe_0000.QrFilialNFeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmNFe_0000.QrFilialNFeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

procedure TFmNFeSteps_0310.VerificaStatusServico();
begin
  Screen.Cursor := crHourGlass;
  try
    FTextoArq := FmNFeGeraXML_0310.WS_NFeStatusServico(EdUF_Servico.Text, FAmbiente_Int,
      FCodigoUF_Int, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
