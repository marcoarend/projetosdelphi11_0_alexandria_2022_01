unit NFeItsLA_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, ShellAPI, dmkCheckBox;

type
  TFmNFeItsLA_0000 = class(TForm)
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdnItem: TdmkEdit;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Edprod_xProd: TdmkEdit;
    Panel3: TPanel;
    Label17: TLabel;
    EdDetComb_cProdANP: TdmkEdit;
    LinkLabel1: TLinkLabel;
    Label1: TLabel;
    EdDetComb_CODIF: TdmkEdit;
    Label7: TLabel;
    EdDetComb_qTemp: TdmkEdit;
    Label8: TLabel;
    EdDetComb_UFCons: TdmkEdit;
    GroupBox3: TGroupBox;
    CkDetComb_CIDE: TdmkCheckBox;
    Panel5: TPanel;
    Label9: TLabel;
    EdDetComb_CIDE_qBCProd: TdmkEdit;
    Label11: TLabel;
    EdDetComb_CIDE_vAliqProd: TdmkEdit;
    Label12: TLabel;
    EdDetComb_CIDE_vCIDE: TdmkEdit;
    Label14: TLabel;
    EdDetComb_descANP: TdmkEdit;
    LinkLabel2: TLinkLabel;
    Panel6: TPanel;
    Label6: TLabel;
    EdDetComb_pGLP: TdmkEdit;
    Label15: TLabel;
    EdDetComb_pGNn: TdmkEdit;
    Label16: TLabel;
    EdDetComb_pGNi: TdmkEdit;
    Label18: TLabel;
    EdDetComb_vPart: TdmkEdit;
    GroupBox4: TGroupBox;
    CkDetComb_encerrante: TdmkCheckBox;
    Panel7: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    EdDetComb_encerrante_nBico: TdmkEdit;
    EdDetComb_encerrante_nBomba: TdmkEdit;
    EdDetComb_encerrante_nTanque: TdmkEdit;
    EdDetComb_encerrante_vEncIni: TdmkEdit;
    EdDetComb_encerrante_vEncFin: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure LinkLabel1LinkClick(Sender: TObject; const Link: string;
      LinkType: TSysLinkType);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmNFeItsLA_0000: TFmNFeItsLA_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeItsLA_0000.BtOKClick(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
  DetComb_descANP, DetComb_CODIF, DetComb_UFCons: String;
  DetComb_cProdANP, DetComb_CIDE, DetComb_encerrante, DetComb_encerrante_nBico,
  DetComb_encerrante_nBomba, DetComb_encerrante_nTanque: Integer;
  DetComb_pGLP, DetComb_pGNn, DetComb_pGNi, DetComb_vPart, DetComb_qTemp,
  DetComb_CIDE_qBCProd, DetComb_CIDE_vAliqProd, DetComb_CIDE_vCIDE,
  DetComb_encerrante_vEncheIni, DetComb_encerrante_vEncheFin: Double;
  SQLType: TSQLType;
begin
  SQLType                      := ImgTipo.SQLType;
  FatID                        := EdFatID.ValueVariant;
  FatNum                       := EdFatNum.ValueVariant;
  Empresa                      := EdEmpresa.ValueVariant;
  nItem                        := EdnItem.ValueVariant;

  DetComb_cProdANP             := EdDetComb_cProdANP.ValueVariant;
  DetComb_descANP              := EdDetComb_descANP.ValueVariant;
  DetComb_pGLP                 := EdDetComb_pGLP.ValueVariant;
  DetComb_pGNn                 := EdDetComb_pGNn.ValueVariant;
  DetComb_pGNi                 := EdDetComb_pGNi.ValueVariant;
  DetComb_vPart                := EdDetComb_vPart.ValueVariant;
  DetComb_CODIF                := EdDetComb_CODIF.ValueVariant;
  DetComb_qTemp                := EdDetComb_qTemp.ValueVariant;
  DetComb_UFCons               := EdDetComb_UFCons.ValueVariant;

  DetComb_CIDE                 := Geral.BoolToInt(CkDetComb_CIDE.Checked);

  DetComb_CIDE_qBCProd         := EdDetComb_CIDE_qBCProd.ValueVariant;
  DetComb_CIDE_vAliqProd       := EdDetComb_CIDE_vAliqProd.ValueVariant;
  DetComb_CIDE_vCIDE           := EdDetComb_CIDE_vCIDE.ValueVariant;

  DetComb_encerrante           := Geral.BoolToInt(CkDetComb_encerrante.Checked);

  DetComb_encerrante_nBico     := EdDetComb_encerrante_nBico.ValueVariant;
  DetComb_encerrante_nBomba    := EdDetComb_encerrante_nBomba.ValueVariant;
  DetComb_encerrante_nTanque   := EdDetComb_encerrante_nTanque.ValueVariant;
  DetComb_encerrante_vEncheIni := EdDetComb_encerrante_vEncIni.ValueVariant;
  DetComb_encerrante_vEncheFin := EdDetComb_encerrante_vEncFin.ValueVariant;

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeitsla', False, [
  'DetComb_cProdANP', 'DetComb_descANP', 'DetComb_pGLP',
  'DetComb_pGNn', 'DetComb_pGNi', 'DetComb_vPart',
  'DetComb_CODIF', 'DetComb_qTemp', 'DetComb_UFCons',
  'DetComb_CIDE', 'DetComb_CIDE_qBCProd', 'DetComb_CIDE_vAliqProd',
  'DetComb_CIDE_vCIDE', 'DetComb_encerrante', 'DetComb_encerrante_nBico',
  'DetComb_encerrante_nBomba', 'DetComb_encerrante_nTanque', 'DetComb_encerrante_vEncheIni',
  'DetComb_encerrante_vEncheFin'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  DetComb_cProdANP, DetComb_descANP, DetComb_pGLP,
  DetComb_pGNn, DetComb_pGNi, DetComb_vPart,
  DetComb_CODIF, DetComb_qTemp, DetComb_UFCons,
  DetComb_CIDE, DetComb_CIDE_qBCProd, DetComb_CIDE_vAliqProd,
  DetComb_CIDE_vCIDE, DetComb_encerrante, DetComb_encerrante_nBico,
  DetComb_encerrante_nBomba, DetComb_encerrante_nTanque, DetComb_encerrante_vEncheIni,
  DetComb_encerrante_vEncheFin], [
  FatID, FatNum, Empresa, nItem], True) then
  begin
    //
    Close;
  end;
end;

procedure TFmNFeItsLA_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsLA_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsLA_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeItsLA_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeItsLA_0000.LinkLabel1LinkClick(Sender: TObject;
  const Link: string; LinkType: TSysLinkType);
begin
  ShellExecute(0, nil, PChar(Link), nil, nil, 1);
end;

procedure TFmNFeItsLA_0000.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
end;

end.
