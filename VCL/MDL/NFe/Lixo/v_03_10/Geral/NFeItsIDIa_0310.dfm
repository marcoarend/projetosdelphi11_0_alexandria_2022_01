object FmNFeItsIDIa_0310: TFmNFeItsIDIa_0310
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-009 :: Adi'#231#227'o de DI'
  ClientHeight = 362
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 219
    Width = 547
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 444
    object CkContinuar: TCheckBox
      Left = 4
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 547
    Height = 61
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 0
    ExplicitLeft = 12
    ExplicitTop = 36
    object Label5: TLabel
      Left = 168
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Item:'
      FocusControl = DBEdnItem
    end
    object Label3: TLabel
      Left = 224
      Top = 16
      Width = 28
      Height = 13
      Caption = 'ID DI:'
    end
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdFatID
    end
    object Label10: TLabel
      Left = 40
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdFatNum
    end
    object Label13: TLabel
      Left = 120
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object DBEdnItem: TdmkDBEdit
      Left = 168
      Top = 32
      Width = 53
      Height = 21
      DataField = 'nItem'
      DataSource = FmNFeCabA_0000.DsNfeItsIDI
      TabOrder = 0
      UpdCampo = 'nItem'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdFatID: TdmkDBEdit
      Left = 8
      Top = 32
      Width = 28
      Height = 21
      DataField = 'FatID'
      DataSource = FmNFeCabA_0000.DsNfeItsIDI
      TabOrder = 1
      UpdCampo = 'FatID'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdFatNum: TdmkDBEdit
      Left = 40
      Top = 32
      Width = 76
      Height = 21
      DataField = 'FatNum'
      DataSource = FmNFeCabA_0000.DsNfeItsIDI
      TabOrder = 2
      UpdCampo = 'FatNum'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 120
      Top = 32
      Width = 45
      Height = 21
      DataField = 'Empresa'
      DataSource = FmNFeCabA_0000.DsNfeItsIDI
      TabOrder = 3
      UpdCampo = 'Empresa'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdControle: TdmkDBEdit
      Left = 224
      Top = 32
      Width = 64
      Height = 21
      DataField = 'Controle'
      DataSource = FmNFeCabA_0000.DsNfeItsIDI
      TabOrder = 4
      UpdCampo = 'Controle'
      UpdType = utYes
      Alignment = taLeftJustify
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 109
    Width = 547
    Height = 110
    Align = alClient
    Caption = ' Dados da declara'#231#227'o da importa'#231#227'o:'
    TabOrder = 1
    ExplicitTop = 48
    ExplicitHeight = 104
    object Label2: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label8: TLabel
      Left = 96
      Top = 20
      Width = 50
      Height = 13
      Caption = 'N'#186' adi'#231#227'o:'
    end
    object Label7: TLabel
      Left = 224
      Top = 20
      Width = 94
      Height = 13
      Caption = '$ Desconto item DI:'
    end
    object Label12: TLabel
      Left = 12
      Top = 60
      Width = 418
      Height = 13
      Caption = 
        'C'#243'digo do fabricante estrangeiro (usar o c'#243'digo do cadastro de e' +
        'ntidade de prefer'#234'ncia):'
    end
    object Label4: TLabel
      Left = 160
      Top = 20
      Width = 60
      Height = 13
      Caption = 'Seq. adi'#231#227'o:'
    end
    object Label6: TLabel
      Left = 328
      Top = 20
      Width = 175
      Height = 13
      Caption = 'N'#176' do ato concess'#243'rio de Drawback:'
    end
    object EdConta: TdmkEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdAdi_nAdicao: TdmkEdit
      Left = 96
      Top = 36
      Width = 60
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '1'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      QryCampo = 'Adi_nAdicao'
      UpdCampo = 'Adi_nAdicao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdAdi_vDescDI: TdmkEdit
      Left = 224
      Top = 36
      Width = 100
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Adi_vDescDI'
      UpdCampo = 'Adi_vDescDI'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdAdi_cFabricante: TdmkEdit
      Left = 12
      Top = 76
      Width = 488
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Adi_cFabricante'
      UpdCampo = 'Adi_cFabricante'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdAdi_nSeqAdic: TdmkEdit
      Left = 160
      Top = 36
      Width = 60
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '1'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      QryCampo = 'Adi_nSeqAdic'
      UpdCampo = 'Adi_nSeqAdic'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdAdi_nDraw: TdmkEdit
      Left = 328
      Top = 36
      Width = 173
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 11
      TabOrder = 4
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMax = '99999999999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Adi_nDraw'
      UpdCampo = 'Adi_nDraw'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 547
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 499
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 451
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 164
      ExplicitTop = 24
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 156
        Height = 32
        Caption = 'Adi'#231#227'o de DI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 156
        Height = 32
        Caption = 'Adi'#231#227'o de DI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 156
        Height = 32
        Caption = 'Adi'#231#227'o de DI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 254
    Width = 547
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 245
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 543
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 298
    Width = 547
    Height = 64
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 289
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 543
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 399
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
