unit NFaEdit_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnDmkProcFunc, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit,
  dmkGeral, Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkMemo,
  dmkRadioGroup, Grids, DBGrids, dmkImage, UnDmkEnums;

type
  TFormChamou = (fcFmFatPedCab, fcFmFatPedNFs);
  TFmNFaEdit_0310 = class(TForm)
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    DsCFOP: TDataSource;
    QrCFOP: TmySQLQuery;
    QrCFOPItens: TLargeintField;
    QrCFOPCFOP: TWideStringField;
    QrCFOPOrdCFOPGer: TIntegerField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    QrFatPedNFs: TmySQLQuery;
    QrImprime: TmySQLQuery;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    DsImprime: TDataSource;
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrParamsEmpAssociada: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DsStqMovValX: TDataSource;
    GradeItens: TDBGrid;
    TbStqMovValX_: TmySQLTable;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmppCredSN_Cfg: TIntegerField;
    Panel4: TPanel;
    BtTodos: TBitBtn;
    QrEnti: TmySQLQuery;
    QrEntiCRT: TSmallintField;
    QrStqMovValX: TmySQLQuery;
    QrStqMovValXID: TIntegerField;
    QrStqMovValXGraGruX: TIntegerField;
    QrStqMovValXCFOP: TWideStringField;
    QrStqMovValXQtde: TFloatField;
    QrStqMovValXPreco: TFloatField;
    QrStqMovValXTotal: TFloatField;
    QrStqMovValXCSOSN: TIntegerField;
    QrStqMovValXpCredSN: TFloatField;
    QrStqMovValXNO_PRD: TWideStringField;
    QrStqMovValXNO_TAM: TWideStringField;
    QrStqMovValXNO_COR: TWideStringField;
    QrCSOSN: TmySQLQuery;
    QrCSOSNItens: TLargeintField;
    BtSelecionados: TBitBtn;
    QrImprimeSerieNF_Normal: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrStqMovValXiTotTrib: TSmallintField;
    QrStqMovValXvTotTrib: TFloatField;
    QrStqMovValXTXT_iTotTrib: TWideStringField;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrParamsEmpNFeInfCpl: TIntegerField;
    QrParamsEmpNFeInfCpl_TXT: TWideMemoField;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    RNTC: TLabel;
    EdPlacaNr: TdmkEdit;
    EdPlacaUF: TdmkEdit;
    EdObservacao: TdmkEdit;
    EdRNTC: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    LaSerieNF: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    LaNumeroNF: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    SpeedButton21: TSpeedButton;
    Label10: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    EdNumeroNF: TdmkEdit;
    EdQuantidade: TdmkEdit;
    EdEspecie: TdmkEdit;
    EdMarca: TdmkEdit;
    EdNumero: TdmkEdit;
    EdkgBruto: TdmkEdit;
    EdkgLiqui: TdmkEdit;
    TPDtEmissNF: TdmkEditDateTimePicker;
    TPDtEntraSai: TdmkEditDateTimePicker;
    EdSerieNF: TdmkEdit;
    EddhEmiTZD: TdmkEdit;
    EddhSaiEntTZD: TdmkEdit;
    EddhEmiVerao: TdmkEdit;
    EddhSaiEntVerao: TdmkEdit;
    MeinfAdic_infCpl: TdmkMemo;
    StaticText1: TStaticText;
    Panel5: TPanel;
    GroupBox10: TGroupBox;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    EdCompra_XNEmp: TdmkEdit;
    EdCompra_XPed: TdmkEdit;
    EdCompra_XCont: TdmkEdit;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    EdUFEmbarq: TdmkEdit;
    EdxLocEmbarq: TdmkEdit;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    LaAviso1a: TLabel;
    LaAviso2a: TLabel;
    LaAviso1b: TLabel;
    LaAviso2b: TLabel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOk: TBitBtn;
    EdHrEmi: TdmkEdit;
    EdHrEntraSai: TdmkEdit;
    Label22: TLabel;
    EdVagao: TdmkEdit;
    Label23: TLabel;
    EdBalsa: TdmkEdit;
    Label24: TLabel;
    Eddest_email: TdmkEdit;
    RGCRT: TdmkRadioGroup;
    QrStqMovValXpTotTrib: TFloatField;
    QrSumXprod_vFrete: TFloatField;
    QrSumXprod_vSeg: TFloatField;
    QrSumXprod_vDesc: TFloatField;
    QrSumXprod_vOutro: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrImprimeAfterOpen(DataSet: TDataSet);
    procedure QrImprimeBeforeClose(DataSet: TDataSet);
    procedure MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbStqMovValX_AfterInsert(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtSelecionadosClick(Sender: TObject);
    procedure GradeItensDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TPDtEmissNFChange(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    //FStqMovValX: String;
    //
    FConfirmou: Boolean;
    procedure ReopenQrImprime();
    function  IncluiNFs(): Boolean;
    function  AlteraNFAtual(): Boolean;
    procedure ReopenParamsEmp(Empresa: Integer);
    //function ImpedePeloCRT(): Boolean;
    procedure ConfereCSOSN(Quais: TSelType);
  public
    { Public declarations }
    FFormChamou: TFormChamou;
    FThisFatID: Integer;
    FIDCtrl: Integer;
    // Configura��es cfe chamada:
    // F_Tipo=1 >> Faturamento de pedido
    // F_Tipo=102 >> CMPTOut(Blue Derm)
    F_Tipo, F_OriCodi, F_Empresa, F_ModeloNF, F_Cliente, F_EMP_FILIAL, F_AFP_Sit,
    F_Associada, F_ASS_FILIAL, F_EMP_CtaFaturas, F_ASS_CtaFaturas, F_CartEmis,
    F_CondicaoPG, F_EMP_FaturaDta, F_EMP_IDDuplicata, F_EMP_FaturaSeq,
    F_EMP_FaturaNum, F_TIPOCART, F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq,
    F_Financeiro, F_ASS_FaturaNum: Integer;
    F_EMP_TpDuplicata, F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
    F_ASS_TpDuplicata, F_ASS_TxtFaturas, FTabela: String;
    F_AFP_Per: Double;
    F_Abertura: TDateTime;
    ///
    procedure ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
    procedure ReopenStqMovValX(ID: Integer);
    //procedure ReopenFatPedCab(FatPedCab: Integer);
    procedure InsereTextoObserv(Texto: String);
    //function Encerra(): Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
    function Encerra2(): Boolean;
    procedure DefineTZD_UTC();
  end;

  var
  FmNFaEdit_0310: TFmNFaEdit_0310;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, (*NF1b,*) MyDBCheck,
  UnFinanceiro, GetData, UnInternalConsts, UCreate, NFaEditCSOSN,
  UnGrade_Tabs, ModProd, ModuleNFe_0000, ModuleFatura, DmkDAC_PF, GetValor,
  NFe_PF;

{$R *.DFM}

function TFmNFaEdit_0310.AlteraNFAtual(): Boolean;
var
  Empresa, SerieNFCod, NumeroNF, IncSeqAuto,
  UF: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEmi, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
begin
  Result := False;
  //usar QrFatPedNFs deste form!!!
  ReopenFatPedNFs(1,0);
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
{
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
}
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Edvagao.Text;
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  HrEmi        := EdHrEmi.Text;
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  //
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //
  //ReopenQrImprime;
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
    'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
    'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
    'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
    'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
    'RNTC', 'UFEmbarq', 'xLocEmbarq',
    'HrEntraSai', 'vagao', 'balsa', 'dest_email',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
    'dhEmiTZD', 'dhSaiEntTZD',
    'HrEmi'
  ], ['IDCtrl'], [
    F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
    SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
    Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
    Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
    EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
    HrEntraSai, vagao, balsa, dest_email,
    Compra_XNEmp, Compra_XPed, Compra_XCont,
    dhEmiTZD, dhSaiEntTZD,
    HrEmi
  ], [FIDCtrl], True) then
  begin
    Result := True;
  end;
end;

procedure TFmNFaEdit_0310.BtOkClick(Sender: TObject);
var
  Continua: Boolean;
begin
  //ver horario de verao!
  FConfirmou := True;
  //
  if F_Tipo = VAR_FATID_0001 then
  begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpCRT.Value = 1 then
    begin
      QrCSOSN.Close;
      QrCSOSN.Params[00].AsInteger := F_Tipo;
      QrCSOSN.Params[01].AsInteger := F_OriCodi;
      QrCSOSN.Params[02].AsInteger := F_EMpresa;
      UnDmkDAC_PF.AbreQuery(QrCSOSN, Dmod.MyDB);
      if QrCSOSNItens.Value > 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Esta empresa � obridada pelo seu CST = 1 a informar o CSOSN ' +
        sLineBreak + 'para cada produto da NF. Existem ' + FormatFloat('0',
        QrCSOSNItens.Value) + ' item(ns) sem esta informa��o!');
        //
        Exit;
      end;
    end;
  end;
  if ImgTipo.SQLType = stIns then
    Continua := IncluiNFs()
  else
    Continua := AlteraNFAtual();
  //
  if Continua = False then
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant)
  else
    Close;
end;

{
function TFmNFaEdit.ImpedePeloCRT(): Boolean;
begin
  QrEnti.Close;
  QrEnti.Params[0].AsInteger := F_Cliente;
    UnDmkDAC_PF.AbreQuery(QrEnti. O p e n ;
  //
  Result := QrEntiCRT.Value <> 1;
  if Result then
  begin
    Geral.MB_(
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!',
    'Aviso', MB_OK+MB_ICONWARNING)
  end;
end;
}

function TFmNFaEdit_0310.IncluiNFs(): Boolean;
  procedure ExcluiNF();
  begin
    // excluir nfe stqmovnfsa para n�o gerar erro quando tentar de novo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
    Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MB_Erro('Erro no encerramento!' + sLineBreak +
    'Feche a janela e tente encerrar novamente!' + sLineBreak +
    'Caso n�o consiga, AVISE A DERMATEK.');
    //
    Close;
  end;
var
  IDCtrl, Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF, emit_CRT: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont, HrEmi: String;
  Incluir, Encerrou: Boolean;
begin
  Result := False;
  //usar QrFatPedNFs deste form!!!
  Encerrou    := False;
  Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
  {
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
  }
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);

  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Eddest_email.Text;
  emit_CRT    := RGCRT.ItemIndex;
  if MyObjects.FIC(emit_CRT < 1, RGCRT, 'Informe o CRT!') then Exit;
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
  // NFe 3.10
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
  HrEmi        := EdHrEmi.Text;
  //
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
        'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
        'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
  end;
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
      'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
      'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
      'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
      'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
      'RNTC', 'UFEmbarq', 'xLocEmbarq',
      'HrEntraSai', 'vagao', 'balsa', 'dest_email', 'emit_CRT',
      'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
      'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi'
    ], ['IDCtrl'], [
      F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
      SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
      Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
      Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
      EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text,
      HrEntraSai, vagao, balsa, dest_email, emit_CRT,
      Compra_XNEmp, Compra_XPed, Compra_XCont,
      dhEmiTZD, dhSaiEntTZD, HrEmi
    ], [IDCtrl], True) then
    begin
      ReopenFatPedNFs(2,0);
      if QrFatPedNFs.RecordCount > 0 then
      begin
        while not QrFatPedNFs.Eof do
        begin
          Incluir := True;
          //
          if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
          (*QrFatPedCabSerieDesfe.Value*)-1,
          (*QrFatPedCabNFDesfeita.Value*)0,
          QrImprimeCO_SerieNF.Value,
          QrImprimeCtrl_nfs.Value,
          QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger,
          QrFatPedNFs.FieldByName('Filial').AsInteger,
          QrImprimeMaxSeqLib.Value,
          EdSerieNF, EdNumeroNF(*),
          SerieNFTxt, NumeroNF*)) then
          begin
            // ????
          end;
(*
          SerieNFTxt := IntToStr(EdSerieNF.ValueVariant);
          NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfs', 'Sequencial',
          'WHERE Controle=' + dmkPF.FFP(QrImprimeCtrl_nfs.Value, 0), 0,
          QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + sLineBreak +
          'Filial: ' + dmkPF.FFP(QrFatPedNFsFilial.Value, 0));
*)
          //
          Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
          SerieNFCod  := QrImprimeCO_SerieNF.Value;
          IncSeqAuto  := QrImprimeIncSeqAuto.Value;
          //NumeroNF    := EdNumeroNF.ValueVariant;
          {
          FreteVal    := 0;
          Seguro      := 0;
          Outros      := 0;
          }
          kgBruto     := 0;
          kgLiqui     := 0;
          PlacaUF     := '';
          PlacaNr     := '';
          Especie     := '';
          Marca       := '';
          Numero      := '';
          Quantidade  := '';
          Observacao  := '';
          CFOP1       := '';
          //
          if IncSeqAuto = 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
            Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
            Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
            Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
            Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
            Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
            Dmod.QrUpd.ExecSQL;
          end;
          if NumeroNF < 1 then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir and (NumeroNF > QrImprimeMaxSeqLib.Value) then
          begin
            Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
              FormatFloat('000000', NumeroNF));
            Incluir := False;
          end;
          if Incluir then
          begin
            try
              IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
              'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
              'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
              'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
              'Quantidade', 'CFOP1', 'infAdic_infCpl', 'RNTC',
              'UFEmbarq', 'xLocEmbarq',
              'HrEntraSai', 'vagao', 'balsa', 'dest_email',
              'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
              'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi'
              ], ['IDCtrl'], [
              F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
              SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
              Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
              Quantidade, CFOP1, MeinfAdic_infCpl.Text, EdRNTC.Text,
              EdUFEmbarq.Text, EdxLocEmbarq.Text,
              HrEntraSai, vagao, balsa, dest_email,
              Compra_XNEmp, Compra_XPed, Compra_XCont,
              dhEmiTZD, dhSaiEntTZD, HrEmi
              ], [IDCtrl], True) then
              begin
              // 
              end;
            except
              ExcluiNF();
              raise;
            end;
            //
          end else
            ExcluiNF();
          QrFatPedNFs.Next;
        end;
      end;
      //
      if FFormChamou = fcFmFatPedCab then
      begin
        Encerrou := Encerra2();
      end;
      if Encerrou then
      begin
        ReopenFatPedNFs(1,0);
        Result := True;
      end;
    end;
  except
    ExcluiNF();
    raise;
  end;
end;

procedure TFmNFaEdit_0310.InsereTextoObserv(Texto: String);
var
  TextoA, TextoD: String;
  Pos: Integer;
begin
  Pos := MeinfAdic_infCpl.SelStart;
  //
  TextoA := Copy(MeinfAdic_infCpl.Text, 1, Pos);
  TextoD := Copy(MeinfAdic_infCpl.Text, Pos + 1, Length(MeinfAdic_infCpl.Text));
  //
  MeinfAdic_infCpl.Text := TextoA + ' ' + Texto + ' ' + TextoD;
end;

procedure TFmNFaEdit_0310.MeinfAdic_infCplKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    UnNFe_PF.MostraFormNFaInfCpl(MeinfAdic_infCpl);
end;

procedure TFmNFaEdit_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaEdit_0310.BtSelecionadosClick(Sender: TObject);
begin
  ConfereCSOSN(istSelecionados);
end;

procedure TFmNFaEdit_0310.BtTodosClick(Sender: TObject);
begin
  ConfereCSOSN(istTodos);
end;

procedure TFmNFaEdit_0310.ConfereCSOSN(Quais: TSelType);
  procedure AtualizaItemAtual(CSOSN, FinaliCli: Integer; pCredSN: Double);
  var
    ID: Integer;
  begin
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'CSOSN', 'pCredSN', 'FinaliCli'], ['ID'
    ], [
    CSOSN, pCredSN, FinaliCli], [ID
    ], False) then ;
  end;
var
  I, CSOSN, FinaliCli: Integer;
  pCredSN: Double;
begin
  ReopenParamsEmp(F_Empresa);
  //
  if QrParamsEmpCRT.Value <> 1 then
  begin
    Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
    'O CRT da empresa n�o permite informa��es de cr�dito de ICMS pelo Simples Nacional!'
    + sLineBreak + sLineBreak +
    'Verifique com seu contador como proceder em caso de devolu��o de ' + sLineBreak +
    'mercadoria de fornecedor que emitiu NF-e pelo Simples Nacional!');
    // Deixar editar porque pode ser devolu��o de NFe!
    //Exit;
  end;
  if DBCheck.CriaFm(TFmNFaEditCSOSN, FmNFaEditCSOSN, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSN.FDtEmissNF := TPDtEmissNF.Date;
    FmNFaEditCSOSN.FEmpresa   := F_Empresa;
    //
    FmNFaEditCSOSN.ShowModal;
    Screen.Cursor := crHourGlass;
    try
      if FmNFaEditCSOSN.FConfirmou then
      begin
        CSOSN     := FmNFaEditCSOSN.EdCSOSN.ValueVariant;
        pCredSN   := FmNFaEditCSOSN.EdpCredSN.ValueVariant;
        FinaliCli := FmNFaEditCSOSN.RGFinaliCli.ItemIndex;
        case Quais of
          istTodos:
          begin
            QrStqMovValX.First;
            while not QrStqMovValX.Eof do
            begin
              AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              QrStqMovValX.Next;
            end;
          end;
          istSelecionados:
          begin
            if GradeItens.SelectedRows.Count > 0 then
            begin
              with GradeItens.DataSource.DataSet do
              for I:= 0 to GradeItens.SelectedRows.Count-1 do
              begin
                //GotoBookmark(pointer(GradeItens.SelectedRows.Items[I]));
                GotoBookmark(GradeItens.SelectedRows.Items[I]);
                AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
              end;
            end else AtualizaItemAtual(CSOSN, FinaliCli, pCredSN);
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      FmNFaEditCSOSN.Destroy;
    end;
  end;
  ReopenStqMovValX(QrStqMovValXID.Value);
end;

procedure TFmNFaEdit_0310.DefineTZD_UTC();
var
  Data: TDateTime;
  TZD_UTC: Double;
  hVerao: Boolean;
  SimNao: String;
  Cor: Integer;
begin
  // NFE 3.10
  Data := TPDtEmissNF.Date;
  hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
  SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
  Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  EddhEmiVerao.Text := SimNao;
  EddhEmiVerao.Font.Color := Cor;
  EddhEmiTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  if TPDtEntraSai.Date <> TPDtEmissNF.Date then
  begin
    Data   := TPDtEntraSai.Date;
    hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
    SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
    Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  end;
  EddhSaiEntTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  EddhSaiEntVerao.Text := SimNao;
  EddhSaiENtVerao.Font.Color := Cor;
end;

procedure TFmNFaEdit_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFaEdit_0310.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not FConfirmou then
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant)
end;

procedure TFmNFaEdit_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FConfirmou := False;
  FThisFatID := 1;
  FTabela    := '';
  // N�o pode ser aqui precisa criar primeiro!!!
  // Chamar na cria��o antes do ShowModal
  //ReopenFatPedNFs(1,0);
  TPDtEmissNF.Date          := DmodG.ObtemAgora;
  TPDtEntraSai.Date         := DmodG.ObtemAgora;
  EdHrEmi.ValueVariant      := DmodG.ObtemAgora;
  EdHrEntraSai.ValueVariant := DmodG.ObtemAgora;
  //
  TbStqMovValX_.Database := DModG.MyPID_DB;
  //
  //DefineTZD_UTC();
end;

procedure TFmNFaEdit_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaEdit_0310.FormShow(Sender: TObject);
var
  NFeInfCpl_TXT: String;
begin
  ReopenParamsEmp(F_Empresa);
  //
  NFeInfCpl_TXT := QrParamsEmpNFeInfCpl_TXT.Value;
  //
  if QrParamsEmpCRT.Value in [1,2] then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
  //
  PageControl2.ActivePageIndex := PageControl2.PageCount - 1;
  //
  if NFeInfCpl_TXT <> '' then
    InsereTextoObserv(NFeInfCpl_TXT);
end;

procedure TFmNFaEdit_0310.GradeItensDblClick(Sender: TObject);
const
  FormCaption  = 'XXX-XXXXX-001 :: Valor Aproximado dos Tributos';
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
  iTotTrib     = 1; // Informou manualmente!
var
  vTotTrib, pTotTrib: Double;
  ValVar: Variant;
  ID: Integer;
begin
{
GetValorDmk(ComponentClass: TComponentClass; Reference:
TComponent; FormatType: TAllFormat; Default: Variant;
Casas, LeftZeros: Integer; ValMin, ValMax: String;
Obrigatorio: Boolean; FormCaption, ValCaption: String;
WidthVal: Integer; var Resultado: Variant): Boolean;
}
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    QrStqMovValXvTotTrib.Value, 4, 0, '0,00', FormatFloat('0.00',
    QrStqMovValXTotal.Value), True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    vTotTrib := Geral.DMV(ValVar);
    if QrStqMovValXTotal.Value <> 0 then
      pTotTrib := vTotTrib / QrStqMovValXTotal.Value * 100
    else
      pTotTrib := 0;
    //
    ID := QrStqMovValXID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
    'iTotTrib', 'vTotTrib', 'pTotTrib'], [
    'ID'], [
    iTotTrib, vTotTrib, pTotTrib], [
    ID], False) then
    begin
      ReopenStqMovValX(ID);
    end;
  end;
end;

procedure TFmNFaEdit_0310.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
{
  QrVolumes.Close;
  QrVolumes.Params[00].AsInteger := F_OriCodi;
    UnDmkDAC_PF.AbreQuery(QrVolumes. O p e n ;
}
  ReopenQrImprime();
end;

procedure TFmNFaEdit_0310.QrImprimeAfterOpen(DataSet: TDataSet);
begin
  if QrImprime.RecordCount = 0 then
    Geral.MB_Aviso('Esta filial n�o est� ativa no Modelo de NF selecionado '+
    'na Regra Fiscal deste faturamento!');
  BtOK.Enabled := QrImprime.RecordCount > 0 ;
end;

procedure TFmNFaEdit_0310.QrImprimeBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

{
procedure TFmNFaEdit.ReopenFatPedCab(FatPedCab: Integer);
begin
  F_.Close;
  F_.Params[0].AsInteger := FatPedCab;
    UnDmkDAC_PF.AbreQuery(F_.O p e n ;
end;
}

procedure TFmNFaEdit_0310.ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
var
  Txt: String;
begin
  if QuaisFiliais = 1 then
    Txt := '='
  else
    Txt := '<>';
  //
  QrFatPedNFs.Close;
  QrFatPedNFs.SQL.Clear;
  QrFatPedNFs.SQL.Add('SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP,');
  QrFatPedNFs.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*');
  QrFatPedNFs.SQL.Add('FROM stqmovvala smva');
  QrFatPedNFs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa');
  QrFatPedNFs.SQL.Add('LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa');
  QrFatPedNFs.SQL.Add('          AND smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('          AND smna.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrFatPedNFs.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs.SQL.Add('AND ent.Codigo' + Txt +  FormatFloat('0', F_Empresa));
  QrFatPedNFs.SQL.Add('ORDER BY ent.Filial');
  QrFatPedNFs.SQL.Add('');
  //
  {
  QrFatPedNFs.Params[00].AsInteger := F_Tipo;
  QrFatPedNFs.Params[01].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[02].AsInteger := F_Tipo;
  QrFatPedNFs.Params[03].AsInteger := F_OriCodi;
  QrFatPedNFs.Params[04].AsInteger := F_Empresa;
  }
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs, Dmod.MyDB);
  //
  QrFatPedNFs.Locate('Filial', FilialLoc, []);
end;

procedure TFmNFaEdit_0310.ReopenParamsEmp(Empresa: Integer);
begin
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
end;

procedure TFmNFaEdit_0310.ReopenQrImprime;
begin
  QrImprime.Close;
  if QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger = F_Empresa then
  begin
    QrImprime.Params[00].AsInteger := F_Empresa;
    QrImprime.Params[01].AsInteger := F_ModeloNF;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
  end else begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    end;
  end;
end;

procedure TFmNFaEdit_0310.ReopenStqMovValX(ID: Integer);
begin
  QrStqMovValX.Close;
  QrStqMovValX.SQL.Clear;
  QrStqMovValX.SQL.Add('SELECT smva.ID, smva.GraGruX, smva.CFOP, smva.Qtde,');
  QrStqMovValX.SQL.Add('smva.Preco, smva.Total, smva.CSOSN, smva.pCredSN,');
  QrStqMovValX.SQL.Add('gg1.Nome NO_PRD, gti.Nome NO_TAM, gcc.Nome NO_COR,');
  QrStqMovValX.SQL.Add('smva.iTotTrib, smva.vTotTrib, smva.pTotTrib, ');
  QrStqMovValX.SQL.Add('ELT(smva.iTotTrib + 1, "N", "S") TXT_iTotTrib ');
  QrStqMovValX.SQL.Add('FROM stqmovvala smva');
  QrStqMovValX.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smva.GraGruX');
  QrStqMovValX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrStqMovValX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrStqMovValX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrStqMovValX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrStqMovValX.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo));
  QrStqMovValX.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrStqMovValX.SQL.Add('AND smva.Empresa=' + FormatFloat('0', F_Empresa));
  QrStqMovValX.SQL.Add('ORDER BY smva.IDCtrl');
  UnDmkDAC_PF.AbreQuery(QrStqMovValX, Dmod.MyDB);
  QrStqMovValX.Locate('ID', ID, []);
end;

procedure TFmNFaEdit_0310.SpeedButton21Click(Sender: TObject);
begin
  dmkPF.TZD_UTC_InfoHelp();
end;

procedure TFmNFaEdit_0310.TbStqMovValX_AfterInsert(DataSet: TDataSet);
begin
  TbStqMovValX_.Cancel;
end;

procedure TFmNFaEdit_0310.TPDtEmissNFChange(Sender: TObject);
begin
  DefineTZD_UTC;
end;

(*
Boolean; Foi migrado a gera��o do financeiro para o NFe_PF e foi criado o Encerra2 mantido apenas como backup
function TFmNFaEdit_0310.Encerra(): Boolean;
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
  Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela,
  NotaFiscal, Account: Integer; SerieNF: String; VerificaCliInt: Boolean);
  var
    TabLctA: String;
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    if F_Financeiro = 1 then
      FLAN_Credito    := Valor
    else
    if F_Financeiro = 2 then
      FLAN_Debito    := Valor;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_MoraDia    := QrSumTJurosMes.Value;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    FLAN_Genero     := Genero;
    FLAN_Cliente    := F_Cliente;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
    FLAN_FatID      := FThisFatID;
    FLAN_FatNum     := F_OriCodi;
    FLAN_FatParcela := Parcela;
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := SerieNF;
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
{$IfDef DEFINE_VARLCT}
    TabLctA         := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, F_EMP_Filial);
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
    //
    if UFinanceiro.InsereLancamento(TabLctA) then
    begin
      // nada
    end;
{$Else}
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
      'Controle', VAR_LCT, VAR_LCT, 'Controle');
    //
    if UFinanceiro.InsereLancamento() then
    begin
      // nada
    end;
{$ENdIf}
  end;
var
  DataFat, DataEnc: TDateTime;
  FaturaNum, Agora, Duplicata, NovaDup, NF_Emp_Serie, NF_Ass_Serie: String;
  Parcela, NF_Emp_Numer, NF_Ass_Numer, Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  TemAssociada: Boolean;
  FreteVal, Seguro, Outros: Double;
  TabLctA: String;
begin
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      // Exclui lan�amentos para evitar duplica��o
      if (FThisFatID <> 0) and (F_OriCodi <> 0) then
      begin
{$IfDef DEFINE_VARLCT}
      Filial  := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      //
      UFinanceiro.ExcluiLct_FatNum(nil, FThisFatID, F_OriCodi,
        F_Empresa, 0, dmkPF.MotivDel_ValidaCodigo(311), True, TabLctA);
{$Else}
      UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB, FThisFatID, F_OriCodi,
        F_Empresa, 0, True);
{$EndIf}
      end;
      //
      QrNF_X.Close;
      QrNF_X.Params[00].AsInteger := F_Tipo;
      QrNF_X.Params[01].AsInteger := F_OriCodi;
      QrNF_X.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
      //
      NF_Emp_Serie := QrNF_XSerieNFTxt.Value;
      NF_Emp_Numer := QrNF_XNumeroNF.Value;
      //
      if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', F_EMP_FILIAL) + ' !' + sLineBreak +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"');
        Exit;
      end;
      //
      TemAssociada := (F_AFP_Sit = 1) and (F_AFP_Per > 0);
      //
      if TemAssociada then
      begin
        QrNF_X.Close;
        QrNF_X.Params[00].AsInteger := F_Tipo;
        QrNF_X.Params[01].AsInteger := F_OriCodi;
        QrNF_X.Params[02].AsInteger := F_Associada;
        UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
        NF_Ass_Serie := QrNF_XSerieNFTxt.Value;
        NF_Ass_Numer := QrNF_XNumeroNF.Value;
        if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'O n�mero de nota fiscal n�o foi definida para a empresa ' +
            FormatFloat('000', F_ASS_FILIAL) + ' !' + sLineBreak +
            'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
            'Nota Fiscal"');
          Exit;
        end;
        //
        P2 := F_AFP_Per;
        P1 := 100 - P2;
      end else
      begin
        P1 := 100;
        P2 := 0;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
      end;
      //
      if F_EMP_CtaFaturas = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_EMP_FILIAL) + '!');
        Exit;
      end;
      if (F_Associada <> 0) and
      (F_ASS_CtaFaturas = 0) and TemAssociada then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(F_ASS_FILIAL) + '!');
        Exit;
      end;
      if F_Financeiro > 0 then
      begin
        if F_CartEmis = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'A carteira n�o foi definida no pedido selecionado!');
          Exit;
        end;
        QrPrzT.Close;
        QrPrzT.Params[0].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzT, Dmod.MyDB);
        if QrPrzT.RecordCount = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'N�o h� parcela(s) definida(s) para a empresa ' +
            IntToStr(F_EMP_FILIAL) + ' na condi��o de pagamento ' +
            'cadastrada no pedido selecionado!');
          Exit;
        end;
        if (F_AFP_Sit = 1) and
        (F_AFP_Per > 0) then
        begin
          QrSumT.Close;
          QrSumT.Params[0].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
          if (QrSumTPercent2.Value <> F_AFP_Per) then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
          if QrSumT.RecordCount = 0 then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
        end;
      end else
        Geral.MB_Aviso('N�o ser� gerado lan�amentos financeiros.' + sLineBreak +
        'Na Regra Fiscal est� definido para n�o gerar lan�amentos!');
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      // Faturamento empresa principal
      {
      FreteVal := EdFreteVal.ValueVariant;
      Seguro   := EdSeguro.ValueVariant;
      Outros   := EdOutros.ValueVariant;
      }
      //
      QrSumX.Close;
      {  Erro !!!!????
      QrSumX.Params[00].AsInteger := F_Tipo;
      QrSumX.Params[01].AsInteger := F_OriCodi;
      QrSumX.Params[02].AsInteger := F_Empresa;
      UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
      }
      QrSumX.SQL.Clear;
      QrSumX.SQL.Add('SELECT SUM(Total) Total');
      QrSumX.SQL.Add('FROM stqmovvala');
      QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
      QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
      QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Empresa));
      UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
      //
      T1 := QrSumXTotal.Value + FreteVal + Seguro + Outros;
      F1 := T1;
      //
      if F_Financeiro > 0 then
      begin
        QrPrzX.Close;
        QrPrzX.SQL.Clear;
        QrPrzX.SQL.Add('SELECT Controle, Dias, Percent1 Percent ');
        QrPrzX.SQL.Add('FROM pediprzits');
        QrPrzX.SQL.Add('WHERE Percent1 > 0');
        QrPrzX.SQL.Add('AND Codigo=:P0');
        QrPrzX.SQL.Add('ORDER BY Dias');
        QrPrzX.Params[00].AsInteger := F_CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
        QrPrzX.First;
        while not QrPrzX.Eof do
        begin
          if QrPrzX.RecordCount = QrPrzX.RecNo then
            V1 := F1
          else begin
            if P1 = 0 then V1 := 0 else
              V1 := (Round(T1 * (QrPrzXPercent.Value / P1 * 100))) / 100;

            F1 := F1 - V1;
          end;

          QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
          Parcela := QrPrzT.RecNo;
          if F_EMP_FaturaNum = 0 then
            FaturaNum := FormatFloat('000000', F_EMP_IDDuplicata)
          else
            FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
          Duplicata := F_EMP_TpDuplicata + FaturaNum + F_EMP_FaturaSep +
            dmkPF.ParcelaFatura(QrPrzX.RecNo, F_EMP_FaturaSeq);
          // 2011-08-21
          // Teste para substituir no futuro (uso no form FmFatDivCms)
          NovaDup := DmProd.MontaDuplicata(F_EMP_IDDuplicata, EdNumeroNF.ValueVariant,
          Parcela, F_EMP_FaturaNum, F_EMP_FaturaSeq,
          F_EMP_TpDuplicata, F_EMP_FaturaSep);
          if NovaDup <> Duplicata then
            Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak +
            Duplicata + ' <> ' + NovaDup);
          // Fim 2011-08-21
          IncluiLancto(V1, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
            F_EMP_TxtFaturas, F_TIPOCART,
            F_CartEmis, F_EMP_CtaFaturas,
            F_Empresa, Parcela, NF_Emp_Numer,
            F_Represen, NF_Emp_Serie, True);
          //
          QrPrzX.Next;
        end;


        // Faturamento associada
        if TemAssociada then
        begin
          QrSumX.Close;
          {
          QrSumX.Params[00].AsInteger := F_Tipo;
          QrSumX.Params[01].AsInteger := F_OriCodi;
          QrSumX.Params[02].AsInteger := F_Associada;
          UnDmkDAC_PF.AbreQuery(QrSumX. O p e n;
          }
          QrSumX.SQL.Clear;
          QrSumX.SQL.Add('SELECT SUM(Total) Total');
          QrSumX.SQL.Add('FROM stqmovvala');
          QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', F_Tipo));
          QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', F_OriCodi));
          QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', F_Associada));
          UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
          T2 := QrSumXTotal.Value;
          F2 := T2;
          //
          QrPrzX.Close;
          QrPrzX.SQL.Clear;
          QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
          QrPrzX.SQL.Add('FROM pediprzits');
          QrPrzX.SQL.Add('WHERE Percent2 > 0');
          QrPrzX.SQL.Add('AND Codigo=:P0');
          QrPrzX.SQL.Add('ORDER BY Dias');
          QrPrzX.Params[00].AsInteger := F_CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
          QrPrzX.First;
          while not QrPrzX.Eof do
          begin
            if QrPrzX.RecordCount = QrPrzX.RecNo then
              V2 := F2
            else begin
              if P2 = 0 then V2 := 0 else
                V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
              F2 := F2 - V2;
            end;
            QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
            Parcela := QrPrzT.RecNo;
            if F_EMP_FaturaNum = 0 then
              FaturaNum := FormatFloat('000000', F_ASS_IDDuplicata)
            else
              FaturaNum := FormatFloat('000000', EdNumeroNF.ValueVariant);
            Duplicata := F_ASS_TpDuplicata + FormatFloat('000000', F_ASS_IDDuplicata) +
              F_ASS_FaturaSep + dmkPF.ParcelaFatura(
              QrPrzX.RecNo, F_ASS_FaturaSeq);
            IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
              F_ASS_TxtFaturas, F_TIPOCART,
              F_CartEmis, F_ASS_CtaFaturas,
              F_Associada, Parcela, NF_Ass_Numer,
              F_Represen, NF_Ass_Serie, False);
            //
            QrPrzX.Next;
          end;
        end;
      end;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //

      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
*)

function TFmNFaEdit_0310.Encerra2(): Boolean;
var
  DataFat, DataEnc: TDateTime;
  Agora: String;
  Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  FreteVal, Seguro, Outros: Double;
begin
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo, F_OriCodi], True);
      end;
      //
      Filial := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      //
      if not UnNFe_PF.InsUpdFaturasNFe(Filial, F_Empresa, FThisFatID, F_Tipo,
        F_OriCodi, F_EMP_CtaFaturas, F_Associada, F_ASS_CtaFaturas, F_ASS_FILIAL,
        F_Financeiro, F_CartEmis, F_CondicaoPG, F_AFP_Sit, F_EMP_FaturaNum,
        F_EMP_IDDuplicata, EdNumeroNF.ValueVariant, F_EMP_FaturaSeq, F_TIPOCART,
        F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq, F_Cliente,
        F_AFP_Per,
        // ini 2021-10-23
        //FreteVal, Seguro, 0, Outros,
        // fim 2021-10-23
        F_EMP_TpDuplicata,
        F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
        F_ASS_TpDuplicata, F_ASS_TxtFaturas, DataFat, QrPrzT, QrSumT, QrPrzX,
        QrSumX, QrNF_X) then Exit;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1: // FatPedCab
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else
          Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

{ 2011-11-18

object EdFreteVal: TdmkEdit
  Left = 272
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 4
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label1: TLabel
  Left = 272
  Top = 0
  Width = 36
  Height = 13
  Caption = '$ Frete:'
  Visible = False
end
object Label2: TLabel
  Left = 344
  Top = 0
  Width = 46
  Height = 13
  Caption = '$ Seguro:'
  Visible = False
end
object EdSeguro: TdmkEdit
  Left = 344
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 5
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object EdOutros: TdmkEdit
  Left = 416
  Top = 16
  Width = 68
  Height = 21
  Alignment = taRightJustify
  TabOrder = 6
  Visible = False
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
end
object Label10: TLabel
  Left = 416
  Top = 0
  Width = 57
  Height = 13
  Caption = '$ D. acess.:'
  Visible = False
end

}

end.

