object FmNFaEdit_0310: TFmNFaEdit_0310
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-001 :: Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
  ClientHeight = 850
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PageControl1: TPageControl
    Left = 0
    Top = 113
    Width = 1241
    Height = 737
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Dados Gerais '
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1233
        Height = 706
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cabe'#231'alho e rodap'#233' da NF '
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1225
            Height = 675
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox2: TGroupBox
              Left = 0
              Top = 522
              Width = 1225
              Height = 74
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
              Caption = ' Ve'#237'culo: '
              TabOrder = 0
              object Label11: TLabel
                Left = 15
                Top = 20
                Width = 38
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Placa:'
              end
              object Label12: TLabel
                Left = 118
                Top = 20
                Width = 21
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'UF:'
              end
              object Label13: TLabel
                Left = 335
                Top = 20
                Width = 448
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Observa'#231#245'es: (Nome motorista, tel. doc., etc.) N'#227'o '#233' impresso na' +
                  ' NF/NF-e!!'
              end
              object RNTC: TLabel
                Left = 158
                Top = 20
                Width = 89
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'RNTC (ANTT):'
              end
              object EdPlacaNr: TdmkEdit
                Left = 15
                Top = 39
                Width = 98
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPlacaUF: TdmkEdit
                Left = 118
                Top = 39
                Width = 35
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdObservacao: TdmkEdit
                Left = 335
                Top = 39
                Width = 469
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRNTC: TdmkEdit
                Left = 158
                Top = 39
                Width = 172
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                MaxLength = 20
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 1225
              Height = 522
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 1
              object Panel3: TPanel
                Left = 2
                Top = 18
                Width = 1221
                Height = 214
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object LaSerieNF: TLabel
                  Left = 10
                  Top = -1
                  Width = 56
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'S'#233'rie NF:'
                end
                object Label15: TLabel
                  Left = 74
                  Top = -1
                  Width = 74
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = #218'lt. NF emit.:'
                  FocusControl = DBEdit2
                end
                object Label16: TLabel
                  Left = 177
                  Top = -1
                  Width = 56
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#186' Limite:'
                  FocusControl = DBEdit3
                end
                object LaNumeroNF: TLabel
                  Left = 281
                  Top = -1
                  Width = 89
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#186' Nota Fiscal:'
                end
                object Label4: TLabel
                  Left = 10
                  Top = 54
                  Width = 73
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Quantidade:'
                end
                object Label5: TLabel
                  Left = 121
                  Top = 54
                  Width = 53
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Esp'#233'cie:'
                end
                object Label6: TLabel
                  Left = 418
                  Top = 54
                  Width = 41
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Marca:'
                end
                object Label7: TLabel
                  Left = 716
                  Top = 54
                  Width = 51
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N'#250'mero:'
                end
                object Label8: TLabel
                  Left = 1014
                  Top = 54
                  Width = 68
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Peso bruto:'
                end
                object Label9: TLabel
                  Left = 1113
                  Top = 54
                  Width = 78
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Peso l'#237'quido:'
                end
                object Label18: TLabel
                  Left = 384
                  Top = -1
                  Width = 147
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data e hora Emiss'#227'o NF'
                end
                object Label19: TLabel
                  Left = 788
                  Top = -1
                  Width = 168
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Data e hora Sa'#237'da / entrada:'
                end
                object SpeedButton21: TSpeedButton
                  Left = 665
                  Top = 20
                  Width = 25
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '?'
                  OnClick = SpeedButton21Click
                end
                object Label10: TLabel
                  Left = 596
                  Top = -1
                  Width = 61
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'TZD UTC:'
                end
                object Label25: TLabel
                  Left = 999
                  Top = -1
                  Width = 61
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'TZD UTC:'
                end
                object Label26: TLabel
                  Left = 699
                  Top = 0
                  Width = 86
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Hor'#225'rio ver'#227'o:'
                end
                object Label27: TLabel
                  Left = 1073
                  Top = 0
                  Width = 86
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Hor'#225'rio ver'#227'o:'
                end
                object Label22: TLabel
                  Left = 10
                  Top = 103
                  Width = 115
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Vag'#227'o (transporte):'
                end
                object Label23: TLabel
                  Left = 231
                  Top = 103
                  Width = 109
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Balsa (transporte):'
                end
                object Label24: TLabel
                  Left = 10
                  Top = 158
                  Width = 133
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'E-mail do destinat'#225'rio:'
                end
                object DBEdit2: TDBEdit
                  Left = 74
                  Top = 20
                  Width = 98
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  DataField = 'Sequencial'
                  DataSource = DsImprime
                  TabOrder = 1
                end
                object DBEdit3: TDBEdit
                  Left = 177
                  Top = 20
                  Width = 99
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  DataField = 'MaxSeqLib'
                  DataSource = DsImprime
                  TabOrder = 2
                end
                object EdNumeroNF: TdmkEdit
                  Left = 281
                  Top = 20
                  Width = 98
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdQuantidade: TdmkEdit
                  Left = 10
                  Top = 74
                  Width = 106
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 12
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdEspecie: TdmkEdit
                  Left = 121
                  Top = 74
                  Width = 290
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 13
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdMarca: TdmkEdit
                  Left = 418
                  Top = 74
                  Width = 291
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 14
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdNumero: TdmkEdit
                  Left = 716
                  Top = 74
                  Width = 291
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 60
                  TabOrder = 15
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdkgBruto: TdmkEdit
                  Left = 1014
                  Top = 74
                  Width = 95
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 16
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdkgLiqui: TdmkEdit
                  Left = 1113
                  Top = 74
                  Width = 99
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 17
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object TPDtEmissNF: TdmkEditDateTimePicker
                  Left = 384
                  Top = 20
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.587303645843349000
                  Time = 0.587303645843349000
                  TabOrder = 4
                  OnClick = TPDtEmissNFChange
                  OnChange = TPDtEmissNFChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDtEntraSai: TdmkEditDateTimePicker
                  Left = 788
                  Top = 20
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Date = 0.587380277778720500
                  Time = 0.587380277778720500
                  TabOrder = 8
                  OnClick = TPDtEmissNFChange
                  OnChange = TPDtEmissNFChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdSerieNF: TdmkEdit
                  Left = 10
                  Top = 20
                  Width = 60
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EddhEmiTZD: TdmkEdit
                  Left = 596
                  Top = 20
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 6
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfTZD_UTC
                  Texto = '+00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EddhSaiEntTZD: TdmkEdit
                  Left = 999
                  Top = 20
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 10
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfTZD_UTC
                  Texto = '+00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EddhEmiVerao: TdmkEdit
                  Left = 699
                  Top = 20
                  Width = 85
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EddhSaiEntVerao: TdmkEdit
                  Left = 1073
                  Top = 20
                  Width = 85
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 11
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdHrEmi: TdmkEdit
                  Left = 522
                  Top = 20
                  Width = 74
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 5
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  QryCampo = 'HrEmi'
                  UpdCampo = 'HrEmi'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdHrEntraSai: TdmkEdit
                  Left = 926
                  Top = 20
                  Width = 73
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 9
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  QryCampo = 'HrEntraSai'
                  UpdCampo = 'HrEntraSai'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdVagao: TdmkEdit
                  Left = 10
                  Top = 123
                  Width = 216
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 18
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdBalsa: TdmkEdit
                  Left = 231
                  Top = 123
                  Width = 217
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 19
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object Eddest_email: TdmkEdit
                  Left = 10
                  Top = 177
                  Width = 438
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 20
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object RGCRT: TdmkRadioGroup
                  Left = 458
                  Top = 113
                  Width = 754
                  Height = 90
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CRT (C'#243'digo de Regime Tribut'#225'rio): '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Simples Nacional'
                    '2 - Simples Nacional - excesso de sublimite de receita bruta'
                    '3 - Regime Normal')
                  TabOrder = 21
                  UpdType = utYes
                  OldValor = 0
                end
              end
              object MeinfAdic_infCpl: TdmkMemo
                Left = 2
                Top = 252
                Width = 1221
                Height = 194
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 1
                OnKeyDown = MeinfAdic_infCplKeyDown
                UpdType = utYes
              end
              object StaticText1: TStaticText
                Left = 2
                Top = 232
                Width = 1221
                Height = 20
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Observa'#231#245'es adicionais NF-e [F4]'
                TabOrder = 2
              end
              object Panel5: TPanel
                Left = 2
                Top = 446
                Width = 1221
                Height = 74
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 3
                object GroupBox10: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 848
                  Height = 74
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = 
                    ' Informa'#231#245'es de compras (Obs.: A nota de  empenho se refere a co' +
                    'mpras p'#250'blicas): '
                  TabOrder = 0
                  object Label183: TLabel
                    Left = 15
                    Top = 20
                    Width = 111
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Nota de empenho:'
                    FocusControl = EdCompra_XNEmp
                  end
                  object Label184: TLabel
                    Left = 133
                    Top = 20
                    Width = 47
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Pedido:'
                    FocusControl = EdCompra_XPed
                  end
                  object Label185: TLabel
                    Left = 487
                    Top = 20
                    Width = 121
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Contrato de compra:'
                    FocusControl = EdCompra_XCont
                  end
                  object EdCompra_XNEmp: TdmkEdit
                    Left = 15
                    Top = 39
                    Width = 114
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'Compra_XNEmp'
                    UpdCampo = 'Compra_XNEmp'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCompra_XPed: TdmkEdit
                    Left = 133
                    Top = 39
                    Width = 349
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'Compra_XPed'
                    UpdCampo = 'Compra_XPed'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCompra_XCont: TdmkEdit
                    Left = 487
                    Top = 39
                    Width = 350
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 2
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'Compra_XCont'
                    UpdCampo = 'Compra_XCont'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 848
                  Top = 0
                  Width = 373
                  Height = 74
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' Exporta'#231#227'o: (NF-e: ZA - Informa'#231#245'es de com'#233'rcio exterior) '
                  TabOrder = 1
                  object Label20: TLabel
                    Left = 15
                    Top = 20
                    Width = 21
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'UF:'
                  end
                  object Label21: TLabel
                    Left = 54
                    Top = 20
                    Width = 285
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Local onde ocorrer'#225' o embarque dos produtos: '
                  end
                  object EdUFEmbarq: TdmkEdit
                    Left = 15
                    Top = 39
                    Width = 34
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    CharCase = ecUpperCase
                    MaxLength = 2
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdxLocEmbarq: TdmkEdit
                    Left = 54
                    Top = 39
                    Width = 302
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    MaxLength = 60
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
              end
            end
            object GBRodaPe: TGroupBox
              Left = 0
              Top = 596
              Width = 1225
              Height = 79
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
              TabOrder = 2
              object Panel7: TPanel
                Left = 2
                Top = 18
                Width = 1221
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LaAviso1a: TLabel
                  Left = 208
                  Top = 7
                  Width = 542
                  Height = 19
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
                    'DID-005 '
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clSilver
                  Font.Height = -17
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAviso2a: TLabel
                  Left = 207
                  Top = 6
                  Width = 542
                  Height = 19
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'Utilize o item "Edita NF-e" do bot'#227'o "Dados NF" na janela FAT-PE' +
                    'DID-005 '
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -17
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAviso1b: TLabel
                  Left = 208
                  Top = 27
                  Width = 540
                  Height = 19
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
                    'or item).'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clSilver
                  Font.Height = -17
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAviso2b: TLabel
                  Left = 207
                  Top = 26
                  Width = 540
                  Height = 19
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'para informar o frete, seguro, desconto e despesas acess'#243'rias (p' +
                    'or item).'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -17
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object PnSaiDesis: TPanel
                  Left = 1044
                  Top = 0
                  Width = 177
                  Height = 59
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 1
                  object BtSaida: TBitBtn
                    Tag = 13
                    Left = 2
                    Top = 4
                    Width = 148
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '&Fechar'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtSaidaClick
                  end
                end
                object BtOk: TBitBtn
                  Tag = 14
                  Left = 25
                  Top = 4
                  Width = 147
                  Height = 49
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '&OK'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtOkClick
                end
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Itens da NF'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GradeItens: TDBGrid
        Left = 0
        Top = 31
        Width = 1233
        Height = 616
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsStqMovValX
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GradeItensDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'CSOSN'
            PickList.Strings = (
              '101'
              '102'
              '103'
              '201'
              '202'
              '203'
              '300'
              '400'
              '500'
              '900')
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pCredSN'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD'
            Title.Caption = 'Nome do Produto'
            Width = 269
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tam.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CFOP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TXT_iTotTrib'
            Title.Caption = 'I'
            Width = 16
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vTotTrib'
            Title.Caption = '$ Tot.Trib.'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pTotTrib'
            Title.Caption = '% Trib'
            Width = 40
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 647
        Width = 1233
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object BtTodos: TBitBtn
          Tag = 127
          Left = 25
          Top = 5
          Width = 147
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtTodosClick
        end
        object BtSelecionados: TBitBtn
          Tag = 241
          Left = 177
          Top = 5
          Width = 148
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Selecionados'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtSelecionadosClick
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1233
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Label1: TLabel
          Left = 11
          Top = 2
          Width = 692
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label2: TLabel
          Left = 10
          Top = 1
          Width = 692
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Duplo clique na linha permite informar manualmente o "Valor Apro' +
            'ximado dos Tributos" do item.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 514
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 514
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 514
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pr'#233'-edi'#231#227'o de Dados de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 59
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPesqPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Preco '
      'FROM tabeprcgrg'
      'WHERE Nivel1=:P0')
    Left = 564
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqPrcPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 836
    Top = 56
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT smva.ID) Itens, smva.CFOP, '
      'frc.OrdCFOPGer, cfp.Nome, Descricao, Complementacao'
      'FROM stqmovvala smva'
      'LEFT JOIN fisregcfop frc ON frc.Codigo=1 AND frc.CFOP=smva.CFOP'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=smva.CFOP'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'GROUP BY smva.CFOP'
      'ORDER BY frc.OrdCFOPGer DESC')
    Left = 836
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCFOPItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 6
    end
    object QrCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrFatPedNFs: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP, '
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*'
      'FROM stqmovvala smva'
      'LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa'
      'LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa '
      '          AND smva.Tipo=:P0 '
      '          AND smna.OriCodi=:P1'
      'WHERE smva.Tipo=:P2'
      'AND smva.OriCodi=:P3'
      'AND ent.Codigo<>:P4'
      'ORDER BY ent.Filial')
    Left = 636
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrImprimeAfterOpen
    BeforeClose = QrImprimeBeforeClose
    SQL.Strings = (
      'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprimeempr ime'
      'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=ime.ParamsNFs'
      'WHERE ime.Empresa=:P0'
      'AND ime.Codigo=:P1')
    Left = 700
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
      Origin = 'imprime.SerieNF'
      Required = True
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
      Origin = 'paramsnfs.Sequencial'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'paramsnfs.IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Origin = 'paramsnfs.Controle'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Origin = 'paramsnfs.MaxSeqLib'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
      Origin = 'imprime.TipoImpressao'
      Required = True
    end
    object QrImprimeSerieNF_Normal: TIntegerField
      FieldName = 'SerieNF_Normal'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Associada, emp.AssocModNF, emp.CRT, emp.CSOSN,'
      
        'emp.pCredSNAlq, emp.pCredSNMez, emp.NFeInfCpl, cpl.Texto NFeInfC' +
        'pl_TXT'
      'FROM paramsemp emp'
      'LEFT JOIN nfeinfcpl cpl ON cpl.Codigo = emp.NFeInfCpl'
      'WHERE emp.Codigo=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
    end
    object QrParamsEmppCredSN_Cfg: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'pCredSN_Cfg'
      Calculated = True
    end
    object QrParamsEmpNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
    end
    object QrParamsEmpNFeInfCpl_TXT: TWideMemoField
      FieldName = 'NFeInfCpl_TXT'
      BlobType = ftWideMemo
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 700
    Top = 56
  end
  object QrNF_X: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2')
    Left = 240
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 288
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 340
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 388
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1 '
      'AND Empresa=:P2')
    Left = 444
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrSumXprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrSumXprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrSumXprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object DsStqMovValX: TDataSource
    DataSet = QrStqMovValX
    Left = 952
    Top = 56
  end
  object TbStqMovValX_: TMySQLTable
    Database = Dmod.ZZDB
    AfterInsert = TbStqMovValX_AfterInsert
    TableName = 'stqmovvalx'
    Left = 564
    Top = 56
  end
  object QrEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CRT'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 892
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCRT: TSmallintField
      FieldName = 'CRT'
    end
  end
  object QrStqMovValX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 952
    Top = 8
    object QrStqMovValXID: TIntegerField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QrStqMovValXGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object QrStqMovValXCFOP: TWideStringField
      FieldName = 'CFOP'
      ReadOnly = True
      Size = 6
    end
    object QrStqMovValXQtde: TFloatField
      FieldName = 'Qtde'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.000;-#,###,###,###,##0.000; '
    end
    object QrStqMovValXPreco: TFloatField
      FieldName = 'Preco'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXTotal: TFloatField
      FieldName = 'Total'
      ReadOnly = True
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrStqMovValXpCredSN: TFloatField
      FieldName = 'pCredSN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrStqMovValXNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      ReadOnly = True
      Size = 50
    end
    object QrStqMovValXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      ReadOnly = True
      Size = 5
    end
    object QrStqMovValXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      ReadOnly = True
      Size = 30
    end
    object QrStqMovValXiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrStqMovValXvTotTrib: TFloatField
      FieldName = 'vTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovValXTXT_iTotTrib: TWideStringField
      FieldName = 'TXT_iTotTrib'
      Size = 1
    end
    object QrStqMovValXpTotTrib: TFloatField
      FieldName = 'pTotTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrCSOSN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM stqmovvala smva'
      'WHERE smva.Tipo=:P0'
      'AND smva.OriCodi=:P1'
      'AND smva.Empresa=:P2'
      'AND smva.CSOSN=0')
    Left = 636
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCSOSNItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
end
