unit NFeItsI03_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums,
  dmkRadioGroup;

type
  TFmNFeItsI03_0310 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label5: TLabel;
    DBEdnItem: TdmkDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    DBEdFatID: TdmkDBEdit;
    Label10: TLabel;
    DBEdFatNum: TdmkDBEdit;
    Label13: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label17: TLabel;
    EddetExport_nDraw: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel2: TPanel;
    Label18: TLabel;
    EddetExport_nRE: TdmkEdit;
    Label7: TLabel;
    EddetExport_chNFe: TdmkEdit;
    EddetExport_qExport: TdmkEdit;
    Label14: TLabel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeItsI03_0310: TFmNFeItsI03_0310;

implementation

uses UnMyObjects, Module, NFeCabA_0000, UMySQLModule, UnInternalConsts, MyDBCheck,
  ModuleGeral;

{$R *.DFM}

procedure TFmNFeItsI03_0310.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
{
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'nfeitsi03', auto_increment?[
'detExport_nDraw', 'detExport_nRE', 'detExport_chNFe',
'detExport_qExport'], [
'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle'], [
detExport_nDraw, detExport_nRE, detExport_chNFe,
detExport_qExport], [
FatID, FatNum, Empresa, nItem, Controle], UserDataAlterweb?, IGNORE?
}
  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeitsi03', '', EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'nfeitsi03',
  Controle, Dmod.QrUpd) then
  begin
    FmNFeCabA_0000.ReopenNFeItsI03(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Declara��o inclu�da / alterada!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EddetExport_nDraw.SetFocus;
    end else Close;
  end;
end;

procedure TFmNFeItsI03_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeItsI03_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeItsI03_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
