object FmNFeItsLA_0000: TFmNFeItsLA_0000
  Left = 339
  Top = 185
  Caption = 'NFe-EMISS-022 :: NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
  ClientHeight = 620
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 474
    Width = 757
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 173
    ExplicitWidth = 617
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 109
    Width = 757
    Height = 365
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 0
    ExplicitTop = 113
    ExplicitWidth = 711
    ExplicitHeight = 64
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 753
      Height = 348
      Align = alClient
      TabOrder = 0
      ExplicitTop = 19
      ExplicitHeight = 346
      object Label17: TLabel
        Left = 12
        Top = 12
        Width = 158
        Height = 13
        Caption = 'LA02-C'#243'digo do produto da ANP:'
      end
      object Label1: TLabel
        Left = 12
        Top = 184
        Width = 204
        Height = 13
        Caption = 'C'#243'digo de autoriza'#231#227'o / registro do CODIF:'
      end
      object Label7: TLabel
        Left = 12
        Top = 212
        Width = 197
        Height = 13
        Caption = 'Qtde de comb. faturado '#224' temperat. amb.:'
      end
      object Label8: TLabel
        Left = 12
        Top = 240
        Width = 119
        Height = 13
        Caption = 'Sigla da UF de consumo:'
      end
      object Label14: TLabel
        Left = 12
        Top = 40
        Width = 177
        Height = 13
        Caption = 'Descri'#231#227'o do produto conforme ANP:'
      end
      object EdDetComb_cProdANP: TdmkEdit
        Left = 228
        Top = 8
        Width = 109
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 11
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DetComb_cProdANP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object LinkLabel1: TLinkLabel
        Left = 344
        Top = 12
        Width = 123
        Height = 17
        Caption = 
          '<a href="https://simp.anp.gov.br/tabela-codigos.asp">NT 2012/003' +
          ' anp.gov.br</a>'
        TabOrder = 1
        OnLinkClick = LinkLabel1LinkClick
      end
      object EdDetComb_CODIF: TdmkEdit
        Left = 228
        Top = 180
        Width = 165
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DetComb_CODIF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDetComb_qTemp: TdmkEdit
        Left = 228
        Top = 208
        Width = 165
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'DetComb_qTemp'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDetComb_UFCons: TdmkEdit
        Left = 228
        Top = 236
        Width = 33
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DetComb_UFCons'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox3: TGroupBox
        Left = 408
        Top = 64
        Width = 333
        Height = 111
        Caption = '                                   '
        TabOrder = 8
        object CkDetComb_CIDE: TdmkCheckBox
          Left = 16
          Top = -1
          Width = 97
          Height = 17
          Caption = '  CIDE:  '
          TabOrder = 0
          QryCampo = 'DetComb_CIDE'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 329
          Height = 94
          Align = alClient
          TabOrder = 1
          object Label9: TLabel
            Left = 8
            Top = 7
            Width = 63
            Height = 13
            Caption = 'BC da CIDE: '
          end
          object Label11: TLabel
            Left = 8
            Top = 35
            Width = 130
            Height = 13
            Caption = 'Valor da al'#237'quota da CIDE: '
          end
          object Label12: TLabel
            Left = 8
            Top = 63
            Width = 63
            Height = 13
            Caption = 'BC da CIDE: '
          end
          object EdDetComb_CIDE_qBCProd: TdmkEdit
            Left = 152
            Top = 3
            Width = 165
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'DetComb_CIDE_qBCProd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDetComb_CIDE_vAliqProd: TdmkEdit
            Left = 152
            Top = 31
            Width = 165
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'DetComb_CIDE_vAliqProd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDetComb_CIDE_vCIDE: TdmkEdit
            Left = 152
            Top = 59
            Width = 165
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'DetComb_CIDE_vCIDE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object EdDetComb_descANP: TdmkEdit
        Left = 228
        Top = 36
        Width = 377
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DetComb_descANP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object LinkLabel2: TLinkLabel
        Left = 612
        Top = 40
        Width = 123
        Height = 17
        Caption = 
          '<a href="https://simp.anp.gov.br/tabela-codigos.asp">NT 2012/003' +
          ' anp.gov.br</a>'
        TabOrder = 3
        OnLinkClick = LinkLabel1LinkClick
      end
      object Panel6: TPanel
        Left = 8
        Top = 60
        Width = 397
        Height = 113
        TabOrder = 4
        object Label6: TLabel
          Left = 4
          Top = 8
          Width = 230
          Height = 13
          Caption = '% de GLP derivado de petr'#243'leo no produtio GLP:'
        end
        object Label15: TLabel
          Left = 4
          Top = 36
          Width = 258
          Height = 13
          Caption = '% de G'#225's Natural Nacional - GLGNn  no produtio GLP:'
        end
        object Label16: TLabel
          Left = 4
          Top = 64
          Width = 259
          Height = 13
          Caption = '% de G'#225's Natural Importado - GLGNi  no produtio GLP:'
        end
        object Label18: TLabel
          Left = 4
          Top = 92
          Width = 153
          Height = 13
          Caption = 'Valor de partida ($/kg sem ICM):'
        end
        object EdDetComb_pGLP: TdmkEdit
          Left = 280
          Top = 4
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'DetComb_pGLP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDetComb_pGNn: TdmkEdit
          Left = 280
          Top = 32
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'DetComb_pGNn'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDetComb_pGNi: TdmkEdit
          Left = 280
          Top = 60
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'DetComb_pGNi'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDetComb_vPart: TdmkEdit
          Left = 280
          Top = 88
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'DetComb_vPart'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 408
        Top = 176
        Width = 333
        Height = 165
        Caption = '                                   '
        TabOrder = 9
        object CkDetComb_encerrante: TdmkCheckBox
          Left = 16
          Top = -1
          Width = 97
          Height = 17
          Caption = '  Encerrante:  '
          TabOrder = 0
          QryCampo = 'DetComb_encerrante'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 329
          Height = 148
          Align = alClient
          TabOrder = 1
          ExplicitLeft = 288
          ExplicitTop = 54
          ExplicitHeight = 145
          object Label19: TLabel
            Left = 4
            Top = 8
            Width = 110
            Height = 13
            Caption = 'N'#176' bico abastecimento:'
          end
          object Label20: TLabel
            Left = 4
            Top = 36
            Width = 122
            Height = 13
            Caption = 'N'#176' bomba abastecimento:'
          end
          object Label21: TLabel
            Left = 4
            Top = 64
            Width = 123
            Height = 13
            Caption = 'N'#176' tanque abastecimento:'
          end
          object Label22: TLabel
            Left = 4
            Top = 92
            Width = 125
            Height = 13
            Caption = 'Valor do encerrante inicial:'
          end
          object Label23: TLabel
            Left = 4
            Top = 120
            Width = 118
            Height = 13
            Caption = 'Valor do encerrante final:'
          end
          object EdDetComb_encerrante_nBico: TdmkEdit
            Left = 152
            Top = 4
            Width = 164
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DetComb_encerrante_nBico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDetComb_encerrante_nBomba: TdmkEdit
            Left = 152
            Top = 32
            Width = 164
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DetComb_encerrante_nBomba'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDetComb_encerrante_nTanque: TdmkEdit
            Left = 152
            Top = 60
            Width = 164
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DetComb_encerrante_nTanque'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDetComb_encerrante_vEncIni: TdmkEdit
            Left = 152
            Top = 88
            Width = 164
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'DetComb_encerrante_vEncIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDetComb_encerrante_vEncFin: TdmkEdit
            Left = 152
            Top = 116
            Width = 164
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'DetComb_encerrante_vEncFin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 757
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 709
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 661
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 595
        Height = 32
        Caption = 'NF-e - Detalhamento Espec'#237'fico de Combust'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 506
    Width = 757
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 295
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 753
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 757
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 339
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 611
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 609
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 757
    Height = 61
    Align = alTop
    Caption = ' Dados do item da NF-e: '
    Enabled = False
    TabOrder = 5
    ExplicitWidth = 617
    object Label5: TLabel
      Left = 168
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Item:'
      FocusControl = EdnItem
    end
    object Label3: TLabel
      Left = 224
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label4: TLabel
      Left = 304
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = EdFatID
    end
    object Label10: TLabel
      Left = 40
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = EdFatNum
    end
    object Label13: TLabel
      Left = 120
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = EdEmpresa
    end
    object EdnItem: TdmkEdit
      Left = 168
      Top = 32
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFatID: TdmkEdit
      Left = 8
      Top = 32
      Width = 28
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'FatID'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFatNum: TdmkEdit
      Left = 40
      Top = 32
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'FatNum'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEmpresa: TdmkEdit
      Left = 120
      Top = 32
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object Edprod_cProd: TdmkEdit
      Left = 224
      Top = 32
      Width = 77
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Edprod_xProd: TdmkEdit
      Left = 304
      Top = 32
      Width = 433
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdCampo = 'nItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
end
