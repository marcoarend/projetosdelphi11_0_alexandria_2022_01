object FmNFeSteps_0310: TFmNFeSteps_0310
  Left = 339
  Top = 185
  Caption = 'NFe-STEPS-001 :: Passos da  NF-e 3.10'
  ClientHeight = 776
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 47
    Width = 772
    Height = 617
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLoteEnv: TPanel
      Left = 0
      Top = 0
      Width = 772
      Height = 617
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaWait: TLabel
        Left = 0
        Top = 425
        Width = 772
        Height = 26
        Align = alTop
        Alignment = taCenter
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -22
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 18
      end
      object LaExpiraCertDigital: TLabel
        Left = 0
        Top = 398
        Width = 772
        Height = 27
        Align = alTop
        Alignment = taCenter
        Caption = 'Expira'#231#227'o do Certificado Digital'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 352
      end
      object Panel4: TPanel
        Left = 0
        Top = 571
        Width = 772
        Height = 46
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 264
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Posi'#231#227'o do cursor:'
          end
          object dmkEdit1: TdmkEdit
            Left = 8
            Top = 20
            Width = 78
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit2: TdmkEdit
            Left = 86
            Top = 20
            Width = 80
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object dmkEdit3: TdmkEdit
            Left = 178
            Top = 20
            Width = 78
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object REWarning: TRichEdit
          Left = 264
          Top = 0
          Width = 508
          Height = 46
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = 4227327
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
      end
      object PnJustificativa: TPanel
        Left = 0
        Top = 259
        Width = 772
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 169
          Height = 13
          Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
        end
        object SpeedButton2: TSpeedButton
          Left = 738
          Top = 20
          Width = 20
          Height = 20
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdNFeJust: TdmkEditCB
          Left = 4
          Top = 20
          Width = 55
          Height = 20
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBNFeJust
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNFeJust: TdmkDBLookupComboBox
          Left = 59
          Top = 20
          Width = 675
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsNFeJust
          TabOrder = 1
          dmkEditCB = EdNFeJust
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object PnCancInutiliza: TPanel
        Left = 0
        Top = 163
        Width = 772
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object PnChaveNFe: TPanel
          Left = 210
          Top = 0
          Width = 284
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 127
            Height = 13
            Caption = 'Chave de acesso da NF-e:'
          end
          object EdchNFe: TEdit
            Left = 4
            Top = 20
            Width = 276
            Height = 21
            MaxLength = 255
            ReadOnly = True
            TabOrder = 0
          end
        end
        object Panel8: TPanel
          Left = 686
          Top = 0
          Width = 86
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label20: TLabel
            Left = 4
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object EdVersaoAcao: TdmkEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '3,10'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 3.100000000000000000
            ValWarn = False
            OnChange = EdVersaoAcaoChange
          end
        end
        object PnRecibo: TPanel
          Left = 0
          Top = 0
          Width = 210
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          Visible = False
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 37
            Height = 13
            Caption = 'Recibo:'
          end
          object EdRecibo: TdmkEdit
            Left = 4
            Top = 20
            Width = 201
            Height = 20
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnProtocolo: TPanel
          Left = 494
          Top = 0
          Width = 113
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object Label9: TLabel
            Left = 4
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
          end
          object EdnProt: TEdit
            Left = 4
            Top = 20
            Width = 106
            Height = 21
            MaxLength = 255
            ReadOnly = True
            TabOrder = 0
          end
        end
        object PnIDCtrl: TPanel
          Left = 607
          Top = 0
          Width = 79
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 4
          object Label16: TLabel
            Left = 4
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Controle NFe:'
          end
          object EdIDCtrl: TdmkEdit
            Left = 0
            Top = 20
            Width = 72
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object PnInutiliza: TPanel
        Left = 0
        Top = 212
        Width = 772
        Height = 47
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 3
        Visible = False
        object Label14: TLabel
          Left = 370
          Top = 4
          Width = 73
          Height = 13
          Caption = 'CNPJ empresa:'
        end
        object Label15: TLabel
          Left = 485
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object GroupBox1: TGroupBox
          Left = 86
          Top = 0
          Width = 276
          Height = 47
          Align = alLeft
          Caption = ' Intervalo de numera'#231#227'o a ser inutilizado:  '
          TabOrder = 0
          object Label10: TLabel
            Left = 8
            Top = 20
            Width = 61
            Height = 13
            Caption = 'N'#186' NF inicial:'
          end
          object Label11: TLabel
            Left = 146
            Top = 20
            Width = 54
            Height = 13
            Caption = 'N'#186' NF final:'
          end
          object EdnNFIni: TdmkEdit
            Left = 71
            Top = 16
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdnNFFim: TdmkEdit
            Left = 201
            Top = 16
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 86
          Height = 47
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label12: TLabel
            Left = 4
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Modelo:'
          end
          object Label13: TLabel
            Left = 47
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object EdModelo: TdmkEdit
            Left = 4
            Top = 20
            Width = 39
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '55'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 55
            ValWarn = False
          end
          object EdSerie: TdmkEdit
            Left = 47
            Top = 20
            Width = 33
            Height = 20
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '899'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object EdEmitCNPJ: TdmkEdit
          Left = 370
          Top = 20
          Width = 111
          Height = 20
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAno: TdmkEdit
          Left = 485
          Top = 20
          Width = 24
          Height = 20
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object PnConfig1: TPanel
        Left = 0
        Top = 101
        Width = 772
        Height = 62
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Panel6: TPanel
          Left = 111
          Top = 0
          Width = 394
          Height = 62
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 12
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label2: TLabel
            Left = 122
            Top = 13
            Width = 53
            Height = 13
            Caption = 'UF (WS):'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label17: TLabel
            Left = 4
            Top = 39
            Width = 127
            Height = 13
            Caption = 'Serial do Certificado digital:'
          end
          object Label18: TLabel
            Left = 252
            Top = 12
            Width = 74
            Height = 13
            Caption = 'Servi'#231'o [F4]:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdSerialNumber: TdmkEdit
            Left = 134
            Top = 35
            Width = 256
            Height = 21
            Enabled = False
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEmpresaChange
          end
          object EdUF_Servico: TdmkEdit
            Left = 327
            Top = 8
            Width = 63
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdUF_ServicoKeyDown
          end
          object CBUF: TComboBox
            Left = 178
            Top = 8
            Width = 67
            Height = 24
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Items.Strings = (
              'AC'
              'AL'
              'AM'
              'AP'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MG'
              'MS'
              'MT'
              'PA'
              'PB'
              'PE'
              'PI'
              'PR'
              'RJ'
              'RN'
              'RO'
              'RR'
              'RS'
              'SC'
              'SE'
              'SP'
              'TO')
          end
          object EdEmpresa: TdmkEdit
            Left = 51
            Top = 8
            Width = 60
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 111
          Height = 62
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object RGAmbiente: TRadioGroup
            Left = 0
            Top = 0
            Width = 111
            Height = 62
            Align = alClient
            Caption = ' Ambiente: '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Produ'#231#227'o'
              'Homologa'#231#227'o')
            ParentFont = False
            TabOrder = 0
          end
        end
        object PnLote: TPanel
          Left = 505
          Top = 0
          Width = 267
          Height = 62
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label4: TLabel
            Left = 4
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
          end
          object EdLote: TdmkEdit
            Left = 0
            Top = 20
            Width = 76
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object RGIndSinc: TdmkRadioGroup
            Left = 78
            Top = 13
            Width = 186
            Height = 38
            Caption = ' Envio da NF-e ao Fisco (NFe 3.10):'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Ass'#237'ncrono'
              'S'#237'ncrono')
            TabOrder = 1
            QryCampo = 'IndSinc'
            UpdCampo = 'IndSinc'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 772
        Height = 101
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object RGAcao: TRadioGroup
          Left = 0
          Top = 0
          Width = 658
          Height = 101
          Align = alClient
          Caption = ' A'#231#227'o a realizar: '
          Columns = 3
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Status do servi'#231'o'
            'Envio de lote de NF-e ao fisco'
            'Consultar lote enviado'
            'Pedir cancelamento de NF-e'
            'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e'
            'Consultar NF-e'
            'Enviar lote de eventos da NFe'
            'Consulta Cadastro Entidade'
            'Consulta situa'#231#227'o da NFE (inoperante)'
            'Consulta NF-es Destinadas'
            'Download de NF-e(s)'
            'Consulta Distribui'#231#227'o de DFe de Interesse')
          TabOrder = 0
          OnClick = RGAcaoClick
        end
        object PnAbrirXML: TPanel
          Left = 658
          Top = 0
          Width = 114
          Height = 101
          Align = alRight
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 1
          Visible = False
          object BtAbrir: TButton
            Left = 8
            Top = 4
            Width = 98
            Height = 25
            Caption = 'Abrir arquivo XML'
            Enabled = False
            TabOrder = 0
            OnClick = BtAbrirClick
          end
          object Button1: TButton
            Left = 8
            Top = 31
            Width = 98
            Height = 25
            Caption = 'Aviso'
            TabOrder = 1
            OnClick = Button1Click
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 451
        Width = 772
        Height = 91
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 6
        object TabSheet1: TTabSheet
          Caption = ' XML de envio '
          object RETxtEnvio: TMemo
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            TabOrder = 0
            WordWrap = False
            OnChange = RETxtEnvioChange
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'XML de envio (Formatado)'
          ImageIndex = 1
          object WBEnvio: TWebBrowser
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            TabOrder = 0
            ControlData = {
              4C000000214F0000F20600000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' XML Retornado (Texto) '
          ImageIndex = 2
          object RETxtRetorno: TMemo
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            TabOrder = 0
            WantReturns = False
            OnChange = RETxtRetornoChange
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' XML Retornado (Formatado) '
          ImageIndex = 3
          object WBResposta: TWebBrowser
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            TabOrder = 0
            ControlData = {
              4C000000214F0000F20600000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Informa'#231#245'es do XML de retorno '
          ImageIndex = 4
          object MeInfo: TMemo
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Chaves'
          ImageIndex = 5
          object MeChaves: TMemo
            Left = 0
            Top = 0
            Width = 766
            Height = 67
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 542
        Width = 772
        Height = 29
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Panel11'
        TabOrder = 7
        object Label19: TLabel
          Left = 8
          Top = 8
          Width = 68
          Height = 13
          Caption = 'Web Service: '
        end
        object EdWebService: TEdit
          Left = 75
          Top = 4
          Width = 682
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
      end
      object PnCadastroContribuinte: TPanel
        Left = 0
        Top = 303
        Width = 772
        Height = 47
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 8
        Visible = False
        object Label21: TLabel
          Left = 35
          Top = 4
          Width = 103
          Height = 13
          Caption = 'CNPJ do contribuinte:'
        end
        object Label22: TLabel
          Left = 4
          Top = 4
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object EdContribuinte_CNPJ: TdmkEdit
          Left = 35
          Top = 20
          Width = 111
          Height = 20
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdContribuinte_UF: TdmkEdit
          Left = 4
          Top = 20
          Width = 29
          Height = 20
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object PnDesConC: TPanel
        Left = 0
        Top = 350
        Width = 772
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 9
        Visible = False
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 102
          Height = 13
          Caption = 'CNPJ do destinat'#225'rio:'
        end
        object Label24: TLabel
          Left = 122
          Top = 4
          Width = 37
          Height = 13
          Caption = 'indNFe:'
        end
        object Label25: TLabel
          Left = 166
          Top = 4
          Width = 31
          Height = 13
          Caption = 'indEmi'
        end
        object Label26: TLabel
          Left = 209
          Top = 4
          Width = 37
          Height = 13
          Caption = 'ultNSU:'
        end
        object Label23: TLabel
          Left = 315
          Top = 4
          Width = 26
          Height = 13
          Caption = 'NSU:'
        end
        object EddestCNPJ: TdmkEdit
          Left = 8
          Top = 20
          Width = 110
          Height = 20
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdIndNFe: TdmkEdit
          Left = 122
          Top = 20
          Width = 40
          Height = 20
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdindEmi: TdmkEdit
          Left = 166
          Top = 20
          Width = 39
          Height = 20
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdultNSU: TdmkEdit
          Left = 209
          Top = 20
          Width = 102
          Height = 20
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfInt64
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdultNSUKeyDown
        end
        object EdNSU: TdmkEdit
          Left = 315
          Top = 20
          Width = 103
          Height = 20
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfInt64
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdultNSUKeyDown
        end
        object RGFrmaCnslt: TdmkRadioGroup
          Left = 426
          Top = 0
          Width = 346
          Height = 48
          Align = alRight
          Caption = ' Forma da consulta: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Pelo '#250'ltimo NSU'
            'Tudo que for poss'#237'vel'
            'NSU '#250'nico')
          TabOrder = 5
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 725
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 678
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 241
        Height = 31
        Caption = 'Passos da NF-e 3.10'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 241
        Height = 31
        Caption = 'Passos da NF-e 3.10'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 241
        Height = 31
        Caption = 'Passos da NF-e 3.10'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 664
    Width = 772
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 768
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 707
    Width = 772
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 629
      Top = 14
      Width = 141
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 14
      Width = 627
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Panel10: TPanel
        Left = 289
        Top = -4
        Width = 177
        Height = 44
        BevelOuter = bvNone
        TabOrder = 1
        object CkSoLer: TCheckBox
          Left = 0
          Top = 20
          Width = 186
          Height = 17
          Caption = 'Somente ler arquivo j'#225' gravado.'
          Enabled = False
          TabOrder = 0
        end
      end
    end
  end
  object QrNFeCabA1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND LoteEnv=:P1'
      '')
    Left = 404
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabA1IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrNFeJust: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 44
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrNFeJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsNFeJust: TDataSource
    DataSet = QrNFeJust
    Left = 72
    Top = 8
  end
  object QrNFeCabA2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND IDCtrl=:P1'
      '')
    Left = 404
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA2FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabA2Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl, infProt_ID, infProt_nProt'
      'FROM nfecaba '
      'WHERE Empresa=:P0'
      'AND id=:P1')
    Left = 524
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAinfProt_ID: TWideStringField
      FieldName = 'infProt_ID'
      Size = 30
    end
    object QrCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 16
    Top = 8
  end
  object QrDFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 644
    Top = 396
  end
end
