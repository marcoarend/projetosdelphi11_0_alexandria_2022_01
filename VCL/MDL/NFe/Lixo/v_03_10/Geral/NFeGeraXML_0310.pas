// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria
unit NFeGeraXML_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Variants, Math,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnXXe_PF,
  // Consulta status servi�o
  consStatServ_v310,
  // Consulta situa��o NF-e
  //consSitNFe_v200,
  consSitNFe_v310,
  // Pede inutiliza��o NF-e
  inutNFe_v310,
  // Gera NFe
  //nfe_v200,
  nfe_v310,
  // Envia lote de NF-es
  // ???
  // Consulta Lote de NF-es
  consReciNFe_v200,
  // Cancela NF-e
  //cancNFe_v200,
  // Consulta cadastro contribuinte
  consCad_v200,
  // Consulta NFes Destinadas
  //consNFeDest_v101,
  // Download NFes Confirmadas (destinadas)
  downloadNFe_v100,
  //
  // Distribui��o de DFe de Interesse
  distDFeInt_v100,
  //
  XSBuiltIns, StrUtils, ComObj, SoapConst, mySQLDbTables, dmkImage, UnDmkEnums;

const
  //FVersaoWS           = '3.10'; // 0310
  NFe_AllModelos      = '55'; // SQL 55,?,?
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/nfe"';
  NAME_SPACE_CTE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  sCampoNulo = '#NULL#';

type
  TEnumeracao = (enumProcessoEmissao, enumtpEmis, enumFinNFe, enumTpNF,
    enumIndPag, enumOrigemMercadoria, enumCSTICMS);
  // Copiado de ACBr
  TStatusACBrNFe = ( stIdle, stNFeStatusServico, stNFeRecepcao, stNFeRetRecepcao, stNFeConsulta, stNFeCancelamento, stNFeInutilizacao, stNFeRecibo, stNFeCadastro, stNFeEmail, stNFeEnvDPEC, stNFeConsultaDPEC, stNFeCCe );
  TStatusACBrCTe = ( stCTeIdle, stCTeStatusServico, stCTeRecepcao, stCTeRetRecepcao, stCTeConsulta, stCTeCancelamento, stCTeInutilizacao, stCTeRecibo, stCTeCadastro, stCTeEmail, stCTeCCe );
  (* IMPORTANTE - Sempre que alterar um Tipo efetuar a atualiza��o das fun��es de convers�o correspondentes *)
  TLayOut = (LayNfeRecepcao,LayNfeRetRecepcao,LayNfeCancelamento,LayNfeInutilizacao,LayNfeConsulta,LayNfeStatusServico,LayNfeCadastro, LayNfeEnvDPEC, LayNfeConsultaDPEC,
             LayCTeRecepcao,LayCTeRetRecepcao,LayCTeCancelamento,LayCTeInutilizacao,LayCTeConsultaCT,LayCTeStatusServico,LayCTeCadastro,LayNFeCCe);

  TpcnSchema = (TsPL005c, TsPL006);
  // Incluido o tlConsStatServCTe para CTe por possuir uma vers�o diferente da NFe
  TpcnTipoLayout = (tlAtuCadEmiDFe, tlCadEmiDFe, tlCancNFe, tlConsCad, tlConsReciNFe, tlConsSitNFe, tlConsStatServ,
    tlInutNFe, tlNFe, tlProcNFe, tlProcInutNFe, tlRetAtuCadEmiDFe, tlRetCancNFe, tlRetConsCad, tlRetConsReciNFe,
    tlRetConsStatServ, tlRetConsSitNFe, tlRetEnvNFe, tlRetInutNFe, tlEnvNFe, tlProcCancNFe,
                    tlCancCTe, tlConsReciCTe, tlConsSitCTe,
    tlInutCTe, tlCTe, tlProcCTe, tlProcInutCTe, tlRetCancCTe, tlRetConsReciCTe,
    tlRetConsSitCTe, tlRetEnvCTe, tlRetInutCTe, tlEnvCTe, tlProcCancCTe, tlEnvDPEC, tlConsDPEC, tlConsStatServCTe, tlCCeNFe, tlEnvCCeNFe, tlRetEnvCCeNFe);

  // Tipo tcDe6 incluido por Italo em 30/09/2010 (usado no CTe campo 435: vTar = valor da tarifa do modal Dutovi�rio)
  TpcnTipoCampo = (tcStr, tcInt, tcDat, tcDatHor, tcEsp, tcDe2, tcDe3, tcDe4, tcDe10, tcHor, tcDe6 ); // tcEsp = String: somente numeros;
  TpcnFormatoGravacao = (fgXML, fgTXT);
  TpcnTagAssinatura = (taSempre, taNunca, taSomenteSeAssinada, taSomenteParaNaoAssinada);

  TpcnIndicadorPagamento = (ipVista, ipPrazo, ipOutras);
  TpcnTipoNFe = (tnEntrada, tnSaida);
  TpcnTipoImpressao = (tiRetrato, tiPaisagem);
  TpcnTipoEmissao = (teNormal, teContingencia, teSCAN, teDPEC, teFSDA);
  TpcnTipoAmbiente = (taProducao, taHomologacao);
  TpcnSituacaoEmissor = (seHomologacao, seProducao);
  TpcnFinalidadeNFe = (fnNormal, fnComplementar, fnAjuste);
  TpcnProcessoEmissao = (peAplicativoContribuinte, peAvulsaFisco, peAvulsaContribuinte, peContribuinteAplicativoFisco);
  TpcnTipoOperacao = (toVendaConcessionaria, toFaturamentoDireto, toVendaDireta, toOutros);
  TpcnCondicaoVeiculo = (cvAcabado, cvInacabado, cvSemiAcabado);
  TpcnTipoArma = (taUsoPermitido, taUsoRestrito);
  TpcnOrigemMercadoria = (oeNacional, oeEstrangeiraImportacaoDireta, oeEstrangeiraAdquiridaBrasil);
  TpcnCSTIcms = (cst00, cst10, cst20, cst30, cst40, cst41, cst45, cst50, cst51, cst60, cst70, cst80, cst81, cst90, cstPart10, cstPart90, cstRep41, cstVazio); //80 e 81 apenas para CTe
  TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
  TpcnDeterminacaoBaseIcms = (dbiMargemValorAgregado, dbiPauta, dbiPrecoTabelado, dbiValorOperacao);
  TpcnDeterminacaoBaseIcmsST = (dbisPrecoTabelado, dbisListaNegativa, dbisListaPositiva, dbisListaNeutra, dbisMargemValorAgregado, dbisPauta);
  TpcnMotivoDesoneracaoICMS = (mdiTaxi, mdiDeficienteFisico, mdiProdutorAgropecuario, mdiFrotistaLocadora, mdiDiplomaticoConsular, mdiAmazoniaLivreComercio, mdiSuframa, mdiOutros );
  TpcnCstIpi = (ipi00, ipi49, ipi50, ipi99, ipi01, ipi02, ipi03, ipi04, ipi05, ipi51, ipi52, ipi53, ipi54, ipi55);
  TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis49, pis50, pis51, pis52, pis53, pis54, pis55, pis56, pis60, pis61, pis62, pis63, pis64, pis65, pis66, pis67, pis70, pis71, pis72, pis73, pis74, pis75, pis98, pis99);
  TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof49, cof50, cof51, cof52, cof53, cof54, cof55, cof56, cof60, cof61, cof62, cof63, cof64, cof65, cof66, cof67, cof70, cof71, cof72, cof73, cof74, cof75, cof98, cof99);
  TpcnModalidadeFrete = (mfContaEmitente, mfContaDestinatario, mfContaTerceiros, mfSemFrete);
  TpcnIndicadorProcesso = (ipSEFAZ, ipJusticaFederal, ipJusticaEstadual, ipSecexRFB, ipOutros);
  TpcnCRT = (crtSimplesNacional, crtSimplesExcessoReceita, crtRegimeNormal);
  TpcnIndicadorTotal = (itSomaTotalNFe, itNaoSomaTotalNFe );

  TpcteFormaPagamento = (fpPago, fpAPagar, fpOutros);
  TpcteTipoCTe = (tcNormal, tcComplemento, tcAnulacao, tcSubstituto);
  TpcteModal = (mdRodoviario, mdAereo, mdAquaviario, mdFerroviario, mdDutoviario);
  TpcteTipoServico = (tsNormal, tsSubcontratacao, tsRedespacho, tsIntermediario);
  TpcteRetira = (rtSim, rtNao);
  TpcteTomador = ( tmRemetente, tmExpedidor, tmRecebedor, tmDestinatario, tmOutros);
  TpcteRspSeg = (rsRemetente, rsExpedidor, rsRecebedor, rsDestinatario, rsEmitenteCTe, rsTomadorServico);
  TpcteLotacao = (ltNao, ltSim);
  TpcteProp = (tpTACAgregado, tpTACIndependente, tpOutros);
  TpcteMask = (msk4x2, msk7x2, msk9x2, msk10x2, msk13x2, msk15x2, msk6x3, mskAliq);
  UnidMed = (uM3,uKG, uTON, uUNIDADE, uLITROS);
  TpcnECFModRef = (ECFModRefVazio, ECFModRef2B,ECFModRef2C,ECFModRef2D);
  TpcnISSQNcSitTrib  = ( ISSQNcSitTribVazio , ISSQNcSitTribNORMAL, ISSQNcSitTribRETIDA, ISSQNcSitTribSUBSTITUTA,ISSQNcSitTribISENTA);

  // Incluido por Italo em 31/09/2010
  TpcteDirecao = (drNorte, drLeste, drSul, drOeste);
  TpcteTipoNavegacao = (tnInterior, tnCabotagem);
  // Incluido por Italo em 19/11/2010
  TpcteTipoTrafego = (ttProprio, ttMutuo, ttRodoferroviario, ttRodoviario);
  // Incluido por Italo em 24/01/2011
  TpcteTipoDataPeriodo = (tdSemData, tdNaData, tdAteData, tdApartirData, tdNoPeriodo);
  TpcteTipoHorarioIntervalo = (thSemHorario, thNoHorario, thAteHorario, thApartirHorario, thNoIntervalo);
  TpcteTipoDocumento = (tdDeclaracao, tdOutros);
  TpcteTipoDocumentoAnterior = (daCTRC, daCTAC, daACT, daNF7, daNF27, daCAN, daCTMC, daATRE, daDTA, daCAI, daCCPI, daCA, daTIF, daOutros);
  TpcteRspPagPedagio = (rpEmitente, rpRemetente, rpExpedidor, rpRecebedor, rpDestinatario, rpTomadorServico);
  TpcteTipoDispositivo = (tdCartaoMagnetico, tdTAG, tdTicket);
  TpcteTipoPropriedade = (tpProprio, tpTerceiro);
  TpcteTipoVeiculo = (tvTracao, tvReboque);
  TpcteTipoRodado = (trNaoAplicavel, trTruck, trToco, trCavaloMecanico, trVAN, trUtilitario, trOutros);
  TpcteTipoCarroceria = (tcNaoAplicavel, tcAberta, tcFechada, tcGraneleira, tcPortaContainer, tcSider);
  // Incluido por Italo em 28/04/2011
  TPosRecibo = (prCabecalho, prRodape);
  // Fim ACBr
  TTipoConsumoWS = (tcwsStatusServico,
                    //tcwsEnviaLoteNF,
                    //tcwsConsultaLote,
                    tcwsPedeCancelamento,
                    tcwsPediInutilizacao,
                    tcwsConsultaNFe,
                    tcwsRecepcaoEvento,
                    tcwsConsultaCadastro,
                    tcwsConsultaNFeDest,
                    //tcwsDownloadNFeDest, substituido por tcwsNFeDownloadNF ????
                    //
                    tcwsNFeDownloadNF,
                    tcwsAutorizacao,
                    tcwsRetAutorizacao,
                    //
                    tcwsDistDFeInt);
  TFmNFeGeraXML_0310 = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    //
    function  ObtemWebServer(UFServico: String; Ambiente, CodigoUF: Integer;
              Acao: TTipoConsumoWS; sAviso: String; LaAviso1, LaAviso2: TLabel):
              Boolean;
    function  ObtemWebServer2(UFServico: String; Ambiente, CodigoUF: Integer;
              Acao: TTipoConsumoWS; sAviso: String): Boolean;
    // Montagem da NFe
    function  GerarXMLdaNFe(FatID, FatNum, Empresa: Integer; const nfeID: String;
              GravaCampos: Integer): Boolean;
    function  Def(const Codigo, ID: String; const Source: Variant; var Dest:
              String): Boolean;
    function  Def_UTC(const Codigo, ID: String; const Source1, Source2, Source3: Variant;
              var Dest: String): Boolean;
    function  FMT_IE(Valor: String): String;
    function  OutroPais_emit(): Boolean;
    function  OutroPais_dest(): Boolean;
    function  OutroPais_retirada(): Boolean;
    function  OutroPais_entrega(): Boolean;
    // 2.00
    procedure ConfiguraReqResp( ReqResp : THTTPReqResp);
    function  SeparaDados(Texto: String; Chave: String; MantemChave: Boolean = False ): String;
    //  Adaptado ACBR
    function  SeparaApartir(Chave, Texto: String): String;
    function  SeparaAte(Chave, Texto: String; var Resto: String): String;
    //function LerCampo(Texto, NomeCampo: string; Tamanho : Integer = 0): string;
    function  LerCampoA(var Texto: String; const NomeCampo: string;
              const Tamanho: Integer): string;
    function  RetornarCodigoNumerico(Chave, Versao: String): String;
    function  ConverteStrToNumero(Valor: String ): String;
    function  StrToprocEmi(var ok: boolean; const s: string): TpcnProcessoEmissao;
    function  StrToTpEmis(var ok: boolean; const s: string): TpcnTipoEmissao;
    function  StrToEnumerado(var ok: boolean; const s: string; const AString:
              array of string; const AEnumerados: array of variant): variant;
    function  StrToFinNFe(var ok: boolean; const s: string): TpcnFinalidadeNFe;
    function  StrToTpNF(var ok: boolean; const s: string): TpcnTipoNFe;
    function  StrToIndpag(var ok: boolean; const s: string): TpcnIndicadorPagamento;
    function  StrToOrig(var ok: boolean; const s: string): TpcnOrigemMercadoria;
    function  StrToCSTICMS(var ok: boolean; const s: string): TpcnCSTICMS;


    function  dmk_DefValDeEnum(var Campo: String; const Enum: TEnumeracao;
              var Texto: String; const NomeCampo: string; const Tamanho: Integer = 0): Boolean;
    function  dmk_DefValDeText(var Campo: String; var Texto: String; const
              NomeCampo: string; const Tamanho : Integer = 0): Boolean;
    // fim ACBR
    procedure VerificaSeHaTextoExtra(Grupo, Texto, Titulo: String);
    procedure AvisaTextoNaoProcessado(MyID_Loc: Integer; Grupo, Texto: String);
    function  VersaoWS(Acao: TTipoConsumoWS; Formata: Boolean = True): String;
  public
    { Public declarations }
    function  Alltrim(const Search: string): string;
    procedure ConfiguraRio( Rio : THTTPRIO);
  // ver!!! Delphi Alexandria
    //procedure OnBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
  // ver!!! Delphi Alexandria

    function  MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
              nNFFim: String; var Id: String): Boolean;
    function  DesmontaID_Inutilizacao(const Id: String; var cUF, Ano,
              emitCNPJ, Modelo, Serie, nNFIni, nNFFim: String): Boolean;
    function  StrZero(Num : Real; Zeros, Deci: Integer): String;
    function  TipoXML(NoStandAlone: Boolean): String;
    //
    //function XML_CabecMsg(VersaoDados: String; NoStandAlone: Boolean): String;
    function  XML_ConsStatServ(TpAmb, CUF: Integer): String;
    function  XML_ConsReciNFe(TpAmb: Integer; NRec: String): String;
    function  XML_ConsSitNFe(TpAmb: Integer; ChNFe, VersaoAcao: String): String;
    function  XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte; Ano: Integer;
              CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): String;
    //function XML_CancNFe(ChNFe: String; TpAmb: Integer; nProt, XJust: String): String;
    function  XML_ConsCad(xUF, Contribuinte_CNPJ: String): String;
    //
(*
    function  XML_ConsNFeDest(VersaoAcao: String; TpAmb: Integer; CNPJ: String;
              indNFe, indEmi, ultNSU: String): String;
*)
    function  XML_DistDFeInt(TpAmb: Integer; UFAutor, CNPJ, CPF:
              String; ultNSU, NSU: String): String;
    function  XML_DowNFeDest(VersaoAcao: String; TpAmb: Integer; CNPJ: String;
              Chaves: TStrings): String;
    function  XML_NfeDownloadNF(VersaoAcao: String; TpAmb: Integer; CNPJ: String;
              Chaves: TStrings): String;
    //
    function  CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
              var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
              GravaCampos: Integer): Boolean;
    //  Assinatura
    function  ExecutaAssinatura(aValue: String; Certificado: ICertificate2;
              URIs: TStringList; out sXML: String): Boolean;
    // Gera lote de envio
(*
    function  GerarLoteNFe(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
*)
    function  GerarLoteNFeNovo(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia: TXXeIndSinc): Boolean;
    function  GerarLoteDownloadNFeConfirmadas(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    function  GerarLoteDownloadNFeDestinadas(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    function  GerarConsultaNFDest(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    function  GerarLoteEvento(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
    //
    function  WS_NFeStatusServico(UFServico: String; Ambiente,
              CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_NFeInutilizacaoNFe(UFServico: String; Ambiente, CodigoUF, Ano: Byte;
              Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
              XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_NFeRecepcaoLote(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc): String;
    function  WS_NFeRetRecepcao(UFServico: String; Ambiente, CodigoUF: Byte;
              Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_NFeConsultaNF(UFServico: String; Ambiente, CodigoUF: Byte;
              ChaveNFe: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  RecuperarXMLdaWeb(TextoHTML: String): String;
    function  GerarXML_Da_WEB_1(const Arquivo: String;
              var XML_Prot: String; var ChaveDeAcesso: String;
              var Empresa: Integer; var Status: Integer; var VersaoNFe: Double) : Boolean;
    function  WS_RecepcaoEvento(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_NFeConsultaCadastro(Servico_UF, Contribuinte_UF,
              Contribuinte_CNPJ: String; Certificado: String; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_NFeConsultaNFDest(Servico_UF, Destinatario_UF,
              VersaoAcao: String; TpAmb: Integer; CNPJ: String;
              indNFe, indEmi, ultNSU: String;
              Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit): String;
    function  WS_NFeConsultaDistDFeInt(Servico_UF, Interesse_UF: String; TpAmb:
              Integer; CNPJ, CPF: String; ultNSU, NSU: String; Certificado:
              String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit; ItemStr: String): String;
    function  WS_NFeDownloadNFeDestinadas(Servico_UF, Destinatario_UF,
              VersaoAcao: String; TpAmb: Integer; CNPJ: String;
              Chaves: TStrings;
              Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit): String;
    function  WS_NFeDownloadNFeConfirmadas(Servico_UF, Destinatario_UF: String;
              TpAmb: Integer; CNPJ: String; Chaves: TStrings;
              Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit): String;
    function  NomeAcao(Acao: TTipoConsumoWS): String;

   // procedure ExecutaReqResp();

  end;
  var
  FmNFeGeraXML_0310: TFmNFeGeraXML_0310;

  //const
  //$verCabecMsg_Versao         = '2.00'; // Cabe�alho
  //verConsStatServ_Versao     = '3.10'; // Consulta status servi�o
  //$verConsSitNFe_Versao_0200  = '2.00'; // Consulta situa��o NF-e 2.00
  //$verConsSitNFe_Versao_0201  = '2.01'; // Consulta situa��o NF-e 2.01
  //verNFeInutNFe_Versao       = '3.10'; // Solicita inutiliza��o de NF-e(s)
  //$verCancNFe_Versao          = '2.00'; // Cancelamento de NF-e
  //$verEnviNFe_Versao          = '2.00'; // Envia Lote de NF-e(s)
  //verConsReciNFe_Versao      = '3.10'; // Consulta Lote de NF-e(s)
  //verProcNFe_Versao        = '1.10'; // Distribui��o de NF-e
  //verConsCad_Versao          = '2.00'; // Consulta Cadastro (Contribuinte)
  //$verConsNFeDest_Versao      = '1.01'; // Consulta NF-es Destinadas
  //$verDowNFeDest_Versao       = '1.00'; // Download NF-es Confirmadas (Destinadas)
  //verEnviNFeNovo_Versao      = '3.10'; // Envia Lote de NF-e(s) Novo 3.10
  //verConsReciNFeNovo_Versao  = '3.10'; // Consulta Lote de NF-e(s) Novo 3.10
  //
  // N�o utiliza!
  //verCabecMsg_VersaoDados  = '1.07';
  //
implementation

uses UnMyObjects, Module, ModuleNFe_0000, UnInternalConsts, UMySQLModule,
//NFeBaixaXMLdaWeb_0200,
NFeSteps_0310, ModuleGeral, NFeXMLGerencia, DmkDAC_PF, UnDmkProcFunc, NFe_PF,
  Recibo;

const
  sWeb_Totais = 'TOTAIS|&|';
  sWeb_IPI = 'IMPOSTO SOBRE PRODUTOS INDUSTRIALIZADOS';

var
  FGravaCampo: Integer;
  FLaAviso1, FLaAviso2: TLabel;
  FdocXML: TXMLDocument;
  FCabecTxt, FDadosTxt, FAssinTxt: String;
  //FWSDL,
  FURL: String;
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;
  //
  NomeArquivo: String;
  CaminhoArquivo: String;
  strTpAmb: string;
  SerieNF: String;
  NumeroNF: String;
  CDV: String;
  cXML: IXMLTNFe;
  arqXML: TXMLDocument;
  (* Objetos do Documento XML... *)
  cAutXML: IXMLAutXML;
  cDetLista: IXMLDet;
  cRefLista: IXMLNFref;
  cRebLista: IXMLTVeiculo;
  cVolLista: IXMLVol;
  cLacLista: IXMLLacres;
  cDupLista: IXMLDup;
  cDILista: IXMLDI;
  //cNVELista: IXMLNVE;
  cDetExport: IXMLDetExport;
  cAdiLista: IXMLAdi;
  cMedLista: IXMLMed;
  cArmLista: IXMLArma;
  cObsCont: IXMLObsCont;
  cObsFisco: IXMLObsFisco;
  cProcRefLista: IXMLProcRef;
  //
  //cChaves: IXMLc
  FFatID,
  FFatNum,
  FEmpresa,
  FOrdem: Integer;


{$R *.DFM}

function TFmNFeGeraXML_0310.Alltrim(const Search: string): string;
const
  BlackSpace = [#33..#126];
var
  Index: byte;
begin
  Index:=1;
  while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
    begin
      Index:=Index + 1;
    end;
  Result:=Copy(Search, Index, 255);
  Index := Length(Result);
  while (Index > 0) and not (Result[Index] in BlackSpace) do
    begin
      Index:=Index - 1;
    end;
  Result := Copy(Result, 1, Index);
end;

procedure TFmNFeGeraXML_0310.AvisaTextoNaoProcessado(MyID_Loc: Integer; Grupo, Texto: String);
var
  Txt: String;
begin
//  Txt := Geral.Substitui(Texto, '|&|', '|&|'#13#10);
  Txt := StringReplace(Texto, '|&|', '|&|'#13#10, [rfReplaceAll]);
  //
  Geral.MB_Erro('O texto abaixo n�o foi processado:' + sLineBreak  +
  '------------------- IDENTIFICA��O: ------------------------------' + sLineBreak  +
  'Grupo: "' + Grupo + '"' + sLineBreak  + 'ID: ' + FormatFloat('00', MyID_Loc) + sLineBreak  +
  '------------------- TEXTO N�O PROCESSADO: -----------------------' + sLineBreak  +
  Txt);
end;

procedure TFmNFeGeraXML_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeGeraXML_0310.ConfiguraReqResp(ReqResp: THTTPReqResp);
begin
  {  Parei aqui! Fazer
  if FConfiguracoes.WebServices.ProxyHost <> '' then
   begin
     ReqResp.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
     ReqResp.UserName     := FConfiguracoes.WebServices.ProxyUser;
     ReqResp.Password     := FConfiguracoes.WebServices.ProxyPass;
   end;
  }
  // ver!!! Delphi Alexandria
  //ReqResp.OnBeforePost := OnBeforePost;
  // ver!!! Delphi Alexandria
end;

procedure TFmNFeGeraXML_0310.ConfiguraRio(Rio: THTTPRIO);
begin
  {
  if FConfiguracoes.WebServices.ProxyHost <> '' then
   begin
     Rio.HTTPWebNode.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
     Rio.HTTPWebNode.UserName     := FConfiguracoes.WebServices.ProxyUser;
     Rio.HTTPWebNode.Password     := FConfiguracoes.WebServices.ProxyPass;
   end;
  }
  // ver!!! Delphi Alexandria
  //Rio.HTTPWebNode.OnBeforePost := OnBeforePost;
  // ver!!! Delphi Alexandria
end;

function TFmNFeGeraXML_0310.ConverteStrToNumero(Valor: String): String;
begin
  //Result := FloatToStr(StrToFloatDef(StringReplace(Valor,ThousandSeparator,'',[rfReplaceAll]), 0));
  Result := Geral.Substitui(Valor, '.', '');
  Result := Geral.Substitui(Result, ',', '.');
  //ShowMessage(Valor + ' --> ' + Result);
end;

function TFmNFeGeraXML_0310.CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
  GravaCampos: Integer): Boolean;
var
  strChaveAcesso: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1; 
  FLaAviso2 := LaAviso2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM nfexmli WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := FatID;
  Dmod.QrUpd.Params[01].AsInteger := FatNum;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  FOrdem   := 0;
  //
  DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetNFe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  }

  SerieNF := FormatFloat('000', DmNFe_0000.QrNFECabAide_Serie.Value);
  NumeroNF := FormatFloat('00000000', DmNFe_0000.QrNFECabAide_cNF.Value); (* C�digo Aleat�rio que ir� compor a Chave de Acesso...*)

  (* Montar a Chave de Acesso da NFe de acordo com as informa��es do Registro...*)
  (* cUF=??,dEmi=...*)
  strChaveAcesso := DmNFe_0000.QrNFECabAId.Value;

  CDV := Copy(strChaveAcesso, Length(strChaveAcesso), Length(strChaveAcesso));
  if GerarXMLdaNFe(FatID, FatNum, Empresa, 'NFe' + strChaveAcesso, GravaCampos) then
  begin
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    DmNFe_0000.SalvaXML(NFE_EXT_NFE_XML, strChaveAcesso, arqXML.XML.Text, nil, False);
    //
    Result := True;
  end;
  arqXML := nil;
end;

function TFmNFeGeraXML_0310.Def(const Codigo, ID: String; const Source: Variant;
  var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  Result := False;
  Dest   := '';
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if DmNFe_0000.QrNFeLayIOcorMin.Value > 0 then
    begin
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source, Codigo + '  ' + ID);
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'N' then
      begin
        // Texto
        if ((VarType(Source) = varString) or (VarType(Source) = varStrArg) or
          (VarType(Source) =  varUString) or (VarType(Source) =  varOleStr))
          and (Length(Source) > 18) then
        begin
          Dest := Source
        end
        else if (Codigo = '17') and (ID = 'B13') then
          Dest := Source
        // Double
        else if DmNFe_0000.QrNFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source, ffFixed, 15,
            DmNFe_0000.QrNFeLayIDeciCasas.Value))
        else begin
        //integer
          if DmNFe_0000.QrNFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmNFe_0000.QrNFeLayITamMax.Value, siPositivo))
          else
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source));
        end;
      end else
      // data
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'D' then
      begin
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source);
        end else
        begin
          if DmNFe_0000.QrNFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmNFe_0000.QrNFeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source);
        end;
      end else
      // hora
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source)
      else
      // texto
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'C' then
        Dest := Source
      else
      // desconhecido
      begin
        Dest := Source;
        Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Codigo, ID));
      if (Dest = '') and (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmNFe_0000.QrNFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
          if (Codigo = '78') and (ID = 'E17') and
          ((DmNFe_0000.QrNFECabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabAdest_CNPJ.Value) = '')) then
            Avisa := False;
          if Avisa then
            Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Codigo, ID, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
  'N�o foi poss�vel definir o valor', 3));
{
  // Teste
  if Geral.IMV(Geral.SoNumero_TT(Codigo)) > 1000 then
    Geral.MB_('item #' + Codigo + '- ID = ' + ID + sLineBreak  +
    'Valor = "' + Dest + '"', 'Informa��o', MB_OK+MB_ICONINFORMATION);
}
end;

function TFmNFeGeraXML_0310.Def_UTC(const Codigo, ID: String;
  const Source1, Source2, Source3: Variant; var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  Result := False;
  Dest   := '';
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if DmNFe_0000.QrNFeLayIOcorMin.Value > 0 then
    begin
      if (Codigo = '255') and (ID = 'O11') and (Source1 = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source1 = 0) then
        Continua := False else
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source1, Codigo + '  ' + ID);
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'N' then
      begin
        // Texto
        if (Codigo = '17') and (ID = 'B13') then
          Dest := Source1
        // Double
        else if DmNFe_0000.QrNFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source1, ffFixed, 15,
            DmNFe_0000.QrNFeLayIDeciCasas.Value))
        else begin
        //integer
          if DmNFe_0000.QrNFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source1),
            DmNFe_0000.QrNFeLayITamMax.Value, siPositivo))
          else
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source1));
        end;
      end else
      // data
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'D' then
      begin
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2);
        end else
        if DmNFe_0000.QrNFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SSTZD' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2) +
                  dmkPF.TZD_UTC_FloatToSignedStr(Source3);
        end else
        begin
          if DmNFe_0000.QrNFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmNFe_0000.QrNFeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source1);
        end;
      end else
      // hora
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source1)
      else
      // texto
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'C' then
        Dest := Source1
      else
      // desconhecido
      begin
        Dest := Source1;
        Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Codigo, ID));
      if (Dest = '') and (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmNFe_0000.QrNFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
          if (Codigo = '78') and (ID = 'E17') and
          ((DmNFe_0000.QrNFECabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabAdest_CNPJ.Value) = '')) then
            Avisa := False;
          if Avisa then
            Geral.MB_Aviso(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True,
        'Inserindo valor de XML no banco de dados.' + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Codigo, ID, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
  'N�o foi poss�vel definir o valor', 3));
end;

function TFmNFeGeraXML_0310.DesmontaID_Inutilizacao(const Id: String; var cUF,
  Ano, emitCNPJ, Modelo, Serie, nNFIni, nNFFim: String): Boolean;
var
  K, N, Z: Integer;
begin
  K := Length(Id);
  if K in ([41,42,43]) then
  begin
    Z := 1 + K - 41;
    N := 3;
    cUF      := Copy(Id, 02, N);
    Ano      := Copy(Id, 02, N);
    emitCNPJ := Copy(Id, 14, N);
    Modelo   := Copy(Id, 02, N);
    Serie    := Copy(Id,  Z, N);
    nNFIni   := Copy(Id, 09, N);
    nNFFim   := Copy(Id, 09, N);
    Result := True;
  end else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmNFeGeraXML_0310.dmk_DefValDeEnum(var Campo: String; const Enum: TEnumeracao;
var Texto: String; const NomeCampo: string; const Tamanho: Integer): Boolean;
var
  Valor: String;
begin
  Valor := LerCampoA(Texto, NomeCampo, Tamanho);
  if Valor = sCampoNulo then
  begin
    Result := False;
    Exit;
  end;
  case Enum of
    enumProcessoEmissao: StrToProcEmi(Result, Valor);
    enumtpEmis: StrToTpEmis(Result, Valor);
    enumFinNFe: StrToFinNFe(Result, Valor);
    enumTpNF: StrTotpNF(Result, Valor);
    enumIndPag: StrToIndPag(Result, Valor);
    enumOrigemMercadoria: StrToOrig(Result, Valor);
    enumCSTICMS: StrToCSTICMS(Result, Valor);
    //
    else Result := False;
  end;
  if Result then
    Campo := Valor
  else
    Campo := '';
end;

function TFmNFeGeraXML_0310.dmk_DefValDeText(var Campo: String; var Texto:
String; const NomeCampo: string; const Tamanho : Integer = 0): Boolean;
var
  Valor: String;
begin
  Result := False;
  //
  Valor := LerCampoA(Texto, NomeCampo, Tamanho);
  if Valor = sCampoNulo then
    Exit
  else begin
    Campo := Valor;
    Result := True;
  end;
end;

function TFmNFeGeraXML_0310.FMT_IE(Valor: String): String;
begin
  if Uppercase(Valor) <> 'ISENTO' then
    Result := Geral.SoNumero_TT(Valor)
  else
    // Mudou na 3.10
    Result := Valor;
    //Result := '';
end;

procedure TFmNFeGeraXML_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeGeraXML_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeGeraXML_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeGeraXML_0310.GerarConsultaNFDest(Lote, Empresa: Integer;
  out PathLote: String; out XML_Lote: String; LaAviso1,
  LaAviso2: TLabel): Boolean;
begin
//
end;

function TFmNFeGeraXML_0310.GerarLoteDownloadNFeConfirmadas(Lote,
  Empresa: Integer; out PathLote, XML_Lote: String; LaAviso1,
  LaAviso2: TLabel): Boolean;
begin
  Result := True;
end;

function TFmNFeGeraXML_0310.GerarLoteDownloadNFeDestinadas(Lote,
  Empresa: Integer; out PathLote: String; out XML_Lote: String; LaAviso1,
  LaAviso2: TLabel): Boolean;
begin
  Result := True;
end;

function TFmNFeGeraXML_0310.GerarLoteEvento(Lote, Empresa: Integer;
  out PathLote: String; out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I: Integer;
  XML_STR: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeEveRCab.Close;
  DmNFe_0000.QrNFeEveRCab.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeEveRCab, Dmod.MyDB, 'TFmNFeGeraXML_0310.GerarLoteEvento()');
  //
  if DmNFe_0000.QrNFeEveRCab.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem eventos para serem enviados!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_EVE_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML de eventos', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_EVE_ENV_LOT_XML;

  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?><envEvento versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe"><idLote>' +
    Geral.FFN(Lote, 15)  + '</idLote>';

  DmNFe_0000.QrNFeEveRCab.First;
  while not DmNFe_0000.QrNFeEveRCab.Eof do
  begin
    case DmNFe_0000.QrNFeEveRCabtpEvento.Value of
      // Carta de corre��o
      NFe_CodEventoCCe: // 110110
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedCCe.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_CCE_XML;
      end;
      // Cancelamento de NFe
      NFe_CodEventoCan: // 110111
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedCan.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_CAN_XML;
      end;
      // Manifesta��o do destinat�rio
      NFe_CodEventoMDeConfirmacao, // = 210200;
      NFe_CodEventoMDeCiencia    , // = 210210;
      NFe_CodEventoMDeDesconhece , // = 210220;
      NFe_CodEventoMDeNaoRealizou: // = 210240;
      begin
        XMLAssinado_Dir := DmNFe_0000.QrFilialDirEvePedMDe.Value;
        if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
        XMLAssinado_Arq := DmNFe_0000.QrNFeEveRCabId.Value;
        XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_EVE_ENV_MDE_XML;
      end
      else
      begin
        XMLArq := '### ERRO ###';
        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' + sLineBreak  +
        'TFmNFeGeraXML_0310.GerarLoteEvento()');
        Result := False;
        Exit;
      end;
    end;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      if Pos('<Evento', MeuXMLAssinado) > 0 then
        buf := Copy(MeuXMLAssinado, Pos('<Evento', MeuXMLAssinado), Length(MeuXMLAssinado))
      else
        buf := Copy(MeuXMLAssinado, Pos('<evento', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      //Write(fArquivoTexto, buf);
      mTexto.Free;
    end else
    begin
      Geral.MB_Aviso('Arquivo n�o encontrado: ' + sLineBreak  + XMLArq);
      Exit;
    end;
    DmNFe_0000.QrNFeEveRCab.Next;
  end;
  XML_Lote := XML_Lote + '</envEvento>';
  //

{ TODO :      VER O QUE FAZER! }
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    //N�o pode ser aqui pois o aquivo do evento j� est� assinado e portanto n�o pode ser modificado
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;
//  XML_STR := XML_Lote;


  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

{
function TFmNFeGeraXML_0310.GerarLoteNFe(Lote, Empresa: Integer;
  out PathLote: String; out XML_Lote: String; LaAviso1, LaAviso2: TLabel): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I: Integer;
  XML_STR: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeLEnC.Close;
  DmNFe_0000.QrNFeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeLEnC, Dmod.MyDB, 'TFmNFeGeraXML_0310.GerarLoteNFe()');
  //
  if DmNFe_0000.QrNFeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem NF-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmNFe_0000.QrFilialDirNFeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+
  verEnviNFe_Versao + '">' +
  '<idLote>' + LoteStr + '</idLote>';
//repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmNFe_0000.QrNFeLEnC.First;
  while not DmNFe_0000.QrNFeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmNFe_0000.QrNFeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_NFE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      // Nesse ponto vc est� copiando o conteudo da tag "NFe" para o buffer...
      buf := Copy(MeuXMLAssinado, Pos('<NFe', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      //Write(fArquivoTexto, buf);
      mTexto.Free;
    end;
    DmNFe_0000.QrNFeLEnC.Next;
  end;

// Fecha o arquivo
  XML_Lote := XML_Lote + '</enviNFe>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;

  Write(fArquivoTexto, XML_STR);
  //Write(fArquivoTexto, '</enviNFe>');
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;
}

function TFmNFeGeraXML_0310.GerarLoteNFeNovo(Lote, Empresa: Integer;
  out PathLote, XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia: TXXeIndSinc): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I, IndSinc: Integer;
  XML_STR: String;
  VersaoDados: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeLEnC.Close;
  DmNFe_0000.QrNFeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeLEnC, Dmod.MyDB, 'TFmNFeGeraXML_0310.GerarLoteNFeNovo()');
  //
  if DmNFe_0000.QrNFeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem NF-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmNFe_0000.QrFilialDirNFeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);
(*==============================================================================
Schema XML: enviNFe_v2.00.xsd #
Campo Ele Pai Tipo Ocor. Tam. Dec. Descri��o/Observa��o
AP01 enviNFe Rai z - - - -  TAG raiz
AP02 versao A AP01 N 1-1 1-4 2 Vers�o do leiaute
AP03 idLote E AP01 N 1-1 1-15  Identificador de controle do envio do lote.
     N�mero sequencial auto-incremental, de controle correspondente ao
     identificador �nico do lote enviado. A responsabilidade de gerar e
     controlar esse n�mero � exclusiva do contribuinte.
--------------------------------------------------------------------------------
------------------ Novo 3.10 ---------------------------------------------------
AP03a indSinc E AP01 N 1-1 1
      0=N�o
      1=Empresa solicita processamento s�ncrono do Lote de NF-e (sem a gera��o
      de Recibo para consulta futura); Observa��o: O processamento s�ncrono do
      Lote corresponde a entrega da resposta do processamento das NF-e do Lote,
      sem a gera��o de um Recibo de Lote para consulta futura. A resposta de
      forma s�ncrona pela SEFAZ Autorizadora s� ocorrer� se: - a empresa
      solicitar e constar unicamente uma NF-e no Lote; - a SEFAZ Autorizadora
      implementar o processamento s�ncrono para a resposta do Lote de NF-e.
--------------------------------------------------------------------------------
AP04 NFe G AP01 xml 1-50 -  Conjunto de NF-e transmitidas (m�ximo de 50 NF-e),
     seguindo defini��o do Anexo I - Leiaute
==============================================================================*)
  case Sincronia of
    nisAssincrono: indSInc := 0;
    nisSincrono:   indSinc := 1;
    else Geral.MB_Erro('Sincronia n�o definida em "FmNFeGeraXML_0310.GerarLoteNFeNovo()"');
  end;
    begin

  end;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+
  VersaoDados + '">' +
  '<idLote>' + LoteStr + '</idLote>' +
  '<indSinc>' + Geral.FF0(indSinc) + '</indSinc>';
//repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmNFe_0000.QrNFeLEnC.First;
  while not DmNFe_0000.QrNFeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmNFe_0000.QrNFeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_NFE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      // Nesse ponto vc est� copiando o conteudo da tag "NFe" para o buffer...
      buf := Copy(MeuXMLAssinado, Pos('<NFe', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      //Write(fArquivoTexto, buf);
      mTexto.Free;
    end;
    DmNFe_0000.QrNFeLEnC.Next;
  end;
  XML_Lote := XML_Lote + '</enviNFe>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;
  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

function TFmNFeGeraXML_0310.GerarXMLdaNFe(FatID, FatNum, Empresa: Integer;
  const nfeID: String; GravaCampos: Integer): Boolean;
var
  K, h, j, nItem, Controle: Integer;
  Valor, IM: String;
  infAdProd: String;
  GeraNT2015_03: Boolean;
begin
  GeraNT2015_03 := UnNFe_PF.GeraGrupoNA_0310(DmNFe_0000.QrNFECabAide_dEmi.Value);
  //
  Result := False;
  FGravaCampo := GravaCampos;
  DmNFe_0000.ReopenNFeLayI();
  //P�g. 108/232  Manual_Integra��o_Contribuinte_vers�o_4.01-NT2009.006.pdf
   //    A - Dados da Nota Fiscal eletr�nica
   //    '1', 'A01' = Grupo das informa��es da NFe
(* Informa��es da TAG InfNFe... *)
  // Vers�o do leiaute
  if Def('2', 'A02', DmNFe_0000.QrNFECabAversao.Value, Valor) then
    cXML.InfNFe.Versao := Valor;
  //if Def('3', 'A03', nfeID, Valor) then
  cXML.InfNFe.Id := nfeID;
  //if Def('4', 'A04', N�o preencher, Valor) then
  // O contribuinte n�o deve preencher.
  //
  // B - Identifica��o da Nota Fiscal eletr�nica
(* Informa��es da TAG IDE... *)
  //if Def('5', 'B01', GRUPO NFe
  // C�digo da UF do emitente do Documento Fiscal. Utilizar a Tabela do IBGE
  if Def('6', 'B02', DmNFe_0000.QrNFECabAide_cUF.Value, Valor) then
    cXML.InfNFe.Ide.CUF := Valor;
  //C�digo num�rico que comp�e a Chave de Acesso. N�mero aleat�rio gerado pelo emitente para cada NF-e
  if Def('7', 'B03', DmNFe_0000.QrNFECabAide_cNF.Value, Valor) then
    cXML.InfNFe.Ide.CNF := Valor;
  //Descri��o da Natureza da Opera��o
  if Def('8', 'B04', DmNFe_0000.QrNFECabAide_natOp.Value, Valor) then
    cXML.InfNFe.Ide.NatOp := Valor;
  //Indicador da forma de pagamento:
    // 0 � pagamento � vista;
    // 1 � pagamento � prazo;
    // 2 � outros.
  if Def('9', 'B05', DmNFe_0000.QrNFECabAide_indPag.Value, Valor) then
    cXML.InfNFe.Ide.IndPag := Valor;//'0'; (* Zero � Aceitav�l como valor... *)
  //C�digo do modelo do Documento Fiscal. Utilizar 55 para identifica��o da NF-e, emitida em substitui��o ao modelo 1 e 1A.
  if Def('10', 'B06', DmNFe_0000.QrNFECabAide_mod.Value, Valor) then
    cXML.InfNFe.Ide.Mod_ := Valor;//'55';
  //S�rie do Documento Fiscal
  if Def('11', 'B07', DmNFe_0000.QrNFECabAide_serie.Value, Valor) then
    cXML.InfNFe.Ide.Serie := Valor;
  //N�mero do Documento Fiscal
  if Def('12', 'B08', DmNFe_0000.QrNFECabAide_nNF.Value, Valor) then
    cXML.InfNFe.Ide.NNF := Valor;


  // 03.2 Data e Hora de Emiss�o e Outros Hor�rios Alterado o campo de Data de
  // Emiss�o para representar tamb�m a Hora de Emiss�o, no formato UTC.
  // Este tipo de representa��o de dados j� � utilizado atualmente no
  // Web Service de Eventos da NF-e e � tecnicamente adequado para a
  // representa��o do hor�rio para um Pa�s com dimens�es continentais como
  // o Brasil. Todos os demais campos com hor�rio foram migrados para este
  // tipo de dado, inclusive os hor�rios que constam nas mensagens de resposta
  // fornecidas pelas SEFAZ. Nesta nova vers�o do leiaute, ser�o aceitos os
  // hor�rios de qualquer regi�o do mundo (faixa de hor�rio UTC de -11 a +12)
  // e n�o apenas as faixas de hor�rio do Brasil.
  // # ID Campo Descri��o Ele Pai Tipo Ocor. Tam. Observa��o
  // 13 B09 dhEmi Data e Hora de emiss�o do Documento Fiscal E B01 D 1-1
  //        Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal Coordinated Time)
  // 14 B10 dhSaiEnt Data e Hora de Sa�da da Mercadoria/Produto.
  //        No caso da NF de entrada, esta � a Data e Hora de entrada.
  //        E B01 D 0-1  Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal
  //        Coordinated Time)
  // 4a B10a hSaiEnt *** Eliminado ***
  //        29C B28 dhCont Data e Hora da entrada em conting�ncia
  //        E B01 D 0-1  Formato: �AAAA-MM-DDThh:mm:ssTZD� (UTC - Universal Coordinated Time)
  // Exemplo: no formato UTC para os campos de Data-Hora, "TZD" pode ser -02:00
  // (Fernando de Noronha), -03:00 (Bras�lia) ou -04:00 (Manaus), no hor�rio de
  // ver�o ser�o -01:00, -02:00 e -03:00. Exemplo: "2010-08-19T13:00:15-03:00".

  //Data e hora no formato UTC (Universal Coordinated Time): AAAA-MM-DDThh:mm:ssTZD
  if Def_UTC('13', 'B09',
  DmNFe_0000.QrNFECabAide_dEmi.Value,
  DmNFe_0000.QrNFECabAide_hEmi.Value,
  DmNFe_0000.QrNFECabAide_dhEmiTZD.Value, Valor) then
    cXML.InfNFe.Ide.DhEmi := Valor;

  //Data de sa�da ou de entrada da mercadoria / produto (AAAA-MM-DD)
  (* Opcional... *)
  if Def_UTC('14', 'B10',
  DmNFe_0000.QrNFECabAide_dSaiEnt.Value,
  DmNFe_0000.QrNFECabAide_hSaiEnt.Value,
  DmNFe_0000.QrNFECabAide_dhSaiEntTZD.Value, Valor) then
    cXML.InfNFe.Ide.DhSaiEnt := Valor;

(* Eliminado na NFe 3.10
  //Hora de sa�da ou de entrada da mercadoria / produto (HH:MM:SS) NFe 2.00
  if Def('14a', 'B10a', DmNFe_0000.QrNFECabAide_hSaiEnt.Value, Valor) then
    cXML.InfNFe.Ide.HSaiEnt := Valor;//
*)

  //Tipo do Documento Fiscal (0 - entrada; 1 - sa�da)
  if Def('15', 'B11', DmNFe_0000.QrNFECabAide_tpNF.Value, Valor) then
    if DmNFe_0000.QrNFECabAide_tpNF.Value in ([0,1]) then
      cXML.InfNFe.Ide.TpNF := Valor
    else begin
      Geral.MB_Aviso('Tipo de emiss�o inv�lido: ' + FormatFloat('0', DmNFe_0000.QrNFECabAide_tpNF.Value) +
      sLineBreak  + 'Valor deve ser (0-entrada/1-sa�da)');
      Exit;
    end;

  //Identificador do local de destino da operacao  NFe 3.10
  if Def('15a', 'B11a', DmNFe_0000.QrNFECabAide_idDest.Value, Valor) then
    cXML.InfNFe.Ide.idDest := Valor;

  //C�digo do Munic�pio de Ocorr�ncia do Fato Gerador (utilizar a tabela do IBGE)
  if Def('16', 'B12', DmNFe_0000.QrNFECabAide_cMunFG.Value, Valor) then
    cXML.InfNFe.Ide.CMunFG := Valor;











  if Def('25', 'B21', DmNFe_0000.QrNFECabAide_tpImp.Value, Valor) then
    cXML.InfNFe.Ide.TpImp := Valor;
  if Def('26', 'B22', DmNFe_0000.QrNFECabAide_tpEmis.Value, Valor) then
    cXML.InfNFe.Ide.TpEmis := Valor;
  if Def('27', 'B23', DmNFe_0000.QrNFECabAide_cDV.Value, Valor) then
    cXML.InfNFe.Ide.CDV := Valor;
  if Def('28', 'B24', DmNFe_0000.QrNFECabAide_tpAmb.Value, Valor) then
    cXML.InfNFe.Ide.TpAmb := Valor;
  if Def('29', 'B25', DmNFe_0000.QrNFECabAide_finNFe.Value, Valor) then
    cXML.InfNFe.Ide.FinNFe := Valor;
  if Def('29.1', 'B25a', DmNFe_0000.QrNFECabAide_indFinal.Value, Valor) then
    cXML.InfNFe.Ide.indFinal := Valor;
  if Def('29.2', 'B25b', DmNFe_0000.QrNFECabAide_indPres.Value, Valor) then
    cXML.InfNFe.Ide.indPres := Valor;
  if Def('29a', 'B26', DmNFe_0000.QrNFECabAide_procEmi.Value, Valor) then
    cXML.InfNFe.Ide.ProcEmi := Valor;
  if Def('29b', 'B27', DmNFe_0000.QrNFECabAide_verProc.Value, Valor) then
    cXML.InfNFe.Ide.VerProc := Valor;
  if Def_UTC('29c', 'B28',
  DmNFe_0000.QrNFECabAide_dhCont.Value,
  0,
  DmNFe_0000.QrNFECabAide_dhContTZD.Value, Valor) then
    cXML.InfNFe.Ide.dhCont := Valor;
  if Def('29d', 'B29', DmNFe_0000.QrNFECabAide_xJust.Value, Valor) then
    cXML.InfNFe.Ide.xJust := Valor;










  (* Informa��es da TAG Notas Fiscais Referenciadas... se Houver... *)
(*
Utilizar esta TAG para referenciar
uma Nota Fiscal Eletr�nica emitida
anteriormente, vinculada a NF-e
atual.
Esta informa��o ser� utilizada nas
hip�teses previstas na legisla��o.
(Ex.: Devolu��o de Mercadorias,
Substitui��o de NF cancelada,
Complementa��o de NF, etc.).
*)
  (* Essa TAG � Opcional e s� aparece no XML se Houver NFes a serem referenciadas na Nota...*)
      // '29x.1', 'BA01' = Informa��o das NF/NF-e Referenciadas
  DmNFe_0000.ReopenNFeCabB(FatID, FatNum, Empresa);
  if DmNFe_0000.QrNFeCabB.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFeCabB.Eof do
    begin
      cRefLista := cXML.InfNFe.Ide.NFref.Add;
      //
      case DmNFe_0000.QrNFECabBQualNFref.Value of
        0:
        begin
          if Def('29x.2', 'BA02', DmNFe_0000.QrNFECabBrefNFe.Value, Valor) then
            cRefLista.RefNFe := Valor;
        end;
        1:
        begin
          //    '29x.3', 'BA03' = informa��o das NF referenciadas (NFs normais > A1 etc)
          if Def('29x.4', 'BA04', DmNFe_0000.QrNFECabBrefNF_cUF.Value, Valor) then
            cRefLista.RefNF.CUF := Valor;
          if Def('29x.5', 'BA05', DmNFe_0000.QrNFECabBrefNF_AAMM.Value, Valor) then
            cRefLista.RefNF.AAMM := Valor;
          if Def('29x.6', 'BA06', DmNFe_0000.QrNFECabBrefNF_CNPJ.Value, Valor) then
            cRefLista.RefNF.CNPJ := Valor;
          if Def('29x.7', 'BA07', DmNFe_0000.QrNFECabBrefNF_mod.Value, Valor) then
            cRefLista.RefNF.Mod_ := Valor;
          if Def('29x.8', 'BA08', DmNFe_0000.QrNFECabBrefNF_serie.Value, Valor) then
            cRefLista.RefNF.Serie := Valor;
          if Def('29x.9', 'BA09', DmNFe_0000.QrNFECabBrefNF_nNF.Value, Valor) then
            cRefLista.RefNF.NNF := Valor;
        end;
        2:
        begin
          // Falta implementar os dados de origem
          // NFe 2.00
          //if Def('29x.10', 'BA10', // Produtor Rural
          if Def('29x.11', 'BA11', DmNFe_0000.QrNFECabBrefNFP_cUF.Value, Valor) then
            cRefLista.RefNFP.cUF := Valor;
          if Def('29x.12', 'BA12', DmNFe_0000.QrNFECabBrefNFP_AAMM.Value, Valor) then
            cRefLista.RefNFP.AAMM := Valor;
          if Geral.SoNumero_TT(DmNFe_0000.QrNFECabBrefNFP_CNPJ.Value) <> '' then
          begin
            if Def('29x.13', 'BA13', DmNFe_0000.QrNFECabBrefNFP_CNPJ.Value, Valor) then
              cRefLista.RefNFP.CNPJ := Valor;
          end else begin
            if Def('29x.14', 'BA14', DmNFe_0000.QrNFECabBrefNFP_CPF.Value, Valor) then
              cRefLista.RefNFP.CPF := Valor;
          end;
          if Def('29x.15', 'BA15', DmNFe_0000.QrNFECabBrefNFP_IE.Value, Valor) then
            cRefLista.RefNFP.IE := Valor;
          if Def('29x.16', 'BA16', DmNFe_0000.QrNFECabBrefNFP_mod.Value, Valor) then
            cRefLista.RefNFP.mod_ := Valor;
          if Def('29x.17', 'BA17', DmNFe_0000.QrNFECabBrefNFP_serie.Value, Valor) then
            cRefLista.RefNFP.serie := Valor;
          if Def('29x.18', 'BA18', DmNFe_0000.QrNFECabBrefNFP_nNF.Value, Valor) then
            cRefLista.RefNFP.nNF := Valor;
          // CTe
          if Def('29x.19', 'BA19', DmNFe_0000.QrNFECabBrefCTe.Value, Valor) then
            cRefLista.RefCTe := Valor;
        end;
        3:
        begin
          //if Def('29x.20', 'BA20', ECF
          if Def('29x.21', 'BA21', DmNFe_0000.QrNFECabBrefECF_mod.Value, Valor) then
            cRefLista.RefECF.mod_ := Valor;
          if Def('29x.22', 'BA22', DmNFe_0000.QrNFECabBrefECF_nECF.Value, Valor) then
            cRefLista.RefECF.nECF := Valor;
          if Def('29x.23', 'BA23', DmNFe_0000.QrNFECabBrefECF_nCOO.Value, Valor) then
            cRefLista.RefECF.nCOO := Valor;
        end;
      end;
      //
      DmNFe_0000.QrNFeCabB.Next;
    end;
  end;









  (* C - Informa��es da TAG EMIT... *)
  // '30', 'C01'  Grupo de identifica��o do emitente da NF-e
  if Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value) <> '' then
  begin
    if Def('31', 'C02', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value), Valor) then
    cXML.InfNFe.Emit.CNPJ := Valor;
  end else begin
    if Def('31a', 'C02a', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CPF.Value), Valor) then
    cXML.InfNFe.Emit.CPF := Valor;
  end;
  if Def('32', 'C03', DmNFe_0000.QrNFECabAemit_xNome.Value, Valor) then
    cXML.InfNFe.Emit.XNome := Valor;
  if Def('33', 'C04', DmNFe_0000.QrNFECabAemit_xFant.Value, Valor) then
    cXML.InfNFe.Emit.XFant := Valor;

  (* TAG EnderEMIT... *)
  //     '34', 'C05'  = Grupo de endere�o do emitente
  if Def('35', 'C06', DmNFe_0000.QrNFECabAemit_xLgr.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XLgr := Valor;
  if Def('36', 'C07', DmNFe_0000.QrNFECabAemit_nro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Nro := Valor;
  if Def('37', 'C08', DmNFe_0000.QrNFECabAemit_xCpl.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XCpl := Valor;
  if Def('38', 'C09', DmNFe_0000.QrNFECabAemit_xBairro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XBairro := Valor;
  if Def('39', 'C10', DmNFe_0000.QrNFECabAemit_cMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.CMun := '9999999'
    else
      cXML.InfNFe.Emit.EnderEmit.CMun := Valor;
  end;
  if Def('40', 'C11', DmNFe_0000.QrNFECabAemit_xMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.XMun := 'EXTERIOR'
    else
      cXML.InfNFe.Emit.EnderEmit.XMun := Valor;
  end;
  if Def('41', 'C12', DmNFe_0000.QrNFECabAemit_UF.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.UF := 'EX'
    else
      cXML.InfNFe.Emit.EnderEmit.UF := Valor;
  end;
  if Def('42', 'C13', DmNFe_0000.QrNFECabAemit_CEP.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CEP := Valor;
  if Def('43', 'C14', DmNFe_0000.QrNFECabAemit_cPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CPais := Valor;
  if Def('44', 'C15', DmNFe_0000.QrNFECabAemit_xPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XPais := Valor;
  if Def('45', 'C16', DmNFe_0000.QrNFECabAemit_fone.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Fone := Valor;
  if Def('46', 'C17', DmNFe_0000.QrNFECabAemit_IE.Value, Valor) then
    cXML.InfNFe.Emit.IE := FMT_IE(Valor);
  if Def('47', 'C18', DmNFe_0000.QrNFECabAemit_IEST.Value, Valor) then
    cXML.InfNFe.Emit.IEST := FMT_IE(Valor);
  IM := DmNFe_0000.QrNFECabAemit_IM.Value;
  if IM = '0' then IM := '';
  if Def('48', 'C19', IM, Valor) then
    cXML.InfNFe.Emit.IM := Valor;
  if Def('49', 'C20', DmNFe_0000.QrNFECabAemit_CNAE.Value, Valor) then
    cXML.InfNFe.Emit.CNAE := Valor;
  if Def('49a', 'C21', DmNFe_0000.QrNFECabAemit_CRT.Value, Valor) then
    cXML.InfNFe.Emit.CRT := Valor;
  //
  //
  // D Itentifica��o do Fisco emitente da NF-e
  // N�o preencher (� do fisco)
  // vai de '50', 'D01'
  // at�    '61', 'D12'

(* E - Informa��es da TAG DEST... *)
  //       '62', 'E01' - Grupo de identifica��o do destinat�rio da NF-e

///////////////////////////////////////////////////////////////////////////////////////////
///  Nota T�cnica 2013/005                                                               //
///  Altera��o Leiaute da NF-e                                                           //
///  - Vers�o Nacional 2014 -                                                            //
///  Vers�o 1.10                                                                         //
///  Outubro 2014                                                                        //
///                                                                                      //
(*                                                                                       //
P�g. 140 / 141                                                                           //
DOCUMENTACIONAL D01. Orienta��o para as Empresas                                         //
D01.1 Preenchimento dos campos da NF-e na Vers�o 3.10                                    //
A. Dados do Destinat�rio                                                                 //
  Veja as mudan�as no preenchimento da NF-e, conforme quadro que segue:                  //
-------------------------------------------------------------------------------------------
Situa��o             |Leiaute vers�o "2.00"          |Leiaute vers�o "3.10"               |
-------------------------------------------------------------------------------------------
Opera��o com Exterior|- dest/CNPJ com valor nulo     |- Informar �dest/idEstrangeiro�,    |
                     |                               | podendo ser comvalor nulo;         |
-------------------------------------------------------------------------------------------
Venda interna para   |- dest/enderDest/UF = �EX�;    |- dest/enderDest/UF = �EX�;         |
Estrangeiro          |- dest/CNPJ = Nulo;            |- dest/idEstrangeiro, pode ser nulo;|
                     |- CFOP dos itens inicia com �5�|- CFOP dos itens inicia com �5�;    |
-------------------------------------------------------------------------------------------
Nota: Com a mudan�a no leiaute, agora o campo �dest/CNPJ� n�o pode ser mais              //
informado com valor Nulo, em nenhuma situa��o.                                           //
*)                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
///
  if DmNFe_0000.QrNFECabAEstrangDef.Value = 1 then
  begin
    if Def('64a', 'E03a', Geral.SoNumero_TT(
    DmNFe_0000.QrNFECabAdest_idEstrangeiro.Value), Valor) then
       cXML.InfNFe.Dest.idEstrangeiro := Valor
    else
      cXML.InfNFe.Dest.idEstrangeiro := ''
  end else
  begin
    if (DmNFe_0000.QrNFECabAdest_CNPJ.Value <> '') then
    begin
      if Def('63', 'E02', Geral.SoNumero_TT(
      DmNFe_0000.QrNFECabAdest_CNPJ.Value), Valor) then
        cXML.InfNFe.Dest.CNPJ := Valor;
    end else if DmNFe_0000.QrNFECabAdest_CPF.Value <> '' then
    begin
      if Def('64', 'E03', Geral.SoNumero_TT(
      DmNFe_0000.QrNFECabAdest_CPF.Value), Valor) then
        cXML.InfNFe.Dest.CPF := Valor;
    end else
    begin
      //  Erro!
      Geral.MB_Erro(
      'CNPJ ou CPF do remetente/destinat�rio n�o pode ser vazio para venda interna a partir da NFe 3.10!');
      //cXML.InfNFe.Dest.CNPJ := '';
    end;
  end;
  if Def('65', 'E04', DmNFe_0000.QrNFECabAdest_xNome.Value, Valor) then
      cXML.InfNFe.Dest.XNome := Valor;
  (* TAG EnderDEST... *)
  //     '66', 'E05' - Grupo de endere�o do destinat�rio da NF-e
  if Def('67', 'E06', DmNFe_0000.QrNFECabAdest_xLgr.Value, Valor) then
   cXML.InfNFe.Dest.EnderDest.XLgr    :=  Valor;
  if Def('68', 'E07', DmNFe_0000.QrNFECabAdest_nro.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.Nro     :=  Valor     ;
  if Def('69', 'E08', DmNFe_0000.QrNFECabAdest_xCpl.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XCpl    :=  Valor    ;
  if Def('70', 'E09', DmNFe_0000.QrNFECabAdest_xBairro.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XBairro :=  Valor ;
  if Def('71', 'E10', DmNFe_0000.QrNFECabAdest_cMun.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.CMun    :=  '9999999'
    else
      cXML.InfNFe.Dest.EnderDest.CMun    :=  Valor    ;
  end;
  if Def('72', 'E11', DmNFe_0000.QrNFECabAdest_xMun.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.XMun    :=  'EXTERIOR'
    else
      cXML.InfNFe.Dest.EnderDest.XMun    :=  Valor    ;
  end;
  if Def('73', 'E12', DmNFe_0000.QrNFECabAdest_UF.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.UF      :=  'EX'
    else
      cXML.InfNFe.Dest.EnderDest.UF      :=  Valor      ;
  end;
  if Def('74', 'E13', DmNFe_0000.QrNFECabAdest_CEP.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.CEP     :=  Valor     ;
  if Def('75', 'E14', DmNFe_0000.QrNFECabAdest_cPais.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.CPais   :=  Valor   ;
  if Def('76', 'E15', DmNFe_0000.QrNFECabAdest_xPais.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XPais   :=  Valor   ;
  if Def('77', 'E16', DmNFe_0000.QrNFECabAdest_fone.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.Fone    :=  Valor    ;
  if Def('77a', 'E16a', DmNFe_0000.QrNFECabAdest_indIEDest.Value, Valor) then
    cXML.InfNFe.Dest.indIEDest    :=  Valor    ;
  case  DmNFe_0000.QrNFECabAdest_indIEDest.Value of
    1:
    begin
      if Def('78', 'E17', DmNFe_0000.QrNFECabAdest_IE.Value, Valor) then
      begin
        if OutroPais_dest() then
          //cXML.InfNFe.Dest.IE              :=  ''
          Valor :=  '';
        if Valor <> '' then
          cXML.InfNFe.Dest.IE              :=  FMT_IE(Valor)      ;
      end;
    end;
    2, 9:
      ;//cXML.InfNFe.Dest.IE := '00'; //Erro no Paran�
    else
      Geral.MB_Aviso('"indIEDest" n�o implementado!');
  end;
  if Def('79', 'E18', DmNFe_0000.QrNFECabAdest_ISUF.Value, Valor) then
    cXML.InfNFe.Dest.ISUF := Valor;
  if Def('79.1', 'E18a', DmNFe_0000.QrNFECabAdest_IM.Value, Valor) then
    cXML.InfNFe.Dest.IM := Valor;
  if Def('79a', 'E19', DmNFe_0000.QrNFECabAdest_email.Value, Valor) then
    cXML.InfNFe.Dest.email := Valor;

(* F - Informa��es da TAG RETIRADA... se Houver *)

(*
  DmNFe_0000.QrNFECabF.Close;
  DmNFe_0000.QrNFECabF.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabF.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabF.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabF. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabF(FatID, FatNum, Empresa);
  //
  (*Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de RETIRADA...*)
  if DmNFe_0000.QrNFECabF.RecordCount > 0 then
  begin
    //     '80', 'F01' = Identifica��o do local de retirada
    if Geral.SoNumero_TT(DmNFe_0000.QrNFECabFretirada_CNPJ.Value) <> '' then
    begin
      if Def('81', 'F02', DmNFe_0000.QrNFECabFretirada_CNPJ.Value, Valor) then
        cXML.InfNFe.Retirada.CNPJ := Valor;
    end else begin
      if Def('81a', 'F02a', DmNFe_0000.QrNFECabFretirada_CPF.Value, Valor) then
        cXML.InfNFe.Retirada.CPF := Valor;
    end;
    if Def('82', 'F03', DmNFe_0000.QrNFECabFretirada_xLgr.Value, Valor) then
      cXML.InfNFe.Retirada.XLgr := Valor;
    if Def('83', 'F04', DmNFe_0000.QrNFECabFretirada_Nro.Value, Valor) then
      cXML.InfNFe.Retirada.Nro := Valor;
    if Def('84', 'F05', DmNFe_0000.QrNFECabFretirada_xCpl.Value, Valor) then
      cXML.InfNFe.Retirada.XCpl := Valor;
    if Def('85', 'F06', DmNFe_0000.QrNFECabFretirada_xBairro.Value, Valor) then
      cXML.InfNFe.Retirada.XBairro := Valor;
    if Def('86', 'F07', DmNFe_0000.QrNFECabFretirada_cMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.CMun := '9999999'
      else
        cXML.InfNFe.Retirada.CMun := Valor;
    end;
    if Def('87', 'F08', DmNFe_0000.QrNFECabFretirada_xMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Retirada.XMun := Valor;
    end;
    if Def('88', 'F09', DmNFe_0000.QrNFECabFretirada_UF.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.UF := 'EX'
      else
        cXML.InfNFe.Retirada.UF := Valor;
    end;
  end;

  //
(* G - Informa��es da TAG ENTREGA... se Houver *)
(*
  DmNFe_0000.QrNFECabG.Close;
  DmNFe_0000.QrNFECabG.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabG.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabG.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabG. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabG(FatID, FatNum, Empresa);
  //
  //
  (*Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de ENTREGA...*)
  if DmNFe_0000.QrNFECabG.RecordCount > 0 then
  begin
    //     '89', 'G01' = Identifica��o do local de entrega
    if DmNFe_0000.QrNFECabGentrega_CNPJ.Value <> '' then
    begin
      if Def('90', 'G02', DmNFe_0000.QrNFECabGentrega_CNPJ.Value, Valor) then
        cXML.InfNFe.Entrega.CNPJ := Valor;
    end else begin
      if Def('90a', 'G02a', DmNFe_0000.QrNFECabGentrega_CPF.Value, Valor) then
        cXML.InfNFe.Entrega.CPF := Valor;
    end;
    if Def('91', 'G03', DmNFe_0000.QrNFECabGentrega_xLgr.Value, Valor) then
      cXML.InfNFe.Entrega.XLgr := Valor;
    if Def('92', 'G04', DmNFe_0000.QrNFECabGentrega_Nro.Value, Valor) then
      cXML.InfNFe.Entrega.Nro := Valor;
    if Def('93', 'G05', DmNFe_0000.QrNFECabGentrega_xCpl.Value, Valor) then
      cXML.InfNFe.Entrega.XCpl := Valor;
    if Def('94', 'G06', DmNFe_0000.QrNFECabGentrega_xBairro.Value, Valor) then
      cXML.InfNFe.Entrega.XBairro := Valor;
    if Def('95', 'G07', DmNFe_0000.QrNFECabGentrega_cMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.CMun := '9999999'
      else
        cXML.InfNFe.Entrega.CMun := Valor;
    end;
    if Def('96', 'G08', DmNFe_0000.QrNFECabGentrega_xMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Entrega.XMun := Valor;
    end;
    if Def('97', 'G09', DmNFe_0000.QrNFECabGentrega_UF.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.UF := 'EX'
      else
        cXML.InfNFe.Entrega.UF := Valor;
    end;
  end;

(* GA - Informa��es da TAG autXML... *)
(*
  DmNFe_0000.QrNFECabGA.Close;
  DmNFe_0000.QrNFECabGA.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabGA.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabGA.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabGA. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabGA(FatID, FatNum, Empresa);
  if DmNFe_0000.QrNFECabGA.RecordCount > 0 then
  begin
    DmNFe_0000.QrNFECabGA.First;
    while not DmNFe_0000.QrNFECabGA.Eof do
    begin
      if (DmNFe_0000.QrNFeCabGAautXML_CNPJ.Value <> '')
      or (DmNFe_0000.QrNFeCabGAautXML_CPF.Value <> '') then
      begin
        cAutXML := cXML.InfNFe.autXML.Add;
        //
        if (DmNFe_0000.QrNFeCabGATipo.Value = 0) then
        begin
          if Def('97a.2', 'GA02', DmNFe_0000.QrNFECabGAautXML_CNPJ.Value, Valor) then
            cAutXML.CNPJ := Valor
          else
            Geral.MB_Aviso('Item ' + Geral.FF0(DmNFe_0000.QrNFECabGA.RecNo) +
            ' da lista de "Autoriza��o para obter o XML" com CNPJ indefinido!');
        end else
        begin
          if (DmNFe_0000.QrNFeCabGAautXML_CPF.Value <> '') then
            if Def('97a.3', 'GA03', DmNFe_0000.QrNFeCabGAautXML_CPF.Value, Valor) then
              cAutXML.CPF := Valor
          else
            Geral.MB_Aviso('Item ' + Geral.FF0(DmNFe_0000.QrNFECabGA.RecNo) +
            ' da lista de "Autoriza��o para obter o XML" com CPF indefinido!');
        end;
      end;
      DmNFe_0000.QrNFECabGA.Next;
    end;
  end;

(* H - Informa��es da TAG DET... *)
(*
  DmNFe_0000.QrNFEItsI.Close;
  DmNFe_0000.QrNFEItsI.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFEItsI.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFEItsI.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFEItsI. O p e n ;
*)
  DmNFe_0000.ReopenNFeItsI(FatID, FatNum, Empresa);
  //
  while not DmNFe_0000.QrNFEItsI.Eof do
  begin
    nItem := DmNFe_0000.QrNFEItsInItem.Value;
    //j := 1;  // M�ximo 990 itens
        // '98', 'H01' = Grupo de detalhamento de produtos e servi�os da NF-e
    cDetLista := cXML.InfNFe.Det.Add;
    if Def('99', 'H02', DmNFe_0000.QrNFEItsInItem.Value, Valor) then
      cDetLista.NItem := Valor;
        // '100', 'I01' = Grupo de detalhamento de produtos e servi�os da NF-e
    if Def('101', 'I02', DmNFe_0000.QrNFEItsIprod_cProd.Value, Valor) then
      cDetLista.Prod.CProd := Valor;
    if Def('102', 'I03', DmNFe_0000.QrNFEItsIprod_cEAN.Value, Valor) then
      cDetLista.Prod.CEAN := Valor;//''; (* Se n�o tiver EAN tem que colocar em Branco...*)
    if Def('103', 'I04', DmNFe_0000.QrNFEItsIprod_xProd.Value, Valor) then
      cDetLista.Prod.XProd := Valor;
    if Def('104', 'I05', DmNFe_0000.QrNFEItsIprod_NCM.Value, Valor) then
      cDetLista.Prod.NCM := Valor;
    //
    // NFe 3.10
    DmNFe_0000.ReopenNFeItsINVE(FatID, FatNum, Empresa, nItem);
    if DmNFe_0000.QrNFEItsINVE.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFEItsINVE.Eof do
      begin
        Controle := DmNFe_0000.QrNFeItsINVEControle.Value;
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFEItsIDI.RecNo;
        //
        //cXML.InfNFe.Det.Items[h].Prod.NVE.Add;
        if Def('104a', 'I05a', DmNFe_0000.QrNFeItsINVENVE.Value, Valor) then
          cXML.InfNFe.Det.Items[h].Prod.NVE.Add(Valor);
        //
        DmNFe_0000.QrNFEItsINVE.Next;
      end;
    end;
    // Fim NFe 3.10
    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    if GeraNT2015_03 then
    begin
      if Def('104d', 'I05c', DmNFe_0000.QrNFEItsIprod_CEST.Value, Valor) then
        cDetLista.Prod.CEST := Valor;
    end;
    //Fim PL 008h
    if Def('105', 'I06', DmNFe_0000.QrNFEItsIprod_EXTIPI.Value, Valor) then
    begin
      if Valor <> '0' then
        cDetLista.Prod.EXTIPI := Valor;
    end;
    (* foi tirado da NFe 2.00
    if Def('106', 'I07', DmNFe_0000.QrNFEItsIprod_genero.Value, Valor) then
      cDetLista.Prod.Genero := Valor;
    *)
    if Def('107', 'I08', DmNFe_0000.QrNFEItsIprod_CFOP.Value, Valor) then
      cDetLista.Prod.CFOP := Valor;
    if Def('108', 'I09', DmNFe_0000.QrNFEItsIprod_uCom.Value, Valor) then
      cDetLista.Prod.UCom := Valor;
    if Def('109', 'I10', DmNFe_0000.QrNFEItsIprod_qCom.Value, Valor) then
      cDetLista.Prod.QCom := Valor;
    if Def('109a', 'I10a', DmNFe_0000.QrNFEItsIprod_vUnCom.Value, Valor) then
      cDetLista.Prod.VUnCom := Valor;
    if Def('110', 'I11', DmNFe_0000.QrNFEItsIprod_vProd.Value, Valor) then
      cDetLista.Prod.VProd := Valor;
    if Def('111', 'I12', DmNFe_0000.QrNFEItsIprod_cEANTrib.Value, Valor) then
      cDetLista.Prod.CEANTrib := Valor;//''; (* Se n�o tiver EAN Trib. tem que colocar em Branco...*)
    if Def('112', 'I13', DmNFe_0000.QrNFEItsIprod_uTrib.Value, Valor) then
      cDetLista.Prod.UTrib := Valor;
    if Def('113', 'I14', DmNFe_0000.QrNFEItsIprod_qTrib.Value, Valor) then
      cDetLista.Prod.QTrib := Valor;
    if Def('113a', 'I14a', DmNFe_0000.QrNFEItsIprod_vUnTrib.Value, Valor) then
      cDetLista.Prod.VUnTrib := Valor;
    if Def('114', 'I15', DmNFe_0000.QrNFEItsIprod_vFrete.Value, Valor) then
      cDetLista.Prod.VFrete := Valor;
    if Def('115', 'I16', DmNFe_0000.QrNFEItsIprod_vSeg.Value, Valor) then
      cDetLista.Prod.VSeg := Valor;
    if Def('116', 'I17', DmNFe_0000.QrNFEItsIprod_vDesc.Value, Valor) then
      cDetLista.Prod.VDesc := Valor;
    if Def('116a', 'I17a', DmNFe_0000.QrNFEItsIprod_vOutro.Value, Valor) then
      cDetLista.Prod.VOutro := Valor;
    if Def('116b', 'I17b', DmNFe_0000.QrNFEItsIprod_indTot.Value, Valor) then
      cDetLista.Prod.indTot := Valor;

    (*     '117', 'I18' = Informa��es da TAG DI... Opcional *)
(*
    DmNFe_0000.QrNFeItsIDI.Close;
    DmNFe_0000.QrNFeItsIDI.Params[00].AsInteger := DmNFe_0000.QrNFeItsIFatID.Value;
    DmNFe_0000.QrNFeItsIDI.Params[01].AsInteger := DmNFe_0000.QrNFeItsIFatNum.Value;
    DmNFe_0000.QrNFeItsIDI.Params[02].AsInteger := DmNFe_0000.QrNFeItsIEmpresa.Value;
    DmNFe_0000.QrNFeItsIDI.Params[03].AsInteger := DmNFe_0000.QrNFeItsInItem.Value;
    DmNFe_0000.QrNFeItsIDI. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsIDI(FatID, FatNum, Empresa, nItem);
    //
    if DmNFe_0000.QrNFEItsIDI.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFEItsIDI.Eof do
      begin
        Controle := DmNFe_0000.QrNFeItsIDIControle.Value;
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFEItsIDI.RecNo;
        //
        cDILista := cXML.InfNFe.Det.Items[h].Prod.DI.Add;
        if Def('118', 'I19', DmNFe_0000.QrNFEItsIDIDI_nDI.Value, Valor) then
          cDILista.NDI := Valor;
        if Def('119', 'I20', DmNFe_0000.QrNFEItsIDIDI_dDI.Value, Valor) then
          cDILista.DDI := Valor;
        if Def('120', 'I21', DmNFe_0000.QrNfeItsIDIDI_xLocDesemb.Value, Valor) then
          cDILista.XLocDesemb := Valor;
        if Def('121', 'I22', DmNFe_0000.QrNfeItsIDIDI_UFDesemb.Value, Valor) then
          cDILista.UFDesemb := Valor;
        if Def('122', 'I23', DmNFe_0000.QrNfeItsIDIDI_dDesemb.Value, Valor) then
          cDILista.DDesemb := Valor;
        if Def('122a', 'I23a', DmNFe_0000.QrNfeItsIDIDI_tpViaTransp.Value, Valor) then
          cDILista.tpViaTransp := Valor;
        if Def('122b', 'I23b', DmNFe_0000.QrNfeItsIDIDI_vAFRMM.Value, Valor) then
          cDILista.vAFRMM := Valor;
        if Def('122c', 'I23c', DmNFe_0000.QrNfeItsIDIDI_tpIntermedio.Value, Valor) then
          cDILista.tpIntermedio := Valor;
        if Def('122d', 'I23d', DmNFe_0000.QrNfeItsIDIDI_CNPJ.Value, Valor) then
          cDILista.CNPJ := Valor;
        if Def('122e', 'I23e', DmNFe_0000.QrNfeItsIDIDI_UFTerceiro.Value, Valor) then
          cDILista.UFTerceiro := Valor;
        if Def('123', 'I24', DmNFe_0000.QrNfeItsIDIDI_cExportador.Value, Valor) then
          cDILista.CExportador := Valor;

        /////////////////////
        ///
       (* Informa��es da TAG ADI... Opcional *)
{
        DmNFe_0000.QrNFeItsIDIa.Close;
        DmNFe_0000.QrNFeItsIDIa.Params[00].AsInteger := DmNFe_0000.QrNFeItsIDIFatID.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[01].AsInteger := DmNFe_0000.QrNFeItsIDIFatNum.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[02].AsInteger := DmNFe_0000.QrNFeItsIDIEmpresa.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[03].AsInteger := DmNFe_0000.QrNFeItsIDInItem.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[04].AsInteger := DmNFe_0000.QrNFeItsIDIControle.Value;
        DmNFe_0000.QrNFeItsIDIa. O p e n ;
}
        DmNFe_0000.ReopenNFeItsIDIa(FatID, FatNum, Empresa, nItem, Controle);
        //
        if DmNFe_0000.QrNFEItsIDIa.RecordCount > 0 then
        begin
          j := DmNFe_0000.QrNFEItsIDI.RecNo - 1;//DmNFe_0000.QrNFeItsIDIa.RecNo;
          //
          cAdiLista := cXML.InfNFe.Det.Items[h].Prod.DI.Items[j].Adi.Add;
          if Def('125', 'I26', DmNFe_0000.QrNfeItsIDIaAdi_nAdicao.Value, Valor) then
            cAdiLista.NAdicao := Valor;
          if Def('126', 'I27', DmNFe_0000.QrNfeItsIDIaAdi_nSeqAdic.Value, Valor) then
            cAdiLista.NSeqAdic := Valor;
          if Def('127', 'I28', DmNFe_0000.QrNfeItsIDIaAdi_cFabricante.Value, Valor) then
            cAdiLista.CFabricante := Valor;
          if Def('128', 'I29', DmNFe_0000.QrNfeItsIDIaAdi_vDescDI.Value, Valor) then
            cAdiLista.VDescDI := Valor;
          if Def('128.1', 'I29a', DmNFe_0000.QrNfeItsIDIaAdi_nDraw.Value, Valor) then
            cAdiLista.nDraw := Valor;
        end else Geral.MB_Aviso('Declara��o de importa��o sem Adi��es!');
        ///
        /////////////////////
        DmNFe_0000.QrNFEItsIDI.Next;
      end;

    end;

(* I03 - Informa��es da TAG detExport... *)
(*
    DmNFe_0000.QrNFeItsI03.Close;
    DmNFe_0000.QrNFeItsI03.Params[00].AsInteger := DmNFe_0000.QrNFeItsIFatID.Value;
    DmNFe_0000.QrNFeItsI03.Params[01].AsInteger := DmNFe_0000.QrNFeItsIFatNum.Value;
    DmNFe_0000.QrNFeItsI03.Params[02].AsInteger := DmNFe_0000.QrNFeItsIEmpresa.Value;
    DmNFe_0000.QrNFeItsI03.Params[03].AsInteger := DmNFe_0000.QrNFeItsInItem.Value;
    DmNFe_0000.QrNFeItsI03. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsI03(FatID, FatNum, Empresa, nItem);

    //
    if DmNFe_0000.QrNFeItsI03.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFeItsI03.Eof do
      begin
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFeItsI03.RecNo;
        //
        cDetExport := cXML.InfNFe.Det.Items[h].Prod.detExport.Add;
        //
        if Def('128g', 'I51', DmNFe_0000.QrNFeItsI03detExport_nDraw.Value, Valor) then
          cDetExport.nDraw := Valor;
        //  '128h', 'I52'  Grupo exportInd
        if (DmNFe_0000.QrNFeItsI03detExport_nRE.Value <> 0)
        or (DmNFe_0000.QrNFeItsI03detExport_chNFe.Value <> '')
        or (DmNFe_0000.QrNFeItsI03detExport_qExport.Value <> 0) then
        begin
          if Def('128i', 'I53', DmNFe_0000.QrNFeItsI03detExport_nRE.Value, Valor) then
            cDetExport.exportInd.nRE := Valor;
          if Def('128j', 'I54', DmNFe_0000.QrNFeItsI03detExport_chNFe.Value, Valor) then
            cDetExport.exportInd.chNFe := Valor;
          if Def('128k', 'I55', DmNFe_0000.QrNFeItsI03detExport_qExport.Value, Valor) then
            cDetExport.exportInd.qExport := Valor;
        end;
        //
        DmNFe_0000.QrNFeItsI03.Next;
      end;
    end;
    (*
    if Def('128a', 'I30', DmNFe_0000.QrNfeItsIprod_xPed.Value, Valor) then
      cDetLista.xPed := Valor;
    if Def('128b', 'I31', DmNFe_0000.QrNfeItsIprod_nItemPed.Value, Valor) then
    *)
    // I05. Produtos e servicos / Pedido de compra
    // Informacao de interesse do emissor para controle de B2B
    if Def('128m', 'I60', DmNFe_0000.QrNfeItsIprod_xPed.Value, Valor) then
      cDetLista.prod.xPed := Valor;
    if Trim(DmNFe_0000.QrNfeItsIprod_xPed.Value) <> '' then
    begin
      if Def('128n', 'I61', DmNFe_0000.QrNfeItsIprod_nItemPed.Value, Valor) then
        cDetLista.prod.nItemPed := Valor;
    end;
    // I07. Produtos e servicos / Grupo Diversos
    // Numero de controle da FCI - Ficha de Conteudo de  Importacao
    if Def('128p', 'I70', DmNFe_0000.QrNfeItsIprod_nFCI.Value, Valor) then
      cDetLista.prod.nFCI := Valor;
    (* J - Informa��es da TAG VEICPROD... Opcional *)
    (*
    cDetLista.Prod.VeicProd.TpOp := Valor; // Zero � Aceitav�l como valor...
    cDetLista.Prod.VeicProd.Chassi := Valor;
    cDetLista.Prod.VeicProd.CCor := Valor;
    cDetLista.Prod.VeicProd.XCor := Valor;
    cDetLista.Prod.VeicProd.Pot := Valor;
    cDetLista.Prod.VeicProd.CM3 := Valor;
    cDetLista.Prod.VeicProd.PesoL := Valor;
    cDetLista.Prod.VeicProd.PesoB := Valor;
    cDetLista.Prod.VeicProd.NSerie := Valor;
    cDetLista.Prod.VeicProd.TpComb := Valor;
    cDetLista.Prod.VeicProd.NMotor := Valor;
    cDetLista.Prod.VeicProd.CMKG := Valor;
    cDetLista.Prod.VeicProd.Dist := Valor;
    cDetLista.Prod.VeicProd.RENAVAM := Valor;
    cDetLista.Prod.VeicProd.AnoMod := Valor;
    cDetLista.Prod.VeicProd.AnoFab := Valor;
    cDetLista.Prod.VeicProd.TpPint := Valor;
    cDetLista.Prod.VeicProd.TpVeic := Valor;
    cDetLista.Prod.VeicProd.EspVeic := Valor;
    cDetLista.Prod.VeicProd.VIN := Valor;
    cDetLista.Prod.VeicProd.CondVeic := Valor;
    cDetLista.Prod.VeicProd.CMod := Valor;
    *)

    (* K - Informa��es da TAG MED... Opcional *)
    (*
    cMedLista := cXML.InfNFe.Det.Items[J - 1].Prod.Med.Add;
    cMedLista.NLote := Valor;
    cMedLista.QLote := DmNFe_0000.DecimalPonto(FormatFloat('##0.000', Valor));;
    cMedLista.DFab := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.DVal := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.VPMC := DmNFe_0000.DecimalPonto(FormatFloat('##0.00', Valor));
    *)

    (* L - Informa��o da TAG ARMA... Opcional *)
    (*
    cArmLista := cXML.InfNFe.Det.Items[j].Prod.Arma.Add;
    cArmLista.TpArma := Valor; // Zero � Aceitav�l como valor...
    cArmLista.NSerie := Valor;
    cArmLista.NCano := Valor;
    cArmLista.Descr := Valor;
    *)

    (* LA - Informa��es da TAG COMB... Opcional *)
    (*
    cDetLista.Prod.Comb.CProdANP := Valor;
    cDetLista.Prod.Comb.CODIF := Valor;
    cDetLista.Prod.Comb.QTemp := Valor;
    cDetLista.Prod.Comb.CIDE.QBCProd := Valor;
    cDetLista.Prod.Comb.CIDE.VAliqProd := Valor;
    cDetLista.Prod.Comb.CIDE.VCIDE := Valor;
    cDetLista.Prod.Comb.ICMSComb.VBCICMS := Valor;
    cDetLista.Prod.Comb.ICMSComb.VICMS := Valor;
    cDetLista.Prod.Comb.ICMSComb.VBCICMSST := Valor;
    cDetLista.Prod.Comb.ICMSComb.VICMSST := Valor;
    cDetLista.Prod.Comb.ICMSInter.VBCICMSSTDest := Valor;
    cDetLista.Prod.Comb.ICMSInter.VICMSSTDest := Valor;
    cDetLista.Prod.Comb.ICMSCons.VBCICMSSTCons := Valor;
    cDetLista.Prod.Comb.ICMSCons.VICMSSTCons := Valor;
    cDetLista.Prod.Comb.ICMSCons.UFCons := Valor;
    *)



    // M - Tributos incidentes no produto ou servi�o
    // '163', 'M01'  = Grupo de tributos incidentes no produto ou servi�o

  // FIM 2013-05-07
    // '163a', 'M012'  = {vTotTrib) Valor aproximado total de tributos federais, estaduais e municipais
    //if DmNFe_0000.QrFilialNFeNT2013_003LTT.Value > 0 then
    begin
      DmNFe_0000.QrNFEItsM.Close;
      DmNFe_0000.QrNFEItsM.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsM.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsM.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsM.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      UnDmkDAC_PF.AbreQuery(DmNFe_0000.QrNFEItsM, Dmod.MyDB);
      //
      //if DmNFe_0000.QrNFEItsMiTotTrib.Value > 0 then
        if Def('163a', 'M02', DmNFe_0000.QrNFEItsMvTotTrib.Value, Valor) then
          cDetLista.Imposto.vTotTrib  := Valor;
    end;
  // FIM 2013-05-07


  (*  Ser� necess�rio criar regras de neg�cio  de acordo com o ERP na qual definir� a ocorr�ncia das TAGs de IMPOSTO...*)

    // N - ICMS Normal e ST
    // '164', 'N01'  = Grupo do ICMS na opera��o pr�pria e ST
    DmNFe_0000.ReopenNFEItsN(FatID, FatNum, Empresa, nItem);
    //
    (*
    Informar apenas um dos grupos N02, N03, N04, N05, N06, N07, N08, N09, N10,
    N10a, N10b ou N10c com base no conte�do informado na TAG Tributa��o do ICMS.
    (v2.0)
    *)
    if (DmNFe_0000.QrNFECabAemit_CRT.Value <> 1) and
    // 2011-09-18
    (DmNFe_0000.QrNFEItsNICMS_CSOSN.Value = 0) then
    begin
      case DmNFe_0000.QrNFEItsNICMS_CST.Value of
        0:
        begin
        (* TAG ICMS.ICMS00... *)
              // '165', 'N02'  = Grupo do CST = 00
          if Def('166', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.Orig  := Valor;
          if Def('167', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.CST   := Valor;
          if Def('168', 'N13', DmNFe_0000.QrNFEItsNICMS_ModBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.ModBC := Valor;
          if Def('169', 'N15', DmNFe_0000.QrNFEItsNICMS_VBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.VBC   := Valor;
          if Def('170', 'N16', DmNFe_0000.QrNFEItsNICMS_PICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.PICMS := Valor;
          if Def('171', 'N17', DmNFe_0000.QrNFEItsNICMS_VICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS00.VICMS := Valor;
        end;
        10:
        begin
          (* TAG ICMS.ICMS10... *)
              // '172', 'N03'  = Grupo do CST = 10
          if Def('173', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.Orig      := Valor;
          if Def('174', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.CST       := Valor;
          if Def('175', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.ModBC     := Valor;
          if Def('176', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VBC       := Valor;
          if Def('177', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PICMS     := Valor;
          if Def('178', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VICMS     := Valor;
          if Def('179', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.ModBCST   := Valor;
          if Def('180', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PMVAST    := Valor;
          if Def('181', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PRedBCST  := Valor;
          if Def('184', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VBCST     := Valor;
          if Def('183', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PICMSST   := Valor;
          if Def('184', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.VICMSST   := Valor;
        end;
        20:
        begin
          (* TAG ICMS.ICMS20... *)
              // '185', 'N04'  = Grupo do CST = 20
          if Def('186', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.Orig      := Valor;
          if Def('187', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.CST       := Valor;
          if Def('188', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.ModBC     := Valor;
          if Def('189', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.PRedBC    := Valor;
          if Def('190', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VBC       := Valor;
          if Def('191', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.PICMS     := Valor;
          if Def('192', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VICMS     := Valor;

          //Mudado em 03/09/2015 => In�cio
          if DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value <> 0 then
          begin
            if Def('192.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.VICMSDeson     := Valor;
            if Def('192.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.motDesICMS     := Valor;
          end;
          (*
          if Def('192.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.VICMSDeson     := Valor;
          if Def('192.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS20.motDesICMS     := Valor;
          *)
          //Mudado em 03/09/2015 => Fim
        end;
        30:
        begin
          (* TAG ICMS.ICMS30... *)
              // '193', 'N05'  = Grupo do CST = 30
          if Def('194', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.Orig      := Valor;
          if Def('195', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.CST       := Valor;
          if Def('196', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.ModBCST   := Valor;
          if Def('197', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PMVAST    := Valor;
          if Def('198', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PRedBCST  := Valor;
          if Def('199', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VBCST     := Valor;
          if Def('200', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.PICMSST   := Valor;
          if Def('201', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VICMSST   := Valor;
          if Def('201.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.VICMSDeson     := Valor;
          if Def('201.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS30.motDesICMS     := Valor;
        end;
        // modificado 2010-07-07 faltava CST 41 e 50!
        40,41,50:
        begin
          { Mudado em 12/05/2015 - Ini
          (* TAG ICMS.ICMS40... *)
              // '202', 'N06'  = Grupo do CST = 40, 41, 50
          if Def('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
          if Def('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
          //if Def('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          //  cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
          if Def('204.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.VICMSDeson     := Valor;
          if Def('204.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.motDesICMS     := Valor;
          }
          (* TAG ICMS.ICMS40... *)
              // '202', 'N06'  = Grupo do CST = 40, 41, 50
          if Def('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
          if Def('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
          //if Def('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          //  cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
          if DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value <> 0 then
          begin
            if Def('204.01', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.VICMSDeson := Valor;
            if Def('204.02', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.motDesICMS := Valor;
          end;
          //Mudado em 12/05/2015 - Fim
        end;
        51:
        begin
          (* TAG ICMS.ICMS51... *)
              // '205', 'N07'  = Grupo do CST = 51
          if Def('206', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.Orig      := Valor;
          if Def('207', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.CST       := Valor;
          if Def('208', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.ModBC     := Valor;
          if Def('209', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.PRedBC    := Valor;
          if Def('210', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.VBC       := Valor;
          if Def('211', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.PICMS     := Valor;
          if Def('211.01', 'N16a', DmNFe_0000.QrNFEItsNICMS_vICMSOp.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.vICMSOp     := Valor;
          if Def('211.02', 'N16b', DmNFe_0000.QrNFEItsNICMS_pDif.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.pDif     := Valor;
          if Def('211.03', 'N16c', DmNFe_0000.QrNFEItsNICMS_vICMSDif.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.vICMSDif     := Valor;

          if Def('212', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS51.VICMS     := Valor;
        end;
        60:
        begin
          (* TAG ICMS.ICMS60... *)
              // '213', 'N08'  = Grupo do CST = 60
          if Def('214', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.Orig      := Valor;
          if Def('215', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.CST       := Valor;
          if Def('216', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.VBCSTRet     := Valor;
          if Def('217', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS60.VICMSSTRet   := Valor;
        end;
        70:
        begin
          (* TAG ICMS.ICMS70... *)
              // '218', 'N09'  = Grupo do CST = 70
          if Def('219', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.Orig      := Valor;
          if Def('220', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.CST       := Valor;
          if Def('221', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.ModBC     := Valor;
          if Def('222', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PRedBC    := Valor;
          if Def('223', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VBC       := Valor;
          if Def('224', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PICMS     := Valor;
          if Def('225', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMS     := Valor;
          if Def('226', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.ModBCST   := Valor;
          if Def('227', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PMVAST    := Valor;
          if Def('228', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PRedBCST  := Valor;
          if Def('229', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VBCST     := Valor;
          if Def('230', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.PICMSST   := Valor;
          if Def('231', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMSST   := Valor;
          if Def('231.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.VICMSDeson     := Valor;
          if Def('231.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS70.motDesICMS     := Valor;
        end;
        90:
        begin
          (* TAG ICMS.ICMS90... *)
              // '232', 'N10'  = Grupo do CST = 90
          if Def('233', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.Orig      := Valor;
          if Def('234', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.CST       := Valor;
          if Def('235', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.ModBC     := Valor;
          if Def('236', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VBC       := Valor;
          if Def('237', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PRedBC    := Valor;
          if Def('238', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PICMS     := Valor;
          if Def('239', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VICMS     := Valor;
          if Def('240', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.ModBCST   := Valor;
          if Def('241', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PMVAST    := Valor;
          if Def('242', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PRedBCST  := Valor;
          if Def('243', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VBCST     := Valor;
          if Def('244', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.PICMSST   := Valor;
          if Def('245', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VICMSST   := Valor;
          if Def('245.2', 'N27a', DmNFe_0000.QrNFEItsNICMS_vICMSDeson.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.VICMSDeson     := Valor;
          if Def('245.3', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS90.motDesICMS     := Valor;
        end;
(*
245.01 N10a ICMSPart Partilha do ICMS entre a UF
de origem e UF de destino ou
a UF definida na legisla��o.
CG N01 1-1 Opera��o interestadual para
consumidor final com partilha do
ICMS devido na opera��o entre
a UF de origem e a UF do
destinat�rio ou a UF definida na
P�gina 136 de 232
# ID Campo Descri��o Ele Pai Tipo Ocorr�ncia tamanho Dec. Observa��o
legisla��o. (Ex. UF da
concession�ria de entrega do
ve�culos) (v2.0)
245.02 N11 orig Origem da mercadoria E N10a N 1-1 1 Origem da mercadoria:
0 � Nacional;
1 � Estrangeira � Importa��o
direta;
2 � Estrangeira � Adquirida no
mercado interno. (v2.0)
245.03 N12 CST Tributa��o do ICMS E N10a N 1-1 2 Tributa��o pelo ICMS
10 - Tributada e com cobran�a
do ICMS por substitui��o
tribut�ria;
90 � Outros.
245.04 N13 modBC Modalidade de determina��o
da BC do ICMS
E N10a N 1-1 1 0 - Margem Valor Agregado (%);
1 - Pauta (Valor);
2 - Pre�o Tabelado M�x. (valor);
3 - valor da opera��o.
(v2.0)
245.05 N15 vBC Valor da BC do ICMS E N10a N 1-1 15 2 (v2.0)
245.06 N14 pRedBC Percentual da Redu��o de BC E N10a N 0-1 5 2 (v2.0)
245.07 N16 pICMS Al�quota do imposto E N10a N 1-1 5 2 (v2.0)
245.08 N17 vICMS Valor do ICMS E N10a N 1-1 15 2
245.09 N18 modBCST Modalidade de determina��o
da BC do ICMS ST
E N10a N 1-1 1 0 � Pre�o tabelado ou m�ximo
sugerido;
1 - Lista Negativa (valor);
2 - Lista Positiva (valor);
3 - Lista Neutra (valor);
4 - Margem Valor Agregado (%);
5 - Pauta (valor);
245.10 N19 pMVAST Percentual da margem de
valor Adicionado do ICMS ST
E N10a N 0-1 5 2 (v2.0)
245.11 N20 pRedBCST Percentual da Redu��o de BC
do ICMS ST
E N10a N 0-1 5 2 (v2.0)
245.12 N21 vBCST Valor da BC do ICMS ST E N10a N 1-1 15 2 (v2.0)
245.13 N22 pICMSST Al�quota do imposto do ICMS E N10a N 1-1 5 2 (v2.0)
P�gina 137 de 232
# ID Campo Descri��o Ele Pai Tipo Ocorr�ncia tamanho Dec. Observa��o
ST
245.14 N23 vICMSST Valor do ICMS ST E N10a N 1-1 15 2 Valor do ICMS ST(v2.0)
245.15 N25 pBCOp Percentual da BC opera��o
pr�pria
E N10a N 1-1 5 2 Percentual para determina��o
do valor da Base de C�lculo da
opera��o pr�pria. (v2.0)
245.16 N24 UFST UF para qual � devido o ICMS
ST
E N10a C 1-1 2 Sigla da UF para qual � devido
o ICMS ST da opera��o. (v2.0)
245.17 N10b ICMSST ICMS ST � repasse de ICMS
ST retido anteriormente em
opera��es interestaduais com
repasses atrav�s do Substituto
Tribut�rio
CG N01 1-1 Grupo de informa��o do ICMS
ST devido para a UF de destino,
nas opera��es interestaduais de
produtos que tiveram reten��o
antecipada de ICMS por ST na
UF do remetente. Repasse via
Substituto Tribut�rio. (v2.0)
245.18 N11 orig Origem da mercadoria E N10b N 1-1 1 Origem da mercadoria:
0 � Nacional;
1 � Estrangeira � Importa��o
direta;
2 � Estrangeira � Adquirida no
mercado interno. (v2.0)
245.19 N12 CST Tributa��o do ICMS E N10b N 1-1 2 Tributa��o pelo ICMS
41 � N�o Tributado (v2.0)
245.20 N26 vBCSTRet Valor do BC do ICMS ST
retido na UF remetente
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST retido na UF remetente
(v2.0)
245.21 N27 vICMSSTRet Valor do ICMS ST retido na
UF remetente
E N10b N 1-1 15 2 Informar o valor do ICMS ST
retido na UF remetente (iv2.0)
245.22 N31 vBCSTDest Valor da BC do ICMS ST da
UF destino
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST da UF destino (v2.0)
245.23 N32 vICMSSTDes
t
Valor do ICMS ST da UF
destino
E N10b N 1-1 15 2 Informar o valor da BC do ICMS
ST da UF destino (v2.0)
*)
        else Geral.MB_Aviso('CST do ICMS n�o implementado!');
      end;
    end else // Simples Nacional!
    begin
      case DmNFe_0000.QrNFEItsNICMS_CSOSN.Value of
        101:
        begin
        (* TAG ICMS.ICMSSN101... *)
              // '245.24', 'N10c'  = Grupo CRT=1 - Simples Nacional e CSOSN = 101
          if Def('245.25', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.Orig  := Valor;
          if Def('245.26', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.CSOSN  := Valor;
          if Def('245.27', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.pCredSN  := Valor;
          if Def('245.28', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN101.vCredICMSSN  := Valor;
        end;
        102, 103, 300, 400:
        begin
        (* TAG ICMS.ICMSSN102... *)
              // '245.24', 'N10d'  = Grupo CRT=1 - Simples Nacional e CSOSN = 102, 103, 300 ou 400
          if Def('245.25', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN102.Orig  := Valor;
          if Def('245.26', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN102.CSOSN  := Valor;
        end;
        (*
        201:
        begin
         TAG ICMS.ICMSSN201...
              // '245.27', 'N10e'  = Grupo CRT=1 - Simples Nacional e CSOSN = 201
          if Def('245.28', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.Orig  := Valor;
          if Def('245.29', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.CSOSN  := Valor;
          if Def('245.30', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.modBCST  := Valor;
          if Def('245.31', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pMVAST  := Valor;
          if Def('224.32', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pRedBCST  := Valor;
          if Def('245.33', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vBCST  := Valor;
          if Def('245.34', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pICMSST  := Valor;
          if Def('245.35', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vICMSST  := Valor;
          if Def('245.36', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.pCredSN  := Valor;
          if Def('245.37', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN201.vCredICMSSN  := Valor;
        end;
        *)
        202, 203:
        begin
        (* TAG ICMS.ICMSSN202... *)
              // '245.38', 'N10f'  = Grupo CRT=1 - Simples Nacional e CSOSN = 202 ou 203
          if Def('245.39', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.Orig  := Valor;
          if Def('245.40', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.CSOSN  := Valor;
          if Def('245.41', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.modBCST  := Valor;
          if Def('245.42', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pMVAST  := Valor;
          if Def('245.43', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pRedBCST  := Valor;
          if Def('245.44', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.vBCST  := Valor;
          if Def('245.45', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.pICMSST  := Valor;
          if Def('245.46', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN202.vICMSST  := Valor;
        end;
        500:
        begin
        (* TAG ICMS.ICMSSN500... *)
              // '245.47', 'N10g'  = Grupo CRT=1 - Simples Nacional e CSOSN = 500
          if Def('245.48', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.Orig  := Valor;
          if Def('245.49', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.CSOSN  := Valor;
          if Def('245.50', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.vBCSTRet  := Valor;
          if Def('245.51', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN500.vICMSSTRet  := Valor;
        end;
        900:
        begin
        (* TAG ICMS.ICMSSN900... *)
              // '245.52', 'N10h'  = Grupo CRT=1 - Simples Nacional e CSOSN = 900
          if Def('245.53', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.Orig  := Valor;
          if Def('245.54', 'N12a', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.CSOSN  := Valor;
          if Def('245.55', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.modBC  := Valor;
          if Def('245.56', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vBC  := Valor;
          if Def('245.57', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pRedBC  := Valor;
          if Def('245.58', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pICMS  := Valor;
          if Def('245.59', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vICMS  := Valor;
          if Def('245.60', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.modBCST  := Valor;
          if Def('245.61', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pMVAST  := Valor;
          if Def('245.62', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pRedBCST  := Valor;
          if Def('245.63', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vBCST  := Valor;
          if Def('245.64', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pICMSST  := Valor;
          if Def('245.65', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vICMSST  := Valor;
          if Def('245.52', 'N29', DmNFe_0000.QrNFEItsNICMS_pCredSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.pCredSN  := Valor;
          if Def('245.53', 'N30', DmNFe_0000.QrNFEItsNICMS_vCredICMSSN.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMSSN900.vCredICMSSN  := Valor;
        end;
        else Geral.MB_Aviso('CSOSN (' +
        FormatFloat('0', DmNFe_0000.QrNFEItsNICMS_CSOSN.Value) +
        ') do CRT=1 n�o implementado!');
      end;
    end;
{
    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    if GeraNT2015_03 then
    begin
      DmNFe_0000.ReopenNFeItsNA(FatID, FatNum, Empresa, nItem);
      if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
      begin
        (* TAG ICMS para a UF de destino... *)
            // '245a.01',  'NA01'  = Grupo de Tributa��o do ICMS para a UF de destino
        if Def('245a.03', 'NA03', DmNFe_0000.QrNFeItsNAICMSUFDest_vBCUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VBCUFDest := Valor;
        if Def('245a.05', 'NA05', DmNFe_0000.QrNFeItsNAICMSUFDest_pFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PFCPUFDest := Valor;
        if Def('245a.07', 'NA07', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSUFDest := Valor;
        if Def('245a.09', 'NA09', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInter.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInter := Valor;
        if Def('245a.11', 'NA11', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInterPart.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInterPart := Valor;
        if Def('245a.13', 'NA13', DmNFe_0000.QrNFeItsNAICMSUFDest_vFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VFCPUFDest := Valor;
        if Def('245a.15', 'NA15', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFDest := Valor;
        if Def('245a.17', 'NA17', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFRemet.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFRemet := Valor;
      end;
    end;
    //Fim PL 008h
}
    if (*(DmNFe_0000.QrFilialSimplesFed.Value = 0) and*)
    (DmNFe_0000.QrNFEItsITem_IPI.Value = 1) then
    begin
(*
      DmNFe_0000.QrNFEItsO.Close;
      DmNFe_0000.QrNFEItsO.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsO.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsO.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsO.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsO. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsO(FatID, FatNum, Empresa, nItem);
      //
      (* TAG IPI... *)
          // '246', 'O01'  = Grupo do IPI
      if Def('247', 'O02', DmNFe_0000.QrNFEItsOIPI_clEnq.Value, Valor) then
        cDetLista.Imposto.IPI.ClEnq := Valor;
      if Def('248', 'O03', DmNFe_0000.QrNFEItsOIPI_CNPJProd.Value, Valor) then
        cDetLista.Imposto.IPI.CNPJProd := Valor;
      if Def('249', 'O04', DmNFe_0000.QrNFEItsOIPI_cSelo.Value, Valor) then
        cDetLista.Imposto.IPI.CSelo := Valor;
      if Def('250', 'O05', DmNFe_0000.QrNFEItsOIPI_qSelo.Value, Valor) then
        cDetLista.Imposto.IPI.QSelo := Valor;
      if Def('251', 'O06', DmNFe_0000.QrNFEItsOIPI_cEnq.Value, Valor) then
        cDetLista.Imposto.IPI.CEnq := Valor;

      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([0,49,50,99]) then
      begin
        (* TAG IPI.IPITRIB... *)
            // '252', 'O08'  = Grupo do CST 00, 40, 50 e 99
        if Def('253', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
        begin
          if Valor = '0' then
            Valor := '00';
          cDetLista.Imposto.IPI.IPITrib.CST := Valor;
        end;
        if DmNFe_0000.QrNFEItsOIPI_vUnid.Value = 0 then
        begin
          if Def('254', 'O10', DmNFe_0000.QrNFEItsOIPI_vBC.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.VBC := Valor;
          if Def('257', 'O13', DmNFe_0000.QrNFEItsOIPI_pIPI.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.PIPI := Valor;
        end else begin
          if Def('255', 'O11', DmNFe_0000.QrNFEItsOIPI_qUnid.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.QUnid := Valor;
          if Def('256', 'O12', DmNFe_0000.QrNFEItsOIPI_vUnid.Value, Valor) then
            cDetLista.Imposto.IPI.IPITrib.VUnid := Valor;
        end;
            // '258', '???'  = ?????
        if Def('259', 'O14', DmNFe_0000.QrNFEItsOIPI_vIPI.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.VIPI := Valor;
      end else
      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([1,2,3,4,51,52,53,54,55]) then
      begin
        (* TAG IPI.IPINT... *)
            // '260', 'O08'  = Grupo do CST 01, 02, 03, 04, 51, 52, 53, 54 e 55
        if Def('261', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
          cDetLista.Imposto.IPI.IPINT.CST := FormatFloat('00', DmNFe_0000.QrNFEItsOIPI_CST.Value)
      end else
        Geral.MB_Erro('CST do IPI desconhecido!');
    end;
    (* TAG II... *)
(*
    DmNFe_0000.QrNFEItsP.Close;
    DmNFe_0000.QrNFEItsP.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsP.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsP.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsP.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsP. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsP(FatID, FatNum, Empresa, nItem);
    // '262', 'P01'  = Grupo do II
    if DmNFe_0000.QrNFEItsP.RecordCount > 0 then
    begin
      if (DmNFe_0000.QrNFEItsPII_vII.Value > 0) or (DmNFe_0000.QrNFEItsITem_II.Value <> 0) then
      begin
        if Def('263', 'P02', DmNFe_0000.QrNFEItsPII_vBC.Value, Valor) then
          cDetLista.Imposto.II.VBC := Valor;
        if Def('264', 'P03', DmNFe_0000.QrNFEItsPII_vDespAdu.Value, Valor) then
          cDetLista.Imposto.II.VDespAdu := Valor;
        if Def('265', 'P04', DmNFe_0000.QrNFEItsPII_vII.Value, Valor) then
          cDetLista.Imposto.II.VII := Valor;
        if Def('266', 'P05', DmNFe_0000.QrNFEItsPII_vIOF.Value, Valor) then
          cDetLista.Imposto.II.VIOF := Valor;
      end;
    end;
    //

    (* TAGs PIS... *)
    // '267', 'Q01'  = Grupo do PIS
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
      cDetLista.Imposto.PIS.PISAliq.CST  := '01';
      cDetLista.Imposto.PIS.PISAliq.VBC  := '0.00';
      cDetLista.Imposto.PIS.PISAliq.PPIS := '0.00';
      cDetLista.Imposto.PIS.PISAliq.VPIS := '0.00';
    end else begin
(*
      DmNFe_0000.QrNFEItsQ.Close;
      DmNFe_0000.QrNFEItsQ.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsQ.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsQ.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsQ.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsQ. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsQ(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsQ.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de PIS!' +
        sLineBreak + 'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      // '268', 'Q02' Tag do PIS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([1,2]) then
      begin
        (* TAG PIS.PISALIQ... *)
        if Def('269', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.CST := Valor;
        if Def('270', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VBC := Valor;
        if Def('271', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.PPIS := Valor;
        if Def('272', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VPIS := Valor;
      end else
      // '273', 'Q03' Tag do PIS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([3]) then
      begin
        (* TAG PIS.PISQTDE... *)
        if Def('274', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.CST := Valor;
        if Def('275', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.QBCProd := Valor;
        if Def('276', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VAliqProd := Valor;
        if Def('277', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VPIS := Valor;
      end else
      // '278', 'Q04' Tag do PIS n�o tributado CST = 04, 05, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([4,5,6,7,8,9]) then
      begin
        (* TAG PIS.PISNT... *)
        if Def('279', 'Q02', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISNT.CST := Valor;
      end else
      // '280', 'Q05' Tag do PIS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([49,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67,70,71,72,73,74,75,98,99]) then
      begin
        (* TAG PIS.PISOUTR... *)
        if Def('281', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsQPIS_pPIS.Value > 0 then
        begin
          if Def('282', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VBC := Valor;
          if Def('283', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.PPIS := Valor;
        end else begin
          if Def('284', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.QBCProd := Valor;
          if Def('285', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VAliqProd := Valor;
        end;
        if Def('286', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.VPIS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MB_Erro('CST do PIS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value));
          //Result := False;
        Exit;
      end;
      // '287', 'R01' Tag do PISST...
      (* TAG PISST... *)
(*
      DmNFe_0000.QrNFEItsR.Close;
      DmNFe_0000.QrNFEItsR.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsR.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsR.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsR.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsR. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsR(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsR.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de PIS ST!' + sLineBreak +
        'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) or
      (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) then
        begin
          if Def('288', 'R02', DmNFe_0000.QrNFEItsRPISST_vBC.Value, Valor) then
            cDetLista.Imposto.PISST.VBC := Valor;
          if Def('289', 'R03', DmNFe_0000.QrNFEItsRPISST_pPIS.Value, Valor) then
            cDetLista.Imposto.PISST.PPIS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
        begin
          if Def('290', 'R04', DmNFe_0000.QrNFEItsRPISST_qBCProd.Value, Valor) then
            cDetLista.Imposto.PISST.QBCProd := Valor;
          if Def('291', 'R05', DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PISST.VAliqProd := Valor;
        end;
        if Def('292', 'R06', DmNFe_0000.QrNFEItsRPISST_vPIS.Value, Valor) then
          cDetLista.Imposto.PISST.VPIS := Valor;
      end;
    end;
      //
    (* TAGs COFINS... *)
    // '293', 'S01'  = Grupo do COFINS
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
      cDetLista.Imposto.COFINS.COFINSAliq.CST     := '01';
      cDetLista.Imposto.COFINS.COFINSAliq.VBC     := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := '0.00';
    end else begin
(*
      DmNFe_0000.QrNFEItsS.Close;
      DmNFe_0000.QrNFEItsS.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsS.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsS.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsS.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsS. O p e n ;
*)
      DmNFe_0000.ReopenNFeItsS(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsS.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de COFINS!' +
        sLineBreak + 'Gera��o abortada!');
        Exit;
      end;
      // '294', 'S02' Tag do COFINS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([1,2]) then
      begin
        (* TAG COFINS.COFINSALIQ... *)
        if Def('295', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.CST := Valor;
        if Def('296', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VBC := Valor;
        if Def('297', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := Valor;
        if Def('298', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := Valor;
      end else
      // '299', 'S03' Tag do COFINS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([3]) then
      begin
        (* TAG COFINS.COFINSQTDE... *)
        if Def('300', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.CST := Valor;
        if Def('301', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.QBCProd := Valor;
        if Def('302', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VAliqProd := Valor;
        if Def('303', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VCOFINS := Valor;
      end else
      // '304', 'S04' Tag do COFINS n�o tributado CST = 04, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([4,5,6,7,8,9]) then
      begin
        (* TAG COFINS.COFINSNT... *)
        if Def('305', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSNT.CST := Valor;
      end else
      // '306', 'S05' Tag do COFINS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([49,50,51,52,53,54,55,56,60,61,62,63,64,65,66,67,70,71,72,73,74,75,98,99]) then
      begin
        (* TAG COFINS.COFINSOUTR... *)
        if Def('307', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value > 0 then
        begin
          if Def('308', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VBC := Valor;
          if Def('309', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.PCOFINS := Valor;
        end else begin
          if Def('310', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.QBCProd := Valor;
          if Def('311', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VAliqProd := Valor;
        end;
        if Def('312', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.VCOFINS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MB_Erro('CST do COFINS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value));
        Exit;
      end;
      // '313', 'T01' Tag do COFINSST...
      (* TAG COFINSST... *)
(*
      DmNFe_0000.QrNFEItsT.Close;
      DmNFe_0000.QrNFEItsT.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsT.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsT.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsT.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsT. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsT(FatID, FatNum, Empresa, nItem);

      if DmNFe_0000.QrNFEItsT.RecordCount = 0 then
      begin
        Geral.MB_Erro('Falta informa��es de COFINS ST!' +
        sLineBreak + 'Gera��o abortada!');
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) or
      (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) then
        begin
          if Def('314', 'T02', DmNFe_0000.QrNFEItsTCOFINSST_vBC.Value, Valor) then
            cDetLista.Imposto.COFINSST.VBC := Valor;
          if Def('315', 'T03', DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINSST.PCOFINS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
        begin
          if Def('316', 'T04', DmNFe_0000.QrNFEItsTCOFINSST_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.QBCProd := Valor;
          if Def('317', 'T05', DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.VAliqProd := Valor;
        end;
        if Def('318', 'T06', DmNFe_0000.QrNFEItsTCOFINSST_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINSST.VCOFINS := Valor;
      end;
    end;
    //
    //Pacote de Liberacao 008h - Valido somente a partir de 01/01/2016
    //Cuidado!!!!! Campo n�o � obrigat�rio e s� valer� a partir de 01/01/2016!!!!
    if GeraNT2015_03 then
    begin
      DmNFe_0000.ReopenNFeItsNA(FatID, FatNum, Empresa, nItem);
      if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
      begin
        (* TAG ICMS para a UF de destino... *)
            // '245a.01',  'NA01'  = Grupo de Tributa��o do ICMS para a UF de destino
        if Def('245a.03', 'NA03', DmNFe_0000.QrNFeItsNAICMSUFDest_vBCUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VBCUFDest := Valor;
        if Def('245a.05', 'NA05', DmNFe_0000.QrNFeItsNAICMSUFDest_pFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PFCPUFDest := Valor;
        if Def('245a.07', 'NA07', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSUFDest := Valor;
        if Def('245a.09', 'NA09', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInter.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInter := Valor;
        if Def('245a.11', 'NA11', DmNFe_0000.QrNFeItsNAICMSUFDest_pICMSInterPart.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.PICMSInterPart := Valor;
        if Def('245a.13', 'NA13', DmNFe_0000.QrNFeItsNAICMSUFDest_vFCPUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VFCPUFDest := Valor;
        if Def('245a.15', 'NA15', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFDest.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFDest := Valor;
        if Def('245a.17', 'NA17', DmNFe_0000.QrNFeItsNAICMSUFDest_vICMSUFRemet.Value, Valor) then
          cDetLista.Imposto.ICMSUFDest.VICMSUFRemet := Valor;
      end;
    end;
    //Fim PL 008h
    // '319', 'U01' Tag do grupo ISSQN...
    (* TAG ISSQN.. *)
(*
O grupo de ISSQN �
mutuamente exclusivo com os
grupos ICMS, IPI e II, isto � se
ISSQN for informado os grupos
ICMS, IPI e II n�o ser�o
informados e vice-versa (v2.0).
*)
(*
    DmNFe_0000.QrNFEItsU.Close;
    DmNFe_0000.QrNFEItsU.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsU.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsU.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsU.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsU. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsU(FatID, FatNum, Empresa, nItem);

    if DmNFe_0000.QrNFEItsU.RecordCount > 0 then
    begin
      if DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value > 0 then
      begin
        if Def('320', 'U02', DmNFe_0000.QrNFEItsUISSQN_vBC.Value, Valor) then
          cDetLista.Imposto.ISSQN.VBC := Valor;
        if Def('321', 'U03', DmNFe_0000.QrNFEItsUISSQN_vAliq.Value, Valor) then
          cDetLista.Imposto.ISSQN.VAliq := Valor;
        if Def('322', 'U04', DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value, Valor) then
          cDetLista.Imposto.ISSQN.VISSQN := Valor;
        if Def('323', 'U05', DmNFe_0000.QrNFEItsUISSQN_cMunFG.Value, Valor) then
          cDetLista.Imposto.ISSQN.CMunFG := Valor;
        if Def('324', 'U06', DmNFe_0000.QrNFEItsUISSQN_cListServ.Value, Valor) then
          cDetLista.Imposto.ISSQN.CListServ := Valor;
        // 2.00
        //if Def('324a', 'U07', DmNFe_0000.QrNFEItsUISSQN_cSitTrib.Value, Valor) then
        //  cDetLista.Imposto.ISSQN.cSitTrib := Valor;
        // 3.10
        if Def('324a', 'U07', DmNFe_0000.QrNFEItsUISSQN_vDeducao.Value, Valor) then
          cDetLista.Imposto.ISSQN.vDeducao := Valor;
        if Def('324b', 'U08', DmNFe_0000.QrNFEItsUISSQN_vOutro.Value, Valor) then
          cDetLista.Imposto.ISSQN.vOutro := Valor;
        if Def('324c', 'U09', DmNFe_0000.QrNFEItsUISSQN_vDescIncond.Value, Valor) then
          cDetLista.Imposto.ISSQN.vdescIncond := Valor;
        if Def('324d', 'U10', DmNFe_0000.QrNFEItsUISSQN_vDescCond.Value, Valor) then
          cDetLista.Imposto.ISSQN.vDescCond := Valor;
        if Def('324f', 'U11', DmNFe_0000.QrNFEItsUISSQN_vISSRet.Value, Valor) then
          cDetLista.Imposto.ISSQN.vISSRet := Valor;
        if Def('324g', 'U12', DmNFe_0000.QrNFEItsUISSQN_indISS.Value, Valor) then
          cDetLista.Imposto.ISSQN.indISS := Valor;
        if Def('324h', 'U13', DmNFe_0000.QrNFEItsUISSQN_cServico.Value, Valor) then
          cDetLista.Imposto.ISSQN.cServico := Valor;
        if Def('324i', 'U14', DmNFe_0000.QrNFEItsUISSQN_cMun.Value, Valor) then
          cDetLista.Imposto.ISSQN.cMun := Valor;
        if Def('324j', 'U15', DmNFe_0000.QrNFEItsUISSQN_cPais.Value, Valor) then
          cDetLista.Imposto.ISSQN.cPais := Valor;
        if Def('324k', 'U16', DmNFe_0000.QrNFEItsUISSQN_nProcesso.Value, Valor) then
          cDetLista.Imposto.ISSQN.nProcesso := Valor;
        if Def('324l', 'U17', DmNFe_0000.QrNFEItsUISSQN_indIncentivo.Value, Valor) then
          cDetLista.Imposto.ISSQN.indIncentivo := Valor;
      end;
    end;
(*
    DmNFe_0000.QrNFEItsV.Close;
    DmNFe_0000.QrNFEItsV.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsV.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsV.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsV.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsV. O p e n ;
*)
    DmNFe_0000.ReopenNFeItsV(FatID, FatNum, Empresa, nItem);

    if DmNFe_0000.QrNFEItsV.RecordCount > 0 then
      InfAdProd := DmNFe_0000.QrNFEItsVinfAdProd.Value
    else
      InfAdProd := '';
    //
    if Def('325', 'V01', InfAdProd, Valor) then
      cDetLista.InfAdProd := Valor;
    //
    DmNFe_0000.QrNFEItsI.Next;
  end;

(* W - Informa��es da TAG TOTAL... *)
  (* TAG ICMSTOT... *)
      // '326', 'W01'  =  Grupo de valores totais da NF-e
      // '327', 'W02'  =  Grupo de valores totais referentes ao ICMS
  if Def('328', 'W03', DmNFe_0000.QrNFECabAICMSTot_vBC.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBC := Valor;
  if Def('329', 'W04', DmNFe_0000.QrNFECabAICMSTot_vICMS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VICMS := Valor;
  if Def('329.01', 'W04a', DmNFe_0000.QrNFECabAICMSTot_vICMSDeson.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.vICMSDeson := Valor;
  if GeraNT2015_03 then
  begin
    if DmNFe_0000.QrNFeItsNA.RecordCount > 0 then
    begin
      if Def('329.03', 'W04c', DmNFe_0000.QrNFECabAICMSTot_vFCPUFDest.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VFCPUFDest := Valor;
      if Def('329.05', 'W04e', DmNFe_0000.QrNFECabAICMSTot_vICMSUFDest.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VICMSUFDest := Valor;
      if Def('329.07', 'W04g', DmNFe_0000.QrNFECabAICMSTot_vICMSUFRemet.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.VICMSUFRemet := Valor;
    end;
  end;
  if Def('330', 'W05', DmNFe_0000.QrNFECabAICMSTot_vBCST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBCST := Valor;
  if Def('331', 'W06', DmNFe_0000.QrNFECabAICMSTot_vST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VST := Valor;
  if Def('332', 'W07', DmNFe_0000.QrNFECabAICMSTot_vProd.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VProd := Valor;
  if Def('333', 'W08', DmNFe_0000.QrNFECabAICMSTot_vFrete.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VFrete := Valor;
  if Def('334', 'W09', DmNFe_0000.QrNFECabAICMSTot_vSeg.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VSeg := Valor;
  if Def('335', 'W10', DmNFe_0000.QrNFECabAICMSTot_vDesc.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VDesc := Valor;
  if Def('336', 'W11', DmNFe_0000.QrNFECabAICMSTot_vII.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VII := Valor;
  if Def('337', 'W12', DmNFe_0000.QrNFECabAICMSTot_vIPI.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VIPI := Valor;
  if Def('338', 'W13', DmNFe_0000.QrNFECabAICMSTot_vPIS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VPIS := Valor;
  if Def('339', 'W14', DmNFe_0000.QrNFECabAICMSTot_vCOFINS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VCOFINS := Valor;
  if Def('340', 'W15', DmNFe_0000.QrNFECabAICMSTot_vOutro.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VOutro := Valor;
  if Def('341', 'W16', DmNFe_0000.QrNFECabAICMSTot_vNF.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VNF := Valor;
  // 2013-05-07
  //if DmNFe_0000.QrFilialNFeNT2013_003LTT.Value > 0 then
  begin
    // 2017-01-30 In�cio
    //if DmNFe_0000.QrNFECabANFeNT2013_003LTT.Value > 0 then
    // 2017-01-30 Fim
    if UnNFe_PF.VerificaSeCalculaTributos(DmNFe_0000.QrNFECabANFeNT2013_003LTT.Value,
      DModG.QrPrmsEmpNFeNFe_indFinalCpl.Value, DmNFe_0000.QrNFECabAide_indFinal.Value) then
    begin
      if Def('341a', 'W16a', DmNFe_0000.QrNFECabAvTotTrib.Value, Valor) then
        cXML.InfNFe.Total.ICMSTot.vTotTrib := Valor;
    end;
  end;    
  // FIM 2013-05-07
  //
  (* TAG ISSQNTOT... Opcional *)
  (*  Parei aqui
  cXML.InfNFe.Total.ISSQNtot.VServ := Valor;
  cXML.InfNFe.Total.ISSQNtot.VBC := Valor;
  cXML.InfNFe.Total.ISSQNtot.VISS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VPIS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VCOFINS := Valor;

  (* TAG  RETTRIB... Opcional *)
  (*cXML.InfNFe.Total.RetTrib.VRetPIS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCOFINS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCSLL := Valor;
  cXML.InfNFe.Total.RetTrib.VBCIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VBCRetPrev := Valor;
  cXML.InfNFe.Total.RetTrib.VRetPrev := Valor;*)

(* X - Informa��es da TAG TRANSP... *)
      // '356', 'X01'  =  Grupo de informa��oes do transporte da NF-e
  if Def('357', 'X02', DmNFe_0000.QrNFECabAModFrete.Value, Valor) then
    cXML.InfNFe.Transp.ModFrete := Valor;
      // '358', 'X03'  =  Grupo do transportador
  if Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value) <> '' then
  begin
    if Def('359', 'X04', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CNPJ := Valor;
  end else begin
    if Def('360', 'X05', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CPF.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CPF := Valor;
  end;
  if Def('361', 'X06', DmNFe_0000.QrNFECabATransporta_XNome.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XNome := Valor;
  if Def('362', 'X07', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_IE.Value), Valor) then
    cXML.InfNFe.Transp.Transporta.IE := FMT_IE(Valor);
  if Def('363', 'X08', DmNFe_0000.QrNFECabATransporta_XEnder.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XEnder := Valor;
  if Def('364', 'X09', DmNFe_0000.QrNFECabATransporta_XMun.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XMun := Valor;
  if Def('365', 'X10', DmNFe_0000.QrNFECabATransporta_UF.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.UF := Valor;
  //     '366', 'X11'  = Grupo de retenc��o do ICMS do transporte
  (*
      PAREI AQUI
  if Def('367', 'X12', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VServ := Valor;
  if Def('368', 'X13', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VBCRet := Valor;
  if Def('369', 'X14', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.PICMSRet := Valor;
  if Def('370', 'X15', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VICMSRet := Valor;
  if Def('371', 'X16', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CFOP := Valor;
  if Def('372', 'X17', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CMunFG := Valor;*)

  (* Informa��es da TAG VEICTRANSP... Opcional *)

  //     '373', 'X18'  =  Grupo Ve�culo
  if (Trim(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value) <> '')
  and (Trim(DmNFe_0000.QrNFECabAVeicTransp_UF.Value) <> '') then
  begin
    if Def('374', 'X19', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value), Valor) then
      cXML.InfNFe.Transp.VeicTransp.Placa := Valor;
    if Def('375', 'X20', DmNFe_0000.QrNFECabAVeicTransp_UF.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.UF := Valor;
    if Def('376', 'X21', DmNFe_0000.QrNFECabAVeicTransp_RNTC.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.RNTC := Valor;

    //     '377', 'X22'  =  reboque(s)
(*
    DmNFe_0000.QrNFECabXReb.Close;
    DmNFe_0000.QrNFECabXReb.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFECabXReb.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFECabXReb.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFECabXReb. O p e n ;
*)
    DmNFe_0000.ReopenNFeCabXReb(FatID, FatNum, Empresa);
    //
    if DmNFe_0000.QrNFECabXReb.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFECabXReb.Eof do
      begin
        cRebLista := cXML.InfNFe.Transp.Reboque.Add;
        if Def('378', 'X23', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabXRebplaca.Value), Valor) then
          cRebLista.Placa := Valor;
        if Def('379', 'X24', DmNFe_0000.QrNFECabXRebUF.Value, Valor) then
          cRebLista.UF := Valor;
        if Def('380', 'X25', DmNFe_0000.QrNFECabXRebRNTC.Value, Valor) then
          cRebLista.RNTC := Valor;
        //
        DmNFe_0000.QrNFECabXReb.Next;
      end;
    end;
  end;
  if Def('380a', 'X25a', DmNFe_0000.QrNFECabAVagao.Value, Valor) then
    cXML.InfNFe.Transp.Vagao := Valor;
  if Def('380b', 'X25b', DmNFe_0000.QrNFECabABalsa.Value, Valor) then
    cXML.InfNFe.Transp.Balsa := Valor;
  //
(*
  DmNFe_0000.QrNFECabXVol.Close;
  DmNFe_0000.QrNFECabXVol.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabXVol.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabXVol.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabXVol. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabXVol(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabXVol.RecordCount > 0 then
  begin
    K := -1;
    //if DmNFe_0000.QrNFECabXVolqVol.Value > 0 then
    while not DmNFe_0000.QrNFECabXVol.Eof do
    begin
      //K := 1; (* Essa vari�vel tem que ser incrementada no caso do Valume possuir mais de um lacre...*)

      (* Informa��es da TAG VOLUMES... Opcional *)
      //     '381', 'X26'  =  Grupo volumes
      cVolLista := cXML.InfNFe.Transp.Vol.Add;
      K := K + 1;
      if Def('382', 'X27', DmNFe_0000.QrNFECabXVolqVol.Value, Valor) then
        cVolLista.QVol := Valor;
      if Def('383', 'X28', DmNFe_0000.QrNFECabXVolesp.Value, Valor) then
        cVolLista.Esp := Valor;
      if Def('384', 'X29', DmNFe_0000.QrNFECabXVolmarca.Value, Valor) then
        cVolLista.Marca := Valor;
      if Def('385', 'X30', DmNFe_0000.QrNFECabXVolnVol.Value, Valor) then
        cVolLista.NVol := Valor;
      if Def('386', 'X31', DmNFe_0000.QrNFECabXVolpesoL.Value, Valor) then
        cVolLista.PesoL := Valor;
      if Def('387', 'X32', DmNFe_0000.QrNFECabXVolpesoB.Value, Valor) then
        cVolLista.PesoB := Valor;

      (* Informa��es da TAG LACRES... Opcional *)
      //  '387a', 'X33'
(*
      DmNFe_0000.QrNFECabXLac.Close;
      DmNFe_0000.QrNFECabXLac.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFECabXLac.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFECabXLac.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFECabXLac.Params[03].AsInteger := DmNFe_0000.QrNFECabXVolControle.Value;
      DmNFe_0000.QrNFECabXLac. O p e n ;
*)
      Controle := DmNFe_0000.QrNFECabXVolControle.Value;
      DmNFe_0000.ReopenNFeCabXLac(FatID, FatNum, Empresa, Controle);
      //
      //if DmNFe_0000.QrNFECabXLac.RecordCount > 0 then
      while not DmNFe_0000.QrNFECabXLac.Eof do
      begin
        cLacLista := cXML.InfNFe.Transp.Vol.Items[K].Lacres.Add;
        if Def('388', 'X34', DmNFe_0000.QrNFECabXLacnLacre.Value, Valor) then
          cLacLista.NLacre := Valor;
        //
        DmNFe_0000.QrNFECabXLac.Next;
      end;

      DmNFe_0000.QrNFECabXVol.Next;
    end;
  end;

  (* Y - Informa��es da TAG COBR... se Houver *)
  //   //'389', 'Y01'  =  Grupo de cobran�a
  //   //'390', 'Y02'  =  Grupo da fatura
  if Def('391', 'Y03', DmNFe_0000.QrNFECabACobr_Fat_NFat  .Value, Valor) then
    cXML.InfNFe.Cobr.Fat.NFat := Valor;
  if Def('392', 'Y04', DmNFe_0000.QrNFECabACobr_Fat_vOrig.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VOrig := Valor;
  if Def('393', 'Y05', DmNFe_0000.QrNFECabACobr_Fat_vDesc.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VDesc := Valor;
  if Def('394', 'Y06', DmNFe_0000.QrNFECabACobr_Fat_vLiq.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VLiq := Valor;

  (* Informa��es da TAG DUP...*)
  //   '395', 'Y07'  =  Grupo da duplicata
(*
  DmNFe_0000.QrNFECabY.Close;
  DmNFe_0000.QrNFECabY.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabY.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabY.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabY. O p e n ;
*)
  DmNFe_0000.ReopenNFeCabY(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabY.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabY.Eof do
    begin
      if DmNFe_0000.QrNFECabYvDup.Value > 0 then
      begin
        cDupLista := cXML.InfNFe.Cobr.Dup.Add;
        //
        if Def('396', 'Y08', DmNFe_0000.QrNFECabYnDup.Value, Valor) then
          cDupLista.NDup := Valor;
        if Def('397', 'Y09', DmNFe_0000.QrNFECabYdVenc.Value, Valor) then
          cDupLista.DVenc := Valor;
        if Def('398', 'Y10', DmNFe_0000.QrNFECabYvDup.Value, Valor) then
          cDupLista.VDup := Valor;
      end;
      DmNFe_0000.QrNFECabY.Next;
    end;
  end;

(* Z - Informa��es da TAG INFADIC... se Houver *)
  //     '399', 'Z01' Grupo de informa��es adicionais
  if Def('400', 'Z02', DmNFe_0000.QrNFECabAInfAdic_InfAdFisco.Value, Valor) then
    cXML.InfNFe.InfAdic.InfAdFisco := Valor;
  //
  Valor := '';
  if Trim(DmNFe_0000.QrNFECabAInfAdic_InfCpl.Value) <> '' then
  Valor := DmNFe_0000.QrNFECabAInfAdic_InfCpl.Value;
  if Trim(DmNFe_0000.QrNFECabAInfCpl_totTrib.Value) <> '' then
    Valor := Trim(Valor + ' ' + DmNFe_0000.QrNFECabAInfCpl_totTrib.Value);
  if Def('401', 'Z03', Valor, Valor) then
  begin
    cXML.InfNFe.InfAdic.InfCpl := Valor;
  end;

  (* Informa��es da TAG ObsCont... *)
  //     '401a', 'Z04' Grupo do campo de uso livre do contribuinte
  DmNFe_0000.ReopenNFeCabZCon(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZCon.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZCon.Eof do
    begin
      cObsCont := cXML.InfNFe.InfAdic.ObsCont.Add;
      //
      if Def('401b', 'Z05', DmNFe_0000.QrNFECabZConxCampo.Value, Valor) then
        cObsCont.XCampo := Valor;
      if Def('401c', 'Z06', DmNFe_0000.QrNFECabZConxTexto.Value, Valor) then
        cObsCont.XTexto := Valor;
      //
      DmNFe_0000.QrNFECabZCon.Next;
    end;
  end;
  (* Informa��es da TAG ObsFisco... *)
  // '401d', 'Z07' Grupo Campo de uso livre do fisco
  DmNFe_0000.ReopenNFeCabZFis(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZFis.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZFis.Eof do
    begin
      cObsFisco := cXML.InfNFe.InfAdic.ObsFisco.Add;
      //
      if Def('401e', 'Z08', DmNFe_0000.QrNFECabZFisxCampo.Value, Valor) then
        cObsFisco.XCampo := Valor;
      if Def('401f', 'Z09', DmNFe_0000.QrNFECabZFisxTexto.Value, Valor) then
        cObsFisco.XTexto := Valor;
      //
      DmNFe_0000.QrNFECabZFis.Next;
    end;
  end;

  (* Informa��es da TAG ProcRef... *)
  // '401g', 'Z10'  =  Grupo de processo referenciado
  DmNFe_0000.ReopenNFeCabZPro(FatID, FatNum, Empresa);
  //
  if DmNFe_0000.QrNFECabZPro.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabZPro.Eof do
    begin
      cProcRefLista := cXML.InfNFe.InfAdic.ProcRef.Add;
      //
      if Def('401h', 'Z11', DmNFe_0000.QrNFECabZPronProc.Value, Valor) then
        cProcRefLista.nProc := Valor;
      if Def('401i', 'Z12', DmNFe_0000.QrNFECabZProindProc.Value, Valor) then
        cProcRefLista.indProc := Valor;
      //
      DmNFe_0000.QrNFECabZPro.Next;
    end;
  end;

  (* ZA - Informa��es da TAG EXPORTA... se Houver *)
  //  '402', 'ZA01'
  if (Trim(DmNFe_0000.QrNFECabAExporta_UFEmbarq.Value) <> '')
  or (Trim(DmNFe_0000.QrNFECabAExporta_XLocEmbarq.Value) <> '') then
  begin
    if Def('403', 'ZA02', DmNFe_0000.QrNFECabAExporta_UFEmbarq.Value, Valor) then
      cXML.InfNFe.Exporta.UFSaidaPais := Valor;
    if Def('404', 'ZA03', DmNFe_0000.QrNFECabAExporta_XLocEmbarq.Value, Valor) then
      cXML.InfNFe.Exporta.XLocExporta := Valor;
    if Def('404a', 'ZA04', DmNFe_0000.QrNFECabAExporta_XLocDespacho.Value, Valor) then
      cXML.InfNFe.Exporta.XLocDespacho := Valor;
  end;
  (* ZB - Informa��es da TAG COMPRA... se Houver *)
  // '405', 'ZB01'
  // Habilitado em 2011-09-18
  //2016/01/15 17:11 => erro desconhecido est� fazendo mesmo com tag vazia
  if Trim(DmNFe_0000.QrNFECabACompra_XNEmp.Value +
    DmNFe_0000.QrNFECabACompra_XPed.Value +
    DmNFe_0000.QrNFECabACompra_XCont.Value) <> '' then
  begin
    if Def('406', 'ZB02', DmNFe_0000.QrNFECabACompra_XNEmp.Value, Valor) then
      cXML.InfNFe.Compra.XNEmp := Valor;
    if Def('407', 'ZB03', DmNFe_0000.QrNFECabACompra_XPed.Value, Valor) then
      cXML.InfNFe.Compra.XPed := Valor;
    if Def('408', 'ZB04', DmNFe_0000.QrNFECabACompra_XCont.Value, Valor) then
      cXML.InfNFe.Compra.XCont := Valor;
  end;
  // Fim 2011-09-18
  //

  (* ZC - Informa��es da assinatura digital
  // '409', 'ZC01'  -  Assinatura XML da NF-e  segundo o padr�o XML Digital signature
  *)

  //
  Result := True;
end;

function TFmNFeGeraXML_0310.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo,
  Serie, nNFIni, nNFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + Ano + emitCNPJ + Modelo + StrZero(StrToInt(Serie),3,0) +
    StrZero(StrToInt(nNFIni),9,0) + StrZero(StrToInt(nNFFim),9,0);
  K := Length(Id);
  if K = 43 then
    Result := True
  else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 43 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeRetRecepcao(UFServico: String; Ambiente,
  CodigoUF: Byte; Recibo: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  //TipoConsumo = tcwsConsultaLote;
  TipoConsumo = tcwsRetAutorizacao;
var
  sAviso, VersaoDados, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Screen.Cursor := crHourGlass;
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsReciNFe(Ambiente, Recibo);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'NfeRetAutorizacao';
  //ProcedimentoRet abaixo!

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);
    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);
    if pos('nfeRetAutorizacaoResult', StrStream.DataString) > 0 then
      ProcedimentoRet := 'nfeRetAutorizacaoResult'
    else
      ProcedimentoRet := 'nfeRetAutorizacaoLoteResult';
    //
    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeConsultaCadastro(Servico_UF, Contribuinte_UF,
  Contribuinte_CNPJ: String; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsConsultaCadastro;
  Ambiente = 1; // Produ��o
var
  Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso, VersaoDados: String;
  CodigoUF: Integer;
begin
  Screen.Cursor := crHourGlass;
  CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Servico_UF);
  //
  //if not ObtemWebServer(Servico_UF, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  FDadosTxt := XML_ConsCad(Contribuinte_UF, Contribuinte_CNPJ);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
  ProcedimentoEnv := 'CadConsultaCadastro2';
  ProcedimentoRet := 'consultaCadastro2Result';

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsCad_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Cadastro Contribuinte:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeConsultaDistDFeInt(Servico_UF,
  Interesse_UF: String; TpAmb: Integer; CNPJ, CPF, ultNSU, NSU,
  Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit; ItemStr: String): String;
const
  TipoConsumo = tcwsDistDFeInt;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso: String;
  CodigoUF: Integer;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;
  //CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Interesse_UF);
  //
  //if not ObtemWebServer(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_DistDFEInt(TpAmb, Interesse_UF, CNPJ, CPF, ultNSU, NSU);
  //
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  //
  DmNFe_0000.SalvaXML(NFE_EXT_PED_DFE_DIS_INT_XML, ItemStr, FDadosTxt, (*RETxtEnvio*)nil, False);
  //
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto  + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto  +   '<soap12:Body>';
  Texto := Texto  +    '<nfeDistDFeInteresse xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe">';
  Texto := Texto  +      '<nfeDadosMsg>' + FDadosTxt + '</nfeDadosMsg>';
  Texto := Texto  +    '</nfeDistDFeInteresse>';
  Texto := Texto  +  '</soap12:Body>';
  Texto := Texto  + '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe';
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString,'nfeDistDFeInteresseResult');
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Distribui��o DFe de Interesse:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeConsultaNF(UFServico: String; Ambiente, CodigoUF: Byte;
  ChaveNFe: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
  TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsConsultaNFe;
var
  sAviso, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConNFe.Value, 2, siNegativo);

  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsSitNFe(Ambiente, ChaveNFe, VersaoDados);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;


  if pos('NFECONSULTA2', UpperCase(FURL)) > 0 then
  begin
    ProcedimentoEnv := 'NfeConsulta2';
    ProcedimentoRet := 'nfeConsultaNF2Result';
  end else
  begin
    ProcedimentoEnv := 'NfeConsulta3';
    ProcedimentoRet := 'nfeConsultaNFResult';
  end;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';

 //TODO :       VER AQUI SE NAO FUNCIONAR CONSULTA NFE!
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
       RETxtEnvio.Text :=  Acao.Text;
       EdWS.Text := FURL;
       ReqResp.Execute(Acao.Text, Stream);
       StrStream := TStringStream.Create('');
       StrStream.CopyFrom(Stream, 0);
       Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeConsultaNFDest(Servico_UF, Destinatario_UF,
VersaoAcao: String; TpAmb: Integer; CNPJ: String;
indNFe, indEmi, ultNSU: String;
Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
EdWS: TEdit): String;
const
  TipoConsumo = tcwsConsultaNFeDest;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso: String;
  CodigoUF: Integer;
begin
{
  Screen.Cursor := crHourGlass;
  CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Destinatario_UF);
  //
  //if not ObtemWebServer(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsNFeDest(VersaoAcao, TpAmb, CNPJ, indNFe, indEmi, ultNSU);
  //
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeConsultaDest">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + verConsNFeDest_Versao + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeConsultaDest">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeConsultaDest';
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := ??? SeparaDados(StrStream.DataString,'nfeConsultaNFDestResult');
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Cadastro Contribuinte:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
}
end;

function TFmNFeGeraXML_0310.WS_NFeDownloadNFeConfirmadas(Servico_UF,
  Destinatario_UF: String; TpAmb: Integer; CNPJ: String;
  Chaves: TStrings; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsNFeDownloadNF;
var
  Texto, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso: String;
  CodigoUF: Integer;
begin
  Screen.Cursor := crHourGlass;
  CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Destinatario_UF);
  //
  VersaoDados := Geral.FFT_Dot(1, 2, siNegativo);//Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDowNFe.Value, 2, siNegativo);
  //
  //if not ObtemWebServer(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_NfeDownloadNF(VersaoDados, TpAmb, CNPJ, Chaves);
  //
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  //
  ProcedimentoEnv := 'NfeDownloadNF';
  ProcedimentoRet := 'nfeDownloadNFResult';


  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF/nfeDownloadNF';
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Cadastro Contribuinte:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeDownloadNFeDestinadas(Servico_UF,
  Destinatario_UF, VersaoAcao: String; TpAmb: Integer; CNPJ: String;
  Chaves: TStrings; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
(*&
const
  TipoConsumo = tcwsDownloadNFeDest;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  // Meu
  sAviso: String;
  CodigoUF: Integer;
&*)
begin
(*&
  Screen.Cursor := crHourGlass;
  CodigoUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Destinatario_UF);
  //
  //if not ObtemWebServer(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(Servico_UF, tpAmb, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_DowNFeDest(VersaoAcao, TpAmb, CNPJ, Chaves);
  //
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + verDowNFeDest_Versao + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF/nfeDownloadNF';
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := ??? SeparaDados(StrStream.DataString,'nfeDownloadNFResult');
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Cadastro Contribuinte:' +
                              sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
&*)
end;

function TFmNFeGeraXML_0310.WS_NFeInutilizacaoNFe(UFServico: String; Ambiente,
  CodigoUF, Ano: Byte; Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
  XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsPediInutilizacao;
var
  sAviso: String;
var
  Texto, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_NFeInutNFe(Id, Ambiente, CodigoUF, Ano, CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if NFeXMLGeren.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
    begin
      Acao := TStringList.Create;
      Stream := TMemoryStream.Create;

      VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);

      if pos('NFEINUTILIZACAO2', UpperCase(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeInutilizacao2';
        ProcedimentoRet := 'nfeInutilizacaoNF2Result';
      end else
      begin
        ProcedimentoEnv := 'NfeInutilizacao3';
        ProcedimentoRet := 'nfeInutilizacaoNFResult';
      end;

      Texto := '<?xml version="1.0" encoding="utf-8"?>';
      Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
      Texto := Texto +   '<soap12:Header>';
      //Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2">';
      Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
      //Texto := Texto +       '<versaoDados>' + verNFeInutNFe_Versao + '</versaoDados>';
      Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
      Texto := Texto +     '</nfeCabecMsg>';
      Texto := Texto +   '</soap12:Header>';
      Texto := Texto +   '<soap12:Body>';
      //Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2">';
      Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto + FAssinTxt;
      Texto := Texto +     '</nfeDadosMsg>';
      Texto := Texto +   '</soap12:Body>';
      Texto := Texto +'</soap12:Envelope>';

      Acao.Text := Texto;
      Acao.SaveToStream(Stream);

      ReqResp := THTTPReqResp.Create(nil);
      ConfiguraReqResp( ReqResp );
      ReqResp.URL := FURL;
      ReqResp.UseUTF8InHeader := True;
      //ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2';
      ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
      try
        RETxtEnvio.Text :=  Acao.Text;
        EdWS.Text := FURL;
        ReqResp.Execute(Acao.Text, Stream);
        StrStream := TStringStream.Create('');
        StrStream.CopyFrom(Stream, 0);
        //Result := SeparaDados(StrStream.DataString,'nfeInutilizacaoNF2Result');
        Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
      finally
        Acao.Free;
        Stream.Free;
      end;
    end;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc): String;
var
  sAviso: String;
  //
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  I: Integer;
  XML_STR: String;
  //
  VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  TipoConsumo: TTipoConsumoWS;
begin
  Screen.Cursor := crHourGlass;
  //
  TipoConsumo := tcwsAutorizacao;
  //VersaoDados :=  verEnviNFeNovo_Versao;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := FmNFeSteps_0310.FXML_LoteNFe;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value, 2, siNegativo);

(*
  if pos('NFE...', UpperCase(FURL)) > 0 then
  begin
    ProcedimentoEnv := '';
    ProcedimentoRet := '';
  end else
  begin
*)
    // Cuidado! O PR aceitou 'NFeAutorizacao' mas o MS nao!
    ProcedimentoEnv := 'NfeAutorizacao';
    ProcedimentoRet := 'nfeAutorizacaoLoteResult';
(*
  end;
*)

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';


  XML_STR := '';
  for i := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
      XML_STR := XML_STR + Texto[I]
  end;
  Texto := XML_STR;
  //
  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);

    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);

    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_NFeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit): String;
const
  TipoConsumo = tcwsStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);

  if pos('NFESTATUSSERVICO2', UPPERCASE(FURL)) > 0 then
  begin
    ProcedimentoEnv := 'NfeStatusServico2';
    ProcedimentoRet := 'nfeStatusServicoNF2Result';
  end else
  begin
    ProcedimentoEnv := 'NfeStatusServico3';
    ProcedimentoRet := 'nfeStatusServicoNFResult';
  end;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;

     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         //Result := ??? SeparaDados(StrStream.DataString,'nfeStatusServicoNF2Result');
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Status servi�o:' + sLineBreak +
                              '- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.WS_RecepcaoEvento(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsRecepcaoEvento;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := FmNFeSteps_0310.FXML_LoteEvento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">';
  Texto := Texto +        '<versaoDados>' + verEnviEvento_Versao + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</nfeCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">' + FDadosTxt + '</nfeDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/nfeRecepcaoEvento';
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString,'nfeRecepcaoEventoResult');
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmNFeGeraXML_0310.ObtemWebServer(UFServico: String; Ambiente,
  CodigoUF: Integer; Acao: TTipoConsumoWS; sAviso: String; LaAviso1, LaAviso2:
  TLabel): Boolean;
  //
  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
      1: Result := 'Produ��o';
      2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;

  function TextoAcao(Acao: TTipoConsumoWS): String;
  begin
    case Acao of
      tcwsStatusServico:    Result := 'Status do servi�o';
      //tcwsEnviaLoteNF:      Result := 'Enviar lote de NF-e ao fisco';
      //tcwsConsultaLote:     Result := 'Consultar lote enviado';
      tcwsPedeCancelamento: Result := 'Pedir cancelamento de NF-e';
      tcwsPediInutilizacao: Result := 'Pedir inutiliza��o de n�mero(s) de NF-e';
      tcwsConsultaNFe:      Result := 'Consultar NF-e';
      tcwsRecepcaoEvento:   Result := 'Enviar lote de evento de NF-e ao fisco';
      tcwsConsultaCadastro: Result := 'Consulta cadastro de Contribuinte';
      // Aqui vai Consulta situa�ao nfe
      tcwsConsultaNFeDest:  Result := 'Consulta NFe-s destinadas';
      // Substitu�do por tcwsNFeDownloadNF ???
      //tcwsDownloadNFeDest:  Result := 'Download de NFe destinadas confirmadas';
      tcwsNFeDownloadNF:    Result := 'Download de NF-e';
      tcwsAutorizacao:      Result := 'Envio Lote NF-e (novo 3.10)';
      tcwsRetAutorizacao:   Result := 'Consulta Recibo Lote (novo 3.10)';
      tcwsDistDFeInt:       Result := 'Distribui��o de DFe de Interesse';
       else Result := '[Desconhecido]';
    end;
  end;

  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS): Byte;
  begin
    case TipoConsumoWS of
      tcwsStatusServico:    Result := 0;
      //tcwsEnviaLoteNF:      Result := 1;
      //tcwsConsultaLote:     Result := 2;
      tcwsPedeCancelamento: Result := 3;
      tcwsPediInutilizacao: Result := 4;
      tcwsConsultaNFe:      Result := 5;
      tcwsRecepcaoEvento:   Result := 6;
      tcwsConsultaCadastro: Result := 7;
      // Aqui vai Consulta situa�ao nfe
      tcwsConsultaNFeDest:  Result := 9;
      //tcwsDownloadNFeDest:  Result := 10;
      tcwsNFeDownloadNF:    Result := 10;
      tcwsAutorizacao:      Result := 11;
      tcwsRetAutorizacao:   Result := 12;
      tcwsDistDFeInt:       Result := 13;
      else                  Result := Null;
    end;
  end;
var
  cUF, A: Integer;
  Versao, x: String;
begin
  //FWSDL := '';
  FURL  := '';
  // Consulta a Web Services
  // http://www.nfe.fazenda.gov.br/portal/WebServices.aspx
  A := TipoConsumoWs2Acao(Acao);
  cUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  {
  I := (cUF * 100) + (A * 10) + Ambiente;
  }
  x := Uppercase(UFServico + ' '+ NomeAcao(Acao) + ' ' + VersaoWS(Acao, False));
  if Ambiente = 1 then // Produ��o
  begin
//Lista atualizada em 28/10/2014

// Sefaz Amazonas - (AM)
// Servi�o Vers�o URL
if x = Uppercase('AM RecepcaoEvento 1.00') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/RecepcaoEvento' else
if x = Uppercase('AM NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRecepcao2' else
if x = Uppercase('AM NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetRecepcao2' else
if x = Uppercase('AM NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2' else
if x = Uppercase('AM NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeConsulta2' else
if x = Uppercase('AM NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeStatusServico2' else
if x = Uppercase('AM NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2' else
if x = Uppercase('AM NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeAutorizacao' else
if x = Uppercase('AM NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Bahia - (BA)
// Servi�o Vers�o URL
if x = Uppercase('BA NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeRecepcao2.asmx' else
if x = Uppercase('BA NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeRetRecepcao2.asmx' else
if x = Uppercase('BA NfeInutilizacao 2.00') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/nfeinutilizacao2.asmx' else
if x = Uppercase('BA NfeConsultaProtocolo 2.00') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/nfeconsulta2.asmx' else
if x = Uppercase('BA NfeStatusServico 2.00') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/NfeStatusServico2.asmx' else
if x = Uppercase('BA NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx' else
if x = Uppercase('BA RecepcaoEvento 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx' else
if x = Uppercase('BA NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/NfeInutilizacao/NfeInutilizacao.asmx' else
if x = Uppercase('BA NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/NfeConsulta/NfeConsulta.asmx' else
if x = Uppercase('BA NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/NfeStatusServico/NfeStatusServico.asmx' else
if x = Uppercase('BA NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('BA NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Cear� - (CE)
// Servi�o Vers�o URL
if x = Uppercase('CE RecepcaoEvento 1.00') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento?wsdl' else
if x = Uppercase('CE NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRecepcao2?wsdl' else
if x = Uppercase('CE NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetRecepcao2?wsdl' else
if x = Uppercase('CE NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2?wsdl' else
if x = Uppercase('CE NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeConsulta2?wsdl' else
if x = Uppercase('CE NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2?wsdl' else
if x = Uppercase('CE NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2?wsdl' else
if x = Uppercase('CE NfeDownloadNF 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF?wsdl' else
if x = Uppercase('CE NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao?wsdl' else
if x = Uppercase('CE NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Goias - (GO)
// Servi�o Vers�o URL
if x = Uppercase('GO RecepcaoEvento 1.00') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento?wsdl' else
if x = Uppercase('GO NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRecepcao2?wsdl' else
if x = Uppercase('GO NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetRecepcao2?wsdl' else
if x = Uppercase('GO NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2?wsdl' else
if x = Uppercase('GO NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2?wsdl' else
if x = Uppercase('GO NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2?wsdl' else
if x = Uppercase('GO NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2?wsdl' else
if x = Uppercase('GO NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl' else
if x = Uppercase('GO NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sef Minas Gerais - (MG)
// Servi�o Vers�o URL
if x = Uppercase('MG NfeRecepcao 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRecepcao2' else
if x = Uppercase('MG NfeRetRecepcao 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetRecepcao2' else
if x = Uppercase('MG NfeInutilizacao 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2' else
if x = Uppercase('MG NfeConsultaProtocolo 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2' else
if x = Uppercase('MG NfeStatusServico 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeStatus2' else
if x = Uppercase('MG NfeConsultaCadastro 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/cadconsultacadastro2' else
if x = Uppercase('MG RecepcaoEvento 2.00') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento' else
if x = Uppercase('MG NFeAutorizacao 3.10') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao' else
if x = Uppercase('MG NFeRetAutorizacao 3.10') then FURL := 'https://nfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Maranh�o - (MA)
// Servi�o Vers�o URL
if x = Uppercase('MA NfeConsultaCadastro 2.00') then FURL := 'https://sistemas.sefaz.ma.gov.br/wscadastro/CadConsultaCadastro2?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Mato Grosso do Sul - (MS)
// Servi�o Vers�o URL
if x = Uppercase('MS RecepcaoEvento 1.00') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/RecepcaoEvento' else
if x = Uppercase('MS NfeRecepcao 2.00') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRecepcao2' else
if x = Uppercase('MS NfeRetRecepcao 2.00') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetRecepcao2' else
if x = Uppercase('MS NfeConsultaCadastro 2.00') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/CadConsultaCadastro2' else
if x = Uppercase('MS NfeInutilizacao 3.10') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeInutilizacao2' else
if x = Uppercase('MS NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeConsulta2' else
if x = Uppercase('MS NfeStatusServico 3.10') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeStatusServico2' else
if x = Uppercase('MS NFeAutorizacao 3.10') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeAutorizacao' else
if x = Uppercase('MS NFeRetAutorizacao 3.10') then FURL := 'https://nfe.fazenda.ms.gov.br/producao/services2/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Mato Grosso - (MT)
// Servi�o Vers�o URL
if x = Uppercase('MT NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRecepcao2?wsdl' else
if x = Uppercase('MT NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetRecepcao2?wsdl' else
if x = Uppercase('MT NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao2?wsdl' else
if x = Uppercase('MT NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta2?wsdl' else
if x = Uppercase('MT NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico2?wsdl' else
if x = Uppercase('MT NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2?wsdl' else
if x = Uppercase('MT RecepcaoEvento 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento?wsdl' else
if x = Uppercase('MT NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl' else
if x = Uppercase('MT NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Pernambuco - (PE)
// Servi�o Vers�o URL
if x = Uppercase('PE RecepcaoEvento 1.00') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento' else
if x = Uppercase('PE NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRecepcao2' else
if x = Uppercase('PE NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetRecepcao2' else
if x = Uppercase('PE NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2' else
if x = Uppercase('PE NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2' else
if x = Uppercase('PE NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2' else
if x = Uppercase('PE NfeConsultaCadastro 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/CadConsultaCadastro2' else
if x = Uppercase('PE NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl' else
if x = Uppercase('PE NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Paran� - (PR)
// Servi�o Vers�o URL
if x = Uppercase('PR NfeRecepcao 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/NFeRecepcao2?wsdl' else
if x = Uppercase('PR NfeRetRecepcao 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/NFeRetRecepcao2?wsdl' else
if x = Uppercase('PR NfeInutilizacao 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/NFeInutilizacao2?wsdl' else
if x = Uppercase('PR NfeConsultaProtocolo 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/NFeConsulta2?wsdl' else
if x = Uppercase('PR NfeStatusServico 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/NFeStatusServico2?wsdl' else
if x = Uppercase('PR NfeConsultaCadastro 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe/CadConsultaCadastro2?wsdl' else
if x = Uppercase('PR RecepcaoEvento 2.00') then FURL := 'https://nfe2.fazenda.pr.gov.br/nfe-evento/NFeRecepcaoEvento?wsdl' else
if x = Uppercase('PR NfeInutilizacao 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeInutilizacao3?wsdl' else
if x = Uppercase('PR NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeConsulta3?wsdl' else
if x = Uppercase('PR NfeStatusServico 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeStatusServico3?wsdl' else
if x = Uppercase('PR NfeConsultaCadastro 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/CadConsultaCadastro2?wsdl' else
if x = Uppercase('PR RecepcaoEvento 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento?wsdl' else
if x = Uppercase('PR NFeAutorizacao 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl' else
if x = Uppercase('PR NFeRetAutorizacao 3.10') then FURL := 'https://nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Rio Grande do Sul - (RS)
// Servi�o Vers�o URL
if x = Uppercase('RS RecepcaoEvento 1.00') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('RS NfeRecepcao 2.00') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('RS NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('RS NfeConsultaCadastro 2.00') then FURL := 'https://sef.sefaz.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx' else
if x = Uppercase('RS NfeConsultaDest 2.00') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/nfeConsultaDest/nfeConsultaDest.asmx' else
if x = Uppercase('RS NfeDownloadNF 2.00') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/nfeDownloadNF/nfeDownloadNF.asmx' else
if x = Uppercase('RS NfeInutilizacao 3.10') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeInutilizacao/NfeInutilizacao2.asmx' else
if x = Uppercase('RS NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('RS NfeStatusServico 3.10') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('RS NFeAutorizacao 3.10') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('RS NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz S�o Paulo - (SP)
// Servi�o Vers�o URL
if x = Uppercase('SP NfeRecepcao 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nferecepcao2.asmx' else
if x = Uppercase('SP NfeRetRecepcao 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nferetrecepcao2.asmx' else
if x = Uppercase('SP NfeInutilizacao 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nfeinutilizacao2.asmx' else
if x = Uppercase('SP NfeConsultaProtocolo 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nfeconsulta2.asmx' else
if x = Uppercase('SP NfeStatusServico 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nfestatusservico2.asmx' else
if x = Uppercase('SP NfeConsultaCadastro 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/nfeweb/services/cadconsultacadastro2.asmx' else
if x = Uppercase('SP RecepcaoEvento 2.00') then FURL := 'https://nfe.fazenda.sp.gov.br/eventosWEB/services/RecepcaoEvento.asmx' else
if x = Uppercase('SP NfeInutilizacao 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx' else
if x = Uppercase('SP NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/nfeconsulta2.asmx' else
if x = Uppercase('SP NfeStatusServico 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/nfestatusservico2.asmx' else
if x = Uppercase('SP NfeConsultaCadastro 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/cadconsultacadastro2.asmx' else
if x = Uppercase('SP RecepcaoEvento 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx' else
if x = Uppercase('SP NFeAutorizacao 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx' else
if x = Uppercase('SP NFeRetAutorizacao 3.10') then FURL := 'https://nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual Ambiente Nacional - (SVAN)
// Servi�o Vers�o URL
if x = Uppercase('SVAN RecepcaoEvento 1.00') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SVAN NfeRecepcao 2.00') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SVAN NfeRetRecepcao 2.00') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVAN NfeInutilizacao 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx' else
if x = Uppercase('SVAN NfeConsultaProtocolo 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SVAN NfeStatusServico 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SVAN NfeDownloadNF 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
if x = Uppercase('SVAN NFeAutorizacao 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SVAN NFeRetAutorizacao 3.10') then FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual Rio Grande do Sul - (SVRS)
// Servi�o Vers�o URL
if x = Uppercase('SVRS RecepcaoEvento 1.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('SVRS NfeRecepcao 2.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('SVRS NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVRS NfeConsultaCadastro 2.00') then FURL := 'https://svp-ws.sefazvirtual.rs.gov.br/ws/CadConsultaCadastro/CadConsultaCadastro2.asmx' else
if x = Uppercase('SVRS NfeInutilizacao 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx' else
if x = Uppercase('SVRS NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('SVRS NfeStatusServico 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('SVRS NFeAutorizacao 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('SVRS NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Conting�ncia Ambiente Nacional - (SCAN)
// Servi�o Vers�o URL
if x = Uppercase('SCAN RecepcaoEvento 1.00') then FURL := 'https://www.scan.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SCAN NfeRecepcao 2.00') then FURL := 'https://www.scan.fazenda.gov.br/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SCAN NfeRetRecepcao 2.00') then FURL := 'https://www.scan.fazenda.gov.br/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SCAN NfeInutilizacao 3.10') then FURL := 'https://www.scan.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx' else
if x = Uppercase('SCAN NfeConsultaProtocolo 3.10') then FURL := 'https://www.scan.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SCAN NfeStatusServico 3.10') then FURL := 'https://www.scan.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SCAN NFeAutorizacao 3.10') then FURL := 'https://www.scan.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SCAN NFeRetAutorizacao 3.10') then FURL := 'https://www.scan.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN)
// Servi�o Vers�o URL
if x = Uppercase('SVC-AN RecepcaoEvento 1.00') then FURL := 'https://www.svc.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SVC-AN NfeRecepcao 2.00') then FURL := 'https://www.svc.fazenda.gov.br/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SVC-AN NfeRetRecepcao 2.00') then FURL := 'https://www.svc.fazenda.gov.br/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVC-AN NfeConsultaProtocolo 3.10') then FURL := 'https://www.svc.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SVC-AN NfeStatusServico 3.10') then FURL := 'https://www.svc.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SVC-AN NFeAutorizacao 3.10') then FURL := 'https://www.svc.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SVC-AN NFeRetAutorizacao 3.10') then FURL := 'https://www.svc.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS)
// Servi�o Vers�o URL
if x = Uppercase('SVC-RS RecepcaoEvento 1.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('SVC-RS NfeRecepcao 2.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('SVC-RS NfeRetRecepcao 2.00') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVC-RS NfeConsultaProtocolo 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('SVC-RS NfeStatusServico 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('SVC-RS NFeAutorizacao 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('SVC-RS NFeRetAutorizacao 3.10') then FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Ambiente Nacional - (AN)
// Servi�o Vers�o URL
if x = Uppercase('AN RecepcaoEvento 1.00') then FURL := 'https://www.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('AN NfeConsultaDest 3.10') then FURL := 'https://www.nfe.fazenda.gov.br/NFeConsultaDest/NFeConsultaDest.asmx' else
if x = Uppercase('AN NfeDownloadNF 1.00') then FURL := 'https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
//if x = Uppercase('AN NfeDownloadNF 2.00') then FURL := 'https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
//if x = Uppercase('AN NfeDownloadNF 3.10') then FURL := 'https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
// Voltar ao topo da p�gin
//
//
    FURL := '';
  end else begin  // Homologa��o  > http://hom.nfe.fazenda.gov.br/portal/WebServices.aspx
                                 // http://hom.nfe.fazenda.gov.br/portal/webServices.aspx?tipoConteudo=Wak0FwB7dKs=

// Sefaz Amazonas - (AM)
// Servi�o Vers�o URL
if x = Uppercase('AM RecepcaoEvento 1.00') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/RecepcaoEvento' else
if x = Uppercase('AM NfeRecepcao 2.00') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeRecepcao2' else
if x = Uppercase('AM NfeRetRecepcao 2.00') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeRetRecepcao2' else
if x = Uppercase('AM NfeInutilizacao 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeInutilizacao2' else
if x = Uppercase('AM NfeConsultaProtocolo 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeConsulta2' else
if x = Uppercase('AM NfeStatusServico 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeStatusServico2' else
if x = Uppercase('AM NfeConsultaCadastro 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/cadconsultacadastro2' else
if x = Uppercase('AM NFeAutorizacao 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeAutorizacao' else
if x = Uppercase('AM NFeRetAutorizacao 3.10') then FURL := 'https://homnfe.sefaz.am.gov.br/services2/services/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Bahia - (BA)
// Servi�o Vers�o URL
if x = Uppercase('BA NfeRecepcao 2.00') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/NfeRecepcao2.asmx' else
if x = Uppercase('BA NfeRetRecepcao 2.00') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/NfeRetRecepcao2.asmx' else
if x = Uppercase('BA NfeInutilizacao 2.00') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/nfeinutilizacao2.asmx' else
if x = Uppercase('BA NfeConsultaProtocolo 2.00') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/nfeconsulta2.asmx' else
if x = Uppercase('BA NfeStatusServico 2.00') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/NfeStatusServico2.asmx' else
if x = Uppercase('BA NfeConsultaCadastro 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/nfenw/CadConsultaCadastro2.asmx' else
if x = Uppercase('BA RecepcaoEvento 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx' else
if x = Uppercase('BA NfeInutilizacao 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/NfeInutilizacao/NfeInutilizacao.asmx' else
if x = Uppercase('BA NfeConsultaProtocolo 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/NfeConsulta/NfeConsulta.asmx' else
if x = Uppercase('BA NfeStatusServico 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/NfeStatusServico/NfeStatusServico.asmx' else
if x = Uppercase('BA NFeAutorizacao 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('BA NFeRetAutorizacao 3.10') then FURL := 'https://hnfe.sefaz.ba.gov.br/webservices/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Cear� - (CE)
// Servi�o Vers�o URL
if x = Uppercase('CE RecepcaoEvento 1.00') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento?wsdl' else
if x = Uppercase('CE NfeRecepcao 2.00') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRecepcao2?wsdl' else
if x = Uppercase('CE NfeRetRecepcao 2.00') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRetRecepcao2?wsdl' else
if x = Uppercase('CE NfeInutilizacao 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeInutilizacao2?wsdl' else
if x = Uppercase('CE NfeConsultaProtocolo 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeConsulta2?wsdl' else
if x = Uppercase('CE NfeStatusServico 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeStatusServico2?wsdl' else
if x = Uppercase('CE NfeConsultaCadastro 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/CadConsultaCadastro2?wsdl' else
if x = Uppercase('CE NfeDownloadNF 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF?wsdl' else
if x = Uppercase('CE NFeAutorizacao 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeAutorizacao?wsdl' else
if x = Uppercase('CE NFeRetAutorizacao 3.10') then FURL := 'https://nfeh.sefaz.ce.gov.br/nfe2/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Goias - (GO)
// Servi�o Vers�o URL
if x = Uppercase('GO RecepcaoEvento 1.00') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento?wsdl' else
if x = Uppercase('GO NfeRecepcao 2.00') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRecepcao2?wsdl' else
if x = Uppercase('GO NfeRetRecepcao 2.00') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetRecepcao2?wsdl' else
if x = Uppercase('GO NfeInutilizacao 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeInutilizacao2?wsdl' else
if x = Uppercase('GO NfeConsultaProtocolo 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2?wsdl' else
if x = Uppercase('GO NfeStatusServico 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeStatusServico2?wsdl' else
if x = Uppercase('GO NfeConsultaCadastro 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2?wsdl' else
if x = Uppercase('GO NFeAutorizacao 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeAutorizacao?wsdl' else
if x = Uppercase('GO NFeRetAutorizacao 3.10') then FURL := 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sef Minas Gerais - (MG)
// Servi�o Vers�o URL
if x = Uppercase('MG RecepcaoEvento 1.00') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento' else
if x = Uppercase('MG NfeRecepcao 2.00') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRecepcao2' else
if x = Uppercase('MG NfeRetRecepcao 2.00') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRetRecepcao2' else
if x = Uppercase('MG NfeInutilizacao 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeInutilizacao2' else
if x = Uppercase('MG NfeConsultaProtocolo 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeConsulta2' else
if x = Uppercase('MG NfeStatusServico 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeStatusServico2' else
if x = Uppercase('MG NfeConsultaCadastro 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/cadconsultacadastro2' else
if x = Uppercase('MG NFeAutorizacao 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeAutorizacao' else
if x = Uppercase('MG NFeRetAutorizacao 3.10') then FURL := 'https://hnfe.fazenda.mg.gov.br/nfe2/services/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Maranh�o - (MA)
// Servi�o Vers�o URL
if x = Uppercase('MA NfeConsultaCadastro 2.0') then FURL := 'https://sistemas.sefaz.ma.gov.br/wscadastro/CadConsultaCadastro2?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Mato Grosso do Sul - (MS)
// Servi�o Vers�o URL
if x = Uppercase('MS RecepcaoEvento 1.00') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/RecepcaoEvento' else
if x = Uppercase('MS NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRecepcao2' else
if x = Uppercase('MS NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRetRecepcao2' else
if x = Uppercase('MS NfeConsultaCadastro 2.00') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/CadConsultaCadastro2' else
if x = Uppercase('MS NfeInutilizacao 3.10') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeInutilizacao2' else
if x = Uppercase('MS NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeConsulta2' else
if x = Uppercase('MS NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeStatusServico2' else
if x = Uppercase('MS NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeAutorizacao' else
if x = Uppercase('MS NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeRetAutorizacao' else
// Voltar ao topo da p�gina
// Sefaz Mato Grosso - (MT)
// Servi�o Vers�o URL
if x = Uppercase('MT NfeRecepcao 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRecepcao2?wsdl' else
if x = Uppercase('MT NfeRetRecepcao 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRetRecepcao2?wsdl' else
if x = Uppercase('MT NfeInutilizacao 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeInutilizacao2?wsdl' else
if x = Uppercase('MT NfeConsultaProtocolo 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeConsulta2?wsdl' else
if x = Uppercase('MT NfeStatusServico 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeStatusServico2?wsdl' else
if x = Uppercase('MT RecepcaoEvento 2.00') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento?wsdl' else
if x = Uppercase('MT NfeConsultaCadastro 3.10') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/CadConsultaCadastro2?wsdl' else
if x = Uppercase('MT NFeAutorizacao 3.10') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeAutorizacao?wsdl' else
if x = Uppercase('MT NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.sefaz.mt.gov.br/nfews/v2/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Pernambuco - (PE)
// Servi�o Vers�o URL
if x = Uppercase('PE RecepcaoEvento 1.00') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento' else
if x = Uppercase('PE NfeRecepcao 2.00') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRecepcao2' else
if x = Uppercase('PE NfeRetRecepcao 2.00') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRetRecepcao2' else
if x = Uppercase('PE NfeInutilizacao 3.10') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeInutilizacao2' else
if x = Uppercase('PE NfeConsultaProtocolo 3.10') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeConsulta2' else
if x = Uppercase('PE NfeStatusServico 3.10') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeStatusServico2' else
if x = Uppercase('PE NFeAutorizacao 3.10') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeAutorizacao?wsdl' else
if x = Uppercase('PE NFeRetAutorizacao 3.10') then FURL := 'https://nfehomolog.sefaz.pe.gov.br/nfe-service/services/NfeRetAutorizacao?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Paran� - (PR)
// Servi�o Vers�o URL
if x = Uppercase('PR NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRecepcao2?wsdl' else
if x = Uppercase('PR NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeRetRecepcao2?wsdl' else
if x = Uppercase('PR NfeInutilizacao 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeInutilizacao2?wsdl' else
if x = Uppercase('PR NfeConsultaProtocolo 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeConsulta2?wsdl' else
if x = Uppercase('PR NfeStatusServico 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/NFeStatusServico2?wsdl' else
if x = Uppercase('PR NfeConsultaCadastro 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe/CadConsultaCadastro2?wsdl' else
if x = Uppercase('PR RecepcaoEvento 2.00') then FURL := 'https://homologacao.nfe2.fazenda.pr.gov.br/nfe-evento/NFeRecepcaoEvento?wsdl' else
if x = Uppercase('PR NfeInutilizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeInutilizacao3?wsdl' else
if x = Uppercase('PR NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeConsulta3?wsdl' else
if x = Uppercase('PR NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeStatusServico3?wsdl' else
if x = Uppercase('PR NfeConsultaCadastro 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/CadConsultaCadastro2?wsdl' else
if x = Uppercase('PR RecepcaoEvento 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento?wsdl' else
if x = Uppercase('PR NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeAutorizacao3?wsdl' else
if x = Uppercase('PR NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/nfe/NFeRetAutorizacao3?wsdl' else
// Voltar ao topo da p�gina
// Sefaz Rio Grande do Sul - (RS)
// Servi�o Vers�o URL
if x = Uppercase('RS RecepcaoEvento 1.00') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('RS NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('RS NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('RS NfeConsultaCadastro 2.00') then FURL := 'https://sef.sefaz.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx' else
if x = Uppercase('RS NfeConsultaDest 2.00') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/nfeConsultaDest/nfeConsultaDest.asmx' else
if x = Uppercase('RS NfeDownloadNF 2.00') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/nfeDownloadNF/nfeDownloadNF.asmx' else
if x = Uppercase('RS NfeInutilizacao 3.10') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx' else
if x = Uppercase('RS NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('RS NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('RS NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('RS NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefaz.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz S�o Paulo - (SP)
// Servi�o Vers�o URL
if x = Uppercase('SP NfeConsultaCadastro 2.0') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/cadconsultacadastro2.asmx' else
if x = Uppercase('SP NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/NfeRecepcao2.asmx' else
if x = Uppercase('SP NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/NfeRetRecepcao2.asmx' else
if x = Uppercase('SP NfeInutilizacao 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/nfeinutilizacao2.asmx' else
if x = Uppercase('SP NfeConsultaProtocolo 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/nfeconsulta2.asmx' else
if x = Uppercase('SP NfeStatusServico 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/nfeweb/services/nfestatusservico2.asmx' else
if x = Uppercase('SP RecepcaoEvento 2.00') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/eventosWEB/services/RecepcaoEvento.asmx' else
if x = Uppercase('SP NfeInutilizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeinutilizacao2.asmx' else
if x = Uppercase('SP NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeconsulta2.asmx' else
if x = Uppercase('SP NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfestatusservico2.asmx' else
if x = Uppercase('SP NfeConsultaCadastro 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/cadconsultacadastro2.asmx' else
if x = Uppercase('SP RecepcaoEvento 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx' else
if x = Uppercase('SP NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nfeautorizacao.asmx' else
if x = Uppercase('SP NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.fazenda.sp.gov.br/ws/nferetautorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual Ambiente Nacional - (SVAN)
// Servi�o Vers�o URL
if x = Uppercase('SVAN RecepcaoEvento 1.00') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SVAN NfeRecepcao 2.00') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SVAN NfeRetRecepcao 2.00') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVAN NfeInutilizacao 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeInutilizacao2/NfeInutilizacao2.asmx' else
if x = Uppercase('SVAN NfeConsultaProtocolo 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SVAN NfeStatusServico 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SVAN NfeDownloadNF 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
if x = Uppercase('SVAN NFeAutorizacao 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SVAN NFeRetAutorizacao 3.10') then FURL := 'https://hom.sefazvirtual.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual Rio Grande do Sul - (SVRS)
// Servi�o Vers�o URL
if x = Uppercase('SVRS RecepcaoEvento 1.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('SVRS NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('SVRS NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVRS NfeConsultaCadastro 2.00') then FURL := 'https://homologacao.sef.sefaz.rs.gov.br/ws/cadconsultacadastro/cadconsultacadastro2.asmx' else
if x = Uppercase('SVRS NfeInutilizacao 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao2.asmx' else
if x = Uppercase('SVRS NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('SVRS NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('SVRS NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('SVRS NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Conting�ncia Ambiente Nacional - (SCAN)
// Servi�o Vers�o URL
if x = Uppercase('SCAN RecepcaoEvento 1.00') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SCAN NfeRecepcao 2.00') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SCAN NfeRetRecepcao 2.00') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SCAN NfeInutilizacao 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeInutilizacao2/NfeInutilizacao2.asmx' else
if x = Uppercase('SCAN NfeConsultaProtocolo 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SCAN NfeStatusServico 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SCAN NFeAutorizacao 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SCAN NFeRetAutorizacao 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/SCAN/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual de Conting�ncia Ambiente Nacional - (SVC-AN)
// Servi�o Vers�o URL
if x = Uppercase('SVC-AN RecepcaoEvento 1.00') then FURL := 'https://hom.svc.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('SVC-AN NfeRecepcao 2.00') then FURL := 'https://hom.svc.fazenda.gov.br/NfeRecepcao2/NfeRecepcao2.asmx' else
if x = Uppercase('SVC-AN NfeRetRecepcao 2.00') then FURL := 'https://hom.svc.fazenda.gov.br/NfeRetRecepcao2/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVC-AN NfeConsultaProtocolo 3.10') then FURL := 'https://hom.svc.fazenda.gov.br/NfeConsulta2/NfeConsulta2.asmx' else
if x = Uppercase('SVC-AN NfeStatusServico 3.10') then FURL := 'https://hom.svc.fazenda.gov.br/NfeStatusServico2/NfeStatusServico2.asmx' else
if x = Uppercase('SVC-AN NFeAutorizacao 3.10') then FURL := 'https://hom.svc.fazenda.gov.br/NfeAutorizacao/NfeAutorizacao.asmx' else
if x = Uppercase('SVC-AN NFeRetAutorizacao 3.10') then FURL := 'https://hom.svc.fazenda.gov.br/NfeRetAutorizacao/NfeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Sefaz Virtual de Conting�ncia Rio Grande do Sul - (SVC-RS)
// Servi�o Vers�o URL
if x = Uppercase('SVC-RS RecepcaoEvento 1.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx' else
if x = Uppercase('SVC-RS NfeRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/Nferecepcao/NFeRecepcao2.asmx' else
if x = Uppercase('SVC-RS NfeRetRecepcao 2.00') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetRecepcao/NfeRetRecepcao2.asmx' else
if x = Uppercase('SVC-RS NfeConsultaProtocolo 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeConsulta/NfeConsulta2.asmx' else
if x = Uppercase('SVC-RS NfeStatusServico 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeStatusServico/NfeStatusServico2.asmx' else
if x = Uppercase('SVC-RS NFeAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao.asmx' else
if x = Uppercase('SVC-RS NFeRetAutorizacao 3.10') then FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao.asmx' else
// Voltar ao topo da p�gina
// Ambiente Nacional - (AN)
// Servi�o Vers�o URL
if x = Uppercase('AN RecepcaoEvento 1.00') then FURL := 'https://hom.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx' else
if x = Uppercase('AN NFeDistribuicaoDFe 1.00') then FURL := 'https://hom.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx' else
if x = Uppercase('AN NfeConsultaDest 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/NFeConsultaDest/NFeConsultaDest.asmx' else
if x = Uppercase('AN NfeDownloadNF 1.00') then FURL := 'https://hom.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
//if x = Uppercase('AN NfeDownloadNF 2.00') then FURL := 'https://hom.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
//if x = Uppercase('AN NfeDownloadNF 3.10') then FURL := 'https://hom.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx' else
//
//
//
    FURL := '';
  end;
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + sLineBreak  +
    x + sLineBreak +
    'UF SEFAZ: ' + IntToStr(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + sLineBreak  + 'UF Servi�o: ' + IntToStr(cUF) + ' = ' + UFServico
    + sLineBreak  + 'Ambiente: ' + IntToStr(Ambiente) + ' = ' + TextoAmbiente(
    Ambiente) + sLineBreak  + 'A��o: ' + IntToStr(A) + ' = ' + TextoAcao(Acao)
    + slineBreak + 'x = "' + x + '"' + sLineBreak +
    sLineBreak  + 'AVISE A DERMATEK' + sLineBreak;
    Result := False;
    Geral.MB_Aviso(sAviso);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
  //Label1.Caption := sAviso;
end;

function TFmNFeGeraXML_0310.NomeAcao(Acao: TTipoConsumoWS): String;
begin
  case Acao of
    tcwsStatusServico:    Result := 'NfeStatusServico';
    //tcwsEnviaLoteNF:      Result := 'NfeRecepcao';
    //tcwsConsultaLote:     Result := 'NfeRetRecepcao';
    tcwsPedeCancelamento: Result := 'NfeCancelamento';
    tcwsPediInutilizacao: Result := 'NfeInutilizacao';
    tcwsConsultaNFe:      Result := 'NfeConsultaProtocolo';
    tcwsRecepcaoEvento:   Result := 'RecepcaoEvento';
    tcwsConsultaCadastro: Result := 'NfeConsultaCadastro';
    // Aqui vai Consulta situa�ao nfe
    tcwsConsultaNFeDest:  Result := 'NfeConsultaDest';
    // Substitu�do por tcwsNFeDownloadNF ???
    //tcwsDownloadNFeDest:  Result := 'NfeDownloadNF';
    tcwsNFeDownloadNF:    Result := 'NfeDownloadNF';
    tcwsDistDFeInt:       Result := 'NFeDistribuicaoDFe';
    tcwsAutorizacao:      Result := 'NfeAutorizacao';
    tcwsRetAutorizacao:   Result := 'NfeRetAutorizacao';
    else Result := '[Desconhecido]';
  end;
end;

function TFmNFeGeraXML_0310.ObtemWebServer2(UFServico: String; Ambiente,
  CodigoUF: Integer; Acao: TTipoConsumoWS; sAviso: String): Boolean;

  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
        1: Result := 'Produ��o';
        2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;

  function TextoAcao(Acao: TTipoConsumoWS): String;
  begin
    case Acao of
      tcwsStatusServico:    Result := 'Status do servi�o';
      //tcwsEnviaLoteNF:      Result := 'Enviar lote de NF-e ao fisco';
      //tcwsConsultaLote:     Result := 'Consultar lote enviado';
      tcwsPedeCancelamento: Result := 'Pedir cancelamento de NF-e';
      tcwsPediInutilizacao: Result := 'Pedir inutiliza��o de n�mero(s) de NF-e';
      tcwsConsultaNFe:      Result := 'Consultar NF-e';
      tcwsRecepcaoEvento:   Result := 'Enviar lote de evento de NF-e ao fisco';
      tcwsConsultaCadastro: Result := 'Consulta cadastro de Contribuinte';
      // Aqui vai Consulta situa�ao nfe
      tcwsConsultaNFeDest:  Result := 'Consulta NFe-s destinadas';
      // Substitu�do por tcwsNFeDownloadNF ???
      //tcwsDownloadNFeDest:  Result := 'Download de NFe destinadas confirmadas';
      tcwsNFeDownloadNF:    Result := 'Download de NF-e';
      tcwsAutorizacao:      Result := 'Envio Lote NF-e (novo 3.10)';
      tcwsRetAutorizacao:   Result := 'Consulta Recibo Lote (novo 3.10)';
      tcwsDistDFeInt:       Result := 'Distribui��o de DFe de Interesse';
       else Result := '[Desconhecido]';
    end;
  end;

  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS): Byte;
  begin
    case TipoConsumoWS of
      tcwsStatusServico:    Result := 0;
      //tcwsEnviaLoteNF:      Result := 1;
      //tcwsConsultaLote:     Result := 2;
      tcwsPedeCancelamento: Result := 3;
      tcwsPediInutilizacao: Result := 4;
      tcwsConsultaNFe:      Result := 5;
      tcwsRecepcaoEvento:   Result := 6;
      tcwsConsultaCadastro: Result := 7;
      // Aqui vai Consulta situa�ao nfe
      tcwsConsultaNFeDest:  Result := 9;
      //tcwsDownloadNFeDest:  Result := 10;
      tcwsNFeDownloadNF:    Result := 10;
      tcwsAutorizacao:      Result := 11;
      tcwsRetAutorizacao:   Result := 12;
      tcwsDistDFeInt:       Result := 13;
      else                  Result := Null;
    end;
  end;

var
  cUF, A: Integer;
  Versao, x: String;
begin
  FURL := UnNFe_PF.ObtemURLWebServerNFe(UFServico, NomeAcao(Acao), VersaoWS(Acao, True),
  Ambiente, 55);
  A    := TipoConsumoWs2Acao(Acao);
  cUF  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  x    := Uppercase(UFServico + ' '+ NomeAcao(Acao) + ' ' + VersaoWS(Acao, True));
  //
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + sLineBreak  +
    x + sLineBreak +
    'UF SEFAZ: ' + Geral.FF0(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + sLineBreak  + 'UF Servi�o: ' + Geral.FF0(cUF) + ' = ' + UFServico
    + sLineBreak  + 'Ambiente: ' + Geral.FF0(Ambiente) + ' = ' + TextoAmbiente(
    Ambiente) + sLineBreak  + 'A��o: ' + Geral.FF0(A) + ' = ' + TextoAcao(Acao)
    + slineBreak + 'x = "' + x + '"' + sLineBreak +
    sLineBreak  + 'AVISE A DERMATEK' + sLineBreak;
    Result := False;
    Geral.MB_Aviso(sAviso);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
end;

(*
procedure TFmNFeGeraXML_0310.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  ContentHeader: string;
begin
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  // N�o funciona!!
  //Store.Open( CAPICOM_CURRENT_USER_STORE, 'ADDRESSBOOK',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(FmNFeSteps_0310.EdSerialNumber.Text) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MB_Aviso('Falha ao selecionar o certificado.');
        end;

  ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
  HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;
*)

{
procedure TFmNFeGeraXML_0310.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : Pointer;
  ContentHeader: string;
begin
  Cert := FConfiguracoes.Certificados.GetCertificado;
  CertContext :=  Cert as ICertContext;
  CertContext.Get_CertContext(Integer(PCertContext));

  if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext,SizeOf(CERT_CONTEXT)) then
   begin
     if Assigned(TACBrNFe( FACBrNFe ).OnGerarLog) then
        TACBrNFe( FACBrNFe ).OnGerarLog('ERRO: Erro OnBeforePost: ' + IntToStr(GetLastError));
     raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;

   if trim(FConfiguracoes.WebServices.ProxyUser) <> '' then begin
     if not InternetSetOption(Data, INTERNET_OPTION_PROXY_USERNAME, PChar(FConfiguracoes.WebServices.ProxyUser), Length(FConfiguracoes.WebServices.ProxyUser)) then
       raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;
   if trim(FConfiguracoes.WebServices.ProxyPass) <> '' then begin
     if not InternetSetOption(Data, INTERNET_OPTION_PROXY_PASSWORD, PChar(FConfiguracoes.WebServices.ProxyPass),Length (FConfiguracoes.WebServices.ProxyPass)) then
       raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;

  ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
  HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);
end;
}

function TFmNFeGeraXML_0310.OutroPais_dest(): Boolean;
begin
  Result :=
  (DmNFe_0000.QrNFECabAdest_cPais.Value <> 0) and
  (DmNFe_0000.QrNFECabAdest_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0310.OutroPais_emit: Boolean;
begin
  Result :=
  (DmNFe_0000.QrNFECabAemit_cPais.Value <> 0) and
  (DmNFe_0000.QrNFECabAemit_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0310.OutroPais_entrega: Boolean;
begin
  Result := DmNFe_0000.QrNFECabGentrega_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0310.OutroPais_retirada(): Boolean;
begin
  Result := DmNFe_0000.QrNFECabFretirada_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0310.RecuperarXMLdaWeb(TextoHTML: String): String;
var
  strChaveAcesso: String;
  Protocolo, XML_Distribuicao: String;
  Empresa, Status: Integer;
  Protocolar: Boolean;
  Versao: Double;
  ReciAmbNacional: TDateTime;
begin
{
  Result := '';
  // Criando o Documento XML e Gravando cabe�alho...
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetNFe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)
  if UnNFeBaixaXMLdaWeb_0310.GerarXML_Da_WEB_2(
  TextoHTML, Protocolo, strChaveAcesso, Empresa, Status, Versao, ReciAmbNacional) then
  begin
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    Protocolar := True;
    //
    if NFeXMLGeren.XML_DistribuiNFe(strChaveAcesso, Status, arqXML.XML.Text,
    Protocolo, '', Protocolar, XML_Distribuicao, Versao,
    'nas informa��es obtidas da Web') then
      Result := DmNFe_0000.SalvaXML(NFE_EXT_NFE_WEB_XML, strChaveAcesso,
        XML_Distribuicao, nil, False);
  end;
  arqXML := nil;
}
end;

function TFmNFeGeraXML_0310.RetornarCodigoNumerico(Chave, Versao: String): String;
var
  Chv: String;
begin
  Chv := Geral.SoNumero_TT(Chave);
  //
  if Geral.DMV(Versao) < 2 then
     Result := Copy(Chv, 35, 9)
  else
     Result := Copy(Chv, 36, 8);
end;

function TFmNFeGeraXML_0310.SeparaApartir(Chave, Texto: String): String;
var
  I: integer;
  C, T: String;
begin
  C := AnsiUpperCase(Trim(Chave));
  T := AnsiUpperCase(Texto);
  I := pos(C, T);
  //
  if I = 0 then
  begin
    Result := Texto;
  end else
  begin
    Result := Copy(Texto, I);
  end;
end;

function TFmNFeGeraXML_0310.SeparaAte(Chave, Texto: String;
  var Resto: String): String;
var
  I: integer;
  C, T: String;
begin
  C := AnsiUpperCase(Trim(Chave));
  T := AnsiUpperCase(Texto);
  I := pos(C, T);
  //
  if I = 0 then
  begin
    Result := '';
    Resto := Texto;
  end else
  begin
    Resto  := Copy(Texto, I);
    Result := Copy(Texto, 1, I - 1);
  end;
end;

function TFmNFeGeraXML_0310.SeparaDados(Texto: String; Chave: String;
  MantemChave: Boolean): String;
var
  PosIni, PosFim : Integer;
begin
  if MantemChave then
   begin
     PosIni := Pos(Chave,Texto)-1;
     PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)-1;
        PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
      end;
   end
  else
   begin
     PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
     PosFim := Pos('/'+Chave,Texto);

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
        PosFim := Pos('/ns2:'+Chave,Texto);
      end;
   end;
  Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  if Result = '' then
    Geral.MB_Erro(Texto);
end;

function TFmNFeGeraXML_0310.StrToCSTICMS(var ok: boolean;
  const s: string): TpcnCSTICMS;
begin
  Result := StrToEnumerado(ok, s,
  ['00', '10', '20', '30', '40', '41', '50', '51', '60', '70', '80', '81', '90'],
  [cst00, cst10, cst20, cst30, cst40, cst41, cst50, cst51, cst60, cst70, cst80, cst81, cst90]);
end;

function TFmNFeGeraXML_0310.StrToEnumerado(var ok: boolean; const s: string;
  const AString: array of string; const AEnumerados: array of variant): variant;
var
  i: integer;
begin
  result := -1;
  for i := Low(AString) to High(AString) do
    if AnsiSameText(s, AString[i]) then
      result := AEnumerados[i];
  ok := result <> -1;
  if not ok then
    result := AEnumerados[0];
end;

function TFmNFeGeraXML_0310.StrToFinNFe(var ok: boolean;
  const s: string): TpcnFinalidadeNFe;
begin
  Result := StrToEnumerado(ok, s, ['1', '2', '3'], [
  fnNormal, fnComplementar, fnAjuste]);
end;
function TFmNFeGeraXML_0310.StrToIndpag(var ok: boolean;
  const s: string): TpcnIndicadorPagamento;
begin
  Result := StrToEnumerado(ok, s, ['0', '1', '2'], [
  ipVista, ipPrazo, ipOutras]);
end;

function TFmNFeGeraXML_0310.StrToOrig(var ok: boolean;
  const s: string): TpcnOrigemMercadoria;
begin
  Result := StrToEnumerado(ok, s, ['0', '1', '2'], [
  oeNacional, oeEstrangeiraImportacaoDireta, oeEstrangeiraAdquiridaBrasil]);
end;

function TFmNFeGeraXML_0310.StrToprocEmi(var ok: boolean;
  const s: string): TpcnProcessoEmissao;
begin
  Result := StrToEnumerado(ok, s, ['0', '1', '2', '3'], [
  peAplicativoContribuinte, peAvulsaFisco, peAvulsaContribuinte,
  peContribuinteAplicativoFisco]);
end;

function TFmNFeGeraXML_0310.StrToTpEmis(var ok: boolean;
  const s: string): TpcnTipoEmissao;
begin
  Result := StrToEnumerado(ok, s, ['1', '2', '3', '4', '5'], [
  teNormal, teContingencia, teSCAN, teDPEC, teFSDA]);
end;

function TFmNFeGeraXML_0310.StrToTpNF(var ok: boolean;
  const s: string): TpcnTipoNFe;
begin
  Result := StrToEnumerado(ok, s, ['0', '1'], [tnEntrada, tnSaida]);
end;

function TFmNFeGeraXML_0310.StrZero(Num: Real; Zeros, Deci: Integer): String;
var
  tam, z : Integer;
  res, zer : String;
begin
  str(Num:Zeros:Deci, res);
  res := Alltrim(res);
  tam := length(res);
  zer := '';
  for z := 1 to (Zeros-tam) do
    zer := zer + '0';
  result := zer+res
end;

function TFmNFeGeraXML_0310.TipoXML(NoStandAlone: Boolean): String;
begin
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

procedure TFmNFeGeraXML_0310.VerificaSeHaTextoExtra(Grupo, Texto, Titulo: String);
var
  Extra, Inocuo: String;
  I: Integer;
begin

  //'NFeEmitenteDestinat�rioProdutos/Servi�osTotaisCom�rcio ExteriorTransporteCobran�aInf. AdicionaisAvulsa |&|'
  Extra := SeparaAte(Texto, Grupo, Inocuo);
  if Trim(Extra) <> '' then
    AvisaTextoNaoProcessado(01, Titulo, Extra);
  I := pos('|&|', Inocuo);
  Extra := Copy(Inocuo, I + 3);
  if Trim(Extra) <> '' then
    AvisaTextoNaoProcessado(02, Titulo, Extra);
end;

function TFmNFeGeraXML_0310.VersaoWS(Acao: TTipoConsumoWS; Formata: Boolean = True): String;
  function Fmt(Val: Double): String;
  begin
    if Formata then
      Result := Geral.FFT_Dot(Val, 2, siNegativo)
    else
      Result := Geral.FFI(Val);
  end;
begin
  case Acao of
    //DModG.QrPrmsEmpNFeNFeVerCanNFe
    //DModG.QrPrmsEmpNFeNFeVerConDes
    tcwsRetAutorizacao:   Result := Fmt(DModG.QrPrmsEmpNFeNFeVerConLot.Value);
    //tcws             DModG.QrPrmsEmpNFeNFeVerConNFe
    tcwsConsultaCadastro: Result := Fmt(DModG.QrPrmsEmpNFeNFeVerConsCad.Value);
    tcwsDistDFeInt:       Result := Fmt(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value);
    tcwsAutorizacao:      Result := Fmt(DModG.QrPrmsEmpNFeNFeVerEnvLot.Value);
    tcwsPediInutilizacao: Result := Fmt(DModG.QrPrmsEmpNFeNFeVerInuNum.Value);
    tcwsRecepcaoEvento:   Result := Fmt(DModG.QrPrmsEmpNFeNFeVerLotEve.Value);
    tcwsStatusServico:    Result := Fmt(DModG.QrPrmsEmpNFeNFeVerStaSer.Value);
    //tcwsNFeDownloadNF:    Result := Fmt(DModG.QrPrmsEmpNFeNFeVerDowNFe.Value);
    tcwsNFeDownloadNF:    Result := Fmt(DModG.QrPrmsEmpNFeNFeVerDowNFe.Value);

    //
    else Result := '3.10';
  end;
end;

(*
function TFmNFeGeraXML_0310.XML_CabecMsg(VersaoDados: String; NoStandAlone: Boolean): String;
var
  XML_CabecMsg_Str: IXMLCabecMsg;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_CabecMsg_Str := GetcabecMsg(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_CabecMsg_Str.Versao      := verCabecMsg_Versao;
    XML_CabecMsg_Str.VersaoDados := VersaoDados;
    //
    Result := TipoXML(NoStandAlone) + XML_CabecMsg_Str.XML;
  finally
    FdocXML := nil;
  end;
end;
*)

{
function TFmNFeGeraXML_0310.XML_CancNFe(chNFe: String; TpAmb: Integer; nProt, XJust: String): String;
var
  XML_CancNFe_Str: IXMLTCancNFe;
  //xUF, xAno,
  Id: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_CancNFe_Str := GetCancNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    Id := 'ID' + ChNFe;
    XML_CancNFe_Str.Versao         := verCancNFe_Versao;
    XML_CancNFe_Str.InfCanc.Id     := Id;
    XML_CancNFe_Str.InfCanc.TpAmb  := FormatFloat('0', TpAmb);
    XML_CancNFe_Str.InfCanc.XServ  := 'CANCELAR';
    XML_CancNFe_Str.InfCanc.ChNFe  := ChNFe;
    XML_CancNFe_Str.InfCanc.NProt  := NProt;
    XML_CancNFe_Str.InfCanc.XJust  := xJust;
    Result := TipoXML(False) + XML_CancNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;
}

function TFmNFeGeraXML_0310.XML_ConsCad(xUF, Contribuinte_CNPJ: String): String;
var
  XML_ConsCad_Str: IXMLTConsCad;
  VersaoDados: String;
begin
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConsCad.Value, 2, siNegativo);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsCad_Str := GetConsCad(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;
    //XML_ConsCad_Str.Versao      := verConsCad_Versao;
    XML_ConsCad_Str.Versao      := VersaoDados;
    XML_ConsCad_Str.InfCons.XServ := 'CONS-CAD';
    XML_ConsCad_Str.InfCons.UF := xUF;
    XML_ConsCad_Str.InfCons.CNPJ := Geral.SoNumero_TT(Contribuinte_CNPJ);
    //
    Result := TipoXML(False) + XML_ConsCad_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

(*
function TFmNFeGeraXML_0310.XML_ConsNFeDest(VersaoAcao: String; TpAmb: Integer; CNPJ: String;
             indNFe, indEmi, ultNSU: String): String;
var
  XML_ConsNFeDest_Str: IXMLTConsNFeDest;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsNFeDest_Str := GetconsNFeDest(FdocXML);
    FdocXML.Version     := sXML_Version;
    FdocXML.Encoding    := sXML_Encoding;
    XML_ConsNFeDest_Str.Versao      := VersaoAcao;
    XML_ConsNFeDest_Str.tpAmb       := Geral.FF0(tpAmb);
    XML_ConsNFeDest_Str.xServ       := CO_CONSULTAR_NFE_DEST;
    XML_ConsNFeDest_Str.CNPJ        := Geral.SoNumero_TT(CNPJ);
    XML_ConsNFeDest_Str.indNfe      := indNFe;
    XML_ConsNFeDest_Str.indEmi      := indEmi;
    XML_ConsNFeDest_Str.ultNSU      := ultNSU;
    //
    Result := TipoXML(False) + XML_ConsNFeDest_Str.XML;
  finally
    FdocXML := nil;
  end;
end;
*)

function TFmNFeGeraXML_0310.XML_ConsReciNFe(TpAmb: Integer;
  NRec: String): String;
var
  XML_ConsReciNFe_Str: IXMLTConsReciNFe;
  VersaoDados: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsReciNFe_Str := GetConsReciNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    //
    VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerConLot.Value, 2, siNegativo);

    //XML_ConsReciNFe_Str.Versao      := verConsReciNFe_Versao;
    XML_ConsReciNFe_Str.Versao      := VersaoDados;
    //XML_ConsReciNFe_Str.Versao      := Versao;
    XML_ConsReciNFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsReciNFe_Str.NRec        := NRec;
    //
    Result := TipoXML(False) + XML_ConsReciNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0310.XML_ConsSitNFe(TpAmb: Integer; ChNFe, VersaoAcao: String): String;
var
  XML_ConsSitNFe_Str: IXMLTConsSitNFe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsSitNFe_Str := GetConsSitNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_ConsSitNFe_Str.Versao      := VersaoAcao;
    XML_ConsSitNFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsSitNFe_Str.XServ       := 'CONSULTAR';
    XML_ConsSitNFe_Str.ChNFe       := ChNFe;
    //
    Result := TipoXML(False) + XML_ConsSitNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0310.XML_ConsStatServ(TpAmb, CUF: Integer): String;
var
  XML_ConsStatServ_Str: IXMLTConsStatServ;
  Versao: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsStatServ_Str := GetConsStatServ(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;

    Versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);

    //XML_ConsStatServ_Str.Versao      := verConsStatServ_Versao;
    XML_ConsStatServ_Str.Versao      := Versao;
    XML_ConsStatServ_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsStatServ_Str.CUF         := FormatFloat('00', CUF);
    XML_ConsStatServ_Str.XServ       := 'STATUS';
    //
    Result := TipoXML(False) + XML_ConsStatServ_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0310.XML_DistDFeInt(TpAmb: Integer;
  UFAutor, CNPJ, CPF, ultNSU, NSU: String): String;
  // Erro no WebService???
  function CompletaZeros(Texto: String): String;
  begin
    Result := Texto;
    while Length(Result) < 15 do
      Result := '0' + Result;
  end;
var
  XML_DistDFeInt_Str: IXMLDistDFeInt;
  VersaoDados: String;
begin
  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value, 2, siNegativo);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_DistDFeInt_Str := GetDistDFeInt(FdocXML);
    FdocXML.Version     := sXML_Version;
    FdocXML.Encoding    := sXML_Encoding;
    XML_DistDFeInt_Str.Versao      := VersaoDados;
    XML_DistDFeInt_Str.tpAmb       := Geral.FF0(tpAmb);
    XML_DistDFeInt_Str.cUFAutor    := UFAutor;
    //
    if CNPJ <> '' then
      XML_DistDFeInt_Str.CNPJ      := Geral.SoNumero_TT(CNPJ)
    else
      XML_DistDFeInt_Str.CPF       := Geral.SoNumero_TT(CPF);
    //
    if NSU <> '' then
      XML_DistDFeInt_Str.consNSU.NSU := CompletaZeros(NSU)
    else
    if ultNSU <> '' then
      XML_DistDFeInt_Str.distNSU.ultNSU      := CompletaZeros(ultNSU)
    else
      XML_DistDFeInt_Str.distNSU.ultNSU      := CompletaZeros('0');
    //
    Result := TipoXML(False) + XML_DistDFeInt_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0310.XML_DowNFeDest(VersaoAcao: String; TpAmb: Integer;
  CNPJ: String; Chaves: TStrings): String;
var
  //XML_DowNFeDest_Str: IXMLTDownloadNFe;
  I: Integer;
begin
{ XSD ou Webservice inst�vel????
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_DowNFeDest_Str := GetdownloadNFe(FdocXML);
    FdocXML.Version     := sXML_Version;
    FdocXML.Encoding    := sXML_Encoding;
    XML_DowNFeDest_Str.Versao      := VersaoAcao;
    XML_DowNFeDest_Str.tpAmb       := Geral.FF0(tpAmb);
    XML_DowNFeDest_Str.xServ       := CO_DOWNLOAD_NFE_DEST;
    XML_DowNFeDest_Str.CNPJ        := Geral.SoNumero_TT(CNPJ);
    for I := 0 to Chaves.Count -1 do
      XML_DowNFeDest_Str.ChNFe := Chaves[I];
    //
    Result := TipoXML(False) + XML_DowNFeDest_Str.XML;
  finally
    FdocXML := nil;
  end;
}
{
<?xml version="1.0" encoding="utf-8"?>
  <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
    <soap12:Header>

<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF"><cUF>50</cUF><versaoDados>1.00</versaoDados></nfeCabecMsg></soap12:Header><soap12:Body><nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeDownloadNF">
}




  //Fazer desse jeito?
  //O XSD est� com problemas!
  //

  Result := '';
  for I := 0 to Chaves.Count -1 do
    Result := Result +
  '<chNFe>' + Chaves[I] + '</chNFe>';
  Result :=
  '<downloadNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + VersaoAcao + '">' +
    '<tpAmb>' + Geral.FF0(tpAmb) + '</tpAmb>' +
    '<xServ>DOWNLOAD NFE</xServ>' +            // CO_DOWNLOAD_NFE_DEST;
    '<CNPJ>' + Geral.SoNumero_TT(CNPJ) + '</CNPJ>'

    +  Result +


  '</downloadNFe>';









{
</nfeDadosMsg>

    </soap12:Body>
  </soap12:Envelope>
}
end;

function TFmNFeGeraXML_0310.XML_NfeDownloadNF(VersaoAcao: String; TpAmb: Integer;
  CNPJ: String; Chaves: TStrings): String;
var
  I: Integer;
begin
  //Fazer desse jeito?
  //O XSD n�o est� (n�o encontrei) no portal da NFe!!
  Result := '';
  for I := 0 to Chaves.Count -1 do
    Result := Result +
  '<chNFe>' + Chaves[I] + '</chNFe>';
  Result :=
  '<downloadNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + VersaoAcao + '">' +
    '<tpAmb>' + Geral.FF0(tpAmb) + '</tpAmb>' +
    '<xServ>DOWNLOAD NFE</xServ>' +
    '<CNPJ>' + Geral.SoNumero_TT(CNPJ) + '</CNPJ>'
    +  Result +
    '</downloadNFe>';
end;

function TFmNFeGeraXML_0310.XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte; Ano: Integer;
CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): String;
var
  XML_NFeInutNFe_Str: IXMLTInutNFe;
  xUF, xAno, Versao: String;
begin
  xUF  := FormatFloat('00', CUF);
  xAno := FormatFloat('00', Ano);
  MontaID_Inutilizacao(xUF, xAno, CNPJ, Mod_, Serie, nNFIni, nNFFin, Id);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_NFeInutNFe_Str := GetinutNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;

    Versao := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerInuNum.Value, 2, siNegativo);

    //XML_NFeInutNFe_Str.Versao         := verNFeInutNFe_Versao;
    XML_NFeInutNFe_Str.Versao         := Versao;
    XML_NFeInutNFe_Str.InfInut.Id     := Id;
    XML_NFeInutNFe_Str.InfInut.TpAmb  := FormatFloat('0', TpAmb);
    XML_NFeInutNFe_Str.InfInut.XServ  := 'INUTILIZAR';
    XML_NFeInutNFe_Str.InfInut.CUF    := FormatFloat('00', CUF);
    XML_NFeInutNFe_Str.InfInut.Ano    := FormatFloat('00', Ano);
    XML_NFeInutNFe_Str.InfInut.CNPJ   := CNPJ;
    XML_NFeInutNFe_Str.InfInut.Mod_   := Mod_;
    XML_NFeInutNFe_Str.InfInut.Serie  := Serie;
    XML_NFeInutNFe_Str.InfInut.NNFIni := NNFIni;
    XML_NFeInutNFe_Str.InfInut.NNFFin := NNFFin;
    XML_NFeInutNFe_Str.InfInut.XJust  := XJust;
    Result := TipoXML(False) + XML_NFeInutNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;


function TFmNFeGeraXML_0310.ExecutaAssinatura(aValue: String; Certificado:
ICertificate2; URIs: TStringList; out sXML: String): Boolean;
(** ASSINATURA **)
var
  ////////////// A T E N � � O /////////////////////////////////////////////////
  //                                                                          //
  // ADICIONAR AO PROJETO AS SEGUINTES UNITS:                                 //
  // C:\Projetos\Delphi 2007\TLB\MSXML2_TLB.pas                               //
  // C:\Projetos\Delphi 2007\TLB\CAPICOM_TLB.pas                              //
  //                                                                          //
  // COLOCAR AS SEGUINTES DLLS NO WINDOWS:                                    //
  // MSXML5.DLL, MSXML5R.DLL E CAPICOM.DLL                                    //
  //                                                                          //
  // REGISTRAR AS DLL NO APLICATIVO EXECUTAR.EXE DO WINDOWS:                  //
  // regsvr32 msxml5.dll (/s para silencioso = sem aviso)                     //
  // regsvr32 capicom.dll                                                     //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  XMLDoc                           : IXMLDOMDocument3; // ver aviso acima !!!
  XMLDSig                          : IXMLDigitalSignature;
  dsigKey                          : IXMLDSigKey;
  signedKey                        : IXMLDSigKey;
  nodePai, nodeX509Data, nodeIrmao : IXMLDOMNode;

(** CERTIFICADO **)
var
  CertStore                        : IStore3;
  CertStoreMem                     : IStore3;
  PrivateKey                       : IPrivateKey;
  Certs                            : ICertificates2;
  Cert                             : ICertificate2;

(** GERAL **)
var
  C, I                             : Integer;
  xmlHeaderAntes, xmlHeaderDepois  : String;

  function FindNode(lstNodes : IXMLDOMNodeList; strNome:string):IXMLDOMNode;
  var
    node, noderet: IXMLDOMNode;
  begin
    result := nil;
    node   := lstNodes.nextNode as IXMLDOMNode;
    while node <> nil do
    begin
      If node.nodeName = strNome then
      begin
        result := node;
        exit;
      end;

      if node.hasChildNodes then
      begin
        noderet := findNode(node.childNodes,strNome);
        if noderet <> nil then
        begin
          result := noderet;
          exit;
        end;
      end;

      node := lstNodes.nextNode as IXMLDOMNode;
    end;
  end;

  function FindNodeURI(lstNodes: IXMLDOMNodeList; Tag, URI: String) : IXMLDOMNode;
  var
    Node, NodeRet: IXMLDOMNode;
    TextXML: String;
    P: Integer;
  begin
     Result := nil;
     Node   := (lstNodes.nextNode as IXMLDOMNode);
     while (Node <> nil) do
     begin
        TextXML := Node.XML;
        P := pos(URI,TextXML);
        if (Node.NodeName = Tag) and (P > 0) Then
        begin
           Result := Node;
           Exit;
        end;
        if (Node.HasChildNodes) then
        begin
           NodeRet := FindNodeURI(Node.ChildNodes,Tag,URI);
           if (NodeRet <> nil) then
           begin
              Result := NodeRet;
              Exit;
           end;
        end;
        Node := (lstNodes.nextNode as IXMLDOMNode);
     end;
  end;

  procedure DeleteFindNode(TagURI: String);
  begin
    nodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', TagURI);
    nodePai := FindNode(NodePai.ChildNodes,'KeyInfo');
    if (nodePai <> nil) then
    begin
      nodeX509Data := findNode(nodePai.childNodes,'X509Data');
      nodeIrmao    := nodeX509Data.nextSibling;
      while nodeIrmao <> nil do
      begin
        nodePai.removeChild(nodeIrmao);
        nodeIrmao := nodeX509Data.nextSibling;
      end;
    end;
  end;

begin
  If (Trim(aValue) = '') then
    raise Exception.Create('N�o existe  informa��o para fazer a Assinatura Digital');

  aValue:=StringReplace( aValue, #10, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #13, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #9, '', [rfReplaceAll] );


   (* Pegando o header antes de assinar *)
  xmlHeaderAntes := '' ;
  I := pos('?>', aValue) ;
  if I > 0 then
    xmlHeaderAntes := copy(aValue, 1, I+1);

  (*** CONFIGURANDO O XML DOC ***)
  XMLDoc := CoDOMDocument50.Create;

  XMLDoc.async := FALSE;
  XMLDoc.validateOnParse := FALSE;
  XMLDoc.preserveWhiteSpace := TRUE;
  (******************************)

  XMLDSig := CoMXDigitalSignature50.Create;

  if (not XMLDoc.LoadXML(aValue) ) then
    raise Exception.Create('N�o foi poss�vel carregar o "texto" XML');

  XMLDoc.setProperty('SelectionNamespaces', DSIGNS); //DSIGNS->Constante declarada

  C:=0;
  repeat
    NodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', URIs.Strings[C]);

    XMLDSig.signature := NodePai;

    If (XMLDSig.signature = nil) then
      raise Exception.Create('Falha ao setar assinatura.');

    If (XMLDSig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   {
   if Certificado.SerialNumber <> SerialCertificado then
      CertStoreMem := nil;
   }

    if CertStoreMem = nil then
    begin

      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for I := 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
        begin
          CertStoreMem.Add(Cert);
        end;
      end;
    end;

(*
//      NomeStore                   := 'AddressBook';
    if CertStoreMem = nil then
    begin

      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'AddressBook', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for I := 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
        begin
          CertStoreMem.Add(Cert);
        end;
      end;
    end;
*)
    OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey, PrivateKey));

    xmldsig.store := CertStoreMem;

    dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
    if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP.');

    signedKey := xmldsig.sign(dsigKey, $00000002);

    if (signedKey = nil) then
      raise Exception.Create('Assinatura Falhou.');

    if (signedKey <> nil) then
      DeleteFindNode(URIs.Strings[C]);

    Inc(C);
  until C>=URIs.Count;

  sXML:=XMLDoc.Xml;

  if xmlHeaderAntes <> '' then
  begin
    I:=pos('?>', sXML);
    if I > 0 then
    begin
      xmlHeaderDepois:=copy(sXML,1,I+1);
      if xmlHeaderAntes <> xmlHeaderDepois then
        sXML:=StuffString(sXML, 1, length(xmlHeaderDepois), xmlHeaderAntes);
    end else
      sXML:=xmlHeaderAntes + sXML;
  end;

  Result := True;
  //
  //
  Geral.MB_Aviso(sXML);
  //
  //
  dsigKey   := nil;
  signedKey := nil;
  xmldoc    := nil;
  xmldsig   := nil;
end;

{
procedure TFmNFeGeraXML_0310.ExecutaReqResp(ReqResp: THTTPReqResp; Texto, Stream: ?);
begin
  ReqResp.Execute(Acao.Text, Stream);
end;
}

{
Para Validar utilize esse c�digo: Este valida xml s� est� validando o lote RPS para validar os demais xml vc tem que modificar.
Function TfrmMain.Valida_XML(XML: String; SchemaPath : String): Boolean;
var
  DOMDocument : IXMLDOMDocument3;
  ParseError  : IXMLDOMParseError;
  Schema      : XMLSchemaCache;

begin
  DOMDocument:=CoDOMDocument50.Create;

  DOMdocument.Async:=FALSE;
  DOMdocument.ResolveExternals:=FALSE;
  DOMdocument.ValidateOnParse:=TRUE;

  DOMdocument.LoadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_simples.xsd', SchemaPath+'Tipos_Simples.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_complexos.xsd', SchemaPath+'Tipos_Complexos.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/servico_enviar_lote_rps_envio.xsd', SchemaPath+'servico_enviar_lote_rps_envio.xsd');

  DOMdocument.Schemas := Schema;

  ParseError:=DOMdocument.validate;
  Result:=(ParseError.errorCode = 0);

  if ParseError.errorCode <> S_OK then
    raise Exception.Create(IntTostr(ParseError.errorCode)+' - '+ParseError.reason);

  DOMDocument:=Nil;
  ParseError:=Nil;
  Schema:=Nil;
end;

Function TfrmMain.Valida_XML(XML: String; SchemaPath : String): Boolean;
var
  DOMDocument : IXMLDOMDocument3;
  ParseError  : IXMLDOMParseError;
  Schema      : XMLSchemaCache;

begin
  DOMDocument:=CoDOMDocument50.Create;

  DOMdocument.Async:=FALSE;
  DOMdocument.ResolveExternals:=FALSE;
  DOMdocument.ValidateOnParse:=TRUE;

  DOMdocument.LoadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_simples.xsd', SchemaPath+'Tipos_Simples.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_complexos.xsd', SchemaPath+'Tipos_Complexos.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/servico_enviar_lote_rps_envio.xsd', SchemaPath+'servico_enviar_lote_rps_envio.xsd');

  DOMdocument.Schemas := Schema;

  ParseError:=DOMdocument.validate;
  Result:=(ParseError.errorCode = 0);

  if ParseError.errorCode <> S_OK then
    raise Exception.Create(IntTostr(ParseError.errorCode)+' - '+ParseError.reason);

  DOMDocument:=Nil;
  ParseError:=Nil;
  Schema:=Nil;
end;
}

{
function TFmNFeGeraXML.PosLast(const SubStr, S: String ): Integer;
var
  P : Integer ;
begin
  Result := 0 ;
  P := Pos( SubStr, S) ;
  while P <> 0 do
  begin
     Result := P ;
     P := PosEx( SubStr, S, P+1) ;
  end ;
end;
}

{
item #245.25- ID = N11
Valor = "0"
}

function TFmNFeGeraXML_0310.GerarXML_Da_WEB_1(const Arquivo: String;
var XML_Prot: String; var ChaveDeAcesso: String;
var Empresa: Integer; var Status: Integer; var VersaoNFe: Double): Boolean;
var
{
 NFe: TNFe;
 GeradorXML : TNFeW;
}
 ok, bIgnoraDuplicata : Boolean;
 dData, Valor: String;
 i, posIni, posFim : Integer;
 sDataEmissao, Versao: String;
 CaminhoXML, Grupo, SubGrupo, GrupoICMS, GrupoIPI, (*Sobra,*) ArquivoTXT, ArquivoRestante: String;
 ArquivoItens, ArquivoItensTemp, ArquivoDuplicatas, ArquivoVolumes : String;
 produtos, N: Integer;
 //
 ResEnum: Variant;
 //
 digVal, nProt, dhRecbto, xMotivo, cStat: String;
 DataHora: TDateTime;
 Ano, Mes, Dia: Word;
 //
 Orig, CST, CSOSN, Inocuo, Extra, Multipl, Texto: String;
 J0, J1, J2: Integer;
begin
{
  Result := False;

  ChaveDeAcesso := '';
  Empresa := 0;
  Status := 0;
  VersaoNFe := 0;
(*
 NFe := TNFe.Create;
*)
  ArquivoTXT := StringReplace(Arquivo,#$D#$A,'|&|',[rfReplaceAll]);
  // fazer primeiro ...
  ArquivoTxt := SeparaApartir('Voc� est� aqui:', ArquivoTxt);
  // porque tem mais de um "Consultar NF-e Completa"
  ArquivoTxt := SeparaApartir('Consultar NF-e Completa', ArquivoTxt);
  ArquivoTxt := SeparaAte('Conhe�a a NF-e', ArquivoTxt, ArquivoTxt);
  //
  //ArquivoTxt := SeparaApartir('Chave de acesso', ArquivoTxt);
  ArquivoRestante := SeparaApartir('Chave de acesso', ArquivoTxt);

  //Grupo := SeparaAte('Dados da NF-e', ArquivoTXT, ArquivoRestante);
  Grupo := SeparaAte('Dados da NF-e', ArquivoRestante, ArquivoRestante);

  if Trim(Grupo) = '' then
  begin
    ArquivoTXT := StringReplace(ArquivoTxt, 'Consultar NF-e Completa|&|', '', [rfReplaceAll]);
    ArquivoTXT := StringReplace(ArquivoTxt, '|&|', #$D#$A, [rfReplaceAll]);
    Geral.MB_Aviso(ArquivoTxt);
    Exit;
  end;

  //cXML.infNFe.ID := Geral.SoNumero_TT(LerCampo(Grupo,'Chave de acesso'));
  if dmk_DefValDeText(Valor, Grupo, 'Chave de acesso') then
  begin
    cXML.infNFe.ID := Geral.SoNumero_TT(Valor);
    ChaveDeAcesso := cXML.infNFe.ID;
  end;
  //

  //cXML.infNFe.Ide.nNF := Geral.SoNumero_TT(LerCampo(Grupo,'N�mero NF-e'));
  if dmk_DefValDeText(Valor, Grupo, 'N�mero NF-e') then
    cXML.infNFe.Ide.nNF := Geral.SoNumero_TT(Valor);

  // Incluido campo que recebe qual a Vers�o do XML que o arquivo est�.
  //Versao := LerCampo(Grupo,'Vers�o');
  if dmk_DefValDeText(Valor, Grupo, 'Vers�o') then
  begin
    Versao := Valor;
    VersaoNFe := Geral.DMV(Geral.Substitui(Versao, '.', ','));
  end;
  //'NFeEmitenteDestinat�rioProdutos/Servi�osTotaisCom�rcio ExteriorTransporteCobran�aInf. AdicionaisAvulsa |&|'
  VerificaSeHaTextoExtra(Grupo, 'NFeEmitenteDestinat�rio', 'Dados da NF-e');

  cXML.infNFe.Ide.cNF := RetornarCodigoNumerico(cXML.infNFe.ID, cXML.infNFe.Versao);

  ArquivoRestante := SeparaApartir('N�mero', ArquivoRestante);
  Grupo :=  SeparaAte('EMITENTE', ArquivoRestante, ArquivoRestante);

  if dmk_DefValDeText(Valor, Grupo, 'N�mero') then
    cXML.infNFe.Ide.nNF := Geral.SoNumero_TT(Valor);

  if dmk_DefValDeText(Valor, Grupo, 'S�rie') then
    cXML.infNFe.Ide.Serie := Geral.SoNumero_TT(Valor);


  (* Alterado forma de atribui��o do campo Data de emiss�o pois devido a
     diferen�a de vers�es (1.10 e 2.00) da NF-e a formata��o de Datas estava
     com problemas. *)
  if dmk_DefValDeText(Valor, Grupo, 'Data de emiss�o') then
  begin
    sDataEmissao := Valor; //LerCampo(Grupo,'Data de emiss�o');

    if Length(sDataEmissao) > 0 then
     dData := Geral.FDT(EncodeDate(StrToInt(copy(sDataEmissao, 07, 4)), StrToInt(copy(sDataEmissao, 04, 2)),
                StrToInt(copy(sDataEmissao, 01, 2))), 1)
    else
     dData := '0000-00-00';

    cXML.infNFe.Ide.dEmi := dData;
  end;

  if dmk_DefValDeText(Valor, Grupo, 'Valor Total da Nota Fiscal') then
    cXML.infNFe.Total.ICMSTot.vNF := ConverteStrToNumero(Valor);//ConverteStrToNumero(LerCampo(Grupo,'Valor Total da Nota Fiscal'));
  cXML.infNFe.Ide.mod_ := Copy(Geral.SoNumero_TT(cXML.infNFe.ID), 21, 2);
  cXML.infNFe.Ide.serie := Copy(Geral.SoNumero_TT(cXML.infNFe.ID), 23, 3);

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(03, 'Cabe�alho', Grupo);

  //ArquivoRestante := SeparaApartir('CNPJ', ArquivoRestante);
  ArquivoRestante := Copy(ArquivoRestante, Length('EMITENTE |&|') + 1);
  Grupo :=  SeparaAte('DESTINAT�RIO |&|', ArquivoRestante, ArquivoRestante);

  //cXML.InfNFe.Emit.CNPJ := LerCampo(Grupo,'CNPJ');
  if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
  begin
    Valor := Geral.SoNumero_TT(Valor);
    if Geral.SoNumero_TT(Valor) <> '' then
    begin
      if Length(Valor) >= 14 then
        cXML.InfNFe.Emit.CNPJ := Valor
      else
        cXML.InfNFe.Emit.CPF := Valor;
    end;
  end;

  if dmk_DefValDeText(Valor, Grupo, 'Nome/Raz�o Social') then
    cXML.infNFe.Emit.xNome := Valor; //LerCampo(Grupo,'Nome/Raz�o Social');

  if dmk_DefValDeText(Valor, Grupo, 'Inscri��o Estadual') then
    cXML.infNFe.Emit.IE := Geral.SoNumeroELetra_TT(Valor);

  if dmk_DefValDeText(Valor, Grupo, 'UF') then
    cXML.infNFe.Emit.EnderEmit.UF := Valor; //LerCampo(Grupo,'UF');

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(04, 'EMITENTE', Grupo);


  //ArquivoRestante := SeparaApartir('CNPJ', ArquivoRestante);
  Inocuo := 'DESTINAT�RIO |&|';
  ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
  Grupo :=  SeparaAte('EMISS�O', ArquivoRestante, ArquivoRestante);

  //cXML.InfNFe.Dest.CNPJ := LerCampo(Grupo,'CNPJ');
  if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
  begin
    Valor := Geral.SoNumero_TT(Valor);
    if Valor <> '' then
    begin
      if Length(Valor) >= 14 then
        cXML.InfNFe.Dest.CNPJ := Valor
      else
        cXML.InfNFe.Dest.CPF := Valor;
    end;
  end;

  if dmk_DefValDeText(Valor, Grupo, 'Nome/Raz�o Social') then
    cXML.infNFe.Dest.xNome := Valor; //LerCampo(Grupo,'Nome/Raz�o Social');

  if dmk_DefValDeText(Valor, Grupo, 'Inscri��o Estadual') then
    cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(Valor); //LerCampo(Grupo,'Inscri��o Estadual'));

  //continuar verifica��o de dados e texto que resta

  if dmk_DefValDeText(Valor, Grupo, 'UF') then
    cXML.infNFe.Dest.EnderDest.UF := Valor; //LerCampo(Grupo,'UF');

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(05, 'DESTINAT�RIO', Grupo);

  Inocuo := 'EMISS�O |&|';
  ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
  Grupo :=  SeparaAte('Dados do Emitente',ArquivoRestante,ArquivoRestante);

  if dmk_DefValDeEnum(Valor, enumProcessoEmissao, Grupo, 'Processo', 1) then
    cXML.infNFe.Ide.procEmi := Valor;

  if dmk_DefValDeText(Valor, Grupo, 'Vers�o do Processo') then
    cXML.infNFe.Ide.verProc := Valor; //LerCampo(Grupo, 'Vers�o do Processo');

  //cXML.infNFe.ide.tpEmis  := StrToTpEmis(ok, LerCampo(Grupo, 'Tipo de Emiss�o',1));
  if dmk_DefValDeEnum(Valor, enumtpEmis, Grupo, 'Tipo de Emiss�o', 1) then
    cXML.infNFe.ide.tpEmis := Valor;

  //cXML.infNFe.Ide.finNFe  := StrToFinNFe(ok, LerCampo(Grupo, 'Finalidade',1));
  if dmk_DefValDeEnum(Valor, enumFinNFe, Grupo, 'Finalidade', 1) then
    cXML.infNFe.ide.FinNFe := Valor;

  if dmk_DefValDeText(Valor, Grupo, 'Natureza da Opera��o') then
  cXML.infNFe.Ide.natOp := Valor; //LerCampo(Grupo, 'Natureza da Opera��o');

  // cXML.infNFe.ide.tpNF := StrToTpNF(ok, LerCampo(Grupo, 'Tipo da Opera��o',1));
  if dmk_DefValDeEnum(Valor, enumTpNF, Grupo, 'Tipo da Opera��o', 1) then
    cXML.infNFe.ide.tpNF := Valor;

  //cXML.infNFe.ide.indPag  := StrToIndpag(ok, LerCampo(Grupo, 'Forma de Pagamento',1));
  if dmk_DefValDeEnum(Valor, enumIndPag, Grupo, 'Forma de Pagamento', 1) then
    cXML.infNFe.ide.indPag := Valor;

//  Fazer!

// TODO 5 : Fazer protocolo de autoriza��o!

  //cXML.infNFe.proccXML.infNFe.digVal   := LerCampo(Grupo, 'Digest Value da NF-e');
  dmk_DefValDeText(digVal, Grupo, 'Digest Value da NF-e');

  //cXML.infNFe.proccXML.infNFe.xMotivo  := LerCampo(Grupo, 'Ocorr�ncia');
  dmk_DefValDeText(xMotivo, Grupo, 'SITUA��O ATUAL:');

  //cXML.infNFe.proccXML.infNFe.xMotivo  := LerCampo(Grupo, 'Ocorr�ncia');
  dmk_DefValDeText(xMotivo, Grupo, 'Ocorr�ncia');

  //cXML.infNFe.proccXML.infNFe.nProt    := LerCampo(Grupo, 'Protocolo');
  dmk_DefValDeText(nProt, Grupo, 'Protocolo');

  //cXML.infNFe.proccXML.infNFe.dhRecbto := StrToDateDef(LerCampo(Grupo,'Data/Hora'),0);
  if dmk_DefValDeText(dhRecbto, Grupo, 'Data/Hora') then
  begin
    Ano := Geral.IMV(Copy(dhRecbto, 7, 4));
    Mes := Geral.IMV(Copy(dhRecbto, 4, 2));
    Dia := Geral.IMV(Copy(dhRecbto, 1, 2));
    DataHora := EncodeDate(Ano, Mes, Dia);// + StrToTime(Copy(dhRecbto, 12));
    dhRecbto := FormatDateTime('yyyy-mm-dd', DataHora) + 'T' +
                //FormatDateTime('hh:nn:ss', DataHora);
                Copy(dhRecbto, 12);
    (*
    dhRecbto := Trim(dhRecbto);
    dhRecbto := Geral.Substitui(dhRecbto, ' ', 'T');
    dhRecbto := Geral.Substitui(dhRecbto, '/', '-');
    *)
  end;


(*
<protNFe versao="1.10" xmlns="http://www.portalfiscal.inf.br/nfe">
  <infProt Id="ID415100003306461">
    <tpAmb>1</tpAmb>
    <verAplic>4.00</verAplic>
    <chNFe>15100404333952000188550010000000296153224315</chNFe>
    <dhRecbto>2010-04-15T14:25:03</dhRecbto>
    <nProt>415100003306461</nProt>
    <digVal>4TK5W3rv/frbNLKFJb3vGr7s6pA=</digVal>
    <cStat>100</cStat>
    <xMotivo>Autorizado o uso da NF-e</xMotivo>
  </infProt>
</protNFe>


<protNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="2.00">
  <infProt Id="ID415110009025778">
    <tpAmb>1</tpAmb>
    <verAplic>SVAN_4.01</verAplic>
    <chNFe>15110604333952000188550010000016891006514304</chNFe>
    <dhRecbto>2011-06-08T09:20:56</dhRecbto>
    <nProt>415110009025778</nProt>
    <digVal>u60W+wszOQAk7TgSzAb3Us9+vqI=</digVal>
    <cStat>100</cStat>
    <xMotivo>Autorizado o uso da NF-e</xMotivo>
  </infProt>
</protNFe>

*)
  if pos('AUTORIZA', AnsiUppercase(xMotivo)) > 0 then
    cStat := '100'
  else
  if pos('CANCELA', AnsiUppercase(xMotivo)) > 0 then
    cStat := '101'
  else
  if pos('DENEGA', AnsiUppercase(xMotivo)) > 0 then
    cStat := '301'
  else
    cStat := '000';
  //
  versao := Geral.SoNumeroEPonto_TT(Versao);
  XML_Prot :=
'<protNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + versao + '">' +
'  <infProt Id="ID' + nProt + '">' +  // por dedu��o
'    <tpAmb>' + '1' + '</tpAmb>' +  // por dedu��o
'    <verAplic>' + 'NULL' + '</verAplic>' + // Como saber?
'    <chNFe>' + cXML.infNFe.ID + '</chNFe>' +
'    <dhRecbto>' + dhRecbto + '</dhRecbto>' +
'    <nProt>' + nProt + '</nProt>' +
'    <digVal>' + digVal + '</digVal>' +
'    <cStat>' + cStat + '</cStat>' +
'    <xMotivo>' + xMotivo + '</xMotivo>' +
'  </infProt>' +
'</protNFe>';


  //SITUA��O ATUAL:
  (*
  cXML.infNFe.proccXML.infNFe.digVal   := LerCampo(Grupo, 'Digest Value da NF-e');
  cXML.infNFe.proccXML.infNFe.xMotivo  := LerCampo(Grupo, 'Ocorr�ncia');
  cXML.infNFe.proccXML.infNFe.nProt    := LerCampo(Grupo, 'Protocolo');
  cXML.infNFe.proccXML.infNFe.dhRecbto := StrToDateDef(LerCampo(Grupo,'Data/Hora'),0);
  *)
  //Recebimento no Ambiente Nacional
  //Recebimento no Amb. Nacional24/08/2011 11:04:18|&|'

  VerificaSeHaTextoExtra(Grupo, 'Recebimento no Amb. Nacional', 'Dados da Emiss�o');

  Inocuo := 'Dados do Emitente|&|';
  ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
  Grupo :=  SeparaAte('Dados do destinat�rio',ArquivoRestante,ArquivoRestante);
  //

  //cXML.infNFe.Emit.xNome   := LerCampo(Grupo,'Nome / Raz�o Social');
  if dmk_DefValDeText(Valor, Grupo, 'Nome / Raz�o Social') then
    cXML.infNFe.Emit.xNome := Valor;

  //cXML.infNFe.Emit.xFant   := LerCampo(Grupo,'Nome Fantasia');
  if dmk_DefValDeText(Valor, Grupo, 'Nome Fantasia') then
    cXML.infNFe.Emit.xFant := Valor;

  //cXML.infNFe.Emit.CNPJCPF := Geral.SoNumero_TT(LerCampo(Grupo,'CNPJ'));
  if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
  begin
    Valor := Geral.SoNumero_TT(Valor);
    if Valor <> '' then
    begin
      if Length(Valor) >= 14 then
        cXML.InfNFe.Emit.CNPJ := Valor
      else
        cXML.InfNFe.Emit.CPF := Valor;
    end;
  end;

  //cXML.infNFe.Emit.EnderEmit.xLgr := LerCampo(Grupo,'Endere�o');
  if dmk_DefValDeText(Valor, Grupo, 'Endere�o') then
    cXML.infNFe.Emit.EnderEmit.xLgr := Valor;

  //cXML.infNFe.Emit.EnderEmit.xBairro := LerCampo(Grupo,'Bairro/Distrito');
  if dmk_DefValDeText(Valor, Grupo, 'Bairro/Distrito') then
    cXML.infNFe.Emit.EnderEmit.xBairro := Valor;

  //cXML.infNFe.Emit.EnderEmit.CEP := StrToIntDef(Geral.SoNumero_TT(LerCampo(Grupo,'CEP')),0);
  if dmk_DefValDeText(Valor, Grupo, 'CEP') then
    cXML.infNFe.Emit.EnderEmit.CEP := Geral.SoNumero_TT(Valor);

  //cXML.infNFe.Emit.EnderEmit.cMun := StrToIntDef(LerCampo(Grupo,'Munic�pio',7),0);
  if dmk_DefValDeText(Valor, Grupo, 'Munic�pio') then
  begin
    cXML.infNFe.Emit.EnderEmit.cMun := Geral.SoNumero_TT(Copy(Valor, 1, 7));

    //cXML.infNFe.Ide.cUF := StrToIntDef(LerCampo(Grupo,'Munic�pio',2),0);
    cXML.infNFe.Ide.cUF := Geral.SoNumero_TT(Copy(Valor, 1, 2));

    //cXML.infNFe.Emit.EnderEmit.xMun := copy(LerCampo(Grupo,'Munic�pio'),10,60);
    cXML.infNFe.Emit.EnderEmit.xMun := copy(Valor, 10, 60);
  end;

  //cXML.infNFe.Emit.EnderEmit.fone := Geral.SoNumeroELetra_TT(LerCampo(Grupo,' Fone/Fax'));
  if dmk_DefValDeText(Valor, Grupo, 'Fone/Fax') then
    cXML.infNFe.Emit.EnderEmit.fone := Geral.SoNumeroELetra_TT(Valor);

  //cXML.infNFe.Emit.EnderEmit.UF := LerCampo(Grupo,'UF');
  if dmk_DefValDeText(Valor, Grupo, 'UF') then
    cXML.infNFe.Emit.EnderEmit.UF := Valor;

  if dmk_DefValDeText(Valor, Grupo, 'Pa�s') then
  begin
    //cXML.infNFe.Emit.EnderEmit.cPais := StrToIntDef(LerCampo(Grupo,'Pa�s',4),1058);
    cXML.infNFe.Emit.EnderEmit.cPais := Copy(Valor, 1, 4);

    //cXML.infNFe.Emit.EnderEmit.xPais := copy(LerCampo(Grupo,'Pa�s'),8,60);
    cXML.infNFe.Emit.EnderEmit.xPais := copy(Valor, 8, 60);
  end;

  //cXML.infNFe.Emit.IE      := Geral.SoNumeroELetra_TT(LerCampo(Grupo,'Inscri��o Estadual'));
  if dmk_DefValDeText(Valor, Grupo, 'Inscri��o Estadual') then
    cXML.infNFe.Emit.IE := Geral.SoNumeroELetra_TT(Valor);

  //cXML.infNFe.Ide.cMunFG := StrToIntDef(LerCampo(Grupo,'Munic�pio da Ocorr�ncia do Fato Gerador do ICMS'),0);
  if dmk_DefValDeText(Valor, Grupo, 'Munic�pio da Ocorr�ncia do Fato Gerador do ICMS') then
    cXML.infNFe.Ide.cMunFG := Valor;

  if dmk_DefValDeText(Valor, Grupo, 'C�digo de Regime Tribut�rio') then
    cXML.infNFe.Emit.CRT := Copy(Valor, 1, 1);

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(06, 'Dados do Emitente', Grupo);

  Inocuo := 'Dados do Destinat�rio|&|';
  ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
  Grupo :=  SeparaAte('Dados dos Produtos e Servi�os',ArquivoRestante,ArquivoRestante);

  //cXML.infNFe.Dest.xNome   := LerCampo(Grupo,'Nome / Raz�o social');
  if dmk_DefValDeText(Valor, Grupo, 'Nome / Raz�o social') then
    cXML.infNFe.Dest.xNome   := Valor;

  //cXML.infNFe.Dest.CNPJCPF := Geral.SoNumero_TT(LerCampo(Grupo,'CNPJ/CPF'));
  if dmk_DefValDeText(Valor, Grupo, 'CNPJ/CPF') then
  begin
    Valor := Geral.SoNumero_TT(Valor);
    if Valor <> '' then
    begin
      if Length(Valor) >= 14 then
        cXML.InfNFe.Dest.CNPJ := Valor
      else
        cXML.InfNFe.Dest.CPF := Valor;
    end;
  end;

  //cXML.infNFe.Dest.CNPJCPF := Geral.SoNumero_TT(LerCampo(Grupo,'CNPJ'));
  if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
  begin
    Valor := Geral.SoNumero_TT(Valor);
    if Valor <> '' then
    begin
      if Length(Valor) >= 14 then
        cXML.InfNFe.Dest.CNPJ := Valor
      else
        cXML.InfNFe.Dest.CPF := Valor;
    end;
  end;

  //cXML.infNFe.Dest.EnderDest.xLgr := LerCampo(Grupo,'Endere�o');
  if dmk_DefValDeText(Valor, Grupo, 'Endere�o') then
    cXML.infNFe.Dest.EnderDest.xLgr := Valor;

  //cXML.infNFe.Dest.EnderDest.xBairro := LerCampo(Grupo,'Bairro/Distrito');
  if dmk_DefValDeText(Valor, Grupo, 'Bairro / Distrito') then
    cXML.infNFe.Dest.EnderDest.xBairro := Valor;

  //cXML.infNFe.Dest.EnderDest.CEP := StrToIntDef(Geral.SoNumero_TT(LerCampo(Grupo,'CEP')),0);
  if dmk_DefValDeText(Valor, Grupo, 'CEP') then
    cXML.infNFe.Dest.EnderDest.CEP := Geral.SoNumero_TT(Valor);

  if dmk_DefValDeText(Valor, Grupo, 'Munic�pio') then
  begin
    //cXML.infNFe.Dest.EnderDest.cMun := StrToIntDef(LerCampo(Grupo,'Munic�pio',7),0);
    cXML.infNFe.Dest.EnderDest.cMun := Copy(Valor, 1, 7);

    //cXML.infNFe.Dest.EnderDest.xMun := copy(LerCampo(Grupo,'Munic�pio'),10,60);
    cXML.infNFe.Dest.EnderDest.xMun := copy(Valor, 10, 60);
  end;

  //cXML.infNFe.Dest.EnderDest.fone := Geral.SoNumeroELetra_TT(LerCampo(Grupo,' Fone/Fax'));
  if dmk_DefValDeText(Valor, Grupo, 'Fone / Fax') then
    cXML.infNFe.Dest.EnderDest.fone := Geral.SoNumeroELetra_TT(Valor);

  //cXML.infNFe.Dest.EnderDest.UF := LerCampo(Grupo,'UF');
  if dmk_DefValDeText(Valor, Grupo, 'UF') then
    cXML.infNFe.Dest.EnderDest.UF := Valor;

  if dmk_DefValDeText(Valor, Grupo, 'Pa�s') then
  begin
    //cXML.infNFe.Dest.EnderDest.cPais := StrToIntDef(LerCampo(Grupo,'Pa�s',4),1058);
    cXML.infNFe.Dest.EnderDest.cPais := Copy(Valor, 1, 4);

    //cXML.infNFe.Dest.EnderDest.xPais := copy(LerCampo(Grupo,'Pa�s'),8,60);
    cXML.infNFe.Dest.EnderDest.xPais := copy(Valor, 8, 60);
  end;

  //cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(LerCampo(Grupo,'Inscri��o estadual'));
  if dmk_DefValDeText(Valor, Grupo, 'Inscri��o estadual') then
    cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(Valor);



  Inocuo := 'LOCAL DE ENTREGA |&|';
  if pos(Inocuo, Grupo) = 1 then
  begin
    Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo));

    Multipl := '';
    if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if dmk_DefValDeText(Valor, Grupo, 'CPF') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if Length(Multipl) >= 14 then
      cXML.infNFe.Entrega.CNPJ := Geral.SoNumero_TT(Multipl)
    else
      cXML.infNFe.Entrega.CPF := Geral.SoNumero_TT(Multipl);

    if dmk_DefValDeText(Valor, Grupo, 'Logradouro') then
      cXML.infNFe.Entrega.xLgr := Valor;

    if dmk_DefValDeText(Valor, Grupo, 'Bairro') then
      cXML.infNFe.Entrega.xBairro := Valor;

    if dmk_DefValDeText(Valor, Grupo, 'Munic�pio') then
    begin
      cXML.infNFe.Entrega.cMun := Copy(Valor, 1, 7);

      cXML.infNFe.Entrega.xMun := Copy(Valor, 11, 60);
    end;

    if dmk_DefValDeText(Valor, Grupo, 'UF') then
      cXML.infNFe.Entrega.UF := Valor;

  end;

  Grupo := Trim(Grupo);
  if Grupo =
  '$(''.oculta'').hide();|&|      $(this).parent().parent().children(''label'').toggle();|&|      $(this).parent().parent().parent().parent().parent().next().toggle();|&|' then
    Grupo := '';
  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(07, 'Dados do Destinat�rio', Grupo);


  ArquivoItens :=  SeparaAte('Dados do Transporte',ArquivoRestante,ArquivoItens);
  //Geral.MB_(ArquivoItens, 'Texto', MB_OK+MB_ICONINFORMATION);

  produtos := 0;
  ArquivoItensTemp := copy(ArquivoItens, 88, length(ArquivoItens));
  ArquivoItensTemp := copy(ArquivoItensTemp, Pos('|', ArquivoItensTemp));

  //aki faz o teste com o inteiro para achar quantidade de produtos
  for I := 1 to 990 do
  begin
    if pos('|&|' + intTostr(i) + '|&|', ArquivoItensTemp) > 0 then Inc(produtos);
  end;

  for I := 1 to produtos do
  begin

    if i < produtos then
      GrupoICMS := SeparaAte('|&|' + intTostr(i + 1) + '|&|', ArquivoItensTemp, ArquivoItensTemp)
    else
    begin
      GrupoICMS := SeparaAte(sWeb_Totais, ArquivoItensTemp, Grupo);
      //Grupo := SeparaApartir(sWeb_Totais, ArquivoItensTemp);
    end;
    // Separa IPI do ICMS
    if pos(sWeb_IPI, GrupoICMS) > 0 then
      GrupoICMS := SeparaAte(sWeb_IPI, GrupoICMS, GrupoIPI)
    else
      // caso n�o tenha IPI:
      GrupoIPI := '';

(*
    if GrupoICMS = '' then
    begin
      // caso n�o tenha IPI:
      GrupoICMS := Sobra;
      GrupoIPI := '';
    end else
      GrupoIPI := Sobra;
*)
    //

    cDetLista := cXML.InfNFe.Det.Add;

    //cDetLista.Prod.nItem := StrToIntDef(LerCampo(GrupoICMS, 'Num.'), 0);
    cDetLista.nItem := FormatFloat('000', I);

    //cDetLista.Prod.xProd := LerCampo(GrupoICMS, '|&|' + intTostr(i) + '|&|');
    if dmk_DefValDeText(Valor, GrupoICMS, '|&|' + intTostr(i) + '|&|') then
      cDetLista.Prod.xProd := Valor;

    //retira o c�digo '|&|1|&|'
    //GrupoICMS := copy(GrupoICMS, 8, length(GrupoICMS));
    //GrupoICMS := Copy(GrupoICMS, 7 + Length(FormatFloat('0', I)), Length(GrupoICMS));
    GrupoICMS := '|&|' + GrupoICMS;
    //separa at� a pr�xima tag |&|

    //cDetLista.Prod.qCom := ConverteStrToNumero(LerCampo(GrupoICMS, '|&|'));
    if dmk_DefValDeText(Valor, GrupoICMS, '|&|') then
      cDetLista.Prod.qCom := ConverteStrToNumero(Valor);

     //separa at� a pr�xima tag |&|
    //GrupoICMS := copy(GrupoICMS, pos('|&|', GrupoICMS) + 3, length(GrupoICMS));
    GrupoICMS := '|&|' + GrupoICMS;

    //cDetLista.Prod.uCom := LerCampo(GrupoICMS, '|&|');
    if dmk_DefValDeText(Valor, GrupoICMS, '|&|') then
      cDetLista.Prod.uCom := Valor;

      //separa at� a pr�xima tag |&|
    //GrupoICMS := copy(GrupoICMS, pos('|&|', GrupoICMS) + 3, length(GrupoICMS));
    GrupoICMS := '|&|' + GrupoICMS;

    //cDetLista.Prod.vProd := ConverteStrToNumero(LerCampo(GrupoICMS, '|&|'));
    if dmk_DefValDeText(Valor, GrupoICMS, '|&|') then
      cDetLista.Prod.vProd := ConverteStrToNumero(Valor);

      //separa at� a pr�xima tag |&|
    //GrupoICMS := copy(GrupoICMS, pos('|&|', GrupoICMS) + 3, length(GrupoICMS));

    //Daqui em diante continua mesmo layout

    //cDetLista.Prod.cProd := LerCampo(GrupoICMS, 'C�digo do Produto');
    if dmk_DefValDeText(Valor, GrupoICMS, 'C�digo do Produto') then
      cDetLista.Prod.cProd := Valor;

    //cDetLista.Prod.NCM := LerCampo(GrupoICMS, 'C�digo NCM');
    if dmk_DefValDeText(Valor, GrupoICMS, 'C�digo NCM') then
      cDetLista.Prod.NCM := Geral.SoNUmero_TT(Valor);

    //cDetLista.Prod.CFOP := LerCampo(GrupoICMS, 'CFOP');
    if dmk_DefValDeText(Valor, GrupoICMS, 'CFOP') then
      cDetLista.Prod.CFOP := Geral.SoNUmero_TT(Valor);


    //cDetLista.Prod.genero := StrToIntDef(LerCampo(GrupoICMS,'G�nero'),0);
    // N�o usa mais na 2.00

    //cDetLista.Prod.vFrete := ConverteStrToNumero(LerCampo(GrupoICMS, 'Valor Total do Frete'));
    if dmk_DefValDeText(Valor, GrupoICMS, 'Valor Total do Frete') then
      cDetLista.Prod.vFrete := ConverteStrToNumero(Valor);

    if dmk_DefValDeText(Valor, GrupoICMS, 'Indicador de Composi��o do Valor Total da NF-e') then
      cDetLista.Prod.indTot := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    //cDetLista.Prod.cEAN := LerCampo(GrupoICMS, 'C�digo EAN Comercial');
    if dmk_DefValDeText(Valor, GrupoICMS, 'C�digo EAN Comercial') then
      cDetLista.Prod.cEAN := Valor;

    //cDetLista.Prod.uCom := LerCampo(GrupoICMS, 'Unidade Comercial');
    if dmk_DefValDeText(Valor, GrupoICMS, 'Unidade Comercial') then
      cDetLista.Prod.uCom := Valor;

    //cDetLista.Prod.qCom := ConverteStrToNumero(LerCampo(GrupoICMS, 'Quantidade Comercial'));
    if dmk_DefValDeText(Valor, GrupoICMS, 'Quantidade Comercial') then
      cDetLista.Prod.qCom := ConverteStrToNumero(Valor);

    //cDetLista.Prod.cEANTrib := LerCampo(GrupoICMS, 'C�digo EAN Tribut�vel');
    if dmk_DefValDeText(Valor, GrupoICMS, 'C�digo EAN Tribut�vel') then
      cDetLista.Prod.cEANTrib := Valor;

    //cDetLista.Prod.uTrib := LerCampo(GrupoICMS, 'Unidade Tribut�vel');
    if dmk_DefValDeText(Valor, GrupoICMS, 'Unidade Tribut�vel') then
      cDetLista.Prod.uTrib := Valor;

    //cDetLista.Prod.qTrib := ConverteStrToNumero(LerCampo(GrupoICMS, 'Quantidade Tribut�vel'));
    if dmk_DefValDeText(Valor, GrupoICMS, 'Quantidade Tribut�vel') then
      cDetLista.Prod.qTrib := ConverteStrToNumero(Valor);

    //cDetLista.Prod.vUnCom := ConverteStrToNumero(LerCampo(GrupoICMS, 'Valor unit�rio de comercializa��o'));
    if dmk_DefValDeText(Valor, GrupoICMS, 'Valor unit�rio de comercializa��o') then
      cDetLista.Prod.vUnCom := ConverteStrToNumero(Valor);

    //cDetLista.Prod.vUnTrib := ConverteStrToNumero(LerCampo(GrupoICMS, 'Valor unit�rio de tributa��o'));
    if dmk_DefValDeText(Valor, GrupoICMS, 'Valor unit�rio de tributa��o') then
      cDetLista.Prod.vUnTrib := ConverteStrToNumero(Valor);


(*       Parei aqui! N�o uso!
    if LerCampo(GrupoICMS,'Chassi do ve�culo ') <> '' then
    begin
      // preencher as tags referente a ve�culo
      cDetLista.Prod.veiccDetLista.Prod.chassi  := LerCampo(GrupoICMS,'Chassi do ve�culo ');
      cDetLista.Prod.veiccDetLista.Prod.cCor    := LerCampo(GrupoICMS,'Cor ');
      cDetLista.Prod.veiccDetLista.Prod.xCor    := LerCampo(GrupoICMS,'Descri��o da cor ');
      cDetLista.Prod.veiccDetLista.Prod.nSerie  := LerCampo(GrupoICMS,'Serial (S�rie) ');
      cDetLista.Prod.veiccDetLista.Prod.tpComb  := LerCampo(GrupoICMS,'Tipo de Combust�vel ');
      cDetLista.Prod.veiccDetLista.Prod.nMotor  := LerCampo(GrupoICMS,'N�mero de Motor ');
      //cDetLista.Prod.veiccDetLista.Prod.RENAVAM := LerCampo(GrupoICMS,'RENAVAM');
      cDetLista.Prod.veiccDetLista.Prod.anoMod  := StrToInt(LerCampo(GrupoICMS,'Ano Modelo de Fabrica��o '));
      cDetLista.Prod.veiccDetLista.Prod.anoFab  := StrToInt(LerCampo(GrupoICMS,'Ano de Fabrica��o '));
    end;
*)


    Inocuo := 'ICMS NORMAL e ST |&|';
    if pos(Inocuo, GrupoICMS) = 1 then
    begin
      GrupoICMS := Copy(GrupoICMS, pos(Inocuo, GrupoICMS) + Length(Inocuo));

      if dmk_DefValDeEnum(Valor, enumOrigemMercadoria, GrupoICMS, 'Origem da Mercadoria', 1) then
        Orig := Valor;

      //CST := StrToCSTICMS(ok, Trim(LerCampo(GrupoICMS, 'Tributa��o do ICMS', 3)));
      CST := '';
      CSOSN := '';
      if dmk_DefValDeText(Valor, GrupoICMS, 'Tributa��o do ICMS', 3) then
      begin
        CST := Geral.SoNumero_TT(Valor);
        if CST = '' then
        begin
          Geral.MB_Aviso('CST n�o informado no sitio!' + sLineBreak  +
          'Item de produto/servi�o: ' + FormatFloat('000', I) + sLineBreak  +
          'Ser� usado o CST "00".' + sLineBreak  +
          'Corrija o lan�amento manualmente ap�s a importa��o!');
          CST := '00';
        end;
        //
        //Modalidade Defini��o da BC ICMS NOR

        //separa at� a pr�xima tag
        GrupoICMS := Copy(GrupoICMS, Pos('Modalidade', GrupoICMS), Length(GrupoICMS));

        case Geral.IMV(CST) of
          0:
          begin
            cDetLista.Imposto.ICMS.ICMS00.Orig  := Orig;
            cDetLista.Imposto.ICMS.ICMS00.CST   := CST;

            if dmk_DefValDeText(Valor, GrupoICMS, 'Modalidade Defini��o da BC ICMS NORMAL', 1) then
              cDetLista.Imposto.ICMS.ICMS00.ModBC := Valor;

            if dmk_DefValDeText(Valor, GrupoICMS, 'Base de C�lculo do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS00.VBC := ConverteStrToNumero(Valor);

            //separa at� a TAG al�quota
            GrupoICMS := Copy(GrupoICMS, Pos('Al�quota ICMS Normal', GrupoICMS), Length(GrupoICMS));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS00.PICMS := ConverteStrToNumero(Valor);


            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS00.VICMS := ConverteStrToNumero(Valor);
          end;

(*
          10:
          begin
            cDetLista.Imposto.ICMS.ICMS10.Orig      := Orig;
            cDetLista.Imposto.ICMS.ICMS10.CST       := CST;

            if dmk_DefValDeText(Valor, GrupoICMS, 'Modalidade Defini��o da BC ICMS NORMAL', 1) then
              cDetLista.Imposto.ICMS.ICMS10.ModBC := Valor;

            if dmk_DefValDeText(Valor, GrupoICMS, 'Base de C�lculo do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS10.VBC := ConverteStrToNumero(Valor);

            //separa at� a TAG al�quota
            GrupoICMS := Copy(GrupoICMS, Pos('Al�quota ICMS Normal', GrupoICMS), Length(GrupoICMS));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS10.PICMS := ConverteStrToNumero(Valor);


            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor do ICMS Normal') then
              cDetLista.Imposto.ICMS.ICMS10.VICMS := ConverteStrToNumero(Valor);

            if dmk_DefValDeText(Valor, GrupoICMS, 'Modalidade Defini��o da BC ICMS ST', 1) then
              cDetLista.Imposto.ICMS.ICMS10.ModBCST := Valor;

          (*
          if Def('180', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PMVAST    := Valor;
          if Def('181', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
            cDetLista.Imposto.ICMS.ICMS10.PRedBCST  := Valor;
          *)

            if dmk_DefValDeText(Valor, GrupoICMS, 'Base de C�lculo do ICMS ST') then
              cDetLista.Imposto.ICMS.ICMS10.vBCST := ConverteStrToNumero(Valor);

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota do ICMS ST') then
              cDetLista.Imposto.ICMS.ICMS10.PICMSST := ConverteStrToNumero(Valor);

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor do ICMS ST') then
              cDetLista.Imposto.ICMS.ICMS10.VICMSST := ConverteStrToNumero(Valor);
          end;
          20:
          begin
            (* TAG ICMS.ICMS20... *)
                // '185', 'N04'  = GrupoICMS do CST = 20
            if Def('186', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.Orig      := Valor;
            if Def('187', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.CST       := Valor;
            if Def('188', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.ModBC     := Valor;
            if Def('189', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.PRedBC    := Valor;
            if Def('190', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.VBC       := Valor;
            if Def('191', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.PICMS     := Valor;
            if Def('192', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS20.VICMS     := Valor;
          end;
          30:
          begin
            (* TAG ICMS.ICMS30... *)
                // '193', 'N05'  = GrupoICMS do CST = 30
            if Def('194', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.Orig      := Valor;
            if Def('195', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.CST       := Valor;
            if Def('196', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.ModBCST   := Valor;
            if Def('197', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.PMVAST    := Valor;
            if Def('198', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.PRedBCST  := Valor;
            if Def('199', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.VBCST     := Valor;
            if Def('200', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.PICMSST   := Valor;
            if Def('201', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS30.VICMSST   := Valor;
          end;
          // modificado 2010-07-07 faltava CST 41 e 50!
          40,41,50:
          begin
            (* TAG ICMS.ICMS40... *)
                // '202', 'N06'  = GrupoICMS do CST = 40, 41, 50
            if Def('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
            if Def('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
            if Def('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
            if Def('204.02', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS40.motDesICMS       := Valor;
          end;

          51:
          begin
            (* TAG ICMS.ICMS51... *)
                // '205', 'N07'  = GrupoICMS do CST = 51
            if Def('206', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.Orig      := Valor;
            if Def('207', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.CST       := Valor;
            if Def('208', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.ModBC     := Valor;
            if Def('209', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.PRedBC    := Valor;
            if Def('210', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.VBC       := Valor;
            if Def('211', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.PICMS     := Valor;
            if Def('212', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS51.VICMS     := Valor;
          end;
          60:
          begin
            (* TAG ICMS.ICMS60... *)
                // '213', 'N08'  = GrupoICMS do CST = 60
            if Def('214', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.Orig      := Valor;
            if Def('215', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.CST       := Valor;
            if Def('216', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.VBCSTRet     := Valor;
            if Def('217', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS60.VICMSSTRet   := Valor;
          end;
          70:
          begin
            (* TAG ICMS.ICMS70... *)
                // '218', 'N09'  = GrupoICMS do CST = 70
            if Def('219', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.Orig      := Valor;
            if Def('220', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.CST       := Valor;
            if Def('221', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.ModBC     := Valor;
            if Def('222', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.PRedBC    := Valor;
            if Def('223', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.VBC       := Valor;
            if Def('224', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.PICMS     := Valor;
            if Def('225', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.VICMS     := Valor;
            if Def('226', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.ModBCST   := Valor;
            if Def('227', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.PMVAST    := Valor;
            if Def('228', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.PRedBCST  := Valor;
            if Def('229', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.VBCST     := Valor;
            if Def('230', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.PICMSST   := Valor;
            if Def('231', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS70.VICMSST   := Valor;
          end;
          90:
          begin
            (* TAG ICMS.ICMS90... *)
                // '232', 'N10'  = GrupoICMS do CST = 90
            if Def('233', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.Orig      := Valor;
            if Def('234', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.CST       := Valor;
            if Def('235', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.ModBC     := Valor;
            if Def('236', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VBC       := Valor;
            if Def('237', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.PRedBC    := Valor;
            if Def('238', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.PICMS     := Valor;
            if Def('239', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VICMS     := Valor;
            if Def('240', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.ModBCST   := Valor;
            if Def('241', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.PMVAST    := Valor;
            if Def('242', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.PRedBCST  := Valor;
            if Def('243', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VBCST     := Valor;
            if Def('244', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.PICMSST   := Valor;
            if Def('245', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS90.VICMSST   := Valor;
          end;
*)
          else Geral.MB_Aviso('CST ICMS n�o implementado: "' + CST + '"' +
          sLineBreak  + 'AVISE A DERMATEK!');
        end;
      end
      else
      if dmk_DefValDeText(Valor, GrupoICMS, 'C�digo de Situa��o da Opera��o', 6) then
      begin
        // Valor veio vazio!!!
        CSOSN := Geral.SoNumero_TT(Valor);
        if CSOSN = '' then
        begin
          Geral.MB_Aviso('CSOSN n�o informado no sitio!' + sLineBreak  +
          'Item de produto/servi�o: ' + FormatFloat('000', I) + sLineBreak  +
          'Ser� usado o CSOSN "900".' + sLineBreak  +
          'Corrija o lan�amento manualmente ap�s a importa��o!');
          CSOSN := '900'
        end;
        //
        //Modalidade Defini��o da BC ICMS NOR

        //separa at� a pr�xima tag
        GrupoICMS := Copy(GrupoICMS, Pos('Modalidade', GrupoICMS), Length(GrupoICMS));

        case Geral.IMV(CSOSN) of
          900:
          begin
            cDetLista.Imposto.ICMS.ICMSSN900.Orig  := Orig;
            cDetLista.Imposto.ICMS.ICMSSN900.CSOSN := CSOSN;

            if dmk_DefValDeText(Valor, GrupoICMS, 'Modalidade de determina��o da BC do ICMS', 3) then
              cDetLista.Imposto.ICMS.ICMSSN900.ModBC := Geral.SoNumero_TT(Valor);

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor da BC do ICMS') then
              cDetLista.Imposto.ICMS.ICMSSN900.vBC := ConverteStrToNumero(Trim(Valor));

            // N�o foi testado ainda!
            if dmk_DefValDeText(Valor, GrupoICMS, 'Percentual de Redu��o de BC') then
              cDetLista.Imposto.ICMS.ICMSSN900.pRedBC := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota do imposto') then
              cDetLista.Imposto.ICMS.ICMSSN900.pICMS := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor do ICMS') then
              cDetLista.Imposto.ICMS.ICMSSN900.vICMS := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Modalidade de determina��o da BC do ICMS ST', 3) then
              cDetLista.Imposto.ICMS.ICMSSN900.modBCST := Geral.SoNumero_TT(Valor);

            // N�o foi testado ainda!
            if dmk_DefValDeText(Valor, GrupoICMS, 'Percentual da margem de valor Adicionado do ICMS ST') then
              cDetLista.Imposto.ICMS.ICMSSN900.pMVAST := ConverteStrToNumero(Trim(Valor));

            // N�o foi testado ainda!
            if dmk_DefValDeText(Valor, GrupoICMS, 'Percentual da Redu��o de BC do ICMS ST') then
              cDetLista.Imposto.ICMS.ICMSSN900.pRedBCST := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor da BC do ICMS '(*ST'*)) then
              cDetLista.Imposto.ICMS.ICMSSN900.vBCST := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota do imposto'(* do ICMS ST'*)) then
              cDetLista.Imposto.ICMS.ICMSSN900.pICMSST := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor do ICMS '(*ST'*)) then
              cDetLista.Imposto.ICMS.ICMSSN900.vICMSST := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Al�quota aplic�vel de c�lculo do cr�dito '(*(SIMPLES NACIONAL)'*)) then
              cDetLista.Imposto.ICMS.ICMSSN900.pCredSN := ConverteStrToNumero(Trim(Valor));

            if dmk_DefValDeText(Valor, GrupoICMS, 'Valor de cr�dito do ICMS'(* que pode ser aproveitado nos termos do art. 23 da LC 123 (SIMPLS NACIONAL)'*)) then
              cDetLista.Imposto.ICMS.ICMSSN900.vCredICMSSN := ConverteStrToNumero(Trim(Valor));

(*
Valor da BC do ICMS 0,00
Al�quota do imposto0,00
Valor do ICMS 0,00
Al�quota aplic�vel de c�lculo do cr�dito 0,00
Valor de cr�dito do ICMS0,00
*)

(*
O texto abaixo n�o foi processado:
Grupo: "ICMS NORMAL e ST"
Al�quota do imposto0,00|&|
Valor do ICMS 0,00|&|
Al�quota aplic�vel de c�lculo do cr�dito 0,00|&|
Valor de cr�dito do ICMS0,00|&|
Num.|&|Descri��o|&|Qtd.|&|Unidade Comercial|&|Valor(R$)
*)
          end;
          else Geral.MB_Aviso('CSOSN ICMS n�o implementado: "' + CSOSN + '"' +
          sLineBreak  + 'AVISE A DERMATEK!');
          //
        end;
      end;
      if Trim(GrupoICMS) <> '' then
      if AnsiUpperCase(GrupoICMS) <> AnsiUpperCase('Num.|&|Descri��o|&|Qtd.|&|Unidade Comercial|&|Valor(R$)') then
        AvisaTextoNaoProcessado(08, 'ICMS NORMAL e ST', GrupoICMS);
      // fim item produto
    end;
  end;

(*

  N := pos(sWeb_IPI, AnsiUppercase(Grupo));
  if N = 0 then
    N := Length(Grupo);
  SubGrupo := Copy(Grupo, 1, N);
  if Trim(SubGrupo) <> '' then
    AvisaTextoNaoProcessado(09, 'ICMS NORMAL e ST', SubGrupo);


  Inocuo := sWeb_IPI + ' |&|';
  if pos(Inocuo, Grupo) = 1 then
(*
  if GrupoIPI <> '' then
  begin
    // � necess�rio separar dos totais pois pode haver omiss�o do campo
    // 'Base de C�lculo' e confundir com o campo 'Base de C�lculo... do 'TOTAIS'
    SubGrupo := SeparaAte(sWeb_Totais, Grupo, Grupo);
    SubGrupo := Copy(SubGrupo, Length(Inocuo) + 1);
(*
    N := pos(sWeb_Totais, AnsiUppercase(Grupo)) - Length(Inocuo) - 1;
    if N <= 0 then
      N := Length(Grupo);
    //
    SubGrupo := Copy(Grupo, Length(Inocuo) + 1, N);
*)
    //cEnq := LerCampo(SubGrupo, 'C�digo de Enquadramento');
    if dmk_DefValDeText(Valor, SubGrupo, 'C�digo de Enquadramento') then
      cDetLista.Imposto.IPI.cEnq := Valor;

    //vBC := ConverteStrToNumero(LerCampo(SubGrupo, 'Base de C�lculo'));
    if pos('Base de C�lculo', SubGrupo) = 1 then
    if dmk_DefValDeText(Valor, SubGrupo, 'Base de C�lculo') then
      cDetLista.Imposto.IPI.IPITrib.vBC := ConverteStrToNumero(Valor);

    //pIPI := ConverteStrToNumero(LerCampo(SubGrupo, 'Al�quota'));
    if pos('Al�quota', SubGrupo) = 1 then
    if dmk_DefValDeText(Valor, SubGrupo, 'Al�quota') then
      cDetLista.Imposto.IPI.IPITrib.pIPI := ConverteStrToNumero(Valor);

    //vIPI := ConverteStrToNumero(LerCampo(SubGrupo, 'Valor'));
    if pos('Valor IPI', SubGrupo) = 1 then
    if dmk_DefValDeText(Valor, SubGrupo, 'Valor IPI') then
      cDetLista.Imposto.IPI.IPITrib.vIPI := ConverteStrToNumero(Valor);

    //CST := StrToCSTIPI(ok, LerCampo(SubGrupo, 'CST', 2));
    if dmk_DefValDeText(Valor, SubGrupo, 'CST') then
      cDetLista.Imposto.IPI.IPITrib.CST := Geral.SoNumero_TT(Copy(Valor, 1, 4));

    if Trim(SubGrupo) <> '' then
      AvisaTextoNaoProcessado(10, 'IPI', SubGrupo);
    //
  end;

  Inocuo := sWeb_Totais;
  if pos(Inocuo, AnsiUppercase(Grupo)) = 1 then
  begin
    Grupo := Copy(Grupo, Length(Inocuo) + 1);

    Inocuo := 'ICMS |&|';
    if pos(Inocuo, AnsiUppercase(Grupo)) = 1 then
    begin
      Grupo := Copy(Grupo, Length(Inocuo) + 1);

      //cXML.InfNFe.Total.ICMSTot.VBC := ConverteStrToNumero(LerCampo(Grupo,'Base de C�lculo ICMS'));
      if dmk_DefValDeText(Valor, Grupo, 'Base de C�lculo ICMS') then
        cXML.InfNFe.Total.ICMSTot.VBC := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vICMS := ConverteStrToNumero(LerCampo(Grupo,'Valor do ICMS'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor do ICMS') then
        cXML.InfNFe.Total.ICMSTot.vICMS := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vBCST := ConverteStrToNumero(LerCampo(Grupo,'Base de C�lculo ICMS ST'));
      if dmk_DefValDeText(Valor, Grupo, 'Base de C�lculo ICMS ST') then
        cXML.InfNFe.Total.ICMSTot.vBCST := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vST   := ConverteStrToNumero(LerCampo(Grupo,'Valor ICMS Substitui��o'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor ICMS Substitui��o') then
        cXML.InfNFe.Total.ICMSTot.vST := ConverteStrToNumero(Valor);


      (*
      Incluida condicional que Verifica a vers�o do XML e ent�o atribui qual o
         texto de busca que dever� ser procurado no arquivo.
      *)

      //sTexto := IfThen(Trim(Versao) = '2.00', 'Valor Total dos Produtos', 'Valor dos Produtos');
      //cXML.InfNFe.Total.ICMSTot.vProd   := ConverteStrToNumero(LerCampo(Grupo, sTexto));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Total dos Produtos') then
        cXML.InfNFe.Total.ICMSTot.vProd := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vFrete:= ConverteStrToNumero(LerCampo(Grupo,'Valor do Frete'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor do Frete') then
        cXML.InfNFe.Total.ICMSTot.vFrete := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vSeg  := ConverteStrToNumero(LerCampo(Grupo,'Valor do Seguro'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor do Seguro') then
        cXML.InfNFe.Total.ICMSTot.vSeg := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vOutro := ConverteStrToNumero(LerCampo(Grupo,'Outras Despesas Acess�rias'));
      if dmk_DefValDeText(Valor, Grupo, 'Outras Despesas Acess�rias') then
        cXML.InfNFe.Total.ICMSTot.vOutro := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vIPI  := ConverteStrToNumero(LerCampo(Grupo,'Valor do IPI'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Total do IPI') then
        cXML.InfNFe.Total.ICMSTot.vIPI := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vNF   := ConverteStrToNumero(LerCampo(Grupo,'Valor Total da NFe'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Total da NFe') then
        cXML.InfNFe.Total.ICMSTot.vNF := ConverteStrToNumero(Valor);


      (*
      Incluida condicional que Verifica a vers�o do XML e ent�o atribui qual o
         texto de busca que dever� ser procurado no arquivo.
      *)
      //sTexto := IfThen(Trim(Versao) = '2.00', 'Valor Total dos Descontos', 'Valor dos Descontos');
      //cXML.InfNFe.Total.ICMSTot.vDesc   := ConverteStrToNumero(LerCampo(Grupo, sTexto));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Total dos Descontos') then
        cXML.InfNFe.Total.ICMSTot.vDesc := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vII   := ConverteStrToNumero(LerCampo(Grupo,'Valor do II'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Total do II') then
        cXML.InfNFe.Total.ICMSTot.vII := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vPIS  := ConverteStrToNumero(LerCampo(Grupo,'Valor do PIS'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor do PIS') then
        cXML.InfNFe.Total.ICMSTot.vPIS := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Total.ICMSTot.vCOFINS := ConverteStrToNumero(LerCampo(Grupo,'Valor da COFINS'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor da COFINS') then
        cXML.InfNFe.Total.ICMSTot.vCOFINS := ConverteStrToNumero(Valor);

    end;

  end;

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(11, 'Dados dos Produtos e Servi�os', Grupo);


(*
  //ArquivoRestante := SeparaApartir('CNPJ', ArquivoRestante);
  Inocuo := 'DESTINAT�RIO |&|';
  ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
  Grupo :=  SeparaAte('EMISS�O', ArquivoRestante, ArquivoRestante);
*)
  ArquivoRestante := copy(ArquivoRestante,pos(AnsiUpperCase('Dados do Transporte'),AnsiUpperCase(ArquivoRestante)),length(ArquivoRestante));
  Grupo :=  SeparaAte('Dados de Cobran�a',ArquivoRestante,ArquivoItens);

  Inocuo := 'Dados do Transporte|&|';
  if pos(Inocuo, Grupo) = 1 then
  begin
    Grupo := Copy(Grupo, Length(Inocuo) + 1);

    //cXML.InfNFe.Transp.modFrete := StrTomodFrete( ok, LerCampo(Grupo,'Modalidade do Frete',1) );
    if dmk_DefValDeText(Valor, Grupo, 'Modalidade do Frete') then
      cXML.InfNFe.Transp.modFrete := Geral.SoNumero_TT(Copy(Valor, 1, 2));

    //cXML.InfNFe.Transp.Transporta.CNPJCPF := OnlyNumber(LerCampo(Grupo,'CNPJ'));

    Inocuo := 'TRANSPORTADOR |&|';
    if pos(Inocuo, Grupo) = 1 then
    begin
      Grupo := Copy(Grupo, Length(Inocuo) + 1);

      Multipl := '';
      if dmk_DefValDeText(Valor, Grupo, 'CNPJ') then
        Multipl := Multipl + Geral.SoNumero_TT(Valor);
      if dmk_DefValDeText(Valor, Grupo, 'CPF') then
        Multipl := Multipl + Geral.SoNumero_TT(Valor);
      if Length(Multipl) >= 14 then
        cXML.InfNFe.Transp.Transporta.CNPJ := Geral.SoNumero_TT(Multipl)
      else
        cXML.InfNFe.Transp.Transporta.CPF := Geral.SoNumero_TT(Multipl);

      //cXML.InfNFe.Transp.Transporta.xNome   := LerCampo(Grupo,'Raz�o Social / Nome');
      if dmk_DefValDeText(Valor, Grupo, 'Raz�o Social / Nome') then
        cXML.InfNFe.Transp.Transporta.xNome := Valor;

      //cXML.InfNFe.Transp.Transporta.IE      := LerCampo(Grupo,'Inscri��o Estadual');
      if dmk_DefValDeText(Valor, Grupo, 'Inscri��o Estadual') then
        cXML.InfNFe.Transp.Transporta.IE := Valor;

      //cXML.InfNFe.Transp.Transporta.xEnder  := LerCampo(Grupo,'Endere�o Completo');
      if dmk_DefValDeText(Valor, Grupo, 'Endere�o Completo') then
        cXML.InfNFe.Transp.Transporta.xEnder := Valor;

      //cXML.InfNFe.Transp.Transporta.xMun    := LerCampo(Grupo,'Munic�pio');
      if dmk_DefValDeText(Valor, Grupo, 'Munic�pio') then
        cXML.InfNFe.Transp.Transporta.xMun := Valor;

      //cXML.InfNFe.Transp.Transporta.UF      := LerCampo(Grupo,'UF');
      if dmk_DefValDeText(Valor, Grupo, 'UF') then
        cXML.InfNFe.Transp.Transporta.UF := Valor;

      //cXML.InfNFe.Transp.veicTransp.placa   := LerCampo(Grupo,'Placa');
      if dmk_DefValDeText(Valor, Grupo, 'Placa') then
        cXML.InfNFe.Transp.veicTransp.placa := Valor;

      //cXML.InfNFe.Transp.veicTransp.UF      := LerCampo(Grupo,'UF');
      if dmk_DefValDeText(Valor, Grupo, 'UF') then
        cXML.InfNFe.Transp.veicTransp.UF := Valor;
    end;

    Inocuo := 'VOLUMES |&|';
    if pos(Inocuo, Grupo) = 1 then
    begin
      Grupo := Copy(Grupo, Length(Inocuo) + 1);


//VOLUMES |&|Quantidade 1|&|Esp�cie VOLUME|&|Peso L�quido 20|&|Peso Bruto 20|&|

      i := 0;
      cXML.InfNFe.Transp.Vol.Add;
      //cXML.InfNFe.Transp.Vol[i].qVol  := StrToIntDef(LerCampo(Grupo,'Quantidade'),0);
      if dmk_DefValDeText(Valor, Grupo, 'Quantidade') then
        cXML.InfNFe.Transp.Vol[i].qVol  := Valor;

      //cXML.InfNFe.Transp.vol[i].esp   := LerCampo(Grupo,'Esp�cie');
      if dmk_DefValDeText(Valor, Grupo, 'Esp�cie') then
        cXML.InfNFe.Transp.Vol[i].esp  := Valor;

      //cXML.InfNFe.Transp.Vol[i].marca := LerCampo(Grupo,'Marca');
      if dmk_DefValDeText(Valor, Grupo, 'Marca') then
        cXML.InfNFe.Transp.Vol[i].marca  := Valor;

      //cXML.InfNFe.Transp.Vol[i].nVol  := LerCampo(Grupo,'Numera��o');
      if dmk_DefValDeText(Valor, Grupo, 'Numera��o') then
        cXML.InfNFe.Transp.Vol[i].nVol  := Valor;

      //cXML.InfNFe.Transp.Vol[i].pesoL := ConverteStrToNumero(LerCampo(Grupo,'Peso L�quido'));
      if dmk_DefValDeText(Valor, Grupo, 'Peso L�quido') then
        cXML.InfNFe.Transp.Vol[i].pesoL  := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Transp.Vol[i].pesoB := ConverteStrToNumero(LerCampo(Grupo,'Peso Bruto'));
      if dmk_DefValDeText(Valor, Grupo, 'Peso Bruto') then
        cXML.InfNFe.Transp.Vol[i].pesoB  := ConverteStrToNumero(Valor);

      // Falta rotina para pegar v�rios volumes
      //Inc(i);
    end;
  end;

  if Trim(Grupo) <> '' then
    AvisaTextoNaoProcessado(12, 'Dados do Transporte', Grupo);

  Inocuo := 'Dados de Cobran�a|&|';
  if pos(Inocuo, ArquivoRestante) > 0 then
  begin
    ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
    Grupo :=  SeparaAte('Informa��es Adicionais', ArquivoRestante, ArquivoRestante);

    Inocuo := 'FATURA|&|';
    if pos(Inocuo, Grupo) = 1 then
    begin
      Grupo := Copy(Grupo, Length(Inocuo) + 1);

      //cXML.InfNFe.Cobr.Fat.nFat  := LerCampo(Grupo,'N�mero');
      if dmk_DefValDeText(Valor, Grupo, 'N�mero') then
        cXML.InfNFe.Cobr.Fat.nFat  := Valor;

      //cXML.InfNFe.Cobr.Fat.vOrig := ConverteStrToNumero(LerCampo(Grupo,'Valor Original'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Original') then
        cXML.InfNFe.Cobr.Fat.vOrig  := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Cobr.Fat.vDesc := ConverteStrToNumero(LerCampo(Grupo,'Valor Desconto'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor Desconto') then
        cXML.InfNFe.Cobr.Fat.vDesc  := ConverteStrToNumero(Valor);

      //cXML.InfNFe.Cobr.Fat.vLiq  := ConverteStrToNumero(LerCampo(Grupo,'Valor L�quido'));
      if dmk_DefValDeText(Valor, Grupo, 'Valor L�quido') then
        cXML.InfNFe.Cobr.Fat.vLiq  := ConverteStrToNumero(Valor);
    end;

(*
DUPLICATAS|&|N�mero |&|Vencimento |&|Valor |&|002202-A|&|08/10/2011|&|1.381,30|&|002202-B|&|07/11/2011|&|1.381,30|&|          else|&|
*)
    Inocuo := 'DUPLICATAS|&|';
    if pos(Inocuo, Grupo) = 1 then
    begin
      Grupo := Copy(Grupo, Length(Inocuo) + 1);

      Inocuo := 'Valor |&|';
      Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo) + 1);

      i := 0;
      while Length(Geral.SONumero_TT(Grupo)) > 0 do
      begin
        cXML.InfNFe.Cobr.Dup.Add;

        Grupo := '|&|' + Grupo;
        //cXML.InfNFe.Cobr.Dup[i].nDup  := copy(ArquivoDuplicatas,1,pos('|&|',ArquivoDuplicatas)-1);
        if dmk_DefValDeText(Valor, Grupo, '|&|') then
          cXML.InfNFe.Cobr.Dup[i].nDup  := Valor;

        Grupo := '|&|' + Grupo;
        //cXML.InfNFe.Cobr.Dup[i].dVenc := FormatDateTime('yyyy-mm-dd', StrToDateDef(copy(ArquivoDuplicatas,1,pos('|&|',ArquivoDuplicatas)-1),0));
        if dmk_DefValDeText(Valor, Grupo, '|&|') then
          cXML.InfNFe.Cobr.Dup[i].dVenc  := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(Valor, True, False));

        Grupo := '|&|' + Grupo;
        //cXML.InfNFe.Cobr.Dup[i].vDup  := ConverteStrToNumero(copy(ArquivoDuplicatas,1,pos('|&|',ArquivoDuplicatas)-1));;;
        if dmk_DefValDeText(Valor, Grupo, '|&|') then
          cXML.InfNFe.Cobr.Dup[i].vDup  := ConverteStrToNumero(Valor);

        //

        Inc(i);
      end;
    end;
    //
    if Trim(Grupo) = 'else|&|' then
       Grupo := '';
    //
    if Trim(Grupo) <> '' then
      AvisaTextoNaoProcessado(13, 'Dados de Cobran�a', Grupo);

  end;

  Inocuo := 'Informa��es Adicionais |&|';
  if pos(Inocuo, ArquivoRestante) > 0 then
  begin
    ArquivoRestante := Copy(ArquivoRestante, pos(Inocuo, ArquivoRestante) + Length(Inocuo));
    // j� separou no in�cio por causa de NFe canceladas
    //Grupo :=  SeparaAte('Conhe�a a NF-e', ArquivoRestante, ArquivoRestante);
    Grupo := ArquivoRestante;
    
    if dmk_DefValDeText(Valor, Grupo, 'Formato de Impress�o') then
      cXML.InfNFe.ide.TpImp := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if dmk_DefValDeText(Valor, Grupo, 'Formato de Emiss�o') then
      cXML.InfNFe.ide.TpEmis := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if dmk_DefValDeText(Valor, Grupo, 'D�gito Verificador da Chave de Acesso') then
      cXML.InfNFe.ide.cDV := Geral.SoNumero_TT(Valor);

    if dmk_DefValDeText(Valor, Grupo, 'Identifica��o do Ambiente') then
      cXML.InfNFe.ide.tpAmb := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if dmk_DefValDeText(Valor, Grupo, 'Finalidade') then
      cXML.InfNFe.ide.FinNFe := Geral.SoNumero_TT(Copy(Valor, 1, 1));

    if dmk_DefValDeText(Valor, Grupo, 'Processo') then
      cXML.InfNFe.ide.procEmi := Geral.SoNumero_TT(Copy(Valor, 1, 1));

    if dmk_DefValDeText(Valor, Grupo, 'Vers�o') then
      cXML.InfNFe.ide.verProc := Valor;

    Inocuo := 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO FISCO |&|';
    if pos(Inocuo, Grupo) > 0 then
    begin
      Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo));

      if dmk_DefValDeText(Valor, Grupo, 'Descri��o') then
        cXML.InfNFe.infAdic.InfAdFisco := Valor;
    end;

    Inocuo := 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO CONTRIBUINTE |&|';
    if pos(Inocuo, Grupo) > 0 then
    begin
      Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo));

      if dmk_DefValDeText(Valor, Grupo, 'Descri��o') then
        cXML.InfNFe.infAdic.InfCpl := Valor;
    end;

(*
O texto abaixo n�o foi processado:
-----------------------------------------------------------------
Grupo: "Informa��es Adicionais"
ID: 14
-----------------------------------------------------------------
OBSERVA��ES DO FISCO |&|
Campo |&|
Texto |&|
Texto|&|
OBSERVA��ES DO CONTRIBUINTE |&|
Campo |&|
Texto |&|
I Doc.Emit ME/EPP Opt.Simp.Nacional|&|
*)

    Inocuo := 'OBSERVA��ES DO FISCO |&|';
    if pos(Inocuo, Grupo) > 0 then
    begin
      Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo));
      repeat
        //J0 := 0;
        J1 := pos(AnsiUpperCase('Campo'), AnsiUpperCase(Grupo));
        //
        if (J1 = 1) then
        begin
          if dmk_DefValDeText(Valor, Grupo, 'Campo') then
          begin
            // 2013-05-07
            cObsFisco := cXML.InfNFe.infAdic.ObsFisco.Add;
            //cObsFisco := cXML.InfNFe.infAdic.ObsFisco;
            // FIM 2013-05-07
            cObsFisco.XCampo := Valor;
            Texto := '';
            repeat
              J2 := pos(AnsiUpperCase('Texto'), AnsiUpperCase(Grupo));
              if J2 = 1 then
              begin
                if dmk_DefValDeText(Valor, Grupo, 'Texto') then
                Texto := Texto + ' ' + Valor;
              end;
            until (J2 <> 1);
            cObsFisco.XTexto := Texto;
          end;
        end;
        //J0 := J0 + 1;
      until (J1 <> 1);
    end;

    Inocuo := 'OBSERVA��ES DO CONTRIBUINTE ';
    if pos(Inocuo, Grupo) > 0 then
    begin
      Grupo := Copy(Grupo, pos(Inocuo, Grupo) + Length(Inocuo));
      repeat
        //J0 := 0;
        J1 := pos(AnsiUpperCase('Campo'), AnsiUpperCase(Grupo));
        //
        if (J1 = 1) then
        begin
          if dmk_DefValDeText(Valor, Grupo, 'Campo') then
          begin
            // 2013-05-07
            cObsCont := cXML.InfNFe.infAdic.ObsCont.Add;
            //cObsCont := cXML.InfNFe.infAdic.ObsCont;
            // FIM 2013-05-07
            cObsCont.XCampo := Valor;
            Texto := '';
            repeat
              J2 := pos(AnsiUpperCase('Texto'), AnsiUpperCase(Grupo));
              if J2 = 1 then
              begin
                if dmk_DefValDeText(Valor, Grupo, 'Texto') then
                Texto := Texto + ' ' + Valor;
              end;
            until (J2 <> 1);
            cObsCont.XTexto := Texto;
          end;
        end;
        //J0 := J0 + 1;
      until (J1 <> 1);
    end;

    if Trim(Grupo) <> '' then
      AvisaTextoNaoProcessado(14, 'Informa��es Adicionais', Grupo);
  end;
  //
  Status := Geral.IMV(cStat);
  if not DModG.ObtemEntidadeDeCNPJCFP(cXML.InfNFe.Dest.CNPJ + cXML.InfNFe.Dest.CPF,
  Empresa) then
    Geral.MB_Aviso('O CNPJ/CPF ' + cXML.InfNFe.Dest.CNPJ + cXML.InfNFe.Dest.CPF +
    ' n�o pertence a uma entidade (Empresa ou filial) cadastrada!')
  else begin
    Result := DModG.ReopenEntiCliInt(Empresa);
    if not Result then
      Geral.MB_Aviso('O CNPJ/CPF ' + cXML.InfNFe.Dest.CNPJ +
      cXML.InfNFe.Dest.CPF + ' Empresa: "' + FormatFloat('0', Empresa) +
      '" n�o � uma Empresa ou filial v�lida!');
  end;
}
end;

(* N�o uso
function TFmNFeGeraXML_0310.LerCampo(Texto, NomeCampo: string;
  Tamanho: Integer): string;
var
  ConteudoTag: string;
  Inicio, Fim: integer;
begin
  NomeCampo := AnsiUpperCase(Trim(NomeCampo));
  Inicio := pos(NomeCampo, AnsiUpperCase(Texto));
  if Inicio = 0 then
    ConteudoTag := sCampoNulo
  else
  begin
    if Inicio > 1 then
    begin
      Geral.MB_('Texto n�o consumido:' + sLineBreak  +
      'Antes do campo: "' + NomeCampo + '"' + sLineBreak  + 'Texto:' + sLineBreak  +
      Copy(Texto, 1, Inicio - 1) + sLineBreak  + sLineBreak  + 'AVISE A DERMATEK!',
      'AVISE A DERMATEK!', MB_OK+MB_ICONWARNING);
    end;
    Inicio := Inicio + Length(NomeCampo);
    if Tamanho > 0 then
       Fim := Tamanho
    else
     begin
       Texto := copy(Texto, Inicio, Length(Texto));
       Inicio := 0;
       Fim := pos('|&|',Texto)-1;
     end;
    ConteudoTag := Trim(copy(Texto, Inicio, Fim));
  end;
  try
     Result := ConteudoTag;
  except
     raise Exception.Create('Conte�do inv�lido. ' + ConteudoTag);
  end;
end;
*)

function TFmNFeGeraXML_0310.LerCampoA(var Texto: String;
  const NomeCampo: string; const Tamanho: Integer): string;
var
  ConteudoTag, nC: string;
  Inicio, Fim: integer;
begin
  nC := AnsiUpperCase(Trim(NomeCampo));
  Inicio := pos(nC, AnsiUpperCase(Texto));
  if Inicio = 0 then
    ConteudoTag := sCampoNulo
  else
  begin
    if Inicio > 1 then
    begin
      Geral.MB_Aviso('Texto n�o consumido:' + sLineBreak  + 'Antes do campo: "'
      + NomeCampo + '"' + sLineBreak  + 'Texto:' + sLineBreak  +
      Copy(Texto, 1, Inicio - 1) + sLineBreak  + sLineBreak  +
      'AVISE A DERMATEK!');
    end;
    Inicio := Inicio + Length(nC);
    if Tamanho > 0 then
      Fim := Tamanho
    else
    begin
      Texto := copy(Texto, Inicio, Length(Texto));
      Inicio := 0;
      Fim := pos('|&|',Texto)-1;
    end;
    ConteudoTag := Trim(copy(Texto, Inicio, Fim));
    Texto := Copy(Texto, pos('|&|',Texto) + 3);
  end;
  try
     Result := ConteudoTag;
  except
     raise Exception.Create('Conte�do inv�lido. ' + ConteudoTag);
  end;
end;

//A a��o "Enviar lote de eventos da NFe" n�o est� implementada! AVISE A DERMATEK!

//ver nome do arquivo - Linha 810?
end.
