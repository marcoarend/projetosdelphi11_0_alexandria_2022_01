unit FatPedNFs_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DmkDAC_PF,
  Grids, DBGrids, Mask, DBCtrls, dmkGeral, MyDBCheck, Menus, dmkEdit,
  dmkDBGrid, dmkDBLookupComboBox, dmkEditCB, dmkLabel, Variants, UnDmkProcFunc,
  dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkPageControl, dmkRadioGroup, dmkValUsu;

type
  TFmFatPedNFs_0310 = class(TForm)
    Panel1: TPanel;
    QrFatPedNFs: TmySQLQuery;
    DsFatPedNFs: TDataSource;
    DBGrid1: TDBGrid;
    QrImprime: TmySQLQuery;
    DsImprime: TDataSource;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    QrFatPedNFsFilial: TIntegerField;
    QrFatPedNFsIDCtrl: TIntegerField;
    QrFatPedNFsTipo: TSmallintField;
    QrFatPedNFsOriCodi: TIntegerField;
    QrFatPedNFsEmpresa: TIntegerField;
    QrFatPedNFsNumeroNF: TIntegerField;
    QrFatPedNFsIncSeqAuto: TSmallintField;
    QrFatPedNFsAlterWeb: TSmallintField;
    QrFatPedNFsAtivo: TSmallintField;
    QrFatPedNFsSerieNFCod: TIntegerField;
    QrFatPedNFsSerieNFTxt: TWideStringField;
    QrFatPedNFsCO_ENT_EMP: TIntegerField;
    QrFatPedNFsDataCad: TDateField;
    QrFatPedNFsDataAlt: TDateField;
    QrFatPedNFsDataAlt_TXT: TWideStringField;
    QrFatPedNFsFreteVal: TFloatField;
    QrFatPedNFsSeguro: TFloatField;
    QrFatPedNFsOutros: TFloatField;
    QrFatPedNFsPlacaUF: TWideStringField;
    QrFatPedNFsPlacaNr: TWideStringField;
    QrFatPedNFsEspecie: TWideStringField;
    QrFatPedNFsMarca: TWideStringField;
    QrFatPedNFsNumero: TWideStringField;
    QrFatPedNFskgBruto: TFloatField;
    QrFatPedNFskgLiqui: TFloatField;
    PMGeraNF: TPopupMenu;
    GeraNF1: TMenuItem;
    AlteradadosNF1: TMenuItem;
    QrFatPedNFsQuantidade: TWideStringField;
    QrFatPedNFsObservacao: TWideStringField;
    QrFatPedNFsCFOP1: TWideStringField;
    QrTotal: TmySQLQuery;
    Panel6: TPanel;
    EdPedido: TdmkEdit;
    Label10: TLabel;
    dmkDBGrid1: TdmkDBGrid;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    DsFiliais: TDataSource;
    QrParamsNFs: TmySQLQuery;
    QrParamsNFsCodigo: TIntegerField;
    QrParamsNFsControle: TIntegerField;
    QrParamsNFsSequencial: TIntegerField;
    DsParamsNFs: TDataSource;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    EdSerieNF: TdmkEditCB;
    CBSerieNF: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFatPedCab: TmySQLQuery;
    DsFatPedCab: TDataSource;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabNO_EMP: TWideStringField;
    QrFatPedCabNO_CLI: TWideStringField;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedNFsDtEmissNF: TDateField;
    QrFatPedNFsDtEntraSai: TDateField;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    DsNFeCabA: TDataSource;
    PMNFe: TPopupMenu;
    RecriatodaNFe1: TMenuItem;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrFatPedNFsStatus: TIntegerField;
    QrFatPedNFsinfProt_cStat: TIntegerField;
    QrParamsNFsSerieNF: TIntegerField;
    QrFatPedNFsinfAdic_infCpl: TWideMemoField;
    QrFatPedNFsinfCanc_cStat: TIntegerField;
    QrFatPedCabDTAEMISS_TXT: TWideStringField;
    QrFatPedCabDTAENTRA_TXT: TWideStringField;
    QrFatPedCabDTAINCLU_TXT: TWideStringField;
    QrFatPedCabDTAPREVI_TXT: TWideStringField;
    QrFatPedNFsDTEMISSNF_TXT: TWideStringField;
    QrFatPedNFsDTENTRASAI_TXT: TWideStringField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    QrTotalQtde: TLargeintField;
    QrFatPedCabinfAdFisco: TWideStringField;
    QrFatPedNFsRNTC: TWideStringField;
    QrFatPedNFsUFembarq: TWideStringField;
    QrFatPedNFsxLocEmbarq: TWideStringField;
    SpeedButton1: TSpeedButton;
    Continuaprocessonormaldecriao1: TMenuItem;
    QrFatPedCabUF_TXT_emp: TWideStringField;
    QrFatPedCabUF_TXT_cli: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrFatPedCabFinanceiro: TSmallintField;
    QrFatPedNFside_tpNF: TSmallintField;
    QrParamsEmpAssociada: TIntegerField;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrImprimeNO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrFatPedNFsHrEntraSai: TTimeField;
    QrFatPedNFside_dhCont: TDateTimeField;
    QrFatPedNFside_xJust: TWideStringField;
    QrFatPedNFsemit_CRT: TSmallintField;
    QrFatPedNFsdest_email: TWideStringField;
    QrFatPedNFsvagao: TWideStringField;
    QrFatPedNFsbalsa: TWideStringField;
    QrFatPedCabtpNF: TSmallintField;
    AlteradadosNFe1: TMenuItem;
    QrNFeCabAIDCtrl: TIntegerField;
    QrFatPedNFsCabA_FatID: TIntegerField;
    ApenasGeraXML1: TMenuItem;
    N1: TMenuItem;
    este1: TMenuItem;
    QrParamsEmpNFetpEmis: TSmallintField;
    QrCTG: TmySQLQuery;
    QrCTGORDEM: TLargeintField;
    QrCTGtpEmis: TIntegerField;
    QrCTGdhEntrada: TDateTimeField;
    QrCTGNome: TWideStringField;
    QrFatPedNFsCompra_XNEmp: TWideStringField;
    QrFatPedNFsCompra_XPed: TWideStringField;
    QrFatPedNFsCompra_XCont: TWideStringField;
    QrFatPedCabCODMUNICI: TFloatField;
    QrParamsEmpCRT: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel10: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel11: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    QrParamsEmpNFeNT2013_003LTT: TSmallintField;
    BtNFe: TBitBtn;
    BtGeraNF: TBitBtn;
    SbNFePesq: TBitBtn;
    QrFatPedCabTotalFatur: TFloatField;
    QrFatPedCabDtaEmiss: TDateField;
    QrFatPedCabDtaEntra: TDateField;
    QrFatPedCabDtaInclu: TDateField;
    QrFatPedCabDtaPrevi: TDateField;
    QrFatPedCabFretePor: TIntegerField;
    QrFatPedCabModeloNF: TFloatField;
    QrFatPedCabRegrFiscal: TFloatField;
    QrFatPedCabCartEmis: TFloatField;
    QrFatPedCabTabelaPrc: TFloatField;
    QrFatPedCabCondicaoPG: TFloatField;
    QrFatPedCabTransporta: TFloatField;
    QrFatPedCabEmpresa: TFloatField;
    QrFatPedCabCliente: TFloatField;
    QrFatPedCabidDest: TSmallintField;
    QrFatPedNFsdhEmiTZD: TFloatField;
    QrFatPedNFsdhSaiEntTZD: TFloatField;
    QrFatPedCabindFinal: TSmallintField;
    QrFatPedCabindPres: TSmallintField;
    QrFatPedCab1: TmySQLQuery;
    QrFatPedCab1MedDDSimpl: TFloatField;
    QrFatPedCab1Codigo: TIntegerField;
    QrFatPedCab1CodUsu: TIntegerField;
    QrFatPedCab1Encerrou: TDateTimeField;
    QrFatPedCab1NO_EMP: TWideStringField;
    QrFatPedCab1CODMUNICI: TFloatField;
    QrFatPedCab1UF_TXT_emp: TWideStringField;
    QrFatPedCab1UF_TXT_cli: TWideStringField;
    QrFatPedCab1NO_CLI: TWideStringField;
    QrFatPedCab1ModeloNF: TIntegerField;
    QrFatPedCab1infAdFisco: TWideStringField;
    QrFatPedCab1Financeiro: TSmallintField;
    QrFatPedCab1TotalFatur: TFloatField;
    QrFatPedCab1tpNF: TSmallintField;
    QrFatPedCab1DtaEmiss: TDateField;
    QrFatPedCab1DtaEntra: TDateField;
    QrFatPedCab1DtaInclu: TDateField;
    QrFatPedCab1DtaPrevi: TDateField;
    QrFatPedCab1RegrFiscal: TIntegerField;
    QrFatPedCab1CartEmis: TIntegerField;
    QrFatPedCab1TabelaPrc: TIntegerField;
    QrFatPedCab1CondicaoPG: TIntegerField;
    QrFatPedCab1FretePor: TSmallintField;
    QrFatPedCab1Transporta: TIntegerField;
    QrFatPedCab1Empresa: TIntegerField;
    QrFatPedCab1Cliente: TIntegerField;
    QrFatPedCab1idDest: TSmallintField;
    QrFatPedCab1indFinal: TSmallintField;
    QrFatPedCab1indPres: TSmallintField;
    QrFatPedNFsHrEmi: TTimeField;
    QrFatPedCabindSinc: TSmallintField;
    BtRapido: TBitBtn;
    Envioorlotemanual1: TMenuItem;
    Passo2EditarNFesenecessriodadosespecficos1: TMenuItem;
    Passo4Mostrarjaneladelotes1: TMenuItem;
    QrFatPedCabTpCalcTrib: TSmallintField;
    BtAnterior: TBitBtn;
    BtProximo: TBitBtn;
    VuEmpresa: TdmkValUsu;
    QrFatPedCabPedidoCli: TWideStringField;
    BtLote: TBitBtn;
    QrFatPedNFsLoteEnv: TIntegerField;
    PageControl1: TPageControl;
    TSEtapas: TTabSheet;
    TSOutros: TTabSheet;
    PCEtapas: TdmkPageControl;
    TabSheet1: TTabSheet;
    BtGeraNFe: TBitBtn;
    TabSheet2: TTabSheet;
    Label19: TLabel;
    BtEditaNFe: TBitBtn;
    TabSheet3: TTabSheet;
    Label20: TLabel;
    BtEnviaNFe: TBitBtn;
    CkNFeUnica: TCheckBox;
    TBEtapas: TTrackBar;
    DBMemo2: TDBMemo;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    Label15: TLabel;
    Label1: TLabel;
    Label18: TLabel;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit16: TDBEdit;
    DBMemo1: TDBMemo;
    LaValida: TLabel;
    QrNFeCabAId: TWideStringField;
    QrFatPedCabfinNFe: TSmallintField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCIDADE: TWideStringField;
    QrClientesNOMEUF: TWideStringField;
    QrClientesCodUsu: TIntegerField;
    QrClientesIE: TWideStringField;
    QrClientesindIEDest: TIntegerField;
    QrClientesTipo: TIntegerField;
    QrClientesDOCENT: TWideStringField;
    DsClientes: TDataSource;
    QrFatPedCabId: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFatPedNFsBeforeClose(DataSet: TDataSet);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure BtGeraNFClick(Sender: TObject);
    procedure QrFatPedNFsCalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure GeraNF1Click(Sender: TObject);
    procedure AlteradadosNF1Click(Sender: TObject);
    procedure PMGeraNFPopup(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdPedidoChange(Sender: TObject);
    procedure EdSerieNFChange(Sender: TObject);
    procedure QrFatPedCabAfterScroll(DataSet: TDataSet);
    procedure QrFatPedCabBeforeClose(DataSet: TDataSet);
    procedure BtNFeClick(Sender: TObject);
    procedure RecriatodaNFe1Click(Sender: TObject);
    procedure Continuaprocessonormaldecriao1Click(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure QrFatPedCabCalcFields(DataSet: TDataSet);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure SpeedButton1Click(Sender: TObject);
    procedure AlteradadosNFe1Click(Sender: TObject);
    procedure ApenasGeraXML1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SbNFePesqClick(Sender: TObject);
    procedure BtRapidoClick(Sender: TObject);
    procedure Passo2EditarNFesenecessriodadosespecficos1Click(Sender: TObject);
    procedure Passo4Mostrarjaneladelotes1Click(Sender: TObject);
    procedure BtAnteriorClick(Sender: TObject);
    procedure BtProximoClick(Sender: TObject);
    procedure CkNFeUnicaClick(Sender: TObject);
    procedure PCEtapasChange(Sender: TObject);
    procedure BtGeraNFeClick(Sender: TObject);
    procedure BtEditaNFeClick(Sender: TObject);
    procedure BtEnviaNFeClick(Sender: TObject);
    procedure BtLoteClick(Sender: TObject);
    procedure LaValidaMouseEnter(Sender: TObject);
    procedure LaValidaMouseLeave(Sender: TObject);
    procedure LaValidaClick(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
  private
    { Private declarations }
    F_finNFe: MyArrayLista;
    procedure ReopenQrImprime();
    procedure ReopenNFeCabA();
    procedure MostraEdicao(Acao: TSQLType);
    //procedure ReopenParamsNFs(Controle: Integer);
    procedure ReopenFatPedCab(Id: Integer);
    //procedure CriaNFe(Recria: Boolean);
    procedure EditaNFe();
    procedure EnvioNFeAutomatico();
    procedure ConfiguraEtapas(Etapa: Integer);
  public
    { Public declarations }
    FNaoCriouXML: Boolean;
    function ReopenFatPedNFs(Filial: Integer): Boolean;
    function GerarNFe(Recria, ApenasGeraXML, CalculaAutomatico: Boolean): Boolean;
  end;

  var
  FmFatPedNFs_0310: TFmFatPedNFs_0310;

  const
  FThisFatID = 1;

implementation

uses UnMyObjects, (*FatPedCab,*) UMySQLModule, Module, ModuleGeral, (*NF1b,*)
  NFaEdit_0310, ModPediVda, UnInternalconsts, ModuleNFe_0000, Principal,
  NFeCabA_0000, NFe_PF, UnDmkWeb;

{$R *.DFM}

procedure TFmFatPedNFs_0310.AlteradadosNF1Click(Sender: TObject);
begin
  MostraEdicao(stUpd);
end;

procedure TFmFatPedNFs_0310.BtNFeClick(Sender: TObject);
begin
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
    Exit;
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmFatPedNFs_0310.BtProximoClick(Sender: TObject);
var
  Etapa: Integer;
begin
  if PCEtapas.ActivePageIndex < 2 then
  begin
    Etapa := PCEtapas.ActivePageIndex + 1;
    //
    ConfiguraEtapas(Etapa);
  end;
end;

procedure TFmFatPedNFs_0310.BtRapidoClick(Sender: TObject);
  procedure Editavel(CalculaAuto: Boolean);
  begin
    FNaoCriouXML := False;
    //
    GerarNFe(True, False, CalculaAuto);
    EditaNFe();
    EnvioNFeAutomatico();
  end;
const
  CalculoAutomatico = True;
begin
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
    Exit;
  case TTpCalcTribNFe(QrFatPedCabTpCalcTrib.Value) of
    (*0*)tctnfeIndefinido:
      Geral.MB_Aviso('Tipo de c�lculo n�o definido na regra fiscal!');
    tctnfeAutomatico: EnvioNFeAutomatico();
    tctnfeManual: Editavel(not CalculoAutomatico);
    tctnfeMixto:  Editavel(CalculoAutomatico);
  end;
end;

function TFmFatPedNFs_0310.GerarNFe(Recria, ApenasGeraXML, CalculaAutomatico: Boolean): Boolean;
const
  CNPJCPFAvulso = '';
  RazaoNomeAvulso = '';
  InfIntermedEnti = 0;
  EmiteAvulso = False;
var
  ide_serie: Variant;
  ide_dEmi, ide_dSaiEnt: TDateTime;
  FatNum, Empresa, Cliente, ide_indPag, ide_nNF, ide_tpNF, ide_tpEmis,
  modFrete, Transporta, NFeStatus, FretePor: Integer;
  //
  IDCtrl: Integer;
  //
  CodTXT, EmpTXT, infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq,
  SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String;
  //
  FreteVal, Seguro, Outros: Double;
  GravaCampos, RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, cNF_Atual,
  Financeiro: Integer;
  //
  UF_TXT_Emp, UF_TXT_Cli: String;
  //
  //2.00
  ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime;
  emit_CRT: Integer;
  ide_xJust, dest_email, Vagao, Balsa: String;
  // fim 2.00
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  idDest, indFinal, indPres, finNFe: Integer;
  ide_hEmi: TTime;
  ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  // Fim NFe 3.10
  // NFe 4.00 NT 2018/5
  RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti,
  L_Ativo: Integer;
  // Fim NFe 4.00 NT 2018/5
begin
  RetiradaUsa  := -1;
  RetiradaEnti := -1;
  EntregaUsa   := -1;
  EntregaEnti  := -1;
  L_Ativo      := -1;
  //
  ReopenFatPedCab(QrFatPedCabId.Value);
  //
  if ApenasGeraXML then
    ConfiguraEtapas(1);
  //
  Result     := False;
  FatNum     := QrFatPedCabCodigo.Value;
  Empresa    := QrFatPedNFsEmpresa.Value;
  IDCtrl     := QrFatPedNFsIDCtrl.Value;
  FreteVal   := QrFatPedNFsFreteVal.Value;
  Seguro     := QrFatPedNFsSeguro.Value;
  Outros     := QrFatPedNFsOutros.Value;
  RegrFiscal := Trunc(QrFatPedCabRegrFiscal.Value);
  CartEmiss  := Trunc(QrFatPedCabCartEmis.Value);
  TabelaPrc  := Trunc(QrFatPedCabTabelaPrc.Value);
  CondicaoPg := TrunC(QrFatPedCabCondicaoPG.Value);
  Financeiro := QrFatPedCabFinanceiro.Value;
  // NFe 3.10
  idDest     := QrFatPedCabidDest.Value;
  indFinal   := QrFatPedCabindFinal.Value;
  indPres    := QrFatPedCabindPres.Value;
  finNFe     := QrFatPedCabfinNFe.Value;
  //
  if not DmodG.QrFiliLog.Locate('Codigo', Empresa, []) then
  begin
    Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') diferente da logada (' + VAR_LIB_EMPRESAS +
      ')! � necess�rio logoff!');
    Exit;
  end;
  //
  if Geral.IMV(EdFilial.Text) = 0 then
  begin
    Geral.MB_Aviso('Empresa n�o definida!');
    EdFilial.SetFocus;
    Exit;
  end;
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
    Exit;
  if not DModG.DefineFretePor(QrFatPedCabFretePor.Value, FretePor, modFrete) then
    Exit;
  //
  Cliente     := QrClientesCodigo.Value;
  Transporta  := Trunc(QrFatPedCabTransporta.Value);
  ide_indPag  := MLAGeral.EscolhaDe2Int(QrFatPedCabMedDDSimpl.Value = 0, 0, 1);
  ide_Serie   := QrFatPedNFsSerieNFTxt.Value;
  ide_nNF     := QrFatPedNFsNumeroNF.Value;
  ide_dEmi    := QrFatPedNFsDtEmissNF.Value;
  ide_dSaiEnt := QrFatPedNFsDtEntraSai.Value;
  ide_tpNF    := QrFatPedCabtpNF.Value; // 1 = Sa�da
  // NFe 3.10
  ide_dhEmiTZD    := QrFatPedNFsdhEmiTZD.Value;
  ide_dhSaiEntTZD := QrFatPedNFsdhSaiEntTZD.Value;
  ide_hEmi        := QrFatPedNFsHrEmi.Value;
  // Nao Resolveu o erro!
   //ide_hEmi        := QrFatPedNFs.FieldByName('HrEmi').AsFloat;

  //ide_tpImp   :=  1-Retrato/ 2-Paisagem
  // 2011-08-25
  //ide_tpEmis  := 1;
  ide_tpEmis  := QrParamsEmpNFetpEmis.Value;
  if ide_tpEmis <> 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCTG, Dmod.MyDB, [
    'SELECT IF(Empresa=0, 1, 0) ORDEM,   ',
    'tpEmis, dhEntrada, Nome ',
    'FROM nfecntngnc ',
    'WHERE dhSaida < 2 ',
    'AND Empresa = 0 ',
    'OR Empresa=' + FormatFloat('0', Empresa),
    'ORDER BY ORDEM, dhEntrada DESC ',
    '']);
    if QrCTG.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
      'Refa�a a configura��o de conting�ncia!');
      Exit;
    end else
    begin
      ide_dhCont  := QrCTGdhEntrada.Value;
      ide_xJust   := QrCTGNome.Value;
    end;
  end else
  begin
    ide_dhCont  := 0;
    ide_xJust   := '';
  end;
  // fim 2011-08-25
  {1 � Normal � emiss�o normal;
   2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
   3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
   4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
   5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
  }
  infAdic_infAdFisco := QrFatPedCabinfAdFisco.Value;
  infAdic_infCpl     := QrFatPedNFsinfAdic_infCpl.Value;
  VeicTransp_Placa   := QrFatPedNFsPlacaNr.Value;
  VeicTransp_UF      := QrFatPedNFsPlacaUF.Value;
  VeicTransp_RNTC    := QrFatPedNFsRNTC.Value;
  Exporta_UFEmbarq   := QrFatPedNFsUFEmbarq.Value;
  Exporta_XLocEmbarq := QrFatPedNFsxLocEmbarq.Value;
  //
  CodTXT      := dmkPF.FFP(QrFatPedCabCodigo.Value, 0);
  EmpTXT      := dmkPF.FFP(QrFatPedCabEmpresa.Value, 0);
  UF_TXT_emp  := QrFatPedCabUF_TXT_emp.Value;
  UF_TXT_cli  := QrFatPedCabUF_TXT_cli.Value;
  SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
{
  'SELECT med.Sigla NO_UNIDADE, smva.Empresa, smva.Preco,          'sLineBreak +
  'smva.ICMS_Per, smva.IPI_Per, smva.GraGruX CU_PRODUTO,           'sLineBreak +
  'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,             'sLineBreak +
  'SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,             'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde, smva.CFOP,      'sLineBreak +
  'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         'sLineBreak +
  'gg1.CST_A, gg1.CST_B,                                           'sLineBreak +
  'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              'sLineBreak +
  'gg1.NCM, ncm.Letras,  gg1.IPI_CST, gg1.IPI_cEnq,                'sLineBreak +
  'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              'sLineBreak +
  'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,              'sLineBreak +
  'gg1.InfAdProd, smva.MedidaC, smva.MedidaL,                      'sLineBreak +
  'smva.MedidaA,smva.MedidaE,                                      'sLineBreak +
  'mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4,             'sLineBreak +
  'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4,                 'sLineBreak +
  'gta.PrintTam, gcc.PrintCor                                      'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             'sLineBreak +
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             'sLineBreak +
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             'sLineBreak +
  'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo                'sLineBreak +
  'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           'sLineBreak +
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                 'sLineBreak +
  'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem              'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT +                                    sLineBreak +
  'GROUP BY smva.Preco, smva.ICMS_Per, smva.IPI_Per, smva.GraGruX  ';
  //
  {
  SQL_CUSTOMZ :=
  'SELECT gg1.SiglaCustm                                           'sLineBreak +
  'FROM pedivdacuz pvc                                             'sLineBreak +
  'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX             'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'WHERE pvc.Controle=:P0                                          'sLineBreak +
  'AND gg1.SiglaCustm <> "";
  }
  SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  {
  'SELECT SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,      'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 'sLineBreak +
  'COUNT(DISTINCT OriCnta) + 0.000 Volumes                         'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT;
  }
  //
  SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  {
  'SELECT FatID, FatNum, Vencimento, Credito Valor,                'sLineBreak +
  'FatParcela, Duplicata                                           'sLineBreak +
  'FROM lan ctos                                                    'sLineBreak +
  'WHERE FatID = 1                                                 'sLineBreak +
  'AND FatNum=' + CodTXT +                                          sLineBreak +
  'AND CliInt=' + EmpTXT;
  }
  SQL_FAT_TOT :=  DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_VOLUMES :=  DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  {
  'SELECT Quantidade qVol, Especie esp, Marca marca,               'sLineBreak +
  'Numero nVol, kgLiqui pesoL, kgBruto pesoB                       'sLineBreak +
  'FROM stqmovnfsa                                                 'sLineBreak +
  'WHERE Tipo = 1                                                  'sLineBreak +
  'AND OriCodi=' + CodTXT +                                         sLineBreak +
  'AND Empresa=' + EmpTXT;
  }
(*
  NFeStatus := QrNFeCabAStatus.Value;
  if DBCheck.CriaFm(TFmNFeSteps_ 0 1 1 0 , FmNFeSteps_ 0 1 1 0 , afmoNegarComAviso) then
  begin
    FmNFeSteps_ 0 1 1 0 .FSource_IDCtrl := 'stqmovnfsa';
    FmNFeSteps_ 0 1 1 0 .Show;
    //
*)
    {
    GravaCampos := Geral.MB_Pergunta('Deseja salvar demonstrativo ' +
    ' no banco de dados para verifica��o de dados em relat�rio? ' + sLineBreak +
    'Este demostrativo n�o � obrigat�rio e sua grava��o pode ser demorada!' +
    sLineBreak + 'Deseja salvar assim mesmo?');
    }
    cNF_Atual := QrNFeCabAide_cNF.Value;
    GravaCampos := ID_NO;
(*
    if GravaCampos <> ID_CANCEL then
      FmNFeSteps_xxxx.Cria NFe Normal(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
        IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
        RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
        ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
        SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro)
    else
      Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
    //
    FmNFeSteps_XXXX.Destroy;
    //
*)
  // 2.00
  ide_hSaiEnt := QrFatPedNFsHrEntraSai.Value;
  emit_CRT    := QrFatPedNFsemit_CRT.Value;
  dest_email  := QrFatPedNFsdest_EMail.Value;
  Vagao       := QrFatPedNFsVagao.Value;
  Balsa       := QrFatPedNFsBalsa.Value;
  // fim 2.00
  Compra_XNEmp := QrFatPedNFsCompra_XNEmp.Value;
  Compra_XPed  := QrFatPedNFsCompra_XPed.Value;
  Compra_XCont := QrFatPedNFsCompra_XCont.Value;
  //
  NFeStatus := 0;  // T� certo???
  if UnNFe_PF.CriaNFe_vXX_XX(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
      IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
      RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, FreteVal, Seguro, Outros,
      ide_Serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
      infAdic_infAdFisco, infAdic_infCpl,
      VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
      Exporta_UFEmbarq, Exporta_xLocEmbarq,
      SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
      SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro,
      ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
      Compra_XNEmp, Compra_XPed, Compra_XCont,
      // NFe 3.10
      idDest, ide_hEmi, ide_dhEmiTZD, ide_dhSaiEntTZD, indFinal, indPres, finNFe,
      //
      RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti, L_Ativo, // NFe 4.00 NT 2018/5
      //
      ApenasGeraXML, CalculaAutomatico, CNPJCPFAvulso, RazaoNomeAvulso,
      EmiteAvulso, True, InfIntermedEnti)
  then
    Result := ReopenFatPedNFs(QrFatPedNFsFilial.Value)
  else
    Result := False;
(*
  end;
*)
  (*
  DESATIVADO EM: 05/04/2017
  MOTIVO: Estava excluindo as altera��es nas faturas e ao recriar estava zarando as altera��es

  if ApenasGeraXML then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DmNFe_0000.AtualizaLctoFaturaNFe(FThisFatID, FatNum, Empresa);
    finally
      Screen.Cursor := crDefault
    end;
  end;
  *)
end;

procedure TFmFatPedNFs_0310.LaValidaClick(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := DmNFe_0000.Obtem_Arquivo_XML_(VuEmpresa.ValueVariant,
                NFE_EXT_NFE_XML, QrNFeCabAid.Value, True);
  //
  UnNFe_PF.ValidaXML_NFe(Arquivo);
end;

procedure TFmFatPedNFs_0310.LaValidaMouseEnter(Sender: TObject);
begin
  LaValida.Font.Color := clBlue;
  LaValida.Font.Style := [fsUnderline];
end;

procedure TFmFatPedNFs_0310.LaValidaMouseLeave(Sender: TObject);
begin
  LaValida.Font.Color := clBlue;
  LaValida.Font.Style := [];
end;

procedure TFmFatPedNFs_0310.AlteradadosNFe1Click(Sender: TObject);
begin
  EditaNFe();
end;

procedure TFmFatPedNFs_0310.ApenasGeraXML1Click(Sender: TObject);
begin
  GerarNFe(True, True, True);
end;

procedure TFmFatPedNFs_0310.BtAnteriorClick(Sender: TObject);
var
  Etapa: Integer;
begin
  if PCEtapas.ActivePageIndex > 0 then
  begin
    Etapa := PCEtapas.ActivePageIndex - 1;
    //
    ConfiguraEtapas(Etapa);
  end;
end;

procedure TFmFatPedNFs_0310.BtEditaNFeClick(Sender: TObject);
begin
  if (FNaoCriouXML) or (QrNFeCabAIDCtrl.Value = 0) then
  begin
    Geral.MB_Aviso('Antes de editar a nota voc� deve cri�-la!');
    ConfiguraEtapas(0);
    Exit;
  end;
  EditaNFe();
end;

procedure TFmFatPedNFs_0310.BtEnviaNFeClick(Sender: TObject);
begin
  if (FNaoCriouXML) or (QrNFeCabAIDCtrl.Value = 0) then
  begin
    Geral.MB_Aviso('Antes de editar a nota voc� deve cri�-la!');
    ConfiguraEtapas(0);
    Exit;
  end;
  EnvioNFeAutomatico();
end;

procedure TFmFatPedNFs_0310.BtGeraNFClick(Sender: TObject);
begin
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
    Exit;
  MyObjects.MostraPopUpDeBotao(PMGeraNf, BtGeraNF);
end;

procedure TFmFatPedNFs_0310.BtGeraNFeClick(Sender: TObject);
begin
  FNaoCriouXML := False;
  //
  GerarNFe(True, False, True);
end;

procedure TFmFatPedNFs_0310.BtImprimeClick(Sender: TObject);
(*
var
  SQL_NCMs, SQL_ITS, SQL_Fat, SQL_TOT: String;
  CodTXT, EmpTXT: String;
*)
begin
  (*
  if QrImprimeTipoImpressao.Value = 3 then // NF1
  begin
    //colocar valor de despesas acess�rias etc.
    //
    if DBCheck.CriaFm(TFmNF1b, FmNF1b, afmoNegarComAviso) then
    begin
      FmNF1b.LimpaVars;
      //
      CodTXT := dmkPF.FFP(QrFatPedCabCodigo.Value, 0);
      EmpTXT := dmkPF.FFP(QrFatPedCabEmpresa.Value, 0);
      SQL_ITS :=
      'SELECT med.Sigla NO_UNIDADE, smva.Empresa, smva.Preco,          'sLineBreak +
      'smva.ICMS_Per, smva.IPI_Per, smva.GraGruX CU_PRODUTO,           'sLineBreak +
      'smva.ID CONTROLE,                                               'sLineBreak +
      'SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,             'sLineBreak +
      'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 'sLineBreak +
      'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         'sLineBreak +
      'gg1.CST_A, gg1.CST_B,                                           'sLineBreak +
      'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              'sLineBreak +
      'gg1.NCM, ncm.Letras, gcc.PrintCor,                              'sLineBreak +
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              'sLineBreak +
      'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per               'sLineBreak +
      'FROM stqmovvala smva                                            'sLineBreak +
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              'sLineBreak +
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             'sLineBreak +
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             'sLineBreak +
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             'sLineBreak +
      'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           'sLineBreak +
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                 'sLineBreak +
      'WHERE smva.Tipo=1                                               'sLineBreak +
      'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
      'AND smva.Empresa=' + EmpTXT +                                    sLineBreak +
      'GROUP BY smva.Preco, smva.ICMS_Per, smva.IPI_Per, smva.GraGruX  ';
      //
      SQL_TOT :=
      'SELECT SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,      'sLineBreak +
      'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 'sLineBreak +
      'COUNT(DISTINCT OriCnta) + 0.000 Volumes                         'sLineBreak +
      'FROM stqmovvala smva                                            'sLineBreak +
      'WHERE smva.Tipo=1                                               'sLineBreak +
      'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
      'AND smva.Empresa=' + EmpTXT;
      //
      FmNF1b.CarregaNF1Pro(SQL_ITS, SQL_TOT);
      //

      SQL_Fat :=
      'SELECT Vencimento, Credito, Duplicata                           'sLineBreak +
      'FROM ' + TabLctX                                                  +sLineBreak +
      'WHERE FatID=1                                                   'sLineBreak +
      'AND FatNum=' + CodTXT +                                          sLineBreak +
      'AND CliInt=' + EmpTXT;
      //
      FmNF1b.CarregaNF1Fat(SQL_Fat, QrFatPedCabModeloNF.Value);
      //

      FmNF1b.EdNFSerie.Text         := Geral.FF0(QrImprimeNO_SerieNF.Value);
      FmNF1b.EdNFNumero.Text        := Geral.FFT(QrImprimeSequencial.Value, 0, siNegativo);
      FmNF1b.EdCliente.Text         := Geral.FF0(QrFatPedCabCliente.Value);
      FmNF1b.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
      FmNF1b.EdBaseICMS.Text        := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdValorICMS.Text       := Geral.FFT(FmNF1b.QrTotalICMS_Val.Value, 2, siNegativo);
      FmNF1b.EdBaseICMS_Sub.Text    := '0,00';
      FmNF1b.EdValorICMS_Sub.Text   := '0,00';
      FmNF1b.EdTotProd.Text         := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdValorIPI.Text        := Geral.FFT(FmNF1b.QrTotalIPI_Val.Value, 2, siNegativo);
      FmNF1b.EdTotalNota.Text       := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdTransp.Text          := Geral.FF0(QrFatPedCabTransporta.Value);
      FmNF1b.CBTransp.KeyValue      := QrFatPedCabTransporta.Value;
      FmNF1b.EdFPC.Text             := Geral.FF0(QrFatPedCabFretePor.Value);
      FmNF1b.EdFrete.Text           := Geral.FFT(QrFatPedNFsFreteVal.Value, 2, siNegativo);
      FmNF1b.EdSeguro.Text          := Geral.FFT(QrFatPedNFsSeguro.Value, 2, siNegativo);
      FmNF1b.EdOutros.Text          := Geral.FFT(QrFatPedNFsOutros.Value, 2, siNegativo);
      FmNF1b.EdUFPlaca.Text         := QrFatPedNFsPlacaUF.Value;
      FmNF1b.EdPlaca.Text           := QrFatPedNFsPlacaNr.Value;
      FmNF1b.EdQuantidade.Text      := QrFatPedNFsQuantidade.Value;
      FmNF1b.EdEspecie.Text         := QrFatPedNFsEspecie.Value;
      FmNF1b.EdMarca.Text           := QrFatPedNFsMarca.Value;
      FmNF1b.EdNumero.Text          := QrFatPedNFsNumero.Value;
      FmNF1b.EdkgB.Text             := Geral.FFT(QrFatPedNFskgBruto.Value, 3, siNegativo);
      FmNF1b.EdkgL.Text             := Geral.FFT(QrFatPedNFskgLiqui.Value, 3, siNegativo);
      FmNF1b.EdImprime.Text         := Geral.FF0(QrFatPedCabModeloNF.Value);
      FmNF1b.CBImprime.KeyValue     := QrFatPedCabModeloNF.Value;
      FmNF1b.EDCFOP1.Text           := QrFatPedNFsCFOP1.Value;
      FmNF1b.CBCFOP1.KeyValue       := QrFatPedNFsCFOP1.Value;
      //
      SQL_NCMs :=
      'SELECT DISTINCT gg1.NCM, ncm.Letras,                  'sLineBreak +
      'ncm.ImpNaLista, ncm.Codigo                            'sLineBreak +
      'FROM stqmovvala smva                                  'sLineBreak +
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX    'sLineBreak +
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1     'sLineBreak +
      'LEFT JOIN ncms ncm ON ncm.NCM=gg1.NCM                 'sLineBreak +
      'WHERE smva.Tipo=1                                     'sLineBreak +
      'AND smva.OriCodi=' +
      dmkPF.FFP(QrFatPedCabCodigo.Value,0)    +sLineBreak +
      'AND smva.Empresa=' +
      dmkPF.FFP(QrFatPedCabEmpresa.Value,0)   +sLineBreak +
      'AND gg1.NCM <> ""                                     'sLineBreak +
      'ORDER BY ncm.Letras                                   ';
      FmNF1b.CarregaNCMs(QrFatPedCabModeloNF.Value, SQL_NCMs);
      //

      FmNF1b.ShowModal;
      FmNF1b.Destroy;
    end;
  end else Geral.MB_Aviso('Tipo de impress�o n�o implementado! ' +
  'Verifique se o tipo de impress�o cadastrado no modelo de impress�o � para ' +
  'Nota Fiscal. Caso for informe a DERMATEK para implementa��o!');
  *)
end;

procedure TFmFatPedNFs_0310.BtLoteClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(QrFatPedNFsLoteEnv.Value);
  //
  ReopenFatPedCab(0);
end;

procedure TFmFatPedNFs_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedNFs_0310.CkNFeUnicaClick(Sender: TObject);
begin
  BtEnviaNFe.Enabled := CkNFeUnica.Checked;
end;

procedure TFmFatPedNFs_0310.ConfiguraEtapas(Etapa: Integer);
var
  Texto: String;
begin
  PageControl1.ActivePageIndex := 0;
  //
  if Etapa = -1 then
  begin
    PageControl1.ActivePage := TSOutros;
    TSEtapas.TabVisible     := False;
    BtAnterior.Visible      := False;
    BtProximo.Visible       := False;
  end else
  begin
    TSEtapas.TabVisible     := True;
    PageControl1.ActivePage := TSEtapas;
    //
    case Etapa of
      0: //Gera NF-e
      begin
        BtAnterior.Visible := False;
        BtProximo.Visible  := True;
        //
        PCEtapas.ActivePageIndex := 0;
        //
        TBEtapas.SelEnd   := 1;
        TBEtapas.Position := 1;
      end;
      1: //Edita dados NF-e
      begin
        BtAnterior.Visible := True;
        BtProximo.Visible  := True;
        //
        PCEtapas.ActivePageIndex := 1;
        //
        TBEtapas.SelEnd   := 2;
        TBEtapas.Position := 2;
      end;
      2: //Envia lote ao fisco
      begin
        if QrFatPedCabfinNFe.Value <> 1 then
        begin
          DmNFe_0000.ReopenNFeCabB(FThisFatID, QrFatPedCabCodigo.Value,
            QrFatPedNFsEmpresa.Value);
          //
          if DmNFe_0000.QrNFeCabB.RecordCount = 0 then
          begin
            F_finNFe := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
            //
            Geral.DescricaoDeArrStrStr(Geral.FF0(QrFatPedCabfinNFe.Value),
              F_finNFe, Texto, 0, 1);
            //
            if QrFatPedCabfinNFe.Value = 2 then
            begin
              Geral.MB_Aviso('Para a finalidade "'+ Texto +'" voc� deve informar a nota referenciada!');
              //
              (* Reativar quando arrumar a Ajuda na WEB
              if Geral.MB_Pergunta('Deseja abrir a ajuda sobre notas referenciadas?') = ID_YES then
                DmkWeb.MostraWSuporte(False, 0, nil, 'Referenciada');
              *)
              //
              Exit;
            end else
            begin
              if Geral.MB_Pergunta('N�o foi definida nenhuma nota referenciada para a finalidade "'+ Texto +'"!' +
                  sLineBreak + 'Deseja continuar?') <> ID_YES then
              begin
                (* Reativar quando arrumar a Ajuda na WEB
                if Geral.MB_Pergunta('Deseja abrir a ajuda sobre notas referenciadas?') = ID_YES then
                  DmkWeb.MostraWSuporte(False, 0, nil, 'Referenciada');
                *)
                Exit;
              end;
            end;
          end;
        end;
        //
        BtAnterior.Visible := True;
        BtProximo.Visible  := False;
        //
        PCEtapas.ActivePageIndex := 2;
        //
        CkNFeUnica.Checked := False;
        BtEnviaNFe.Enabled := False;
        //
        TBEtapas.SelEnd   := 3;
        TBEtapas.Position := 3;
      end;
    end;
  end;
end;

procedure TFmFatPedNFs_0310.Continuaprocessonormaldecriao1Click(Sender: TObject);
begin
  GerarNFe(False, False, True);
end;

procedure TFmFatPedNFs_0310.dmkDBGrid1CellClick(Column: TColumn);
begin
  if Uppercase(Column.FieldName) = 'CLIENTE' then
  begin
    if (QrFatPedCab.State <> dsInactive) and (QrFatPedCab.RecordCount > 0) then
    begin
      EdCliente.ValueVariant := QrFatPedCabCliente.Value;
      CBCliente.KeyValue     := QrFatPedCabCliente.Value;
    end;  
  end;
end;

procedure TFmFatPedNFs_0310.EdPedidoChange(Sender: TObject);
begin
  ReopenFatPedCab(0);
end;

procedure TFmFatPedNFs_0310.EdClienteRedefinido(Sender: TObject);
begin
  ReopenFatPedCab(QrFatPedCabId.Value);
end;

procedure TFmFatPedNFs_0310.EdFilialChange(Sender: TObject);
begin
  ReopenFatPedCab(0);
end;

procedure TFmFatPedNFs_0310.EditaNFe;
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    if not FmNFeCabA_0000.LocCod(QrFatPedNFsIDCtrl.Value, QrNFeCabAIDCtrl.Value) then
      Geral.MB_Erro('ERRO!, N�o foi poss�vel localizar o ID ' +
        FormatFloat('0', QrFatPedNFsIDCtrl.Value) + ' localize manualmente!');
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
    //
    Screen.Cursor := crHourGlass;
    try
      GerarNFe(True, True, True);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatPedNFs_0310.EdSerieNFChange(Sender: TObject);
begin
  ReopenFatPedCab(0);
end;

procedure TFmFatPedNFs_0310.EnvioNFeAutomatico();
begin
  FNaoCriouXML := False;
  //
  if UnNFe_PF.MostraFormNFeLEnU() then
    Close;
end;

procedure TFmFatPedNFs_0310.este1Click(Sender: TObject);
begin
  EnvioNFeAutomatico();
end;

procedure TFmFatPedNFs_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedNFs_0310.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if FNaoCriouXML then
  begin
    Geral.MB_Aviso('Antes de sair desta janela voc� deve recriar o XML!');
    Abort;
  end;
end;

procedure TFmFatPedNFs_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FNaoCriouXML := False;
  //
  CBFilial.ListSource  := DsFiliais;
  CBCliente.ListSource := DsClientes;
  //
  if DmodG.QrFiliLog.RecordCount > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFiliais, Dmod.MyDB, [
      'SELECT ent.Filial, ent.Codigo, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL ',
      'FROM entidades ent ',
      'WHERE ent.Codigo<-10 ',
      'AND ent.Codigo IN (' + VAR_LIB_EMPRESAS + ') ',
      'ORDER BY NOMEFILIAL ',
      '']);
    //
    EdFilial.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBFilial.KeyValue     := DmodG.QrFiliLogFilial.Value;
  end;
  DmPediVda.ReopenClientes(QrClientes);
  //
  //ReopenFatPedNFs(0);
  //
  ConfiguraEtapas(-1);
end;

procedure TFmFatPedNFs_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedNFs_0310.GeraNF1Click(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmFatPedNFs_0310.MostraEdicao(Acao: TSQLType);
begin
  if DBCheck.CriaFm(TFmNFaEdit_0310, FmNFaEdit_0310, afmoSoBoss) then
  begin
    if (QrParamsEmpCRT.Value = 3) and
    (QrParamsEmpNFeNT2013_003LTT.Value < 2) then
      FmNFaEdit_0310.PageControl1.ActivePageIndex := 0
    else
      FmNFaEdit_0310.PageControl1.ActivePageIndex := 1;
    case Acao of
      stIns:
      begin
        FmNFaEdit_0310.FIDCtrl := 0;// Inclus�o
        QrTotal.Close;
        QrTotal.Params[00].AsInteger := QrFatPedCabCodigo.Value;
        UMyMod.AbreQuery(QrTotal, Dmod.MyDB, 'TFmFatPedNFs_0310.MostraEdicao()');
        //
        if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFe(
        (*QrFatPedCabSerieDesfe.Value*)-1,
        (*QrFatPedCabNFDesfeita.Value*) 0,
        FmNFaEdit_0310.QrImprimeSerieNF_Normal.Value,
        FmNFaEdit_0310.QrImprimeCtrl_nfs.Value,
        Trunc(QrFatPedCabEmpresa.Value),
        FmNFaEdit_0310.QrFatPedNFs.FieldByName('Filial').AsInteger,
        FmNFaEdit_0310.QrImprimeMaxSeqLib.Value,
        FmNFaEdit_0310.EdSerieNF, FmNFaEdit_0310.EdNumeroNF(*),
        SerieNFTxt, NumeroNF*)) then
        begin
          // ???
        end;
        FmNFaEdit_0310.EdQuantidade.ValueVariant := Geral.FFT(QrTotalQtde.Value, 2, siNegativo);
      end;
      stUpd:
      begin
        FmNFaEdit_0310.FIDCtrl := QrFatPedNFsIDCtrl.Value;
        DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
        //
        FmNFaEdit_0310.EdNumeroNF  .ValueVariant := QrFatPedNFsNumeroNF  .Value;
        //
        FmNFaEdit_0310.EdkgBruto   .ValueVariant := QrFatPedNFskgBruto   .Value;
        FmNFaEdit_0310.EdkgLiqui   .ValueVariant := QrFatPedNFskgLiqui   .Value;
        FmNFaEdit_0310.EdPlacaUF   .ValueVariant := QrFatPedNFsPlacaUF   .Value;
        FmNFaEdit_0310.EdPlacaNr   .ValueVariant := QrFatPedNFsPlacaNr   .Value;
        FmNFaEdit_0310.EdEspecie   .ValueVariant := QrFatPedNFsEspecie   .Value;
        FmNFaEdit_0310.EdMarca     .ValueVariant := QrFatPedNFsMarca     .Value;
        FmNFaEdit_0310.EdNumero    .ValueVariant := QrFatPedNFsNumero    .Value;
        FmNFaEdit_0310.EdQuantidade.ValueVariant := QrFatPedNFsQuantidade.Value;
        FmNFaEdit_0310.EdObservacao.ValueVariant := QrFatPedNFsObservacao.Value;
        FmNFaEdit_0310.TPDtEmissNF .Date         := QrFatPedNFsDtEmissNF .Value;
        FmNFaEdit_0310.TPDtEntraSai.Date         := QrFatPedNFsDtEntraSai.Value;
        FmNFaEdit_0310.MeinfAdic_infCpl.Text     := QrFatPedNFsinfAdic_infCpl.Value;
        FmNFaEdit_0310.EdRNTC      .Text         := QrFatPedNFsRNTC.Value;
        FmNFaEdit_0310.EdUFEmbarq  .Text         := QrFatPedNFsUFEmbarq.Value;
        FmNFaEdit_0310.EdxLocEmbarq.Text         := QrFatPedNFsxLocEmbarq.Value;
        // NF-e 2.00
        FmNFaEdit_0310.RGCRT.ItemIndex           := QrFatPedNFsEmit_CRT.Value;
        FmNFaEdit_0310.EdHrEntraSai.ValueVariant := QrFatPedNFsHrEntraSai.Value;
        FmNFaEdit_0310.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(Trunc(QrFatPedCabEmpresa.Value), Trunc(QrFatPedCabCliente.Value));
        FmNFaEdit_0310.EdVagao.Text              := QrFatPedNFsvagao.Value;
        FmNFaEdit_0310.EdBalsa.Text              := QrFatPedNFsbalsa.Value;
        // fim NF-e 2.00
        FmNFaEdit_0310.EdCompra_XNEmp.Text       := QrFatPedNFsCompra_XNEmp.Value;
        FmNFaEdit_0310.EdCompra_XPed.Text        := QrFatPedNFsCompra_XPed.Value;
        FmNFaEdit_0310.EdCompra_XCont.Text       := QrFatPedNFsCompra_XCont.Value;
      end;
    end;
    FmNFaEdit_0310.EdNumeroNF.Enabled := (QrImprimeIncSeqAuto.Value = 0);
    //
    //
    FmNFaEdit_0310.ReopenStqMovValX(0);
    FmNFaEdit_0310.ImgTipo.SQLType := Acao;
    FmNFaEdit_0310.ShowModal;
    FmNFaEdit_0310.Destroy;
    //
    ReopenFatPedNFs(Trunc(QrFatPedCabEmpresa.Value));
    //
    Geral.MB_Info('Caso ap�s recriar o XML voc� tenha editado algum dado na NF-e' +
      sLineBreak + 'clique no bot�o "NF-e => Apenas Gera XML"');
  end;
end;

procedure TFmFatPedNFs_0310.Passo2EditarNFesenecessriodadosespecficos1Click(
  Sender: TObject);
begin
  EditaNFe();
end;

procedure TFmFatPedNFs_0310.Passo4Mostrarjaneladelotes1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmFatPedNFs_0310.PCEtapasChange(Sender: TObject);
begin
  ConfiguraEtapas(PCEtapas.ActivePageIndex);
end;

procedure TFmFatPedNFs_0310.PMGeraNFPopup(Sender: TObject);
var
  Habil, Habil2: Boolean;
begin
  Habil  := (QrImprime.State <> dsInactive) and (QrImprime.RecordCount > 0);
  Habil2 := DmNFe_0000.PodeAlterarNFe(QrNFeCabAStatus.Value);
  //
  GeraNF1.Enabled         := (QrFatPedNFsNumeroNF.Value = 0) and Habil;
  AlteradadosNF1.Enabled  := (QrFatPedNFsNumeroNF.Value > 0) and Habil and Habil2;
  AlteradadosNFe1.Enabled := (QrFatPedNFsNumeroNF.Value > 0) and Habil and Habil2;
end;

procedure TFmFatPedNFs_0310.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeCabAStatus.Value < DmNFe_0000.stepNFeAdedLote()) or
                DmNFe_0000.NotaRejeitada(QrNFeCabAinfProt_cStat.Value);
  //
  RecriatodaNFe1.Enabled := Habilita;
  este1.Enabled          := Habilita;
  ApenasGeraXML1.Enabled := (Habilita) and (FNaoCriouXML = False);
  // Eliminar?
  Continuaprocessonormaldecriao1.Enabled := False;//Habilita;
end;

procedure TFmFatPedNFs_0310.QrFatPedCabAfterScroll(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  ReopenFatPedNFs(Trunc(QrFatPedCabEmpresa.Value));
  //
  Enab := (QrNFeCabAStatus.Value < DmNFe_0000.stepNFeAdedLote()) or
            DmNFe_0000.NotaRejeitada(QrNFeCabAinfProt_cStat.Value);
  //
  if Enab = False then
    ConfiguraEtapas(-1)
  else
    ConfiguraEtapas(0);
  //
  Panel1.Update;
  Application.ProcessMessages;
end;

procedure TFmFatPedNFs_0310.QrFatPedCabBeforeClose(DataSet: TDataSet);
begin
  QrFatPedNFs.Close;
  //
  ConfiguraEtapas(-1);
end;

procedure TFmFatPedNFs_0310.QrFatPedCabCalcFields(DataSet: TDataSet);
begin
  QrFatPedCabDTAEMISS_TXT.Value := dmkPF.FDT_NULO(QrFatPedCabDtaEmiss.Value, 3);
  QrFatPedCabDTAENTRA_TXT.Value := dmkPF.FDT_NULO(QrFatPedCabDtaEntra.Value, 3);
  QrFatPedCabDTAINCLU_TXT.Value := dmkPF.FDT_NULO(QrFatPedCabDtaInclu.Value, 3);
  QrFatPedCabDTAPREVI_TXT.Value := dmkPF.FDT_NULO(QrFatPedCabDtaPrevi.Value, 3);
end;

procedure TFmFatPedNFs_0310.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
  ReopenQrImprime();
  ReopenNFeCabA();
  {
  //N�o usa na NFF-e
  BtImprime.Enabled := (QrFatPedNFsNumeroNF.Value > 0) and
    (QrFatPedCabEncerrou.Value > 0);
  }
  BtImprime.Visible := False;
end;

procedure TFmFatPedNFs_0310.QrFatPedNFsBeforeClose(DataSet: TDataSet);
begin
  QrImprime.Close;
  QrNFeCabA.Close;
end;

procedure TFmFatPedNFs_0310.QrFatPedNFsCalcFields(DataSet: TDataSet);
begin
  QrFatPedNFsDataAlt_TXT.Value := Geral.FDT(QrFatPedNFsDataAlt.Value, 2);
  QrFatPedNFsDTEMISSNF_TXT.Value := dmkPF.FDT_NULO(QrFatPedNFsDtEmissNF.Value, 3);
  QrFatPedNFsDTENTRASAI_TXT.Value := dmkPF.FDT_NULO(QrFatPedNFsDtEntraSai.Value, 3);
end;

procedure TFmFatPedNFs_0310.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  if QrNFeCabAinfCanc_cStat.Value = 101 then
  begin
    QrNFeCabAcStat.Value := QrNFeCabAinfCanc_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfCanc_xMotivo.Value;
  end else begin
    QrNFeCabAcStat.Value := QrNFeCabAinfProt_cStat.Value;
    QrNFeCabAxMotivo.Value := QrNFeCabAinfProt_xMotivo.Value;
  end;
    QrNFeCabAcStat_xMotivo.Value := Geral.FF0(QrNFeCabAcStat.Value) + ': ' +
      QrNFeCabAxMotivo.Value;
end;

procedure TFmFatPedNFs_0310.RecriatodaNFe1Click(Sender: TObject);
begin
  FNaoCriouXML := False;
  //
  GerarNFe(True, False, True);
end;

procedure TFmFatPedNFs_0310.ReopenFatPedCab(Id: Integer);
var
  Empresa: Integer;
begin
  QrFatPedCab.Close;
  if EdFilial.ValueVariant = 0 then
    Exit;
  //
  Empresa := QrFiliaisCodigo.Value;
(*
  QrFatPedCab.SQL.Clear;
  QrFatPedCab.SQL.Add('SELECT ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou,');
  QrFatPedCab.SQL.Add('IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP,');
  QrFatPedCab.SQL.Add('IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI,');
  QrFatPedCab.SQL.Add('ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli,');
  QrFatPedCab.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI,');
  // 2014-10-14
  //QrFatPedCab.SQL.Add('frc.ModeloNF, frc.infAdFisco, frc.Financeiro, ');
  QrFatPedCab.SQL.Add('frc.ModeloNF + 0.000 ModeloNF, frc.infAdFisco, frc.Financeiro, ');
  //QrFatPedCab.SQL.Add('IF(frc.TipoMov=1,0,1) tpNF, pvd.*');
  QrFatPedCab.SQL.Add('SUM(val.Total) TotalFatur, frc.TipoMov tpNF, ');
  QrFatPedCab.SQL.Add('pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, ');
  // 2014-10-14
  //QrFatPedCab.SQL.Add('pvd.RegrFiscal, pvd.CartEmis, pvd.TabelaPrc, pvd.CondicaoPG, ');
  QrFatPedCab.SQL.Add('pvd.RegrFiscal + 0.000 RegrFiscal, pvd.CartEmis + 0.000 ');
  QrFatPedCab.SQL.Add('CartEmis , pvd.TabelaPrc + 0.000 TabelaPrc, ');
  QrFatPedCab.SQL.Add('pvd.CondicaoPG + 0.000 CondicaoPG, pvd.FretePor, ');
  QrFatPedCab.SQL.Add('pvd.Transporta + 0.000 Transporta, pvd.Empresa + 0.000 ');
  // 2014-10-14 idDest
  QrFatPedCab.SQL.Add('Empresa, pvd.Cliente + 0.000 Cliente, pvd.idDest, pvd.indFinal, pvd.indPres');
  //
  QrFatPedCab.SQL.Add('FROM stqmovvala val ');
  QrFatPedCab.SQL.Add('LEFT JOIN fatpedcab fpc ON fpc.Codigo = val.OriCodi ');
  QrFatPedCab.SQL.Add('LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido');
  QrFatPedCab.SQL.Add('');
  QrFatPedCab.SQL.Add('LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal');
  QrFatPedCab.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa');
  QrFatPedCab.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente');
  QrFatPedCab.SQL.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg');
  QrFatPedCab.SQL.Add('LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF)');
  QrFatPedCab.SQL.Add('LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF)');
  QrFatPedCab.SQL.Add('WHERE fpc.Encerrou > 0');
  QrFatPedCab.SQL.Add('');
  QrFatPedCab.SQL.Add('AND pvd.Empresa=' + FormatFloat('0', Empresa));
  //
  if EdPedido.ValueVariant <> 0 then
    QrFatPedCab.SQL.Add('AND pvd.CodUsu=' + FormatFloat('0', EdPedido.ValueVariant));
  if EdCliente.ValueVariant <> 0 then
    QrFatPedCab.SQL.Add('AND pvd.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
  QrFatPedCab.SQL.Add('GROUP BY val.OriCodi');
  QrFatPedCab.SQL.Add('ORDER BY Encerrou DESC');
  //
  UMyMod.AbreQuery(QrFatPedCab, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenFatPedCab()');
  //
*)

  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedCab, Dmod.MyDB, [
    'SELECT val.Id, ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou, ',
    'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP, ',
    'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI, ',
    'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli, ',
    'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI, ',
    'frc.ModeloNF + 0.000 ModeloNF, frc.infAdFisco, frc.Financeiro,  ',
    'SUM(val.Total) TotalFatur, frc.TipoMov tpNF,  frc.TpCalcTrib, ',
    'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,  ',
    'pvd.RegrFiscal + 0.000 RegrFiscal, pvd.CartEmis + 0.000  ',
    'CartEmis , pvd.TabelaPrc + 0.000 TabelaPrc,  ',
    'pvd.CondicaoPG + 0.000 CondicaoPG, pvd.FretePor,  ',
    'pvd.Transporta + 0.000 Transporta, pvd.Empresa + 0.000  ',
    'Empresa, pvd.Cliente + 0.000 Cliente, pvd.PedidoCli, ',
    // NFe 3.10
    'pvd.idDest, pvd.indFinal, pvd.indPres, pvd.finNFe, pvd.indSinc ',
    //
    'FROM stqmovvala val  ',
    'LEFT JOIN fatpedcab fpc ON fpc.Codigo = val.OriCodi  ',
    'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF) ',
    'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF) ',
    'WHERE fpc.Encerrou > 0 ',
    'AND pvd.Empresa=' + FormatFloat('0', Empresa),
    Geral.ATS_If(EdPedido.ValueVariant <> 0,
      ['AND pvd.CodUsu=' + FormatFloat('0', EdPedido.ValueVariant)]),
    Geral.ATS_If(EdCliente.ValueVariant <> 0,
      ['AND pvd.Cliente=' + FormatFloat('0', EdCliente.ValueVariant)]),
    //
    'GROUP BY val.OriCodi ',
    'ORDER BY Encerrou DESC ',
    '']);
  if Id <> 0 then
    QrFatPedCab.Locate('Id', Id, []);
(*
////////////////////////////////////////////////////////////////////////////////
Muito erro de LargeInt x Integer!!
////////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedCab1, Dmod.MyDB, [
  'SELECT ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou, ',
  'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP, ',
  'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI, ',
  'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli, ',
  'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI, ',
  'frc.ModeloNF, frc.infAdFisco, frc.Financeiro, ',
  'SUM(val.Total) TotalFatur, frc.TipoMov tpNF, ',
  'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, ',
  'pvd.RegrFiscal, pvd.CartEmis, pvd.TabelaPrc, ',
  'pvd.CondicaoPG, pvd.FretePor, ',
  'pvd.Transporta, pvd.Empresa, pvd.Cliente, ',
  'pvd.idDest, pvd.indFinal, pvd.indPres ',
  'FROM stqmovvala val ',
  'LEFT JOIN fatpedcab fpc ON fpc.Codigo = val.OriCodi ',
  'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido ',
  ' ',
  'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
  'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa ',
  'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF) ',
  'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF) ',
  'WHERE fpc.Encerrou > 0 ',
  'AND pvd.Empresa=' + FormatFloat('0', Empresa),
  Geral.ATS_If(EdPedido.ValueVariant <> 0,
    ['AND pvd.CodUsu=' + FormatFloat('0', EdPedido.ValueVariant)]),
  Geral.ATS_If(EdCliente.ValueVariant <> 0,
    ['AND pvd.Cliente=' + FormatFloat('0', EdCliente.ValueVariant)]),
  //
  'GROUP BY val.OriCodi ',
  'ORDER BY Encerrou DESC ',
  '']);
*)
end;

function TFmFatPedNFs_0310.ReopenFatPedNFs(Filial: Integer): Boolean;
begin
(*
  QrFatPedNFs.Close;
  QrFatPedNFs.Params[00].AsInteger := FThisFatID;
  QrFatPedNFs.Params[01].AsInteger := QrFatPedCabCodigo.Value;
  QrFatPedNFs.Params[02].AsInteger := Trunc(QrFatPedCabEmpresa.Value);
  UMyMod.AbreQuery(QrFatPedNFs, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenFatPedNFs()');
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedNFs, Dmod.MyDB, [
  'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, ',
  'nfe.infCanc_cStat, nfe.LoteEnv, ',
  'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.* ',
  'FROM stqmovnfsa smna ',
  'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa ',
  'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl ',
  'WHERE smna.Tipo=' + Geral.FF0(FThisFatID),
  'AND smna.OriCodi=' + Geral.FF0(QrFatPedCabCodigo.Value),
  'AND smna.Empresa=' + Geral.FF0(Trunc(QrFatPedCabEmpresa.Value)),
  'ORDER BY ent.Filial ',
  '']);
  //
  Result := QrFatPedNFs.Locate('Filial', Filial, []);
end;

procedure TFmFatPedNFs_0310.ReopenNFeCabA();
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[00].AsInteger := QrFatPedNFsCabA_FatID.Value;  //Mudei 2010-09-19 //FThisFatID;
  QrNFeCabA.Params[01].AsInteger := QrFatPedCabCodigo.Value;
  QrNFeCabA.Params[02].AsInteger := QrFatPedNFsEmpresa.Value;
  QrNFeCabA.Params[03].AsInteger := QrFatPedNFsNumeroNF.Value;
  UMyMod.AbreQuery(QrNFeCabA, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenNFeCabA()');
end;

{
procedure TFmFatPedNFs_0310.ReopenParamsNFs(Controle: Integer);
begin
  QrParamsNFs.Close;
  QrParamsNFs.Params[0].AsInteger := QrFiliaisCodigo.Value;
  QrParamsNFs.Open;
  QrParamsNFs.Locate('Controle', Controle, []);
  //
  EdSerieNF.ValueVariant := Null;
  CBSerieNF.KeyValue     := Null;
end;
}

procedure TFmFatPedNFs_0310.ReopenQrImprime();
begin
  // 2011-08-25
  //  Mudei para c� por causa do tpEmis
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Trunc(QrFatPedCabEmpresa.Value);
  UMyMod.AbreQuery(QrParamsEmp, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenQrImprime()');
  // Fim 2011-08-25
  QrImprime.Close;
  if QrFatPedNFsCO_ENT_EMP.Value = QrFatPedCabEmpresa.Value then
  begin
    QrImprime.Params[00].AsInteger := Trunc(QrFatPedCabEmpresa.Value);
    QrImprime.Params[01].AsInteger := Trunc(QrFatPedCabModeloNF.Value);
    UMyMod.AbreQuery(QrImprime, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenQrImprime()');
  end else begin
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UMyMod.AbreQuery(QrImprime, Dmod.MyDB, 'TFmFatPedNFs_0310.ReopenQrImprime()');
    end;
  end;
end;

procedure TFmFatPedNFs_0310.SbNFePesqClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFePesq(False, nil, nil, EdCliente.ValueVariant);
end;

procedure TFmFatPedNFs_0310.SpeedButton1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
end;

{
procedure TFmImprime.ReopenParamsNFs(Controle: Integer);
begin
end;
}

end.

