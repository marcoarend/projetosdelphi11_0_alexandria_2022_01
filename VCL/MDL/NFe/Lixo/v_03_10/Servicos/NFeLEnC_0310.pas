unit NFeLEnC_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums, UnGrl_consts;

type
  TFmNFeLEnC_0310 = class(TForm)
    PnDados: TPanel;
    DsNFeLEnC: TDataSource;
    QrNFeLEnC: TmySQLQuery;
    PnEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    QrNFeLEnCDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMNFe: TPopupMenu;
    IncluiNFeaoloteatual1: TMenuItem;
    DsNFeLEnI: TDataSource;
    QrNFeLEnI: TmySQLQuery;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAid: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    DsNFeCabA: TDataSource;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrNFeLEnCCodigo: TIntegerField;
    QrNFeLEnCCodUsu: TIntegerField;
    QrNFeLEnCNome: TWideStringField;
    QrNFeLEnCEmpresa: TIntegerField;
    QrNFeLEnCversao: TFloatField;
    QrNFeLEnCtpAmb: TSmallintField;
    QrNFeLEnCverAplic: TWideStringField;
    QrNFeLEnCcStat: TIntegerField;
    QrNFeLEnCxMotivo: TWideStringField;
    QrNFeLEnCcUF: TSmallintField;
    QrNFeLEnCnRec: TWideStringField;
    QrNFeLEnCdhRecbto: TDateTimeField;
    QrNFeLEnCtMed: TIntegerField;
    QrNFeLEnCFilial: TIntegerField;
    QrNFeLEnCNO_Empresa: TWideStringField;
    QrNFeLEnCNO_Ambiente: TWideStringField;
    Verificalotenofisco1: TMenuItem;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    Enviodelote1: TMenuItem;
    Consultadelote1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrNFeLEnM: TmySQLQuery;
    DsNFeLEnM: TDataSource;
    DBGrid2: TDBGrid;
    QrNFeLEnMControle: TIntegerField;
    QrNFeLEnMversao: TFloatField;
    QrNFeLEnMtpAmb: TSmallintField;
    QrNFeLEnMverAplic: TWideStringField;
    QrNFeLEnMcStat: TIntegerField;
    QrNFeLEnMxMotivo: TWideStringField;
    QrNFeLEnMcUF: TSmallintField;
    QrNFeLEnMnRec: TWideStringField;
    QrNFeLEnMdhRecbto: TDateTimeField;
    QrNFeLEnMtMed: TIntegerField;
    RemoveNFesselecionadas1: TMenuItem;
    N3: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    WB_XML: TWebBrowser;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNFEnonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtNFe: TBitBtn;
    BtXML: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RetornodoLotedeenvio1: TMenuItem;
    RGIndSinc: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrNFeLEnCIndSinc: TSmallintField;
    N4: TMenuItem;
    ValidarXMLdaNFE1: TMenuItem;
    Assinada2: TMenuItem;
    Lotedeenvio1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeLEnCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeLEnCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure IncluiNFeaoloteatual1Click(Sender: TObject);
    procedure BtNFeClick(Sender: TObject);
    procedure QrNFeLEnCAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Enviodelote1Click(Sender: TObject);
    procedure Consultadelote1Click(Sender: TObject);
    procedure RemoveNFesselecionadas1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure RetornodoLotedeenvio1Click(Sender: TObject);
    procedure PMXMLPopup(Sender: TObject);
    procedure Assinada2Click(Sender: TObject);
    procedure Lotedeenvio1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure EnviaLoteAoFisco(SohLer: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeLEnI();
    procedure ReopenNFeLEnM();
  end;

var
  FmNFeLEnC_0310: TFmNFeLEnC_0310;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, NFeLEnI_0310, ModuleNFe_0000,
  NFeSteps_0310, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeLEnC_0310.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeLEnC_0310.Lote1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrNFeLEnCCodigo.Value);
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmNFeLEnC_0310.Lotedeenvio1Click(Sender: TObject);
var
  LoteStr, Arquivo: String;
begin
  LoteStr := FormatFloat('000000000', QrNFeLEnCCodigo.Value);
  Arquivo := DmNFe_0000.Obtem_Arquivo_XML_(QrNFeLEnCEmpresa.Value,
               NFE_EXT_ENV_LOT_XML, LoteStr, True);
  //
  UnNFe_PF.ValidaXML_NFe(Arquivo);
end;

procedure TFmNFeLEnC_0310.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeLEncCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeLEnC_0310.Verificalotenofisco1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFeSteps_0310, FmNFeSteps_0310, afmoNegarComAviso) then
  begin
    FmNFeSteps_0310.PnLoteEnv.Visible := True;
    FmNFeSteps_0310.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
    FmNFeSteps_0310.Show;
    //
    FmNFeSteps_0310.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado);  // 2 - Verifica lote no fisco
    FmNFeSteps_0310.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
      QrNFeLEnCnRec.Value);
    FmNFeSteps_0310.FFormChamou      := 'FmNFeLEnc_0310';
    //
    //FmNFeSteps_0310.Destroy;
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeLEnC_0310.DefParams;
begin
  VAR_GOTOTABELA := 'NFeLEnc';
  VAR_GOTOMYSQLTABLE := QrNFeLEnc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfelenc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  VAR_SQLx.Add('AND lot.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Empresa IN (' + VAR_LIB_EMPRESAS + ')';
end;

procedure TFmNFeLEnC_0310.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeLEnC_0310.EnviaLoteAoFisco(SohLer: Boolean);
var
  IndSinc: TXXeIndSinc;
  Lote, Empresa: Integer;
begin
  IndSinc := TXXeIndSinc(QrNFeLEnCindSinc.Value);
  Lote    := QrNFeLEnCCodigo.Value;
  Empresa := QrNFeLEnCEmpresa.Value;
  UnNFe_PF.EnviaLoteAoFisco(IndSinc, Lote, Empresa, SohLer, CO_MODELO_NFE_55);
end;

procedure TFmNFeLEnC_0310.Envialoteaofisco1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(False);
end;

procedure TFmNFeLEnC_0310.Enviodelote1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(True);
end;

procedure TFmNFeLEnC_0310.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeLEnC_0310.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB, HabilitaC: Boolean;
begin
  HabilitaA := (QrNFeLEnc.State = dsBrowse) and (QrNFeLEnc.RecordCount > 0);
  HabilitaC := (QrNFeCabA.State = dsBrowse) and (QrNFeCabA.RecordCount > 0);
  Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  Enviodelote1.Enabled     := HabilitaA;
  Consultadelote1.Enabled  := HabilitaA;//? and (QrNFeLEnc.);
  //
  HabilitaB := HabilitaA and (not (QrNFeLEnCcStat.Value in ([103,104,105]))) and HabilitaC;
  Envialoteaofisco1.Enabled := HabilitaB;
  HabilitaB := HabilitaA and (QrNFeLEnCcStat.Value > 102);
  Verificalotenofisco1.Enabled := HabilitaB;

  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmNFeLEnC_0310.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeLEnc.State <> dsInactive) and (QrNFeLEnc.RecordCount > 0) and (QrNFeLEnCcStat.Value <> 103);
  //
  Alteraloteatual1.Enabled        := Habilita;
  RemoveNFesselecionadas1.Enabled := Habilita;
end;

procedure TFmNFeLEnC_0310.PMXMLPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrNFeLEnC.State <> dsInactive) and (QrNFeLEnC.RecordCount > 0);
  Enab2 := (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
  //
  AbrirXMLnonavegadordestajanela1.Enabled := Enab and Enab2;
  AbrirXMLdaNFEnonavegadorpadro1.Enabled  := Enab and Enab2;
  ValidarXMLdaNFE1.Enabled                := Enab and Enab2;
end;

procedure TFmNFeLEnC_0310.Consultadelote1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFeSteps_0310, FmNFeSteps_0310, afmoNegarComAviso) then
  begin
    FmNFeSteps_0310.PnLoteEnv.Visible := True;
    FmNFeSteps_0310.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
    FmNFeSteps_0310.Show;
    //
    FmNFeSteps_0310.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado);  // 2 - verifica lote no fisco
    FmNFeSteps_0310.CkSoLer.Checked  := True;
    FmNFeSteps_0310.PreparaConsultaLote(QrNFeLEnCCodigo.Value,
      QrNFeLEnCEmpresa.Value, QrNFeLEnCnRec.Value);
    FmNFeSteps_0310.FFormChamou      := 'FmNFeLEnc_0310';
    //
    //
    //FmNFeSteps_0310.Destroy;
    //
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

procedure TFmNFeLEnC_0310.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNFeLEnC.AlteraRegistro;
var
  NFeLEnc : Integer;
begin
  NFeLEnc := QrNFeLEncCodigo.Value;
  if QrNFeLEncCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NFeLEnc, Dmod.MyDB, 'NFeLEnc', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFeLEnc, Dmod.MyDB, 'NFeLEnc', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFeLEnC.IncluiRegistro;
var
  Cursor : TCursor;
  NFeLEnc : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NFeLEnc := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'NFeLEnc', 'NFeLEnc', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NFeLEnc))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, NFeLEnc);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNFeLEnC_0310.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeLEnC_0310.RemoveNFesselecionadas1Click(Sender: TObject);
var
  Status: Integer;
begin
  QrNFeLEnM.First;
  if (QrNFeCabAStatus.Value < 100) or (not (QrNFeLEnCcStat.Value in ([103,104,105])))
  or ((QrNFeLEnMcStat.Value in ([103,104,105])) and (QrNFeCabAStatus.Value > 200)) then
  begin
    if Geral.MB_Pergunta('Deseja realmente tirar a NF-e deste lote?') = mrYes then
    begin
      if (QrNFeCabAStatus.Value = 50)
      or ((QrNFeCabAStatus.Value = 103) and (QrNFeLEnMcStat.Value > 200)) then
        Status := 30
      else
        Status := QrNFeCabAStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET LoteEnv = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 AND Empresa=:P3');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrNFeCabAFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrNFeCabAFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrNFeCabAEmpresa.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenNFeLEnI();
    end;
  end else
    Geral.MB_Aviso('A��o n�o permitida!');

end;

procedure TFmNFeLEnC_0310.ReopenNFeLEnI;
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[0].AsInteger := QrNFeLEnCCodigo.Value;
  QrNFeCabA.Open;
end;

procedure TFmNFeLEnC_0310.ReopenNFeLEnM;
begin
  QrNFeLEnM.Close;
  QrNFeLEnM.Params[0].AsInteger := QrNFeLEnCCodigo.Value;
  QrNFeLEnM.Open;
end;

procedure TFmNFeLEnC_0310.RetornodoLotedeenvio1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrNFeLEnCCodigo.Value);
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_REC_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmNFeLEnC_0310.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeLEnC_0310.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeLEnC_0310.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeLEnC_0310.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeLEnC_0310.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeLEnC_0310.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrNFeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'nfelenc');
end;

procedure TFmNFeLEnC_0310.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeLEncCodigo.Value;
  Close;
end;

procedure TFmNFeLEnC_0310.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmNFeLEnC_0310.BtNFeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeLEnC_0310.Assina1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, True);
end;

procedure TFmNFeLEnC_0310.Assinada1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, True, WB_XML);
end;

procedure TFmNFeLEnC_0310.Assinada2Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := DmNFe_0000.Obtem_Arquivo_XML_(QrNFeLEnCEmpresa.Value,
                NFE_EXT_NFE_XML, QrNFeCabAid.Value, True);
  //
  UnNFe_PF.ValidaXML_NFe(Arquivo);
end;

procedure TFmNFeLEnC_0310.BtConfirmaClick(Sender: TObject);
var
  Nome(*, verAplic, xMotivo, nRec, dhRecbto, cMsg, xMsg*): String;
  Codigo, CodUsu, Empresa, (*tpAmb, cStat, cUF, tMed,*) IndSinc: Integer;
  //versao, dhRecbtoTZD: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)    DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
(*  versao         := ;
  tpAmb          := ;
  verAplic       := ;
  cStat          := ;
  xMotivo        := ;
  cUF            := ;
  nRec           := ;
  dhRecbto       := ;
  tMed           := ;
  cMsg           := ;
  xMsg           := ;
  dhRecbtoTZD    := ;*)
  IndSinc        := RGIndSinc.ItemIndex;

  //
  if ImgTipo.SQLType = stUpd then
    Codigo := EdCodigo.ValueVariant
  else
    Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfelenc', False, [
  'CodUsu', 'Nome', 'Empresa',
  (*'versao', 'tpAmb', 'verAplic',
  'cStat', 'xMotivo', 'cUF',
  'nRec', 'dhRecbto', 'tMed',
  'cMsg', 'xMsg', 'dhRecbtoTZD',*)
  'IndSinc'], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  (*versao, tpAmb, verAplic,
  cStat, xMotivo, cUF,
  nRec, dhRecbto, tMed,
  cMsg, xMsg, dhRecbtoTZD,*)
  IndSinc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
{
var
  Codigo: Integer;
  //Nome: String;
begin
  if ImgTipo.SQLType = stUpd then
    Codigo := EdCodigo.ValueVariant
  else
    Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  //
  EdCodigo.ValueVariant := Codigo;
  EdCodUsu.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNFeLEnc_0310, PainelEdit,
    'NFeLEnc', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
  end;
}
end;

procedure TFmNFeLEnC_0310.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeLEnc', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeLEnC_0310.BtLoteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeLEnC_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmNFeLEnC_0310.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeLEncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0310.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeLEnC_0310.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0310.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeLEncCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0310.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeLEnC_0310.QrNFeLEnCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeLEnC_0310.QrNFeLEnCAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeLEnI();
  ReopenNFeLEnM();
end;

procedure TFmNFeLEnC_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLEnC_0310.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeLEncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeLEnc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeLEnC_0310.SelecionaroXML1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\NFE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
end;

procedure TFmNFeLEnC_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeLEnC_0310.Gerada1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, False);
end;

procedure TFmNFeLEnC_0310.Gerada2Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, False, WB_XML);
end;

procedure TFmNFeLEnC_0310.IncluiNFeaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLEnI_0310, FmNFeLEnI_0310, afmoNegarComAviso) then
  begin
    FmNFeLEnI_0310.QrNFeCabA.Close;
    FmNFeLEnI_0310.QrNFeCabA.Params[00].AsInteger := QrNFeLEncEmpresa.Value;
    FmNFeLEnI_0310.QrNFeCabA.Params[01].AsInteger := DmNFe_0000.stepNFeAssinada();
    FmNFeLEnI_0310.QrNFeCabA.Open;
    //
    FmNFeLEnI_0310.ShowModal;
    FmNFeLEnI_0310.Destroy;
    //
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

procedure TFmNFeLEnC_0310.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'nfelenc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFeLEnC_0310.QrNFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  QrNFeLEncCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeLEnC_0310.QrNFeLEnCCalcFields(DataSet: TDataSet);
begin
  QrNFeLEncDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeLEnCdhRecbto.Value, 0);
  case QrNFeLEnCtpAmb.Value of
    1: QrNFeLEnCNO_Ambiente.Value := 'Produ��o';
    2: QrNFeLEnCNO_Ambiente.Value := 'Homologa��o';
    else QrNFeLEnCNO_Ambiente.Value := '';
  end;
end;

end.

