object FmNFeLEnU_0310: TFmNFeLEnU_0310
  Left = 339
  Top = 185
  Caption = 'NFe-LOTES-003 :: Envio de NF-e '#218'nica'
  ClientHeight = 427
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 582
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 534
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 250
        Height = 32
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 250
        Height = 32
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 250
        Height = 32
        Caption = 'Envio de NF-e '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 357
    Width = 630
    Height = 70
    Align = alBottom
    TabOrder = 1
    Visible = False
    object PnSaiDesis: TPanel
      Left = 484
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 630
    Height = 265
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 630
      Height = 265
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 630
        Height = 265
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 164
          Width = 72
          Height = 13
          Caption = 'Status do Lote:'
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 8
          Top = 204
          Width = 74
          Height = 13
          Caption = 'Status da NF-e:'
          FocusControl = DBEdit3
        end
        object CheckBox1: TCheckBox
          Left = 12
          Top = 16
          Width = 400
          Height = 17
          Caption = 'Gerar XML da NF-e'
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 12
          Top = 36
          Width = 400
          Height = 17
          Caption = 'Criar novo lote de envio.'
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 12
          Top = 56
          Width = 400
          Height = 17
          Caption = 'Adicionar NF-e ao lote.'
          TabOrder = 2
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 180
          Width = 32
          Height = 21
          DataField = 'cStat'
          DataSource = DsNFeLEnC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnChange = DBEdit1Change
        end
        object DBEdit2: TDBEdit
          Left = 40
          Top = 180
          Width = 577
          Height = 21
          DataField = 'xMotivo'
          DataSource = DsNFeLEnC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object CheckBox4: TCheckBox
          Left = 12
          Top = 76
          Width = 400
          Height = 17
          Caption = 'Enviar lote ao Fisco.'
          TabOrder = 5
        end
        object CheckBox5: TCheckBox
          Left = 12
          Top = 96
          Width = 400
          Height = 17
          Caption = 'Consultar lote no fisco.'
          TabOrder = 6
        end
        object CheckBox6: TCheckBox
          Left = 12
          Top = 116
          Width = 400
          Height = 17
          Caption = 'Preview do DANFE.'
          TabOrder = 7
        end
        object DBEdit3: TDBEdit
          Left = 8
          Top = 220
          Width = 32
          Height = 21
          DataField = 'Status'
          DataSource = DsNFeCabA
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnChange = DBEdit3Change
        end
        object DBEdit4: TDBEdit
          Left = 40
          Top = 220
          Width = 577
          Height = 21
          DataField = 'Motivo'
          DataSource = DsNFeCabA
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
        end
        object CheckBox7: TCheckBox
          Left = 12
          Top = 136
          Width = 400
          Height = 17
          Caption = 'Envio de DANFE e XML por email'
          TabOrder = 10
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 313
    Width = 630
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrNFeLEnC: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeLEnCBeforeOpen
    BeforeClose = QrNFeLEnCBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM nfelenc'
      'WHERE Codigo=:P0')
    Left = 280
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrNFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeLEnCversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrNFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
    end
    object QrNFeLEnCcMsg: TWideStringField
      FieldName = 'cMsg'
      Size = 4
    end
    object QrNFeLEnCxMsg: TWideStringField
      FieldName = 'xMsg'
      Size = 200
    end
    object QrNFeLEnCindSinc: TSmallintField
      FieldName = 'indSinc'
    end
  end
  object DsNFeLEnC: TDataSource
    DataSet = QrNFeLEnC
    Left = 280
    Top = 192
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Status, IF(Status=infProt_cStat, infProt_xMotivo,  '
      'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  '
      'FROM nfecaba  '
      'WHERE IDCtrl=106138 ')
    Left = 352
    Top = 148
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFeCabAMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 352
    Top = 192
  end
end
