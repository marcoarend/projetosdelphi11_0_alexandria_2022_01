unit NFeLEnU_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums;

type
  TFmNFeLEnU_0310 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    QrNFeLEnC: TmySQLQuery;
    QrNFeLEnCCodigo: TIntegerField;
    QrNFeLEnCCodUsu: TIntegerField;
    QrNFeLEnCNome: TWideStringField;
    QrNFeLEnCEmpresa: TIntegerField;
    QrNFeLEnCversao: TFloatField;
    QrNFeLEnCtpAmb: TSmallintField;
    QrNFeLEnCverAplic: TWideStringField;
    QrNFeLEnCcStat: TIntegerField;
    QrNFeLEnCxMotivo: TWideStringField;
    QrNFeLEnCcUF: TSmallintField;
    QrNFeLEnCnRec: TWideStringField;
    QrNFeLEnCdhRecbto: TDateTimeField;
    QrNFeLEnCtMed: TIntegerField;
    QrNFeLEnCcMsg: TWideStringField;
    QrNFeLEnCxMsg: TWideStringField;
    QrNFeLEnCindSinc: TSmallintField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DsNFeLEnC: TDataSource;
    DBEdit2: TDBEdit;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAMotivo: TWideStringField;
    DsNFeCabA: TDataSource;
    CheckBox7: TCheckBox;
    QrNFeCabAIDCtrl: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeLEnCBeforeClose(DataSet: TDataSet);
    procedure QrNFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure DBEdit3Change(Sender: TObject);
    procedure DBEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure TraduzMensagem(CodErro: Integer);
  public
    { Public declarations }
    FIDCtrl: Integer;
    //
    function EnviarNFe(): Boolean;
    procedure ReabreNFeLEnC(Codigo: Integer);
    procedure ReopenNFeCabA();
    function Interrompe(Passo: Integer): Boolean;
  end;

  var
  FmNFeLEnU_0310: TFmNFeLEnU_0310;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule,
  ModuleNFe_0000, NFeSteps_0310, MyDBCheck, NFe_Pesq_0000, FatPedNFs_0310;

{$R *.DFM}

procedure TFmNFeLEnU_0310.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLEnU_0310.DBEdit1Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeLEnC.State <> dsInactive then
  begin
    case QrNFeLEnCcStat.Value of
      0..99: Cor := clBlack;
      103: Cor := clGreen;
      104: Cor := clBlue;
      105: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit1.Font.Color := Cor;
    DBEdit2.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFeLEnU_0310.DBEdit3Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeCabA.State <> dsInactive then
  begin
    case QrNFeCabAStatus.Value of
      0..99: Cor := clBlack;
      100: Cor := clBlue;
      101..199: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit3.Font.Color := Cor;
    DBEdit4.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFeLEnU_0310.TraduzMensagem(CodErro: Integer);
begin
  case CodErro of
    232: Geral.MB_Aviso('Destinat�rio com IE n�o informado!' + slinebreak +
      'A UF iformou que o destinat�rio possui IE ativa na UF!');
  end;
end;

function TFmNFeLEnU_0310.EnviarNFe(): Boolean;
var
  CodUsu, Codigo, Empresa, LoteEnv, FatID, FatNum, Status, infProt_cStat,
  indSinc: Integer;
  infProt_xMotivo: String;
  Sincronia: TXXeIndSinc;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando XML da NF-e');
  if FmFatPedNFs_0310.QrNFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na NF-e 3.10
    if not FmFatPedNFs_0310.GerarNFe(True, True, True) then
      if Interrompe(1) then Exit;
    *)
  end else
  begin
    if not FmFatPedNFs_0310.GerarNFe(True, False, True) then
      if Interrompe(1) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := FmFatPedNFs_0310.QrNFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  Empresa := FmFatPedNFs_0310.QrFatPedNFsEmpresa.Value;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  indSinc := FmFatPedNFs_0310.QrFatPedCabindSinc.Value;
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(2) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando NF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreNFeLEnC(LoteEnv);
  FatID := FmFatPedNFs_0310.QrFatPedNFsCabA_FatID.Value;
  FatNum := FmFatPedNFs_0310.QrFatPedNFsOriCodi.Value;
  Status  := DmNFe_0000.stepNFeAdedLote();
  infProt_cStat   := 0; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  LoteEnv, Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(3) then Exit;
  CheckBox3.Checked := True;

  //
  Sincronia := TXXeIndSinc(indSinc);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
  if DBCheck.CriaFm(TFmNFeSteps_0310, FmNFeSteps_0310, afmoNegarComAviso) then
  begin
    FmNFeSteps_0310.PnLoteEnv.Visible := True;
    FmNFeSteps_0310.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
    FmNFeSteps_0310.Show;
    //
    FmNFeSteps_0310.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
    FmNFeSteps_0310.RGIndSinc.ItemIndex := Integer(Sincronia);
    FmNFeSteps_0310.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia);
    // Ver o que fazer!
    FmNFeSteps_0310.FFormChamou      := 'FmNFeLEnU_0310';
    //
    FmNFeSteps_0310.BtOKClick(Self);
    //
    CheckBox4.Checked := True;
    if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
    begin
      FmNFeSteps_0310.Destroy;
      if Interrompe(4) then Exit;
    end;
    //
    if Sincronia = TXXeIndSinc.nisAssincrono then
    begin
      //
      // Usar Ck???.Checked se der muitos bugs
      while QrNFeLEnCcStat.Value = 105 do
      begin
        Sleep(5000);
        FmNFeSteps_0310.BtOKClick(Self);
      end;
      // Esperar Timer1
      while FmNFeSteps_0310.FSegundos < FmNFeSteps_0310.FSecWait do
      begin
        FmNFeSteps_0310.LaWait.Update;
        Application.ProcessMessages;
      end;
    end;
    FmNFeSteps_0310.Destroy;
  end else if
    Interrompe(5) then Exit;
  //CheckBox4.Checked := True;

    //

  if Sincronia = TXXeIndSinc.nisAssincrono then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
    if DBCheck.CriaFm(TFmNFeSteps_0310, FmNFeSteps_0310, afmoNegarComAviso) then
    begin
      FmNFeSteps_0310.PnLoteEnv.Visible := True;
      FmNFeSteps_0310.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
      FmNFeSteps_0310.Show;
      //
      FmNFeSteps_0310.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
      FmNFeSteps_0310.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
        QrNFeLEnCnRec.Value);
      FmNFeSteps_0310.FFormChamou      := 'FmNFeLEnU_0310';
      //
      FmNFeSteps_0310.BtOKClick(Self);
      //
      FmNFeSteps_0310.Destroy;
      if QrNFeLEnCcStat.Value <> 104 then
        if Interrompe(7) then Exit;
    end else if
      Interrompe(6) then Exit;
    CheckBox5.Checked := True;
    //
    FIDCtrl := FmFatPedNFs_0310.QrNFeCabAIDCtrl.Value;
  end else
  begin
    // Consulta sincrona!
    CheckBox5.Checked := True;
    ReopenNFeCabA();
    FIDCtrl := QrNFeCabAIDCtrl.Value;
  end;
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Localizando NF-e');
  //

  FIDCtrl := FmFatPedNFs_0310.QrNFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      //
      FmNFe_Pesq_0000.ReopenNFeCabA(FIDCtrl, True);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      if (FmNFe_Pesq_0000.QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (FmNFe_Pesq_0000.QrNFeCabA.RecordCount = 1) then
        begin
          if (FmNFe_Pesq_0000.QrNFeCabAStatus.Value = 100) then
          //if FmNFe_Pesq_0000.BtImprime.Enabled then
          begin
            //FmNFe_Pesq_0000.DefineFrx(FmNFe_Pesq_0000.frxA4A_002, ficMostra);
            FmNFe_Pesq_0000.DefineQual_frxNFe(ficMostra);
            CheckBox6.Checked := FmNFe_Pesq_0000.FDANFEImpresso;
            //
            FmNFe_Pesq_0000.BtEnviaClick(Self);
            //
            FmNFe_Pesq_0000.Destroy;
            CheckBox7.Checked := FmNFe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else begin
            FmNFe_Pesq_0000.Destroy;
            if Interrompe(12) then Exit;
          end;
        end else
        begin
          FmNFe_Pesq_0000.Destroy;
          if Interrompe(11) then Exit;
        end;
      end else
      begin
        FmNFe_Pesq_0000.Destroy;
        if Interrompe(10) then Exit;
      end;
    end else if
      Interrompe(9) then Exit;
    CheckBox5.Checked := True;
    //
    Result := True;
    //
  end else
    if Interrompe(8) then Exit;
end;


procedure TFmNFeLEnU_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFeLEnU_0310.FormCreate(Sender: TObject);
begin
  FIDCtrl := 0;
end;

procedure TFmNFeLEnU_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeLEnU_0310.Interrompe(Passo: Integer): Boolean;
var
  Aviso: String;
begin
  Result := True;
  Aviso := 'ABORTADO NO PASSO ' + FormatFloat('0', Passo) +
    ' POR ERRO EM: ' + LaAviso1.Caption;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
  GBRodaPe.Visible := True;
  ReopenNFeCabA();
end;

procedure TFmNFeLEnU_0310.QrNFeLEnCBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmNFeLEnU_0310.QrNFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  ReopenNFeCabA();
end;

procedure TFmNFeLEnU_0310.ReopenNFeCabA();
begin
  if FIDCtrl <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
    'SELECT Status, IDCtrl, ',
    'IF(Status=infProt_cStat, infProt_xMotivo,  ',
    'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  ',
    'FROM nfecaba  ',
    'WHERE IDCtrl=' + FormatFloat('0', FIDCtrl),
    '']);
  end;
end;

procedure TFmNFeLEnU_0310.ReabreNFeLEnC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeLEnC, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfelenc ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
end;

end.
