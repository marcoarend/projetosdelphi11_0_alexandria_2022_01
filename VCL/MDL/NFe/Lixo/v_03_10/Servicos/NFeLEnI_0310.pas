unit NFeLEnI_0310;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkImage, UnDmkEnums;

type
  TFmNFeLEnI_0310 = class(TForm)
    Panel1: TPanel;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAid: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    DBGrid1: TDBGrid;
    DsNFeCabA: TDataSource;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLEnI_0310: TFmNFeLEnI_0310;

implementation

uses UnMyObjects, NFeLEnC_0310, Module, UMySQLModule, dmkGeral, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLEnI_0310.BtConfirmaClick(Sender: TObject);
  procedure AdicionaNFeAoLote();
  var
    LoteEnv, FatID, FatNum, Empresa, Status: Integer;
    infProt_cStat, infProt_xMotivo: String;
  begin
    LoteEnv := FmNFeLEnC_0310.QrNFeLEnCCodigo.Value;
    FatID   := QrNFeCabAFatID.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    Status  := DmNFe_0000.stepNFeAdedLote();
    infProt_cStat   := '0'; // Limpar variaveis (ver historico)
    infProt_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'], ['FatID', 'FatNum', 'Empresa'], [
    LoteEnv, Status, infProt_cStat, infProt_xMotivo], [FatID, FatNum, Empresa], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmNFeLEnC_0310.QrNFeCabA.RecordCount > 50 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 50 NFs permitidas por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
    ' NF-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        //GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        GotoBookmark(DBGrid1.SelectedRows.Items[I]);
        AdicionaNFeAoLote();
      end;
    end;
  end else AdicionaNFeAoLote();
  FmNFeLEnC_0310.ReopenNFeLEnI();
  Close;
end;

procedure TFmNFeLEnI_0310.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLEnI_0310.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLEnI_0310.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmNFeLEnC_0310.LocCod(FmNFeLEnC_0310.QrNFeLEnCCodigo.Value, FmNFeLEnC_0310.QrNFeLEnCCodigo.Value);
end;

procedure TFmNFeLEnI_0310.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFeLEnI_0310.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
