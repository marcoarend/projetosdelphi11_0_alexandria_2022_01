object FmNFeDesConC_0101: TFmNFeDesConC_0101
  Left = 368
  Top = 194
  Caption = 'NFe-DESTI-001 :: Consulta NF-es Destinadas 1.01'
  ClientHeight = 617
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label9: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 837
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 840
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 860
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
            NumGlyphs = 2
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
          NumGlyphs = 2
        end
      end
    end
    object RGindNFe: TdmkRadioGroup
      Left = 8
      Top = 88
      Width = 897
      Height = 93
      Caption = 'Indicador de NF-e consultada:'
      ItemIndex = 3
      Items.Strings = (
        '0 = Todas as NF-e;'
        
          '1 = Somente as NF-e que ainda n'#227'o tiveram manifesta'#231#227'o do destin' +
          'at'#225'rio (Desconhecimento da opera'#231#227'o, Opera'#231#227'o n'#227'o Realizada ou C' +
          'onfirma'#231#227'o da Opera'#231#227'o);'
        
          '2 = Idem anterior, incluindo as NF-e que tamb'#233'm n'#227'o tiveram a Ci' +
          #234'ncia da Opera'#231#227'o.'
        'N'#227'o definido')
      TabOrder = 2
      UpdType = utYes
      OldValor = 0
    end
    object RGindEmi: TdmkRadioGroup
      Left = 8
      Top = 184
      Width = 897
      Height = 77
      Caption = 'Indicador do Emissor da NF-e:'
      ItemIndex = 2
      Items.Strings = (
        '0 = Todos os Emitentes / Remetentes;'
        
          '1 = Somente as NF-e emitidas por emissores / remetentes que n'#227'o ' +
          'tenham o mesmo CNPJ-Base do destinat'#225'rio (para excluir as notas ' +
          'fiscais de transfer'#234'ncia entre filiais).'
        'N'#227'o definido')
      TabOrder = 3
      UpdType = utYes
      OldValor = 0
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 45
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 68
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label5: TLabel
          Left = 508
          Top = 4
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdit2: TDBEdit
          Left = 508
          Top = 20
          Width = 56
          Height = 21
          DataField = 'Filial'
          DataSource = DsNFeDescConC
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 564
          Top = 20
          Width = 436
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsNFeDescConC
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 68
          Top = 20
          Width = 436
          Height = 21
          DataField = 'Nome'
          DataSource = DsNFeDescConC
          TabOrder = 2
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNFeDescConC
          TabOrder = 3
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 45
      Width = 1008
      Height = 316
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        object Splitter1: TSplitter
          Left = 0
          Top = 105
          Width = 1000
          Height = 5
          Cursor = crVSplit
          Align = alTop
        end
        object DBGNFeDesRIts: TDBGrid
          Left = 0
          Top = 110
          Width = 1000
          Height = 122
          Align = alClient
          DataSource = DsNFeDesRIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGNFeDesRItsDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_cSitConf'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Situa'#231#227'o da Manifesta'#231#227'o do Destinat'#225'rio'
              Width = 205
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDCad'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_REGISTROU'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Registrou?'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_chNFe'
              Title.Caption = 'Chave da NF-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF_TXT'
              Title.Caption = 'CNPJ / CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Nome'
              Width = 221
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_dhRecbto'
              Title.Caption = 'Data/hora recebimento'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_vNF'
              Title.Caption = 'Valor NF'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_cSitNFe'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Situa'#231#227'o da NF-e'
              Visible = True
            end>
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 105
          Align = alTop
          Caption = ' Grupos de NF-es encontradas: '
          TabOrder = 1
          object DBGrid2: TDBGrid
            Left = 2
            Top = 15
            Width = 996
            Height = 88
            Align = alClient
            DataSource = DsNFeDesRCab
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora_TXT'
                Title.Caption = 'Data / Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_indCont'
                Title.Caption = 'Continua?'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Ambiente'
                Title.Caption = 'Ambiente'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ultNSU'
                Title.Caption = #218'ltimo NSU'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'verAplic'
                Title.Caption = 'Vers'#227'o da aplica'#231#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'versao'
                Title.Caption = 'Vers'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'cStat'
                Title.Caption = 'Status'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'xMotivo'
                Title.Caption = 'Motivo (descri'#231#227'o do status)'
                Width = 640
                Visible = True
              end>
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 232
          Width = 1000
          Height = 56
          Align = alBottom
          Caption = 
            ' Texto da carta de corre'#231#227'o no momento da pesquisa (quando houve' +
            'r): '
          TabOrder = 2
          object DBMemo1: TDBMemo
            Left = 2
            Top = 15
            Width = 996
            Height = 39
            Align = alClient
            DataField = 'CCe_xCorrecao'
            DataSource = DsNFeDesRIts
            TabOrder = 0
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 998
          ControlData = {
            4C0000005A6700000E1100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 258
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 432
        Top = 15
        Width = 574
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 441
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtLote: TBitBtn
          Tag = 526
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
          NumGlyphs = 2
        end
        object BtNFe: TBitBtn
          Tag = 456
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbImprimeClick
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 396
        Height = 32
        Caption = 'Consulta NF-es Destinadas 1.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 396
        Height = 32
        Caption = 'Consulta NF-es Destinadas 1.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 396
        Height = 32
        Caption = 'Consulta NF-es Destinadas 1.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeDescConC: TDataSource
    DataSet = QrNFeDesConC
    Left = 40
    Top = 65524
  end
  object QrNFeDesConC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeDesConCBeforeOpen
    AfterOpen = QrNFeDesConCAfterOpen
    AfterScroll = QrNFeDesConCAfterScroll
    OnCalcFields = QrNFeDesConCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfedesconc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 65524
    object QrNFeDesConCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDesConCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDesConCNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrNFeDesConCversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDesConCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDesConCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeDesConCindNFe: TSmallintField
      FieldName = 'indNFe'
    end
    object QrNFeDesConCindEmi: TSmallintField
      FieldName = 'indEmi'
    end
    object QrNFeDesConCultNSU: TLargeintField
      FieldName = 'ultNSU'
      Required = True
    end
    object QrNFeDesConCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDesConCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDesConCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDesConCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDesConCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDesConCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDesConCAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeDesConCFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFeDesConCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtNFe
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 452
    Top = 536
    object Incluinovolote1: TMenuItem
      Caption = '&Nova consulta'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&AlteraConsulta'
      Enabled = False
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui consulta atual'
      Enabled = False
      Visible = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo retorno'
      OnClick = Lerarquivo1Click
    end
  end
  object DsNFeDesRIts: TDataSource
    DataSet = QrNFeDesRIts
    Left = 488
    Top = 52
  end
  object QrNFeDesRIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeDesRItsCalcFields
    SQL.Strings = (
      'SELECT IF(ndi.Nfa_IDCtrl=0, "N'#195'O", "SIM") NO_REGISTROU, '
      'ndi.* '
      'FROM nfedesrits ndi '
      'WHERE ndi.IDCad IN ( '
      '     SELECT IDCad '
      '     FROM nfedesrnfe '
      '     WHERE Codigo>0'
      ' '
      '     UNION '
      ' '
      '     SELECT IDCad '
      '     FROM nfedesrcan '
      '     WHERE Codigo>0'
      ' '
      '     UNION '
      ' '
      '     SELECT IDCad '
      '     FROM nfedesrcce '
      '     WHERE Codigo>0'
      ') ')
    Left = 460
    Top = 52
    object QrNFeDesRItsIDCad: TIntegerField
      FieldName = 'IDCad'
    end
    object QrNFeDesRItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDesRItsNFe_NSU: TLargeintField
      FieldName = 'NFe_NSU'
      Required = True
    end
    object QrNFeDesRItsNFe_chNFe: TWideStringField
      FieldName = 'NFe_chNFe'
      Size = 44
    end
    object QrNFeDesRItsNFe_CNPJ: TWideStringField
      FieldName = 'NFe_CNPJ'
      Size = 18
    end
    object QrNFeDesRItsNFe_CPF: TWideStringField
      FieldName = 'NFe_CPF'
      Size = 18
    end
    object QrNFeDesRItsNFe_xNome: TWideStringField
      FieldName = 'NFe_xNome'
      Size = 18
    end
    object QrNFeDesRItsNFe_IE: TWideStringField
      FieldName = 'NFe_IE'
      Size = 18
    end
    object QrNFeDesRItsNFe_dEmi: TDateTimeField
      FieldName = 'NFe_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeDesRItsNFe_tpNF: TSmallintField
      FieldName = 'NFe_tpNF'
    end
    object QrNFeDesRItsNFe_vNF: TFloatField
      FieldName = 'NFe_vNF'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeDesRItsNFe_digVal: TWideStringField
      FieldName = 'NFe_digVal'
      Size = 28
    end
    object QrNFeDesRItsNFe_dhRecbto: TDateTimeField
      FieldName = 'NFe_dhRecbto'
      DisplayFormat = 'dd/hh/yy hh:nn:ss'
    end
    object QrNFeDesRItsNFe_cSitNFe: TSmallintField
      FieldName = 'NFe_cSitNFe'
    end
    object QrNFeDesRItsNFe_cSitConf: TSmallintField
      FieldName = 'NFe_cSitConf'
    end
    object QrNFeDesRItsCanc_NSU: TLargeintField
      FieldName = 'Canc_NSU'
      Required = True
    end
    object QrNFeDesRItsCanc_chNFe: TWideStringField
      FieldName = 'Canc_chNFe'
      Size = 44
    end
    object QrNFeDesRItsCanc_CNPJ: TWideStringField
      FieldName = 'Canc_CNPJ'
      Size = 18
    end
    object QrNFeDesRItsCanc_CPF: TWideStringField
      FieldName = 'Canc_CPF'
      Size = 18
    end
    object QrNFeDesRItsCanc_xNome: TWideStringField
      FieldName = 'Canc_xNome'
      Size = 18
    end
    object QrNFeDesRItsCanc_IE: TWideStringField
      FieldName = 'Canc_IE'
      Size = 18
    end
    object QrNFeDesRItsCanc_dEmi: TDateTimeField
      FieldName = 'Canc_dEmi'
    end
    object QrNFeDesRItsCanc_tpNF: TSmallintField
      FieldName = 'Canc_tpNF'
    end
    object QrNFeDesRItsCanc_vNF: TFloatField
      FieldName = 'Canc_vNF'
    end
    object QrNFeDesRItsCanc_digVal: TWideStringField
      FieldName = 'Canc_digVal'
      Size = 28
    end
    object QrNFeDesRItsCanc_dhRecbto: TDateTimeField
      FieldName = 'Canc_dhRecbto'
    end
    object QrNFeDesRItsCanc_cSitNFe: TSmallintField
      FieldName = 'Canc_cSitNFe'
    end
    object QrNFeDesRItsCanc_cSitConf: TSmallintField
      FieldName = 'Canc_cSitConf'
    end
    object QrNFeDesRItsCCe_NSU: TLargeintField
      FieldName = 'CCe_NSU'
      Required = True
    end
    object QrNFeDesRItsCCe_chNFe: TWideStringField
      FieldName = 'CCe_chNFe'
      Size = 44
    end
    object QrNFeDesRItsCCe_dhEvento: TDateTimeField
      FieldName = 'CCe_dhEvento'
    end
    object QrNFeDesRItsCCe_tpEvento: TIntegerField
      FieldName = 'CCe_tpEvento'
      Required = True
    end
    object QrNFeDesRItsCCe_nSeqEvento: TSmallintField
      FieldName = 'CCe_nSeqEvento'
    end
    object QrNFeDesRItsCCe_descEvento: TWideStringField
      FieldName = 'CCe_descEvento'
      Size = 60
    end
    object QrNFeDesRItsCCe_xCorrecao: TWideMemoField
      FieldName = 'CCe_xCorrecao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeDesRItsCCe_tpNF: TSmallintField
      FieldName = 'CCe_tpNF'
    end
    object QrNFeDesRItsCCe_dhRecbto: TDateTimeField
      FieldName = 'CCe_dhRecbto'
    end
    object QrNFeDesRItsCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeDesRItsNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME'
      Size = 60
      Calculated = True
    end
    object QrNFeDesRItscSitNFe: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'cSitNFe'
      Calculated = True
    end
    object QrNFeDesRItsNO_cSitNFe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_cSitNFe'
      Size = 15
      Calculated = True
    end
    object QrNFeDesRItscSitConf: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'cSitConf'
      Calculated = True
    end
    object QrNFeDesRItsNO_cSitConf: TWideStringField
      DisplayWidth = 40
      FieldKind = fkCalculated
      FieldName = 'NO_cSitConf'
      Size = 40
      Calculated = True
    end
    object QrNFeDesRItsNFa_IDCtrl: TIntegerField
      FieldName = 'NFa_IDCtrl'
    end
    object QrNFeDesRItsNO_REGISTROU: TWideStringField
      FieldName = 'NO_REGISTROU'
      Required = True
      Size = 3
    end
  end
  object QrNFeDesRCab: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeDesRCabCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM nfedesrcab'
      'WHERE Codigo=:P0'
      '')
    Left = 404
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeDesRCabNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Size = 15
      Calculated = True
    end
    object QrNFeDesRCabDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Size = 19
      Calculated = True
    end
    object QrNFeDesRCabNO_indCont: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_indCont'
      Size = 3
      Calculated = True
    end
    object QrNFeDesRCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDesRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeDesRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDesRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDesRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDesRCabverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeDesRCabcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeDesRCabdhResp: TDateTimeField
      FieldName = 'dhResp'
    end
    object QrNFeDesRCabindCont: TSmallintField
      FieldName = 'indCont'
    end
    object QrNFeDesRCabultNSU: TLargeintField
      FieldName = 'ultNSU'
      Required = True
    end
  end
  object DsNFeDesRCab: TDataSource
    DataSet = QrNFeDesRCab
    Left = 432
    Top = 52
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 580
    Top = 536
    object RegistraChaveNFeparaManifestar1: TMenuItem
      Caption = 'Registra Chave NF-e para Manifestar'
      OnClick = RegistraChaveNFeparaManifestar1Click
    end
  end
end
