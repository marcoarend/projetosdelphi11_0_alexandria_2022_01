object FmNFeConsulta_0201: TFmNFeConsulta_0201
  Left = 339
  Top = 185
  Caption = 'NFe-CNSLT-001 :: Consulta NF-e 2.01'
  ClientHeight = 493
  ClientWidth = 966
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 966
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 918
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 870
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 237
        Height = 32
        Caption = 'Consulta NF-e 2.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 237
        Height = 32
        Caption = 'Consulta NF-e 2.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 237
        Height = 32
        Caption = 'Consulta NF-e 2.01'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 966
    Height = 331
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 966
      Height = 331
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 966
        Height = 331
        Align = alClient
        TabOrder = 0
        object Memo1: TMemo
          Left = 2
          Top = 61
          Width = 266
          Height = 268
          Align = alLeft
          Lines.Strings = (
            '')
          TabOrder = 2
          WantReturns = False
          WordWrap = False
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 962
          Height = 46
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 544
            Top = 4
            Width = 75
            Height = 13
            Caption = 'Chave da NF-e:'
          end
          object Label2: TLabel
            Left = 12
            Top = 4
            Width = 23
            Height = 13
            Caption = 'Filial:'
          end
          object EdChaveNFe: TdmkEdit
            Left = 544
            Top = 20
            Width = 400
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtChaveNFe
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1512 0304 3339 5200 0188 5500 9000 0002 0317 8000 4121'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '1512 0304 3339 5200 0188 5500 9000 0002 0317 8000 4121'
            ValWarn = False
          end
          object EdEmpresa: TdmkEditCB
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 469
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 2
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Memo2: TMemo
          Left = 268
          Top = 61
          Width = 368
          Height = 268
          Align = alLeft
          Lines.Strings = (
            'Memo2')
          TabOrder = 1
        end
        object Memo3: TMemo
          Left = 636
          Top = 61
          Width = 328
          Height = 268
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'Memo3')
          ParentFont = False
          TabOrder = 3
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 379
    Width = 966
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 962
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 423
    Width = 966
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 818
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Button1: TButton
        Left = 156
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        Visible = False
        OnClick = Button1Click
      end
    end
  end
  object XMLDoc1: TXMLDocument
    Left = 172
    Top = 136
    DOMVendorDesc = 'MSXML'
  end
end
