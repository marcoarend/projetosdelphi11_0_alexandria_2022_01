object FmNFeLEnC_0200: TFmNFeLEnC_0200
  Left = 368
  Top = 194
  Caption = 'NFe-LOTES-001 :: Lotes de NF-e'
  ClientHeight = 617
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 192
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 793
        Height = 192
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 152
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label3: TLabel
          Left = 68
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdNome
        end
        object Label5: TLabel
          Left = 8
          Top = 44
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdNome: TDBEdit
          Left = 68
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsNFeLEnC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 60
          Width = 56
          Height = 21
          DataField = 'Filial'
          DataSource = DsNFeLEnC
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 68
          Top = 60
          Width = 717
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsNFeLEnC
          TabOrder = 2
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 84
          Width = 493
          Height = 101
          Caption = ' Resposta do envio do XML: '
          TabOrder = 3
          object Label11: TLabel
            Left = 8
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Vers'#227'o do leiaute:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 108
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Tipo de ambiente:'
            FocusControl = DBEdit7
          end
          object Label13: TLabel
            Left = 276
            Top = 20
            Width = 171
            Height = 13
            Caption = 'Vers'#227'o do aplic. que recebeu o lote:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 8
            Top = 60
            Width = 48
            Height = 13
            Caption = 'Resposta:'
            FocusControl = DBEdit9
          end
          object Label15: TLabel
            Left = 456
            Top = 20
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit12
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 36
            Width = 97
            Height = 21
            DataField = 'versao'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 108
            Top = 36
            Width = 21
            Height = 21
            DataField = 'tpAmb'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 276
            Top = 36
            Width = 177
            Height = 21
            DataField = 'verAplic'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 8
            Top = 76
            Width = 33
            Height = 21
            DataField = 'cStat'
            DataSource = DsNFeLEnC
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 44
            Top = 76
            Width = 441
            Height = 21
            DataField = 'xMotivo'
            DataSource = DsNFeLEnC
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 128
            Top = 36
            Width = 145
            Height = 21
            DataField = 'NO_Ambiente'
            DataSource = DsNFeLEnC
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 456
            Top = 36
            Width = 29
            Height = 21
            DataField = 'cUF'
            DataSource = DsNFeLEnC
            TabOrder = 6
          end
        end
        object GroupBox2: TGroupBox
          Left = 504
          Top = 84
          Width = 281
          Height = 101
          Caption = ' Recibo de aceite do lote: '
          TabOrder = 4
          object Label16: TLabel
            Left = 8
            Top = 16
            Width = 87
            Height = 13
            Caption = 'N'#250'mero do recibo:'
            FocusControl = DBEdit13
          end
          object Label10: TLabel
            Left = 8
            Top = 56
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
            FocusControl = DBEdit5
          end
          object Label17: TLabel
            Left = 184
            Top = 56
            Width = 84
            Height = 13
            Caption = 't m'#233'd. resp. (seg):'
            FocusControl = DBEdit14
          end
          object DBEdit13: TDBEdit
            Left = 8
            Top = 32
            Width = 265
            Height = 21
            DataField = 'nRec'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 72
            Width = 173
            Height = 21
            DataField = 'DataHora_TXT'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 184
            Top = 72
            Width = 89
            Height = 21
            DataField = 'tMed'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
        end
        object DBEdit4: TDBEdit
          Left = 152
          Top = 20
          Width = 633
          Height = 21
          DataField = 'Nome'
          DataSource = DsNFeLEnC
          TabOrder = 5
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNFeLEnC
          TabOrder = 6
        end
      end
      object DBGrid2: TDBGrid
        Left = 793
        Top = 0
        Width = 215
        Height = 192
        Align = alClient
        DataSource = DsNFeLEnM
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 192
      Width = 1008
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          DataSource = DsNFeCabA
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'Nota fiscal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSTot_vNF'
              Title.Caption = 'Valor total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'id'
              Title.Caption = 'Chave de acesso da NF-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_cStat'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_xMotivo'
              Width = 660
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 998
          ControlData = {
            4C0000005A6700000E1100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 432
        Top = 15
        Width = 574
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 441
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtNFe: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtNFeClick
        end
        object BtXML: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 720
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 860
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 173
        Height = 32
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 173
        Height = 32
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 173
        Height = 32
        Caption = 'Lotes de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeLEnC: TDataSource
    DataSet = QrNFeLEnC
    Left = 40
    Top = 65524
  end
  object QrNFeLEnC: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeLEnCBeforeOpen
    AfterOpen = QrNFeLEnCAfterOpen
    AfterScroll = QrNFeLEnCAfterScroll
    OnCalcFields = QrNFeLEnCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfelenc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 65524
    object QrNFeLEnCDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrNFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfelenc.Codigo'
    end
    object QrNFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'nfelenc.CodUsu'
    end
    object QrNFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfelenc.Nome'
      Size = 30
    end
    object QrNFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfelenc.Empresa'
    end
    object QrNFeLEnCversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfelenc.versao'
      DisplayFormat = '0.00'
    end
    object QrNFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfelenc.tpAmb'
    end
    object QrNFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfelenc.verAplic'
    end
    object QrNFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
      Origin = 'nfelenc.cStat'
    end
    object QrNFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfelenc.xMotivo'
      Size = 255
    end
    object QrNFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
      Origin = 'nfelenc.cUF'
    end
    object QrNFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Origin = 'nfelenc.nRec'
      Size = 15
    end
    object QrNFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
      Origin = 'nfelenc.dhRecbto'
    end
    object QrNFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
      Origin = 'nfelenc.tMed'
    end
    object QrNFeLEnCFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeLEnCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeLEnCNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtXML
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 568
    Top = 476
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object Verificalotenofisco1: TMenuItem
      Caption = 'Verifica lote no fisco'
      Enabled = False
      OnClick = Verificalotenofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      object Enviodelote1: TMenuItem
        Caption = '&Envio de lote'
        OnClick = Enviodelote1Click
      end
      object Consultadelote1: TMenuItem
        Caption = '&Consulta de lote'
        OnClick = Consultadelote1Click
      end
    end
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 664
    Top = 476
    object IncluiNFeaoloteatual1: TMenuItem
      Caption = '&Inclui NFe ao lote atual'
      OnClick = IncluiNFeaoloteatual1Click
    end
    object RemoveNFesselecionadas1: TMenuItem
      Caption = '&Remove NF-e(s) selecionada(s)'
      OnClick = RemoveNFesselecionadas1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
  object DsNFeLEnI: TDataSource
    DataSet = QrNFeLEnI
    Left = 512
    Top = 8
  end
  object QrNFeLEnI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 8
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, ide_nNF, ide_dEmi, '
      'id, ide_cNF, ide_serie, Status, ICMSTot_vNF,'
      'infProt_cStat, infProt_xMotivo'
      'FROM nfecaba '
      'WHERE LoteEnv=:P0'
      '')
    Left = 428
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      DisplayFormat = '000000000'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeCabAid: TWideStringField
      FieldName = 'id'
      Size = 44
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
      DisplayFormat = '000'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 456
    Top = 8
  end
  object QrNFeLEnM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfelenm'
      'WHERE Codigo=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 544
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnMcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeLEnMxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeLEnMnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrNFeLEnMdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeLEnMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeLEnMversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeLEnMtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeLEnMverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeLEnMcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeLEnMtMed: TIntegerField
      FieldName = 'tMed'
    end
  end
  object DsNFeLEnM: TDataSource
    DataSet = QrNFeLEnM
    Left = 572
    Top = 8
  end
  object PMXML: TPopupMenu
    Left = 744
    Top = 472
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote'
        OnClick = Lote1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaNFEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML da NFE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assina'
        OnClick = Assina1Click
      end
    end
  end
end
