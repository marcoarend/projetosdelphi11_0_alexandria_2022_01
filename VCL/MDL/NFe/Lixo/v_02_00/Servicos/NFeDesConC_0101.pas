unit NFeDesConC_0101;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmNFeDesConC_0101 = class(TForm)
    PainelDados: TPanel;
    DsNFeDescConC: TDataSource;
    QrNFeDesConC: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    DsNFeDesRIts: TDataSource;
    QrNFeDesRIts: TmySQLQuery;
    QrNFeDesRCab: TmySQLQuery;
    DsNFeDesRCab: TDataSource;
    N1: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGNFeDesRIts: TDBGrid;
    WB_XML: TWebBrowser;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrNFeDesConCCodigo: TIntegerField;
    QrNFeDesConCEmpresa: TIntegerField;
    QrNFeDesConCNome: TWideStringField;
    QrNFeDesConCversao: TFloatField;
    QrNFeDesConCtpAmb: TSmallintField;
    QrNFeDesConCCNPJ: TWideStringField;
    QrNFeDesConCindNFe: TSmallintField;
    QrNFeDesConCindEmi: TSmallintField;
    QrNFeDesConCultNSU: TLargeintField;
    QrNFeDesConCLk: TIntegerField;
    QrNFeDesConCDataCad: TDateField;
    QrNFeDesConCDataAlt: TDateField;
    QrNFeDesConCUserCad: TIntegerField;
    QrNFeDesConCUserAlt: TIntegerField;
    QrNFeDesConCAlterWeb: TSmallintField;
    QrNFeDesConCAtivo: TSmallintField;
    QrNFeDesConCFilial: TIntegerField;
    QrNFeDesConCNO_Empresa: TWideStringField;
    RGindNFe: TdmkRadioGroup;
    RGindEmi: TdmkRadioGroup;
    QrNFeDesRCabNO_Ambiente: TWideStringField;
    QrNFeDesRCabDataHora_TXT: TWideStringField;
    GroupBox2: TGroupBox;
    DBGrid2: TDBGrid;
    Splitter1: TSplitter;
    QrNFeDesRCabNO_indCont: TWideStringField;
    QrNFeDesRCabCodigo: TIntegerField;
    QrNFeDesRCabControle: TIntegerField;
    QrNFeDesRCabEmpresa: TIntegerField;
    QrNFeDesRCabversao: TFloatField;
    QrNFeDesRCabtpAmb: TSmallintField;
    QrNFeDesRCabverAplic: TWideStringField;
    QrNFeDesRCabcStat: TIntegerField;
    QrNFeDesRCabdhResp: TDateTimeField;
    QrNFeDesRCabindCont: TSmallintField;
    QrNFeDesRCabultNSU: TLargeintField;
    QrNFeDesRItsIDCad: TIntegerField;
    QrNFeDesRItsEmpresa: TIntegerField;
    QrNFeDesRItsNFe_NSU: TLargeintField;
    QrNFeDesRItsNFe_chNFe: TWideStringField;
    QrNFeDesRItsNFe_CNPJ: TWideStringField;
    QrNFeDesRItsNFe_CPF: TWideStringField;
    QrNFeDesRItsNFe_xNome: TWideStringField;
    QrNFeDesRItsNFe_IE: TWideStringField;
    QrNFeDesRItsNFe_dEmi: TDateTimeField;
    QrNFeDesRItsNFe_tpNF: TSmallintField;
    QrNFeDesRItsNFe_vNF: TFloatField;
    QrNFeDesRItsNFe_digVal: TWideStringField;
    QrNFeDesRItsNFe_dhRecbto: TDateTimeField;
    QrNFeDesRItsNFe_cSitNFe: TSmallintField;
    QrNFeDesRItsNFe_cSitConf: TSmallintField;
    QrNFeDesRItsCanc_NSU: TLargeintField;
    QrNFeDesRItsCanc_chNFe: TWideStringField;
    QrNFeDesRItsCanc_CNPJ: TWideStringField;
    QrNFeDesRItsCanc_CPF: TWideStringField;
    QrNFeDesRItsCanc_xNome: TWideStringField;
    QrNFeDesRItsCanc_IE: TWideStringField;
    QrNFeDesRItsCanc_dEmi: TDateTimeField;
    QrNFeDesRItsCanc_tpNF: TSmallintField;
    QrNFeDesRItsCanc_vNF: TFloatField;
    QrNFeDesRItsCanc_digVal: TWideStringField;
    QrNFeDesRItsCanc_dhRecbto: TDateTimeField;
    QrNFeDesRItsCanc_cSitNFe: TSmallintField;
    QrNFeDesRItsCanc_cSitConf: TSmallintField;
    QrNFeDesRItsCCe_NSU: TLargeintField;
    QrNFeDesRItsCCe_chNFe: TWideStringField;
    QrNFeDesRItsCCe_dhEvento: TDateTimeField;
    QrNFeDesRItsCCe_tpEvento: TIntegerField;
    QrNFeDesRItsCCe_nSeqEvento: TSmallintField;
    QrNFeDesRItsCCe_descEvento: TWideStringField;
    QrNFeDesRItsCCe_xCorrecao: TWideMemoField;
    QrNFeDesRItsCCe_tpNF: TSmallintField;
    QrNFeDesRItsCCe_dhRecbto: TDateTimeField;
    QrNFeDesRItsCNPJ_CPF_TXT: TWideStringField;
    QrNFeDesRItsNOME: TWideStringField;
    QrNFeDesRItscSitNFe: TSmallintField;
    QrNFeDesRItsNO_cSitNFe: TWideStringField;
    QrNFeDesRItscSitConf: TSmallintField;
    QrNFeDesRItsNO_cSitConf: TWideStringField;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    PMNFe: TPopupMenu;
    RegistraChaveNFeparaManifestar1: TMenuItem;
    BtNFe: TBitBtn;
    QrNFeDesRItsNFa_IDCtrl: TIntegerField;
    QrNFeDesRItsNO_REGISTROU: TWideStringField;
    QrDup: TmySQLQuery;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeDesConCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeDesConCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeDesConCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure QrNFeDesConCAfterScroll(DataSet: TDataSet);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFeDesRCabCalcFields(DataSet: TDataSet);
    procedure QrNFeDesRItsCalcFields(DataSet: TDataSet);
    procedure DBGNFeDesRItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure RegistraChaveNFeparaManifestar1Click(Sender: TObject);
    procedure BtNFeClick(Sender: TObject);
  private
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad: Integer;
              ReopenQry: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeDesRCab();
    procedure ReopenNFeDesRIts(IDCad: Integer);
    //
    procedure ExecutaPesquisa(Empresa, Lote: Integer);
  end;

var
  FmNFeDesConC_0101: TFmNFeDesConC_0101;
const
  FFormatFloat = '00000';
  verConsNFeDest_Int =  1.01;

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleNFe_0000,
NFeSteps_0400, DmkDAC_PF, NFeXMLGerencia, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeDesConC_0101.Lerarquivo1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
  begin
    FmNFeSteps_0400.PnLoteEnv.Visible := True;
    FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpNFeVerConDes.Value;
    FmNFeSteps_0400.Show;
    //
    FmNFeSteps_0400.RGAcao.ItemIndex := 9; // Consulta NF-es Destinadas
    FmNFeSteps_0400.CkSoLer.Checked  := True;
    FmNFeSteps_0400.PreparaConsultaDeNFesDestinadas(QrNFeDesConCEmpresa.Value,
      QrNFeDesConCCodigo.Value, QrNFeDesRCabultNSU.Value);
    FmNFeSteps_0400.FFormChamou      := 'FmNFeDesConC_0400';
    //
  end;
end;

procedure TFmNFeDesConC_0101.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeDesConC_0101.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeDesConCCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeDesConC_0101.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeDesConC_0101.DefParams;
begin
  VAR_GOTOTABELA := 'NFeDesConC';
  VAR_GOTOMYSQLTABLE := QrNFeDesConC;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfedesconc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmNFeDesConC_0101.ExecutaPesquisa(Empresa, Lote: Integer);
var
  Qry: TmySQLQuery;
  ultNSU: Int64;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
    //
    Qry.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(ultNSU) ultNSU',
    'FROM nfedesrcab',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND tpAmb=' + Geral.FF0(DModG.QrPrmsEmpNFeide_tpAmb.Value),
    '']);
    ultNSU := Geral.I64(Qry.FieldByName('ultNSU').AsString);
    if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
    begin
      FmNFeSteps_0400.PnLoteEnv.Visible := True;
      FmNFeSteps_0400.PnDesConC.Visible := True;
      FmNFeSteps_0400.EdVersaoAcao.ValueVariant := verConsNFeDest_Int;
      FmNFeSteps_0400.Show;
      //
      FmNFeSteps_0400.RGAcao.ItemIndex := 9; // Consulta NF-es Destinadas
      FmNFeSteps_0400.PreparaConsultaDeNFesDestinadas(Empresa, Lote, ultNSU);
      FmNFeSteps_0400.FFormChamou      := 'FmNFeDesConC_0400';
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmNFeDesConC_0101.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeDesConC_0101.PMLotePopup(Sender: TObject);
var
  HabilitaA: Boolean;
begin
  HabilitaA := (QrNFeDesConC.State = dsBrowse) and (QrNFeDesConC.RecordCount > 0);
  //Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  //
end;

procedure TFmNFeDesConC_0101.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeDesRIts.RecordCount > 0)
  and (QrNFeDesRItsNfa_IDCtrl.Value = 0)
  (*and (QrNFeDesRItscSitConf.Value = 0)*);
  RegistraChaveNFeparaManifestar1.Enabled := Habilita;
end;

procedure TFmNFeDesConC_0101.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeDesConC_0101.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeDesConC_0101.RegistraChaveNFeparaManifestar1Click(
  Sender: TObject);
var
  infCanc_dhRecbto, infProt_dhRecbto, ide_dEmi: TDateTime;
  //
  Id, emit_CNPJ, emit_CPF, emit_xNome, emit_IE, infProt_Id, infProt_digVal,
  infCanc_Id, emit_UF: String;
  //
  IDCad, NFa_IDCtrl, CodInfoEmit, CodInfoDest, cSitNFe, cSitCan, cSitConf,
  FatID, FatNum, Empresa, IDCtrl, ide_tpNF, infProt_cStat, ide_tpAmb, Status,
  infProt_tpAmb, infCanc_tpAmb, infCanc_cStat: Integer;
  //
  ICMSTot_vNF: Double;
  //
  VersaoNFe: Double;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
{
  NFa_IDCtrl := DmNFE_0000.ObtemIDCtrlDeChaveNFe(QrNFeDesRItsNFe_chNFe.Value);
  if NFa_IDCtrl <> 0 then
  begin
    Geral.MB_Aviso('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
    ' j� est� registrada na base de dados com o ID ' + Geral.FF0(NFa_IDCtrl));
    if QrNFeDesRItsNFa_IDCtrl.Value <> NFa_IDCtrl then
    begin
      IDCad := QrNFeDesRItsIDCad.Value;
      AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad, True);
    end;
    Exit;
  end
  else
  begin
    ide_tpAmb        := QrNFeDesConCtpAmb.Value;
    //
    Empresa          := QrNFeDesRItsEmpresa        .Value;
    Id               := QrNFeDesRItsNFe_chNFe      .Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDup, Dmod.MyDB, [
    'SELECT IDCtrl',
    'FROM nfecaba ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Id="' + Id + '"',
    '']);
    if QrDup.RecordCount > 0 then
    begin
      Geral.MB_Aviso('A chave ' + Id +
      ' j� est� registrada na base de dados com o ID ' +
      Geral.FF0(QrDup.FieldByName('IDCtrl').AsInteger));
      //
      Exit;
    end else begin
      emit_CNPJ        := QrNFeDesRItsNFe_CNPJ       .Value;
      emit_CPF         := QrNFeDesRItsNFe_CPF        .Value;
      emit_xNome       := QrNFeDesRItsNFe_xNome      .Value;
      emit_IE          := QrNFeDesRItsNFe_IE         .Value;
      ide_dEmi         := QrNFeDesRItsNFe_dEmi       .Value;
      ide_tpNF         := QrNFeDesRItsNFe_tpNF       .Value;
      ICMSTot_vNF      := QrNFeDesRItsNFe_vNF        .Value;
      infProt_digVal   := QrNFeDesRItsNFe_digVal     .Value;
      infProt_dhRecbto := QrNFeDesRItsNFe_dhRecbto   .Value;
      cSitNFe          := QrNFeDesRItsNFe_cSitNFe    .Value;
      cSitConf         := QrNFeDesRItsNFe_cSitConf   .Value;
      infProt_cStat    := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitNFe);


      infProt_Id       := 'ID' + Id;

      infCanc_Id       := QrNFeDesRItsCanc_chNFe     .Value;
      infCanc_dhRecbto := QrNFeDesRItsCanc_dhRecbto  .Value;
      cSitCan          := QrNFeDesRItsCanc_cSitNFe   .Value;
      infCanc_cStat    := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitCan);

      infProt_tpAmb    := ide_tpAmb;
      if infCanc_cStat = 101 then
      begin
        infCanc_tpAmb  := ide_tpAmb;
        Status         := infCanc_cStat;
      end else
      begin
        infCanc_tpAmb  := 0;
        Status         := infProt_cStat;
      end;
      //
(*   Fazer?
      := QrNFeDesRItsCCe_NSU        .Value;
      := QrNFeDesRItsCCe_chNFe      .Value;
      := QrNFeDesRItsCCe_dhEvento   .Value;
      := QrNFeDesRItsCCe_tpEvento   .Value;
      := QrNFeDesRItsCCe_nSeqEvento .Value;
      := QrNFeDesRItsCCe_descEvento .Value;
      := QrNFeDesRItsCCe_xCorrecao  .Value;
      := QrNFeDesRItsCCe_tpNF       .Value;
      := QrNFeDesRItsCCe_dhRecbto   .Value;
*)

      DModG.ObtemEntidadeDeCNPJCFP(emit_CNPJ, CodInfoEmit);
      CodInfoDest:= Empresa;

      // N�o presisa saber! (at� quando?)
      VersaoNFe := 0;
      NFeXMLGeren.DesmontaChaveDeAcesso(Id, VersaoNFe,
        UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
      emit_UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(UF_IBGE));
      //
      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      FatID  := VAR_FATID_0052;
      // Ver como implementar no StqInCad
      FatNum := QrNFeDesRItsIdCad.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
      'FatID', 'FatNum', 'Empresa',
      'Id', 'emit_CNPJ', 'emit_CPF',
      'emit_xNome', 'emit_IE', 'ide_dEmi',
      'ide_tpNF', 'ICMSTot_vNF', 'infProt_digVal',
      'infProt_dhRecbto', 'infProt_cStat', 'infProt_Id',
      'infCanc_Id', 'infCanc_dhRecbto', 'infCanc_cStat',
      'infProt_tpAmb', 'infCanc_tpAmb', 'Status',
      'ide_tpAmb', 'CodInfoEmit', 'CodInfoDest',
      'cSitNFe', 'cSitConf', 'emit_UF'
      ], [
      'IDCtrl'
      ], [
      FatID, FatNum, Empresa,
      Id, emit_CNPJ, emit_CPF,
      emit_xNome, emit_IE, ide_dEmi,
      ide_tpNF, ICMSTot_vNF, infProt_digVal,
      infProt_dhRecbto, infProt_cStat, infProt_Id,
      infCanc_Id, infCanc_dhRecbto, infCanc_cStat,
      infProt_tpAmb, infCanc_tpAmb, Status,
      ide_tpAmb, CodInfoEmit, CodInfoDest,
      cSitNFe, cSitConf, emit_UF
      ], [
      IDCtrl], True) then
      begin
        IDCad := QrNFeDesRItsIDCad.Value;
        NFa_IDCtrl := IDCtrl;
        //
        AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad, True);
        //
        if QrNFeDesRItsNFa_IDCtrl.Value <> 0 then
          Geral.MB_Info('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
          ' foi registrada com sucesso!')
        else
          Geral.MB_Erro('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
          ' pode n�o ter sido registrada!');
      end;
    end;
  end;
}
end;

procedure TFmNFeDesConC_0101.ReopenNFeDesRCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeDesRCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfedesrcab ',
  'WHERE Codigo=' + Geral.FF0(QrNFeDesConCCodigo.Value),
  '']);
end;

procedure TFmNFeDesConC_0101.ReopenNFeDesRIts(IDCad: Integer);
var
  Cod_Txt: String;
begin
  Cod_Txt := Geral.FF0(QrNFeDesRCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeDesRIts, Dmod.MyDB, [
  'SELECT IF(ndi.Nfa_IDCtrl=0, "N�O", "SIM") NO_REGISTROU, ',
  'ndi.* ',
  'FROM nfedesrits ndi ',
  'WHERE ndi.IDCad IN ( ',
  '     SELECT IDCad ',
  '     FROM nfedesrnfe ',
  '     WHERE Codigo=' + Cod_Txt,
  ' ',
  '     UNION ',
  ' ',
  '     SELECT IDCad ',
  '     FROM nfedesrcan ',
  '     WHERE Codigo=' + Cod_Txt,
  ' ',
  '     UNION ',
  ' ',
  '     SELECT IDCad ',
  '     FROM nfedesrcce ',
  '     WHERE Codigo=' + Cod_Txt,
  ') ',
  '']);
  //
  if IDCad > 0 then
    QrNFeDesRIts.Locate('IDCad', IDCad, []);
end;

procedure TFmNFeDesConC_0101.DBGNFeDesRItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFe_0000.ColoreStatusNFeEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State, QrNFeDesRItscSitNFe.Value);
  //
  DmNFe_0000.ColoreSitConfNFeEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State, QrNFeDesRItscSitConf.Value);
  //
  MyObjects.ColoreSimNaoEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State,
    'NO_REGISTROU',  QrNFeDesRItsNO_REGISTROU.Value);
end;

procedure TFmNFeDesConC_0101.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeDesConC_0101.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeDesConC_0101.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeDesConC_0101.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeDesConC_0101.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeDesConC_0101.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeDesConC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfedesconc');
end;

procedure TFmNFeDesConC_0101.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeDesConCCodigo.Value;
  Close;
end;

procedure TFmNFeDesConC_0101.AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad: Integer;
ReopenQry: Boolean);
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedesrits', False, [
  'NFa_IDCtrl'], ['IDCad'], [
  NFa_IDCtrl], [IDCad], True) then
    if ReopenQry then
      ReopenNFeDesRIts(IDCad);
end;

procedure TFmNFeDesConC_0101.BtConfirmaClick(Sender: TObject);
const
  _Hora = 1/24;
var
  Nome, (*xServ,*) CNPJ(*, XML*): String;
  Codigo, Empresa, tpAmb, indNFe, indEmi: Integer;
  versao, ultNSU: Double;
  Qry: TmySQLQuery;
  Antes, Agora, dhDif: TDateTime;
begin
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa') then
    Exit;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  DmNFe_0000.ReopenOpcoesNFe(Empresa, True);
  Nome           := EdNome.Text;
  versao         := verConsNFeDest_Int;
  tpAmb          := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
  //xServ          := CO_CONSULTAR_NFE_DEST;
  CNPJ           := Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value);
  indNFe         := RGindEmi.ItemIndex;
  indEmi         := RGindNFe.ItemIndex;
  ultNSU         := 0; //???
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(dhResp) dhResp ',
    'FROM nfedesrcab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND tpAmb=' + Geral.FF0(tpAmb),
    ' ']);
    //
    Antes := Qry.FieldByName('dhResp').AsDateTime;
    Agora := DModG.ObtemAgora();
    dhDif := Agora - Antes;
    if dhDif < _Hora then
    begin
      Geral.MB_Aviso('O tempo m�nimo de espera entre consultas � de 1 hora.' +
      'Faltam ' + Geral.FDT(_Hora - dhDif, 110));
      Exit;
    end;
  finally
    Qry.Free;
  end;
  {
  if MyObjects.FIC(not(tpAmb in [1,2]), RGtpAmb, 'Informe o ambiente!') then
    Exit;
  }
  if MyObjects.FIC(not(indNFe in [0,1,2]), RGindNFe, 'Informe o "Indicador de NFe consultada"!') then
    Exit;
  if MyObjects.FIC(not(indEmi in [0,1]), RGindEmi, 'Informe o "Indicador do Emissor da NF-e"!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('nfedesconc', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfedesconc', False, [
  'Empresa', 'Nome', 'versao',
  'tpAmb', (*'xServ',*) 'CNPJ',
  'indNFe', 'indEmi', 'ultNSU'(*,
  'XML'*)], [
  'Codigo'], [
  Empresa, Nome, versao,
  tpAmb, (*xServ,*) CNPJ,
  indNFe, indEmi, ultNSU(*,
  XML*)], [
  Codigo], True) then
  begin
    MostraEdicao(0, stLok, 0);
    ExecutaPesquisa(Empresa, Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeDesConC_0101.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeDesConC', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeDesConC_0101.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeDesConC_0101.BtNFeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeDesConC_0101.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmNFeDesConC_0101.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeDesConCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeDesConC_0101.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeDesConC_0101.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeDesConC_0101.SbNovoClick(Sender: TObject);
begin
{:::
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeDesConCCodUsu.Value, LaRegistro.Caption);
}
end;

procedure TFmNFeDesConC_0101.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeDesConC_0101.QrNFeDesConCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeDesConC_0101.QrNFeDesConCAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeDesRCab();
  ReopenNFeDesRIts(0);
end;

procedure TFmNFeDesConC_0101.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeDesConC_0101.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeDesConCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeDesConC', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeDesConC_0101.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeDesConC_0101.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeDesConC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfedesconc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFeDesConC_0101.QrNFeDesConCBeforeOpen(DataSet: TDataSet);
begin
  QrNFeDesConCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeDesConC_0101.QrNFeDesConCCalcFields(DataSet: TDataSet);
begin
{
  QrNFeDesConCDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeDesConCdhRecbto.Value, 0);
  case QrNFeDesConCtpAmb.Value of
    1: QrNFeDesConCNO_Ambiente.Value := 'Produ��o';
    2: QrNFeDesConCNO_Ambiente.Value := 'Homologa��o';
    else QrNFeDesConCNO_Ambiente.Value := '';
  end;
}
end;

procedure TFmNFeDesConC_0101.QrNFeDesRCabCalcFields(DataSet: TDataSet);
begin
  QrNFeDesRCabDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeDesRCabdhResp.Value, 0);
  QrNFeDesRCabNO_Ambiente.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,
    QrNFeDesRCabtpAmb.Value);
  QrNFeDesRCabNO_indCont.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indCont,
    QrNFeDesRCabindCont.Value);
end;

procedure TFmNFeDesConC_0101.QrNFeDesRItsCalcFields(DataSet: TDataSet);
begin
  if (QrNFeDesRItsNFe_CNPJ.Value <> '') or (QrNFeDesRItsNFe_CPF.Value <> '') then
    QrNFeDesRItsCNPJ_CPF_TXT.Text := Geral.FormataCNPJ_TT(
    QrNFeDesRItsNFe_CNPJ.Value + QrNFeDesRItsNFe_CPF.Value)
  else
  if (QrNFeDesRItsCanc_CNPJ.Value <> '') or (QrNFeDesRItsCanc_CPF.Value <> '') then
    QrNFeDesRItsCNPJ_CPF_TXT.Text := Geral.FormataCNPJ_TT(
    QrNFeDesRItsCanc_CNPJ.Value + QrNFeDesRItsCanc_CPF.Value)
  else
    QrNFeDesRItsCNPJ_CPF_TXT.Text := '';
  //
  if QrNFeDesRItsNFe_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsNFe_xNome.Value
  else
  if QrNFeDesRItsNFe_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsNFe_xNome.Value
  else
  if QrNFeDesRItsCanc_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsCanc_xNome.Value
  else
    QrNFeDesRItsNOME.Text := '';
  //
  if QrNFeDesRItsCanc_cSitNFe.Value = 3 then
    QrNFeDesRItscSitNFe.Value := 3
  else
    QrNFeDesRItscSitNFe.Value := QrNFeDesRItsNFe_cSitNFe.Value;
{
  case QrNFeDesRItscSitNFe.Value of
    1: QrNFeDesRItsNO_cSitNFe.Value := 'Autorizada';
    2: QrNFeDesRItsNO_cSitNFe.Value := 'Denegada';
    3: QrNFeDesRItsNO_cSitNFe.Value := 'Cancelada';
    else QrNFeDesRItsNO_cSitNFe.Value := '? ? ? ? ?';
  end;
}
  QrNFeDesRItsNO_cSitNFe.Value :=
    NFeXMLGeren.Obtem_DeManifestacao_de_cSitNFe_xSituacao(
    QrNFeDesRItscSitNFe.Value);
  //
  if QrNFeDesRItsCanc_dhRecbto.Value > QrNFeDesRItsNFe_dhRecbto.Value then
    QrNFeDesRItscSitConf.Value := QrNFeDesRItsCanc_cSitConf.Value
  else
    QrNFeDesRItscSitConf.Value := QrNFeDesRItsNFe_cSitConf.Value;
  //
  QrNFeDesRItsNO_cSitConf.Value :=
    NFeXMLGeren.Obtem_DeManifestacao_de_cSitConf_xEvento(
    QrNFeDesRItscSitConf.Value);
  {
  case QrNFeDesRItscSitConf.Value of
    0: QrNFeDesRItsNO_cSitConf.Value := 'Sem manifesta��o do destinat�rio';
    1: QrNFeDesRItsNO_cSitConf.Value := 'Confirmada a opera��o';
    2: QrNFeDesRItsNO_cSitConf.Value := 'Opera��o desconhecida';
    3: QrNFeDesRItsNO_cSitConf.Value := 'Opera��o n�o realizada';
    4: QrNFeDesRItsNO_cSitConf.Value := 'Ci�ncia da opera��o';
    else QrNFeDesRItsNO_cSitConf.Value := '? ? ? ? ?';
  end;
  }
end;

end.


