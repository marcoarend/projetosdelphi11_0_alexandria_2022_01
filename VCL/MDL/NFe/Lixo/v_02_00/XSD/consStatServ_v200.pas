
{************************************************************************************************}
{                                                                                                }
{                                        XML Data Binding                                        }
{                                                                                                }
{         Generated on: 08/07/2010 00:34:06                                                      }
{       Generated from: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consStatServ_v2.00.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consStatServ_v2.00.xdb   }
{                                                                                                }
{************************************************************************************************}

unit consStatServ_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsStatServ = interface;

{ IXMLTConsStatServ }

  IXMLTConsStatServ = interface(IXMLNode)
    ['{96EC2319-4351-446C-BBF3-FA00B99FC09C}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_CUF: WideString;
    function Get_XServ: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_CUF(Value: WideString);
    procedure Set_XServ(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property CUF: WideString read Get_CUF write Set_CUF;
    property XServ: WideString read Get_XServ write Set_XServ;
  end;

{ Forward Decls }

  TXMLTConsStatServ = class;

{ TXMLTConsStatServ }

  TXMLTConsStatServ = class(TXMLNode, IXMLTConsStatServ)
  protected
    { IXMLTConsStatServ }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_CUF: WideString;
    function Get_XServ: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_CUF(Value: WideString);
    procedure Set_XServ(Value: WideString);
  end;

{ Global Functions }

function GetconsStatServ(Doc: IXMLDocument): IXMLTConsStatServ;
function LoadconsStatServ(const FileName: WideString): IXMLTConsStatServ;
function NewconsStatServ: IXMLTConsStatServ;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsStatServ(Doc: IXMLDocument): IXMLTConsStatServ;
begin
  Result := Doc.GetDocBinding('consStatServ', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

function LoadconsStatServ(const FileName: WideString): IXMLTConsStatServ;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consStatServ', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

function NewconsStatServ: IXMLTConsStatServ;
begin
  Result := NewXMLDocument.GetDocBinding('consStatServ', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

{ TXMLTConsStatServ }

function TXMLTConsStatServ.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsStatServ.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsStatServ.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsStatServ.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsStatServ.Get_CUF: WideString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTConsStatServ.Set_CUF(Value: WideString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTConsStatServ.Get_XServ: WideString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsStatServ.Set_XServ(Value: WideString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

end.