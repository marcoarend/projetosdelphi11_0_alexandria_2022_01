
{**********************************************************************************************}
{                                                                                              }
{                                       XML Data Binding                                       }
{                                                                                              }
{         Generated on: 08/07/2010 12:47:09                                                    }
{       Generated from: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consSitNFe_v2.00.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consSitNFe_v2.00.xdb   }
{                                                                                              }
{**********************************************************************************************}

unit consSitNFe_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsSitNFe = interface;

{ IXMLTConsSitNFe }

  IXMLTConsSitNFe = interface(IXMLNode)
    ['{9058E015-32E9-4B69-8027-1E6CB746049F}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_ChNFe: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_ChNFe(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property XServ: WideString read Get_XServ write Set_XServ;
    property ChNFe: WideString read Get_ChNFe write Set_ChNFe;
  end;

{ Forward Decls }

  TXMLTConsSitNFe = class;

{ TXMLTConsSitNFe }

  TXMLTConsSitNFe = class(TXMLNode, IXMLTConsSitNFe)
  protected
    { IXMLTConsSitNFe }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_ChNFe: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_ChNFe(Value: WideString);
  end;

{ Global Functions }

function GetconsSitNFe(Doc: IXMLDocument): IXMLTConsSitNFe;
function LoadconsSitNFe(const FileName: WideString): IXMLTConsSitNFe;
function NewconsSitNFe: IXMLTConsSitNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsSitNFe(Doc: IXMLDocument): IXMLTConsSitNFe;
begin
  Result := Doc.GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

function LoadconsSitNFe(const FileName: WideString): IXMLTConsSitNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

function NewconsSitNFe: IXMLTConsSitNFe;
begin
  Result := NewXMLDocument.GetDocBinding('consSitNFe', TXMLTConsSitNFe, TargetNamespace) as IXMLTConsSitNFe;
end;

{ TXMLTConsSitNFe }

function TXMLTConsSitNFe.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsSitNFe.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsSitNFe.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsSitNFe.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsSitNFe.Get_XServ: WideString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsSitNFe.Set_XServ(Value: WideString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsSitNFe.Get_ChNFe: WideString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLTConsSitNFe.Set_ChNFe(Value: WideString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

end.