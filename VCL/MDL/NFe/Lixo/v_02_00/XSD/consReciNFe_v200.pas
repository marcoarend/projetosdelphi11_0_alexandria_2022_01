
{***********************************************************************************************}
{                                                                                               }
{                                       XML Data Binding                                        }
{                                                                                               }
{         Generated on: 08/07/2010 17:37:29                                                     }
{       Generated from: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consReciNFe_v2.00.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consReciNFe_v2.00.xdb   }
{                                                                                               }
{***********************************************************************************************}

unit consReciNFe_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsReciNFe = interface;

{ IXMLTConsReciNFe }

  IXMLTConsReciNFe = interface(IXMLNode)
    ['{DEFF7813-6B9C-4433-A3A0-CCF707EDFF56}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_NRec: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_NRec(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property NRec: WideString read Get_NRec write Set_NRec;
  end;

{ Forward Decls }

  TXMLTConsReciNFe = class;

{ TXMLTConsReciNFe }

  TXMLTConsReciNFe = class(TXMLNode, IXMLTConsReciNFe)
  protected
    { IXMLTConsReciNFe }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_NRec: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_NRec(Value: WideString);
  end;

{ Global Functions }

function GetconsReciNFe(Doc: IXMLDocument): IXMLTConsReciNFe;
function LoadconsReciNFe(const FileName: WideString): IXMLTConsReciNFe;
function NewconsReciNFe: IXMLTConsReciNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsReciNFe(Doc: IXMLDocument): IXMLTConsReciNFe;
begin
  Result := Doc.GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

function LoadconsReciNFe(const FileName: WideString): IXMLTConsReciNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

function NewconsReciNFe: IXMLTConsReciNFe;
begin
  Result := NewXMLDocument.GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

{ TXMLTConsReciNFe }

function TXMLTConsReciNFe.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsReciNFe.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsReciNFe.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsReciNFe.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsReciNFe.Get_NRec: WideString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTConsReciNFe.Set_NRec(Value: WideString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

end.