
{*******************************************************************************************}
{                                                                                           }
{                                     XML Data Binding                                      }
{                                                                                           }
{         Generated on: 27/11/2012 14:38:18                                                 }
{       Generated from: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consCad_v2.00.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\v_02_00\XSD\PL_006g\consCad_v2.00.xdb   }
{                                                                                           }
{*******************************************************************************************}

unit consCad_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsCad = interface;
  IXMLInfCons = interface;

{ IXMLTConsCad }

  IXMLTConsCad = interface(IXMLNode)
    ['{04FFABCB-6AA0-4829-8B28-9776A8F09715}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_InfCons: IXMLInfCons;
    procedure Set_Versao(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property InfCons: IXMLInfCons read Get_InfCons;
  end;

{ IXMLInfCons }

  IXMLInfCons = interface(IXMLNode)
    ['{5DD6D2F7-E5CE-479B-BBF0-57861800CA94}']
    { Property Accessors }
    function Get_XServ: WideString;
    function Get_UF: WideString;
    function Get_IE: WideString;
    function Get_CNPJ: WideString;
    function Get_CPF: WideString;
    procedure Set_XServ(Value: WideString);
    procedure Set_UF(Value: WideString);
    procedure Set_IE(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_CPF(Value: WideString);
    { Methods & Properties }
    property XServ: WideString read Get_XServ write Set_XServ;
    property UF: WideString read Get_UF write Set_UF;
    property IE: WideString read Get_IE write Set_IE;
    property CNPJ: WideString read Get_CNPJ write Set_CNPJ;
    property CPF: WideString read Get_CPF write Set_CPF;
  end;

{ Forward Decls }

  TXMLTConsCad = class;
  TXMLInfCons = class;

{ TXMLTConsCad }

  TXMLTConsCad = class(TXMLNode, IXMLTConsCad)
  protected
    { IXMLTConsCad }
    function Get_Versao: WideString;
    function Get_InfCons: IXMLInfCons;
    procedure Set_Versao(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCons }

  TXMLInfCons = class(TXMLNode, IXMLInfCons)
  protected
    { IXMLInfCons }
    function Get_XServ: WideString;
    function Get_UF: WideString;
    function Get_IE: WideString;
    function Get_CNPJ: WideString;
    function Get_CPF: WideString;
    procedure Set_XServ(Value: WideString);
    procedure Set_UF(Value: WideString);
    procedure Set_IE(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_CPF(Value: WideString);
  end;

{ Global Functions }

function GetConsCad(Doc: IXMLDocument): IXMLTConsCad;
function LoadConsCad(const FileName: WideString): IXMLTConsCad;
function NewConsCad: IXMLTConsCad;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetConsCad(Doc: IXMLDocument): IXMLTConsCad;
begin
  Result := Doc.GetDocBinding('ConsCad', TXMLTConsCad, TargetNamespace) as IXMLTConsCad;
end;

function LoadConsCad(const FileName: WideString): IXMLTConsCad;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('ConsCad', TXMLTConsCad, TargetNamespace) as IXMLTConsCad;
end;

function NewConsCad: IXMLTConsCad;
begin
  Result := NewXMLDocument.GetDocBinding('ConsCad', TXMLTConsCad, TargetNamespace) as IXMLTConsCad;
end;

{ TXMLTConsCad }

procedure TXMLTConsCad.AfterConstruction;
begin
  RegisterChildNode('infCons', TXMLInfCons);
  inherited;
end;

function TXMLTConsCad.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsCad.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsCad.Get_InfCons: IXMLInfCons;
begin
  Result := ChildNodes['infCons'] as IXMLInfCons;
end;

{ TXMLInfCons }

function TXMLInfCons.Get_XServ: WideString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLInfCons.Set_XServ(Value: WideString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLInfCons.Get_UF: WideString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLInfCons.Set_UF(Value: WideString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLInfCons.Get_IE: WideString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLInfCons.Set_IE(Value: WideString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLInfCons.Get_CNPJ: WideString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLInfCons.Set_CNPJ(Value: WideString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLInfCons.Get_CPF: WideString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLInfCons.Set_CPF(Value: WideString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

end.