unit NFeValidaXML_0200;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, ACBrMSXML2_TLB, dmkEdit, dmkImage,
  UnDmkProcFunc, UnDmkEnums, NFe_PF;

type
  TFmNFeValidaXML_0200 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    MeLoaded: TMemo;
    Panel4: TPanel;
    MeScaned: TMemo;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Label64: TLabel;
    EdSchema: TdmkEdit;
    Label1: TLabel;
    EdArquivo: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdArquivoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTextoArq: AnsiString;
    function ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    function ValidaMSXML_0200(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    procedure AbreArquivoSelecionado();
    function RetornarConteudoEntre(const Frase, Inicio, Fim: string): string;
  public
    { Public declarations }
  end;

  var
    FmNFeValidaXML_0200: TFmNFeValidaXML_0200;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts;

{$R *.DFM}

procedure TFmNFeValidaXML_0200.AbreArquivoSelecionado();
var
  Arquivo: String;
begin
  FTextoArq := '';
  Arquivo := EdArquivo.Text;
  if FileExists(Arquivo) then
  begin
    FTextoArq := dmkPF.LoadFileToText(Arquivo);
    MeLoaded.Text := FTextoArq;
  end;
end;

procedure TFmNFeValidaXML_0200.BtOKClick(Sender: TObject);
begin
  ValidaXML(2.00, FTextoArq);
end;

procedure TFmNFeValidaXML_0200.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeValidaXML_0200.EdArquivoChange(Sender: TObject);
begin
  AbreArquivoSelecionado();
end;

procedure TFmNFeValidaXML_0200.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeValidaXML_0200.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdSchema.Text := UnNFe_PF.SchemaNFe(Geral.IMV(VAR_LIB_EMPRESAS));
end;

procedure TFmNFeValidaXML_0200.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeValidaXML_0200.RetornarConteudoEntre(const Frase, Inicio,
  Fim: string): string;
var
  i: integer;
  s: string;
begin
  result := '';
  i := pos(Inicio, Frase);
  if i = 0 then
    exit;
  s := Copy(Frase, i + length(Inicio), maxInt);
  result := Copy(s, 1, pos(Fim, s) - 1);
end;

procedure TFmNFeValidaXML_0200.SpeedButton1Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione a pasta com os Schemas', '', [], Arquivo) then
    EdSchema.Text := ExtractFilePath(Arquivo);
end;

procedure TFmNFeValidaXML_0200.SpeedButton2Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    EdArquivo.Text := Arquivo;
end;

function TFmNFeValidaXML_0200.ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
 TextoAValidar: AnsiString;
begin
  Result := False;
  if pos('<Signature', XML) = 0 then
  begin
    Geral.MB_Aviso('Nota n�o pode ser validada pois n�o foi assinada!');
    Exit;
  end;
  case Trunc((Versao + 0.009) * 100) of
    200:
    begin
      if pos('<NFe xmlns', XML) > 0 then
        TextoAValidar := '<NFe xmlns' + RetornarConteudoEntre(XML, '<NFe xmlns', '</NFe>')+ '</NFe>'
      else
        TextoAValidar := '<evento xmlns' + RetornarConteudoEntre(XML, '<evento', '</evento>')+ '</evento>';
      Result := ValidaMSXML_0200(TextoAValidar, APathSchemas);
    end
    else Geral.MB_Aviso('ValidaXML: Vers�o do XML n�o implementada : ' +
       FloatToStr(Versao));
  end;
  if Result then
    MeScaned.Text := 'Arquivo v�lido!' + sLineBreak + MeScaned.Text;
end;

function TFmNFeValidaXML_0200.ValidaMSXML_0200(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
  DOMDocument: IXMLDOMDocument2;
  ParseError: IXMLDOMParseError;
  Schema: XMLSchemaCache;
  Tipo, I : Integer;
  Dir, Arquivo: String;
begin
  Result := False;
  //
  I := pos('<infNFe',XML) ;
  Tipo := 1;
  if I = 0  then
   begin
     I := pos('<infCanc',XML) ;
     if I > 0 then
        Tipo := 2
     else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
           Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
            Tipo := 5
          else
            Tipo := 4;
        end;
      end;
   end;

  DOMDocument := CoDOMDocument50.Create;
  DOMDocument.async := False;
  DOMDocument.resolveExternals := False;
  DOMDocument.validateOnParse := True;
  DOMDocument.loadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Dir := EdSchema.Text;
  if not Geral.VerificaDir(
    Dir, '\', 'Diretorio de Schemas xml (*.xsd)', True) then Exit;
  //
  case Tipo of
    1: Arquivo := Dir + 'nfe_v2.00.xsd';
    2: Arquivo := Dir + 'cancNFe_v2.00.xsd';
    3: Arquivo := Dir + 'inutNFe_v2.00.xsd';
    4: Arquivo := Dir + 'envDPEC_v2.01.xsd';
    5: Arquivo := Dir + 'e110110_v1.00.xsd'; // ??
    else Arquivo := '### ERRO ###';
  end;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('O Schema XML n�o foi localizado:' + sLineBreak + Arquivo);
    Exit;
  end;
  Schema.add( 'http://www.portalfiscal.inf.br/nfe', Arquivo);
  //
  DOMDocument.schemas := Schema;
  ParseError := DOMDocument.validate;
  Result := (ParseError.errorCode = 0);
  MeScaned.Text := ParseError.reason;

  DOMDocument := nil;
  ParseError := nil;
  Schema := nil;
end;

end.
