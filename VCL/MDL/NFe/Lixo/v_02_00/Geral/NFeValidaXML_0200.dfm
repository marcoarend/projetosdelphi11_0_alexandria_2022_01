object FmNFeValidaXML_0200: TFmNFeValidaXML_0200
  Left = 339
  Top = 185
  Caption = 'NFe-VALID-001 :: Valida'#231#227'o de Arquivo XML 2.00'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 197
      Width = 784
      Height = 7
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 146
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label64: TLabel
        Left = 8
        Top = 4
        Width = 263
        Height = 13
        Caption = 'Pasta que cont'#233'm os arquivos dos Schemas xml (*.xsd):'
      end
      object SpeedButton1: TSpeedButton
        Left = 752
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 133
        Height = 13
        Caption = 'Arquivo XML a ser validado:'
      end
      object SpeedButton2: TSpeedButton
        Left = 752
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object EdSchema: TdmkEdit
        Left = 8
        Top = 20
        Width = 741
        Height = 21
        MaxLength = 255
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdArquivo: TdmkEdit
        Left = 8
        Top = 60
        Width = 741
        Height = 21
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdArquivoChange
      end
    end
    object MeLoaded: TMemo
      Left = 0
      Top = 108
      Width = 784
      Height = 89
      Align = alTop
      TabOrder = 1
    end
    object Panel4: TPanel
      Left = 0
      Top = 204
      Width = 784
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Resultado da Valida'#231#227'o:'
      TabOrder = 2
    end
    object MeScaned: TMemo
      Left = 0
      Top = 220
      Width = 784
      Height = 118
      Align = alClient
      TabOrder = 3
    end
    object Panel5: TPanel
      Left = 0
      Top = 92
      Width = 784
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Texto a ser validado:'
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 375
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML 2.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 375
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML 2.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 375
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML 2.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
