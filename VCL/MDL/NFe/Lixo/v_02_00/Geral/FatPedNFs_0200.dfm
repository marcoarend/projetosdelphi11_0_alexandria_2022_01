object FmFatPedNFs_0200: TFmFatPedNFs_0200
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-005 :: Impress'#227'o de Nota Fiscal'
  ClientHeight = 674
  ClientWidth = 1014
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 1014
    Height = 518
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 266
      Width = 1014
      Height = 84
      Align = alBottom
      DataSource = DsFatPedNFs
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Filial'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieNFTxt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'S'#233'rie NF'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NumeroNF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'N'#250'mero'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTEMISSNF_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Emiss'#227'o NF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTENTRASAI_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Data sa'#237'da'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataAlt_TXT'
          Title.Caption = 'Altera'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FreteVal'
          Title.Caption = '$ Frete'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Seguro'
          Title.Caption = '$ Seguro'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Outros'
          Title.Caption = '$ Desp. Acess.'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CFOP1'
          Title.Caption = 'CFOP 1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Status'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'infProt_cStat'
          Title.Caption = 'Cria'#231#227'o NF-e'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'infCanc_cStat'
          Title.Caption = 'Cancel. NF-e'
          Width = 68
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1014
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label10: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object dmkLabel1: TdmkLabel
        Left = 92
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Empresa (obrigat'#243'rio):'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 544
        Top = 4
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Nota Fiscal:'
        Visible = False
        UpdType = utYes
        SQLType = stNil
      end
      object Label14: TLabel
        Left = 8
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object SpeedButton1: TSpeedButton
        Left = 868
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object EdPedido: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPedidoChange
      end
      object EdFilial: TdmkEditCB
        Left = 92
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 148
        Top = 20
        Width = 393
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsFiliais
        TabOrder = 2
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSerieNF: TdmkEditCB
        Left = 544
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSerieNFChange
        DBLookupComboBox = CBSerieNF
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSerieNF: TdmkDBLookupComboBox
        Left = 600
        Top = 20
        Width = 124
        Height = 21
        KeyField = 'Controle'
        ListField = 'SerieNF'
        ListSource = DsParamsNFs
        TabOrder = 4
        Visible = False
        dmkEditCB = EdSerieNF
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 60
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 801
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DmPediVda.DsClientes
        TabOrder = 6
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object CkSemNFe: TCheckBox
        Left = 732
        Top = 24
        Width = 133
        Height = 17
        Caption = 'Somente NFs sem NF-e.'
        TabOrder = 7
        OnClick = CkSemNFeClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 895
        Top = 41
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 8
        OnClick = BtGraGruNClick
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 350
      Width = 1014
      Height = 168
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel3: TPanel
        Left = 221
        Top = 0
        Width = 520
        Height = 168
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 518
          Height = 168
          Align = alLeft
          Caption = ' Dados da Nota Fiscal selecionada: '
          TabOrder = 0
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 514
            Height = 44
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label4: TLabel
              Left = 8
              Top = 4
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object Label5: TLabel
              Left = 98
              Top = 4
              Width = 41
              Height = 13
              Caption = 'Esp'#233'cie:'
            end
            object Label6: TLabel
              Left = 188
              Top = 4
              Width = 33
              Height = 13
              Caption = 'Marca:'
            end
            object Label7: TLabel
              Left = 278
              Top = 4
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
            end
            object Label8: TLabel
              Left = 368
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Peso buto:'
            end
            object Label9: TLabel
              Left = 440
              Top = 4
              Width = 62
              Height = 13
              Caption = 'Peso l'#237'quido:'
            end
            object DBEdit7: TDBEdit
              Left = 8
              Top = 20
              Width = 86
              Height = 21
              DataField = 'Quantidade'
              DataSource = DsFatPedNFs
              TabOrder = 0
            end
            object DBEdit4: TDBEdit
              Left = 98
              Top = 20
              Width = 86
              Height = 21
              DataField = 'Especie'
              DataSource = DsFatPedNFs
              TabOrder = 1
            end
            object DBEdit5: TDBEdit
              Left = 188
              Top = 20
              Width = 86
              Height = 21
              DataField = 'Marca'
              DataSource = DsFatPedNFs
              TabOrder = 2
            end
            object DBEdit6: TDBEdit
              Left = 278
              Top = 20
              Width = 86
              Height = 21
              DataField = 'Numero'
              DataSource = DsFatPedNFs
              TabOrder = 3
            end
            object DBEdit8: TDBEdit
              Left = 368
              Top = 20
              Width = 68
              Height = 21
              DataField = 'kgBruto'
              DataSource = DsFatPedNFs
              TabOrder = 4
            end
            object DBEdit9: TDBEdit
              Left = 440
              Top = 20
              Width = 68
              Height = 21
              DataField = 'kgLiqui'
              DataSource = DsFatPedNFs
              TabOrder = 5
            end
          end
          object GroupBox2: TGroupBox
            Left = 2
            Top = 59
            Width = 514
            Height = 100
            Align = alTop
            Caption = ' Ve'#237'culo: '
            TabOrder = 1
            object Label11: TLabel
              Left = 12
              Top = 16
              Width = 30
              Height = 13
              Caption = 'Placa:'
            end
            object Label12: TLabel
              Left = 96
              Top = 16
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label13: TLabel
              Left = 272
              Top = 16
              Width = 183
              Height = 13
              Caption = 'Observa'#231#245'es: (N'#227'o '#233' impresso na NF!)'
            end
            object Label16: TLabel
              Left = 128
              Top = 16
              Width = 33
              Height = 13
              Caption = 'RNTC:'
              FocusControl = DBEdit13
            end
            object Label17: TLabel
              Left = 12
              Top = 56
              Width = 187
              Height = 13
              Caption = 'UF e local de embarque de exporta'#231#227'o:'
              FocusControl = DBEdit14
            end
            object DBEdit10: TDBEdit
              Left = 272
              Top = 32
              Width = 237
              Height = 21
              DataField = 'Observacao'
              DataSource = DsFatPedNFs
              TabOrder = 0
            end
            object DBEdit11: TDBEdit
              Left = 96
              Top = 32
              Width = 28
              Height = 21
              DataField = 'PlacaUF'
              DataSource = DsFatPedNFs
              TabOrder = 1
            end
            object DBEdit12: TDBEdit
              Left = 12
              Top = 32
              Width = 80
              Height = 21
              DataField = 'PlacaNr'
              DataSource = DsFatPedNFs
              TabOrder = 2
            end
            object DBEdit13: TDBEdit
              Left = 128
              Top = 32
              Width = 140
              Height = 21
              DataField = 'RNTC'
              DataSource = DsFatPedNFs
              TabOrder = 3
            end
            object DBEdit14: TDBEdit
              Left = 12
              Top = 72
              Width = 30
              Height = 21
              DataField = 'UFembarq'
              DataSource = DsFatPedNFs
              TabOrder = 4
            end
            object DBEdit15: TDBEdit
              Left = 44
              Top = 72
              Width = 465
              Height = 21
              DataField = 'xLocEmbarq'
              DataSource = DsFatPedNFs
              TabOrder = 5
            end
          end
        end
      end
      object Panel8: TPanel
        Left = 741
        Top = 0
        Width = 273
        Height = 168
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object DBMemo2: TDBMemo
          Left = 0
          Top = 0
          Width = 273
          Height = 168
          Align = alClient
          DataField = 'infAdic_infCpl'
          DataSource = DsFatPedNFs
          TabOrder = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 221
        Height = 168
        Align = alLeft
        Caption = ' Dados do Modelo de Nota Fiscal: '
        TabOrder = 2
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 217
          Height = 68
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 128
            Top = 4
            Width = 45
            Height = 13
            Caption = 'N'#186' Limite:'
            FocusControl = DBEdit3
          end
          object Label2: TLabel
            Left = 60
            Top = 4
            Width = 61
            Height = 13
            Caption = #218'lt. NF emit.:'
            FocusControl = DBEdit2
          end
          object Label15: TLabel
            Left = 152
            Top = 51
            Width = 59
            Height = 13
            Caption = 'Status NF-e:'
          end
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 44
            Height = 13
            Caption = 'S'#233'rie NF:'
            FocusControl = DBEdit1
          end
          object Label18: TLabel
            Left = 4
            Top = 48
            Width = 29
            Height = 13
            Caption = 'IDCtrl:'
            FocusControl = DBEdit16
          end
          object DBEdit3: TDBEdit
            Left = 128
            Top = 20
            Width = 64
            Height = 21
            DataField = 'MaxSeqLib'
            DataSource = DsImprime
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 60
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Sequencial'
            DataSource = DsImprime
            TabOrder = 1
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 20
            Width = 53
            Height = 21
            DataField = 'NO_SerieNF'
            DataSource = DsImprime
            TabOrder = 2
          end
          object DBEdit16: TDBEdit
            Left = 36
            Top = 44
            Width = 88
            Height = 21
            DataField = 'IDCtrl'
            DataSource = DsNFeCabA
            TabOrder = 3
          end
        end
        object DBMemo1: TDBMemo
          Left = 2
          Top = 83
          Width = 217
          Height = 83
          Align = alClient
          DataField = 'cStat_xMotivo'
          DataSource = DsNFeCabA
          TabOrder = 1
        end
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 88
      Width = 1014
      Height = 178
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Encerrou'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLI'
          Title.Caption = 'Nome cliente'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAEMISS_TXT'
          Title.Caption = 'Dta emiss'#227'o'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAENTRA_TXT'
          Title.Caption = 'Dta entrada'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAINCLU_TXT'
          Title.Caption = 'Dta inclus'#227'o'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAPREVI_TXT'
          Title.Caption = 'Dta previs'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedidoCli'
          Title.Caption = 'Pedido cliente'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotalFatur'
          Title.Caption = 'Valor L'#237'quido'
          Width = 72
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFatPedCab
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Encerrou'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLI'
          Title.Caption = 'Nome cliente'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAEMISS_TXT'
          Title.Caption = 'Dta emiss'#227'o'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAENTRA_TXT'
          Title.Caption = 'Dta entrada'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAINCLU_TXT'
          Title.Caption = 'Dta inclus'#227'o'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTAPREVI_TXT'
          Title.Caption = 'Dta previs'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PedidoCli'
          Title.Caption = 'Pedido cliente'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotalFatur'
          Title.Caption = 'Valor L'#237'quido'
          Width = 72
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 966
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 50
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbNFePesq: TBitBtn
        Tag = 387
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbNFePesqClick
      end
    end
    object GB_M: TGroupBox
      Left = 50
      Top = 0
      Width = 916
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 310
        Height = 32
        Caption = 'Impress'#227'o de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 310
        Height = 32
        Caption = 'Impress'#227'o de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 310
        Height = 32
        Caption = 'Impress'#227'o de Nota Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1014
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 1010
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 381
        Height = 16
        Caption = 
          'Para selecionar o cliente clique na coluna Cliente da grade abai' +
          'xo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 381
        Height = 16
        Caption = 
          'Para selecionar o cliente clique na coluna Cliente da grade abai' +
          'xo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 610
    Width = 1014
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1010
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 866
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 270
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtNFe: TBitBtn
        Tag = 5
        Left = 145
        Top = 3
        Width = 120
        Height = 40
        Caption = '&NF-e'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNFeClick
      end
      object BtGeraNF: TBitBtn
        Tag = 14
        Left = 19
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Dados NF'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtGeraNFClick
      end
    end
  end
  object QrFatPedNFs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFatPedNFsBeforeClose
    AfterScroll = QrFatPedNFsAfterScroll
    OnCalcFields = QrFatPedNFsCalcFields
    SQL.Strings = (
      
        'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, nfe.' +
        'infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.*'
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 108
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFsFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFsNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFsIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFsSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFsSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFsCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFsPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFsEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFsMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFsNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFskgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFskgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFsQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFsObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFsCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFsDtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Origin = 'stqmovnfsa.DtEmissNF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Origin = 'stqmovnfsa.DtEntraSai'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrFatPedNFsinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrFatPedNFsinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      Origin = 'stqmovnfsa.infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFatPedNFsinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrFatPedNFsDTEMISSNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTEMISSNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsDTENTRASAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTENTRASAI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsRNTC: TWideStringField
      FieldName = 'RNTC'
      Origin = 'stqmovnfsa.RNTC'
    end
    object QrFatPedNFsUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Origin = 'stqmovnfsa.UFembarq'
      Size = 2
    end
    object QrFatPedNFsxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Origin = 'stqmovnfsa.xLocEmbarq'
      Size = 60
    end
    object QrFatPedNFside_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrFatPedNFsHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Origin = 'stqmovnfsa.HrEntraSai'
    end
    object QrFatPedNFside_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
      Origin = 'stqmovnfsa.ide_dhCont'
    end
    object QrFatPedNFside_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Origin = 'stqmovnfsa.ide_xJust'
      Size = 255
    end
    object QrFatPedNFsemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Origin = 'stqmovnfsa.emit_CRT'
    end
    object QrFatPedNFsdest_email: TWideStringField
      FieldName = 'dest_email'
      Origin = 'stqmovnfsa.dest_email'
      Size = 60
    end
    object QrFatPedNFsvagao: TWideStringField
      FieldName = 'vagao'
      Origin = 'stqmovnfsa.vagao'
    end
    object QrFatPedNFsbalsa: TWideStringField
      FieldName = 'balsa'
      Origin = 'stqmovnfsa.balsa'
    end
    object QrFatPedNFsCabA_FatID: TIntegerField
      FieldName = 'CabA_FatID'
      Origin = 'nfecaba.FatID'
      Required = True
    end
    object QrFatPedNFsCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'stqmovnfsa.Compra_XNEmp'
      Size = 17
    end
    object QrFatPedNFsCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'stqmovnfsa.Compra_XPed'
      Size = 60
    end
    object QrFatPedNFsCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'stqmovnfsa.Compra_XCont'
      Size = 60
    end
    object QrFatPedNFsHrEmi: TTimeField
      FieldName = 'HrEmi'
    end
  end
  object DsFatPedNFs: TDataSource
    DataSet = QrFatPedNFs
    Left = 108
    Top = 272
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF NO_SerieNF, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprimeempr ime'
      'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=ime.ParamsNFs'
      'WHERE ime.Empresa=:P0'
      'AND ime.Codigo=:P1')
    Left = 184
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
    end
    object QrImprimeNO_SerieNF: TIntegerField
      FieldName = 'NO_SerieNF'
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 184
    Top = 272
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Associada, AssocModNF, NFetpEmis, CRT, NFeNT2013_003LTT'
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 260
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpNFetpEmis: TSmallintField
      FieldName = 'NFetpEmis'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrParamsEmpNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
    end
  end
  object PMGeraNF: TPopupMenu
    OnPopup = PMGeraNFPopup
    Left = 200
    Top = 524
    object GeraNF1: TMenuItem
      Caption = '&Gera n'#250'mero NF'
      Enabled = False
      OnClick = GeraNF1Click
    end
    object AlteradadosNF1: TMenuItem
      Caption = '&Dados NF'
      Enabled = False
      OnClick = AlteradadosNF1Click
    end
    object AlteradadosNFe1: TMenuItem
      Caption = '&Edita NF-e'
      OnClick = AlteradadosNFe1Click
    end
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(OriCnta) Qtde'
      'FROM stqmovitsa smi'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      ''
      '/*'
      'UNION'
      ''
      'SELECT COUNT(OriCnta) Qtde'
      'FROM stqmovitsb smi'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P1'
      '*/')
    Left = 328
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalQtde: TLargeintField
      FieldName = 'Qtde'
      Required = True
    end
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Filial, ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL'
      'FROM entidades ent'
      'WHERE ent.Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 428
    Top = 224
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 428
    Top = 272
  end
  object QrParamsNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsnfs'
      'WHERE Codigo=:P0')
    Left = 496
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsNFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsNFsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsNFsSequencial: TIntegerField
      FieldName = 'Sequencial'
    end
    object QrParamsNFsSerieNF: TIntegerField
      FieldName = 'SerieNF'
    end
  end
  object DsParamsNFs: TDataSource
    DataSet = QrParamsNFs
    Left = 496
    Top = 272
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFatPedCabBeforeClose
    AfterScroll = QrFatPedCabAfterScroll
    OnCalcFields = QrFatPedCabCalcFields
    SQL.Strings = (
      'SELECT ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou,'
      'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP,'
      'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI,'
      'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI,'
      'frc.ModeloNF, frc.infAdFisco, frc.Financeiro,'
      'frc.TipoMov tpNF, nfe.ICMSTot_vProd, pvd.*'
      'FROM fatpedcab fpc'
      
        'LEFT JOIN nfecaba nfe ON nfe.FatNum = fpc.Codigo AND  nfe.FatID ' +
        '= 1'
      'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg'
      'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF)'
      'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF)'
      'WHERE fpc.Encerrou > 0'
      'ORDER BY Encerrou DESC')
    Left = 28
    Top = 224
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
    end
    object QrFatPedCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrFatPedCabCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
    end
    object QrFatPedCabUF_TXT_emp: TWideStringField
      FieldName = 'UF_TXT_emp'
      Required = True
      Size = 2
    end
    object QrFatPedCabUF_TXT_cli: TWideStringField
      FieldName = 'UF_TXT_cli'
      Required = True
      Size = 2
    end
    object QrFatPedCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrFatPedCabinfAdFisco: TWideStringField
      FieldName = 'infAdFisco'
      Size = 255
    end
    object QrFatPedCabFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFatPedCabTotalFatur: TFloatField
      FieldName = 'TotalFatur'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFatPedCabtpNF: TSmallintField
      FieldName = 'tpNF'
    end
    object QrFatPedCabDTAEMISS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAEMISS_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAENTRA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAENTRA_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAINCLU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAINCLU_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAPREVI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAPREVI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
    end
    object QrFatPedCabDtaEntra: TDateField
      FieldName = 'DtaEntra'
    end
    object QrFatPedCabDtaInclu: TDateField
      FieldName = 'DtaInclu'
    end
    object QrFatPedCabDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
    end
    object QrFatPedCabFretePor: TIntegerField
      FieldName = 'FretePor'
    end
    object QrFatPedCabModeloNF: TFloatField
      FieldName = 'ModeloNF'
    end
    object QrFatPedCabRegrFiscal: TFloatField
      FieldName = 'RegrFiscal'
    end
    object QrFatPedCabCartEmis: TFloatField
      FieldName = 'CartEmis'
    end
    object QrFatPedCabTabelaPrc: TFloatField
      FieldName = 'TabelaPrc'
    end
    object QrFatPedCabCondicaoPG: TFloatField
      FieldName = 'CondicaoPG'
    end
    object QrFatPedCabTransporta: TFloatField
      FieldName = 'Transporta'
    end
    object QrFatPedCabEmpresa: TFloatField
      FieldName = 'Empresa'
    end
    object QrFatPedCabCliente: TFloatField
      FieldName = 'Cliente'
    end
  end
  object DsFatPedCab: TDataSource
    DataSet = QrFatPedCab
    Left = 28
    Top = 272
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 568
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 568
    Top = 272
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 248
    Top = 524
    object RecriatodaNFe1: TMenuItem
      Caption = '&Recria toda NFe'
      OnClick = RecriatodaNFe1Click
    end
    object Continuaprocessonormaldecriao1: TMenuItem
      Caption = '&Continua processo normal de cria'#231#227'o'
      OnClick = Continuaprocessonormaldecriao1Click
    end
    object ApenasGeraXML1: TMenuItem
      Caption = 'Atualiza &XML NFe'
      OnClick = ApenasGeraXML1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object este1: TMenuItem
      Caption = 'NF-e '#218'nica (Gera, envia e imprime)'
      OnClick = este1Click
    end
  end
  object QrCTG: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Empresa=0, 1, 0) ORDEM,  '
      'tpEmis, dhEntrada, Nome'
      'FROM nfecntngnc'
      'WHERE dhSaida < 2'
      'ORDER BY ORDEM, dhEntrada DESC')
    Left = 376
    Top = 224
    object QrCTGORDEM: TLargeintField
      FieldName = 'ORDEM'
      Required = True
    end
    object QrCTGtpEmis: TIntegerField
      FieldName = 'tpEmis'
    end
    object QrCTGdhEntrada: TDateTimeField
      FieldName = 'dhEntrada'
    end
    object QrCTGNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
end
