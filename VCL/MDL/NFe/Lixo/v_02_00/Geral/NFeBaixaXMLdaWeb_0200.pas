unit NFeBaixaXMLdaWeb_0200;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math, WinInet,
  Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB,
  // Cabecalho
  //cabecMsg_v102,
  // Consulta status servi�o
  consStatServ_v200,
  // Consulta situa��o NF-e
  consSitNFe_v200,
  // Pede inutiliza��o NF-e
  inutNFe_v200,
  // Gera NFe
  nfe_v200,
  // Envia lote de NF-es
  // ???
  // Consulta Lote de NF-es
  consReciNFe_v200,
  // Cancela NF-e
  cancNFe_v200,
  //
  XSBuiltIns, StrUtils, ComObj, SoapConst, mySQLDbTables;

type
  TNFeBaixaXMLdaWeb_0200 = class(TObject)
  private
    { Private declarations }
    procedure AvisaTextoNaoProcessado(MyID_Loc: String; Grupo, Texto: String);
    function Converte_NumBr_To_NumNFe(Valor: String): String; //ConverteStrToNumero
    function Converte_DtaBr_to_DtaNFe(Valor, Sep: String; TamYear: Integer): String;
    function Converte_DtaBr_to_DtaHra(Valor: String; TamYear: Integer): TDateTime;
    function DefValDeText(var Campo: String; var Texto:
             String; const NomeCampo: string;
             const Tamanho: Integer = 0): Boolean;
    // N�o uso! function LastPos(SubStr, Str: String): Integer;
    function LerCampoA(var Texto: String;
             const NomeCampo: string; const Tamanho: Integer): string;
    function SeparaApartirDe(Chave, Texto: String; Avisa: Boolean): String;
    function SeparaAte(Chave, Texto: String;
             var Resto: String; Avisa: Boolean): String;
    function RemoveGrupo(Chave, Texto: String;
             var Grupo, Restante: String; Avisa: Boolean): Boolean;
    function RetiraTag(Texto, Tag: String): String;
    function RetiraTxt(Texto, Txt: String): String;
    function RetornarCodigoNumerico(Chave, Versao: String): String;
    function CasaCamposEValores(const Itens: Integer;const Campos: String;
             var Texto: String): String;

    //
  public
    function GerarXML_Da_WEB_2(const Arquivo: String;
             var XML_Prot: String; var ChaveDeAcesso: String;
             var Empresa: Integer; var Status: Integer;
             var VersaoNFe: Double; var ReciAmbNacional: TDateTime): Boolean;
    function RecuperarXMLdaWeb(TextoHTML: String): String;
    //

  end;

var
  UnNFeBaixaXMLdaWeb_0200: TNFeBaixaXMLdaWeb_0200;

implementation

uses NFeXMLGerencia, ModuleNFE_0000, UnInternalConsts, ModuleGeral;

const
  sCampoNulo = '#NULL#';
  sPipe = '|&|';
  //
  // Fisco
  sWeb_ObsCont = 'OBSERVA��ES DO CONTRIBUINTE ';
  sWeb_ObsFisco = 'OBSERVA��ES DO FISCO ';
  sWeb_infCpl = 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO CONTRIBUINTE ';
  sWeb_infAdFisco = 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO FISCO ';
  sWeb_infAdic = 'INFORMA��ES ADICIONAIS ';
  // Cobran�a
  sWeb_dup = 'DUPLICATAS';
  sWeb_fat = 'FATURA';
  sWeb_cobr = 'DADOS DE COBRAN�A';
  // Transporte
  sWeb_vol = 'VOLUMES ';
  sWeb_transporta = 'TRANSPORTADOR ';
  sWeb_transp = 'DADOS DO TRANSPORTE';
  // Exporta��o ???
  sWeb_Exporta = 'EXPORTA��O ';
  // Totais da NFe
  sWeb_ICMStot = 'ICMS ';
  sWeb_total = 'TOTAIS';
  // Produtos e Servi�os
  sWeb_det = 'DADOS DOS PRODUTOS E SERVI�OS ';
  sTit_det = 'Num.|&|Descri��o|&|Qtd.|&|Unidade Comercial|&|Valor(R$)';
  sWeb_ICMS = 'ICMS NORMAL e ST ';
  sWeb_IPI = 'IMPOSTO SOBRE PRODUTOS INDUSTRIALIZADOS ';
  sWeb_DI = 'DECLARA��O DE IMPORTA��O ';
  sWeb_nDI = 'DOCUMENTO DE IMPORTA��O ';
  sWeb_adi = 'ADI��ES DA DI ';

  //
  sWeb_xx1 = '      $(''.oculta'').hide();|&|'
           + '      $(this).parent().parent().children(''label'').toggle();|&|'
           + '      $(this).parent().parent().parent().parent().parent().next().toggle();';
  //
  // Cabe�alho da NFe
  sWeb_entrega = 'LOCAL DE ENTREGA ';
  sWeb_retirada = 'LOCAL DE RETIRADA ';
  sWeb_dest = 'DADOS DO DESTINAT�RIO';
  sWeb_remet = 'DADOS DO REMETENTE ';
  sWeb_emit = 'DADOS DO EMITENTE';
  sWeb_ide = 'EMISS�O ';
  //
  // Cabe�alho na Web
  sWeb_CabD = 'DESTINAT�RIO ';
  sWeb_CabE = 'EMITENTE ';
  sWeb_CabI = 'DADOS DA NF-E ';
  sWeb_NFeC = 'CONSULTAR NF-E COMPLETA ';
  sTit_NFeC = 'NFeEmitenteDestinat�rioProdutos/Servi�osTotaisCom�rcio ExteriorTransporteCobran�aInf. AdicionaisAvulsa |&|';
var
{
  FGravaCampo: Integer;
  FLabel1: TLabel;
  FdocXML: TXMLDocument;
  FCabecTxt, FDadosTxt, FAssinTxt: String;
  //FWSDL,
  FURL: String;
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;
  //
  NomeArquivo: String;
  CaminhoArquivo: String;
  strTpAmb: string;
  SerieNF: String;
  NumeroNF: String;
  CDV: String;
}
  (* Objetos do Documento XML... *)
  arqXML: TXMLDocument;
  cXML: IXMLTNFe;
  cDetLista: IXMLDet;
  cRefLista: IXMLNFref;
  cRebLista: IXMLTVeiculo;
  cVolLista: IXMLVol;
  cLacLista: IXMLLacres;
  cDupLista: IXMLDup;
  cProcRefLista: IXMLProcRef;
  cDILista: IXMLDI;
  cAdiLista: IXMLAdi;
  cMedLista: IXMLMed;
  cArmLista: IXMLArma;
  cObsCont: IXMLObsCont;
  cObsFisco: IXMLObsFisco;

{
  FFatID,
  FFatNum,
  FEmpresa,
  FOrdem: Integer;
}

{ TNFeBaixaXMLdaWeb_0200 }

procedure TNFeBaixaXMLdaWeb_0200.AvisaTextoNaoProcessado(MyID_Loc: String;
  Grupo, Texto: String);
var
  Txt: String;
begin
//  Txt := Geral.Substitui(Texto, '|&|', '|&|'#13#10);
  Txt := StringReplace(Texto, '|&|', '|&|'#13#10, [rfReplaceAll]);
  //
  Geral.MensagemBox('O texto abaixo n�o foi processado:' + #13#10 +
  '------------------- IDENTIFICA��O: ------------------------------' + #13#10 +
  'Grupo: "' + Grupo + '"' + #13#10 + 'ID: ' + MyID_Loc + #13#10 +
  '------------------- TEXTO N�O PROCESSADO: -----------------------' + #13#10 +
  '"' + Txt + '"', 'AVISE A DERMATEK', MB_OK+MB_ICONERROR);
end;

function TNFeBaixaXMLdaWeb_0200.CasaCamposEValores(const Itens: Integer;
  const Campos: String; var Texto: String): String;
var
  Lista: array of String;
  Txt: String;
  I, K: Integer;
begin
  SetLength(Lista, Itens * 2);
  Txt := Texto;
  I := 0;
  K := pos(sPipe, Txt);
  while K > 0 do
  begin
    Lista[I] := copy(Txt, 1, K - 1);
    Txt := copy(Txt, K + 3);
    //
    I := I + 1;
    K := pos(sPipe, Txt);
  end;
  if Txt = '' then
  begin
    Texto := '';
    for I := 0 to Itens - 1 do
      Result := Result + Lista[I] + Lista[I + Itens] + sPipe;
  end else Result := '';
end;

function TNFeBaixaXMLdaWeb_0200.Converte_DtaBr_to_DtaNFe(Valor, Sep: String;
  TamYear: Integer): String;
var
  K: Integer;
  Dta, Hra: String;
  DataHora: TDateTime;
  Ano, Mes, Dia: Word;
begin
  Dta := Copy(Valor, 1, TamYear + 6);
  K := Length(Valor);
  if K >= 16 then
    Hra := sep + Trim(Copy(Valor, k - 8))
  else
    Hra := '';
  //
  Ano := Geral.IMV(Copy(Dta, 7, TamYear));
  Mes := Geral.IMV(Copy(Dta, 4, 2));
  Dia := Geral.IMV(Copy(Dta, 1, 2));
  DataHora := EncodeDate(Ano, Mes, Dia);
  Result := FormatDateTime('yyyy-mm-dd', DataHora) + Hra;
end;

function TNFeBaixaXMLdaWeb_0200.Converte_DtaBr_to_DtaHra(Valor: String;
  TamYear: Integer): TDateTime;
var
  K: Integer;
  Dta: String;
  Hra: TTime;
  DataHora: TDateTime;
  Ano, Mes, Dia: Word;
begin
  Dta := Copy(Valor, 1, TamYear + 6);
  K := Length(Valor);
  if K >= 16 then
    Hra := StrToTime(Trim(Copy(Valor, k - 8)))
  else
    Hra := 0;
  //
  Ano := Geral.IMV(Copy(Dta, 7, TamYear));
  Mes := Geral.IMV(Copy(Dta, 4, 2));
  Dia := Geral.IMV(Copy(Dta, 1, 2));
  DataHora := EncodeDate(Ano, Mes, Dia);
  Result := DataHora + Hra;
end;

function TNFeBaixaXMLdaWeb_0200.Converte_NumBr_To_NumNFe(Valor: String): String;
begin
  Result := Geral.SoNumeroEVirgula_TT(Valor);
  //Result := Geral.Substitui(Result, '.', '');
  Result := Geral.Substitui(Result, ',', '.');
  if Result = '' then
    Result := '0';
  //ShowMessage(Valor + ' --> ' + Result);
end;

function TNFeBaixaXMLdaWeb_0200.DefValDeText(var Campo: String;
  var Texto: String; const NomeCampo: string;
  const Tamanho: Integer): Boolean;
var
  Valor: String;
begin
  Result := False;
  //
  Valor := LerCampoA(Texto, NomeCampo, Tamanho);
  if (Valor = sCampoNulo) or (Valor = '') then
    Exit
  else begin
    Campo := Valor;
    Result := True;
  end;
end;

function TNFeBaixaXMLdaWeb_0200.GerarXML_Da_WEB_2(const Arquivo: String;
  var XML_Prot: String; var ChaveDeAcesso: String; var Empresa,
  Status: Integer; var VersaoNFe: Double; var ReciAmbNacional: TDateTime): Boolean;
const
  MinItens = 1;
  MaxItens = 990;
var
  Txt_ObsCont, Txt_ObsFisco, Txt_infCpl, Txt_infAdFisco, Txt_infAdic,
  Txt_dup, Txt_fat, Txt_cobr, Txt_vol, Txt_transporta, Txt_transp, Txt_ICMSTot,
  Txt_total, Txt_det, Txt_prod, Txt_DI, Txt_nDI, Txt_adi,
  Txt_ICMS, Txt_IPI, Txt_entrega, Txt_retirada, Txt_xxx, Txt_dest, Txt_remet,
  Txt_emit, Txt_ide, Txt_CabD, Txt_CabE, Txt_CabI, Txt_NFeC, Txt_Exporta1,
  Txt_Exporta2, ArquivoTXT, ArquivoRestante: String;
  I, J, H, J1, J2, Produtos: Integer;
  Valor: String;
  S, Texto, Multipl, Orig, CST, CSOSN: String;
  //Itens: array [MinItens..MaxItens] of String;
  digVal, nProt, dhRecbto, xMotivo, cStat: String;
  sDataEmissao, dData, Versao, Campos, Valores: String;
begin
  Result := False;

  ChaveDeAcesso := '';
  Empresa := 0;
  Status := 0;
  VersaoNFe := 0;

  ArquivoTXT := StringReplace(Arquivo,#$D#$A,'|&|',[rfReplaceAll]);
  // fazer primeiro ...
  ArquivoTxt := SeparaApartirDe('|&|Voc� est� aqui: |&|', ArquivoTxt, True);
  // porque tem mais de um "Consultar NF-e Completa"
  ArquivoTxt := SeparaApartirDe('|&|Consultar NF-e Completa |&|', ArquivoTxt, True);

  ArquivoTxt := SeparaAte('|&|Conhe�a a NF-e |', ArquivoTxt, ArquivoTxt, True) + '|&|';

  // Separar do final para o in�cio
  // 'OBSERVA��ES DO CONTRIBUINTE '
  RemoveGrupo(sWeb_ObsCont, ArquivoTxt, Txt_ObsCont, ArquivoTxt, False);
  // 'OBSERVA��ES DO FISCO |&|';
  RemoveGrupo(sWeb_ObsFisco, ArquivoTxt, Txt_ObsFisco, ArquivoTxt, False);
  // 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO CONTRIBUINTE |&|';
  RemoveGrupo(sWeb_infCpl, ArquivoTxt, Txt_infCpl, ArquivoTxt, False);
  // 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO FISCO |&|';
  RemoveGrupo(sWeb_infAdFisco, ArquivoTxt, Txt_infAdFisco, ArquivoTxt, False);
  // INFORMA��ES ADICIONAIS
  RemoveGrupo(sWeb_infAdic, ArquivoTxt, Txt_infAdic, ArquivoTxt, False);
  // DUPLICATAS
  RemoveGrupo(sWeb_dup, ArquivoTxt, Txt_dup, ArquivoTxt, False);
  // 'FATURA|&|';
  RemoveGrupo(sWeb_fat, ArquivoTxt, Txt_fat, ArquivoTxt, False);
  //'DADOS DE COBRAN�A'
  RemoveGrupo(sWeb_cobr, ArquivoTxt, Txt_cobr, ArquivoTxt, False);
  // 'VOLUMES '
  RemoveGrupo(sWeb_vol, ArquivoTxt, Txt_vol, ArquivoTxt, False);
  // 'TRANSPORTADOR ';
  RemoveGrupo(sWeb_transporta, ArquivoTxt, Txt_transporta, ArquivoTxt, False);
  // 'DADOS DO TRANSPORTE';
  RemoveGrupo(sWeb_transp, ArquivoTxt, Txt_transp, ArquivoTxt, False);
  // 'EXPORTA��O ';
  RemoveGrupo(sWeb_Exporta, ArquivoTxt, Txt_Exporta2, ArquivoTxt, False);
  // 'TOTAIS DE ICMS';
  RemoveGrupo(sWeb_ICMSTot, ArquivoTxt, Txt_ICMSTot, ArquivoTxt, False);
  // 'TOTAIS'
  if RemoveGrupo(sWeb_total, ArquivoTxt, Txt_total, ArquivoTxt, False) then
  begin
    Txt_total := RetiraTag(Txt_total, sWeb_total);

    //
    if Trim(Txt_total) <> '' then
      AvisaTextoNaoProcessado('326 # W01', sWeb_total, Txt_total);
  end;
  // 'DADOS DOS PRODUTOS E SERVI�OS '
  RemoveGrupo(sWeb_det, ArquivoTxt, Txt_det, ArquivoTxt, False);
  // Lixo de java script
  if RemoveGrupo(sWeb_xx1, ArquivoTxt, Txt_xxx, ArquivoTxt, False) then
  begin
    Txt_xxx := RetiraTag(Txt_xxx, sWeb_xx1);

    //
    if Trim(Txt_xxx) <> '' then
      AvisaTextoNaoProcessado('00 # XX1', sWeb_xx1, Txt_xxx);
  end;

  // 'LOCAL DE ENTREGA '
  RemoveGrupo(sWeb_entrega, ArquivoTxt, Txt_entrega, ArquivoTxt, False);
  // GRUPO INTEIRO N�O TESTADO!!!!!
  // 'LOCAL DE RETIRADA '
  RemoveGrupo(sWeb_retirada, ArquivoTxt, Txt_retirada, ArquivoTxt, False);
  // 'DADOS DO DESTINAT�RIO'
  RemoveGrupo(sWeb_dest, ArquivoTxt, Txt_dest, ArquivoTxt, False);
  // 'DADOS DO REMETENTE'
  RemoveGrupo(sWeb_remet, ArquivoTxt, Txt_remet, ArquivoTxt, False);
  // 'DADOS DO EMITENTE'
  RemoveGrupo(sWeb_emit, ArquivoTxt, Txt_emit, ArquivoTxt, False);
  // 'EMISS�O '
  RemoveGrupo(sWeb_ide, ArquivoTxt, Txt_ide, ArquivoTxt, False);
  //'DESTINAT�RIO '
  RemoveGrupo(sWeb_CabD, ArquivoTxt, Txt_CabD, ArquivoTxt, False);
  //'EMITENTE '
  RemoveGrupo(sWeb_CabE, ArquivoTxt, Txt_CabE, ArquivoTxt, False);
  // 'DADOS DA NF-E ';  Parte 2
  RemoveGrupo(sWeb_CabI, ArquivoTxt, Txt_CabI, ArquivoTxt, False);
  // 'DADOS DA NF-E ';  Parte 1
  RemoveGrupo(sWeb_NFeC, ArquivoTxt, Txt_NFeC, ArquivoTxt, False);


  // 'DADOS DA NF-E ';  Parte 1
  if Length(Txt_NFeC) > 0 then
  begin
    Txt_NFeC := RetiraTag(Txt_NFeC, sWeb_NFeC);
    //

    if DefValDeText(Valor, Txt_NFeC, 'Chave de acesso') then
    begin
      cXML.infNFe.ID := Geral.SoNumero_TT(Valor);
      ChaveDeAcesso := cXML.infNFe.ID;
    end;
    //

    if DefValDeText(Valor, Txt_NFeC, 'N�mero NF-e') then
      cXML.infNFe.Ide.nNF := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_NFeC, 'Vers�o') then
    begin
      Versao := Valor;
      cXML.infNFe.Versao := Versao;
      VersaoNFe := Geral.DMV(Geral.Substitui(Valor, '.', ','));
    end;
    //'NFeEmitenteDestinat�rioProdutos/Servi�osTotaisCom�rcio ExteriorTransporteCobran�aInf. AdicionaisAvulsa |&|'
    //VerificaSeHaTextoExtra(Txt_NFeC, 'NFeEmitenteDestinat�rio', 'Dados da NF-e');

    cXML.infNFe.Ide.cNF := RetornarCodigoNumerico(cXML.infNFe.ID, cXML.infNFe.Versao);
    //
    Txt_NFeC := RetiraTxt(Txt_NFeC, sTit_NFeC);
    if Trim(Txt_NFeC) <> '' then
      AvisaTextoNaoProcessado('0 # ***', 'Chave - vers�o', Txt_NFeC);
  end;



  // 'DADOS DA NF-E ';  Parte 2
  if Length(Txt_CabI) > 0 then
  begin
    Txt_CabI := RetiraTag(Txt_CabI, sWeb_CabI);
    //

    if DefValDeText(Valor, Txt_CabI, 'N�mero') then
      cXML.infNFe.Ide.nNF := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_CabI, 'S�rie') then
      cXML.infNFe.Ide.Serie := Geral.SoNumero_TT(Valor);


    if DefValDeText(Valor, Txt_CabI, 'Data de emiss�o') then
    begin
      sDataEmissao := Valor;

      if Length(sDataEmissao) > 0 then
       dData := Geral.FDT(EncodeDate(StrToInt(copy(sDataEmissao, 07, 4)), StrToInt(copy(sDataEmissao, 04, 2)),
                  StrToInt(copy(sDataEmissao, 01, 2))), 1)
      else
       dData := '0000-00-00';

      cXML.infNFe.Ide.dEmi := dData;
    end;

    if DefValDeText(Valor, Txt_CabI, 'Valor Total da Nota Fiscal') then
      cXML.infNFe.Total.ICMSTot.vNF := Converte_NumBr_To_NumNFe(Valor);

    //
    if Trim(Txt_CabI) <> '' then
      AvisaTextoNaoProcessado('0 # ***', 'Dados NF', Txt_CabI);
  end;

  //'EMITENTE '
  if Length(Txt_CabE) > 0 then
  begin
    Txt_CabE := RetiraTag(Txt_CabE, sWeb_CabE);
    //

    if DefValDeText(Valor, Txt_CabE, 'CNPJ') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Geral.SoNumero_TT(Valor) <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Emit.CNPJ := Valor
        else
          cXML.InfNFe.Emit.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_CabE, 'Nome/Raz�o Social') then
      cXML.infNFe.Emit.xNome := Valor;

    if DefValDeText(Valor, Txt_CabE, 'Inscri��o Estadual') then
      cXML.infNFe.Emit.IE := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_CabE, 'UF') then
      cXML.infNFe.Emit.EnderEmit.UF := Valor;

    //
    if Trim(Txt_CabE) <> '' then
      AvisaTextoNaoProcessado('0 # ***', 'EMITENTE', Txt_CabE);

  end;


  //'DESTINAT�RIO '
  if Length(Txt_CabD) > 0 then
  begin
    Txt_CabD := RetiraTag(Txt_CabD, sWeb_CabD);
    //

    if DefValDeText(Valor, Txt_CabD, 'CNPJ') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Dest.CNPJ := Valor
        else
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_CabD, 'Nome/Raz�o Social') then
      cXML.infNFe.Dest.xNome := Valor;

    if DefValDeText(Valor, Txt_CabD, 'Inscri��o Estadual') then
      cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(Valor);

    //continuar verifica��o de dados e texto que resta

    if DefValDeText(Valor, Txt_CabD, 'UF') then
      cXML.infNFe.Dest.EnderDest.UF := Valor;

    //
    if Trim(Txt_CabD) <> '' then
      AvisaTextoNaoProcessado('0 # ***', 'DESTINAT�RIO', Txt_CabD);

  end;


  // 'EMISS�O '
  if Length(Txt_ide) > 0 then
  begin
    Txt_ide := RetiraTag(Txt_ide, sWeb_ide);
    //

    if DefValDeText(Valor, Txt_ide, 'Processo', 1) then
      cXML.infNFe.Ide.procEmi := Valor;

    if DefValDeText(Valor, Txt_ide, 'Vers�o do Processo') then
      cXML.infNFe.Ide.verProc := Valor;

    if DefValDeText(Valor, Txt_ide, 'Tipo de Emiss�o', 1) then
      cXML.infNFe.ide.tpEmis := Valor;

    if DefValDeText(Valor, Txt_ide, 'Finalidade', 1) then
      cXML.infNFe.ide.FinNFe := Valor;

    if DefValDeText(Valor, Txt_ide, 'Natureza da Opera��o') then
    cXML.infNFe.Ide.natOp := Valor;

    if DefValDeText(Valor, Txt_ide, 'Tipo da Opera��o', 1) then
      cXML.infNFe.ide.tpNF := Valor;

    if DefValDeText(Valor, Txt_ide, 'Forma de Pagamento', 1) then
      cXML.infNFe.ide.indPag := Valor;

    DefValDeText(digVal, Txt_ide, 'Digest Value da NF-e');

    DefValDeText(xMotivo, Txt_ide, 'SITUA��O ATUAL:');

    DefValDeText(xMotivo, Txt_ide, 'Ocorr�ncia');

    DefValDeText(nProt, Txt_ide, 'Protocolo');

    if DefValDeText(dhRecbto, Txt_ide, 'Data/Hora') then
    begin
{
      Ano := Geral.IMV(Copy(dhRecbto, 7, 4));
      Mes := Geral.IMV(Copy(dhRecbto, 4, 2));
      Dia := Geral.IMV(Copy(dhRecbto, 1, 2));
      DataHora := EncodeDate(Ano, Mes, Dia);
      dhRecbto := FormatDateTime('yyyy-mm-dd', DataHora) + 'T' +
                  Copy(dhRecbto, 12);
}
      dhRecbto := Converte_DtaBr_to_DtaNFe(dhRecbto, 'T', 4);
    end;

    if pos('AUTORIZA', AnsiUppercase(xMotivo)) > 0 then
      cStat := '100'
    else
    if pos('CANCELA', AnsiUppercase(xMotivo)) > 0 then
      cStat := '101'
    else
    if pos('DENEGA', AnsiUppercase(xMotivo)) > 0 then
      cStat := '301'
    else
      cStat := '000';
    //
    //SITUA��O ATUAL:
    (*
    cXML.infNFe.proccXML.infNFe.digVal   := LerCampo(Grupo, 'Digest Value da NF-e');
    cXML.infNFe.proccXML.infNFe.xMotivo  := LerCampo(Grupo, 'Ocorr�ncia');
    cXML.infNFe.proccXML.infNFe.nProt    := LerCampo(Grupo, 'Protocolo');
    cXML.infNFe.proccXML.infNFe.dhRecbto := StrToDateDef(LerCampo(Grupo,'Data/Hora'),0);
    *)
    //Recebimento no Ambiente Nacional
    //Recebimento no Amb. Nacional24/08/2011 11:04:18|&|'

    //VerificaSeHaTextoExtra(Grupo, 'Recebimento no Amb. Nacional', 'Dados da Emiss�o');
    if DefValDeText(Valor, Txt_ide, 'Recebimento no Amb. Nacional') then
    begin
{
      Ano := Geral.IMV(Copy(Valor, 7, 4));
      Mes := Geral.IMV(Copy(Valor, 4, 2));
      Dia := Geral.IMV(Copy(Valor, 1, 2));
      ReciAmbNacional := EncodeDate(Ano, Mes, Dia) + StrToTime(Copy(Valor, 12, 8));
}
      ReciAmbNacional := Converte_DtaBr_to_DtaHra(Valor, 4);
    end else
      ReciAmbNacional := 0;
    //
    //
    if Trim(Txt_ide) <> '' then
      AvisaTextoNaoProcessado('5 # B01', sWeb_ide, Txt_ide);
  end;

  // 'DADOS DO EMITENTE'
  if sWeb_emit <> '' then
  begin
    Txt_emit := RetiraTag(Txt_emit, sWeb_emit);
    //

    if DefValDeText(Valor, Txt_emit, 'Nome / Raz�o Social') then
      cXML.infNFe.Emit.xNome := Valor;

    if DefValDeText(Valor, Txt_emit, 'Nome Fantasia') then
      cXML.infNFe.Emit.xFant := Valor;

    if DefValDeText(Valor, Txt_emit, 'CNPJ') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Emit.CNPJ := Valor
        else
          cXML.InfNFe.Emit.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_emit, 'Endere�o') then
      cXML.infNFe.Emit.EnderEmit.xLgr := Valor;

    if DefValDeText(Valor, Txt_emit, 'Bairro/Distrito') then
      cXML.infNFe.Emit.EnderEmit.xBairro := Valor;

    if DefValDeText(Valor, Txt_emit, 'CEP') then
      cXML.infNFe.Emit.EnderEmit.CEP := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_emit, 'Munic�pio') then
    begin
      cXML.infNFe.Emit.EnderEmit.cMun := Geral.SoNumero_TT(Copy(Valor, 1, 7));

      cXML.infNFe.Ide.cUF := Geral.SoNumero_TT(Copy(Valor, 1, 2));

      cXML.infNFe.Emit.EnderEmit.xMun := copy(Valor, 10, 60);
    end;

    if DefValDeText(Valor, Txt_emit, 'Fone/Fax') then
      cXML.infNFe.Emit.EnderEmit.fone := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_emit, 'UF') then
      cXML.infNFe.Emit.EnderEmit.UF := Valor;

    if DefValDeText(Valor, Txt_emit, 'Pa�s') then
    begin
      cXML.infNFe.Emit.EnderEmit.cPais := Copy(Valor, 1, 4);
      cXML.infNFe.Emit.EnderEmit.xPais := copy(Valor, 8, 60);
    end;

    if DefValDeText(Valor, Txt_emit, 'Inscri��o Estadual') then
      cXML.infNFe.Emit.IE := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_emit, 'Munic�pio da Ocorr�ncia do Fato Gerador do ICMS') then
      cXML.infNFe.Ide.cMunFG := Valor;

    if DefValDeText(Valor, Txt_emit, 'C�digo de Regime Tribut�rio') then
      cXML.infNFe.Emit.CRT := Copy(Valor, 1, 1);

    //
    if Trim(Txt_emit) <> '' then
      AvisaTextoNaoProcessado('62 # E01', sWeb_emit, Txt_emit);
  end;

  // 'DADOS DO DESTINAT�RIO'
  if Length(Txt_dest) > 0 then
  begin
    Txt_dest := RetiraTag(Txt_dest, sWeb_dest);


    if DefValDeText(Valor, Txt_dest, 'Nome / Raz�o social') then
      cXML.infNFe.Dest.xNome   := Valor;

    if DefValDeText(Valor, Txt_dest, 'CNPJ/CPF') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Dest.CNPJ := Valor
        else
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_dest, 'CNPJ') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Dest.CNPJ := Valor
        else
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_dest, 'Endere�o') then
      cXML.infNFe.Dest.EnderDest.xLgr := Valor;

    if DefValDeText(Valor, Txt_dest, 'Bairro / Distrito') then
      cXML.infNFe.Dest.EnderDest.xBairro := Valor;

    if DefValDeText(Valor, Txt_dest, 'CEP') then
      cXML.infNFe.Dest.EnderDest.CEP := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_dest, 'Munic�pio') then
    begin
      cXML.infNFe.Dest.EnderDest.cMun := Copy(Valor, 1, 7);
      cXML.infNFe.Dest.EnderDest.xMun := copy(Valor, 10, 60);
    end;

    if DefValDeText(Valor, Txt_dest, 'Fone / Fax') then
      cXML.infNFe.Dest.EnderDest.fone := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_dest, 'UF') then
      cXML.infNFe.Dest.EnderDest.UF := Valor;

    if DefValDeText(Valor, Txt_dest, 'Pa�s') then
    begin
      cXML.infNFe.Dest.EnderDest.cPais := Copy(Valor, 1, 4);

      cXML.infNFe.Dest.EnderDest.xPais := copy(Valor, 8, 60);
    end;

    if DefValDeText(Valor, Txt_dest, 'Inscri��o estadual') then
      cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_dest, 'E-mail') then
      cXML.infNFe.Dest.Email := Valor;

    //
    if Trim(Txt_dest) <> '' then
      AvisaTextoNaoProcessado('62 # E01', sWeb_dest, Txt_dest);
  end;


  // 'DADOS DO REMETENTE '
  if Length(Txt_remet) > 0 then
  begin
    Txt_remet := RetiraTag(Txt_remet, sWeb_remet);


    if DefValDeText(Valor, Txt_remet, 'Nome / Raz�o social') then
      cXML.infNFe.Dest.xNome   := Valor;

    if DefValDeText(Valor, Txt_remet, 'CNPJ/CPF') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Dest.CNPJ := Valor
        else
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_remet, 'CNPJ') then
    begin
      Valor := Geral.SoNumero_TT(Valor);
      if Valor <> '' then
      begin
        if Length(Valor) >= 14 then
          cXML.InfNFe.Dest.CNPJ := Valor
        else
          cXML.InfNFe.Dest.CPF := Valor;
      end;
    end;

    if DefValDeText(Valor, Txt_remet, 'Endere�o') then
      cXML.infNFe.Dest.EnderDest.xLgr := Valor;

    if DefValDeText(Valor, Txt_remet, 'Bairro / Distrito') then
      cXML.infNFe.Dest.EnderDest.xBairro := Valor;

    if DefValDeText(Valor, Txt_remet, 'CEP') then
      cXML.infNFe.Dest.EnderDest.CEP := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_remet, 'Munic�pio') then
    begin
      cXML.infNFe.Dest.EnderDest.cMun := Copy(Valor, 1, 7);
      cXML.infNFe.Dest.EnderDest.xMun := copy(Valor, 10, 60);
    end;

    if DefValDeText(Valor, Txt_remet, 'Fone / Fax') then
      cXML.infNFe.Dest.EnderDest.fone := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_remet, 'UF') then
      cXML.infNFe.Dest.EnderDest.UF := Valor;

    if DefValDeText(Valor, Txt_remet, 'Pa�s') then
    begin
      cXML.infNFe.Dest.EnderDest.cPais := Copy(Valor, 1, 4);

      cXML.infNFe.Dest.EnderDest.xPais := copy(Valor, 8, 60);
    end;

    if DefValDeText(Valor, Txt_remet, 'Inscri��o estadual') then
      cXML.infNFe.Dest.IE := Geral.SoNumeroELetra_TT(Valor);

    if DefValDeText(Valor, Txt_remet, 'E-mail') then
      cXML.infNFe.Dest.Email := Valor;

    //
    if Trim(Txt_remet) <> '' then
      AvisaTextoNaoProcessado('62 # E01', sWeb_remet, Txt_remet);
  end;



  // GRUPO INTEIRO N�O TESTADO!!!!!
  // 'LOCAL DE RETIRADA '
  if Length(Txt_retirada) > 0 then
  begin
    Txt_retirada := RetiraTag(Txt_retirada, sWeb_retirada);

    Multipl := '';
    if DefValDeText(Valor, Txt_retirada, 'CNPJ') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if DefValDeText(Valor, Txt_retirada, 'CPF') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if Length(Multipl) >= 14 then
      cXML.infNFe.retirada.CNPJ := Geral.SoNumero_TT(Multipl)
    else
      cXML.infNFe.retirada.CPF := Geral.SoNumero_TT(Multipl);

    if DefValDeText(Valor, Txt_retirada, 'Logradouro') then
      cXML.infNFe.retirada.xLgr := Valor;

    if DefValDeText(Valor, Txt_retirada, 'Bairro') then
      cXML.infNFe.retirada.xBairro := Valor;

    if DefValDeText(Valor, Txt_retirada, 'Munic�pio') then
    begin
      cXML.infNFe.retirada.cMun := Copy(Valor, 1, 7);

      cXML.infNFe.retirada.xMun := Copy(Valor, 11, 60);
    end;

    if DefValDeText(Valor, Txt_retirada, 'UF') then
      cXML.infNFe.retirada.UF := Valor;

    //
    if Trim(Txt_retirada) <> '' then
      AvisaTextoNaoProcessado('80 # F01', sWeb_retirada, Txt_retirada);
  end;


  // 'LOCAL DE ENTREGA '
  if Length(Txt_entrega) > 0 then
  begin
    Txt_entrega := RetiraTag(Txt_entrega, sWeb_entrega);

    Multipl := '';
    if DefValDeText(Valor, Txt_entrega, 'CNPJ') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if DefValDeText(Valor, Txt_entrega, 'CPF') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if Length(Multipl) >= 14 then
      cXML.infNFe.Entrega.CNPJ := Geral.SoNumero_TT(Multipl)
    else
      cXML.infNFe.Entrega.CPF := Geral.SoNumero_TT(Multipl);

    if DefValDeText(Valor, Txt_entrega, 'Logradouro') then
      cXML.infNFe.Entrega.xLgr := Valor;

    if DefValDeText(Valor, Txt_entrega, 'Bairro') then
      cXML.infNFe.Entrega.xBairro := Valor;

    if DefValDeText(Valor, Txt_entrega, 'Munic�pio') then
    begin
      cXML.infNFe.Entrega.cMun := Copy(Valor, 1, 7);

      cXML.infNFe.Entrega.xMun := Copy(Valor, 11, 60);
    end;

    if DefValDeText(Valor, Txt_entrega, 'UF') then
      cXML.infNFe.Entrega.UF := Valor;

    //
    if Trim(Txt_entrega) <> '' then
      AvisaTextoNaoProcessado('89 # G01', sWeb_entrega, Txt_entrega);
  end;






  // 'DADOS DOS PRODUTOS E SERVI�OS '
  if Length(Txt_det) > 0 then
  begin
    Txt_det := RetiraTag(Txt_det, sWeb_det);

    // Pode ocorrer aqui!
    Txt_det := RetiraTxt(Txt_det, sTit_det);

    // Achar quantidade de produtos
    for I := MinItens to MaxItens do
    begin
      if pos('|&|' + intTostr(I) + '|&|', Txt_det) > 0 then Inc(Produtos);
    end;

    for I := MinItens to Produtos do
    begin
      if I < Produtos then
        Txt_prod := SeparaAte('|&|' + intTostr(i + 1) + '|&|', Txt_det, Txt_det, False)
      else
      begin
        Txt_prod := Txt_det;
        Txt_det := '';
      end;
      //
      // Separa IPI do ICMS
      if pos(sWeb_IPI, txt_prod) > 0 then
        Txt_prod := SeparaAte(sWeb_IPI, Txt_prod, Txt_IPI, False)
      else
        // caso n�o tenha IPI:
        Txt_IPI := '';

      //

      //
      // Separa DI do ICMS
      if pos(sWeb_DI, txt_prod) > 0 then
        Txt_prod := SeparaAte(sWeb_DI, Txt_prod, Txt_DI, False)
      else
        // caso n�o tenha DI:
        Txt_DI := '';

      //

      cDetLista := cXML.InfNFe.Det.Add;

      cDetLista.nItem := FormatFloat('000', I);

      if DefValDeText(Valor, Txt_prod, '|&|' + intTostr(i) + '|&|') then
        cDetLista.Prod.xProd := Valor;

      Txt_prod := '|&|' + Txt_prod;
      if DefValDeText(Valor, Txt_prod, '|&|') then
        cDetLista.Prod.qCom := Converte_NumBr_To_NumNFe(Valor);

      Txt_prod := '|&|' + Txt_prod;
      if DefValDeText(Valor, Txt_prod, '|&|') then
        cDetLista.Prod.uCom := Valor;

      Txt_prod := '|&|' + Txt_prod;
      if DefValDeText(Valor, Txt_prod, '|&|') then
        cDetLista.Prod.vProd := Converte_NumBr_To_NumNFe(Valor);

      if DefValDeText(Valor, Txt_prod, 'C�digo do Produto') then
        cDetLista.Prod.cProd := Valor;

      if DefValDeText(Valor, Txt_prod, 'C�digo NCM') then
        cDetLista.Prod.NCM := Geral.SoNUmero_TT(Valor);

      if DefValDeText(Valor, Txt_prod, 'CFOP') then
        cDetLista.Prod.CFOP := Geral.SoNUmero_TT(Valor);

      if DefValDeText(Valor, Txt_prod, 'Valor Total do Frete') then
        cDetLista.Prod.vFrete := Converte_NumBr_To_NumNFe(Valor);

      if DefValDeText(Valor, Txt_prod, 'Indicador de Composi��o do Valor Total da NF-e') then
        cDetLista.Prod.indTot := Geral.SoNumero_TT(Copy(Valor, 1, 3));

      cDetLista.Prod.cEAN := '';
      if DefValDeText(Valor, Txt_prod, 'C�digo EAN Comercial') then
        cDetLista.Prod.cEAN := Valor;

      if DefValDeText(Valor, Txt_prod, 'Unidade Comercial') then
        cDetLista.Prod.uCom := Valor;

      if DefValDeText(Valor, Txt_prod, 'Quantidade Comercial') then
        cDetLista.Prod.qCom := Converte_NumBr_To_NumNFe(Valor);

      cDetLista.Prod.cEANTrib := '';
      if DefValDeText(Valor, Txt_prod, 'C�digo EAN Tribut�vel') then
        cDetLista.Prod.cEANTrib := Valor;

      if DefValDeText(Valor, Txt_prod, 'Unidade Tribut�vel') then
        cDetLista.Prod.uTrib := Valor;

      if DefValDeText(Valor, Txt_prod, 'Quantidade Tribut�vel') then
        cDetLista.Prod.qTrib := Converte_NumBr_To_NumNFe(Valor);

      if DefValDeText(Valor, Txt_prod, 'Valor unit�rio de comercializa��o') then
        cDetLista.Prod.vUnCom := Converte_NumBr_To_NumNFe(Valor);

      if DefValDeText(Valor, Txt_prod, 'Valor unit�rio de tributa��o') then
        cDetLista.Prod.vUnTrib := Converte_NumBr_To_NumNFe(Valor);



      if pos(sWeb_DI, Txt_DI) = 1 then
        Txt_DI := sPipe + Txt_DI;
      //
      H := -1;
      if Txt_DI <> '' then
      begin
        Txt_DI := RetiraTag(Txt_DI, sWeb_DI);
        //
        // Como vir� o texto do site quando tiver mais de um?
        H := H + 1;
        cDILista := cXML.InfNFe.Det.Items[H].Prod.DI.Add;
        //

{      while not DmNFe_0000.QrNFEItsIDI.Eof do
      begin
        h := DmNFe_0000.QrNFEItsI.RecNo - 1;//DmNFe_0000.QrNFEItsIDI.RecNo;
        //
        cDILista := cXML.InfNFe.Det.Items[h].Prod.DI.Add;
        if Def('118', 'I19', DmNFe_0000.QrNFEItsIDIDI_nDI.Value, Valor) then
          cDILista.NDI := Valor;
        if Def('119', 'I20', DmNFe_0000.QrNFEItsIDIDI_dDI.Value, Valor) then
          cDILista.DDI := Valor;
        if Def('120', 'I21', DmNFe_0000.QrNfeItsIDIDI_xLocDesemb.Value, Valor) then
          cDILista.XLocDesemb := Valor;
        if Def('121', 'I22', DmNFe_0000.QrNfeItsIDIDI_UFDesemb.Value, Valor) then
          cDILista.UFDesemb := Valor;
        if Def('122', 'I23', DmNFe_0000.QrNfeItsIDIDI_dDesemb.Value, Valor) then
          cDILista.DDesemb := Valor;
        if Def('123', 'I24', DmNFe_0000.QrNfeItsIDIDI_cExportador.Value, Valor) then
          cDILista.CExportador := Valor;
        /////////////////////
        ///
       (* Informa��es da TAG ADI... Opcional *)
        DmNFe_0000.QrNFeItsIDIa.Close;
        DmNFe_0000.QrNFeItsIDIa.Params[00].AsInteger := DmNFe_0000.QrNFeItsIDIFatID.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[01].AsInteger := DmNFe_0000.QrNFeItsIDIFatNum.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[02].AsInteger := DmNFe_0000.QrNFeItsIDIEmpresa.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[03].AsInteger := DmNFe_0000.QrNFeItsIDInItem.Value;
        DmNFe_0000.QrNFeItsIDIa.Params[04].AsInteger := DmNFe_0000.QrNFeItsIDIControle.Value;
        DmNFe_0000.QrNFeItsIDIa.Open;
        //
        if DmNFe_0000.QrNFEItsIDIa.RecordCount > 0 then
        begin
          j := DmNFe_0000.QrNFEItsIDI.RecNo - 1;//DmNFe_0000.QrNFeItsIDIa.RecNo;
          //
          cAdiLista := cXML.InfNFe.Det.Items[h].Prod.DI.Items[j].Adi.Add;
          if Def('125', 'I26', DmNFe_0000.QrNfeItsIDIaAdi_nAdicao.Value, Valor) then
            cAdiLista.NAdicao := Valor;
          if Def('126', 'I27', DmNFe_0000.QrNfeItsIDIaAdi_nSeqAdic.Value, Valor) then
            cAdiLista.NSeqAdic := Valor;
          if Def('127', 'I28', DmNFe_0000.QrNfeItsIDIaAdi_cFabricante.Value, Valor) then
            cAdiLista.CFabricante := Valor;
          if Def('128', 'I29', DmNFe_0000.QrNfeItsIDIaAdi_vDescDI.Value, Valor) then
            cAdiLista.VDescDI := Valor;
        end else Geral.MensagemBox('Declara��o de importa��o sem Adi��es!',
        'Aviso', MB_OK+MB_ICONWARNING);
        ///
        /////////////////////
        DmNFe_0000.QrNFEItsIDI.Next;
      end;
}
        if pos(sWeb_nDI, txt_DI) > 0 then
        begin
          RemoveGrupo(sWeb_nDI, Txt_DI, Txt_nDI, Txt_DI, False);
          Txt_nDI := RetiraTag(Txt_nDI, sWeb_nDI);
          //
          if DefValDeText(Valor, Txt_nDI, 'N�MERO DI 1') then
            cDILista.NDI := Valor;

          //

          if Trim(Txt_nDI) <> '' then
            AvisaTextoNaoProcessado('118 # I19', sWeb_nDI, Txt_nDI);
        end;

        J := -1;
        if pos(sWeb_adi, txt_DI) > 0 then
        begin
          J := J + 1;
          cAdiLista := cXML.InfNFe.Det.Items[H].Prod.DI.Items[J].Adi.Add;
          //
          RemoveGrupo(sWeb_adi, Txt_DI, Txt_adi, Txt_DI, False);
          Txt_adi := RetiraTag(Txt_adi, sWeb_adi);
          //
          Campos := 'No. Adi��o |&|Item |&|C�digo Fabricante Estrangeiro |&|Valor do Desconto |&|';
          if pos(Campos, Txt_adi) = 1 then
          begin
            Valores := CasaCamposEValores(4, Campos, Txt_adi);
            if Valores <> '' then
            begin
              if DefValDeText(Valor, Valores, 'No. Adi��o ') then
                cAdiLista.NAdicao := Valor;
              //
              if DefValDeText(Valor, Valores, 'Item ') then
                cAdiLista.NSeqAdic := Valor;
              //
              if DefValDeText(Valor, Valores, 'C�digo Fabricante Estrangeiro ') then
                cAdiLista.CFabricante := Valor;
              //
              if DefValDeText(Valor, Valores, 'Valor do Desconto ') then
                cAdiLista.VDescDI := Converte_NumBr_To_NumNFe(Valor);
              //
(*
              if DefValDeText(Valor, Valores, 'N�mero do pedido de compra ') then
                cAdiLista.NAdicao := Valor;
              //
              if DefValDeText(Valor, Valores, 'Item do Pedido de Compra ') then
                cAdiLista.NAdicao := Valor;
              //
*)
              if Trim(Valores) <> '' then
                AvisaTextoNaoProcessado('124 # I25', sWeb_adi, Valores);

            end;
          end;

          //
          if Trim(Txt_adi) <> '' then
            AvisaTextoNaoProcessado('124 # I25', sWeb_adi, Txt_adi);
          //
        end;
        //
        // Campos da DI
        Campos := 'DI/DSI/DA |&|Data Registro|&|Local Desembara�o Aduaneiro|&|Data |&|UF |&|C�digo do Exportador|&|';
        if pos(Campos, Txt_DI) = 1 then
        begin
          Valores := CasaCamposEValores(6, Campos, Txt_DI);
          if Valores <> '' then
          begin
            if DefValDeText(Valor, Valores, 'DI/DSI/DA ') then
              cDILista.nDI := Valor;
            //
            if DefValDeText(Valor, Valores, 'Data Registro') then
            begin
{
              Ano := Geral.IMV(Copy(Valor, 7, 4));
              Mes := Geral.IMV(Copy(Valor, 4, 2));
              Dia := Geral.IMV(Copy(Valor, 1, 2));
              DataHora := EncodeDate(Ano, Mes, Dia);
              Valor := FormatDateTime('yyyy-mm-dd', DataHora);
}
              Valor := Converte_DtaBr_to_DtaNFe(Valor, '', 4);
              cDILista.dDI := Valor;
            end;
            //
            if DefValDeText(Valor, Valores, 'Local Desembara�o Aduaneiro') then
              cDILista.xLocDesemb := Valor;
            //
            if DefValDeText(Valor, Valores, 'Data ') then
            begin
              Valor := Converte_DtaBr_to_DtaNFe(Valor, '', 4);
              cDILista.dDesemb := Valor;
            end;
            //
            if DefValDeText(Valor, Valores, 'UF ') then
              cDILista.UFDesemb := Valor;
            //
            if DefValDeText(Valor, Valores, 'C�digo do Exportador') then
              cDILista.CExportador := Valor;
            //

            //
            if Trim(Valores) <> '' then
              AvisaTextoNaoProcessado('117 # I18', sWeb_DI, Valores);
          end;
        end;
        //
      end;



  (*       Parei aqui! N�o uso!
      if LerCampo(Txt_prod,'Chassi do ve�culo ') <> '' then
      begin
        // preencher as tags referente a ve�culo
        cDetLista.Prod.veiccDetLista.Prod.chassi  := LerCampo(Txt_prod,'Chassi do ve�culo ');
        cDetLista.Prod.veiccDetLista.Prod.cCor    := LerCampo(Txt_prod,'Cor ');
        cDetLista.Prod.veiccDetLista.Prod.xCor    := LerCampo(Txt_prod,'Descri��o da cor ');
        cDetLista.Prod.veiccDetLista.Prod.nSerie  := LerCampo(Txt_prod,'Serial (S�rie) ');
        cDetLista.Prod.veiccDetLista.Prod.tpComb  := LerCampo(Txt_prod,'Tipo de Combust�vel ');
        cDetLista.Prod.veiccDetLista.Prod.nMotor  := LerCampo(Txt_prod,'N�mero de Motor ');
        //cDetLista.Prod.veiccDetLista.Prod.RENAVAM := LerCampo(Txt_prod,'RENAVAM');
        cDetLista.Prod.veiccDetLista.Prod.anoMod  := StrToInt(LerCampo(Txt_prod,'Ano Modelo de Fabrica��o '));
        cDetLista.Prod.veiccDetLista.Prod.anoFab  := StrToInt(LerCampo(Txt_prod,'Ano de Fabrica��o '));
      end;
  *)

      if pos(sWeb_ICMS, Txt_Prod) = 1 then
        Txt_Prod := sPipe + Txt_Prod;
      //
      if RemoveGrupo(sWeb_ICMS, Txt_Prod, Txt_ICMS, Txt_prod, False) then
      begin
        Txt_ICMS := RetiraTag(Txt_ICMS, sWeb_ICMS);

        if DefValDeText(Valor, Txt_ICMS, 'Origem da Mercadoria', 1) then
          Orig := Valor;

        //CST := StrToCSTICMS(ok, Trim(LerCampo(Txt_ICMS, 'Tributa��o do ICMS', 3)));
        CST := '';
        CSOSN := '';
        if DefValDeText(Valor, Txt_ICMS, 'Tributa��o do ICMS', 3) then
        begin
          CST := Geral.SoNumero_TT(Valor);
          if CST = '' then
          begin
            Geral.MensagemBox('CST n�o informado no sitio!' + #13#10 +
            'Item de produto/servi�o: ' + FormatFloat('000', I) + #13#10 +
            'Ser� usado o CST "00".' + #13#10 +
            'Corrija o lan�amento manualmente ap�s a importa��o!',
            'Aviso', MB_OK+MB_ICONWARNING);
            CST := '00';
          end;
          //
          //Modalidade Defini��o da BC ICMS NOR

          //separa at� a pr�xima tag
          // Gerar mensagem caso exista texto!
          //Txt_ICMS := Copy(Txt_ICMS, Pos('Modalidade', Txt_ICMS), Length(Txt_ICMS));

          case Geral.IMV(CST) of
            0:
            begin
              cDetLista.Imposto.ICMS.ICMS00.Orig  := Orig;
              cDetLista.Imposto.ICMS.ICMS00.CST   := CST;

              if DefValDeText(Valor, Txt_ICMS, 'Modalidade Defini��o da BC ICMS NORMAL', 1) then
                cDetLista.Imposto.ICMS.ICMS00.ModBC := Valor;

              if DefValDeText(Valor, Txt_ICMS, 'Base de C�lculo do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS00.VBC := Converte_NumBr_To_NumNFe(Valor);

              //separa at� a TAG al�quota
              Txt_ICMS := Copy(Txt_ICMS, Pos('Al�quota ICMS Normal', Txt_ICMS), Length(Txt_ICMS));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS00.PICMS := Converte_NumBr_To_NumNFe(Valor);


              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS00.VICMS := Converte_NumBr_To_NumNFe(Valor);
            end;

  {
            10:
            begin
              cDetLista.Imposto.ICMS.ICMS10.Orig      := Orig;
              cDetLista.Imposto.ICMS.ICMS10.CST       := CST;

              if DefValDeText(Valor, Txt_ICMS, 'Modalidade Defini��o da BC ICMS NORMAL', 1) then
                cDetLista.Imposto.ICMS.ICMS10.ModBC := Valor;

              if DefValDeText(Valor, Txt_ICMS, 'Base de C�lculo do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS10.VBC := Converte_NumBr_To_NumNFe(Valor);

              //separa at� a TAG al�quota
              Txt_ICMS := Copy(Txt_ICMS, Pos('Al�quota ICMS Normal', Txt_ICMS), Length(Txt_ICMS));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS10.PICMS := Converte_NumBr_To_NumNFe(Valor);


              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS10.VICMS := Converte_NumBr_To_NumNFe(Valor);

              if DefValDeText(Valor, Txt_ICMS, 'Modalidade Defini��o da BC ICMS ST', 1) then
                cDetLista.Imposto.ICMS.ICMS10.ModBCST := Valor;

            (*
            if Def('180', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.PMVAST    := Valor;
            if Def('181', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
              cDetLista.Imposto.ICMS.ICMS10.PRedBCST  := Valor;
            *)

              if DefValDeText(Valor, Txt_ICMS, 'Base de C�lculo do ICMS ST') then
                cDetLista.Imposto.ICMS.ICMS10.vBCST := Converte_NumBr_To_NumNFe(Valor);

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do ICMS ST') then
                cDetLista.Imposto.ICMS.ICMS10.PICMSST := Converte_NumBr_To_NumNFe(Valor);

              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS ST') then
                cDetLista.Imposto.ICMS.ICMS10.VICMSST := Converte_NumBr_To_NumNFe(Valor);
            end;
            20:
            begin
              (* TAG ICMS.ICMS20... *)
                  // '185', 'N04'  = Txt_ICMS do CST = 20
              if Def('186', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.Orig      := Valor;
              if Def('187', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.CST       := Valor;
              if Def('188', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.ModBC     := Valor;
              if Def('189', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.PRedBC    := Valor;
              if Def('190', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.VBC       := Valor;
              if Def('191', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.PICMS     := Valor;
              if Def('192', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS20.VICMS     := Valor;
            end;
            30:
            begin
              (* TAG ICMS.ICMS30... *)
                  // '193', 'N05'  = Txt_ICMS do CST = 30
              if Def('194', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.Orig      := Valor;
              if Def('195', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.CST       := Valor;
              if Def('196', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.ModBCST   := Valor;
              if Def('197', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.PMVAST    := Valor;
              if Def('198', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.PRedBCST  := Valor;
              if Def('199', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.VBCST     := Valor;
              if Def('200', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.PICMSST   := Valor;
              if Def('201', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS30.VICMSST   := Valor;
            end;
}
            40,41,50:
            begin
              (* TAG ICMS.ICMS40... *)
                  // '202', 'N06'  = Txt_ICMS do CST = 40, 41, 50
              cDetLista.Imposto.ICMS.ICMS40.Orig  := Orig;
              cDetLista.Imposto.ICMS.ICMS40.CST   := CST;

{
              if Def('204.01', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS40.vICMS       := Valor;
              if Def('204.02', 'N28', DmNFe_0000.QrNFEItsNICMS_motDesICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS40.motDesICMS       := Valor;
              if DefValDeText(Valor, Txt_ICMS, 'Modalidade Defini��o da BC ICMS NORMAL', 1) then
                cDetLista.Imposto.ICMS.ICMS40.ModBC := Valor;

              if DefValDeText(Valor, Txt_ICMS, 'Base de C�lculo do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS40.VBC := Converte_NumBr_To_NumNFe(Valor);

              //separa at� a TAG al�quota
              Txt_ICMS := Copy(Txt_ICMS, Pos('Al�quota ICMS Normal', Txt_ICMS), Length(Txt_ICMS));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS40.PICMS := Converte_NumBr_To_NumNFe(Valor);


              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS Normal') then
                cDetLista.Imposto.ICMS.ICMS40.VICMS := Converte_NumBr_To_NumNFe(Valor);
}

            end;
{
            51:
            begin
              (* TAG ICMS.ICMS51... *)
                  // '205', 'N07'  = Txt_ICMS do CST = 51
              if Def('206', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.Orig      := Valor;
              if Def('207', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.CST       := Valor;
              if Def('208', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.ModBC     := Valor;
              if Def('209', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.PRedBC    := Valor;
              if Def('210', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.VBC       := Valor;
              if Def('211', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.PICMS     := Valor;
              if Def('212', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS51.VICMS     := Valor;
            end;
            60:
            begin
              (* TAG ICMS.ICMS60... *)
                  // '213', 'N08'  = Txt_ICMS do CST = 60
              if Def('214', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS60.Orig      := Valor;
              if Def('215', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS60.CST       := Valor;
              if Def('216', 'N26', DmNFe_0000.QrNFEItsNICMS_vBCSTRet.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS60.VBCSTRet     := Valor;
              if Def('217', 'N27', DmNFe_0000.QrNFEItsNICMS_vICMSSTRet.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS60.VICMSSTRet   := Valor;
            end;
            70:
            begin
              (* TAG ICMS.ICMS70... *)
                  // '218', 'N09'  = Txt_ICMS do CST = 70
              if Def('219', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.Orig      := Valor;
              if Def('220', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.CST       := Valor;
              if Def('221', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.ModBC     := Valor;
              if Def('222', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.PRedBC    := Valor;
              if Def('223', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.VBC       := Valor;
              if Def('224', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.PICMS     := Valor;
              if Def('225', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.VICMS     := Valor;
              if Def('226', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.ModBCST   := Valor;
              if Def('227', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.PMVAST    := Valor;
              if Def('228', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.PRedBCST  := Valor;
              if Def('229', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.VBCST     := Valor;
              if Def('230', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.PICMSST   := Valor;
              if Def('231', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS70.VICMSST   := Valor;
            end;
            90:
            begin
              (* TAG ICMS.ICMS90... *)
                  // '232', 'N10'  = Txt_ICMS do CST = 90
              if Def('233', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.Orig      := Valor;
              if Def('234', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.CST       := Valor;
              if Def('235', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.ModBC     := Valor;
              if Def('236', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.VBC       := Valor;
              if Def('237', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.PRedBC    := Valor;
              if Def('238', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.PICMS     := Valor;
              if Def('239', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.VICMS     := Valor;
              if Def('240', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.ModBCST   := Valor;
              if Def('241', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.PMVAST    := Valor;
              if Def('242', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.PRedBCST  := Valor;
              if Def('243', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.VBCST     := Valor;
              if Def('244', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.PICMSST   := Valor;
              if Def('245', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
                cDetLista.Imposto.ICMS.ICMS90.VICMSST   := Valor;
            end;
  }
            else Geral.MensagemBox('CST ICMS n�o implementado: "' + CST + '"' +
            #13#10 + 'AVISE A DERMATEK!', 'Aviso', MB_OK + MB_ICONWARNING);
          end;
        end
        else
        if DefValDeText(Valor, Txt_ICMS, 'C�digo de Situa��o da Opera��o', 6) then
        begin
          // Valor veio vazio!!!
          CSOSN := Geral.SoNumero_TT(Valor);
          if CSOSN = '' then
          begin
            Geral.MensagemBox('CSOSN n�o informado no sitio!' + #13#10 +
            'Item de produto/servi�o: ' + FormatFloat('000', I) + #13#10 +
            'Ser� usado o CSOSN "900".' + #13#10 +
            'Corrija o lan�amento manualmente ap�s a importa��o!',
            'Aviso', MB_OK+MB_ICONWARNING);
            CSOSN := '900'
          end;
          //
          //Modalidade Defini��o da BC ICMS NOR

          //separa at� a pr�xima tag
          Txt_ICMS := Copy(Txt_ICMS, Pos('Modalidade', Txt_ICMS), Length(Txt_ICMS));

          case Geral.IMV(CSOSN) of
            900:
            begin
              cDetLista.Imposto.ICMS.ICMSSN900.Orig  := Orig;
              cDetLista.Imposto.ICMS.ICMSSN900.CSOSN := CSOSN;

              if DefValDeText(Valor, Txt_ICMS, 'Modalidade de determina��o da BC do ICMS', 3) then
                cDetLista.Imposto.ICMS.ICMSSN900.ModBC := Geral.SoNumero_TT(Valor);

              if DefValDeText(Valor, Txt_ICMS, 'Valor da BC do ICMS') then
                cDetLista.Imposto.ICMS.ICMSSN900.vBC := Converte_NumBr_To_NumNFe(Trim(Valor));

              // N�o foi testado ainda!
              if DefValDeText(Valor, Txt_ICMS, 'Percentual de Redu��o de BC') then
                cDetLista.Imposto.ICMS.ICMSSN900.pRedBC := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do imposto') then
                cDetLista.Imposto.ICMS.ICMSSN900.pICMS := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS') then
                cDetLista.Imposto.ICMS.ICMSSN900.vICMS := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Modalidade de determina��o da BC do ICMS ST', 3) then
                cDetLista.Imposto.ICMS.ICMSSN900.modBCST := Geral.SoNumero_TT(Valor);

              // N�o foi testado ainda!
              if DefValDeText(Valor, Txt_ICMS, 'Percentual da margem de valor Adicionado do ICMS ST') then
                cDetLista.Imposto.ICMS.ICMSSN900.pMVAST := Converte_NumBr_To_NumNFe(Trim(Valor));

              // N�o foi testado ainda!
              if DefValDeText(Valor, Txt_ICMS, 'Percentual da Redu��o de BC do ICMS ST') then
                cDetLista.Imposto.ICMS.ICMSSN900.pRedBCST := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Valor da BC do ICMS '(*ST'*)) then
                cDetLista.Imposto.ICMS.ICMSSN900.vBCST := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota do imposto'(* do ICMS ST'*)) then
                cDetLista.Imposto.ICMS.ICMSSN900.pICMSST := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Valor do ICMS '(*ST'*)) then
                cDetLista.Imposto.ICMS.ICMSSN900.vICMSST := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Al�quota aplic�vel de c�lculo do cr�dito '(*(SIMPLES NACIONAL)'*)) then
                cDetLista.Imposto.ICMS.ICMSSN900.pCredSN := Converte_NumBr_To_NumNFe(Trim(Valor));

              if DefValDeText(Valor, Txt_ICMS, 'Valor de cr�dito do ICMS'(* que pode ser aproveitado nos termos do art. 23 da LC 123 (SIMPLS NACIONAL)'*)) then
                cDetLista.Imposto.ICMS.ICMSSN900.vCredICMSSN := Converte_NumBr_To_NumNFe(Trim(Valor));

            end;
            else Geral.MensagemBox('CSOSN ICMS n�o implementado: "' + CSOSN + '"' +
            #13#10 + 'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
            //
          end;
        end;
        if Trim(Txt_prod) <> '' then
          AvisaTextoNaoProcessado('100 # I01', sWeb_det, Txt_prod);

        if Trim(Txt_ICMS) <> '' then
        if AnsiUpperCase(Txt_ICMS) <> AnsiUpperCase('Num.|&|Descri��o|&|Qtd.|&|Unidade Comercial|&|Valor(R$)') then
          AvisaTextoNaoProcessado('165 # N02', 'ICMS NORMAL e ST', Txt_ICMS);
        // fim item produto
      end;

      if pos(sWeb_IPI, Txt_IPI) = 1 then
        Txt_IPI := sPipe + Txt_IPI;
      //
      if Txt_IPI <> '' then
      begin
        // � necess�rio separar dos totais pois pode haver omiss�o do campo
        // 'Base de C�lculo' e confundir com o campo 'Base de C�lculo... do 'TOTAIS'
        Txt_IPI := RetiraTag(Txt_IPI, sWeb_IPI);

        if DefValDeText(Valor, Txt_IPI, 'C�digo de Enquadramento') then
          cDetLista.Imposto.IPI.cEnq := Valor;

        if pos('Base de C�lculo', Txt_IPI) = 1 then
        if DefValDeText(Valor, Txt_IPI, 'Base de C�lculo') then
          cDetLista.Imposto.IPI.IPITrib.vBC := Converte_NumBr_To_NumNFe(Valor);

        if pos('Al�quota', Txt_IPI) = 1 then
        if DefValDeText(Valor, Txt_IPI, 'Al�quota') then
          cDetLista.Imposto.IPI.IPITrib.pIPI := Converte_NumBr_To_NumNFe(Valor);

        if pos('Valor IPI', Txt_IPI) = 1 then
        if DefValDeText(Valor, Txt_IPI, 'Valor IPI') then
          cDetLista.Imposto.IPI.IPITrib.vIPI := Converte_NumBr_To_NumNFe(Valor);

        if DefValDeText(Valor, Txt_IPI, 'CST') then
          cDetLista.Imposto.IPI.IPITrib.CST := Geral.SoNumero_TT(Copy(Valor, 1, 4));

        // pode ocorrer aqui!
        Txt_IPI := RetiraTxt(Txt_IPI, sTit_det);

        if Trim(Txt_IPI) <> '' then
          AvisaTextoNaoProcessado('246 # O01', sWeb_IPI, Txt_IPI);
        //
      end;

      if Trim(Txt_prod) <> '' then
        AvisaTextoNaoProcessado('100 # I01', 'Item de produto', Txt_prod);
    end;
    if Trim(Txt_det) <> '' then
      AvisaTextoNaoProcessado('98 # H01', sWeb_det, Txt_det);
  end;

  // 'TOTAIS DE ICMS';
  if Length(Txt_ICMSTot) > 0 then
  begin
    Txt_ICMSTot := RetiraTag(Txt_ICMSTot, sWeb_ICMSTot);
    //

    if DefValDeText(Valor, Txt_ICMSTot, 'Base de C�lculo ICMS') then
      cXML.InfNFe.Total.ICMSTot.VBC := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor do ICMS') then
      cXML.InfNFe.Total.ICMSTot.vICMS := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Base de C�lculo ICMS ST') then
      cXML.InfNFe.Total.ICMSTot.vBCST := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor ICMS Substitui��o') then
      cXML.InfNFe.Total.ICMSTot.vST := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor Total dos Produtos') then
      cXML.InfNFe.Total.ICMSTot.vProd := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor do Frete') then
      cXML.InfNFe.Total.ICMSTot.vFrete := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor do Seguro') then
      cXML.InfNFe.Total.ICMSTot.vSeg := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Outras Despesas Acess�rias') then
      cXML.InfNFe.Total.ICMSTot.vOutro := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor Total do IPI') then
      cXML.InfNFe.Total.ICMSTot.vIPI := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor Total da NFe') then
      cXML.InfNFe.Total.ICMSTot.vNF := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor Total dos Descontos') then
      cXML.InfNFe.Total.ICMSTot.vDesc := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor Total do II') then
      cXML.InfNFe.Total.ICMSTot.vII := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor do PIS') then
      cXML.InfNFe.Total.ICMSTot.vPIS := Converte_NumBr_To_NumNFe(Valor);

    if DefValDeText(Valor, Txt_ICMSTot, 'Valor da COFINS') then
      cXML.InfNFe.Total.ICMSTot.vCOFINS := Converte_NumBr_To_NumNFe(Valor);

    //
    if Trim(Txt_ICMSTot) <> '' then
      AvisaTextoNaoProcessado('327 # W02', sWeb_ICMSTot, Txt_ICMSTot);
  end;

  // 'EXPORTA��O |&|';
  if Length(Txt_Exporta2) > 0 then
  begin
    Txt_Exporta2 := RetiraTag(Txt_Exporta2, sWeb_Exporta);
    //
    if DefValDeText(Valor, Txt_Exporta2, 'Sigla da UF onde ocorrer� o embarque dos produtos') then
      cXML.InfNFe.exporta.UFEmbarq := Valor;
    //
    if DefValDeText(Valor, Txt_Exporta2, 'Local onde ocorrer� o embarque dos produtos') then
      cXML.InfNFe.exporta.xLocEmbarq := Valor;
    //
    if Trim(Txt_Exporta2) <> '' then
      AvisaTextoNaoProcessado('402 # ZA01', sWeb_Exporta, Txt_Exporta2);
  end;

  // 'DADOS DO TRANSPORTE';
  if Length(Txt_transp) > 0 then
  begin
    Txt_transp := RetiraTag(Txt_transp, sWeb_transp);

    //

    if DefValDeText(Valor, Txt_transp, 'Modalidade do Frete') then
      cXML.InfNFe.Transp.modFrete := Geral.SoNumero_TT(Copy(Valor, 1, 2));

    //
    if Trim(Txt_transp) <> '' then
      AvisaTextoNaoProcessado('356 # X01', sWeb_transp, Txt_transp);
  end;

  // 'TRANSPORTADOR ';
  if Length(Txt_transporta) > 0 then
  begin
    Txt_transporta := RetiraTag(Txt_transporta, sWeb_transporta);

    //

    Multipl := '';
    if DefValDeText(Valor, Txt_transporta, 'CNPJ') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if DefValDeText(Valor, Txt_transporta, 'CPF') then
      Multipl := Multipl + Geral.SoNumero_TT(Valor);
    if Length(Multipl) >= 14 then
      cXML.InfNFe.Transp.Transporta.CNPJ := Geral.SoNumero_TT(Multipl)
    else
      cXML.InfNFe.Transp.Transporta.CPF := Geral.SoNumero_TT(Multipl);

    if DefValDeText(Valor, Txt_transporta, 'Raz�o Social / Nome') then
      cXML.InfNFe.Transp.Transporta.xNome := Valor;

    if DefValDeText(Valor, Txt_transporta, 'Inscri��o Estadual') then
      cXML.InfNFe.Transp.Transporta.IE := Valor;

    if DefValDeText(Valor, Txt_transporta, 'Endere�o Completo') then
      cXML.InfNFe.Transp.Transporta.xEnder := Valor;

    if DefValDeText(Valor, Txt_transporta, 'Munic�pio') then
      cXML.InfNFe.Transp.Transporta.xMun := Valor;

    if DefValDeText(Valor, Txt_transporta, 'UF') then
      cXML.InfNFe.Transp.Transporta.UF := Valor;

    if DefValDeText(Valor, Txt_transporta, 'Placa') then
      cXML.InfNFe.Transp.veicTransp.placa := Valor;

    if DefValDeText(Valor, Txt_transporta, 'UF') then
      cXML.InfNFe.Transp.veicTransp.UF := Valor;

    //
    if Trim(Txt_transporta) <> '' then
      AvisaTextoNaoProcessado('358 # X03', sWeb_transporta, Txt_transporta);
  end;

  // 'VOLUMES '
  if Length(Txt_vol) > 0 then
  begin
    Txt_vol := RetiraTag(Txt_vol, sWeb_vol);
    //
    // N�o testado!  -> mais de um item!!!
    begin
      i := 0;
      cXML.InfNFe.Transp.Vol.Add;

      if DefValDeText(Valor, Txt_vol, 'Quantidade') then
        cXML.InfNFe.Transp.Vol[i].qVol  := Valor;

      if DefValDeText(Valor, Txt_vol, 'Esp�cie') then
        cXML.InfNFe.Transp.Vol[i].esp  := Valor;

      // N�o testado!
      if DefValDeText(Valor, Txt_vol, 'Marca') then
        cXML.InfNFe.Transp.Vol[i].marca  := Valor;

      // N�o testado!
      if DefValDeText(Valor, Txt_vol, 'Numera��o') then
        cXML.InfNFe.Transp.Vol[i].nVol  := Valor;

      if DefValDeText(Valor, Txt_vol, 'Peso L�quido') then
        cXML.InfNFe.Transp.Vol[i].pesoL  := Converte_NumBr_To_NumNFe(Valor);

      if DefValDeText(Valor, Txt_vol, 'Peso Bruto') then
        cXML.InfNFe.Transp.Vol[i].pesoB  := Converte_NumBr_To_NumNFe(Valor);

      // N�o testado
      // Falta rotina para pegar v�rios volumes
      //Inc(i);
    end;

    //
    if Trim(Txt_vol) <> '' then
      AvisaTextoNaoProcessado('381 # X26', sWeb_vol, Txt_vol);
  end;

  //'DADOS DE COBRAN�A'
  if Length(Txt_cobr) > 0 then
  begin
    Txt_cobr := RetiraTag(Txt_cobr, sWeb_cobr);
    //

    //
    if Trim(Txt_cobr) <> '' then
      AvisaTextoNaoProcessado('389 # Y01', sWeb_cobr, Txt_cobr);
  end;

  // 'FATURA|&|';
  if Length(Txt_fat) > 0 then
  begin
    Txt_fat := RetiraTag(Txt_fat, sWeb_fat);
    //

    if DefValDeText(Valor, Txt_fat, 'N�mero') then
      cXML.InfNFe.Cobr.Fat.nFat  := Valor;

    if DefValDeText(Valor, Txt_fat, 'Valor Original') then
      cXML.InfNFe.Cobr.Fat.vOrig  := Converte_NumBr_To_NumNFe(Valor);

    // N�o testado?
    if DefValDeText(Valor, Txt_fat, 'Valor Desconto') then
      cXML.InfNFe.Cobr.Fat.vDesc  := Converte_NumBr_To_NumNFe(Valor);

    // N�o testado?
    if DefValDeText(Valor, Txt_fat, 'Valor L�quido') then
      cXML.InfNFe.Cobr.Fat.vLiq  := Converte_NumBr_To_NumNFe(Valor);
    //
    if Trim(Txt_fat) = 'else|&|' then
       Txt_fat := '';
    //
    if Trim(Txt_fat) = 'else' then
       Txt_fat := '';
    //
    if Trim(Txt_fat) <> '' then
      AvisaTextoNaoProcessado('390 # Y02', sWeb_fat, Txt_fat);
  end;

  // DUPLICATAS
  if Length(Txt_dup) > 0 then
  begin
    Txt_dup := RetiraTag(Txt_dup, sWeb_dup);
    //
    S := 'Valor |&|';
    Txt_dup := Copy(Txt_dup, pos(S, Txt_dup) + Length(S) + 1);

    I := 0;
    while Length(Geral.SONumero_TT(Txt_dup)) > 0 do
    begin
      cXML.InfNFe.Cobr.Dup.Add;

      Txt_dup := sPipe + Txt_dup;
      if DefValDeText(Valor, Txt_dup, sPipe) then
        cXML.InfNFe.Cobr.Dup[I].nDup  := Valor;

      Txt_dup := sPipe + Txt_dup;
      if DefValDeText(Valor, Txt_dup, sPipe) then
        cXML.InfNFe.Cobr.Dup[I].dVenc  := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(Valor, True, False));

      Txt_dup := sPipe + Txt_dup;
      if DefValDeText(Valor, Txt_dup, sPipe) then
        cXML.InfNFe.Cobr.Dup[I].vDup  := Converte_NumBr_To_NumNFe(Valor);

      //

      Inc(i);
    end;
    //
    if Trim(Txt_dup) = 'else|&|' then
       Txt_dup := '';
    //
    if Trim(Txt_dup) <> '' then
      AvisaTextoNaoProcessado('395 # Y07', sWeb_dup, Txt_dup);
  end;



  // INFORMA��ES ADICIONAIS
  if Length(Txt_infAdic) > 0 then
  begin
    Txt_infAdic := RetiraTag(Txt_infAdic, sWeb_infAdic);
    //

    if DefValDeText(Valor, Txt_infAdic, 'Formato de Impress�o') then
      cXML.InfNFe.ide.TpImp := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if DefValDeText(Valor, Txt_infAdic, 'Formato de Emiss�o') then
      cXML.InfNFe.ide.TpEmis := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if DefValDeText(Valor, Txt_infAdic, 'D�gito Verificador da Chave de Acesso') then
      cXML.InfNFe.ide.cDV := Geral.SoNumero_TT(Valor);

    if DefValDeText(Valor, Txt_infAdic, 'Identifica��o do Ambiente') then
      cXML.InfNFe.ide.tpAmb := Geral.SoNumero_TT(Copy(Valor, 1, 3));

    if DefValDeText(Valor, Txt_infAdic, 'Finalidade') then
      cXML.InfNFe.ide.FinNFe := Geral.SoNumero_TT(Copy(Valor, 1, 1));

    if DefValDeText(Valor, Txt_infAdic, 'Processo') then
      cXML.InfNFe.ide.procEmi := Geral.SoNumero_TT(Copy(Valor, 1, 1));

    if DefValDeText(Valor, Txt_infAdic, 'Vers�o') then
      cXML.InfNFe.ide.verProc := Valor;

    // 'EXPORTA��O ';
    if pos(sWeb_Exporta, Txt_infAdic) = 1 then
    begin
      Txt_infAdic := '|&|' + Txt_infAdic;
      RemoveGrupo(sWeb_Exporta, Txt_infAdic, Txt_Exporta1, Txt_infAdic, False);
    end;
    //
    if Trim(Txt_infAdic) <> '' then
      AvisaTextoNaoProcessado('399 # Z01', sWeb_infAdic, Txt_infAdic);
  end;

  // 'EXPORTA��O |&|';
  if Length(Txt_Exporta1) > 0 then
  begin
    Txt_Exporta1 := RetiraTag(Txt_Exporta1, sWeb_Exporta);
    //
    if DefValDeText(Valor, Txt_Exporta1, 'Local de Embarque') then
      cXML.InfNFe.exporta.xLocEmbarq := Valor;
    //
    if DefValDeText(Valor, Txt_Exporta1, 'UF de Embarque') then
      cXML.InfNFe.exporta.UFEmbarq := Valor;
    //
    if Trim(Txt_Exporta1) <> '' then
      AvisaTextoNaoProcessado('402 # ZA01', sWeb_Exporta, Txt_Exporta1);
  end;

  // 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO FISCO |&|';
  if Length(Txt_infAdFisco) > 0 then
  begin
    Txt_infAdFisco := RetiraTag(Txt_infAdFisco, sWeb_infAdFisco);
    //
    if DefValDeText(Valor, Txt_infAdFisco, 'Descri��o') then
      cXML.InfNFe.infAdic.infAdFisco := Valor;
    //
    if Trim(Txt_infAdFisco) <> '' then
      AvisaTextoNaoProcessado('400 # Z02', sWeb_infAdFisco, Txt_infAdFisco);
  end;

  // 'INFORMA��ES COMPLEMENTARES DE INTERESSE DO CONTRIBUINTE |&|';
  if Length(Txt_infCpl) > 0 then
  begin
    Txt_infCpl := RetiraTag(Txt_infCpl, sWeb_infCpl);
    //

    if DefValDeText(Valor, Txt_infCpl, 'Descri��o') then
      cXML.InfNFe.infAdic.InfCpl := Valor;
    //
    if Trim(Txt_infCpl) <> '' then
      AvisaTextoNaoProcessado('401 # Z03', sWeb_infCpl, Txt_infCpl);
  end;

  //  'OBSERVA��ES DO FISCO |&|';
  if Length(Txt_ObsFisco) > 0 then
  begin
    Txt_ObsFisco := RetiraTag(Txt_ObsFisco, sWeb_ObsFisco);
    //
    repeat
      J1 := pos(AnsiUpperCase('Campo'), AnsiUpperCase(Txt_ObsFisco));
      //
      if (J1 = 1) then
      begin
        if DefValDeText(Valor, Txt_ObsFisco, 'Campo') then
        begin
          cObsFisco := cXML.InfNFe.infAdic.ObsFisco.Add;
          cObsFisco.XCampo := Valor;
          Texto := '';
          repeat
            J2 := pos(AnsiUpperCase('Texto'), AnsiUpperCase(Txt_ObsFisco));
            if J2 = 1 then
            begin
              if DefValDeText(Valor, Txt_ObsFisco, 'Texto') then
              Texto := Texto + ' ' + Valor;
            end;
          until (J2 <> 1);
          cObsFisco.XTexto := Texto;
        end;
      end;
    until (J1 <> 1);
    //
    if Trim(Txt_ObsFisco) <> '' then
      AvisaTextoNaoProcessado('401d # Z07', sWeb_ObsFisco, Txt_ObsFisco);
  end;

  // 'OBSERVA��ES DO CONTRIBUINTE '
  if Length(Txt_ObsCont) > 0 then
  begin
    Txt_ObsCont := RetiraTag(Txt_ObsCont, sWeb_ObsCont);
    //
    repeat
      J1 := pos(AnsiUpperCase('Campo'), AnsiUpperCase(Txt_ObsCont));
      //
      if (J1 = 1) then
      begin
        if DefValDeText(Valor, Txt_ObsCont, 'Campo') then
        begin
          cObsCont := cXML.InfNFe.infAdic.ObsCont.Add;
          cObsCont.XCampo := Valor;
          Texto := '';
          repeat
            J2 := pos(AnsiUpperCase('Texto'), AnsiUpperCase(Txt_ObsCont));
            if J2 = 1 then
            begin
              if DefValDeText(Valor, Txt_ObsCont, 'Texto') then
              Texto := Texto + ' ' + Valor;
            end;
          until (J2 <> 1);
          cObsCont.XTexto := Texto;
        end;
      end;
    until (J1 <> 1);
    //
    if Trim(Txt_ObsCont) <> '' then
      AvisaTextoNaoProcessado('401a # Z04', sWeb_ObsCont, Txt_ObsCont);
  end;


    if Trim(ArquivoTxt) <> '' then
      AvisaTextoNaoProcessado('0 # ***', 'Arquivo', ArquivoTxt);


  cXML.infNFe.Ide.mod_ := Copy(Geral.SoNumero_TT(cXML.infNFe.ID), 21, 2);
  cXML.infNFe.Ide.serie := Copy(Geral.SoNumero_TT(cXML.infNFe.ID), 23, 3);

  XML_Prot :=
'<protNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + versao + '">' +
'  <infProt Id="ID' + nProt + '">' +  // por dedu��o
'    <tpAmb>' + '1' + '</tpAmb>' +  // por dedu��o
'    <verAplic>' + 'NULL' + '</verAplic>' + // Como saber?
'    <chNFe>' + cXML.infNFe.ID + '</chNFe>' +
'    <dhRecbto>' + dhRecbto + '</dhRecbto>' +
'    <nProt>' + nProt + '</nProt>' +
'    <digVal>' + digVal + '</digVal>' +
'    <cStat>' + cStat + '</cStat>' +
'    <xMotivo>' + xMotivo + '</xMotivo>' +
'  </infProt>' +
'</protNFe>';

  //
  Status := Geral.IMV(cStat);
  if not DModG.ObtemEntidadeDeCNPJCFP(cXML.InfNFe.Dest.CNPJ + cXML.InfNFe.Dest.CPF,
  Empresa) then
    Geral.MensagemBox('O CNPJ/CPF ' + cXML.InfNFe.Dest.CNPJ + cXML.InfNFe.Dest.CPF +
    ' n�o pertence a uma entidade (Empresa ou filial) cadastrada!', 'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    Result := DModG.ReopenEntiCliInt(Empresa);
    if not Result then
      Geral.MensagemBox('O CNPJ/CPF ' + cXML.InfNFe.Dest.CNPJ + cXML.InfNFe.Dest.CPF +
      ' Empresa: "' + FormatFloat('0', Empresa) + '" n�o � uma Empresa ou filial v�lida!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
  // Provis�rio
  Result := True;
end;

{ N�o uso
function TNFeBaixaXMLdaWeb_0200.LastPos(SubStr, Str: String): Integer;
var
  I: Integer;
  Txt: String;
begin
  Result := 0;
  Txt := Str;
  repeat
    I :=  pos(SubStr, Txt);
    if I > 0 then
    begin
      Result := Result + I;
      Txt := Copy(Str, Result + Length(Substr));
    end;
  until I = 0;
end;
}

function TNFeBaixaXMLdaWeb_0200.LerCampoA(var Texto: String;
  const NomeCampo: string; const Tamanho: Integer): string;
var
  ConteudoTag, nC: string;
  Inicio, Fim: integer;
begin
  nC := AnsiUpperCase(Trim(NomeCampo));
  Inicio := pos(nC, AnsiUpperCase(Texto));
  if Inicio = 0 then
    ConteudoTag := sCampoNulo
  else
  begin
    if Inicio > 1 then
    begin
      Geral.MensagemBox('Texto n�o consumido:' + #13#10 +
      'Antes do campo: "' + NomeCampo + '"' + #13#10 + 'Texto:' + #13#10 +
      Copy(Texto, 1, Inicio - 1) + #13#10 + #13#10 + 'AVISE A DERMATEK!',
      'AVISE A DERMATEK!', MB_OK+MB_ICONWARNING);
    end;
    Inicio := Inicio + Length(nC);
    if Tamanho > 0 then
      Fim := Tamanho
    else
    begin
      Texto := copy(Texto, Inicio, Length(Texto));
      Inicio := 0;
      Fim := pos('|&|',Texto)-1;
    end;
    if Fim = -1 then
    begin
      ConteudoTag := Trim(copy(Texto, Inicio));
      Texto := Copy(Texto, Length(ConteudoTag + sPipe));
    end else
    begin
      ConteudoTag := Trim(copy(Texto, Inicio, Fim));
      Texto := Copy(Texto, pos('|&|',Texto) + 3);
    end;
  end;
  try
     Result := ConteudoTag;
  except
     raise Exception.Create('Conte�do inv�lido. ' + ConteudoTag);
  end;
end;

function TNFeBaixaXMLdaWeb_0200.RecuperarXMLdaWeb(
  TextoHTML: String): String;
var
  strChaveAcesso: String;
  Protocolo, XML_Distribuicao: String;
  Empresa, Status: Integer;
  Protocolar: Boolean;
  Versao: Double;
  ReciAmbNacional: TDateTime;
begin
  Result := '';
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetNFe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)
  if UnNFeBaixaXMLdaWeb_0200.GerarXML_Da_WEB_2(
  TextoHTML, Protocolo, strChaveAcesso, Empresa, Status, versao, ReciAmbNacional) then
  begin
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    Protocolar := True;
    //
    if NFeXMLGeren.XML_DistribuiNFe(strChaveAcesso, Status, arqXML.XML.Text,
    Protocolo, '', Protocolar, XML_Distribuicao, Versao,
    'nas informa��es obtidas da Web') then
      Result := DmNFe_0000.SalvaXML(NFE_EXT_NFE_WEB_XML, strChaveAcesso,
        XML_Distribuicao, nil, False);
  end;
  arqXML := nil;
end;

function TNFeBaixaXMLdaWeb_0200.RemoveGrupo(Chave, Texto: String;
  var Grupo, Restante: String; Avisa: Boolean): Boolean;
var
  I: integer;
  C, T: String;
begin
  Result := False;
  C := AnsiUpperCase(Trim(sPipe + Chave + sPipe));
  T := AnsiUpperCase(Texto);
  I := pos(C, T);
  //
  if I = 0 then
  begin
    Grupo := '';
    Restante := Texto;
    //
    if Avisa then
    Geral.MensagemBox('N�o h� resultado para "Remove Grupo" no texto abaixo:' +
    #13#10 + #13#10 + 'Separador: "' + Chave + '"' + #13#10 + #13#10 + '"' +
    Texto + '"', 'AVISO - AVISE A DERMATEK', MB_OK+MB_ICONWARNING);
    //
  end else
  begin
    Grupo  := Copy(Texto, I);
    Restante := Copy(Texto, 1, I - 1);
    if Length(Restante) > 3 then
      if Copy(Restante, Length(Restante)-3) <> sPipe then
        Restante := Restante + sPipe;
    //
    Result := True;
  end;
end;

function TNFeBaixaXMLdaWeb_0200.RetiraTag(Texto, Tag: String): String;
begin
  Result := Copy(Texto, Length(sPipe + Tag + sPipe));
  //
  if Result <> '' then
  begin
    if Result[1] = '|' then
      Result := Copy(Result, 2);
  end;
end;

function TNFeBaixaXMLdaWeb_0200.RetiraTxt(Texto, Txt: String): String;
var
  p: Integer;
begin
  p := pos(Txt, Texto);
  if p = 1 then
    Result := Copy(Texto, Length(Txt) + 1)
  else
  begin
    Result := Texto;
    if p > 0 then
      Geral.MensagemBox(
      'O texto abaixo n�o est� na posi��o "1" conforme esperado!' + #13#10 +
      '"' + Txt + '"', 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

function TNFeBaixaXMLdaWeb_0200.RetornarCodigoNumerico(Chave,
  Versao: String): String;
var
  Chv: String;
begin
  Chv := Geral.SoNumero_TT(Chave);
  //
  if Geral.DMV(Versao) < 2 then
     Result := Copy(Chv, 35, 9)
  else
     Result := Copy(Chv, 36, 8);
end;

function TNFeBaixaXMLdaWeb_0200.SeparaApartirDe(Chave,
  Texto: String; Avisa: Boolean): String;
var
  I: integer;
  C, T: String;
begin
  C := AnsiUpperCase(Trim(Chave));
  T := AnsiUpperCase(Texto);
  I := pos(C, T);
  //
  if I = 0 then
  begin
    Result := Texto;
    if Avisa then Geral.MensagemBox(
    'N�o h� separa��o para "Separar a partir de..." no texto abaixo:' +
    #13#10 + #13#10 + 'Separador: "' + Chave + '"' + #13#10 + #13#10 + Texto,
    'AVISO - AVISE A DERMATEK', MB_OK+MB_ICONWARNING);
  end else
  begin
    Result := Copy(Texto, I);
  end;
end;

function TNFeBaixaXMLdaWeb_0200.SeparaAte(Chave, Texto: String;
  var Resto: String; Avisa: Boolean): String;
var
  I: integer;
  C, T: String;
begin
  C := AnsiUpperCase(Trim(Chave));
  T := AnsiUpperCase(Texto);
  I := pos(C, T);
  //
  if I = 0 then
  begin
    Result := '';
    Resto := Texto;
    //
    Geral.MensagemBox('N�o h� resultado para "Separar at�..." no texto abaixo:' +
    #13#10 + #13#10 + 'Separador: "' + Chave + '"' + #13#10 + #13#10 + Texto,
    'AVISO - AVISE A DERMATEK', MB_OK+MB_ICONWARNING);
    //
  end else
  begin
    Resto  := Copy(Texto, I);
    Result := Copy(Texto, 1, I - 1);
  end;
end;

end.
