unit NFeLeXML_0200;

interface

uses
{
 Windows, Forms, SysUtils, Classes, Controls, ComCtrls, StdCtrls, DB, DBClient,
 XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, dmkGeral, Variants;
}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkImage, dmkEditDateTimePicker, OleCtrls, SHDocVw,
  (*XML*)XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB,(*FIM XML*)
  Variants, dmkCheckBox, dmkDBGrid, dmkMemo, dmkRadioGroup, frxClass,
  frxDBSet, UnDmkProcFunc;

type
  //TResLeXML = (rlxOK, rlxNewInfo, rlxErr);
  TTipoAcaoArq = (taaUnknown, taaAny, retConsSitNFe);
  TNFeLeXML_0200 = class(TObject)
  private
    { Private declarations }
    //
  public
    { Public declarations }
    function  DomToTree(const Nivel: Integer; const XmlNode: IXMLNode;
              var SQL: String; Memo: TMemo): Boolean;
    function  LeXML(Acao: TTipoAcaoArq; XMLDoc: TXMLDocument; Ext, Arq: String;
              XML: WideString; MeAviso: TMemo; Assinado: Boolean;
              MemoTeste: TMemo): Boolean;
    function  ArquivoCorreto(const NomeNoh: String): Boolean;
    function  ObtemCampoDeNoh(NomeNoh: String): String;
    function  ObtemValorDeNoh(NomeNoh, ValorNoh: String): String;
    procedure zzz();
  end;

var
  UnNFeLeXML_0200: TNFeLeXML_0200;

implementation

uses ModuleNFe_0000, Principal;

var
  FAcao: TTipoAcaoArq;
  //FNivel: Integer;
  //FCampo: String;
  //FValor: String;
{
function TDmod.InsereDadosTabela(IdxTable: Integer): Boolean;
  procedure DomToTree(XmlNode: IXMLNode; Tabl: TABSTable);
  var
    I: Integer;
    AttrNode: IXMLNode;
    NomeNoh, Noh: String;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;

    // add the node itself
    //if XmlNode.NodeName = 'detNadador' then
    NomeNoh := Lowercase(XmlNode.NodeName);
    Noh := Copy(NomeNoh, 1, 3);
    //
    if Noh = 'fld' then
      SetaValorCampo(Tabl, NomeNoh, XmlNode.NodeValue)
    else
    if Noh = 'det' then
    begin
      if FTbTabls[IdxTable].State = dsInsert then
        FTbTabls[IdxTable].Post;
      FTbTabls[IdxTable].Insert;
    end else
    if Noh = 'fim' then
    begin
      if FTbTabls[IdxTable].State = dsInsert then
        FTbTabls[IdxTable].Post;
    end;
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      NomeNoh := Lowercase(AttrNode.NodeName);
      Noh := Copy(NomeNoh, 1, 3);
      //
      if Noh = 'fld' then
        SetaValorCampo(Tabl, NomeNoh, AttrNode.NodeValue)
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree(xmlNode.ChildNodes.Nodes[I], Tabl);
  end;
var
  Arquivo: String;
  xmlNodeB, xmlNodeC: IXMLNode;
  //
  j: Integer;
  X: IXMLNode;
  MyCursor: TCursor;
begin
  //Result := False;
  Arquivo := NomeCompletoArquivo(FTbNomes[IdxTable]);
  if not FileExists(Arquivo) then
    //Result :=
    CriaTabelaVazia(IdxTable);
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    XMLDocument.Active := False;
    XMLDocument.FileName := Arquivo;
    XMLDocument.Active := True;
    //XMLDocument.DocumentElement.NodeValue;                         // Erro
    xmlNodeB := XMLDocument.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
      begin
        X := xmlNodeB.AttributeNodes.Nodes[J];
      end;
      //
      xmlNodeC := xmlNodeB.ChildNodes.First;
      while xmlNodeC <> nil do
      begin
        //N := N + 1;
        DomToTree(xmlNodeC, FTbTabls[IdxTable]);
        xmlNodeC := xmlNodeC.NextSibling;
      end;
      xmlNodeB := xmlNodeB.NextSibling;
    end;
    //
    // N�o precisa mais ?
    if FTbTabls[IdxTable].State = dsInsert then
      FTbTabls[IdxTable].Post;
    Result := True;
  finally
    Screen.Cursor := MyCursor;
  end;
end;
}

{ TNFeLeXML_0200 }

{
procedure TNFeLeXML_0200.DomToTree(XmlNode: IXMLNode);
var
  I: Integer;
  AttrNode: IXMLNode;
  NodeText, NomeNoh, ValrNoh, Noh, ntNome: String;
begin
  //Geral.MensagemBox('Collection.Count' + IntToStr(XmlNode.Collection.Count), 'Aviso', MB_OK+MB_ICONWARNING);
  // skip text nodes and other special cases
  case XmlNode.NodeType  of
    ntReserved:        ntNome := 'ntReserved:       ';
    ntElement:         ntNome := 'ntElement:        ';
    ntAttribute:       ntNome := 'ntAttribute:      ';
    ntText:            ntNome := 'ntText:           ';
    ntCData:           ntNome := 'ntCData:          ';
    ntEntityRef:       ntNome := 'ntEntityRef:      ';
    ntEntity:          ntNome := 'ntEntity:         ';
    ntProcessingInstr: ntNome := 'ntProcessingInstr:';
    ntComment:         ntNome := 'ntComment:        ';
    ntDocument:        ntNome := 'ntDocument:       ';
    ntDocType:         ntNome := 'ntDocType:        ';
    ntDocFragment:     ntNome := 'ntDocFragment:    ';
    ntNotation:        ntNome := 'ntNotation:       ';
  end;

//  if not (XmlNode.NodeType = ntElement) then  begin
    NodeText := 'ntNome = ' + ntNome + #13#10 +
    XmlNode.NodeName + ' = ' + XmlNode.NodeValue + #13#10 +
    'LocalName = ' + XmlNode.LocalName + #13#10 +
    'XmlNode = ' + XmlNode.Prefix + #13#10 +
    'Text = ' + XmlNode.Text + #13#10 +
    'XML = ' + XmlNode.XML;
    Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
    //Exit;
//  end;

  // add the node itself
  //if XmlNode.NodeName = 'detNadador' then
(*
  NomeNoh := Lowercase(XmlNode.NodeName);
  ValrNoh := XmlNode.NodeValue;
  Geral.MensagemBox('Nome: ' + NomeNoh + #13#10 + 'Valor: ' + ValrNoh,
  'Aviso', MB_OK+MB_ICONWARNING);
*)
(*
  Noh := Copy(NomeNoh, 1, 3);
  //
  if Noh = 'fld' then
    SetaValorCampo(Tabl, NomeNoh, XmlNode.NodeValue)
  else
  if Noh = 'det' then
  begin
    if FTbTabls[IdxTable].State = dsInsert then
      FTbTabls[IdxTable].Post;
    FTbTabls[IdxTable].Insert;
  end else
  if Noh = 'fim' then
  begin
    if FTbTabls[IdxTable].State = dsInsert then
      FTbTabls[IdxTable].Post;
  end;
*)

  // add attributes
  for I := 0 to xmlNode.AttributeNodes.Count - 1 do
  begin
    AttrNode := xmlNode.AttributeNodes.Nodes[I];
    NomeNoh := Lowercase(AttrNode.NodeName);
    ValrNoh := AttrNode.NodeValue;
    Geral.MensagemBox('Nome: ' + NomeNoh + #13#10 + 'Valor: ' + ValrNoh,
    'Aviso', MB_OK+MB_ICONWARNING);
(*
    Noh := Copy(NomeNoh, 1, 3);
    //
    if Noh = 'fld' then
      SetaValorCampo(Tabl, NomeNoh, AttrNode.NodeValue)
*)
  end;
  // add each child node
  if XmlNode.HasChildNodes then
    for I := 0 to xmlNode.ChildNodes.Count - 1 do
      DomToTree(xmlNode.ChildNodes.Nodes[I]);
end;
}

{
function TDmod.InsereDadosTabela(IdxTable: Integer): Boolean;
  procedure DomToTree(XmlNode: IXMLNode; Tabl: TABSTable);
  var
    I: Integer;
    AttrNode: IXMLNode;
    NomeNoh, Noh: String;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;

    // add the node itself
    //if XmlNode.NodeName = 'detNadador' then
    NomeNoh := Lowercase(XmlNode.NodeName);
    Noh := Copy(NomeNoh, 1, 3);
    //
    if Noh = 'fld' then
      SetaValorCampo(Tabl, NomeNoh, XmlNode.NodeValue)
    else
    if Noh = 'det' then
    begin
      if FTbTabls[IdxTable].State = dsInsert then
        FTbTabls[IdxTable].Post;
      FTbTabls[IdxTable].Insert;
    end else
    if Noh = 'fim' then
    begin
      if FTbTabls[IdxTable].State = dsInsert then
        FTbTabls[IdxTable].Post;
    end;
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      NomeNoh := Lowercase(AttrNode.NodeName);
      Noh := Copy(NomeNoh, 1, 3);
      //
      if Noh = 'fld' then
        SetaValorCampo(Tabl, NomeNoh, AttrNode.NodeValue)
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree(xmlNode.ChildNodes.Nodes[I], Tabl);
  end;
var
  Arquivo: String;
  xmlNodeB, xmlNodeC: IXMLNode;
  //
  j: Integer;
  X: IXMLNode;
  MyCursor: TCursor;
begin
  //Result := False;
  Arquivo := NomeCompletoArquivo(FTbNomes[IdxTable]);
  if not FileExists(Arquivo) then
    //Result :=
    CriaTabelaVazia(IdxTable);
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    XMLDocument.Active := False;
    XMLDocument.FileName := Arquivo;
    XMLDocument.Active := True;
    //XMLDocument.DocumentElement.NodeValue;                         // Erro
    xmlNodeB := XMLDocument.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
      begin
        X := xmlNodeB.AttributeNodes.Nodes[J];
      end;
      //
      xmlNodeC := xmlNodeB.ChildNodes.First;
      while xmlNodeC <> nil do
      begin
        //N := N + 1;
        DomToTree(xmlNodeC, FTbTabls[IdxTable]);
        xmlNodeC := xmlNodeC.NextSibling;
      end;
      xmlNodeB := xmlNodeB.NextSibling;
    end;
    //
    // N�o precisa mais ?
    if FTbTabls[IdxTable].State = dsInsert then
      FTbTabls[IdxTable].Post;
    Result := True;
  finally
    Screen.Cursor := MyCursor;
  end;
end;
}

function TNFeLeXML_0200.ArquivoCorreto(const NomeNoh: String): Boolean;
var
  AcaoInformada: TTipoAcaoArq;
begin
  Result := True;
  if lowercase(NomeNoh) = 'retconssitnfe' then
  begin
    AcaoInformada := retConsSitNFe;
    //SQL := '';
  end else
  begin
    Result := False;
    AcaoInformada := taaUnknown;
    Geral.MensagemBox('N� principal desconhecido: "' + NomeNoh + '"',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Result then
  begin
    Result := (FAcao = taaAny) or (FAcao = AcaoInformada);
    if not Result then
    Geral.MensagemBox('N� principal n�o � o esperado: "' + NomeNoh + '"',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  FAcao := AcaoInformada;
end;

function TNFeLeXML_0200.DomToTree(const Nivel: Integer; const XmlNode: IXMLNode;
var SQL: String; Memo: TMemo): Boolean;
var
  //M
  I, J: Integer;
  //AttrNode: IXMLAttribute;
  //x: IXMLNodeList;
  y: IXMLNode;
  NomeNoh, ValorNoh, Txt: String;
  //Texto: WideString;
begin
  Result := False;
  // skip text nodes and other special cases
  if not (XmlNode.NodeType = ntElement) then
  begin
    //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
    //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
    Exit;
  end;

  // add the node itself
  NomeNoh := Lowercase(XmlNode.NodeName);
  if XmlNode.IsTextElement then
    ValorNoh := XmlNode.NodeValue
  else
    ValorNoh := '';
  if Memo <> nil then
  begin
    Txt := 'N�vel:';
    for J := 1 to Nivel do
      Txt := Txt + #9;
    Txt := Txt + '[E][';
    Memo.Lines.Add(
      Txt + Geral.FF0(Nivel)+ '] ' + NomeNoh + ' = ' + ValorNoh);
  end;
  if Nivel = 1 then
  begin
    Result := ArquivoCorreto(NomeNoh); //retconssitnfe
    if not Result then
      Exit;
  end;
  if Nivel = 2 then
  begin
    SQL := SQL + ', ' + ObtemCampoDeNoh(NomeNoh) + '=' +
      ObtemValorDeNoh(NomeNoh, ValorNoh) + #13#10;
  end;
  if Nivel = 3 then
  begin
    // ??
  end;

  // add attributes
  for I := 0 to xmlNode.AttributeNodes.Count - 1 do
  begin
    //x := xmlNode.AttributeNodes[I];
    y := xmlNode.AttributeNodes[I];
    NomeNoh := y.NodeName;
    try
      ValorNoh := y.NodeValue;
    except
      ValorNoh := '';
    end;
    if Memo <> nil then
    begin
      Txt := 'N�vel:';
      for J := 1 to Nivel do
        Txt := Txt + #9;
      Txt := Txt + '[A][';
      Memo.Lines.Add(
        Txt + Geral.FF0(Nivel)+ '] ' + NomeNoh + ' = ' + ValorNoh);
    end;
  end;

  // 
  if XmlNode.HasChildNodes then
  begin
    for I := 0 to xmlNode.ChildNodes.Count - 1 do
    DomToTree(Nivel + 1, xmlNode.ChildNodes.Nodes[I], SQL, Memo);
  end;
end;

function TNFeLeXML_0200.LeXML(Acao: TTipoAcaoArq; XMLDoc: TXMLDocument;
  Ext, Arq: String; XML: WideString; MeAviso: TMemo; Assinado: Boolean;
  MemoTeste: TMemo): Boolean;
var
  //xmlNodeB, xmlNodeC,
  xmlNodeA: IXMLNode;
  //j: Integer;
  //X: IXMLNode;
  MyCursor: TCursor;
  //
  Arquivo: String;

  Stream: TMemoryStream;
  Linhas: TStringList;
  SQL: String;
begin
  Result := False;
  FAcao := Acao;
  Arquivo := DmNFe_0000.SalvaXML(Ext, Arq, XML, MeAviso, Assinado);
  if Not FileExists(Arquivo) then
  begin
    Result := False;
    Geral.MensagemBox('N�o foi poss�vel salvar / localizar o aqruivo:' +
    #13#10 + Arquivo, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;


  //XMLDocument := TXMLDocument.Create(nil);
  try
    XMLDoc.Active := False;
    Stream := TMemoryStream.Create;
    Linhas := TStringList.Create;
    try
      Linhas.Text := XML;
      Linhas.SaveToStream(Stream);
      try
        XMLDoc.LoadFromStream(Stream);
      except
        Exit;
      end;
    finally
      Stream.Free;
      Linhas.Free;
    end;
    //XMLDoc.FileName := 'C:\Dermatek\XML\SWMS\Data\Baleia Azul\nadadores.xml';
    XMLDoc.Active := True;
    //XMLDoc.DocumentElement.NodeValue;   // Erro
    xmlNodeA := XMLDoc.DocumentElement;
    SQL := '';
    DomToTree(1, xmlNodeA, SQL, MemoTeste);
    {
    //Geral.MensagemBox(SQL, 'SQL', MB_OK+MB_ICONWARNING);
    xmlNodeB := XMLDoc.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      DomToTree(2, xmlNodeB, SQL, MemoTeste);
      for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
      begin
        X := xmlNodeB.AttributeNodes.Nodes[J];
      end;
      //
      xmlNodeB := xmlNodeB.NextSibling;
    end;
    }
    //
    Result := True;
  finally
    //XMLDoc.Free;
    Screen.Cursor := MyCursor;
  end;
end;

function TNFeLeXML_0200.ObtemCampoDeNoh(NomeNoh: String): String;
begin
  Result := NomeNoh;
end;

function TNFeLeXML_0200.ObtemValorDeNoh(NomeNoh, ValorNoh: String): String;
begin
  Result := '"' + ValorNoh + '"';
end;


procedure TNFeLeXML_0200.zzz;
begin
{
N�vel:	[E][1] retconssitnfe = 
N�vel:	[A][1] versao = 2.01
N�vel:	[A][1] xmlns = http://www.portalfiscal.inf.br/nfe
N�vel:		[E][2] tpamb = 2
N�vel:		[E][2] veraplic = SVAN_4.02
N�vel:		[E][2] cstat = 101
N�vel:		[E][2] xmotivo = Cancelamento de NF-e homologado
N�vel:		[E][2] cuf = 15
N�vel:		[E][2] chnfe = 15120304333952000188550090000002031780004121
N�vel:		[E][2] retcancnfe = 
N�vel:		[A][2] versao = 2.00
N�vel:		[A][2] xmlns = http://www.portalfiscal.inf.br/nfe
N�vel:			[E][3] infcanc = 
N�vel:			[A][3] Id = ID415120000007429
N�vel:				[E][4] tpamb = 2
N�vel:				[E][4] veraplic = SVAN_4.02
N�vel:				[E][4] cstat = 101
N�vel:				[E][4] xmotivo = Cancelamento de NF-e homologado
N�vel:				[E][4] cuf = 15
N�vel:				[E][4] chnfe = 15120304333952000188550090000002031780004121
N�vel:				[E][4] dhrecbto = 2012-03-28T18:02:43
N�vel:				[E][4] nprot = 415120000007429
N�vel:		[E][2] proceventonfe = 
N�vel:		[A][2] xmlns = http://www.portalfiscal.inf.br/nfe
N�vel:		[A][2] versao = 1.00
N�vel:			[E][3] evento = 
N�vel:			[A][3] xmlns = http://www.portalfiscal.inf.br/nfe
N�vel:			[A][3] versao = 1.00
N�vel:				[E][4] infevento = 
N�vel:				[A][4] Id = ID1101101512030433395200018855009000000203178000412101
N�vel:					[E][5] corgao = 15
N�vel:					[E][5] tpamb = 2
N�vel:					[E][5] cnpj = 04333952000188
N�vel:					[E][5] chnfe = 15120304333952000188550090000002031780004121
N�vel:					[E][5] dhevento = 2012-03-28T18:01:38-03:00
N�vel:					[E][5] tpevento = 110110
N�vel:					[E][5] nseqevento = 1
N�vel:					[E][5] verevento = 1.00
N�vel:					[E][5] detevento = 
N�vel:					[A][5] versao = 1.00
N�vel:						[E][6] descevento = Carta de Correcao
N�vel:						[E][6] xcorrecao = TESTE DE CARAT DE CORRECAO DE NF-E QUE SERA EXCLUIDA PARA GERAR HISTORICO DE AUTORIZACAO, CARTA DE CORRECAO E CANCELAMENTO.
N�vel:						[E][6] xconduso = A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
N�vel:				[E][4] signature = 
N�vel:				[A][4] xmlns = http://www.w3.org/2000/09/xmldsig#
N�vel:					[E][5] signedinfo = 
N�vel:						[E][6] canonicalizationmethod = 
N�vel:						[A][6] Algorithm = http://www.w3.org/TR/2001/REC-xml-c14n-20010315
N�vel:						[E][6] signaturemethod = 
N�vel:						[A][6] Algorithm = http://www.w3.org/2000/09/xmldsig#rsa-sha1
N�vel:						[E][6] reference = 
N�vel:						[A][6] URI = #ID1101101512030433395200018855009000000203178000412101
N�vel:							[E][7] transforms = 
N�vel:								[E][8] transform = 
N�vel:								[A][8] Algorithm = http://www.w3.org/2000/09/xmldsig#enveloped-signature
N�vel:								[E][8] transform = 
N�vel:								[A][8] Algorithm = http://www.w3.org/TR/2001/REC-xml-c14n-20010315
N�vel:							[E][7] digestmethod = 
N�vel:							[A][7] Algorithm = http://www.w3.org/2000/09/xmldsig#sha1
N�vel:							[E][7] digestvalue = tu/9PWqdl29Q5hSvYJ0ZXEQ5NPc=
N�vel:					[E][5] signaturevalue = frOqyp2n0mxoJq95IfPF8J5R3DryWeYjb7mpFLrLQnz8vo67T390RDtJnExkuMi2HhsjFyFgsWM87kXKv8xKgJiwi8Zh05r2BtdfEDAB6QC216/45F4p7+aoHzomT4DCMzL4fMTbQCvdAxyA19wRD0fyfKnuptd1ZZiUT7A6tA0=
N�vel:					[E][5] keyinfo = 
N�vel:						[E][6] x509data = 
N�vel:							[E][7] x509certificate = MIIGwzCCBau....
N�vel:			[E][3] retevento = 
N�vel:			[A][3] versao = 1.00
N�vel:			[A][3] xmlns = http://www.portalfiscal.inf.br/nfe
N�vel:				[E][4] infevento = 
N�vel:				[A][4] Id = ID415120000007428
N�vel:					[E][5] tpamb = 2
N�vel:					[E][5] veraplic = SVAN_4.02
N�vel:					[E][5] corgao = 15
N�vel:					[E][5] cstat = 135
N�vel:					[E][5] xmotivo = Evento registrado e vinculado a NF-e
N�vel:					[E][5] chnfe = 15120304333952000188550090000002031780004121
N�vel:					[E][5] tpevento = 110110
N�vel:					[E][5] nseqevento = 1
N�vel:					[E][5] dhregevento = 2012-03-28T18:02:11-03:00
N�vel:					[E][5] nprot = 415120000007428

}
end;

end.
