unit UnitNFeImporta_0200;

interface

uses Forms, Windows, Controls, Dialogs, SysUtils, dmkGeral, Entidade2, OmniXML,
  OmniXMLUtils, ModuleNFe_0000, UnitMyXML, MyDBCheck, ModuleGeral,
  UMySQLModule, Module, GraGruE_P, UnDmkProcFunc, UnDmkEnums;

type
  TUnitNFeImporta = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }

    //  Importa��o de NFe de terceiros
    function ImportaDadosNFeDeXML(const Form: TForm; FatID: Integer; var FatNum,
             Empresa, CodInfoEmit, Transporta, _qVol: Integer;
             _PesoB, _PesoL: Double; var DataFiscal: TDateTime): Boolean;

  end;

  var
  UnNFeImporta: TUnitNFeImporta;

implementation

uses UnMyObjects, UnMLAGeral;

function TUnitNFeImporta.ImportaDadosNFeDeXML(const Form: TForm; FatID: Integer;
var FatNum, Empresa, CodInfoEmit, Transporta, _qVol: Integer; _PesoB, _PesoL:
Double; var DataFiscal: TDateTime): Boolean;
  function ExcluirEntrada(Form: TForm; FatID, FatNum, Empresa: Integer; Avisa: Boolean): Boolean;
  begin
    DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa, True);
    //

    if Form.Name = 'FmPQE' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM pqe WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else
    if Form.Name = 'FmMPIn' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpin WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mpinits WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else
    if Form.Name = 'FmStqInnCad' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqinncad WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := FatNum;
      Dmod.QrUpd.ExecSQL;
    end else Geral.MensagemBox('A janela "' + Form.Name +
    '" n�o est� implementada na function "ExcluirEntrada" da function "' +
    'TUnitNFeImporta.ImportaDadosNFeDeXML()"!' + #13#10 + 'AVISA DERMATEK',
    'ERRO', MB_OK+MB_ICONERROR);
    //
    Result := True;
    //
    Screen.Cursor := crHourGlass;
    if Avisa then
    Geral.MensagemBox('Ocorreu um erro na importa��o do XML.' + #13#10 +
    'Os dados da entrada foram exclu�dos por serem parciais!',
    'Aviso', MB_OK+MB_ICONERROR);
  end;
var
  Arquivo: String;
  xmlDocA, xmlDocB : IXMLDocument;
  xmlNodeA, xmlNodeB: IXMLNode;
  xmlList: IXMLNodeList;
  //
  Texto,
  infNFe_versao, infNFe_Id,

ide_cUF, ide_cNF,
ide_natOp, ide_indPag, ide_mod,
ide_serie, ide_nNF, ide_dEmi,
ide_dSaiEnt, ide_tpNF, ide_cMunFG,
ide_tpImp, ide_tpEmis, ide_cDV,
ide_tpAmb, ide_finNFe, ide_procEmi,
ide_verProc,

emit_CNPJ, emit_CPF,
emit_xNome, emit_xFant, emit_xLgr,
emit_nro, emit_xCpl, emit_xBairro,
emit_cMun, emit_xMun, emit_UF,
emit_CEP, emit_cPais, emit_xPais,
emit_fone, emit_IE, emit_IEST,
emit_IM, emit_CNAE, dest_CNPJ,
dest_CPF, dest_xNome, dest_xLgr,
dest_nro, dest_xCpl, dest_xBairro,
dest_cMun, dest_xMun, dest_UF,
dest_CEP, dest_cPais, dest_xPais,
dest_fone, dest_IE, dest_ISUF,

modFrete,
Transporta_CNPJ, Transporta_CPF, Transporta_xNome,
Transporta_IE, Transporta_xEnder, Transporta_xMun,
Transporta_UF,

ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,
ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ

,vol_pesoL
,vol_pesoB
,vol_qVol
,vol_esp
,vol_marca
,vol_nVol

,prod_cProd
,prod_cEAN
,prod_xProd
,prod_NCM
,prod_EX_TIPI
,prod_Genero
,prod_CFOP
,prod_uCom
,prod_qCom
,prod_vUnCom
,prod_vProd
,prod_cEANTrib
,prod_uTrib
,prod_qTrib
,prod_vUnTrib
,prod_vFrete
,prod_vSeg
,prod_vDesc

,ICMS_orig
,ICMS_CST
,ICMS_modBC
,ICMS_vBC
,ICMS_pRedBC
,ICMS_pICMS
,ICMS_vICMS
,ICMS_modBCST
,ICMS_pMVAST
,ICMS_pRedBCST
,ICMS_vBCST
,ICMS_pICMSST
,ICMS_vICMSST

,IPI_cEnq
,IPI_CST
,IPI_clEnq
,IPI_CNPJProd
,IPI_cSelo
,IPI_qSelo
,IPI_vBC
,IPI_qUnid
,IPI_vUnid
,IPI_pIPI
,IPI_vIPI

,PIS_CST
,PIS_vBC
,PIS_pPIS
,PIS_vPIS
,PIS_qBCProd
,PIS_vAliqProd
,PISST_CST
,PISST_vBC
,PISST_pPIS
,PISST_vPIS
,PISST_qBCProd
,PISST_vAliqProd

,COFINS_CST
,COFINS_vBC
,COFINS_pCOFINS
,COFINS_vCOFINS
,COFINS_qBCProd
,COFINS_vAliqProd
,COFINSST_CST
,COFINSST_vBC
,COFINSST_pCOFINS
,COFINSST_vCOFINS
,COFINSST_qBCProd
,COFINSST_vAliqProd

,infAdProd

,ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS, RetTrib_vRetPIS,
RetTrib_vRetCOFINS, RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
RetTrib_vBCRetPrev, RetTrib_vRetPrev,

RetTransp_vServ, RetTransp_vBCRet,
RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
Cobr_Fat_vDesc, Cobr_Fat_vLiq,

InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
Compra_XNEmp, Compra_XPed, Compra_XCont,

protNFe_versao, infProt_Id, infProt_tpAmb,
infProt_verAplic, infProt_dhRecbto, infProt_nProt,
infProt_digVal, infProt_cStat, infProt_xMotivo,

Status, nItem: WideString;

ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq,
ICMSRec_vICMS, IPIRec_pRedBC, IPIRec_vBC,
IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
PISRec_vBC, PISRec_pAliq, PISRec_vPIS,
COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
COFINSRec_vCOFINS: Double;


  LoteEnv ,IDCtrl ,its_qVol ,its_Volumes, Tipo, Nivel1, cab_TipoNF, cab_Pedido,
  cab_Conhecimento, Controle, Conta: Integer;
  Msg, cab_refNFe, cab_modNF, cab_Serie, cab_NF, cab_DataE, cab_DataS, cab_Data,
  cab_Cancelado: String;
  _x_
  ,cab_ICMS
  ,cab_ValProd
  ,cab_Seguro
  ,cab_Desconto
  ,cab_IPI
  ,cab_PIS
  ,cab_COFINS
  ,cab_Frete
  ,cab_Outros
  ,cab_ValorNF
  ,cab_Juros
  ,cab_RICMS
  ,cab_RICMSF
  ,its_PesoVB
  ,its_PesoVL
  ,its_ValorItem
  ,its_RIPI
  ,its_CFin
  ,its_Frete
  ,its_Seguro
  ,its_Desconto
  ,its_TotalCusto
  ,its_TotalPeso
  ,its_IPI
  ,its_ICMS
  ,its_RICMS
  ,its_pesoL
  ,its_pesoB
  : Double;



  tot_Rec_ICMS, tot_Rec_IPI, tot_Rec_PIS, tot_Rec_COFINS: Double;

  MeuID, ResMsg, CodCad, Tem_IPI, _Ativo_, InfAdCuztm, EhServico: Integer;

begin
  Screen.Cursor := crHourGlass;
  Result := False;
  try
  //FatID                  := VAR_FATID_0051;
  //
  its_Volumes            := 1;
  Transporta             := 0;
  cab_Frete              := 0;
  cab_Seguro             := 0;
  cab_Desconto           := 0;
  cab_ICMS               := 0;
  cab_IPI                := 0;
  cab_PIS                := 0;
  cab_COFINS             := 0;
  cab_Outros             := 0;
  cab_ValorNF            := 0;
  cab_ValProd            := 0;
  Empresa                := 0;
  //
  ISSQNtot_vBC       := '';
  ISSQNtot_vISS      := '';
  ISSQNtot_vPIS      := '';
  ISSQNtot_vCOFINS   := '';
  RetTrib_vRetPIS    := '';
  RetTrib_vRetCOFINS := '';
  RetTrib_vRetCSLL   := '';
  RetTrib_vBCIRRF    := '';
  RetTrib_vIRRF      := '';
  RetTrib_vBCRetPrev := '';
  RetTrib_vRetPrev   := '';
  //
  RetTransp_vServ    := '';
  RetTransp_vBCRet   := '';
  RetTransp_PICMSRet := '';
  RetTransp_vICMSRet := '';
  RetTransp_CFOP     := '';
  RetTransp_CMunFG   := '';
  VeicTransp_Placa   := '';
  VeicTransp_UF      := '';
  VeicTransp_RNTC    := '';
  Cobr_Fat_nFat      := '';
  Cobr_Fat_vOrig     := '';
  Cobr_Fat_vDesc     := '';
  Cobr_Fat_vLiq      := '';
  //
  Exporta_UFEmbarq   := '';
  Exporta_XLocEmbarq := '';
  Compra_XNEmp       := '';
  Compra_XPed        := '';
  Compra_XCont       := '';
  //
  ICMSRec_pRedBC     := 0;
  ICMSRec_vBC        := 0;
  ICMSRec_pAliq      := 0;
  ICMSRec_vICMS      := 0;
  IPIRec_pRedBC      := 0;
  IPIRec_vBC         := 0;
  IPIRec_pAliq       := 0;
  IPIRec_vIPI        := 0;
  PISRec_pRedBC      := 0;
  PISRec_vBC         := 0;
  PISRec_pAliq       := 0;
  PISRec_vPIS        := 0;
  COFINSRec_pRedBC   := 0;
  COFINSRec_vBC      := 0;
  COFINSRec_pAliq    := 0;
  COFINSRec_vCOFINS  := 0;
  //
  if MyObjects.FileOpenDialog(Form, 'C:\Dermatek\NFE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
  begin
    if not UnMyXML.CarregaXML(Arquivo, Texto) then
      Exit;
    if UnMyXML.DefineXMLDoc(xmlDocA, Texto) then
    begin
      // Verifica se � envio de NFe
      xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc');
      if assigned(xmlNodeA) then
      begin
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//    NFE                                                                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        infNFe_versao := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_versao);
        infNFe_Id     := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
        //xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe');
        xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe');

        if assigned(xmlNodeA) then
        begin
          infNFe_Id          := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
          //
          cab_refNFe         := Copy(infNFe_Id, 4); // chave NFe
          cab_modNF          := Copy(cab_refNFe, 21, 2);
          cab_serie          := Copy(cab_refNFe, 23, 3);
          cab_NF             := Copy(cab_refNFe, 26, 9);
        end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//   IDENTIFICACAO NFE                                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/ide');
        if assigned(xmlNodeA) then
        begin
          ide_cUF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cUF    );
          ide_cNF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cNF    );
          ide_natOp   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_natOp  );
          ide_indPag  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_indPag );
          ide_mod     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_mod    );
          ide_serie   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_serie  );
          ide_nNF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nNF    );
          ide_dEmi    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dEmi   );
          ide_dSaiEnt := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dSaiEnt);
          ide_tpNF    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpNF   );
          ide_cMunFG  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMunFG );
          ide_tpImp   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpImp  );
          ide_tpEmis  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpEmis );
          ide_cDV     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cDV    );
          ide_tpAmb   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpAmb  );
          ide_finNFe  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_finNFe );
          ide_procEmi := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_procEmi);
          ide_verProc := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_verProc);
          //
          cab_DataE   := Copy(ide_dEmi    , 1, 10);
          cab_DataS   := Copy(ide_dSaiEnt , 1, 10);
        end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            EMIT - FORNECEDOR                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
        xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/emit');
        if assigned(xmlNodeA) then
        begin
          emit_CNPJ  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ   );
          emit_CPF   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF    );
          emit_xNome := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome  );
          emit_xFant := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xFant  );
          emit_IE    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE     );
          emit_IEST  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IEST   );
          emit_IM    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IM     );
          emit_CNAE  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNAE   );
          (*
          if emit_CNPJ = '' then emit_CNPJ := '???';
          if emit_CPF = '' then emit_CPF := '???';
          *)
          Tipo := -1;
          if emit_CNPJ <> '' then Tipo := 0 else
          if emit_CPF  <> '' then Tipo := 1;
          if Tipo = -1 then
          begin
            Geral.MensagemBox(
            'N�o foi poss�vel definir o tipo de pessoa do emitente da NF-e!',
            'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end else
          begin
            CodInfoEmit := DmNFe_0000.DefineEntidadePeloDoc(emit_CNPJ, emit_CPF);
            //
            if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
            begin
              Msg := 'O fornecedor abaixo n�o est� cadastrado:' + #13#10 +
              'Nome: ' + emit_xNome + #13#10 + 'Fantasia: ' + emit_xFant + #13#10;
              //if emit_CNPJ <> '???' then
              if emit_CNPJ <> '' then
                Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(emit_CNPJ) + #13#10;
              //if emit_CPF <> '???' then
              if emit_CPF <> '' then
                emit_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(emit_CPF) + #13#10;
              Msg := Msg + 'I.E.: ' + emit_IE + #13#10;
              Msg := Msg + #13#10 + 'Deseja cadastr�-lo?';
              if Geral.MensagemBox(Msg, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
              begin
                if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
                begin
                  FmEntidade2.Incluinovaentidade1Click(Self);
                  FmEntidade2.CadastroPorXML_NFe_1(entNFe_emit, Arquivo);
                  FmEntidade2.ShowModal;
                  FmEntidade2.Destroy;
                  //
                  CodInfoEmit := DmNFe_0000.DefineEntidadePeloDoc(emit_CNPJ, emit_CPF);
                end;
              end;
            end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            ENDERE�O EMITENTE                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
            xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/emit/enderEmit');
            if assigned(xmlNodeA) then
            begin
              emit_xLgr    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xLgr   );
              emit_nro     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nro    );
              emit_xCpl    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xCpl   );
              emit_xBairro := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xBairro);
              emit_cMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMun   );
              emit_xMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun   );
              emit_UF      := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF     );
              emit_CEP     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CEP    );
              emit_cPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cPais  );
              emit_xPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xPais  );
              emit_fone    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_fone   );
            end;
            if DmNFe_0000.QrEnt_Doc.RecordCount > 0 then
            begin
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            CLIENTE INTERNO                                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/dest');
              if assigned(xmlNodeA) then
              begin
                dest_CNPJ  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ   );
                dest_CPF   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF    );
                dest_xNome := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome  );
                dest_IE    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE     );
                dest_ISUF  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_ISUF   );
                {
                if dest_CNPJ = '' then dest_CNPJ := '???';
                if dest_CPF = '' then dest_CPF := '???';
                }
                Tipo := -1;
                if dest_CNPJ <> '' then Tipo := 0 else
                if dest_CPF  <> '' then Tipo := 1;
                if Tipo = -1 then
                begin
                  Msg := 'Cliente interno n�o est� cadastrado:' + #13#10 +
                  'Nome: ' + dest_xNome + #13#10;
                  //if dest_CNPJ <> '???' then
                  if dest_CNPJ <> '' then
                    Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(dest_CNPJ) + #13#10;
                  //if dest_CPF <> '???' then
                  if dest_CPF <> '' then
                    dest_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(dest_CPF) + #13#10;
                  Msg := Msg + 'I.E.: ' + dest_IE + #13#10;
                  Msg := Msg + #13#10 + 'Deseja cadastr�-lo';
                  Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
                  Exit;
                end else
                begin
                  Empresa := DmNFe_0000.DefineEntidadePeloDoc(dest_CNPJ, dest_CPF);
                  //
                  if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
                  begin
                    Geral.MensagemBox('Destinat�rio da NF-e n�o cadastrado!',
                    'Aviso', MB_OK+MB_ICONWARNING);
                    Exit;
                  end;
                end;
              end;
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/dest/enderDest');
              if assigned(xmlNodeA) then
              begin
                dest_xLgr    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xLgr   );
                dest_nro     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nro    );
                dest_xCpl    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xCpl   );
                dest_xBairro := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xBairro);
                dest_cMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cMun   );
                dest_xMun    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun   );
                dest_UF      := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF     );
                dest_CEP     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CEP    );
                dest_cPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cPais  );
                dest_xPais   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xPais  );
                dest_fone    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_fone   );
              end;
              {
              QrLocNFe.Close;
              QrLocNFe.SQL.Clear;
              QrLocNFe.SQL.Add('SELECT Codigo');
              QrLocNFe.SQL.Add('FROM pqe');
              QrLocNFe.SQL.Add('WHERE refNFe=:P0');
              QrLocNFe.Params[0].AsString := cab_refNFe;
              UMyMod.AbreQuery(DmNFe_0000.QrLocNFe, 'TUnitNFeImporta.ImportaDadosNFeDeXML()');
              if QrLocNFe.RecordCount = 0 then
              begin
              }
                DmNFe_0000.QrLocNFe.Close;
                {
                QrLocNFe.SQL.Clear;
                QrLocNFe.SQL.Add('SELECT FatNum Codigo');
                QrLocNFe.SQL.Add('FROM nfecaba');
                QrLocNFe.SQL.Add('WHERE Id=:P0');
                }
                DmNFe_0000.QrLocNFe.Params[0].AsString := cab_refNFe;
                UMyMod.AbreQuery(DmNFe_0000.QrLocNFe, Dmod.MyDB, 'TUnitNFeImporta.ImportaDadosNFeDeXML()');
              //end;
              CodCad := DmNFe_0000.QrLocNFeFatNum.Value;
              if CodCad > 0 then
              begin
                DataFiscal := DmNFe_0000.QrLocNFeDataFiscal.Value;
                ResMsg := ID_NO;
                ResMsg := Geral.MensagemBox('A NFe selecionada j� foi lan�ada ' +
                'no c�digo ' + IntToStr(CodCad) + '. Deseja lan�ar novamente?',
                'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
                if ResMsg = ID_YES then
                begin
                  ResMsg := Geral.MensagemBox('Tem certeza que deseja lan�ar ' +
                  'novamente o xml selecionado?' + #13#10 +
                  'Os dados j� lan�ados ser�o previamente exclu�dos!',
                  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
                end;
                if ResMsg = ID_YES then
                begin
                  ExcluirEntrada(Form, FatID, CodCad, Empresa, False);
                  FatNum := CodCad;
                end else Exit;
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            TRANSPORTADORA                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/transp');
              if assigned(xmlNodeA) then
              begin
                modFrete := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_modFrete);
              end;
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/transp/transporta');
              if assigned(xmlNodeA) then
              begin
                Transporta_CNPJ   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CNPJ);
                Transporta_CPF    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_CPF);
                Transporta_xNome  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xNome);
                Transporta_IE     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_IE);
                Transporta_xEnder := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xEnder);
                Transporta_xMun   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMun);
                Transporta_UF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_UF);
                {
                if Transporta_CNPJ = '' then Transporta_CNPJ := '???';
                if Transporta_CPF = '' then Transporta_CPF := '???';
                }
                Tipo := -1;
                if Transporta_CNPJ <> '' then Tipo := 0 else
                if Transporta_CPF  <> '' then Tipo := 1;
                if Tipo = -1 then
                begin
                  Geral.MensagemBox(
                  'N�o foi poss�vel definir o tipo de pessoa da transportadora!',
                  'Aviso', MB_OK+MB_ICONWARNING);
                  //Exit;
                end else
                begin
                  Transporta := DmNFe_0000.DefineEntidadePeloDoc(Transporta_CNPJ, Transporta_CPF);
                  //
                  if DmNFe_0000.QrEnt_Doc.RecordCount = 0 then
                  begin
                    Msg := 'A transportadora abaixo n�o est� cadastrada:' + #13#10 +
                    'Nome: ' + Transporta_xNome + #13#10;
                    //if Transporta_CNPJ <> '???' then
                    if Transporta_CNPJ <> '' then
                      Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(Transporta_CNPJ) + #13#10;
                    //if Transporta_CPF <> '???' then
                    if Transporta_CPF <> '' then
                      Transporta_CPF := 'CPF: ' + Geral.FormataCNPJ_TT(Transporta_CPF) + #13#10;
                    Msg := Msg + 'I.E.: ' + Transporta_IE + #13#10;
                    Msg := Msg + #13#10 + 'Deseja cadastr�-la?';
                    if Geral.MensagemBox(Msg, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
                    begin
                      if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
                      begin
                        FmEntidade2.Incluinovaentidade1Click(Self);
                        FmEntidade2.CadastroPorXML_NFe_1(entNFe_transp, Arquivo);
                        FmEntidade2.ShowModal;
                        FmEntidade2.Destroy;
                        //
                        Transporta := DmNFe_0000.DefineEntidadePeloDoc(Transporta_CNPJ, Transporta_CPF);
                      end;
                    end;
                  end;
                end;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            TOTAIS                                                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/total/ICMSTot');
              if assigned(xmlNodeA) then
              begin
                ICMSTot_vBC     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vBC    );
                ICMSTot_vICMS   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vICMS  );
                ICMSTot_vBCST   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vBCST  );
                ICMSTot_vST     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vST    );
                ICMSTot_vProd   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vProd  );
                ICMSTot_vFrete  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vFrete );
                ICMSTot_vSeg    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vSeg   );
                ICMSTot_vDesc   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vDesc  );
                ICMSTot_vII     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vII    );
                ICMSTot_vIPI    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vIPI   );
                ICMSTot_vPIS    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vPIS   );
                ICMSTot_vCOFINS := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vCOFINS);
                ICMSTot_vOutro  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vOutro );
                ICMSTot_vNF     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_vNF    );
                //
                cab_ICMS    := Geral.DMV_Dot(ICMSTot_vICMS);
                cab_ValProd := Geral.DMV_Dot(ICMSTot_vProd);
                cab_Frete   := Geral.DMV_Dot(ICMSTot_vFrete);
                cab_Seguro  := Geral.DMV_Dot(ICMSTot_vSeg);
                cab_Desconto:= Geral.DMV_Dot(ICMSTot_vDesc);
                cab_IPI     := Geral.DMV_Dot(ICMSTot_vIPI);
                cab_PIS     := Geral.DMV_Dot(ICMSTot_vPIS);
                cab_COFINS  := Geral.DMV_Dot(ICMSTot_vCOFINS);
                cab_Outros  := Geral.DMV_Dot(ICMSTot_vOutro);
                cab_ValorNF := Geral.DMV_Dot(ICMSTot_vNF);
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            VOLUMES - PESO                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              {
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/transp/vol');
              if assigned(xmlNodeA) then
              begin
                vol_qVol  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_qVol);
                vol_pesoL := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_pesoL);
                vol_pesoB := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_pesoB);
                //
                _qVol    := Geral.IMV(vol_qVol);
                _PesoL   := Geral.DMV_Dot(vol_PesoL);
                _PesoB   := Geral.DMV_Dot(vol_PesoB);
                //
              end else
              begin
                _qVol    := 0;
                _PesoL   := 0;
                _PesoB   := 0;
              end;
              cab_Data          := FormatDateTime('yyyy-mm-dd', Date); // Entrada
              cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
              //
              cab_Pedido        := 0;
              cab_Juros         := 0;
              cab_RICMS         := 0;
              cab_RICMSF        := 0;
              cab_Conhecimento  := 0;
              cab_Cancelado     := 'V';
              //
              }
              LoteEnv := 0;
              IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);


              // VOLUMES POR LISTA !!!


              _qVol    := 0;
              _PesoL   := 0;
              _PesoB   := 0;
              xmlList := xmlDocA.SelectNodes('/nfeProc/NFe/infNFe/transp/vol');
              if xmlList.Length > 0 then
              begin
                while xmlList.Length > 0 do
                begin
                  nItem     := UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem);
                  vol_qVol  := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_qVol);
                  vol_esp   := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_esp);
                  vol_marca := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_marca);
                  vol_nVol  := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nVol);
                  vol_pesoL := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_pesoL);
                  vol_pesoB := UnMyXML.LeNoXML(xmlList.Item[0], tnxTextStr, ttx_pesoB);
                  //
                  its_qVol  := Geral.IMV(vol_qVol);
                  its_pesoL := Geral.DMV_Dot(vol_PesoL);
                  its_pesoB := Geral.DMV_Dot(vol_PesoB);
                  //
                  _qVol    := _qVol  + its_qVol;
                  _PesoL   := _PesoL + its_pesoL;
                  _PesoB   := _PesoB + its_pesoB;
                  //
                  Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecabxvol', '', 0);
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
                  'qVol', 'esp', 'marca',
                  'nVol', 'pesoL', 'pesoB'], [
                  'FatID', 'FatNum', 'Empresa', 'Controle'], [
                  its_qVol, vol_esp, vol_marca,
                  vol_nVol, its_pesoL, its_pesoB], [
                  FatID, FatNum, Empresa, Controle], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

{
                  nItem    := UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem);
                  UnMyXML.DefineXMLDoc(xmlDocB, xmlList.Item[0].XML);
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/prod');
                  if assigned(xmlNodeB) then
                  begin

                  end
}
                  //
                  xmlList.Remove(xmlList.Item[0]);
                  //
                end;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            INFORMA��ES ADICIONAIS                                          //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/NFe/infNFe/infAdic');
              if assigned(xmlNodeA) then
              begin
                infAdic_infAdFisco := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_infAdFisco);
                infAdic_infCpl     := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_infCpl);
              end;
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            CONSULTA NFe                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/protNFe');
              if assigned(xmlNodeA) then
              begin
                protNFe_versao := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_versao);
              end;
              xmlNodeA := UnMyXML.SelecionaUmNo(xmlDocA, '/nfeProc/protNFe/infProt');
              if assigned(xmlNodeA) then
              begin
                infProt_Id       := UnMyXML.LeNoXML(xmlNodeA, tnxAttrStr, ttx_Id);
                Status           := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cStat);
                infProt_tpAmb    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_tpAmb   );
                infProt_verAplic := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_verAplic);
                infProt_dhRecbto := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_dhRecbto);
                infProt_nProt    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_nProt   );
                infProt_digVal   := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_digVal  );
                infProt_cStat    := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_cStat   );
                infProt_xMotivo  := UnMyXML.LeNoXML(xmlNodeA, tnxTextStr, ttx_xMotivo );
              end;

              //  Evitar erro de duplica��o
              //DmNFe_0000.ExcluiNfe(0(*Status*), FatID, FatNum, Empresa);
              //
              try
                if DataFiscal < 1000 then
                  if cab_DataS <> '' then
                    DataFiscal := dmkPF.ValidaDataDeSQL(cab_DataS, True) + 2;
                if DataFiscal < 1000 then
                  if cab_DataE <> '' then
                    DataFiscal := dmkPF.ValidaDataDeSQL(cab_DataE, True) + 2;
              finally
              end;
              if DataFiscal < 1000 then
                DataFiscal := Date;
              //
              DBCheck.ObtemData(DataFiscal, DataFiscal, 0);
              //
              if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
              'IDCtrl', 'LoteEnv', 'versao',
              'Id', 'ide_cUF', 'ide_cNF',
              'ide_natOp', 'ide_indPag', 'ide_mod',
              'ide_serie', 'ide_nNF', 'ide_dEmi',
              'ide_dSaiEnt', 'ide_tpNF', 'ide_cMunFG',
              'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
              'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
              'ide_verProc', 'emit_CNPJ', 'emit_CPF',
              'emit_xNome', 'emit_xFant', 'emit_xLgr',
              'emit_nro', 'emit_xCpl', 'emit_xBairro',
              'emit_cMun', 'emit_xMun', 'emit_UF',
              'emit_CEP', 'emit_cPais', 'emit_xPais',
              'emit_fone', 'emit_IE', 'emit_IEST',
              'emit_IM', 'emit_CNAE', 'dest_CNPJ',
              'dest_CPF', 'dest_xNome', 'dest_xLgr',
              'dest_nro', 'dest_xCpl', 'dest_xBairro',
              'dest_cMun', 'dest_xMun', 'dest_UF',
              'dest_CEP', 'dest_cPais', 'dest_xPais',
              'dest_fone', 'dest_IE', 'dest_ISUF',
              'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
              'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
              'ICMSTot_vSeg', 'ICMSTot_vDesc', 'ICMSTot_vII',
              'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
              'ICMSTot_vOutro', 'ICMSTot_vNF', 'ISSQNtot_vServ',
              'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
              'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
              'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
              'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
              'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
              'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
              'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
              'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
              'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
              'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
              'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
              'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
              'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
              'Status', 'infProt_Id', 'infProt_tpAmb',
              'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
              'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
              'protNFe_versao',
              (*
              'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
              'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
              'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
              'infCanc_xJust', '_Ativo_', 'FisRegCad',
              'CartEmiss', 'TabelaPrc', 'CondicaoPg',
              'CliFor', 'FreteExtra', 'SegurExtra',
              *)
              'ICMSRec_pRedBC', 'ICMSRec_vBC', 'ICMSRec_pAliq',
              'ICMSRec_vICMS', 'IPIRec_pRedBC', 'IPIRec_vBC',
              'IPIRec_pAliq', 'IPIRec_vIPI', 'PISRec_pRedBC',
              'PISRec_vBC', 'PISRec_pAliq', 'PISRec_vPIS',
              'COFINSRec_pRedBC', 'COFINSRec_vBC', 'COFINSRec_pAliq',
              'COFINSRec_vCOFINS',
              'CodInfoEmit', 'CodInfoDest', 'DataFiscal'], [
              'FatID', 'FatNum', 'Empresa'], [
              IDCtrl, LoteEnv, infNFe_versao,
              cab_refNFe, ide_cUF, ide_cNF,
              ide_natOp, ide_indPag, ide_mod,
              ide_serie, ide_nNF, ide_dEmi,
              ide_dSaiEnt, ide_tpNF, ide_cMunFG,
              ide_tpImp, ide_tpEmis, ide_cDV,
              ide_tpAmb, ide_finNFe, ide_procEmi,
              ide_verProc, emit_CNPJ, emit_CPF,
              emit_xNome, emit_xFant, emit_xLgr,
              emit_nro, emit_xCpl, emit_xBairro,
              emit_cMun, emit_xMun, emit_UF,
              emit_CEP, emit_cPais, emit_xPais,
              emit_fone, emit_IE, emit_IEST,
              emit_IM, emit_CNAE, dest_CNPJ,
              dest_CPF, dest_xNome, dest_xLgr,
              dest_nro, dest_xCpl, dest_xBairro,
              dest_cMun, dest_xMun, dest_UF,
              dest_CEP, dest_cPais, dest_xPais,
              dest_fone, dest_IE, dest_ISUF,
              ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
              ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
              ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,
              ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
              ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,
              ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
              ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
              RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
              RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
              Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
              Transporta_IE, Transporta_XEnder, Transporta_XMun,
              Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
              RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
              RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
              VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
              Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
              InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
              Compra_XNEmp, Compra_XPed, Compra_XCont,
              Status, infProt_Id, infProt_tpAmb,
              infProt_verAplic, infProt_dhRecbto, infProt_nProt,
              infProt_digVal, infProt_cStat, infProt_xMotivo,
              protNFe_versao,
              (*
              infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
              infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
              infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
              infCanc_xJust, _Ativo_, FisRegCad,
              CartEmiss, TabelaPrc, CondicaoPg,
              CliFor, FreteExtra, SegurExtra,
              *)
              ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq,
              ICMSRec_vICMS, IPIRec_pRedBC, IPIRec_vBC,
              IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC,
              PISRec_vBC, PISRec_pAliq, PISRec_vPIS,
              COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
              COFINSRec_vCOFINS,
              CodInfoEmit, Empresa(*CodInfoDest*), DataFiscal], [
              FatID, FatNum, Empresa], True) then
              begin
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                Exit;
              end;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//            ITENS DE PRODUTOS                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
              try
              xmlList := xmlDocA.SelectNodes('/nfeProc/NFe/infNFe/det');
              if xmlList.Length > 0 then
              begin
                while xmlList.Length > 0 do
                begin
                  prod_cProd         := '';
                  prod_cEAN          := '';
                  prod_xProd         := '';
                  prod_NCM           := '';
                  prod_EX_TIPI       := '';
                  prod_Genero        := '0';
                  prod_CFOP          := '';
                  prod_uCom          := '0';
                  prod_qCom          := '0';
                  prod_vUnCom        := '0';
                  prod_vProd         := '0';
                  prod_cEANTrib      := '';
                  prod_uTrib         := '';
                  prod_qTrib         := '0';
                  prod_vUnTrib       := '0';
                  prod_vFrete        := '0';
                  prod_vSeg          := '0';
                  prod_vDesc         := '0';

                  ICMS_orig          := '0';
                  ICMS_CST           := '0';
                  ICMS_modBC         := '0';
                  ICMS_vBC           := '0';
                  ICMS_pRedBC        := '0';
                  ICMS_pICMS         := '0';
                  ICMS_vICMS         := '0';
                  ICMS_modBCST       := '0';
                  ICMS_pMVAST        := '0';
                  ICMS_pRedBCST      := '0';
                  ICMS_vBCST         := '0';
                  ICMS_pICMSST       := '0';
                  ICMS_vICMSST       := '0';

                  IPI_cEnq           := '';
                  IPI_CST            := '';
                  IPI_clEnq          := '';
                  IPI_CNPJProd       := '';
                  IPI_cSelo          := '';
                  IPI_qSelo          := '';
                  IPI_cEnq           := '';
                  IPI_vBC            := '0';
                  IPI_qUnid          := '0';
                  IPI_vUnid          := '0';
                  IPI_pIPI           := '0';
                  IPI_vIPI           := '0';

                  PIS_CST            := '0';
                  PIS_vBC            := '0';
                  PIS_pPIS           := '0';
                  PIS_vPIS           := '0';
                  PIS_qBCProd        := '0';
                  PIS_vAliqProd      := '0';
                  PISST_CST          := '0';
                  PISST_vBC          := '0';
                  PISST_pPIS         := '0';
                  PISST_vPIS         := '0';
                  PISST_qBCProd      := '0';
                  PISST_vAliqProd    := '0';

                  COFINS_CST         := '0';
                  COFINS_vBC         := '0';
                  COFINS_pCOFINS     := '0';
                  COFINS_vCOFINS     := '0';
                  COFINS_qBCProd     := '0';
                  COFINS_vAliqProd   := '0';
                  COFINSST_CST       := '0';
                  COFINSST_vBC       := '0';
                  COFINSST_pCOFINS   := '0';
                  COFINSST_vCOFINS   := '0';
                  COFINSST_qBCProd   := '0';
                  COFINSST_vAliqProd := '0';

                  nItem    := UnMyXML.LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_nItem);
                  UnMyXML.DefineXMLDoc(xmlDocB, xmlList.Item[0].XML);
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/prod');
                  if assigned(xmlNodeB) then
                  begin
                    prod_cProd    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cProd    );
                    prod_cEAN     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEAN     );
                    prod_xProd    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_xProd    );
                    prod_NCM      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_NCM      );
                    prod_EX_TIPI  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_EXTIPI  );
                    prod_Genero   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_Genero   );
                    prod_CFOP     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CFOP     );
                    prod_uCom     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_uCom     );
                    prod_qCom     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qCom     );
                    prod_vUnCom   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnCom   );
                    prod_vProd    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vProd    );
                    prod_cEANTrib := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEANTrib );
                    prod_uTrib    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_uTrib    );
                    prod_qTrib    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qTrib    );
                    prod_vUnTrib  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnTrib  );
                    prod_vFrete   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vFrete   );
                    prod_vSeg     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vSeg     );
                    prod_vDesc    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vDesc    );
                  end else begin
                    prod_cProd    := '';
                    prod_cEAN     := '';
                    prod_xProd    := '';
                    prod_NCM      := '';
                    prod_EX_TIPI  := '';
                    prod_Genero   := '';
                    prod_CFOP     := '';
                    prod_uCom     := '';
                    prod_qCom     := '';
                    prod_vUnCom   := '';
                    prod_vProd    := '';
                    prod_cEANTrib := '';
                    prod_uTrib    := '';
                    prod_qTrib    := '';
                    prod_vUnTrib  := '';
                    prod_vFrete   := '';
                    prod_vSeg     := '';
                    prod_vDesc    := '';
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS00');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS10');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS20');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS30');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS40');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS41');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS50');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS51');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS60');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS70');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/ICMS/ICMS90');
                  if assigned(xmlNodeB) then
                  begin
                    ICMS_orig     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_orig    );
                    ICMS_CST      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST     );
                    ICMS_modBC    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_modBC   );
                    ICMS_vBC      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC     );
                    ICMS_pRedBC   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pRedBC  );
                    ICMS_pICMS    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pICMS   );
                    ICMS_vICMS    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMS   );
                    ICMS_modBCST  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_modBCST );
                    ICMS_pMVAST   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pMVAST  );
                    ICMS_pRedBCST := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pRedBCST);
                    ICMS_vBCST    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBCST   );
                    ICMS_pICMSST  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pICMSST );
                    ICMS_vICMSST  := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vICMSST );
                  end else begin
                    ICMS_orig     := '';
                    ICMS_CST      := '';
                    ICMS_modBC    := '';
                    ICMS_vBC      := '';
                    ICMS_pRedBC   := '';
                    ICMS_pICMS    := '';
                    ICMS_vICMS    := '';
                    ICMS_modBCST  := '';
                    ICMS_pMVAST   := '';
                    ICMS_pRedBCST := '';
                    ICMS_vBCST    := '';
                    ICMS_pICMSST  := '';
                    ICMS_vICMSST  := '';
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/IPI');
                  if assigned(xmlNodeB) then
                  begin
                    IPI_clEnq    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_clEnq   );
                    IPI_CNPJProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CNPJProd);
                    IPI_cSelo    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cSelo   );
                    IPI_qSelo    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qSelo   );
                    IPI_cEnq     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_cEnq);
                  end else begin
                    IPI_clEnq    := '';
                    IPI_CNPJProd := '';
                    IPI_cSelo    := '';
                    IPI_qSelo    := '';
                    IPI_cEnq     := '';
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/IPI/IPITrib');
                  if assigned(xmlNodeB) then
                  begin
                    IPI_CST      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    IPI_vBC      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    IPI_qUnid    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qUnid);
                    IPI_vUnid    := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vUnid);
                    IPI_pIPI     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pIPI);
                    IPI_vIPI     := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vIPI);
                  end else begin
                    IPI_CST      := '';
                    IPI_vBC      := '';
                    IPI_qUnid    := '';
                    IPI_vUnid    := '';
                    IPI_pIPI     := '';
                    IPI_vIPI     := '';
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/PIS/PISAliq');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/PIS/PISQtde');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/PIS/PISNT');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/PIS/PISOutr');
                  if assigned(xmlNodeB) then
                  begin
                    PIS_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    PIS_vBC       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    PIS_pPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS);
                    PIS_vPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS);
                    PIS_qBCProd   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd);
                    PIS_vAliqProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd);
                  end else begin
                    PIS_CST       := '';
                    PIS_vBC       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    PIS_pPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS);
                    PIS_vPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS);
                    PIS_qBCProd   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd);
                    PIS_vAliqProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd);
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/PISST');
                  if assigned(xmlNodeB) then
                  begin
                    PISST_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    PISST_vBC       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    PISST_pPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pPIS);
                    PISST_vPIS      := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vPIS);
                    PISST_qBCProd   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd);
                    PISST_vAliqProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd);
                  end else begin
                    PISST_CST       := '';
                    PISST_vBC       := '';
                    PISST_pPIS      := '';
                    PISST_vPIS      := '';
                    PISST_qBCProd   := '';
                    PISST_vAliqProd := '';
                  end;
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/COFINS/COFINSAliq');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/COFINS/COFINSQtde');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/COFINS/COFINSNT');
                  if not assigned(xmlNodeB) then
                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/COFINS/COFINSOutr');
                  if assigned(xmlNodeB) then
                  begin
                    COFINS_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    COFINS_vBC       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    COFINS_pCOFINS   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pCOFINS);
                    COFINS_vCOFINS   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCOFINS);
                    COFINS_qBCProd   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd);
                    COFINS_vAliqProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd);
                  end else begin
                    COFINS_CST       := '';
                    COFINS_vBC       := '';
                    COFINS_pCOFINS   := '';
                    COFINS_vCOFINS   := '';
                    COFINS_qBCProd   := '';
                    COFINS_vAliqProd := '';
                  end;

                  //

                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/imposto/COFINSST');
                  if assigned(xmlNodeB) then
                  begin
                    COFINSST_CST       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_CST);
                    COFINSST_vBC       := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vBC);
                    COFINSST_pCOFINS   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_pCOFINS);
                    COFINSST_vCOFINS   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vCOFINS);
                    COFINSST_qBCProd   := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_qBCProd);
                    COFINSST_vAliqProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_vAliqProd);
                  end else begin
                    COFINSST_CST       := '';
                    COFINSST_vBC       := '';
                    COFINSST_pCOFINS   := '';
                    COFINSST_vCOFINS   := '';
                    COFINSST_qBCProd   := '';
                    COFINSST_vAliqProd := '';
                  end;

                  //

                  xmlNodeB := UnMyXML.SelecionaUmNo(xmlDocB, '/det/infAdProd');
                  if assigned(xmlNodeB) then
                  begin
                    infAdProd := UnMyXML.LeNoXML(xmlNodeB, tnxTextStr, ttx_infAdProd);
                  end else
                  begin
                    infAdProd := '';
                  end;
                  //
                  DmNFe_0000.QrCod.Close;
                  DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
                  DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
                  UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, 'TUnitNFeImporta.ImportaDadosNFeDeXML()');
                  //
                  Nivel1 := 0;
                  case DmNFe_0000.QrCod.RecordCount of
                    0:
                    begin
                      if DBCheck.CriaFm(TFmGraGruE_P, FmGraGruE_P, afmoNegarComAviso) then
                      begin
                        FmGraGruE_P.EdxProd.Text            := prod_xProd;
                        FmGraGruE_P.EdcProd.Text            := prod_cProd;
                        FmGraGruE_P.FFormChamou             := Form.Name;
                        FmGraGruE_P.EdFornece.ValueVariant  := CodInfoEmit;
                        FmGraGruE_P.CBFornece.KeyValue      := CodInfoEmit;
                        FmGraGruE_P.ImgTipo.SQLType         := stIns;
                        FmGraGruE_P.ShowModal;
                        FmGraGruE_P.Destroy;
                      end;
                      DmNFe_0000.QrCod.Close;
                      DmNFe_0000.QrCod.Params[00].AsInteger := CodInfoEmit;
                      DmNFe_0000.QrCod.Params[01].AsString  := Trim(prod_cProd);
                      UMyMod.AbreQuery(DmNFe_0000.QrCod, Dmod.MyDB, 'TUnitNFeImporta.ImportaDadosNFeDeXML()');
                      Nivel1 := DmNFe_0000.QrCodNivel1.Value;
                    end;
                    else Nivel1 := DmNFe_0000.QrCodNivel1.Value;
                  end;
                  //
                  if Nivel1 = 0 then
                    if ExcluirEntrada(Form, FatID, FatNum, Empresa, True) then Exit;
                  //
                  xmlList.Remove(xmlList.Item[0]);
                  //
                  Conta      := Geral.IMV(nItem);
                  its_PesoVB     := 0;
                  its_PesoVL     := Geral.DMV_Dot(prod_qCom);
                  its_ValorItem  := Geral.DMV_Dot(prod_vProd);
                  its_RIPI       := 0;
                  its_CFin       := 0;
                  its_Seguro     := Geral.DMV_Dot(prod_vSeg);
                  its_Frete      := Geral.DMV_Dot(prod_vFrete);
                  its_Desconto   := Geral.DMV_Dot(prod_vDesc);
                  its_TotalCusto := its_ValorItem - its_Desconto + Geral.DMV_Dot(IPI_vIPI);
                  its_TotalPeso  := its_PesoVL;
                  //
  ICMSRec_pRedBC     := DmNFe_0000.QrCodICMSRec_pRedBC.Value;
  ICMSRec_vBC        := (100 - ICMSRec_pRedBC) * its_ValorItem / 100;
  if DmNFe_0000.QrCodICMSRec_tCalc.Value = 1 then
    ICMSRec_pAliq      := Geral.DMV_Dot(ICMS_pICMS)
  else
    ICMSRec_pAliq      := DmNFe_0000.QrCodICMSRec_pAliq.Value;
  ICMSRec_vICMS      := ICMSRec_vBC * ICMSRec_pAliq / 100;
  //tot_Rec_ICMS       := tot_Rec_ICMS + ICMSRec_vICMS;
  //
  IPIRec_pRedBC      := DmNFe_0000.QrCodIPIRec_pRedBC.Value;
  IPIRec_vBC         := (100 - IPIRec_pRedBC) * its_ValorItem / 100;
  if DmNFe_0000.QrCodIPIRec_tCalc.Value = 1 then
    IPIRec_pAliq      := Geral.DMV_Dot(IPI_pIPI)
  else
    IPIRec_pAliq      := DmNFe_0000.QrCodIPIRec_pAliq.Value;
  IPIRec_vIPI        := IPIRec_vBC * IPIRec_pAliq / 100;
  //tot_Rec_IPI        := tot_Rec_IPI + IPIRec_vIPI;
  //
  PISRec_pRedBC      := DmNFe_0000.QrCodPISRec_pRedBC.Value;
  PISRec_vBC         := (100 - PISRec_pRedBC) * its_ValorItem / 100;
  if DmNFe_0000.QrCodPISRec_tCalc.Value = 1 then
    PISRec_pAliq      := Geral.DMV_Dot(PIS_pPIS)
  else
    PISRec_pAliq      := DmNFe_0000.QrCodPISRec_pAliq.Value;
  PISRec_vPIS        := PISRec_vBC * PISRec_pAliq / 100;
  //tot_Rec_PIS        := tot_Rec_PIS + PISRec_vPIS;
  //
  COFINSRec_pRedBC   := DmNFe_0000.QrCodCOFINSRec_pRedBC.Value;
  COFINSRec_vBC      := (100 - COFINSRec_pRedBC) * its_ValorItem / 100;
  if DmNFe_0000.QrCodCOFINSRec_tCalc.Value = 1 then
    COFINSRec_pAliq      := Geral.DMV_Dot(COFINS_pCOFINS)
  else
    COFINSRec_pAliq      := DmNFe_0000.QrCodCOFINSRec_pAliq.Value;
  COFINSRec_vCOFINS  := COFINSRec_vBC * COFINSRec_pAliq / 100;
  //tot_Rec_COFINS     := tot_Rec_ICMS + ICMSRec_vICMS;

                  {
                  Controle := UMyMod.BuscaEmLivreY_Def('pqeits', 'Controle', stIns, 0);
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pqeits', False, [
                  'Codigo', 'Conta', 'Insumo',
                  'Volumes', 'PesoVB', 'PesoVL',
                  'ValorItem', 'IPI', 'RIPI',
                  'CFin', 'ICMS', 'RICMS',
                  'TotalCusto', 'TotalPeso', 'prod_cProd',
                  'prod_cEAN', 'prod_xProd', 'prod_NCM',
                  'prod_EX_TIPI', 'prod_genero', 'prod_CFOP',
                  'prod_uCom', 'prod_qCom', 'prod_vUnCom',
                  'prod_vProd', 'prod_cEANTrib', 'prod_uTrib',
                  'prod_qTrib', 'prod_vUnTrib', 'prod_vFrete',
                  'prod_vSeg', 'prod_vDesc', 'ICMS_Orig',
                  'ICMS_CST', 'ICMS_modBC', 'ICMS_pRedBC',
                  'ICMS_vBC', 'ICMS_pICMS', 'ICMS_vICMS',
                  'ICMS_modBCST', 'ICMS_pMVAST', 'ICMS_pRedBCST',
                  'ICMS_vBCST', 'ICMS_pICMSST', 'ICMS_vICMSST',
                  'IPI_cEnq', 'IPI_CST', 'IPI_vBC',
                  'IPI_qUnid', 'IPI_vUnid', 'IPI_pIPI',
                  'IPI_vIPI', 'PIS_CST', 'PIS_vBC',
                  'PIS_pPIS', 'PIS_vPIS', 'PIS_qBCProd',
                  'PIS_vAliqProd', 'PISST_vBC', 'PISST_pPIS',
                  'PISST_qBCProd', 'PISST_vAliqProd', 'PISST_vPIS',
                  'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
                  'COFINS_qBCProd', 'COFINS_vAliqProd', 'COFINS_vCOFINS',
                  'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
                  'COFINSST_vAliqProd', 'COFINSST_vCOFINS'], [
                  'Controle'], [
                  Codigo, Conta, Produto,
                  its_Volumes, its_PesoVB, its_PesoVL,
                  its_ValorItem, its_IPI, its_RIPI,
                  its_CFin, its_ICMS, its_RICMS,
                  its_TotalCusto, its_TotalPeso, prod_cProd,
                  prod_cEAN, prod_xProd, prod_NCM,
                  prod_EX_TIPI, prod_genero, prod_CFOP,
                  prod_uCom, prod_qCom, prod_vUnCom,
                  prod_vProd, prod_cEANTrib, prod_uTrib,
                  prod_qTrib, prod_vUnTrib, prod_vFrete,
                  prod_vSeg, prod_vDesc, ICMS_Orig,
                  ICMS_CST, ICMS_modBC, ICMS_pRedBC,
                  ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
                  ICMS_modBCST, ICMS_pMVAST, ICMS_pRedBCST,
                  ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST,
                  IPI_cEnq, IPI_CST, IPI_vBC,
                  IPI_qUnid, IPI_vUnid, IPI_pIPI,
                  IPI_vIPI, PIS_CST, PIS_vBC,
                  PIS_pPIS, PIS_vPIS, PIS_qBCProd,
                  PIS_vAliqProd, PISST_vBC, PISST_pPIS,
                  PISST_qBCProd, PISST_vAliqProd, PISST_vPIS,
                  COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
                  COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS,
                  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
                  COFINSST_vAliqProd, COFINSST_vCOFINS], [
                  Controle], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  }
                  
                  //

                  if IPI_vIPI <> '' then
                     Tem_IPI := 1
                   else
                     Tem_IPI := 0;
                  _Ativo_ := 1;
                  InfAdCuztm := 0;
                  EhServico := 0;
                  // N�o se sabe ainda!
                  MeuID := 0;
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
                  'prod_cProd', 'prod_cEAN', 'prod_xProd',
                  'prod_NCM', 'prod_EXTIPI', 'prod_genero',
                  'prod_CFOP', 'prod_uCom', 'prod_qCom',
                  'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
                  'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
                  'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
                  'Tem_IPI', '_Ativo_', 'InfAdCuztm',
                  'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
                  'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
                  'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
                  'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
                  'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
                  'COFINSRec_pAliq', 'COFINSRec_vCOFINS',
                  'MeuID', 'Nivel1'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  prod_cProd, prod_cEAN, prod_xProd,
                  prod_NCM, prod_EX_TIPI, prod_genero,
                  prod_CFOP, prod_uCom, prod_qCom,
                  prod_vUnCom, prod_vProd, prod_cEANTrib,
                  prod_uTrib, prod_qTrib, prod_vUnTrib,
                  prod_vFrete, prod_vSeg, prod_vDesc,
                  Tem_IPI, _Ativo_, InfAdCuztm,
                  EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
                  ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
                  IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
                  PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
                  PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
                  COFINSRec_pAliq, COFINSRec_vCOFINS,
                  MeuID, Nivel1], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
                  'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
                  'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
                  'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
                  'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
                  'ICMS_vICMSST', '_Ativo_'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  ICMS_Orig, ICMS_CST, ICMS_modBC,
                  ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
                  ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
                  ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
                  ICMS_vICMSST, _Ativo_], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  if IPI_vIPI <> '' then
                  begin
                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
                    'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
                    'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
                    'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',
                    'IPI_pIPI', 'IPI_vIPI', '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
                    IPI_qSelo, IPI_cEnq, IPI_CST,
                    IPI_vBC, IPI_qUnid, IPI_vUnid,
                    IPI_pIPI, IPI_vIPI, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                    begin
                      ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      Exit;
                    end;
                  end;
                  {
                  //Exportadores n�o tem NFe???!!!
                  if II_vII > 0 then
                  begin
                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsp', False, [
                    'II_vBC', 'II_vDespAdu', 'II_vII',
                    'II_vIOF', '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    II_vBC, II_vDespAdu, II_vII,
                    II_vIOF, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                    begin
                      ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      Exit;
                    end;
                  end;
                  }
                  //
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsq', False, [
                  'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
                  'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
                  '_Ativo_'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  PIS_CST, PIS_vBC, PIS_pPIS,
                  PIS_vPIS, PIS_qBCProd, PIS_vAliqProd,
                  _Ativo_], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsr', False, [
                  'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
                  'PISST_vAliqProd', 'PISST_vPIS', '_Ativo_'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  PISST_vBC, PISST_pPIS, PISST_qBCProd,
                  PISST_vAliqProd, PISST_vPIS, _Ativo_], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  //
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitss', False, [
                  'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
                  'COFINS_vCOFINS', 'COFINS_qBCProd', 'COFINS_vAliqProd',
                  '_Ativo_'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
                  COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd,
                  _Ativo_], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;
                  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitst', False, [
                  'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
                  'COFINSST_vAliqProd', 'COFINSST_vCOFINS', '_Ativo_'], [
                  'FatID', 'FatNum', 'Empresa', 'nItem'], [
                  COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd,
                  COFINSST_vAliqProd, COFINSST_vCOFINS, _Ativo_], [
                  FatID, FatNum, Empresa, nItem], True) then
                  begin
                    ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                    Exit;
                  end;

                  if Trim(InfAdProd) <> '' then
                  begin
                    InfAdProd := Geral.TiraAspasDeTexto(InfAdProd);
                    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsv', False, [
                    'InfAdProd', '_Ativo_'], [
                    'FatID', 'FatNum', 'Empresa', 'nItem'], [
                    InfAdProd, _Ativo_], [
                    FatID, FatNum, Empresa, nItem], True) then
                    begin
                      ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                      Exit;
                    end;
                  end;
                end;
              end;
              if not DmNFe_0000.AtualizaRecuperacaoDeImpostos(FatID, FatNum, Empresa) then
              begin
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
                Exit;
              end;
              {
              LocCod(Codigo, Codigo);
              FinalizaEntrada();
              LocCod(Codigo, Codigo);
              }
              except
                ExcluirEntrada(Form, FatID, FatNum, Empresa, True);
              end;
            end else Geral.MensagemBox('Importa��o de NF-e cancelada!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          end;
        end;
      end else Geral.MensagemBox('O arquivo n�o � uma NF-e!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    end;
    Result := True;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
