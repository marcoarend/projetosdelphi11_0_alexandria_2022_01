object FmNFeOpcoes: TFmNFeOpcoes
  Left = 339
  Top = 185
  Caption = 'NFE-OPCAO-001 :: Op'#231#245'es de NF-e'
  ClientHeight = 442
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 394
    Width = 479
    Height = 48
    Align = alBottom
    TabOrder = 1
    OnClick = BtDesisteClick
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 367
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Op'#231#245'es de NF-e'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 393
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 469
    end
    object LaTipo: TdmkLabel
      Left = 395
      Top = 2
      Width = 82
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 346
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 252
      Width = 157
      Height = 13
      Caption = 'Tipo de email para envio de NFe:'
    end
    object SpeedButton1: TSpeedButton
      Left = 452
      Top = 268
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label4: TLabel
      Left = 8
      Top = 300
      Width = 461
      Height = 13
      Caption = 
        'Email pr'#243'prio para receber email enviado (mais de um separar por' +
        ' ponte e v'#237'rgula) m'#225'x. 255 letras:'
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 477
      Height = 160
      Align = alTop
      Caption = ' Dados e identifica'#231#227'o da NF-e: '
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Ver'#227'o:'
      end
      object Label2: TLabel
        Left = 56
        Top = 16
        Width = 38
        Height = 13
        Caption = 'Modelo:'
      end
      object EdVersao: TdmkEdit
        Left = 12
        Top = 32
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,10'
        QryCampo = 'versao'
        UpdCampo = 'versao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.100000000000000000
      end
      object Edide_mod: TdmkEdit
        Left = 56
        Top = 32
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '55'
        QryCampo = 'ide_mod'
        UpdCampo = 'ide_mod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 55
      end
      object RGide_tpImp: TdmkRadioGroup
        Left = 12
        Top = 56
        Width = 461
        Height = 45
        Caption = ' Formato de impress'#227'o: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '0 - Nenhum'
          '1 - Retrato'
          '2 - Paisagem')
        TabOrder = 2
        QryCampo = 'ide_tpImp'
        UpdCampo = 'ide_tpImp'
        UpdType = utYes
        OldValor = 0
      end
      object RGide_tpAmb: TdmkRadioGroup
        Left = 12
        Top = 104
        Width = 461
        Height = 45
        Caption = ' Identifica'#231#227'o do Ambiente: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '0 - Nenhum'
          '1 - Produ'#231#227'o'
          '2 - Homologa'#231#227'o')
        TabOrder = 3
        QryCampo = 'ide_tpAmb'
        UpdCampo = 'ide_tpAmb'
        UpdType = utYes
        OldValor = 0
      end
    end
    object RGAppCode: TdmkRadioGroup
      Left = 1
      Top = 161
      Width = 477
      Height = 44
      Align = alTop
      Caption = ' Gerenciamento NF-e: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Pascal'
        'C++')
      TabOrder = 1
      QryCampo = 'AppCode'
      UpdCampo = 'AppCode'
      UpdType = utYes
      OldValor = 0
    end
    object EdEntiTipCto: TdmkEditCB
      Left = 8
      Top = 268
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBEntiTipCto
    end
    object CBEntiTipCto: TdmkDBLookupComboBox
      Left = 64
      Top = 268
      Width = 385
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsEntiTipCto
      TabOrder = 3
      dmkEditCB = EdEntiTipCto
      UpdType = utYes
    end
    object EdMyEmailNFe: TdmkEdit
      Left = 8
      Top = 316
      Width = 465
      Height = 21
      CharCase = ecLowerCase
      MaxLength = 255
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'MyEmailNFe'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object RGAssDigMode: TdmkRadioGroup
      Left = 1
      Top = 205
      Width = 477
      Height = 44
      Align = alTop
      Caption = ' Modo de assinar a NF-e no Pascal: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'CAPICOM.DLL'
        '.NET FRAMEWORK')
      TabOrder = 5
      QryCampo = 'AssDigMode'
      UpdCampo = 'AssDigMode'
      UpdType = utYes
      OldValor = 0
    end
  end
  object QrEntiTipCto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 36
    Top = 8
  end
  object VUEntiTipCto: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = Panel1
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 8
  end
end
