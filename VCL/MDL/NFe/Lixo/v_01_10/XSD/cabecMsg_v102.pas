
{*************************************************************************************************}
{                                                                                                 }
{                                        XML Data Binding                                         }
{                                                                                                 }
{         Generated on: 16/11/2009 19:32:34                                                       }
{       Generated from: C:\Projetos\Delphi 2007\NFe\Schemas\Produ��o\PL_005d\cabecMsg_v1.02.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\Schemas\Produ��o\PL_005d\cabecMsg_v1.02.xdb   }
{                                                                                                 }
{*************************************************************************************************}

unit cabecMsg_v102;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLCabecMsg = interface;

{ IXMLCabecMsg }

  IXMLCabecMsg = interface(IXMLNode)
    ['{685FB034-6B58-4E98-B2CB-A1170326493B}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_VersaoDados: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_VersaoDados(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property VersaoDados: WideString read Get_VersaoDados write Set_VersaoDados;
  end;

{ Forward Decls }

  TXMLCabecMsg = class;

{ TXMLCabecMsg }

  TXMLCabecMsg = class(TXMLNode, IXMLCabecMsg)
  protected
    { IXMLCabecMsg }
    function Get_Versao: WideString;
    function Get_VersaoDados: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_VersaoDados(Value: WideString);
  end;

{ Global Functions }

function GetcabecMsg(Doc: IXMLDocument): IXMLCabecMsg;
function LoadcabecMsg(const FileName: WideString): IXMLCabecMsg;
function NewcabecMsg: IXMLCabecMsg;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetcabecMsg(Doc: IXMLDocument): IXMLCabecMsg;
begin
  Result := Doc.GetDocBinding('cabecMsg', TXMLCabecMsg, TargetNamespace) as IXMLCabecMsg;
end;

function LoadcabecMsg(const FileName: WideString): IXMLCabecMsg;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('cabecMsg', TXMLCabecMsg, TargetNamespace) as IXMLCabecMsg;
end;

function NewcabecMsg: IXMLCabecMsg;
begin
  Result := NewXMLDocument.GetDocBinding('cabecMsg', TXMLCabecMsg, TargetNamespace) as IXMLCabecMsg;
end;

{ TXMLCabecMsg }

function TXMLCabecMsg.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLCabecMsg.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLCabecMsg.Get_VersaoDados: WideString;
begin
  Result := ChildNodes['versaoDados'].Text;
end;

procedure TXMLCabecMsg.Set_VersaoDados(Value: WideString);
begin
  ChildNodes['versaoDados'].NodeValue := Value;
end;

end.