
{*******************************************************************************}
{                                                                               }
{                               XML Data Binding                                }
{                                                                               }
{         Generated on: 07/07/2010 22:19:55                                     }
{       Generated from: C:\_MLArend\NFe 002\PL_005d\retConsStatServ_v1.07.xsd   }
{   Settings stored in: C:\_MLArend\NFe 002\PL_005d\retConsStatServ_v1.07.xdb   }
{                                                                               }
{*******************************************************************************}

unit retConsStatServ_v107;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTRetConsStatServ = interface;

{ IXMLTRetConsStatServ }

  IXMLTRetConsStatServ = interface(IXMLNode)
    ['{3B909DF6-AFDD-4DE1-BD6B-CFFB4D65DD9B}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_VerAplic: WideString;
    function Get_CStat: WideString;
    function Get_XMotivo: WideString;
    function Get_CUF: WideString;
    function Get_DhRecbto: WideString;
    function Get_TMed: WideString;
    function Get_DhRetorno: WideString;
    function Get_XObs: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_VerAplic(Value: WideString);
    procedure Set_CStat(Value: WideString);
    procedure Set_XMotivo(Value: WideString);
    procedure Set_CUF(Value: WideString);
    procedure Set_DhRecbto(Value: WideString);
    procedure Set_TMed(Value: WideString);
    procedure Set_DhRetorno(Value: WideString);
    procedure Set_XObs(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property VerAplic: WideString read Get_VerAplic write Set_VerAplic;
    property CStat: WideString read Get_CStat write Set_CStat;
    property XMotivo: WideString read Get_XMotivo write Set_XMotivo;
    property CUF: WideString read Get_CUF write Set_CUF;
    property DhRecbto: WideString read Get_DhRecbto write Set_DhRecbto;
    property TMed: WideString read Get_TMed write Set_TMed;
    property DhRetorno: WideString read Get_DhRetorno write Set_DhRetorno;
    property XObs: WideString read Get_XObs write Set_XObs;
  end;

{ Forward Decls }

  TXMLTRetConsStatServ = class;

{ TXMLTRetConsStatServ }

  TXMLTRetConsStatServ = class(TXMLNode, IXMLTRetConsStatServ)
  protected
    { IXMLTRetConsStatServ }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_VerAplic: WideString;
    function Get_CStat: WideString;
    function Get_XMotivo: WideString;
    function Get_CUF: WideString;
    function Get_DhRecbto: WideString;
    function Get_TMed: WideString;
    function Get_DhRetorno: WideString;
    function Get_XObs: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_VerAplic(Value: WideString);
    procedure Set_CStat(Value: WideString);
    procedure Set_XMotivo(Value: WideString);
    procedure Set_CUF(Value: WideString);
    procedure Set_DhRecbto(Value: WideString);
    procedure Set_TMed(Value: WideString);
    procedure Set_DhRetorno(Value: WideString);
    procedure Set_XObs(Value: WideString);
  end;

{ Global Functions }

function GetretConsStatServ(Doc: IXMLDocument): IXMLTRetConsStatServ;
function LoadretConsStatServ(const FileName: WideString): IXMLTRetConsStatServ;
function NewretConsStatServ: IXMLTRetConsStatServ;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetretConsStatServ(Doc: IXMLDocument): IXMLTRetConsStatServ;
begin
  Result := Doc.GetDocBinding('retConsStatServ', TXMLTRetConsStatServ, TargetNamespace) as IXMLTRetConsStatServ;
end;

function LoadretConsStatServ(const FileName: WideString): IXMLTRetConsStatServ;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('retConsStatServ', TXMLTRetConsStatServ, TargetNamespace) as IXMLTRetConsStatServ;
end;

function NewretConsStatServ: IXMLTRetConsStatServ;
begin
  Result := NewXMLDocument.GetDocBinding('retConsStatServ', TXMLTRetConsStatServ, TargetNamespace) as IXMLTRetConsStatServ;
end;

{ TXMLTRetConsStatServ }

function TXMLTRetConsStatServ.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsStatServ.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsStatServ.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsStatServ.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_VerAplic: WideString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsStatServ.Set_VerAplic(Value: WideString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CStat: WideString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CStat(Value: WideString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XMotivo: WideString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XMotivo(Value: WideString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CUF: WideString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CUF(Value: WideString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRecbto: WideString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRecbto(Value: WideString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_TMed: WideString;
begin
  Result := ChildNodes['tMed'].Text;
end;

procedure TXMLTRetConsStatServ.Set_TMed(Value: WideString);
begin
  ChildNodes['tMed'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRetorno: WideString;
begin
  Result := ChildNodes['dhRetorno'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRetorno(Value: WideString);
begin
  ChildNodes['dhRetorno'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XObs: WideString;
begin
  Result := ChildNodes['xObs'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XObs(Value: WideString);
begin
  ChildNodes['xObs'].NodeValue := Value;
end;

end.