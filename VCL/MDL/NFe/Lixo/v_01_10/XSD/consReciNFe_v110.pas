
{****************************************************************************************************}
{                                                                                                    }
{                                          XML Data Binding                                          }
{                                                                                                    }
{         Generated on: 23/11/2009 15:49:40                                                          }
{       Generated from: C:\Projetos\Delphi 2007\NFe\Schemas\Produ��o\PL_005d\consReciNFe_v1.10.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\Schemas\Produ��o\PL_005d\consReciNFe_v1.10.xdb   }
{                                                                                                    }
{****************************************************************************************************}

unit consReciNFe_v110;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsReciNFe = interface;

{ IXMLTConsReciNFe }

  IXMLTConsReciNFe = interface(IXMLNode)
    ['{FA47C4A1-9D39-488E-BC5F-6864D990770A}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_NRec: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_NRec(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property NRec: WideString read Get_NRec write Set_NRec;
  end;

{ Forward Decls }

  TXMLTConsReciNFe = class;

{ TXMLTConsReciNFe }

  TXMLTConsReciNFe = class(TXMLNode, IXMLTConsReciNFe)
  protected
    { IXMLTConsReciNFe }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_NRec: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_NRec(Value: WideString);
  end;

{ Global Functions }

function GetconsReciNFe(Doc: IXMLDocument): IXMLTConsReciNFe;
function LoadconsReciNFe(const FileName: WideString): IXMLTConsReciNFe;
function NewconsReciNFe: IXMLTConsReciNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsReciNFe(Doc: IXMLDocument): IXMLTConsReciNFe;
begin
  Result := Doc.GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

function LoadconsReciNFe(const FileName: WideString): IXMLTConsReciNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

function NewconsReciNFe: IXMLTConsReciNFe;
begin
  Result := NewXMLDocument.GetDocBinding('consReciNFe', TXMLTConsReciNFe, TargetNamespace) as IXMLTConsReciNFe;
end;

{ TXMLTConsReciNFe }

function TXMLTConsReciNFe.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsReciNFe.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsReciNFe.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsReciNFe.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsReciNFe.Get_NRec: WideString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTConsReciNFe.Set_NRec(Value: WideString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

end.