// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Componentes Delphi 2007\Ativos\NFe\Ramos informatica\NFe dll\Codigo Fonte\NFe DLL\Web References\wsStatusServicoHomSV\NFeStatusServico.wsdl
//  >Import : C:\Componentes Delphi 2007\Ativos\NFe\Ramos informatica\NFe dll\Codigo Fonte\NFe DLL\Web References\wsStatusServicoHomSV\NFeStatusServico.wsdl:0
// Encoding : utf-8
// Codegen  : [wfUseSerializerClassForAttrs-]
// Version  : 1.0
// (19/10/2009 21:15:02 - - $Rev: 10138 $)
// ************************************************************************ //

unit NFeStatusServico;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]



  // ************************************************************************ //
  // Namespace : http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico
  // soapAction: http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico/nfeStatusServicoNF
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : NfeStatusServicoSoap
  // service   : NfeStatusServico
  // port      : NfeStatusServicoSoap
  // URL       : https://hom.nfe.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx
  // ************************************************************************ //
  NfeStatusServicoSoap = interface(IInvokable)
  ['{5E393C9B-1708-45AE-674E-967E74889DFF}']
    function  nfeStatusServicoNF(const nfeCabecMsg: WideString; const nfeDadosMsg: WideString): WideString; stdcall;
  end;

function GetNfeStatusServicoSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): NfeStatusServicoSoap;


implementation
  uses SysUtils;

function GetNfeStatusServicoSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): NfeStatusServicoSoap;
const
  defWSDL = 'C:\Componentes Delphi 2007\Ativos\NFe\Ramos informatica\NFe dll\Codigo Fonte\NFe DLL\Web References\wsStatusServicoHomSV\NFeStatusServico.wsdl';
  defURL  = 'https://hom.nfe.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx';
  defSvc  = 'NfeStatusServico';
  defPrt  = 'NfeStatusServicoSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as NfeStatusServicoSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(NfeStatusServicoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(NfeStatusServicoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico/nfeStatusServicoNF');
  InvRegistry.RegisterInvokeOptions(TypeInfo(NfeStatusServicoSoap), ioDocument);

end.