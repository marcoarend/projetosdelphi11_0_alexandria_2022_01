// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRetRecepcao.wsdl
//  >Import : C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRetRecepcao.wsdl:0
// Encoding : utf-8
// Version  : 1.0
// (23/11/2009 16:01:11 - - $Rev: 10138 $)
// ************************************************************************ //

unit NFeRetRecepcao;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]



  // ************************************************************************ //
  // Namespace : http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetRecepcao
  // soapAction: http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetRecepcao/nfeRetRecepcao
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : NfeRetRecepcaoSoap
  // service   : NfeRetRecepcao
  // port      : NfeRetRecepcaoSoap
  // URL       : https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferetrecepcao/nferetrecepcao.asmx
  // ************************************************************************ //
  NfeRetRecepcaoSoap = interface(IInvokable)
  ['{22022493-5794-6850-8746-74643A8A987E}']
    function  nfeRetRecepcao(const nfeCabecMsg: WideString; const nfeDadosMsg: WideString): WideString; stdcall;
  end;

function GetNfeRetRecepcaoSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): NfeRetRecepcaoSoap;


implementation
  uses SysUtils;

function GetNfeRetRecepcaoSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): NfeRetRecepcaoSoap;
const
  defWSDL = 'C:\Projetos\Delphi 2007\NFe\WSDL\Homologacao\RS\NFeRetRecepcao.wsdl';
  defURL  = 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferetrecepcao/nferetrecepcao.asmx';
  defSvc  = 'NfeRetRecepcao';
  defPrt  = 'NfeRetRecepcaoSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as NfeRetRecepcaoSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(NfeRetRecepcaoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetRecepcao', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(NfeRetRecepcaoSoap), 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetRecepcao/nfeRetRecepcao');
  InvRegistry.RegisterInvokeOptions(TypeInfo(NfeRetRecepcaoSoap), ioDocument);

end.