unit NFeGeraXML_0110;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math, WinInet,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, Rio, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, cabecMsg_v102,
  // Consulta status servi�o
  consStatServ_v107, NFeStatusServico,
  // Consulta situa��o NF-e
  consSitNFe_v107, NFeConsultaNF,
  // Pede inutiliza��o NF-e
  inutNFe_v107, NFeInutilizacaoNF,
  // Gera NFe
  nfe_v110,
  // Envia lote de NF-es
  NFeRecepcao,
  // Consulta Lote de NF-es
  consReciNFe_v110, NFeRetRecepcao,
  // Cancela NF-e
  cancNFe_v107, NFeCancelamento,
  //
  StrUtils, ComObj;

const
  //NFe_VerAtual        = '3.00';
  //NFe_AllVersoes      = '3.00'; // SQL ?
  //NFe_ModeloAtual     = '55';
  NFe_AllModelos      = '55'; // SQL 55,?,?
  NFe_CodAutorizaTxt  = '100';
  NFe_CodCanceladTxt  = '101';
  NFe_CodInutilizTxt  = '102';
  NFe_CodDenegadoTxt  = '110,301,302';
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';

type
  TTipoConsumoWS = (tcwsStatusServico,
                    tcwsEnviaLoteNF,
                    tcwsConsultaLote,
                    tcwsPedeCancelamento,
                    tcwsPediInutilizacao,
                    tcwsConsultaNFe);
  TFmNFeGeraXML_0110 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    //
    function ObtemWebServer(UFServico: String; Ambiente, CodigoUF: Integer;
             Acao: TTipoConsumoWS; sAviso: String; Label1: TLabel): Boolean;
    {
    function ObtemCertificado(const NumeroSerial: String;
             out Cert: ICertificate2): Boolean;
    }
    // Montagem da NFe
    function GerarNFe(FatID, FatNum, Empresa: Integer; const nfeID: String;
             GravaCampos: Integer): Boolean;
    function VarTypeValido(Valor: Variant; Nome: String): Boolean;
    function Def(const Codigo, ID: String; const Source: Variant; var Dest:
             String): Boolean;
    function FMT_IE(Valor: String): String;
    function OutroPais_emit(): Boolean;
    function OutroPais_dest(): Boolean;
    function OutroPais_retirada(): Boolean;
    function OutroPais_entrega(): Boolean;
  public
    { Public declarations }
    {
    function NFeEstahAutorizada(Codigo: Integer): Boolean;
    function NFeEstahCancelada(Codigo: Integer): Boolean;
    }
    {
    verCabecMsg_Versao,        // Cabe�alho
    verConsStatServ_Versao,    // Consulta status servi�o
    verConsSitNFe_Versao,      // Consulta situa��o NF-e
    verNFeInutNFe_Versao,      // Solicita inutiliza��o de NF-e(s)
    verCancNFe_Versao,         // Cancelamento de NF-e
    verEnviNFe_Versao,         // Envia Lote de NF-e(s)
    verConsReciNFe_Versao:     // Consulta Lote de NF-e(s)
    String;
    }
    //
    {
    function SalvaXML(Arquivo: String; XML: WideString): Boolean;
    }
    {
    function AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
             TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
    }
    function Alltrim(const Search: string): string;
    procedure ConfiguraRio( Rio : THTTPRIO);
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
    function MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
              nNFFim: String; var Id: String): Boolean;
    function DesmontaID_Inutilizacao(const Id: WideString; var cUF, Ano,
             emitCNPJ, Modelo, Serie, nNFIni, nNFFim: WideString): Boolean;
    function StrZero(Num : Real; Zeros, Deci: Integer): String;
    function TipoXML(NoStandAlone: Boolean): WideString;
    //
    function XML_CabecMsg(VersaoDados: String; NoStandAlone: Boolean): WideString;
    function XML_ConsStatServ(TpAmb, CUF: Integer): WideString;
    function XML_ConsReciNFe(TpAmb: Integer; NRec: String): WideString;
    function XML_ConsSitNFe(TpAmb: Integer; ChNFe: String): WideString;
    function XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte; Ano: Integer;
             CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): WideString;
    function XML_CancNFe(ChNFe: String; TpAmb: Integer; nProt, XJust: String): WideString;
    function Ajusta_dhRecbto(var dhRecbto: WideString): Boolean;
    function CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
             var XMLGerado_Arq, XMLGerado_Dir: String; Label1: TLabel;
             GravaCampos: Integer): Boolean;
    {
    function XML_DistribuiNFe(const Id: String; const Status: Integer;
             const XML_NFe, XML_Aut, XML_Can: WideString; const Protocolar:
             Boolean; var XML_Distribuicao: WideString): Boolean;
    }
    //  Assinatura
    function ExecutaAssinatura(aValue: AnsiString; Certificado: ICertificate2;
             URIs: TStringList; out sXML: AnsiString): Boolean;
    function GerarLote(Lote, Empresa: Integer; out PathLote: String;
             out XML_Lote: WideString; Label1: TLabel): Boolean;
    {
    function AssinarMSXML(XML: WideString; Certificado:
             ICertificate2; out XMLAssinado: WideString): Boolean;
    function PosEx(const SubStr, S: AnsiString; Offset: Cardinal = 1): Integer;
    function PosLast(const SubStr, S: AnsiString ): Integer;
    }
    //
    function WS_NFeStatusServico(UFServico: String; Ambiente,
             CodigoUF: Byte; Certificado: String; Label1: TLabel; RETxtEnvio:
             TMemo): WideString;
    function WS_NFeCancelamentoNFe(UFServico: String; Ambiente,
             CodigoUF: Byte; ChNFe, NumeroSerial, nProt, xJust: String; Label1:
             TLabel; RETxtEnvio: TMemo): WideString;
    function WS_NFeInutilizacaoNFe(UFServico: String; Ambiente, CodigoUF, Ano: Byte;
             Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
             XJust, NumeroSerial: String; Label1: TLabel; RETxtEnvio:
             TMemo): WideString;
    function WS_NFeRecepcaoLote(UFServico: String; Ambiente, CodigoUF: Byte;
             NumeroSerial: String; Lote: Integer; Label1: TLabel; RETxtEnvio:
             TMemo): WideString;
    function WS_NFeRetRecepcao(UFServico: String; Ambiente, CodigoUF: Byte;
             Recibo: String; Label1: TLabel; RETxtEnvio: TMemo): WideString;
    function WS_NFeConsultaNF(UFServico: String; Ambiente, CodigoUF: Byte;
             ChaveNFe: String; Label1: TLabel; RETxtEnvio:
             TMemo): WideString;
    {
    function Texto_StatusNFe(Status: Integer): String;
    Usar >> TNFeGeraXMLGerencia.Texto_StatusNFe(Status: Integer): String;
    fica em C:\Projetos\Delphi 2007\NFe\Geral
    }
  end;
  var
  FmNFeGeraXML_0110: TFmNFeGeraXML_0110;

  const
  verCabecMsg_Versao       = '1.02'; // Cabe�alho
  //verCabecMsg_VersaoDados  = '1.07';
  verConsStatServ_Versao   = '1.07'; // Consulta status servi�o
  verConsSitNFe_Versao     = '1.07'; // Consulta situa��o NF-e
  verNFeInutNFe_Versao     = '1.07'; // Solicita inutiliza��o de NF-e(s)
  verCancNFe_Versao        = '1.07'; // Cancelamento de NF-e
  verEnviNFe_Versao        = '1.10'; // Envia Lote de NF-e(s)
  verConsReciNFe_Versao    = '1.10'; // Consulta Lote de NF-e(s)
  verProcNFe_Versao        = '1.10'; // Distribui��o de NF-e

implementation

uses UnMyObjects, NFeSteps_0110, Module, ModuleNFe_0000, UnInternalConsts, UMySQLModule, ModuleGeral,
NFeXMLGerencia;

var
  FGravaCampo: Integer;
  FLabel1: TLabel;
  FdocXML: TXMLDocument;
  FCabecTxt, FDadosTxt, FAssinTxt: WideString;
  //FWSDL,
  FURL: String;
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;
  //
  NomeArquivo: String;
  CaminhoArquivo: String;
  strTpAmb: string;
  SerieNF: String;
  NumeroNF: String;
  CDV: String;
  cXML: IXMLTNFe;
  arqXML: TXMLDocument;
  (* Objetos do Documento XML... *)
  cDetLista: IXMLDet;
  cRefLista: IXMLNFref;
  cRebLista: IXMLTVeiculo;
  cVolLista: IXMLVol;
  cLacLista: IXMLLacres;
  cDupLista: IXMLDup;
  cProcRefLista: IXMLProcRef;
  cDILista: IXMLDI;
  cAdiLista: IXMLAdi;
  cMedLista: IXMLMed;
  cArmLista: IXMLArma;
  cObsLista: IXMLObsFisco;
  FFatID,
  FFatNum,
  FEmpresa,
  FOrdem: Integer;


{$R *.DFM}

function TFmNFeGeraXML_0110.Ajusta_dhRecbto(var dhRecbto: WideString): Boolean;
var
  P: Integer;
begin
  P := pos('T', dhRecbto);
  if P > 0 then
    dhRecbto[P] := ' ';
  Result := True;
end;

function TFmNFeGeraXML_0110.Alltrim(const Search: string): string;
const
  BlackSpace = [#33..#126];
var
  Index: byte;
begin
  Index:=1;
  while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
    begin
      Index:=Index + 1;
    end;
  Result:=Copy(Search, Index, 255);
  Index := Length(Result);
  while (Index > 0) and not (Result[Index] in BlackSpace) do
    begin
      Index:=Index - 1;
    end;
  Result := Copy(Result, 1, Index);
end;

procedure TFmNFeGeraXML_0110.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeGeraXML_0110.ConfiguraRio(Rio: THTTPRIO);
begin
  {
  if FConfiguracoes.WebServices.ProxyHost <> '' then
   begin
     Rio.HTTPWebNode.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
     Rio.HTTPWebNode.UserName     := FConfiguracoes.WebServices.ProxyUser;
     Rio.HTTPWebNode.Password     := FConfiguracoes.WebServices.ProxyPass;
   end;
  }
  Rio.HTTPWebNode.OnBeforePost := OnBeforePost;
end;

function TFmNFeGeraXML_0110.CriarDocumentoNFe(FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; Label1: TLabel;
  GravaCampos: Integer): Boolean;
var
  strChaveAcesso: String;
begin
  Result := False;
  FLabel1 := Label1;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM nfexmli WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := FatID;
  Dmod.QrUpd.Params[01].AsInteger := FatNum;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  FOrdem   := 0;
  //
  DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetNFe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  }
  
  SerieNF := FormatFloat('000', DmNFe_0000.QrNFECabAide_Serie.Value);//'001';
  NumeroNF := FormatFloat('000000000', DmNFe_0000.QrNFECabAide_cNF.Value);//'518005127'; (* C�digo Aleat�rio que ir� compor a Chave de Acesso...*)

  (* Montar a Chave de Acesso da NFe de acordo com as informa��es do Registro...*)
  (* cUF=??,dEmi=...*)
  strChaveAcesso := DmNFe_0000.QrNFECabAId.Value;//'35080599999090910270550010000000015180051273';

  CDV := Copy(strChaveAcesso, Length(strChaveAcesso), Length(strChaveAcesso));
  if GerarNFe(FatID, FatNum, Empresa, 'NFe' + strChaveAcesso, GravaCampos) then
  begin
    DmNFe_0000.ReopenEmpresa(Empresa);
    //
    DmNFe_0000.SalvaXML(NFE_EXT_NFE_XML, strChaveAcesso, arqXML.XML.Text, nil, False);
    //
    Result := True;
  end;
  arqXML := nil;
end;

function TFmNFeGeraXML_0110.Def(const Codigo, ID: String; const Source: Variant;
  var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  Result := False;
  Dest   := '';
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if DmNFe_0000.QrNFeLayIOcorMin.Value > 0 then
    begin
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
        Continua := True;
    end else
      Continua := VarTypeValido(Source, Codigo + '  ' + ID);
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'N' then
      begin
        // Double
        if DmNFe_0000.QrNFeLayIDeciCasas.Value > 0 then
          Dest := DmNFe_0000.DecimalPonto(FloatToStrF(Source, ffFixed, 15,
            DmNFe_0000.QrNFeLayIDeciCasas.Value))
        else begin
        //integer
          if DmNFe_0000.QrNFeLayILeftZeros.Value = 1 then
            Dest := DmNFe_0000.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmNFe_0000.QrNFeLayITamMax.Value, siPositivo))
          else
            Dest := DmNFe_0000.DecimalPonto(FloatToStr(Source));
        end;
      end else
      // data
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'D' then
        Dest := FormatDateTime('yyyy-mm-dd', Source)
      else
      // texto
      if Uppercase(DmNFe_0000.QrNFeLayITipo.Value) = 'C' then
        Dest := Source
      else
      // desconhecido
      begin
        Dest := Source;
        Geral.MensagemBox(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
        'Tipo de formata��o desconhecida:', 1), 'Erro', MB_OK+MB_ICONWARNING);
      end;
      //
      Dest := Trim(DmNFe_0000.ValidaTexto_XML(Dest, Codigo, ID));
      if (Dest = '') and (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmNFe_0000.QrNFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
          if (Codigo = '78') and (ID = 'E17') and
          (DmNFe_0000.QrNFECabAdest_CNPJ.Value = '') then
            Avisa := False;
          if Avisa then
            Geral.MensagemBox(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
            'Valor n�o definido:', 2), 'Erro', MB_OK+MB_ICONWARNING);
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        FLabel1.Caption := 'Aguarde... Inserindo valor de XML no banco de dados...'
        + IntToStr(FOrdem);
        //Label1.
        FLabel1.Update;
        Application.ProcessMessages;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Codigo, ID, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MensagemBox(DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Dest,
  'N�o foi poss�vel definir o valor', 3), 'Erro', MB_OK+MB_ICONERROR);
  // Teste
  if Geral.IMV(Geral.SoNumero_TT(Codigo)) > 1000 then
    Geral.MensagemBox('item #' + Codigo + '- ID = ' + ID + #13#10 +
    'Valor = "' + Dest + '"', 'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

function TFmNFeGeraXML_0110.DesmontaID_Inutilizacao(const Id: WideString; var cUF,
  Ano, emitCNPJ, Modelo, Serie, nNFIni, nNFFim: WideString): Boolean;
var
  K, N, Z: Integer;
begin
  K := Length(Id);
  if K in ([41,42,43]) then
  begin
    Z := 1 + K - 41;
    N := 3;
    cUF      := Copy(Id, 02, N);
    Ano      := Copy(Id, 02, N);
    emitCNPJ := Copy(Id, 14, N);
    Modelo   := Copy(Id, 02, N);
    Serie    := Copy(Id,  Z, N);
    nNFIni   := Copy(Id, 09, N);
    nNFFim   := Copy(Id, 09, N);
    Result := True;
  end else begin
    Result := False;
    Geral.MensagemBox('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.', 'Erro',
    MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFeGeraXML_0110.FMT_IE(Valor: String): String;
begin
  if Uppercase(Valor) <> 'ISENTO' then
    Result := Geral.SoNumero_TT(Valor)
  else
    Result := Valor;
end;

procedure TFmNFeGeraXML_0110.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeGeraXML_0110.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmNFeGeraXML_0110.GerarLote(Lote, Empresa: Integer;
  out PathLote: String; out XML_Lote: WideString; Label1: TLabel): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmNFe_0000.QrNFeLEnC.Close;
  DmNFe_0000.QrNFeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmNFe_0000.QrNFeLEnC, Dmod.MyDB, 'TFmNFeGeraXML_0110.GerarLote()');
  //
  if DmNFe_0000.QrNFeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem NF-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := StrZero(Lote, 9, 0);
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmNFe_0000.QrFilialDirNFeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + NFE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);

// Adiciona as namespaces
  {
  Write(fArquivoTexto, '<?xml version="1.0" encoding="UTF-8"?>');
  Write(fArquivoTexto, '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" versao="1.10">' );
  Write(fArquivoTexto, '<idLote>' + LoteStr + '</idLote>');
}
  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" versao="1.10">' +
  '<idLote>' + LoteStr + '</idLote>';
//repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmNFe_0000.QrNFeLEnC.First;
  while not DmNFe_0000.QrNFeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmNFe_0000.QrNFeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + NFE_EXT_NFE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      // Nesse ponto vc est� copiando o conteudo da tag "NFe" para o buffer...
      buf := Copy(MeuXMLAssinado, Pos('<NFe', MeuXMLAssinado), Length(MeuXMLAssinado));

      XML_Lote := XML_Lote + buf;
      //Write(fArquivoTexto, buf);
      mTexto.Free;
    end;
    DmNFe_0000.QrNFeLEnC.Next;
  end;

// Fecha o arquivo
  XML_Lote := XML_Lote + '</enviNFe>';
  Write(fArquivoTexto, XML_Lote);
  //Write(fArquivoTexto, '</enviNFe>');
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

function TFmNFeGeraXML_0110.GerarNFe(FatID, FatNum, Empresa: Integer;
  const nfeID: String; GravaCampos: Integer): Boolean;
var
  i: Integer;
  Valor, IM: String;
  infAdProd: WideString;
begin
  Result := False;
  FGravaCampo := GravaCampos;
  DmNFe_0000.ReopenNFeLayI();
  //P�g. 92/145  Manual_de_Integracao_Contribuinte_v3.00-2009-03-16
   //    '1', 'A01' = Grupo das informa��es da NFe
(* Informa��es da TAG InfNFe... *)
  if Def('2', 'A02', DmNFe_0000.QrNFECabAversao.Value, Valor) then
    cXML.InfNFe.Versao := Valor;
  //if Def('3', 'A03', nfeID, Valor) then
  cXML.InfNFe.Id := nfeID;
  //if Def('4', 'A04', N�o preencher, Valor) then

(* Informa��es da TAG IDE... *)
  //if Def('5', 'B01', GRUPO NFe
  // C�digo da UF do emitente do Documento Fiscal. Utilizar a Tabela do IBGE
  if Def('6', 'B02', DmNFe_0000.QrNFECabAide_cUF.Value, Valor) then
    cXML.InfNFe.Ide.CUF := Valor;//'41=PR';
  //C�digo num�rico que comp�e a Chave de Acesso. N�mero aleat�rio gerado pelo emitente para cada NF-e
  if Def('7', 'B03', DmNFe_0000.QrNFECabAide_cNF.Value, Valor) then
    cXML.InfNFe.Ide.CNF := Valor;//'518005127';
  //Descri��o da Natureza da Opera��o
  if Def('8', 'B04', DmNFe_0000.QrNFECabAide_natOp.Value, Valor) then
    cXML.InfNFe.Ide.NatOp := Valor;//'NATUREZA DE OPERACAO DE TESTE DA ENDSIS NFE';
  //Indicador da forma de pagamento:
    // 0 � pagamento � vista;
    // 1 � pagamento � prazo;
    // 2 � outros.
  if Def('9', 'B05', DmNFe_0000.QrNFECabAide_indPag.Value, Valor) then
    cXML.InfNFe.Ide.IndPag := Valor;//'0'; (* Zero � Aceitav�l como valor... *)
  //C�digo do modelo do Documento Fiscal. Utilizar 55 para identifica��o da NF-e, emitida em substitui��o ao modelo 1 e 1A.
  if Def('10', 'B06', DmNFe_0000.QrNFECabAide_mod.Value, Valor) then
    cXML.InfNFe.Ide.Mod_ := Valor;//'55';
  //S�rie do Documento Fiscal
  if Def('11', 'B07', DmNFe_0000.QrNFECabAide_serie.Value, Valor) then
    cXML.InfNFe.Ide.Serie := Valor;//'1';
  //N�mero do Documento Fiscal
  if Def('12', 'B08', DmNFe_0000.QrNFECabAide_nNF.Value, Valor) then
    cXML.InfNFe.Ide.NNF := Valor;//'1';
  //Data de emiss�o do Documento Fiscal (AAAA-MM-DD)
  if Def('13', 'B09', DmNFe_0000.QrNFECabAide_dEmi.Value, Valor) then
    cXML.InfNFe.Ide.DEmi := Valor;//'2008-05-06';

  //Data de sa�da ou de entrada da mercadoria / produto (AAAA-MM-DD)
  (* Opcional... *)
  if Def('14', 'B10', DmNFe_0000.QrNFECabAide_dSaiEnt.Value, Valor) then
    cXML.InfNFe.Ide.DSaiEnt := Valor;//'2008-05-06';

  //Tipo do Documento Fiscal (0 - entrada; 1 - sa�da)
  if Def('15', 'B11', DmNFe_0000.QrNFECabAide_tpNF.Value, Valor) then
  begin
    if DmNFe_0000.QrNFECabAide_tpNF.Value in ([0,1]) then
      cXML.InfNFe.Ide.TpNF := Valor
    else begin
      Geral.MensagemBox('Tipo de emiss�o inv�lido: ' + FormatFloat('0', DmNFe_0000.QrNFECabAide_tpNF.Value) +
      #13#10 + 'Valor deve ser (0-entrada/1-sa�da)', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  //C�digo do Munic�pio de Ocorr�ncia do Fato Gerador (utilizar a tabela do IBGE)
  if Def('16', 'B12', DmNFe_0000.QrNFECabAide_cMunFG.Value, Valor) then
    cXML.InfNFe.Ide.CMunFG := Valor;//'3550308';

  (* Informa��es da TAG Notas Fiscais Referenciadas... se Houver... *)
{
Utilizar esta TAG para referenciar
uma Nota Fiscal Eletr�nica emitida
anteriormente, vinculada a NF-e
atual.
Esta informa��o ser� utilizada nas
hip�teses previstas na legisla��o.
(Ex.: Devolu��o de Mercadorias,
Substitui��o de NF cancelada,
Complementa��o de NF, etc.).
}
  { Essa TAG � Opcional e s� aparece no XML se Houver NFes a serem referenciadas na Nota...}
      // '16a', 'B12a' = Informa��o das NF/NF-e Referenciadas
  DmNFe_0000.ReopenNFeCabB(FatID, FatNum, Empresa);
  if DmNFe_0000.QrNFeCabB.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFeCabB.Eof do
    begin
      cRefLista := cXML.InfNFe.Ide.NFref.Add;
      //
      if DmNFe_0000.QrNFECabBrefNFe.Value <> '' then
      begin
        if Def('17', 'B13', DmNFe_0000.QrNFECabBrefNFe.Value, Valor) then
          cRefLista.RefNFe := Valor;
      end else
      if DmNFe_0000.QrNFECabBrefNF_nNF.Value > 0 then
      begin
        //    '18', 'B14' = informa��o das NF referenciadas (NFs normais > A1 etc)
        if Def('19', 'B15', DmNFe_0000.QrNFECabBrefNF_cUF.Value, Valor) then
          cRefLista.RefNF.CUF := Valor;
        if Def('20', 'B16', DmNFe_0000.QrNFECabBrefNF_AAMM.Value, Valor) then
          cRefLista.RefNF.AAMM := Valor;
        if Def('21', 'B17', DmNFe_0000.QrNFECabBrefNF_CNPJ.Value, Valor) then
          cRefLista.RefNF.CNPJ := Valor;
        if Def('22', 'B18', DmNFe_0000.QrNFECabBrefNF_mod.Value, Valor) then
          cRefLista.RefNF.Mod_ := Valor;
        if Def('23', 'B19', DmNFe_0000.QrNFECabBrefNF_serie.Value, Valor) then
          cRefLista.RefNF.Serie := Valor;
        if Def('24', 'B20', DmNFe_0000.QrNFECabBrefNF_nNF.Value, Valor) then
          cRefLista.RefNF.NNF := Valor;
        //
      end;
      DmNFe_0000.QrNFeCabB.Next;
    end;
  end;

  if Def('25', 'B21', DmNFe_0000.QrNFECabAide_tpImp.Value, Valor) then
    cXML.InfNFe.Ide.TpImp := Valor;//'1';
  if Def('26', 'B22', DmNFe_0000.QrNFECabAide_tpEmis.Value, Valor) then
    cXML.InfNFe.Ide.TpEmis := Valor;//'1';
  if Def('27', 'B23', DmNFe_0000.QrNFECabAide_cDV.Value, Valor) then
    cXML.InfNFe.Ide.CDV := Valor;//'3';
  if Def('28', 'B24', DmNFe_0000.QrNFECabAide_tpAmb.Value, Valor) then
    cXML.InfNFe.Ide.TpAmb := Valor;//'2';
  if Def('29', 'B25', DmNFe_0000.QrNFECabAide_finNFe.Value, Valor) then
    cXML.InfNFe.Ide.FinNFe := Valor;//'1';
  if Def('29a', 'B26', DmNFe_0000.QrNFECabAide_procEmi.Value, Valor) then
    cXML.InfNFe.Ide.ProcEmi := Valor;//'0'; (* Zero � Aceitav�l como valor... *)
  if Def('29b', 'B27', DmNFe_0000.QrNFECabAide_verProc.Value, Valor) then
    cXML.InfNFe.Ide.VerProc := Valor;//'ENDSIS NFE'; //Vers�o do Aplicativo Emissor da NFe...


(* C - Informa��es da TAG EMIT... *)
  // '30', 'C01'  Grupo de identifica��o do emitente da NF-e
  if Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value) <> '' then
  begin
    if Def('31', 'C02', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CNPJ.Value), Valor) then
    cXML.InfNFe.Emit.CNPJ := Valor;
  end else begin
    if Def('31a', 'C02a', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAemit_CPF.Value), Valor) then
    cXML.InfNFe.Emit.CPF := Valor;
  end;
  if Def('32', 'C03', DmNFe_0000.QrNFECabAemit_xNome.Value, Valor) then
    cXML.InfNFe.Emit.XNome := Valor;//'NF-e Associacao NF-e';
  if Def('33', 'C04', DmNFe_0000.QrNFECabAemit_xFant.Value, Valor) then
    cXML.InfNFe.Emit.XFant := Valor;//'NF-e';

  (* TAG EnderEMIT... *)
  //     '34', 'C05'  = Grupo de endere�o do emitente
  if Def('35', 'C06', DmNFe_0000.QrNFECabAemit_xLgr.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XLgr := Valor;//'Rua Central';
  if Def('36', 'C07', DmNFe_0000.QrNFECabAemit_nro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Nro := Valor;//'100';
  if Def('37', 'C08', DmNFe_0000.QrNFECabAemit_xCpl.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XCpl := Valor;//'Fundos';
  if Def('38', 'C09', DmNFe_0000.QrNFECabAemit_xBairro.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XBairro := Valor;//'Distrito Industrial';
  if Def('39', 'C10', DmNFe_0000.QrNFECabAemit_cMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.CMun := '9999999'
    else
      cXML.InfNFe.Emit.EnderEmit.CMun := Valor;//'3502200';
  end;
  if Def('40', 'C11', DmNFe_0000.QrNFECabAemit_xMun.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.XMun := 'EXTERIOR'
    else
      cXML.InfNFe.Emit.EnderEmit.XMun := Valor;
  end;
  if Def('41', 'C12', DmNFe_0000.QrNFECabAemit_UF.Value, Valor) then
  begin
    if OutroPais_emit() then
      cXML.InfNFe.Emit.EnderEmit.UF := 'EX'
    else
      cXML.InfNFe.Emit.EnderEmit.UF := Valor;//'SP';
  end;
  if Def('42', 'C13', DmNFe_0000.QrNFECabAemit_CEP.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CEP := Valor;//'17100171';
  if Def('43', 'C14', DmNFe_0000.QrNFECabAemit_cPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.CPais := Valor;//'1058';
  if Def('44', 'C15', DmNFe_0000.QrNFECabAemit_xPais.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.XPais := Valor;//'Brasil';
  if Def('45', 'C16', DmNFe_0000.QrNFECabAemit_fone.Value, Valor) then
    cXML.InfNFe.Emit.EnderEmit.Fone := Valor;//'1733021717';
  if Def('46', 'C17', DmNFe_0000.QrNFECabAemit_IE.Value, Valor) then
    cXML.InfNFe.Emit.IE := FMT_IE(Valor);//'123456789012';
  if Def('47', 'C18', DmNFe_0000.QrNFECabAemit_IEST.Value, Valor) then
    cXML.InfNFe.Emit.IEST := FMT_IE(Valor);
  IM := DmNFe_0000.QrNFECabAemit_IM.Value;
  if IM = '0' then IM := '';
  if Def('48', 'C19', IM, Valor) then
    cXML.InfNFe.Emit.IM := Valor;
  if Def('49', 'C20', DmNFe_0000.QrNFECabAemit_CNAE.Value, Valor) then
    cXML.InfNFe.Emit.CNAE := Valor;
  //
  //
  // D Itentifica��o do Fisco emitente da NF-e
  // N�o preencher (� do fisco)
  // vai de '50', 'D01'
  // at�    '61', 'D12'

(* E - Informa��es da TAG DEST... *)
  //       '62', 'E01' - Grupo de identifica��o do destinat�rio da NF-e
  if OutroPais_dest() then
  begin
    cXML.InfNFe.Dest.CNPJ := ''
    {
    if DmNFe_0000.QrNFECabACodInfoDest.Value <> 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := DmNFe_0000.QrNFECabACodInfoDest.Value;
      Dmod.QrAux.Open;
      //
      if Dmod.QrAux.FieldByName('Tipo').AsInteger = 0 then
        cXML.InfNFe.Dest.CNPJ := ''
      else
        cXML.InfNFe.Dest.CPF := '00000000000';
    end else cXML.InfNFe.Dest.CPF := '00000000000';
    }
  end else begin
    if (DmNFe_0000.QrNFECabAdest_CNPJ.Value <> '') then
    begin
      if Def('63', 'E02', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAdest_CNPJ.Value), Valor) then
        cXML.InfNFe.Dest.CNPJ := Valor;
    end else begin
      if Def('64', 'E03', Geral.SoNumero_TT(DmNFe_0000.QrNFECabAdest_CPF.Value), Valor) then
        cXML.InfNFe.Dest.CPF := Valor;
    end;
  end;
  if Def('65', 'E04', DmNFe_0000.QrNFECabAdest_xNome.Value, Valor) then
    cXML.InfNFe.Dest.XNome := Valor;//'DISTRIBUIDORA DE AGUAS MINERAIS';

  (* TAG EnderDEST... *)
  //     '66', 'E05' - Grupo de endere�o do destinat�rio da NF-e
  if Def('67', 'E06', DmNFe_0000.QrNFECabAdest_xLgr.Value, Valor) then
   cXML.InfNFe.Dest.EnderDest.XLgr    :=  Valor;
  if Def('68', 'E07', DmNFe_0000.QrNFECabAdest_nro.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.Nro     :=  Valor     ;
  if Def('69', 'E08', DmNFe_0000.QrNFECabAdest_xCpl.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XCpl    :=  Valor    ;
  if Def('70', 'E09', DmNFe_0000.QrNFECabAdest_xBairro.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XBairro :=  Valor ;
  if Def('71', 'E10', DmNFe_0000.QrNFECabAdest_cMun.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.CMun    :=  '9999999'
    else
      cXML.InfNFe.Dest.EnderDest.CMun    :=  Valor    ;
  end;
  if Def('72', 'E11', DmNFe_0000.QrNFECabAdest_xMun.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.XMun    :=  'EXTERIOR'
    else
      cXML.InfNFe.Dest.EnderDest.XMun    :=  Valor    ;
  end;
  if Def('73', 'E12', DmNFe_0000.QrNFECabAdest_UF.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.EnderDest.UF      :=  'EX'
    else
      cXML.InfNFe.Dest.EnderDest.UF      :=  Valor      ;
  end;
  if Def('74', 'E13', DmNFe_0000.QrNFECabAdest_CEP.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.CEP     :=  Valor     ;
  if Def('75', 'E14', DmNFe_0000.QrNFECabAdest_cPais.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.CPais   :=  Valor   ;
  if Def('76', 'E15', DmNFe_0000.QrNFECabAdest_xPais.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.XPais   :=  Valor   ;
  if Def('77', 'E16', DmNFe_0000.QrNFECabAdest_fone.Value, Valor) then
    cXML.InfNFe.Dest.EnderDest.Fone    :=  Valor    ;
  if Def('78', 'E17', DmNFe_0000.QrNFECabAdest_IE.Value, Valor) then
  begin
    if OutroPais_dest() then
      cXML.InfNFe.Dest.IE              :=  ''
    else
      //'ISENTO'; (* Caso o destinat�rio n�o tenha Inscri��o Estadual coloque a palavra "ISENTO" ...*)
      cXML.InfNFe.Dest.IE              :=  FMT_IE(Valor)      ;
  end;
  if Def('79', 'E18', DmNFe_0000.QrNFECabAdest_ISUF.Value, Valor) then
    cXML.InfNFe.Dest.ISUF := Valor;

(* F - Informa��es da TAG RETIRADA... se Houver *)

  DmNFe_0000.QrNFECabF.Close;
  DmNFe_0000.QrNFECabF.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabF.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabF.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabF.Open;
  //
  {Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de RETIRADA...}
  if DmNFe_0000.QrNFECabF.RecordCount > 0 then
  begin
    //     '80', 'F01' = Identifica��o do local de retirada
    if Def('81', 'F02', DmNFe_0000.QrNFECabFretirada_CNPJ.Value, Valor) then
      cXML.InfNFe.Retirada.CNPJ := Valor;
    if Def('82', 'F03', DmNFe_0000.QrNFECabFretirada_xLgr.Value, Valor) then
      cXML.InfNFe.Retirada.XLgr := Valor;
    if Def('83', 'F04', DmNFe_0000.QrNFECabFretirada_Nro.Value, Valor) then
      cXML.InfNFe.Retirada.Nro := Valor;
    if Def('84', 'F05', DmNFe_0000.QrNFECabFretirada_xCpl.Value, Valor) then
      cXML.InfNFe.Retirada.XCpl := Valor;
    if Def('85', 'F06', DmNFe_0000.QrNFECabFretirada_xBairro.Value, Valor) then
      cXML.InfNFe.Retirada.XBairro := Valor;
    if Def('86', 'F07', DmNFe_0000.QrNFECabFretirada_cMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.CMun := '9999999'
      else
        cXML.InfNFe.Retirada.CMun := Valor;
    end;
    if Def('87', 'F08', DmNFe_0000.QrNFECabFretirada_xMun.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Retirada.XMun := Valor;
    end;
    if Def('88', 'F09', DmNFe_0000.QrNFECabFretirada_UF.Value, Valor) then
    begin
      if OutroPais_retirada() then
        cXML.InfNFe.Retirada.UF := 'EX'
      else
        cXML.InfNFe.Retirada.UF := Valor;
    end;
  end;

  //
(* G - Informa��es da TAG ENTREGA... se Houver *)
  DmNFe_0000.QrNFECabG.Close;
  DmNFe_0000.QrNFECabG.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabG.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabG.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabG.Open;
  //
  {Essa TAG � Opcional e s� aparece no XML se Houverem informa��es de ENTREGA...}
  if DmNFe_0000.QrNFECabG.RecordCount > 0 then
  begin
    //     '89', 'G01' = Identifica��o do local de entrega
    if Def('90', 'G02', DmNFe_0000.QrNFECabGentrega_CNPJ.Value, Valor) then
      cXML.InfNFe.Entrega.CNPJ := Valor;
    if Def('91', 'G03', DmNFe_0000.QrNFECabGentrega_xLgr.Value, Valor) then
      cXML.InfNFe.Entrega.XLgr := Valor;
    if Def('92', 'G04', DmNFe_0000.QrNFECabGentrega_Nro.Value, Valor) then
      cXML.InfNFe.Entrega.Nro := Valor;
    if Def('93', 'G05', DmNFe_0000.QrNFECabGentrega_xCpl.Value, Valor) then
      cXML.InfNFe.Entrega.XCpl := Valor;
    if Def('94', 'G06', DmNFe_0000.QrNFECabGentrega_xBairro.Value, Valor) then
      cXML.InfNFe.Entrega.XBairro := Valor;
    if Def('95', 'G07', DmNFe_0000.QrNFECabGentrega_cMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.CMun := '9999999'
      else
        cXML.InfNFe.Entrega.CMun := Valor;
    end;
    if Def('96', 'G08', DmNFe_0000.QrNFECabGentrega_xMun.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.XMun := 'EXTERIOR'
      else
        cXML.InfNFe.Entrega.XMun := Valor;
    end;
    if Def('97', 'G09', DmNFe_0000.QrNFECabGentrega_UF.Value, Valor) then
    begin
      if OutroPais_entrega() then
        cXML.InfNFe.Entrega.UF := 'EX'
      else
        cXML.InfNFe.Entrega.UF := Valor;
    end;
  end;

(* H - Informa��es da TAG DET... *)
  DmNFe_0000.QrNFEItsI.Close;
  DmNFe_0000.QrNFEItsI.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFEItsI.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFEItsI.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFEItsI.Open;
  //
  while not DmNFe_0000.QrNFEItsI.Eof do
  begin
    //j := 1;  // M�ximo 990 itens
        // '98', 'H01' = Grupo de detalhamento de produtos e servi�os da NF-e
    cDetLista := cXML.InfNFe.Det.Add;
    if Def('99', 'H02', DmNFe_0000.QrNFEItsInItem.Value, Valor) then
      cDetLista.NItem := Valor;
        // '100', 'I01' = Grupo de detalhamento de produtos e servi�os da NF-e
    if Def('101', 'I02', DmNFe_0000.QrNFEItsIprod_cProd.Value, Valor) then
      cDetLista.Prod.CProd := Valor;//'00001';
    if Def('102', 'I03', DmNFe_0000.QrNFEItsIprod_cEAN.Value, Valor) then
      cDetLista.Prod.CEAN := Valor;//''; (* Se n�o tiver EAN tem que colocar em Branco...*)
    if Def('103', 'I04', DmNFe_0000.QrNFEItsIprod_xProd.Value, Valor) then
      cDetLista.Prod.XProd := Valor;//'Agua Mineral';
    if Def('104', 'I05', DmNFe_0000.QrNFEItsIprod_NCM.Value, Valor) then
      cDetLista.Prod.NCM := Valor;
    if Def('105', 'I06', DmNFe_0000.QrNFEItsIprod_EXTIPI.Value, Valor) then
    begin
      if Valor <> '0' then
        cDetLista.Prod.EXTIPI := Valor;
    end else
    if Def('106', 'I07', DmNFe_0000.QrNFEItsIprod_genero.Value, Valor) then
      cDetLista.Prod.Genero := Valor;
    if Def('107', 'I08', DmNFe_0000.QrNFEItsIprod_CFOP.Value, Valor) then
      cDetLista.Prod.CFOP := Valor;//'5101';
    if Def('108', 'I09', DmNFe_0000.QrNFEItsIprod_uCom.Value, Valor) then
      cDetLista.Prod.UCom := Valor;//'dz';
    if Def('109', 'I10', DmNFe_0000.QrNFEItsIprod_qCom.Value, Valor) then
      cDetLista.Prod.QCom := Valor;//1000000.0000';
    if Def('109a', 'I10a', DmNFe_0000.QrNFEItsIprod_vUnCom.Value, Valor) then
      cDetLista.Prod.VUnCom := Valor;//'1';
    if Def('110', 'I11', DmNFe_0000.QrNFEItsIprod_vProd.Value, Valor) then
      cDetLista.Prod.VProd := Valor;//'10000000.00';
    if Def('111', 'I12', DmNFe_0000.QrNFEItsIprod_cEANTrib.Value, Valor) then
      cDetLista.Prod.CEANTrib := Valor;//''; (* Se n�o tiver EAN Trib. tem que colocar em Branco...*)
    if Def('112', 'I13', DmNFe_0000.QrNFEItsIprod_uTrib.Value, Valor) then
      cDetLista.Prod.UTrib := Valor;//'und';
    if Def('113', 'I14', DmNFe_0000.QrNFEItsIprod_qTrib.Value, Valor) then
      cDetLista.Prod.QTrib := Valor;//'12000000.0000';
    if Def('113a', 'I14a', DmNFe_0000.QrNFEItsIprod_vUnTrib.Value, Valor) then
      cDetLista.Prod.VUnTrib := Valor;//'1';
    if Def('114', 'I15', DmNFe_0000.QrNFEItsIprod_vFrete.Value, Valor) then
      cDetLista.Prod.VFrete := Valor;
    if Def('115', 'I16', DmNFe_0000.QrNFEItsIprod_vSeg.Value, Valor) then
      cDetLista.Prod.VSeg := Valor;
    if Def('116', 'I17', DmNFe_0000.QrNFEItsIprod_vDesc.Value, Valor) then
      cDetLista.Prod.VDesc := Valor;


    (*     '117', 'I18' = Informa��es da TAG DI... Opcional *)
    // Parei aqui Falta fazer !!!
    //   vai at� '128', 'I29'
    {
    h := 1;
    cDILista := cXML.InfNFe.Det.Items[j].Prod.DI.Add;
    cDILista.NDI := Valor;
    cDILista.DDI := Valor;
    cDILista.XLocDesemb := Valor;
    cDILista.UFDesemb := Valor;
    cDILista.DDesemb := Valor;
    cDILista.NDI := Valor;
    }

    (* Informa��es da TAG ADI... Opcional *)
    {
    cAdiLista := cXML.InfNFe.Det.Items[j].Prod.DI.Items[h].Adi.Add;
    cAdiLista.NAdicao := Valor;
    cAdiLista.NSeqAdic := Valor;
    cAdiLista.CFabricante := Valor;
    cAdiLista.VDescDI := Valor;
    }

    (* J - Informa��es da TAG VEICPROD... Opcional *)
    {
    cDetLista.Prod.VeicProd.TpOp := Valor; (* Zero � Aceitav�l como valor... *)
    cDetLista.Prod.VeicProd.Chassi := Valor;
    cDetLista.Prod.VeicProd.CCor := Valor;
    cDetLista.Prod.VeicProd.XCor := Valor;
    cDetLista.Prod.VeicProd.Pot := Valor;
    cDetLista.Prod.VeicProd.CM3 := Valor;
    cDetLista.Prod.VeicProd.PesoL := Valor;
    cDetLista.Prod.VeicProd.PesoB := Valor;
    cDetLista.Prod.VeicProd.NSerie := Valor;
    cDetLista.Prod.VeicProd.TpComb := Valor;
    cDetLista.Prod.VeicProd.NMotor := Valor;
    cDetLista.Prod.VeicProd.CMKG := Valor;
    cDetLista.Prod.VeicProd.Dist := Valor;
    cDetLista.Prod.VeicProd.RENAVAM := Valor;
    cDetLista.Prod.VeicProd.AnoMod := Valor;
    cDetLista.Prod.VeicProd.AnoFab := Valor;
    cDetLista.Prod.VeicProd.TpPint := Valor;
    cDetLista.Prod.VeicProd.TpVeic := Valor;
    cDetLista.Prod.VeicProd.EspVeic := Valor;
    cDetLista.Prod.VeicProd.VIN := Valor;
    cDetLista.Prod.VeicProd.CondVeic := Valor;
    cDetLista.Prod.VeicProd.CMod := Valor;
    }

    (* K - Informa��es da TAG MED... Opcional *)
    {
    cMedLista := cXML.InfNFe.Det.Items[J - 1].Prod.Med.Add;
    cMedLista.NLote := Valor;
    cMedLista.QLote := DmNFe_0000.DecimalPonto(FormatFloat('##0.000', Valor));;
    cMedLista.DFab := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.DVal := FormatDateTime('yyyy-mm-dd' , Valor);
    cMedLista.VPMC := DmNFe_0000.DecimalPonto(FormatFloat('##0.00', Valor));
    }

    (* L - Informa��o da TAG ARMA... Opcional *)
    {
    cArmLista := cXML.InfNFe.Det.Items[j].Prod.Arma.Add;
    cArmLista.TpArma := Valor; (* Zero � Aceitav�l como valor... *)
    cArmLista.NSerie := Valor;
    cArmLista.NCano := Valor;
    cArmLista.Descr := Valor;
    }

    (* L1 - Informa��es da TAG COMB... Opcional *)
    {
    cDetLista.Prod.Comb.CProdANP := Valor;
    cDetLista.Prod.Comb.CODIF := Valor;
    cDetLista.Prod.Comb.QTemp := Valor;
    cDetLista.Prod.Comb.CIDE.QBCProd := Valor;
    cDetLista.Prod.Comb.CIDE.VAliqProd := Valor;
    cDetLista.Prod.Comb.CIDE.VCIDE := Valor;
    cDetLista.Prod.Comb.ICMSComb.VBCICMS := Valor;
    cDetLista.Prod.Comb.ICMSComb.VICMS := Valor;
    cDetLista.Prod.Comb.ICMSComb.VBCICMSST := Valor;
    cDetLista.Prod.Comb.ICMSComb.VICMSST := Valor;
    cDetLista.Prod.Comb.ICMSInter.VBCICMSSTDest := Valor;
    cDetLista.Prod.Comb.ICMSInter.VICMSSTDest := Valor;
    cDetLista.Prod.Comb.ICMSCons.VBCICMSSTCons := Valor;
    cDetLista.Prod.Comb.ICMSCons.VICMSSTCons := Valor;
    cDetLista.Prod.Comb.ICMSCons.UFCons := Valor;
    }



    // M - Tributos incidentes no produto ou servi�o
    // '163', 'M01'  = Grupo de tributos incidentes no produto ou servi�o

  (*  Ser� necess�rio criar regras de neg�cio  de acordo com o ERP na qual definir� a ocorr�ncia das TAGs de IMPOSTO...*)

    // N - ICMS Normal e ST
    // '164', 'N01'  = Grupo do ICMS na opera��o pr�pria e ST
    DmNFe_0000.QrNFEItsN.Close;
    DmNFe_0000.QrNFEItsN.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsN.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsN.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsN.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsN.Open;
    //
    case DmNFe_0000.QrNFEItsNICMS_CST.Value of
      0:
      begin
      (* TAG ICMS.ICMS00... *)
            // '165', 'N02'  = Grupo do CST = 00
        if Def('166', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.Orig  := Valor;
        if Def('167', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.CST   := Valor;
        if Def('168', 'N13', DmNFe_0000.QrNFEItsNICMS_ModBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.ModBC := Valor;
        if Def('169', 'N15', DmNFe_0000.QrNFEItsNICMS_VBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.VBC   := Valor;
        if Def('170', 'N16', DmNFe_0000.QrNFEItsNICMS_PICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.PICMS := Valor;
        if Def('171', 'N17', DmNFe_0000.QrNFEItsNICMS_VICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS00.VICMS := Valor;
      end;
      10:
      begin
        (* TAG ICMS.ICMS10... *)
            // '172', 'N03'  = Grupo do CST = 10
        if Def('173', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.Orig      := Valor;
        if Def('174', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.CST       := Valor;
        if Def('175', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.ModBC     := Valor;
        if Def('176', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.VBC       := Valor;
        if Def('177', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.PICMS     := Valor;
        if Def('178', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.VICMS     := Valor;
        if Def('179', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.ModBCST   := Valor;
        if Def('180', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.PMVAST    := Valor;
        if Def('181', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.PRedBCST  := Valor;
        if Def('184', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.VBCST     := Valor;
        if Def('183', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.PICMSST   := Valor;
        if Def('184', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS10.VICMSST   := Valor;
      end;
      20:
      begin
        (* TAG ICMS.ICMS20... *)
            // '185', 'N04'  = Grupo do CST = 20
        if Def('186', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.Orig      := Valor;
        if Def('187', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.CST       := Valor;
        if Def('188', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.ModBC     := Valor;
        if Def('189', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.PRedBC    := Valor;
        if Def('190', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.VBC       := Valor;
        if Def('191', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.PICMS     := Valor;
        if Def('192', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS20.VICMS     := Valor;
      end;
      30:
      begin
        (* TAG ICMS.ICMS30... *)
            // '193', 'N05'  = Grupo do CST = 30
        if Def('194', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.Orig      := Valor;
        if Def('195', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.CST       := Valor;
        if Def('196', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.ModBCST   := Valor;
        if Def('197', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.PMVAST    := Valor;
        if Def('198', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.PRedBCST  := Valor;
        if Def('199', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.VBCST     := Valor;
        if Def('200', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.PICMSST   := Valor;
        if Def('201', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS30.VICMSST   := Valor;
      end;
      // modificado 2010-07-07 faltava CST 41 e 50!
      40,41,50:
      begin
        (* TAG ICMS.ICMS40... *)
            // '202', 'N06'  = Grupo do CST = 40, 41, 50
        if Def('203', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS40.Orig      := Valor;
        if Def('204', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS40.CST       := Valor;
      end;
      51:
      begin
        (* TAG ICMS.ICMS51... *)
            // '205', 'N07'  = Grupo do CST = 51
        if Def('206', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.Orig      := Valor;
        if Def('207', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.CST       := Valor;
        if Def('208', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.ModBC     := Valor;
        if Def('209', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.PRedBC    := Valor;
        if Def('210', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.VBC       := Valor;
        if Def('211', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.PICMS     := Valor;
        if Def('212', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS51.VICMS     := Valor;
      end;
      60:
      begin
        (* TAG ICMS.ICMS60... *)
            // '213', 'N08'  = Grupo do CST = 60
        if Def('214', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS60.Orig      := Valor;
        if Def('215', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS60.CST       := Valor;
        if Def('216', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS60.VBCST     := Valor;
        if Def('217', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS60.VICMSST   := Valor;
      end;
      70:
      begin
        (* TAG ICMS.ICMS70... *)
            // '218', 'N09'  = Grupo do CST = 70
        if Def('219', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.Orig      := Valor;
        if Def('220', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.CST       := Valor;
        if Def('221', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.ModBC     := Valor;
        if Def('222', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.PRedBC    := Valor;
        if Def('223', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.VBC       := Valor;
        if Def('224', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.PICMS     := Valor;
        if Def('225', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.VICMS     := Valor;
        if Def('226', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.ModBCST   := Valor;
        if Def('227', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.PMVAST    := Valor;
        if Def('228', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.PRedBCST  := Valor;
        if Def('229', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.VBCST     := Valor;
        if Def('230', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.PICMSST   := Valor;
        if Def('231', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS70.VICMSST   := Valor;
      end;
      90:
      begin
        (* TAG ICMS.ICMS90... *)
            // '232', 'N10'  = Grupo do CST = 90
        if Def('233', 'N11', DmNFe_0000.QrNFEItsNICMS_Orig.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.Orig      := Valor;
        if Def('234', 'N12', DmNFe_0000.QrNFEItsNICMS_CST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.CST       := Valor;
        if Def('235', 'N13', DmNFe_0000.QrNFEItsNICMS_modBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.ModBC     := Valor;
        if Def('236', 'N15', DmNFe_0000.QrNFEItsNICMS_vBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.VBC       := Valor;
        if Def('237', 'N14', DmNFe_0000.QrNFEItsNICMS_pRedBC.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.PRedBC    := Valor;
        if Def('238', 'N16', DmNFe_0000.QrNFEItsNICMS_pICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.PICMS     := Valor;
        if Def('239', 'N17', DmNFe_0000.QrNFEItsNICMS_vICMS.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.VICMS     := Valor;
        if Def('240', 'N18', DmNFe_0000.QrNFEItsNICMS_modBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.ModBCST   := Valor;
        if Def('241', 'N19', DmNFe_0000.QrNFEItsNICMS_pMVAST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.PMVAST    := Valor;
        if Def('242', 'N20', DmNFe_0000.QrNFEItsNICMS_pRedBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.PRedBCST  := Valor;
        if Def('243', 'N21', DmNFe_0000.QrNFEItsNICMS_vBCST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.VBCST     := Valor;
        if Def('244', 'N22', DmNFe_0000.QrNFEItsNICMS_pICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.PICMSST   := Valor;
        if Def('245', 'N23', DmNFe_0000.QrNFEItsNICMS_vICMSST.Value, Valor) then
          cDetLista.Imposto.ICMS.ICMS90.VICMSST   := Valor;
      end;
      else Geral.MensagemBox(
        'CST do ICMS n�o implementado!', 'Aviso', MB_OK+MB_ICONERROR);
    end;
    if (DmNFe_0000.QrFilialSimplesFed.Value = 0) and
    (DmNFe_0000.QrNFEItsITem_IPI.Value = 1) then
    begin
      DmNFe_0000.QrNFEItsO.Close;
      DmNFe_0000.QrNFEItsO.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsO.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsO.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsO.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsO.Open;
      //
      (* TAG IPI... *)
          // '246', 'O01'  = Grupo do IPI
      if Def('247', 'O02', DmNFe_0000.QrNFEItsOIPI_clEnq.Value, Valor) then
        cDetLista.Imposto.IPI.ClEnq := Valor;
      if Def('248', 'O03', DmNFe_0000.QrNFEItsOIPI_CNPJProd.Value, Valor) then
        cDetLista.Imposto.IPI.CNPJProd := Valor;
      if Def('249', 'O04', DmNFe_0000.QrNFEItsOIPI_cSelo.Value, Valor) then
        cDetLista.Imposto.IPI.CSelo := Valor;
      if Def('250', 'O05', DmNFe_0000.QrNFEItsOIPI_qSelo.Value, Valor) then
        cDetLista.Imposto.IPI.QSelo := Valor;
      if Def('251', 'O06', DmNFe_0000.QrNFEItsOIPI_cEnq.Value, Valor) then
        cDetLista.Imposto.IPI.CEnq := Valor;

      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([0,49,50,99]) then
      begin
        (* TAG IPI.IPITRIB... *)
            // '252', 'O08'  = Grupo do CST 00, 40, 50 e 99
        if Def('253', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.CST := Valor;
        if Def('254', 'O10', DmNFe_0000.QrNFEItsOIPI_vBC.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.VBC := Valor;
        if Def('255', 'O11', DmNFe_0000.QrNFEItsOIPI_qUnid.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.QUnid := Valor;
        if Def('256', 'O12', DmNFe_0000.QrNFEItsOIPI_vUnid.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.VUnid := Valor;
        if Def('257', 'O13', DmNFe_0000.QrNFEItsOIPI_pIPI.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.PIPI := Valor;
            // '258', '???'  = ?????
        if Def('259', 'O14', DmNFe_0000.QrNFEItsOIPI_vIPI.Value, Valor) then
          cDetLista.Imposto.IPI.IPITrib.VIPI := Valor;
      end else
      if DmNFe_0000.QrNFEItsOIPI_CST.Value in ([1,2,3,4,51,52,53,54,55]) then
      begin
        (* TAG IPI.IPINT... *)
            // '260', 'O08'  = Grupo do CST 01, 02, 03, 04, 51, 52, 53, 54 e 55
        if Def('261', 'O09', DmNFe_0000.QrNFEItsOIPI_CST.Value, Valor) then
          cDetLista.Imposto.IPI.IPINT.CST := Valor;
      end else
        Geral.MensagemBox('CST do IPI desconhecido!', 'ERRO',
          MB_OK+MB_ICONERROR);
    end;
    (* TAG II... *)
    DmNFe_0000.QrNFEItsP.Close;
    DmNFe_0000.QrNFEItsP.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsP.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsP.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsP.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsP.Open;
    // '262', 'P01'  = Grupo do II
    if DmNFe_0000.QrNFEItsP.RecordCount > 0 then
    begin
      if DmNFe_0000.QrNFEItsPII_vII.Value > 0 then
      begin
        if Def('263', 'P02', DmNFe_0000.QrNFEItsPII_vBC.Value, Valor) then
          cDetLista.Imposto.II.VBC := Valor;
        if Def('264', 'P03', DmNFe_0000.QrNFEItsPII_vDespAdu.Value, Valor) then
          cDetLista.Imposto.II.VDespAdu := Valor;
        if Def('265', 'P04', DmNFe_0000.QrNFEItsPII_vII.Value, Valor) then
          cDetLista.Imposto.II.VII := Valor;
        if Def('266', 'P05', DmNFe_0000.QrNFEItsPII_vIOF.Value, Valor) then
          cDetLista.Imposto.II.VIOF := Valor;
      end;
    end;
    //

    (* TAGs PIS... *)
    // '267', 'Q01'  = Grupo do PIS
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
      cDetLista.Imposto.PIS.PISAliq.CST  := '01';
      cDetLista.Imposto.PIS.PISAliq.VBC  := '0.00';
      cDetLista.Imposto.PIS.PISAliq.PPIS := '0.00';
      cDetLista.Imposto.PIS.PISAliq.VPIS := '0.00';
    end else begin
      DmNFe_0000.QrNFEItsQ.Close;
      DmNFe_0000.QrNFEItsQ.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsQ.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsQ.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsQ.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsQ.Open;
      if DmNFe_0000.QrNFEItsQ.RecordCount = 0 then
      begin
        Geral.MensagemBox('Falta informa��es de PIS!'+#13#10+'Gera��o abortada!',
        'Erro', MB_OK+MB_ICONERROR);
        //Result := False;
        Exit;
      end;
      // '268', 'Q02' Tag do PIS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([1,2]) then
      begin
        (* TAG PIS.PISALIQ... *)
        if Def('269', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.CST := Valor;
        if Def('270', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VBC := Valor;
        if Def('271', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.PPIS := Valor;
        if Def('272', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISAliq.VPIS := Valor;
      end else
      // '273', 'Q03' Tag do PIS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([3]) then
      begin
        (* TAG PIS.PISQTDE... *)
        if Def('274', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.CST := Valor;
        if Def('275', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.QBCProd := Valor;
        if Def('276', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VAliqProd := Valor;
        if Def('277', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISQtde.VPIS := Valor;
      end else
      // '278', 'Q04' Tag do PIS n�o tributado CST = 04, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([4,6,7,8,9]) then
      begin
        (* TAG PIS.PISNT... *)
        if Def('279', 'Q02', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISNT.CST := Valor;
      end else
      // '280', 'Q05' Tag do PIS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsQPIS_CST.Value in ([99]) then
      begin
        (* TAG PIS.PISOUTR... *)
        if Def('281', 'Q06', DmNFe_0000.QrNFEItsQPIS_CST.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsQPIS_pPIS.Value > 0 then
        begin
          if Def('282', 'Q07', DmNFe_0000.QrNFEItsQPIS_vBC.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VBC := Valor;
          if Def('283', 'Q08', DmNFe_0000.QrNFEItsQPIS_pPIS.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.PPIS := Valor;
        end else begin
          if Def('284', 'Q10', DmNFe_0000.QrNFEItsQPIS_qBCProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.QBCProd := Valor;
          if Def('285', 'Q11', DmNFe_0000.QrNFEItsQPIS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PIS.PISOutr.VAliqProd := Valor;
        end;
        if Def('286', 'Q09', DmNFe_0000.QrNFEItsQPIS_vPIS.Value, Valor) then
          cDetLista.Imposto.PIS.PISOutr.VPIS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MensagemBox('CST do PIS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value), 'Erro', MB_OK+MB_ICONERROR);
          //Result := False;
        Exit;
      end;
      // '287', 'R01' Tag do PISST...
      (* TAG PISST... *)
      DmNFe_0000.QrNFEItsR.Close;
      DmNFe_0000.QrNFEItsR.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsR.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsR.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsR.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsR.Open;
      if DmNFe_0000.QrNFEItsR.RecordCount = 0 then
      begin
        Geral.MensagemBox('Falta informa��es de PIS ST!'+#13#10+'Gera��o abortada!',
        'Erro', MB_OK+MB_ICONERROR);
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) or
      (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsRPISST_pPIS.Value > 0) then
        begin
          if Def('288', 'R02', DmNFe_0000.QrNFEItsRPISST_vBC.Value, Valor) then
            cDetLista.Imposto.PISST.VBC := Valor;
          if Def('289', 'R03', DmNFe_0000.QrNFEItsRPISST_pPIS.Value, Valor) then
            cDetLista.Imposto.PISST.PPIS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value > 0) then
        begin
          if Def('290', 'R04', DmNFe_0000.QrNFEItsRPISST_qBCProd.Value, Valor) then
            cDetLista.Imposto.PISST.QBCProd := Valor;
          if Def('291', 'R05', DmNFe_0000.QrNFEItsRPISST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.PISST.VAliqProd := Valor;
        end;
        if Def('292', 'R06', DmNFe_0000.QrNFEItsRPISST_vPIS.Value, Valor) then
          cDetLista.Imposto.PISST.VPIS := Valor;
      end;
    end;
      //
    (* TAGs COFINS... *)
    // '293', 'S01'  = Grupo do COFINS
    if (DmNFe_0000.QrFilialSimplesFed.Value = 1) then
    begin
      cDetLista.Imposto.COFINS.COFINSAliq.CST     := '01';
      cDetLista.Imposto.COFINS.COFINSAliq.VBC     := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := '0.00';
      cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := '0.00';
    end else begin
      DmNFe_0000.QrNFEItsS.Close;
      DmNFe_0000.QrNFEItsS.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsS.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsS.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsS.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsS.Open;
      if DmNFe_0000.QrNFEItsS.RecordCount = 0 then
      begin
        Geral.MensagemBox('Falta informa��es de COFINS!'+#13#10+'Gera��o abortada!',
        'Erro', MB_OK+MB_ICONERROR);
        //Result := False;
        Exit;
      end;
      // '294', 'S02' Tag do COFINS tributado pela al�quota CST = 01 e 02
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([1,2]) then
      begin
        (* TAG COFINS.COFINSALIQ... *)
        if Def('295', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.CST := Valor;
        if Def('296', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VBC := Valor;
        if Def('297', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.PCOFINS := Valor;
        if Def('298', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSAliq.VCOFINS := Valor;
      end else
      // '299', 'S03' Tag do COFINS tributado por qte CST = 03
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([3]) then
      begin
        (* TAG COFINS.COFINSQTDE... *)
        if Def('300', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.CST := Valor;
        if Def('301', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.QBCProd := Valor;
        if Def('302', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VAliqProd := Valor;
        if Def('303', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSQtde.VCOFINS := Valor;
      end else
      // '304', 'S04' Tag do COFINS n�o tributado CST = 04, 06, 07, 08 ou 09
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([4,6,7,8,9]) then
      begin
        (* TAG COFINS.COFINSNT... *)
        if Def('305', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSNT.CST := Valor;
      end else
      // '306', 'S05' Tag do COFINS outras opera��es CST = 99
      if DmNFe_0000.QrNFEItsSCOFINS_CST.Value in ([99]) then
      begin
        (* TAG COFINS.COFINSOUTR... *)
        if Def('307', 'S06', DmNFe_0000.QrNFEItsSCOFINS_CST.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.CST := Valor;
        if DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value > 0 then
        begin
          if Def('308', 'S07', DmNFe_0000.QrNFEItsSCOFINS_vBC.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VBC := Valor;
          if Def('309', 'S08', DmNFe_0000.QrNFEItsSCOFINS_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.PCOFINS := Valor;
        end else begin
          if Def('310', 'S09', DmNFe_0000.QrNFEItsSCOFINS_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.QBCProd := Valor;
          if Def('311', 'S10', DmNFe_0000.QrNFEItsSCOFINS_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINS.COFINSOutr.VAliqProd := Valor;
        end;
        if Def('312', 'S11', DmNFe_0000.QrNFEItsSCOFINS_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINS.COFINSOutr.VCOFINS := Valor;
      end else
      begin
        // N�o existe CST = zero
        Geral.MensagemBox('CST do COFINS incorreto: ' +
          IntToStr(DmNFe_0000.QrNFEItsQPIS_CST.Value), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      // '313', 'T01' Tag do COFINSST...
      (* TAG COFINSST... *)
      DmNFe_0000.QrNFEItsT.Close;
      DmNFe_0000.QrNFEItsT.Params[00].AsInteger := FatID;
      DmNFe_0000.QrNFEItsT.Params[01].AsInteger := FatNum;
      DmNFe_0000.QrNFEItsT.Params[02].AsInteger := Empresa;
      DmNFe_0000.QrNFEItsT.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
      DmNFe_0000.QrNFEItsT.Open;
      if DmNFe_0000.QrNFEItsT.RecordCount = 0 then
      begin
        Geral.MensagemBox('Falta informa��es de COFINS ST!'+#13#10+'Gera��o abortada!',
        'Erro', MB_OK+MB_ICONERROR);
        //Result := False;
        Exit;
      end;
      if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) or
      (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
      begin
        if (DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value > 0) then
        begin
          if Def('314', 'T02', DmNFe_0000.QrNFEItsTCOFINSST_vBC.Value, Valor) then
            cDetLista.Imposto.COFINSST.VBC := Valor;
          if Def('315', 'T03', DmNFe_0000.QrNFEItsTCOFINSST_pCOFINS.Value, Valor) then
            cDetLista.Imposto.COFINSST.PCOFINS := Valor;
        end else
        if (DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value > 0) then
        begin
          if Def('316', 'T04', DmNFe_0000.QrNFEItsTCOFINSST_qBCProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.QBCProd := Valor;
          if Def('317', 'T05', DmNFe_0000.QrNFEItsTCOFINSST_vAliqProd.Value, Valor) then
            cDetLista.Imposto.COFINSST.VAliqProd := Valor;
        end;
        if Def('318', 'T06', DmNFe_0000.QrNFEItsTCOFINSST_vCOFINS.Value, Valor) then
          cDetLista.Imposto.COFINSST.VCOFINS := Valor;
      end;
    end;
    //
    // '319', 'U01' Tag do grupo ISSQN...
    (* TAG ISSQN.. *)
    DmNFe_0000.QrNFEItsU.Close;
    DmNFe_0000.QrNFEItsU.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsU.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsU.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsU.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsU.Open;
    if DmNFe_0000.QrNFEItsU.RecordCount > 0 then
    begin
      if DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value > 0 then
      begin
        if Def('320', 'U02', DmNFe_0000.QrNFEItsUISSQN_vBC.Value, Valor) then
          cDetLista.Imposto.ISSQN.VBC := Valor;
        if Def('321', 'U03', DmNFe_0000.QrNFEItsUISSQN_vAliq.Value, Valor) then
          cDetLista.Imposto.ISSQN.VAliq := Valor;
        if Def('322', 'U04', DmNFe_0000.QrNFEItsUISSQN_vISSQN.Value, Valor) then
          cDetLista.Imposto.ISSQN.VISSQN := Valor;
        if Def('323', 'U05', DmNFe_0000.QrNFEItsUISSQN_cMunFG.Value, Valor) then
          cDetLista.Imposto.ISSQN.CMunFG := Valor;
        if Def('324', 'U06', DmNFe_0000.QrNFEItsUISSQN_cListServ.Value, Valor) then
          cDetLista.Imposto.ISSQN.CListServ := Valor;
      end;
    end;
    // Marco 2009-07-05  Parei aqui
    DmNFe_0000.QrNFEItsV.Close;
    DmNFe_0000.QrNFEItsV.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFEItsV.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFEItsV.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFEItsV.Params[03].AsInteger := DmNFe_0000.QrNFEItsInItem.Value;
    DmNFe_0000.QrNFEItsV.Open;
    if DmNFe_0000.QrNFEItsV.RecordCount > 0 then
      InfAdProd := DmNFe_0000.QrNFEItsVinfAdProd.Value
    else
      InfAdProd := '';
    //
    if Def('325', 'V01', InfAdProd, Valor) then
      cDetLista.InfAdProd := Valor;
    //
    DmNFe_0000.QrNFEItsI.Next;
  end;

(* W - Informa��es da TAG TOTAL... *)
  (* TAG ICMSTOT... *)
      // '326', 'W01'  =  Grupo de valores totais da NF-e
      // '327', 'W02'  =  Grupo de valores totais referentes ao ICMS
  if Def('328', 'W03', DmNFe_0000.QrNFECabAICMSTot_vBC.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBC := Valor;
  if Def('329', 'W04', DmNFe_0000.QrNFECabAICMSTot_vICMS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VICMS := Valor;
  if Def('330', 'W05', DmNFe_0000.QrNFECabAICMSTot_vBCST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VBCST := Valor;
  if Def('331', 'W06', DmNFe_0000.QrNFECabAICMSTot_vST.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VST := Valor;
  if Def('332', 'W07', DmNFe_0000.QrNFECabAICMSTot_vProd.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VProd := Valor;
  if Def('333', 'W08', DmNFe_0000.QrNFECabAICMSTot_vFrete.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VFrete := Valor;
  if Def('334', 'W09', DmNFe_0000.QrNFECabAICMSTot_vSeg.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VSeg := Valor;
  if Def('335', 'W10', DmNFe_0000.QrNFECabAICMSTot_vDesc.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VDesc := Valor;
  if Def('336', 'W11', DmNFe_0000.QrNFECabAICMSTot_vII.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VII := Valor;
  if Def('337', 'W12', DmNFe_0000.QrNFECabAICMSTot_vIPI.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VIPI := Valor;
  if Def('338', 'W13', DmNFe_0000.QrNFECabAICMSTot_vPIS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VPIS := Valor;
  if Def('339', 'W14', DmNFe_0000.QrNFECabAICMSTot_vCOFINS.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VCOFINS := Valor;
  if Def('340', 'W15', DmNFe_0000.QrNFECabAICMSTot_vOutro.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VOutro := Valor;
  if Def('341', 'W16', DmNFe_0000.QrNFECabAICMSTot_vNF.Value, Valor) then
    cXML.InfNFe.Total.ICMSTot.VNF := Valor;
{
  (* TAG ISSQNTOT... Opcional *)
  {  Parei aqui
  cXML.InfNFe.Total.ISSQNtot.VServ := Valor;
  cXML.InfNFe.Total.ISSQNtot.VBC := Valor;
  cXML.InfNFe.Total.ISSQNtot.VISS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VPIS := Valor;
  cXML.InfNFe.Total.ISSQNtot.VCOFINS := Valor;

  (* TAG  RETTRIB... Opcional *)
  cXML.InfNFe.Total.RetTrib.VRetPIS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCOFINS := Valor;
  cXML.InfNFe.Total.RetTrib.VRetCSLL := Valor;
  cXML.InfNFe.Total.RetTrib.VBCIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VIRRF := Valor;
  cXML.InfNFe.Total.RetTrib.VBCRetPrev := Valor;
  cXML.InfNFe.Total.RetTrib.VRetPrev := Valor;}

(* X - Informa��es da TAG TRANSP... *)
      // '356', 'X01'  =  Grupo de informa��oes do transporte da NF-e
  if Def('357', 'X02', DmNFe_0000.QrNFECabAModFrete.Value, Valor) then
    cXML.InfNFe.Transp.ModFrete := Valor;
      // '358', 'X03'  =  Grupo do transportador
  if Geral.SoNumero1a9_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value) <> '' then
  begin
    if Def('359', 'X04', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CNPJ.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CNPJ := Valor;
  end else begin
    if Def('360', 'X05', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_CPF.Value), Valor) then
      cXML.InfNFe.Transp.Transporta.CPF := Valor;
  end;
  if Def('361', 'X06', DmNFe_0000.QrNFECabATransporta_XNome.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XNome := Valor;
  if Def('362', 'X07', Geral.SoNumero_TT(DmNFe_0000.QrNFECabATransporta_IE.Value), Valor) then
    cXML.InfNFe.Transp.Transporta.IE := FMT_IE(Valor);
  if Def('363', 'X08', DmNFe_0000.QrNFECabATransporta_XEnder.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XEnder := Valor;
  if Def('364', 'X09', DmNFe_0000.QrNFECabATransporta_XMun.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.XMun := Valor;
  if Def('365', 'X10', DmNFe_0000.QrNFECabATransporta_UF.Value, Valor) then
    cXML.InfNFe.Transp.Transporta.UF := Valor;
  //     '366', 'X11'  = Grupo de retenc��o do ICMS do transporte
  {
      PAREI AQUI
  if Def('367', 'X12', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VServ := Valor;
  if Def('368', 'X13', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VBCRet := Valor;
  if Def('369', 'X14', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.PICMSRet := Valor;
  if Def('370', 'X15', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.VICMSRet := Valor;
  if Def('371', 'X16', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CFOP := Valor;
  if Def('372', 'X17', DmNFe_0000.QrNFECabATransporta_.Value, Valor) then
    cXML.InfNFe.Transp.RetTransp.CMunFG := Valor;
  }

  (* Informa��es da TAG VEICTRANSP... Opcional *)

  //     '373', 'X18'  =  Grupo Ve�culo
  if (Trim(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value) <> '')
  and (Trim(DmNFe_0000.QrNFECabAVeicTransp_UF.Value) <> '') then
  begin
    if Def('374', 'X19', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabAVeicTransp_Placa.Value), Valor) then
      cXML.InfNFe.Transp.VeicTransp.Placa := Valor;
    if Def('375', 'X20', DmNFe_0000.QrNFECabAVeicTransp_UF.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.UF := Valor;
    if Def('376', 'X21', DmNFe_0000.QrNFECabAVeicTransp_RNTC.Value, Valor) then
      cXML.InfNFe.Transp.VeicTransp.RNTC := Valor;

    //     '377', 'X22'  =  reboque(s)
    DmNFe_0000.QrNFECabXReb.Close;
    DmNFe_0000.QrNFECabXReb.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFECabXReb.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFECabXReb.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFECabXReb.Open;
    //
    if DmNFe_0000.QrNFECabXReb.RecordCount > 0 then
    begin
      while not DmNFe_0000.QrNFECabXReb.Eof do
      begin
        cRebLista := cXML.InfNFe.Transp.Reboque.Add;
        if Def('378', 'X23', Geral.SoNumeroELetra_TT(DmNFe_0000.QrNFECabXRebplaca.Value), Valor) then
          cRebLista.Placa := Valor;
        if Def('379', 'X24', DmNFe_0000.QrNFECabXRebUF.Value, Valor) then
          cRebLista.UF := Valor;
        if Def('380', 'X25', DmNFe_0000.QrNFECabXRebRNTC.Value, Valor) then
          cRebLista.RNTC := Valor;
        //
        DmNFe_0000.QrNFECabXReb.Next;
      end;
    end;
  end;



  DmNFe_0000.QrNFECabXVol.Close;
  DmNFe_0000.QrNFECabXVol.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabXVol.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabXVol.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabXVol.Open;
  //
  if DmNFe_0000.QrNFECabXVol.RecordCount > 0 then
  begin

    i := 1; (* Essa vari�vel tem que ser incrementada no caso do Valume possuir mais de um lacre...*)

    (* Informa��es da TAG VOLUMES... Opcional *)
    //     '381', 'X26'  =  Grupo volumes
    cVolLista := cXML.InfNFe.Transp.Vol.Add;
    if Def('382', 'X27', DmNFe_0000.QrNFECabXVolqVol.Value, Valor) then
      cVolLista.QVol := Valor;
    if Def('383', 'X28', DmNFe_0000.QrNFECabXVolesp.Value, Valor) then
      cVolLista.Esp := Valor;
    if Def('384', 'X29', DmNFe_0000.QrNFECabXVolmarca.Value, Valor) then
      cVolLista.Marca := Valor;
    if Def('385', 'X30', DmNFe_0000.QrNFECabXVolnVol.Value, Valor) then
      cVolLista.NVol := Valor;
    if Def('386', 'X31', DmNFe_0000.QrNFECabXVolpesoL.Value, Valor) then
      cVolLista.PesoL := Valor;
    if Def('387', 'X32', DmNFe_0000.QrNFECabXVolpesoB.Value, Valor) then
      cVolLista.PesoB := Valor;

    (* Informa��es da TAG LACRES... Opcional *)
    //  '387a', 'X33'
    DmNFe_0000.QrNFECabXLac.Close;
    DmNFe_0000.QrNFECabXLac.Params[00].AsInteger := FatID;
    DmNFe_0000.QrNFECabXLac.Params[01].AsInteger := FatNum;
    DmNFe_0000.QrNFECabXLac.Params[02].AsInteger := Empresa;
    DmNFe_0000.QrNFECabXLac.Params[03].AsInteger := DmNFe_0000.QrNFECabXVolControle.Value;
    DmNFe_0000.QrNFECabXLac.Open;
    //
    if DmNFe_0000.QrNFECabXLac.RecordCount > 0 then
    begin
      cLacLista := cXML.InfNFe.Transp.Vol.Items[i].Lacres.Add;
      if Def('388', 'X34', DmNFe_0000.QrNFECabXLacnLacre.Value, Valor) then
        cLacLista.NLacre := Valor;
      //
      DmNFe_0000.QrNFECabXLac.Next;
    end;

    DmNFe_0000.QrNFECabXVol.Next;
  end;

  (* Y - Informa��es da TAG COBR... se Houver *)
  //   //'389', 'Y01'  =  Grupo de cobran�a
  //   //'390', 'Y02'  =  Grupo da fatura
  if Def('391', 'Y03', DmNFe_0000.QrNFECabACobr_Fat_NFat  .Value, Valor) then
    cXML.InfNFe.Cobr.Fat.NFat := Valor;
  if Def('392', 'Y04', DmNFe_0000.QrNFECabACobr_Fat_vOrig.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VOrig := Valor;
  if Def('393', 'Y05', DmNFe_0000.QrNFECabACobr_Fat_vDesc.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VDesc := Valor;
  if Def('394', 'Y06', DmNFe_0000.QrNFECabACobr_Fat_vLiq.Value, Valor) then
    cXML.InfNFe.Cobr.Fat.VLiq := Valor;

  (* Informa��es da TAG DUP...*)
  //   '395', 'Y07'  =  Grupo da duplicata
  DmNFe_0000.QrNFECabY.Close;
  DmNFe_0000.QrNFECabY.Params[00].AsInteger := FatID;
  DmNFe_0000.QrNFECabY.Params[01].AsInteger := FatNum;
  DmNFe_0000.QrNFECabY.Params[02].AsInteger := Empresa;
  DmNFe_0000.QrNFECabY.Open;
  if DmNFe_0000.QrNFECabY.RecordCount > 0 then
  begin
    while not DmNFe_0000.QrNFECabY.Eof do
    begin
      cDupLista := cXML.InfNFe.Cobr.Dup.Add;
      //
      if Def('396', 'Y08', DmNFe_0000.QrNFECabYnDup.Value, Valor) then
        cDupLista.NDup := Valor;
      if Def('397', 'Y09', DmNFe_0000.QrNFECabYdVenc.Value, Valor) then
        cDupLista.DVenc := Valor;
      if Def('398', 'Y10', DmNFe_0000.QrNFECabYvDup.Value, Valor) then
        cDupLista.VDup := Valor;
      //
      DmNFe_0000.QrNFECabY.Next;
    end;
  end;

(* Z - Informa��es da TAG INFADIC... se Houver *)
  //     '399', 'Z01' Grupo de informa��es adicionais
  if Def('400', 'Z02', DmNFe_0000.QrNFECabAInfAdic_InfAdFisco.Value, Valor) then
    cXML.InfNFe.InfAdic.InfAdFisco := Valor;
  if Def('401', 'Z03', DmNFe_0000.QrNFECabAInfAdic_InfCpl.Value, Valor) then
    cXML.InfNFe.InfAdic.InfCpl := Valor;

  (* Informa��es da TAG OBSCONT... *)
  {  Parei aqui - usar se algum cliente precisar
  //     '401a', 'Z04' Grupo do campo de uso livre do contribuinte
  if Def('401b', 'Z05', ?, Valor) then
    cXML.InfNFe.InfAdic.ObsCont.XCampo := Valor;
  if Def('401c', 'Z06', ?, Valor) then
    cXML.InfNFe.InfAdic.ObsCont.XTexto := Valor;
  }
  // Campo de uso livre do fisco
  // '401d', 'Z07'
  // '401e', 'Z08'
  // '401f', 'Z09'

  {
  // Parei aqui
  // '401g', 'Z10'  =  Grupo de processo
  cProcRefLista := cXML.InfNFe.InfAdic.ProcRef.Add;
  if Def('401h', 'Z11', ?, Valor) then
    cProcRefLista.NProc := Valor;
  if Def('401i', 'Z12', ?, Valor) then
    cProcRefLista.IndProc := Valor;
  }

  { Parei aqui = Fazer!
  (* ZA - Informa��es da TAG EXPORTA... se Houver *)
  //  '402', 'ZA01'
  if Def('403', 'ZA02', ?, Valor) then
    cXML.InfNFe.Exporta.UFEmbarq := Valor;
  if Def('404', 'ZA03', ?, Valor) then
    cXML.InfNFe.Exporta.XLocEmbarq := Valor;
  }

  {  Parei aqui
  (* ZB - Informa��es da TAG COMPRA... se Houver *)
  // '405', 'ZB01'
  if Def('406', 'ZB02', ?, Valor) then
    cXML.InfNFe.Compra.XNEmp := Valor;
  if Def('407', 'ZB03', ?, Valor) then
    cXML.InfNFe.Compra.XPed := Valor;
  if Def('408', 'ZB04', ?, Valor) then
    cXML.InfNFe.Compra.XCont := Valor;
  }

  //

  { ZC - Informa��es da assinatura digital
  // '409', 'ZC01'  -  Assinatura XML da NF-e  segundo o padr�o XML Digital signature
  }

  //

  Result := True;
end;

function TFmNFeGeraXML_0110.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo,
  Serie, nNFIni, nNFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + (*Ano +*) emitCNPJ + Modelo + StrZero(StrToInt(Serie),3,0) +
    StrZero(StrToInt(nNFIni),9,0) + StrZero(StrToInt(nNFFim),9,0);
  K := Length(Id);
  if K in ([41,42,43]) then
    Result := True
  else begin
    Result := False;
    Geral.MensagemBox('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41, 42 ou 43 carateres e tem ' + IntToStr(K) + '.', 'Erro',
    MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFeGeraXML_0110.WS_NFeRetRecepcao(UFServico: String; Ambiente,
  CodigoUF: Byte; Recibo: String; Label1: TLabel;
  RETxtEnvio: TMemo): WideString;
const
  TipoConsumo = tcwsConsultaLote;
var
  Nota: NfeRetRecepcaoSoap;
  sAviso: String;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeRetRecepcaoSoap(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verConsReciNFe_Versao, True);
  FDadosTxt := XML_ConsReciNFe(Ambiente, Recibo);
  RETxtEnvio.Text :=  'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FDadosTxt;
  Result := Nota.nfeRetRecepcao(FCabecTxt, FDadosTxt);
end;

function TFmNFeGeraXML_0110.WS_NFeCancelamentoNFe(UFServico: String; Ambiente,
  CodigoUF: Byte; ChNFe, NumeroSerial, nProt, xJust: String; Label1: TLabel;
  RETxtEnvio: TMemo): WideString;
const
  TipoConsumo = tcwsPedeCancelamento;
var
  Nota: NfeCancelamentoSoap;
  sAviso: String;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeCancelamentoSoap(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verCancNFe_Versao, False);
  FDadosTxt := XML_CancNFe(ChNFe, Ambiente, nProt, XJust);
  RETxtEnvio.Text :=  'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FDadosTxt;
  //
  //
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if NFeXMLGeren.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
      RETxtEnvio.Text := 'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FAssinTxt;
    Result := Nota.nfeCancelamentoNF(FCabecTxt, FAssinTxt);
  end;
end;

function TFmNFeGeraXML_0110.WS_NFeConsultaNF(UFServico: String; Ambiente, CodigoUF: Byte;
  ChaveNFe: String; Label1: TLabel; RETxtEnvio:
  TMemo): WideString;
const
  TipoConsumo = tcwsConsultaNFe;
var
  Nota: NfeConsulta;
  sAviso: String;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeConsulta(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verConsSitNFe_Versao, True);
  FDadosTxt := XML_ConsSitNFe(Ambiente, ChaveNFe);
  RETxtEnvio.Text :=  'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FDadosTxt;
  Result := Nota.nfeConsultaNF(FCabecTxt, FDadosTxt);
end;

function TFmNFeGeraXML_0110.WS_NFeInutilizacaoNFe(UFServico: String; Ambiente,
  CodigoUF, Ano: Byte; Id, CNPJ, Mod_, Serie, NNFIni, NNFFin: String;
  XJust, NumeroSerial: String; Label1: TLabel; RETxtEnvio: TMemo): WideString;
const
  TipoConsumo = tcwsPediInutilizacao;
var
  Nota: NFeInutilizacao;
  sAviso: String;
  //Dados: WideString;
  Cert: ICertificate2;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeInutilizacao(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verNFeInutNFe_Versao, True);
  FDadosTxt := XML_NFeInutNFe(Id, Ambiente, CodigoUF, Ano, CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust);
  //
  if NFeXMLGeren.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if NFeXMLGeren.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
      RETxtEnvio.Text := 'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FAssinTxt;
    Result := Nota.nfeInutilizacaoNF(FCabecTxt, FAssinTxt);
  end;
end;

function TFmNFeGeraXML_0110.WS_NFeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; Label1: TLabel;
  RETxtEnvio: TMemo): WideString;
const
  TipoConsumo = tcwsEnviaLoteNF;
var
  Nota: NfeRecepcaoSoap;
  sAviso: String;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeRecepcaoSoap(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verEnviNFe_Versao, False);
  FDadosTxt := FmNFeSteps_0110.FXML_Lote;
  RETxtEnvio.Text :=  'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FDadosTxt;
  //
  Result := Nota.nfeRecepcaoLote(FCabecTxt, FDadosTxt);
end;

function TFmNFeGeraXML_0110.WS_NFeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; Label1: TLabel; RETxtEnvio: TMemo):
  WideString;
const
  TipoConsumo = tcwsStatusServico;
var
  Nota: NfeStatusServicoSoap;
  sAviso: String;
  Rio: THTTPRIO;
begin
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfeStatusServicoSoap(False, FURL, Rio);
  FCabecTxt := XML_CabecMsg(verConsStatServ_Versao, False);
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  RETxtEnvio.Text :=  'URL: ' + FURL + #13#10 + FCabecTxt + #13#10 + FDadosTxt;
  //
  Result := Nota.nfeStatusServicoNF(FCabecTxt, FDadosTxt);
end;

function TFmNFeGeraXML_0110.ObtemWebServer(UFServico: String; Ambiente, CodigoUF:
  Integer; Acao: TTipoConsumoWS; sAviso: String; Label1: TLabel): Boolean;
  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
      1: Result := 'Produ��o';
      2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;
  function TextoAcao(Acao: TTipoConsumoWS): String;
  begin
    case Acao of
      tcwsStatusServico:    Result := 'Status do servi�o';
      tcwsEnviaLoteNF:      Result := 'Enviar lote de NF-e ao fisco';
      tcwsConsultaLote:     Result := 'Consultar lote enviado';
      tcwsPedeCancelamento: Result := 'Pedir cancelamento de NF-e';
      tcwsPediInutilizacao: Result := 'Pedir inutiliza��o de n�mero(s) de NF-e';
      tcwsConsultaNFe:      Result := 'Consultar NF-e';
       else Result := '[Desconhecido]';
    end;
  end;
  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS): Byte;
  begin
    case TipoConsumoWS of
      tcwsStatusServico:    Result := 0;
      tcwsEnviaLoteNF:      Result := 1;
      tcwsConsultaLote:     Result := 2;
      tcwsPedeCancelamento: Result := 3;
      tcwsPediInutilizacao: Result := 4;
      tcwsConsultaNFe:      Result := 5;
       else                 Result := Null;
    end;
  end;
var
  cUF, I, A: Integer;
begin
  //FWSDL := '';
  FURL  := '';
  // Consulta a Web Services
  // http://www.nfe.fazenda.gov.br/portal/WebServices.aspx
  A := TipoConsumoWs2Acao(Acao);
  cUF := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  I := (cUF * 100) + (A * 10) + Ambiente;
  case I of

{
0=Status do servi�o
1=Enviar lote de NF-e ao fisco
2=Consultar lote enviado
3=Pedir cancelamento de NF-e
4=Pedir inutiliza��o de n�mero(s) de NF-e
5=Consultar NF-e
}

    // PR - PARAN�

    //41=PR 0=StatusServico 1=Produ��o
    4101: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF';
    //41=PR 0=StatusServico 2=Homologa��o
    4102: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeStatusServicoNF';
    //41=PR 1=Enviar lote de NF-e ao fisco 1=Produ��o
    4111: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeRecepcao.xml';
    //41=PR 1=Enviar lote de NF-e ao fisco 2=Homologa��o
    4112: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeRecepcao';
    //41=PR 2=Consultar lote enviado 1=Produ��o
    4121: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeRetRecepcao';
    //41=PR 2=Consultar lote enviado 2=Homologa��o
    4122: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeRetRecepcao';
    //41=PR 3=Pedir cancelamento de NF-e 1=Produ��o
    4131: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeCancelamentoNF';
    //41=PR 3=Pedir cancelamento de NF-e 2=Homologa��o
    4132: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeCancelamentoNF';
    //41=PR 4=Pedir inutiliza��o de n�mero(s) de NF-e 1=Produ��o
    4141: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeInutilizacaoNF';
    //41=PR 4=Pedir inutiliza��o de n�mero(s) de NF-e 2=Homologa��o
    4142: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeInutilizacaoNF';
    //41=PR 5=Consultar NF-e 1=Produ��o
    4151: FURL := 'https://nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeConsultaNF';
    //41=PR 5=Consultar NF-e 2=Homologa��o
    4152: FURL := 'https://homologacao.nfe.fazenda.pr.gov.br/NFENWebServices/services/nfeConsultaNF';


    // MS - Mato Grosso do Sul

    //50=MS 0=StatusServico 1=Produ��o
    5001: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeStatusServico';
    //50=MS 0=StatusServico 2=Homologa��o
    5002: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeStatusServico';
    //50=MS 1=Enviar lote de NF-e ao fisco 1=Produ��o
    5011: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeRecepcao';
    //50=MS 1=Enviar lote de NF-e ao fisco 2=Homologa��o
    5012: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeRecepcao';
    //50=MS 2=Consultar lote enviado 1=Produ��o
    5021: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeRetRecepcao';
    //50=MS 2=Consultar lote enviado 2=Homologa��o
    5022: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeRetRecepcao';
    //50=MS 3=Pedir cancelamento de NF-e 1=Produ��o
    5031: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeCancelamento';
    //50=MS 3=Pedir cancelamento de NF-e 2=Homologa��o
    5032: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeCancelamento';
    //50=MS 4=Pedir inutiliza��o de n�mero(s) de NF-e 1=Produ��o
    5041: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeInutilizacao';
    //50=MS 4=Pedir inutiliza��o de n�mero(s) de NF-e 2=Homologa��o
    5042: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeInutilizacao';
    //50=MS 5=Consultar NF-e 1=Produ��o
    5051: FURL := 'https://producao.nfe.ms.gov.br/producao/services/NfeConsulta';
    //50=MS 5=Consultar NF-e 2=Homologa��o
    5052: FURL := 'https://homologacao.nfe.ms.gov.br/homologacao/services/NfeConsulta';
// CadConsultaCadastro
//URL de conex�o: https://homologacao.nfe.ms.gov.br/homologacao/services/CadConsultaCadastro


    // SVAN (SEFAZ Virtual Ambiente Nacional)

    //100=SVAN 0=StatusServico 1=Produ��o
    10001: FURL := 'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx';
    //100=SVAN 0=StatusServico 2=Homologa��o
    10002: FURL := 'https://hom.nfe.fazenda.gov.br/NFeStatusServico/NFeStatusServico.asmx';
    //100=SVAN 1=Enviar Lote 1=Produ��o
    10011: FURL := 'https://www.sefazvirtual.fazenda.gov.br/NfeRecepcao/NfeRecepcao.asmx';
    //100=SVAN 1=Enviar Lote 2=Homologa��o
    10012: FURL := 'https://hom.nfe.fazenda.gov.br/NfeRecepcao/NfeRecepcao.asmx';
    //100=SVAN 2=Consultar Lote 1=Produ��o
    10021: FURL := 'https://www.sefazvirtual.fazenda.gov.br/NFeRetRecepcao/NFeRetRecepcao.asmx';
    //100=SVAN 2=Consultar Lote 2=Homologa��o
    10022: FURL := 'https://hom.nfe.fazenda.gov.br/NFeRetRecepcao/NFeRetRecepcao.asmx';
    //100=SVAN 3=Pedir cancelamento de NF-e 1=Produ��o
    10031: FURL := 'https://www.sefazvirtual.fazenda.gov.br/NFeCancelamento/NFeCancelamento.asmx';
    //100=SVAN 3=Pedir cancelamento de NF-e 2=Homologa��o
    10032: FURL := 'https://hom.nfe.fazenda.gov.br/NFeCancelamento/NFeCancelamento.asmx';
    //100=SVAN 4=Pedir inutiliza��o de n�mero(s) de NF-e 1=Produ��o
    10041: FURL := 'https://www.sefazvirtual.fazenda.gov.br/NFeInutilizacao/NFeInutilizacao.asmx';
    //100=SVAN 4=Pedir inutiliza��o de n�mero(s) de NF-e 2=Homologa��o
    10042: FURL := 'https://hom.nfe.fazenda.gov.br/NFeInutilizacao/NFeInutilizacao.asmx';
    //100=SVAN 5=Consultar NF-e 1=Produ��o
    10051: FURL := 'https://www.sefazvirtual.fazenda.gov.br/nfeconsulta/nfeconsulta.asmx';
    //100=SVAN 5=Consultar NF-e 2=Homologa��o
    10052: FURL := 'https://hom.nfe.fazenda.gov.br/nfeconsulta/nfeconsulta.asmx';

    // SEFAZ Virtual RS

    //143=SVRS 0=StatusServico 2=Homologa��o

    //143=SVAN 0=StatusServico 1=Produ��o
    14301: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nfestatusservico/NfeStatusServico.asmx';
    //143=SVAN 0=StatusServico 2=Homologa��o
    14302: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfestatusservico/NfeStatusServico.asmx';
    //143=SVAN 1=Enviar Lote 1=Produ��o
    14311: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nferecepcao/NfeRecepcao.asmx';
    //143=SVAN 1=Enviar Lote 2=Homologa��o
    14312: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferecepcao/NfeRecepcao.asmx';
    //143=SVAN 2=Consultar Lote 1=Produ��o
    14321: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nferetrecepcao/NfeRetRecepcao.asmx';
    //143=SVAN 2=Consultar Lote 2=Homologa��o
    14322: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nferetrecepcao/NfeRetRecepcao.asmx';
    //143=SVAN 3=Pedir cancelamento de NF-e 1=Produ��o
    14331: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nfecancelamento/NfeCancelamento.asmx';
    //143=SVAN 3=Pedir cancelamento de NF-e 2=Homologa��o
    14332: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfecancelamento/NfeCancelamento.asmx';
    //143=SVAN 4=Pedir inutiliza��o de n�mero(s) de NF-e 1=Produ��o
    14341: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nfeinutilizacao/NfeInutilizacao.asmx';
    //143=SVAN 4=Pedir inutiliza��o de n�mero(s) de NF-e 2=Homologa��o
    14342: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfeinutilizacao/NfeInutilizacao.asmx';
    //143=SVAN 5=Consultar NF-e 1=Produ��o
    14351: FURL := 'https://nfe.sefazvirtual.rs.gov.br/ws/nfeconsulta/NfeConsulta.asmx';
    //143=SVAN 5=Consultar NF-e 1=Homologa��o
    14352: FURL := 'https://homologacao.nfe.sefazvirtual.rs.gov.br/ws/nfeconsulta/NfeConsulta.asmx';
  end;
  //
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + #13#10 +
    'UF SEFAZ: ' + IntToStr(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + #13#10 + 'UF Servi�o: ' + IntToStr(cUF) + ' = ' + UFServico
    + #13#10 + 'Ambiente: ' + IntToStr(Ambiente) + ' = ' + TextoAmbiente(
    Ambiente) + #13#10 + 'A��o: ' + IntToStr(A) + ' = ' + TextoAcao(Acao) +
    #13#10 + 'AVISE A DERMATEK';
    Result := False;
    Geral.MensagemBox(sAviso, 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
  //Label1.Caption := sAviso;
end;

procedure TFmNFeGeraXML_0110.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
{
var
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : Pointer;
begin
  Cert := FConfiguracoes.Certificados.GetCertificado;
  CertContext :=  Cert as ICertContext;
  CertContext.Get_CertContext(Integer(PCertContext));

  if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext,sizeof(CertContext)*5) then
   begin
     if Assigned(TACBrNFe( FACBrNFe ).OnGerarLog) then
        TACBrNFe( FACBrNFe ).OnGerarLog('ERRO: Erro OnBeforePost: ' + IntToStr(GetLastError));
     raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;
}
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
begin
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(FmNFeSteps_0110.EdSerialNumber.Text) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MensagemBox('Falha ao selecionar o certificado.', 'Aviso', MB_OK+MB_ICONWARNING);
        end;

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;

function TFmNFeGeraXML_0110.OutroPais_dest(): Boolean;
begin
  Result :=
  (DmNFe_0000.QrNFECabAdest_cPais.Value <> 0) and
  (DmNFe_0000.QrNFECabAdest_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0110.OutroPais_emit: Boolean;
begin
  Result :=
  (DmNFe_0000.QrNFECabAemit_cPais.Value <> 0) and
  (DmNFe_0000.QrNFECabAemit_cPais.Value <> 1058);
end;

function TFmNFeGeraXML_0110.OutroPais_entrega: Boolean;
begin
  Result := DmNFe_0000.QrNFECabGentrega_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0110.OutroPais_retirada(): Boolean;
begin
  Result := DmNFe_0000.QrNFECabFretirada_UF.Value = 'EX';
end;

function TFmNFeGeraXML_0110.StrZero(Num: Real; Zeros, Deci: Integer): String;
var
  tam, z : Integer;
  res, zer : String;
begin
  str(Num:Zeros:Deci, res);
  res := Alltrim(res);
  tam := length(res);
  zer := '';
  for z := 1 to (Zeros-tam) do
    zer := zer + '0';
  result := zer+res
end;

function TFmNFeGeraXML_0110.TipoXML(NoStandAlone: Boolean): WideString;
begin
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

function TFmNFeGeraXML_0110.VarTypeValido(Valor: Variant; Nome: String): Boolean;
var
  Tipo: TVarType;
begin
  Tipo := VarType(Valor);
  case Tipo of
    varEmpty    : Result := False; //= $0000; { vt_empty        0 }
    varNull     : Result := False;     //= $0001; { vt_null         1 }
    varSmallint : Result := Valor > 0; //= $0002; { vt_i2           2 }
    varInteger  : Result := Valor > 0;  //= $0003; { vt_i4           3 }
    varSingle   : Result := Valor > 0;  //= $0004; { vt_r4           4 }
    varDouble   : Result := Valor > 0;  //= $0005; { vt_r8           5 }
    varCurrency : Result := Valor > 0; //= $0006; { vt_cy           6 }
    varDate     : Result := Valor > 0;  //= $0007; { vt_date         7 }
    //varOleStr   : Result := 'OleStr ';  //= $0008; { vt_bstr         8 }
    //varDispatch : Result := 'Dispatch'; //= $0009; { vt_dispatch     9 }
    //varError    : Result := 'Error  ';  //= $000A; { vt_error       10 }
    varBoolean  : Result := Valor = True;  //= $000B; { vt_bool        11 }
    varVariant  : Result := Geral.VariantToString(Valor) <> '';  //= $000C; { vt_variant     12 }
    //varUnknown  : Result := 'Unknown';  //= $000D; { vt_unknown     13 }
  //varDecimal  = $000E; { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
  //varUndef0F  = $000F; { undefined      15 } {UNSUPPORTED per Microsoft}
    varShortInt : Result := Valor > 0; //= $0010; { vt_i1          16 }
    varByte     : Result := Valor > 0;  //= $0011; { vt_ui1         17 }
    varWord     : Result := Valor > 0;  //= $0012; { vt_ui2         18 }
    varLongWord : Result := Valor > 0; //= $0013; { vt_ui4         19 }
    varInt64    : Result := Valor > 0;  //= $0014; { vt_i8          20 }
  //varWord64   = $0015; { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
  {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

    //varStrArg   : Result := 'StrArg';   //= $0048; { vt_clsid       72 }
    varString   : Result := Valor <> '';  //= $0100; { Pascal string 256 } {not OLE compatible }
    //varAny      : Result := 'Any    ';  //= $0101; { Corba any     257 } {not OLE compatible }
    // custom types range from $110 (272) to $7FF (2047)

    //varTypeMask : Result := 'TypeMask'; //= $0FFF;
    //varArray    : Result := 'Array  ';  //= $2000;
    //varByRef    : Result := 'ByRef  ';  //= $4000;
    $0008       : Result := Valor <> '';
    else begin
      Geral.MensagemBox('Tipo de vari�vel desconhecida: ' + #13#10 +
      'Nome da vari�vel: ' + Nome + #13#10 +
      'Tipo de vari�vel = '  + IntToHex(VarType(Valor), 4), 'Erro', MB_OK+MB_ICONERROR);
      Geral.MensagemBox('Valor vari�vel desconhecida: ' + #13#10 +
      Geral.VariantToString(Valor), 'Aviso', MB_OK+MB_ICONWARNING);
      Result := False;
    end;
  end;
end;

function TFmNFeGeraXML_0110.XML_CabecMsg(VersaoDados: String; NoStandAlone: Boolean): WideString;
var
  XML_CabecMsg_Str: IXMLCabecMsg;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_CabecMsg_Str := GetcabecMsg(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_CabecMsg_Str.Versao      := verCabecMsg_Versao;
    XML_CabecMsg_Str.VersaoDados := VersaoDados;
    //
    Result := TipoXML(NoStandAlone) + XML_CabecMsg_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0110.XML_CancNFe(chNFe: String; TpAmb: Integer; nProt, XJust: String): WideString;
var
  XML_CancNFe_Str: IXMLTCancNFe;
  //xUF, xAno,
  Id: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_CancNFe_Str := GetCancNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    Id := 'ID' + ChNFe;
    XML_CancNFe_Str.Versao         := verCancNFe_Versao;
    XML_CancNFe_Str.InfCanc.Id     := Id;
    XML_CancNFe_Str.InfCanc.TpAmb  := FormatFloat('0', TpAmb);
    XML_CancNFe_Str.InfCanc.XServ  := 'CANCELAR';
    XML_CancNFe_Str.InfCanc.ChNFe  := ChNFe;
    XML_CancNFe_Str.InfCanc.NProt  := NProt;
    XML_CancNFe_Str.InfCanc.XJust  := xJust;
    Result := TipoXML(False) + XML_CancNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0110.XML_ConsReciNFe(TpAmb: Integer;
  NRec: String): WideString;
var
  XML_ConsReciNFe_Str: IXMLTConsReciNFe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsReciNFe_Str := GetConsReciNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_ConsReciNFe_Str.Versao      := verConsReciNFe_Versao;
    XML_ConsReciNFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsReciNFe_Str.NRec        := NRec;
    //
    Result := TipoXML(False) + XML_ConsReciNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0110.XML_ConsSitNFe(TpAmb: Integer; ChNFe: String): WideString;
var
  XML_ConsSitNFe_Str: IXMLTConsSitNFe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsSitNFe_Str := GetConsSitNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_ConsSitNFe_Str.Versao      := verConsSitNFe_Versao;
    XML_ConsSitNFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsSitNFe_Str.XServ       := 'CONSULTAR';
    XML_ConsSitNFe_Str.ChNFe       := ChNFe;
    //
    Result := TipoXML(False) + XML_ConsSitNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0110.XML_ConsStatServ(TpAmb, CUF: Integer): WideString;
var
  XML_ConsStatServ_Str: IXMLTConsStatServ;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsStatServ_Str := GetConsStatServ(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    {
    '<consStatServ xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07">'+
    '<tpAmb>'+ IntToStr(Ambiente) +'</tpAmb>'+
    '<cUF>' + IntToStr(CodigoUF) + '</cUF><xServ>STATUS</xServ></consStatServ>');
    }
    XML_ConsStatServ_Str.Versao      := verConsStatServ_Versao;
    XML_ConsStatServ_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsStatServ_Str.CUF         := FormatFloat('00', CUF);
    XML_ConsStatServ_Str.XServ       := 'STATUS';
    //
    Result := TipoXML(False) + XML_ConsStatServ_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmNFeGeraXML_0110.XML_NFeInutNFe(Id: String; TpAmb, CUF: Byte; Ano: Integer;
CNPJ, Mod_, Serie, NNFIni, NNFFin, XJust: String): WideString;
var
  XML_NFeInutNFe_Str: IXMLTInutNFe;
  xUF, xAno: String;
begin
  xUF  := FormatFloat('00', CUF);
  xAno := FormatFloat('00', Ano);
  MontaID_Inutilizacao(xUF, xAno, CNPJ, Mod_, Serie, nNFIni, nNFFin, Id);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_NFeInutNFe_Str := GetinutNFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
    }
    XML_NFeInutNFe_Str.Versao         := verNFeInutNFe_Versao;
    XML_NFeInutNFe_Str.InfInut.Id     := Id;
    XML_NFeInutNFe_Str.InfInut.TpAmb  := FormatFloat('0', TpAmb);
    XML_NFeInutNFe_Str.InfInut.XServ  := 'INUTILIZAR';
    XML_NFeInutNFe_Str.InfInut.CUF    := FormatFloat('00', CUF);
    XML_NFeInutNFe_Str.InfInut.Ano    := FormatFloat('00', Ano);
    XML_NFeInutNFe_Str.InfInut.CNPJ   := CNPJ;
    XML_NFeInutNFe_Str.InfInut.Mod_   := Mod_;
    XML_NFeInutNFe_Str.InfInut.Serie  := Serie;
    XML_NFeInutNFe_Str.InfInut.NNFIni := NNFIni;
    XML_NFeInutNFe_Str.InfInut.NNFFin := NNFFin;
    XML_NFeInutNFe_Str.InfInut.XJust  := XJust;
   (*
    //
  - <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
    - <SignedInfo>
        <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
        <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
      - <Reference URI="#ID2109060176240001065599000000000000000001">
        - <Transforms>
            <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
            <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
          </Transforms>
          <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
          <DigestValue>4hX5W2/XhqUVhBH1KZTZfmM3dLU=</DigestValue>
        </Reference>
      </SignedInfo>
        <SignatureValue></SignatureValue>
    - <KeyInfo>
      - <X509Data>
          <X509Certificate></X509Certificate>
        </X509Data>
      </KeyInfo>
    </Signature>
  </inutNFe>
  *)
    Result := TipoXML(False) + XML_NFeInutNFe_Str.XML;
  finally
    FdocXML := nil;
  end;
{
'<?xml version="1.0" encoding="UTF-8"?>
<inutNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.07">
<infInut Id="' + Id + '">
  <tpAmb>'+ tpAmb +'</tpAmb>
  <xServ>INUTILIZAR</xServ>
  <cUF>'+cUF+'</cUF>
  <ano>'+ Ano +'</ano>');
  '<CNPJ>'+ emitCNPJ +'</CNPJ>
  <mod>'+Modelo+'</mod>
  <serie>'+ Serie +'</serie>
  <nNFIni>'+ nNFIni +'</nNFIni>
  <nNFFin>'+ nNFFim +'</nNFFin>
  <xJust>'+ iJust +'</xJust>
</infInut>
</inutNFe>');
}
end;


function TFmNFeGeraXML_0110.ExecutaAssinatura(aValue: AnsiString; Certificado:
ICertificate2; URIs: TStringList; out sXML: AnsiString): Boolean;
(** ASSINATURA **)
var
  ////////////// A T E N � � O /////////////////////////////////////////////////
  //                                                                          //
  // ADICIONAR AO PROJETO AS SEGUINTES UNITS:                                 //
  // C:\Projetos\Delphi 2007\TLB\MSXML2_TLB.pas                               //
  // C:\Projetos\Delphi 2007\TLB\CAPICOM_TLB.pas                              //
  //                                                                          //
  // COLOCAR AS SEGUINTES DLLS NO WINDOWS:                                    //
  // MSXML5.DLL, MSXML5R.DLL E CAPICOM.DLL                                    //
  //                                                                          //
  // REGISTRAR AS DLL NO APLICATIVO EXECUTAR.EXE DO WINDOWS:                  //
  // regsvr32 msxml5.dll (/s para silencioso = sem aviso)                     //
  // regsvr32 capicom.dll                                                     //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  XMLDoc                           : IXMLDOMDocument3; // ver aviso acima !!!
  XMLDSig                          : IXMLDigitalSignature;
  dsigKey                          : IXMLDSigKey;
  signedKey                        : IXMLDSigKey;
  nodePai, nodeX509Data, nodeIrmao : IXMLDOMNode;

(** CERTIFICADO **)
var
  CertStore                        : IStore3;
  CertStoreMem                     : IStore3;
  PrivateKey                       : IPrivateKey;
  Certs                            : ICertificates2;
  Cert                             : ICertificate2;

(** GERAL **)
var
  C, I                             : Integer;
  xmlHeaderAntes, xmlHeaderDepois  : String;

  function FindNode(lstNodes : IXMLDOMNodeList; strNome:string):IXMLDOMNode;
  var
    node, noderet: IXMLDOMNode;
  begin
    result := nil;
    node   := lstNodes.nextNode as IXMLDOMNode;
    while node <> nil do
    begin
      If node.nodeName = strNome then
      begin
        result := node;
        exit;
      end;

      if node.hasChildNodes then
      begin
        noderet := findNode(node.childNodes,strNome);
        if noderet <> nil then
        begin
          result := noderet;
          exit;
        end;
      end;

      node := lstNodes.nextNode as IXMLDOMNode;
    end;
  end;

  function FindNodeURI(lstNodes: IXMLDOMNodeList; Tag, URI: String) : IXMLDOMNode;
  var
    Node, NodeRet: IXMLDOMNode;
    TextXML: String;
    P: Integer;
  begin
     Result := nil;
     Node   := (lstNodes.nextNode as IXMLDOMNode);
     while (Node <> nil) do
     begin
        TextXML := Node.XML;
        P := pos(URI,TextXML);
        if (Node.NodeName = Tag) and (P > 0) Then
        begin
           Result := Node;
           Exit;
        end;
        if (Node.HasChildNodes) then
        begin
           NodeRet := FindNodeURI(Node.ChildNodes,Tag,URI);
           if (NodeRet <> nil) then
           begin
              Result := NodeRet;
              Exit;
           end;
        end;
        Node := (lstNodes.nextNode as IXMLDOMNode);
     end;
  end;

  procedure DeleteFindNode(TagURI: String);
  begin
    nodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', TagURI);
    nodePai := FindNode(NodePai.ChildNodes,'KeyInfo');
    if (nodePai <> nil) then
    begin
      nodeX509Data := findNode(nodePai.childNodes,'X509Data');
      nodeIrmao    := nodeX509Data.nextSibling;
      while nodeIrmao <> nil do
      begin
        nodePai.removeChild(nodeIrmao);
        nodeIrmao := nodeX509Data.nextSibling;
      end;
    end;
  end;

begin
  If (Trim(aValue) = '') then
    raise Exception.Create('N�o existe  informa��o para fazer a Assinatura Digital');

  aValue:=StringReplace( aValue, #10, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #13, '', [rfReplaceAll] );
  aValue:=StringReplace( aValue, #9, '', [rfReplaceAll] );


   (* Pegando o header antes de assinar *)
  xmlHeaderAntes := '' ;
  I := pos('?>', aValue) ;
  if I > 0 then
    xmlHeaderAntes := copy(aValue, 1, I+1);

  (*** CONFIGURANDO O XML DOC ***)
  XMLDoc := CoDOMDocument50.Create;

  XMLDoc.async := FALSE;
  XMLDoc.validateOnParse := FALSE;
  XMLDoc.preserveWhiteSpace := TRUE;
  (******************************)

  XMLDSig := CoMXDigitalSignature50.Create;

  if (not XMLDoc.LoadXML(aValue) ) then
    raise Exception.Create('N�o foi poss�vel carregar o "texto" XML');

  XMLDoc.setProperty('SelectionNamespaces', DSIGNS); //DSIGNS->Constante declarada

  C:=0;
  repeat
    NodePai := FindNodeURI(XMLDoc.ChildNodes, 'Signature', URIs.Strings[C]);

    XMLDSig.signature := NodePai;

    If (XMLDSig.signature = nil) then
      raise Exception.Create('Falha ao setar assinatura.');

    If (XMLDSig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   {
   if Certificado.SerialNumber <> SerialCertificado then
      CertStoreMem := nil;
   }

    if CertStoreMem = nil then
    begin

      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for I := 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
        begin
          CertStoreMem.Add(Cert);
        end;
      end;

    end;

    OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey, PrivateKey));

    xmldsig.store := CertStoreMem;

    dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
    if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP.');

    signedKey := xmldsig.sign(dsigKey, $00000002);

    if (signedKey = nil) then
      raise Exception.Create('Assinatura Falhou.');

    if (signedKey <> nil) then
      DeleteFindNode(URIs.Strings[C]);

    Inc(C);
  until C>=URIs.Count;

  sXML:=XMLDoc.Xml;

  if xmlHeaderAntes <> '' then
  begin
    I:=pos('?>', sXML);
    if I > 0 then
    begin
      xmlHeaderDepois:=copy(sXML,1,I+1);
      if xmlHeaderAntes <> xmlHeaderDepois then
        sXML:=StuffString(sXML, 1, length(xmlHeaderDepois), xmlHeaderAntes);
    end else
      sXML:=xmlHeaderAntes + sXML;
  end;

  Result := True;
  //
  //
  ShowMessage(sXML);
  //
  //
  dsigKey   := nil;
  signedKey := nil;
  xmldoc    := nil;
  xmldsig   := nil;
end;

{
Para Validar utilize esse c�digo: Este valida xml s� est� validando o lote RPS para validar os demais xml vc tem que modificar.
Function TfrmMain.Valida_XML(XML: AnsiString; SchemaPath : String): Boolean;
var
  DOMDocument : IXMLDOMDocument3;
  ParseError  : IXMLDOMParseError;
  Schema      : XMLSchemaCache;

begin
  DOMDocument:=CoDOMDocument50.Create;

  DOMdocument.Async:=FALSE;
  DOMdocument.ResolveExternals:=FALSE;
  DOMdocument.ValidateOnParse:=TRUE;

  DOMdocument.LoadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_simples.xsd', SchemaPath+'Tipos_Simples.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_complexos.xsd', SchemaPath+'Tipos_Complexos.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/servico_enviar_lote_rps_envio.xsd', SchemaPath+'servico_enviar_lote_rps_envio.xsd');

  DOMdocument.Schemas := Schema;

  ParseError:=DOMdocument.validate;
  Result:=(ParseError.errorCode = 0);

  if ParseError.errorCode <> S_OK then
    raise Exception.Create(IntTostr(ParseError.errorCode)+' - '+ParseError.reason);

  DOMDocument:=Nil;
  ParseError:=Nil;
  Schema:=Nil;
end;

Function TfrmMain.Valida_XML(XML: AnsiString; SchemaPath : String): Boolean;
var
  DOMDocument : IXMLDOMDocument3;
  ParseError  : IXMLDOMParseError;
  Schema      : XMLSchemaCache;

begin
  DOMDocument:=CoDOMDocument50.Create;

  DOMdocument.Async:=FALSE;
  DOMdocument.ResolveExternals:=FALSE;
  DOMdocument.ValidateOnParse:=TRUE;

  DOMdocument.LoadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_simples.xsd', SchemaPath+'Tipos_Simples.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/tipos_complexos.xsd', SchemaPath+'Tipos_Complexos.xsd');
  Schema.add('http://bhissdigital.pbh.gov.br/bhiss-ws/schemas/servico_enviar_lote_rps_envio.xsd', SchemaPath+'servico_enviar_lote_rps_envio.xsd');

  DOMdocument.Schemas := Schema;

  ParseError:=DOMdocument.validate;
  Result:=(ParseError.errorCode = 0);

  if ParseError.errorCode <> S_OK then
    raise Exception.Create(IntTostr(ParseError.errorCode)+' - '+ParseError.reason);

  DOMDocument:=Nil;
  ParseError:=Nil;
  Schema:=Nil;
end;
}

{
function TFmNFeGeraXML.PosLast(const SubStr, S: AnsiString ): Integer;
var
  P : Integer ;
begin
  Result := 0 ;
  P := Pos( SubStr, S) ;
  while P <> 0 do
  begin
     Result := P ;
     P := PosEx( SubStr, S, P+1) ;
  end ;
end;
}

end.
