unit NFeValidaXML_0110;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, MSXML2_TLB, dmkEdit,
  dmkImage, UnDmkEnums, NFe_PF;

type
  TFmNFeValidaXML_0110 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    MeLoaded: TMemo;
    Panel4: TPanel;
    MeScaned: TMemo;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Label64: TLabel;
    EdSchema: TdmkEdit;
    Label1: TLabel;
    EdArquivo: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdArquivoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTextoArq: AnsiString;
    function ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    function ValidaMSXML_0110(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    procedure AbreArquivoSelecionado();
    function RetornarConteudoEntre(const Frase, Inicio, Fim: string): string;
  public
    { Public declarations }
  end;

  var
    FmNFeValidaXML_0110: TFmNFeValidaXML_0110;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts;

{$R *.DFM}

procedure TFmNFeValidaXML_0110.AbreArquivoSelecionado();
var
  Arquivo: String;
begin
  FTextoArq := '';
  Arquivo := EdArquivo.Text;
  if FileExists(Arquivo) then
  begin
    FTextoArq := MLAGeral.LoadFileToText(Arquivo);
    MeLoaded.Text := FTextoArq;
  end;
end;

procedure TFmNFeValidaXML_0110.BtOKClick(Sender: TObject);
begin
  ValidaXML(1.10, FTextoArq);
end;

procedure TFmNFeValidaXML_0110.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeValidaXML_0110.EdArquivoChange(Sender: TObject);
begin
  AbreArquivoSelecionado();
end;

procedure TFmNFeValidaXML_0110.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeValidaXML_0110.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdSchema.Text := UnNFe_PF.SchemaNFe(Geral.IMV(VAR_LIB_EMPRESAS));
end;

procedure TFmNFeValidaXML_0110.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFeValidaXML_0110.RetornarConteudoEntre(const Frase, Inicio,
  Fim: string): string;
var
  i: integer;
  s: string;
begin
  result := '';
  i := pos(Inicio, Frase);
  if i = 0 then
    exit;
  s := Copy(Frase, i + length(Inicio), maxInt);
  result := Copy(s, 1, pos(Fim, s) - 1);
end;

procedure TFmNFeValidaXML_0110.SpeedButton1Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione a pasta com os Schemas', '', [], Arquivo) then
    EdSchema.Text := ExtractFilePath(Arquivo);
end;

procedure TFmNFeValidaXML_0110.SpeedButton2Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    EdArquivo.Text := Arquivo;
end;

function TFmNFeValidaXML_0110.ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
 TextoAValidar: AnsiString;
begin
  Result := False;
  if pos('<Signature', XML) = 0 then
  begin
    Geral.MensagemBox('Nota n�o pode ser validada pois n�o foi assinada!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  case Trunc((Versao + 0.009) * 100) of
    110:
    begin
      TextoAValidar := '<NFe xmlns' + RetornarConteudoEntre(XML, '<NFe xmlns', '</NFe>')+ '</NFe>';
      Result := ValidaMSXML_0110(TextoAValidar, APathSchemas);
    end
    else Geral.MensagemBox('ValidaXML: Vers�o do XML n�o implementada : ' +
       FloatToStr(Versao), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Result then
    MeScaned.Text := 'Arquivo v�lido!' + #13#10 + MeScaned.Text;
end;

function TFmNFeValidaXML_0110.ValidaMSXML_0110(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
  DOMDocument: IXMLDOMDocument2;
  ParseError: IXMLDOMParseError;
  Schema: msXMLSchemaCache;
  Tipo, I : Integer;
  Dir, Arquivo: String;
begin
  Result := False;
  //
  I := pos('<infNFe',XML) ;
  Tipo := 1;
  if I = 0  then
   begin
     I := pos('<infCanc',XML) ;
     if I > 0 then
        Tipo := 2
     else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
           Tipo := 3
        else
           Tipo := 4;
      end;
   end;

  DOMDocument := CoDOMDocument50.Create;
  DOMDocument.async := False;
  DOMDocument.resolveExternals := False;
  DOMDocument.validateOnParse := True;
  DOMDocument.loadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Dir := EdSchema.Text;
  if not Geral.VerificaDir(
    Dir, '\', 'Diretorio de Schemas xml (*.xsd)', True) then Exit;
  //
  case Tipo of
    1: Arquivo := Dir + 'nfe_v1.10.xsd';
    2: Arquivo := Dir + 'cancNFe_v1.07.xsd';
    3: Arquivo := Dir + 'inutNFe_v1.07.xsd';
    4: Arquivo := Dir + 'envDPEC_v1.01.xsd';
    else Arquivo := '### ERRO ###';
  end;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox('O Schema XML n�o foi localizado:' + #13#10 +
    Arquivo, 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Schema.add( 'http://www.portalfiscal.inf.br/nfe', Arquivo);

  DOMDocument.schemas := Schema;
  ParseError := DOMDocument.validate;
  Result := (ParseError.errorCode = 0);
  MeScaned.Text := ParseError.reason;

  DOMDocument := nil;
  ParseError := nil;
  Schema := nil;
end;

end.
