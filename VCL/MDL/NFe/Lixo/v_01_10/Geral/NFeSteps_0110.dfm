object FmNFeSteps_0110: TFmNFeSteps_0110
  Left = 339
  Top = 185
  Caption = 'NFe-STEPS-001 :: Passos da  NF-e 1.10'
  ClientHeight = 567
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnConfirma: TPanel
    Left = 0
    Top = 519
    Width = 775
    Height = 48
    Align = alBottom
    TabOrder = 1
    Visible = False
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 663
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 48
    Align = alTop
    Caption = 'Passos da NF-e 1.10'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 773
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 775
    Height = 25
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 170
      Height = 14
      Caption = 'Aguarde... Inciando processo...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 73
    Width = 775
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object PnLoteEnv: TPanel
      Left = 0
      Top = 0
      Width = 775
      Height = 446
      Align = alClient
      TabOrder = 0
      object LaExpiraCertDigital: TLabel
        Left = 1
        Top = 271
        Width = 773
        Height = 24
        Align = alTop
        Alignment = taCenter
        Caption = 'Expira'#231#227'o do Certificado Digital'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 316
      end
      object Panel4: TPanel
        Left = 1
        Top = 397
        Width = 773
        Height = 48
        Align = alBottom
        TabOrder = 0
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 46
          Align = alLeft
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Posi'#231#227'o do cursor:'
          end
          object dmkEdit1: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object dmkEdit2: TdmkEdit
            Left = 88
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object dmkEdit3: TdmkEdit
            Left = 180
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object REWarning: TRichEdit
          Left = 269
          Top = 1
          Width = 503
          Height = 46
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 4227327
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
      end
      object PnJustificativa: TPanel
        Left = 1
        Top = 226
        Width = 773
        Height = 45
        Align = alTop
        TabOrder = 1
        Visible = False
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 169
          Height = 13
          Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
        end
        object EdNFeJust: TdmkEditCB
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBNFeJust
        end
        object CBNFeJust: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 705
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsNFeJust
          TabOrder = 1
          dmkEditCB = EdNFeJust
          UpdType = utYes
        end
      end
      object PnCancInutiliza: TPanel
        Left = 1
        Top = 129
        Width = 773
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object PnChaveNFe: TPanel
          Left = 213
          Top = 0
          Width = 288
          Height = 49
          Align = alLeft
          TabOrder = 0
          Visible = False
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 127
            Height = 13
            Caption = 'Chave de acesso da NF-e:'
          end
          object EdchNFe: TEdit
            Left = 4
            Top = 20
            Width = 280
            Height = 21
            MaxLength = 255
            ReadOnly = True
            TabOrder = 0
          end
        end
        object Panel8: TPanel
          Left = 697
          Top = 0
          Width = 76
          Height = 49
          Align = alClient
          TabOrder = 1
        end
        object PnRecibo: TPanel
          Left = 0
          Top = 0
          Width = 213
          Height = 49
          Align = alLeft
          TabOrder = 2
          Visible = False
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 37
            Height = 13
            Caption = 'Recibo:'
          end
          object EdRecibo: TdmkEdit
            Left = 4
            Top = 20
            Width = 204
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object PnProtocolo: TPanel
          Left = 501
          Top = 0
          Width = 116
          Height = 49
          Align = alLeft
          TabOrder = 3
          object Label9: TLabel
            Left = 4
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
          end
          object EdnProt: TEdit
            Left = 4
            Top = 20
            Width = 108
            Height = 21
            MaxLength = 255
            ReadOnly = True
            TabOrder = 0
          end
        end
        object PnIDCtrl: TPanel
          Left = 617
          Top = 0
          Width = 80
          Height = 49
          Align = alLeft
          TabOrder = 4
          object Label16: TLabel
            Left = 4
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Controle NFe:'
          end
          object EdIDCtrl: TdmkEdit
            Left = 5
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object PnInutiliza: TPanel
        Left = 1
        Top = 178
        Width = 773
        Height = 48
        Align = alTop
        Enabled = False
        TabOrder = 3
        Visible = False
        object Label14: TLabel
          Left = 376
          Top = 4
          Width = 73
          Height = 13
          Caption = 'CNPJ empresa:'
        end
        object Label15: TLabel
          Left = 492
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object GroupBox1: TGroupBox
          Left = 89
          Top = 1
          Width = 280
          Height = 46
          Align = alLeft
          Caption = ' Intervalo de numera'#231#227'o a ser inutilizado:  '
          TabOrder = 0
          object Label10: TLabel
            Left = 8
            Top = 20
            Width = 61
            Height = 13
            Caption = 'N'#186' NF inicial:'
          end
          object Label11: TLabel
            Left = 148
            Top = 20
            Width = 54
            Height = 13
            Caption = 'N'#186' NF final:'
          end
          object EdnNFIni: TdmkEdit
            Left = 72
            Top = 16
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdnNFFim: TdmkEdit
            Left = 204
            Top = 16
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 88
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label12: TLabel
            Left = 4
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Modelo:'
          end
          object Label13: TLabel
            Left = 48
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object EdModelo: TdmkEdit
            Left = 4
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '55'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 55
          end
          object EdSerie: TdmkEdit
            Left = 48
            Top = 20
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '899'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object EdEmitCNPJ: TdmkEdit
          Left = 376
          Top = 20
          Width = 112
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdAno: TdmkEdit
          Left = 492
          Top = 20
          Width = 25
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
      end
      object PnConfig1: TPanel
        Left = 1
        Top = 65
        Width = 773
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Panel6: TPanel
          Left = 113
          Top = 0
          Width = 404
          Height = 64
          Align = alLeft
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 12
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label2: TLabel
            Left = 124
            Top = 13
            Width = 53
            Height = 13
            Caption = 'UF (WS):'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label17: TLabel
            Left = 4
            Top = 40
            Width = 127
            Height = 13
            Caption = 'Serial do Certificado digital:'
          end
          object Label18: TLabel
            Left = 256
            Top = 12
            Width = 74
            Height = 13
            Caption = 'Servi'#231'o [F4]:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdEmpresa: TdmkEdit
            Left = 56
            Top = 8
            Width = 61
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdEmpresaChange
          end
          object EdSerialNumber: TdmkEdit
            Left = 136
            Top = 36
            Width = 260
            Height = 21
            Enabled = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdEmpresaChange
          end
          object EdUF_Servico: TdmkEdit
            Left = 332
            Top = 8
            Width = 64
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdUF_ServicoKeyDown
          end
          object CBUF: TComboBox
            Left = 180
            Top = 8
            Width = 69
            Height = 21
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Items.Strings = (
              'AC'
              'AL'
              'AM'
              'AP'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MG'
              'MS'
              'MT'
              'PA'
              'PB'
              'PE'
              'PI'
              'PR'
              'RJ'
              'RN'
              'RO'
              'RR'
              'RS'
              'SC'
              'SE'
              'SP'
              'TO')
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 113
          Height = 64
          Align = alLeft
          TabOrder = 1
          object RGAmbiente: TRadioGroup
            Left = 1
            Top = 1
            Width = 111
            Height = 62
            Align = alClient
            Caption = ' Ambiente: '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Produ'#231#227'o'
              'Homologa'#231#227'o')
            ParentFont = False
            TabOrder = 0
          end
        end
        object Panel10: TPanel
          Left = 593
          Top = 0
          Width = 180
          Height = 64
          Align = alClient
          TabOrder = 2
          object CkSoLer: TCheckBox
            Left = 8
            Top = 20
            Width = 169
            Height = 17
            Caption = 'Somente ler arquivo j'#225' gravado.'
            Enabled = False
            TabOrder = 0
          end
        end
        object PnLote: TPanel
          Left = 517
          Top = 0
          Width = 76
          Height = 64
          Align = alLeft
          TabOrder = 3
          object Label4: TLabel
            Left = 4
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
          end
          object EdLote: TdmkEdit
            Left = 4
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object Panel15: TPanel
        Left = 1
        Top = 1
        Width = 773
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object RGAcao: TRadioGroup
          Left = 0
          Top = 0
          Width = 657
          Height = 64
          Align = alClient
          Caption = ' A'#231#227'o a realizar: '
          Columns = 3
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Status do servi'#231'o'
            'Enviar lote de NF-e ao fisco'
            'Consultar lote enviado'
            'Pedir cancelamento de NF-e'
            'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e'
            'Consultar NF-e')
          TabOrder = 0
          OnClick = RGAcaoClick
        end
        object PnAbrirXML: TPanel
          Left = 657
          Top = 0
          Width = 116
          Height = 64
          Align = alRight
          Enabled = False
          TabOrder = 1
          Visible = False
          object BtAbrir: TButton
            Left = 8
            Top = 4
            Width = 100
            Height = 25
            Caption = 'Abrir arquivo XML'
            Enabled = False
            TabOrder = 0
            OnClick = BtAbrirClick
          end
          object Button1: TButton
            Left = 8
            Top = 32
            Width = 100
            Height = 25
            Caption = 'Aviso'
            TabOrder = 1
            OnClick = Button1Click
          end
        end
      end
      object PageControl1: TPageControl
        Left = 1
        Top = 295
        Width = 773
        Height = 102
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 6
        object TabSheet1: TTabSheet
          Caption = ' XML de envio '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object RETxtEnvio: TMemo
            Left = 0
            Top = 0
            Width = 765
            Height = 84
            Align = alClient
            TabOrder = 0
            WordWrap = False
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' XML Retornado (Texto) '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 767
          ExplicitHeight = 0
          object RETxtRetorno: TMemo
            Left = 0
            Top = 0
            Width = 765
            Height = 84
            Align = alClient
            TabOrder = 0
            WantReturns = False
            OnChange = RETxtRetornoChange
            ExplicitLeft = 52
            ExplicitTop = -5
            ExplicitWidth = 185
            ExplicitHeight = 89
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' XML Retornado (Formatado) '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object WBResposta: TWebBrowser
            Left = 0
            Top = 0
            Width = 765
            Height = 74
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 371
            ExplicitHeight = 258
            ControlData = {
              4C000000114F0000A60700000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Informa'#231#245'es do XML de retorno '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 30
          object MeInfo: TMemo
            Left = 0
            Top = 0
            Width = 765
            Height = 74
            Align = alClient
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
            ExplicitHeight = 30
          end
        end
      end
    end
  end
  object QrNFeCabA1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND LoteEnv=:P1'
      '')
    Left = 108
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrNFeJust: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 44
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrNFeJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsNFeJust: TDataSource
    DataSet = QrNFeJust
    Left = 72
    Top = 8
  end
  object QrNFeCabA2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND IDCtrl=:P1'
      '')
    Left = 140
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA2FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl, infProt_ID, infProt_nProt'
      'FROM nfecaba '
      'WHERE Empresa=:P0'
      'AND id=:P1')
    Left = 524
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAinfProt_ID: TWideStringField
      FieldName = 'infProt_ID'
      Size = 30
    end
    object QrCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
  end
end
