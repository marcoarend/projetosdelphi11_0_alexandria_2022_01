object FmNFeLEnC_0110: TFmNFeLEnC_0110
  Left = 368
  Top = 194
  Caption = 'NFe-LOTES-001 :: Lotes de NF-e'
  ClientHeight = 555
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 507
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 458
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 124
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 720
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 507
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 458
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtNFe: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtNFeClick
          NumGlyphs = 2
        end
        object BtLote: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtXML: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&XML'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
          NumGlyphs = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 192
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 793
        Height = 192
        Align = alLeft
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 152
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label3: TLabel
          Left = 68
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdNome
        end
        object Label5: TLabel
          Left = 8
          Top = 44
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdNome: TDBEdit
          Left = 68
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsNFeLEnC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 60
          Width = 56
          Height = 21
          DataField = 'Filial'
          DataSource = DsNFeLEnC
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 68
          Top = 60
          Width = 717
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsNFeLEnC
          TabOrder = 2
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 84
          Width = 493
          Height = 101
          Caption = ' Resposta do envio do XML: '
          TabOrder = 3
          object Label11: TLabel
            Left = 8
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Vers'#227'o do leiaute:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 108
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Tipo de ambiente:'
            FocusControl = DBEdit7
          end
          object Label13: TLabel
            Left = 276
            Top = 20
            Width = 171
            Height = 13
            Caption = 'Vers'#227'o do aplic. que recebeu o lote:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 8
            Top = 60
            Width = 48
            Height = 13
            Caption = 'Resposta:'
            FocusControl = DBEdit9
          end
          object Label15: TLabel
            Left = 456
            Top = 20
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit12
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 36
            Width = 97
            Height = 21
            DataField = 'versao'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 108
            Top = 36
            Width = 21
            Height = 21
            DataField = 'tpAmb'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 276
            Top = 36
            Width = 177
            Height = 21
            DataField = 'verAplic'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 8
            Top = 76
            Width = 33
            Height = 21
            DataField = 'cStat'
            DataSource = DsNFeLEnC
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 44
            Top = 76
            Width = 441
            Height = 21
            DataField = 'xMotivo'
            DataSource = DsNFeLEnC
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 128
            Top = 36
            Width = 145
            Height = 21
            DataField = 'NO_Ambiente'
            DataSource = DsNFeLEnC
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 456
            Top = 36
            Width = 29
            Height = 21
            DataField = 'cUF'
            DataSource = DsNFeLEnC
            TabOrder = 6
          end
        end
        object GroupBox2: TGroupBox
          Left = 504
          Top = 84
          Width = 281
          Height = 101
          Caption = ' Recibo de aceite do lote: '
          TabOrder = 4
          object Label16: TLabel
            Left = 8
            Top = 16
            Width = 87
            Height = 13
            Caption = 'N'#250'mero do recibo:'
            FocusControl = DBEdit13
          end
          object Label10: TLabel
            Left = 8
            Top = 56
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
            FocusControl = DBEdit5
          end
          object Label17: TLabel
            Left = 184
            Top = 56
            Width = 84
            Height = 13
            Caption = 't m'#233'd. resp. (seg):'
            FocusControl = DBEdit14
          end
          object DBEdit13: TDBEdit
            Left = 8
            Top = 32
            Width = 265
            Height = 21
            DataField = 'nRec'
            DataSource = DsNFeLEnC
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 72
            Width = 173
            Height = 21
            DataField = 'DataHora_TXT'
            DataSource = DsNFeLEnC
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 184
            Top = 72
            Width = 89
            Height = 21
            DataField = 'tMed'
            DataSource = DsNFeLEnC
            TabOrder = 2
          end
        end
        object DBEdit4: TDBEdit
          Left = 152
          Top = 20
          Width = 633
          Height = 21
          DataField = 'Nome'
          DataSource = DsNFeLEnC
          TabOrder = 5
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNFeLEnC
          TabOrder = 6
        end
      end
      object DBGrid2: TDBGrid
        Left = 793
        Top = 0
        Width = 213
        Height = 192
        Align = alClient
        DataSource = DsNFeLEnM
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 193
      Width = 1006
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 998
          Height = 165
          Align = alClient
          DataSource = DsNFeCabA
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'Nota fiscal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSTot_vNF'
              Title.Caption = 'Valor total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'id'
              Title.Caption = 'Chave de acesso da NF-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_cStat'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_xMotivo'
              Width = 660
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 281
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 998
          Height = 165
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 371
          ExplicitHeight = 258
          ControlData = {
            4C000000114F0000AF0800000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Lotes de NF-e'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsNFeLEnC: TDataSource
    DataSet = QrNFeLEnC
    Left = 40
    Top = 12
  end
  object QrNFeLEnC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeLEnCBeforeOpen
    AfterOpen = QrNFeLEnCAfterOpen
    AfterScroll = QrNFeLEnCAfterScroll
    OnCalcFields = QrNFeLEnCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfelenc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 12
    object QrNFeLEnCDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrNFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfelenc.Codigo'
    end
    object QrNFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'nfelenc.CodUsu'
    end
    object QrNFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfelenc.Nome'
      Size = 30
    end
    object QrNFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfelenc.Empresa'
    end
    object QrNFeLEnCversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfelenc.versao'
      DisplayFormat = '0.00'
    end
    object QrNFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfelenc.tpAmb'
    end
    object QrNFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfelenc.verAplic'
    end
    object QrNFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
      Origin = 'nfelenc.cStat'
    end
    object QrNFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfelenc.xMotivo'
      Size = 255
    end
    object QrNFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
      Origin = 'nfelenc.cUF'
    end
    object QrNFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Origin = 'nfelenc.nRec'
      Size = 15
    end
    object QrNFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
      Origin = 'nfelenc.dhRecbto'
    end
    object QrNFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
      Origin = 'nfelenc.tMed'
    end
    object QrNFeLEnCFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeLEnCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrNFeLEnCNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtLote
    CanUpd01 = BtNFe
    Left = 68
    Top = 12
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 12
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 568
    Top = 476
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object Verificalotenofisco1: TMenuItem
      Caption = 'Verifica lote no fisco'
      Enabled = False
      OnClick = Verificalotenofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      object Enviodelote1: TMenuItem
        Caption = '&Envio de lote'
        OnClick = Enviodelote1Click
      end
      object Consultadelote1: TMenuItem
        Caption = '&Consulta de lote'
        OnClick = Consultadelote1Click
      end
    end
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 664
    Top = 476
    object IncluiNFeaoloteatual1: TMenuItem
      Caption = '&Inclui NFe ao lote atual'
      OnClick = IncluiNFeaoloteatual1Click
    end
    object RemoveNFesselecionadas1: TMenuItem
      Caption = '&Remove NF-e(s) selecionada(s)'
      OnClick = RemoveNFesselecionadas1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
  object DsNFeLEnI: TDataSource
    DataSet = QrNFeLEnI
    Left = 512
    Top = 8
  end
  object QrNFeLEnI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 8
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, ide_nNF, ide_dEmi, '
      'id, ide_cNF, ide_serie, Status, ICMSTot_vNF,'
      'infProt_cStat, infProt_xMotivo'
      'FROM nfecaba '
      'WHERE LoteEnv=:P0'
      '')
    Left = 428
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      DisplayFormat = '000000000'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeCabAid: TWideStringField
      FieldName = 'id'
      Size = 44
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
      DisplayFormat = '000'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 456
    Top = 8
  end
  object QrNFeLEnM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfelenm'
      'WHERE Codigo=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 544
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeLEnMcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFeLEnMxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrNFeLEnMnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrNFeLEnMdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeLEnMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeLEnMversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeLEnMtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeLEnMverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrNFeLEnMcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrNFeLEnMtMed: TIntegerField
      FieldName = 'tMed'
    end
  end
  object DsNFeLEnM: TDataSource
    DataSet = QrNFeLEnM
    Left = 572
    Top = 8
  end
  object PMXML: TPopupMenu
    Left = 744
    Top = 472
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote'
        OnClick = Lote1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaNFEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML da NFE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assina'
        OnClick = Assina1Click
      end
    end
  end
end
