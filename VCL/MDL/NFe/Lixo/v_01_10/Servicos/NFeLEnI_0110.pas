unit NFeLEnI_0110;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids;

type
  TFmNFeLEnI_0110 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAid: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    DBGrid1: TDBGrid;
    DsNFeCabA: TDataSource;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLEnI_0110: TFmNFeLEnI_0110;

implementation

uses UnMyObjects, NFeLEnC_0110, Module, UMySQLModule, dmkGeral, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLEnI_0110.BtOKClick(Sender: TObject);
  procedure AdicionaNFeAoLote();
  var
    LoteEnv, FatID, FatNum, Empresa, Status: Integer;
    infProt_cStat, infProt_xMotivo: String;
  begin
    LoteEnv := FmNFeLEnC_0110.QrNFeLEnCCodigo.Value;
    FatID   := QrNFeCabAFatID.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    Status  := DmNFe_0000.stepNFeAdedLote();
    infProt_cStat   := ''; // Limpar variaveis (ver historico)
    infProt_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'], ['FatID', 'FatNum', 'Empresa'], [
    LoteEnv, Status, infProt_cStat, infProt_xMotivo], [FatID, FatNum, Empresa], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmNFeLEnC_0110.QrNFeCabA.RecordCount > 50 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 50 NFs permitidas por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
    ' NF-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        AdicionaNFeAoLote();
      end;
    end;
  end else AdicionaNFeAoLote();
  FmNFeLEnC_0110.ReopenNFeLEnI();
  Close;
end;

procedure TFmNFeLEnI_0110.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLEnI_0110.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLEnI_0110.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmNFeLEnC_0110.LocCod(FmNFeLEnC_0110.QrNFeLEnCCodigo.Value, FmNFeLEnC_0110.QrNFeLEnCCodigo.Value);
end;

procedure TFmNFeLEnI_0110.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
