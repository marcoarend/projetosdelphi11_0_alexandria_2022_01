unit NFeLEnC_0110;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc;

type
  TFmNFeLEnC_0110 = class(TForm)
    PainelDados: TPanel;
    DsNFeLEnC: TDataSource;
    QrNFeLEnC: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtNFe: TBitBtn;
    BtLote: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    QrNFeLEnCDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMNFe: TPopupMenu;
    IncluiNFeaoloteatual1: TMenuItem;
    DsNFeLEnI: TDataSource;
    QrNFeLEnI: TmySQLQuery;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAid: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    DsNFeCabA: TDataSource;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrNFeLEnCCodigo: TIntegerField;
    QrNFeLEnCCodUsu: TIntegerField;
    QrNFeLEnCNome: TWideStringField;
    QrNFeLEnCEmpresa: TIntegerField;
    QrNFeLEnCversao: TFloatField;
    QrNFeLEnCtpAmb: TSmallintField;
    QrNFeLEnCverAplic: TWideStringField;
    QrNFeLEnCcStat: TIntegerField;
    QrNFeLEnCxMotivo: TWideStringField;
    QrNFeLEnCcUF: TSmallintField;
    QrNFeLEnCnRec: TWideStringField;
    QrNFeLEnCdhRecbto: TDateTimeField;
    QrNFeLEnCtMed: TIntegerField;
    QrNFeLEnCFilial: TIntegerField;
    QrNFeLEnCNO_Empresa: TWideStringField;
    QrNFeLEnCNO_Ambiente: TWideStringField;
    Verificalotenofisco1: TMenuItem;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    Enviodelote1: TMenuItem;
    Consultadelote1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrNFeLEnM: TmySQLQuery;
    DsNFeLEnM: TDataSource;
    DBGrid2: TDBGrid;
    QrNFeLEnMControle: TIntegerField;
    QrNFeLEnMversao: TFloatField;
    QrNFeLEnMtpAmb: TSmallintField;
    QrNFeLEnMverAplic: TWideStringField;
    QrNFeLEnMcStat: TIntegerField;
    QrNFeLEnMxMotivo: TWideStringField;
    QrNFeLEnMcUF: TSmallintField;
    QrNFeLEnMnRec: TWideStringField;
    QrNFeLEnMdhRecbto: TDateTimeField;
    QrNFeLEnMtMed: TIntegerField;
    RemoveNFesselecionadas1: TMenuItem;
    N3: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    WB_XML: TWebBrowser;
    BtXML: TBitBtn;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNFEnonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeLEnCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeLEnCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure IncluiNFeaoloteatual1Click(Sender: TObject);
    procedure BtNFeClick(Sender: TObject);
    procedure QrNFeLEnCAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Enviodelote1Click(Sender: TObject);
    procedure Consultadelote1Click(Sender: TObject);
    procedure RemoveNFesselecionadas1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeLEnI();
    procedure ReopenNFeLEnM();
  end;

var
  FmNFeLEnC_0110: TFmNFeLEnC_0110;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, NFeLEnI_0110, ModuleNFe_0000, NFeSteps_0110;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeLEnC_0110.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeLEnC_0110.Lote1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrNFeLEnCCodigo.Value);
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmNFeLEnC_0110.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeLEncCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeLEnC_0110.Verificalotenofisco1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeSteps_0110, FmNFeSteps_0110, afmoNegarComAviso) then
  begin
    FmNFeSteps_0110.PnLoteEnv.Visible := True;
    FmNFeSteps_0110.Show;
    //
    FmNFeSteps_0110.RGAcao.ItemIndex := 2; // Verifica lote no fisco
    FmNFeSteps_0110.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
      QrNFeLEnCnRec.Value);
    FmNFeSteps_0110.FFormChamou      := 'FmNFeLEnc_0110';
    //
    //FmNFeSteps_0110.Destroy;
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeLEnC_0110.DefParams;
begin
  VAR_GOTOTABELA := 'NFeLEnc';
  VAR_GOTOMYSQLTABLE := QrNFeLEnc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfelenc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmNFeLEnC_0110.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFeLEnC_0110.Envialoteaofisco1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeSteps_0110, FmNFeSteps_0110, afmoNegarComAviso) then
  begin
    FmNFeSteps_0110.PnLoteEnv.Visible := True;
    FmNFeSteps_0110.Show;
    //
    FmNFeSteps_0110.RGAcao.ItemIndex := 1; // Envia lote ao fisco
    FmNFeSteps_0110.PreparaEnvioDeLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value);
    FmNFeSteps_0110.FFormChamou      := 'FmNFeLEnc_0110';
    //
    //
    //FmNFeSteps_0110.Destroy;
    //
  end;
end;

procedure TFmNFeLEnC_0110.Enviodelote1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeSteps_0110, FmNFeSteps_0110, afmoNegarComAviso) then
  begin
    FmNFeSteps_0110.PnLoteEnv.Visible := True;
    FmNFeSteps_0110.Show;
    //
    FmNFeSteps_0110.RGAcao.ItemIndex := 1; // Envia lote ao fisco
    FmNFeSteps_0110.CkSoLer.Checked  := True;
    FmNFeSteps_0110.PreparaEnvioDeLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value);
    FmNFeSteps_0110.FFormChamou      := 'FmNFeLEnc_0110';
    //
    //
    //FmNFeSteps_0110.Destroy;
    //
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

procedure TFmNFeLEnC_0110.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmNFeLEnC_0110.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB: Boolean;
begin
  HabilitaA := (QrNFeLEnc.State = dsBrowse) and (QrNFeLEnc.RecordCount > 0);
  Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  Enviodelote1.Enabled     := HabilitaA;
  Consultadelote1.Enabled  := HabilitaA;//? and (QrNFeLEnc.);
  //
  HabilitaB := HabilitaA and (not (QrNFeLEnCcStat.Value in ([103,104,105])));
  Envialoteaofisco1.Enabled := HabilitaB;
  HabilitaB := HabilitaA and (QrNFeLEnCcStat.Value > 102);
  Verificalotenofisco1.Enabled := HabilitaB;

  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmNFeLEnC_0110.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrNFeLEnc.RecordCount > 0) and (QrNFeLEnCcStat.Value <> 103);
  Alteraloteatual1.Enabled := Habilita;
end;

procedure TFmNFeLEnC_0110.Consultadelote1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeSteps_0110, FmNFeSteps_0110, afmoNegarComAviso) then
  begin
    FmNFeSteps_0110.PnLoteEnv.Visible := True;
    FmNFeSteps_0110.Show;
    //
    FmNFeSteps_0110.RGAcao.ItemIndex := 2; // verifica lote no fisco
    FmNFeSteps_0110.CkSoLer.Checked  := True;
    FmNFeSteps_0110.PreparaConsultaLote(QrNFeLEnCCodigo.Value,
      QrNFeLEnCEmpresa.Value, QrNFeLEnCnRec.Value);
    FmNFeSteps_0110.FFormChamou      := 'FmNFeLEnc_0110';
    //
    //
    //FmNFeSteps_0110.Destroy;
    //
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

procedure TFmNFeLEnC_0110.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNFeLEnC.AlteraRegistro;
var
  NFeLEnc : Integer;
begin
  NFeLEnc := QrNFeLEncCodigo.Value;
  if QrNFeLEncCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NFeLEnc, Dmod.MyDB, 'NFeLEnc', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFeLEnc, Dmod.MyDB, 'NFeLEnc', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFeLEnC.IncluiRegistro;
var
  Cursor : TCursor;
  NFeLEnc : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NFeLEnc := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'NFeLEnc', 'NFeLEnc', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NFeLEnc))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, NFeLEnc);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNFeLEnC_0110.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeLEnC_0110.RemoveNFesselecionadas1Click(Sender: TObject);
var
  Status: Integer;
begin
  QrNFeLEnM.First;
  if (QrNFeCabAStatus.Value < 100)
  or ((QrNFeLEnMcStat.Value in ([103,104])) and (QrNFeCabAStatus.Value > 200)) then
  begin
    if Application.MessageBox('Deseja realmente tirar a NF-e deste lote?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = mrYes then
    begin
      if (QrNFeCabAStatus.Value = 50)
      or ((QrNFeCabAStatus.Value = 103) and (QrNFeLEnMcStat.Value > 200)) then
        Status := 30
      else
        Status := QrNFeCabAStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET LoteEnv = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 AND Empresa=:P3');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrNFeCabAFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrNFeCabAFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrNFeCabAEmpresa.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenNFeLEnI();
    end;
  end else
    Geral.MensagemBox('A��o n�o permitida!', 'Aviso', MB_OK+MB_ICONWARNING);

end;

procedure TFmNFeLEnC_0110.ReopenNFeLEnI;
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[0].AsInteger := QrNFeLEnCCodigo.Value;
  QrNFeCabA.Open;
end;

procedure TFmNFeLEnC_0110.ReopenNFeLEnM;
begin
  QrNFeLEnM.Close;
  QrNFeLEnM.Params[0].AsInteger := QrNFeLEnCCodigo.Value;
  QrNFeLEnM.Open;
end;

procedure TFmNFeLEnC_0110.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeLEnC_0110.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeLEnC_0110.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeLEnC_0110.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeLEnC_0110.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeLEnC_0110.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeLEnc, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'nfelenc');
end;

procedure TFmNFeLEnC_0110.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeLEncCodigo.Value;
  Close;
end;

procedure TFmNFeLEnC_0110.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmNFeLEnC_0110.BtNFeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeLEnC_0110.Assina1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, True);
end;

procedure TFmNFeLEnC_0110.Assinada1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, True, WB_XML);
end;

procedure TFmNFeLEnC_0110.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  //Nome: String;
begin
  {
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('NFeLEnc', 'Codigo', LaTipo.SQLType,
    QrNFeLEncCodigo.Value);
  }
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  EdCodigo.ValueVariant := Codigo;
  EdCodUsu.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmNFeLEnC_0110, PainelEdit,
    'NFeLEnc', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFeLEnC_0110.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFeLEnc', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmNFeLEnC_0110.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeLEnC_0110.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmNFeLEnC_0110.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeLEncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0110.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0110.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeLEncCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFeLEnC_0110.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmNFeLEnC_0110.QrNFeLEnCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeLEnC_0110.QrNFeLEnCAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeLEnI();
  ReopenNFeLEnM();
end;

procedure TFmNFeLEnC_0110.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLEnC_0110.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeLEncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFeLEnc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeLEnC_0110.SelecionaroXML1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\NFE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
end;

procedure TFmNFeLEnC_0110.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmNFeLEnC_0110.Gerada1Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoNavegadorPadrao(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, False);
end;

procedure TFmNFeLEnC_0110.Gerada2Click(Sender: TObject);
begin
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFeLEnCEmpresa.Value, NFE_EXT_NFE_XML,
    QrNFeCabAid.Value, False, WB_XML);
end;

procedure TFmNFeLEnC_0110.IncluiNFeaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLEnI_0110, FmNFeLEnI_0110, afmoNegarComAviso) then
  begin
    FmNFeLEnI_0110.QrNFeCabA.Close;
    FmNFeLEnI_0110.QrNFeCabA.Params[00].AsInteger := QrNFeLEncEmpresa.Value;
    FmNFeLEnI_0110.QrNFeCabA.Params[01].AsInteger := DmNFe_0000.stepNFeAssinada();
    FmNFeLEnI_0110.QrNFeCabA.Open;
    //
    FmNFeLEnI_0110.ShowModal;
    FmNFeLEnI_0110.Destroy;
    //
    //LocCod(QrNFeLEnCCodigo.Value, QrNFeLEnCCodigo.Value);
  end;
end;

procedure TFmNFeLEnC_0110.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeLEnc, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'nfelenc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFeLEnC_0110.QrNFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  QrNFeLEncCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeLEnC_0110.QrNFeLEnCCalcFields(DataSet: TDataSet);
begin
  QrNFeLEncDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeLEnCdhRecbto.Value, 0);
  case QrNFeLEnCtpAmb.Value of
    1: QrNFeLEnCNO_Ambiente.Value := 'Produ��o';
    2: QrNFeLEnCNO_Ambiente.Value := 'Homologa��o';
    else QrNFeLEnCNO_Ambiente.Value := '';
  end;
end;

end.

