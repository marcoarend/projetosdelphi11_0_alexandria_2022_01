unit NFeOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     ComCtrls,
  DBCtrls,    Db, mySQLDbTables, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, dmkGeral, dmkRadioGroup, dmkLabel, dmkValUsu;

type
  TFmNFeOpcoes = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    EdVersao: TdmkEdit;
    Label1: TLabel;
    Edide_mod: TdmkEdit;
    Label2: TLabel;
    RGide_tpImp: TdmkRadioGroup;
    RGide_tpAmb: TdmkRadioGroup;
    LaTipo: TdmkLabel;
    RGAppCode: TdmkRadioGroup;
    Label3: TLabel;
    EdEntiTipCto: TdmkEditCB;
    CBEntiTipCto: TdmkDBLookupComboBox;
    QrEntiTipCto: TmySQLQuery;
    DsEntiTipCto: TDataSource;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    VUEntiTipCto: TdmkValUsu;
    EdMyEmailNFe: TdmkEdit;
    Label4: TLabel;
    RGAssDigMode: TdmkRadioGroup;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeOpcoes: TFmNFeOpcoes;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, EntiTipCto, MyDBCheck;

{$R *.DFM}

procedure TFmNFeOpcoes.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeOpcoes.FormCreate(Sender: TObject);
begin
  QrEntiTipCto.Close;
  QrEntiTipCto.Open;
end;

procedure TFmNFeOpcoes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmNFeOpcoes.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
    if VAR_CADASTRO > 0 then
    begin
      if VAR_CADASTRO <> 0 then
        UMyMod.SetaCodUsuDeCodigo(EdEntiTipCto, CBEntiTipCto, QrEntiTipCto, VAR_CADASTRO);
    end;
  end;
end;

procedure TFmNFeOpcoes.BtOKClick(Sender: TObject);
var
  EntiTipCto: Integer;
begin
  //if not
  UMyMod.ObtemCodigoDeCodUsu(EdEntiTipCto, EntiTipCto,
  'Tipo de email para envio de NFe n�o definido!');
  //
  if Geral.MensagemBox('TODAS filiais ficar�o com estas mesmas op��es! ' +
  'Deseja continuar assim mesmo?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False, [
      'versao', 'ide_mod', 'ide_tpImp',
      'ide_tpAmb', 'EntiTipCto', 'MyEmailNFe',
      'AppCode', 'AssDigMode'], [], [
      Edversao.ValueVariant, Edide_mod.ValueVariant, RGide_tpImp.ItemIndex,
      RGide_tpAmb.ItemIndex, EntiTipCto, EdMyEmailNFe.Text,
      RGAppCode.ItemIndex, RGAssDigMode.ItemIndex], [], True);
    //
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfectrl', False, [
    'versao', 'ide_mod', 'ide_tpImp',
    'ide_tpAmb', 'EntiTipCto', 'MyEmailNFe',
    'AppCode', 'AssDigMode'], [
    'Codigo'
  ], [
    Edversao.ValueVariant, Edide_mod.ValueVariant, RGide_tpImp.ItemIndex,
    RGide_tpAmb.ItemIndex, EntiTipCto, EdMyEmailNFe.Text,
    RGAppCode.ItemIndex, RGAssDigMode.ItemIndex], [
    1], True) then
  Close;
end;

end.
