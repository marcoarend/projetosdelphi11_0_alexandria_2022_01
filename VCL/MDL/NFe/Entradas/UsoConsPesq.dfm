object FmUsoConsPesq: TFmUsoConsPesq
  Left = 348
  Top = 185
  Caption = 'USO-ENTRA-003 :: Pesquisa de Entrada de Uso e Consumo'
  ClientHeight = 446
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 398
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object Panel1: TPanel
      Left = 900
      Top = 1
      Width = 107
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 5
        Top = 1
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 1008
    Height = 93
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 40
      Height = 13
      Caption = 'Produto:'
    end
    object Label4: TLabel
      Left = 596
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label5: TLabel
      Left = 692
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label6: TLabel
      Left = 560
      Top = 48
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label7: TLabel
      Left = 712
      Top = 48
      Width = 56
      Height = 13
      Caption = 'Nota Fiscal:'
    end
    object Label2: TLabel
      Left = 636
      Top = 48
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 525
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFornecedores
      TabOrder = 1
      OnKeyDown = CBFornecedorKeyDown
      dmkEditCB = EdFornecedor
      UpdType = utYes
    end
    object CBGraGru1: TdmkDBLookupComboBox
      Left = 68
      Top = 64
      Width = 485
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraGru1
      TabOrder = 3
      OnKeyDown = CBGraGru1KeyDown
      dmkEditCB = EdGraGru1
      UpdType = utYes
    end
    object TPIni: TdmkEditDateTimePicker
      Left = 596
      Top = 20
      Width = 93
      Height = 21
      Date = 37670.521748657400000000
      Time = 37670.521748657400000000
      TabOrder = 4
      OnClick = TPIniClick
      OnChange = TPIniChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPFim: TdmkEditDateTimePicker
      Left = 692
      Top = 20
      Width = 93
      Height = 21
      Date = 37670.521748657400000000
      Time = 37670.521748657400000000
      TabOrder = 5
      OnClick = TPFimClick
      OnChange = TPFimChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdCodigo: TdmkEdit
      Left = 560
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnExit = EdCodigoExit
    end
    object EdNF: TdmkEdit
      Left = 712
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnExit = EdNFExit
    end
    object EdFornecedor: TdmkEditCB
      Left = 12
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdFornecedorChange
      DBLookupComboBox = CBFornecedor
    end
    object EdGraGru1: TdmkEditCB
      Left = 12
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdGraGru1Change
      DBLookupComboBox = CBGraGru1
    end
    object EdIDCtrl: TdmkEdit
      Left = 636
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnExit = EdCodigoExit
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 133
    Width = 1008
    Height = 265
    Align = alClient
    TabOrder = 2
    object DBGrid1: TdmkDBGrid
      Left = 1
      Top = 1
      Width = 1006
      Height = 263
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_FRN'
          Title.Caption = 'Fornecedor'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel1'
          Title.Caption = 'Produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Descri'#231#227'o do produto'
          Width = 325
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nNF'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFiscal'
          Title.Caption = 'Data fiscal'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'C'#243'digo'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 76
          Visible = True
        end>
      Color = clWindow
      DataSource = DsNFs
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_FRN'
          Title.Caption = 'Fornecedor'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel1'
          Title.Caption = 'Produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Descri'#231#227'o do produto'
          Width = 325
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nNF'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFiscal'
          Title.Caption = 'Data fiscal'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'C'#243'digo'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 76
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 40
    Align = alTop
    Caption = 'Pesquisa de Entrada de Uso e Consumo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object QrNFs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'nfa.ide_nNF, nfa.emit_CNPJ, nfa.DataFiscal, '
      'gg1.Nivel1, gg1.Nome NO_PRD, nfa.FatNum, nfa.IDCtrl'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN nfecaba    nfa ON nfa.FatID=smia.Tipo'
      '  AND nfa.FatNum=smia.OriCodi'
      'LEFT JOIN entidades  frn ON frn.Codigo=nfa.CodInfoEmit'
      'WHERE smia.Tipo=151'
      ''
      '')
    Left = 8
    Top = 5
    object QrNFsNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrNFside_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFsemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFsDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrNFsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrNFsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrNFsFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
  end
  object DsNFs: TDataSource
    DataSet = QrNFs
    Left = 36
    Top = 5
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFornecedoresAfterScroll
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      'WHERE Fornece2="V" '
      'ORDER BY Nome')
    Left = 24
    Top = 193
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 52
    Top = 193
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Nivel1, Nome'
      'FROM GraGru1'
      'WHERE PrdGrupTip=-2'
      'ORDER BY Nome')
    Left = 24
    Top = 225
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 52
    Top = 225
  end
end
