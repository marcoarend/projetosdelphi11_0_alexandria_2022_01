unit UsoConsCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Grids, DBGrids,
  dmkDBLookupComboBox, dmkEditCB, ComCtrls, dmkEditDateTimePicker, dmkRadioGroup,
  dmkEditF7, Menus, UnDmkProcFunc, UnDmkEnums, NFe_PF;

type
  TFmUsoConsCab = class(TForm)
    PainelDados: TPanel;
    DsNFeCabA: TDataSource;
    QrNFeCabA: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItem: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdIDCtrl: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    dmkDBEdit1: TdmkDBEdit;
    QrNFeCabANO_EMP: TWideStringField;
    QrNFeCabAFilial: TIntegerField;
    Label4: TLabel;
    DBEdFatID: TDBEdit;
    QrNFeCabANO_FATID: TWideStringField;
    DBEdNO_FatID: TDBEdit;
    QrNFeCabACodInfoEmit: TIntegerField;
    QrNFeCabACodInfoDest: TIntegerField;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    QrNFeCabANO_FRN: TWideStringField;
    DBEdit6: TDBEdit;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_mod: TSmallintField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_dSaiEnt: TDateField;
    QrNFeCabAide_hSaiEnt: TTimeField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabAide_tpAmb: TSmallintField;
    QrNFeCabAide_procEmi: TSmallintField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    DBEdit8: TDBEdit;
    Label12: TLabel;
    DBEdit9: TDBEdit;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    Label14: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit13: TDBEdit;
    Label17: TLabel;
    DBEdit14: TDBEdit;
    Label18: TLabel;
    DBEdit15: TDBEdit;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    Label20: TLabel;
    DBEdit17: TDBEdit;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vBCST: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vII: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabACodInfoTrsp: TIntegerField;
    QrNFeCabAModFrete: TSmallintField;
    Label15: TLabel;
    DBEdit18: TDBEdit;
    Label21: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    Label25: TLabel;
    DBEdit23: TDBEdit;
    Label26: TLabel;
    DBEdit24: TDBEdit;
    Label27: TLabel;
    DBEdit25: TDBEdit;
    Label28: TLabel;
    DBEdit26: TDBEdit;
    Label29: TLabel;
    DBEdit27: TDBEdit;
    Label30: TLabel;
    DBEdit28: TDBEdit;
    Label31: TLabel;
    DBEdit29: TDBEdit;
    Label32: TLabel;
    DBEdit30: TDBEdit;
    Label33: TLabel;
    DBEdit31: TDBEdit;
    Label34: TLabel;
    DBEdit32: TDBEdit;
    Label35: TLabel;
    DBEdit33: TDBEdit;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabADataFiscal: TDateField;
    Label36: TLabel;
    DBEdit34: TDBEdit;
    Label38: TLabel;
    DBEdit36: TDBEdit;
    Label39: TLabel;
    DBEdit37: TDBEdit;
    QrNFeCabAStatus: TIntegerField;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    QrStqMovItsGrupoBal: TIntegerField;
    QrStqMovItsBaixa: TSmallintField;
    DBGItens: TDBGrid;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    GroupBox1: TGroupBox;
    EdIDCtrl: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdFatNum: TdmkEdit;
    Label40: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Edide_mod: TdmkEditCB;
    CBide_mod: TdmkDBLookupComboBox;
    QrNFMoDocFis: TmySQLQuery;
    DsNFMoDocFis: TDataSource;
    QrNFMoDocFisCodigo: TIntegerField;
    QrNFMoDocFisNome: TWideStringField;
    EdNFG_Serie: TdmkEdit;
    Label41: TLabel;
    Edide_nNF: TdmkEdit;
    Label42: TLabel;
    Label43: TLabel;
    TPide_dEmi: TdmkEditDateTimePicker;
    Label44: TLabel;
    TPide_dSaiEnt: TdmkEditDateTimePicker;
    Label45: TLabel;
    TPDataFiscal: TdmkEditDateTimePicker;
    Edide_hSaiEnt: TdmkEdit;
    Label46: TLabel;
    RGide_tpNF: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    Label37: TLabel;
    EdCodInfoEmit: TdmkEditCB;
    CBCodInfoEmit: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOME: TWideStringField;
    DsFornece: TDataSource;
    QrTransporta: TmySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOME: TWideStringField;
    DsTransporta: TDataSource;
    SpeedButton5: TSpeedButton;
    GroupBox4: TGroupBox;
    Label47: TLabel;
    EdICMSTot_vBC: TdmkEdit;
    EdICMSTot_vICMS: TdmkEdit;
    Label48: TLabel;
    Label49: TLabel;
    EdICMSTot_vBCST: TdmkEdit;
    Label50: TLabel;
    EdICMSTot_vST: TdmkEdit;
    Label51: TLabel;
    EdICMSTot_vProd: TdmkEdit;
    Label52: TLabel;
    EdICMSTot_vFrete: TdmkEdit;
    Label53: TLabel;
    EdICMSTot_vSeg: TdmkEdit;
    Label54: TLabel;
    EdICMSTot_vDesc: TdmkEdit;
    Label55: TLabel;
    EdICMSTot_vII: TdmkEdit;
    Label56: TLabel;
    EdICMSTot_vIPI: TdmkEdit;
    Label57: TLabel;
    EdICMSTot_vPIS: TdmkEdit;
    Label58: TLabel;
    EdICMSTot_vCOFINS: TdmkEdit;
    Label59: TLabel;
    EdICMSTot_vOutro: TdmkEdit;
    Label60: TLabel;
    EdICMSTot_vNF: TdmkEdit;
    GroupBox5: TGroupBox;
    EdModFrete: TdmkEdit;
    Label163: TLabel;
    EdModFrete_TXT: TdmkEdit;
    Label61: TLabel;
    EdCodInfoTrsp: TdmkEditCB;
    CBCodInfoTrsp: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    EdRetTransp_vServ: TdmkEdit;
    Label62: TLabel;
    Label63: TLabel;
    EdRetTransp_vBCRet: TdmkEdit;
    Label64: TLabel;
    EdRetTransp_PICMSRet: TdmkEdit;
    Label65: TLabel;
    EdRetTransp_vICMSRet: TdmkEdit;
    Label174: TLabel;
    EdRetTransp_CFOP: TdmkEdit;
    Label175: TLabel;
    EdRetTransp_CMunFG: TdmkEdit;
    EdRetTransp_CMunFG_TXT: TdmkEditF7;
    EdNFe_Id: TdmkEdit;
    Label66: TLabel;
    QrForneceCNPJ_CPF: TWideStringField;
    DBEdCNPJ_CPF_TXT_Fornece: TDBEdit;
    QrForneceCNPJ_CPF_TXT: TWideStringField;
    QrTransportaCNPJ_CPF_TXT: TWideStringField;
    QrTransportaCNPJ_CPF: TWideStringField;
    DBEdCNPJ_CPF_TXT_Transporta: TDBEdit;
    PMInclui: TPopupMenu;
    porXML1: TMenuItem;
    Manualmente1: TMenuItem;
    IncluinovaNF1: TMenuItem;
    AlteraNF1: TMenuItem;
    QrNFeCabANFG_Serie: TWideStringField;
    QrNFeCabARetTransp_vServ: TFloatField;
    QrNFeCabARetTransp_vBCRet: TFloatField;
    QrNFeCabARetTransp_PICMSRet: TFloatField;
    QrNFeCabARetTransp_vICMSRet: TFloatField;
    QrNFeCabARetTransp_CFOP: TWideStringField;
    QrNFeCabARetTransp_CMunFG: TWideStringField;
    PMItem: TPopupMenu;
    Incluinovoitem1: TMenuItem;
    AlteraItemAtual1: TMenuItem;
    QrNFeCabACNPJ_CPF_FRN: TWideStringField;
    QrNFeCabACNPJ_CPF_FRN_TXT: TWideStringField;
    ExcluiitemAtual1: TMenuItem;
    QrNFeItsI: TmySQLQuery;
    QrNFeItsN: TmySQLQuery;
    QrNFeItsO: TmySQLQuery;
    QrNFeItsInItem: TIntegerField;
    QrNFeItsIprod_cProd: TWideStringField;
    QrNFeItsIprod_NCM: TWideStringField;
    QrNFeItsIprod_EXTIPI: TWideStringField;
    QrNFeItsIprod_CFOP: TIntegerField;
    QrNFeItsIprod_uCom: TWideStringField;
    QrNFeItsIprod_qCom: TFloatField;
    QrNFeItsIprod_vUnCom: TFloatField;
    QrNFeItsIprod_vProd: TFloatField;
    QrNFeItsIprod_uTrib: TWideStringField;
    QrNFeItsIprod_qTrib: TFloatField;
    QrNFeItsIprod_vUnTrib: TFloatField;
    QrNFeItsIprod_vFrete: TFloatField;
    QrNFeItsIprod_vSeg: TFloatField;
    QrNFeItsIprod_vDesc: TFloatField;
    QrNFeItsIprod_vOutro: TFloatField;
    QrNFeItsIGraGruX: TIntegerField;
    QrNFeItsNICMS_Orig: TSmallintField;
    QrNFeItsNICMS_CST: TSmallintField;
    QrNFeItsNICMS_vBC: TFloatField;
    QrNFeItsNICMS_pICMS: TFloatField;
    QrNFeItsNICMS_vICMS: TFloatField;
    QrNFeItsIUnidMedCom: TIntegerField;
    QrNFeItsIUnidMedTrib: TIntegerField;
    QrNFeItsOIPI_pIPI: TFloatField;
    QrNFeItsOIPI_vIPI: TFloatField;
    QrNFeCabACriAForca: TSmallintField;
    QrNFeItsIMeuID: TIntegerField;
    PMNumero: TPopupMenu;
    Cdigo1: TMenuItem;
    ID1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeCabABeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdModFreteChange(Sender: TObject);
    procedure EdRetTransp_CMunFGChange(Sender: TObject);
    procedure QrForneceCalcFields(DataSet: TDataSet);
    procedure QrTransportaCalcFields(DataSet: TDataSet);
    procedure EdCodInfoEmitChange(Sender: TObject);
    procedure EdCodInfoTrspChange(Sender: TObject);
    procedure Manualmente1Click(Sender: TObject);
    procedure porXML1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure AlteraNF1Click(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure Incluinovoitem1Click(Sender: TObject);
    procedure ExcluiitemAtual1Click(Sender: TObject);
    procedure AlteraItemAtual1Click(Sender: TObject);
    procedure Cdigo1Click(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrStqMovItsBeforeClose(DataSet: TDataSet);
  private
    //

    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; IDCtrl: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenNFeIts_I_N_O();
  public
    { Public declarations }
    FIDs_Txt: String;
    FLocIDCtrl: Integer;
    procedure LocCod(Atual, IDCtrl: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure ExcluiItem(FatID, FatNum, Empresa, nItem: Integer; Pergunta:
              Boolean);
  end;

var
  FmUsoConsCab: TFmUsoConsCab;
const
  FFormatFloat = '000000000';

implementation

uses UnMyObjects, Module, ModuleGeral, NFeXMLGerencia, ModuleNFe_0000, MyDBCheck,
UsoConsIts, UsoConsPesq, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUsoConsCab.LocCod(Atual, IDCtrl: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, IDCtrl);
end;

procedure TFmUsoConsCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeCabAIDCtrl.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmUsoConsCab.DefParams;
begin
  VAR_GOTOTABELA := 'nfecaba';
  VAR_GOTOMYSQLTABLE := QrNFeCabA;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'IDCtrl';
  VAR_GOTONOME := '';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,');
  VAR_SQLx.Add('emp.Filial, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.CNPJ, frn.CPF) CNPJ_CPF_FRN,');
  VAR_SQLx.Add('nfa.*');
  VAR_SQLx.Add('FROM nfecaba nfa');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=nfa.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=nfa.CodInfoEmit');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE nfa.IDCtrl=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
  VAR_GOTOVAR1 := '(FatID IN (' + FIDs_Txt + ')' + #13#10 +
                  'AND Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmUsoConsCab.EdCodInfoEmitChange(Sender: TObject);
begin
  if EdCodInfoEmit.ValueVariant = 0 then
    DBEdCNPJ_CPF_TXT_Fornece.DataField := ''
  else
    DBEdCNPJ_CPF_TXT_Fornece.DataField := 'CNPJ_CPF_TXT';
end;

procedure TFmUsoConsCab.EdCodInfoTrspChange(Sender: TObject);
begin
  if EdCodInfoTrsp.ValueVariant = 0 then
    DBEdCNPJ_CPF_TXT_Transporta.DataField := ''
  else
    DBEdCNPJ_CPF_TXT_Transporta.DataField := 'CNPJ_CPF_TXT';
end;

procedure TFmUsoConsCab.EdModFreteChange(Sender: TObject);
begin
  EdModFrete_TXT.Text := UnNFE_PF.TextoDeCodigoNFe(nfeCTModFrete, EdModFrete.ValueVariant);
end;

procedure TFmUsoConsCab.EdRetTransp_CMunFGChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', EdRetTransp_CMunFG.ValueVariant, []) then
    EdRetTransp_CMunFG_TXT.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    EdRetTransp_CMunFG_TXT.Text := '';
end;

procedure TFmUsoConsCab.ExcluiItem(FatID, FatNum, Empresa, nItem: Integer;
  Pergunta: Boolean);
var
  Exclui: Boolean;
begin
  if Pergunta then
    Exclui := Geral.MensagemBox('Confirma a exclus�o do item ' + FormatFloat(
    '000', nItem) + '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  else
    Exclui := true;
  if Exclui then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND OriCtrl=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitso WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsn WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfeitsi WHERE FatID=:P0 AND FatNum=:P1');
    Dmod.QrUpd.SQL.Add('AND Empresa=:P2 AND nItem=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsInteger := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := Empresa;
    Dmod.QrUpd.Params[03].AsInteger := nItem;
    Dmod.QrUpd.ExecSQL;
  end;
end;

procedure TFmUsoConsCab.ExcluiitemAtual1Click(Sender: TObject);
begin
  ExcluiItem(QrStqMovItsTipo.Value, QrStqMovItsOriCodi.Value,
    QrStqMovItsEmpresa.Value, QrStqMovItsOriCtrl.Value, True);
end;

procedure TFmUsoConsCab.Manualmente1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeCabA, [PainelDados],
  [PainelEdita], EdEmpresa, LaTipo, 'nfecaba');
end;

procedure TFmUsoConsCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; IDCtrl: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdIDCtrl.Text := FormatFloat(FFormatFloat, IDCtrl);
        //...
      end else begin
        EdIDCtrl.Text := DBEdIDCtrl.Text;
        //...
      end;
      EdEmpresa.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmUsoConsCab.PMIncluiPopup(Sender: TObject);
begin
  AlteraNF1.Enabled :=
    (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
end;

procedure TFmUsoConsCab.porXML1Click(Sender: TObject);
begin
// Parei aqui!
end;

procedure TFmUsoConsCab.Cdigo1Click(Sender: TObject);
var
  FatNum: Variant;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Entrada de Uso e Consumo', 'Informe o c�digo do lan�amento: ',
  0, FatNum) then
  begin
    if Integer(FatNum) <> 0 then
    begin
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IDCtrl FROM nfecaba');
      Dmod.QrAux.SQL.Add('WHERE ' + VAR_GOTOVAR1);
      Dmod.QrAux.SQL.Add('AND FatNum=' + FormatFloat('0', FatNum));
      Dmod.QrAux.Open;
      //
      LocCod(0, Dmod.QrAux.FieldByName('IDCtrl').AsInteger);
    end;
  end;
end;

procedure TFmUsoConsCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmUsoConsCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmUsoConsCab.ReopenNFeIts_I_N_O();
begin
  QrNFeItsI.Close;
  QrNFeItsI.SQL.Clear;
  QrNFeItsI.SQL.Add('SELECT *');
  QrNFeItsI.SQL.Add('FROM nfeitsi');
  QrNFeItsI.SQL.Add('WHERE FatID=:P0');
  QrNFeItsI.SQL.Add('AND FatNum=:P1');
  QrNFeItsI.SQL.Add('AND Empresa=:P2');
  if QrNFeCabACriAForca.Value = 1 then
  begin
    QrNFeItsI.SQL.Add('AND MeuID=:P3');
    QrNFeItsI.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  end else begin
    QrNFeItsI.SQL.Add('AND nItem=:P3');
    QrNFeItsI.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  end;
  //
  QrNFeItsI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeItsI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeItsI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeItsI.Open;
  //
  QrNFeItsN.Close;
  QrNFeItsN.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeItsN.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeItsN.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeItsN.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  QrNFeItsN.Open;
  //
  QrNFeItsO.Close;
  QrNFeItsO.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrNFeItsO.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrNFeItsO.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrNFeItsO.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  QrNFeItsO.Open;
  //
end;

procedure TFmUsoConsCab.ReopenStqMovIts(IDCtrl: Integer);
  procedure ComplementaSQL(Tab, FatID, FatNum: String);
  begin
    QrStqMovIts.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrStqMovIts.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    QrStqMovIts.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrStqMovIts.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*');
    QrStqMovIts.SQL.Add('FROM stqmovits' + Tab + ' smia');
    QrStqMovIts.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrStqMovIts.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrStqMovIts.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrStqMovIts.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrStqMovIts.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrStqMovIts.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrStqMovIts.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
    QrStqMovIts.SQL.Add('WHERE smia.Tipo=' + FatID);
    QrStqMovIts.SQL.Add('AND smia.OriCodi=' + FatNum);
    //QrStqMovIts.SQL.Add('AND smia.OriCtrl=' + Ctrl);
  end;
var
  FatID, FatNum: String;
begin
  FatID  := FormatFloat('0', QrNFeCabAFatID.Value);
  FatNum := FormatFloat('0', QrNFeCabAFatNum.Value);
  //
  QrStqMovIts.Close;
  QrStqMovIts.SQL.Clear;
  ComplementaSQL('a', FatID, FatNum);
  QrStqMovIts.SQL.Add('');
  QrStqMovIts.SQL.Add('UNION');
  QrStqMovIts.SQL.Add('');
  ComplementaSQL('b', FatID, FatNum);
  QrStqMovIts.SQL.Add('');
  QrStqMovIts.Open;
  //
  QrStqMovIts.Locate('IDCtrl', IDCtrl, []);
end;

procedure TFmUsoConsCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmUsoConsCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmUsoConsCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmUsoConsCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmUsoConsCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmUsoConsCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := EdCodInfoEmit.ValueVariant;
  DModG.CadastroDeEntidade(VAR_CADASTRO, fmcadSelecionar, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCodInfoEmit, CBCodInfoEmit, QrFornece, VAR_CADASTRO);
end;

procedure TFmUsoConsCab.AlteraItemAtual1Click(Sender: TObject);
var
  Continua: Boolean;
begin
{
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
}
  if DBCheck.CriaFm(TFmUsoConsIts, FmUsoConsIts, afmoNegarComAviso) then
  begin
    ReopenNFeIts_I_N_O();
    //
    FmUsoConsIts.LaTipo.SQLType              := stUpd;
    FmUsoConsIts.FIDCtrl                     := QrStqMovItsIDCtrl.Value;
    FmUsoConsIts.EdnItem.ValueVariant        := QrNFEItsInItem.Value;
    FmUsoConsIts.Edprod_cProd.Text           := QrNFEItsIprod_cProd.Value;
    // 2011-05-03
    //FmUsoConsIts.EdGraGruX.ValueVariant      := QrNFEItsIGraGruX.Value;
    //FmUsoConsIts.CBGraGruX.KeyValue          := QrNFEItsIGraGruX.Value;
    FmUsoConsIts.EdGraGruX.ValueVariant      := QrStqMovItsGraGruX.Value;
    FmUsoConsIts.CBGraGruX.KeyValue          := QrStqMovItsGraGruX.Value;
    FmUsoConsIts.FMeuID                      := QrNfeItsIMeuID.Value;
    FmUsoConsIts.FTipo                       := QrStqMovItsTipo.Value;
    FmUsoConsIts.FOriCodi                    := QrStqMovItsOriCodi.Value;
    FmUsoConsIts.FOriCtrl                    := QrStqMovItsOriCtrl.Value;
    FmUsoConsIts.FMeuID                      := QrNfeItsIMeuID.Value;
    FmUsoConsIts.EdPecas.ValueVariant        := QrStqMovItsPecas.Value;
    FmUsoConsIts.EdPeso.ValueVariant         := QrStqMovItsPecas.Value;
    // fazer antes do m2 para n�o mudar o m2 original!
    FmUsoConsIts.EdAreaP2.ValueVariant       := QrStqMovItsAreaP2.Value;
    FmUsoConsIts.EdAreaM2.ValueVariant       := QrStqMovItsAreaM2.Value;
    // Deixar por �ltimo para s� mudar se alterar algum dado!
    FmUsoConsIts.EdQtde.ValueVariant         := QrStqMovItsQtde.Value;

    // Fim 2011-05-03
    FmUsoConsIts.Edprod_NCM.Text             := QrNFEItsIprod_NCM.Value;
    FmUsoConsIts.EdICMS_Orig.ValueVariant    := QrNFeItsNICMS_Orig.Value;
    FmUsoConsIts.EdICMS_CST.ValueVariant     := QrNFeItsNICMS_CST.Value;
    FmUsoConsIts.Edprod_CFOP.ValueVariant    := QrNFeItsIprod_CFOP.Value;
    FmUsoConsIts.Edprod_EXTIPI.ValueVariant  := QrNFeItsIprod_EXTIPI.Value;
    FmUsoConsIts.EdUnidMedCom.ValueVariant   := QrNFeItsIUnidMedCom.Value;
    FmUsoConsIts.Edprod_uCom.ValueVariant    := QrNFeItsIprod_uCom.Value;
    FmUsoConsIts.CBUnidMedCom.KeyValue       := QrNFeItsIUnidMedCom.Value;
    FmUsoConsIts.Edprod_qCom.ValueVariant    := QrNFeItsIprod_qCom.Value;
    FmUsoConsIts.EdUnidMedTrib.ValueVariant  := QrNFeItsIUnidMedTrib.Value;
    FmUsoConsIts.Edprod_uTrib.ValueVariant   := QrNFeItsIprod_uTrib.Value;
    FmUsoConsIts.CBUnidMedTrib.KeyValue      := QrNFeItsIUnidMedTrib.Value;
    FmUsoConsIts.Edprod_qTrib.ValueVariant   := QrNFeItsIprod_qTrib.Value;
    FmUsoConsIts.Edprod_vUnTrib.ValueVariant := QrNFeItsIprod_vUnTrib.Value;
    FmUsoConsIts.Edprod_vProd.ValueVariant   := QrNFeItsIprod_vProd.Value;
    FmUsoConsIts.EdICMS_vBC.ValueVariant     := QrNFeItsNICMS_vBC.Value;
    FmUsoConsIts.EdICMS_vICMS.ValueVariant   := QrNFeItsNICMS_vICMS.Value;
    FmUsoConsIts.EdIPI_vIPI.ValueVariant     := QrNFeItsOIPI_vIPI.Value;
    FmUsoConsIts.EdICMS_pICMS.ValueVariant   := QrNFeItsNICMS_pICMS.Value;
    FmUsoConsIts.EdIPI_pIPI.ValueVariant     := QrNFeItsOIPI_pIPI.Value;
    FmUsoConsIts.Edprod_vFrete.ValueVariant  := QrNFeItsIprod_vFrete.Value;
    FmUsoConsIts.Edprod_vSeg.ValueVariant    := QrNFeItsIprod_vSeg.Value;
    FmUsoConsIts.Edprod_vDesc.ValueVariant   := QrNFeItsIprod_vDesc.Value;
    FmUsoConsIts.Edprod_vOutro.ValueVariant  := QrNFeItsIprod_vOutro.Value;
    //
    if QrNFeCabACriAForca.Value = 1 then
    begin
      if Trim(QrNFeItsIprod_uCom.Value) = '' then
      begin
        FmUsoConsIts.Edprod_uCom.ValueVariant  := QrStqMovItsSIGLA.Value;
        FmUsoConsIts.EdUnidMedCom.ValueVariant := QrStqMovItsUnidMed.Value;
        FmUsoConsIts.CBUnidMedCom.KeyValue     := QrStqMovItsUnidMed.Value;
      end;
      if Trim(QrNFeItsIprod_uTrib.Value) = '' then
      begin
        FmUsoConsIts.Edprod_uTrib.ValueVariant  := QrStqMovItsSIGLA.Value;
        FmUsoConsIts.EdUnidMedTrib.ValueVariant := QrStqMovItsUnidMed.Value;
        FmUsoConsIts.CBUnidMedTrib.KeyValue     := QrStqMovItsUnidMed.Value;
      end;
      //
      if Geral.MensagemBox(
      'Os dados fiscais desta Nota Fiscal foram gerados na janela de exporta��o' + #13#10 +
      'para o aplicativo Prosoft. Para poder editar os itens desta nota �' + #13#10 +
      'necess�rio torn�-la como n�o "criada � for�a". Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET CriAForca=0');
        Dmod.QrUpd.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', QrNFeCabAIDCtrl.Value));
        Dmod.QrUpd.ExecSQL;
        //
        Continua := True;
      end;
    end else Continua := True;
    if Continua then
      FmUsoConsIts.ShowModal;
    FmUsoConsIts.Destroy;
  end;
end;

procedure TFmUsoConsCab.AlteraNF1Click(Sender: TObject);
begin
  if QrNFeCabAFatID.Value = 151 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeCabA, [PainelDados],
    [PainelEdita], EdEmpresa, LaTipo, 'nfecaba');
  end else
  begin
    // Parei Aqui!
  end;


end;

procedure TFmUsoConsCab.BtSaidaClick(Sender: TObject);
begin
 // ver o que fazer quando precisar
//  VAR_CADASTRO := QrNFeCabACodigo.Value;
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmUsoConsCab.BtConfirmaClick(Sender: TObject);
const
  FatID = 151;
  Status = 100;
  CriAForca = 0;
var
  FatNum, IDCtrl, Filial, Empresa: Integer;
  Id, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt, NFG_Serie: String;
  ide_mod, ide_serie, ide_nNF, ide_tpNF: Integer;
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF: Double;
  ModFrete, RetTransp_CMunFG: Integer;
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet: Double;
  RetTransp_CFOP, DataFiscal: String;
  CodInfoEmit, CodInfoDest, CodInfoTrsp: Integer;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a Empresa!') then
    Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  //






  Id := EdNFe_Id.Text;
  if not NFeXMLGeren.VerificaChaveAcesso(Id, True) then
    Exit;
  ide_dEmi           := Geral.FDT(TPide_dEmi.Date, 1);
  ide_dSaiEnt        := Geral.FDT(TPide_dSaiEnt.Date, 1);
  ide_hSaiEnt        := Edide_hSaiEnt.Text;
  ide_mod            := Edide_mod.ValueVariant;
  NFG_Serie          := EdNFG_Serie.Text;
  if not NFeXMLGeren.NFG_SerieToide_serie(ide_mod, NFG_Serie, ide_Serie) then
    Exit;
  ide_nNF            := Edide_nNF.ValueVariant;
  ide_tpNF           := RGide_tpNF.ItemIndex;
  //
  ICMSTot_vBC        := EdICMSTot_vBC    .ValueVariant;
  ICMSTot_vICMS      := EdICMSTot_vICMS  .ValueVariant;
  ICMSTot_vBCST      := EdICMSTot_vBCST  .ValueVariant;
  ICMSTot_vST        := EdICMSTot_vST    .ValueVariant;
  ICMSTot_vProd      := EdICMSTot_vProd  .ValueVariant;
  ICMSTot_vFrete     := EdICMSTot_vFrete .ValueVariant;
  ICMSTot_vSeg       := EdICMSTot_vSeg   .ValueVariant;
  ICMSTot_vDesc      := EdICMSTot_vDesc  .ValueVariant;
  ICMSTot_vII        := EdICMSTot_vII    .ValueVariant;
  ICMSTot_vIPI       := EdICMSTot_vIPI   .ValueVariant;
  ICMSTot_vPIS       := EdICMSTot_vPIS   .ValueVariant;
  ICMSTot_vCOFINS    := EdICMSTot_vCOFINS.ValueVariant;
  ICMSTot_vOutro     := EdICMSTot_vOutro .ValueVariant;
  ICMSTot_vNF        := EdICMSTot_vNF    .ValueVariant;
  //
  ModFrete           := EdModFrete.ValueVariant;
  //
  RetTransp_vServ    := EdRetTransp_vServ   .ValueVariant;
  RetTransp_vBCRet   := EdRetTransp_vBCRet  .ValueVariant;
  RetTransp_PICMSRet := EdRetTransp_PICMSRet.ValueVariant;
  RetTransp_vICMSRet := EdRetTransp_vICMSRet.ValueVariant;
  RetTransp_CFOP     := Geral.SoNumero_TT(EdRetTransp_CFOP.ValueVariant);
  RetTransp_CMunFG   := EdRetTransp_CMunFG  .ValueVariant;
  //
  DataFiscal         := Geral.FDT(TPDataFiscal.Date, 1);
  CodInfoEmit        := EdCodInfoEmit.ValueVariant;
  CodInfoDest        := Empresa;
  CodInfoTrsp        := EdCodInfoTrsp.ValueVariant;
  //
  IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);
  FatNum := EdFatNum.ValueVariant;
  FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', LaTipo.SQLType, FatNum, nil, True);
  //
{
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmUsoConsCab, PainelEdit,
    'nfecaba', IDCtrl, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(IDCtrl, IDCtrl);
  end;
}
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'nfecaba', False, [
  'IDCtrl', (*'LoteEnv', 'versao',*)
  'Id', (*'ide_cUF', 'ide_cNF',
  'ide_natOp', 'ide_indPag',*) 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
  (*'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
  'ide_cDV', 'ide_tpAmb', 'ide_finNFe',
  'ide_procEmi', 'ide_verProc', 'ide_dhCont',
  'ide_xJust', 'emit_CNPJ', 'emit_CPF',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone', 'emit_IE', 'emit_IEST',
  'emit_IM', 'emit_CNAE', 'emit_CRT',
  'dest_CNPJ', 'dest_CPF', 'dest_xNome',
  'dest_xLgr', 'dest_nro', 'dest_xCpl',
  'dest_xBairro', 'dest_cMun', 'dest_xMun',
  'dest_UF', 'dest_CEP', 'dest_cPais',
  'dest_xPais', 'dest_fone', 'dest_IE',
  'dest_ISUF', 'dest_email',*) 'ICMSTot_vBC',
  'ICMSTot_vICMS', 'ICMSTot_vBCST', 'ICMSTot_vST',
  'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
  'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
  'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
  'ICMSTot_vNF', (*'ISSQNtot_vServ', 'ISSQNtot_vBC',
  'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
  'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
  'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
  'RetTrib_vRetPrev',*) 'ModFrete', (*'Transporta_CNPJ',
  'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
  'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',*)
  'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
  'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
  (*'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
  'Vagao', 'Balsa', 'Cobr_Fat_nFat',
  'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
  'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
  'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
  'Compra_XCont',*) 'Status', (*'protNFe_versao',
  'infProt_Id', 'infProt_tpAmb', 'infProt_verAplic',
  'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', 'retCancNFe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', '_Ativo_', 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
  'DataFiscal', (*'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
  'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
  'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
  'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
  'CodInfoDest', 'CriAForca', 'CodInfoTrsp',
  (*'OrdemServ', 'Situacao', 'Antigo',*)
  'NFG_Serie'], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, (*LoteEnv, versao,*)
  Id, (*ide_cUF, ide_cNF,
  ide_natOp, ide_indPag,*) ide_mod,
  ide_serie, ide_nNF, ide_dEmi,
  ide_dSaiEnt, ide_hSaiEnt, ide_tpNF,
  (*ide_cMunFG, ide_tpImp, ide_tpEmis,
  ide_cDV, ide_tpAmb, ide_finNFe,
  ide_procEmi, ide_verProc, ide_dhCont,
  ide_xJust, emit_CNPJ, emit_CPF,
  emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro,
  emit_cMun, emit_xMun, emit_UF,
  emit_CEP, emit_cPais, emit_xPais,
  emit_fone, emit_IE, emit_IEST,
  emit_IM, emit_CNAE, emit_CRT,
  dest_CNPJ, dest_CPF, dest_xNome,
  dest_xLgr, dest_nro, dest_xCpl,
  dest_xBairro, dest_cMun, dest_xMun,
  dest_UF, dest_CEP, dest_cPais,
  dest_xPais, dest_fone, dest_IE,
  dest_ISUF, dest_email,*) ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST,
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
  ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro,
  ICMSTot_vNF, (*ISSQNtot_vServ, ISSQNtot_vBC,
  ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
  RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
  RetTrib_vRetPrev,*) ModFrete, (*Transporta_CNPJ,
  Transporta_CPF, Transporta_XNome, Transporta_IE,
  Transporta_XEnder, Transporta_XMun, Transporta_UF,*)
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, 
  RetTransp_vICMSRet, RetTransp_CFOP, RetTransp_CMunFG, 
  (*VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Vagao, Balsa, Cobr_Fat_nFat, 
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq, 
  InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq, 
  Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed, 
  Compra_XCont,*) Status, (*protNFe_versao,
  infProt_Id, infProt_tpAmb, infProt_verAplic,
  infProt_dhRecbto, infProt_nProt, infProt_digVal,
  infProt_cStat, infProt_xMotivo, retCancNFe_versao,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
  infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
  infCanc_xJust, _Ativo_, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra, ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
  DataFiscal, (*SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
  SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
  SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
  SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) CodInfoEmit,
  CodInfoDest, CriAForca, CodInfoTrsp,
  (*OrdemServ, Situacao, Antigo,*)
  NFG_Serie], [
  FatID, FatNum, Empresa], True) then
  begin
    if LaTipo.SQLType = stUpd then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET');
      Dmod.QrUpd.SQL.Add('DataHora=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P1');
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P2');
      Dmod.QrUpd.Params[00].AsString  := DataFiscal;
      Dmod.QrUpd.Params[01].AsInteger := FatID;
      Dmod.QrUpd.Params[02].AsInteger := FatNum;
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.ExecSQL;
      {
      QrStqMovIts.First;
      while not QrStqMovIts.Eof do
      begin

        // N�o usar pois tem o IDCtrl do NFeCabA
        //IDCtrl := QrStqMovItsIDCtrl.Value;
        //Fazer assim
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
        'DataHora'], ['IDCtrl'], [
        DataFiscal], [QrStqMovItsIDCtrl.Value], False);
        //
        QrStqMovIts.Next;
      end;
      }
    end;
    LocCod(IDCtrl, IDCtrl);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmUsoConsCab.BtDesisteClick(Sender: TObject);
var
  IDCtrl : Integer;
begin
  IDCtrl := Geral.IMV(EdIDCtrl.Text);
{
  if LaTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'nfecaba', IDCtrl);
}
  UMyMod.UpdUnlockY(IDCtrl, Dmod.MyDB, 'nfecaba', 'IDCtrl');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmUsoConsCab.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmUsoConsCab.BtItemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItem, BtItem);
end;

procedure TFmUsoConsCab.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
//CB?.ListSource = DModG.DsEmpresas;

begin
  FIDs_Txt := FormatFloat('0', VAR_FATID_0051) + ', ' +
              FormatFloat('0', VAR_FATID_0151) + ', ' +
              FormatFloat('0', VAR_FATID_0251);

  PainelEdit.Align  := alClient;
  DBGItens.Align    := alClient;
  CriaOForm;
  //
  QrNFMoDocFis.Close;
  QrNFMoDocFis.Database := Dmod.MyDB;
  QrNFMoDocFis.Open;
  //
  QrFornece.Close;
  QrFornece.Database := Dmod.MyDB;
  QrFornece.Open;
  //
  QrTransporta.Close;
  QrTransporta.Database := Dmod.MyDB;
  QrTransporta.Open;
  //
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_Munici, DModG.AllID_DB);
end;

procedure TFmUsoConsCab.SbNumeroClick(Sender: TObject);
begin
  //N�o funcionam, Parei Aqui!
  MyObjects.MostraPopUpDeBotao(PMNumero, SbNumero);
end;

procedure TFmUsoConsCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmUsoConsCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNFeCabACodUsu.Value, LaRegistro.Caption);
end;

procedure TFmUsoConsCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmUsoConsCab.QrForneceCalcFields(DataSet: TDataSet);
begin
  QrForneceCNPJ_CPF_TXT.Value :=
  Geral.FormataCNPJ_TT(QrForneceCNPJ_CPF.Value);
end;

procedure TFmUsoConsCab.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmUsoConsCab.QrNFeCabAAfterScroll(DataSet: TDataSet);
var
  Cor: TColor;
begin
  ReopenStqMovIts(0);
  case QrNFeCabAFatID.Value of
    VAR_FATID_0051: Cor := clGreen;
    VAR_FATID_0151: Cor := clRed;
    VAR_FATID_0251: Cor := clBlue;
    else Cor := clFuchsia;
  end;
  //
  DBEdFatID.Font.Color := Cor;
  DBEdNO_FATID.Font.Color := Cor;
end;

procedure TFmUsoConsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;







procedure TFmUsoConsCab.SbQueryClick(Sender: TObject);
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmUsoConsPesq, FmUsoConsPesq);
    FmUsoConsPesq.ShowModal;
    FmUsoConsPesq.Destroy;
  finally
    Screen.Cursor := Cursor;
  end;
  if VAR_PQELANCTO <> -1 then LocCod(VAR_PQELANCTO, VAR_PQELANCTO);
end;

procedure TFmUsoConsCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;











procedure TFmUsoConsCab.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeCabAIDCtrl.Value, LaRegistro.Caption);
end;

procedure TFmUsoConsCab.Incluinovoitem1Click(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
{
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
}
  if DBCheck.CriaFm(TFmUsoConsIts, FmUsoConsIts, afmoNegarComAviso) then
  begin
    FmUsoConsIts.LaTipo.SQLType := stIns;
    FatID   := QrNFeCabAFatId.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    nItem := DModG.BuscaProximoInteiro('nfeitsI', 'nItem', 'FatID', FatID,
    'FatNum', FatNum, 'Empresa', Empresa);
    FmUsoConsIts.EdnItem.ValueVariant := nItem;
    //
    {
    FmUsoConsIts.FForm                   := 'NFeCabA_0000';
    FmUsoConsIts.Fdest_CNPJ              := QrNFeCabAdest_CNPJ.Value;
    FmUsoConsIts.Fdest_CPF               := QrNFeCabAdest_CPF.Value;
    FmUsoConsIts.FEmpresa                := QrNFeCabAEmpresa.Value;
    FmUsoConsIts.FFisRegCad              := QrNFeCabAFisRegCad.Value;
    FmUsoConsIts.FTabelaPrc              := QrNFeCabATabelaPrc.Value;
    FmUsoConsIts.FCondicaoPg             := QrNFeCabACondicaoPG.Value;
    FmUsoConsIts.FMedDDSimpl             := QrNFeCabAMedDDSimpl.Value;
    FmUsoConsIts.FMedDDReal              := QrNFeCabAMedDDReal.Value;
    }
    //
    FmUsoConsIts.ShowModal;
    FmUsoConsIts.Destroy;
  end;
end;

procedure TFmUsoConsCab.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
end;

procedure TFmUsoConsCab.QrNFeCabABeforeOpen(DataSet: TDataSet);
begin
  QrNFeCabAIDCtrl.DisplayFormat := FFormatFloat;
end;

procedure TFmUsoConsCab.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  QrNFeCabACNPJ_CPF_FRN_TXT.Value :=
    Geral.FormataCNPJ_TT(QrNFeCabACNPJ_CPF_FRN.Value);
  case QrNFeCabAFatID.Value of
    VAR_FATID_0051: QrNFeCabANO_FATID.Value := 'Importa��o NFe';
    VAR_FATID_0151: QrNFeCabANO_FATID.Value := 'Digitado Manual';
    VAR_FATID_0251: QrNFeCabANO_FATID.Value := 'Importa��o SPED-EFD';
    else QrNFeCabANO_FATID.Value := '? ? ? ? ?';
  end;




end;

procedure TFmUsoConsCab.QrStqMovItsBeforeClose(DataSet: TDataSet);
begin
  QrNFeItsI.Close;
  QrNFeItsN.Close;
end;

procedure TFmUsoConsCab.QrTransportaCalcFields(DataSet: TDataSet);
begin
  QrTransportaCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrTransportaCNPJ_CPF.Value);
end;

end.

