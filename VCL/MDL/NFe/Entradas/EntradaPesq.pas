unit EntradaPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, Db, (*DBTables,*) StdCtrls, Buttons,
  ComCtrls, DBCtrls, UnInternalConsts, mySQLDbTables, Variants, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkDBGrid, dmkEditDateTimePicker,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmEntradaPesq = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    DBGrid1: TdmkDBGrid;
    QrNFs: TmySQLQuery;
    DsNFs: TDataSource;
    CBFornecedor: TdmkDBLookupComboBox;
    Label1: TLabel;
    Label3: TLabel;
    CBGraGru1: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    EdCodigo: TdmkEdit;
    EdNF: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    EdFornecedor: TdmkEditCB;
    EdGraGru1: TdmkEditCB;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNome: TWideStringField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrNFsNO_FRN: TWideStringField;
    QrNFside_nNF: TIntegerField;
    QrNFsemit_CNPJ: TWideStringField;
    QrNFsDataFiscal: TDateField;
    QrNFsNivel1: TIntegerField;
    QrNFsNO_PRD: TWideStringField;
    QrNFsIDCtrl: TIntegerField;
    QrNFsFatNum: TIntegerField;
    Label2: TLabel;
    EdIDCtrl: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodigoExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdConhecimentoExit(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrFornecedoresAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FLiberado: Boolean;

    procedure Pesquisa();

  public
    { Public declarations }
  end;

var
  FmEntradaPesq: TFmEntradaPesq;

implementation

uses UnMyObjects, EntradaCab, Principal, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmEntradaPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FLiberado := True;
end;

procedure TFmEntradaPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLiberado := False;
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  TPIni.Date := Date - 90;
  TPFim.Date := Date;
end;

procedure TFmEntradaPesq.CBFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBFornecedor.KeyValue := NULL;
  Pesquisa();
end;

procedure TFmEntradaPesq.CBGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBGraGru1.KeyValue := NULL;
  Pesquisa();
end;

procedure TFmEntradaPesq.EdCodigoExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.EdNFExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.EdConhecimentoExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.Pesquisa();
var
  Ini, Fim: String;
begin
  if FLiberado then
  begin
    Ini := Geral.FDT(TPIni.Date, 1);
    Fim := Geral.FDT(TPFim.Date, 1);
    ////////////
    QrNFs.Close;
    QrNFs.SQL.Clear;
{
    QrNFs.SQL.Add('SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
    QrNFs.SQL.Add('nfa.ide_nNF, nfa.emit_CNPJ, nfa.DataFiscal,');
    QrNFs.SQL.Add('gg1.Nivel1, gg1.Nome NO_PRD, nfa.FatNum, nfa.IDCtrl');
    QrNFs.SQL.Add('FROM stqmovitsa smia');
    QrNFs.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrNFs.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrNFs.SQL.Add('LEFT JOIN nfecaba    nfa ON nfa.FatID=smia.Tipo');
    QrNFs.SQL.Add('  AND nfa.FatNum=smia.OriCodi');
    QrNFs.SQL.Add('LEFT JOIN entidades  frn ON frn.Codigo=nfa.CodInfoEmit');
    QrNFs.SQL.Add('WHERE smia.Tipo IN (' + FmEntradaCab.FIDs_Txt + ')');
    QrNFs.SQL.Add(dmkPF.SQL_Periodo('AND nfa.DataFiscal ',
      TPIni.Date, TPFim.Date, True, True));
}
    QrNFs.SQL.Add('SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
    QrNFs.SQL.Add('nfa.ide_nNF, nfa.emit_CNPJ, nfa.DataFiscal,');
    QrNFs.SQL.Add('gg1.Nivel1, gg1.Nome NO_PRD, nfa.FatNum, nfa.IDCtrl');
    QrNFs.SQL.Add('FROM nfecaba    nfa');
    QrNFs.SQL.Add('LEFT JOIN stqmovitsa smia ON nfa.FatID=smia.Tipo');
    QrNFs.SQL.Add('  AND nfa.FatNum=smia.OriCodi AND smia.Tipo IN (' + FmEntradaCab.FIDs_Txt + ')');
    QrNFs.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrNFs.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrNFs.SQL.Add('LEFT JOIN entidades  frn ON frn.Codigo=nfa.CodInfoEmit');
    QrNFs.SQL.Add(dmkPF.SQL_Periodo('WHERE nfa.DataFiscal ',
      TPIni.Date, TPFim.Date, True, True));
    //  
    if EdCodigo.ValueVariant <> 0 then
     QrNFs.SQL.Add('AND nfa.FatNum=' + FormatFloat('0', EdCodigo.ValueVariant));
    if EdNF.ValueVariant <> 0 then
      QrNFs.SQL.Add('AND nfa.ide_nNF=' + FormatFloat('0', EdNF.ValueVariant));
    if EdIDCtrl.ValueVariant <> 0 then
      QrNFs.SQL.Add('AND nfa.IDCtrl=' + FormatFloat('0', EdIDCtrl.ValueVariant));
    if CBFornecedor.KeyValue <> NULL then
      QrNFs.SQL.Add('AND nfa.CodInfoEmit=' + FormatFloat('0', EdFornecedor.ValueVariant));
    if EdGraGru1.ValueVariant <> 0 then
    //  ver Reduzidos
      QrNFs.SQL.Add('AND smia.GraGruX=' + FormatFloat('0', EdGraGru1.ValueVariant));
    //MLAGeral.LeMeuSQLy(QrNFs, '', nil, False, True);
    UnDmkDAC_PF.AbreQuery(QrNFs, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  end;
end;

procedure TFmEntradaPesq.RGOrdem1Click(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.EdFornecedorChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.EdGraGru1Change(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.TPIniClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.TPFimClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.BtSaidaClick(Sender: TObject);
begin
  VAR_PQELANCTO := -1;
  Close;
end;

procedure TFmEntradaPesq.BtConfirmaClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrNFsIDCtrl.Value;
  Close;
end;

procedure TFmEntradaPesq.QrFornecedoresAfterScroll(DataSet: TDataSet);
begin
{
  QrInsumos.Close;
  QrInsumos.SQL.Clear;
  QrInsumos.SQL.Add('SELECT * FROM pq');
  if CBFornecedor.KeyValue <> NULL then
  begin
    CBInsumo.KeyValue := NULL;
    QrInsumos.SQL.Add('WHERE IQ='''+IntToStr(CBFornecedor.KeyValue)+'''');
  end;
  QrInsumos.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrInsumos, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
}
end;

procedure TFmEntradaPesq.DBGrid1DblClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrNFsIDCtrl.Value;
  Close;
end;

procedure TFmEntradaPesq.TPFimChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.TPIniChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmEntradaPesq.FormResize(Sender: TObject);
var
  Titulo: String;
begin
  //12345678901234567890123456789012345678901
  //Pesquisa de Entrada de ???
  Titulo := Copy(LaTitulo1C.Caption, 1, 23) + FmPrincipal.FTipoEntradTitu;
  LaTitulo1C.Caption := Titulo;
  // 1234567890123456789012345678901234567890
  // INN-GERAL-001 :: Pesquisa de Entrada de ???
  Titulo := Copy(Caption, 1, 40) + FmPrincipal.FTipoEntradTitu;
  Caption := Titulo;
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

