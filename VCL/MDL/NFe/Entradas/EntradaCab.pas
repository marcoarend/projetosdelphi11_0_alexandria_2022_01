unit EntradaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, Variants,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Grids, DBGrids, MyListas,
  dmkDBLookupComboBox, dmkEditCB, ComCtrls, dmkEditDateTimePicker, dmkRadioGroup,
  dmkEditF7, Menus, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmEntradaCab = class(TForm)
    PainelDados: TPanel;
    DsNFeCabA: TDataSource;
    QrNFeCabA: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabANO_EMP: TWideStringField;
    QrNFeCabAFilial: TIntegerField;
    QrNFeCabANO_FATID: TWideStringField;
    QrNFeCabACodInfoEmit: TIntegerField;
    QrNFeCabACodInfoDest: TIntegerField;
    QrNFeCabANO_FRN: TWideStringField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAide_natOp: TWideStringField;
    QrNFeCabAide_mod: TSmallintField;
    QrNFeCabAide_serie: TIntegerField;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_dSaiEnt: TDateField;
    QrNFeCabAide_hSaiEnt: TTimeField;
    QrNFeCabAide_tpNF: TSmallintField;
    QrNFeCabAide_tpEmis: TSmallintField;
    QrNFeCabAide_tpAmb: TSmallintField;
    QrNFeCabAide_procEmi: TSmallintField;
    QrNFeCabAICMSTot_vBC: TFloatField;
    QrNFeCabAICMSTot_vICMS: TFloatField;
    QrNFeCabAICMSTot_vBCST: TFloatField;
    QrNFeCabAICMSTot_vST: TFloatField;
    QrNFeCabAICMSTot_vProd: TFloatField;
    QrNFeCabAICMSTot_vFrete: TFloatField;
    QrNFeCabAICMSTot_vSeg: TFloatField;
    QrNFeCabAICMSTot_vDesc: TFloatField;
    QrNFeCabAICMSTot_vII: TFloatField;
    QrNFeCabAICMSTot_vIPI: TFloatField;
    QrNFeCabAICMSTot_vPIS: TFloatField;
    QrNFeCabAICMSTot_vCOFINS: TFloatField;
    QrNFeCabAICMSTot_vOutro: TFloatField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabACodInfoTrsp: TIntegerField;
    QrNFeCabAModFrete: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabADataFiscal: TDateField;
    QrNFeCabAStatus: TIntegerField;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    QrStqMovItsGrupoBal: TIntegerField;
    QrStqMovItsBaixa: TSmallintField;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    GroupBox1: TGroupBox;
    EdIDCtrl: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdFatNum: TdmkEdit;
    Label40: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Edide_mod: TdmkEditCB;
    CBide_mod: TdmkDBLookupComboBox;
    QrNFMoDocFis: TmySQLQuery;
    DsNFMoDocFis: TDataSource;
    QrNFMoDocFisCodigo: TIntegerField;
    QrNFMoDocFisNome: TWideStringField;
    EdNFG_Serie: TdmkEdit;
    Label41: TLabel;
    Edide_nNF: TdmkEdit;
    Label42: TLabel;
    Label43: TLabel;
    TPide_dEmi: TdmkEditDateTimePicker;
    Label44: TLabel;
    TPide_dSaiEnt: TdmkEditDateTimePicker;
    Label45: TLabel;
    TPDataFiscal: TdmkEditDateTimePicker;
    Edide_hSaiEnt: TdmkEdit;
    Label46: TLabel;
    RGide_tpNF: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    Label37: TLabel;
    EdCodInfoEmit: TdmkEditCB;
    CBCodInfoEmit: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOME: TWideStringField;
    DsFornece: TDataSource;
    QrTransporta: TmySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOME: TWideStringField;
    DsTransporta: TDataSource;
    SpeedButton5: TSpeedButton;
    GroupBox4: TGroupBox;
    Label47: TLabel;
    EdICMSTot_vBC: TdmkEdit;
    EdICMSTot_vICMS: TdmkEdit;
    Label48: TLabel;
    Label49: TLabel;
    EdICMSTot_vBCST: TdmkEdit;
    Label50: TLabel;
    EdICMSTot_vST: TdmkEdit;
    Label51: TLabel;
    EdICMSTot_vProd: TdmkEdit;
    Label52: TLabel;
    EdICMSTot_vFrete: TdmkEdit;
    Label53: TLabel;
    EdICMSTot_vSeg: TdmkEdit;
    Label54: TLabel;
    EdICMSTot_vDesc: TdmkEdit;
    Label55: TLabel;
    EdICMSTot_vII: TdmkEdit;
    Label56: TLabel;
    EdICMSTot_vIPI: TdmkEdit;
    Label57: TLabel;
    EdICMSTot_vPIS: TdmkEdit;
    Label58: TLabel;
    EdICMSTot_vCOFINS: TdmkEdit;
    Label59: TLabel;
    EdICMSTot_vOutro: TdmkEdit;
    Label60: TLabel;
    EdICMSTot_vNF: TdmkEdit;
    GroupBox5: TGroupBox;
    EdModFrete: TdmkEdit;
    Label163: TLabel;
    EdModFrete_TXT: TdmkEdit;
    Label61: TLabel;
    EdCodInfoTrsp: TdmkEditCB;
    CBCodInfoTrsp: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    EdRetTransp_vServ: TdmkEdit;
    Label62: TLabel;
    Label63: TLabel;
    EdRetTransp_vBCRet: TdmkEdit;
    Label64: TLabel;
    EdRetTransp_PICMSRet: TdmkEdit;
    Label65: TLabel;
    EdRetTransp_vICMSRet: TdmkEdit;
    Label174: TLabel;
    EdRetTransp_CFOP: TdmkEdit;
    Label175: TLabel;
    EdRetTransp_CMunFG: TdmkEdit;
    EdRetTransp_CMunFG_TXT: TdmkEditF7;
    EdNFe_Id: TdmkEdit;
    Label66: TLabel;
    QrForneceCNPJ_CPF: TWideStringField;
    DBEdCNPJ_CPF_TXT_Fornece: TDBEdit;
    QrForneceCNPJ_CPF_TXT: TWideStringField;
    QrTransportaCNPJ_CPF_TXT: TWideStringField;
    QrTransportaCNPJ_CPF: TWideStringField;
    DBEdCNPJ_CPF_TXT_Transporta: TDBEdit;
    PMInclui: TPopupMenu;
    porXML1: TMenuItem;
    Manualmente1: TMenuItem;
    IncluinovaNF1: TMenuItem;
    AlteraNF1: TMenuItem;
    QrNFeCabANFG_Serie: TWideStringField;
    QrNFeCabARetTransp_vServ: TFloatField;
    QrNFeCabARetTransp_vBCRet: TFloatField;
    QrNFeCabARetTransp_PICMSRet: TFloatField;
    QrNFeCabARetTransp_vICMSRet: TFloatField;
    QrNFeCabARetTransp_CFOP: TWideStringField;
    QrNFeCabARetTransp_CMunFG: TWideStringField;
    PMItem: TPopupMenu;
    Incluinovoitem1: TMenuItem;
    AlteraItemAtual1: TMenuItem;
    QrNFeCabACNPJ_CPF_FRN: TWideStringField;
    QrNFeCabACNPJ_CPF_FRN_TXT: TWideStringField;
    ExcluiitemAtual1: TMenuItem;
    QrI: TmySQLQuery;
    QrN: TmySQLQuery;
    QrO: TmySQLQuery;
    QrInItem: TIntegerField;
    QrIprod_cProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrIprod_vOutro: TFloatField;
    QrIGraGruX: TIntegerField;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrIUnidMedCom: TIntegerField;
    QrIUnidMedTrib: TIntegerField;
    QrOIPI_pIPI: TFloatField;
    QrOIPI_vIPI: TFloatField;
    QrNFeCabACriAForca: TSmallintField;
    QrIMeuID: TIntegerField;
    PMNumero: TPopupMenu;
    Cdigo1: TMenuItem;
    ID1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGItens: TDBGrid;
    DBGrid1: TDBGrid;
    DsNFeItsI: TDataSource;
    QrNFeItsI: TmySQLQuery;
    EstoqueeFiscal1: TMenuItem;
    EstoqueeFiscal2: TMenuItem;
    SomenteFiscal1: TMenuItem;
    QrNFeItsN: TmySQLQuery;
    QrNFeItsNICMS_Orig: TSmallintField;
    QrNFeItsNICMS_CST: TSmallintField;
    QrNFeItsNICMS_modBC: TSmallintField;
    QrNFeItsNICMS_pRedBC: TFloatField;
    QrNFeItsNICMS_vBC: TFloatField;
    QrNFeItsNICMS_pICMS: TFloatField;
    QrNFeItsNICMS_vICMS: TFloatField;
    QrNFeItsNICMS_modBCST: TSmallintField;
    QrNFeItsNICMS_pMVAST: TFloatField;
    QrNFeItsNICMS_pRedBCST: TFloatField;
    QrNFeItsNICMS_vBCST: TFloatField;
    QrNFeItsNICMS_pICMSST: TFloatField;
    QrNFeItsNICMS_vICMSST: TFloatField;
    DsNFeItsN: TDataSource;
    QrNFeItsO: TmySQLQuery;
    QrNFeItsOIPI_clEnq: TWideStringField;
    QrNFeItsOIPI_CNPJProd: TWideStringField;
    QrNFeItsOIPI_cSelo: TWideStringField;
    QrNFeItsOIPI_qSelo: TFloatField;
    QrNFeItsOIPI_cEnq: TWideStringField;
    QrNFeItsOIPI_CST: TSmallintField;
    QrNFeItsOIPI_vBC: TFloatField;
    QrNFeItsOIPI_qUnid: TFloatField;
    QrNFeItsOIPI_vUnid: TFloatField;
    QrNFeItsOIPI_pIPI: TFloatField;
    QrNFeItsOIPI_vIPI: TFloatField;
    DsNFeItsO: TDataSource;
    QrNFeItsP: TmySQLQuery;
    QrNFeItsPII_vBC: TFloatField;
    QrNFeItsPII_vDespAdu: TFloatField;
    QrNFeItsPII_vII: TFloatField;
    QrNFeItsPII_vIOF: TFloatField;
    DsNFeItsP: TDataSource;
    QrNFeItsQ: TmySQLQuery;
    QrNFeItsQPIS_CST: TSmallintField;
    QrNFeItsQPIS_vBC: TFloatField;
    QrNFeItsQPIS_pPIS: TFloatField;
    QrNFeItsQPIS_vPIS: TFloatField;
    QrNFeItsQPIS_qBCProd: TFloatField;
    QrNFeItsQPIS_vAliqProd: TFloatField;
    DsNFeItsQ: TDataSource;
    QrNFeItsR: TmySQLQuery;
    QrNFeItsRPISST_vBC: TFloatField;
    QrNFeItsRPISST_pPIS: TFloatField;
    QrNFeItsRPISST_qBCProd: TFloatField;
    QrNFeItsRPISST_vAliqProd: TFloatField;
    QrNFeItsRPISST_vPIS: TFloatField;
    QrNFeItsRPISST_fatorBCST: TFloatField;
    DsNFeItsR: TDataSource;
    QrNFeItsS: TmySQLQuery;
    QrNFeItsSCOFINS_CST: TSmallintField;
    QrNFeItsSCOFINS_vBC: TFloatField;
    QrNFeItsSCOFINS_pCOFINS: TFloatField;
    QrNFeItsSCOFINS_qBCProd: TFloatField;
    QrNFeItsSCOFINS_vAliqProd: TFloatField;
    QrNFeItsSCOFINS_vCOFINS: TFloatField;
    DsNFeItsS: TDataSource;
    QrNFeItsT: TmySQLQuery;
    QrNFeItsTCOFINSST_vBC: TFloatField;
    QrNFeItsTCOFINSST_pCOFINS: TFloatField;
    QrNFeItsTCOFINSST_qBCProd: TFloatField;
    QrNFeItsTCOFINSST_vAliqProd: TFloatField;
    QrNFeItsTCOFINSST_vCOFINS: TFloatField;
    QrNFeItsTCOFINSST_fatorBCST: TFloatField;
    DsNFeItsT: TDataSource;
    QrNFeItsV: TmySQLQuery;
    QrNFeItsVInfAdProd: TWideMemoField;
    DsNFeItsV: TDataSource;
    QrNFeItsIFatID: TIntegerField;
    QrNFeItsIFatNum: TIntegerField;
    QrNFeItsIEmpresa: TIntegerField;
    QrNFeItsInItem: TIntegerField;
    QrNFeItsIprod_cProd: TWideStringField;
    QrNFeItsIprod_cEAN: TWideStringField;
    QrNFeItsIprod_xProd: TWideStringField;
    QrNFeItsIprod_NCM: TWideStringField;
    QrNFeItsIprod_EXTIPI: TWideStringField;
    QrNFeItsIprod_genero: TSmallintField;
    QrNFeItsIprod_CFOP: TIntegerField;
    QrNFeItsIprod_uCom: TWideStringField;
    QrNFeItsIprod_qCom: TFloatField;
    QrNFeItsIprod_vUnCom: TFloatField;
    QrNFeItsIprod_vProd: TFloatField;
    QrNFeItsIprod_cEANTrib: TWideStringField;
    QrNFeItsIprod_uTrib: TWideStringField;
    QrNFeItsIprod_qTrib: TFloatField;
    QrNFeItsIprod_vUnTrib: TFloatField;
    QrNFeItsIprod_vFrete: TFloatField;
    QrNFeItsIprod_vSeg: TFloatField;
    QrNFeItsIprod_vDesc: TFloatField;
    QrNFeItsITem_IPI: TSmallintField;
    QrNFeItsI_Ativo_: TSmallintField;
    QrNFeItsIInfAdCuztm: TIntegerField;
    QrNFeItsIEhServico: TIntegerField;
    QrNFeItsILk: TIntegerField;
    QrNFeItsIDataCad: TDateField;
    QrNFeItsIDataAlt: TDateField;
    QrNFeItsIUserCad: TIntegerField;
    QrNFeItsIUserAlt: TIntegerField;
    QrNFeItsIAlterWeb: TSmallintField;
    QrNFeItsIAtivo: TSmallintField;
    QrNFeItsIICMSRec_pRedBC: TFloatField;
    QrNFeItsIICMSRec_vBC: TFloatField;
    QrNFeItsIICMSRec_pAliq: TFloatField;
    QrNFeItsIICMSRec_vICMS: TFloatField;
    QrNFeItsIIPIRec_pRedBC: TFloatField;
    QrNFeItsIIPIRec_vBC: TFloatField;
    QrNFeItsIIPIRec_pAliq: TFloatField;
    QrNFeItsIIPIRec_vIPI: TFloatField;
    QrNFeItsIPISRec_pRedBC: TFloatField;
    QrNFeItsIPISRec_vBC: TFloatField;
    QrNFeItsIPISRec_pAliq: TFloatField;
    QrNFeItsIPISRec_vPIS: TFloatField;
    QrNFeItsICOFINSRec_pRedBC: TFloatField;
    QrNFeItsICOFINSRec_vBC: TFloatField;
    QrNFeItsICOFINSRec_pAliq: TFloatField;
    QrNFeItsICOFINSRec_vCOFINS: TFloatField;
    QrNFeItsIMeuID: TIntegerField;
    QrNFeItsINivel1: TIntegerField;
    QrNFeItsIprod_vOutro: TFloatField;
    QrNFeItsIprod_indTot: TSmallintField;
    QrNFeItsIprod_xPed: TWideStringField;
    QrNFeItsIprod_nItemPed: TIntegerField;
    QrNFeItsIGraGruX: TIntegerField;
    QrNFeItsIUnidMedCom: TIntegerField;
    QrNFeItsIUnidMedTrib: TIntegerField;
    QrNFeCabANF_ICMSAlq: TFloatField;
    QrNFeCabANF_CFOP: TWideStringField;
    QrNFeCabAImportado: TSmallintField;
    QrNFeCabANFG_SubSerie: TWideStringField;
    QrNFeCabANFG_ValIsen: TFloatField;
    QrNFeCabANFG_NaoTrib: TFloatField;
    QrNFeCabANFG_Outros: TFloatField;
    QrNFeCabACOD_MOD: TWideStringField;
    QrNFeCabACOD_SIT: TSmallintField;
    QrNFeCabAVL_ABAT_NT: TFloatField;
    QrNFeCabAEFD_INN_AnoMes: TIntegerField;
    QrNFeCabAEFD_INN_Empresa: TIntegerField;
    QrNFeCabAEFD_INN_LinArq: TIntegerField;
    QrNFeCabAICMSRec_vBCST: TFloatField;
    QrNFeCabAICMSRec_vICMSST: TFloatField;
    QrNFeCabAEFD_EXP_REG: TWideStringField;
    QrNFeCabAMODELO_TXT: TWideStringField;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    QrNFeCabAIMPORTADO_TXT: TWideStringField;
    QrNFeCabAICMSRec_pRedBC: TFloatField;
    QrNFeCabAICMSRec_vBC: TFloatField;
    QrNFeCabAICMSRec_pAliq: TFloatField;
    QrNFeCabAICMSRec_vICMS: TFloatField;
    QrNFeCabAIPIRec_pRedBC: TFloatField;
    QrNFeCabAIPIRec_vBC: TFloatField;
    QrNFeCabAIPIRec_pAliq: TFloatField;
    QrNFeCabAIPIRec_vIPI: TFloatField;
    QrNFeCabAPISRec_pRedBC: TFloatField;
    QrNFeCabAPISRec_vBC: TFloatField;
    QrNFeCabAPISRec_pAliq: TFloatField;
    QrNFeCabAPISRec_vPIS: TFloatField;
    QrNFeCabACOFINSRec_pRedBC: TFloatField;
    QrNFeCabACOFINSRec_vBC: TFloatField;
    QrNFeCabACOFINSRec_pAliq: TFloatField;
    QrNFeCabACOFINSRec_vCOFINS: TFloatField;
    GroupBox6: TGroupBox;
    Label69: TLabel;
    Label77: TLabel;
    Label79: TLabel;
    Label68: TLabel;
    Label75: TLabel;
    Label4: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label74: TLabel;
    Label76: TLabel;
    DBEdit35: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit45: TDBEdit;
    GroupBox7: TGroupBox;
    Panel6: TPanel;
    GroupBox8: TGroupBox;
    Label84: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    DBEdit53: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    GroupBox9: TGroupBox;
    Label78: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    DBEdit47: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    GroupBox10: TGroupBox;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    GroupBox11: TGroupBox;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    GroupBox12: TGroupBox;
    Label93: TLabel;
    Label95: TLabel;
    DBEdit62: TDBEdit;
    DBEdit64: TDBEdit;
    GroupBox13: TGroupBox;
    DBEdit50: TDBEdit;
    Label81: TLabel;
    GroupBox14: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label15: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label67: TLabel;
    Label70: TLabel;
    DBEdIDCtrl: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdFatID: TDBEdit;
    DBEdNO_FatID: TDBEdit;
    DBEdit38: TDBEdit;
    QrNFeItsIICMSRec_pAliqST: TFloatField;
    DBEdit42: TDBEdit;
    Label96: TLabel;
    QrNFeCabAICMSRec_pAliqST: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtItem: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBOutros: TGroupBox;
    PnBlDrm: TPanel;
    Label73: TLabel;
    EdNF_RP: TdmkEdit;
    EdNF_CC: TdmkEdit;
    Label80: TLabel;
    EdConhecimento: TdmkEdit;
    Label94: TLabel;
    Label97: TLabel;
    EdFrete: TdmkEdit;
    Label98: TLabel;
    EdPesoL: TdmkEdit;
    Label99: TLabel;
    EdPesoB: TdmkEdit;
    GroupBox15: TGroupBox;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    QrUsoCons: TmySQLQuery;
    QrUsoConsErrNota: TFloatField;
    QrUsoConsErrBruto: TFloatField;
    QrUsoConsErrLiq: TFloatField;
    QrUsoConsErrPagT: TFloatField;
    QrUsoConsErrPagF: TFloatField;
    DBEdErrNota: TDBEdit;
    DsUsoCons: TDataSource;
    DBEdErrBruto: TDBEdit;
    DBEdErrLiq: TDBEdit;
    DBEdErrPagT: TDBEdit;
    DBEdErrPagF: TDBEdit;
    QrUsoConsCodigo: TIntegerField;
    QrOIPI_CST: TIntegerField;
    QrOIND_APUR: TWideStringField;
    QrCI: TmySQLQuery;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    DsCI: TDataSource;
    EdCodInfoCliI: TdmkEditCB;
    Label105: TLabel;
    CBCodInfoCliI: TdmkDBLookupComboBox;
    QrNFeCabACodInfoCliI: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeCabABeforeOpen(DataSet: TDataSet);
    procedure QrNFeCabACalcFields(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdModFreteChange(Sender: TObject);
    procedure EdRetTransp_CMunFGChange(Sender: TObject);
    procedure QrForneceCalcFields(DataSet: TDataSet);
    procedure QrTransportaCalcFields(DataSet: TDataSet);
    procedure EdCodInfoEmitChange(Sender: TObject);
    procedure EdCodInfoTrspChange(Sender: TObject);
    procedure Manualmente1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure AlteraNF1Click(Sender: TObject);
    procedure BtItemClick(Sender: TObject);
    procedure Incluinovoitem1Click(Sender: TObject);
    procedure Cdigo1Click(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrStqMovItsBeforeClose(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure PMItemPopup(Sender: TObject);
    procedure EstoqueeFiscal1Click(Sender: TObject);
    procedure EstoqueeFiscal2Click(Sender: TObject);
    procedure SomenteFiscal1Click(Sender: TObject);
    procedure QrNFeItsIAfterScroll(DataSet: TDataSet);
    procedure QrNFeItsIBeforeClose(DataSet: TDataSet);
    procedure porXML1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure EdNFe_IdExit(Sender: TObject);
    procedure TPide_dSaiEntClick(Sender: TObject);
    procedure TPide_dSaiEntChange(Sender: TObject);
    procedure DBEdErrChange(Sender: TObject);
  private
    //
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD: Integer;
    FTipoEntradTitu: String;
    FPreparando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; IDCtrl: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure FormataCoresDBEdit(Sender: TObject);
    function  Reopen_I_N_O(): Boolean;
    procedure RecalculaExatidaoTotais();
    function  ImpedePeloEstoqueNF(): Boolean;
  public
    { Public declarations }
    FIDs_Txt: String;
    FLocIDCtrl: Integer;
    procedure IniciaForm(TipoEntradTitu: String; TipoEntradaDig, TipoEntradaNFe,
              TipoEntradaEFD: Integer);
    procedure LocCod(Atual, IDCtrl: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure ReopenNFeItsN(nItem: Integer);
    procedure ReopenNFeItsO(nItem: Integer);
    procedure ReopenNFeItsP(nItem: Integer);
    procedure ReopenNFeItsQ(nItem: Integer);
    procedure ReopenNFeItsR(nItem: Integer);
    procedure ReopenNFeItsS(nItem: Integer);
    procedure ReopenNFeItsT(nItem: Integer);
    //procedure ReopenNFeItsU(nItem: Integer);
    procedure ReopenNFeItsV(nItem: Integer);
    procedure ExcluiItem(FatID, FatNum, Empresa, OriCtrl, nItem: Integer; Pergunta:
              Boolean);
  end;

var
  FmEntradaCab: TFmEntradaCab;
const
  FFormatFloat = '000000000';

implementation

uses UnMyObjects, Module, UnGrade_Tabs, ModuleGeral,
{$IFNDef semNFe_v0000}
  NFeXMLGerencia, ModuleNFe_0000, NFeItsI_0000, NFeLoad_Dir, NFe_PF,
{$EndIf}
{$IFNDef sPQ}
  UnPQ_PF,
{$EndIf}
  UnXXe_PF, MyDBCheck, EntradaIts, EntradaPesq, GetValor, Principal, ModAppGraG1;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntradaCab.LocCod(Atual, IDCtrl: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, IDCtrl);
end;

procedure TFmEntradaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeCabAIDCtrl.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntradaCab.DefParams;
begin
  VAR_GOTOTABELA := 'nfecaba';
  VAR_GOTOMYSQLTABLE := QrNFeCabA;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'IDCtrl';
  VAR_GOTONOME := '';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,');
  VAR_SQLx.Add('emp.Filial, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.CNPJ, frn.CPF) CNPJ_CPF_FRN,');
  VAR_SQLx.Add('nfa.*');
  VAR_SQLx.Add('FROM nfecaba nfa');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=nfa.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=nfa.CodInfoEmit');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE nfa.IDCtrl=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
  VAR_GOTOVAR1 := '(FatID IN (' + FIDs_Txt + ')' + #13#10 +
                  'AND Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmEntradaCab.EdCodInfoEmitChange(Sender: TObject);
begin
  if EdCodInfoEmit.ValueVariant = 0 then
    DBEdCNPJ_CPF_TXT_Fornece.DataField := ''
  else
    DBEdCNPJ_CPF_TXT_Fornece.DataField := 'CNPJ_CPF_TXT';
end;

procedure TFmEntradaCab.EdCodInfoTrspChange(Sender: TObject);
begin
  if EdCodInfoTrsp.ValueVariant = 0 then
    DBEdCNPJ_CPF_TXT_Transporta.DataField := ''
  else
    DBEdCNPJ_CPF_TXT_Transporta.DataField := 'CNPJ_CPF_TXT';
end;

procedure TFmEntradaCab.EdModFreteChange(Sender: TObject);
begin
{$IFNDef semNFe_v0000}
  EdModFrete_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete, EdModFrete.ValueVariant);
{$EndIf}
end;

procedure TFmEntradaCab.EdNFe_IdExit(Sender: TObject);
begin
  XXe_PF.XXeDesmontaEPreencheObjectsChaveCTe(FPreparando, EdNFe_Id, Edide_mod,
  nil, EdNFG_Serie, Edide_nNF, EdCodInfoEmit, CBCodInfoEmit, TPide_dEmi);
(*
end;
const
  VersaoXXe = 0.00;
var
  Chave, UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer, xTpDoc: String;
  Entidade: Integer;
begin
  if FPreparando then
    Exit;
  Chave := EdNFe_Id.Text;
  if Length(Chave) = 44 then
  begin
    XXe_PF.DesmontaChaveDeAcesso(Chave, VersaoXXe,
    UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer);
    //
    Edide_Mod.ValueVariant := Geral.IMV(ModDoc);
    EdNFG_Serie.Text       := SerDoc;
    Edide_nNF.ValueVariant := Geral.IMV(NumDoc);
    if DModG.ObtemEntidadeDeCNPJCFP(CNPJ, Entidade) then
    begin
      EdCodInfoEmit.ValueVariant := Entidade;
      CBCodInfoEmit.KeyValue     := Entidade;
      TPide_dEmi.SetFocus;
    end else
    begin
      if Geral.MB_Pergunta('N�o existe cadastro de entidade para o CNPJ ' +
      Geral.FormataCNPJ_TT(CNPJ) + slineBreak + 'Deseja cadastr�-lo?') = ID_YES
      then
      begin
        Entidade := 0;
        VAR_CNPJ_A_CADASTRAR := CNPJ;
        DmodG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
      end;
      if DModG.ObtemEntidadeDeCNPJCFP(CNPJ, Entidade) then
      begin
        UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
        EdCodInfoEmit.ValueVariant := Entidade;
        CBCodInfoEmit.KeyValue     := Entidade;
        TPide_dEmi.SetFocus;
      end;
    end;
    //
    EdCodInfoEmit.SetFocus;
  end;
*)
end;

procedure TFmEntradaCab.EdRetTransp_CMunFGChange(Sender: TObject);
begin
{$IFNDef semNFe_v0000}
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', EdRetTransp_CMunFG.ValueVariant, []) then
    EdRetTransp_CMunFG_TXT.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    EdRetTransp_CMunFG_TXT.Text := '';
{$EndIf}    
end;

procedure TFmEntradaCab.EstoqueeFiscal1Click(Sender: TObject);
var
  Continua: Boolean;
begin
  Continua := False;
{
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
}

  if ImpedePeloEstoqueNF() then
    Exit;
  if not Reopen_I_N_O() then
    Exit;
  if DBCheck.CriaFm(TFmEntradaIts, FmEntradaIts, afmoNegarComAviso) then
  begin
    //
    FmEntradaIts.ImgTipo.SQLType              := stUpd;
    FmEntradaIts.FIDCtrl                     := QrStqMovItsIDCtrl.Value;
    FmEntradaIts.EdnItem.ValueVariant        := QrInItem.Value;
    FmEntradaIts.Edprod_cProd.Text           := QrIprod_cProd.Value;
    FmEntradaIts.CBGraGruX.KeyValue          := QrStqMovItsGraGruX.Value;
    //
    FmEntradaIts.FMeuID                      := QrIMeuID.Value;
    FmEntradaIts.FTipo                       := QrStqMovItsTipo.Value;
    FmEntradaIts.FOriCodi                    := QrStqMovItsOriCodi.Value;
    FmEntradaIts.FOriCtrl                    := QrStqMovItsOriCtrl.Value;
    FmEntradaIts.FMeuID                      := QrIMeuID.Value;
    //
    FmEntradaIts.FAnoMes                     := dmkPF.DataToAnoMes(QrNFeCabADataFiscal.Value);
    FmEntradaIts.ReopenTabelas();
    // Fazer primeiro para evitar erro!
    FmEntradaIts.RGBaixa.ItemIndex           := QrStqMovItsBaixa.Value;
    //
    FmEntradaIts.EdUnidMed.ValueVariant      := QrStqMovItsUnidMed.Value;
    FmEntradaIts.CBUnidMed.KeyValue          := QrStqMovItsUnidMed.Value;
    FmEntradaIts.EdGraGruX.ValueVariant      := QrStqMovItsGraGruX.Value;
    FmEntradaIts.EdPecas.ValueVariant        := QrStqMovItsPecas.Value;
    FmEntradaIts.EdPeso.ValueVariant         := QrStqMovItsPeso.Value;
    // fazer antes do m2 para n�o mudar o m2 original!
    FmEntradaIts.EdAreaP2.ValueVariant       := QrStqMovItsAreaP2.Value;
    FmEntradaIts.EdAreaM2.ValueVariant       := QrStqMovItsAreaM2.Value;
    // Deixar por �ltimo para s� mudar se alterar algum dado!
    FmEntradaIts.EdQtde.ValueVariant         := QrStqMovItsQtde.Value;
    //
    FmEntradaIts.Edprod_NCM.Text             := QrIprod_NCM.Value;
    FmEntradaIts.EdICMS_Orig.ValueVariant    := QrNICMS_Orig.Value;
    FmEntradaIts.EdICMS_CST.ValueVariant     := QrNICMS_CST.Value;
    FmEntradaIts.Edprod_CFOP.ValueVariant    := QrIprod_CFOP.Value;
    FmEntradaIts.Edprod_EXTIPI.ValueVariant  := QrIprod_EXTIPI.Value;
    FmEntradaIts.EdUnidMedCom.ValueVariant   := QrIUnidMedCom.Value;
    FmEntradaIts.Edprod_uCom.ValueVariant    := QrIprod_uCom.Value;
    FmEntradaIts.CBUnidMedCom.KeyValue       := QrIUnidMedCom.Value;
    FmEntradaIts.Edprod_qCom.ValueVariant    := QrIprod_qCom.Value;
    FmEntradaIts.EdUnidMedTrib.ValueVariant  := QrIUnidMedTrib.Value;
    FmEntradaIts.Edprod_uTrib.ValueVariant   := QrIprod_uTrib.Value;
    FmEntradaIts.CBUnidMedTrib.KeyValue      := QrIUnidMedTrib.Value;
    FmEntradaIts.Edprod_qTrib.ValueVariant   := QrIprod_qTrib.Value;
    FmEntradaIts.Edprod_vUnTrib.ValueVariant := QrIprod_vUnTrib.Value;
    FmEntradaIts.Edprod_vProd.ValueVariant   := QrIprod_vProd.Value;
    FmEntradaIts.EdICMS_vBC.ValueVariant     := QrNICMS_vBC.Value;
    FmEntradaIts.EdICMS_vICMS.ValueVariant   := QrNICMS_vICMS.Value;
    FmEntradaIts.EdIPI_vIPI.ValueVariant     := QrOIPI_vIPI.Value;
    FmEntradaIts.EdICMS_pICMS.ValueVariant   := QrNICMS_pICMS.Value;
    FmEntradaIts.EdIPI_pIPI.ValueVariant     := QrOIPI_pIPI.Value;
    FmEntradaIts.Edprod_vFrete.ValueVariant  := QrIprod_vFrete.Value;
    FmEntradaIts.Edprod_vSeg.ValueVariant    := QrIprod_vSeg.Value;
    FmEntradaIts.Edprod_vDesc.ValueVariant   := QrIprod_vDesc.Value;
    FmEntradaIts.Edprod_vOutro.ValueVariant  := QrIprod_vOutro.Value;
    //
    if QrOIND_APUR.Value <> '' then
    begin
      FmEntradaIts.RGIND_APUR.ItemIndex      := Geral.IMV(QrOInd_APUR.Value);
      FmEntradaIts.EdIPI_CST.Text            := Geral.FFF(QrOIPI_CST.Value, 2);
      FmEntradaIts.CBIPI_CST.KeyValue        := QrOIPI_CST.Value;
    end else
    begin
      FmEntradaIts.RGIND_APUR.ItemIndex      := 2; // Sem apuracao
      FmEntradaIts.EdIPI_CST.ValueVariant    := Null;
      FmEntradaIts.CBIPI_CST.KeyValue        := Null;
    end;
    //
    if QrNFeCabACriAForca.Value = 1 then
    begin
      if Trim(QrIprod_uCom.Value) = '' then
      begin
        FmEntradaIts.Edprod_uCom.ValueVariant  := QrStqMovItsSIGLA.Value;
        FmEntradaIts.EdUnidMedCom.ValueVariant := QrStqMovItsUnidMed.Value;
        FmEntradaIts.CBUnidMedCom.KeyValue     := QrStqMovItsUnidMed.Value;
      end;
      if Trim(QrIprod_uTrib.Value) = '' then
      begin
        FmEntradaIts.Edprod_uTrib.ValueVariant  := QrStqMovItsSIGLA.Value;
        FmEntradaIts.EdUnidMedTrib.ValueVariant := QrStqMovItsUnidMed.Value;
        FmEntradaIts.CBUnidMedTrib.KeyValue     := QrStqMovItsUnidMed.Value;
      end;
      //
      if Geral.MensagemBox(
      'Os dados fiscais desta Nota Fiscal foram gerados na janela de exporta��o' + #13#10 +
      'para o aplicativo Prosoft. Para poder editar os itens desta nota �' + #13#10 +
      'necess�rio torn�-la como n�o "criada � for�a". Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET CriAForca=0');
        Dmod.QrUpd.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', QrNFeCabAIDCtrl.Value));
        Dmod.QrUpd.ExecSQL;
        //
        Continua := True;
      end;
    end else Continua := True;
    if Continua then
      FmEntradaIts.ShowModal;
    FmEntradaIts.Destroy;
    //
    RecalculaExatidaoTotais();
  end;
end;

procedure TFmEntradaCab.EstoqueeFiscal2Click(Sender: TObject);
begin
  //if not Reopen_I_N_O() then
    //Exit;
  if QrStqMovIts.RecNo <> QrStqMovIts.RecordCount then
  begin
    Geral.MB_Aviso(
    'Somente o �ltimo item pode ser exclu�do! (nItem deve ser sequencial)');
    Exit;
  end;
  if ImpedePeloEstoqueNF() then
    Exit;
  ExcluiItem(QrStqMovItsTipo.Value, QrStqMovItsOriCodi.Value,
    QrStqMovItsEmpresa.Value, QrStqMovItsOriCtrl.Value,
    //QrInItem.Value, True);
    QrStqMovItsOriCtrl.Value, True);
  RecalculaExatidaoTotais();
end;

procedure TFmEntradaCab.ExcluiItem(FatID, FatNum, Empresa, OriCtrl, nItem: Integer;
  Pergunta: Boolean);
var
  Exclui: Boolean;
begin
  if Pergunta then
    Exclui := Geral.MB_Pergunta('Confirma a exclus�o do item ' +
    Geral.FF0(nItem) + '?') = ID_YES
  else
    Exclui := True;
  if Exclui then
  begin
    DmNFe_0000.ExcluiEntradaItsDeNFe(FatID, FatNum, Empresa, nItem, OriCtrl);
    if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
      DfModAppGraG1.InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa, nItem);
    ReopenStqMovIts(0);
  end;
end;

procedure TFmEntradaCab.Manualmente1Click(Sender: TObject);
begin
  FPreparando := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeCabA, [PainelDados],
  [PainelEdita], EdEmpresa, ImgTipo, 'nfecaba');
  FPreparando := False;
end;

procedure TFmEntradaCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; IDCtrl: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdIDCtrl.Text := FormatFloat(FFormatFloat, IDCtrl);
        //...
      end else begin
        EdIDCtrl.Text := DBEdIDCtrl.Text;
        //...
      end;
      EdEmpresa.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntradaCab.PMIncluiPopup(Sender: TObject);
begin
  AlteraNF1.Enabled :=
    (QrNFeCabA.State <> dsInactive) and (QrNFeCabA.RecordCount > 0);
end;

procedure TFmEntradaCab.PMItemPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (PageControl1.ActivePageIndex = 0) and
    (QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount > 0);
  EstoqueeFiscal1.Enabled := Habilita;
  EstoqueeFiscal2.Enabled := Habilita;
end;

procedure TFmEntradaCab.porXML1Click(Sender: TObject);
begin
{$IFNDef semNFe_v0000}
  if MyObjects.CriaForm_AcessoTotal(TFmNFeLoad_Dir, FmNFeLoad_Dir) then
  begin
    FmNFeLoad_Dir.ShowModal;
    FmNFeLoad_Dir.Destroy;
  end;
{$EndIf}
end;

procedure TFmEntradaCab.Cdigo1Click(Sender: TObject);
var
  FatNum: Variant;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Entrada de Uso e Consumo', 'Informe o c�digo do lan�amento: ',
  0, FatNum) then
  begin
    if Integer(FatNum) <> 0 then
    begin
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IDCtrl FROM nfecaba');
      Dmod.QrAux.SQL.Add('WHERE ' + VAR_GOTOVAR1);
      Dmod.QrAux.SQL.Add('AND FatNum=' + FormatFloat('0', FatNum));
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      //
      LocCod(0, Dmod.QrAux.FieldByName('IDCtrl').AsInteger);
    end;
  end;
end;

procedure TFmEntradaCab.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmEntradaCab.QueryPrincipalAfterOpen;
begin
end;

function TFmEntradaCab.Reopen_I_N_O(): Boolean;
var
  FatID, FatNum, Empresa, OriCtrl: String;
begin
(*
  QrI.Close;
  QrI.SQL.Clear;
  QrI.SQL.Add('SELECT *');
  QrI.SQL.Add('FROM nfeitsi');
  QrI.SQL.Add('WHERE FatID=:P0');
  QrI.SQL.Add('AND FatNum=:P1');
  QrI.SQL.Add('AND Empresa=:P2');

  if QrNFeCabACriAForca.Value = 1 then
  begin
    QrI.SQL.Add('AND MeuID=:P3');
    QrI.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  end else begin
    QrI.SQL.Add('AND nItem=:P3');
    QrI.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
  end;
    QrI.SQL.Add('AND (MeuID=:P3');
    QrI.Params[03].AsInteger := QrStqMovItsOriCtrl.Value;
    QrI.SQL.Add('OR nItem=:P4)');
    QrI.Params[04].AsInteger := QrStqMovItsOriCtrl.Value;
  //
  QrI.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrI.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrI.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  FatID := FormatFloat('0', QrNFeCabAFatID.Value);
  FatNum := FormatFloat('0', QrNFeCabAFatNum.Value);
  Empresa := FormatFloat('0', QrNFeCabAEmpresa.Value);
  OriCtrl := FormatFloat('0', QrStqMovItsOriCtrl.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrI, Dmod.MyDB, [
  'SELECT *',
  'FROM nfeitsi',
  'WHERE FatID=' + FatID,
  'AND FatNum=' + FatNum,
  'AND Empresa=' + EMpresa,
  'AND (MeuID=' + OriCtrl,
  'OR nItem=' + OriCtrl + ')',
  '']);
  //
  QrN.Close;
  QrN.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrN.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrN.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrN.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrO.Close;
  QrO.Params[00].AsInteger := QrNFeCabAFatID.Value;
  QrO.Params[01].AsInteger := QrNFeCabAFatNum.Value;
  QrO.Params[02].AsInteger := QrNFeCabAEmpresa.Value;
  QrO.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  Result := QrI.RecordCount = 1;
  if not Result then
  begin
    if QrI.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi poss�vel localizar os dados fiscais do item!'
      + #13#10 + 'Avise a DERMATEK!')
    else
      Geral.MB_Info('Foram localizados ' + Geral.FF0(QrI.RecordCount) +
      ' registros de dados fiscais para o item selecionado!'
      + #13#10 + 'A altera��o ser� abortada!' + #13#10 + 'Avise a DERMATEK!');
  end;
end;

procedure TFmEntradaCab.RecalculaExatidaoTotais();
var
  Filial: Integer;
  TabLctA: String;
begin
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    Filial := DModG.ObtemFilialDeEntidade(QrNFeCabAEmpresa.Value);
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    DfModAppGraG1.InsAltUsoConsumoCalculaDiferencas(apNinguem, QrUsoConsCodigo.Value, TabLctA);
    DfModAppGraG1.InsAltUsoConsumoFinalizaEntrada(True, stIns, QrUsoConsCodigo.Value);
    LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value);
  end;
end;

procedure TFmEntradaCab.ReopenNFeItsN(nItem: Integer);
begin
  QrNFeItsN.Close;
  QrNFeItsN.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsN.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsN.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsN.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsO(nItem: Integer);
begin
  QrNFeItsO.Close;
  QrNFeItsO.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsO.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsO.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsO.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsP(nItem: Integer);
begin
  QrNFeItsP.Close;
  QrNFeItsP.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsP.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsP.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsP.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsP, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsQ(nItem: Integer);
begin
  QrNFeItsQ.Close;
  QrNFeItsQ.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsQ.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsQ.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsQ.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsQ, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsR(nItem: Integer);
begin
  QrNFeItsR.Close;
  QrNFeItsR.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsR.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsR.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsR.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsR, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsS(nItem: Integer);
begin
  QrNFeItsS.Close;
  QrNFeItsS.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsS.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsS.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsS.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsS, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsT(nItem: Integer);
begin
  QrNFeItsT.Close;
  QrNFeItsT.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsT.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsT.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsT.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsT, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenNFeItsV(nItem: Integer);
begin
  QrNFeItsV.Close;
  QrNFeItsV.Params[00].AsInteger := QrNFeItsIFatID.Value;
  QrNFeItsV.Params[01].AsInteger := QrNFeItsIFatNum.Value;
  QrNFeItsV.Params[02].AsInteger := QrNFeItsIEmpresa.Value;
  QrNFeItsV.Params[03].AsInteger := QrNFeItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeItsV, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmEntradaCab.ReopenStqMovIts(IDCtrl: Integer);
  procedure ComplementaSQL(Tab, FatID, FatNum: String);
  begin
    QrStqMovIts.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrStqMovIts.SQL.Add('pgt.Nome NO_PGT, IF(smia.UnidMed=-1,');
    QrStqMovIts.SQL.Add('mec.Sigla, mei.Sigla) SIGLA,');
    QrStqMovIts.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrStqMovIts.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*');
    QrStqMovIts.SQL.Add('FROM stqmovitsa smia');
    QrStqMovIts.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrStqMovIts.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrStqMovIts.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrStqMovIts.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrStqMovIts.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrStqMovIts.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrStqMovIts.SQL.Add('LEFT JOIN unidmed    mec ON mec.Codigo=gg1.UnidMed');
    QrStqMovIts.SQL.Add('LEFT JOIN unidmed    mei ON mei.Codigo=smia.UnidMed');
    QrStqMovIts.SQL.Add('');
    QrStqMovIts.SQL.Add('WHERE smia.Tipo=' + FatID);
    QrStqMovIts.SQL.Add('AND smia.OriCodi=' + FatNum);
    //QrStqMovIts.SQL.Add('AND smia.OriCtrl=' + Ctrl);
  end;
var
  FatID, FatNum: String;
begin
  FatID  := FormatFloat('0', QrNFeCabAFatID.Value);
  FatNum := FormatFloat('0', QrNFeCabAFatNum.Value);
  //Empresa := FormatFloat('0', QrNFeCabAEmpresa.Value);
  //
  QrStqMovIts.Close;
  QrStqMovIts.SQL.Clear;
  ComplementaSQL('a', FatID, FatNum);
  QrStqMovIts.SQL.Add('');
  QrStqMovIts.SQL.Add('UNION');
  QrStqMovIts.SQL.Add('');
  ComplementaSQL('b', FatID, FatNum);
  QrStqMovIts.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrStqMovIts, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  QrStqMovIts.Locate('IDCtrl', IDCtrl, []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfeitsi ',
  'WHERE FatID=' + FatID,
  'AND FatNum=' + FatNum,
  //'AND Empresa=' + Empresa,
  '']);
end;

procedure TFmEntradaCab.DBEdErrChange(Sender: TObject);
begin
  FormataCoresDBEdit(Sender);
end;

procedure TFmEntradaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntradaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntradaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntradaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntradaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntradaCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := EdCodInfoEmit.ValueVariant;
  DModG.CadastroDeEntidade(VAR_CADASTRO, fmcadSelecionar, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCodInfoEmit, CBCodInfoEmit, QrFornece, VAR_CADASTRO);
end;

procedure TFmEntradaCab.TPide_dSaiEntChange(Sender: TObject);
begin
  TPDataFiscal.Date := TPide_dSaiEnt.Date;
end;

procedure TFmEntradaCab.TPide_dSaiEntClick(Sender: TObject);
begin
  TPDataFiscal.Date := TPide_dSaiEnt.Date;
end;

procedure TFmEntradaCab.AlteraNF1Click(Sender: TObject);
begin
  if QrNFeCabAFatID.Value = FTipoEntradaDig then
  begin
    FPreparando := True;
    //
    if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
    begin
      GBOutros.Visible := True;
      PnBlDrm.Visible := True;
      //
      DfModAppGraG1.PreparaUpdUsoConsumoCab(
      QrNFeCabAFatID.Value, QrNFeCabAFatNum.Value, QrNFeCabAEmpresa.Value,
      EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB);
    end;
    //
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeCabA, [PainelDados],
    [PainelEdita], EdEmpresa, ImgTipo, 'nfecaba');
    FPreparando := False;
  end else
  begin
    Geral.MensagemBox('Notas fiscais importadas de XML ou SPED n�o podem ser editadas!'
    + sLineBreak + 'Solicite implementa��o � DERMATEK!',
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmEntradaCab.BtSaidaClick(Sender: TObject);
begin
 // ver o que fazer quando precisar
//  VAR_CADASTRO := QrNFeCabACodigo.Value;
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmEntradaCab.BtConfirmaClick(Sender: TObject);
const
  UpdCodigo = 0;
  Status = 100;
  CriAForca = 0;
  Pedido = 0;
  TipoNF = 1;
  HowLoad = Integer(pqehlEntradaCab);
var
  FatNum, IDCtrl, Filial, Empresa: Integer;
  Id, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt, NFG_Serie: String;
  ide_mod, ide_serie, ide_nNF, ide_tpNF: Integer;
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF: Double;
  ModFrete, RetTransp_CMunFG: Integer;
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet: Double;
  RetTransp_CFOP, DataFiscal: String;
  CodInfoEmit, CodInfoDest, CodInfoTrsp, CodInfoCliI, FatID: Integer;
(*
  ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
  IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC, COFINSRec_pAliq,
  COFINSRec_vCOFINS, ICMSRec_vBCST, ICMSRec_pAliqST, ICMSRec_vICMSST: Double;
*)
  //
  NF_RP, NF_CC, Conhecimento: Integer;
  Frete, PesoB, PesoL: Double;
begin
  ide_mod := 0;
  if ImgTipo.SQLType = stIns then
    FatID := FTipoEntradaDig
  else
    FatID := QrNFeCabAFatID.Value;
  //
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a Empresa!') then
    Exit;
  Empresa := DModG.QrEmpresasCodigo.Value;
  //

  Id := EdNFe_Id.Text;
{$IFNDef semNFe_v0000}
  if not NFeXMLGeren.VerificaChaveAcesso(Id, True) then
    Exit;
  if not NFeXMLGeren.NFG_SerieToide_serie(ide_mod, NFG_Serie, ide_Serie) then
    Exit;
{$EndIf}
  ide_dEmi           := Geral.FDT(TPide_dEmi.Date, 1);
  ide_dSaiEnt        := Geral.FDT(TPide_dSaiEnt.Date, 1);
  ide_hSaiEnt        := Edide_hSaiEnt.Text;
  ide_mod            := Edide_mod.ValueVariant;
  NFG_Serie          := EdNFG_Serie.Text;
  ide_nNF            := Edide_nNF.ValueVariant;
  ide_tpNF           := RGide_tpNF.ItemIndex;
  //
  ICMSTot_vBC        := EdICMSTot_vBC    .ValueVariant;
  ICMSTot_vICMS      := EdICMSTot_vICMS  .ValueVariant;
  ICMSTot_vBCST      := EdICMSTot_vBCST  .ValueVariant;
  ICMSTot_vST        := EdICMSTot_vST    .ValueVariant;
  ICMSTot_vProd      := EdICMSTot_vProd  .ValueVariant;
  ICMSTot_vFrete     := EdICMSTot_vFrete .ValueVariant;
  ICMSTot_vSeg       := EdICMSTot_vSeg   .ValueVariant;
  ICMSTot_vDesc      := EdICMSTot_vDesc  .ValueVariant;
  ICMSTot_vII        := EdICMSTot_vII    .ValueVariant;
  ICMSTot_vIPI       := EdICMSTot_vIPI   .ValueVariant;
  ICMSTot_vPIS       := EdICMSTot_vPIS   .ValueVariant;
  ICMSTot_vCOFINS    := EdICMSTot_vCOFINS.ValueVariant;
  ICMSTot_vOutro     := EdICMSTot_vOutro .ValueVariant;
  ICMSTot_vNF        := EdICMSTot_vNF    .ValueVariant;
  //
  ModFrete           := EdModFrete.ValueVariant;
  //
  RetTransp_vServ    := EdRetTransp_vServ   .ValueVariant;
  RetTransp_vBCRet   := EdRetTransp_vBCRet  .ValueVariant;
  RetTransp_PICMSRet := EdRetTransp_PICMSRet.ValueVariant;
  RetTransp_vICMSRet := EdRetTransp_vICMSRet.ValueVariant;
  RetTransp_CFOP     := Geral.SoNumero_TT(EdRetTransp_CFOP.ValueVariant);
  RetTransp_CMunFG   := EdRetTransp_CMunFG  .ValueVariant;
  //
(*
  ICMSRec_pRedBC :=     EdICMSRec_pRedBC.ValueVariant;
  ICMSRec_vBC :=        EdICMSRec_vBC.ValueVariant;
  ICMSRec_pAliq :=      EdICMSRec_pAliq.ValueVariant;
  ICMSRec_vICMS :=      EdICMSRec_vICMS.ValueVariant;
  IPIRec_pRedBC :=      EdIPIRec_pRedBC.ValueVariant;
  IPIRec_vBC :=         EdIPIRec_vBC.ValueVariant;
  IPIRec_pAliq :=       EdIPIRec_pAliq.ValueVariant;
  IPIRec_vIPI :=        EdIPIRec_vIPI.ValueVariant;
  PISRec_pRedBC :=      EdPISRec_pRedBC.ValueVariant;
  PISRec_vBC :=         EdPISRec_vBC.ValueVariant;
  PISRec_pAliq :=       EdPISRec_pAliq.ValueVariant;
  PISRec_vPIS :=        EdPISRec_vPIS.ValueVariant;
  COFINSRec_pRedBC :=   EdCOFINSRec_pRedBC.ValueVariant;
  COFINSRec_vBC :=      EdCOFINSRec_vBC.ValueVariant;
  COFINSRec_pAliq :=    EdCOFINSRec_pAliq.ValueVariant;
  COFINSRec_vCOFINS :=  EdCOFINSRec_vCOFINS.ValueVariant;
  ICMSRec_vBCST :=      EdICMSRec_vBCST.ValueVariant;
  ICMSRec_pAliqST :=    EdICMSRec_pAliqST.ValueVariant;
  ICMSRec_vICMSST :=    EdICMSRec_vICMSST.ValueVariant;
*)
  DataFiscal         := Geral.FDT(TPDataFiscal.Date, 1);
  CodInfoDest        := Empresa;
  CodInfoEmit        := EdCodInfoEmit.ValueVariant;
  CodInfoCliI        := EdCodInfoCliI.ValueVariant;
  CodInfoTrsp        := EdCodInfoTrsp.ValueVariant;
  //
  if MyObjects.FIC(TPide_dEmi.Date < 2, TPide_dEmi, 'Informe a data de emiss�o!') then
    Exit;
  if MyObjects.FIC(TPide_dEmi.Date < 2, TPide_dEmi, 'Informe a data de entrada!') then
    Exit;
  if MyObjects.FIC(TPDataFiscal.Date < 2, TPide_dEmi, 'Data fiscal inv�lida!') then
    Exit;
  if MyObjects.FIC(CodInfoTrsp = 0, EdCodInfoTrsp, 'Informe a transportadora!') then
    Exit;
  if MyObjects.FIC(CodInfoCliI = 0, EdCodInfoCliI, 'Informe o Cliente interno dono do material!') then
    Exit;
  //
  //
  IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);
  FatNum := EdFatNum.ValueVariant;
  FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', ImgTipo.SQLType, FatNum, nil, True);
  //
{
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmEntradaCab, PainelEdit,
    'nfecaba', IDCtrl, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(IDCtrl, IDCtrl);
  end;
}
  //
  Conhecimento := EdConhecimento.ValueVariant;
  NF_RP := EdNF_RP.ValueVariant;
  NF_CC := EdNF_CC.ValueVariant;
  Frete := EdFrete.ValueVariant;
  PesoB := EdPesoB.ValueVariant;
  PesoL := EdPesoL.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecaba', False, [
  'IDCtrl', (*'LoteEnv', 'versao',*)
  'Id', (*'ide_cUF', 'ide_cNF',
  'ide_natOp', 'ide_indPag',*) 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
  (*'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
  'ide_cDV', 'ide_tpAmb', 'ide_finNFe',
  'ide_procEmi', 'ide_verProc', 'ide_dhCont',
  'ide_xJust', 'emit_CNPJ', 'emit_CPF',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone', 'emit_IE', 'emit_IEST',
  'emit_IM', 'emit_CNAE', 'emit_CRT',
  'dest_CNPJ', 'dest_CPF', 'dest_xNome',
  'dest_xLgr', 'dest_nro', 'dest_xCpl',
  'dest_xBairro', 'dest_cMun', 'dest_xMun',
  'dest_UF', 'dest_CEP', 'dest_cPais',
  'dest_xPais', 'dest_fone', 'dest_IE',
  'dest_ISUF', 'dest_email',*) 'ICMSTot_vBC',
  'ICMSTot_vICMS', 'ICMSTot_vBCST', 'ICMSTot_vST',
  'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
  'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
  'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
  'ICMSTot_vNF', (*'ISSQNtot_vServ', 'ISSQNtot_vBC',
  'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
  'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
  'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
  'RetTrib_vRetPrev',*) 'ModFrete', (*'Transporta_CNPJ',
  'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
  'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',*)
  'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
  'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
  (*'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
  'Vagao', 'Balsa', 'Cobr_Fat_nFat',
  'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
  'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
  'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
  'Compra_XCont',*) 'Status', (*'protNFe_versao',
  'infProt_Id', 'infProt_tpAmb', 'infProt_verAplic',
  'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', 'retCancNFe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', '_Ativo_', 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra',*) (*'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
  'DataFiscal', (*'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
  'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
  'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
  'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
  'CodInfoDest', 'CriAForca', 'CodInfoTrsp',
  (*'OrdemServ', 'Situacao', 'Antigo',*)
  'NFG_Serie'(*,
  'ICMSRec_vBCST', 'ICMSRec_vICMSST', 'ICMSRec_pAliqST'*),
  'CodInfoCliI'], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, (*LoteEnv, versao,*)
  Id, (*ide_cUF, ide_cNF,
  ide_natOp, ide_indPag,*) ide_mod,
  ide_serie, ide_nNF, ide_dEmi,
  ide_dSaiEnt, ide_hSaiEnt, ide_tpNF,
  (*ide_cMunFG, ide_tpImp, ide_tpEmis,
  ide_cDV, ide_tpAmb, ide_finNFe,
  ide_procEmi, ide_verProc, ide_dhCont,
  ide_xJust, emit_CNPJ, emit_CPF,
  emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro,
  emit_cMun, emit_xMun, emit_UF,
  emit_CEP, emit_cPais, emit_xPais,
  emit_fone, emit_IE, emit_IEST,
  emit_IM, emit_CNAE, emit_CRT,
  dest_CNPJ, dest_CPF, dest_xNome,
  dest_xLgr, dest_nro, dest_xCpl,
  dest_xBairro, dest_cMun, dest_xMun,
  dest_UF, dest_CEP, dest_cPais,
  dest_xPais, dest_fone, dest_IE,
  dest_ISUF, dest_email,*) ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST,
  ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
  ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro,
  ICMSTot_vNF, (*ISSQNtot_vServ, ISSQNtot_vBC,
  ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
  RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
  RetTrib_vRetPrev,*) ModFrete, (*Transporta_CNPJ,
  Transporta_CPF, Transporta_XNome, Transporta_IE,
  Transporta_XEnder, Transporta_XMun, Transporta_UF,*)
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet,
  RetTransp_vICMSRet, RetTransp_CFOP, RetTransp_CMunFG,
  (*VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Vagao, Balsa, Cobr_Fat_nFat,
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq,
  InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq,
  Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed,
  Compra_XCont,*) Status, (*protNFe_versao,
  infProt_Id, infProt_tpAmb, infProt_verAplic,
  infProt_dhRecbto, infProt_nProt, infProt_digVal,
  infProt_cStat, infProt_xMotivo, retCancNFe_versao,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
  infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
  infCanc_xJust, _Ativo_, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra,*) (*ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
  DataFiscal, (*SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
  SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
  SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
  SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) CodInfoEmit,
  CodInfoDest, CriAForca, CodInfoTrsp,
  (*OrdemServ, Situacao, Antigo,*)
  NFG_Serie(*,
  ICMSRec_vBCST, ICMSRec_vICMSST, ICMSRec_pAliqST*),
  CodInfoCliI], [
  FatID, FatNum, Empresa], True) then
  begin
    case TdmkAppID(CO_DMKID_APP) of
      dmkappB_L_U_E_D_E_R_M:
      begin
        DfModAppGraG1.InsAltUsoConsumoCab(ide_dEmi, ide_dSaiEnt, ID,
        UpdCodigo, CodInfoEmit, CodInfoCliI, CodInfoTrsp, ide_nNF,
        Conhecimento, Pedido, TipoNF, ide_mod, ide_Serie, NF_RP, NF_CC,
        HowLoad, Frete, PesoB, PesoL, ICMSTot_vNF, ICMSTot_vICMS,
        ImgTipo.SQLType, FatID, FatNum, Empresa);
      end;
    end;
    if ImgTipo.SQLType = stUpd then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET');
      Dmod.QrUpd.SQL.Add('DataHora=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P1');
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P2');
      Dmod.QrUpd.Params[00].AsString  := DataFiscal;
      Dmod.QrUpd.Params[01].AsInteger := FatID;
      Dmod.QrUpd.Params[02].AsInteger := FatNum;
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.ExecSQL;
      {
      QrStqMovIts.First;
      while not QrStqMovIts.Eof do
      begin

        // N�o usar pois tem o IDCtrl do NFeCabA
        //IDCtrl := QrStqMovItsIDCtrl.Value;
        //Fazer assim
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
        'DataHora'], ['IDCtrl'], [
        DataFiscal], [QrStqMovItsIDCtrl.Value], False);
        //
        QrStqMovIts.Next;
      end;
      }
    end;
    LocCod(IDCtrl, IDCtrl);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmEntradaCab.BtDesisteClick(Sender: TObject);
var
  IDCtrl : Integer;
begin
  IDCtrl := Geral.IMV(EdIDCtrl.Text);
{
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'nfecaba', IDCtrl);
}
  UMyMod.UpdUnlockY(IDCtrl, Dmod.MyDB, 'nfecaba', 'IDCtrl');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmEntradaCab.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmEntradaCab.BtItemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItem, BtItem);
end;

procedure TFmEntradaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPreparando := True;
  //
  PainelEdit.Align  := alClient;
  PageControl1.Align    := alClient;
  PageControl1.ActivePageIndex := 0;
  //
  CriaOForm;
  //
  QrNFMoDocFis.Close;
  QrNFMoDocFis.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreQuery(QrNFMoDocFis, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrFornece.Close;
  QrFornece.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrTransporta.Close;
  QrTransporta.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  {$IFNDef semNFe_v0000}
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_Munici, DModG.AllID_DB);
  {$EndIf}
  UMyMod.AbreQuery(QrCI, Dmod.MyDB);
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmEntradaCab.SbNumeroClick(Sender: TObject);
begin
  //N�o funcionam, Parei Aqui!
  MyObjects.MostraPopUpDeBotao(PMNumero, SbNumero);
end;

procedure TFmEntradaCab.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmEntradaCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntradaCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNFeCabACodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEntradaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntradaCab.QrForneceCalcFields(DataSet: TDataSet);
begin
  QrForneceCNPJ_CPF_TXT.Value :=
  Geral.FormataCNPJ_TT(QrForneceCNPJ_CPF.Value);
end;

procedure TFmEntradaCab.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntradaCab.QrNFeCabAAfterScroll(DataSet: TDataSet);
var
  Cor: TColor;
  Tabela: String;
begin
  ReopenStqMovIts(0);
  case QrNFeCabAFatID.Value of
    VAR_FATID_0051: Cor := clGreen;
    VAR_FATID_0151: Cor := clRed;
    VAR_FATID_0251: Cor := clBlue;
    else Cor := clFuchsia;
  end;
  //
  DBEdFatID.Font.Color := Cor;
  DBEdNO_FATID.Font.Color := Cor;
  //
  case TdmkAppID(CO_DMKID_APP) of
    dmkappB_L_U_E_D_E_R_M: Tabela := 'pqe';
    else Tabela := '';
  end;
  if Tabela <> '' then
  begin
    UnDmkDAC_PF.AbremySQLQuery0(QrUsoCons, Dmod.MyDB, [
    'SELECT Codigo, ErrNota, ErrBruto, ErrLiq, ErrPagT, ErrPagF ',
    'FROM ' + Tabela,
    'WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
    '']);
  end;
end;

procedure TFmEntradaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if Trim(FIDs_Txt) = '' then
  begin
    Geral.MensagemBox('Janela n�o ser� criada! Tipo de entrada n�o foi definida!',
    'ERRO', MB_OK+MB_ICONERROR);
    Close;
  end;
end;

procedure TFmEntradaCab.FormataCoresDBEdit(Sender: TObject);
var
  Valor: Double;
begin
  if TDBEdit(Sender).Text = '' then
    Valor := 0
  else
    Valor := Geral.DMV(TDBEdit(Sender).Text);
  if Valor = 0 then
  begin
    TDBEdit(Sender).Color := clWindow;
    TDBEdit(Sender).Font.Color := clWindowText;
    TDBEdit(Sender).Font.Style := TDBEdit(Sender).Font.Style - [fsBold];
  end else
  begin
    TDBEdit(Sender).Color := clYellow;
    TDBEdit(Sender).Font.Color := clRed;
    TDBEdit(Sender).Font.Style := TDBEdit(Sender).Font.Style + [fsBold];
  end;
end;

procedure TFmEntradaCab.SbQueryClick(Sender: TObject);
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmEntradaPesq, FmEntradaPesq);
    FmEntradaPesq.ShowModal;
    FmEntradaPesq.Destroy;
  finally
    Screen.Cursor := Cursor;
  end;
  if VAR_PQELANCTO <> -1 then LocCod(VAR_PQELANCTO, VAR_PQELANCTO);
end;

procedure TFmEntradaCab.SomenteFiscal1Click(Sender: TObject);
begin
(*
  if FSoPermiteAlterarPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
*)
{$IFNDef semNFe_v0000}
  if UmyMod.FormInsUpd_Mul_Cria(TFmNFeItsI_0000, FmNFeItsI_0000, afmoNegarComAviso,
    [QrNFeItsI, QrNFeItsN, QrNFeItsO, QrNFeItsP, QrNFeItsQ, QrNFeItsR,
    QrNFeItsS, QrNFeItsT, QrNFeItsV], stUpd) then
  begin
    FmNFeItsI_0000.FForm := 'FmEntradaCab';
    FmNFeItsI_0000.ShowModal;
    FmNFeItsI_0000.Destroy;
  end;
{$EndIf}
end;

procedure TFmEntradaCab.FormResize(Sender: TObject);
var
  Titulo: String;
begin
  //12345678901234567890123456789012345678901
  //                              Entrada de ???
  Titulo := Copy(LaTitulo1C.Caption, 1, 41) + FTipoEntradTitu;
  LaTitulo1C.Caption := Titulo;
  // 1234567890123456789012345678
  // INN-GERAL-001 :: Entrada de ???
  Titulo := Copy(Caption, 1, 28) + FTipoEntradTitu;
  Caption := Titulo;
  //
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Titulo, True, taCenter, 2, 10, 20);
end;

procedure TFmEntradaCab.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeCabAIDCtrl.Value, LaRegistro.Caption);
end;

function TFmEntradaCab.ImpedePeloEstoqueNF(): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle ',
    'FROM pqeits',
    'WHERE FatID=' + Geral.FF0(QrNFeCabAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrNFeCabAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrNFeCabAEmpresa.Value),
    'AND nItem=' + Geral.FF0(QrStqMovItsOriCtrl.Value),
    '']);
    if Qry.RecordCount = 1 then
    begin
      {$IFNDef sPQ}
      Result := PQ_PF.ImpedePeloEstoqueNF_Item(
        Qry.FieldByName('Codigo').AsInteger,
        Qry.FieldByName('Controle').AsInteger, VAR_FATID_0010);
      {$Else}
      Result := True;
      {$EndIf}
    end else
    begin
      Geral.MB_ERRO('Itens difere de um em "ImpedePeloEstoqueNF()"');
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmEntradaCab.Incluinovoitem1Click(Sender: TObject);
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
{
  if FSoPermiteIncluirPelaSenhaAdmin then
    if not DBCheck.LiberaPelaSenhaAdmin then Exit;
}
  if DBCheck.CriaFm(TFmEntradaIts, FmEntradaIts, afmoNegarComAviso) then
  begin
    FmEntradaIts.ImgTipo.SQLType := stIns;
    FatID   := QrNFeCabAFatId.Value;
    FatNum  := QrNFeCabAFatNum.Value;
    Empresa := QrNFeCabAEmpresa.Value;
    nItem := DModG.BuscaProximoInteiro('nfeitsI', 'nItem', 'FatID', FatID,
    'FatNum', FatNum, 'Empresa', Empresa);
    FmEntradaIts.EdnItem.ValueVariant := nItem;
    //
    FmEntradaIts.FAnoMes                     := dmkPF.DataToAnoMes(QrNFeCabADataFiscal.Value);
    FmEntradaIts.ReopenTabelas();
    //
    {
    FmEntradaIts.FForm                   := 'NFeCabA_0000';
    FmEntradaIts.Fdest_CNPJ              := QrNFeCabAdest_CNPJ.Value;
    FmEntradaIts.Fdest_CPF               := QrNFeCabAdest_CPF.Value;
    FmEntradaIts.FEmpresa                := QrNFeCabAEmpresa.Value;
    FmEntradaIts.FFisRegCad              := QrNFeCabAFisRegCad.Value;
    FmEntradaIts.FTabelaPrc              := QrNFeCabATabelaPrc.Value;
    FmEntradaIts.FCondicaoPg             := QrNFeCabACondicaoPG.Value;
    FmEntradaIts.FMedDDSimpl             := QrNFeCabAMedDDSimpl.Value;
    FmEntradaIts.FMedDDReal              := QrNFeCabAMedDDReal.Value;
    }
    //
    FmEntradaIts.ShowModal;
    FmEntradaIts.Destroy;
    //
    RecalculaExatidaoTotais();
  end;
end;

procedure TFmEntradaCab.IniciaForm(TipoEntradTitu: String; TipoEntradaDig,
  TipoEntradaNFe, TipoEntradaEFD: Integer);
begin
  FIDs_Txt := '';
  // Copiar valores para form para n�o dar conflito caso o usu�rio abra mais de
  // uma inst�ncia da janela. Acho que funciona.
  FTipoEntradaDig := TipoEntradaDig;
  FTipoEntradaNFe := TipoEntradaNFe;
  FTipoEntradaEFD := TipoEntradaEFD;
  //
  FTipoEntradTitu := TipoEntradTitu;
  // Fim Copiar

  //
  if FTipoEntradaDig <> 0 then
    FIDs_Txt := FIDs_Txt + FormatFloat('0', FTipoEntradaDig) + ', ';
  if FTipoEntradaNFe <> 0 then
    FIDs_Txt := FIDs_Txt + FormatFloat('0', FTipoEntradaNFe) + ', ';
  if FTipoEntradaEFD <> 0 then
    FIDs_Txt := FIDs_Txt + FormatFloat('0', FTipoEntradaEFD) + ', ';
  if Length(FIDs_Txt) > 2 then
    FIDs_Txt := Copy(FIDs_Txt, 1, Length(FIDs_Txt) - 2);
  //
  Va(vpLast);
end;

procedure TFmEntradaCab.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
  QrNFeItsI.Close;
  //
  QrUsoCons.Close;
  //
  QrI.Close;
  QrN.Close;
  QrO.Close;
end;

procedure TFmEntradaCab.QrNFeCabABeforeOpen(DataSet: TDataSet);
begin
  QrNFeCabAIDCtrl.DisplayFormat := FFormatFloat;
end;

procedure TFmEntradaCab.QrNFeCabACalcFields(DataSet: TDataSet);
begin
  QrNFeCabACNPJ_CPF_FRN_TXT.Value :=
    Geral.FormataCNPJ_TT(QrNFeCabACNPJ_CPF_FRN.Value);
  //
  if QrNFeCabAide_serie.Value > 0 then
    QrNFeCabAMODELO_TXT.Value := FormatFloat('00', QrNFeCabAide_serie.Value)
  else
    QrNFeCabAMODELO_TXT.Value := QrNFeCabACOD_MOD.Value;
  //
  if QrNFeCabAFatID.Value = FTipoEntradaNFe then
    QrNFeCabANO_FATID.Value := 'Importa��o NFe'
  else
  if QrNFeCabAFatID.Value = FTipoEntradaDig then
    QrNFeCabANO_FATID.Value := 'Digitado Manual'
  else
  if QrNFeCabAFatID.Value = FTipoEntradaEFD then
    QrNFeCabANO_FATID.Value := 'Importa��o SPED-EFD'
  else
    QrNFeCabANO_FATID.Value := '? ? ? ? ?';
end;

procedure TFmEntradaCab.QrNFeItsIAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeItsN(0);
  ReopenNFeItsO(0);
  ReopenNFeItsP(0);
  ReopenNFeItsQ(0);
  ReopenNFeItsR(0);
  ReopenNFeItsS(0);
  ReopenNFeItsT(0);
  //ReopenNFeItsU(0);
  ReopenNFeItsV(0);
  //ReopenNFeItsIDI(0);

end;

procedure TFmEntradaCab.QrNFeItsIBeforeClose(DataSet: TDataSet);
begin
  QrNFeItsN.Close;
  QrNFeItsO.Close;
  QrNFeItsP.Close;
  QrNFeItsQ.Close;
  QrNFeItsR.Close;
  QrNFeItsS.Close;
  QrNFeItsT.Close;
  //QrNFeItsU.Close;
  QrNFeItsV.Close;
end;

procedure TFmEntradaCab.QrStqMovItsBeforeClose(DataSet: TDataSet);
begin
  QrI.Close;
  QrN.Close;
  QrO.Close;
end;

procedure TFmEntradaCab.QrTransportaCalcFields(DataSet: TDataSet);
begin
  QrTransportaCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrTransportaCNPJ_CPF.Value);
end;

(***** Recuperacao de impostos****
object GroupBox32: TGroupBox
  Left = 0
  Top = 316
  Width = 1008
  Height = 120
  Align = alTop
  Caption = ' Recupera'#231#227'o de impostos: '
  TabOrder = 5
  Visible = False
  object GroupBox33: TGroupBox
    Left = 602
    Top = 15
    Width = 200
    Height = 103
    Align = alLeft
    Caption = ' COFINS: '
    TabOrder = 3
    object Label312: TLabel
      Left = 8
      Top = 20
      Width = 51
      Height = 13
      Caption = '% Red BC:'
      FocusControl = EdCOFINSRec_pRedBC
    end
    object Label313: TLabel
      Left = 68
      Top = 20
      Width = 26
      Height = 13
      Caption = '$ BC:'
      FocusControl = EdCOFINSRec_vBC
    end
    object Label343: TLabel
      Left = 68
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = EdCOFINSRec_vCOFINS
    end
    object Label347: TLabel
      Left = 8
      Top = 60
      Width = 53
      Height = 13
      Caption = '% COFINS:'
      FocusControl = EdCOFINSRec_pAliq
    end
    object EdCOFINSRec_pRedBC: TdmkEdit
      Left = 8
      Top = 36
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '100,00'
      QryCampo = 'COFINSRec_pRedBC'
      UpdCampo = 'COFINSRec_pRedBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 100.000000000000000000
      ValWarn = False
    end
    object EdCOFINSRec_vBC: TdmkEdit
      Left = 68
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'COFINSRec_vBC'
      UpdCampo = 'COFINSRec_vBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCOFINSRec_vCOFINS: TdmkEdit
      Left = 68
      Top = 76
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'COFINSRec_vCOFINS'
      UpdCampo = 'COFINSRec_vCOFINS'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCOFINSRec_pAliq: TdmkEdit
      Left = 8
      Top = 76
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'COFINSRec_pAliq'
      UpdCampo = 'COFINSRec_pAliq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox34: TGroupBox
    Left = 402
    Top = 15
    Width = 200
    Height = 103
    Align = alLeft
    Caption = ' PIS: '
    TabOrder = 2
    object Label334: TLabel
      Left = 8
      Top = 20
      Width = 51
      Height = 13
      Caption = '% Red BC:'
      FocusControl = EdPISRec_pRedBC
    end
    object Label335: TLabel
      Left = 68
      Top = 20
      Width = 26
      Height = 13
      Caption = '$ BC:'
      FocusControl = EdPISRec_vBC
    end
    object Label342: TLabel
      Left = 68
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = EdPISRec_vPIS
    end
    object Label346: TLabel
      Left = 8
      Top = 60
      Width = 31
      Height = 13
      Caption = '% PIS:'
      FocusControl = EdPISRec_pAliq
    end
    object EdPISRec_pRedBC: TdmkEdit
      Left = 8
      Top = 36
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '100,00'
      QryCampo = 'PISRec_pRedBC'
      UpdCampo = 'PISRec_pRedBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 100.000000000000000000
      ValWarn = False
    end
    object EdPISRec_vBC: TdmkEdit
      Left = 68
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'PISRec_vBC'
      UpdCampo = 'PISRec_vBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPISRec_vPIS: TdmkEdit
      Left = 68
      Top = 76
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'PISRec_vPIS'
      UpdCampo = 'PISRec_vPIS'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPISRec_pAliq: TdmkEdit
      Left = 8
      Top = 76
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'PISRec_pAliq'
      UpdCampo = 'PISRec_pAliq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox35: TGroupBox
    Left = 202
    Top = 15
    Width = 200
    Height = 103
    Align = alLeft
    Caption = ' IPI: '
    TabOrder = 1
    object Label336: TLabel
      Left = 8
      Top = 20
      Width = 51
      Height = 13
      Caption = '% Red BC:'
      FocusControl = EdIPIRec_pRedBC
    end
    object Label337: TLabel
      Left = 68
      Top = 20
      Width = 26
      Height = 13
      Caption = '$ BC:'
      FocusControl = EdIPIRec_vBC
    end
    object Label341: TLabel
      Left = 68
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = EdIPIRec_vIPI
    end
    object Label345: TLabel
      Left = 8
      Top = 60
      Width = 27
      Height = 13
      Caption = '% IPI:'
      FocusControl = EdIPIRec_pAliq
    end
    object EdIPIRec_pRedBC: TdmkEdit
      Left = 8
      Top = 36
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '100,00'
      QryCampo = 'IPIRec_pRedBC'
      UpdCampo = 'IPIRec_pRedBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 100.000000000000000000
      ValWarn = False
    end
    object EdIPIRec_vBC: TdmkEdit
      Left = 68
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'IPIRec_vBC'
      UpdCampo = 'IPIRec_vBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdIPIRec_vIPI: TdmkEdit
      Left = 68
      Top = 76
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'IPIRec_vIPI'
      UpdCampo = 'IPIRec_vIPI'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdIPIRec_pAliq: TdmkEdit
      Left = 8
      Top = 76
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'IPIRec_pAliq'
      UpdCampo = 'IPIRec_pAliq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox36: TGroupBox
    Left = 2
    Top = 15
    Width = 200
    Height = 103
    Align = alLeft
    Caption = ' ICMS: '
    TabOrder = 0
    object Label338: TLabel
      Left = 8
      Top = 20
      Width = 51
      Height = 13
      Caption = '% Red BC:'
      FocusControl = EdICMSRec_pRedBC
    end
    object Label339: TLabel
      Left = 68
      Top = 20
      Width = 26
      Height = 13
      Caption = '$ BC:'
      FocusControl = EdICMSRec_vBC
    end
    object Label340: TLabel
      Left = 68
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = EdICMSRec_vICMS
    end
    object Label344: TLabel
      Left = 8
      Top = 60
      Width = 40
      Height = 13
      Caption = '% ICMS:'
      FocusControl = EdICMSRec_pAliq
    end
    object EdICMSRec_pRedBC: TdmkEdit
      Left = 8
      Top = 36
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '100,00'
      QryCampo = 'ICMSRec_pRedBC'
      UpdCampo = 'ICMSRec_pRedBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 100.000000000000000000
      ValWarn = False
    end
    object EdICMSRec_vBC: TdmkEdit
      Left = 68
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_vBC'
      UpdCampo = 'ICMSRec_vBC'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMSRec_vICMS: TdmkEdit
      Left = 68
      Top = 76
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_vICMS'
      UpdCampo = 'ICMSRec_vICMS'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMSRec_pAliq: TdmkEdit
      Left = 8
      Top = 76
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_pAliq'
      UpdCampo = 'ICMSRec_pAliq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox15: TGroupBox
    Left = 802
    Top = 15
    Width = 200
    Height = 103
    Align = alLeft
    Caption = ' ICMSST: '
    TabOrder = 4
    object Label80: TLabel
      Left = 68
      Top = 20
      Width = 26
      Height = 13
      Caption = '$ BC:'
      FocusControl = EdICMSRec_vBCST
    end
    object Label94: TLabel
      Left = 68
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Valor:'
      FocusControl = EdICMSRec_vICMSST
    end
    object Label73: TLabel
      Left = 8
      Top = 60
      Width = 54
      Height = 13
      Caption = '% ICMSST:'
      FocusControl = EdICMSRec_pAliqST
    end
    object EdICMSRec_vBCST: TdmkEdit
      Left = 68
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_vBCST'
      UpdCampo = 'ICMSRec_vBCST'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMSRec_vICMSST: TdmkEdit
      Left = 68
      Top = 79
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_vICMSST'
      UpdCampo = 'ICMSRec_vICMSST'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdICMSRec_pAliqST: TdmkEdit
      Left = 8
      Top = 76
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMSRec_pAliqST'
      UpdCampo = 'ICMSRec_pAliqST'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
end
*)
end.


