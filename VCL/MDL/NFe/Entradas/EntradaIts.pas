unit EntradaIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, Variants,
  mySQLDbTables, dmkRadioGroup, dmkDBEdit, Mask, dmkEditCalc, dmkImage,
  UnDmkEnums, MyListas, SPED_Listas, UnProjGroup_Consts;

type
  TCalculoRelacaoValor = (crvQtde, crvPreco, crvTotal);
  TFmEntradaIts = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label40: TLabel;
    Label66: TLabel;
    DsNFeCabA: TDataSource;
    DBEdit1: TDBEdit;
    DBEdIDCtrl: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit17: TDBEdit;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    GroupBox3: TGroupBox;
    Label37: TLabel;
    DBEdCNPJ_CPF_TXT_Fornece: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    SpeedButton3: TSpeedButton;
    GroupBox4: TGroupBox;
    QrUnidMedC: TmySQLQuery;
    QrUnidMedCCodigo: TIntegerField;
    QrUnidMedCCodUsu: TIntegerField;
    QrUnidMedCSigla: TWideStringField;
    QrUnidMedCNome: TWideStringField;
    DsUnidMedC: TDataSource;
    QrUnidMedT: TmySQLQuery;
    DsUnidMedT: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrUnidMedCGrandeza: TSmallintField;
    GroupBox5: TGroupBox;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrUnidMed: TmySQLQuery;
    DsUnidMed: TDataSource;
    RGBaixa: TdmkRadioGroup;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    GroupBox6: TGroupBox;
    Panel2: TPanel;
    Label5: TLabel;
    EdICMS_vBC: TdmkEdit;
    Label12: TLabel;
    EdICMS_pICMS: TdmkEdit;
    Label10: TLabel;
    EdICMS_vICMS: TdmkEdit;
    Label13: TLabel;
    EdIPI_pIPI: TdmkEdit;
    Label11: TLabel;
    EdIPI_vIPI: TdmkEdit;
    Label14: TLabel;
    Edprod_vFrete: TdmkEdit;
    Label17: TLabel;
    Edprod_vSeg: TdmkEdit;
    Label18: TLabel;
    Edprod_vDesc: TdmkEdit;
    Label19: TLabel;
    Edprod_vOutro: TdmkEdit;
    PnBaixa: TPanel;
    Label21: TLabel;
    EdPecas: TdmkEdit;
    Label22: TLabel;
    EdAreaM2: TdmkEditCalc;
    Label23: TLabel;
    EdAreaP2: TdmkEditCalc;
    Label24: TLabel;
    EdPeso: TdmkEdit;
    Label25: TLabel;
    EdQtde: TdmkEdit;
    EdUnidMed: TdmkEditCB;
    Label26: TLabel;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    QrUnidMedTCodigo: TIntegerField;
    QrUnidMedTCodUsu: TIntegerField;
    QrUnidMedTSigla: TWideStringField;
    QrUnidMedTNome: TWideStringField;
    QrUnidMedTGrandeza: TSmallintField;
    Panel5: TPanel;
    CkContinuar: TCheckBox;
    Panel6: TPanel;
    Edprod_EXTIPI: TdmkEdit;
    Label236: TLabel;
    Label6: TLabel;
    EdUnidMedCom: TdmkEditCB;
    Edprod_uCom: TdmkEdit;
    CBUnidMedCom: TdmkDBLookupComboBox;
    Edprod_qCom: TdmkEdit;
    Label16: TLabel;
    SbMedCom: TSpeedButton;
    EdUnidMedTrib: TdmkEditCB;
    Edprod_uTrib: TdmkEdit;
    CBUnidMedTrib: TdmkDBLookupComboBox;
    SbMedTrib: TSpeedButton;
    Edprod_qTrib: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edprod_vUnTrib: TdmkEdit;
    Edprod_vProd: TdmkEdit;
    Label4: TLabel;
    Label15: TLabel;
    Panel7: TPanel;
    Label231: TLabel;
    EdnItem: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    Label1: TLabel;
    Label232: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label235: TLabel;
    Edprod_NCM: TdmkEdit;
    SbGraGruX: TSpeedButton;
    EdICMS_Orig: TdmkEdit;
    Label237: TLabel;
    EdICMS_CST: TdmkEdit;
    QrCST_ICMS: TmySQLQuery;
    QrCST_ICMSCodTxt: TWideStringField;
    QrCST_ICMSNome: TWideStringField;
    DsCST_ICMS: TDataSource;
    QrTbSPEDEFD002: TmySQLQuery;
    QrTbSPEDEFD002CodTxt: TWideStringField;
    QrTbSPEDEFD002Nome: TWideStringField;
    DsTbSPEDEFD002: TDataSource;
    EdCST_ICMS: TdmkEditCB;
    CBCST_ICMS: TdmkDBLookupComboBox;
    Label27: TLabel;
    Edprod_CFOP: TdmkEditCB;
    CBprod_CFOP: TdmkDBLookupComboBox;
    EdIPI_CST: TdmkEditCB;
    CBIPI_CST: TdmkDBLookupComboBox;
    Label20: TLabel;
    QrCST_IPI: TmySQLQuery;
    DsCST_IPI: TDataSource;
    QrCST_IPICodTxt: TWideStringField;
    QrCST_IPINome: TWideStringField;
    RGIND_APUR: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edprod_cProdEnter(Sender: TObject);
    procedure Edprod_cProdExit(Sender: TObject);
    procedure EdUnidMedComChange(Sender: TObject);
    procedure EdUnidMedComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_uComChange(Sender: TObject);
    procedure Edprod_uComExit(Sender: TObject);
    procedure Edprod_uComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedTribChange(Sender: TObject);
    procedure EdUnidMedTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_uTribChange(Sender: TObject);
    procedure Edprod_uTribExit(Sender: TObject);
    procedure Edprod_uTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbMedComClick(Sender: TObject);
    procedure SbMedTribClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Edprod_vProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_vUnTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_qTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdICMS_vBCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGBaixaClick(Sender: TObject);
    procedure Edprod_qComRedefinido(Sender: TObject);
    procedure EdICMS_vICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIPI_vIPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_qTribRedefinido(Sender: TObject);
    procedure EdICMS_OrigRedefinido(Sender: TObject);
    procedure EdICMS_CSTRedefinido(Sender: TObject);
    procedure EdCST_ICMSRedefinido(Sender: TObject);
  private
    { Private declarations }
    FTemp_prod_cProd: String;
    procedure PesquisaPorSigla(EdSigla, EdUnidMed: TdmkEdit;
             CBUnidMed: TdmkDBLookupComboBox; Limpa: Boolean);
    procedure PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
    procedure CalculaRelacionaveis(Calculo: TCalculoRelacaoValor);
    procedure DefineQtde();
    procedure MontaCST_ICMS();
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FIDCtrl,
    FTipo,
    FOriCodi,
    FOriCtrl,
    FMeuID,
    FAnoMes: Integer;
    //
    procedure ReopenTabelas();
  end;

  var
  FmEntradaIts: TFmEntradaIts;

implementation

uses UnMyObjects, EntradaCab, UMySQLModule, MyDBCheck, (*GraGruE_P2,*) Module,
  UnMySQLCuringa, UnInternalConsts, UnidMed, GraGruN, ModuleGeral, ModProd,
  ModAppGraG1, DmkDAC_PF, UnDmkProcFunc, UnGrade_PF;

{$R *.DFM}

procedure TFmEntradaIts.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  OriPart = 0;
  StqCenCad = 1;
  //
  Volumes = 1;
var
  nItem, FatID, FatNum, Empresa: Integer;
  prod_cProd, (*prod_cEAN, prod_xProd,*)
  prod_NCM, prod_EXTIPI: String;
  (*prod_genero,*) prod_CFOP,
  prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;
  (*prod_cEANTrib,*) prod_uTrib: String;
  prod_qTrib, prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  MeuID, Nivel1, GraGruX: Integer;
  // NFeItsN
  ICMS_Orig, ICMS_CST: Integer;
  ICMS_vBC, ICMS_pICMS, ICMS_vICMS: Double;
  // NFeItsO
  IPI_pIPI, IPI_vIPI: Double;
  // StqmovItsA
  IDCtrl, Tipo, OriCodi, OriCtrl, UnidMedCom, UnidMedTrib, Baixa, UnidMed: Integer;
  DataHora: String;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll, TotalCusto: Double;
  //
  Insumo, CFOP: Integer;
  IPI_CST: Variant;
  IND_APUR: String;
  //
begin
  OriCtrl := 0;
  nItem := EdnItem.ValueVariant;
  if MyObjects.FIC(nItem = 0, EdnItem, 'Informe o n�mero do item!') then
    Exit;
  GraGruX       := EdGraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto (Reduzido)!') then
    Exit;
  //
  if MyObjects.FIC(RGBaixa.ItemIndex = 2, RGBaixa, 'Informe "Movimenta estoque"!') then
    Exit;
  //
  Nivel1        := QrGraGruXGraGru1.Value;
  FatID         := FmEntradaCab.QrNfeCabAFatID.Value;
  FatNum        := FmEntradaCab.QrNfeCabAFatNum.Value;
  Empresa       := FmEntradaCab.QrNfeCabAEmpresa.Value;
  //
  prod_cProd    := Edprod_cProd.Text;
  (*prod_cEAN,*)
  //prod_xProd    := '';
  prod_NCM      := Geral.SoNumero_TT(Edprod_NCM.Text);
  prod_EXTIPI   := Edprod_EXTIPI.Text;
  (*prod_genero,*)
  prod_CFOP     := Geral.SoNumero_TT(Edprod_CFOP.Text);
  prod_uCom     := Edprod_uCom.Text;
  prod_qCom     := Edprod_qCom.ValueVariant;
  prod_vProd    := Edprod_vProd.ValueVariant;
  (*prod_cEANTrib,*)
  prod_uTrib    := Edprod_uTrib.Text;
  prod_qTrib    := Edprod_qTrib.ValueVariant;
  prod_vUnTrib  := Edprod_vUnTrib.ValueVariant;
  prod_vFrete   := Edprod_vFrete.ValueVariant;
  prod_vSeg     := Edprod_vSeg.ValueVariant;
  prod_vDesc    := Edprod_vDesc.ValueVariant;
  prod_vOutro   := Edprod_vOutro.ValueVariant;
  MeuID         := FMeuID;
  UnidMedCom    := EdUnidMedCom.ValueVariant;
  UnidMedTrib   := EdUnidMedTrib.ValueVariant;
  //
  if prod_qCom = 0 then
    prod_vUnCom   := 0
  else
    prod_vUnCom   := prod_vProd / prod_qCom;
  //
  //  2011-05-03
{
  Qtde     := prod_qTrib;
  //
  Pecas    := 0;
  Peso     := 0;
  AreaM2   := 0;
  AreaP2   := 0;
  case QrUnidMedCGrandeza.Value of
    0: Pecas := prod_qCom;
    1:
    begin
      if LowerCase(prod_ucom) = 'm2' then
      begin
        AreaM2 := prod_qCom;
        AreaP2 := Geral.ConverteArea(prod_qCom, ctM2toP2, cfQuarto);
      end else if (LowerCase(prod_ucom) = 'p2') or (LowerCase(prod_ucom) = 'ft') then
      begin
        AreaP2 := prod_qCom;
        AreaM2 := Geral.ConverteArea(prod_qCom, ctP2toM2, cfCento);
      end else
      begin
        Geral.MensagemBox('N�o foi poss�vel definir a unidade de �rea!',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    2: Peso := prod_qCom;
  end;
  //
}
  if PnBaixa.Visible then
  begin
    Qtde   := EdQtde.ValueVariant;
    Pecas  := EdPecas.ValueVariant;
    Peso   := EdPeso.ValueVariant;
    AreaM2 := EdAreaM2.ValueVariant;
    AreaP2 := EdAreaP2.ValueVariant;
  end else
  begin
    Qtde   := 0;
    Pecas  := 0;
    Peso   := 0;
    AreaM2 := 0;
    AreaP2 := 0;
  end;
  //
  if RGBaixa.ItemIndex = 1 then
    if MyObjects.FIC(Qtde <> prod_qTrib, Edprod_qTrib,
    '"Quantidade no estoque" difere de "Quantidade tribut�vel"!') then
      Exit;
  //  2011-08-03

  if RGBaixa.ItemIndex = 1 then
    if MyObjects.FIC(EdUnidMed.ValueVariant <> EdUnidMedTrib.ValueVariant,  EdUnidMed,
    'A unidade de medida da "Quantidade no estoque" difere da "Unidade de Medida tribut�vel"!') then
      Exit;
  //
  if MyObjects.FIC(Uppercase(Trim(QrGraGruXSIGLAUNIDMED.Value)) <>
  Uppercase(Trim(Edprod_uTrib.Text)), EdUnidMedTrib,
  'AVISO:' + sLineBreak +
  'A unidade de medida da "Quantidade no estoque" difere da unidade de medida do cadastro do produto!') then
    (*Exit s� avisa*);
  //  Fim 2011-08-03
  //  Fim 2011-05-03
  //
  IPI_pIPI := EdIPI_pIPI.ValueVariant;
  IPI_vIPI := EdIPI_vIPI.ValueVariant;
  IPI_CST  := EdIPI_CST.ValueVariant;
  IND_APUR := Geral.SoNumero_TT(RGIND_APUR.Items[RGIND_APUR.ItemIndex][1]);
  if IPI_CST = '' then
    IPI_CST := null;
  if ((IND_APUR = '') and (IPI_CST <> null))
  or ((IND_APUR <> '') and (IPI_CST = null)) then
  begin
    Geral.MB_Aviso(
    'Quando o per�odo de apura��o ou o CST do IPI � informado, ambos devem ser informados!');
    Exit;
  end;
  //
  //
  DmProd.VerificaCadastroProduto(QrGraGruXGraGru1.Value,
    EdUnidMedCom.ValueVariant, Edprod_NCM.Text, Edprod_EXTIPI.Text);
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsi', False, [
    'prod_cProd', (*'prod_cEAN', 'prod_xProd',*)
    'prod_NCM', 'prod_EXTIPI', (*'prod_genero',*)
    'prod_CFOP', 'prod_uCom', 'prod_qCom',
    'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
    'prod_vOutro', (*'prod_indTot', 'prod_xPed',
    'prod_nItemPed', 'Tem_IPI', '_Ativo_',
    'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
    'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
    'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
    'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
    'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
    'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
    'MeuID', 'Nivel1', 'GraGruX',
    'UnidMedCom', 'UnidMedTrib'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    prod_cProd, (*prod_cEAN, prod_xProd,*)
    prod_NCM, prod_EXTIPI, (*prod_genero,*)
    prod_CFOP, prod_uCom, prod_qCom,
    prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
    prod_uTrib, prod_qTrib, prod_vUnTrib,
    prod_vFrete, prod_vSeg, prod_vDesc,
    prod_vOutro, (*prod_indTot, prod_xPed,
    prod_nItemPed, Tem_IPI, _Ativo_,
    InfAdCuztm, EhServico, ICMSRec_pRedBC,
    ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
    IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
    IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
    PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
    COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
    MeuID, Nivel1, GraGruX,
    UnidMedCom, UnidMedTrib], [
    FatID, FatNum, Empresa, nItem], True) then
    begin
      ICMS_Orig  := EdICMS_Orig.ValueVariant;
      ICMS_CST   := EdICMS_CST.ValueVariant;
      ICMS_vBC   := EdICMS_vBC.ValueVariant;
      ICMS_pICMS := EdICMS_pICMS.ValueVariant;
      ICMS_vICMS := EdICMS_vICMS.ValueVariant;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitsn', False, [
      'ICMS_Orig', 'ICMS_CST', (*'ICMS_modBC',
      'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS',
      'ICMS_vICMS'(*, 'ICMS_modBCST', 'ICMS_pMVAST',
      'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
      'ICMS_vICMSST', '_Ativo_', 'ICMS_CSOSN',
      'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
      'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
      'ICMS_vCredICMSSN'*)], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      ICMS_Orig, ICMS_CST, (*ICMS_modBC,
      ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS,
      ICMS_vICMS(*, ICMS_modBCST, ICMS_pMVAST,
      ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
      ICMS_vICMSST, _Ativo_, ICMS_CSOSN,
      ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
      ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
      ICMS_vCredICMSSN*)], [
      FatID, FatNum, Empresa, nItem], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfeitso', False, [
        (*'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
        'IPI_qSelo', 'IPI_cEnq',*) 'IPI_CST',
        (*'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',*)
        'IPI_pIPI', 'IPI_vIPI'(*, '_Ativo_'*),
        'IND_APUR'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        (*IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
        IPI_qSelo, IPI_cEnq,*) IPI_CST,
        (*IPI_vBC, IPI_qUnid, IPI_vUnid,*)
        IPI_pIPI, IPI_vIPI(*, _Ativo_*),
        IND_APUR], [
        FatID, FatNum, Empresa, nItem], True) then
        begin
          DataHora := Geral.FDT(FmEntradaCab.QrNFeCabADataFiscal.Value, 1);
          if ImgTipo.SQLType = stIns then
          begin
            Tipo     := FatID;
            OriCodi  := FatNum;
            OriCtrl  := nItem;
          end else begin
            Tipo     := FTipo;
            OriCodi  := FOriCodi;
            OriCtrl  := FOriCtrl;
          end;
          IDCtrl   := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', ImgTipo.SQLType, FIDCtrl);
          //
          CustoAll := prod_vProd;
          Baixa := RGBaixa.ItemIndex;
          UnidMed := EdUnidMed.ValueVariant;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
          'DataHora', 'Tipo', 'OriCodi',
          'OriCtrl', 'OriCnta', 'OriPart',
          'Empresa', 'StqCenCad', 'GraGruX',
          'Qtde', 'Pecas', 'Peso',
          'AreaM2', 'AreaP2', (*'FatorClas',
          'QuemUsou', 'Retorno', 'ParTipo',
          'ParCodi', 'DebCtrl', 'SMIMultIns',*)
          'CustoAll', (*'ValorAll', 'GrupoBal',*)
          'Baixa', 'UnidMed'], [
          'IDCtrl'], [
          DataHora, Tipo, OriCodi,
          OriCtrl, OriCnta, OriPart,
          Empresa, StqCenCad, GraGruX,
          Qtde, Pecas, Peso,
          AreaM2, AreaP2, (*FatorClas,
          QuemUsou, Retorno, ParTipo,
          ParCodi, DebCtrl, SMIMultIns,*)
          CustoAll, (*ValorAll, GrupoBal,*)
          Baixa, UnidMed], [
          IDCtrl], False) then
          begin
            DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
            //
            TotalCusto := prod_vProd + prod_vFrete + prod_vSeg + prod_vDesc +
                          prod_vOutro;
            CFOP := Geral.IMV(prod_CFOP);
            DfModAppGraG1.InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem,
            OriCtrl, GraGrux, Volumes, prod_qCom, prod_qTrib, prod_vProd,
            IPI_pIPI, IPI_vIPI, TotalCusto, prod_qTrib, CFOP, ImgTipo.SQLType);
            //
            FmEntradaCab.ReopenStqMovIts(IDCtrl);
            //
            Close;
          end;
        end;
      end;
    end;
  except
    FmEntradaCab.ExcluiItem(FatID, FatNum, Empresa, OriCtrl, nItem, False);
    raise;
  end;
end;

procedure TFmEntradaIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntradaIts.CalculaRelacionaveis(Calculo: TCalculoRelacaoValor);
var
  Qtde, Preco, Total: Double;
begin
  Qtde  := Edprod_qTrib.ValueVariant;
  Preco := Edprod_vUnTrib.ValueVariant;
  Total := Edprod_vProd.ValueVariant;
  //
  case Calculo of
    crvQtde:  if Preco = 0 then Qtde := 0 else Qtde := Total / Preco;
    crvPreco: if Qtde = 0 then Preco := 0 else Preco := Total / Qtde;
    crvTotal: Total := Qtde * Preco;
  end;
  //
  case Calculo of
    crvQtde:  Edprod_qTrib.ValueVariant := Qtde;
    crvPreco: Edprod_vUnTrib.ValueVariant := Preco;
    crvTotal: Edprod_vProd.ValueVariant := Total;
  end;
end;

procedure TFmEntradaIts.CBUnidMedComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger);
end;

procedure TFmEntradaIts.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmEntradaIts.CBUnidMedTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger);
end;

procedure TFmEntradaIts.DefineQtde();
begin
end;

procedure TFmEntradaIts.EdAreaM2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmEntradaIts.EdAreaP2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmEntradaIts.EdCST_ICMSRedefinido(Sender: TObject);
var
  Orig, CST: String;
begin
  Orig := Copy(EdCST_ICMS.Text, 1, 1);
  CST  := Copy(EdCST_ICMS.Text, 2);
  //
  if EdICMS_Orig.Text <> Orig then
    EdICMS_Orig.Text := Orig;
  if EdICMS_CST.Text <> CST then
    EdICMS_CST.Text := CST;
end;

procedure TFmEntradaIts.EdGraGruXChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmEntradaIts.EdGraGruXRedefinido(Sender: TObject);
var
  NCM: String;
begin
  case QrGraGruXGerBxaEstq.Value of
    //0: ? ? ?
    1: EdQtde.ValueVariant := EdPecas.ValueVariant;  // Pe�a
    2: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // m�
    3: EdQtde.ValueVariant := EdPeso.ValueVariant;   // kg
    4: EdQtde.ValueVariant := EdPeso.ValueVariant / 1000;   // t (ton)
    5: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // ft�
    else EdQtde.ValueVariant := 0;
  end;
  if ImgTipo.SQLType = stIns then
  begin
    NCM := Geral.SoNumero_TT(QrGraGruXNCM.Value);
    if Geral.IMV(NCM) > 0 then
      Edprod_NCM.Text := NCM;
    if QrGraGruXUnidMed.Value <> 0 then
    begin
      EdUnidMedCom.ValueVariant  := QrGraGruXUnidMed.Value;
      CBUnidMedCom.KeyValue      := QrGraGruXUnidMed.Value;
      EdUnidMedTrib.ValueVariant := QrGraGruXUnidMed.Value;
      CBUnidMedTrib.KeyValue     := QrGraGruXUnidMed.Value;
      if RGBaixa.ItemIndex = 1 then
      begin
        EdUnidMed.ValueVariant     := QrGraGruXUnidMed.Value;
        CBUnidMed.KeyValue         := QrGraGruXUnidMed.Value;
      end;
    end;
  end;
end;

procedure TFmEntradaIts.EdICMS_CSTRedefinido(Sender: TObject);
begin
  MontaCST_ICMS();
end;

procedure TFmEntradaIts.EdICMS_OrigRedefinido(Sender: TObject);
begin
  MontaCST_ICMS();
end;

procedure TFmEntradaIts.EdICMS_vBCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdICMS_vBC.ValueVariant := EdProd_vProd.ValueVariant;
end;

procedure TFmEntradaIts.EdICMS_vICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdICMS_vICMS.ValueVariant := Geral.RoundC(EdICMS_vBC.ValueVariant *
    EdICMS_pICMS.ValueVariant / 100, 2);
end;

procedure TFmEntradaIts.EdIPI_vIPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdIPI_vIPI.ValueVariant := Geral.RoundC(Edprod_vProd.ValueVariant *
    EdIPI_pIPI.ValueVariant / 100, 2);
end;

procedure TFmEntradaIts.EdPecasChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmEntradaIts.EdPesoChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmEntradaIts.Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pesq: String;
begin
  if Key = VK_F3 then
  begin
    Pesq := CuringaLoc.CriaForm4('Codigo', 'Nome', 'cfop2003', Dmod.MyDB, '');
    if Pesq <> '' then
      Edprod_CFOP.Text := VAR_NOME_X;
  end;
end;

procedure TFmEntradaIts.Edprod_cProdEnter(Sender: TObject);
begin
  FTemp_prod_cProd := Edprod_cProd.Text;
end;

procedure TFmEntradaIts.Edprod_cProdExit(Sender: TObject);
var
  Fornece, GraGruX(*, Nivel1*): Integer;
  cProd: String;
  Habilita: Boolean;
begin
{  Liberar aqui !!!
  if Edprod_cProd.Text <> FTemp_prod_cProd then
  begin
    Fornece := FmEntradaCab.QrNFeCabACodInfoEmit.Value;
    cProd   := Edprod_cProd.Text;
    //
    GraGruX := UnGrade_PF.SelecionaGraGruXDeGraGruE(Fornece, cProd, xProd);
    //
    EdGraGruX.ValueVariant := GraGruX;
    CBGraGruX.KeyValue     := GraGruX;
    Habilita := (GraGruX = 0) and (cProd = '');
    EdGraGruX.Enabled := Habilita;
    CBGraGruX.Enabled := Habilita;
    //
    //
    if not Habilita then
      Edprod_NCM.SetFocus;
  end;
}
end;

procedure TFmEntradaIts.Edprod_qComRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    Edprod_qTrib.ValueVariant := Edprod_qCom.ValueVariant;
end;

procedure TFmEntradaIts.Edprod_qTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvQtde);
end;

procedure TFmEntradaIts.Edprod_qTribRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdQtde.ValueVariant := Edprod_qTrib.ValueVariant;
end;

procedure TFmEntradaIts.Edprod_uComChange(Sender: TObject);
begin
  if Edprod_uCom.Focused then
    PesquisaPorSigla(Edprod_uCom, EdUnidMedCom, CBUnidMedCom, False);
end;

procedure TFmEntradaIts.Edprod_uComExit(Sender: TObject);
begin
  PesquisaPorSigla(Edprod_uCom, EdUnidMedCom, CBUnidMedCom, False);
end;

procedure TFmEntradaIts.Edprod_uComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger)
end;

procedure TFmEntradaIts.Edprod_uTribChange(Sender: TObject);
begin
  if Edprod_uTrib.Focused then
    PesquisaPorSigla(Edprod_uTrib, EdUnidMedTrib, CBUnidMedTrib, False);
end;

procedure TFmEntradaIts.Edprod_uTribExit(Sender: TObject);
begin
  PesquisaPorSigla(Edprod_uTrib, EdUnidMedTrib, CBUnidMedTrib, False);
end;

procedure TFmEntradaIts.Edprod_uTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger)
end;

procedure TFmEntradaIts.Edprod_vProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvTotal);
end;

procedure TFmEntradaIts.Edprod_vUnTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvPreco);
end;

procedure TFmEntradaIts.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    PesquisaPorSigla(EdSigla, EdUnidMed, CBUnidMed, False);
end;

procedure TFmEntradaIts.EdSiglaExit(Sender: TObject);
begin
  PesquisaPorSigla(EdSigla, EdUnidMed, CBUnidMed, False);
end;

procedure TFmEntradaIts.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmEntradaIts.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);

end;

procedure TFmEntradaIts.EdUnidMedComChange(Sender: TObject);
begin
  if not Edprod_uCom.Focused then
    PesquisaPorCodigo(EdUnidMedCom.ValueVariant, Edprod_uCom);
end;

procedure TFmEntradaIts.EdUnidMedComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger);
end;

procedure TFmEntradaIts.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmEntradaIts.EdUnidMedTribChange(Sender: TObject);
begin
  if not Edprod_uTrib.Focused then
    PesquisaPorCodigo(EdUnidMedTrib.ValueVariant, Edprod_uTrib)
  else
end;

procedure TFmEntradaIts.EdUnidMedTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger);
end;

procedure TFmEntradaIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntradaIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGraGruX();
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  UnDmkDAC_PF.AbreQuery(QrUnidMedC, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  UnDmkDAC_PF.AbreQuery(QrUnidMedT, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  FIDCtrl  := 0;
  FTipo    := 0;
  FOriCodi := 0;
  FOriCtrl := 0;
  FMeuID   := 0;
end;

procedure TFmEntradaIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntradaIts.MontaCST_ICMS();
var
  CST_ICMS: String;
begin
  CST_ICMS := EdICMS_Orig.Text + EdICMS_CST.Text;
  if CST_ICMS <> EdCST_ICMS.Text then
    EdCST_ICMS.Text := CST_ICMS;
end;

procedure TFmEntradaIts.PesquisaPorCodigo(UnidMed: Integer;
EdSigla: TdmkEdit);
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsInteger := UnidMed;
  UnDmkDAC_PF.AbreQuery(QrPesq2, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

procedure TFmEntradaIts.PesquisaPorSigla(EdSigla, EdUnidMed: TdmkEdit;
CBUnidMed: TdmkDBLookupComboBox; Limpa: Boolean);
begin
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := EdSigla.Text;
  UnDmkDAC_PF.AbreQuery(QrPesq1, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  if QrPesq1.RecordCount > 0 then
  begin
    if EdUnidMed.ValueVariant <> QrPesq1CodUsu.Value then
      EdUnidMed.ValueVariant := QrPesq1CodUsu.Value;
    if CBUnidMed.KeyValue     <> QrPesq1CodUsu.Value then
      CBUnidMed.KeyValue     := QrPesq1CodUsu.Value;
  end else if Limpa then
    EdSigla.ValueVariant := '';
end;

procedure TFmEntradaIts.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > -900000 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmEntradaIts.ReopenTabelas();
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';

(*
  // COD_SIT >  QrTbSPEDEFD018
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD018, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedefd018 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
*)
  // CST_ICMS > QrTbSPEDEFD130
  UnDmkDAC_PF.AbreMySQLQuery0(QrCST_ICMS, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedefd130 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
  // CST_IPI > QrTbSPEDEFD026
  UnDmkDAC_PF.AbreMySQLQuery0(QrCST_IPI, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedefd026 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
  // CFOP > QrTbSPEDEFD002
  {$IFDEF comSPED_0000}
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD002, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  //
  {$EndIf}
end;

procedure TFmEntradaIts.RGBaixaClick(Sender: TObject);
begin
  PnBaixa.Visible := RGBaixa.Itemindex = 1;
  if PnBaixa.Visible then
  begin
    //EdPecas.ValueVariant   := ?;
    //EdAreaM2.ValueVariant  := ?;
    //EdPeso.ValueVariant    := ?;
    EdQtde.ValueVariant    := Edprod_qTrib.ValueVariant;
    EdUnidMed.ValueVariant := EdUnidMedTrib.ValueVariant;
    CBUnidMed.KeyValue     := CBUnidMedTrib.KeyValue;
    case QrUnidMedTGrandeza.Value of
      //Pe�a
      0: EdPecas.ValueVariant := Edprod_qTrib.ValueVariant;
      //�rea (m�)
      1: EdAreaM2.ValueVariant := Edprod_qTrib.ValueVariant;
      //Peso (kg)
      2: EdPeso.ValueVariant := Edprod_qTrib.ValueVariant;
      //3.Volume ( m�)
      //4.Linear (m)
      //5.? ? ? (outros)
      //�rea (ft�)
      6: EdAreaP2.ValueVariant := Edprod_qTrib.ValueVariant;
      //Peso (t)
      7: EdPeso.ValueVariant := Edprod_qTrib.ValueVariant / 1000;
    end;
  end else
  begin
    EdPecas.ValueVariant   := 0;
    EdAreaM2.ValueVariant  := 0;
    EdPeso.ValueVariant    := 0;
    EdQtde.ValueVariant    := 0;
    EdUnidMed.ValueVariant := 0;
    CBUnidMed.KeyValue     := 0;
  end;
end;

procedure TFmEntradaIts.SbGraGruXClick(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdGraGruX, CBGraGruX, QrGraGruX, VAR_CADASTRO,
      'Controle', 'Controle');
    end;
  end;
}
end;

procedure TFmEntradaIts.SbMedComClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMedCom, CBUnidMedCom, QrUnidMedC, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

procedure TFmEntradaIts.SbMedTribClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMedTrib, CBUnidMedTrib, QrUnidMedT, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

procedure TFmEntradaIts.SBUnidMedClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

end.
