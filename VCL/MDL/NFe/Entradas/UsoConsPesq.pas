unit UsoConsPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, Db, (*DBTables,*) StdCtrls, Buttons,
  ComCtrls, DBCtrls, UnInternalConsts, mySQLDbTables, Variants, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkDBGrid, dmkEditDateTimePicker,
  UnDmkProcFunc;

type
  TFmUsoConsPesq = class(TForm)
    PainelControle: TPanel;
    PainelDados: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGrid;
    QrNFs: TmySQLQuery;
    DsNFs: TDataSource;
    BtConfirma: TBitBtn;
    CBFornecedor: TdmkDBLookupComboBox;
    Label1: TLabel;
    Label3: TLabel;
    CBGraGru1: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    EdCodigo: TdmkEdit;
    EdNF: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    EdFornecedor: TdmkEditCB;
    EdGraGru1: TdmkEditCB;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNome: TWideStringField;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrNFsNO_FRN: TWideStringField;
    QrNFside_nNF: TIntegerField;
    QrNFsemit_CNPJ: TWideStringField;
    QrNFsDataFiscal: TDateField;
    QrNFsNivel1: TIntegerField;
    QrNFsNO_PRD: TWideStringField;
    QrNFsIDCtrl: TIntegerField;
    QrNFsFatNum: TIntegerField;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Label2: TLabel;
    EdIDCtrl: TdmkEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodigoExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdConhecimentoExit(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrFornecedoresAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FLiberado: Boolean;

    procedure Pesquisa();

  public
    { Public declarations }
  end;

var
  FmUsoConsPesq: TFmUsoConsPesq;

implementation

uses UnMyObjects, UsoConsCab;

{$R *.DFM}

procedure TFmUsoConsPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FLiberado := True;
end;

procedure TFmUsoConsPesq.FormCreate(Sender: TObject);
begin
  FLiberado := False;
  QrFornecedores.Open;
  QrGraGru1.Open;
  TPIni.Date := Date - 90;
  TPFim.Date := Date;
end;

procedure TFmUsoConsPesq.CBFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBFornecedor.KeyValue := NULL;
  Pesquisa();
end;

procedure TFmUsoConsPesq.CBGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBGraGru1.KeyValue := NULL;
  Pesquisa();
end;

procedure TFmUsoConsPesq.EdCodigoExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.EdNFExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.EdConhecimentoExit(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.Pesquisa();
var
  Ini, Fim: String;
begin
  if FLiberado then
  begin
    Ini := Geral.FDT(TPIni.Date, 1);
    Fim := Geral.FDT(TPFim.Date, 1);
    ////////////
    QrNFs.Close;
    QrNFs.SQL.Clear;
    QrNFs.SQL.Add('SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
    QrNFs.SQL.Add('nfa.ide_nNF, nfa.emit_CNPJ, nfa.DataFiscal,');
    QrNFs.SQL.Add('gg1.Nivel1, gg1.Nome NO_PRD, nfa.FatNum, nfa.IDCtrl');
    QrNFs.SQL.Add('FROM stqmovitsa smia');
    QrNFs.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrNFs.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrNFs.SQL.Add('LEFT JOIN nfecaba    nfa ON nfa.FatID=smia.Tipo');
    QrNFs.SQL.Add('  AND nfa.FatNum=smia.OriCodi');
    QrNFs.SQL.Add('LEFT JOIN entidades  frn ON frn.Codigo=nfa.CodInfoEmit');
    QrNFs.SQL.Add('WHERE smia.Tipo IN (' + FmUsoConsCab.FIDs_Txt + ')');
    QrNFs.SQL.Add(dmkPF.SQL_Periodo('AND nfa.DataFiscal ',
      TPIni.Date, TPFim.Date, True, True));
    if EdCodigo.ValueVariant <> 0 then
     QrNFs.SQL.Add('AND nfa.FatNum=' + FormatFloat('0', EdCodigo.ValueVariant));
    if EdNF.ValueVariant <> 0 then
      QrNFs.SQL.Add('AND nfa.ide_nNF=' + FormatFloat('0', EdNF.ValueVariant));
    if EdIDCtrl.ValueVariant <> 0 then
      QrNFs.SQL.Add('AND nfa.IDCtrl=' + FormatFloat('0', EdIDCtrl.ValueVariant));
    if CBFornecedor.KeyValue <> NULL then
      QrNFs.SQL.Add('AND nfa.CodInfoEmit=' + FormatFloat('0', EdFornecedor.ValueVariant));
    if EdGraGru1.ValueVariant <> 0 then
    //  ver Reduzidos
      QrNFs.SQL.Add('AND smia.GraGruX=' + FormatFloat('0', EdGraGru1.ValueVariant));
    QrNFs.Open;
  end;
end;

procedure TFmUsoConsPesq.RGOrdem1Click(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.EdFornecedorChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.EdGraGru1Change(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.TPIniClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.TPFimClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.BtSaidaClick(Sender: TObject);
begin
  VAR_PQELANCTO := -1;
  Close;
end;

procedure TFmUsoConsPesq.BtConfirmaClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrNFsIDCtrl.Value;
  Close;
end;

procedure TFmUsoConsPesq.QrFornecedoresAfterScroll(DataSet: TDataSet);
begin
{
  QrInsumos.Close;
  QrInsumos.SQL.Clear;
  QrInsumos.SQL.Add('SELECT * FROM pq');
  if CBFornecedor.KeyValue <> NULL then
  begin
    CBInsumo.KeyValue := NULL;
    QrInsumos.SQL.Add('WHERE IQ='''+IntToStr(CBFornecedor.KeyValue)+'''');
  end;
  QrInsumos.SQL.Add('ORDER BY Nome');
  QrInsumos.Open;
}
end;

procedure TFmUsoConsPesq.DBGrid1DblClick(Sender: TObject);
begin
  VAR_PQELANCTO := QrNFsIDCtrl.Value;
  Close;
end;

procedure TFmUsoConsPesq.TPFimChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.TPIniChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmUsoConsPesq.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

