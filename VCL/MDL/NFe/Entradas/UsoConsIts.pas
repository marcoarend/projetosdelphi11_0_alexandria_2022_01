unit UsoConsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkRadioGroup, dmkDBEdit, Mask, dmkEditCalc, UnDmkEnums;

type
  TCalculoRelacaoValor = (crvQtde, crvPreco, crvTotal);
  TFmUsoConsIts = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label40: TLabel;
    Label66: TLabel;
    DsNFeCabA: TDataSource;
    DBEdit1: TDBEdit;
    DBEdIDCtrl: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit17: TDBEdit;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    GroupBox3: TGroupBox;
    Label37: TLabel;
    DBEdCNPJ_CPF_TXT_Fornece: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    QrLocGGE: TmySQLQuery;
    QrLocGGEGraGruX: TIntegerField;
    QrLocGGENivel1: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    SpeedButton3: TSpeedButton;
    GroupBox4: TGroupBox;
    Label231: TLabel;
    Label232: TLabel;
    SbGraGruX: TSpeedButton;
    Label235: TLabel;
    Label236: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    SbMedCom: TSpeedButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdnItem: TdmkEdit;
    Edprod_NCM: TdmkEdit;
    Edprod_EXTIPI: TdmkEdit;
    EdICMS_Orig: TdmkEdit;
    Edprod_CFOP: TdmkEdit;
    Edprod_cProd: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdICMS_CST: TdmkEdit;
    EdUnidMedCom: TdmkEditCB;
    Edprod_uCom: TdmkEdit;
    CBUnidMedCom: TdmkDBLookupComboBox;
    Edprod_qTrib: TdmkEdit;
    Edprod_vUnTrib: TdmkEdit;
    Edprod_vProd: TdmkEdit;
    EdICMS_vBC: TdmkEdit;
    EdICMS_vICMS: TdmkEdit;
    EdIPI_vIPI: TdmkEdit;
    EdICMS_pICMS: TdmkEdit;
    EdIPI_pIPI: TdmkEdit;
    Label15: TLabel;
    EdUnidMedTrib: TdmkEditCB;
    Edprod_uTrib: TdmkEdit;
    CBUnidMedTrib: TdmkDBLookupComboBox;
    SbMedTrib: TSpeedButton;
    Label16: TLabel;
    Edprod_qCom: TdmkEdit;
    QrUnidMedC: TmySQLQuery;
    QrUnidMedCCodigo: TIntegerField;
    QrUnidMedCCodUsu: TIntegerField;
    QrUnidMedCSigla: TWideStringField;
    QrUnidMedCNome: TWideStringField;
    DsUnidMedC: TDataSource;
    QrUnidMedT: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    DsUnidMedT: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    Label14: TLabel;
    Edprod_vFrete: TdmkEdit;
    Label17: TLabel;
    Edprod_vSeg: TdmkEdit;
    Label18: TLabel;
    Edprod_vDesc: TdmkEdit;
    Label19: TLabel;
    Edprod_vOutro: TdmkEdit;
    QrUnidMedCGrandeza: TSmallintField;
    Label20: TLabel;
    GroupBox5: TGroupBox;
    Label21: TLabel;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    Label22: TLabel;
    EdAreaP2: TdmkEditCalc;
    Label23: TLabel;
    Label24: TLabel;
    EdPeso: TdmkEdit;
    DBEdit13: TDBEdit;
    Label25: TLabel;
    EdQtde: TdmkEdit;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edprod_cProdEnter(Sender: TObject);
    procedure Edprod_cProdExit(Sender: TObject);
    procedure EdUnidMedComChange(Sender: TObject);
    procedure EdUnidMedComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_uComChange(Sender: TObject);
    procedure Edprod_uComExit(Sender: TObject);
    procedure Edprod_uComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedComKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedTribChange(Sender: TObject);
    procedure EdUnidMedTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_uTribChange(Sender: TObject);
    procedure Edprod_uTribExit(Sender: TObject);
    procedure Edprod_uTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbMedComClick(Sender: TObject);
    procedure SbMedTribClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Edprod_vProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_vUnTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edprod_qTribKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
  private
    { Private declarations }
    FTemp_prod_cProd: String;
    function PesquisaPorSigla(EdSigla, EdUnidMed: TdmkEdit;
             CBUnidMed: TdmkDBLookupComboBox; Limpa: Boolean): Integer;
    procedure PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
    procedure CalculaRelacionaveis(Calculo: TCalculoRelacaoValor);
    procedure DefineQtde();
  public
    { Public declarations }
    FIDCtrl,
    FTipo,
    FOriCodi,
    FOriCtrl,
    FMeuID: Integer;
  end;

  var
  FmUsoConsIts: TFmUsoConsIts;

implementation

uses UnMyObjects, EntradaCab, UMySQLModule, MyDBCheck, GraGruE_P2, Module, UnMySQLCuringa,
UnInternalConsts, UnidMed, GraGruN, ModuleGeral;

{$R *.DFM}

procedure TFmUsoConsIts.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  OriPart = 0;
  StqCenCad = 1;
var
  nItem, FatID, FatNum, Empresa: Integer;
  prod_cProd, (*prod_cEAN, prod_xProd,*)
  prod_NCM, prod_EXTIPI: String;
  (*prod_genero,*) prod_CFOP,
  prod_uCom: String;
  prod_qCom, prod_vUnCom, prod_vProd: Double;
  (*prod_cEANTrib,*) prod_uTrib: String;
  prod_qTrib, prod_vUnTrib, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  MeuID, Nivel1, GraGruX: Integer;
  // NFeItsN
  ICMS_Orig, ICMS_CST: Integer;
  ICMS_vBC, ICMS_pICMS, ICMS_vICMS: Double;
  // NFeItsO
  IPI_pIPI, IPI_vIPI: Double;
  // StqmovItsA
  IDCtrl, Tipo, OriCodi, OriCtrl, UnidMedCom, UnidMedTrib: Integer;
  DataHora: String;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll: Double;
begin
  nItem := EdnItem.ValueVariant;
  if MyObjects.FIC(nItem = 0, EdnItem, 'Informe o n�mero do item!') then
    Exit;
  GraGruX       := EdGraGruX.ValueVariant;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto (Reduzido)!') then
    Exit;
  //
  Nivel1        := QrGraGruXGraGru1.Value;
  FatID         := FmEntradaCab.QrNfeCabAFatID.Value;
  FatNum        := FmEntradaCab.QrNfeCabAFatNum.Value;
  Empresa       := FmEntradaCab.QrNfeCabAEmpresa.Value;
  //
  prod_cProd    := Edprod_cProd.Text;
  (*prod_cEAN,*)
  //prod_xProd    := '';
  prod_NCM      := Geral.SoNumero_TT(Edprod_NCM.Text);
  prod_EXTIPI   := Edprod_EXTIPI.Text;
  (*prod_genero,*)
  prod_CFOP     := Geral.SoNumero_TT(Edprod_CFOP.Text);
  prod_uCom     := Edprod_uCom.Text;
  prod_qCom     := Edprod_qCom.ValueVariant;
  prod_vProd    := Edprod_vProd.ValueVariant;
  (*prod_cEANTrib,*)
  prod_uTrib    := Edprod_uTrib.Text;
  prod_qTrib    := Edprod_qTrib.ValueVariant;
  prod_vUnTrib  := Edprod_vUnTrib.ValueVariant;
  prod_vFrete   := Edprod_vFrete.ValueVariant;
  prod_vSeg     := Edprod_vSeg.ValueVariant;
  prod_vDesc    := Edprod_vDesc.ValueVariant;
  prod_vOutro   := Edprod_vOutro.ValueVariant;
  MeuID         := FMeuID;
  UnidMedCom    := EdUnidMedCom.ValueVariant;
  UnidMedTrib   := EdUnidMedTrib.ValueVariant;
  //
  if prod_qCom = 0 then
    prod_vUnCom   := 0
  else
    prod_vUnCom   := prod_vProd / prod_qCom;
  //
  //  2011-05-03
{
  Qtde     := prod_qTrib;
  //
  Pecas    := 0;
  Peso     := 0;
  AreaM2   := 0;
  AreaP2   := 0;
  case QrUnidMedCGrandeza.Value of
    0: Pecas := prod_qCom;
    1:
    begin
      if LowerCase(prod_ucom) = 'm2' then
      begin
        AreaM2 := prod_qCom;
        AreaP2 := Geral.ConverteArea(prod_qCom, ctM2toP2, cfQuarto);
      end else if (LowerCase(prod_ucom) = 'p2') or (LowerCase(prod_ucom) = 'ft') then
      begin
        AreaP2 := prod_qCom;
        AreaM2 := Geral.ConverteArea(prod_qCom, ctP2toM2, cfCento);
      end else
      begin
        Geral.MensagemBox('N�o foi poss�vel definir a unidade de �rea!',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    2: Peso := prod_qCom;
  end;
  //
}
  Qtde   := EdQtde.ValueVariant;
  Pecas  := EdPecas.ValueVariant;
  Peso   := EdPeso.ValueVariant;
  AreaM2 := EdAreaM2.ValueVariant;
  AreaP2 := EdAreaP2.ValueVariant;
  //
  if MyObjects.FIC(Qtde <> prod_qTrib, Edprod_qTrib,
  '"Quantidade no estoque" difere de "Quantidade tribut�vel"!') then
    Exit;
  if MyObjects.FIC(Uppercase(Trim(QrGraGruXSIGLAUNIDMED.Value)) <>
  Uppercase(Trim(Edprod_uTrib.Text)), EdUnidMedTrib,
  'A unidade de medida da "Quantidade no estoque" difere da "Unidade de Medida tribut�vel"!') then
    Exit;
  //  Fim 2011-05-03
  //
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'nfeitsi', False, [
    'prod_cProd', (*'prod_cEAN', 'prod_xProd',*)
    'prod_NCM', 'prod_EXTIPI', (*'prod_genero',*)
    'prod_CFOP', 'prod_uCom', 'prod_qCom',
    'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
    'prod_vOutro', (*'prod_indTot', 'prod_xPed',
    'prod_nItemPed', 'Tem_IPI', '_Ativo_',
    'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
    'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
    'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
    'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
    'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
    'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
    'MeuID', 'Nivel1', 'GraGruX',
    'UnidMedCom', 'UnidMedTrib'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    prod_cProd, (*prod_cEAN, prod_xProd,*)
    prod_NCM, prod_EXTIPI, (*prod_genero,*)
    prod_CFOP, prod_uCom, prod_qCom,
    prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
    prod_uTrib, prod_qTrib, prod_vUnTrib,
    prod_vFrete, prod_vSeg, prod_vDesc,
    prod_vOutro, (*prod_indTot, prod_xPed,
    prod_nItemPed, Tem_IPI, _Ativo_,
    InfAdCuztm, EhServico, ICMSRec_pRedBC,
    ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
    IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
    IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
    PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
    COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
    MeuID, Nivel1, GraGruX,
    UnidMedCom, UnidMedTrib], [
    FatID, FatNum, Empresa, nItem], True) then
    begin
      ICMS_Orig  := EdICMS_Orig.ValueVariant;
      ICMS_CST   := EdICMS_CST.ValueVariant;
      ICMS_vBC   := EdICMS_vBC.ValueVariant;
      ICMS_pICMS := EdICMS_pICMS.ValueVariant;
      ICMS_vICMS := EdICMS_vICMS.ValueVariant;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'nfeitsn', False, [
      'ICMS_Orig', 'ICMS_CST', (*'ICMS_modBC',
      'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS',
      'ICMS_vICMS'(*, 'ICMS_modBCST', 'ICMS_pMVAST',
      'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
      'ICMS_vICMSST', '_Ativo_', 'ICMS_CSOSN',
      'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
      'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
      'ICMS_vCredICMSSN'*)], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      ICMS_Orig, ICMS_CST, (*ICMS_modBC,
      ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS,
      ICMS_vICMS(*, ICMS_modBCST, ICMS_pMVAST,
      ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
      ICMS_vICMSST, _Ativo_, ICMS_CSOSN,
      ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
      ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
      ICMS_vCredICMSSN*)], [
      FatID, FatNum, Empresa, nItem], True) then
      begin
        IPI_pIPI := EdIPI_pIPI.ValueVariant;
        IPI_vIPI := EdIPI_vIPI.ValueVariant;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'nfeitso', False, [
        (*'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
        'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
        'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',*)
        'IPI_pIPI', 'IPI_vIPI'(*, '_Ativo_'*)], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        (*IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
        IPI_qSelo, IPI_cEnq, IPI_CST,
        IPI_vBC, IPI_qUnid, IPI_vUnid,*)
        IPI_pIPI, IPI_vIPI(*, _Ativo_*)], [
        FatID, FatNum, Empresa, nItem], True) then
        begin
          DataHora := Geral.FDT(FmEntradaCab.QrNFeCabADataFiscal.Value, 1);
          if LaTipo.SQLType = stIns then
          begin
            Tipo     := FatID;
            OriCodi  := FatNum;
            OriCtrl  := nItem;
          end else begin
            Tipo     := FTipo;
            OriCodi  := FOriCodi;
            OriCtrl  := FOriCtrl;
          end;
          IDCtrl   := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', LaTipo.SQLType, FIDCtrl);
          //
          CustoAll := prod_vProd;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'stqmovitsa', False, [
          'DataHora', 'Tipo', 'OriCodi',
          'OriCtrl', 'OriCnta', 'OriPart',
          'Empresa', 'StqCenCad', 'GraGruX',
          'Qtde', 'Pecas', 'Peso',
          'AreaM2', 'AreaP2', (*'FatorClas',
          'QuemUsou', 'Retorno', 'ParTipo',
          'ParCodi', 'DebCtrl', 'SMIMultIns',*)
          'CustoAll'(*, 'ValorAll', 'GrupoBal',
          'Baixa'*)], [
          'IDCtrl'], [
          DataHora, Tipo, OriCodi,
          OriCtrl, OriCnta, OriPart,
          Empresa, StqCenCad, GraGruX,
          Qtde, Pecas, Peso,
          AreaM2, AreaP2, (*FatorClas,
          QuemUsou, Retorno, ParTipo,
          ParCodi, DebCtrl, SMIMultIns,*)
          CustoAll(*, ValorAll, GrupoBal,
          Baixa*)], [
          IDCtrl], False) then
          begin
            DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
            FmEntradaCab.ReopenStqMovIts(IDCtrl);
            Close;
          end;
        end;
      end;
    end;
  except
    FmEntradaCab.ExcluiItem(FatID, FatNum, Empresa, OriCtrl, nItem, False);
    raise;
  end;
end;

procedure TFmUsoConsIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUsoConsIts.CalculaRelacionaveis(Calculo: TCalculoRelacaoValor);
var
  Qtde, Preco, Total: Double;
begin
  Qtde  := Edprod_qTrib.ValueVariant;
  Preco := Edprod_vUnTrib.ValueVariant;
  Total := Edprod_vProd.ValueVariant;
  //
  case Calculo of
    crvQtde:  if Preco = 0 then Qtde := 0 else Qtde := Total / Preco;
    crvPreco: if Qtde = 0 then Preco := 0 else Preco := Total / Qtde;
    crvTotal: Total := Qtde * Preco;
  end;
  //
  case Calculo of
    crvQtde:  Edprod_qTrib.ValueVariant := Qtde;
    crvPreco: Edprod_vUnTrib.ValueVariant := Preco;
    crvTotal: Edprod_vProd.ValueVariant := Total;
  end;
end;

procedure TFmUsoConsIts.CBUnidMedComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger);
end;

procedure TFmUsoConsIts.CBUnidMedTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger);
end;

procedure TFmUsoConsIts.DefineQtde();
begin
  case QrGraGruXGerBxaEstq.Value of
    //0: ? ? ?
    1: EdQtde.ValueVariant := EdPecas.ValueVariant;  // Pe�a
    2: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // m�
    3: EdQtde.ValueVariant := EdPeso.ValueVariant;   // kg
    4: EdQtde.ValueVariant := EdPeso.ValueVariant / 1000;   // t (ton)
    5: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // ft�
    else EdQtde.ValueVariant := 0;
  end;
end;

procedure TFmUsoConsIts.EdAreaM2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmUsoConsIts.EdAreaP2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmUsoConsIts.EdGraGruXChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmUsoConsIts.EdPecasChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmUsoConsIts.EdPesoChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmUsoConsIts.Edprod_CFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Pesq: String;
begin
  if Key = VK_F3 then
  begin
    Pesq := CuringaLoc.CriaForm4('Codigo', 'Nome', 'cfop2003', Dmod.MyDB, '');
    if Pesq <> '' then
      Edprod_CFOP.Text := VAR_NOME_X;
  end;
end;

procedure TFmUsoConsIts.Edprod_cProdEnter(Sender: TObject);
begin
  FTemp_prod_cProd := Edprod_cProd.Text;
end;

procedure TFmUsoConsIts.Edprod_cProdExit(Sender: TObject);
  function CadastraGraGruE(const Fornece: Integer; const prod_cProd: String;
  var GraGruX: Integer): Boolean;
  begin
    Result := False;
    GraGruX := 0;
    if DBCheck.CriaFm(TFmGraGruE_P2, FmGraGruE_P2, afmoNegarComAviso) then
    begin
      FmGraGruE_P2.EdxProd.Text            := '';
      //FmGraGruE_P2.EdxProd.Enabled         := True; nada a ver
      FmGraGruE_P2.EdcProd.Text            := prod_cProd;
      FmGraGruE_P2.FFormChamou             := Name;
      FmGraGruE_P2.EdFornece.ValueVariant  := Fornece;
      FmGraGruE_P2.CBFornece.KeyValue      := Fornece;
      FmGraGruE_P2.ImgTipo.SQLType         := stIns;
{
      // Dados do produto
      FmGraGruE_P2.F_prod_NCM              := QrErrIprod_NCM.Value;
      FmGraGruE_P2.F_prod_EXTIPI           := QrErrIprod_EXTIPI.Value;
      FmGraGruE_P2.F_prod_uCom             := QrErrIprod_uCom.Value;
      FmGraGruE_P2.F_prod_cEAN             := QrErrIprod_cEAN.Value;
      FmGraGruE_P2.F_InfAdProd             := QrErrIInfAdProd.Value;
      FmGraGruE_P2.F_ICMS_CST_A            := FormatFloat('0', QrErrIICMS_Orig.Value);
      FmGraGruE_P2.F_ICMS_CST_B            := FormatFloat('00', QrErrIICMS_CST.Value);
      FmGraGruE_P2.F_IPI_CST               := FormatFloat('00', QrErrIIPI_CST.Value);
      FmGraGruE_P2.F_PIS_CST               := FormatFloat('00', QrErrIPIS_CST.Value);
      FmGraGruE_P2.F_COFINS_CST            := FormatFloat('00', QrErrICOFINS_CST.Value);
      FmGraGruE_P2.F_prod_EXTIPI           := QrErrIprod_EXTIPI.Value;
      FmGraGruE_P2.F_IPI_cEnq              := QrErrIIPI_cEnq.Value;
      // FIM dados do produto
}
      FmGraGruE_P2.ShowModal;
      GraGruX := FmGraGruE_P2.FGraGruX;
      {
      NomeGGX := FmGraGruE_P2.FNomeGGX;
      }
      FmGraGruE_P2.Destroy;
      QrGraGruX.Close;
      QrGraGruX.Open;
      //
      Result := GraGruX <> 0;
    end;
  end;
var
  Fornece, GraGruX(*, Nivel1*): Integer;
  cProd: String;
  Habilita: Boolean;
begin
  if Edprod_cProd.Text <> FTemp_prod_cProd then
  begin
    Fornece := FmEntradaCab.QrNFeCabACodInfoEmit.Value;
    cProd   := Edprod_cProd.Text;
    //
    QrLocGGE.Close;
    QrLocGGE.Params[00].AsInteger := Fornece;
    QrLocGGE.Params[01].AsString  := cProd;
    UMyMod.AbreQuery(QrLocGGE, Dmod.MyDB, 'TFmFormBase.Edprod_cProdExit()');
    //
    if QrLocGGE.RecordCount > 0 then
      GraGruX := QrLocGGEGraGruX.Value
    else
      CadastraGraGruE(Fornece, cProd, GraGruX);
    EdGraGruX.ValueVariant := GraGruX;
    CBGraGruX.KeyValue     := GraGruX;
    Habilita := (GraGruX = 0) and (cProd = '');
    EdGraGruX.Enabled := Habilita;
    CBGraGruX.Enabled := Habilita;
    if not Habilita then
      Edprod_NCM.SetFocus;
  end;
  // N�o preisa! FTemp_prod_cProd := '';
end;

procedure TFmUsoConsIts.Edprod_qTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvQtde);
end;

procedure TFmUsoConsIts.Edprod_uComChange(Sender: TObject);
begin
  if Edprod_uCom.Focused then
    PesquisaPorSigla(Edprod_uCom, EdUnidMedCom, CBUnidMedCom, False);
end;

procedure TFmUsoConsIts.Edprod_uComExit(Sender: TObject);
begin
  PesquisaPorSigla(Edprod_uCom, EdUnidMedCom, CBUnidMedCom, False);
end;

procedure TFmUsoConsIts.Edprod_uComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger)
end;

procedure TFmUsoConsIts.Edprod_uTribChange(Sender: TObject);
begin
  if Edprod_uTrib.Focused then
    PesquisaPorSigla(Edprod_uTrib, EdUnidMedTrib, CBUnidMedTrib, False);
end;

procedure TFmUsoConsIts.Edprod_uTribExit(Sender: TObject);
begin
  PesquisaPorSigla(Edprod_uTrib, EdUnidMedTrib, CBUnidMedTrib, False);
end;

procedure TFmUsoConsIts.Edprod_uTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger)
end;

procedure TFmUsoConsIts.Edprod_vProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvTotal);
end;

procedure TFmUsoConsIts.Edprod_vUnTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CalculaRelacionaveis(crvPreco);
end;

procedure TFmUsoConsIts.EdUnidMedComChange(Sender: TObject);
begin
  if not Edprod_uCom.Focused then
    PesquisaPorCodigo(EdUnidMedCom.ValueVariant, Edprod_uCom);
end;

procedure TFmUsoConsIts.EdUnidMedComKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCom, CBUnidMedCom, dmktfInteger);
end;

procedure TFmUsoConsIts.EdUnidMedTribChange(Sender: TObject);
begin
  if not Edprod_uTrib.Focused then
    PesquisaPorCodigo(EdUnidMedTrib.ValueVariant, Edprod_uTrib);
end;

procedure TFmUsoConsIts.EdUnidMedTribKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedTrib, CBUnidMedTrib, dmktfInteger);
end;

procedure TFmUsoConsIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUsoConsIts.FormCreate(Sender: TObject);
begin
  QrGraGruX.Open;
  QrUnidMedC.Open;
  QrUnidMedT.Open;
  FIDCtrl  := 0;
  FTipo    := 0;
  FOriCodi := 0;
  FOriCtrl := 0;
  FMeuID   := 0;
end;

procedure TFmUsoConsIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmUsoConsIts.PesquisaPorCodigo(UnidMed: Integer;
EdSigla: TdmkEdit);
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsInteger := UnidMed;
  QrPesq2.Open;
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

function TFmUsoConsIts.PesquisaPorSigla(EdSigla, EdUnidMed: TdmkEdit;
CBUnidMed: TdmkDBLookupComboBox; Limpa: Boolean): Integer;
begin
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := EdSigla.Text;
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdUnidMed.ValueVariant <> QrPesq1CodUsu.Value then
      EdUnidMed.ValueVariant := QrPesq1CodUsu.Value;
    if CBUnidMed.KeyValue     <> QrPesq1CodUsu.Value then
      CBUnidMed.KeyValue     := QrPesq1CodUsu.Value;
  end else if Limpa then
    EdSigla.ValueVariant := '';
end;

procedure TFmUsoConsIts.SbGraGruXClick(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdGraGruX, CBGraGruX, QrGraGruX, VAR_CADASTRO,
      'Controle', 'Controle');
    end;
  end;
}
end;

procedure TFmUsoConsIts.SbMedComClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMedCom, CBUnidMedCom, QrUnidMedC, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

procedure TFmUsoConsIts.SbMedTribClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMedTrib, CBUnidMedTrib, QrUnidMedT, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

end.
