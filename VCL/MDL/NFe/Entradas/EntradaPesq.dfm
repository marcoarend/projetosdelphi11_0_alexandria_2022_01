object FmEntradaPesq: TFmEntradaPesq
  Left = 348
  Top = 185
  Caption = 'INN-GERAL-003 :: Pesquisa de Entrada de ???'
  ClientHeight = 578
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 93
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 40
      Height = 13
      Caption = 'Produto:'
    end
    object Label4: TLabel
      Left = 596
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label5: TLabel
      Left = 692
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label6: TLabel
      Left = 560
      Top = 48
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label7: TLabel
      Left = 712
      Top = 48
      Width = 56
      Height = 13
      Caption = 'Nota Fiscal:'
    end
    object Label2: TLabel
      Left = 636
      Top = 48
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object CBFornecedor: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 525
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFornecedores
      TabOrder = 1
      OnKeyDown = CBFornecedorKeyDown
      dmkEditCB = EdFornecedor
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CBGraGru1: TdmkDBLookupComboBox
      Left = 68
      Top = 64
      Width = 485
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraGru1
      TabOrder = 3
      OnKeyDown = CBGraGru1KeyDown
      dmkEditCB = EdGraGru1
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object TPIni: TdmkEditDateTimePicker
      Left = 596
      Top = 20
      Width = 93
      Height = 21
      Date = 37670.000000000000000000
      Time = 0.521748657396528900
      TabOrder = 4
      OnClick = TPIniClick
      OnChange = TPIniChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPFim: TdmkEditDateTimePicker
      Left = 692
      Top = 20
      Width = 93
      Height = 21
      Date = 37670.000000000000000000
      Time = 0.521748657396528900
      TabOrder = 5
      OnClick = TPFimClick
      OnChange = TPFimChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdCodigo: TdmkEdit
      Left = 560
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdCodigoExit
    end
    object EdNF: TdmkEdit
      Left = 712
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdNFExit
    end
    object EdFornecedor: TdmkEditCB
      Left = 12
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdFornecedorChange
      DBLookupComboBox = CBFornecedor
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdGraGru1: TdmkEditCB
      Left = 12
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGru1Change
      DBLookupComboBox = CBGraGru1
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdIDCtrl: TdmkEdit
      Left = 636
      Top = 64
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdCodigoExit
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 141
    Width = 1008
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 1008
      Height = 329
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_FRN'
          Title.Caption = 'Fornecedor'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel1'
          Title.Caption = 'Produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Descri'#231#227'o do produto'
          Width = 325
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nNF'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFiscal'
          Title.Caption = 'Data fiscal'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'C'#243'digo'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 76
          Visible = True
        end>
      Color = clWindow
      DataSource = DsNFs
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_FRN'
          Title.Caption = 'Fornecedor'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel1'
          Title.Caption = 'Produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Descri'#231#227'o do produto'
          Width = 325
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nNF'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFiscal'
          Title.Caption = 'Data fiscal'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'C'#243'digo'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 76
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 343
        Height = 32
        Caption = 'Pesquisa de Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 343
        Height = 32
        Caption = 'Pesquisa de Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 343
        Height = 32
        Caption = 'Pesquisa de Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 470
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 514
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 5
          Top = 1
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'nfa.ide_nNF, nfa.emit_CNPJ, nfa.DataFiscal, '
      'gg1.Nivel1, gg1.Nome NO_PRD, nfa.FatNum, nfa.IDCtrl'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN nfecaba    nfa ON nfa.FatID=smia.Tipo'
      '  AND nfa.FatNum=smia.OriCodi'
      'LEFT JOIN entidades  frn ON frn.Codigo=nfa.CodInfoEmit'
      'WHERE smia.Tipo=151'
      ''
      '')
    Left = 8
    Top = 5
    object QrNFsNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrNFside_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFsemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFsDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrNFsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrNFsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrNFsFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
  end
  object DsNFs: TDataSource
    DataSet = QrNFs
    Left = 36
    Top = 5
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFornecedoresAfterScroll
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Nome'
      'FROM entidades'
      '/*WHERE Fornece2="V"*/ '
      'ORDER BY Nome')
    Left = 24
    Top = 193
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 52
    Top = 193
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Nivel1, Nome'
      'FROM GraGru1'
      'WHERE PrdGrupTip=-2'
      'ORDER BY Nome')
    Left = 24
    Top = 225
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 52
    Top = 225
  end
end
