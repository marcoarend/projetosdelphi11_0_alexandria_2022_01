object FmEntradaCab: TFmEntradaCab
  Left = 368
  Top = 194
  Caption = 'INN-GERAL-001 :: Entrada de ???'
  ClientHeight = 669
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 573
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 164
      Width = 1008
      Height = 224
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 1
      object TabSheet2: TTabSheet
        Caption = 'Estoque'
        object DBGItens: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 196
          Align = alClient
          DataSource = DsStqMovIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'IDCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD'
              Title.Caption = 'Nome do produto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Cor'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tam.'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA'
              Title.Caption = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoAll'
              Title.Caption = 'Custo total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Baixa'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'Area m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorAll'
              Title.Caption = 'ValorTotal'
              Visible = True
            end>
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Itens da NFe '
        ImageIndex = 1
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 196
          Align = alClient
          DataSource = DsNFeItsI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'nItem'
              Title.Caption = 'Item'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_cProd'
              Title.Caption = 'C'#243'd. Fornec.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduz.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_xProd'
              Title.Caption = 'Nome produto'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_CFOP'
              Title.Caption = 'CFOP'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_NCM'
              Title.Caption = 'NCM'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_uCom'
              Title.Caption = 'uCom'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_qCom'
              Title.Caption = 'qCom'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_uTrib'
              Title.Caption = 'uTrib'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_qTrib'
              Title.Caption = 'qTrib'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vProd'
              Title.Caption = 'vProd'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSRec_vBC'
              Title.Caption = 'BC ICMS Rec'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSRec_pAliq'
              Title.Caption = '% ICMS Rec'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSRec_vICMS'
              Title.Caption = 'Val ICMS Rec'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vDesc'
              Title.Caption = 'vDesc'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vFrete'
              Title.Caption = 'vFrete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vOutro'
              Title.Caption = 'vOutro'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vSeg'
              Title.Caption = 'vFrete'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Recupera'#231'ao de impostos e importa'#231#227'o e exporta'#231#227'o de arquivos'
        ImageIndex = 2
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 196
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox6: TGroupBox
            Left = 0
            Top = 76
            Width = 1000
            Height = 68
            Align = alTop
            Caption = ' Importa'#231#227'o: '
            TabOrder = 0
            object Label69: TLabel
              Left = 8
              Top = 20
              Width = 56
              Height = 13
              Caption = 'Importa'#231#227'o:'
              FocusControl = DBEdit35
            end
            object Label77: TLabel
              Left = 228
              Top = 20
              Width = 100
              Height = 13
              Caption = 'Ano M'#234's (AAAAMM):'
              FocusControl = DBEdit46
            end
            object Label79: TLabel
              Left = 384
              Top = 20
              Width = 39
              Height = 13
              Caption = 'Lin. Arq.'
              FocusControl = DBEdit48
            end
            object Label68: TLabel
              Left = 768
              Top = 20
              Width = 48
              Height = 13
              Caption = 'CFOP NF:'
              FocusControl = DBEdit3
            end
            object Label75: TLabel
              Left = 884
              Top = 20
              Width = 49
              Height = 13
              Caption = 'COD_SIT:'
              FocusControl = DBEdit44
            end
            object Label4: TLabel
              Left = 8
              Top = 44
              Width = 69
              Height = 13
              Caption = 'Aliq. ICMS NF:'
              FocusControl = DBEdit2
            end
            object Label71: TLabel
              Left = 124
              Top = 44
              Width = 52
              Height = 13
              Caption = 'Val. isento:'
              FocusControl = DBEdit39
            end
            object Label72: TLabel
              Left = 276
              Top = 44
              Width = 68
              Height = 13
              Caption = 'Val. N'#227'o Trib.:'
              FocusControl = DBEdit41
            end
            object Label74: TLabel
              Left = 444
              Top = 44
              Width = 55
              Height = 13
              Caption = 'Val. Outros:'
              FocusControl = DBEdit43
            end
            object Label76: TLabel
              Left = 600
              Top = 44
              Width = 141
              Height = 13
              Caption = 'Val. abatimento n'#227'o tributado:'
              FocusControl = DBEdit45
            end
            object DBEdit35: TDBEdit
              Left = 68
              Top = 16
              Width = 20
              Height = 21
              DataField = 'Importado'
              DataSource = DsNFeCabA
              TabOrder = 0
            end
            object DBEdit40: TDBEdit
              Left = 88
              Top = 16
              Width = 136
              Height = 21
              DataField = 'IMPORTADO_TXT'
              DataSource = DsNFeCabA
              TabOrder = 1
            end
            object DBEdit46: TDBEdit
              Left = 332
              Top = 16
              Width = 48
              Height = 21
              DataField = 'EFD_INN_AnoMes'
              DataSource = DsNFeCabA
              TabOrder = 2
            end
            object DBEdit48: TDBEdit
              Left = 428
              Top = 16
              Width = 52
              Height = 21
              DataField = 'EFD_INN_LinArq'
              DataSource = DsNFeCabA
              TabOrder = 3
            end
            object DBEdit3: TDBEdit
              Left = 820
              Top = 16
              Width = 56
              Height = 21
              DataField = 'NF_CFOP'
              DataSource = DsNFeCabA
              TabOrder = 4
            end
            object DBEdit44: TDBEdit
              Left = 936
              Top = 16
              Width = 48
              Height = 21
              DataField = 'COD_SIT'
              DataSource = DsNFeCabA
              TabOrder = 5
            end
            object DBEdit2: TDBEdit
              Left = 80
              Top = 40
              Width = 40
              Height = 21
              DataField = 'NF_ICMSAlq'
              DataSource = DsNFeCabA
              TabOrder = 6
            end
            object DBEdit39: TDBEdit
              Left = 180
              Top = 40
              Width = 92
              Height = 21
              DataField = 'NFG_ValIsen'
              DataSource = DsNFeCabA
              TabOrder = 7
            end
            object DBEdit41: TDBEdit
              Left = 348
              Top = 40
              Width = 92
              Height = 21
              DataField = 'NFG_NaoTrib'
              DataSource = DsNFeCabA
              TabOrder = 8
            end
            object DBEdit43: TDBEdit
              Left = 504
              Top = 40
              Width = 92
              Height = 21
              DataField = 'NFG_Outros'
              DataSource = DsNFeCabA
              TabOrder = 9
            end
            object DBEdit45: TDBEdit
              Left = 748
              Top = 40
              Width = 92
              Height = 21
              DataField = 'VL_ABAT_NT'
              DataSource = DsNFeCabA
              TabOrder = 10
            end
          end
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 76
            Align = alTop
            Caption = ' Recupera'#231#227'o de impostos: '
            TabOrder = 1
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 996
              Height = 59
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox8: TGroupBox
                Left = 0
                Top = 0
                Width = 199
                Height = 59
                Align = alLeft
                Caption = ' ICMS: '
                TabOrder = 0
                object Label84: TLabel
                  Left = 4
                  Top = 16
                  Width = 79
                  Height = 13
                  Caption = 'Base de c'#225'lculo:'
                  FocusControl = DBEdit53
                end
                object Label82: TLabel
                  Left = 86
                  Top = 16
                  Width = 31
                  Height = 13
                  Caption = '% Aliq.'
                  FocusControl = DBEdit51
                end
                object Label83: TLabel
                  Left = 124
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                  FocusControl = DBEdit52
                end
                object DBEdit53: TDBEdit
                  Left = 4
                  Top = 32
                  Width = 80
                  Height = 21
                  DataField = 'ICMSRec_vBC'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit51: TDBEdit
                  Left = 86
                  Top = 32
                  Width = 36
                  Height = 21
                  DataField = 'ICMSRec_pAliq'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit52: TDBEdit
                  Left = 124
                  Top = 32
                  Width = 70
                  Height = 21
                  DataField = 'ICMSRec_vICMS'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object GroupBox9: TGroupBox
                Left = 199
                Top = 0
                Width = 199
                Height = 59
                Align = alLeft
                Caption = ' PIS: '
                TabOrder = 1
                object Label78: TLabel
                  Left = 4
                  Top = 16
                  Width = 79
                  Height = 13
                  Caption = 'Base de c'#225'lculo:'
                  FocusControl = DBEdit47
                end
                object Label85: TLabel
                  Left = 86
                  Top = 16
                  Width = 31
                  Height = 13
                  Caption = '% Aliq.'
                  FocusControl = DBEdit54
                end
                object Label86: TLabel
                  Left = 124
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                  FocusControl = DBEdit55
                end
                object DBEdit47: TDBEdit
                  Left = 4
                  Top = 32
                  Width = 80
                  Height = 21
                  DataField = 'PISRec_vBC'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit54: TDBEdit
                  Left = 86
                  Top = 32
                  Width = 36
                  Height = 21
                  DataField = 'PISRec_pAliq'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit55: TDBEdit
                  Left = 124
                  Top = 32
                  Width = 70
                  Height = 21
                  DataField = 'PISRec_vPIS'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object GroupBox10: TGroupBox
                Left = 398
                Top = 0
                Width = 199
                Height = 59
                Align = alLeft
                Caption = ' COFINS: '
                TabOrder = 2
                object Label87: TLabel
                  Left = 4
                  Top = 16
                  Width = 79
                  Height = 13
                  Caption = 'Base de c'#225'lculo:'
                  FocusControl = DBEdit56
                end
                object Label88: TLabel
                  Left = 86
                  Top = 16
                  Width = 31
                  Height = 13
                  Caption = '% Aliq.'
                  FocusControl = DBEdit57
                end
                object Label89: TLabel
                  Left = 124
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                  FocusControl = DBEdit58
                end
                object DBEdit56: TDBEdit
                  Left = 4
                  Top = 32
                  Width = 80
                  Height = 21
                  DataField = 'COFINSRec_vBC'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit57: TDBEdit
                  Left = 86
                  Top = 32
                  Width = 36
                  Height = 21
                  DataField = 'COFINSRec_pAliq'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit58: TDBEdit
                  Left = 124
                  Top = 32
                  Width = 70
                  Height = 21
                  DataField = 'COFINSRec_vCOFINS'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object GroupBox11: TGroupBox
                Left = 597
                Top = 0
                Width = 199
                Height = 59
                Align = alLeft
                Caption = ' IPI: '
                TabOrder = 3
                object Label90: TLabel
                  Left = 4
                  Top = 16
                  Width = 79
                  Height = 13
                  Caption = 'Base de c'#225'lculo:'
                  FocusControl = DBEdit59
                end
                object Label91: TLabel
                  Left = 86
                  Top = 16
                  Width = 31
                  Height = 13
                  Caption = '% Aliq.'
                  FocusControl = DBEdit60
                end
                object Label92: TLabel
                  Left = 124
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                  FocusControl = DBEdit61
                end
                object DBEdit59: TDBEdit
                  Left = 4
                  Top = 32
                  Width = 80
                  Height = 21
                  DataField = 'IPIRec_vBC'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit60: TDBEdit
                  Left = 86
                  Top = 32
                  Width = 36
                  Height = 21
                  DataField = 'IPIRec_pAliq'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit61: TDBEdit
                  Left = 124
                  Top = 32
                  Width = 70
                  Height = 21
                  DataField = 'IPIRec_vIPI'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
              object GroupBox12: TGroupBox
                Left = 796
                Top = 0
                Width = 200
                Height = 59
                Align = alClient
                Caption = ' ICMSST: '
                TabOrder = 4
                object Label93: TLabel
                  Left = 4
                  Top = 16
                  Width = 79
                  Height = 13
                  Caption = 'Base de c'#225'lculo:'
                  FocusControl = DBEdit62
                end
                object Label95: TLabel
                  Left = 124
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                  FocusControl = DBEdit64
                end
                object Label96: TLabel
                  Left = 86
                  Top = 16
                  Width = 31
                  Height = 13
                  Caption = '% Aliq.'
                  FocusControl = DBEdit42
                end
                object DBEdit62: TDBEdit
                  Left = 4
                  Top = 32
                  Width = 80
                  Height = 21
                  DataField = 'ICMSRec_vBCST'
                  DataSource = DsNFeCabA
                  TabOrder = 0
                end
                object DBEdit64: TDBEdit
                  Left = 124
                  Top = 32
                  Width = 69
                  Height = 21
                  DataField = 'ICMSRec_vICMSST'
                  DataSource = DsNFeCabA
                  TabOrder = 1
                end
                object DBEdit42: TDBEdit
                  Left = 86
                  Top = 32
                  Width = 36
                  Height = 21
                  DataField = 'ICMSRec_pAliqST'
                  DataSource = DsNFeCabA
                  TabOrder = 2
                end
              end
            end
          end
          object GroupBox13: TGroupBox
            Left = 0
            Top = 144
            Width = 1000
            Height = 40
            Align = alTop
            Caption = ' Exporta'#231#227'o: '
            TabOrder = 2
            object Label81: TLabel
              Left = 8
              Top = 19
              Width = 86
              Height = 13
              Caption = 'Reg. Export. EFD:'
              FocusControl = DBEdit50
            end
            object DBEdit50: TDBEdit
              Left = 100
              Top = 15
              Width = 56
              Height = 21
              DataField = 'EFD_EXP_REG'
              DataSource = DsNFeCabA
              TabOrder = 0
            end
          end
        end
      end
    end
    object GroupBox14: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 164
      Align = alTop
      Caption = ' Dados gerais da NF: '
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdIDCtrl
      end
      object Label2: TLabel
        Left = 216
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 100
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        FocusControl = DBEdit5
      end
      object Label5: TLabel
        Left = 732
        Top = 68
        Width = 46
        Height = 13
        Caption = 'Natureza:'
        FocusControl = DBEdit4
      end
      object Label10: TLabel
        Left = 552
        Top = 44
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 624
        Top = 44
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
        FocusControl = DBEdit8
      end
      object Label12: TLabel
        Left = 784
        Top = 44
        Width = 32
        Height = 13
        Caption = 'N'#186' NF:'
        FocusControl = DBEdit9
      end
      object Label13: TLabel
        Left = 720
        Top = 20
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
        FocusControl = DBEdit10
      end
      object Label14: TLabel
        Left = 848
        Top = 20
        Width = 32
        Height = 13
        Caption = 'Sa'#237'da:'
        FocusControl = DBEdit11
      end
      object Label16: TLabel
        Left = 80
        Top = 68
        Width = 87
        Height = 13
        Caption = 'Tipo de opera'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label17: TLabel
        Left = 188
        Top = 68
        Width = 80
        Height = 13
        Caption = 'Tipo de emiss'#227'o:'
        FocusControl = DBEdit14
      end
      object Label18: TLabel
        Left = 8
        Top = 68
        Width = 47
        Height = 13
        Caption = 'Ambiente:'
        FocusControl = DBEdit15
      end
      object Label19: TLabel
        Left = 292
        Top = 68
        Width = 103
        Height = 13
        Caption = 'Processo de emiss'#227'o:'
        FocusControl = DBEdit16
      end
      object Label20: TLabel
        Left = 420
        Top = 68
        Width = 34
        Height = 13
        Caption = 'Chave:'
        FocusControl = DBEdit17
      end
      object Label15: TLabel
        Left = 8
        Top = 92
        Width = 46
        Height = 13
        Caption = 'BC ICMS:'
        FocusControl = DBEdit18
      end
      object Label21: TLabel
        Left = 148
        Top = 92
        Width = 56
        Height = 13
        Caption = 'Valor ICMS:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 292
        Top = 92
        Width = 60
        Height = 13
        Caption = 'BCST ICMS:'
        FocusControl = DBEdit20
      end
      object Label23: TLabel
        Left = 440
        Top = 92
        Width = 73
        Height = 13
        Caption = 'Valor ICMS ST:'
        FocusControl = DBEdit21
      end
      object Label24: TLabel
        Left = 600
        Top = 92
        Width = 71
        Height = 13
        Caption = 'Valor produtos:'
        FocusControl = DBEdit22
      end
      object Label25: TLabel
        Left = 756
        Top = 92
        Width = 27
        Height = 13
        Caption = 'Frete:'
        FocusControl = DBEdit23
      end
      object Label26: TLabel
        Left = 872
        Top = 92
        Width = 37
        Height = 13
        Caption = 'Seguro:'
        FocusControl = DBEdit24
      end
      object Label27: TLabel
        Left = 700
        Top = 116
        Width = 76
        Height = 13
        Caption = 'Valor Desconto:'
        FocusControl = DBEdit25
      end
      object Label28: TLabel
        Left = 8
        Top = 116
        Width = 42
        Height = 13
        Caption = 'Valor I.I.:'
        FocusControl = DBEdit26
      end
      object Label29: TLabel
        Left = 132
        Top = 116
        Width = 43
        Height = 13
        Caption = 'Valor IPI:'
        FocusControl = DBEdit27
      end
      object Label30: TLabel
        Left = 264
        Top = 116
        Width = 47
        Height = 13
        Caption = 'Valor PIS:'
        FocusControl = DBEdit28
      end
      object Label31: TLabel
        Left = 400
        Top = 116
        Width = 69
        Height = 13
        Caption = 'Valor COFINS:'
        FocusControl = DBEdit29
      end
      object Label32: TLabel
        Left = 556
        Top = 116
        Width = 59
        Height = 13
        Caption = 'Valor outros:'
        FocusControl = DBEdit30
      end
      object Label33: TLabel
        Left = 864
        Top = 116
        Width = 44
        Height = 13
        Caption = 'Total NF:'
        FocusControl = DBEdit31
      end
      object Label34: TLabel
        Left = 8
        Top = 140
        Width = 36
        Height = 13
        Caption = 'Transp:'
        FocusControl = DBEdit32
      end
      object Label35: TLabel
        Left = 108
        Top = 140
        Width = 54
        Height = 13
        Caption = 'Modo frete:'
        FocusControl = DBEdit33
      end
      object Label36: TLabel
        Left = 180
        Top = 140
        Width = 33
        Height = 13
        Caption = 'Status:'
        FocusControl = DBEdit34
      end
      object Label38: TLabel
        Left = 248
        Top = 140
        Width = 66
        Height = 13
        Caption = 'Data cont'#225'bil:'
        FocusControl = DBEdit36
      end
      object Label39: TLabel
        Left = 886
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Entrada:'
        FocusControl = DBEdit37
      end
      object Label67: TLabel
        Left = 760
        Top = 140
        Width = 43
        Height = 13
        Caption = 'Inclus'#227'o:'
        FocusControl = DBEdFatID
      end
      object Label70: TLabel
        Left = 688
        Top = 44
        Width = 44
        Height = 13
        Caption = 'Subs'#233'rie:'
        FocusControl = DBEdit38
      end
      object DBEdIDCtrl: TdmkDBEdit
        Left = 24
        Top = 16
        Width = 72
        Height = 21
        TabStop = False
        DataField = 'IDCtrl'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 264
        Top = 16
        Width = 45
        Height = 21
        Color = clWhite
        DataField = 'Filial'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 140
        Top = 16
        Width = 72
        Height = 21
        DataField = 'FatNum'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 308
        Top = 16
        Width = 409
        Height = 21
        Color = clWhite
        DataField = 'NO_EMP'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit5: TDBEdit
        Left = 68
        Top = 40
        Width = 56
        Height = 21
        DataField = 'CodInfoEmit'
        DataSource = DsNFeCabA
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 124
        Top = 40
        Width = 425
        Height = 21
        DataField = 'NO_FRN'
        DataSource = DsNFeCabA
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 784
        Top = 64
        Width = 209
        Height = 21
        DataField = 'ide_natOp'
        DataSource = DsNFeCabA
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 592
        Top = 40
        Width = 28
        Height = 21
        DataField = 'MODELO_TXT'
        DataSource = DsNFeCabA
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 656
        Top = 40
        Width = 32
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsNFeCabA
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 820
        Top = 40
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsNFeCabA
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 764
        Top = 16
        Width = 80
        Height = 21
        DataField = 'ide_dEmi'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 884
        Top = 16
        Width = 64
        Height = 21
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 948
        Top = 16
        Width = 44
        Height = 21
        DataField = 'ide_hSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 168
        Top = 64
        Width = 16
        Height = 21
        DataField = 'ide_tpNF'
        DataSource = DsNFeCabA
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 272
        Top = 64
        Width = 16
        Height = 21
        DataField = 'ide_tpEmis'
        DataSource = DsNFeCabA
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 60
        Top = 64
        Width = 16
        Height = 21
        DataField = 'ide_tpAmb'
        DataSource = DsNFeCabA
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 400
        Top = 64
        Width = 16
        Height = 21
        DataField = 'ide_procEmi'
        DataSource = DsNFeCabA
        TabOrder = 16
      end
      object DBEdit17: TDBEdit
        Left = 456
        Top = 64
        Width = 272
        Height = 21
        DataField = 'Id'
        DataSource = DsNFeCabA
        TabOrder = 17
      end
      object DBEdit18: TDBEdit
        Left = 60
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vBC'
        DataSource = DsNFeCabA
        TabOrder = 18
      end
      object DBEdit19: TDBEdit
        Left = 208
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vICMS'
        DataSource = DsNFeCabA
        TabOrder = 19
      end
      object DBEdit20: TDBEdit
        Left = 356
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vBCST'
        DataSource = DsNFeCabA
        TabOrder = 20
      end
      object DBEdit21: TDBEdit
        Left = 516
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vST'
        DataSource = DsNFeCabA
        TabOrder = 21
      end
      object DBEdit22: TDBEdit
        Left = 672
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vProd'
        DataSource = DsNFeCabA
        TabOrder = 22
      end
      object DBEdit23: TDBEdit
        Left = 788
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vFrete'
        DataSource = DsNFeCabA
        TabOrder = 23
      end
      object DBEdit24: TDBEdit
        Left = 912
        Top = 88
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vSeg'
        DataSource = DsNFeCabA
        TabOrder = 24
      end
      object DBEdit25: TDBEdit
        Left = 780
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vDesc'
        DataSource = DsNFeCabA
        TabOrder = 25
      end
      object DBEdit26: TDBEdit
        Left = 48
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vII'
        DataSource = DsNFeCabA
        TabOrder = 26
      end
      object DBEdit27: TDBEdit
        Left = 180
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vIPI'
        DataSource = DsNFeCabA
        TabOrder = 27
      end
      object DBEdit28: TDBEdit
        Left = 316
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vPIS'
        DataSource = DsNFeCabA
        TabOrder = 28
      end
      object DBEdit29: TDBEdit
        Left = 472
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vCOFINS'
        DataSource = DsNFeCabA
        TabOrder = 29
      end
      object DBEdit30: TDBEdit
        Left = 616
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vOutro'
        DataSource = DsNFeCabA
        TabOrder = 30
      end
      object DBEdit31: TDBEdit
        Left = 912
        Top = 112
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vNF'
        DataSource = DsNFeCabA
        TabOrder = 31
      end
      object DBEdit32: TDBEdit
        Left = 48
        Top = 136
        Width = 56
        Height = 21
        DataField = 'CodInfoTrsp'
        DataSource = DsNFeCabA
        TabOrder = 32
      end
      object DBEdit33: TDBEdit
        Left = 164
        Top = 136
        Width = 14
        Height = 21
        DataField = 'ModFrete'
        DataSource = DsNFeCabA
        TabOrder = 33
      end
      object DBEdit34: TDBEdit
        Left = 216
        Top = 136
        Width = 28
        Height = 21
        DataField = 'Status'
        DataSource = DsNFeCabA
        TabOrder = 34
      end
      object DBEdit36: TDBEdit
        Left = 320
        Top = 136
        Width = 68
        Height = 21
        DataField = 'DataFiscal'
        DataSource = DsNFeCabA
        TabOrder = 35
      end
      object DBEdit37: TDBEdit
        Left = 930
        Top = 40
        Width = 64
        Height = 21
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 36
      end
      object DBEdFatID: TDBEdit
        Left = 804
        Top = 136
        Width = 28
        Height = 21
        DataField = 'FatID'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 37
      end
      object DBEdNO_FatID: TDBEdit
        Left = 832
        Top = 136
        Width = 160
        Height = 21
        DataField = 'NO_FATID'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 38
      end
      object DBEdit38: TDBEdit
        Left = 740
        Top = 40
        Width = 43
        Height = 21
        DataField = 'NFG_SubSerie'
        DataSource = DsNFeCabA
        TabOrder = 39
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 509
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 318
        Top = 15
        Width = 688
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 555
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dados NF'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtItem: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItemClick
        end
      end
    end
    object GroupBox15: TGroupBox
      Left = 0
      Top = 448
      Width = 1008
      Height = 61
      Align = alBottom
      Caption = ' Confer'#234'ncia: '
      TabOrder = 3
      object Label100: TLabel
        Left = 100
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Nota     R$:'
      end
      object Label101: TLabel
        Left = 192
        Top = 16
        Width = 42
        Height = 13
        Caption = 'kg bruto:'
      end
      object Label102: TLabel
        Left = 284
        Top = 16
        Width = 33
        Height = 13
        Caption = 'kg l'#237'q.:'
      end
      object Label103: TLabel
        Left = 468
        Top = 16
        Width = 60
        Height = 13
        Caption = 'Pag.F     R$:'
      end
      object Label104: TLabel
        Left = 376
        Top = 16
        Width = 61
        Height = 13
        Caption = 'Pag. T    R$:'
      end
      object DBEdErrNota: TDBEdit
        Left = 100
        Top = 32
        Width = 88
        Height = 21
        DataField = 'ErrNota'
        DataSource = DsUsoCons
        TabOrder = 0
        OnChange = DBEdErrChange
      end
      object DBEdErrBruto: TDBEdit
        Left = 192
        Top = 32
        Width = 88
        Height = 21
        DataField = 'ErrBruto'
        DataSource = DsUsoCons
        TabOrder = 1
        OnChange = DBEdErrChange
      end
      object DBEdErrLiq: TDBEdit
        Left = 284
        Top = 32
        Width = 88
        Height = 21
        DataField = 'ErrLiq'
        DataSource = DsUsoCons
        TabOrder = 2
        OnChange = DBEdErrChange
      end
      object DBEdErrPagT: TDBEdit
        Left = 376
        Top = 32
        Width = 88
        Height = 21
        DataField = 'ErrPagT'
        DataSource = DsUsoCons
        TabOrder = 3
        OnChange = DBEdErrChange
      end
      object DBEdErrPagF: TDBEdit
        Left = 468
        Top = 32
        Width = 88
        Height = 21
        DataField = 'ErrPagF'
        DataSource = DsUsoCons
        TabOrder = 4
        OnChange = DBEdErrChange
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 573
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 465
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 44
        Align = alTop
        Caption = ' Grupo A - Dados da Nota Fiscal: '
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 80
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label40: TLabel
          Left = 188
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label66: TLabel
          Left = 660
          Top = 20
          Width = 60
          Height = 13
          Caption = 'Chave NF-e:'
        end
        object EdIDCtrl: TdmkEdit
          Left = 20
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'IDCtrl'
          UpdCampo = 'IDCtrl'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatNum: TdmkEdit
          Left = 120
          Top = 16
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatNum'
          UpdCampo = 'FatNum'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 236
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Filial'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 276
          Top = 16
          Width = 381
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 3
          dmkEditCB = EdEmpresa
          QryCampo = 'Filial'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNFe_Id: TdmkEdit
          Left = 724
          Top = 16
          Width = 271
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Id'
          UpdCampo = 'Id'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNFe_IdExit
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 44
        Width = 1008
        Height = 68
        Align = alTop
        Caption = ' Grupo B - Identifica'#231#227'o da Nota Fiscal: '
        TabOrder = 1
        object Label9: TLabel
          Left = 4
          Top = 20
          Width = 38
          Height = 13
          Caption = 'Modelo:'
        end
        object Label41: TLabel
          Left = 860
          Top = 20
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label42: TLabel
          Left = 4
          Top = 44
          Width = 57
          Height = 13
          Caption = 'N'#250'mero NF:'
        end
        object Label43: TLabel
          Left = 132
          Top = 44
          Width = 67
          Height = 13
          Caption = 'Data emiss'#227'o:'
        end
        object Label44: TLabel
          Left = 312
          Top = 44
          Width = 65
          Height = 13
          Caption = 'Data entrada:'
        end
        object Label45: TLabel
          Left = 679
          Top = 44
          Width = 140
          Height = 13
          Caption = 'Data Fiscal (data da entrada):'
          Enabled = False
        end
        object Label46: TLabel
          Left = 488
          Top = 44
          Width = 65
          Height = 13
          Caption = 'Hora entrada:'
        end
        object Edide_mod: TdmkEditCB
          Left = 44
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ide_mod'
          UpdCampo = 'ide_mod'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBide_mod
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBide_mod: TdmkDBLookupComboBox
          Left = 100
          Top = 16
          Width = 757
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsNFMoDocFis
          TabOrder = 1
          dmkEditCB = Edide_mod
          QryCampo = 'ide_mod'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNFG_Serie: TdmkEdit
          Left = 888
          Top = 16
          Width = 40
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 3
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NFG_Serie'
          UpdCampo = 'NFG_Serie'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Edide_nNF: TdmkEdit
          Left = 64
          Top = 40
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ide_nNF'
          UpdCampo = 'ide_nNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPide_dEmi: TdmkEditDateTimePicker
          Left = 200
          Top = 40
          Width = 105
          Height = 21
          Date = 1.000000000000000000
          Time = 1.000000000000000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ide_dEmi'
          UpdCampo = 'ide_dEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPide_dSaiEnt: TdmkEditDateTimePicker
          Left = 380
          Top = 40
          Width = 105
          Height = 21
          Date = 1.000000000000000000
          Time = 1.000000000000000000
          TabOrder = 5
          OnClick = TPide_dSaiEntClick
          OnChange = TPide_dSaiEntChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ide_dSaiEnt'
          UpdCampo = 'ide_dSaiEnt'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDataFiscal: TdmkEditDateTimePicker
          Left = 823
          Top = 40
          Width = 105
          Height = 21
          Date = 1.000000000000000000
          Time = 0.688972615738749000
          Enabled = False
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataFiscal'
          UpdCampo = 'DataFiscal'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object Edide_hSaiEnt: TdmkEdit
          Left = 556
          Top = 40
          Width = 49
          Height = 21
          TabOrder = 6
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'ide_hSaiEnt'
          UpdCampo = 'ide_hSaiEnt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGide_tpNF: TdmkRadioGroup
          Left = 936
          Top = 15
          Width = 70
          Height = 51
          Align = alRight
          Caption = ' Opera'#231#227'o: '
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Entrada'
            'Sa'#237'da')
          TabOrder = 8
          QryCampo = 'ide_tpNF'
          UpdCampo = 'ide_tpNF'
          UpdType = utYes
          OldValor = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 112
        Width = 1008
        Height = 44
        Align = alTop
        Caption = ' Grupo C - Identifica'#231#227'o do emitente: '
        TabOrder = 2
        object Label37: TLabel
          Left = 8
          Top = 20
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
        end
        object SpeedButton5: TSpeedButton
          Left = 851
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object EdCodInfoEmit: TdmkEditCB
          Left = 68
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodInfoEmit'
          UpdCampo = 'CodInfoEmit'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodInfoEmitChange
          DBLookupComboBox = CBCodInfoEmit
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCodInfoEmit: TdmkDBLookupComboBox
          Left = 124
          Top = 16
          Width = 725
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsFornece
          TabOrder = 1
          dmkEditCB = EdCodInfoEmit
          QryCampo = 'CodInfoEmit'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object DBEdCNPJ_CPF_TXT_Fornece: TDBEdit
          Left = 876
          Top = 16
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'CNPJ_CPF_TXT'
          DataSource = DsFornece
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 156
        Width = 1008
        Height = 65
        Align = alTop
        Caption = ' Grupo W - Valores totais da NF: '
        TabOrder = 3
        object Label47: TLabel
          Left = 8
          Top = 20
          Width = 46
          Height = 13
          Caption = 'BC ICMS:'
        end
        object Label48: TLabel
          Left = 152
          Top = 20
          Width = 56
          Height = 13
          Caption = 'Valor ICMS:'
        end
        object Label49: TLabel
          Left = 300
          Top = 20
          Width = 63
          Height = 13
          Caption = 'BC ICMS ST:'
        end
        object Label50: TLabel
          Left = 452
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Valor ICMS ST:'
        end
        object Label51: TLabel
          Left = 612
          Top = 20
          Width = 45
          Height = 13
          Caption = 'Produtos:'
        end
        object Label52: TLabel
          Left = 748
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Frete:'
        end
        object Label53: TLabel
          Left = 868
          Top = 20
          Width = 37
          Height = 13
          Caption = 'Seguro:'
        end
        object Label54: TLabel
          Left = 4
          Top = 44
          Width = 49
          Height = 13
          Caption = 'Desconto:'
        end
        object Label55: TLabel
          Left = 152
          Top = 44
          Width = 36
          Height = 13
          Caption = 'Total II:'
        end
        object Label56: TLabel
          Left = 276
          Top = 44
          Width = 43
          Height = 13
          Caption = 'Total IPI:'
        end
        object Label57: TLabel
          Left = 408
          Top = 44
          Width = 47
          Height = 13
          Caption = 'Total PIS:'
        end
        object Label58: TLabel
          Left = 548
          Top = 44
          Width = 69
          Height = 13
          Caption = 'Total COFINS:'
        end
        object Label59: TLabel
          Left = 708
          Top = 44
          Width = 59
          Height = 13
          Caption = 'Total outros:'
        end
        object Label60: TLabel
          Left = 860
          Top = 44
          Width = 44
          Height = 13
          Caption = 'Total NF:'
        end
        object EdICMSTot_vBC: TdmkEdit
          Left = 60
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vBC'
          UpdCampo = 'ICMSTot_vBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vICMS: TdmkEdit
          Left = 212
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vICMS'
          UpdCampo = 'ICMSTot_vICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vBCST: TdmkEdit
          Left = 368
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vBCST'
          UpdCampo = 'ICMSTot_vBCST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vST: TdmkEdit
          Left = 528
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vST'
          UpdCampo = 'ICMSTot_vST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vProd: TdmkEdit
          Left = 660
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vProd'
          UpdCampo = 'ICMSTot_vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vFrete: TdmkEdit
          Left = 780
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vFrete'
          UpdCampo = 'ICMSTot_vFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vSeg: TdmkEdit
          Left = 908
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vSeg'
          UpdCampo = 'ICMSTot_vSeg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vDesc: TdmkEdit
          Left = 60
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vDesc'
          UpdCampo = 'ICMSTot_vDesc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vII: TdmkEdit
          Left = 192
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vII'
          UpdCampo = 'ICMSTot_vII'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vIPI: TdmkEdit
          Left = 320
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vIPI'
          UpdCampo = 'ICMSTot_vIPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vPIS: TdmkEdit
          Left = 460
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vPIS'
          UpdCampo = 'ICMSTot_vPIS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vCOFINS: TdmkEdit
          Left = 620
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vCOFINS'
          UpdCampo = 'ICMSTot_vCOFINS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vOutro: TdmkEdit
          Left = 772
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vOutro'
          UpdCampo = 'ICMSTot_vOutro'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vNF: TdmkEdit
          Left = 908
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vNF'
          UpdCampo = 'ICMSTot_vNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 221
        Width = 1008
        Height = 92
        Align = alTop
        Caption = ' Grupo X - Informa'#231#245'es do transporte da NF: '
        TabOrder = 4
        object Label163: TLabel
          Left = 8
          Top = 20
          Width = 97
          Height = 13
          Caption = 'Modalidade do frete:'
          FocusControl = EdModFrete
        end
        object Label61: TLabel
          Left = 316
          Top = 20
          Width = 69
          Height = 13
          Caption = 'Transportador:'
        end
        object SpeedButton6: TSpeedButton
          Left = 851
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label62: TLabel
          Left = 8
          Top = 44
          Width = 79
          Height = 13
          Caption = 'Valor do servi'#231'o:'
        end
        object Label63: TLabel
          Left = 176
          Top = 44
          Width = 46
          Height = 13
          Caption = 'BC ICMS:'
        end
        object Label64: TLabel
          Left = 308
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Aliq. Reten'#231#227'o:'
        end
        object Label65: TLabel
          Left = 428
          Top = 44
          Width = 100
          Height = 13
          Caption = 'Valor do ICMS retido:'
        end
        object Label174: TLabel
          Left = 616
          Top = 44
          Width = 31
          Height = 13
          Caption = 'CFOP:'
          FocusControl = EdRetTransp_CFOP
        end
        object Label175: TLabel
          Left = 8
          Top = 69
          Width = 352
          Height = 13
          Caption = 
            'C'#243'digo do munic'#237'pio de ocorr'#234'ncia do fato gerador do ICMS do tra' +
            'nsporte:'
          FocusControl = EdRetTransp_CMunFG
        end
        object EdModFrete: TdmkEdit
          Left = 108
          Top = 16
          Width = 20
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '9'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ModFrete'
          UpdCampo = 'ModFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdModFreteChange
        end
        object EdModFrete_TXT: TdmkEdit
          Left = 128
          Top = 16
          Width = 185
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'Por conta do emitente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'Por conta do emitente'
          ValWarn = False
        end
        object EdCodInfoTrsp: TdmkEditCB
          Left = 388
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodInfoTrsp'
          UpdCampo = 'CodInfoTrsp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodInfoTrspChange
          DBLookupComboBox = CBCodInfoTrsp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCodInfoTrsp: TdmkDBLookupComboBox
          Left = 444
          Top = 16
          Width = 405
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTransporta
          TabOrder = 3
          dmkEditCB = EdCodInfoTrsp
          QryCampo = 'CodInfoTrsp'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdRetTransp_vServ: TdmkEdit
          Left = 92
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vServ'
          UpdCampo = 'RetTransp_vServ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_vBCRet: TdmkEdit
          Left = 224
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vBCRet'
          UpdCampo = 'RetTransp_vBCRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_PICMSRet: TdmkEdit
          Left = 388
          Top = 40
          Width = 36
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_PICMSRet'
          UpdCampo = 'RetTransp_PICMSRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_vICMSRet: TdmkEdit
          Left = 532
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vICMSRet'
          UpdCampo = 'RetTransp_vICMSRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_CFOP: TdmkEdit
          Left = 648
          Top = 40
          Width = 56
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'RetTransp_CFOP'
          UpdCampo = 'RetTransp_CFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRetTransp_CMunFG: TdmkEdit
          Left = 368
          Top = 65
          Width = 56
          Height = 21
          Alignment = taRightJustify
          MaxLength = 7
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'RetTransp_CMunFG'
          UpdCampo = 'RetTransp_CMunFG'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdRetTransp_CMunFGChange
        end
        object EdRetTransp_CMunFG_TXT: TdmkEditF7
          Left = 428
          Top = 65
          Width = 560
          Height = 21
          ReadOnly = True
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          dmkEditCod = EdRetTransp_CMunFG
          LocF7CodiFldName = 'Codigo'
          LocF7NameFldName = 'Nome'
          LocF7TableName = 'dtb_munici'
        end
        object DBEdCNPJ_CPF_TXT_Transporta: TDBEdit
          Left = 876
          Top = 16
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'CNPJ_CPF_TXT'
          DataSource = DsTransporta
          TabOrder = 4
        end
      end
      object GBOutros: TGroupBox
        Left = 0
        Top = 313
        Width = 1008
        Height = 105
        Align = alTop
        Caption = ' Outras informa'#231#245'es:'
        TabOrder = 5
        object PnBlDrm: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 88
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label73: TLabel
            Left = 8
            Top = 4
            Width = 35
            Height = 13
            Caption = 'NF RP:'
          end
          object Label80: TLabel
            Left = 80
            Top = 4
            Width = 34
            Height = 13
            Caption = 'NF CC:'
          end
          object Label94: TLabel
            Left = 152
            Top = 4
            Width = 43
            Height = 13
            Caption = 'Conhec.:'
          end
          object Label97: TLabel
            Left = 248
            Top = 4
            Width = 106
            Height = 13
            Caption = 'Valor Frete conhecim.:'
          end
          object Label98: TLabel
            Left = 364
            Top = 5
            Width = 61
            Height = 13
            Caption = 'Qtde l'#237'quido:'
          end
          object Label99: TLabel
            Left = 460
            Top = 5
            Width = 43
            Height = 13
            Caption = 'kg Bruto:'
          end
          object Label105: TLabel
            Left = 556
            Top = 5
            Width = 157
            Height = 13
            Caption = 'Cliente interno (dono do material):'
          end
          object EdNF_RP: TdmkEdit
            Left = 8
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNF_CC: TdmkEdit
            Left = 80
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdCampo = 'NF_CC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConhecimento: TdmkEdit
            Left = 152
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFrete: TdmkEdit
            Left = 248
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPesoL: TdmkEdit
            Left = 364
            Top = 21
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPesoB: TdmkEdit
            Left = 460
            Top = 21
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCodInfoCliI: TdmkEditCB
            Left = 556
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CodInfoCliI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodInfoCliI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCodInfoCliI: TdmkDBLookupComboBox
            Left = 616
            Top = 21
            Width = 368
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOME'
            ListSource = DsCI
            TabOrder = 7
            dmkEditCB = EdCodInfoCliI
            QryCampo = 'CodInfoCliI'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 509
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 860
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 186
        Height = 32
        Caption = 'Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 186
        Height = 32
        Caption = 'Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 186
        Height = 32
        Caption = 'Entrada de ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 16
    Top = 60
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeCabABeforeOpen
    AfterOpen = QrNFeCabAAfterOpen
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, '
      'emp.Filial, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'IF(frn.Tipo=0, frn.CNPJ, frn.CPF) CNPJ_CPF_FRN,'
      'nfa.*'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades frn ON frn.Codigo=nfa.CodInfoEmit'
      'LEFT JOIN entidades emp ON emp.Codigo=nfa.Empresa'
      '')
    Left = 12
    Top = 12
    object QrNFeCabAICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Origin = 'nfecaba.ICMSRec_pAliqST'
    end
    object QrNFeCabAFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
      Origin = 'nfecaba.CodInfoEmit'
    end
    object QrNFeCabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
      Origin = 'nfecaba.CodInfoDest'
    end
    object QrNFeCabANO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrNFeCabANO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfecaba.FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfecaba.FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfecaba.Empresa'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfecaba.IDCtrl'
    end
    object QrNFeCabANO_FATID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_FATID'
      Size = 25
      Calculated = True
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfecaba.Id'
      Size = 44
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'nfecaba.ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'nfecaba.ide_mod'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'nfecaba.ide_serie'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Origin = 'nfecaba.ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'nfecaba.ide_dEmi'
    end
    object QrNFeCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      Origin = 'nfecaba.ide_dSaiEnt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeCabAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
      Origin = 'nfecaba.ide_hSaiEnt'
      DisplayFormat = 'hh:nn'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'nfecaba.ide_tpEmis'
    end
    object QrNFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'nfecaba.ide_tpAmb'
    end
    object QrNFeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
      Origin = 'nfecaba.ide_procEmi'
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Origin = 'nfecaba.ICMSTot_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Origin = 'nfecaba.ICMSTot_vICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Origin = 'nfecaba.ICMSTot_vBCST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Origin = 'nfecaba.ICMSTot_vST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Origin = 'nfecaba.ICMSTot_vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Origin = 'nfecaba.ICMSTot_vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Origin = 'nfecaba.ICMSTot_vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Origin = 'nfecaba.ICMSTot_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Origin = 'nfecaba.ICMSTot_vII'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Origin = 'nfecaba.ICMSTot_vIPI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Origin = 'nfecaba.ICMSTot_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Origin = 'nfecaba.ICMSTot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Origin = 'nfecaba.ICMSTot_vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Origin = 'nfecaba.ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
      Origin = 'nfecaba.CodInfoTrsp'
    end
    object QrNFeCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Origin = 'nfecaba.ModFrete'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrNFeCabADataFiscal: TDateField
      FieldName = 'DataFiscal'
      Origin = 'nfecaba.DataFiscal'
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrNFeCabANFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Origin = 'nfecaba.NFG_Serie'
      Size = 3
    end
    object QrNFeCabARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
      Origin = 'nfecaba.RetTransp_vServ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
      Origin = 'nfecaba.RetTransp_vBCRet'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
      Origin = 'nfecaba.RetTransp_PICMSRet'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
      Origin = 'nfecaba.RetTransp_vICMSRet'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Origin = 'nfecaba.RetTransp_CFOP'
      Size = 5
    end
    object QrNFeCabARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Origin = 'nfecaba.RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeCabACNPJ_CPF_FRN: TWideStringField
      FieldName = 'CNPJ_CPF_FRN'
      Size = 18
    end
    object QrNFeCabACNPJ_CPF_FRN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_FRN_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
      Origin = 'nfecaba.CriAForca'
    end
    object QrNFeCabANF_ICMSAlq: TFloatField
      DefaultExpression = '#,###,##0.00;-#,###,##0.00; '
      FieldName = 'NF_ICMSAlq'
      Origin = 'nfecaba.NF_ICMSAlq'
    end
    object QrNFeCabANF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Origin = 'nfecaba.NF_CFOP'
      Size = 4
    end
    object QrNFeCabAImportado: TSmallintField
      FieldName = 'Importado'
      Origin = 'nfecaba.Importado'
    end
    object QrNFeCabANFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Origin = 'nfecaba.NFG_SubSerie'
      Size = 3
    end
    object QrNFeCabANFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
      Origin = 'nfecaba.NFG_ValIsen'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabANFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
      Origin = 'nfecaba.NFG_NaoTrib'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabANFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
      Origin = 'nfecaba.NFG_Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'nfecaba.COD_MOD'
      Size = 2
    end
    object QrNFeCabACOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Origin = 'nfecaba.COD_SIT'
    end
    object QrNFeCabAVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Origin = 'nfecaba.VL_ABAT_NT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAEFD_INN_AnoMes: TIntegerField
      FieldName = 'EFD_INN_AnoMes'
      Origin = 'nfecaba.EFD_INN_AnoMes'
      DisplayFormat = '0;-0; '
    end
    object QrNFeCabAEFD_INN_Empresa: TIntegerField
      FieldName = 'EFD_INN_Empresa'
      Origin = 'nfecaba.EFD_INN_Empresa'
      DisplayFormat = '0;-0; '
    end
    object QrNFeCabAEFD_INN_LinArq: TIntegerField
      FieldName = 'EFD_INN_LinArq'
      Origin = 'nfecaba.EFD_INN_LinArq'
      DisplayFormat = '0;-0; '
    end
    object QrNFeCabAICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Origin = 'nfecaba.ICMSRec_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Origin = 'nfecaba.ICMSRec_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAEFD_EXP_REG: TWideStringField
      FieldName = 'EFD_EXP_REG'
      Origin = 'nfecaba.EFD_EXP_REG'
      Size = 4
    end
    object QrNFeCabAMODELO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MODELO_TXT'
      Size = 2
      Calculated = True
    end
    object QrNFeCabAIMPORTADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IMPORTADO_TXT'
      Size = 15
      Calculated = True
    end
    object QrNFeCabAICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Origin = 'nfecaba.ICMSRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Origin = 'nfecaba.ICMSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Origin = 'nfecaba.ICMSRec_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Origin = 'nfecaba.IPIRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Origin = 'nfecaba.IPIRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Origin = 'nfecaba.IPIRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Origin = 'nfecaba.IPIRec_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Origin = 'nfecaba.PISRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Origin = 'nfecaba.PISRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Origin = 'nfecaba.PISRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Origin = 'nfecaba.PISRec_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Origin = 'nfecaba.COFINSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Origin = 'nfecaba.COFINSRec_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Origin = 'nfecaba.COFINSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Origin = 'nfecaba.COFINSRec_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabAICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Origin = 'nfecaba.ICMSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNFeCabACodInfoCliI: TIntegerField
      FieldName = 'CodInfoCliI'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel7
    CanUpd01 = BtInclui
    Left = 56
    Top = 36
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrStqMovItsBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'pgt.Nome NO_PGT, IF(smia.UnidMed=-1,'
      'mec.Sigla, mei.Sigla) SIGLA, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    mec ON mec.Codigo=gg1.UnidMed'
      'LEFT JOIN unidmed    mei ON mei.Codigo=smia.UnidMed'
      'WHERE smia.Tipo IN (51,151)'
      'AND OriCodi=23'
      ''
      'UNION'
      ''
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'pgt.Nome NO_PGT, IF(smia.UnidMed=-1,'
      'mec.Sigla, mei.Sigla) SIGLA, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*'
      'FROM stqmovitsb smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    mec ON mec.Codigo=gg1.UnidMed'
      'LEFT JOIN unidmed    mei ON mei.Codigo=smia.UnidMed'
      'WHERE smia.Tipo IN (51,151)'
      'AND OriCodi=23'
      ''
      '')
    Left = 96
    Top = 12
    object QrStqMovItsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrStqMovItsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 50
    end
    object QrStqMovItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrStqMovItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrStqMovItsNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrStqMovItsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrStqMovItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrStqMovItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrStqMovItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrStqMovItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrStqMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrStqMovItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrStqMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrStqMovItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqMovItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrStqMovItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqMovItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqMovItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqMovItsOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrStqMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrStqMovItsQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrStqMovItsRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrStqMovItsParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrStqMovItsParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrStqMovItsDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrStqMovItsSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrStqMovItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsValorAll: TFloatField
      FieldName = 'ValorAll'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrStqMovItsBaixa: TSmallintField
      FieldName = 'Baixa'
    end
  end
  object DsStqMovIts: TDataSource
    DataSet = QrStqMovIts
    Left = 92
    Top = 60
  end
  object QrNFMoDocFis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfmodocfis'
      'ORDER BY Nome')
    Left = 312
    Top = 40
    object QrNFMoDocFisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFMoDocFisNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNFMoDocFis: TDataSource
    DataSet = QrNFMoDocFis
    Left = 312
    Top = 84
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrForneceCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOME,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Fornece7="V"'
      'OR Fornece8="V"'
      'ORDER BY Nome')
    Left = 396
    Top = 40
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrForneceCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrForneceCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 396
    Top = 88
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransportaCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOME,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF'
      'FROM entidades'
      'WHERE Fornece3='#39'V'#39
      'ORDER BY Nome')
    Left = 448
    Top = 20
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrTransportaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrTransportaCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 448
    Top = 64
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 368
    Top = 596
    object IncluinovaNF1: TMenuItem
      Caption = '&Inclui nova NF'
      object Manualmente1: TMenuItem
        Caption = '&Manualmente'
        OnClick = Manualmente1Click
      end
      object porXML1: TMenuItem
        Caption = '&por &XML'
        Enabled = False
        OnClick = porXML1Click
      end
    end
    object AlteraNF1: TMenuItem
      Caption = '&Altera NF'
      Enabled = False
      OnClick = AlteraNF1Click
    end
  end
  object PMItem: TPopupMenu
    OnPopup = PMItemPopup
    Left = 496
    Top = 588
    object Incluinovoitem1: TMenuItem
      Caption = '&Inclui novo item'
      OnClick = Incluinovoitem1Click
    end
    object AlteraItemAtual1: TMenuItem
      Caption = '&Altera Item Atual'
      object EstoqueeFiscal1: TMenuItem
        Caption = 'Estoque e Fiscal'
        OnClick = EstoqueeFiscal1Click
      end
      object SomenteFiscal1: TMenuItem
        Caption = 'Somente Fiscal'
        OnClick = SomenteFiscal1Click
      end
    end
    object ExcluiitemAtual1: TMenuItem
      Caption = '&Exclui item Atual'
      object EstoqueeFiscal2: TMenuItem
        Caption = 'Estoque e Fiscal'
        OnClick = EstoqueeFiscal2Click
      end
    end
  end
  object QrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND MeuID=:P3')
    Left = 188
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
    end
    object QrIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
    end
    object QrIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
  end
  object QrN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 220
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
  end
  object QrO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 256
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrOIPI_CST: TIntegerField
      FieldName = 'IPI_CST'
    end
    object QrOIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
  end
  object PMNumero: TPopupMenu
    Left = 144
    Top = 36
    object Cdigo1: TMenuItem
      Caption = '&C'#243'digo'
      OnClick = Cdigo1Click
    end
    object ID1: TMenuItem
      Caption = '&ID'
      OnClick = ID1Click
    end
  end
  object DsNFeItsI: TDataSource
    DataSet = QrNFeItsI
    Left = 504
    Top = 88
  end
  object QrNFeItsI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeItsIBeforeClose
    AfterScroll = QrNFeItsIAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 504
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeItsIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFeItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrNFeItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrNFeItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFeItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFeItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrNFeItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrNFeItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFeItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrNFeItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrNFeItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrNFeItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFeItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrNFeItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrNFeItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrNFeItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrNFeItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrNFeItsIEhServico: TIntegerField
      FieldName = 'EhServico'
    end
    object QrNFeItsILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrNFeItsINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrNFeItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,###,##0.00;#,###,###,##0.00; '
    end
    object QrNFeItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFeItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
    end
    object QrNFeItsIICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
    end
    object QrNFeItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
    end
  end
  object QrNFeItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsn nfen'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 552
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFeItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNFeItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNFeItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNFeItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNFeItsN: TDataSource
    DataSet = QrNFeItsN
    Left = 552
    Top = 72
  end
  object QrNFeItsO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitso nfeo'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 596
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrNFeItsOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrNFeItsOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrNFeItsOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrNFeItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrNFeItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrNFeItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNFeItsO: TDataSource
    DataSet = QrNFeItsO
    Left = 604
    Top = 92
  end
  object QrNFeItsP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsp nfep'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 644
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsPII_vBC: TFloatField
      FieldName = 'II_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vDespAdu: TFloatField
      FieldName = 'II_vDespAdu'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vII: TFloatField
      FieldName = 'II_vII'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsPII_vIOF: TFloatField
      FieldName = 'II_vIOF'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNFeItsP: TDataSource
    DataSet = QrNFeItsP
    Left = 648
    Top = 72
  end
  object QrNFeItsQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsq nfeq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 688
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrNFeItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFeItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrNFeItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,###,##0.0000'
    end
  end
  object DsNFeItsQ: TDataSource
    DataSet = QrNFeItsQ
    Left = 688
    Top = 44
  end
  object QrNFeItsR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsr nfer'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 780
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrNFeItsRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrNFeItsRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrNFeItsRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrNFeItsRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrNFeItsRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
  end
  object DsNFeItsR: TDataSource
    DataSet = QrNFeItsR
    Left = 780
    Top = 44
  end
  object QrNFeItsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitss nfes'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 736
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrNFeItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFeItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrNFeItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsNFeItsS: TDataSource
    DataSet = QrNFeItsS
    Left = 736
    Top = 72
  end
  object QrNFeItsT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitst nfet'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 824
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrNFeItsTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrNFeItsTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrNFeItsTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrNFeItsTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrNFeItsTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
  end
  object DsNFeItsT: TDataSource
    DataSet = QrNFeItsT
    Left = 824
    Top = 64
  end
  object QrNFeItsV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsv nfev'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3'
      '')
    Left = 876
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsNFeItsV: TDataSource
    DataSet = QrNFeItsV
    Left = 876
    Top = 52
  end
  object QrUsoCons: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ErrNota, ErrBruto, ErrLiq, ErrPagT, ErrPagF'
      'FROM pqe'
      'WHERE FatID<>0'
      'AND FatNum<>0'
      'AND Empresa <> 0')
    Left = 192
    Top = 368
    object QrUsoConsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsoConsErrNota: TFloatField
      FieldName = 'ErrNota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrUsoConsErrBruto: TFloatField
      FieldName = 'ErrBruto'
      DisplayFormat = '#,###,##0.000'
    end
    object QrUsoConsErrLiq: TFloatField
      FieldName = 'ErrLiq'
      DisplayFormat = '#,###,##0.000'
    end
    object QrUsoConsErrPagT: TFloatField
      FieldName = 'ErrPagT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrUsoConsErrPagF: TFloatField
      FieldName = 'ErrPagF'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsUsoCons: TDataSource
    DataSet = QrUsoCons
    Left = 192
    Top = 416
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 920
    Top = 32
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 920
    Top = 80
  end
end
