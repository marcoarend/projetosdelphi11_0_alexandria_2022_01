object FmUsoConsIts: TFmUsoConsIts
  Left = 339
  Top = 185
  Caption = 'USO-ENTRA-002 :: Item de Entrada de Uso e Consumo'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton3: TSpeedButton
    Left = 975
    Top = 39
    Width = 21
    Height = 21
    Caption = '...'
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label20: TLabel
      Left = 416
      Top = 16
      Width = 163
      Height = 13
      Caption = #185' : Calcula ao pressionar a tecla F4'
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Item de Entrada de Uso e Consumo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 924
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 406
    Align = alClient
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 1006
      Height = 44
      Align = alTop
      Caption = ' Grupo A - Dados da Nota Fiscal: '
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 92
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label40: TLabel
        Left = 208
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label66: TLabel
        Left = 660
        Top = 20
        Width = 60
        Height = 13
        Caption = 'Chave NF-e:'
      end
      object DBEdit1: TDBEdit
        Left = 132
        Top = 16
        Width = 72
        Height = 21
        TabStop = False
        DataField = 'FatNum'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdIDCtrl: TdmkDBEdit
        Left = 20
        Top = 16
        Width = 68
        Height = 21
        TabStop = False
        DataField = 'IDCtrl'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 256
        Top = 16
        Width = 40
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Filial'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 296
        Top = 16
        Width = 360
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'NO_EMP'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit17: TDBEdit
        Left = 724
        Top = 16
        Width = 272
        Height = 21
        TabStop = False
        DataField = 'Id'
        DataSource = DsNFeCabA
        TabOrder = 4
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 45
      Width = 1006
      Height = 44
      Align = alTop
      Caption = ' Grupo B - Identifica'#231#227'o da Nota Fiscal: '
      TabOrder = 1
      object Label9: TLabel
        Left = 4
        Top = 20
        Width = 38
        Height = 13
        Caption = 'Modelo:'
      end
      object Label41: TLabel
        Left = 108
        Top = 20
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label42: TLabel
        Left = 184
        Top = 20
        Width = 57
        Height = 13
        Caption = 'N'#250'mero NF:'
      end
      object Label43: TLabel
        Left = 316
        Top = 20
        Width = 67
        Height = 13
        Caption = 'Data emiss'#227'o:'
      end
      object Label44: TLabel
        Left = 456
        Top = 20
        Width = 103
        Height = 13
        Caption = 'Data sa'#237'da / entrada:'
      end
      object Label45: TLabel
        Left = 795
        Top = 20
        Width = 130
        Height = 13
        Caption = 'Data Fiscal (chegada aqui):'
      end
      object Label46: TLabel
        Left = 632
        Top = 20
        Width = 103
        Height = 13
        Caption = 'Hora sa'#237'da / entrada:'
      end
      object DBEdit2: TDBEdit
        Left = 48
        Top = 16
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'ide_mod'
        DataSource = DsNFeCabA
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 136
        Top = 16
        Width = 40
        Height = 21
        TabStop = False
        DataField = 'NFG_Serie'
        DataSource = DsNFeCabA
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 244
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_nNF'
        DataSource = DsNFeCabA
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 384
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_dEmi'
        DataSource = DsNFeCabA
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 560
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 736
        Top = 16
        Width = 48
        Height = 21
        TabStop = False
        DataField = 'ide_hSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 932
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'DataFiscal'
        DataSource = DsNFeCabA
        TabOrder = 6
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 89
      Width = 1006
      Height = 44
      Align = alTop
      Caption = ' Grupo C - Identifica'#231#227'o do emitente: '
      TabOrder = 2
      object Label37: TLabel
        Left = 8
        Top = 20
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object DBEdCNPJ_CPF_TXT_Fornece: TDBEdit
        Left = 884
        Top = 16
        Width = 112
        Height = 21
        TabStop = False
        DataField = 'CNPJ_CPF_FRN_TXT'
        DataSource = DsNFeCabA
        TabOrder = 0
      end
      object DBEdit9: TDBEdit
        Left = 68
        Top = 16
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'CodInfoEmit'
        DataSource = DsNFeCabA
        TabOrder = 1
      end
      object DBEdit10: TDBEdit
        Left = 120
        Top = 16
        Width = 760
        Height = 21
        TabStop = False
        DataField = 'NO_FRN'
        DataSource = DsNFeCabA
        TabOrder = 2
      end
    end
    object GroupBox4: TGroupBox
      Left = 1
      Top = 133
      Width = 1006
      Height = 148
      Align = alTop
      Caption = ' Grupo I - Produtos e servi'#231'os da NF: '
      TabOrder = 3
      object Label231: TLabel
        Left = 8
        Top = 20
        Width = 23
        Height = 13
        Caption = 'Item:'
      end
      object Label232: TLabel
        Left = 316
        Top = 20
        Width = 223
        Height = 13
        Caption = 'Meu c'#243'digo e descri'#231#227'o do produto ou servi'#231'o:'
      end
      object SbGraGruX: TSpeedButton
        Left = 975
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbGraGruXClick
      end
      object Label235: TLabel
        Left = 8
        Top = 44
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label236: TLabel
        Left = 280
        Top = 44
        Width = 37
        Height = 13
        Caption = 'EXTIPI:'
      end
      object Label237: TLabel
        Left = 108
        Top = 44
        Width = 24
        Height = 13
        Caption = 'CST:'
      end
      object Label238: TLabel
        Left = 176
        Top = 44
        Width = 52
        Height = 13
        Caption = 'CFOP [F3]:'
      end
      object Label1: TLabel
        Left = 68
        Top = 20
        Width = 105
        Height = 13
        Caption = 'C'#243'digo do fornecedor:'
      end
      object Label6: TLabel
        Left = 356
        Top = 44
        Width = 165
        Height = 13
        Caption = 'Unidade de Medida comercial [F3]:'
      end
      object SbMedCom: TSpeedButton
        Left = 975
        Top = 39
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMedComClick
      end
      object Label2: TLabel
        Left = 804
        Top = 68
        Width = 107
        Height = 13
        Caption = 'Quantidade tribut'#225'vel:'#185
      end
      object Label3: TLabel
        Left = 8
        Top = 96
        Width = 113
        Height = 13
        Caption = 'Valor unit'#225'rio tribut'#225'vel:'#185
      end
      object Label4: TLabel
        Left = 232
        Top = 96
        Width = 53
        Height = 13
        Caption = 'Valor total:'#185
      end
      object Label5: TLabel
        Left = 388
        Top = 96
        Width = 46
        Height = 13
        Caption = 'BC ICMS:'
      end
      object Label10: TLabel
        Left = 536
        Top = 96
        Width = 56
        Height = 13
        Caption = 'Valor ICMS:'
      end
      object Label11: TLabel
        Left = 696
        Top = 96
        Width = 43
        Height = 13
        Caption = 'Valor IPI:'
      end
      object Label12: TLabel
        Left = 848
        Top = 96
        Width = 40
        Height = 13
        Caption = '% ICMS:'
      end
      object Label13: TLabel
        Left = 932
        Top = 96
        Width = 27
        Height = 13
        Caption = '% IPI:'
      end
      object Label15: TLabel
        Left = 200
        Top = 68
        Width = 163
        Height = 13
        Caption = 'Unidade de Medida tribut'#225'vel [F3]:'
      end
      object SbMedTrib: TSpeedButton
        Left = 779
        Top = 63
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMedTribClick
      end
      object Label16: TLabel
        Left = 8
        Top = 68
        Width = 106
        Height = 13
        Caption = 'Quantidade comercial:'
      end
      object Label14: TLabel
        Left = 8
        Top = 120
        Width = 66
        Height = 13
        Caption = 'Valor do frete:'
      end
      object Label17: TLabel
        Left = 180
        Top = 120
        Width = 77
        Height = 13
        Caption = 'Valor do seguro:'
      end
      object Label18: TLabel
        Left = 364
        Top = 120
        Width = 89
        Height = 13
        Caption = 'Valor do desconto:'
      end
      object Label19: TLabel
        Left = 556
        Top = 120
        Width = 122
        Height = 13
        Caption = 'Valor de outras despesas:'
      end
      object EdnItem: TdmkEdit
        Left = 32
        Top = 16
        Width = 32
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        QryName = 'QrNFeItsI'
        QryCampo = 'nItem'
        UpdCampo = 'nItem'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object Edprod_NCM: TdmkEdit
        Left = 40
        Top = 40
        Width = 64
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 8
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000000'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_NCM'
        UpdCampo = 'prod_NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00000000'
      end
      object Edprod_EXTIPI: TdmkEdit
        Left = 320
        Top = 40
        Width = 32
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_EXTIPI'
        UpdCampo = 'prod_EXTIPI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdICMS_Orig: TdmkEdit
        Left = 136
        Top = 40
        Width = 14
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 1
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrNFeItsI'
        QryCampo = 'ICMS_Orig'
        UpdCampo = 'ICMS_Orig'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object Edprod_CFOP: TdmkEdit
        Left = 232
        Top = 40
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_CFOP'
        UpdCampo = 'prod_CFOP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = Edprod_CFOPKeyDown
      end
      object Edprod_cProd: TdmkEdit
        Left = 176
        Top = 16
        Width = 137
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsI'
        QryCampo = 'prod_cProd'
        UpdCampo = 'prod_cProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnEnter = Edprod_cProdEnter
        OnExit = Edprod_cProdExit
      end
      object EdGraGruX: TdmkEditCB
        Left = 540
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 598
        Top = 16
        Width = 376
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdICMS_CST: TdmkEdit
        Left = 150
        Top = 40
        Width = 20
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        QryName = 'QrNFeItsI'
        QryCampo = 'ICMS_CST'
        UpdCampo = 'ICMS_CST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdUnidMedCom: TdmkEditCB
        Left = 524
        Top = 40
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'UnidMedCom'
        UpdCampo = 'UnidMedCom'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdUnidMedComChange
        OnKeyDown = EdUnidMedComKeyDown
        DBLookupComboBox = CBUnidMedCom
        IgnoraDBLookupComboBox = False
      end
      object Edprod_uCom: TdmkEdit
        Left = 564
        Top = 40
        Width = 56
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_uCom'
        UpdCampo = 'prod_uCom'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edprod_uComChange
        OnExit = Edprod_uComExit
        OnKeyDown = Edprod_uComKeyDown
      end
      object CBUnidMedCom: TdmkDBLookupComboBox
        Left = 620
        Top = 40
        Width = 354
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMedC
        TabOrder = 11
        OnKeyDown = CBUnidMedComKeyDown
        dmkEditCB = EdUnidMedCom
        QryCampo = 'UnidMedCom'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edprod_qTrib: TdmkEdit
        Left = 914
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'prod_qTrib'
        UpdCampo = 'prod_qTrib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnKeyDown = Edprod_qTribKeyDown
      end
      object Edprod_vUnTrib: TdmkEdit
        Left = 124
        Top = 92
        Width = 106
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'prod_vUnTrib'
        UpdCampo = 'prod_vUnTrib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnKeyDown = Edprod_vUnTribKeyDown
      end
      object Edprod_vProd: TdmkEdit
        Left = 288
        Top = 92
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'prod_vProd'
        UpdCampo = 'prod_vProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnKeyDown = Edprod_vProdKeyDown
      end
      object EdICMS_vBC: TdmkEdit
        Left = 436
        Top = 92
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ICMS_vBC'
        UpdCampo = 'ICMS_vBC'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdICMS_vICMS: TdmkEdit
        Left = 596
        Top = 92
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ICMS_vICMS'
        UpdCampo = 'ICMS_vICMS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdIPI_vIPI: TdmkEdit
        Left = 740
        Top = 92
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdICMS_pICMS: TdmkEdit
        Left = 888
        Top = 92
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ICMS_pICMS'
        UpdCampo = 'ICMS_pICMS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdIPI_pIPI: TdmkEdit
        Left = 960
        Top = 92
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdUnidMedTrib: TdmkEditCB
        Left = 364
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'UnidMedTrib'
        UpdCampo = 'UnidMedTrib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdUnidMedTribChange
        OnKeyDown = EdUnidMedTribKeyDown
        DBLookupComboBox = CBUnidMedTrib
        IgnoraDBLookupComboBox = False
      end
      object Edprod_uTrib: TdmkEdit
        Left = 404
        Top = 64
        Width = 56
        Height = 21
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_uTrib'
        UpdCampo = 'prod_uTrib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edprod_uTribChange
        OnExit = Edprod_uTribExit
        OnKeyDown = Edprod_uTribKeyDown
      end
      object CBUnidMedTrib: TdmkDBLookupComboBox
        Left = 460
        Top = 64
        Width = 317
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMedT
        TabOrder = 15
        OnKeyDown = CBUnidMedTribKeyDown
        dmkEditCB = EdUnidMedTrib
        QryCampo = 'UnidMedTrib'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edprod_qCom: TdmkEdit
        Left = 116
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'prod_qCom'
        UpdCampo = 'prod_qCom'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object Edprod_vFrete: TdmkEdit
        Left = 76
        Top = 116
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'prod_vFrete'
        UpdCampo = 'prod_vFrete'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object Edprod_vSeg: TdmkEdit
        Left = 260
        Top = 116
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'prod_vSeg'
        UpdCampo = 'prod_vSeg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object Edprod_vDesc: TdmkEdit
        Left = 456
        Top = 116
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'prod_vDesc'
        UpdCampo = 'prod_vDesc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object Edprod_vOutro: TdmkEdit
        Left = 680
        Top = 116
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 27
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'prod_vOutro'
        UpdCampo = 'prod_vOutro'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object GroupBox5: TGroupBox
      Left = 1
      Top = 281
      Width = 1006
      Height = 60
      Align = alTop
      Caption = ' Estoque: '
      TabOrder = 4
      object Label21: TLabel
        Left = 12
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label22: TLabel
        Left = 100
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label23: TLabel
        Left = 188
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label24: TLabel
        Left = 268
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label25: TLabel
        Left = 360
        Top = 16
        Width = 114
        Height = 13
        Caption = 'Quantidade no estoque:'
        FocusControl = DBEdit13
      end
      object EdPecas: TdmkEdit
        Left = 12
        Top = 32
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPecasChange
      end
      object EdAreaM2: TdmkEditCalc
        Left = 100
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdAreaM2Change
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 184
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdAreaP2Change
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPeso: TdmkEdit
        Left = 268
        Top = 32
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPesoChange
      end
      object DBEdit13: TDBEdit
        Left = 360
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'SIGLAUNIDMED'
        DataSource = DsGraGruX
        TabOrder = 4
      end
      object EdQtde: TdmkEdit
        Left = 416
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = FmEntradaCab.QrNFeCabA
    Left = 16
    Top = 12
  end
  object QrLocGGE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, GraGruX'
      'FROM gragrue'
      'WHERE Fornece=:P0'
      'AND cProd=:P1'
      '/* For'#231'ar Gragrux com Nivel1 diferente de 0 */'
      'ORDER BY Nivel1 DESC, GraGruX')
    Left = 44
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocGGEGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocGGENivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 100
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 72
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrUnidMedC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome, Grandeza'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 732
    Top = 8
    object QrUnidMedCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrUnidMedCGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsUnidMedC: TDataSource
    DataSet = QrUnidMedC
    Left = 760
    Top = 8
  end
  object QrUnidMedT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 788
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField1: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedT: TDataSource
    DataSet = QrUnidMedT
    Left = 816
    Top = 8
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > -900000'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 128
    Top = 12
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 166
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 156
    Top = 12
  end
end
