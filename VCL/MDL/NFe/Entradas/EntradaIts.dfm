object FmEntradaIts: TFmEntradaIts
  Left = 339
  Top = 185
  Caption = 'USO-ENTRA-002 :: Item de Entrada de Uso e Consumo'
  ClientHeight = 702
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton3: TSpeedButton
    Left = 975
    Top = 39
    Width = 21
    Height = 21
    Caption = '...'
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 44
      Align = alTop
      Caption = ' Grupo A - Dados da Nota Fiscal: '
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 92
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label40: TLabel
        Left = 208
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label66: TLabel
        Left = 660
        Top = 20
        Width = 60
        Height = 13
        Caption = 'Chave NF-e:'
      end
      object DBEdit1: TDBEdit
        Left = 132
        Top = 16
        Width = 72
        Height = 21
        TabStop = False
        DataField = 'FatNum'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdIDCtrl: TdmkDBEdit
        Left = 20
        Top = 16
        Width = 68
        Height = 21
        TabStop = False
        DataField = 'IDCtrl'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 256
        Top = 16
        Width = 40
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Filial'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 296
        Top = 16
        Width = 360
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'NO_EMP'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit17: TDBEdit
        Left = 724
        Top = 16
        Width = 272
        Height = 21
        TabStop = False
        DataField = 'Id'
        DataSource = DsNFeCabA
        TabOrder = 4
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 44
      Width = 1008
      Height = 44
      Align = alTop
      Caption = ' Grupo B - Identifica'#231#227'o da Nota Fiscal: '
      TabOrder = 1
      object Label9: TLabel
        Left = 4
        Top = 20
        Width = 38
        Height = 13
        Caption = 'Modelo:'
      end
      object Label41: TLabel
        Left = 108
        Top = 20
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label42: TLabel
        Left = 184
        Top = 20
        Width = 57
        Height = 13
        Caption = 'N'#250'mero NF:'
      end
      object Label43: TLabel
        Left = 316
        Top = 20
        Width = 67
        Height = 13
        Caption = 'Data emiss'#227'o:'
      end
      object Label44: TLabel
        Left = 456
        Top = 20
        Width = 103
        Height = 13
        Caption = 'Data sa'#237'da / entrada:'
      end
      object Label45: TLabel
        Left = 795
        Top = 20
        Width = 130
        Height = 13
        Caption = 'Data Fiscal (chegada aqui):'
      end
      object Label46: TLabel
        Left = 632
        Top = 20
        Width = 103
        Height = 13
        Caption = 'Hora sa'#237'da / entrada:'
      end
      object DBEdit2: TDBEdit
        Left = 48
        Top = 16
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'ide_mod'
        DataSource = DsNFeCabA
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 136
        Top = 16
        Width = 40
        Height = 21
        TabStop = False
        DataField = 'NFG_Serie'
        DataSource = DsNFeCabA
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 244
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_nNF'
        DataSource = DsNFeCabA
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 384
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_dEmi'
        DataSource = DsNFeCabA
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 560
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 736
        Top = 16
        Width = 48
        Height = 21
        TabStop = False
        DataField = 'ide_hSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 932
        Top = 16
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'DataFiscal'
        DataSource = DsNFeCabA
        TabOrder = 6
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 88
      Width = 1008
      Height = 44
      Align = alTop
      Caption = ' Grupo C - Identifica'#231#227'o do emitente: '
      TabOrder = 2
      object Label37: TLabel
        Left = 8
        Top = 20
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object DBEdCNPJ_CPF_TXT_Fornece: TDBEdit
        Left = 884
        Top = 16
        Width = 112
        Height = 21
        TabStop = False
        DataField = 'CNPJ_CPF_FRN_TXT'
        DataSource = DsNFeCabA
        TabOrder = 0
      end
      object DBEdit9: TDBEdit
        Left = 68
        Top = 16
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'CodInfoEmit'
        DataSource = DsNFeCabA
        TabOrder = 1
      end
      object DBEdit10: TDBEdit
        Left = 120
        Top = 16
        Width = 760
        Height = 21
        TabStop = False
        DataField = 'NO_FRN'
        DataSource = DsNFeCabA
        TabOrder = 2
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 132
      Width = 1008
      Height = 224
      Align = alTop
      Caption = ' Grupo I - Produtos e servi'#231'os da NF: '
      TabOrder = 3
      object Panel6: TPanel
        Left = 2
        Top = 141
        Width = 1004
        Height = 81
        Align = alBottom
        TabOrder = 1
        object Label236: TLabel
          Left = 100
          Top = 0
          Width = 37
          Height = 13
          Caption = 'EXTIPI:'
        end
        object Label6: TLabel
          Left = 144
          Top = 0
          Width = 165
          Height = 13
          Caption = 'Unidade de Medida comercial [F3]:'
        end
        object Label16: TLabel
          Left = 620
          Top = 0
          Width = 106
          Height = 13
          Caption = 'Quantidade comercial:'
        end
        object SbMedCom: TSpeedButton
          Left = 596
          Top = 15
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbMedComClick
        end
        object SbMedTrib: TSpeedButton
          Left = 596
          Top = 55
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbMedTribClick
        end
        object Label2: TLabel
          Left = 620
          Top = 40
          Width = 107
          Height = 13
          Caption = 'Quantidade tribut'#225'vel:'#185
        end
        object Label3: TLabel
          Left = 756
          Top = 40
          Width = 113
          Height = 13
          Caption = 'Valor unit'#225'rio tribut'#225'vel:'#185
        end
        object Label4: TLabel
          Left = 880
          Top = 40
          Width = 53
          Height = 13
          Caption = 'Valor total:'#185
        end
        object Label15: TLabel
          Left = 144
          Top = 40
          Width = 163
          Height = 13
          Caption = 'Unidade de Medida tribut'#225'vel [F3]:'
        end
        object Edprod_EXTIPI: TdmkEdit
          Left = 100
          Top = 16
          Width = 40
          Height = 21
          MaxLength = 3
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_EXTIPI'
          UpdCampo = 'prod_EXTIPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdUnidMedCom: TdmkEditCB
          Left = 144
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'UnidMedCom'
          UpdCampo = 'UnidMedCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedComChange
          OnKeyDown = EdUnidMedComKeyDown
          DBLookupComboBox = CBUnidMedCom
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object Edprod_uCom: TdmkEdit
          Left = 184
          Top = 16
          Width = 56
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'prod_uCom'
          UpdCampo = 'prod_uCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_uComChange
          OnExit = Edprod_uComExit
          OnKeyDown = Edprod_uComKeyDown
        end
        object CBUnidMedCom: TdmkDBLookupComboBox
          Left = 240
          Top = 16
          Width = 352
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedC
          TabOrder = 3
          OnKeyDown = CBUnidMedComKeyDown
          dmkEditCB = EdUnidMedCom
          QryCampo = 'UnidMedCom'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object Edprod_qCom: TdmkEdit
          Left = 620
          Top = 16
          Width = 132
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryCampo = 'prod_qCom'
          UpdCampo = 'prod_qCom'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = Edprod_qComRedefinido
        end
        object EdUnidMedTrib: TdmkEditCB
          Left = 144
          Top = 56
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'UnidMedTrib'
          UpdCampo = 'UnidMedTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedTribChange
          OnKeyDown = EdUnidMedTribKeyDown
          DBLookupComboBox = CBUnidMedTrib
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object Edprod_uTrib: TdmkEdit
          Left = 184
          Top = 56
          Width = 56
          Height = 21
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'prod_uTrib'
          UpdCampo = 'prod_uTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Edprod_uTribChange
          OnExit = Edprod_uTribExit
          OnKeyDown = Edprod_uTribKeyDown
        end
        object CBUnidMedTrib: TdmkDBLookupComboBox
          Left = 240
          Top = 56
          Width = 352
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedT
          TabOrder = 7
          OnKeyDown = CBUnidMedTribKeyDown
          dmkEditCB = EdUnidMedTrib
          QryCampo = 'UnidMedTrib'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object Edprod_qTrib: TdmkEdit
          Left = 620
          Top = 56
          Width = 132
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryCampo = 'prod_qTrib'
          UpdCampo = 'prod_qTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = Edprod_qTribKeyDown
          OnRedefinido = Edprod_qTribRedefinido
        end
        object Edprod_vUnTrib: TdmkEdit
          Left = 756
          Top = 56
          Width = 120
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'prod_vUnTrib'
          UpdCampo = 'prod_vUnTrib'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = Edprod_vUnTribKeyDown
        end
        object Edprod_vProd: TdmkEdit
          Left = 880
          Top = 56
          Width = 120
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'prod_vProd'
          UpdCampo = 'prod_vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = Edprod_vProdKeyDown
        end
        object RGIND_APUR: TdmkRadioGroup
          Left = 1
          Top = 1
          Width = 96
          Height = 79
          Align = alLeft
          Caption = 'Indic. Apura. IPI:'
          ItemIndex = 2
          Items.Strings = (
            '0-Mensal'
            '1-Decendial'
            'x-N'#227'o apura')
          TabOrder = 11
          QryCampo = 'CO_IND_APUR'
          UpdType = utYes
          OldValor = 0
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 126
        Align = alClient
        TabOrder = 0
        object Label231: TLabel
          Left = 8
          Top = 4
          Width = 23
          Height = 13
          Caption = 'Item:'
        end
        object Label1: TLabel
          Left = 44
          Top = 4
          Width = 105
          Height = 13
          Caption = 'C'#243'digo do fornecedor:'
        end
        object Label232: TLabel
          Left = 408
          Top = 4
          Width = 223
          Height = 13
          Caption = 'Meu c'#243'digo e descri'#231#227'o do produto ou servi'#231'o:'
        end
        object Label235: TLabel
          Left = 932
          Top = 4
          Width = 27
          Height = 13
          Caption = 'NCM:'
        end
        object SbGraGruX: TSpeedButton
          Left = 908
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbGraGruXClick
        end
        object Label237: TLabel
          Left = 8
          Top = 44
          Width = 68
          Height = 13
          Caption = 'CST do ICMS:'
        end
        object Label27: TLabel
          Left = 8
          Top = 84
          Width = 31
          Height = 13
          Caption = 'CFOP:'
        end
        object Label20: TLabel
          Left = 524
          Top = 44
          Width = 55
          Height = 13
          Caption = 'CST do IPI:'
        end
        object EdnItem: TdmkEdit
          Left = 8
          Top = 20
          Width = 32
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          QryName = 'QrNFeItsI'
          QryCampo = 'nItem'
          UpdCampo = 'nItem'
          UpdType = utIdx
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object Edprod_cProd: TdmkEdit
          Left = 44
          Top = 20
          Width = 360
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_cProd'
          UpdCampo = 'prod_cProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnEnter = Edprod_cProdEnter
          OnExit = Edprod_cProdExit
        end
        object EdGraGruX: TdmkEditCB
          Left = 408
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          OnRedefinido = EdGraGruXRedefinido
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 466
          Top = 20
          Width = 439
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 3
          dmkEditCB = EdGraGruX
          QryCampo = 'GraGruX'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object Edprod_NCM: TdmkEdit
          Left = 932
          Top = 20
          Width = 64
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 8
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00000000'
          QryName = 'QrNFeItsI'
          QryCampo = 'prod_NCM'
          UpdCampo = 'prod_NCM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '00000000'
          ValWarn = False
        end
        object EdICMS_Orig: TdmkEdit
          Left = 136
          Top = 40
          Width = 16
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrNFeItsI'
          QryCampo = 'ICMS_Orig'
          UpdCampo = 'ICMS_Orig'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdICMS_OrigRedefinido
        end
        object EdICMS_CST: TdmkEdit
          Left = 152
          Top = 40
          Width = 20
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          QryName = 'QrNFeItsI'
          QryCampo = 'ICMS_CST'
          UpdCampo = 'ICMS_CST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdICMS_CSTRedefinido
        end
        object EdCST_ICMS: TdmkEditCB
          Left = 8
          Top = 60
          Width = 36
          Height = 21
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          QryCampo = 'CST_ICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '000'
          ValWarn = False
          OnRedefinido = EdCST_ICMSRedefinido
          DBLookupComboBox = CBCST_ICMS
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCST_ICMS: TdmkDBLookupComboBox
          Left = 44
          Top = 60
          Width = 473
          Height = 21
          KeyField = 'CodTxt'
          ListField = 'Nome'
          ListSource = DsCST_ICMS
          TabOrder = 8
          dmkEditCB = EdCST_ICMS
          QryCampo = 'CST_ICMS'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object Edprod_CFOP: TdmkEditCB
          Left = 8
          Top = 100
          Width = 40
          Height = 21
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CFOP'
          UpdCampo = 'prod_CFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          DBLookupComboBox = CBprod_CFOP
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBprod_CFOP: TdmkDBLookupComboBox
          Left = 47
          Top = 100
          Width = 950
          Height = 21
          KeyField = 'CodTxt'
          ListField = 'Nome'
          ListSource = DsTbSPEDEFD002
          TabOrder = 12
          dmkEditCB = Edprod_CFOP
          QryCampo = 'CFOP'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdIPI_CST: TdmkEditCB
          Left = 524
          Top = 60
          Width = 36
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CST_IPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          DBLookupComboBox = CBIPI_CST
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBIPI_CST: TdmkDBLookupComboBox
          Left = 559
          Top = 60
          Width = 438
          Height = 21
          KeyField = 'CodTxt'
          ListField = 'Nome'
          ListSource = DsCST_IPI
          TabOrder = 10
          dmkEditCB = EdIPI_CST
          QryCampo = 'CST_IPI'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object GroupBox5: TGroupBox
      Left = 0
      Top = 356
      Width = 1008
      Height = 56
      Align = alTop
      Caption = ' Estoque: '
      TabOrder = 4
      object RGBaixa: TdmkRadioGroup
        Left = 2
        Top = 15
        Width = 137
        Height = 39
        Align = alLeft
        Caption = ' Movimenta estoque: '
        Columns = 3
        ItemIndex = 2
        Items.Strings = (
          'N'#227'o'
          'Sim'
          '???')
        TabOrder = 0
        OnClick = RGBaixaClick
        UpdType = utYes
        OldValor = 0
      end
      object PnBaixa: TPanel
        Left = 139
        Top = 15
        Width = 867
        Height = 39
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label21: TLabel
          Left = 528
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label22: TLabel
          Left = 612
          Top = 0
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label23: TLabel
          Left = 700
          Top = 0
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
          Enabled = False
        end
        object Label24: TLabel
          Left = 780
          Top = 0
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label25: TLabel
          Left = 400
          Top = 0
          Width = 114
          Height = 13
          Caption = 'Quantidade no estoque:'
        end
        object Label26: TLabel
          Left = 4
          Top = 0
          Width = 96
          Height = 13
          Caption = 'Unidade de Medida:'
        end
        object SBUnidMed: TSpeedButton
          Left = 376
          Top = 15
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object EdPecas: TdmkEdit
          Left = 528
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPecasChange
        end
        object EdAreaM2: TdmkEditCalc
          Left = 612
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaM2Change
          dmkEditCalcA = EdAreaP2
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdAreaP2: TdmkEditCalc
          Left = 696
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAreaP2Change
          dmkEditCalcA = EdAreaM2
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
        object EdPeso: TdmkEdit
          Left = 780
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPesoChange
        end
        object EdQtde: TdmkEdit
          Left = 400
          Top = 16
          Width = 124
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdUnidMed: TdmkEditCB
          Left = 4
          Top = 15
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedChange
          OnKeyDown = EdUnidMedKeyDown
          DBLookupComboBox = CBUnidMed
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdSigla: TdmkEdit
          Left = 44
          Top = 15
          Width = 56
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaChange
          OnExit = EdSiglaExit
          OnKeyDown = EdSiglaKeyDown
        end
        object CBUnidMed: TdmkDBLookupComboBox
          Left = 100
          Top = 15
          Width = 273
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedC
          TabOrder = 2
          OnKeyDown = CBUnidMedKeyDown
          dmkEditCB = EdUnidMed
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 412
      Width = 1008
      Height = 56
      Align = alTop
      Caption = ' C'#225'lculo dos Impostos: '
      TabOrder = 5
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 39
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 0
          Width = 49
          Height = 13
          Caption = 'BC ICMS'#185':'
        end
        object Label12: TLabel
          Left = 124
          Top = 0
          Width = 40
          Height = 13
          Caption = '% ICMS:'
        end
        object Label10: TLabel
          Left = 168
          Top = 0
          Width = 59
          Height = 13
          Caption = 'Valor ICMS'#185':'
        end
        object Label13: TLabel
          Left = 284
          Top = 0
          Width = 27
          Height = 13
          Caption = '% IPI:'
        end
        object Label11: TLabel
          Left = 328
          Top = 0
          Width = 46
          Height = 13
          Caption = 'Valor IPI'#185':'
        end
        object Label14: TLabel
          Left = 444
          Top = 0
          Width = 66
          Height = 13
          Caption = 'Valor do frete:'
        end
        object Label17: TLabel
          Left = 560
          Top = 0
          Width = 77
          Height = 13
          Caption = 'Valor do seguro:'
        end
        object Label18: TLabel
          Left = 676
          Top = 0
          Width = 89
          Height = 13
          Caption = 'Valor do desconto:'
        end
        object Label19: TLabel
          Left = 792
          Top = 0
          Width = 103
          Height = 13
          Caption = 'Valor de outras desp.:'
        end
        object EdICMS_vBC: TdmkEdit
          Left = 8
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_vBC'
          UpdCampo = 'ICMS_vBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdICMS_vBCKeyDown
        end
        object EdICMS_pICMS: TdmkEdit
          Left = 124
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_pICMS'
          UpdCampo = 'ICMS_pICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMS_vICMS: TdmkEdit
          Left = 168
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_vICMS'
          UpdCampo = 'ICMS_vICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdICMS_vICMSKeyDown
        end
        object EdIPI_pIPI: TdmkEdit
          Left = 284
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdIPI_vIPI: TdmkEdit
          Left = 328
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdIPI_vIPIKeyDown
        end
        object Edprod_vFrete: TdmkEdit
          Left = 444
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'prod_vFrete'
          UpdCampo = 'prod_vFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vSeg: TdmkEdit
          Left = 560
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'prod_vSeg'
          UpdCampo = 'prod_vSeg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vDesc: TdmkEdit
          Left = 676
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'prod_vDesc'
          UpdCampo = 'prod_vDesc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Edprod_vOutro: TdmkEdit
          Left = 792
          Top = 16
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'prod_vOutro'
          UpdCampo = 'prod_vOutro'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 468
      Width = 1008
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 6
      object CkContinuar: TCheckBox
        Left = 12
        Top = 4
        Width = 133
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 435
        Height = 32
        Caption = 'Item de Entrada de Uso e Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 638
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 594
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 203
        Height = 16
        Caption = #185' : Calcula ao pressionar a tecla F4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 203
        Height = 16
        Caption = #185' : Calcula ao pressionar a tecla F4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = FmEntradaCab.QrNFeCabA
    Left = 320
    Top = 128
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 492
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 544
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrUnidMedC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome, Grandeza'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 732
    Top = 8
    object QrUnidMedCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrUnidMedCGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsUnidMedC: TDataSource
    DataSet = QrUnidMedC
    Left = 732
    Top = 56
  end
  object QrUnidMedT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome, Grandeza'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 800
    Top = 8
    object QrUnidMedTCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedTCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedTSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedTNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrUnidMedTGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsUnidMedT: TDataSource
    DataSet = QrUnidMedT
    Left = 800
    Top = 56
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq,'
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > -900000'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 436
    Top = 44
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 166
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 436
    Top = 88
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 860
    Top = 8
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 864
    Top = 56
  end
  object QrCST_ICMS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(t024.CodTxt, t025.CodTxt) CST, '
      't024.Nome NO_CST_A, t025.Nome NO_CST_B'
      'FROM tbspedefd024 t024'
      'INNER JOIN tbspedefd025 t025'
      'ORDER BY t024.Nome, t025.Nome')
    Left = 384
    Top = 316
    object QrCST_ICMSCodTxt: TWideStringField
      DisplayWidth = 60
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrCST_ICMSNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCST_ICMS: TDataSource
    DataSet = QrCST_ICMS
    Left = 384
    Top = 364
  end
  object QrTbSPEDEFD002: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodTxt, Nome'
      'FROM tbspedefd002'
      'ORDER BY Nome')
    Left = 452
    Top = 340
    object QrTbSPEDEFD002CodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrTbSPEDEFD002Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsTbSPEDEFD002: TDataSource
    DataSet = QrTbSPEDEFD002
    Left = 452
    Top = 388
  end
  object QrCST_IPI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(t024.CodTxt, t025.CodTxt) CST, '
      't024.Nome NO_CST_A, t025.Nome NO_CST_B'
      'FROM tbspedefd024 t024'
      'INNER JOIN tbspedefd025 t025'
      'ORDER BY t024.Nome, t025.Nome')
    Left = 520
    Top = 316
    object QrCST_IPICodTxt: TWideStringField
      DisplayWidth = 60
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrCST_IPINome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCST_IPI: TDataSource
    DataSet = QrCST_IPI
    Left = 520
    Top = 364
  end
end
