object FmUsoConsCab: TFmUsoConsCab
  Left = 368
  Top = 194
  Caption = 'USO-ENTRA-001 :: Entrada de Uso e Consumo'
  ClientHeight = 535
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 487
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 438
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 376
      Align = alTop
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 1004
        Height = 44
        Align = alTop
        Caption = ' Grupo A - Dados da Nota Fiscal: '
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdIDCtrl
        end
        object Label8: TLabel
          Left = 80
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label40: TLabel
          Left = 188
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label66: TLabel
          Left = 660
          Top = 20
          Width = 60
          Height = 13
          Caption = 'Chave NF-e:'
        end
        object EdIDCtrl: TdmkEdit
          Left = 20
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'IDCtrl'
          UpdCampo = 'IDCtrl'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFatNum: TdmkEdit
          Left = 120
          Top = 16
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatNum'
          UpdCampo = 'FatNum'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 236
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Filial'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 276
          Top = 16
          Width = 381
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 3
          dmkEditCB = EdEmpresa
          QryCampo = 'Filial'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNFe_Id: TdmkEdit
          Left = 724
          Top = 16
          Width = 271
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Id'
          UpdCampo = 'Id'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 1
        Top = 45
        Width = 1004
        Height = 68
        Align = alTop
        Caption = ' Grupo B - Identifica'#231#227'o da Nota Fiscal: '
        TabOrder = 1
        object Label9: TLabel
          Left = 4
          Top = 20
          Width = 38
          Height = 13
          Caption = 'Modelo:'
        end
        object Label41: TLabel
          Left = 860
          Top = 20
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label42: TLabel
          Left = 4
          Top = 44
          Width = 57
          Height = 13
          Caption = 'N'#250'mero NF:'
        end
        object Label43: TLabel
          Left = 132
          Top = 44
          Width = 67
          Height = 13
          Caption = 'Data emiss'#227'o:'
        end
        object Label44: TLabel
          Left = 312
          Top = 44
          Width = 103
          Height = 13
          Caption = 'Data sa'#237'da / entrada:'
        end
        object Label45: TLabel
          Left = 687
          Top = 44
          Width = 130
          Height = 13
          Caption = 'Data Fiscal (chegada aqui):'
        end
        object Label46: TLabel
          Left = 528
          Top = 44
          Width = 103
          Height = 13
          Caption = 'Hora sa'#237'da / entrada:'
        end
        object Edide_mod: TdmkEditCB
          Left = 44
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ide_mod'
          UpdCampo = 'ide_mod'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBide_mod
          IgnoraDBLookupComboBox = False
        end
        object CBide_mod: TdmkDBLookupComboBox
          Left = 100
          Top = 16
          Width = 757
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsNFMoDocFis
          TabOrder = 1
          dmkEditCB = Edide_mod
          QryCampo = 'ide_mod'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNFG_Serie: TdmkEdit
          Left = 888
          Top = 16
          Width = 40
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 3
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NFG_Serie'
          UpdCampo = 'NFG_Serie'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Edide_nNF: TdmkEdit
          Left = 64
          Top = 40
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ide_nNF'
          UpdCampo = 'ide_nNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPide_dEmi: TdmkEditDateTimePicker
          Left = 200
          Top = 40
          Width = 105
          Height = 21
          Date = 1.000000000000000000
          Time = 1.000000000000000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ide_dEmi'
          UpdCampo = 'ide_dEmi'
          UpdType = utYes
        end
        object TPide_dSaiEnt: TdmkEditDateTimePicker
          Left = 416
          Top = 40
          Width = 105
          Height = 21
          Date = 1.000000000000000000
          Time = 1.000000000000000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'ide_dSaiEnt'
          UpdCampo = 'ide_dSaiEnt'
          UpdType = utYes
        end
        object TPDataFiscal: TdmkEditDateTimePicker
          Left = 823
          Top = 40
          Width = 105
          Height = 21
          Date = 1.688972615738749000
          Time = 1.688972615738749000
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataFiscal'
          UpdCampo = 'DataFiscal'
          UpdType = utYes
        end
        object Edide_hSaiEnt: TdmkEdit
          Left = 632
          Top = 40
          Width = 49
          Height = 21
          TabOrder = 6
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'ide_hSaiEnt'
          UpdCampo = 'ide_hSaiEnt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGide_tpNF: TdmkRadioGroup
          Left = 932
          Top = 15
          Width = 70
          Height = 51
          Align = alRight
          Caption = ' Opera'#231#227'o: '
          ItemIndex = 1
          Items.Strings = (
            'Entrada'
            'Sa'#237'da')
          TabOrder = 8
          QryCampo = 'ide_tpNF'
          UpdCampo = 'ide_tpNF'
          UpdType = utYes
          OldValor = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 1
        Top = 113
        Width = 1004
        Height = 44
        Align = alTop
        Caption = ' Grupo C - Identifica'#231#227'o do emitente: '
        TabOrder = 2
        object Label37: TLabel
          Left = 8
          Top = 20
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
        end
        object SpeedButton5: TSpeedButton
          Left = 851
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object EdCodInfoEmit: TdmkEditCB
          Left = 68
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodInfoEmit'
          UpdCampo = 'CodInfoEmit'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodInfoEmitChange
          DBLookupComboBox = CBCodInfoEmit
          IgnoraDBLookupComboBox = False
        end
        object CBCodInfoEmit: TdmkDBLookupComboBox
          Left = 124
          Top = 16
          Width = 725
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsFornece
          TabOrder = 1
          dmkEditCB = EdCodInfoEmit
          QryCampo = 'CodInfoEmit'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdCNPJ_CPF_TXT_Fornece: TDBEdit
          Left = 876
          Top = 16
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'CNPJ_CPF_TXT'
          DataSource = DsFornece
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 1
        Top = 157
        Width = 1004
        Height = 68
        Align = alTop
        Caption = ' Grupo W - Valores totais da NF: '
        TabOrder = 3
        object Label47: TLabel
          Left = 8
          Top = 20
          Width = 46
          Height = 13
          Caption = 'BC ICMS:'
        end
        object Label48: TLabel
          Left = 144
          Top = 20
          Width = 56
          Height = 13
          Caption = 'Valor ICMS:'
        end
        object Label49: TLabel
          Left = 292
          Top = 20
          Width = 63
          Height = 13
          Caption = 'BC ICMS ST:'
        end
        object Label50: TLabel
          Left = 448
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Valor ICMS ST:'
        end
        object Label51: TLabel
          Left = 612
          Top = 20
          Width = 45
          Height = 13
          Caption = 'Produtos:'
        end
        object Label52: TLabel
          Left = 748
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Frete:'
        end
        object Label53: TLabel
          Left = 868
          Top = 20
          Width = 37
          Height = 13
          Caption = 'Seguro:'
        end
        object Label54: TLabel
          Left = 8
          Top = 44
          Width = 49
          Height = 13
          Caption = 'Desconto:'
        end
        object Label55: TLabel
          Left = 152
          Top = 44
          Width = 36
          Height = 13
          Caption = 'Total II:'
        end
        object Label56: TLabel
          Left = 276
          Top = 44
          Width = 43
          Height = 13
          Caption = 'Total IPI:'
        end
        object Label57: TLabel
          Left = 408
          Top = 44
          Width = 47
          Height = 13
          Caption = 'Total PIS:'
        end
        object Label58: TLabel
          Left = 548
          Top = 44
          Width = 69
          Height = 13
          Caption = 'Total COFINS:'
        end
        object Label59: TLabel
          Left = 708
          Top = 44
          Width = 59
          Height = 13
          Caption = 'Total outros:'
        end
        object Label60: TLabel
          Left = 860
          Top = 44
          Width = 44
          Height = 13
          Caption = 'Total NF:'
        end
        object EdICMSTot_vBC: TdmkEdit
          Left = 56
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vBC'
          UpdCampo = 'ICMSTot_vBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vICMS: TdmkEdit
          Left = 204
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vICMS'
          UpdCampo = 'ICMSTot_vICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vBCST: TdmkEdit
          Left = 356
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vBCST'
          UpdCampo = 'ICMSTot_vBCST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vST: TdmkEdit
          Left = 524
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vST'
          UpdCampo = 'ICMSTot_vST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vProd: TdmkEdit
          Left = 660
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vProd'
          UpdCampo = 'ICMSTot_vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vFrete: TdmkEdit
          Left = 780
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vFrete'
          UpdCampo = 'ICMSTot_vFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vSeg: TdmkEdit
          Left = 908
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vSeg'
          UpdCampo = 'ICMSTot_vSeg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vDesc: TdmkEdit
          Left = 60
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vDesc'
          UpdCampo = 'ICMSTot_vDesc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vII: TdmkEdit
          Left = 192
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vII'
          UpdCampo = 'ICMSTot_vII'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vIPI: TdmkEdit
          Left = 320
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vIPI'
          UpdCampo = 'ICMSTot_vIPI'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vPIS: TdmkEdit
          Left = 460
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vPIS'
          UpdCampo = 'ICMSTot_vPIS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vCOFINS: TdmkEdit
          Left = 620
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vCOFINS'
          UpdCampo = 'ICMSTot_vCOFINS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vOutro: TdmkEdit
          Left = 772
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vOutro'
          UpdCampo = 'ICMSTot_vOutro'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdICMSTot_vNF: TdmkEdit
          Left = 908
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMSTot_vNF'
          UpdCampo = 'ICMSTot_vNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox5: TGroupBox
        Left = 1
        Top = 225
        Width = 1004
        Height = 92
        Align = alTop
        Caption = ' Grupo X - Informa'#231#245'es do transporte da NF: '
        TabOrder = 4
        object Label163: TLabel
          Left = 8
          Top = 20
          Width = 97
          Height = 13
          Caption = 'Modalidade do frete:'
          FocusControl = EdModFrete
        end
        object Label61: TLabel
          Left = 316
          Top = 20
          Width = 69
          Height = 13
          Caption = 'Transportador:'
        end
        object SpeedButton6: TSpeedButton
          Left = 851
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label62: TLabel
          Left = 8
          Top = 44
          Width = 79
          Height = 13
          Caption = 'Valor do servi'#231'o:'
        end
        object Label63: TLabel
          Left = 176
          Top = 44
          Width = 46
          Height = 13
          Caption = 'BC ICMS:'
        end
        object Label64: TLabel
          Left = 308
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Aliq. Reten'#231#227'o:'
        end
        object Label65: TLabel
          Left = 428
          Top = 44
          Width = 100
          Height = 13
          Caption = 'Valor do ICMS retido:'
        end
        object Label174: TLabel
          Left = 616
          Top = 44
          Width = 31
          Height = 13
          Caption = 'CFOP:'
          FocusControl = EdRetTransp_CFOP
        end
        object Label175: TLabel
          Left = 8
          Top = 69
          Width = 352
          Height = 13
          Caption = 
            'C'#243'digo do munic'#237'pio de ocorr'#234'ncia do fato gerador do ICMS do tra' +
            'nsporte:'
          FocusControl = EdRetTransp_CMunFG
        end
        object EdModFrete: TdmkEdit
          Left = 108
          Top = 16
          Width = 20
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '9'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ModFrete'
          UpdCampo = 'ModFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdModFreteChange
        end
        object EdModFrete_TXT: TdmkEdit
          Left = 128
          Top = 16
          Width = 185
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'Por conta do emitente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'Por conta do emitente'
          ValWarn = False
        end
        object EdCodInfoTrsp: TdmkEditCB
          Left = 388
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodInfoTrsp'
          UpdCampo = 'CodInfoTrsp'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodInfoTrspChange
          DBLookupComboBox = CBCodInfoTrsp
          IgnoraDBLookupComboBox = False
        end
        object CBCodInfoTrsp: TdmkDBLookupComboBox
          Left = 444
          Top = 16
          Width = 405
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTransporta
          TabOrder = 3
          dmkEditCB = EdCodInfoTrsp
          QryCampo = 'CodInfoTrsp'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdRetTransp_vServ: TdmkEdit
          Left = 92
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vServ'
          UpdCampo = 'RetTransp_vServ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_vBCRet: TdmkEdit
          Left = 224
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vBCRet'
          UpdCampo = 'RetTransp_vBCRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_PICMSRet: TdmkEdit
          Left = 388
          Top = 40
          Width = 36
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_PICMSRet'
          UpdCampo = 'RetTransp_PICMSRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_vICMSRet: TdmkEdit
          Left = 532
          Top = 40
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'RetTransp_vICMSRet'
          UpdCampo = 'RetTransp_vICMSRet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdRetTransp_CFOP: TdmkEdit
          Left = 648
          Top = 40
          Width = 56
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'RetTransp_CFOP'
          UpdCampo = 'RetTransp_CFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRetTransp_CMunFG: TdmkEdit
          Left = 368
          Top = 65
          Width = 56
          Height = 21
          Alignment = taRightJustify
          MaxLength = 7
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'RetTransp_CMunFG'
          UpdCampo = 'RetTransp_CMunFG'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdRetTransp_CMunFGChange
        end
        object EdRetTransp_CMunFG_TXT: TdmkEditF7
          Left = 428
          Top = 65
          Width = 560
          Height = 21
          ReadOnly = True
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          dmkEditCod = EdRetTransp_CMunFG
          LocF7CodiFldName = 'Codigo'
          LocF7NameFldName = 'Nome'
          LocF7TableName = 'dtb_munici'
        end
        object DBEdCNPJ_CPF_TXT_Transporta: TDBEdit
          Left = 876
          Top = 16
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'CNPJ_CPF_TXT'
          DataSource = DsTransporta
          TabOrder = 4
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 487
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 156
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 628
        Top = 12
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdIDCtrl
      end
      object Label2: TLabel
        Left = 124
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 8
        Top = 12
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 760
        Top = 132
        Width = 43
        Height = 13
        Caption = 'Inclus'#227'o:'
        FocusControl = DBEdFatID
      end
      object Label6: TLabel
        Left = 8
        Top = 36
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        FocusControl = DBEdit5
      end
      object Label5: TLabel
        Left = 732
        Top = 60
        Width = 46
        Height = 13
        Caption = 'Natureza:'
        FocusControl = DBEdit4
      end
      object Label10: TLabel
        Left = 644
        Top = 36
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 716
        Top = 36
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
        FocusControl = DBEdit8
      end
      object Label12: TLabel
        Left = 784
        Top = 36
        Width = 32
        Height = 13
        Caption = 'N'#186' NF:'
        FocusControl = DBEdit9
      end
      object Label13: TLabel
        Left = 720
        Top = 12
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
        FocusControl = DBEdit10
      end
      object Label14: TLabel
        Left = 848
        Top = 12
        Width = 32
        Height = 13
        Caption = 'Sa'#237'da:'
        FocusControl = DBEdit11
      end
      object Label16: TLabel
        Left = 80
        Top = 60
        Width = 87
        Height = 13
        Caption = 'Tipo de opera'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label17: TLabel
        Left = 188
        Top = 60
        Width = 80
        Height = 13
        Caption = 'Tipo de emiss'#227'o:'
        FocusControl = DBEdit14
      end
      object Label18: TLabel
        Left = 8
        Top = 60
        Width = 47
        Height = 13
        Caption = 'Ambiente:'
        FocusControl = DBEdit15
      end
      object Label19: TLabel
        Left = 292
        Top = 60
        Width = 103
        Height = 13
        Caption = 'Processo de emiss'#227'o:'
        FocusControl = DBEdit16
      end
      object Label20: TLabel
        Left = 420
        Top = 60
        Width = 34
        Height = 13
        Caption = 'Chave:'
        FocusControl = DBEdit17
      end
      object Label15: TLabel
        Left = 8
        Top = 84
        Width = 46
        Height = 13
        Caption = 'BC ICMS:'
        FocusControl = DBEdit18
      end
      object Label21: TLabel
        Left = 148
        Top = 84
        Width = 56
        Height = 13
        Caption = 'Valor ICMS:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 292
        Top = 84
        Width = 60
        Height = 13
        Caption = 'BCST ICMS:'
        FocusControl = DBEdit20
      end
      object Label23: TLabel
        Left = 440
        Top = 84
        Width = 73
        Height = 13
        Caption = 'Valor ICMS ST:'
        FocusControl = DBEdit21
      end
      object Label24: TLabel
        Left = 600
        Top = 84
        Width = 71
        Height = 13
        Caption = 'Valor produtos:'
        FocusControl = DBEdit22
      end
      object Label25: TLabel
        Left = 756
        Top = 84
        Width = 27
        Height = 13
        Caption = 'Frete:'
        FocusControl = DBEdit23
      end
      object Label26: TLabel
        Left = 872
        Top = 84
        Width = 37
        Height = 13
        Caption = 'Seguro:'
        FocusControl = DBEdit24
      end
      object Label27: TLabel
        Left = 700
        Top = 108
        Width = 76
        Height = 13
        Caption = 'Valor Desconto:'
        FocusControl = DBEdit25
      end
      object Label28: TLabel
        Left = 8
        Top = 108
        Width = 42
        Height = 13
        Caption = 'Valor I.I.:'
        FocusControl = DBEdit26
      end
      object Label29: TLabel
        Left = 132
        Top = 108
        Width = 43
        Height = 13
        Caption = 'Valor IPI:'
        FocusControl = DBEdit27
      end
      object Label30: TLabel
        Left = 264
        Top = 108
        Width = 47
        Height = 13
        Caption = 'Valor PIS:'
        FocusControl = DBEdit28
      end
      object Label31: TLabel
        Left = 400
        Top = 108
        Width = 69
        Height = 13
        Caption = 'Valor COFINS:'
        FocusControl = DBEdit29
      end
      object Label32: TLabel
        Left = 556
        Top = 108
        Width = 59
        Height = 13
        Caption = 'Valor outros:'
        FocusControl = DBEdit30
      end
      object Label33: TLabel
        Left = 864
        Top = 108
        Width = 44
        Height = 13
        Caption = 'Total NF:'
        FocusControl = DBEdit31
      end
      object Label34: TLabel
        Left = 8
        Top = 132
        Width = 36
        Height = 13
        Caption = 'Transp:'
        FocusControl = DBEdit32
      end
      object Label35: TLabel
        Left = 108
        Top = 132
        Width = 54
        Height = 13
        Caption = 'Modo frete:'
        FocusControl = DBEdit33
      end
      object Label36: TLabel
        Left = 180
        Top = 132
        Width = 33
        Height = 13
        Caption = 'Status:'
        FocusControl = DBEdit34
      end
      object Label38: TLabel
        Left = 248
        Top = 132
        Width = 66
        Height = 13
        Caption = 'Data cont'#225'bil:'
        FocusControl = DBEdit36
      end
      object Label39: TLabel
        Left = 886
        Top = 36
        Width = 40
        Height = 13
        Caption = 'Entrada:'
        FocusControl = DBEdit37
      end
      object DBEdIDCtrl: TdmkDBEdit
        Left = 644
        Top = 8
        Width = 68
        Height = 21
        TabStop = False
        DataField = 'IDCtrl'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 172
        Top = 8
        Width = 45
        Height = 21
        Color = clWhite
        DataField = 'Filial'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 48
        Top = 8
        Width = 72
        Height = 21
        DataField = 'FatNum'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 220
        Top = 8
        Width = 405
        Height = 21
        Color = clWhite
        DataField = 'NO_EMP'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdFatID: TDBEdit
        Left = 804
        Top = 128
        Width = 28
        Height = 21
        DataField = 'FatID'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object DBEdNO_FatID: TDBEdit
        Left = 832
        Top = 128
        Width = 160
        Height = 21
        DataField = 'NO_FATID'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 68
        Top = 32
        Width = 56
        Height = 21
        DataField = 'CodInfoEmit'
        DataSource = DsNFeCabA
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 124
        Top = 32
        Width = 516
        Height = 21
        DataField = 'NO_FRN'
        DataSource = DsNFeCabA
        TabOrder = 7
      end
      object DBEdit4: TDBEdit
        Left = 784
        Top = 56
        Width = 209
        Height = 21
        DataField = 'ide_natOp'
        DataSource = DsNFeCabA
        TabOrder = 8
      end
      object DBEdit7: TDBEdit
        Left = 684
        Top = 32
        Width = 28
        Height = 21
        DataField = 'ide_mod'
        DataSource = DsNFeCabA
        TabOrder = 9
      end
      object DBEdit8: TDBEdit
        Left = 748
        Top = 32
        Width = 32
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsNFeCabA
        TabOrder = 10
      end
      object DBEdit9: TDBEdit
        Left = 820
        Top = 32
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsNFeCabA
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 764
        Top = 8
        Width = 80
        Height = 21
        DataField = 'ide_dEmi'
        DataSource = DsNFeCabA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 884
        Top = 8
        Width = 64
        Height = 21
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 948
        Top = 8
        Width = 44
        Height = 21
        DataField = 'ide_hSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 168
        Top = 56
        Width = 16
        Height = 21
        DataField = 'ide_tpNF'
        DataSource = DsNFeCabA
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 272
        Top = 56
        Width = 16
        Height = 21
        DataField = 'ide_tpEmis'
        DataSource = DsNFeCabA
        TabOrder = 16
      end
      object DBEdit15: TDBEdit
        Left = 60
        Top = 56
        Width = 16
        Height = 21
        DataField = 'ide_tpAmb'
        DataSource = DsNFeCabA
        TabOrder = 17
      end
      object DBEdit16: TDBEdit
        Left = 400
        Top = 56
        Width = 16
        Height = 21
        DataField = 'ide_procEmi'
        DataSource = DsNFeCabA
        TabOrder = 18
      end
      object DBEdit17: TDBEdit
        Left = 456
        Top = 56
        Width = 272
        Height = 21
        DataField = 'Id'
        DataSource = DsNFeCabA
        TabOrder = 19
      end
      object DBEdit18: TDBEdit
        Left = 60
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vBC'
        DataSource = DsNFeCabA
        TabOrder = 20
      end
      object DBEdit19: TDBEdit
        Left = 208
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vICMS'
        DataSource = DsNFeCabA
        TabOrder = 21
      end
      object DBEdit20: TDBEdit
        Left = 356
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vBCST'
        DataSource = DsNFeCabA
        TabOrder = 22
      end
      object DBEdit21: TDBEdit
        Left = 516
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vST'
        DataSource = DsNFeCabA
        TabOrder = 23
      end
      object DBEdit22: TDBEdit
        Left = 672
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vProd'
        DataSource = DsNFeCabA
        TabOrder = 24
      end
      object DBEdit23: TDBEdit
        Left = 788
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vFrete'
        DataSource = DsNFeCabA
        TabOrder = 25
      end
      object DBEdit24: TDBEdit
        Left = 912
        Top = 80
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vSeg'
        DataSource = DsNFeCabA
        TabOrder = 26
      end
      object DBEdit25: TDBEdit
        Left = 780
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vDesc'
        DataSource = DsNFeCabA
        TabOrder = 27
      end
      object DBEdit26: TDBEdit
        Left = 48
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vII'
        DataSource = DsNFeCabA
        TabOrder = 28
      end
      object DBEdit27: TDBEdit
        Left = 180
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vIPI'
        DataSource = DsNFeCabA
        TabOrder = 29
      end
      object DBEdit28: TDBEdit
        Left = 316
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vPIS'
        DataSource = DsNFeCabA
        TabOrder = 30
      end
      object DBEdit29: TDBEdit
        Left = 472
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vCOFINS'
        DataSource = DsNFeCabA
        TabOrder = 31
      end
      object DBEdit30: TDBEdit
        Left = 616
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vOutro'
        DataSource = DsNFeCabA
        TabOrder = 32
      end
      object DBEdit31: TDBEdit
        Left = 912
        Top = 104
        Width = 80
        Height = 21
        DataField = 'ICMSTot_vNF'
        DataSource = DsNFeCabA
        TabOrder = 33
      end
      object DBEdit32: TDBEdit
        Left = 48
        Top = 128
        Width = 56
        Height = 21
        DataField = 'CodInfoTrsp'
        DataSource = DsNFeCabA
        TabOrder = 34
      end
      object DBEdit33: TDBEdit
        Left = 164
        Top = 128
        Width = 14
        Height = 21
        DataField = 'ModFrete'
        DataSource = DsNFeCabA
        TabOrder = 35
      end
      object DBEdit34: TDBEdit
        Left = 216
        Top = 128
        Width = 28
        Height = 21
        DataField = 'Status'
        DataSource = DsNFeCabA
        TabOrder = 36
      end
      object DBEdit36: TDBEdit
        Left = 320
        Top = 128
        Width = 68
        Height = 21
        DataField = 'DataFiscal'
        DataSource = DsNFeCabA
        TabOrder = 37
      end
      object DBEdit37: TDBEdit
        Left = 930
        Top = 32
        Width = 64
        Height = 21
        DataField = 'ide_dSaiEnt'
        DataSource = DsNFeCabA
        TabOrder = 38
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 438
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TStaticText
        Left = 173
        Top = 1
        Width = 30
        Height = 17
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItem: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItemClick
        end
        object BtInclui: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dados NF'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object DBGItens: TDBGrid
      Left = 1
      Top = 157
      Width = 1006
      Height = 216
      Align = alTop
      DataSource = DsStqMovIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Nome do produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_COR'
          Title.Caption = 'Cor'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TAM'
          Title.Caption = 'Tam.'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = 'Custo total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Baixa'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = 'Area m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorAll'
          Title.Caption = 'ValorTotal'
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Entrada de Uso e Consumo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 40
    Top = 12
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeCabABeforeOpen
    AfterOpen = QrNFeCabAAfterOpen
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    OnCalcFields = QrNFeCabACalcFields
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, '
      'emp.Filial, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'IF(frn.Tipo=0, frn.CNPJ, frn.CPF) CNPJ_CPF_FRN,'
      'nfa.*'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades frn ON frn.Codigo=nfa.CodInfoEmit'
      'LEFT JOIN entidades emp ON emp.Codigo=nfa.Empresa'
      '')
    Left = 12
    Top = 12
    object QrNFeCabAFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrNFeCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
      Origin = 'nfecaba.CodInfoEmit'
    end
    object QrNFeCabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
      Origin = 'nfecaba.CodInfoDest'
    end
    object QrNFeCabANO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrNFeCabANO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfecaba.FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfecaba.FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfecaba.Empresa'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfecaba.IDCtrl'
    end
    object QrNFeCabANO_FATID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_FATID'
      Size = 25
      Calculated = True
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfecaba.Id'
      Size = 44
    end
    object QrNFeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'nfecaba.ide_natOp'
      Size = 60
    end
    object QrNFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'nfecaba.ide_mod'
    end
    object QrNFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'nfecaba.ide_serie'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Origin = 'nfecaba.ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'nfecaba.ide_dEmi'
    end
    object QrNFeCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      Origin = 'nfecaba.ide_dSaiEnt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeCabAide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
      Origin = 'nfecaba.ide_hSaiEnt'
      DisplayFormat = 'hh:nn'
    end
    object QrNFeCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrNFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'nfecaba.ide_tpEmis'
    end
    object QrNFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'nfecaba.ide_tpAmb'
    end
    object QrNFeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
      Origin = 'nfecaba.ide_procEmi'
    end
    object QrNFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      Origin = 'nfecaba.ICMSTot_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Origin = 'nfecaba.ICMSTot_vICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      Origin = 'nfecaba.ICMSTot_vBCST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      Origin = 'nfecaba.ICMSTot_vST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      Origin = 'nfecaba.ICMSTot_vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      Origin = 'nfecaba.ICMSTot_vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      Origin = 'nfecaba.ICMSTot_vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      Origin = 'nfecaba.ICMSTot_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      Origin = 'nfecaba.ICMSTot_vII'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Origin = 'nfecaba.ICMSTot_vIPI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Origin = 'nfecaba.ICMSTot_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Origin = 'nfecaba.ICMSTot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      Origin = 'nfecaba.ICMSTot_vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Origin = 'nfecaba.ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeCabACodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
      Origin = 'nfecaba.CodInfoTrsp'
    end
    object QrNFeCabAModFrete: TSmallintField
      FieldName = 'ModFrete'
      Origin = 'nfecaba.ModFrete'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrNFeCabADataFiscal: TDateField
      FieldName = 'DataFiscal'
      Origin = 'nfecaba.DataFiscal'
    end
    object QrNFeCabAStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrNFeCabANFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Origin = 'nfecaba.NFG_Serie'
      Size = 3
    end
    object QrNFeCabARetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrNFeCabARetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrNFeCabARetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrNFeCabARetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrNFeCabARetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 5
    end
    object QrNFeCabARetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeCabACNPJ_CPF_FRN: TWideStringField
      FieldName = 'CNPJ_CPF_FRN'
      Size = 18
    end
    object QrNFeCabACNPJ_CPF_FRN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_FRN_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtItem
    Left = 68
    Top = 12
  end
  object QrStqMovIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrStqMovItsBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Tipo IN (51,151)'
      'AND OriCodi=23'
      ''
      'UNION'
      ''
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*'
      'FROM stqmovitsb smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Tipo IN (51,151)'
      'AND OriCodi=23'
      '')
    Left = 96
    Top = 12
    object QrStqMovItsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrStqMovItsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 50
    end
    object QrStqMovItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrStqMovItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrStqMovItsNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrStqMovItsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrStqMovItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrStqMovItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrStqMovItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrStqMovItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrStqMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrStqMovItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrStqMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrStqMovItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqMovItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrStqMovItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqMovItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqMovItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqMovItsOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrStqMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrStqMovItsQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrStqMovItsRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrStqMovItsParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrStqMovItsParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrStqMovItsDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrStqMovItsSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrStqMovItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsValorAll: TFloatField
      FieldName = 'ValorAll'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqMovItsGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrStqMovItsBaixa: TSmallintField
      FieldName = 'Baixa'
    end
  end
  object DsStqMovIts: TDataSource
    DataSet = QrStqMovIts
    Left = 124
    Top = 12
  end
  object QrNFMoDocFis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfmodocfis'
      'ORDER BY Nome')
    Left = 584
    object QrNFMoDocFisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFMoDocFisNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNFMoDocFis: TDataSource
    DataSet = QrNFMoDocFis
    Left = 612
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrForneceCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOME,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY Nome')
    Left = 640
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrForneceCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrForneceCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 668
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransportaCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOME,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF'
      'FROM entidades'
      'WHERE Fornece3='#39'V'#39
      'ORDER BY Nome')
    Left = 696
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrTransportaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrTransportaCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 724
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 564
    Top = 476
    object IncluinovaNF1: TMenuItem
      Caption = '&Inclui nova NF'
      object Manualmente1: TMenuItem
        Caption = '&Manualmente'
        OnClick = Manualmente1Click
      end
      object porXML1: TMenuItem
        Caption = '&por &XML'
        Enabled = False
        OnClick = porXML1Click
      end
    end
    object AlteraNF1: TMenuItem
      Caption = '&Altera NF'
      Enabled = False
      OnClick = AlteraNF1Click
    end
  end
  object PMItem: TPopupMenu
    Left = 656
    Top = 476
    object Incluinovoitem1: TMenuItem
      Caption = '&Inclui novo item'
      OnClick = Incluinovoitem1Click
    end
    object AlteraItemAtual1: TMenuItem
      Caption = '&Altera Item Atual'
      OnClick = AlteraItemAtual1Click
    end
    object ExcluiitemAtual1: TMenuItem
      Caption = '&Exclui item Atual'
      OnClick = ExcluiitemAtual1Click
    end
  end
  object QrNFeItsI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND MeuID=:P3')
    Left = 460
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFeItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFeItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFeItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrNFeItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFeItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrNFeItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrNFeItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrNFeItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFeItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrNFeItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrNFeItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrNFeItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrNFeItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrNFeItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrNFeItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
    end
    object QrNFeItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
    end
    object QrNFeItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
  end
  object QrNFeItsN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 488
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFeItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrNFeItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNFeItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNFeItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
  end
  object QrNFeItsO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 516
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrNFeItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object PMNumero: TPopupMenu
    Left = 136
    Top = 4
    object Cdigo1: TMenuItem
      Caption = '&C'#243'digo'
      OnClick = Cdigo1Click
    end
    object ID1: TMenuItem
      Caption = '&ID'
      OnClick = ID1Click
    end
  end
end
