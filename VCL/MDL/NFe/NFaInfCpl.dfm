object FmNFaInfCpl: TFmNFaInfCpl
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-002 :: Adi'#231#227'o de Informa'#231#245'es Complementares'
  ClientHeight = 538
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 772
    Height = 384
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 414
      Height = 384
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 414
        Height = 43
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 67
          Height = 13
          Caption = 'Filtro do t'#237'tulo:'
        end
        object SBInfCpl: TSpeedButton
          Left = 382
          Top = 20
          Width = 20
          Height = 19
          Caption = '...'
          OnClick = SBInfCplClick
        end
        object EdFiltro: TEdit
          Left = 8
          Top = 20
          Width = 372
          Height = 21
          TabOrder = 0
          OnChange = EdFiltroChange
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 43
        Width = 414
        Height = 341
        Align = alClient
        DataSource = DsNFeInfCpl
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodUsu'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 252
            Visible = True
          end>
      end
    end
    object DBMemo1: TDBMemo
      Left = 414
      Top = 0
      Width = 358
      Height = 384
      Align = alClient
      DataField = 'Texto'
      DataSource = DsNFeInfCpl
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 725
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 678
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 466
        Height = 31
        Caption = 'Adi'#231#227'o de Informa'#231#245'es Complementares'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 466
        Height = 31
        Caption = 'Adi'#231#227'o de Informa'#231#245'es Complementares'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 466
        Height = 31
        Caption = 'Adi'#231#227'o de Informa'#231#245'es Complementares'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 431
    Width = 772
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 14
      Width = 768
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 474
    Width = 772
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 14
      Width = 768
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 627
        Top = 0
        Width = 142
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 269
        Left = 20
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Adiciona'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrNFeInfCpl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeinfcpl')
    Left = 12
    Top = 8
    object QrNFeInfCplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeInfCplCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeInfCplNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNFeInfCplTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsNFeInfCpl: TDataSource
    DataSet = QrNFeInfCpl
    Left = 40
    Top = 8
  end
end
