unit NFeLayout_Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DB, Mask,
  DBCtrls, dmkEdit, dmkMemo, dmkCheckBox, dmkGeral, UnDmkEnums, dmkImage,
  Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmNFeLayout_Edit = class(TForm)
    Panel1: TPanel;
    EdGrupo: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdID: TdmkEdit;
    EdCampo: TdmkEdit;
    EdDescricao: TdmkEdit;
    EdElemento: TdmkEdit;
    EdPai: TdmkEdit;
    EdTipo: TdmkEdit;
    EdOcorMin: TdmkEdit;
    EdOcorMax: TdmkEdit;
    EdTamMin: TdmkEdit;
    EdTamMax: TdmkEdit;
    EdTamVar: TdmkEdit;
    EdDeciCasas: TdmkEdit;
    MeObservacao: TdmkMemo;
    Label15: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CkInfoVazio: TdmkCheckBox;
    CkLeftZeros: TdmkCheckBox;
    EdVersao: TdmkEdit;
    Label17: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdSubGrupo: TdmkEdit;
    Label18: TLabel;
    EdCodigoN: TdmkEdit;
    Label19: TLabel;
    EdCodigoX: TdmkEdit;
    Label20: TLabel;
    Label16: TLabel;
    EdFormatStr: TdmkEdit;
    TPDtIniProd: TdmkEditDateTimePicker;
    Label21: TLabel;
    Label22: TLabel;
    TPDtIniHomo: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCodigoRedefinido(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FID, FPai: String;
    FVersao: Double;
    FContinua: Boolean;
  end;

  var
  FmNFeLayout_Edit: TFmNFeLayout_Edit;

implementation

{$R *.DFM}

uses UnMyObjects, UMySQLModule, Module, UnDmkProcFunc;


procedure TFmNFeLayout_Edit.BtConfirmaClick(Sender: TObject);
var
(*
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'nfelayi', False, [
  'Grupo', 'Campo', 'Descricao',
  'Elemento', 'Pai', 'Tipo',
  'OcorMin', 'OcorMax', 'TamMin',
  'TamMax', 'TamVar', 'DeciCasas',
  'Observacao', 'LeftZeros', 'InfoVazio',
  'FormatStr', 'Versao',
  'CodigoN', 'CodigoX'], [
  'Codigo', 'ID'], [
  Grupo, Campo, Descricao,
  Elemento, Pai, Tipo,
  OcorMin, OcorMax, TamMin,
  TamMax, TamVar, DeciCasas,
  Observacao, LeftZeros, InfoVazio,
  FormatStr, Versao,
  CodigoN, CodigoX], [
  Codigo, ID], False) then
*)
  DeciCasas, OcorMax, TamMin, TamMax, LeftZeros, InfoVazio: Integer;
  TamVar, Grupo, Campo, Descricao, Elemento, Pai, Tipo, OcorMin, Observacao,
  Codigo, ID, FormatStr, CodigoX, SubGrupo: String;
  CodigoN, Versao: Double;
  //
  Campos: array of String;
  Valores: array of Variant;
  SQLType: TSQLType;
  DtIniProd, DtIniHomo: String;
begin
  SQLType      := ImgTipo.SQLType;
  if SQLType = stCpy then
    SQLType    := stIns;
  //
  DeciCasas    := EdDeciCasas.ValueVariant;
  OcorMax      := EdOcorMax.ValueVariant;
  TamMin       := EdTamMin.ValueVariant;
  TamMax       := EdTamMax.ValueVariant;
  TamVar       := EdTamVar.ValueVariant;
  LeftZeros    := MLAGeral.BTI(CkLeftZeros.Checked);
  InfoVazio    := MLAGeral.BTI(CkInfoVazio.Checked);
  Grupo        := EdGrupo.ValueVariant;
  Campo        := EdCampo.ValueVariant;
  Descricao    := EdDescricao.ValueVariant;
  Elemento     := EdElemento.ValueVariant;
  Pai          := EdPai.ValueVariant;
  Tipo         := EdTipo.ValueVariant;
  OcorMin      := EdOcorMin.ValueVariant;
  Observacao   := MeObservacao.Text;
  Codigo       := EdCodigo.ValueVariant;
  ID           := EdID.ValueVariant;
  FormatStr    := EdFormatStr.ValueVariant;
  Versao       := EdVersao.ValueVariant;
  SubGrupo     := EdSubGrupo.Text;
  CodigoN      := EdCodigoN.ValueVariant;
  CodigoX      := EdCodigoX.Text;
  DtIniProd    := Geral.FDT(TPDtIniProd.Date, 1);
  DtIniHomo    := Geral.FDT(TPDtIniHomo.Date, 1);
  //
  if SQLType = stIns then
  begin
    FCodigo    := Codigo;
    FID        := ID;
    FPai       := Pai;
    FVersao    := Versao;
  end;
  //
  if SQLType = stUpd then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfelayi', False, [
    'Grupo', 'Campo', 'Descricao',
    'Elemento', 'Tipo', 'OcorMin',
    'OcorMax', 'TamMin', 'TamMax',
    'TamVar', 'DeciCasas', 'Observacao',
    'LeftZeros', 'InfoVazio', 'FormatStr',
    'CodigoN', 'CodigoX', 'SubGrupo',
    'Codigo', 'ID', 'Pai', 'Versao',
    'DtIniHomo', 'DtIniProd'],
    [
    'Codigo', 'ID', 'Pai', 'Versao'],
    [
    Grupo, Campo, Descricao,
    Elemento, Tipo, OcorMin,
    OcorMax, TamMin, TamMax,
    TamVar, DeciCasas, Observacao,
    LeftZeros, InfoVazio, FormatStr,
    CodigoN, CodigoX, SubGrupo,
    Codigo, ID, Pai, Versao,
    DtIniHomo, DtIniProd],
    [
    FCodigo, FID, FPai, FVersao], False) then
      Close;
  end else
  // stIns
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfelayi', False, [
    'Grupo', 'Campo', 'Descricao',
    'Elemento', 'Tipo', 'OcorMin',
    'OcorMax', 'TamMin', 'TamMax',
    'TamVar', 'DeciCasas', 'Observacao',
    'LeftZeros', 'InfoVazio', 'FormatStr',
    'CodigoN', 'CodigoX', 'SubGrupo',
    'DtIniHomo', 'DtIniProd'],
    [
    'Codigo', 'ID', 'Pai', 'Versao'],
    [
    Grupo, Campo, Descricao,
    Elemento, Tipo, OcorMin,
    OcorMax, TamMin, TamMax,
    TamVar, DeciCasas, Observacao,
    LeftZeros, InfoVazio, FormatStr,
    CodigoN, CodigoX, SubGrupo,
    DtIniHomo, DtIniProd],
    [
    FCodigo, FID, FPai, FVersao], False) then
      Close;
  end;
end;

procedure TFmNFeLayout_Edit.BtSaidaClick(Sender: TObject);
begin
  FContinua := False;
  Close;
end;

procedure TFmNFeLayout_Edit.EdCodigoRedefinido(Sender: TObject);
var
  Codigo, CodigoX: String;
  CodigoN: Double;
begin
  try
    Codigo := EdCodigo.ValueVariant;
    DmkPF.SeparaNumeroDaDireita_TT_Dot2(Codigo, CodigoN, CodigoX);
    EdCodigoN.ValueVariant := CodigoN;
    EdCodigoX.Text := CodigoX;
  finally
    // Nada
  end;
end;

procedure TFmNFeLayout_Edit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLayout_Edit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FContinua := True;
end;

procedure TFmNFeLayout_Edit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
