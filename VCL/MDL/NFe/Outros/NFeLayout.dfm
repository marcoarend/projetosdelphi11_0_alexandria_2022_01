object FmNFeLayout: TFmNFeLayout
  Left = 339
  Top = 185
  Caption = 'NFE-LAYOU-001 :: Importa'#231#227'o de layout de NF-e'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtAbre: TBitBtn
      Left = 404
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre xls'
      TabOrder = 0
      Visible = False
      OnClick = BtAbreClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtCarrega: TBitBtn
      Left = 588
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Carrega xls'
      Enabled = False
      TabOrder = 2
      Visible = False
      OnClick = BtCarregaClick
      NumGlyphs = 2
    end
    object BitBtn1: TBitBtn
      Left = 496
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Salva txt'
      TabOrder = 3
      Visible = False
      OnClick = BitBtn1Click
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Left = 680
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre txt'
      TabOrder = 4
      Visible = False
      OnClick = BitBtn2Click
      NumGlyphs = 2
    end
    object BitBtn3: TBitBtn
      Tag = 19
      Left = 312
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Baixar'
      TabOrder = 5
      Visible = False
      OnClick = BitBtn3Click
      NumGlyphs = 2
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 5
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui nova entidade'
      Caption = '&Inclui'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Visible = False
      NumGlyphs = 2
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 106
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera entidade atual'
      Caption = '&Altera'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Importa'#231#227'o de layout de NF-e'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 1004
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 97
      Width = 1006
      Height = 300
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Abertos '
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 998
          Height = 272
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Carregados '
        ImageIndex = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 784
          Height = 272
          Align = alClient
          Color = clWindow
          DataSource = DsNFeLayI
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBMemo1: TDBMemo
          Left = 784
          Top = 0
          Width = 214
          Height = 272
          Align = alRight
          DataField = 'Observacao'
          DataSource = DsNFeLayI
          TabOrder = 1
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 96
      Align = alTop
      TabOrder = 1
      Visible = False
      object Label27: TLabel
        Left = 8
        Top = 7
        Width = 116
        Height = 13
        Caption = 'Arquivo a ser carregado:'
      end
      object SpeedButton8: TSpeedButton
        Left = 974
        Top = 23
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton8Click
      end
      object LaAviso: TLabel
        Left = 12
        Top = 48
        Width = 15
        Height = 22
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdArq: TdmkEdit
        Left = 8
        Top = 23
        Width = 965
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'http://www.dermatek.com.br/Instaladores/Manual_de_Integracao_Con' +
          'tribuinte_v3.00-2009-03-16.xls'
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'http://www.dermatek.com.br/Instaladores/Manual_de_Integracao_Con' +
          'tribuinte_v3.00-2009-03-16.xls'
      end
      object PB1: TProgressBar
        Left = 12
        Top = 72
        Width = 985
        Height = 17
        TabOrder = 1
      end
    end
  end
  object QrNFeLayI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfelayi')
    Left = 88
    Top = 204
    object QrNFeLayIGrupo: TWideStringField
      FieldName = 'Grupo'
      Size = 2
    end
    object QrNFeLayICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 11
    end
    object QrNFeLayIID: TWideStringField
      FieldName = 'ID'
      Size = 7
    end
    object QrNFeLayICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrNFeLayIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeLayIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrNFeLayIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeLayITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrNFeLayIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrNFeLayIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrNFeLayITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrNFeLayITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrNFeLayITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrNFeLayIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrNFeLayIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsNFeLayI: TDataSource
    DataSet = QrNFeLayI
    Left = 116
    Top = 204
  end
end
