object FmNFeLayout_0200: TFmNFeLayout_0200
  Left = 339
  Top = 185
  Caption = 'NFE-LAYOU-001 :: Layout de NF-e 2.00'
  ClientHeight = 494
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1264
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtAbre: TBitBtn
      Left = 404
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre xls'
      NumGlyphs = 2
      TabOrder = 0
      Visible = False
      OnClick = BtAbreClick
    end
    object Panel2: TPanel
      Left = 1152
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtCarrega: TBitBtn
      Left = 588
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Carrega xls'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      Visible = False
      OnClick = BtCarregaClick
    end
    object BitBtn1: TBitBtn
      Left = 496
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Salva txt'
      NumGlyphs = 2
      TabOrder = 3
      Visible = False
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 680
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre txt'
      NumGlyphs = 2
      TabOrder = 4
      Visible = False
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Tag = 19
      Left = 312
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Baixar'
      NumGlyphs = 2
      TabOrder = 5
      Visible = False
      OnClick = BitBtn3Click
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 5
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui nova entidade'
      Caption = '&Inclui'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = BtIncluiClick
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 106
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera entidade atual'
      Caption = '&Altera'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = BtAlteraClick
    end
    object BtOrdena: TBitBtn
      Left = 774
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera entidade atual'
      Caption = '&Ordena'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = BtOrdenaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    Caption = 'Layout de NF-e 2.00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1262
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 398
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 97
      Width = 1262
      Height = 300
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Abertos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 1254
          Height = 272
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Carregados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1065
          Height = 272
          Align = alLeft
          DataSource = DsNFeLayI
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Grupo'
              Title.Caption = 'G'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 92
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Elemento'
              Title.Caption = 'Elem.'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pai'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo'
              Title.Caption = 'T'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OcorMin'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OcorMax'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TamMin'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TamMax'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TamVar'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DeciCasas'
              Title.Caption = 'D'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LeftZeros'
              Title.Caption = 'Z'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InfoVazio'
              Title.Caption = 'V'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormatStr'
              Width = 132
              Visible = True
            end>
        end
        object DBMemo1: TDBMemo
          Left = 1065
          Top = 0
          Width = 189
          Height = 272
          Align = alClient
          DataField = 'Observacao'
          DataSource = DsNFeLayI
          TabOrder = 1
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'TabSheet3'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1254
          Height = 272
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 32
            Height = 13
            Caption = 'Grupo:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 44
            Top = 8
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 88
            Top = 8
            Width = 14
            Height = 13
            Caption = 'ID:'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Left = 132
            Top = 8
            Width = 36
            Height = 13
            Caption = 'Campo:'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 8
            Top = 48
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdit5
          end
          object Label6: TLabel
            Left = 316
            Top = 8
            Width = 47
            Height = 13
            Caption = 'Elemento:'
            FocusControl = DBEdit6
          end
          object Label7: TLabel
            Left = 368
            Top = 8
            Width = 18
            Height = 13
            Caption = 'Pai:'
            FocusControl = DBEdit7
          end
          object Label8: TLabel
            Left = 428
            Top = 8
            Width = 24
            Height = 13
            Caption = 'Tipo:'
            FocusControl = DBEdit8
          end
          object Label9: TLabel
            Left = 460
            Top = 8
            Width = 53
            Height = 13
            Caption = 'Ocor. m'#237'n.:'
            FocusControl = DBEdit9
          end
          object Label10: TLabel
            Left = 528
            Top = 8
            Width = 51
            Height = 13
            Caption = 'Ocor. m'#225'x.'
            FocusControl = DBEdit10
          end
          object Label11: TLabel
            Left = 596
            Top = 8
            Width = 51
            Height = 13
            Caption = 'Tam. m'#237'n.:'
            FocusControl = DBEdit11
          end
          object Label12: TLabel
            Left = 664
            Top = 8
            Width = 52
            Height = 13
            Caption = 'Tam. m'#225'x.:'
            FocusControl = DBEdit12
          end
          object Label13: TLabel
            Left = 732
            Top = 8
            Width = 88
            Height = 13
            Caption = 'Tamanho vari'#225'vel:'
            FocusControl = DBEdit13
          end
          object Label14: TLabel
            Left = 924
            Top = 8
            Width = 46
            Height = 13
            Caption = 'Decimais:'
            FocusControl = DBEdit14
          end
          object Label15: TLabel
            Left = 8
            Top = 88
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
            FocusControl = DBMemo2
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 24
            Width = 32
            Height = 21
            DataField = 'Grupo'
            DataSource = DsNFeLayI
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 44
            Top = 24
            Width = 40
            Height = 21
            DataField = 'Codigo'
            DataSource = DsNFeLayI
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 88
            Top = 24
            Width = 40
            Height = 21
            DataField = 'ID'
            DataSource = DsNFeLayI
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 132
            Top = 24
            Width = 180
            Height = 21
            DataField = 'Campo'
            DataSource = DsNFeLayI
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 64
            Width = 981
            Height = 21
            DataField = 'Descricao'
            DataSource = DsNFeLayI
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 316
            Top = 24
            Width = 48
            Height = 21
            DataField = 'Elemento'
            DataSource = DsNFeLayI
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 368
            Top = 24
            Width = 56
            Height = 21
            DataField = 'Pai'
            DataSource = DsNFeLayI
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 428
            Top = 24
            Width = 28
            Height = 21
            DataField = 'Tipo'
            DataSource = DsNFeLayI
            TabOrder = 7
          end
          object DBEdit9: TDBEdit
            Left = 460
            Top = 24
            Width = 64
            Height = 21
            DataField = 'OcorMin'
            DataSource = DsNFeLayI
            TabOrder = 8
          end
          object DBEdit10: TDBEdit
            Left = 528
            Top = 24
            Width = 64
            Height = 21
            DataField = 'OcorMax'
            DataSource = DsNFeLayI
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 596
            Top = 24
            Width = 64
            Height = 21
            DataField = 'TamMin'
            DataSource = DsNFeLayI
            TabOrder = 10
          end
          object DBEdit12: TDBEdit
            Left = 664
            Top = 24
            Width = 64
            Height = 21
            DataField = 'TamMax'
            DataSource = DsNFeLayI
            TabOrder = 11
          end
          object DBEdit13: TDBEdit
            Left = 732
            Top = 24
            Width = 188
            Height = 21
            DataField = 'TamVar'
            DataSource = DsNFeLayI
            TabOrder = 12
          end
          object DBEdit14: TDBEdit
            Left = 924
            Top = 24
            Width = 64
            Height = 21
            DataField = 'DeciCasas'
            DataSource = DsNFeLayI
            TabOrder = 13
          end
          object DBMemo2: TDBMemo
            Left = 8
            Top = 104
            Width = 981
            Height = 281
            DataField = 'Observacao'
            DataSource = DsNFeLayI
            TabOrder = 14
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1262
      Height = 96
      Align = alTop
      TabOrder = 1
      Visible = False
      object Label27: TLabel
        Left = 8
        Top = 7
        Width = 116
        Height = 13
        Caption = 'Arquivo a ser carregado:'
      end
      object SpeedButton8: TSpeedButton
        Left = 974
        Top = 23
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton8Click
      end
      object LaAviso: TLabel
        Left = 12
        Top = 48
        Width = 15
        Height = 22
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdArq: TdmkEdit
        Left = 8
        Top = 23
        Width = 965
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'http://www.dermatek.com.br/Instaladores/Manual_de_Integracao_Con' +
          'tribuinte_v3.00-2009-03-16.xls'
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'http://www.dermatek.com.br/Instaladores/Manual_de_Integracao_Con' +
          'tribuinte_v3.00-2009-03-16.xls'
        ValWarn = False
      end
      object PB1: TProgressBar
        Left = 12
        Top = 72
        Width = 985
        Height = 17
        TabOrder = 1
      end
    end
  end
  object QrNFeLayI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lay.* '
      'FROM nfelayi lay'
      'WHERE lay.versao=2.00'
      'ORDER BY CodigoN, CodigoX, ID')
    Left = 88
    Top = 204
    object QrNFeLayIGrupo: TWideStringField
      FieldName = 'Grupo'
      Origin = 'nfelayi.Grupo'
      Size = 2
    end
    object QrNFeLayICodigo: TWideStringField
      FieldName = 'Codigo'
      Origin = 'nfelayi.Codigo'
      Size = 11
    end
    object QrNFeLayIID: TWideStringField
      FieldName = 'ID'
      Origin = 'nfelayi.ID'
      Size = 7
    end
    object QrNFeLayICampo: TWideStringField
      FieldName = 'Campo'
      Origin = 'nfelayi.Campo'
      Size = 30
    end
    object QrNFeLayIDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'nfelayi.Descricao'
      Size = 100
    end
    object QrNFeLayIElemento: TWideStringField
      FieldName = 'Elemento'
      Origin = 'nfelayi.Elemento'
      Size = 10
    end
    object QrNFeLayIPai: TWideStringField
      FieldName = 'Pai'
      Origin = 'nfelayi.Pai'
      Size = 10
    end
    object QrNFeLayITipo: TWideStringField
      FieldName = 'Tipo'
      Origin = 'nfelayi.Tipo'
      Size = 1
    end
    object QrNFeLayIOcorMin: TSmallintField
      FieldName = 'OcorMin'
      Origin = 'nfelayi.OcorMin'
    end
    object QrNFeLayIOcorMax: TIntegerField
      FieldName = 'OcorMax'
      Origin = 'nfelayi.OcorMax'
    end
    object QrNFeLayITamMin: TSmallintField
      FieldName = 'TamMin'
      Origin = 'nfelayi.TamMin'
    end
    object QrNFeLayITamMax: TIntegerField
      FieldName = 'TamMax'
      Origin = 'nfelayi.TamMax'
    end
    object QrNFeLayITamVar: TWideStringField
      FieldName = 'TamVar'
      Origin = 'nfelayi.TamVar'
      Size = 30
    end
    object QrNFeLayIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
      Origin = 'nfelayi.DeciCasas'
    end
    object QrNFeLayIObservacao: TWideMemoField
      FieldName = 'Observacao'
      Origin = 'nfelayi.Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeLayIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfelayi.AlterWeb'
    end
    object QrNFeLayIAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfelayi.Ativo'
    end
    object QrNFeLayILeftZeros: TSmallintField
      FieldName = 'LeftZeros'
      Origin = 'nfelayi.LeftZeros'
      MaxValue = 1
    end
    object QrNFeLayIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
      Origin = 'nfelayi.InfoVazio'
      MaxValue = 1
    end
    object QrNFeLayIFormatStr: TWideStringField
      DisplayWidth = 30
      FieldName = 'FormatStr'
    end
    object QrNFeLayIVersao: TFloatField
      FieldName = 'Versao'
    end
  end
  object DsNFeLayI: TDataSource
    DataSet = QrNFeLayI
    Left = 116
    Top = 204
  end
  object PMAltera: TPopupMenu
    Left = 140
    Top = 420
    object Somenteoitemselecionado1: TMenuItem
      Caption = '&Somente o item selecionado'
      OnClick = Somenteoitemselecionado1Click
    end
    object odositensapartirdoatual1: TMenuItem
      Caption = '&Todos itens a partir do atual'
      OnClick = odositensapartirdoatual1Click
    end
  end
end
