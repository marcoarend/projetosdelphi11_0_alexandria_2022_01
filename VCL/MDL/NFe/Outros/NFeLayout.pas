unit NFeLayout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     dmkEdit, Grids,
  ComObj, ComCtrls, Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid,
  DBCtrls;

type
  TFmNFeLayout = class(TForm)
    PainelConfirma: TPanel;
    BtAbre: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    BtCarrega: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    QrNFeLayI: TmySQLQuery;
    DsNFeLayI: TDataSource;
    DBMemo1: TDBMemo;
    QrNFeLayIGrupo: TWideStringField;
    QrNFeLayICodigo: TWideStringField;
    QrNFeLayIID: TWideStringField;
    QrNFeLayICampo: TWideStringField;
    QrNFeLayIDescricao: TWideStringField;
    QrNFeLayIElemento: TWideStringField;
    QrNFeLayIPai: TWideStringField;
    QrNFeLayITipo: TWideStringField;
    QrNFeLayIOcorMin: TSmallintField;
    QrNFeLayIOcorMax: TIntegerField;
    QrNFeLayITamMin: TSmallintField;
    QrNFeLayITamMax: TIntegerField;
    QrNFeLayITamVar: TWideStringField;
    QrNFeLayIDeciCasas: TSmallintField;
    QrNFeLayIObservacao: TWideMemoField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdArq: TdmkEdit;
    PB1: TProgressBar;
    BitBtn3: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNFeLayI();
    procedure CarregaXLS();
  public
    { Public declarations }
  end;

  var
  FmNFeLayout: TFmNFeLayout;

implementation

uses UnMyObjects, Module, UMySQLModule;

const
  FTits = 12;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Grupo',
{01}                            '#',
{02}                            'ID',
{03}                            'Campo',
{04}                            'Descri��o',
{05}                            'Ele',
{06}                            'Pai',
{07}                            'Tipo',
{08}                            'Ocorr�ncia',
{09}                            'Tamanho',
{10}                            'Dec.',
{11}                            'Observa��o');
//# ID Campo Descri��o Ele Pai Tipo Ocorr�ncia tamanho Dec. Observa��o

{$R *.DFM}

procedure TFmNFeLayout.BitBtn1Click(Sender: TObject);
begin
  MLAGeral.SalvaStringGridToFile(Grade1, 'C:\Dermatek\StringGrids\NFeLayout.txt');
end;

procedure TFmNFeLayout.BitBtn2Click(Sender: TObject);
begin
  MLAGeral.LeArquivoToStringGrid(Grade1, 'C:\Dermatek\StringGrids\NFeLayout.txt');
  BtCarrega.Enabled := True;
end;

procedure TFmNFeLayout.BitBtn3Click(Sender: TObject);
const
  DestFile = 'C:\Dermatek\Manual_de_Integracao_Contribuinte_NFe.xls';
var
  Fonte: String;
begin
  Fonte := EdArq.Text;
  if FileExists(DestFile) then DeleteFile(DestFile);
  LaAviso.Caption := 'Aguarde... Baixando arquivo!';
  Application.ProcessMessages;
  if MLAGeral.DownloadFile(Fonte, DestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
    {
    LaAviso.Caption := 'Aguarde... Renomeando arquivo!';
    Application.ProcessMessages;

    Arq := ExtractFilePath(Application.ExeName) +
      STAppArqName.Caption + ExtractFileExt(Application.ExeName);
        //
    if STAppArqName.Caption <> ExtractFileName(Application.ExeName) then
    begin
      Renomear := FileExists(Arq);
    end;
    if Renomear then
    begin
      if not MLAGeral.RenameAppOlder(Arq) then
      begin
        LaAviso.Caption := 'Erro... N�o foi poss�vel renomear o arquivo!';
        Application.ProcessMessages;
        Exit;
      end;
    end;
    LaAviso.Caption := 'Aguarde... Decompactando arquivo!';
    Application.ProcessMessages;
    if DesZipaArquivo(Destino, ExtractFilePath(Application.ExeName)) then
    begin
      LaAviso.Caption := 'OK... Processo terminado!';
      Application.ProcessMessages;
      Application.MessageBox('OK... Processo terminado!', 'Aviso',
      MB_OK+MB_ICONEXCLAMATION);
      case RGTipo.ItemIndex of
        0: // Executavel
        begin
          if STAppArqName.Caption = ExtractFileName(Application.ExeName) then
            Application.Terminate
          else begin
            //terminar aplicativo e come�ar novo
            (*HandleJan := FindWindow(PChar(STAppArqName.Caption), nil);
            if HandleJan <> 0 then
              SendMessage(HandleJan, WM_CLOSE, 0, 0);*)
            MLAGeral.FecharOutraAplicacao(STAppArqTitle.Caption, Handle);
            // Qual dos dois � ? Acima ou abaixo ?
            MLAGeral.FecharOutraAplicacao(STAppArqName.Caption, Handle);
            dmkPF.WinExecAndWaitOrNot(Arq, 0, nil, False);
            // For�ar encerramento
            Halt(0);
          end;
        end;
        1: ;// Tabelas
      end;
    end else
    begin
      MLAGeral.RenameAppOlder_Cancel;
      LaAviso.Caption := 'Erro ao descompactar arquivo!';
      Application.ProcessMessages;
    end;
    }
  end
  else LaAviso.Caption := 'Erro durante o download de "' +Fonte + '"';
end;

procedure TFmNFeLayout.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso);
  BtCarrega.Enabled := True;
end;

procedure TFmNFeLayout.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmNFeLayout.CarregaXLS();
var
  LeftZeros, InfoVazio: Byte;
  R, P, L, N: Integer;
  Grupo, Nome, ID, Campo, Descricao, Elemento, Pai, Tipo, Tamanho, Ocorrencia,
  Codigo, Observacao, Casas, TamVar, CodTxt, MinTxt, MaxTxt: String;
  OcorMin, OcorMax, TamMin, TamMax, DeciCasas, Ordem: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Ordem := 0;
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfelayc');
    Dmod.QrUpd.ExecSQL;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfelayi');
    Dmod.QrUpd.ExecSQL;
    //
    for R := 1 to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      CodTxt := Trim(Grade1.Cells[02,R]);
      if Grade1.Cells[01,R] <> '' then
      begin
        Nome := Trim(Grade1.Cells[01,R]);
        P := pos('-', Nome);
        Grupo := Trim(Copy(Nome, 1, P - 1));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelayc', False, [
        'Nome'], ['Grupo'], [Nome], [Grupo], False);
      end else
      if CodTxt <> '' then
      begin
        if CodTxt[1] in (['0'..'9']) then
          Codigo := CodTxt
        else
          Codigo := '';
        if Codigo <> '' then
        begin
          L := R + 1;
          while (Trim(Grade1.Cells[02,L]) = '') and (L < Grade1.RowCount) do
            L := L + 1;
          L := L -1;
          //#	ID	Campo	Descri��o	Ele	Pai	Tipo	Ocorr�ncia	tamanho	Dec.	Observa��o
          ID         := Grade1.Cells[03,R];
          Campo      := Grade1.Cells[04,R];
          Descricao  := Grade1.Cells[05,R];
          Elemento   := Grade1.Cells[06,R];
          Pai        := Grade1.Cells[07,R];
          Tipo       := Grade1.Cells[08,R];
          Ocorrencia := Grade1.Cells[09,R];
          Tamanho    := Grade1.Cells[10,R];
          Casas      := Grade1.Cells[11,R];
          Observacao := Grade1.Cells[12,R];
          for N := R + 1 to L do
          begin
            ID         := ID         + Grade1.Cells[03,N];
            Campo      := Campo      + Grade1.Cells[04,N];
            Descricao  := Descricao  + ' ' + Grade1.Cells[05,N];
            Elemento   := Elemento   + Grade1.Cells[06,N];
            Pai        := Pai        + Grade1.Cells[07,N];
            Tipo       := Tipo       + Grade1.Cells[08,N];
            Ocorrencia := Ocorrencia + Grade1.Cells[09,N];
            Tamanho    := Tamanho    + Grade1.Cells[10,N];
            Casas      := Casas      + Grade1.Cells[11,N];
            Observacao := Observacao + ' ' + Grade1.Cells[12,N];
          end;
          DeciCasas := Geral.IMV(Trim(Casas));
          //
          P := pos('-', Ocorrencia);
          if P > 0 then
          begin
            MinTxt   := Copy(Ocorrencia, 1, p - 1);
            OcorMin  := Geral.IMV(MinTxt);
            MaxTxt   := Copy(Ocorrencia, p + 1);
            if Uppercase(MaxTxt) = 'N' then
              OcorMax  := High(Integer)
            else
              OcorMax  := Geral.IMV(MaxTxt);
          end else begin
            OcorMin    := Geral.IMV(Ocorrencia);
            OcorMax    := OcorMin;
          end;
          //
          MLAGeral.SoNumeroDeTamanhoDeCampoDeNFe(Tamanho, Tamanho);
          //if not k then
          MLAGeral.SeparaIntervalosDeTamanhosDeNFe(Tamanho, Tamanho, TamVar);
          //
          Tamanho := Trim(Tamanho);
          //T := Length(Tamanho);
          P := pos('-', Tamanho);
          if P > 0 then
          begin
            MinTxt  := Trim(Copy(Tamanho, 1, p - 1));
            TamMin  := Geral.IMV(MinTxt);
            //
            MaxTxt  := Trim(Copy(Tamanho, P + 1));
            if Uppercase(MaxTxt) = 'N' then
              TamMax  := High(Integer)
            else
              TamMax  := Geral.IMV(MaxTxt);
            //
            //TamVar  := '';
          end else begin
            TamMin  := Geral.IMV(Tamanho);
            TamMax  := TamMin;
            //TamVar  := '';
          end;
          if pos('CNPJ', Campo) > 0 then LeftZeros := 1 else
          if pos('CPF',  Campo) > 0 then LeftZeros := 1 else
          if pos('CEP',  Campo) > 0 then LeftZeros := 1 else
          if pos('CST',  Campo) > 0 then LeftZeros := 1 else
          if pos('NCM',  Campo) > 0 then LeftZeros := 1 else
          if pos('EAN',  Campo) > 0 then LeftZeros := 1 else
          if Campo = 'cNF'          then LeftZeros := 1 else
                                         LeftZeros := 0;
          if pos('EAN',  Campo) > 0 then InfoVazio := 1 else
                                         InfoVazio := 0;
          Ordem := Ordem + 1;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelayi', False, [
          'Grupo', 'Codigo', 'ID',
          'Campo', 'Descricao', 'Elemento',
          'Pai', 'Tipo', 'OcorMin',
          'OcorMax', 'TamMin', 'TamMax',
          'TamVar', 'DeciCasas', 'Observacao',
          'LeftZeros', 'InfoVazio'], [
          'Ordem'], [
          Grupo, Codigo, ID,
          Campo, Descricao, Elemento,
          Pai, Tipo, OcorMin,
          OcorMax, TamMin, TamMax,
          TamVar, DeciCasas, Observacao,
          LeftZeros, InfoVazio
          ], [
          Ordem], False);
        end;
      end
    end;
    PB1.Position := PB1.Max;
    ReopenNFeLayI();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeLayout.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLayout.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLayout.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  Grade1.ColCount := FTits;
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  ReopenNFeLayI();
end;

procedure TFmNFeLayout.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmNFeLayout.ReopenNFeLayI();
begin
  QrNFeLayI.Close;
  QrNFeLayI.Open;
end;

procedure TFmNFeLayout.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArq.Text);
  Arquivo := ExtractFileName(EdArq.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArq.Text := Arquivo;
end;

end.
