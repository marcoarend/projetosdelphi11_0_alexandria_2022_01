object FmNFeValidaXML_0001: TFmNFeValidaXML_0001
  Left = 339
  Top = 185
  Caption = 'NFe-VALID-001 :: Valida'#231#227'o de Arquivo XML NF-e'
  ClientHeight = 612
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Valida'#231#227'o de Arquivo XML NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 548
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtValida: TBitBtn
        Tag = 14
        Left = 135
        Top = 3
        Width = 120
        Height = 40
        Caption = 'Validar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtValidaClick
      end
      object BtCarrega: TBitBtn
        Tag = 14
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Cancel = True
        Caption = 'Carrega'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCarregaClick
      end
    end
  end
  object PCValidacao: TPageControl
    Left = 0
    Top = 96
    Width = 784
    Height = 408
    ActivePage = TabSheet1
    Align = alClient
    TabHeight = 25
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Valida Sefaz RS'
      object WBSefaz: TWebBrowser
        Left = 0
        Top = 0
        Width = 776
        Height = 373
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 768
        ExplicitHeight = 108
        ControlData = {
          4C000000345000008D2600000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E12620A000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Valida usando Schemas'
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 373
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 222
          Width = 776
          Height = 7
          Cursor = crVSplit
          Align = alBottom
          ExplicitTop = 146
          ExplicitWidth = 784
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label64: TLabel
            Left = 8
            Top = 4
            Width = 136
            Height = 13
            Caption = 'Arquivos Schema xml (*.xsd):'
          end
          object SpeedButton4: TSpeedButton
            Left = 752
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton4Click
          end
          object CBSchema: TComboBox
            Left = 8
            Top = 20
            Width = 741
            Height = 21
            Style = csDropDownList
            TabOrder = 0
          end
        end
        object MeScaned: TMemo
          Left = 0
          Top = 246
          Width = 776
          Height = 127
          Align = alBottom
          TabOrder = 1
        end
        object PCTexto: TPageControl
          Left = 0
          Top = 65
          Width = 776
          Height = 157
          ActivePage = TabSheet4
          Align = alClient
          TabHeight = 25
          TabOrder = 2
          object TabSheet3: TTabSheet
            Caption = 'XML'
            object WBXml: TWebBrowser
              Left = 0
              Top = 0
              Width = 768
              Height = 106
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 776
              ExplicitHeight = 149
              ControlData = {
                4C000000604F0000F50A00000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E12620A000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
            object Panel2: TPanel
              Left = 0
              Top = 106
              Width = 768
              Height = 16
              Align = alBottom
              BevelOuter = bvNone
              Caption = 'Precione as teclas Ctrl + F para pesquisar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Texto'
            ImageIndex = 1
            object MeLoaded: TMemo
              Left = 0
              Top = 0
              Width = 768
              Height = 122
              Align = alClient
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 48
          Width = 776
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Texto a ser validado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          ExplicitWidth = 124
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 229
          Width = 776
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Resultado da Valida'#231#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          ExplicitWidth = 143
        end
      end
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 48
    Align = alTop
    TabOrder = 0
    object SpeedButton2: TSpeedButton
      Left = 752
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 133
      Height = 13
      Caption = 'Arquivo XML a ser validado:'
    end
    object EdArquivo: TdmkEdit
      Left = 8
      Top = 20
      Width = 741
      Height = 21
      MaxLength = 255
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdArquivoChange
    end
  end
end
