unit NFeValidaXML_0001;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, ACBrMSXML2_TLB, dmkEdit, dmkImage,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, Vcl.OleCtrls, SHDocVw, ShellAPI,
  Xml.XMLDoc, MSXML2_TLB;

type
  TFmNFeValidaXML_0001 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtValida: TBitBtn;
    BtCarrega: TBitBtn;
    PCValidacao: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Label64: TLabel;
    SpeedButton4: TSpeedButton;
    CBSchema: TComboBox;
    MeScaned: TMemo;
    PCTexto: TPageControl;
    TabSheet3: TTabSheet;
    WBXml: TWebBrowser;
    Panel2: TPanel;
    TabSheet4: TTabSheet;
    MeLoaded: TMemo;
    Panel9: TPanel;
    SpeedButton2: TSpeedButton;
    EdArquivo: TdmkEdit;
    Label1: TLabel;
    WBSefaz: TWebBrowser;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdArquivoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtValidaClick(Sender: TObject);
  private
    { Private declarations }
    FDir: String;
    FTextoArq: AnsiString;
    procedure AbreArquivoSelecionado();
    procedure LimpaCamposXml;
    function  ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    function  ValidaMSXML_0200(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
    function  RetornarConteudoEntre(const Frase, Inicio, Fim: string): string;
    function  ValidaMSXML_Geral(): Boolean;
  public
    { Public declarations }
  end;

  var
    FmNFeValidaXML_0001: TFmNFeValidaXML_0001;
  const
    CO_URL_SEFAZ = 'https://www.sefaz.rs.gov.br/nfe/nfe-val.aspx';

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, UnDmkWeb;

{$R *.DFM}

procedure TFmNFeValidaXML_0001.AbreArquivoSelecionado();
var
  Arquivo: String;
begin
  FTextoArq := '';
  Arquivo   := EdArquivo.Text;
  //
  if FileExists(Arquivo) then
  begin
    FTextoArq     := dmkPF.LoadFileToText(Arquivo);
    MeLoaded.Text := FTextoArq;
    //
    WBXml.Navigate('file://' + dmkPF.InverteBarras(Arquivo));
  end else
    Geral.MB_Aviso('O arquivo selecionado n�o foi localizado!');
end;

procedure TFmNFeValidaXML_0001.BtCarregaClick(Sender: TObject);
begin
  AbreArquivoSelecionado();
end;

procedure TFmNFeValidaXML_0001.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeValidaXML_0001.BtValidaClick(Sender: TObject);
begin
  if PCValidacao.ActivePageIndex = 0 then
  begin
    if MyObjects.FIC(MeLoaded.Text = '', nil, 'Arquivo n�o carregado!') then Exit;
    //
    DmkWeb.WebBrowserPost(CO_URL_SEFAZ, WBSefaz, ['txtxml'], [MeLoaded.Text]);
  end else
  begin
    //ValidaXML(2.00, FTextoArq);
    ValidaMSXML_Geral();
  end;
end;

procedure TFmNFeValidaXML_0001.EdArquivoChange(Sender: TObject);
begin
  AbreArquivoSelecionado();
end;

procedure TFmNFeValidaXML_0001.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeValidaXML_0001.FormCreate(Sender: TObject);
begin
  //N�o clocar o & nos bot�es por causa do WebBrowser
  ImgTipo.SQLType := stLok;
  //
  FDir := '';
  //
  LimpaCamposXml;
end;

procedure TFmNFeValidaXML_0001.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeValidaXML_0001.LimpaCamposXml;
begin
  PCValidacao.ActivePageIndex := 0;
  PCTexto.ActivePageIndex     := 0;
  //
  WBSefaz.Navigate('about:blank');
  WBXml.Navigate('about:blank');
  //
  MeLoaded.Text := '';
  MeScaned.Text := '';
end;

function TFmNFeValidaXML_0001.RetornarConteudoEntre(const Frase, Inicio,
  Fim: string): string;
var
  i: integer;
  s: string;
begin
  result := '';
  i := pos(Inicio, Frase);
  if i = 0 then
    exit;
  s := Copy(Frase, i + length(Inicio), maxInt);
  result := Copy(s, 1, pos(Fim, s) - 1);
end;

procedure TFmNFeValidaXML_0001.SpeedButton2Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir  := ExtractFileDir(CO_DIR_RAIZ_DMK + '\NFe\');
  Arquivo := '';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo XML',
    '', [], Arquivo) then
  begin
    EdArquivo.Text := Arquivo;
    //
    LimpaCamposXml;
  end;
end;

procedure TFmNFeValidaXML_0001.SpeedButton4Click(Sender: TObject);
var
  Edit: TdmkEdit;
begin
  Edit := TdmkEdit.Create(Self);
  try
    Edit.ValueVariant := CO_DIR_RAIZ_DMK + '\';
    FDir              := MyObjects.DefineDiretorio(Self, Edit);
    //
    if FDir <> '' then
    begin
      CBSchema.Items.Clear;
      CBSchema.Items := dmkPF.GetFileList(FDir, ['.xsd']);
    end;
  finally
    Edit.Free;
  end;
end;

function TFmNFeValidaXML_0001.ValidaXML(const versao: Double; const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
  TextoAValidar: AnsiString;
begin
  Result := False;
  if pos('<Signature', XML) = 0 then
  begin
    Geral.MB_Aviso('Nota n�o pode ser validada pois n�o foi assinada!');
    Exit;
  end;
  case Trunc((Versao + 0.009) * 100) of
    200:
    begin
      if pos('<NFe xmlns', XML) > 0 then
        TextoAValidar := '<NFe xmlns' + RetornarConteudoEntre(XML, '<NFe xmlns', '</NFe>')+ '</NFe>'
      else
        TextoAValidar := '<evento xmlns' + RetornarConteudoEntre(XML, '<evento', '</evento>')+ '</evento>';
      Result := ValidaMSXML_0200(TextoAValidar, APathSchemas);
    end
    else Geral.MB_Aviso('ValidaXML: Vers�o do XML n�o implementada : ' +
       FloatToStr(Versao));
  end;
  if Result then
    MeScaned.Text := 'Arquivo v�lido!' + sLineBreak + MeScaned.Text;
end;

function TFmNFeValidaXML_0001.ValidaMSXML_0200(const XML: AnsiString; const APathSchemas: string = ''): Boolean;
var
  DOMDocument: IXMLDOMDocument2;
  ParseError: IXMLDOMParseError;
  //{$IfDef VER320}
    Schema: msXMLSchemaCache;
  //{$Else}
    // 20230320 Schema: XMLSchemaCache;
    //Schema: XMLSchemaCache;
  //{$EndIf}
  Tipo, I : Integer;
  Dir, Arquivo: String;
begin
  Result := False;
  //
  I := pos('<infNFe',XML) ;
  Tipo := 1;
  if I = 0  then
   begin
     I := pos('<infCanc',XML) ;
     if I > 0 then
        Tipo := 2
     else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
           Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
            Tipo := 5
          else
            Tipo := 4;
        end;
      end;
   end;

  DOMDocument := CoDOMDocument50.Create;
  DOMDocument.async := False;
  DOMDocument.resolveExternals := False;
  DOMDocument.validateOnParse := True;
  DOMDocument.loadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  Dir := CBSchema.Text;
  if not Geral.VerificaDir(
    Dir, '\', 'Diretorio de Schemas xml (*.xsd)', True) then Exit;
  //
  case Tipo of
    1: Arquivo := Dir + 'nfe_v2.00.xsd';
    2: Arquivo := Dir + 'cancNFe_v2.00.xsd';
    3: Arquivo := Dir + 'inutNFe_v2.00.xsd';
    4: Arquivo := Dir + 'envDPEC_v2.01.xsd';
    5: Arquivo := Dir + 'e110110_v1.00.xsd'; // ??
    else Arquivo := '### ERRO ###';
  end;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('O Schema XML n�o foi localizado:' + sLineBreak + Arquivo);
    Exit;
  end;
  Schema.add( 'http://www.portalfiscal.inf.br/nfe', Arquivo);
  //
  DOMDocument.schemas := Schema;
  ParseError := DOMDocument.validate;
  Result := (ParseError.errorCode = 0);
  MeScaned.Text := ParseError.reason;

  DOMDocument := nil;
  ParseError := nil;
  Schema := nil;
end;

function TFmNFeValidaXML_0001.ValidaMSXML_Geral(): Boolean;
var
  DOMDocument: IXMLDOMDocument2;
  ParseError: IXMLDOMParseError;
  //{$IfDef VER320}
    Schema: msXMLSchemaCache;
  //{$Else}
    //Schema: XMLSchemaCache;
  //{$EndIf}
  Tipo, I : Integer;
  Dir, Arquivo: String;
  //
  XML: String;
begin
  if MyObjects.FIC(EdArquivo.ValueVariant = '', EdArquivo, 'Selecione o Arquivo XML a ser validado!') then Exit;
  if MyObjects.FIC(MeLoaded.Text = '', EdArquivo, 'Selecione o Arquivo XML a ser validado!') then Exit;
  //
  Result := False;
  //
  XML := MeLoaded.Text;
(*
  I := pos('<infNFe',XML) ;
  Tipo := 1;
  if I = 0  then
   begin
     I := pos('<infCanc',XML) ;
     if I > 0 then
        Tipo := 2
     else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
           Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
            Tipo := 5
          else
            Tipo := 4;
        end;
      end;
   end;
*)

  DOMDocument                  := CoDOMDocument50.Create;
  DOMDocument.async            := False;
  DOMDocument.resolveExternals := False;
  DOMDocument.validateOnParse  := True;
  DOMDocument.loadXML(XML);

  Schema := CoXMLSchemaCache50.Create;

  {
  Dir := EdSchema.Text;
  if not Geral.VerificaDir(
    Dir, '\', 'Diretorio de Schemas xml (*.xsd)', True) then Exit;
  //
  case Tipo of
    1: Arquivo := Dir + 'nfe_v2.00.xsd';
    2: Arquivo := Dir + 'cancNFe_v2.00.xsd';
    3: Arquivo := Dir + 'inutNFe_v2.00.xsd';
    4: Arquivo := Dir + 'envDPEC_v2.01.xsd';
    5: Arquivo := Dir + 'e110110_v1.00.xsd'; // ??
    else Arquivo := '### ERRO ###';
  end;
  }
  Arquivo := FDir + '\' + CBSchema.Text;//EdSchema.Text;
  //////
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('O Schema XML n�o foi localizado:' + sLineBreak + Arquivo);
    Exit;
  end;
  Schema.add( 'http://www.portalfiscal.inf.br/nfe', Arquivo);
  //
  DOMDocument.schemas := Schema;
  ParseError          := DOMDocument.validate;
  Result              := (ParseError.errorCode = 0);
  MeScaned.Text       := ParseError.reason;

  DOMDocument := nil;
  ParseError  := nil;
  Schema      := nil;

  if MeScaned.Text = '' then
    Geral.MB_Aviso('Arquivo validado com sucesso!');
end;

end.
