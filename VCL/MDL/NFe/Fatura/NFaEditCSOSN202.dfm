object FmNFaEditCSOSN202: TFmNFaEditCSOSN202
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-005 :: CSOSN - ICMS ST'
  ClientHeight = 461
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 448
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 400
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 352
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 232
        Height = 32
        Caption = 'CSOSN - ICMS ST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 232
        Height = 32
        Caption = 'CSOSN - ICMS ST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 232
        Height = 32
        Caption = 'CSOSN - ICMS ST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 448
    Height = 299
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 448
      Height = 299
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 448
        Height = 299
        Align = alClient
        TabOrder = 0
        object GroupBox9: TGroupBox
          Left = 2
          Top = 45
          Width = 444
          Height = 118
          Align = alTop
          Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object RGICMS_modBCST: TdmkRadioGroup
            Left = 2
            Top = 15
            Width = 435
            Height = 101
            Align = alLeft
            Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
            Columns = 2
            Items.Strings = (
              '0 - Pre'#231'o tabelado ou m'#225'ximo sugerido'
              '1 - Lista Negativa (valor)'
              '2 - Lista Positiva (valor)'
              '3 - Lista Neutra (valor)'
              '4 - Margem valor agregado (%)'
              '5 - Pauta (valor)'
              '6 - Valor da Opera'#231#227'o')
            TabOrder = 0
            QryCampo = 'ICMS_modBCST'
            UpdCampo = 'ICMS_modBCST'
            UpdType = utYes
            OldValor = 0
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 163
          Width = 444
          Height = 205
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label36: TLabel
            Left = 4
            Top = 8
            Width = 83
            Height = 13
            Caption = 'N19: % MVA. BC:'
          end
          object Label37: TLabel
            Left = 4
            Top = 32
            Width = 101
            Height = 13
            Caption = 'N20: % Redu'#231#227'o BC:'
          end
          object Label1: TLabel
            Left = 4
            Top = 56
            Width = 87
            Height = 13
            Caption = 'N21: Valor BC ST:'
          end
          object Label38: TLabel
            Left = 4
            Top = 80
            Width = 95
            Height = 13
            Caption = 'N22: % Aliquota ST:'
          end
          object Label2: TLabel
            Left = 4
            Top = 104
            Width = 114
            Height = 13
            Caption = 'N23: Valor do ICMS ST:'
          end
          object EdICMS_pMVAST: TdmkEdit
            Left = 132
            Top = 4
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pMVAST'
            UpdCampo = 'ICMS_pMVAST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pRedBCST: TdmkEdit
            Left = 132
            Top = 28
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pRedBCST'
            UpdCampo = 'ICMS_pRedBCST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_vBCST: TdmkEdit
            Left = 132
            Top = 52
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMSST'
            UpdCampo = 'ICMS_pICMSST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pICMSST: TdmkEdit
            Left = 132
            Top = 76
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMSST'
            UpdCampo = 'ICMS_pICMSST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_vICMSST: TdmkEdit
            Left = 132
            Top = 100
            Width = 84
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMSST'
            UpdCampo = 'ICMS_pICMSST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 444
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 2
          object Label3: TLabel
            Left = 8
            Top = 8
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object EdCodigo: TdmkEdit
            Left = 36
            Top = 4
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 96
            Top = 4
            Width = 341
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 347
    Width = 448
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 444
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 391
    Width = 448
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 302
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 300
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
