unit NFaEditCSOSN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFaEditCSOSN = class(TForm)
    Panel1: TPanel;
    Label106: TLabel;
    EdCSOSN: TdmkEdit;
    Memo1: TMemo;
    EdpCredSN: TdmkEdit;
    LapCredSN: TLabel;
    RGFinaliCli: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtAliq: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCSOSNChange(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGFinaliCliClick(Sender: TObject);
    procedure BtAliqClick(Sender: TObject);
    procedure EdpCredSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure VerificaDados();
  public
    { Public declarations }
    FConfirmou: Boolean;
    FEmpresa: Integer;
    FDtEmissNF: TDate;
  end;

  var
  FmNFaEditCSOSN: TFmNFaEditCSOSN;

implementation

uses UnMyObjects, UnFinanceiro, ModuleNFe_0000, ModuleGeral, NFaEditCSOSNAlq, MyDBCheck;

{$R *.DFM}

procedure TFmNFaEditCSOSN.BtAliqClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFaEditCSOSNAlq, FmNFaEditCSOSNAlq, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSNAlq.ShowModal;
    FmNFaEditCSOSNAlq.Destroy;
    //
    DModG.ReopenParamsEmp(FEmpresa);
    //
    EdpCredSN.ValueVariant := DModG.QrPrmsEmpNFepCredSNAlq.Value;
    //
    VerificaDados();
  end;
end;

procedure TFmNFaEditCSOSN.BtOKClick(Sender: TObject);
begin
  FConfirmou := True;
  Close;
end;

procedure TFmNFaEditCSOSN.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaEditCSOSN.EdCSOSNChange(Sender: TObject);
begin
  VerificaDados();
end;

procedure TFmNFaEditCSOSN.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();
end;

procedure TFmNFaEditCSOSN.EdpCredSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  pCredSN: String;
begin
  pCredSN := '0.00';
  if not InputQuery('pCredSN',
  'Informe a al�quota aplic�vel de c�lculo do cr�dito:', pCredSN) then
    Exit;
  //
  EdpCredSN.ValueVariant := Geral.DMV(pCredSN);
end;

procedure TFmNFaEditCSOSN.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFaEditCSOSN.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FConfirmou := False;
end;

procedure TFmNFaEditCSOSN.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaEditCSOSN.Memo1Change(Sender: TObject);
begin
  BtOK.Enabled := Memo1.Text <> '';
end;

procedure TFmNFaEditCSOSN.RGFinaliCliClick(Sender: TObject);
begin
  VerificaDados();
end;

procedure TFmNFaEditCSOSN.VerificaDados();
var
  Mez: Integer;
begin
  Memo1.Text := UFinanceiro.CSOSN_Get(1, EdCSOSN.ValueVariant);
  //
  EdpCredSN.ValueVariant := 0;
  case EdCSOSN.ValueVariant of
    101, 201:
    begin
      Mez := Geral.IMV(Geral.FDT(FDtEmissNF, 20));
      if Mez <> DModG.QrParamsEmppCredSNMez.Value then
      begin
        DmNFe_0000.AvisaFaltaDaMudancaDaAliquotaSimplesNacional();
        EdCSOSN.ValueVariant := 0;
      end else
      if RGFinaliCli.ItemIndex <> 2 then
      begin
        DmNFe_0000.AvisaUsoIndevidoICMS_doCSOSN();
        EdCSOSN.ValueVariant := 0;
      end else
        EdpCredSN.ValueVariant := DModG.QrPrmsEmpNFepCredSNAlq.Value;
    end;
  end;
end;

end.
