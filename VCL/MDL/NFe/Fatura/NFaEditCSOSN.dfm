object FmNFaEditCSOSN: TFmNFaEditCSOSN
  Left = 339
  Top = 185
  Caption = 'NFa-EDITA-003 :: CSOSN e Cr'#233'dito de ICMS'
  ClientHeight = 336
  ClientWidth = 451
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 451
    Height = 180
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label106: TLabel
      Left = 8
      Top = 104
      Width = 327
      Height = 13
      Caption = 'CSOSN (C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional): [F3]'
    end
    object LapCredSN: TLabel
      Left = 8
      Top = 152
      Width = 375
      Height = 13
      Caption = 
        'pCredSN - Al'#237'quota aplic'#225'vel de c'#225'lculo do cr'#233'dito (SIMPLES NACI' +
        'ONAL) [F4]:'
      Enabled = False
    end
    object EdCSOSN: TdmkEdit
      Left = 8
      Top = 120
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CSOSN'
      UpdCampo = 'CSOSN'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCSOSNChange
      OnKeyDown = EdCSOSNKeyDown
    end
    object Memo1: TMemo
      Left = 63
      Top = 120
      Width = 380
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 2
      OnChange = Memo1Change
    end
    object EdpCredSN: TdmkEdit
      Left = 398
      Top = 148
      Width = 45
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CSOSN'
      UpdCampo = 'CSOSN'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdpCredSNKeyDown
    end
    object RGFinaliCli: TRadioGroup
      Left = 8
      Top = 4
      Width = 435
      Height = 89
      Caption = ' Finalidade da mercadoria no meu cliente: '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o sei'
        'Uso ou consumo'
        'Comercializa'#231#227'o ou industrializa'#231#227'o')
      TabOrder = 0
      OnClick = RGFinaliCliClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 403
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 355
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 330
        Height = 32
        Caption = 'CSOSN e Cr'#233'dito de ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 330
        Height = 32
        Caption = 'CSOSN e Cr'#233'dito de ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 330
        Height = 32
        Caption = 'CSOSN e Cr'#233'dito de ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 228
    Width = 451
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 447
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 272
    Width = 451
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 447
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 303
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtAliq: TBitBtn
        Tag = 222
        Left = 160
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Aliquota'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAliqClick
      end
    end
  end
end
