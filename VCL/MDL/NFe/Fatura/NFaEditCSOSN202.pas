unit NFaEditCSOSN202;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkRadioGroup;

type
  TFmNFaEditCSOSN202 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox9: TGroupBox;
    RGICMS_modBCST: TdmkRadioGroup;
    Panel5: TPanel;
    Label36: TLabel;
    EdICMS_pMVAST: TdmkEdit;
    Label37: TLabel;
    EdICMS_pRedBCST: TdmkEdit;
    Label1: TLabel;
    EdICMS_vBCST: TdmkEdit;
    Label38: TLabel;
    EdICMS_pICMSST: TdmkEdit;
    Label2: TLabel;
    EdICMS_vICMSST: TdmkEdit;
    Panel6: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label3: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FOK: Boolean;
  end;

  var
  FmNFaEditCSOSN202: TFmNFaEditCSOSN202;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmNFaEditCSOSN202.BtOKClick(Sender: TObject);
begin
  FOK := True;
  //
  Close;
end;

procedure TFmNFaEditCSOSN202.BtSaidaClick(Sender: TObject);
begin
  FOK := False;
  Close;
end;

procedure TFmNFaEditCSOSN202.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFaEditCSOSN202.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmNFaEditCSOSN202.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
