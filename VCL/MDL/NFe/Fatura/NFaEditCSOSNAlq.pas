unit NFaEditCSOSNAlq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkRadioGroup, Variants, dmkImage, UnDmkEnums;

type
  TFmNFaEditCSOSNAlq = class(TForm)
    Panel1: TPanel;
    DsParamsEmp: TDataSource;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpCNPJ_CPF: TWideStringField;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmpNO_ENT: TWideStringField;
    PnEdita: TPanel;
    GroupBox19: TGroupBox;
    Label113: TLabel;
    Label115: TLabel;
    EdpCredSNAlq: TdmkEdit;
    EdpCredSNMez: TdmkEdit;
    Panel3: TPanel;
    Label106: TLabel;
    Label107: TLabel;
    EdCSOSN: TdmkEdit;
    Memo1: TMemo;
    PnDados: TPanel;
    DBGrid1: TDBGrid;
    PnSel: TPanel;
    BtAltera: TBitBtn;
    Panel5: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    RGCRT: TdmkRadioGroup;
    QrParamsEmppCredSNMez_TXT: TWideStringField;
    QrParamsEmpCNPJ_CPF_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GbControle: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrParamsEmpAfterScroll(DataSet: TDataSet);
    procedure EdCSOSNChange(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure RGCRTClick(Sender: TObject);
    procedure QrParamsEmpCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenParamsEmp(Codigo: Integer);
  end;

  var
  FmNFaEditCSOSNAlq: TFmNFaEditCSOSNAlq;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, Module, UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFaEditCSOSNAlq.BtAlteraClick(Sender: TObject);
begin
  PnEdita.Visible := True;
  PnDados.Visible := False;
  //
  RGCRT.SetFocus;
end;

procedure TFmNFaEditCSOSNAlq.BtDesisteClick(Sender: TObject);
begin
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmNFaEditCSOSNAlq.BtOKClick(Sender: TObject);
var
  Codigo, CRT, CSOSN, pCredSNMez: Integer;
  pCredSNAlq: Double;
begin
  Codigo     := QrParamsEmpCodigo.Value;
  CRT        := RGCRT.ItemIndex;
  CSOSN      := EdCSOSN.ValueVariant;
  if EdpCredSNMez.ValueVariant = Null then
    pCredSNMez := 0
  else
    pCredSNMez := Geral.IMV(Geral.FDT(EdpCredSNMez.ValueVariant, 20));
  pCredSNAlq := EdpCredSNAlq.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False, [
  'CRT', 'CSOSN', 'pCredSNAlq', 'pCredSNMez'], ['Codigo'
  ], [CRT, CSOSN, pCredSNAlq, pCredSNMez], [Codigo
  ], True) then
  begin
    ReopenParamsEmp(Codigo);
    //
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
end;

procedure TFmNFaEditCSOSNAlq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFaEditCSOSNAlq.EdCSOSNChange(Sender: TObject);
begin
  Memo1.Text := UFinanceiro.CSOSN_Get(RGCRT.ItemIndex, EdCSOSN.ValueVariant);
end;

procedure TFmNFaEditCSOSNAlq.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();
end;

procedure TFmNFaEditCSOSNAlq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFaEditCSOSNAlq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PnDados.Align := alClient;
  PnEdita.Align := alClient;
  //
  ReopenParamsEmp(0);
end;

procedure TFmNFaEditCSOSNAlq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFaEditCSOSNAlq.QrParamsEmpAfterScroll(DataSet: TDataSet);
begin
  RGCRT.ItemIndex           := QrParamsEmpCRT.Value;
  EdCSOSN.ValueVariant      := QrParamsEmpCSOSN.Value;
  EdpCredSNAlq.ValueVariant := QrParamsEmppCredSNAlq.Value;
  EdpCredSNMez.ValueVariant := QrParamsEmppCredSNMez.Value;
end;

procedure TFmNFaEditCSOSNAlq.QrParamsEmpCalcFields(DataSet: TDataSet);
begin
  QrParamsEmppCredSNMez_TXT.Value := Geral.FDT_AAAAMM(QrParamsEmppCredSNMez.Value, 14);
  QrParamsEmpCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrParamsEmpCNPJ_CPF.Value);
end;

procedure TFmNFaEditCSOSNAlq.ReopenParamsEmp(Codigo: Integer);
begin
  QrParamsEmp.Close;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  QrParamsEmp.Locate('Codigo', Codigo, []);
end;

procedure TFmNFaEditCSOSNAlq.RGCRTClick(Sender: TObject);
begin
  Memo1.Text := UFinanceiro.CSOSN_Get(RGCRT.ItemIndex, EdCSOSN.ValueVariant);
end;

end.
