object FmNFaEditCSOSNAlq: TFmNFaEditCSOSNAlq
  Left = 339
  Top = 185
  Caption = 
    'NFa-EDITA-004 :: CSOSN - Al'#237'quota Aplic'#225'vel de C'#225'lculo de Cr'#233'dit' +
    'o'
  ClientHeight = 614
  ClientWidth = 689
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 689
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnEdita: TPanel
      Left = 0
      Top = 308
      Width = 689
      Height = 214
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object GroupBox19: TGroupBox
        Left = 0
        Top = 104
        Width = 689
        Height = 43
        Align = alTop
        Caption = ' CSOSN = 101, 201 ou 900: '
        TabOrder = 1
        object Label113: TLabel
          Left = 12
          Top = 16
          Width = 280
          Height = 13
          Caption = 'Al'#237'quota aplic'#225'vel de c'#225'lculo do cr'#233'dito (Simples Nacional):'
        end
        object Label115: TLabel
          Left = 440
          Top = 16
          Width = 107
          Height = 13
          Caption = 'AAAAMM de validade:'
        end
        object EdpCredSNAlq: TdmkEdit
          Left = 300
          Top = 12
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'pCredSNAlq'
          UpdCampo = 'pCredSNAlq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdpCredSNMez: TdmkEdit
          Left = 556
          Top = 12
          Width = 77
          Height = 21
          Alignment = taCenter
          TabOrder = 1
          FormatType = dmktf_AAAAMM
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'pCredSNMez'
          UpdCampo = 'pCredSNMez'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = Null
          ValWarn = False
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 689
        Height = 104
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label106: TLabel
          Left = 328
          Top = 16
          Width = 105
          Height = 13
          Caption = 'CSOSN* para CRT=1:'
        end
        object Label107: TLabel
          Left = 328
          Top = 40
          Width = 264
          Height = 13
          Caption = '*C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional.'
        end
        object EdCSOSN: TdmkEdit
          Left = 436
          Top = 12
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CSOSN'
          UpdCampo = 'CSOSN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCSOSNChange
          OnKeyDown = EdCSOSNKeyDown
        end
        object Memo1: TMemo
          Left = 328
          Top = 55
          Width = 341
          Height = 45
          TabStop = False
          ReadOnly = True
          TabOrder = 2
        end
        object RGCRT: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 324
          Height = 104
          Align = alLeft
          Caption = ' CRT (C'#243'digo do Regime tribut'#225'rio): '
          ItemIndex = 0
          Items.Strings = (
            '0 - Nenhum'
            '1 - Simples Nacional'
            '2 - Simples Nacional - excesso de sublimite de receita bruta'
            '3 - Regime Normal')
          TabOrder = 0
          OnClick = RGCRTClick
          QryCampo = 'CRT'
          UpdCampo = 'CRT'
          UpdType = utYes
          OldValor = 0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 147
        Width = 689
        Height = 67
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object GbControle: TGroupBox
          Left = 0
          Top = 3
          Width = 689
          Height = 64
          Align = alBottom
          TabOrder = 0
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 685
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PnSaiDesis: TPanel
              Left = 541
              Top = 0
              Width = 144
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object BtDesiste: TBitBtn
                Tag = 15
                Left = 2
                Top = 3
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
              end
            end
            object BtOK: TBitBtn
              Tag = 14
              Left = 20
              Top = 3
              Width = 120
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtOKClick
            end
          end
        end
      end
    end
    object PnDados: TPanel
      Left = 0
      Top = 0
      Width = 689
      Height = 76
      Align = alTop
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 687
        Height = 26
        Align = alClient
        DataSource = DsParamsEmp
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ_CPF_TXT'
            Title.Caption = 'CNPJ / CPF'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ENT'
            Title.Caption = 'Nome Filial'
            Width = 280
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CRT'
            Width = 27
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CSOSN'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pCredSNAlq'
            Title.Caption = 'Al'#237'q.  ACCSN'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'pCredSNMez_TXT'
            Title.Caption = 'M'#234's  ACCSN'
            Visible = True
          end>
      end
      object PnSel: TPanel
        Left = 1
        Top = 27
        Width = 687
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtAltera: TBitBtn
          Tag = 11
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtAlteraClick
        end
        object Panel5: TPanel
          Left = 576
          Top = 0
          Width = 111
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 0
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 689
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 641
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 593
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 604
        Height = 32
        Caption = 'CSOSN - Al'#237'quota Aplic'#225'vel de C'#225'lculo de Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 604
        Height = 32
        Caption = 'CSOSN - Al'#237'quota Aplic'#225'vel de C'#225'lculo de Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 604
        Height = 32
        Caption = 'CSOSN - Al'#237'quota Aplic'#225'vel de C'#225'lculo de Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 689
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 685
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsParamsEmp: TDataSource
    DataSet = QrParamsEmp
    Left = 224
    Top = 92
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrParamsEmpAfterScroll
    OnCalcFields = QrParamsEmpCalcFields
    SQL.Strings = (
      'SELECT emp.*,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM paramsemp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo')
    Left = 196
    Top = 92
    object QrParamsEmpCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
      DisplayFormat = '0;-0; '
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
      DisplayFormat = '0;-0; '
    end
    object QrParamsEmpNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrParamsEmppCredSNMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'pCredSNMez_TXT'
      Size = 7
      Calculated = True
    end
    object QrParamsEmpCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
end
