
{**************************************************************************************************************}
{                                                                                                              }
{                                               XML Data Binding                                               }
{                                                                                                              }
{         Generated on: 29/04/2013 14:45:44                                                                    }
{       Generated from: C:\Projetos\Delphi 2007\NFe\ConsultaNFDest\ConsultaNFDest_v101\consNFeDest_v1.01.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\ConsultaNFDest\ConsultaNFDest_v101\consNFeDest_v1.01.xdb   }
{                                                                                                              }
{**************************************************************************************************************}

unit consNFeDest_v101;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsNFeDest = interface;

{ IXMLTConsNFeDest }

  IXMLTConsNFeDest = interface(IXMLNode)
    ['{5D18D983-4BE3-4263-A03E-C2CF6263273F}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_CNPJ: WideString;
    function Get_IndNFe: WideString;
    function Get_IndEmi: WideString;
    function Get_UltNSU: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_IndNFe(Value: WideString);
    procedure Set_IndEmi(Value: WideString);
    procedure Set_UltNSU(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property XServ: WideString read Get_XServ write Set_XServ;
    property CNPJ: WideString read Get_CNPJ write Set_CNPJ;
    property IndNFe: WideString read Get_IndNFe write Set_IndNFe;
    property IndEmi: WideString read Get_IndEmi write Set_IndEmi;
    property UltNSU: WideString read Get_UltNSU write Set_UltNSU;
  end;

{ Forward Decls }

  TXMLTConsNFeDest = class;

{ TXMLTConsNFeDest }

  TXMLTConsNFeDest = class(TXMLNode, IXMLTConsNFeDest)
  protected
    { IXMLTConsNFeDest }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_CNPJ: WideString;
    function Get_IndNFe: WideString;
    function Get_IndEmi: WideString;
    function Get_UltNSU: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_IndNFe(Value: WideString);
    procedure Set_IndEmi(Value: WideString);
    procedure Set_UltNSU(Value: WideString);
  end;

{ Global Functions }

function GetconsNFeDest(Doc: IXMLDocument): IXMLTConsNFeDest;
function LoadconsNFeDest(const FileName: WideString): IXMLTConsNFeDest;
function NewconsNFeDest: IXMLTConsNFeDest;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetconsNFeDest(Doc: IXMLDocument): IXMLTConsNFeDest;
begin
  Result := Doc.GetDocBinding('consNFeDest', TXMLTConsNFeDest, TargetNamespace) as IXMLTConsNFeDest;
end;

function LoadconsNFeDest(const FileName: WideString): IXMLTConsNFeDest;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consNFeDest', TXMLTConsNFeDest, TargetNamespace) as IXMLTConsNFeDest;
end;

function NewconsNFeDest: IXMLTConsNFeDest;
begin
  Result := NewXMLDocument.GetDocBinding('consNFeDest', TXMLTConsNFeDest, TargetNamespace) as IXMLTConsNFeDest;
end;

{ TXMLTConsNFeDest }

function TXMLTConsNFeDest.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsNFeDest.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsNFeDest.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsNFeDest.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsNFeDest.Get_XServ: WideString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsNFeDest.Set_XServ(Value: WideString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsNFeDest.Get_CNPJ: WideString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLTConsNFeDest.Set_CNPJ(Value: WideString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLTConsNFeDest.Get_IndNFe: WideString;
begin
  Result := ChildNodes['indNFe'].Text;
end;

procedure TXMLTConsNFeDest.Set_IndNFe(Value: WideString);
begin
  ChildNodes['indNFe'].NodeValue := Value;
end;

function TXMLTConsNFeDest.Get_IndEmi: WideString;
begin
  Result := ChildNodes['indEmi'].Text;
end;

procedure TXMLTConsNFeDest.Set_IndEmi(Value: WideString);
begin
  ChildNodes['indEmi'].NodeValue := Value;
end;

function TXMLTConsNFeDest.Get_UltNSU: WideString;
begin
  Result := ChildNodes['ultNSU'].Text;
end;

procedure TXMLTConsNFeDest.Set_UltNSU(Value: WideString);
begin
  ChildNodes['ultNSU'].NodeValue := Value;
end;

end.