
{************************************************************************************************************}
{                                                                                                            }
{                                              XML Data Binding                                              }
{                                                                                                            }
{         Generated on: 05/05/2013 19:37:14                                                                  }
{       Generated from: C:\Projetos\Delphi 2007\NFe\ConsultaNFDest\DownloadNFe_v1.00\downloadNFe_v1.00.xsd   }
{   Settings stored in: C:\Projetos\Delphi 2007\NFe\ConsultaNFDest\DownloadNFe_v1.00\downloadNFe_v1.00.xdb   }
{                                                                                                            }
{************************************************************************************************************}

unit downloadNFe_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTDownloadNFe = interface;

{ IXMLTDownloadNFe }

  IXMLTDownloadNFe = interface(IXMLNode)
    ['{E1687A09-1E4A-43F1-95E2-4ABBC6EEA3D8}']
    { Property Accessors }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_CNPJ: WideString;
    function Get_ChNFe: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_ChNFe(Value: WideString);
    { Methods & Properties }
    property Versao: WideString read Get_Versao write Set_Versao;
    property TpAmb: WideString read Get_TpAmb write Set_TpAmb;
    property XServ: WideString read Get_XServ write Set_XServ;
    property CNPJ: WideString read Get_CNPJ write Set_CNPJ;
    property ChNFe: WideString read Get_ChNFe write Set_ChNFe;
  end;

{ Forward Decls }

  TXMLTDownloadNFe = class;

{ TXMLTDownloadNFe }

  TXMLTDownloadNFe = class(TXMLNode, IXMLTDownloadNFe)
  protected
    { IXMLTDownloadNFe }
    function Get_Versao: WideString;
    function Get_TpAmb: WideString;
    function Get_XServ: WideString;
    function Get_CNPJ: WideString;
    function Get_ChNFe: WideString;
    procedure Set_Versao(Value: WideString);
    procedure Set_TpAmb(Value: WideString);
    procedure Set_XServ(Value: WideString);
    procedure Set_CNPJ(Value: WideString);
    procedure Set_ChNFe(Value: WideString);
  end;

{ Global Functions }

function GetdownloadNFe(Doc: IXMLDocument): IXMLTDownloadNFe;
function LoaddownloadNFe(const FileName: WideString): IXMLTDownloadNFe;
function NewdownloadNFe: IXMLTDownloadNFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetdownloadNFe(Doc: IXMLDocument): IXMLTDownloadNFe;
begin
  Result := Doc.GetDocBinding('downloadNFe', TXMLTDownloadNFe, TargetNamespace) as IXMLTDownloadNFe;
end;

function LoaddownloadNFe(const FileName: WideString): IXMLTDownloadNFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('downloadNFe', TXMLTDownloadNFe, TargetNamespace) as IXMLTDownloadNFe;
end;

function NewdownloadNFe: IXMLTDownloadNFe;
begin
  Result := NewXMLDocument.GetDocBinding('downloadNFe', TXMLTDownloadNFe, TargetNamespace) as IXMLTDownloadNFe;
end;

{ TXMLTDownloadNFe }

function TXMLTDownloadNFe.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTDownloadNFe.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTDownloadNFe.Get_TpAmb: WideString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTDownloadNFe.Set_TpAmb(Value: WideString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTDownloadNFe.Get_XServ: WideString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTDownloadNFe.Set_XServ(Value: WideString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTDownloadNFe.Get_CNPJ: WideString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLTDownloadNFe.Set_CNPJ(Value: WideString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLTDownloadNFe.Get_ChNFe: WideString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLTDownloadNFe.Set_ChNFe(Value: WideString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

end.