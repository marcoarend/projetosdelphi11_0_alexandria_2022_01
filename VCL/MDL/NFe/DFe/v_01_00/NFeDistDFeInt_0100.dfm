object FmNFeDistDFeInt_0100: TFmNFeDistDFeInt_0100
  Left = 368
  Top = 194
  Caption = 'NFe-DISTR-001 :: Distribui'#231#227'o de DF-e 1.00'
  ClientHeight = 658
  ClientWidth = 996
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 115
    Width = 996
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 60
    ExplicitTop = 123
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 996
      Height = 90
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label9: TLabel
        Left = 67
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 47
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 55
        Height = 20
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 67
        Top = 20
        Width = 824
        Height = 20
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 63
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 63
        Width = 827
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 480
      Width = 996
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 992
        Height = 46
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 849
          Top = 0
          Width = 143
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 118
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object GroupBox5: TGroupBox
      Left = 8
      Top = 90
      Width = 883
      Height = 52
      Caption = 'Configura'#231#227'o da consulta: '
      TabOrder = 2
      object Label20: TLabel
        Left = 528
        Top = 27
        Width = 58
        Height = 13
        Caption = #218'ltimo NSU:'
      end
      object SbUltNSU: TSpeedButton
        Left = 693
        Top = 24
        Width = 23
        Height = 22
        Caption = '?'
        OnClick = SbUltNSUClick
      end
      object Label21: TLabel
        Left = 721
        Top = 27
        Width = 55
        Height = 13
        Caption = 'NSU '#250'nico:'
      end
      object RGFrmaCnslt: TdmkRadioGroup
        Left = 2
        Top = 15
        Width = 523
        Height = 35
        Align = alLeft
        Caption = ' Forma da consulta: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Pelo '#250'ltimo NSU'
          'Tudo que for poss'#237'vel'
          'NSU '#250'nico')
        TabOrder = 0
        UpdType = utYes
        OldValor = 0
      end
      object EddistDFeInt_ultNSU: TdmkEdit
        Left = 590
        Top = 24
        Width = 100
        Height = 20
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'distDFeInt_ultNSU'
        UpdCampo = 'distDFeInt_ultNSU'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EddistDFeInt_NSU: TdmkEdit
        Left = 780
        Top = 24
        Width = 98
        Height = 20
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'distDFeInt_NSU'
        UpdCampo = 'distDFeInt_NSU'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 115
    Width = 996
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 996
      Height = 142
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 996
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 67
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label5: TLabel
          Left = 500
          Top = 4
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdit2: TDBEdit
          Left = 500
          Top = 20
          Width = 55
          Height = 21
          DataField = 'Filial'
          DataSource = DsNFeDFeICab
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 555
          Top = 20
          Width = 430
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsNFeDFeICab
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 67
          Top = 20
          Width = 429
          Height = 21
          DataField = 'Nome'
          DataSource = DsNFeDFeICab
          TabOrder = 2
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNFeDFeICab
          TabOrder = 3
        end
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 43
        Width = 245
        Height = 96
        Caption = ' Informa'#231#245'es enviadas:'
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 79
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 4
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit1
          end
          object Label6: TLabel
            Left = 78
            Top = 0
            Width = 47
            Height = 13
            Caption = 'Ambiente:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 130
            Top = 0
            Width = 55
            Height = 13
            Caption = #218'limo NSU:'
            FocusControl = DBEdit5
          end
          object Label10: TLabel
            Left = 4
            Top = 39
            Width = 55
            Height = 13
            Caption = 'NSU '#250'nico:'
            FocusControl = DBEdit6
          end
          object Label11: TLabel
            Left = 110
            Top = 39
            Width = 46
            Height = 13
            Caption = 'FrmaCnslt'
            FocusControl = DBEdit7
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 16
            Width = 71
            Height = 21
            DataField = 'distDFeInt_versao'
            DataSource = DsNFeDFeICab
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 78
            Top = 16
            Width = 49
            Height = 21
            DataField = 'distDFeInt_tpAmb'
            DataSource = DsNFeDFeICab
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 130
            Top = 16
            Width = 102
            Height = 21
            DataField = 'distDFeInt_ultNSU'
            DataSource = DsNFeDFeICab
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 4
            Top = 55
            Width = 102
            Height = 21
            DataField = 'distDFeInt_NSU'
            DataSource = DsNFeDFeICab
            TabOrder = 3
          end
          object DBEdit7: TDBEdit
            Left = 110
            Top = 55
            Width = 48
            Height = 21
            DataField = 'FrmaCnslt'
            DataSource = DsNFeDFeICab
            TabOrder = 4
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 256
        Top = 43
        Width = 611
        Height = 96
        Caption = ' Informa'#231#245'es retornadas:'
        TabOrder = 2
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 607
          Height = 79
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label12: TLabel
            Left = 4
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit8
          end
          object Label13: TLabel
            Left = 78
            Top = 0
            Width = 47
            Height = 13
            Caption = 'Ambiente:'
            FocusControl = DBEdit9
          end
          object Label14: TLabel
            Left = 130
            Top = 0
            Width = 55
            Height = 13
            Caption = #218'limo NSU:'
            FocusControl = DBEdit10
          end
          object Label15: TLabel
            Left = 236
            Top = 0
            Width = 64
            Height = 13
            Caption = 'NSU m'#225'ximo:'
            FocusControl = DBEdit11
          end
          object Label16: TLabel
            Left = 342
            Top = 0
            Width = 72
            Height = 13
            Caption = 'Vers'#227'o do app:'
            FocusControl = DBEdit12
          end
          object Label17: TLabel
            Left = 4
            Top = 39
            Width = 33
            Height = 13
            Caption = 'Status:'
            FocusControl = DBEdit13
          end
          object Label18: TLabel
            Left = 39
            Top = 39
            Width = 35
            Height = 13
            Caption = 'Motivo:'
            FocusControl = DBEdit14
          end
          object Label19: TLabel
            Left = 445
            Top = 0
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
            FocusControl = DBEdit15
          end
          object DBEdit8: TDBEdit
            Left = 4
            Top = 16
            Width = 71
            Height = 21
            DataField = 'retDistDFeInt_tpAmb'
            DataSource = DsNFeDFeICab
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 78
            Top = 16
            Width = 49
            Height = 21
            DataField = 'retDistDFeInt_tpAmb'
            DataSource = DsNFeDFeICab
            TabOrder = 1
          end
          object DBEdit10: TDBEdit
            Left = 130
            Top = 16
            Width = 102
            Height = 21
            DataField = 'retDistDFeInt_ultNSU'
            DataSource = DsNFeDFeICab
            TabOrder = 2
          end
          object DBEdit11: TDBEdit
            Left = 236
            Top = 16
            Width = 102
            Height = 21
            DataField = 'retDistDFeInt_maxNSU'
            DataSource = DsNFeDFeICab
            TabOrder = 3
          end
          object DBEdit12: TDBEdit
            Left = 342
            Top = 16
            Width = 99
            Height = 21
            DataField = 'retDistDFeInt_verAplic'
            DataSource = DsNFeDFeICab
            TabOrder = 4
          end
          object DBEdit13: TDBEdit
            Left = 4
            Top = 55
            Width = 33
            Height = 21
            DataField = 'retDistDFeInt_cStat'
            DataSource = DsNFeDFeICab
            TabOrder = 5
          end
          object DBEdit14: TDBEdit
            Left = 39
            Top = 55
            Width = 517
            Height = 21
            DataField = 'retDistDFeInt_xMotivo'
            DataSource = DsNFeDFeICab
            TabOrder = 6
          end
          object DBEdit15: TDBEdit
            Left = 445
            Top = 16
            Width = 110
            Height = 21
            DataField = 'retDistDFeInt_dhResp'
            DataSource = DsNFeDFeICab
            TabOrder = 7
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 142
      Width = 996
      Height = 312
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 988
          Height = 284
          Align = alClient
          Caption = ' Grupos de NF-es encontradas: '
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 2
            Top = 15
            Width = 216
            Height = 267
            Align = alLeft
            DataSource = DsNFeDFeIIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'docZIP_NSU'
                Title.Caption = 'NSU'
                Width = 87
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_DFeSchema'
                Title.Caption = 'Schema'
                Width = 76
                Visible = True
              end>
          end
          object Panel9: TPanel
            Left = 218
            Top = 15
            Width = 768
            Height = 267
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object DBGEve: TDBGrid
              Left = 0
              Top = 162
              Width = 768
              Height = 105
              Align = alBottom
              DataSource = DsNFeDFeIEve
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Visible = False
              OnDblClick = DBGEveDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CNPJ'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'chNFe'
                  Title.Caption = 'Chave NFe'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nSeqEvento'
                  Title.Caption = 'Seq.'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tpEvento'
                  Title.Caption = 'tp Evento'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'xEvento'
                  Title.Caption = 'Texto Evento'
                  Width = 144
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dhEvento'
                  Title.Caption = 'Data / hora'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nProt'
                  Title.Caption = 'Protocolo'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID Schema'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'cOrgao'
                  Title.Caption = 'Org'#227'o'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tpAmb'
                  Title.Caption = 'Amb.'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'versao'
                  Title.Caption = 'Vers'#227'o'
                  Width = 59
                  Visible = True
                end>
            end
            object DBGNFe: TDBGrid
              Left = 0
              Top = 0
              Width = 768
              Height = 135
              Align = alTop
              DataSource = DsNFeDFeINFe
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Visible = False
              OnDblClick = DBGNFeDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CNPJ'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'xNome'
                  Title.Caption = 'Nome'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'chNFe'
                  Title.Caption = 'Chave da NF-e'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'cSitNFe'
                  Title.Caption = 'Sit'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'dhEmi'
                  Title.Caption = 'Emiss'#227'o'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'vNF'
                  Title.Caption = 'Valor NF'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nProt'
                  Title.Caption = 'Protocolo'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tpAmb'
                  Title.Caption = 'Amb.'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'tpNF'
                  Title.Caption = 'tp NF'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'versao'
                  Title.Caption = 'Vers'#227'o'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IE'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID Schema'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFa_IDCtrl'
                  Title.Caption = 'NFa IDCtrl'
                  Width = 59
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 986
          Height = 286
          Align = alClient
          TabOrder = 0
          ControlData = {
            4C000000F36500009A1D00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 480
      Width = 996
      Height = 63
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 169
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 15
        Width = 258
        Height = 46
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 429
        Top = 15
        Width = 565
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 434
          Top = 0
          Width = 131
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Tag = 526
          Left = 4
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtNFe: TBitBtn
          Tag = 456
          Left = 126
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&NFe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtNFeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 996
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 949
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 736
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 31
        Caption = 'Distribui'#231#227'o de DF-e 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 31
        Caption = 'Distribui'#231#227'o de DF-e 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 31
        Caption = 'Distribui'#231#227'o de DF-e 1.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 72
    Width = 996
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 992
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 51
    Width = 996
    Height = 21
    Align = alTop
    TabOrder = 4
  end
  object DsNFeDFeICab: TDataSource
    DataSet = QrNFeDFeICab
    Left = 44
    Top = 444
  end
  object QrNFeDFeICab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFeDFeICabBeforeOpen
    AfterOpen = QrNFeDFeICabAfterOpen
    AfterScroll = QrNFeDFeICabAfterScroll
    OnCalcFields = QrNFeDFeICabCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM nfedfeicab lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 44
    Top = 396
    object QrNFeDFeICabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDFeICabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDFeICabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrNFeDFeICabdistDFeInt_versao: TFloatField
      FieldName = 'distDFeInt_versao'
      DisplayFormat = '0.00'
    end
    object QrNFeDFeICabdistDFeInt_tpAmb: TSmallintField
      FieldName = 'distDFeInt_tpAmb'
    end
    object QrNFeDFeICabdistDFeInt_cUFAutor: TSmallintField
      FieldName = 'distDFeInt_cUFAutor'
    end
    object QrNFeDFeICabdistDFeInt_CNPJ: TWideStringField
      FieldName = 'distDFeInt_CNPJ'
      Size = 18
    end
    object QrNFeDFeICabdistDFeInt_CPF: TWideStringField
      FieldName = 'distDFeInt_CPF'
      Size = 18
    end
    object QrNFeDFeICabdistDFeInt_ultNSU: TLargeintField
      FieldName = 'distDFeInt_ultNSU'
      DisplayFormat = '000000000000000'
    end
    object QrNFeDFeICabdistDFeInt_NSU: TLargeintField
      FieldName = 'distDFeInt_NSU'
      DisplayFormat = '000000000000000'
    end
    object QrNFeDFeICabFrmaCnslt: TSmallintField
      FieldName = 'FrmaCnslt'
    end
    object QrNFeDFeICabretDistDFeInt_versao: TFloatField
      FieldName = 'retDistDFeInt_versao'
      DisplayFormat = '0.00'
    end
    object QrNFeDFeICabretDistDFeInt_tpAmb: TSmallintField
      FieldName = 'retDistDFeInt_tpAmb'
    end
    object QrNFeDFeICabretDistDFeInt_verAplic: TWideStringField
      FieldName = 'retDistDFeInt_verAplic'
      Size = 30
    end
    object QrNFeDFeICabretDistDFeInt_cStat: TIntegerField
      FieldName = 'retDistDFeInt_cStat'
    end
    object QrNFeDFeICabretDistDFeInt_xMotivo: TWideStringField
      FieldName = 'retDistDFeInt_xMotivo'
      Size = 255
    end
    object QrNFeDFeICabretDistDFeInt_dhResp: TDateTimeField
      FieldName = 'retDistDFeInt_dhResp'
    end
    object QrNFeDFeICabretDistDFeInt_ultNSU: TLargeintField
      FieldName = 'retDistDFeInt_ultNSU'
      DisplayFormat = '000000000000000'
    end
    object QrNFeDFeICabretDistDFeInt_maxNSU: TLargeintField
      FieldName = 'retDistDFeInt_maxNSU'
      DisplayFormat = '000000000000000'
    end
    object QrNFeDFeICabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDFeICabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDFeICabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDFeICabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDFeICabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDFeICabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDFeICabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeDFeICabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFeDFeICabNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtNFe
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 452
    Top = 536
    object Incluinovolote1: TMenuItem
      Caption = '&Nova consulta'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&AlteraConsulta'
      Enabled = False
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui consulta atual'
      Enabled = False
      Visible = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Executapesquisa1: TMenuItem
      Caption = '&Executa pesquisa'
      OnClick = Executapesquisa1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo retorno'
      OnClick = Lerarquivo1Click
    end
    object AtualizastatusNFes1: TMenuItem
      Caption = '&Atualiza manifesta'#231#227'o de destinat'#225'rio nas NF-e'#39's '
      OnClick = AtualizastatusNFes1Click
    end
  end
  object QrNFeDFeIIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeDFeIItsBeforeClose
    AfterScroll = QrNFeDFeIItsAfterScroll
    OnCalcFields = QrNFeDFeIItsCalcFields
    SQL.Strings = (
      'SELECT '
      
        'ELT(ndi.DFeSchema, "Resumo NF-e", "NF-e", "Resumo evento", "Even' +
        'to", "?") NO_DFeSchema, '
      'ndi.*'
      'FROM nfedfeiits ndi'
      'WHERE ndi.Codigo=2')
    Left = 132
    Top = 396
    object QrNFeDFeIItsNO_DFeSchema: TWideStringField
      FieldName = 'NO_DFeSchema'
      Size = 13
    end
    object QrNFeDFeIItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDFeIItsControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrNFeDFeIItsdocZIP_NSU: TLargeintField
      FieldName = 'docZIP_NSU'
    end
    object QrNFeDFeIItsdocZIP_schema: TWideStringField
      FieldName = 'docZIP_schema'
      Size = 255
    end
    object QrNFeDFeIItsdocZIP_base64gZip: TWideMemoField
      FieldName = 'docZIP_base64gZip'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeDFeIItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDFeIItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDFeIItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDFeIItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDFeIItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDFeIItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDFeIItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeDFeIItsDFeSchema: TIntegerField
      FieldName = 'DFeSchema'
    end
  end
  object DsNFeDFeIIts: TDataSource
    DataSet = QrNFeDFeIIts
    Left = 132
    Top = 444
  end
  object PMNFe: TPopupMenu
    OnPopup = PMNFePopup
    Left = 580
    Top = 536
    object RegistraChaveNFeparaManifestar1: TMenuItem
      Caption = 'Registra Chave NF-e para Manifestar'
      OnClick = RegistraChaveNFeparaManifestar1Click
    end
  end
  object QrDup: TMySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 84
  end
  object QrNFeDFeINFe: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFeDFeINFeAfterOpen
    SQL.Strings = (
      'SELECT ndn.*'
      'FROM nfedfeinfe ndn'
      'WHERE ndn.Controle=2'
      '')
    Left = 220
    Top = 396
    object QrNFeDFeINFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDFeINFeControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrNFeDFeINFeConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrNFeDFeINFeversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDFeINFetpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDFeINFechNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeDFeINFeCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeDFeINFeCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrNFeDFeINFeIE: TWideStringField
      FieldName = 'IE'
      Size = 18
    end
    object QrNFeDFeINFedhEmi: TDateTimeField
      FieldName = 'dhEmi'
    end
    object QrNFeDFeINFedhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
    end
    object QrNFeDFeINFetpNF: TSmallintField
      FieldName = 'tpNF'
    end
    object QrNFeDFeINFevNF: TFloatField
      FieldName = 'vNF'
    end
    object QrNFeDFeINFedigVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrNFeDFeINFedhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeDFeINFedhRecbtoTZD: TFloatField
      FieldName = 'dhRecbtoTZD'
    end
    object QrNFeDFeINFenProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeDFeINFecSitNFe: TSmallintField
      FieldName = 'cSitNFe'
    end
    object QrNFeDFeINFeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDFeINFeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDFeINFeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDFeINFeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDFeINFeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDFeINFeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDFeINFeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeDFeINFexNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrNFeDFeINFeNFa_IDCtrl: TIntegerField
      FieldName = 'NFa_IDCtrl'
    end
  end
  object QrNFeDFeIEve: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFeDFeIEveAfterOpen
    SQL.Strings = (
      'SELECT nde.*'
      'FROM nfedfeieve nde'
      'WHERE nde.Controle=2')
    Left = 308
    Top = 396
    object QrNFeDFeIEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeDFeIEveControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrNFeDFeIEveConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrNFeDFeIEveversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeDFeIEvetpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrNFeDFeIEvecOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrNFeDFeIEveCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrNFeDFeIEveCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrNFeDFeIEvechNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrNFeDFeIEvedhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrNFeDFeIEvedhEventoTZD: TFloatField
      FieldName = 'dhEventoTZD'
    end
    object QrNFeDFeIEvetpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrNFeDFeIEvenSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrNFeDFeIEvexEvento: TWideStringField
      FieldName = 'xEvento'
      Size = 60
    end
    object QrNFeDFeIEvedhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrNFeDFeIEvedhRecbtoTZD: TFloatField
      FieldName = 'dhRecbtoTZD'
    end
    object QrNFeDFeIEvenProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrNFeDFeIEveLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeDFeIEveDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeDFeIEveDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeDFeIEveUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeDFeIEveUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeDFeIEveAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeDFeIEveAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeDFeINFe: TDataSource
    DataSet = QrNFeDFeINFe
    Left = 220
    Top = 444
  end
  object DsNFeDFeIEve: TDataSource
    DataSet = QrNFeDFeIEve
    Left = 308
    Top = 444
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 488
    Top = 432
    object AbrirNFenoportalnacional1: TMenuItem
      Caption = '&Abrir NF-e no portal nacional'
      OnClick = AbrirNFenoportalnacional1Click
    end
    object MostrarchavedeacessodaNFe1: TMenuItem
      Caption = '&Mostrar chave de acesso da NF-e'
      OnClick = MostrarchavedeacessodaNFe1Click
    end
  end
end
