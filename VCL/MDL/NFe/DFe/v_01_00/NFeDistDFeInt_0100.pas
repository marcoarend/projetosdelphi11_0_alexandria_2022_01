unit NFeDistDFeInt_0100;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmNFeDistDFeInt_0100 = class(TForm)
    PainelDados: TPanel;
    DsNFeDFeICab: TDataSource;
    QrNFeDFeICab: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    QrNFeDFeIIts: TmySQLQuery;
    DsNFeDFeIIts: TDataSource;
    N1: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_XML: TWebBrowser;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    PMNFe: TPopupMenu;
    RegistraChaveNFeparaManifestar1: TMenuItem;
    BtNFe: TBitBtn;
    QrDup: TmySQLQuery;
    QrNFeDFeICabCodigo: TIntegerField;
    QrNFeDFeICabEmpresa: TIntegerField;
    QrNFeDFeICabNome: TWideStringField;
    QrNFeDFeICabdistDFeInt_versao: TFloatField;
    QrNFeDFeICabdistDFeInt_tpAmb: TSmallintField;
    QrNFeDFeICabdistDFeInt_cUFAutor: TSmallintField;
    QrNFeDFeICabdistDFeInt_CNPJ: TWideStringField;
    QrNFeDFeICabdistDFeInt_CPF: TWideStringField;
    QrNFeDFeICabdistDFeInt_ultNSU: TLargeintField;
    QrNFeDFeICabdistDFeInt_NSU: TLargeintField;
    QrNFeDFeICabFrmaCnslt: TSmallintField;
    QrNFeDFeICabretDistDFeInt_versao: TFloatField;
    QrNFeDFeICabretDistDFeInt_tpAmb: TSmallintField;
    QrNFeDFeICabretDistDFeInt_cStat: TIntegerField;
    QrNFeDFeICabretDistDFeInt_xMotivo: TWideStringField;
    QrNFeDFeICabretDistDFeInt_ultNSU: TLargeintField;
    QrNFeDFeICabretDistDFeInt_maxNSU: TLargeintField;
    QrNFeDFeICabLk: TIntegerField;
    QrNFeDFeICabDataCad: TDateField;
    QrNFeDFeICabDataAlt: TDateField;
    QrNFeDFeICabUserCad: TIntegerField;
    QrNFeDFeICabUserAlt: TIntegerField;
    QrNFeDFeICabAlterWeb: TSmallintField;
    QrNFeDFeICabAtivo: TSmallintField;
    QrNFeDFeICabFilial: TIntegerField;
    QrNFeDFeICabNO_Empresa: TWideStringField;
    QrNFeDFeICabretDistDFeInt_dhResp: TDateTimeField;
    QrNFeDFeICabretDistDFeInt_verAplic: TWideStringField;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    Label18: TLabel;
    DBEdit14: TDBEdit;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    QrNFeDFeIItsNO_DFeSchema: TWideStringField;
    QrNFeDFeIItsCodigo: TIntegerField;
    QrNFeDFeIItsControle: TLargeintField;
    QrNFeDFeIItsdocZIP_NSU: TLargeintField;
    QrNFeDFeIItsdocZIP_schema: TWideStringField;
    QrNFeDFeIItsdocZIP_base64gZip: TWideMemoField;
    QrNFeDFeIItsLk: TIntegerField;
    QrNFeDFeIItsDataCad: TDateField;
    QrNFeDFeIItsDataAlt: TDateField;
    QrNFeDFeIItsUserCad: TIntegerField;
    QrNFeDFeIItsUserAlt: TIntegerField;
    QrNFeDFeIItsAlterWeb: TSmallintField;
    QrNFeDFeIItsAtivo: TSmallintField;
    QrNFeDFeIItsDFeSchema: TIntegerField;
    QrNFeDFeINFe: TmySQLQuery;
    QrNFeDFeIEve: TmySQLQuery;
    QrNFeDFeIEveCodigo: TIntegerField;
    QrNFeDFeIEveControle: TLargeintField;
    QrNFeDFeIEveConta: TLargeintField;
    QrNFeDFeIEveversao: TFloatField;
    QrNFeDFeIEvetpAmb: TSmallintField;
    QrNFeDFeIEvecOrgao: TSmallintField;
    QrNFeDFeIEveCNPJ: TWideStringField;
    QrNFeDFeIEveCPF: TWideStringField;
    QrNFeDFeIEvechNFe: TWideStringField;
    QrNFeDFeIEvedhEvento: TDateTimeField;
    QrNFeDFeIEvedhEventoTZD: TFloatField;
    QrNFeDFeIEvetpEvento: TIntegerField;
    QrNFeDFeIEvenSeqEvento: TSmallintField;
    QrNFeDFeIEvexEvento: TWideStringField;
    QrNFeDFeIEvedhRecbto: TDateTimeField;
    QrNFeDFeIEvedhRecbtoTZD: TFloatField;
    QrNFeDFeIEvenProt: TWideStringField;
    QrNFeDFeIEveLk: TIntegerField;
    QrNFeDFeIEveDataCad: TDateField;
    QrNFeDFeIEveDataAlt: TDateField;
    QrNFeDFeIEveUserCad: TIntegerField;
    QrNFeDFeIEveUserAlt: TIntegerField;
    QrNFeDFeIEveAlterWeb: TSmallintField;
    QrNFeDFeIEveAtivo: TSmallintField;
    QrNFeDFeINFeCodigo: TIntegerField;
    QrNFeDFeINFeControle: TLargeintField;
    QrNFeDFeINFeConta: TLargeintField;
    QrNFeDFeINFeversao: TFloatField;
    QrNFeDFeINFetpAmb: TSmallintField;
    QrNFeDFeINFechNFe: TWideStringField;
    QrNFeDFeINFeCNPJ: TWideStringField;
    QrNFeDFeINFeCPF: TWideStringField;
    QrNFeDFeINFeIE: TWideStringField;
    QrNFeDFeINFedhEmi: TDateTimeField;
    QrNFeDFeINFedhEmiTZD: TFloatField;
    QrNFeDFeINFetpNF: TSmallintField;
    QrNFeDFeINFevNF: TFloatField;
    QrNFeDFeINFedigVal: TWideStringField;
    QrNFeDFeINFedhRecbto: TDateTimeField;
    QrNFeDFeINFedhRecbtoTZD: TFloatField;
    QrNFeDFeINFenProt: TWideStringField;
    QrNFeDFeINFecSitNFe: TSmallintField;
    QrNFeDFeINFeLk: TIntegerField;
    QrNFeDFeINFeDataCad: TDateField;
    QrNFeDFeINFeDataAlt: TDateField;
    QrNFeDFeINFeUserCad: TIntegerField;
    QrNFeDFeINFeUserAlt: TIntegerField;
    QrNFeDFeINFeAlterWeb: TSmallintField;
    QrNFeDFeINFeAtivo: TSmallintField;
    QrNFeDFeINFexNome: TWideStringField;
    QrNFeDFeINFeNFa_IDCtrl: TIntegerField;
    DsNFeDFeINFe: TDataSource;
    DsNFeDFeIEve: TDataSource;
    DBGrid1: TDBGrid;
    Panel9: TPanel;
    DBGEve: TDBGrid;
    DBGNFe: TDBGrid;
    GroupBox5: TGroupBox;
    RGFrmaCnslt: TdmkRadioGroup;
    EddistDFeInt_ultNSU: TdmkEdit;
    Label20: TLabel;
    SbUltNSU: TSpeedButton;
    Label21: TLabel;
    EddistDFeInt_NSU: TdmkEdit;
    PB1: TProgressBar;
    AtualizastatusNFes1: TMenuItem;
    Executapesquisa1: TMenuItem;
    N3: TMenuItem;
    PMMenu: TPopupMenu;
    AbrirNFenoportalnacional1: TMenuItem;
    MostrarchavedeacessodaNFe1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFeDFeICabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeDFeICabBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFeDFeICabCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure QrNFeDFeICabAfterScroll(DataSet: TDataSet);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFeDFeIItsCalcFields(DataSet: TDataSet);
    procedure QrNFeDesRItsCalcFields(DataSet: TDataSet);
    procedure DBGNFeDesRItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure PMNFePopup(Sender: TObject);
    procedure RegistraChaveNFeparaManifestar1Click(Sender: TObject);
    procedure BtNFeClick(Sender: TObject);
    procedure QrNFeDFeINFeAfterOpen(DataSet: TDataSet);
    procedure QrNFeDFeIEveAfterOpen(DataSet: TDataSet);
    procedure QrNFeDFeIItsBeforeClose(DataSet: TDataSet);
    procedure QrNFeDFeIItsAfterScroll(DataSet: TDataSet);
    procedure SbUltNSUClick(Sender: TObject);
    procedure AtualizastatusNFes1Click(Sender: TObject);
    procedure Executapesquisa1Click(Sender: TObject);
    procedure DBGNFeDblClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure MostrarchavedeacessodaNFe1Click(Sender: TObject);
    procedure AbrirNFenoportalnacional1Click(Sender: TObject);
    procedure DBGEveDblClick(Sender: TObject);
  private
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure AtualizaIDCtrlDeNFe(NFa_IDCtrl, Conta: Integer;
              ReopenQry: Boolean);
    procedure AtualizaManifestacaoDestinatarioNFes(Codigo: Integer;
              MostraMsg: Boolean = False);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFeDFeIIts(Controle: Integer);
    procedure ReopenNFeDFeIXXX();
    //
    procedure ExecutaPesquisa(Empresa, Lote: Integer; ultNSU, NSU: Int64;
              FrmaCnslt: Integer);
  end;

var
  FmNFeDistDFeInt_0100: TFmNFeDistDFeInt_0100;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleNFe_0000,
NFeSteps_0400,
DmkDAC_PF, NFeXMLGerencia, NFe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFeDistDFeInt_0100.Lerarquivo1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
  begin
    FmNFeSteps_0400.PnLoteEnv.Visible := True;
    FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value;
    FmNFeSteps_0400.Show;
    //
    FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultaDistribuicaoDFeInteresse); // 11 - Consulta Distribui��o de DFe
    FmNFeSteps_0400.CkSoLer.Checked  := True;

    //C:\Dermatek\NFE\SOFTCOURO\RetDistDFeInt\000000028_NSU_000000000000000-ret-dfe-dis-int.xml

    FmNFeSteps_0400.PreparaConsultaDistribuicaoDFeInteresse(QrNFeDFeICabEmpresa.Value,
    //  QrNFeDFeICabCodigo.Value, QrNFeDesRCabultNSU.Value);
      QrNFeDFeICabCodigo.Value, QrNFeDFeICabdistDFeInt_ultNSU.Value,
      QrNFeDFeICabdistDFeInt_NSU.Value, QrNFeDFeICabFrmaCnslt.Value);
    FmNFeSteps_0400.FFormChamou      := 'FmNFeDistDFeInt_0100';
  end;
end;

procedure TFmNFeDistDFeInt_0100.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFeDistDFeInt_0100.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFeDFeICabCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFeDistDFeInt_0100.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFeDistDFeInt_0100.DefParams;
begin
  VAR_GOTOTABELA := 'nfedfeicab';
  VAR_GOTOMYSQLTABLE := QrNFeDFeICab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfedfeicab lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmNFeDistDFeInt_0100.ExecutaPesquisa(Empresa, Lote: Integer;
ultNSU, NSU: Int64; FrmaCnslt: Integer);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
    //
    (* Mudado em: 09/06/2017 �s 09:53
    if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
    begin
      FmNFeSteps_0400.PnLoteEnv.Visible := True;
      FmNFeSteps_0400.PnDesConC.Visible := True;
      FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpNFeVerDistDFeInt.Value;
      FmNFeSteps_0400.Show;
      //
      FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultaDistribuicaoDFeInteresse); // 11 - Consulta Distribui��o de DFe
      FmNFeSteps_0400.PreparaConsultaDistribuicaoDFeInteresse(Empresa, Lote, ultNSU, NSU, FrmaCnslt);
      FmNFeSteps_0400.FFormChamou      := 'FmNFeDistDFeInt_0100';
    end;
    *)
    if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
    begin
      FmNFeSteps_0400.PnLoteEnv.Visible := True;
      FmNFeSteps_0400.PnDesConC.Visible := True;
      FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value;
      FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultaDistribuicaoDFeInteresse); // 11 - Consulta Distribui��o de DFe
      FmNFeSteps_0400.PreparaConsultaDistribuicaoDFeInteresse(Empresa, Lote, ultNSU, NSU, FrmaCnslt);
      FmNFeSteps_0400.FFormChamou      := 'FmNFeDistDFeInt_0100';
      FmNFeSteps_0400.ShowModal;
      FmNFeSteps_0400.Destroy;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmNFeDistDFeInt_0100.Executapesquisa1Click(Sender: TObject);
var
  Codigo, Empresa, FrmaCnslt: Integer;
  distDFeInt_ultNSU, distDFeInt_NSU: Int64;
begin
  Codigo            := QrNFeDFeICabCodigo.Value;
  Empresa           := QrNFeDFeICabEmpresa.Value;
  distDFeInt_ultNSU := QrNFeDFeICabdistDFeInt_ultNSU.Value;
  distDFeInt_NSU    := QrNFeDFeICabdistDFeInt_NSU.Value;
  FrmaCnslt         := QrNFeDFeICabFrmaCnslt.Value;
  //
  if (Codigo <> 0) and (Empresa <> 0) then
  begin
    ExecutaPesquisa(Empresa, Codigo, distDFeInt_ultNSU, distDFeInt_NSU, FrmaCnslt);
    LocCod(Codigo, Codigo);
    AtualizaManifestacaoDestinatarioNFes(Codigo);
  end;
end;

procedure TFmNFeDistDFeInt_0100.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFeDistDFeInt_0100.MostrarchavedeacessodaNFe1Click(
  Sender: TObject);
var
  Chave: String;
begin
  if DBGNFe.Visible = True then
    Chave := QrNFeDFeINFechNFe.Value
  else if DBGEve.Visible = True then
    Chave := QrNFeDFeIEvechNFe.Value
  else
    Chave := '';
  //
  UnNFe_PF.MostraNFeChave(Chave);
end;

procedure TFmNFeDistDFeInt_0100.PMLotePopup(Sender: TObject);
begin
  //Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  //Executapesquisa1.Enabled    := HabilitaA and HabilitaB;
  //
  MyObjects.HabilitaMenuItemItsIns(LerArquivo1, QrNFeDFeICab);
  MyObjects.HabilitaMenuItemItsIns(AtualizastatusNFes1, QrNFeDFeICab);
  //
  MyObjects.HabilitaMenuItemItsIns(Executapesquisa1, QrNFeDFeICab);
  Executapesquisa1.Enabled := Executapesquisa1.Enabled and
                              (QrNFeDFeIIts.State <> dsInactive) and
                              (QrNFeDFeIIts.RecordCount = 0);
end;

procedure TFmNFeDistDFeInt_0100.PMMenuPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrNFeDFeINFe.State <> dsInactive) and (QrNFeDFeINFe.RecordCount > 0);
  Enab2 := (QrNFeDFeIEve.State <> dsInactive) and (QrNFeDFeIEve.RecordCount > 0);
  //
  AbrirNFenoportalnacional1.Enabled  := Enab or Enab2;
  MostrarchavedeacessodaNFe1.Enabled := Enab or Enab2;
end;

procedure TFmNFeDistDFeInt_0100.PMNFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
(*&
  Habilita := (QrNFeDFeIIts.RecordCount > 0)
  and (QrNFeDFeIItsNFa_IDCtrl.Value = 0));
  RegistraChaveNFeparaManifestar1.Enabled := Habilita;
*)
end;

procedure TFmNFeDistDFeInt_0100.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFeDistDFeInt_0100.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFeDistDFeInt_0100.RegistraChaveNFeparaManifestar1Click(
  Sender: TObject);
var
  infCanc_dhRecbto, infProt_dhRecbto, ide_dEmi: TDateTime;
  //
  Id, emit_CNPJ, emit_CPF, emit_xNome, emit_IE, infProt_Id, infProt_digVal,
  infCanc_Id, emit_UF: String;
  //
  IDCad, NFa_IDCtrl, CodInfoEmit, CodInfoDest, cSitNFe, cSitCan, cSitConf,
  FatID, FatNum, Empresa, IDCtrl, ide_tpNF, infProt_cStat, ide_tpAmb, Status,
  infProt_tpAmb, infCanc_tpAmb, infCanc_cStat: Integer;
  //
  ICMSTot_vNF: Double;
  //
  VersaoNFe: Double;
  UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
begin
{
  NFa_IDCtrl := DmNFE_0000.ObtemIDCtrlDeChaveNFe(QrNFeDesRItsNFe_chNFe.Value);
  if NFa_IDCtrl <> 0 then
  begin
    Geral.MB_Aviso('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
    ' j� est� registrada na base de dados com o ID ' + Geral.FF0(NFa_IDCtrl));
    if QrNFeDesRItsNFa_IDCtrl.Value <> NFa_IDCtrl then
    begin
      IDCad := QrNFeDesRItsIDCad.Value;
      AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad, True);
    end;
    Exit;
  end
  else
  begin
    ide_tpAmb        := QrNFeDFeICabdistDFeInt_tpAmb.Value;
    //
    Empresa          := QrNFeDesRItsEmpresa        .Value;
    Id               := QrNFeDesRItsNFe_chNFe      .Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDup, Dmod.MyDB, [
    'SELECT IDCtrl',
    'FROM nfecaba ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Id="' + Id + '"',
    '']);
    if QrDup.RecordCount > 0 then
    begin
      Geral.MB_Aviso('A chave ' + Id +
      ' j� est� registrada na base de dados com o ID ' +
      Geral.FF0(QrDup.FieldByName('IDCtrl').AsInteger));
      //
      Exit;
    end else begin
      emit_CNPJ        := QrNFeDesRItsNFe_CNPJ       .Value;
      emit_CPF         := QrNFeDesRItsNFe_CPF        .Value;
      emit_xNome       := QrNFeDesRItsNFe_xNome      .Value;
      emit_IE          := QrNFeDesRItsNFe_IE         .Value;
      ide_dEmi         := QrNFeDesRItsNFe_dEmi       .Value;
      ide_tpNF         := QrNFeDesRItsNFe_tpNF       .Value;
      ICMSTot_vNF      := QrNFeDesRItsNFe_vNF        .Value;
      infProt_digVal   := QrNFeDesRItsNFe_digVal     .Value;
      infProt_dhRecbto := QrNFeDesRItsNFe_dhRecbto   .Value;
      cSitNFe          := QrNFeDesRItsNFe_cSitNFe    .Value;
      cSitConf         := QrNFeDesRItsNFe_cSitConf   .Value;
      infProt_cStat    := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitNFe);


      infProt_Id       := 'ID' + Id;

      infCanc_Id       := QrNFeDesRItsCanc_chNFe     .Value;
      infCanc_dhRecbto := QrNFeDesRItsCanc_dhRecbto  .Value;
      cSitCan          := QrNFeDesRItsCanc_cSitNFe   .Value;
      infCanc_cStat    := NFeXMLGeren.Obtem_cStat_De_cSitNFe(cSitCan);

      infProt_tpAmb    := ide_tpAmb;
      if infCanc_cStat = 101 then
      begin
        infCanc_tpAmb  := ide_tpAmb;
        Status         := infCanc_cStat;
      end else
      begin
        infCanc_tpAmb  := 0;
        Status         := infProt_cStat;
      end;
      //
(*
   Fazer?
      := QrNFeDesRItsCCe_NSU        .Value;
      := QrNFeDesRItsCCe_chNFe      .Value;
      := QrNFeDesRItsCCe_dhEvento   .Value;
      := QrNFeDesRItsCCe_tpEvento   .Value;
      := QrNFeDesRItsCCe_nSeqEvento .Value;
      := QrNFeDesRItsCCe_descEvento .Value;
      := QrNFeDesRItsCCe_xCorrecao  .Value;
      := QrNFeDesRItsCCe_tpNF       .Value;
      := QrNFeDesRItsCCe_dhRecbto   .Value;
*)

      DModG.ObtemEntidadeDeCNPJCFP(emit_CNPJ, CodInfoEmit);
      CodInfoDest:= Empresa;

      // N�o presisa saber! (at� quando?)
      VersaoNFe := 0;
      NFeXMLGeren.DesmontaChaveDeAcesso(Id, VersaoNFe,
        UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
      emit_UF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(UF_IBGE));
      //
      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      FatID  := VAR_FATID_0052;
      // Ver como implementar no StqInCad
      FatNum := QrNFeDesRItsIdCad.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
      'FatID', 'FatNum', 'Empresa',
      'Id', 'emit_CNPJ', 'emit_CPF',
      'emit_xNome', 'emit_IE', 'ide_dEmi',
      'ide_tpNF', 'ICMSTot_vNF', 'infProt_digVal',
      'infProt_dhRecbto', 'infProt_cStat', 'infProt_Id',
      'infCanc_Id', 'infCanc_dhRecbto', 'infCanc_cStat',
      'infProt_tpAmb', 'infCanc_tpAmb', 'Status',
      'ide_tpAmb', 'CodInfoEmit', 'CodInfoDest',
      'cSitNFe', 'cSitConf', 'emit_UF'
      ], [
      'IDCtrl'
      ], [
      FatID, FatNum, Empresa,
      Id, emit_CNPJ, emit_CPF,
      emit_xNome, emit_IE, ide_dEmi,
      ide_tpNF, ICMSTot_vNF, infProt_digVal,
      infProt_dhRecbto, infProt_cStat, infProt_Id,
      infCanc_Id, infCanc_dhRecbto, infCanc_cStat,
      infProt_tpAmb, infCanc_tpAmb, Status,
      ide_tpAmb, CodInfoEmit, CodInfoDest,
      cSitNFe, cSitConf, emit_UF
      ], [
      IDCtrl], True) then
      begin
        IDCad := QrNFeDesRItsIDCad.Value;
        NFa_IDCtrl := IDCtrl;
        //
        AtualizaIDCtrlDeNFe(NFa_IDCtrl, IDCad, True);
        //
        if QrNFeDesRItsNFa_IDCtrl.Value <> 0 then
          Geral.MB_Info('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
          ' foi registrada com sucesso!')
        else
          Geral.MB_Erro('A chave ' + QrNFeDesRItsNFe_chNFe.Value +
          ' pode n�o ter sido registrada!');
      end;
    end;
  end;
}
end;

procedure TFmNFeDistDFeInt_0100.ReopenNFeDFeIIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeDFeIIts, Dmod.MyDB, [
  'SELECT ',
  'ELT(ndi.DFeSchema, "Resumo NF-e", "NF-e", "Resumo evento", "Evento", "?") NO_DFeSchema, ',
  'ndi.* ',
  'FROM nfedfeiits ndi ',
  'WHERE ndi.Codigo=' + Geral.FF0(QrNFeDFeICabCodigo.Value),
  '']);
  if Controle <> 0 then
    QrNFeDFeIIts.Locate('Controle', Controle, []);
end;

procedure TFmNFeDistDFeInt_0100.ReopenNFeDFeIXXX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeDFeINFe, Dmod.MyDB, [
  'SELECT ndn.* ',
  'FROM nfedfeinfe ndn ',
  'WHERE ndn.Controle=' + Geral.FF0(QrNFeDFeIItsControle.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeDFeIEve, Dmod.MyDB, [
  'SELECT nde.* ',
  'FROM nfedfeieve nde ',
  'WHERE nde.Controle=' + Geral.FF0(QrNFeDFeIItsControle.Value),
  '']);
end;

procedure TFmNFeDistDFeInt_0100.DBGEveDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(DBGEve.Columns[THackDBGrid(DBGEve).Col -1].FieldName);
  //
  if Campo = Uppercase('CHNFE') then
  begin
    if (QrNFeDFeIEve.State <> dsInactive) and (QrNFeDFeIEve.RecordCount > 0) then
      UnNFe_PF.MostraNFeChave(QrNFeDFeIEvechNFe.Value);
  end;
end;

procedure TFmNFeDistDFeInt_0100.DBGNFeDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(DBGNFe.Columns[THackDBGrid(DBGNFe).Col -1].FieldName);
  //
  if Campo = Uppercase('CHNFE') then
  begin
    if (QrNFeDFeINFe.State <> dsInactive) and (QrNFeDFeINFe.RecordCount > 0) then
      UnNFe_PF.MostraNFeChave(QrNFeDFeINFechNFe.Value);
  end;
end;

procedure TFmNFeDistDFeInt_0100.DBGNFeDesRItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
(*&
  DmNFe_0000.ColoreStatusNFeEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State, QrNFeDFeIItscSitNFe.Value);
  //
  DmNFe_0000.ColoreSitConfNFeEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State, QrNFeDFeIItscSitConf.Value);
  //
  MyObjects.ColoreSimNaoEmDBGrid(DBGNFeDesRIts,
    Rect, DataCol, Column, State,
    'NO_REGISTROU',  QrNFeDFeIItsNO_REGISTROU.Value);
&*)
end;

procedure TFmNFeDistDFeInt_0100.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFeDistDFeInt_0100.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFeDistDFeInt_0100.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFeDistDFeInt_0100.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFeDistDFeInt_0100.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFeDistDFeInt_0100.AbrirNFenoportalnacional1Click(Sender: TObject);
var
  tpAmb: Integer;
  Chave: String;
begin
  if DBGNFe.Visible = True then
  begin
    tpAmb := QrNFeDFeINfetpAmb.Value;
    Chave := QrNFeDFeINFechNFe.Value;
  end
  else if DBGEve.Visible = True then
  begin
    tpAmb := QrNFeDFeIEvetpAmb.Value;
    Chave := QrNFeDFeIEvechNFe.Value;
  end else
  begin
    tpAmb := 0;
    Chave := '';
  end;
  //
  if Chave <> '' then
    UnNFe_PF.MostraNFeNoPortalNacional(tpAmb, Chave);
end;

procedure TFmNFeDistDFeInt_0100.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFeDFeICab, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'nfedfeicab');
end;

procedure TFmNFeDistDFeInt_0100.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFeDFeICabCodigo.Value;
  Close;
end;

procedure TFmNFeDistDFeInt_0100.AtualizaIDCtrlDeNFe(NFa_IDCtrl, Conta: Integer;
ReopenQry: Boolean);
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfedfeinfe', False, [
  'NFa_IDCtrl'], ['Conta'], [
  NFa_IDCtrl], [Conta], True) then
    if ReopenQry then
      ReopenNFeDFeIIts(QrNFeDFeIItsControle.Value);
end;

procedure TFmNFeDistDFeInt_0100.AtualizaManifestacaoDestinatarioNFes(
  Codigo: Integer; MostraMsg: Boolean = False);
var
  Qry: TmySQLQuery;
  SQLCompl, chNFe, xEvento, CNPJ, CPF, dhRecbto, nProt: String;
  tpAmb, cOrgao, tpEvento, nSeqEvento, cSitConf, ide_serie, ide_nNF: Integer;
  dhRecbtoTZD: Double;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if Codigo <> 0 then
      SQLCompl := 'AND eve.Codigo=' + Geral.FF0(Codigo)
    else
      SQLCompl := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT eve.* ',
      'FROM nfedfeieve eve ',
      'LEFT JOIN nfecaba cab ON cab.Id = eve.chNFe ',
      'WHERE eve.tpEvento IN (' +
        Geral.FF0(NFe_CodEventoMDeConfirmacao) + ', ' +
        Geral.FF0(NFe_CodEventoMDeCiencia) + ', ' +
        Geral.FF0(NFe_CodEventoMDeDesconhece) + ', ' +
        Geral.FF0(NFe_CodEventoMDeNaoRealizou) + ') ',
      'AND ( ',
      'cab.cSitConf = -1 ',
      'OR ',
      'eve.dhRecbto > cab.eveMDe_dhRegEvento ',
      ') ',
      SQLCompl,
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      PB1.Position := 0;
      while not Qry.EOF do
      begin
        chNFe       := Qry.FieldByName('chNFe').AsString;
        tpAmb       := Qry.FieldByName('tpAmb').AsInteger;
        cOrgao      := Qry.FieldByName('cOrgao').AsInteger;
        tpEvento    := Qry.FieldByName('tpEvento').AsInteger;
        xEvento     := Qry.FieldByName('xEvento').AsString;
        nSeqEvento  := Qry.FieldByName('nSeqEvento').AsInteger;
        CNPJ        := Qry.FieldByName('CNPJ').AsString;
        CPF         := Qry.FieldByName('CPF').AsString;
        dhRecbto    := Geral.FDT(Qry.FieldByName('dhRecbto').AsDateTime, 9);
        dhRecbtoTZD := Qry.FieldByName('dhRecbtoTZD').AsFloat;
        nProt       := Qry.FieldByName('nProt').AsString;
        cSitConf    := NFeXMLGeren.Obtem_DeManifestacao_de_tpEvento_cSitConf(tpEvento);
        //
        UnNFe_PF.ObtemSerieNumeroByID(chNFe, ide_serie, ide_nNF);
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False,
          ['eveMDe_tpAmb', 'eveMDe_cOrgao', 'eveMDe_chNFe',
          'eveMDe_tpEvento', 'eveMDe_xEvento', 'eveMDe_nSeqEvento',
          'eveMDe_CNPJDest', 'eveMDe_CPFDest', 'eveMDe_dhRegEvento',
          'eveMDe_TZD_UTC', 'eveMDe_nProt', 'cSitConf',
          'ide_serie', 'ide_nNF'],
          ['Id'],
          [tpAmb, cOrgao, chNFe,
          tpEvento, xEvento, nSeqEvento,
          CNPJ, CPF, dhRecbto,
          dhRecbtoTZD, nProt, cSitConf,
          ide_serie, ide_nNF],
          [chNFe], True) then
        begin
          Geral.MB_Erro('Falha ao atualizar manifesta��o de destinat�rio!');
          Exit;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
    end;
  finally
    PB1.Position  := 0;
    Screen.Cursor := CrDefault;
    //
    if MostraMsg = True then
      Geral.MB_Aviso('Atualiza��o de manifesta��o de destinat�rio finalizada!');
  end;
end;

procedure TFmNFeDistDFeInt_0100.AtualizastatusNFes1Click(Sender: TObject);
begin
  AtualizaManifestacaoDestinatarioNFes(QrNFeDFeICabCodigo.Value, True);
end;

procedure TFmNFeDistDFeInt_0100.BtConfirmaClick(Sender: TObject);
{
const
  _Hora = 1/24;
var
  Nome, (*xServ,*) CNPJ(*, XML*): String;
  Codigo, Empresa, tpAmb, indNFe, indEmi: Integer;
  versao, ultNSU: Double;
  Qry: TmySQLQuery;
  Antes, Agora, dhDif: TDateTime;
begin
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa') then
    Exit;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  DmNFe_0000.ReopenOpcoesNFe(Empresa, True);
  Nome           := EdNome.Text;
  versao         := verConsNFeDest_Int;
  tpAmb          := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
  //xServ          := CO_CONSULTAR_NFE_DEST;
  CNPJ           := Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value);
  indNFe         := RGindEmi.ItemIndex;
  indEmi         := RGindNFe.ItemIndex;
  ultNSU         := 0; //???
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(dhResp) dhResp ',
    'FROM nfedesrcab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND tpAmb=' + Geral.FF0(tpAmb),
    ' ']);
    //
    Antes := Qry.FieldByName('dhResp').AsDateTime;
    Agora := DModG.ObtemAgora();
    dhDif := Agora - Antes;
    if dhDif < _Hora then
    begin
      Geral.MB_Aviso('O tempo m�nimo de espera entre consultas � de 1 hora.' +
      'Faltam ' + Geral.FDT(_Hora - dhDif, 110));
      Exit;
    end;
  finally
    Qry.Free;
  end;
  if MyObjects.FIC(not(indNFe in [0,1,2]), RGindNFe, 'Informe o "Indicador de NFe consultada"!') then
    Exit;
  if MyObjects.FIC(not(indEmi in [0,1]), RGindEmi, 'Informe o "Indicador do Emissor da NF-e"!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('nfedfeicab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfedfeicab', False, [
  'Empresa', 'Nome', 'versao',
  'tpAmb', (*'xServ',*) 'CNPJ',
  'indNFe', 'indEmi', 'ultNSU'(*,
  'XML'*)], [
  'Codigo'], [
  Empresa, Nome, versao,
  tpAmb, (*xServ,*) CNPJ,
  indNFe, indEmi, ultNSU(*,
  XML*)], [
  Codigo], True) then
  begin
    MostraEdicao(0, stLok, 0);
    ExecutaPesquisa(Empresa, Codigo);
    LocCod(Codigo, Codigo);
  end;
}
var
  // retDistDFeInt_xMotivo, retDistDFeInt_dhResp
  Nome, distDFeInt_CNPJ, distDFeInt_CPF: String;
  //, retDistDFeInt_tpAmb, retDistDFeInt_cStat
  Codigo, Empresa, distDFeInt_tpAmb, distDFeInt_cUFAutor, FrmaCnslt: Integer;
  //retDistDFeInt_versao, retDistDFeInt_verAplic, retDistDFeInt_dhRespTZD, retDistDFeInt_ultNSU, retDistDFeInt_maxNSU
  distDFeInt_versao: Double;
  distDFeInt_ultNSU, distDFeInt_NSU: Int64;
  //
  CNPJ, CPF, IE: String;
  NSU_Unico: Boolean;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DmodG.ReopenParamsEmp(Empresa);
  DmodG.ReopenEnti(Empresa);

  DmodG.ObtemCNPJ_CPF_IE_DeEntidade(Empresa, CNPJ, CPF, IE);
  if CNPJ <> '' then
    CPF := '';
  Codigo              := EdCodigo.ValueVariant;
  Nome                := EdNome.Text;
  distDFeInt_versao   := DModG.QrPrmsEmpNFeNFeVerDistDFeInt.Value;
  distDFeInt_tpAmb    := DModG.QrPrmsEmpNFeide_tpAmb.Value;
  distDFeInt_cUFAutor := Geral.CodigoUF_to_cUF_IBGE(Trunc(DmodG.QrEntiUF_Cod.Value));
  if DmodG.QrEntiTipo.Value = 0 then
  begin
    distDFeInt_CNPJ     := Geral.SoNumero_TT(DmodG.QrEntiCNPJ.Value);
    distDFeInt_CPF      := '';
  end else
  begin
    distDFeInt_CNPJ     := '';
    distDFeInt_CPF      := Geral.SoNumero_TT(DmodG.QrEntiCPF.Value);
  end;
  distDFeInt_ultNSU   := EddistDFeInt_ultNSU.ValueVariant;
  distDFeInt_NSU      := EddistDFeInt_NSU.ValueVariant;
  FrmaCnslt           := RGFrmaCnslt.ItemIndex; // ultNSU
  if MyObjects.FIC(FrmaCnslt < 1, RGFrmaCnslt, 'Informe a forma de consulta!') then
    Exit;
  NSU_Unico := (FrmaCnslt = 3) and (distDFeInt_NSU = 0);
  if MyObjects.FIC(NSU_Unico, EddistDFeInt_NSU, 'Informe o NSU �nico!') then
    Exit;
  NSU_Unico := (FrmaCnslt <> 3) and (distDFeInt_NSU <> 0);
  if MyObjects.FIC(NSU_Unico, EddistDFeInt_NSU,
  'NSU �nico deve ser zero para a forma de consulta selecionada!!') then
    Exit;
(*
  retDistDFeInt_versao:= ;
  retDistDFeInt_tpAmb:= ;
  retDistDFeInt_verAplic:= ;
  retDistDFeInt_cStat:= ;
  retDistDFeInt_xMotivo:= ;
  retDistDFeInt_dhResp:= ;
  retDistDFeInt_dhRespTZD:= ;
  retDistDFeInt_ultNSU:= ;
  retDistDFeInt_maxNSU:= ;
*)
  //
  Codigo := UMyMod.BPGS1I32('nfedfeicab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfedfeicab', False, [
  'Empresa', 'Nome', 'distDFeInt_versao',
  'distDFeInt_tpAmb', 'distDFeInt_cUFAutor', 'distDFeInt_CNPJ',
  'distDFeInt_CPF', 'distDFeInt_ultNSU', 'distDFeInt_NSU',
  'FrmaCnslt'(*, 'retDistDFeInt_versao', 'retDistDFeInt_tpAmb',
  'retDistDFeInt_verAplic', 'retDistDFeInt_cStat', 'retDistDFeInt_xMotivo',
  'retDistDFeInt_dhResp', 'retDistDFeInt_dhRespTZD', 'retDistDFeInt_ultNSU',
  'retDistDFeInt_maxNSU'*)], [
  'Codigo'], [
  Empresa, Nome, distDFeInt_versao,
  distDFeInt_tpAmb, distDFeInt_cUFAutor, distDFeInt_CNPJ,
  distDFeInt_CPF, distDFeInt_ultNSU, distDFeInt_NSU,
  FrmaCnslt(*, retDistDFeInt_versao, retDistDFeInt_tpAmb,
  retDistDFeInt_verAplic, retDistDFeInt_cStat, retDistDFeInt_xMotivo,
  retDistDFeInt_dhResp, retDistDFeInt_dhRespTZD, retDistDFeInt_ultNSU,
  retDistDFeInt_maxNSU*)], [
  Codigo], True) then
  begin
    MostraEdicao(0, stLok, 0);
    ExecutaPesquisa(Empresa, Codigo, distDFeInt_ultNSU, distDFeInt_NSU, FrmaCnslt);
    LocCod(Codigo, Codigo);
    AtualizaManifestacaoDestinatarioNFes(Codigo);
  end;
end;

procedure TFmNFeDistDFeInt_0100.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfedfeicab', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFeDistDFeInt_0100.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFeDistDFeInt_0100.BtNFeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNFe, BtNFe);
end;

procedure TFmNFeDistDFeInt_0100.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Alteraloteatual1.Visible := False;
  Excluiloteatual1.Visible := False;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PainelEdit.Align     := alClient;
  PageControl1.Align   := alClient;
  //
  DBGNFe.Align   := alClient;
  DBGEve.Align   := alClient;
  //
  PageControl1.ActivePageIndex := 0;
  //
  DBGNFe.PopupMenu := PMMenu;
  DBGEve.PopupMenu := PMMenu;
  //
  CriaOForm;
end;

procedure TFmNFeDistDFeInt_0100.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFeDFeICabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFeDistDFeInt_0100.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFeDistDFeInt_0100.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFeDistDFeInt_0100.SbNovoClick(Sender: TObject);
begin
{:::
  LaRegistro.Caption := GOTOy.CodUsu(QrNFeDFeICabCodUsu.Value, LaRegistro.Caption);
}
end;

procedure TFmNFeDistDFeInt_0100.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeICabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeICabAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeDFeIIts(0);
end;

procedure TFmNFeDistDFeInt_0100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeDistDFeInt_0100.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFeDFeICabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfedfeicab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFeDistDFeInt_0100.SbUltNSUClick(Sender: TObject);
begin
  EddistDFeInt_ultNSU.ValueVariant := UnNFe_PF.ObtemUltNSU_NFeDistDFeInt(0);
end;

procedure TFmNFeDistDFeInt_0100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeDistDFeInt_0100.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNFeDFeICab, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'nfedfeicab');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeICabBeforeOpen(DataSet: TDataSet);
begin
  QrNFeDFeICabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeICabCalcFields(DataSet: TDataSet);
begin
{
  QrNFeDFeICabDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeDFeICabdhRecbto.Value, 0);
  case QrNFeDFeICabtpAmb.Value of
    1: QrNFeDFeICabNO_Ambiente.Value := 'Produ��o';
    2: QrNFeDFeICabNO_Ambiente.Value := 'Homologa��o';
    else QrNFeDFeICabNO_Ambiente.Value := '';
  end;
}
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeIEveAfterOpen(DataSet: TDataSet);
begin
  DBGEve.Visible := QrNFeDFeIEve.RecordCount > 0;
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeIItsAfterScroll(DataSet: TDataSet);
begin
  ReopenNFeDFeIXXX();
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeIItsBeforeClose(DataSet: TDataSet);
begin
  QrNFeDFeINFe.Close;
  QrNFeDFeIEve.Close;
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeIItsCalcFields(DataSet: TDataSet);
begin
(*&
  QrNFeDesRCabDataHora_TXT.Value := dmkPF.FDT_NULO(QrNFeDesRCabdhResp.Value, 0);
  QrNFeDesRCabNO_Ambiente.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,
    QrNFeDesRCabtpAmb.Value);
  QrNFeDesRCabNO_indCont.Value := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indCont,
    QrNFeDesRCabindCont.Value);
&*)
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDFeINFeAfterOpen(DataSet: TDataSet);
begin
  DBGNFe.Visible := QrNFeDFeINFe.RecordCount > 0;
end;

procedure TFmNFeDistDFeInt_0100.QrNFeDesRItsCalcFields(DataSet: TDataSet);
begin
(*&
  if (QrNFeDesRItsNFe_CNPJ.Value <> '') or (QrNFeDesRItsNFe_CPF.Value <> '') then
    QrNFeDesRItsCNPJ_CPF_TXT.Text := Geral.FormataCNPJ_TT(
    QrNFeDesRItsNFe_CNPJ.Value + QrNFeDesRItsNFe_CPF.Value)
  else
  if (QrNFeDesRItsCanc_CNPJ.Value <> '') or (QrNFeDesRItsCanc_CPF.Value <> '') then
    QrNFeDesRItsCNPJ_CPF_TXT.Text := Geral.FormataCNPJ_TT(
    QrNFeDesRItsCanc_CNPJ.Value + QrNFeDesRItsCanc_CPF.Value)
  else
    QrNFeDesRItsCNPJ_CPF_TXT.Text := '';
  //
  if QrNFeDesRItsNFe_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsNFe_xNome.Value
  else
  if QrNFeDesRItsNFe_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsNFe_xNome.Value
  else
  if QrNFeDesRItsCanc_xNome.Value <> '' then
    QrNFeDesRItsNOME.Text := QrNFeDesRItsCanc_xNome.Value
  else
    QrNFeDesRItsNOME.Text := '';
  //
  if QrNFeDesRItsCanc_cSitNFe.Value = 3 then
    QrNFeDesRItscSitNFe.Value := 3
  else
    QrNFeDesRItscSitNFe.Value := QrNFeDesRItsNFe_cSitNFe.Value;
  QrNFeDesRItsNO_cSitNFe.Value :=
    NFeXMLGeren.Obtem_DeManifestacao_de_cSitNFe_xSituacao(
    QrNFeDesRItscSitNFe.Value);
  //
  if QrNFeDesRItsCanc_dhRecbto.Value > QrNFeDesRItsNFe_dhRecbto.Value then
    QrNFeDesRItscSitConf.Value := QrNFeDesRItsCanc_cSitConf.Value
  else
    QrNFeDesRItscSitConf.Value := QrNFeDesRItsNFe_cSitConf.Value;
  //
  QrNFeDesRItsNO_cSitConf.Value :=
    NFeXMLGeren.Obtem_DeManifestacao_de_cSitConf_xEvento(
    QrNFeDesRItscSitConf.Value);
&*)
end;

end.

