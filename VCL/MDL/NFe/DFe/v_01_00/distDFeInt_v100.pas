
{*********************************************************************************************************}
{                                                                                                         }
{                                            XML Data Binding                                             }
{                                                                                                         }
{         Generated on: 29/10/2014 12:09:33                                                               }
{       Generated from: C:\_Compilers\Delphi_XE2\NFe\v_03_10\XSD\PL_NFeDistDFe_100\distDFeInt_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\Delphi_XE2\NFe\v_03_10\XSD\PL_NFeDistDFe_100\distDFeInt_v1.00.xdb   }
{                                                                                                         }
{*********************************************************************************************************}

unit distDFeInt_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLDistDFeInt = interface;
  IXMLDistDFeInt_distNSU = interface;
  IXMLDistDFeInt_consNSU = interface;

{ IXMLDistDFeInt }

  IXMLDistDFeInt = interface(IXMLNode)
    ['{945EB34B-459E-4346-AAD2-F0B792D449A8}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUFAutor: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_DistNSU: IXMLDistDFeInt_distNSU;
    function Get_ConsNSU: IXMLDistDFeInt_consNSU;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUFAutor(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CUFAutor: UnicodeString read Get_CUFAutor write Set_CUFAutor;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property DistNSU: IXMLDistDFeInt_distNSU read Get_DistNSU;
    property ConsNSU: IXMLDistDFeInt_consNSU read Get_ConsNSU;
  end;

{ IXMLDistDFeInt_distNSU }

  IXMLDistDFeInt_distNSU = interface(IXMLNode)
    ['{32D85CD1-0395-4EE3-8C36-27770C12C2B9}']
    { Property Accessors }
    function Get_UltNSU: UnicodeString;
    procedure Set_UltNSU(Value: UnicodeString);
    { Methods & Properties }
    property UltNSU: UnicodeString read Get_UltNSU write Set_UltNSU;
  end;

{ IXMLDistDFeInt_consNSU }

  IXMLDistDFeInt_consNSU = interface(IXMLNode)
    ['{22A78F25-8BC2-4194-8062-D61029C9A7C7}']
    { Property Accessors }
    function Get_NSU: UnicodeString;
    procedure Set_NSU(Value: UnicodeString);
    { Methods & Properties }
    property NSU: UnicodeString read Get_NSU write Set_NSU;
  end;

{ Forward Decls }

  TXMLDistDFeInt = class;
  TXMLDistDFeInt_distNSU = class;
  TXMLDistDFeInt_consNSU = class;

{ TXMLDistDFeInt }

  TXMLDistDFeInt = class(TXMLNode, IXMLDistDFeInt)
  protected
    { IXMLDistDFeInt }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUFAutor: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_DistNSU: IXMLDistDFeInt_distNSU;
    function Get_ConsNSU: IXMLDistDFeInt_consNSU;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUFAutor(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDistDFeInt_distNSU }

  TXMLDistDFeInt_distNSU = class(TXMLNode, IXMLDistDFeInt_distNSU)
  protected
    { IXMLDistDFeInt_distNSU }
    function Get_UltNSU: UnicodeString;
    procedure Set_UltNSU(Value: UnicodeString);
  end;

{ TXMLDistDFeInt_consNSU }

  TXMLDistDFeInt_consNSU = class(TXMLNode, IXMLDistDFeInt_consNSU)
  protected
    { IXMLDistDFeInt_consNSU }
    function Get_NSU: UnicodeString;
    procedure Set_NSU(Value: UnicodeString);
  end;

{ Global Functions }

function GetdistDFeInt(Doc: IXMLDocument): IXMLDistDFeInt;
function LoaddistDFeInt(const FileName: string): IXMLDistDFeInt;
function NewdistDFeInt: IXMLDistDFeInt;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

{ Global Functions }

function GetdistDFeInt(Doc: IXMLDocument): IXMLDistDFeInt;
begin
  Result := Doc.GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

function LoaddistDFeInt(const FileName: string): IXMLDistDFeInt;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

function NewdistDFeInt: IXMLDistDFeInt;
begin
  Result := NewXMLDocument.GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

{ TXMLDistDFeInt }

procedure TXMLDistDFeInt.AfterConstruction;
begin
  RegisterChildNode('distNSU', TXMLDistDFeInt_distNSU);
  RegisterChildNode('consNSU', TXMLDistDFeInt_consNSU);
  inherited;
end;

function TXMLDistDFeInt.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLDistDFeInt.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLDistDFeInt.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLDistDFeInt.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CUFAutor: UnicodeString;
begin
  Result := ChildNodes['cUFAutor'].Text;
end;

procedure TXMLDistDFeInt.Set_CUFAutor(Value: UnicodeString);
begin
  ChildNodes['cUFAutor'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLDistDFeInt.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLDistDFeInt.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_DistNSU: IXMLDistDFeInt_distNSU;
begin
  Result := ChildNodes['distNSU'] as IXMLDistDFeInt_distNSU;
end;

function TXMLDistDFeInt.Get_ConsNSU: IXMLDistDFeInt_consNSU;
begin
  Result := ChildNodes['consNSU'] as IXMLDistDFeInt_consNSU;
end;

{ TXMLDistDFeInt_distNSU }

function TXMLDistDFeInt_distNSU.Get_UltNSU: UnicodeString;
begin
  Result := ChildNodes['ultNSU'].Text;
end;

procedure TXMLDistDFeInt_distNSU.Set_UltNSU(Value: UnicodeString);
begin
  ChildNodes['ultNSU'].NodeValue := Value;
end;

{ TXMLDistDFeInt_consNSU }

function TXMLDistDFeInt_consNSU.Get_NSU: UnicodeString;
begin
  Result := ChildNodes['NSU'].Text;
end;

procedure TXMLDistDFeInt_consNSU.Set_NSU(Value: UnicodeString);
begin
  ChildNodes['NSU'].NodeValue := Value;
end;

end.