
{********************************************************************************************************************************************}
{                                                                                                                                            }
{                                                              XML Data Binding                                                              }
{                                                                                                                                            }
{         Generated on: 31/03/2022 10:41:37                                                                                                  }
{       Generated from: C:\_Compilers\projetosdelphi11_0_alexandria_2022_01\VCL\MDL\NFe\v_04_00\XSD\PL_NFeDistDFe_102\distDFeInt_v1.01.xsd   }
{   Settings stored in: C:\_Compilers\projetosdelphi11_0_alexandria_2022_01\VCL\MDL\NFe\v_04_00\XSD\PL_NFeDistDFe_102\distDFeInt_v1.01.xdb   }
{                                                                                                                                            }
{********************************************************************************************************************************************}

unit distDFeInt_v101;

interface

uses Xml.xmldom, Xml.XMLDoc, Xml.XMLIntf;

type

{ Forward Decls }

  IXMLDistDFeInt = interface;
  IXMLDistDFeInt_distNSU = interface;
  IXMLDistDFeInt_consNSU = interface;
  IXMLDistDFeInt_consChNFe = interface;

{ IXMLDistDFeInt }

  IXMLDistDFeInt = interface(IXMLNode)
    ['{E47331AA-D30A-4E68-A749-4BFE2E3EEF92}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUFAutor: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_DistNSU: IXMLDistDFeInt_distNSU;
    function Get_ConsNSU: IXMLDistDFeInt_consNSU;
    function Get_ConsChNFe: IXMLDistDFeInt_consChNFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUFAutor(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CUFAutor: UnicodeString read Get_CUFAutor write Set_CUFAutor;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property DistNSU: IXMLDistDFeInt_distNSU read Get_DistNSU;
    property ConsNSU: IXMLDistDFeInt_consNSU read Get_ConsNSU;
    property ConsChNFe: IXMLDistDFeInt_consChNFe read Get_ConsChNFe;
  end;

{ IXMLDistDFeInt_distNSU }

  IXMLDistDFeInt_distNSU = interface(IXMLNode)
    ['{EA596D36-A9FD-4ADD-A8EB-E1B63C750C6B}']
    { Property Accessors }
    function Get_UltNSU: UnicodeString;
    procedure Set_UltNSU(Value: UnicodeString);
    { Methods & Properties }
    property UltNSU: UnicodeString read Get_UltNSU write Set_UltNSU;
  end;

{ IXMLDistDFeInt_consNSU }

  IXMLDistDFeInt_consNSU = interface(IXMLNode)
    ['{E49F3572-43E2-4DA6-86F1-C951F84F6F49}']
    { Property Accessors }
    function Get_NSU: UnicodeString;
    procedure Set_NSU(Value: UnicodeString);
    { Methods & Properties }
    property NSU: UnicodeString read Get_NSU write Set_NSU;
  end;

{ IXMLDistDFeInt_consChNFe }

  IXMLDistDFeInt_consChNFe = interface(IXMLNode)
    ['{9E6476A3-3C0A-40EB-923D-0D34C527FB82}']
    { Property Accessors }
    function Get_ChNFe: UnicodeString;
    procedure Set_ChNFe(Value: UnicodeString);
    { Methods & Properties }
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
  end;

{ Forward Decls }

  TXMLDistDFeInt = class;
  TXMLDistDFeInt_distNSU = class;
  TXMLDistDFeInt_consNSU = class;
  TXMLDistDFeInt_consChNFe = class;

{ TXMLDistDFeInt }

  TXMLDistDFeInt = class(TXMLNode, IXMLDistDFeInt)
  protected
    { IXMLDistDFeInt }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUFAutor: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_DistNSU: IXMLDistDFeInt_distNSU;
    function Get_ConsNSU: IXMLDistDFeInt_consNSU;
    function Get_ConsChNFe: IXMLDistDFeInt_consChNFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUFAutor(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDistDFeInt_distNSU }

  TXMLDistDFeInt_distNSU = class(TXMLNode, IXMLDistDFeInt_distNSU)
  protected
    { IXMLDistDFeInt_distNSU }
    function Get_UltNSU: UnicodeString;
    procedure Set_UltNSU(Value: UnicodeString);
  end;

{ TXMLDistDFeInt_consNSU }

  TXMLDistDFeInt_consNSU = class(TXMLNode, IXMLDistDFeInt_consNSU)
  protected
    { IXMLDistDFeInt_consNSU }
    function Get_NSU: UnicodeString;
    procedure Set_NSU(Value: UnicodeString);
  end;

{ TXMLDistDFeInt_consChNFe }

  TXMLDistDFeInt_consChNFe = class(TXMLNode, IXMLDistDFeInt_consChNFe)
  protected
    { IXMLDistDFeInt_consChNFe }
    function Get_ChNFe: UnicodeString;
    procedure Set_ChNFe(Value: UnicodeString);
  end;

{ Global Functions }

function GetdistDFeInt(Doc: IXMLDocument): IXMLDistDFeInt;
function LoaddistDFeInt(const FileName: string): IXMLDistDFeInt;
function NewdistDFeInt: IXMLDistDFeInt;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/nfe';

implementation

uses System.Variants, Xml.xmlutil;

{ Global Functions }

function GetdistDFeInt(Doc: IXMLDocument): IXMLDistDFeInt;
begin
  Result := Doc.GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

function LoaddistDFeInt(const FileName: string): IXMLDistDFeInt;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

function NewdistDFeInt: IXMLDistDFeInt;
begin
  Result := NewXMLDocument.GetDocBinding('distDFeInt', TXMLDistDFeInt, TargetNamespace) as IXMLDistDFeInt;
end;

{ TXMLDistDFeInt }

procedure TXMLDistDFeInt.AfterConstruction;
begin
  RegisterChildNode('distNSU', TXMLDistDFeInt_distNSU);
  RegisterChildNode('consNSU', TXMLDistDFeInt_consNSU);
  RegisterChildNode('consChNFe', TXMLDistDFeInt_consChNFe);
  inherited;
end;

function TXMLDistDFeInt.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLDistDFeInt.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLDistDFeInt.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLDistDFeInt.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CUFAutor: UnicodeString;
begin
  Result := ChildNodes['cUFAutor'].Text;
end;

procedure TXMLDistDFeInt.Set_CUFAutor(Value: UnicodeString);
begin
  ChildNodes['cUFAutor'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLDistDFeInt.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLDistDFeInt.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLDistDFeInt.Get_DistNSU: IXMLDistDFeInt_distNSU;
begin
  Result := ChildNodes['distNSU'] as IXMLDistDFeInt_distNSU;
end;

function TXMLDistDFeInt.Get_ConsNSU: IXMLDistDFeInt_consNSU;
begin
  Result := ChildNodes['consNSU'] as IXMLDistDFeInt_consNSU;
end;

function TXMLDistDFeInt.Get_ConsChNFe: IXMLDistDFeInt_consChNFe;
begin
  Result := ChildNodes['consChNFe'] as IXMLDistDFeInt_consChNFe;
end;

{ TXMLDistDFeInt_distNSU }

function TXMLDistDFeInt_distNSU.Get_UltNSU: UnicodeString;
begin
  Result := ChildNodes['ultNSU'].Text;
end;

procedure TXMLDistDFeInt_distNSU.Set_UltNSU(Value: UnicodeString);
begin
  ChildNodes['ultNSU'].NodeValue := Value;
end;

{ TXMLDistDFeInt_consNSU }

function TXMLDistDFeInt_consNSU.Get_NSU: UnicodeString;
begin
  Result := ChildNodes['NSU'].Text;
end;

procedure TXMLDistDFeInt_consNSU.Set_NSU(Value: UnicodeString);
begin
  ChildNodes['NSU'].NodeValue := Value;
end;

{ TXMLDistDFeInt_consChNFe }

function TXMLDistDFeInt_consChNFe.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLDistDFeInt_consChNFe.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

end.