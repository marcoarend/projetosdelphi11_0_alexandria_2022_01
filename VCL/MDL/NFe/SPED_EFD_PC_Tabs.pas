unit SPED_EFD_PC_Tabs;
// antigo SPED_EFD_Tabs

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums,
  mySQLDbTables;

type
  TSPED_EFD_PC_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
  end;

const
  CO_TbTxt_REC_REF_OBRIGACAO = 'REC_REF_OBRIGACAO';
  FNot_SPED_EFD_PC = 2; //2; // 6
  FQtd_SPED_EFD_PC = 57;
  FTbs_SPED_EFD_PC: array[0..FQtd_SPED_EFD_PC-1] of String = (
    'SpedEfdPisCofinsEnce',
    'SpedEfdPisCofinsErrs',
    'SpedEfdPisCofins0000', 'SpedEfdPisCofins0001', //'SpedEfdPisCofins0002', 'SpedEfdPisCofins0005',
    'SpedEfdPisCofins0100', 'SpedEfdPisCofins0110', 'SpedEfdPisCofins0140',
    'SpedEfdPisCofins0150', 'SpedEfdPisCofins0190', 'SpedEfdPisCofins0200', //'SpedEfdPisCofins0210', 'SpedEfdPisCofins0220',
    'SpedEfdPisCofins0400', //'SpedEfdPisCofins0460',
    'SpedEfdPisCofins0500',
    'SpedEfdPisCofins0900',
    'SpedEfdPisCofins0990',
    'SpedEfdPisCofinsA001', 'SpedEfdPisCofinsA990',
    'SpedEfdPisCofinsC001', 'SpedEfdPisCofinsC010', 'SpedEfdPisCofinsC100', 'SpedEfdPisCofinsC170',
    //'SpedEfdPisCofinsC190', 'SpedEfdPisCofinsC195', 'SpedEfdPisCofinsC197',
    'SpedEfdPisCofinsC500', 'SpedEfdPisCofinsC501', 'SpedEfdPisCofinsC505',
    'SpedEfdPisCofinsC990',
    'SpedEfdPisCofinsD001', 'SpedEfdPisCofinsD010', 'SpedEfdPisCofinsD100', 'SpedEfdPisCofinsD101', 'SpedEfdPisCofinsD105',
    //'SpedEfdPisCofinsD190',
    'SpedEfdPisCofinsD500', 'SpedEfdPisCofinsD501', 'SpedEfdPisCofinsD505',
    'SpedEfdPisCofinsD990',
    //'SpedEfdPisCofinsE001', 'SpedEfdPisCofinsE100', 'SpedEfdPisCofinsE110', 'SpedEfdPisCofinsE111', 'SpedEfdPisCofinsE112',
    //'SpedEfdPisCofinsE113', 'SpedEfdPisCofinsE115', 'SpedEfdPisCofinsE116',
    //'SpedEfdPisCofinsE500', 'SpedEfdPisCofinsE510', 'SpedEfdPisCofinsE520', 'SpedEfdPisCofinsE530',
    //'SpedEfdPisCofinsE990',
    'SpedEfdPisCofinsF001', 'SpedEfdPisCofinsF990',
    //'SpedEfdPisCofinsG001', 'SpedEfdPisCofinsG990',
    //'SpedEfdPisCofinsH001', 'SpedEfdPisCofinsH005', 'SpedEfdPisCofinsH010', 'SpedEfdPisCofinsH020', 'SpedEfdPisCofinsH990',
    'SpedEfdPisCofinsI001', 'SpedEfdPisCofinsI990',
    {'SpedEfdPisCofinsK001', 'SpedEfdPisCofinsK100', 'SpedEfdPisCofinsK200', 'SpedEfdPisCofinsK210', 'SpedEfdPisCofinsK215',
    'SpedEfdPisCofinsK220', 'SpedEfdPisCofinsK230', 'SpedEfdPisCofinsK235', 'SpedEfdPisCofinsK250', 'SpedEfdPisCofinsK255',
    'SpedEfdPisCofinsK260', 'SpedEfdPisCofinsK265', 'SpedEfdPisCofinsK270', 'SpedEfdPisCofinsK275', 'SpedEfdPisCofinsK280',
    'SpedEfdPisCofinsK290', 'SpedEfdPisCofinsK291', 'SpedEfdPisCofinsK292',
    'SpedEfdPisCofinsK300', 'SpedEfdPisCofinsK301', 'SpedEfdPisCofinsK302',
    'SpedEfdPisCofinsK990',}
    'SpedEfdPisCofinsM001', 'SpedEfdPisCofinsM200', 'SpedEfdPisCofinsM205', 'SpedEfdPisCofinsM210', 'SpedEfdPisCofinsM400',
    'SpedEfdPisCofinsM410', 'SpedEfdPisCofinsM600', 'SpedEfdPisCofinsM605', 'SpedEfdPisCofinsM610', 'SpedEfdPisCofinsM800',
    'SpedEfdPisCofinsM810', 'SpedEfdPisCofinsM990',
    'SpedEfdPisCofinsP001', 'SpedEfdPisCofinsP990',

    'SpedEfdPisCofins1001', (*'SpedEfdPisCofins1010',*) 'SpedEfdPisCofins1990',
    'SpedEfdPisCofins9001', 'SpedEfdPisCofins9900', 'SpedEfdPisCofins9990', 'SpedEfdPisCofins9999');

var
  SPED_EFD_PC_Tb: TSPED_EFD_PC_Tabs;

implementation

uses UMySQLModule, ModuleGeral;

function TSPED_EFD_PC_Tabs.CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>): Boolean;
var
  CDR: Boolean;
begin
  CDR := False; // VAR_IS_CollectDataServerRepository;
  try
    if (DModG <> nil) and (Database = DModG.AllID_DB) then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsBlcs'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsFlds'), '');
    end else
    begin
(*
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsA001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsA001'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsC001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsC100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsC170'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsC500'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsD001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsD100'), '');
      //Linguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsD190'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsD500'), '');
      //Linguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsD590'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE110'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE111'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE112'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE113'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE115'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE116'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE500'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE510'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE520'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsE530'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsH005'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsH010'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsH020'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK200'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK210'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK215'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK220'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK230'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK235'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK23SubPrd'), 'EfdPisCofinsK230');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK250'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK255'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK25SubPrd'), 'EfdPisCofinsK250');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK260'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK265'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK270'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK275'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK27SubPrd'), 'EfdPisCofinsK270');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK280'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK290'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK291'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK29SubPrd'), 'EfdPisCofinsK291');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK292'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK300'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK301'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK30SubPrd'), 'EfdPisCofinsK301');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsK302'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofinsk_confg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofins_cfgerr'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('EfdPisCofins1010'), '');
*)
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsCtrl'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsEnce'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsErrs'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsVers'), '');
      //



      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0000'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0001'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0002'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0005'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0110'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0140'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0150'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0190'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0200'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0210'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0220'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0400'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0460'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0500'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0900'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins0990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsA001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsA990'), '');

      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsB001'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsB990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC010'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC170'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC190'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC195'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC197'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC500'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC501'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC505'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC590'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsC990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD010'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD101'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD105'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD190'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD500'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD501'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD505'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD590'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsD990'), '');

      (*MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE110'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE111'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE112'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE113'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE115'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE116'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE500'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE510'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE520'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE530'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsE990'), '');*)

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsF001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsF990'), '');

      (*MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsG001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsG990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsH001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsH005'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsH010'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsH020'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsH990'), '');*)

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsI001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsI990'), '');

      (*MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK100'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK200'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK210'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK215'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK220'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK230'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK235'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK250'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK255'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK260'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK265'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK270'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK275'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK280'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK290'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK291'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK292'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK300'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK301'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK302'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsK990'), '');*)

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM200'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM205'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM210'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM400'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM410'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM600'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM605'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM610'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM800'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM810'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsM990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsP001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofinsP990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins1001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins1010'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins1990'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins9001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins9900'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins9990'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SpedEfdPisCofins9999'), '');
      //
    end;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TSPED_EFD_PC_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsblcs') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
}
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsctrl') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabG') then
  begin
{
    FListaSQL.Add('Codigo|Nome|UF');
    FListaSQL.Add('0|"Tabelas com grupo indefinido"|""');
    FListaSQL.Add('1|"Tabelas Globais"|""');
    FListaSQL.Add('2|"Tabelas Gen�ricas - ICMS"|""');
    FListaSQL.Add('3|"Tabelas de Caracter�sticas dos Documentos"|""');
    FListaSQL.Add('4|"Tabelas do IPI"|""');
    FListaSQL.Add('5|"Tabelas de Situa��o Tribut�ria"|""');
    FListaSQL.Add('6|"Tabelas de Classes de Consumo"|""');
    FListaSQL.Add('7|"Tabelas do Acre"|"AC"');
    FListaSQL.Add('8|"Tabelas de Alagoas"|"AL"');
    FListaSQL.Add('9|"Tabelas do Amazonas"|"AM"');
    FListaSQL.Add('10|"Tabelas do Amap�"|"AP"');
    FListaSQL.Add('11|"Tabelas da Bahia"|"BA"');
    FListaSQL.Add('12|"Tabelas do Cear�"|"CE"');
    FListaSQL.Add('13|"Tabelas do Distrito Federal"|"DF"');
    FListaSQL.Add('14|"Tabelas do Esp�rito Santo"|"ES"');
    FListaSQL.Add('15|"Tabelas de Goi�s"|"GO"');
    FListaSQL.Add('16|"Tabelas do Maranh�o"|"MA"');
    FListaSQL.Add('17|"Tabelas do Mato Grosso"|"MT"');
    FListaSQL.Add('18|"Tabelas do Mato Grosso do Sul"|"MS"');
    FListaSQL.Add('19|"Tabelas de Minas Gerais"|"MG"');
    FListaSQL.Add('20|"Tabelas do Par�"|"PA"');
    FListaSQL.Add('21|"Tabelas da Para�ba"|"PB"');
    FListaSQL.Add('22|"Tabelas de Pernambuco"|"PE"');
    FListaSQL.Add('23|"Tabelas do Paran�"|"PR"');
    FListaSQL.Add('24|"Tabelas do Piau�"|"PI"');
    FListaSQL.Add('25|"Tabelas do Rio de Janeiro"|"RJ"');
    FListaSQL.Add('26|"Tabelas do Rio Grande do Norte"|"RN"');
    FListaSQL.Add('27|"Tabelas do Rio Grande do Sul"|"RS"');
    FListaSQL.Add('28|"Tabelas de Roraima"|"RR"');
    FListaSQL.Add('29|"Tabelas de Rond�nia"|"RO"');
    FListaSQL.Add('30|"Tabelas de Santa Catarina"|"SC"');
    FListaSQL.Add('31|"Tabelas de S�o Paulo"|"SP"');
    FListaSQL.Add('32|"Tabelas de Sergipe"|"SE"');
    FListaSQL.Add('33|"Tabelas de Tocantins"|"TO"');
}
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabT') then
  begin
{
    FListaSQL.Add('Controle|Codigo|Nome|TabelaTXT');
    FListaSQL.Add('1|1|"Tabela de Produtos para Combust�veis / Solvente"|""');
    FListaSQL.Add('2|1|"Tabela C�digo Fiscal de Opera��o e Presta��o - CFOP"|""');
    FListaSQL.Add('3|1|"Tabela G�nero do Item de Mercadoria/Servi�o"|""');
    FListaSQL.Add('4|1|"Tabela de Munc�pios do IBGE"|""');
    FListaSQL.Add('5|1|"5.4 - Tabela de C�digos das Obriga��es do ICMS a Recolher"|""');
    FListaSQL.Add('6|1|"Tabela de Pa�ses do IBGE"|""');
    FListaSQL.Add('7|1|"Tipo do Conhecimento de Transporte"|""');
    FListaSQL.Add('8|1|"4.4.6- Tabela de C�digos dos Totalizadores Parciais da REDU��O Z"|""');
    FListaSQL.Add('9|1|"UF C�digo IBGE - Sigla"|""');
    FListaSQL.Add('10|1|"UF Sigla"|""');
    FListaSQL.Add('11|1|"3.1.1 - Tabela Vers�o do Leiaute - Sped Fiscal"|""');
    FListaSQL.Add('12|1|"Tipo de Conhecimento de Transporte Eletr�nico"|""');
    FListaSQL.Add('13|1|"Tabela de Pa�ses SISCOMEX"|""');
    //
    FListaSQL.Add('189|11|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('190|11|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('245|11|"5.2 - Tabela de Informa��es Adicionais da Apura��o - Valores Declarat�rios - BA"|""');
    FListaSQL.Add('408|11|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - BA"|""');
    //
    FListaSQL.Add('46|21|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('47|21|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('48|21|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('49|21|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('125|21|"5.4- Tabela de C�digos das Obriga��es  de ICMS a Recolher"|""');
    FListaSQL.Add('195|21|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('33|8|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('34|8|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('35|8|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('196|8|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('16|3|"Tabela de Indicador de Emitente"|""');
    FListaSQL.Add('17|3|"Tabela Documentos Fiscais do ICMS"|""');
    FListaSQL.Add('18|3|"4.1.2- Tabela Situa��o do Documento"|""');
    //
    FListaSQL.Add('28|6|"4.4.2- Tabela Classes de Consumo de �gua Canalizada"|""');
    FListaSQL.Add('29|6|"4.4.3- Tabela Classes de Consumo de G�s Canalizado"|""');
    FListaSQL.Add('30|6|"4.4.4- Tabela Classes de Consumo dos Servi�os de Comunica��o e Telecomunica��o"|""');
    FListaSQL.Add('31|6|"4.4.1- Tabela Classifica��o de Itens de Energia El�trica, Servi�os de Comunica��o e Telecomunica��o"|""');
    FListaSQL.Add('32|6|"4.4.5 - Tabela - Faixa de Consumo de Energia El�trica"|""');
    //
    FListaSQL.Add('37|15|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('38|15|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('39|15|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('40|15|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('187|15|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('43|19|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('44|19|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('45|19|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('194|19|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('248|19|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - MG"|""');
    FListaSQL.Add('249|19|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios MG"|""');
    //
    FListaSQL.Add('212|22|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    //
    FListaSQL.Add('53|29|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('179|29|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('185|29|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('54|30|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('55|30|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('183|30|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('247|31|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS - SP"|""');
    FListaSQL.Add('598|31|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal SP"|""');
    FListaSQL.Add('246|31|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios"|""');
    //
    FListaSQL.Add('56|32|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('57|32|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('58|32|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('182|32|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('23|5|"Tabela C�digo de Situa��o Tribut�ria da COFINS"|""');
    FListaSQL.Add('26|5|"Tabela C�digo de Tributa��o do IPI - CST_IPI"|""');
    FListaSQL.Add('27|5|"Tabela C�digo de Situa��o Tribut�ria do PIS"|""');
    FListaSQL.Add('130|5|"Tabela C�digo da Situa��o Tribut�ria - CST (ICMS)"|""');
    //
    FListaSQL.Add('59|33|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('60|33|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    //
    FListaSQL.Add('188|9|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('220|9|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('221|9|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios - AM"|""');
    FListaSQL.Add('222|9|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal - AM"|""');
    //
    FListaSQL.Add('36|12|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('197|12|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('171|14|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('172|14|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('193|14|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('555|14|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios ES"|""');
    //
    FListaSQL.Add('19|4|"4.5.4 - Tabela C�digo de Ajuste da Apura��o do IPI"|""');
    FListaSQL.Add('20|4|"4.5.1- Tabela de C�digos da Classe de Enquadramento do IPI"|""');
    FListaSQL.Add('21|4|"Tabela C�digo de Enquadramento Legal do IPI"|""');
    FListaSQL.Add('22|4|"4.5.2- Tabela de C�digo de Selo de Controle"|""');
    //
    FListaSQL.Add('122|16|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('121|16|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    //
    FListaSQL.Add('131|17|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('170|17|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    //
    FListaSQL.Add('41|18|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('42|18|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('192|18|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('123|20|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('124|20|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('186|20|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('50|23|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('51|23|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('191|23|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('127|24|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    //
    FListaSQL.Add('52|25|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('129|25|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('244|25|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RJ"|""');
    //
    FListaSQL.Add('126|26|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('128|26|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('223|26|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RN"|""');
    FListaSQL.Add('224|26|"Tabela de C�digos de Receita RN"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('173|27|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('175|27|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('176|27|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('180|27|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('184|27|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('14|2|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('15|2|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('181|2|"Tabela de C�digos de Receita - GNRE"|""');
(*
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsvers') then
  begin
    FListaSQL.Add('COD_VER|NUM_VER|DT_INI|DT_FIN');
    FListaSQL.Add('2|"1.01"|"2009-01-01"|"2009-12-31"');
    FListaSQL.Add('3|"1.02"|"2010-01-01"|"2010-12-31"');
    FListaSQL.Add('4|"1.03"|"2011-01-01"|"0000-00-00"');
*)
}
  end;
end;

function TSPED_EFD_PC_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TSPED_EFD_PC_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsC001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsC100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsC170') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsC500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsD001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsD100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'D010';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsD101') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'D100';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsD105') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'D100';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsD500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE110') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE111') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E110';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE112') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E111';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE113') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E111';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE115') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E110';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE116') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E110';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE510') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E500';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE520') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E500';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsE530') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E520';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsH005') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsH010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsH020') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK200') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK210') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK215') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK220') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItmOri';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndItmDst';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 11;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK230') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK235') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK250') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK255') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK260') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK265') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK270') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'ORIGEM';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK275') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK280') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'DebCred';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 11;
    FRIndices.Column_name   := 'DT_EST';
    FLIndices.Add(FRIndices);
    //
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK290') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'COD_DOC_OP';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK291') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK292') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK300') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'DT_PROD';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK301') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsK302') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'KndTab';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'KndCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'KndNSU';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'KndItm';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 9;
    FRIndices.Column_name   := 'KndAID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 10;
    FRIndices.Column_name   := 'KndNiv';
    FLIndices.Add(FRIndices);
    //
*)
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'IDSeq1';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'IDSeq2';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofinsk_confg') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'MovimXX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'REGISTRO';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 8;
    FRIndices.Column_name   := 'MovimID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofins_cfgerr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'PeriApu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('EfdPisCofins1010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
*)
  end else
}
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsBlcs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Bloco';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Registro';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsCtrl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsEnce') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsErrs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'REG';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'Campo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 7;
    FRIndices.Column_name   := 'Erro';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsFlds') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Bloco';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Registro';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Numero';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'VersaoIni';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'VersaoFim';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabG') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabT') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0000') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0002') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0005') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0110') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0140') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0150') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0190') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0200') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0400') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0900') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsA001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsA990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsB990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC170') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC190') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC195') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC197') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC501') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC505') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC590') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD101') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD105') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD190') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD501') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD505') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD590') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE110') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE111') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE112') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E111';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE113') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'E111';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE115') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE116') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE500') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE510') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE520') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE530') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsG001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsG990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH005') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH020') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK100') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK200') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK210') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK215') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK220') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK230') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK235') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK250') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK255') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK260') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK265') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK270') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK275') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK280') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK290') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK291') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK292') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK300') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK301') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK302') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsF001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsF990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsI001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsI990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM200') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM205') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM210') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM400') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM405') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM410') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM600') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM605') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM610') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM800') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM810') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsP001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsP990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1010') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9900') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'LinArq';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9990') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9999') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SPEDEstqCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
*)
(*
  if Uppercase(Tabela) = Uppercase('SPEDEstqIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'ImporExpor';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'GraGrux';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'Sit_prod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 6;
    FRIndices.Column_name   := 'Terceiro';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsBlcs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Bloco';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Registro';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsFlds') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Bloco';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Registro';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Numero';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'VersaoIni';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'VersaoFim';
    FLIndices.Add(FRIndices);
    //
  end else
  //
  Result := true;
end;

function TSPED_EFD_PC_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TSPED_EFD_PC_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
{
(*
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsC001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsC100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_NFE';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_E_S';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PGTO';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ABAT_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_MERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FRT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_FRT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SEG';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParTipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParCodi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfVal';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Importado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsC170') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_ITEM';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,5)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_NAT';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_APUR';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_IPI';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ENQ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_IPI';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_p';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_PIS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_r';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_p';
      FRCampos.Tipo       := 'double(6,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_COFINS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_r';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsC500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CONS';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_E_S';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_FORN';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TP_LIGACAO';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_GRUPO_TENSAO';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Importado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_DOCe';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FIN_DOCe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_DOCe_REF';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_DEST';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN_DEST';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD_DOC_REF';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HASH_DOC_REF';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER_DOC_REF';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC_REF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MES_DOC_REF';
      FRCampos.Tipo       := 'int(6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENER_INJET';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OUTRAS_DED';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsD001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsD100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_CTE';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_A_P';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TP_CTE';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_CTE_REF';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FRT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Importado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsD500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_A_P';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TP_ASSINANTE';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Importado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE110') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_AJ_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ESTORNOS_CRED';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_AJ_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ESTORNOS_DEB';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_CREDOR_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_APURADO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_DED';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_RECOLHER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_CREDOR_TRANSPORTAR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DEB_ESP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE111') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_AJ_APUR';
      FRCampos.Tipo       := 'varchar(8)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_APUR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE112') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E111';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_PROC';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PROC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE113') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E111';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)'; // � n�mero mas pode ser vazio!
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE115') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF_ADIC';
      FRCampos.Tipo       := 'varchar(8)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_INF_ADIC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE116') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OR';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_VCTO';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_REC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_PROC';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PROC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MES_REF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_APUR';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE510') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_IPI';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE520') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SD_ANT_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DEB_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CRED_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OD_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SD_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsE530') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E520';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_AJ';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_AJ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_DOC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsH005') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_INV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MOT_INV';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsH010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'H005';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_UNIT';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROP';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM_IR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalEnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NivPlaCta';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GenPlaCta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsH020') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'H010';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK200') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_EST';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EST';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza'; // DmkEnums.TGrandezaUnidMed
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade'; // COD_PART
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo_Item';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK210') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_ORI';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ORI';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK215') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_DES';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_DES';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK210') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_ORI';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ORI';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK215') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_DES';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_DES';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK230') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OP';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ENC';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK235') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_SAIDA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK250') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_PROD';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OP'; // por minha conta!!!
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK255') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_CONS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK260') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OP_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_SAIDA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_SAIDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_RET';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_RET';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK265') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_CONS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_RET';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK220') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItmOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItmDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_MOV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_ORI';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_DEST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ORI';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_DEST';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESOMIEM'; // Origem Geral
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK270') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_AP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_AP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OP_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ORIGEM';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriSPEDEFDKnd'; // Origem Tipo inclusao SPED EFD
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisPai';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK275') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ORIGEM';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriSPEDEFDKnd'; // Origem Tipo inclusao SPED EFD
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisPai';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK280') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_EST';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EST';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DebCred'; // TDebCred
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza'; // DmkEnums.TGrandezaUnidMed
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade'; // COD_PART // (BalEnt)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo_Item';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisPai';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisAvo';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemIDKnd'; // Origem K220
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriSPEDEFDKnd'; // Origem Tipo inclusao SPED EFD
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriBalID'; // Origem do balanco > TSPED_EFD_Bal;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriKndReg'; // Tipo de registro de origem > TSPED_EFD_Bal;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK290') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OP';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK291') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK292') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK300') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_PROD';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK301') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsK302') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndTab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNSU';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndItm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndAID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDSeq2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriOpeProc';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiMovCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofinsk_confg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimXX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REGISTRO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESTSTabSorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim01';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim02';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim03';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim04';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim05';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim06';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim07';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim08';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim09';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim10';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim11';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim12';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim13';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim14';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim15';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim16';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim17';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim18';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim19';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim20';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim21';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim22';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim23';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim24';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim25';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim26';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim27';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim28';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim29';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim30';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim31';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim32';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim33';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim34';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim35';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim36';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim37';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim38';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim39';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim40';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim41';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim42';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim43';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim44';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim45';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim46';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim47';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim48';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim49';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Movim50';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofins_cfgerr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PeriApu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrPesq';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BxaPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BxaAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BxaPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdErros';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrData';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N/I';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrSdo0';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N/I';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrGGXs';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N/I';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrBxa';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N/I';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrClRc';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N/I';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrIMEI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    //
      //
    end else
    if Uppercase(Tabela) = Uppercase('EfdPisCofins1010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EXP';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_CCRF';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_COMB';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_USINA';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_VA';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EE';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_CART';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FORM';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_AER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF1';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF3';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF4';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_REST_RESSARC_COMPL_ICMS';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
}
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsBlcs') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloco';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisPai';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorAciNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';   // 0=n
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorEstNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';   // 0=n
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Implementd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsCtrl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'versao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0.4';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPEDEstqIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VerSPEDLay'; // Vers�o do Layout do SPED
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsEnce') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimXX'; // Antigo ->  M o v i m V S
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0 = nao, 1 = sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerLibEFD'; //
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0 = nao, 1 = sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnvioEFD';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0 = nao, 1 = sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EncerraEFD';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0 = nao, 1 = sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsErrs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Erro';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsFlds') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloco';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Numero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VersaoIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VersaoFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tam';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TObrig';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Decimais';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin1';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin2';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin3';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin4';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin5';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin6';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin7';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin8';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabG') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsTabT') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TabelaTXT';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else

(*
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsVers') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_VER';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_VER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0000') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_VER';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TIPO_ESCRIT';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_SIT_ESP';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_REC_ANTERIOR';
      FRCampos.Tipo       := 'varchar(41)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NOME';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUFRAMA';
      FRCampos.Tipo       := 'varchar(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_NAT_PJ';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_ATIV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NOME';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPF';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRC';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CEP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'END';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COMPL';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAIRRO';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FONE';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FAX';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMAIL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0110') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INC_TRIB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_APRO_CRED';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_TIPO_CONT';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_REG_CUM';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0140') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_EST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NOME';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IE';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IM';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUFRAMA';
      FRCampos.Tipo       := 'varchar(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0150') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NOME';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PAIS';
      FRCampos.Tipo       := 'int(5)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPF';
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IE';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUFRAMA';
      FRCampos.Tipo       := 'varchar(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'END';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COMPL';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAIRRO';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0190') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0200') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_ITEM';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_BARRA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ANT_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID_INV';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TIPO_ITEM';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_NCM';
      FRCampos.Tipo       := 'varchar(8)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EX_IPI';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_GEN';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_LST';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NivelExclu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CEST';
      FRCampos.Tipo       := 'int(7)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0400') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_NAT';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_NAT';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_ALT';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_NAT_CC';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_CTA';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(5)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NOME_CTA';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA_REF';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ_EST';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0900') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_A';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_A';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_C';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_C';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_D';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_D';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_F';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_F';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_I';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_I';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_BLOCO_1';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_NRB_BLOCO_1';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_PERIODO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REC_TOTAL_NRB_PERIODO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins0990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_0';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsA001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsA990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_A';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Stat_C100';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Stat_C500';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_ESCRI';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C010';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_NFE';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_E_S';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PGTO';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ABAT_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_MERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FRT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_FRT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SEG';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParTipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParCodi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfVal';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC170') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_ITEM';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,5)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_NAT';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_APUR';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_IPI';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ENQ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_IPI';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_PIS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_QUANT';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS';
      FRCampos.Tipo       := 'double(8,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_COFINS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_QUANT';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ABAT_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC190') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OPR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OBS';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC195') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OBS';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC197') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C195';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_AJ';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUTROS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_ENT';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_DOCe';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC501') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS';
      FRCampos.Tipo       := 'double(8,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC505') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'C500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS';
      FRCampos.Tipo       := 'double(8,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsC990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_C';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Stat_D100i';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Stat_D500i';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(14)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D010';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_CTE';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_A_P';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TP_CTE';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CHV_CTE_REF';
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FRT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN_ORIG';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MUN_DEST';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD101') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_NAT_FRT';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD105') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_NAT_FRT';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD190') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OPR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OBS';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_OPER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EMIT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_A_P';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SERV_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD501') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS';
      FRCampos.Tipo       := 'double(8,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('spedefdpiscofinsd505') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_BC_CRED';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS';
      FRCampos.Tipo       := 'double(8,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD590') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'D500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OPR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RED_BC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OBS';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsD990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_D';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE110') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_AJ_DEBITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ESTORNOS_CRED';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_AJ_CREDITOS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ESTORNOS_DEB';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_CREDOR_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_APURADO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_DED';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_RECOLHER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SLD_CREDOR_TRANSPORTAR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DEB_ESP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE111') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_AJ_APUR';
      FRCampos.Tipo       := 'varchar(8)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_APUR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE112') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E111';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_PROC';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PROC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE113') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E111';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SUB';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE115') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INF_ADIC';
      FRCampos.Tipo       := 'varchar(8)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_INF_ADIC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_COMPL_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE116') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E110';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OR';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_VCTO';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_REC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_PROC';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PROC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MES_REF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE500') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_APUR';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE510') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_IPI';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE520') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E500';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SD_ANT_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DEB_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CRED_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OD_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_SD_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE530') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'E520';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_AJ';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_AJ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_DOC';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESCR_AJ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsE990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_E';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsG001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsG990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_G';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH005') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_INV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MOT_INV';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'H005';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_UNIT';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_PROP';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXT_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM_IR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH020') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'H010';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsH990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_H';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK100') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK200') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_EST';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EST';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK210') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_ORI';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ORI';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK215') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K210';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_DES';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_DES';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK220') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_MOV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_ORI';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM_DEST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK230') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OP';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_ENC';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK235') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K230';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_SAIDA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK250') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_PROD';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK255') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K250';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_CONS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK260') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OP_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_SAIDA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_SAIDA';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_RET';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_RET';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK265') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K260';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_CONS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_RET';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK270') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_AP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_AP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_OP_OS';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ORIGEM';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK275') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K270';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_INS_SUBST';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK280') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_EST';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_POS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_COR_NEG';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EST';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_PART';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK290') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_INI_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_FIN_OP';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_DOC_OP';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK291') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K290';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK292') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K290';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK300') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K100';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DT_PROD';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK301') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K300';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK302') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'K300';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_SEK';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 1=Importado, 2=Manual
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_ITEM';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsK990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_K';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsF001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsF990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_F';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsI001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsI990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_I';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM200') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_NC_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CRED_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CRED_DESC_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_NC_DEV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RET_NC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DED_NC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_NC_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_CUM_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RET_CUM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DED_CUM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_CUM_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM205') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M200';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_CAMPO';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_REC';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DEBITO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM210') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M200';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CONT';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_REC_BRT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_CONT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_ACRES_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_REDUC_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_CONT_AJUS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_PIS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_QUANT';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_APUR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_ACRES';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_REDUC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_DIFER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_DIFER_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM400') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESC_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM410') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M400';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_REC';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESC_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM600') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_NC_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CRED_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CRED_DESC_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_NC_DEV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RET_NC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DED_NC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_NC_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_CUM_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_RET_CUM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DED_CUM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_CUM_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_CONT_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM605') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M600';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NUM_CAMPO';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_REC';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_DEBITO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM610') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M600';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CONT';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_REC_BRT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_CONT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_ACRES_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_REDUC_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_CONT_AJUS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_QUANT';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_APUR';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_ACRES';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_AJUS_REDUC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_DIFER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_DIFER_ANT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_CONT_PER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM800') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_TOT_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESC_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM810') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M800';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NAT_REC';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VL_REC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DESC_COMPL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsM990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_M';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsP001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsP990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_P';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1010') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EXP';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_CCRF';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_COMB';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_USINA';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_VA';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_EE';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_CART';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_FORM';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_AER';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF1';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF3';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_GIAF4';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_REST_RESSARC_COMPL_ICMS';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins1990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9900') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG_BLC';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_REG_BLC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9990') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN_9';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofins9999') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'REG';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QTD_LIN';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SPEDEstqCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encerrado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
(*
    if Uppercase(Tabela) = Uppercase('SPEDEstqIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImporExpor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sit_Prod';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqIniQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqIniPrc';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqIniVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComprasQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComprasPrc';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComprasVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConsumoQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConsumoPrc';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConsumoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqFimQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqFimPrc';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqFimVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Exportado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsBlcs') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloco';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RegisPai';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorAciNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';   // 0=n
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorEstNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';   // 0=n
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Implementd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SpedEfdPisCofinsFlds') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloco';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Numero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VersaoIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VersaoFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Campo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tam';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TObrig';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Decimais';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SObrig';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin1';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin2';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin3';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin4';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin5';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin6';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin7';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescrLin8';
      FRCampos.Tipo       := 'varchar(80)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TSPED_EFD_PC_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
{
  //
  // ini 2022-04-16 - deesmarcado nesta data! ver o que realmente vai usar!
  //
  // EFD-SPC_C-001 :: Mercadorias
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_C-001';
  FRJanelas.Nome      := 'FmEFD_C001';
  FRJanelas.Descricao := 'Mercadorias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_C-100 :: NF / Avulsa / Produtor / NFe
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_C-100';
  FRJanelas.Nome      := 'FmEFD_C100';
  FRJanelas.Descricao := 'NF / Avulsa / Produtor / NFe';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_C-500 :: Energia El�trica / �gua / G�s
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_C-500';
  FRJanelas.Nome      := 'FmEFD_C500';
  FRJanelas.Descricao := 'Energia El�trica / �gua / G�s';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_D-001 :: Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_D-001';
  FRJanelas.Nome      := 'FmEFD_D001';
  FRJanelas.Descricao := 'Servi�os';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_D-100 :: Servi�o de Transporte
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_D-100';
  FRJanelas.Nome      := 'FmEFD_D500';
  FRJanelas.Descricao := 'Servi�o de Transporte';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_D-500 :: Comunica��o e Telecomunica��o
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_D-500';
  FRJanelas.Nome      := 'FmEFD_D500';
  FRJanelas.Descricao := 'Comunica��o e Telecomunica��o';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-001 :: Apura��o do ICMS e do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-001';
  FRJanelas.Nome      := 'FmEFD_E001';
  FRJanelas.Descricao := 'Apura��o do ICMS e do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-100 :: Per�odo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-100';
  FRJanelas.Nome      := 'FmEFD_E100';
  FRJanelas.Descricao := 'Per�odo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-110 :: Apura��o do ICMS - Opera��es pr�prias
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-110';
  FRJanelas.Nome      := 'FmEFD_E110';
  FRJanelas.Descricao := 'Apura��o do ICMS - Opera��es pr�prias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-111 :: Ajuste/Benef�cio/Incentivo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-111';
  FRJanelas.Nome      := 'FmEFD_E111';
  FRJanelas.Descricao := 'Ajuste/Benef�cio/Incentivo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-112 :: Informa��es Adicionais do Ajustes da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-112';
  FRJanelas.Nome      := 'FmEFD_E112';
  FRJanelas.Descricao := 'Informa��es Adicionais do Ajustes da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-113 :: Identifica��o dos Documentos Fiscais
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-113';
  FRJanelas.Nome      := 'FmEFD_E113';
  FRJanelas.Descricao := 'Identifica��o dos Documentos Fiscais';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-115 :: Informa��es Adicionais da Apura��o - Valores Declarat�rios
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-115';
  FRJanelas.Nome      := 'FmEFD_E115';
  FRJanelas.Descricao := 'Informa��es Adicionais da Apura��o - Valores Declarat�rios';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-116 :: Obriga��es do ICMS a Recolher - Opera��es Pr�prias
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-116';
  FRJanelas.Nome      := 'FmEFD_E116';
  FRJanelas.Descricao := 'Obriga��es do ICMS a Recolher - Opera��es Pr�prias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-500 :: Per�odo da Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-500';
  FRJanelas.Nome      := 'FmEFD_E500';
  FRJanelas.Descricao := 'Per�odo da Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-510 :: Consolida��o dos Valores do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-510';
  FRJanelas.Nome      := 'FmEFD_E510';
  FRJanelas.Descricao := 'Consolida��o dos Valores do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-520 :: Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-520';
  FRJanelas.Nome      := 'FmEFD_E520';
  FRJanelas.Descricao := 'Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_E-530 :: Ajustes da Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_E-530';
  FRJanelas.Nome      := 'FmEFD_E530';
  FRJanelas.Descricao := 'Ajustes da Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_H-005 :: Totais do Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_H-005';
  FRJanelas.Nome      := 'FmEFD_H005';
  FRJanelas.Descricao := 'Totais do Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_H-010 :: Item de Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_H-010';
  FRJanelas.Nome      := 'FmEFD_H010';
  FRJanelas.Descricao := 'Item de Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_H-020 :: Informa��o Complementar de Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_H-020';
  FRJanelas.Nome      := 'FmEFD_H020';
  FRJanelas.Descricao := 'Informa��o Complementar de Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-100 :: Per�odo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-100';
  FRJanelas.Nome      := 'FmEFD_K100';
  FRJanelas.Descricao := 'Per�odo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-200 :: Estoque Escriturado
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-200';
  FRJanelas.Nome      := 'FmEFD_K100';
  FRJanelas.Descricao := 'Estoque Escriturado';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-220 :: Movimenta��es Internas
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-220';
  FRJanelas.Nome      := 'FmEFD_K220';
  FRJanelas.Descricao := 'Movimenta��es Internas';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-230 :: Itens Produzidos
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-230';
  FRJanelas.Nome      := 'FmEFD_K230';
  FRJanelas.Descricao := 'Itens Produzidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-235 :: Insumos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-235';
  FRJanelas.Nome      := 'FmEFD_K235';
  FRJanelas.Descricao := 'Insumos Consumidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-250 :: Itens Produzidos em Terceiros
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-250';
  FRJanelas.Nome      := 'FmEFD_K250';
  FRJanelas.Descricao := 'Itens Produzidos em Terceiros';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EFD-SPC_K-255 :: Insumos Consumidos em Terceiros
  New(FRJanelas);
  FRJanelas.ID        := 'EFD-SPC_K-255';
  FRJanelas.Nome      := 'FmEFD_K255';
  FRJanelas.Descricao := 'Insumos Consumidos em Terceiros';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // fim 2022-04-16 - deesmarcado nesta data! ver o que realmente vai usar!
  //
  // SPE-EFDPC-000 :: Download de Tabelas SPED-EFD
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-000';
  FRJanelas.Nome      := 'FmEfdPisCofinsEnce';
  FRJanelas.Descricao := 'Download de Tabelas SPED-EFD';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-EFDPC-001 :: Importa��o de Arquivo SPED EFD Contribui��es
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-001';
  FRJanelas.Nome      := 'FmSPED_EFD_Importa';
  FRJanelas.Descricao := 'Importa��o de Arquivo SPED EFD Contribui��es';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
}
  // SPE-EFDPC-002 :: Exporta��o de Arquivo SPED EFD Contribui��es
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-002';
  FRJanelas.Nome      := 'FmEfdPisCofinsExporta';
  FRJanelas.Descricao := 'Exporta��o de Arquivo SPED EFD Contribui��es';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-EFDPC-003 :: Erros de Exporta��o SPED-EFD
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-003';
  FRJanelas.Nome      := 'FmEfdPisCofinsErros';
  FRJanelas.Descricao := 'Erros de Exporta��o SPED-EFD';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
{
  // SPE-EFDPC-004 :: Compara Importa��o SPED-EFD com Lan�amentos
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-004';
  FRJanelas.Nome      := 'FmSPED_EFD_Compara';
  FRJanelas.Descricao := 'Compara Importa��o SPED-EFD com Lan�amentos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-EFDPC-005 :: Datas Lan�amento Retroativo
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-EFDPC-005';
  FRJanelas.Nome      := 'FmXXDataEFDData';
  FRJanelas.Descricao := 'Datas Lan�amento Retroativo';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  //
  //
  // EII-SPEDC-001 :: Mercadorias
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDC-001';
  FRJanelas.Nome      := 'FmEfdIcmIpiC001';
  FRJanelas.Descricao := 'Mercadorias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDC-100 :: NF / Avulsa / Produtor / NFe
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDC-100';
  FRJanelas.Nome      := 'FmEfdIcmIpiC100';
  FRJanelas.Descricao := 'NF / Avulsa / Produtor / NFe';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDC-500 :: Energia El�trica / �gua / G�s
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDC-500';
  FRJanelas.Nome      := 'FmEfdIcmIpiC500';
  FRJanelas.Descricao := 'Energia El�trica / �gua / G�s';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDD-001 :: Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDD-001';
  FRJanelas.Nome      := 'FmEfdIcmIpiD001';
  FRJanelas.Descricao := 'Servi�os';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDD-100 :: Servi�o de Transporte
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDD-100';
  FRJanelas.Nome      := 'FmEfdIcmIpiD500';
  FRJanelas.Descricao := 'Servi�o de Transporte';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDD-500 :: Comunica��o e Telecomunica��o
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDD-500';
  FRJanelas.Nome      := 'FmEfdIcmIpiD500';
  FRJanelas.Descricao := 'Comunica��o e Telecomunica��o';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-001 :: Apura��o do ICMS e do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-001';
  FRJanelas.Nome      := 'FmEfdIcmIpiE001';
  FRJanelas.Descricao := 'Apura��o do ICMS e do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-100 :: Per�odo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-100';
  FRJanelas.Nome      := 'FmEfdIcmIpiE100';
  FRJanelas.Descricao := 'Per�odo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-110 :: Apura��o do ICMS - Opera��es pr�prias
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-110';
  FRJanelas.Nome      := 'FmEfdIcmIpiE110';
  FRJanelas.Descricao := 'Apura��o do ICMS - Opera��es pr�prias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-111 :: Ajuste/Benef�cio/Incentivo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-111';
  FRJanelas.Nome      := 'FmEfdIcmIpiE111';
  FRJanelas.Descricao := 'Ajuste/Benef�cio/Incentivo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-112 :: Informa��es Adicionais do Ajustes da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-112';
  FRJanelas.Nome      := 'FmEfdIcmIpiE112';
  FRJanelas.Descricao := 'Informa��es Adicionais do Ajustes da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-113 :: Identifica��o dos Documentos Fiscais
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-113';
  FRJanelas.Nome      := 'FmEfdIcmIpiE113';
  FRJanelas.Descricao := 'Identifica��o dos Documentos Fiscais';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-115 :: Informa��es Adicionais da Apura��o - Valores Declarat�rios
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-115';
  FRJanelas.Nome      := 'FmEfdIcmIpiE115';
  FRJanelas.Descricao := 'Informa��es Adicionais da Apura��o - Valores Declarat�rios';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-116 :: Obriga��es do ICMS a Recolher - Opera��es Pr�prias
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-116';
  FRJanelas.Nome      := 'FmEfdIcmIpiE116';
  FRJanelas.Descricao := 'Obriga��es do ICMS a Recolher - Opera��es Pr�prias';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-500 :: Per�odo da Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-500';
  FRJanelas.Nome      := 'FmEfdIcmIpiE500';
  FRJanelas.Descricao := 'Per�odo da Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-510 :: Consolida��o dos Valores do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-510';
  FRJanelas.Nome      := 'FmEfdIcmIpiE510';
  FRJanelas.Descricao := 'Consolida��o dos Valores do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-520 :: Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-520';
  FRJanelas.Nome      := 'FmEfdIcmIpiE520';
  FRJanelas.Descricao := 'Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDE-530 :: Ajustes da Apura��o do IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDE-530';
  FRJanelas.Nome      := 'FmEfdIcmIpiE530';
  FRJanelas.Descricao := 'Ajustes da Apura��o do IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDH-005 :: Totais do Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDH-005';
  FRJanelas.Nome      := 'FmEfdIcmIpiH005';
  FRJanelas.Descricao := 'Totais do Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDH-010 :: Item de Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDH-010';
  FRJanelas.Nome      := 'FmEfdIcmIpiH010';
  FRJanelas.Descricao := 'Item de Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDH-020 :: Informa��o Complementar de Invent�rio
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDH-020';
  FRJanelas.Nome      := 'FmEfdIcmIpiH020';
  FRJanelas.Descricao := 'Informa��o Complementar de Invent�rio';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-100 :: Per�odo da Apura��o do ICMS
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-100';
  FRJanelas.Nome      := 'FmEfdIcmIpiK100';
  FRJanelas.Descricao := 'Per�odo da Apura��o do ICMS';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-200 :: Item de Estoque Escriturado
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-200';
  FRJanelas.Nome      := 'FmEfdIcmIpiK200';
  FRJanelas.Descricao := 'Item deEstoque Escriturado';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-210 :: Desmontagem de Mercadorias � Item de Origem
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-210';
  FRJanelas.Nome      := 'FmEfdIcmIpiK210';
  FRJanelas.Descricao := 'Desmontagem de Mercadorias � Item de Origem';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-215 :: Desmontagem de Mercadorias � Item de Destino
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-215';
  FRJanelas.Nome      := 'FmEfdIcmIpiK215';
  FRJanelas.Descricao := 'Desmontagem de Mercadorias � Item de Destino';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-220 :: Movimenta��es Internas
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-220';
  FRJanelas.Nome      := 'FmEfdIcmIpiK220';
  FRJanelas.Descricao := 'Movimenta��es Internas';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-230 :: Itens Produzidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-230';
  FRJanelas.Nome      := 'FmEfdIcmIpiK230';
  FRJanelas.Descricao := 'Itens Produzidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-235 :: Insumos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-235';
  FRJanelas.Nome      := 'FmEfdIcmIpiK235';
  FRJanelas.Descricao := 'Insumos Consumidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-250 :: Itens Produzidos em Terceiros
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-250';
  FRJanelas.Nome      := 'FmEfdIcmIpiK250';
  FRJanelas.Descricao := 'Itens Produzidos em Terceiros';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-255 :: Insumos Consumidos em Terceiros
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-255';
  FRJanelas.Nome      := 'FmEfdIcmIpiK255';
  FRJanelas.Descricao := 'Insumos Consumidos em Terceiros';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-260 :: Reprocecssamento/Reparo de Produto/Insumo
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-260';
  FRJanelas.Nome      := 'FmEfdIcmIpiK260';
  FRJanelas.Descricao := 'Reprocecssamento/Reparo de Produto/Insumo';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-265 :: Reprocessamento/Reparo - Mercadoroas Consumidas e/ou Retornadas
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-265';
  FRJanelas.Nome      := 'FmEfdIcmIpiK265';
  FRJanelas.Descricao := 'Reprocessamento/Reparo - Mercadoroas Consumidas e/ou Retornadas';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-270 :: Corre��o de Apontamento
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-270';
  FRJanelas.Nome      := 'FmEfdIcmIpiK270';
  FRJanelas.Descricao := 'Corre��o de Apontamento';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-275: Corre��o de Apontamento e Retorno de Insumos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-275';
  FRJanelas.Nome      := 'FmEfdIcmIpiK275';
  FRJanelas.Descricao := 'Corre��o de Apontamento e Retorno de Insumos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-280 :: Item de Corre��o de Apontamento - Estoque Escriturado
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-280';
  FRJanelas.Nome      := 'FmEfdIcmIpiK280';
  FRJanelas.Descricao := 'Item de Corre��o de Apontamento - Estoque Escriturado';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-290 :: Produ��o Conjunta � Ordem de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-290';
  FRJanelas.Nome      := 'FmEfdIcmIpiK290';
  FRJanelas.Descricao := 'Produ��o Conjunta � Ordem de Produ��o';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-291: Produ��o Conjunta - Itens Produzidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-291';
  FRJanelas.Nome      := 'FmEfdIcmIpiK291';
  FRJanelas.Descricao := 'Produ��o Conjunta - Itens Produzidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-292 :: Produ��o Conjunta � Insumos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-292';
  FRJanelas.Nome      := 'FmEfdIcmIpiK292';
  FRJanelas.Descricao := 'Produ��o Conjunta � Insumos Consumidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-300 :: Produ��o Conjunta � Industrializa��o Efetuada por Terceiros
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-300';
  FRJanelas.Nome      := 'FmEfdIcmIpiK300';
  FRJanelas.Descricao := 'Produ��o Conjunta � Industrializa��o Efetuada por Terceiros';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-301 :: Produ��o Conjunta � Industrializa��o Efetuada por Terceiros � Itens Produzidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-301';
  FRJanelas.Nome      := 'FmEfdIcmIpiK301';
  FRJanelas.Descricao := 'Produ��o Conjunta � Industrializa��o Efetuada por Terceiros � Itens Produzidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-SPEDK-302 :: Produ��o Conjunta � Industrializa��o Efetuada por Terceiros � Insumos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'EII-SPEDK-302';
  FRJanelas.Nome      := 'FmEfdIcmIpiK302';
  FRJanelas.Descricao := 'Produ��o Conjunta � Industrializa��o Efetuada por Terceiros � Insumos Consumidos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_EII000 :: Download de Tabelas SPED-EFD
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_EII000';
  FRJanelas.Nome      := 'FmEfdPisCofinsEnce';
  FRJanelas.Descricao := 'Download de Tabelas SPED-EFD';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_EII001 :: Importa��o de Arquivo SPED EFD Contribui��es
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_EII001';
  FRJanelas.Nome      := 'FmSPED_EFD_Importa';
  FRJanelas.Descricao := 'Importa��o de Arquivo SPED EFD Contribui��es';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_EII002 :: Exporta��o de Arquivo SPED EFD Contribui��es
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_EII002';
  FRJanelas.Nome      := 'FmEfdPisCofinsExporta';
  FRJanelas.Descricao := 'Exporta��o de Arquivo SPED EFD Contribui��es';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_EII003 :: Erros de Exporta��o SPED-EFD
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_EII003';
  FRJanelas.Nome      := 'FmEfdPisCofinsErros';
  FRJanelas.Descricao := 'Erros de Exporta��o SPED-EFD';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_EII004 :: Compara Importa��o SPED-EFD com Lan�amentos
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_EII004';
  FRJanelas.Nome      := 'FmSPED_EFD_Compara';
  FRJanelas.Descricao := 'Compara Importa��o SPED-EFD com Lan�amentos';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-BALAN-000 :: Encerramento de Balancete EFD
  New(FRJanelas);
  FRJanelas.ID        := 'EII-BALAN-000';
  FRJanelas.Nome      := 'FmEfdPisCofinsEnce';
  FRJanelas.Descricao := 'Encerramento de Balancete EFD';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-BALAN-010 :: Importa��o de Balan�o para o SPED
  New(FRJanelas);
  FRJanelas.ID        := 'EII-BALAN-010';
  FRJanelas.Nome      := 'FmSPEDEstqLoadXLS';
  FRJanelas.Descricao := 'Importa��o de Balan�o para o SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // EII-BALAN-011 :: Importa��o de Balan�o para o SPED EFD ICMS /IPI
  New(FRJanelas);
  FRJanelas.ID        := 'EII-BALAN-011';
  FRJanelas.Nome      := 'FmSPED_EFD_X999_Balancos';
  FRJanelas.Descricao := 'Importa��o de Balan�o para o SPED EFD ICMS /IPI';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_K2X-001 :: Importa��o de Movimenta��es
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_K2X-001';
  FRJanelas.Nome      := 'FmSPED_EFD_K2XX';
  FRJanelas.Descricao := 'Importa��o de Movimenta��es';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_STQ-001 :: Confere Estoque SPED
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_STQ-001';
  FRJanelas.Nome      := 'FmSPEDEstqGer';
  FRJanelas.Descricao := 'Confere Estoque SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_STQ-002 :: Cadastra Per�odo Confer�ncia de Estoque SPED
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_STQ-002';
  FRJanelas.Nome      := 'FmSPEDEstqCab';
  FRJanelas.Descricao := 'Cadastra Per�odo Confer�ncia de Estoque SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_STQ-003 :: Insere Item em Confer�ncia de Estoque SPED
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_STQ-003';
  FRJanelas.Nome      := 'FmSPEDEstqIts';
  FRJanelas.Descricao := 'Insere Item em Confer�ncia de Estoque SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-D_STQ-004 :: Carrega Planilha Excel em Confer�ncia de Estoque SPED
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-D_STQ-004';
  FRJanelas.Nome      := 'FmSPEDEstqLoadXLS';
  FRJanelas.Descricao := 'Carrega Planilha Excel em Confer�ncia de Estoque SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-1_OUT-010 :: Obrigatoriedade de Registros do Bloco 1
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-1_OUT-010';
  FRJanelas.Nome      := 'FmEfdIcmIpi1010';
  FRJanelas.Descricao := 'Obrigatoriedade de Registros do Bloco 1';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-1_OUT-100 :: Informa��es sobre Exporta��o
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-1_OUT-100';
  FRJanelas.Nome      := 'FmEfdIcmIpi1100';
  FRJanelas.Descricao := 'Informa��es sobre Exporta��o';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-ENTRA-001 :: NF-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-ENTRA-001';
  FRJanelas.Nome      := 'FmEfdInnNFsCab';
  FRJanelas.Descricao := 'NF-e de Entrada';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-ENTRA-002 :: Itens de NF-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-ENTRA-002';
  FRJanelas.Nome      := 'FmEfdInnNFsIts';
  FRJanelas.Descricao := 'Itens de NF-e de Entrada';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-ENTRA-003 :: CT-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-ENTRA-003';
  FRJanelas.Nome      := 'FmEfdInnCTsCab';
  FRJanelas.Descricao := 'CT-e de Entrada';
  FLJanelas.Add(FRJanelas);
  //
  // SPE-ENTRA-004 :: Itens de CT-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-ENTRA-004';
  FRJanelas.Nome      := 'FmEfdInnCTsIts';
  FRJanelas.Descricao := 'Itens de CT-e de Entrada';
  FLJanelas.Add(FRJanelas);
  //
}
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

(*
      DROP TABLE IF EXISTS SpedEfdPisCofins0000;
      DROP TABLE IF EXISTS SpedEfdPisCofins0001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofins0002;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofins0005;
      DROP TABLE IF EXISTS SpedEfdPisCofins0100;
      DROP TABLE IF EXISTS SpedEfdPisCofins0110;
      DROP TABLE IF EXISTS SpedEfdPisCofins0140;
      DROP TABLE IF EXISTS SpedEfdPisCofins0150;
      DROP TABLE IF EXISTS SpedEfdPisCofins0190;
      DROP TABLE IF EXISTS SpedEfdPisCofins0200;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofins0210;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofins0220;
      DROP TABLE IF EXISTS SpedEfdPisCofins0400;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofins0460;
      DROP TABLE IF EXISTS SpedEfdPisCofins0500;
      DROP TABLE IF EXISTS SpedEfdPisCofins0900;
      DROP TABLE IF EXISTS SpedEfdPisCofins0990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsA001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsA990;

      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsB001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsB990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsC001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsC100;
      DROP TABLE IF EXISTS SpedEfdPisCofinsC170;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsC190;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsC195;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsC197;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsC500;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsC590;
      DROP TABLE IF EXISTS SpedEfdPisCofinsC990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsD001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsD100;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsD190;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsD500;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsD590;
      DROP TABLE IF EXISTS SpedEfdPisCofinsD990;

      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE100;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE110;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE111;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE112;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE113;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE115;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE116;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE500;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE510;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE520;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE530;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsE990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsF001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsF990;

      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsG001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsG990;

      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsH001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsH005;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsH010;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsH020;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsH990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsI001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsI990;

      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK001;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK100;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK200;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK210;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK215;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK220;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK230;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK235;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK250;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK255;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK260;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK265;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK270;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK275;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK280;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK290;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK291;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK292;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK300;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK301;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK302;
      /**/DROP TABLE IF EXISTS SpedEfdPisCofinsK990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsM001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM200;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM205;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM210;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM400;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM410;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM600;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM605;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM610;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM800;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM810;
      DROP TABLE IF EXISTS SpedEfdPisCofinsM990;

      DROP TABLE IF EXISTS SpedEfdPisCofinsP001;
      DROP TABLE IF EXISTS SpedEfdPisCofinsP990;

      DROP TABLE IF EXISTS SpedEfdPisCofins1001;
      DROP TABLE IF EXISTS SpedEfdPisCofins1010;
      DROP TABLE IF EXISTS SpedEfdPisCofins1990;

      DROP TABLE IF EXISTS SpedEfdPisCofins9001;
      DROP TABLE IF EXISTS SpedEfdPisCofins9900;
      DROP TABLE IF EXISTS SpedEfdPisCofins9990;
      DROP TABLE IF EXISTS SpedEfdPisCofins9999;

*)
end.
