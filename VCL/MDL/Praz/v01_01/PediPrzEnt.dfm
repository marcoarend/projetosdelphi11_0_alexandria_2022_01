object FmPediPrzEnt: TFmPediPrzEnt
  Left = 339
  Top = 185
  Caption = 'PED-PRAZO-103 :: Prazo de Pagamento - Entidades'
  ClientHeight = 774
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazo de Pagamento - Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazo de Pagamento - Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazo de Pagamento - Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 113
    Width = 999
    Height = 575
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnEdita: TPanel
          Left = 2
          Top = 312
          Width = 995
          Height = 261
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 0
          Visible = False
          object Label1: TLabel
            Left = 11
            Top = 63
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Entidade:'
          end
          object Label2: TLabel
            Left = 11
            Top = 7
            Width = 58
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
          end
          object Label3: TLabel
            Left = 11
            Top = 119
            Width = 38
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Prazo:'
          end
          object Label4: TLabel
            Left = 500
            Top = 119
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ordem:'
          end
          object Panel9: TPanel
            Left = 1
            Top = 201
            Width = 993
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            ParentBackground = False
            TabOrder = 8
            object BtConfirma2: TBitBtn
              Tag = 14
              Left = 10
              Top = 6
              Width = 147
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtConfirma2Click
            end
            object Panel10: TPanel
              Left = 824
              Top = 1
              Width = 168
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste2: TBitBtn
                Tag = 15
                Left = 9
                Top = 5
                Width = 147
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Desiste'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesiste2Click
              end
            end
          end
          object EdEntidade: TdmkEditCB
            Left = 11
            Top = 83
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 80
            Top = 83
            Width = 500
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'EntNome'
            ListSource = DsEntidade
            TabOrder = 3
            dmkEditCB = EdEntidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmpresa: TdmkEditCB
            Left = 11
            Top = 27
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 80
            Top = 27
            Width = 500
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPrazo: TdmkEditCB
            Left = 11
            Top = 139
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPrazo
            IgnoraDBLookupComboBox = False
          end
          object CBPrazo: TdmkDBLookupComboBox
            Left = 80
            Top = 139
            Width = 415
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            TabOrder = 5
            dmkEditCB = EdPrazo
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdOrdem: TdmkEdit
            Left = 500
            Top = 139
            Width = 80
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkContinuar: TCheckBox
            Left = 11
            Top = 173
            Width = 144
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Continuar inserindo.'
            Checked = True
            State = cbChecked
            TabOrder = 7
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 450
          Height = 294
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object DBGEntidades: TDBGrid
            Left = 0
            Top = 0
            Width = 450
            Height = 294
            Align = alClient
            DataSource = DsEntidades
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EntNome'
                Title.Caption = 'Entidade'
                Width = 300
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 452
          Top = 18
          Width = 545
          Height = 294
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object DBGEmpresas: TDBGrid
            Left = 0
            Top = 0
            Width = 545
            Height = 137
            Align = alTop
            DataSource = DsEmpresas
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EntNome'
                Title.Caption = 'Empresa'
                Width = 400
                Visible = True
              end>
          end
          object DBGPrazos: TDBGrid
            Left = 0
            Top = 137
            Width = 545
            Height = 157
            Align = alClient
            DataSource = DsPrazos
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomePrazo'
                Title.Caption = 'Prazo'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ordem'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 59
    Width = 999
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 999
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 14
        Left = 11
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 308
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtReordena: TBitBtn
        Tag = 236
        Left = 160
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Reordena'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReordenaClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 536
    Top = 219
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    Left = 272
    Top = 142
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesEntNome: TWideStringField
      FieldName = 'EntNome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 300
    Top = 142
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmpresasBeforeClose
    AfterScroll = QrEmpresasAfterScroll
    Left = 272
    Top = 198
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasEntNome: TWideStringField
      FieldName = 'EntNome'
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 300
    Top = 198
  end
  object QrPrazos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 254
    object QrPrazosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrazosNomePrazo: TWideStringField
      FieldName = 'NomePrazo'
      Size = 50
    end
    object QrPrazosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPrazosEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrPrazosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsPrazos: TDataSource
    DataSet = QrPrazos
    Left = 300
    Top = 254
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) EntNome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY EntNome')
    Left = 80
    Top = 278
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'EntNome'
      Size = 100
    end
  end
  object DsEntidade: TDataSource
    DataSet = QrEntidade
    Left = 108
    Top = 278
  end
  object DsPrazo: TDataSource
    DataSet = QrPrazo
    Left = 100
    Top = 342
  end
  object QrPrazo: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pediprzcab'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 72
    Top = 342
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrazoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
