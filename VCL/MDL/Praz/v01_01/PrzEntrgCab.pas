
unit PrzEntrgCab;
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Data.DB, mySQLDbTables, JPEG,
  ExtDlgs, ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, DmkDAC_PF,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup,
  Vcl.Grids, Vcl.DBGrids, Menus, dmkCheckGroup, dmkImage, UnDmkEnums;

type
  TFmPrzEntrgCab = class(TForm)
    PainelDados: TPanel;
    QrPrzEntrgCab: TMySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMCondicoes: TPopupMenu;
    QrPrzEntrgIts: TMySQLQuery;
    DsPrzEntrgIts: TDataSource;
    Incluinovacondio1: TMenuItem;
    Alteracondioatual1: TMenuItem;
    Excluicondioatual1: TMenuItem;
    QrPrzEntrgItsControle: TIntegerField;
    QrPrzEntrgItsDias: TIntegerField;
    QrPrzEntrgItsPercent1: TFloatField;
    QrPrzEntrgItsPercent2: TFloatField;
    PnGrades: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrPrzEntrgItsPERCENTT: TFloatField;
    QrPrzEntrgCabCodigo: TIntegerField;
    QrPrzEntrgCabNome: TWideStringField;
    QrPrzEntrgCabMaxDesco: TFloatField;
    QrPrzEntrgCabJurosMes: TFloatField;
    QrPrzEntrgCabParcelas: TIntegerField;
    QrPrzEntrgCabMedDDSimpl: TFloatField;
    QrPrzEntrgCabMedDDReal: TFloatField;
    QrPrzEntrgCabMedDDPerc1: TFloatField;
    QrPrzEntrgCabMedDDPerc2: TFloatField;
    QrPrzEntrgCabPercentT: TFloatField;
    QrPrzEntrgCabPercent1: TFloatField;
    QrPrzEntrgCabPercent2: TFloatField;
    QrPrzEntrgCabAplicacao: TSmallintField;
    PMFiliais: TPopupMenu;
    AdicionaFilial1: TMenuItem;
    RetiraFilial1: TMenuItem;
    GBCabeca: TGroupBox;
    PnCabeca_: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCondicoes: TBitBtn;
    BtItens: TBitBtn;
    BtFiliais: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    PainelE: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    CGAplicacao: TdmkCheckGroup;
    PMCadLista: TPopupMenu;
    PedidosdeAmostra1: TMenuItem;
    PedidosdeVenda1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrzEntrgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrzEntrgCabBeforeOpen(DataSet: TDataSet);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrPrzEntrgCabBeforeClose(DataSet: TDataSet);
    procedure QrPrzEntrgCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovacondio1Click(Sender: TObject);
    procedure Alteracondioatual1Click(Sender: TObject);
    procedure BtFiliaisClick(Sender: TObject);
    procedure AdicionaFilial1Click(Sender: TObject);
    procedure RetiraFilial1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PedidosdeAmostra1Click(Sender: TObject);
    procedure PedidosdeVenda1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraCadLista(Aplicacao: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrzEntrgIts(Controle: Integer);
    procedure ReopenPrzEntrgEmp(Controle: Integer);
  end;

var
  FmPrzEntrgCab: TFmPrzEntrgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck(*, PediPrzIts1, MasterSelFilial*),
  CfgCadLista;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrzEntrgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrzEntrgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrzEntrgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrzEntrgCab.DefParams;
begin
  VAR_GOTOTABELA := 'PrzEntrgCab';
  VAR_GOTOMYSQLTABLE := QrPrzEntrgCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ppc.Codigo, ppc.Nome,');
  VAR_SQLx.Add('ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,');
  VAR_SQLx.Add('ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,');
  VAR_SQLx.Add('ppc.Percent2, ppc.Aplicacao');
  VAR_SQLx.Add('FROM przentrgcab ppc');
  VAR_SQLx.Add('WHERE ppc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ppc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ppc.Nome Like :P0');
  //
end;

procedure TFmPrzEntrgCab.MostraCadLista(Aplicacao: Integer);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'przentrgcab', 120, ncGerlSeq1,
  'Prazos de entrega',
  [], False, Null(*Maximo*), [], [], False, 0, ', Aplicacao=' + Geral.FF0(Aplicacao));
end;

procedure TFmPrzEntrgCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrzEntrgCab.PedidosdeAmostra1Click(Sender: TObject);
begin
  MostraCadLista(1);
end;

procedure TFmPrzEntrgCab.PedidosdeVenda1Click(Sender: TObject);
begin
  MostraCadLista(2);
end;

procedure TFmPrzEntrgCab.PMCondicoesPopup(Sender: TObject);
begin
  Alteracondioatual1.Enabled :=
    (QrPrzEntrgCab.State <> dsInactive) and (QrPrzEntrgCab.RecordCount > 0);
end;

procedure TFmPrzEntrgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrzEntrgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrzEntrgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrzEntrgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrzEntrgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrzEntrgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrzEntrgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrzEntrgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrzEntrgCabCodigo.Value;
  Close;
end;

procedure TFmPrzEntrgCab.AdicionaFilial1Click(Sender: TObject);
(*
var
  Confirmou: Boolean;
  Empresa, Controle, Codigo: Integer;
*)
begin
(*
  Confirmou := False;
  Empresa   := 0;
  if DBCheck.CriaFm(TFmMasterSelFilial, FmMasterSelFilial, afmoNegarComAviso) then
  begin
    with FmMasterSelFilial do
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT Filial, Codigo,');
      QrFiliais.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL');
      QrFiliais.SQL.Add('FROM entidades');
      QrFiliais.SQL.Add('WHERE Codigo<-10');
      QrFiliais.SQL.Add('AND NOT (Codigo IN (');
      QrFiliais.SQL.Add('  SELECT Empresa');
      QrFiliais.SQL.Add('  FROM pediprzemp');
      QrFiliais.SQL.Add('  WHERE Codigo=' + FormatFloat('0', QrPrzEntrgCabCodigo.Value));
      QrFiliais.SQL.Add('))');
      QrFiliais.SQL.Add('ORDER BY NOMEFILIAL');
      UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
      //
      ShowModal;
      Confirmou := FSelecionou;
      Empresa   := QrFiliaisCodigo.Value;//Geral.IMV(EdFilial.Text);
      Destroy;
      //
    end;
  end;
  if Confirmou then
  begin
    Codigo   := QrPrzEntrgCabCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('pediprzemp', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pediprzemp', False, [
    'Codigo', 'Empresa'], ['Controle'], [Codigo, Empresa], [Controle], True) then
      ReopenPrzEntrgEmp(Controle);
  end;
*)
end;

procedure TFmPrzEntrgCab.Alteracondioatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPrzEntrgCab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'PrzEntrgCab');
end;

procedure TFmPrzEntrgCab.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmPrzEntrgCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Aplicacao(*, Parcelas*): Integer;
  //MaxDesco, JurosMes, PercentT, Percent1, Percent2, MedDDSimpl, MedDDReal, MedDDPerc1, MedDDPerc2: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Aplicacao      := CGAplicacao.Value;
  (*
  MaxDesco       := ;
  JurosMes       := ;
  Parcelas       := ;
  PercentT       := ;
  Percent1       := ;
  Percent2       := ;
  MedDDSimpl     := ;
  MedDDReal      := ;
  MedDDPerc1     := ;
  MedDDPerc2     := ;
  *)
  if MyObjects.FIC(Trim(Nome) = EmptyStr, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('przentrgcab', 'Codigo', '', '', tsPos, stIns, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'przentrgcab', False, [
  'Nome', 'Aplicacao'(*, 'MaxDesco',
  'JurosMes', 'Parcelas', 'PercentT',
  'Percent1', 'Percent2', 'MedDDSimpl',
  'MedDDReal', 'MedDDPerc1', 'MedDDPerc2'*)], [
  'Codigo'], [
  Nome, Aplicacao(*, MaxDesco,
  JurosMes, Parcelas, PercentT,
  Percent1, Percent2, MedDDSimpl,
  MedDDReal, MedDDPerc1, MedDDPerc2*)], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPrzEntrgCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PrzEntrgCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrzEntrgCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrzEntrgCab', 'Codigo');
end;

procedure TFmPrzEntrgCab.BtFiliaisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFiliais, BtFiliais);
end;

procedure TFmPrzEntrgCab.BtItensClick(Sender: TObject);
begin
(*
  UmyMod.FormInsUpd_Show(TFmPediPrzIts1, FmPediPrzIts1, afmoNegarComAviso,
    QrPrzEntrgIts, stIns);
*)
end;

procedure TFmPrzEntrgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  Panel4.Align      := alClient;
  PnGrades.Align    := alClient;
  CriaOForm;
end;

procedure TFmPrzEntrgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrzEntrgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrzEntrgCab.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPrzEntrgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrzEntrgCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMCadLista, SbNovo);
end;

procedure TFmPrzEntrgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrzEntrgCab.QrPrzEntrgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrzEntrgCab.QrPrzEntrgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPrzEntrgIts(0);
  ReopenPrzEntrgEmp(0);
end;

procedure TFmPrzEntrgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrzEntrgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrzEntrgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PrzEntrgCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrzEntrgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrzEntrgCab.Incluinovacondio1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrPrzEntrgCab, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'PrzEntrgCab');
  //
(*
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PrzEntrgCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdNOme);
*)
  CGAplicacao.Value := 1;
  EdNome.SetFocus;
end;

procedure TFmPrzEntrgCab.QrPrzEntrgCabBeforeClose(DataSet: TDataSet);
begin
  QrPrzEntrgIts.Close;
  //QrPrzEntrgEmp.Close;
end;

procedure TFmPrzEntrgCab.QrPrzEntrgCabBeforeOpen(DataSet: TDataSet);
begin
  QrPrzEntrgCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPrzEntrgCab.ReopenPrzEntrgIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrzEntrgIts, Dmod.MyDB, [
  'SELECT ppi.Controle, ppi.Dias,  ',
  'ppi.Percent1, ppi.Percent2, ',
  '(ppi.Percent1 + ppi.Percent2) PERCENTT ',
  'FROM przentrgits ppi ',
  'WHERE ppi.Codigo=' + Geral.FF0(QrPrzEntrgCabCodigo.Value),
  'ORDER BY Dias ',
  '']);
  //
  if Controle <> 0 then
    QrPrzEntrgIts.Locate('Controle', Controle, []);
end;

procedure TFmPrzEntrgCab.RetiraFilial1Click(Sender: TObject);
begin
(*
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrzEntrgEmp, DBGrid2,
    'pediprzemp',  ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenPrzEntrgEmp(0);
*)
end;

procedure TFmPrzEntrgCab.ReopenPrzEntrgEmp(Controle: Integer);
begin
(*
  QrPrzEntrgEmp.Close;
  QrPrzEntrgEmp.Params[0].AsInteger :=   QrPrzEntrgCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPrzEntrgEmp, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPrzEntrgEmp.Locate('Controle', Controle, []);
*)
end;


{
Componentes de DB
object GroupBox1: TGroupBox
  Left = 507
  Top = 54
  Width = 459
  Height = 75
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = ' Parcelamento: '
  TabOrder = 3
  object Label5: TLabel
    Left = 192
    Top = 20
    Width = 97
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'M'#233'dia dias - simples:'
    FocusControl = DBEdit3
  end
  object Label6: TLabel
    Left = 320
    Top = 20
    Width = 80
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'M'#233'dia dias - real:'
    FocusControl = DBEdit4
  end
  object Label4: TLabel
    Left = 15
    Top = 20
    Width = 44
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Parcelas:'
    FocusControl = DBEdit2
  end
  object Label14: TLabel
    Left = 79
    Top = 20
    Width = 77
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Percentual total:'
    FocusControl = DBEdit9
  end
  object DBEdit3: TDBEdit
    Left = 192
    Top = 39
    Width = 123
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    DataField = 'MedDDSimpl'
    DataSource = DsPediPrzCab
    TabOrder = 0
  end
  object DBEdit4: TDBEdit
    Left = 320
    Top = 39
    Width = 123
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    DataField = 'MedDDReal'
    DataSource = DsPediPrzCab
    TabOrder = 1
  end
  object DBEdit2: TDBEdit
    Left = 15
    Top = 39
    Width = 60
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    DataField = 'Parcelas'
    DataSource = DsPediPrzCab
    TabOrder = 2
  end
  object DBEdit9: TDBEdit
    Left = 79
    Top = 39
    Width = 109
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    DataField = 'PercentT'
    DataSource = DsPediPrzCab
    TabOrder = 3
  end
end
object Label12: TLabel
  Left = 714
  Top = 5
  Width = 66
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = '% juros / m'#234's:'
  FocusControl = DBEdit7
end
object DBEdit7: TDBEdit
  Left = 714
  Top = 25
  Width = 123
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  DataField = 'JurosMes'
  DataSource = DsPediPrzCab
  TabOrder = 4
end
object DBEdit8: TDBEdit
  Left = 842
  Top = 25
  Width = 123
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  DataField = 'MaxDesco'
  DataSource = DsPediPrzCab
  TabOrder = 5
end
object Label13: TLabel
  Left = 842
  Top = 5
  Width = 83
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = '% desconto m'#225'x.:'
  FocusControl = DBEdit8
end
}
////////////////////////////////////////////////////////////////////////////////
///
{
  Componentes de Edia��o
object EdJurosMes: TdmkEdit
  Left = 714
  Top = 25
  Width = 123
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Alignment = taRightJustify
  TabOrder = 3
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 6
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,000000'
  QryCampo = 'JurosMes'
  UpdCampo = 'JurosMes'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object Label15: TLabel
  Left = 714
  Top = 5
  Width = 66
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = '% juros / m'#234's:'
end
object Label16: TLabel
  Left = 842
  Top = 5
  Width = 83
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = '% desconto m'#225'x.:'
end
object EdMaxDesco: TdmkEdit
  Left = 842
  Top = 25
  Width = 123
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Alignment = taRightJustify
  TabOrder = 4
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 6
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,000000'
  QryCampo = 'MaxDesco'
  UpdCampo = 'MaxDesco'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
}

/////////////////////////////////////////////////////////////////////////////////
///
(* EMprtesa
object QrPrzEntrgEmp: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT ppe.Controle, ppe.Empresa, ent.Filial,'
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL'
    'FROM pediprzemp ppe'
    'LEFT JOIN entidades ent ON ent.Codigo=ppe.Empresa'
    'WHERE ppe.Codigo=:P0'
    'ORDER BY NOMEFILIAL'
    '')
  Left = 584
  Top = 8
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object QrPrzEntrgEmpControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrPrzEntrgEmpEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrPrzEntrgEmpFilial: TIntegerField
    FieldName = 'Filial'
  end
  object QrPrzEntrgEmpNOMEFILIAL: TWideStringField
    FieldName = 'NOMEFILIAL'
    Size = 100
  end
end
object DsPediPrzEmp: TDataSource
  DataSet = QrPrzEntrgEmp
  Left = 612
  Top = 8
end
*)
end.

