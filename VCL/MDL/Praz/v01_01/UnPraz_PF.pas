unit UnPraz_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls,
  (*DBIProcs,*) Registry, Grids, DBGrids, CheckLst,
  printers, CommCtrl, TypInfo, comobj, ShlObj, RichEdit, ShellAPI, Consts,
  ActiveX, OleCtrls, SHDocVw, UnDmkProcFunc,
  Variants, MaskUtils, RTLConsts, IniFiles, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnPraz_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormPediPrzEnt(Entidade: Integer);
    procedure MostraFormPediPrzCab1(Codigo: Integer);
    //
    procedure CadastroESelecaoDePediPrzCab1(QrPediPrzCab1: TmySQLQuery; EdPediPrzCab1:
              TdmkEditCB; CBPediPrzCab1: TdmkDBLookupCombobox);
    procedure InsereEDefinePediPrzCab(Codigo: Integer; EdCondicaoPG: TdmkEditCB;
              CBCondicaoPG: TdmkDBLookupComboBox; QrPediPrzCab: TmySQLQuery);

    end;

var
  Praz_PF: TUnPraz_PF;

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, PediPrzcab1, PediPrzEnt;

{ TUnPraz_PF }

procedure TUnPraz_PF.MostraFormPediPrzEnt(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmPediPrzEnt, FmPediPrzEnt, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
      FmPediPrzEnt.FEntidade := Entidade;
    FmPediPrzEnt.ShowModal;
    FmPediPrzEnt.Destroy;
  end;
end;

procedure TUnPraz_PF.CadastroESelecaoDePediPrzCab1(QrPediPrzCab1: TmySQLQuery;
  EdPediPrzCab1: TdmkEditCB; CBPediPrzCab1: TdmkDBLookupCombobox);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
  UMyMod.ObtemCodigoDeCodUsu(EdPediPrzCab1, CondicaoPG, '', 'Codigo', 'CodUsu');
  //
  MostraFormPediPrzCab1(CondicaoPG);
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrPediPrzCab1, Dmod.MyDB);
    if QrPediPrzCab1.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdPediPrzCab1.ValueVariant := QrPediPrzCab1.FieldByName('CodUsu').AsInteger;
      CBPediPrzCab1.KeyValue     := QrPediPrzCab1.FieldByName('CodUsu').AsInteger;
      EdPediPrzCab1.SetFocus;
    end;
  end;
end;

procedure TUnPraz_PF.InsereEDefinePediPrzCab(Codigo: Integer;
  EdCondicaoPG: TdmkEditCB; CBCondicaoPG: TdmkDBLookupComboBox;
  QrPediPrzCab: TmySQLQuery);
begin
  VAR_CADASTRO := 0;

  Praz_PF.MostraFormPediPrzCab1(EdCondicaoPG.ValueVariant);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCondicaoPG, CBCondicaoPG, QrPediPrzCab,
      VAR_CADASTRO, 'Codigo');
    EdCondicaoPG.SetFocus;
  end;
end;

procedure TUnPraz_PF.MostraFormPediPrzCab1(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPediPrzCab1.LocCod(Codigo, Codigo);
    FmPediPrzCab1.ShowModal;
    FmPediPrzCab1.Destroy;
  end;
end;

end.
