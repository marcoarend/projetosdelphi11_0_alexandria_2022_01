unit PediPrzEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCheckBox, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, Variants;

type
  TFmPediPrzEnt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnEdita: TPanel;
    Label1: TLabel;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    Panel10: TPanel;
    BtDesiste2: TBitBtn;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdPrazo: TdmkEditCB;
    CBPrazo: TdmkDBLookupComboBox;
    BtExclui: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    QrPrazos: TmySQLQuery;
    DsPrazos: TDataSource;
    QrEmpresasCodigo: TIntegerField;
    Panel5: TPanel;
    DBGEntidades: TDBGrid;
    Panel6: TPanel;
    DBGEmpresas: TDBGrid;
    DBGPrazos: TDBGrid;
    QrPrazosCodigo: TIntegerField;
    QrPrazosNomePrazo: TWideStringField;
    QrPrazosOrdem: TIntegerField;
    QrPrazosEntidade: TIntegerField;
    QrPrazosEmpresa: TIntegerField;
    BtReordena: TBitBtn;
    QrEntidadesEntNome: TWideStringField;
    QrEmpresasEntNome: TWideStringField;
    QrEntidade: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsEntidade: TDataSource;
    DsPrazo: TDataSource;
    QrPrazo: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrPrazoNome: TWideStringField;
    Label4: TLabel;
    EdOrdem: TdmkEdit;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEmpresasAfterScroll(DataSet: TDataSet);
    procedure QrEmpresasBeforeClose(DataSet: TDataSet);
    procedure BtReordenaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
  private
    { Private declarations }
    function  ValidaDados(Empresa, Entidade, Prazo: Integer): Boolean;
    procedure ReopenEntidades(Entidade: Integer);
    procedure ReopenEmpresas(Empresa: Integer);
    procedure ReopenPrazos(Prazo: Integer);
    procedure MostraEdicao(SQLType: TSQLType; Entidade, Empresa, Prazo: Integer);
  public
    { Public declarations }
    FEntidade: Integer;
  end;

  var
  FmPediPrzEnt: TFmPediPrzEnt;

implementation

uses UnMyObjects, DmkDAC_PF, UMySQLModule, Module, ModuleGeral, UnGOTOy,
  MyDBCheck, UnReordena;

{$R *.DFM}

procedure TFmPediPrzEnt.MostraEdicao(SQLType: TSQLType; Entidade, Empresa,
  Prazo: Integer);
var
  Ent, Emp: Integer;
begin
  CBEmpresa.ListSource  := DModG.DsEmpresas;
  CBEntidade.ListSource := DsEntidade;
  CBPrazo.ListSource    := DsPrazo;
  //
  if SQLType = stIns then
  begin
    if FEntidade <> 0 then
      Ent := FEntidade
    else
      Ent := QrEntidadesCodigo.Value;
    //
    Emp := QrEmpresasCodigo.Value;
    //
    if Emp = 0 then
      Emp := DmodG.QrFiliLogCodigo.Value;
    //
    EdEmpresa.ValueVariant  := Emp;
    CBEmpresa.KeyValue      := Emp;
    EdEntidade.ValueVariant := Ent;
    CBEntidade.KeyValue     := Ent;
    EdPrazo.ValueVariant    := 0;
    CBPrazo.KeyValue        := null;
    CkContinuar.Checked     := True;
    //
    QrEntidades.DisableControls;
    QrEmpresas.DisableControls;
    QrPrazos.DisableControls;
    //
    DBGEntidades.Enabled := False;
    DBGEmpresas.Enabled  := False;
    DBGPrazos.Enabled    := False;
    //
    PnEdita.Visible  := True;
    GBRodaPe.Visible := False;
    //
    EdEmpresa.SetFocus;
  end else
  begin
    QrEntidades.EnableControls;
    QrEmpresas.EnableControls;
    QrPrazos.EnableControls;
    //
    DBGEntidades.Enabled := True;
    DBGEmpresas.Enabled  := True;
    DBGPrazos.Enabled    := True;
    //
    ReopenEntidades(Entidade);
    ReopenEmpresas(Empresa);
    ReopenPrazos(Prazo);
    //
    PnEdita.Visible  := False;
    GBRodaPe.Visible := True;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPediPrzEnt.ReopenPrazos(Prazo: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrPrazos, Dmod.MyDB, [
    'SELECT ppe.Codigo, ppe.Entidade, ppe.Empresa, ',
    'prz.Nome NomePrazo, ppe.Ordem ',
    'FROM pediprzent ppe ',
    'LEFT JOIN pediprzcab prz ON prz.Codigo = ppe.Codigo ',
    'WHERE ppe.Entidade=' + Geral.FF0(QrEntidadesCodigo.Value),
    'AND ppe.Empresa=' + Geral.FF0(QrEmpresasCodigo.Value),
    'ORDER BY ppe.Ordem, NomePrazo ',
    '']);
  if Prazo <> 0 then
    QrEntidades.Locate('Codigo', Prazo, []);
end;

procedure TFmPediPrzEnt.ReopenEmpresas(Empresa: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmpresas, Dmod.MyDB, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) EntNome ',
    'FROM pediprzent ppe ',
    'LEFT JOIN entidades ent ON ent.Codigo = ppe.Empresa ',
    'WHERE ppe.Entidade=' + Geral.FF0(QrEntidadesCodigo.Value),
    'GROUP BY ent.Codigo ',
    'ORDER BY EntNome ',
    '']);
  if Empresa <> 0 then
    QrEntidades.Locate('Codigo', Empresa, []);
end;

procedure TFmPediPrzEnt.ReopenEntidades(Entidade: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) EntNome ',
    'FROM pediprzent ppe ',
    'LEFT JOIN entidades ent ON ent.Codigo = ppe.Entidade ',
    'GROUP BY ent.Codigo ',
    'ORDER BY EntNome ',
    '']);
  if Entidade <> 0 then
    QrEntidades.Locate('Codigo', Entidade, []);
end;

procedure TFmPediPrzEnt.BtConfirma2Click(Sender: TObject);
var
  Empresa, Entidade, Prazo, Ordem: Integer;
begin
  Empresa  := EdEmpresa.ValueVariant;
  Entidade := EdEntidade.ValueVariant;
  Prazo    := EdPrazo.ValueVariant;
  Ordem    := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Empresa n�o informada!') then Exit;
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Entidade n�o informada!') then Exit;
  if MyObjects.FIC(Prazo = 0, EdPrazo, 'Prazo n�o informado!') then Exit;
  if MyObjects.FIC(ValidaDados(Empresa, Entidade, Prazo) = False, nil, 'Esta configura��o j� foi adicionada!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pediprzent', False,
    ['Ordem'], ['Empresa', 'Entidade', 'Codigo'],
    [Ordem], [Empresa, Entidade, Prazo], True) then
  begin
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      //
      EdPrazo.ValueVariant := 0;
      CBPrazo.KeyValue     := null;
      EdOrdem.ValueVariant := Ordem + 1;
      //
      EdEmpresa.SetFocus;
    end else
    begin
      FEntidade := 0;
      //
      MostraEdicao(stLok, Entidade, Empresa, Prazo);
    end;
  end;
end;

procedure TFmPediPrzEnt.BtDesiste2Click(Sender: TObject);
var
  Entidade, Empresa, Prazo: Integer;
begin
  Entidade := QrPrazosEntidade.Value;
  Empresa  := QrPrazosEmpresa.Value;
  Prazo    := QrPrazosCodigo.Value;
  //
  MostraEdicao(stLok, Entidade, Empresa, Prazo);
end;

procedure TFmPediPrzEnt.BtExcluiClick(Sender: TObject);
var
  Entidade, Empresa, Prazo: Integer;
begin
  if (QrPrazos.State <> dsInactive) and (QrPrazos.RecordCount > 0) then
  begin
    Entidade := QrEntidadesCodigo.Value;
    Empresa  := QrEmpresasCodigo.Value;
    Prazo    := QrPrazosCodigo.Value;
    //
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrazos, DBGPrazos,
      'pediprzent', ['Entidade', 'Empresa', 'Codigo'],
      ['Entidade', 'Empresa', 'Codigo'], istPergunta, '');
    //
    MostraEdicao(stLok, Entidade, Empresa, Prazo);
  end;
end;

procedure TFmPediPrzEnt.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(stIns, 0, 0, 0);
end;

procedure TFmPediPrzEnt.BtReordenaClick(Sender: TObject);
var
  Entidade, Empresa, Prazo: Integer;
begin
  if (QrPrazos.State <> dsInactive) and (QrPrazos.RecordCount > 0) then
  begin
    Entidade := QrEntidadesCodigo.Value;
    Empresa  := QrEmpresasCodigo.Value;
    Prazo    := QrPrazosCodigo.Value;
    //
    UReordena.ReordenaItens(QrPrazos, Dmod.QrUpd, 'pediprzent',
      'Ordem', 'Codigo', 'NomePrazo', '', '', '', nil);
    //
    MostraEdicao(stLok, Entidade, Empresa, Prazo);
  end;
end;

procedure TFmPediPrzEnt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediPrzEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediPrzEnt.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrEntidade, DMod.MyDB);
  UMyMod.AbreQuery(QrPrazo, DMod.MyDB);
  //
  ImgTipo.SQLType := stLok;
  FEntidade       := 0;
end;

procedure TFmPediPrzEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediPrzEnt.FormShow(Sender: TObject);
begin
  MostraEdicao(stLok, FEntidade, 0, 0);
end;

procedure TFmPediPrzEnt.QrEmpresasAfterScroll(DataSet: TDataSet);
begin
  ReopenPrazos(0);
end;

procedure TFmPediPrzEnt.QrEmpresasBeforeClose(DataSet: TDataSet);
begin
  QrPrazos.Close;
end;

procedure TFmPediPrzEnt.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  ReopenEmpresas(0);
end;

procedure TFmPediPrzEnt.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrEmpresas.Close;
end;

function TFmPediPrzEnt.ValidaDados(Empresa, Entidade, Prazo: Integer): Boolean;
var
  Qry: TMySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM pediprzent',
      'WHERE Codigo=' + Geral.FF0(Prazo),
      'AND Empresa=' + Geral.FF0(Empresa),
      'AND Entidade=' + Geral.FF0(Entidade),
      '']);
    //
    Result := Qry.RecordCount = 0;
  finally
    Qry.Free;
  end;
end;

end.
