object FmPediPrzIts1: TFmPediPrzIts1
  Left = 339
  Top = 185
  Caption = 'PED-PRAZO-102 :: Edi'#231#227'o de Item de Prazo'
  ClientHeight = 501
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 157
    Width = 639
    Height = 139
    Align = alClient
    DataSource = FmPediPrzCab1.DsPediPrzIts
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Dias'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PERCENTT'
        Title.Caption = '% Percentual'
        Width = 125
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'ID'
        Visible = True
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 639
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 591
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 543
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 297
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Prazo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Prazo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 297
        Height = 32
        Caption = 'Edi'#231#227'o de Item de Prazo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBCabeca: TGroupBox
    Left = 0
    Top = 92
    Width = 639
    Height = 65
    Align = alTop
    TabOrder = 2
    object PnCabeca_: TPanel
      Left = 2
      Top = 15
      Width = 635
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 232
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label7: TLabel
        Left = 148
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Percentual:'
        FocusControl = DBEdPercent
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = FmPediPrzCab1.DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 232
        Top = 20
        Width = 397
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = FmPediPrzCab1.DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = FmPediPrzCab1.DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdPercent: TDBEdit
        Left = 148
        Top = 20
        Width = 81
        Height = 21
        DataField = 'PercentT'
        DataSource = FmPediPrzCab1.DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnChange = DBEdPercentChange
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 639
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 635
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 431
    Width = 639
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 493
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 491
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object GBEdita: TGroupBox
    Left = 0
    Top = 296
    Width = 639
    Height = 135
    Align = alBottom
    TabOrder = 5
    Visible = False
    object PnEdita_: TPanel
      Left = 2
      Top = 15
      Width = 635
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 240
        Top = 8
        Width = 49
        Height = 13
        Caption = '% parcela:'
      end
      object Label5: TLabel
        Left = 12
        Top = 8
        Width = 62
        Height = 13
        Caption = 'ID Condi'#231#227'o:'
      end
      object Label6: TLabel
        Left = 112
        Top = 8
        Width = 43
        Height = 13
        Caption = 'ID prazo:'
      end
      object Label8: TLabel
        Left = 196
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Dias:'
      end
      object EdDias: TdmkEdit
        Left = 196
        Top = 24
        Width = 40
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ValMax = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Dias'
        UpdCampo = 'Dias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPercent1: TdmkEdit
        Left = 240
        Top = 24
        Width = 113
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 10
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000000000'
        QryCampo = 'Percent1'
        UpdCampo = 'Percent1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdControle: TdmkEdit
        Left = 112
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 12
        Top = 24
        Width = 97
        Height = 21
        DataField = 'Codigo'
        DataSource = FmPediPrzCab1.DsPediPrzCab
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GroupBox1: TGroupBox
      Left = 2
      Top = 63
      Width = 635
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 489
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 487
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object QrSum1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Dias) Parcelas, '
      'SUM(Percent1 + Percent2) PercentT,'
      'SUM(Percent1) Percent1, SUM(Percent2) Percent2,'
      'SUM(Dias) / COUNT(Dias) MedDDSimpl,'
      '(SUM((Percent1 + Percent2) * Dias) / '
      '  SUM(Percent1 + Percent2)) MedDDReal,'
      'SUM(Percent1 * Dias) / SUM(Percent1) MedDDPerc1,'
      'SUM(Percent2 * Dias) / SUM(Percent2) MedDDPerc2'
      'FROM pediprzits'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1Parcelas: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
    object QrSum1PercentT: TFloatField
      FieldName = 'PercentT'
    end
    object QrSum1Percent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSum1Percent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSum1MedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrSum1MedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrSum1MedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
    end
    object QrSum1MedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
    end
  end
end
