unit PediPrzIts1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, MyDBCheck, Mask, DBCtrls, Grids,
  DBGrids, dmkDBGrid, DB, mySQLDbTables, dmkLabel, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkValUsu, dmkDBEdit, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmPediPrzIts1 = class(TForm)
    QrSum1: TmySQLQuery;
    DBGrid1: TDBGrid;
    QrSum1Parcelas: TLargeintField;
    QrSum1MedDDSimpl: TFloatField;
    QrSum1MedDDReal: TFloatField;
    QrSum1MedDDPerc1: TFloatField;
    QrSum1MedDDPerc2: TFloatField;
    QrSum1PercentT: TFloatField;
    QrSum1Percent1: TFloatField;
    QrSum1Percent2: TFloatField;
    Panel3: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCabeca: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnCabeca_: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdPercent: TDBEdit;
    GBControla: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBEdita: TGroupBox;
    PnEdita_: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    EdDias: TdmkEdit;
    EdPercent1: TdmkEdit;
    EdControle: TdmkEdit;
    dmkDBEdit1: TdmkDBEdit;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    BtConfirma: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBEdPercentChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaPercent(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmPediPrzIts1: TFmPediPrzIts1;

implementation

uses UnMyObjects, UnMySQLCuringa, Module, UnInternalConsts, UMySQLModule, PediPrzCab1;

{$R *.DFM}

procedure TFmPediPrzIts1.AtualizaPercent(Controle: Integer);
var
  Codigo: Integer;
begin
  Codigo := FmPediPrzCab1.QrPediPrzCabCodigo.Value;
  QrSum1.Close;
  QrSum1.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrSum1, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pediprzcab SET Parcelas=:P0, ');
  Dmod.QrUpd.SQL.Add('PercentT=:P1, Percent1=:P2, Percent2=:P3, ');
  Dmod.QrUpd.SQL.Add('MedDDSimpl=:P4, MedDDReal=:P5, ');
  Dmod.QrUpd.SQL.Add('MedDDPerc1=:P6, MedDDPerc2=:P7 ');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrSum1Parcelas.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSum1PercentT.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSum1Percent1.Value;
  Dmod.QrUpd.Params[03].AsFloat   := QrSum1Percent2.Value;
  Dmod.QrUpd.Params[04].AsFloat   := QrSum1MedDDSimpl.Value;
  Dmod.QrUpd.Params[05].AsFloat   := QrSum1MedDDReal.Value;
  Dmod.QrUpd.Params[06].AsFloat   := QrSum1MedDDPerc1.Value;
  Dmod.QrUpd.Params[07].AsFloat   := QrSum1MedDDPerc2.Value;
  //
  Dmod.QrUpd.Params[08].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  FmPediPrzCab1.LocCod(Codigo, Codigo);
  FmPediPrzCab1.QrPediPrzIts.Locate('Controle', Controle, []);
end;

procedure TFmPediPrzIts1.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, FmPediPrzIts1, GBEdita, FmPediPrzCab1.QrPediPrzIts,
  [GBControla], [GBEdita], EdDias, ImgTipo, 'pediprzits');
end;

procedure TFmPediPrzIts1.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('pediprzits', 'Controle', ImgTipo.SQLType,
    Geral.IMV(EdControle.Text));
  EdControle.ValueVariant := Controle;
  UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPediPrzIts1, GBEdita, 'pediprzits',
    Controle, Dmod.QrUpd, [GBEdita], [GBControla], ImgTipo, True);
  AtualizaPercent(Controle);
end;

procedure TFmPediPrzIts1.BtDesisteClick(Sender: TObject);
begin
  GBControla.Visible := True;
  GBEdita.Visible    := False;
end;

procedure TFmPediPrzIts1.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, FmPediPrzCab1.QrPediPrzIts, DBGrid1,
  'pediprzits',  ['Controle'], ['Controle'], istPergunta, '');
  AtualizaPercent(0);
end;

procedure TFmPediPrzIts1.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, FmPediPrzIts1, GBEdita, FmPediPrzCab1.QrPediPrzIts,
  [GBControla], [GBEdita], EdDias, ImgTipo, 'pediprzits');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(Percent1) + SUM(Percent2) Usado ',
  'FROM pediprzits ',
  'WHERE Codigo=' + Geral.FF0(FmPediPrzCab1.QrPediPrzCabCodigo.Value),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
    EdPercent1.ValueVariant := 100 - Dmod.QrAux.FieldByName('Usado').AsFloat;
end;

procedure TFmPediPrzIts1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediPrzIts1.DBEdPercentChange(Sender: TObject);
begin
  if FmPediPrzCab1.QrPediPrzCabPercentT.Value = 100 then
    DBEdPercent.Font.Color := clBlue else
    DBEdPercent.Font.Color := clRed;
end;

procedure TFmPediPrzIts1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediPrzIts1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (FmPediPrzCab1.QrPediPrzCabPercentT.Value <> 100) and
     (FmPediPrzCab1.QrPediPrzCabPercentT.Value <>   0) then
  begin
    Geral.MensagemBox('Antes de fechar a janela, o percentual total de ' +
    'fibras deve ser 100%', 'Aviso', MB_OK+MB_ICONWARNING);
    CanClose := False;
  end;
end;

procedure TFmPediPrzIts1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPediPrzIts1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
