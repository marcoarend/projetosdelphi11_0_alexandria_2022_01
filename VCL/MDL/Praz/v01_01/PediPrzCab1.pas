unit PediPrzCab1;
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Data.DB, mySQLDbTables, JPEG,
  ExtDlgs, ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, DmkDAC_PF,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup,
  Vcl.Grids, Vcl.DBGrids, Menus, dmkCheckGroup, dmkImage, UnDmkEnums;

type
  TFmPediPrzCab1 = class(TForm)
    PainelDados: TPanel;
    DsPediPrzCab: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMCondicoes: TPopupMenu;
    QrPediPrzIts: TmySQLQuery;
    DsPediPrzIts: TDataSource;
    Incluinovacondio1: TMenuItem;
    Alteracondioatual1: TMenuItem;
    Excluicondioatual1: TMenuItem;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    QrPediPrzItsPercent2: TFloatField;
    PnGrades: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrPediPrzItsPERCENTT: TFloatField;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabMedDDSimpl: TFloatField;
    QrPediPrzCabMedDDReal: TFloatField;
    QrPediPrzCabMedDDPerc1: TFloatField;
    QrPediPrzCabMedDDPerc2: TFloatField;
    QrPediPrzCabPercentT: TFloatField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    QrPediPrzEmp: TmySQLQuery;
    DsPediPrzEmp: TDataSource;
    QrPediPrzEmpEmpresa: TIntegerField;
    QrPediPrzEmpFilial: TIntegerField;
    QrPediPrzEmpNOMEFILIAL: TWideStringField;
    QrPediPrzEmpControle: TIntegerField;
    QrPediPrzCabAplicacao: TSmallintField;
    PMFiliais: TPopupMenu;
    AdicionaFilial1: TMenuItem;
    RetiraFilial1: TMenuItem;
    GBCabeca: TGroupBox;
    PnCabeca_: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCondicoes: TBitBtn;
    BtItens: TBitBtn;
    BtFiliais: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    PainelE: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    CGAplicacao: TdmkCheckGroup;
    EdJurosMes: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    EdMaxDesco: TdmkEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    EdCodUsu: TdmkEdit;
    Label8: TLabel;
    RGCondPg: TdmkRadioGroup;
    QrPediPrzCabCondPg: TSmallintField;
    DBRadioGroup1: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediPrzCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediPrzCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrPediPrzCabBeforeClose(DataSet: TDataSet);
    procedure QrPediPrzCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovacondio1Click(Sender: TObject);
    procedure Alteracondioatual1Click(Sender: TObject);
    procedure BtFiliaisClick(Sender: TObject);
    procedure AdicionaFilial1Click(Sender: TObject);
    procedure RetiraFilial1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediPrzIts(Controle: Integer);
    procedure ReopenPediPrzEmp(Controle: Integer);
  end;

var
  FmPediPrzCab1: TFmPediPrzCab1;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PediPrzIts1, MyDBCheck, MasterSelFilial;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPediPrzCab1.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPediPrzCab1.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediPrzCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPediPrzCab1.DefParams;
begin
  VAR_GOTOTABELA := 'PediPrzCab';
  VAR_GOTOMYSQLTABLE := QrPediPrzCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,');
  VAR_SQLx.Add('ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,');
  VAR_SQLx.Add('ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,');
  VAR_SQLx.Add('ppc.Percent2, ppc.Aplicacao, ppc.CondPg ');
  VAR_SQLx.Add('FROM pediprzcab ppc');
  VAR_SQLx.Add('WHERE ppc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND ppc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ppc.Nome Like :P0');
  //
end;

procedure TFmPediPrzCab1.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediPrzCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPediPrzCab1.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPediPrzCab1.PMCondicoesPopup(Sender: TObject);
begin
  Alteracondioatual1.Enabled :=
    (QrPediPrzCab.State <> dsInactive) and (QrPediPrzCab.RecordCount > 0);
end;

procedure TFmPediPrzCab1.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPediPrzCab1.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPediPrzCab1.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPediPrzCab1.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPediPrzCab1.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPediPrzCab1.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPediPrzCab1.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPediPrzCab1.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediPrzCabCodigo.Value;
  Close;
end;

procedure TFmPediPrzCab1.AdicionaFilial1Click(Sender: TObject);
var
  Confirmou: Boolean;
  Empresa, Controle, Codigo: Integer;
begin
  Confirmou := False;
  Empresa   := 0;
  if DBCheck.CriaFm(TFmMasterSelFilial, FmMasterSelFilial, afmoNegarComAviso) then
  begin
    with FmMasterSelFilial do
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT Filial, Codigo,');
      QrFiliais.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL');
      QrFiliais.SQL.Add('FROM entidades');
      QrFiliais.SQL.Add('WHERE Codigo<-10');
      QrFiliais.SQL.Add('AND NOT (Codigo IN (');
      QrFiliais.SQL.Add('  SELECT Empresa');
      QrFiliais.SQL.Add('  FROM pediprzemp');
      QrFiliais.SQL.Add('  WHERE Codigo=' + FormatFloat('0', QrPediPrzCabCodigo.Value));
      QrFiliais.SQL.Add('))');
      QrFiliais.SQL.Add('ORDER BY NOMEFILIAL');
      UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
      //
      ShowModal;
      Confirmou := FSelecionou;
      Empresa   := QrFiliaisCodigo.Value;//Geral.IMV(EdFilial.Text);
      Destroy;
      //
    end;
  end;
  if Confirmou then
  begin
    Codigo   := QrPediPrzCabCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('pediprzemp', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pediprzemp', False, [
    'Codigo', 'Empresa'], ['Controle'], [Codigo, Empresa], [Controle], True) then
      ReopenPediPrzEmp(Controle);
  end;
end;

procedure TFmPediPrzCab1.Alteracondioatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediPrzCab, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'pediprzcab');
end;

procedure TFmPediPrzCab1.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmPediPrzCab1.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PediPrzCab', 'Codigo', ImgTipo.SQLType,
    QrPediPrzCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPediPrzCab1, PainelEdita,
    'PediPrzCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPediPrzCab1.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediPrzCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
end;

procedure TFmPediPrzCab1.BtFiliaisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFiliais, BtFiliais);
end;

procedure TFmPediPrzCab1.BtItensClick(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmPediPrzIts1, FmPediPrzIts1, afmoNegarComAviso,
    QrPediPrzIts, stIns);
end;

procedure TFmPediPrzCab1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  Panel4.Align      := alClient;
  PnGrades.Align    := alClient;
  CriaOForm;
end;

procedure TFmPediPrzCab1.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediPrzCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPediPrzCab1.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPediPrzCab1.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPediPrzCab1.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPediPrzCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPediPrzCab1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPediPrzCab1.QrPediPrzCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPediPrzCab1.QrPediPrzCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPediPrzIts(0);
  ReopenPediPrzEmp(0);
end;

procedure TFmPediPrzCab1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediPrzCab1.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediPrzCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PediPrzCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPediPrzCab1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediPrzCab1.Incluinovacondio1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrPediPrzCab, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'pediprzcab');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'pediprzcab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  CGAplicacao.Value := 1;
  EdNome.SetFocus;
end;

procedure TFmPediPrzCab1.QrPediPrzCabBeforeClose(DataSet: TDataSet);
begin
  QrPediPrzIts.Close;
  QrPediPrzEmp.Close;
end;

procedure TFmPediPrzCab1.QrPediPrzCabBeforeOpen(DataSet: TDataSet);
begin
  QrPediPrzCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPediPrzCab1.ReopenPediPrzIts(Controle: Integer);
begin
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediPrzIts, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPediPrzIts.Locate('Controle', Controle, []);
end;

procedure TFmPediPrzCab1.RetiraFilial1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediPrzEmp, DBGrid2,
    'pediprzemp',  ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenPediPrzEmp(0);
end;

procedure TFmPediPrzCab1.ReopenPediPrzEmp(Controle: Integer);
begin
  QrPediPrzEmp.Close;
  QrPediPrzEmp.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediPrzEmp, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPediPrzEmp.Locate('Controle', Controle, []);
end;

end.

