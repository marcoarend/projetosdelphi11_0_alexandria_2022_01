object FmPrzEntrgCab: TFmPrzEntrgCab
  Left = 368
  Top = 194
  Caption = 'ENT-PRAZO-001 :: Prazos de Entregas'
  ClientHeight = 561
  ClientWidth = 975
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 106
    Width = 975
    Height = 455
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 385
      Width = 975
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 23
        Top = 21
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 832
        Top = 15
        Width = 141
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 1
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 975
      Height = 133
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object PainelE: TPanel
        Left = 2
        Top = 15
        Width = 971
        Height = 116
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 5
          Top = 5
          Width = 14
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label9: TLabel
          Left = 78
          Top = 5
          Width = 51
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 5
          Top = 25
          Width = 69
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 78
          Top = 25
          Width = 883
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CGAplicacao: TdmkCheckGroup
          Left = 5
          Top = 50
          Width = 961
          Height = 51
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Pedidos de Amostra'
            'Pedidos de venda')
          TabOrder = 2
          QryCampo = 'Aplicacao'
          UpdCampo = 'Aplicacao'
          UpdType = utYes
          Value = 1
          OldValor = 0
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 106
    Width = 975
    Height = 455
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnGrades: TPanel
      Left = 0
      Top = 234
      Width = 975
      Height = 151
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 271
        Height = 149
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        DataSource = DsPrzEntrgIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Dias'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENTT'
            Title.Caption = '% Percentual'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 272
        Top = 1
        Width = 702
        Height = 149
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Filial'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEFILIAL'
            Title.Caption = 'Nome filial'
            Width = 481
            Visible = True
          end>
      end
    end
    object GBCabeca: TGroupBox
      Left = 0
      Top = 0
      Width = 975
      Height = 159
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object PnCabeca_: TPanel
        Left = 2
        Top = 15
        Width = 971
        Height = 142
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 5
          Top = 5
          Width = 14
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 78
          Top = 5
          Width = 51
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TDBEdit
          Left = 5
          Top = 25
          Width = 69
          Height = 21
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsPrzEntrgCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 78
          Top = 25
          Width = 887
          Height = 21
          Hint = 'Nome do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsPrzEntrgCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object dmkDBCheckGroup1: TdmkDBCheckGroup
          Left = 5
          Top = 54
          Width = 960
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Aplica'#231#227'o: '
          Columns = 2
          DataField = 'Aplicacao'
          DataSource = DsPrzEntrgCab
          Items.Strings = (
            'Pedidos de Amostra'
            'Pedidos de venda')
          ParentBackground = False
          TabOrder = 2
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 385
      Width = 975
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 175
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 130
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 46
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 177
        Top = 15
        Width = 263
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 440
        Top = 15
        Width = 533
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 404
          Top = 0
          Width = 129
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 1
            Top = 5
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCondicoes: TBitBtn
          Tag = 407
          Left = 4
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Condi'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCondicoesClick
        end
        object BtItens: TBitBtn
          Tag = 10098
          Left = 126
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtFiliais: TBitBtn
          Tag = 10049
          Left = 248
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Filiais'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFiliaisClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 699
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 231
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazos de Entrega'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 231
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazos de Entrega'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 231
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prazos de Entrega'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 975
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 971
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPrzEntrgCab: TDataSource
    DataSet = QrPrzEntrgCab
    Left = 276
    Top = 104
  end
  object QrPrzEntrgCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrzEntrgCabBeforeOpen
    AfterOpen = QrPrzEntrgCabAfterOpen
    BeforeClose = QrPrzEntrgCabBeforeClose
    AfterScroll = QrPrzEntrgCabAfterScroll
    SQL.Strings = (
      'SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,'
      'ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,'
      'ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,'
      'ppc.Percent2, ppc.Aplicacao'
      'FROM pediprzcab ppc')
    Left = 276
    Top = 56
    object QrPrzEntrgCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrzEntrgCabNome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object QrPrzEntrgCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabJurosMes: TFloatField
      FieldName = 'JurosMes'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabParcelas: TIntegerField
      FieldName = 'Parcelas'
      Required = True
      DisplayFormat = '0'
    end
    object QrPrzEntrgCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabMedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabMedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrzEntrgCabPercentT: TFloatField
      FieldName = 'PercentT'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPrzEntrgCabPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPrzEntrgCabPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPrzEntrgCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object PMCondicoes: TPopupMenu
    OnPopup = PMCondicoesPopup
    Left = 316
    Top = 388
    object Incluinovacondio1: TMenuItem
      Caption = '&Inclui nova condi'#231#227'o'
      OnClick = Incluinovacondio1Click
    end
    object Alteracondioatual1: TMenuItem
      Caption = '&Altera condi'#231#227'o atual'
      OnClick = Alteracondioatual1Click
    end
    object Excluicondioatual1: TMenuItem
      Caption = '&Exclui condi'#231#227'o atual'
      Enabled = False
    end
  end
  object QrPrzEntrgIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Controle, ppi.Dias,  '
      'ppi.Percent1, ppi.Percent2, '
      'ppi.Percent1 + ppi.Percent2 PERCENTT '
      'FROM przentrgits ppi '
      'WHERE ppi.Codigo>0 '
      'ORDER BY Dias ')
    Left = 360
    Top = 60
    object QrPrzEntrgItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrzEntrgItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPrzEntrgItsPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPrzEntrgItsPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPrzEntrgItsPERCENTT: TFloatField
      FieldName = 'PERCENTT'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
  end
  object DsPrzEntrgIts: TDataSource
    DataSet = QrPrzEntrgIts
    Left = 360
    Top = 108
  end
  object PMFiliais: TPopupMenu
    Left = 544
    Top = 392
    object AdicionaFilial1: TMenuItem
      Caption = '&Adiciona Filial'
      OnClick = AdicionaFilial1Click
    end
    object RetiraFilial1: TMenuItem
      Caption = '&Retira Filial'
      OnClick = RetiraFilial1Click
    end
  end
  object PMCadLista: TPopupMenu
    Left = 368
    Top = 298
    object PedidosdeAmostra1: TMenuItem
      Caption = 'Pedidos de Amostra'
      OnClick = PedidosdeAmostra1Click
    end
    object PedidosdeVenda1: TMenuItem
      Caption = 'Pedidos de Venda'
      OnClick = PedidosdeVenda1Click
    end
  end
end
