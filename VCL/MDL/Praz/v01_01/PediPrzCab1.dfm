object FmPediPrzCab1: TFmPediPrzCab1
  Left = 368
  Top = 194
  Caption = 'PED-PRAZO-101 :: Prazos de Pagamentos'
  ClientHeight = 501
  ClientWidth = 780
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 95
    Width = 780
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnGrades: TPanel
      Left = 0
      Top = 215
      Width = 780
      Height = 121
      Align = alBottom
      TabOrder = 0
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 176
        Height = 119
        Align = alLeft
        DataSource = DsPediPrzIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -9
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Dias'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENTT'
            Title.Caption = '% Percentual'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 51
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 177
        Top = 1
        Width = 602
        Height = 119
        Align = alClient
        DataSource = DsPediPrzEmp
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -9
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Filial'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEFILIAL'
            Title.Caption = 'Nome filial'
            Width = 385
            Visible = True
          end>
      end
    end
    object GBCabeca: TGroupBox
      Left = 0
      Top = 0
      Width = 780
      Height = 165
      Align = alTop
      TabOrder = 1
      object PnCabeca_: TPanel
        Left = 2
        Top = 15
        Width = 776
        Height = 148
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 146
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 63
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label12: TLabel
          Left = 574
          Top = 7
          Width = 66
          Height = 13
          Caption = '% juros / m'#234's:'
          FocusControl = DBEdit7
        end
        object Label13: TLabel
          Left = 677
          Top = 7
          Width = 83
          Height = 13
          Caption = '% desconto m'#225'x.:'
          FocusControl = DBEdit8
        end
        object DBEdCodigo: TDBEdit
          Left = 4
          Top = 20
          Width = 55
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsPediPrzCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 146
          Top = 20
          Width = 422
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsPediPrzCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 63
          Top = 20
          Width = 79
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsPediPrzCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object dmkDBCheckGroup1: TdmkDBCheckGroup
          Left = 4
          Top = 43
          Width = 398
          Height = 60
          Caption = ' Aplica'#231#227'o: '
          Columns = 2
          DataField = 'Aplicacao'
          DataSource = DsPediPrzCab
          Items.Strings = (
            'Pedidos de venda'
            'Pedidos de compra'
            'Venda balc'#227'o'
            'Frete')
          ParentBackground = False
          TabOrder = 3
        end
        object GroupBox1: TGroupBox
          Left = 406
          Top = 43
          Width = 367
          Height = 60
          Caption = ' Parcelamento: '
          TabOrder = 4
          object Label5: TLabel
            Left = 154
            Top = 16
            Width = 97
            Height = 13
            Caption = 'M'#233'dia dias - simples:'
            FocusControl = DBEdit3
          end
          object Label6: TLabel
            Left = 256
            Top = 16
            Width = 80
            Height = 13
            Caption = 'M'#233'dia dias - real:'
            FocusControl = DBEdit4
          end
          object Label4: TLabel
            Left = 12
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Parcelas:'
            FocusControl = DBEdit2
          end
          object Label14: TLabel
            Left = 63
            Top = 16
            Width = 77
            Height = 13
            Caption = 'Percentual total:'
            FocusControl = DBEdit9
          end
          object DBEdit3: TDBEdit
            Left = 154
            Top = 31
            Width = 98
            Height = 21
            DataField = 'MedDDSimpl'
            DataSource = DsPediPrzCab
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 256
            Top = 31
            Width = 98
            Height = 21
            DataField = 'MedDDReal'
            DataSource = DsPediPrzCab
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 12
            Top = 31
            Width = 48
            Height = 21
            DataField = 'Parcelas'
            DataSource = DsPediPrzCab
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 63
            Top = 31
            Width = 87
            Height = 21
            DataField = 'PercentT'
            DataSource = DsPediPrzCab
            TabOrder = 3
          end
        end
        object DBEdit7: TDBEdit
          Left = 574
          Top = 23
          Width = 99
          Height = 21
          DataField = 'JurosMes'
          DataSource = DsPediPrzCab
          TabOrder = 5
        end
        object DBEdit8: TDBEdit
          Left = 677
          Top = 23
          Width = 98
          Height = 21
          DataField = 'MaxDesco'
          DataSource = DsPediPrzCab
          TabOrder = 6
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 4
          Top = 104
          Width = 769
          Height = 42
          Caption = ' Condi'#231#227'o de pagamento: '
          Columns = 5
          DataField = 'CondPg'
          DataSource = DsPediPrzCab
          Items.Strings = (
            'Indefinido'
            'Adiantado'
            #193' vista'
            #193' Prazo'
            'Outros')
          TabOrder = 7
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 336
      Width = 780
      Height = 70
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 167
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 124
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 84
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 44
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 169
        Top = 15
        Width = 159
        Height = 53
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 328
        Top = 15
        Width = 450
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 347
          Top = 0
          Width = 103
          Height = 53
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 1
            Top = 4
            Width = 96
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCondicoes: TBitBtn
          Tag = 407
          Left = 3
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Condi'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCondicoesClick
        end
        object BtItens: TBitBtn
          Tag = 10098
          Left = 101
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtFiliais: TBitBtn
          Tag = 10049
          Left = 198
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Filiais'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFiliaisClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 95
    Width = 780
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 228
    ExplicitTop = 183
    object GBConfirma: TGroupBox
      Left = 0
      Top = 336
      Width = 780
      Height = 70
      Align = alBottom
      TabOrder = 0
      ExplicitLeft = 60
      ExplicitTop = 328
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 18
        Top = 17
        Width = 96
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 666
        Top = 15
        Width = 112
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 1
          Top = 2
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 780
      Height = 153
      Align = alTop
      TabOrder = 1
      object PainelE: TPanel
        Left = 2
        Top = 15
        Width = 776
        Height = 136
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label9: TLabel
          Left = 146
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label15: TLabel
          Left = 571
          Top = 4
          Width = 66
          Height = 13
          Caption = '% juros / m'#234's:'
        end
        object Label16: TLabel
          Left = 674
          Top = 4
          Width = 83
          Height = 13
          Caption = '% desconto m'#225'x.:'
        end
        object Label8: TLabel
          Left = 62
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object EdCodigo: TdmkEdit
          Left = 4
          Top = 20
          Width = 55
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 146
          Top = 20
          Width = 421
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CGAplicacao: TdmkCheckGroup
          Left = 4
          Top = 44
          Width = 769
          Height = 41
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Pedidos de venda'
            'Pedidos de compra'
            'Venda balc'#227'o'
            'Frete')
          TabOrder = 3
          QryCampo = 'Aplicacao'
          UpdCampo = 'Aplicacao'
          UpdType = utYes
          Value = 1
          OldValor = 0
        end
        object EdJurosMes: TdmkEdit
          Left = 571
          Top = 20
          Width = 99
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'JurosMes'
          UpdCampo = 'JurosMes'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMaxDesco: TdmkEdit
          Left = 674
          Top = 20
          Width = 98
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'MaxDesco'
          UpdCampo = 'MaxDesco'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 63
          Top = 20
          Width = 79
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object RGCondPg: TdmkRadioGroup
          Left = 4
          Top = 87
          Width = 769
          Height = 42
          Caption = ' Condi'#231#227'o de pagamento: '
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Adiantado'
            #193' vista'
            #193' Prazo'
            'Outros')
          TabOrder = 6
          QryCampo = 'CondPg'
          UpdCampo = 'CondPg'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 733
      Top = 0
      Width = 47
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 209
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 44
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 84
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 124
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 164
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 209
      Top = 0
      Width = 524
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 290
        Height = 32
        Caption = 'Prazos de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 290
        Height = 32
        Caption = 'Prazos de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 290
        Height = 32
        Caption = 'Prazos de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 780
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 40
    Top = 12
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPediPrzCabBeforeOpen
    AfterOpen = QrPediPrzCabAfterOpen
    BeforeClose = QrPediPrzCabBeforeClose
    AfterScroll = QrPediPrzCabAfterScroll
    SQL.Strings = (
      'SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,'
      'ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,'
      'ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,'
      'ppc.Percent2, ppc.Aplicacao'
      'FROM pediprzcab ppc')
    Left = 12
    Top = 12
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
      Required = True
      DisplayFormat = '0'
    end
    object QrPediPrzCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabPercentT: TFloatField
      FieldName = 'PercentT'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Required = True
    end
    object QrPediPrzCabCondPg: TSmallintField
      FieldName = 'CondPg'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object PMCondicoes: TPopupMenu
    OnPopup = PMCondicoesPopup
    Left = 356
    Top = 264
    object Incluinovacondio1: TMenuItem
      Caption = '&Inclui nova condi'#231#227'o'
      OnClick = Incluinovacondio1Click
    end
    object Alteracondioatual1: TMenuItem
      Caption = '&Altera condi'#231#227'o atual'
      OnClick = Alteracondioatual1Click
    end
    object Excluicondioatual1: TMenuItem
      Caption = '&Exclui condi'#231#227'o atual'
      Enabled = False
    end
  end
  object QrPediPrzIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Controle, ppi.Dias, '
      'ppi.Percent1, ppi.Percent2,'
      'ppi.Percent1 + ppi.Percent2 PERCENTT'
      'FROM pediprzits ppi'
      'WHERE ppi.Codigo=:P0'
      'ORDER BY Dias')
    Left = 524
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPediPrzItsPercent1: TFloatField
      DisplayWidth = 15
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.0000000000;-0.0000000000; '
    end
    object QrPediPrzItsPercent2: TFloatField
      DisplayWidth = 15
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.0000000000;-0.0000000000; '
    end
    object QrPediPrzItsPERCENTT: TFloatField
      FieldName = 'PERCENTT'
      Required = True
      DisplayFormat = '0.0000000000;-0.0000000000; '
      Precision = 10
    end
  end
  object DsPediPrzIts: TDataSource
    DataSet = QrPediPrzIts
    Left = 552
    Top = 8
  end
  object QrPediPrzEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppe.Controle, ppe.Empresa, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL'
      'FROM pediprzemp ppe'
      'LEFT JOIN entidades ent ON ent.Codigo=ppe.Empresa'
      'WHERE ppe.Codigo=:P0'
      'ORDER BY NOMEFILIAL'
      '')
    Left = 584
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzEmpControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzEmpEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediPrzEmpFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediPrzEmpNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
  end
  object DsPediPrzEmp: TDataSource
    DataSet = QrPediPrzEmp
    Left = 612
    Top = 8
  end
  object PMFiliais: TPopupMenu
    Left = 576
    Top = 264
    object AdicionaFilial1: TMenuItem
      Caption = '&Adiciona Filial'
      OnClick = AdicionaFilial1Click
    end
    object RetiraFilial1: TMenuItem
      Caption = '&Retira Filial'
      OnClick = RetiraFilial1Click
    end
  end
end
