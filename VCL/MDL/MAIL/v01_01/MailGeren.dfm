object FmMailGeren: TFmMailGeren
  Left = 339
  Top = 185
  Caption = 'MAI-ENVIA-001 :: Gerenciamento de E-mails'
  ClientHeight = 898
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOpcoesClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de E-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de E-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de E-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 713
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 713
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 713
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 310
          Top = 18
          Width = 12
          Height = 693
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitHeight = 688
        end
        object Panel10: TPanel
          Left = 322
          Top = 18
          Width = 917
          Height = 693
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 554
            Top = 55
            Width = 12
            Height = 638
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ExplicitTop = 64
            ExplicitHeight = 624
          end
          object TBEmails: TToolBar
            Left = 0
            Top = 0
            Width = 917
            Height = 55
            AutoSize = True
            ButtonHeight = 55
            ButtonWidth = 99
            Caption = 'ToolBar1'
            Images = ImageList1
            ShowCaptions = True
            TabOrder = 0
            object TBVerificaArq: TToolButton
              Left = 0
              Top = 0
              AutoSize = True
              Caption = 'Verificar'
              ImageIndex = 3
              OnClick = TBVerificaArqClick
            end
            object ToolButton8: TToolButton
              Left = 60
              Top = 0
              Width = 10
              Caption = 'ToolButton8'
              ImageIndex = 10
              Style = tbsSeparator
            end
            object TBNovo: TToolButton
              Left = 70
              Top = 0
              AutoSize = True
              Caption = 'Novo'
              ImageIndex = 6
              OnClick = TBNovoClick
            end
            object TBResponder: TToolButton
              Left = 114
              Top = 0
              AutoSize = True
              Caption = 'Responder'
              ImageIndex = 15
              OnClick = TBResponderClick
            end
            object ToolButton9: TToolButton
              Left = 193
              Top = 0
              Width = 10
              Caption = 'ToolButton9'
              ImageIndex = 10
              Style = tbsSeparator
            end
            object TBExcluiMail: TToolButton
              Left = 203
              Top = 0
              AutoSize = True
              Caption = 'Excluir'
              ImageIndex = 9
              OnClick = TBExcluiMailClick
            end
            object TBLido: TToolButton
              Left = 253
              Top = 0
              AutoSize = True
              Caption = 'Lido'
              ImageIndex = 13
              OnClick = TBLidoClick
            end
            object ToolButton3: TToolButton
              Left = 292
              Top = 0
              Width = 8
              Caption = 'ToolButton3'
              ImageIndex = 5
              Style = tbsSeparator
            end
            object TBNLido: TToolButton
              Left = 300
              Top = 0
              AutoSize = True
              Caption = 'N'#227'o lido'
              ImageIndex = 10
              OnClick = TBNLidoClick
            end
            object ToolButton14: TToolButton
              Left = 362
              Top = 0
              Width = 8
              Caption = 'ToolButton14'
              ImageIndex = 10
              Style = tbsSeparator
            end
            object TBSinalizado: TToolButton
              Left = 370
              Top = 0
              AutoSize = True
              Caption = 'Sinalizada'
              ImageIndex = 11
              OnClick = TBSinalizadoClick
            end
            object TBNSinalizado: TToolButton
              Left = 444
              Top = 0
              AutoSize = True
              Caption = 'N'#227'o Sinalizada'
              ImageIndex = 12
              OnClick = TBNSinalizadoClick
            end
            object ToolButton19: TToolButton
              Left = 547
              Top = 0
              Width = 10
              Caption = 'ToolButton19'
              ImageIndex = 4
              Style = tbsSeparator
            end
            object TBPesqMail: TToolButton
              Left = 557
              Top = 0
              AutoSize = True
              Caption = 'Pesquisa'
              ImageIndex = 2
              OnClick = TBPesqMailClick
            end
            object ToolButton11: TToolButton
              Left = 625
              Top = 0
              Width = 8
              Caption = 'ToolButton11'
              ImageIndex = 5
              Style = tbsSeparator
            end
            object TBMais: TToolButton
              Left = 633
              Top = 0
              AutoSize = True
              Caption = 'Mais'
              ImageIndex = 4
              OnClick = TBMaisClick
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 55
            Width = 554
            Height = 638
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = 'Panel8'
            TabOrder = 1
            object STNomeDirSel: TStaticText
              Left = 1
              Top = 1
              Width = 28
              Height = 37
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -27
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object LVEmails: TListView
              Left = 1
              Top = 93
              Width = 552
              Height = 544
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Caption = 'ID'
                  Width = 0
                end
                item
                  Caption = 'UID'
                  Width = 0
                end
                item
                  Caption = 'De'
                  Width = 185
                end
                item
                  AutoSize = True
                  Caption = 'Assunto'
                end
                item
                  Caption = 'Lido'
                  Width = 43
                end
                item
                  Caption = 'Sinalizado'
                  Width = 43
                end
                item
                  Caption = 'Lido'
                  Width = 0
                end
                item
                  Caption = 'Sinalizado'
                  Width = 0
                end
                item
                  Caption = 'Anexos'
                  Width = 43
                end
                item
                  Caption = 'Anexos'
                  Width = 0
                end>
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MultiSelect = True
              RowSelect = True
              ParentFont = False
              SmallImages = ImageList1
              TabOrder = 1
              ViewStyle = vsReport
              OnCustomDrawItem = LVEmailsCustomDrawItem
              OnSelectItem = LVEmailsSelectItem
            end
            object Panel7: TPanel
              Left = 1
              Top = 38
              Width = 552
              Height = 55
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Padding.Left = 6
              Padding.Top = 6
              Padding.Right = 6
              Padding.Bottom = 6
              TabOrder = 2
            end
          end
          object PcMsgs: TPageControl
            Left = 566
            Top = 55
            Width = 351
            Height = 638
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TsAnexos
            Align = alClient
            TabHeight = 25
            TabOrder = 2
            OnChange = PcMsgsChange
            object TSHtml: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'HTML'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object WebBrowser1: TWebBrowser
                Left = 0
                Top = 0
                Width = 343
                Height = 603
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 0
                ExplicitWidth = 345
                ExplicitHeight = 590
                ControlData = {
                  4C0000005C1C0000DC3100000000000000000000000000000000000000000000
                  000000004C000000000000000000000001000000E0D057007335CF11AE690800
                  2B2E126208000000000000004C0000000114020000000000C000000000000046
                  8000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000100000000000000000000000000000000000000}
              end
            end
            object TsPlano: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Texto plano'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object MeTextoPlano: TMemo
                Left = 0
                Top = 0
                Width = 343
                Height = 603
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Lines.Strings = (
                  'Memo1')
                ReadOnly = True
                TabOrder = 0
              end
            end
            object TsAnexos: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Anexos'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 343
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                ParentBackground = False
                TabOrder = 0
                object BitBtn1: TBitBtn
                  Tag = 10124
                  Left = 4
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BitBtn1Click
                end
              end
              object LVAnexos: TListView
                Left = 0
                Top = 59
                Width = 343
                Height = 544
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <>
                SmallImages = ImageList1
                TabOrder = 1
                ViewStyle = vsSmallIcon
              end
            end
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 308
          Height = 693
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 93
            Width = 308
            Height = 600
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DragKind = dkDock
            TabOrder = 0
            ExplicitTop = 90
            ExplicitHeight = 603
            object ListBox1: TListBox
              Left = 1
              Top = 1
              Width = 306
              Height = 173
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 1
              Visible = False
            end
            object TreeView1: TTreeView
              Left = 1
              Top = 174
              Width = 306
              Height = 425
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = []
              Indent = 35
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              OnChange = TreeView1Change
              ExplicitHeight = 428
            end
          end
          object PnEmailGer: TPanel
            Left = 0
            Top = 0
            Width = 308
            Height = 38
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Padding.Left = 6
            Padding.Top = 6
            Padding.Right = 6
            Padding.Bottom = 6
            TabOrder = 1
            object SBEmailGer: TSpeedButton
              Left = 265
              Top = 7
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SBEmailGerClick
            end
            object CBEmailGer: TdmkDBLookupComboBox
              Left = 76
              Top = 6
              Width = 185
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsEmailGer
              TabOrder = 1
              dmkEditCB = EdEmailGer
              QryCampo = 'Cliente'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdEmailGer: TdmkEditCB
              Left = 6
              Top = 6
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Cliente'
              UpdCampo = 'Cliente'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmailGerChange
              DBLookupComboBox = CBEmailGer
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object TBDiretorios: TToolBar
            Left = 0
            Top = 38
            Width = 308
            Height = 55
            AutoSize = True
            ButtonHeight = 55
            ButtonWidth = 68
            Caption = 'TBDiretorios'
            Images = ImageList1
            ShowCaptions = True
            TabOrder = 2
            object TBPastas: TToolButton
              Left = 0
              Top = 0
              AutoSize = True
              Caption = 'Diret'#243'rios'
              DropdownMenu = PMPastas
              Grouped = True
              ImageIndex = 0
            end
            object ToolButton20: TToolButton
              Left = 69
              Top = 0
              Width = 8
              Caption = 'ToolButton20'
              ImageIndex = 8
              Style = tbsSeparator
            end
            object TBPesqDir: TToolButton
              Left = 77
              Top = 0
              AutoSize = True
              Caption = 'Pesquisar'
              ImageIndex = 2
              OnClick = TBPesqDirClick
            end
            object ToolButton17: TToolButton
              Left = 149
              Top = 0
              Width = 8
              Caption = 'ToolButton17'
              ImageIndex = 8
              Style = tbsSeparator
            end
            object TBOn: TToolButton
              Left = 157
              Top = 0
              AutoSize = True
              Caption = 'Conectar'
              ImageIndex = 8
              OnClick = TBOnClick
            end
            object TBOff: TToolButton
              Left = 222
              Top = 0
              AutoSize = True
              Caption = 'Off line'
              ImageIndex = 7
              OnClick = TBOffClick
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 812
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 10
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 12
        Width = 1060
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 18
          Width = 1056
          Height = 34
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 16
            Top = 2
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 15
            Top = 1
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 772
    Width = 1241
    Height = 19
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Panels = <
      item
        Text = 'Status:'
        Width = 150
      end
      item
        Text = #218'ltimo sincronismo em:'
        Width = 320
      end
      item
        Text = 'Verificando mensagens. Aguarde!'
        Width = 200
      end
      item
        Text = 'Progresso:'
        Width = 70
      end
      item
        Style = psOwnerDraw
        Width = 50
      end>
    OnDrawPanel = StatusBar1DrawPanel
  end
  object PB1: TProgressBar
    Left = 0
    Top = 791
    Width = 1241
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 4
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 348
    Top = 248
  end
  object QrEmailGer: TMySQLQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 356
    object QrEmailGerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailGerNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEmailGerDataHoraSinc: TDateTimeField
      FieldName = 'DataHoraSinc'
    end
    object QrEmailGerDownDias: TIntegerField
      FieldName = 'DownDias'
    end
    object QrEmailGerMailBoxSeparator: TWideStringField
      FieldName = 'MailBoxSeparator'
      Size = 3
    end
  end
  object DsEmailGer: TDataSource
    DataSet = QrEmailGer
    Left = 320
    Top = 356
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 676
    Top = 308
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':110'
    MaxLineAction = maException
    Port = 110
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 496
    Top = 568
  end
  object Msg: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    ContentType = 'text/plain'
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 525
    Top = 568
  end
  object QrEmailGerDi: TMySQLQuery
    Database = Dmod.MyDB
    Left = 76
    Top = 436
  end
  object ImageList1: TImageList
    BkColor = clFuchsia
    DrawingStyle = dsTransparent
    Height = 32
    Masked = False
    Width = 32
    Left = 553
    Top = 568
    Bitmap = {
      494C0101120014015C0220002000FF00FF00FF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000080000000A000000001002000000000000040
      010000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004F60C8000029D6000029
      DE000029DE000029D6000029D6000029DE000029DE000029D6000029D6000029
      DE000029DE000029D6004F60C800FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF004F60BF003152E700738CFF00738C
      FF007384FF006B8CFF00738CFF006B8CFF006B8CFF00738CFF006B8CFF007384
      FF00738CFF00738CFF003152E7004F60BF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF005554CA003152E700738CFF00315AFF000839
      FF000839FF000839FF000839FF000839FF000839FF000839FF000839FF000839
      FF000839FF00315AFF00738CFF003152E7005554CA00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF005554CA003152E700738CFF00395AFF000839FF001039
      FF000839FF001039FF000839FF001039FF001039FF000839FF001039FF000839
      FF001039FF000839FF00395AFF00738CFF003152E7005554CA00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005554CA003152E700738CFF00395AFF001042FF001042FF001042
      FF001039FF001039FF001042FF001042FF001042FF001042FF001039FF001039
      FF001042FF001042FF001042FF00395AFF00738CFF003152E7005554CA00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00848484008484
      840084848400848484008483850098989800FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF005554CA003152E7007394FF004263FF001842FF001842FF001842FF001842
      FF001842FF001842FF001842FF001842FF001842FF001842FF001842FF001842
      FF001842FF001842FF001842FF001842FF004263FF007394FF003152E7005554
      CA00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00848484008E8C8C00FF00
      FF00FF00FF00FF00FF00FF00FF008484840095929400FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF005554
      CA00395AE7007B94FF004A63FF00184AFF00184AFF00184AFF00184AFF00184A
      FF00184AFF00184AFF00184AFF00184AFF00184AFF00184AFF00184AFF00184A
      FF00184AFF00184AFF00184AFF00184AFF00184AFF004A63FF007B94FF00395A
      E7005554CA00FF00FF00FF00FF00FF00FF008B8B8B0085858500848484008484
      8400838383008484840084848400848484008484840083838300848484008484
      84008483850085838300FF00FF00FF00FF00FF00FF0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008683850095929400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF004F60C800395A
      E700849CFF004A6BFF00214AFF002152FF00214AFF00214AFF00214AFF00214A
      FF00214AFF00214AFF00214AFF00214AFF00214AFF00214AFF00214AFF00214A
      FF00214AFF00214AFF00214AFF00214AFF002152FF00214AFF004A6BFF00849C
      FF00395AE7004F60C800FF00FF00FF00FF008C8C8C0085858500848484008484
      8400848385008484840084838500848484008284840083838300848484008484
      84008284840084848400FF00FF00FF00FF00FF00FF0093939300FF00FF00FF00
      FF008C8C8C008684840084848400BEBEBE00FF00FF00848484008C8C8C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF006465D800425AE7008CA5
      FF005273FF002952FF002952FF002952FF002952FF002952FF002952FF002952
      FF002952FF002952FF002952FF002952FF002952FF002952FF002952FF002952
      FF002952FF002952FF002952FF002952FF002952FF002952FF002952FF005273
      FF008CA5FF00425AE7006465D800FF00FF008C8C8C00848484008C8C8C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0096949400FF00FF00ADAC
      AE0084848400CCCCCC009999990084848400B5B5B500FF00FF00848484008A8C
      8C00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D6009CADFF006384
      FF003963FF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF00315A
      FF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF00315A
      FF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF003963
      FF006384FF009CADFF000029D600FF00FF008A8C8C0086838500FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084848400FF00FF00ADAC
      AE0084838500B5B5B500FF00FF00A2A2A20086848400B5B5B500FF00FF008684
      84008C8C8C00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D6009CB5FF004A73
      FF004263FF003163FF003963FF003963FF003963FF003963FF003963FF003963
      FF003963FF003963FF003963FF003963FF003963FF003963FF003963FF003963
      FF003963FF003963FF003963FF003963FF003963FF003963FF003163FF004263
      FF004A73FF009CB5FF000029D600FF00FF008C8C8C0082848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00848484008C8C8C00FF00
      FF009A9A9A0084838500BEBEBE00FF00FF00A1A0A20084848400B5B5B500FF00
      FF00848484008E8C8C00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000821DE009CB5FF003963
      FF003963FF003963FF003963FF003963FF003963FF003963FF003963FF003963
      FF003963FF003963FF003963FF003963FF003963FF003963FF003963FF003963
      FF003963FF003963FF003963FF003963FF003963FF003963FF003963FF003963
      FF003963FF009CB5FF000F20DF00FF00FF008D8B8B0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00848484008C8C
      8C00FF00FF009999990086838500BEBEBE00FF00FF00A2A1A30084848400B5B4
      B600FF00FF00848484008E8C8C00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D600A5B5FF00527B
      FF004A73FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A73
      FF00527BFF00A5B5FF000029D600FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008484840084848400848484008484
      84008483850084848400848484008C8C8C00FF00FF00FF00FF00FF00FF008483
      85008C8C8C00FF00FF009999990082848400BEBEBE00FF00FF00A1A3A3008484
      8400B3B5B500FF00FF0086848400FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000021DE00A5B5FF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6B
      FF005273FF00A5B5FF000021DE00FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0083828400868484008484
      8400848484008484840084848400848484008C8B8D00FF00FF00FF00FF00FF00
      FF00848484008C8C8C00FF00FF009897990085858500BEBEBE00FF00FF00A2A2
      A20084848400B5B5B500FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029DE00ADBDFF006B84
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7B
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7B
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7B
      FF006B84FF00ADBDFF000029DE00FF00FF008C8C8C0086848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00838383008484
      8400848385008383830084848400848484008484840094949400FF00FF00FF00
      FF00FF00FF00838383008C8C8C00FF00FF00999A980084848400C5C5C500FF00
      FF009B99990082848500ADACAE00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000021DE00ADBDFF006B84
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005273FF005A7B
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7B
      FF005A7BFF005273FF005273FF005273FF005273FF005A73FF005A7BFF005A7B
      FF006B84FF00ADBDFF000021DE00FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A4A1A3008484
      8400A4A1A300FF00FF008484840084848400848484008484840093929400FF00
      FF00FF00FF00FF00FF00848484008C8B8D00FF00FF0099999900FF00FF00FF00
      FF00FF00FF009494940084848400FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D600B5C6FF007394
      FF006B8CFF006384FF006384FF006384FF006B84FF006B84FF006B84FF006B84
      FF006B84FF006B84FF006B84FF006B84FF006B84FF006B84FF006B84FF006B84
      FF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF005A7BFF006B8C
      FF007394FF00B5C6FF000029D600FF00FF008C8C8C0084838500FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00848484008484840084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00868484008C8C8C00FF00FF00FF00FF00FF00
      FF00FF00FF00BDBDBD0084848400FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000021DE00BDCEFF00738C
      FF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8C
      FF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8C
      FF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8C
      FF00738CFF00BDCEFF000021DE00FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF008B8B8B008484840085828400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00868385008C8C8C00FF00FF00FF00
      FF00FF00FF00A1A1A10086848400FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D600BDCEFF008CA5
      FF007B9CFF007B94FF007394FF007394FF007394FF007394FF007394FF007394
      FF007394FF007394FF007394FF007394FF007394FF007394FF007394FF007394
      FF007394FF007394FF007394FF007394FF007394FF007394FF007B94FF007B9C
      FF008CA5FF00BDCEFF000029D600FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF008683850084848400FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0084848400FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008684840084848400ADAC
      AE009999990084848400FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000021DE00BDCEFF008C9C
      FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94
      FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94
      FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF007B94FF008494
      FF008C9CFF00BDCEFF000021DE00FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00848484008485830084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADACAE008684
      8400ADACAE00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF000029D600CED6FF00B5C6
      FF009CADFF0094A5FF008CA5FF00849CFF00849CFF00849CFF00849CFF00849C
      FF00849CFF00849CFF00849CFF00849CFF00849CFF00849CFF00849CFF00849C
      FF00849CFF00849CFF00849CFF00849CFF00849CFF008CA5FF0094A5FF009CAD
      FF00B5C6FF00CED6FF000029D600FF00FF008C8C8C0084858300FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00838284008484840084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF007473E9005273E700CED6
      FF00B5C6FF009CB5FF0094ADFF008CADFF008CA5FF008CA5FF008CA5FF008CA5
      FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5
      FF008CA5FF008CA5FF008CA5FF008CA5FF008CADFF0094ADFF009CB5FF00B5C6
      FF00CED6FF005273E7007473E900FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00AEABAD0094939500FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF006F80E8005273
      E700D6D6FF00BDCEFF00A5B5FF009CB5FF0094ADFF0094ADFF0094ADFF0094AD
      FF0094ADFF0094A5FF0094ADFF0094ADFF0094ADFF0094ADFF0094A5FF0094AD
      FF0094ADFF0094ADFF0094ADFF0094ADFF009CB5FF00A5B5FF00BDCEFF00D6D6
      FF005273E7006F80E800FF00FF00FF00FF008C8C8C0084848400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008484840084848400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF006F80
      E8005A73EF00D6DEFF00BDCEFF00ADBDFF009CB5FF009CADFF0094ADFF009CAD
      FF0094ADFF0094ADFF009CADFF009CADFF009CADFF009CADFF0094ADFF0094AD
      FF009CADFF0094ADFF009CADFF009CB5FF00ADBDFF00BDCEFF00D6DEFF005A73
      EF006F80E800FF00FF00FF00FF00FF00FF008C8C8C00848385009A979900FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008484840086848400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF006F80E8005A73EF00D6DEFF00BDCEFF00ADC6FF00ADBDFF009CB5FF009CB5
      FF009CB5FF009CB5FF009CB5FF009CB5FF009CB5FF009CB5FF009CB5FF009CB5
      FF009CB5FF009CB5FF00ADBDFF00ADC6FF00BDCEFF00D6DEFF005A73EF006F80
      E800FF00FF00FF00FF00FF00FF00FF00FF008C8C8C0084848400848484008684
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400848484008484840084848400848484008484840084848400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF006F80E8005A73E700D6DEFF00C6D6FF00B5C6FF00ADC6FF00ADBD
      FF00ADBDFF00ADBDFF00A5BDFF00A5BDFF00A5BDFF00A5BDFF00ADBDFF00ADBD
      FF00ADBDFF00ADC6FF00B5C6FF00C6D6FF00D6DEFF005A73E7006F80E800FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008C8C8C0084848400848484008484
      8400848484008684840084848400848484008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400848484008484840084848400868484008484840084838500FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF006F80E8005A73E700DEDEFF00CED6FF00BDCEFF00BDC6
      FF00B5C6FF00B5C6FF00B5C6FF00B5C6FF00B5C6FF00B5C6FF00B5C6FF00B5C6
      FF00BDC6FF00BDCEFF00CED6FF00DEDEFF005A73E7006F80E800FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF006F80E8005A73E700DEDEFF00CEDEFF00C6D6
      FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6
      FF00C6D6FF00CEDEFF00DEDEFF005A73E7006F80E800FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF006F80E8005A73E700DEE7FF00DEE7
      FF00DEE7FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF00DEE7
      FF00DEE7FF00DEE7FF005A73E7006F80E800FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007B7CE8000829D6000029
      D6000029D6000029DE000029D6000029D6000029D6000029D6000029DE000029
      D6000029D6000829D6007B7CE800FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00B5B5B500CECECE00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00ADADAD00BDBDBD00A5A5A500C6BDC600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00429CBD00009C
      C6000094CE000094CE000094CE000094CE000094CE000094CE000094CE000094
      CE000094CE000094CE000094CE000094CE000094CE000094CE000094CE000094
      CE000094CE000094CE000094CE000094CE000094CE000094CE000094CE000094
      CE000094CE004AA5BD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6BDC600B5B5B500EFEFEF00E7E7E700A5A5A500FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004AADCE00C6EFFF009CEF
      FF008CE7FF007BE7FF007BE7FF0073E7FF0073E7FF0073E7FF0073DEFF006BDE
      FF006BE7FF0073E7FF0073E7FF0073E7FF0073E7FF0073E7FF0073E7FF006BE7
      FF006BDEFF0073E7FF0073E7FF0073E7FF0073E7FF007BE7FF007BE7FF008CE7
      FF009CEFFF00BDEFF7005AB5CE00FF00FF00FF00FF00FF00FF00B5ADB500D6D6
      D600F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00F7F7
      F700D6D6D600B5ADB500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00397B9C000063940000639C000063940000639C0000639C000063
      9C0000639C0000639C0000639C0000639C0000639C0000639C0000639C000063
      9C0000639C0000639C0000639C000063940000639C0000639400397B9C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00A5A5A500DEDEDE00EFEFEF00CEC6CE00CECECE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE006BCEEF00B5EF
      FF005ADEFF0031D6FF0021D6FF0018CEFF0010CEFF0010CEFF0010CEFF0010CE
      FF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CE
      FF0010CEFF0010CEFF0010CEFF0010CEFF0018CEFF0021CEFF0031D6FF005ADE
      FF00BDEFFF006BCEEF000094CE00FF00FF00FF00FF00FF00FF00A5A5A500FFFF
      FF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700FFFFFF00A5A5A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF004A8CAD004AA5C60063BDDE005ABDDE0052BDDE0052B5DE0052B5DE0052B5
      DE0052B5DE0052B5DE0052B5DE0052B5DE0052B5DE0052B5DE0052B5DE0052B5
      DE0052B5DE0052B5DE0052B5DE0052BDDE005ABDDE0063BDDE004AA5C6004A8C
      AD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5B5
      B500BDBDBD00EFEFEF00E7E7E700A5A5A500FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE0063CEEF0042C6
      EF00ADEFFF005ADEFF0039D6FF0029D6FF0021D6FF0018CEFF0010D6FF0010D6
      FF0010D6FF0018D6FF0018D6FF0010D6FF0010D6FF0018D6FF0018D6FF0010D6
      FF0010CEFF0010D6FF0018CEFF0021D6FF0029D6FF0039D6FF005ADEFF00ADEF
      FF0042C6E70063CEEF000094CE00FF00FF00FF00FF00FF00FF008C848400CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE008C848400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BC6DE0018A5D600089CC600008CB500007BAD00007BA500007B
      A500007BA500007BA500007BA500007BA500007BA500007BA500007BA500007B
      A500007BA500007BA500007BAD00008CB500089CC60018A5D6006BC6DE000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5
      A500E7E7E700EFEFEF00BDBDBD00CECECE00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE009CEFFF0021BD
      EF0042C6EF00B5EFFF0063DEFF0042DEFF0031D6FF0029D6FF0021D6FF0021D6
      FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6
      FF0021D6FF0021D6FF0029D6FF0031D6FF0042DEFF0063DEFF00B5EFFF0042C6
      EF0021BDEF009CEFFF00009CC600FF00FF00FF00FF00A5A5A5009CA5A500A59C
      9C009CA5A5009C9C9C00A5A5A5009C9CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA5009C9CA500A5A5A5009C9C9C009CA5
      A500A59C9C009CA5A500A5A5A500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BC6E70018A5D6002994B500638C9400A59CA5009C9CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA5009C9CA500A59CA500638C94002994B50018A5D6006BC6E7000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADADAD00C6CE
      CE00EFEFEF00DEDEDE00B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE009CEFFF0042DE
      FF0018BDEF0042C6EF00B5EFFF006BE7FF004ADEFF0039DEFF0031DEFF0029D6
      FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6
      FF0029D6FF0031DEFF0039DEFF004ADEFF006BE7FF00B5EFFF0042C6EF0018BD
      EF0042DEFF009CEFFF000094CE00FF00FF00ADB5B500D6CECE00EFEFEF00E7E7
      E700E7E7E700EFE7E700E7E7E700E7E7EF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7EF00E7E7E700EFE7E700E7E7
      E700E7E7E700EFEFEF00D6CECE00ADB5B500FF00FF00FF00FF00FF00FF00FF00
      FF000063940073C6E70018A5D6006B94A500CED6CE00EFEFEF00E7E7E700E7E7
      E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7E700EFE7
      E700E7E7E700E7E7E700EFEFEF00CED6CE006B94A50018A5D60073C6E7000063
      9400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6CE00ADADAD00EFEF
      EF00EFEFEF00ADADB500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009CCE00A5EFFF0042DE
      FF0031D6FF0018BDEF0042C6EF00B5F7FF006BE7FF0052DEFF004ADEFF0042DE
      FF0039DEFF0039D6FF0031D6F70031CEEF0031CEEF0031CEEF0031CEEF0039DE
      FF0042DEFF004ADEFF0052DEFF006BE7FF00B5F7FF0042C6EF0018BDEF0031D6
      FF0042DEFF00A5EFFF000094CE00FF00FF00A59CA500EFEFEF00DEDEDE00DEDE
      DE00D6D6D600DED6DE00D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600DED6DE00D6D6
      D600DEDEDE00DEDEDE00EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF00006394006BC6E70018ADD600A59CA500EFEFEF00DEDEDE00DEDEDE00D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600DEDEDE00DEDEDE00EFEFEF00A59CA50018ADD6006BC6E7000063
      9400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADA5AD00DEDED600EFF7
      F700D6D6D600C6BDC600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009CCE00A5EFFF004ADE
      FF0039DEFF0031D6FF0021C6EF0042C6EF00B5F7FF0073E7FF0063E7FF0052E7
      FF004ADEFF0042CEEF00D6EFF700FF940000D6EFF700FF940000FFEFDE0031CE
      EF004AE7FF0063E7FF0073E7FF00B5F7FF0042CEEF0021C6EF0031D6FF0039DE
      FF004ADEFF00A5EFFF000094CE00FF00FF00A5A5A500E7EFEF00DEDEDE00E7DE
      DE00E7D6AD00DE9C3900FF940000FF9C0000FF940000FF940000FF940000FF94
      0000FF940000FF940000FF9C0000FF940000FF940000FF940000FF9C0000FF94
      0000FF9C0000FF9C0000FF940000FF940000FF940000FF9C0000DE9C3900E7D6
      AD00E7DEDE00DEDEDE00E7EFEF00A5A5A500FF00FF00FF00FF00FF00FF00FF00
      FF0008639C006BCEE70018ADDE00A59CA500EFEFEF00A59CA500A69CA600A49C
      A4009D9C9D009B9C9B00A19CA100A69CA600A69CA600A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500EFEFEF00A59CA50018ADDE006BCEE7000863
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00BDBDBD00B5B5B500EFEFEF00EFEF
      EF00A5A5A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE00A5EFFF0052DE
      FF0042DEFF0039DEFF0039DEFF0021C6EF004ACEEF00BDF7FF007BEFFF0063E7
      FF0052D6EF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      000031CEEF007BEFFF00BDF7FF004ACEEF0021C6EF0039DEFF0039DEFF0042DE
      FF0052E7FF00A5EFFF00009CCE00FF00FF009CA59C00EFEFEF00DEDEDE00E7E7
      E700EFAD4A00FFCE8C00FFE7C600FFDEC600FFE7BD00FFDEC600FFDEC600FFDE
      C600FFDEC600FFE7BD00FFDEC600FFDEC600FFDEC600FFDEC600FFDEC600FFDE
      C600FFDEC600FFDEC600FFDEC600FFDEC600FFDEC600FFE7C600FFCE8C00EFAD
      4A00E7E7E700DEDEDE00EFEFEF009CA59C00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BCEE70018ADDE00A5A5A500EFEFEF00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00EFEFEF00A5A5A50018ADDE006BCEE7000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5A500E7E7E700EFE7EF008C94
      B500B5ADB500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008CD600A5EFFF0042DE
      FF0042DEFF0042DEFF0042DEFF0042DEFF0029B5EF004ABDEF00ADDEFF009CD6
      F700D6EFF700FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE0031CEEF004ABDEF0029B5EF0042DEFF0042DEFF0042DEFF0042DE
      FF0042DEFF00A5EFFF00008CD600FF00FF00A59CA500EFEFEF00DEDEDE00E7EF
      EF00FF9C0000FFE7D600FFD6AD00FFCEA500FFCEA500FFCEA500FFCEA500FFCE
      A500FFCEA500FFCEA500FFCEA500FFCEA500FFCEA500FFD6A500FFCE9C00FFCE
      A500FFCEA500FFCEA500FFCEA500FFD6A500FFCEA500FFD6AD00FFE7D600FF9C
      0000E7EFEF00DEDEDE00EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF000063940073CEE70018ADDE009C9CA500EFEFEF00A59CA500A69CA6007370
      90005E65960091868D009B949B00A69CA600A69CA600A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500EFEFEF009C9CA50018ADDE0073CEE7000063
      9400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00B5B5B500C6C6C600EFEFEF007B8CCE001842
      DE000029D600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE00ADF7FF0063E7
      FF0052E7FF004AE7FF004AE7FF004AE7FF004AE7FF0029C6E700D6EFF700D6EF
      F700FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF94000029C6E7004AE7FF004AE7FF004AE7FF004AE7FF0052E7
      FF0063E7FF00ADF7FF000094CE00FF00FF00A59CA500EFEFEF00E7DEDE00E7EF
      EF00FF940000FFEFD600FFD6AD00FFE7C600FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFE7CE00FFE7C600FFD6A500FFCEA500FFCEA500FFD6A500FFCE
      A500FFD6A500FFD6A500FFCEA500FFCEA500FFD6A500FFD6AD00FFEFD600FF94
      0000E7EFEF00E7DEDE00EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0073CEEF0018B5DE00A59CA500F7EFEF00DEE7E700E7E7E700737F
      AE002258BA00395CA90042599A0090A1C500C0BCC000E1DDCD00DEDEDE00DEDE
      DE00E7DEDE00E7E7E700DEE7E700F7EFEF00A59CA50018B5DE0073CEEF000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00C6BDD6009494AD00D6D6EF00CECEE7002139C6005A73
      F7004A63EF00394AC600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00088CCE00ADF7FF004AE7
      FF004AE7FF004AE7FF004AE7FF004AE7FF004AE7FF00D6EFF700CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C004AE7FF004AE7FF004AE7FF004AE7FF004AE7
      FF004AE7FF00ADF7FF00088CCE00FF00FF00A59CA500EFEFEF00E7DEDE00EFEF
      EF00FF9C0000FFEFDE00FFD6B50000940000009C0000009C0000009400000094
      0000009C0000009C000000940000FFD6AD00FFD6AD00FF9C0000FF940000FF9C
      0000FF940000FF940000FF940000FF9C0000FF940000FFDEB500FFEFDE00FF94
      0000EFEFEF00E7E7E700EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BCEEF0018B5DE009C9CA500EFEFEF00A59CA500A69CA600A29A
      A5004065A900A39C9F0095B9E600588BD7004766AA004F5E980090839500A59C
      A500A59CA500A59CA500A59CA500EFEFEF009C9CA50018B5DE006BCEEF000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00ADADAD00D6CED600EFEFEF006373C6003152E7007B94
      FF007B94FF004263EF004252BD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE00B5F7FF0073EF
      FF0063EFFF005AE7FF005AEFFF005AE7FF00D6EFF700FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C6005AE7FF005AEFFF005AE7FF0063EF
      FF0073EFFF00B5F7FF000094CE00FF00FF00A59CA500EFEFEF00E7E7E700EFEF
      EF00FF940000FFEFDE00FFDEBD000094000031C66B0031CE6B0031CE6B0031CE
      6B0031CE6B0031C66B0000940000FFD6B500FFD6B500FFD6B500FFDEB500FFD6
      B500FFD6B500FFDEB500FFD6B500FFDEB500FFD6B500FFDEBD00FFEFDE00FF94
      0000EFEFEF00E7E7E700EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF00005AA5006BCEEF0018B5E7009494AD00F7F7F700E7E7E700E7E7E700DCDA
      DA004E6B9D008FB6E300FFFFFF00EEFFFF00BDDAFD00699AE7003463BF007282
      B100E7E7E700E7E7E700E7E7E700F7F7F7009494AD0018B5E7006BCEEF00005A
      A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00B5B5CE009C9CB500DEDEF700BDBDDE000829DE006B7BF7004A63
      FF00294AFF006B84FF00425AEF002942C600FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008CCE00B5F7FF0063EF
      FF0063EFFF0063EFFF0063EFFF00D6EFF700FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF94000063EFFF0063EFFF0063EF
      FF0063EFFF00B5F7FF00008CCE00FF00FF00A59CA500EFEFEF00E7E7E700EFEF
      EF00FF940000FFEFDE00FFDEBD00008C180063E7A50063E7A5005AE7A5005AE7
      A5005AE7A50063E79C00088C1800FFD6B500FFD6B500E78C1800E78C1800E78C
      1800E78C1800E78C1800E78C1800E78C1800E78C1800FFDEBD00FFEFDE00FF94
      0000EFEFEF00E7E7E700EFEFEF00A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BCEEF0018B5E700A5A59C00F7F7F700A59CA500A69CA600A59C
      A3007386A1005FA3E500EEFBFF00EAF7FE00D0F8FF00AEECFF0086D0FF003172
      D0005D6B9F00A69BA300A59CA500F7F7F700A5A59C0018B5E7006BCEEF000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00A5A5AD00DEDEDE00EFEFEF004A63BD004263EF00849CFF00214A
      FF00214AFF003152FF007B94FF005A73F7000831DE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE00BDF7FF0084EF
      FF0073EFFF006BEFF700D6EFF700FFE7D600FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF9400005AC6D6006BEFF70073EF
      FF0084EFFF00BDF7FF000094CE00FF00FF00A5A59C00F7F7F700E7E7E700F7EF
      EF00FF940000FFEFE700FFE7C600949C0000009C000042ADB50029ADCE0029A5
      CE0052ADAD0000940000949C0000FFDEC600FFDEBD00FFDEC600FFDEBD00FFDE
      BD00FFDEC600FFDEC600FFDEC600FFDEC600FFDEBD00FFE7C600FFEFDE00FF94
      0000F7EFEF00E7E7E700F7F7F700A5A59C00FF00FF00FF00FF00FF00FF00FF00
      FF00005A9C0073D6EF0018BDE7009494AD00F7F7F700EFEFEF00E7E7E700E7E7
      E700CAD0D4005096CF00BEE4FF00D0F3FE007FE1FE007DE1FB0091DCFE0081C9
      FF002C6FCF007481AE00EFECE800F7F7F7009494AD0018BDE70073D6EF00005A
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00A5A5BD00ADADBD00E7DEF700949CD6001031DE007B8CFF004263FF00294A
      FF00294AFF00294AFF00294AFF007B8CFF00738CFF00526BEF003952EF002142
      DE001839DE000021DE000021DE001031CE002942C6005A63C600FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008CCE00BDF7FF0073EF
      FF0073EFFF00D6EFF700CE630000FFE7D600FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF94000073D6E7004AADDE0073EF
      FF0073EFFF00BDF7FF00008CCE00FF00FF00A59CA500F7F7F700E7E7E700F7F7
      F700FF940000FFEFE700FFE7C600D68C18005284630052BDF7005ACEFF0063CE
      FF0042ADE70084843900D68C1800FFDEBD00FFDEBD00E78C1800E78C1800E78C
      1800E78C1800E78C1800E78C1800E78C1800E78C1800FFE7C600FFEFDE00FF94
      0000F7F7F700E7EFEF00F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0073D6EF0018BDE700A5A59C00F7F7F700A59CA500A69CA600A49C
      A4009D9C9D005A82B0006CBAEE00ADFEFF007BE3FC0074DEFE006FD9FE0081D1
      FE0072C4FF002369D000616A9A00F7F7F700A5A59C0018BDE70073D6EF000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF009CA5A500EFEFEF00EFEFEF00314ABD00637BEF008CA5FF003963FF00315A
      FF00315AFF00395AFF00315AFF00315AFF003963FF006B84FF00849CFF008C9C
      FF008CA5FF008C9CFF008C9CFF007B94F700637BF7004263E7000829D6005A6B
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE00C6FFFF008CF7
      FF00D6EFF70042BDD600CE630000FFE7D600FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF940000D6EFF70073D6E7005AC6
      D6008CF7FF00C6FFFF000094CE00FF00FF00A59CA500F7F7F700E7EFEF00F7F7
      F700FF9C0000FFF7EF00FFE7D600DE941800529CA50094EFFF0052E7FF0063EF
      FF008CE7F7007B946B00DE941800FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFE7CE00FFE7CE00FFE7D600FFE7CE00FFE7D600FFF7E700FF94
      0000F7F7F700E7EFEF00F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF00005AA5006BD6F70021BDEF009494AD00F7F7F700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00B3C6D10034A6E30079F1FF0089FBFD006EDEFE0064D8FD005CD3
      FD0073CDFE0062BBFF001E67D1007382B2009494AD0021BDEF006BD6F700005A
      A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A59C
      B500BDB5CE00E7DEF7006B7BCE002942DE008C9CFF004263FF00425AFF00395A
      FF00395AFF00395AFF00395AFF00425AFF00395AFF00395AFF00395AFF00395A
      FF00395AFF00395AFF00425AFF004A6BFF006B84FF008C9CFF008494FF003952
      E7005263D600FF00FF00FF00FF00FF00FF00FF00FF00008CCE008CF7FF00D6EF
      F7004AADDE0063C6EF00CE630000FFE7D600FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF940000C6FFFF00D6EFF70073D6
      E7005AC6D6008CF7FF00088CCE00FF00FF00A59CA500F7F7F700EFEFEF00F7F7
      F700FF940000FFF7EF00FFE7D600D6842900638C8C009CD6F7009CE7FF00ADE7
      FF0084C6EF008C846300D68C2900FFE7CE00FFE7CE00E78C1800E78C1800E78C
      1800E78C1800E78C1800E78C1800E78C1800E78C1800FFE7D600FFF7EF00FF94
      0000F7F7F700E7EFEF00F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BD6F70021BDEF009C9CA500F7F7F700A59CA500A69CA600A49C
      A4009D9C9D00A3A3A00070B2DA002BB3F4006DEFFB0072F8FF005BDBFD0052D2
      FD004DD0FC0066C5FF0052ADFF001D64D0005F75B10023BCED006BD6F7000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A5AD
      AD00FFF7F700E7E7EF001839C600849CF700849CFF005273FF004A6BFF004A6B
      FF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF004A6BFF005273FF004A6B
      FF004A73FF004A6BFF004A73FF004A73FF005273FF005A7BFF00849CFF00ADBD
      FF00425AE700FF00FF00FF00FF00FF00FF00FF00FF000094CE00C6EFEF005AC6
      D60073D6E700D6EFF700CE630000FFE7D600FFD6AD00FFC67B00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF9C0000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF9C0000C6FFFF00C6FFFF00D6EF
      F70073D6E700C6E7E7000094CE00FF00FF00A59CA500F7F7F700EFEFEF00F7F7
      F700FF9C0000FFF7F700FFEFDE00FF9C0000C69429005AADB50094D6EF0084D6
      E7005AA5A500EF940800FF940000FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFF7EF00FF94
      0000F7F7F700EFEFEF00F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF00005AA5006BD6F70018C6F7009494A500F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F70086C5F30027B2F60056F2FF005EFAFE0047D6
      FD0041CFFD003ECAFD004EB6FE0043A4FF001960C80033649A006DD3F300005A
      A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C9CB500C6C6
      D600E7E7FF00636BD600395AE7009CADFF00637BFF005273FF00526BFF005273
      FF004A6BFF00526BFF00526BFF00526BFF00526BFF00526BFF00526BFF004A73
      FF00526BFF00526BFF004A6BFF00526BFF005A73FF00637BFF00738CFF00A5AD
      FF001031DE00FF00FF00FF00FF00FF00FF00FF00FF00009CCE007BC6D60084D6
      EF00D6EFF700C6FFFF00CE630000EFD6B500CE630000DE8C4200F7BD8400FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF9C0000FFEFDE00FF9C
      0000FFEFDE00FF940000FFCEA500FFE7C600FF940000C6FFFF00C6FFFF00C6FF
      FF00D6EFF7007BC6D6000094CE00FF00FF00A59CA500F7F7F700EFEFEF00F7F7
      F700FF940000FFFFFF00FFEFDE00E78C1800E78C1800948C5A005A94A5005A94
      9400AD8C4A00E78C1800E78C1800FFEFDE00FFEFDE00E7841800E78C1800E78C
      1800E78C1800E78C1800EF8C1800EF8C1800E7841800FFEFDE00FFF7EF00FF94
      0000F7F7F700EFEFEF00F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BD6F70018C6F700A5A59C00F7F7F700A59CA500A69CA600A49C
      A4009D9C9D009B9C9B00A19CA100A69CA60073ACDC0017A2EA003BEDFF0046F7
      FD003AD1FC0032C9FC002CC6FD003FADFF00329CFD001565D3005085C3000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5ADB500FFFF
      FF00F7FFFF000029D600ADBDFF009CADFF007394FF006B8CFF006B84FF006384
      FF006384FF006384FF006384FF006384FF006384FF006384FF006384FF006384
      FF006384FF006384FF006384FF006B84FF00738CFF007B94FF00B5C6FF006B84
      EF006B84E700FF00FF00FF00FF00FF00FF00FF00FF000094CE0094D6EF00D6EF
      F700C6FFFF00C6FFFF00CE630000DE945200FFDEAD00EFA54A00DE8C3900FFFF
      FF00FFFFFF00FF940000FFFFFF00FF9C0000FFF7EF00FF940000FFEFDE00FF9C
      0000FFEFDE00FF9C0000FFCE9C00FFE7C600FF940000EFFFFF00C6FFFF00C6FF
      FF00E7FFFF0094DEE7000094CE00FF00FF00A59CA500F7F7F700F7F7F700F7F7
      F700FF9C0000FFF7F700FFF7EF00FF9C0000FF940000FF9C0000FF940000FF94
      0000FF940000FF9C0000FF940000FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7
      EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFFFF700FF94
      0000F7F7F700F7F7F700F7F7F700A59CA500FF00FF00FF00FF00FF00FF00FF00
      FF00085AA50073DEF70018C6F700948CAD00FFFFFF00F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F70084ACD4001CA8ED0028ED
      FF002DF3FD0028CEFD001DC5FE001CC0FE002EA7FF002997FF001E69CF003466
      AF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5B5B500E7E7E700FFFF
      FF00CECED6003952CE008C9CEF00BDC6FF008CA5FF00849CFF007B94FF007B94
      FF007394FF00738CFF00738CFF007394FF00738CFF00738CFF00738CFF006B8C
      FF00738CFF00738CFF00738CFF007394FF00849CFF00A5B5FF00B5BDF7000829
      DE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006BBDDE00D6EFF700E7FF
      FF00EFFFFF00E7FFFF00CE630000CE630000FFBD6B00FFD6AD00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF9C0000FFF7EF00FF940000FFEFDE00FF94
      0000FFEFDE00FF940000FFCE9C00FFDEC600FF940000EFFFFF00EFFFFF00C6FF
      FF00E7FFFF00DEF7FF005ABDD600FF00FF009CA59C00FFF7F700F7F7F700F7FF
      F700FF940000FFFFFF00FFF7EF00E78C1800E78C1800E78C1800E78C1800E78C
      1800E78C1800E78C1800E78C1800FFF7EF00FFF7EF00E78C1800E78C1800E78C
      1800E78C1800E78C1800E78C1800E78C1800E78C1800FFF7EF00FFF7EF00FF94
      0000F7FFF700F7F7F700FFF7F7009CA59C00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0073DEF70018C6F7009C9CA500FFFFFF00F7F7F700DEDEDE00CECE
      CE00CEC6CE00CEC6CE00CEC6CE00CEC6CE00CEC6CE00D0C7CC007DADD60020AF
      EF0010EBFC0012F1FF0014CAFE000DBEFD000CBBFC0026A5FF001B91FC001F69
      D2005E849B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5A500DEDE
      DE00A5A5A500ADB5E7002142DE00C6D6FF00ADC6FF0094ADFF0094A5FF008CA5
      FF008CA5FF008CA5FF0084A5FF00849CFF008CA5FF00849CFF00849CFF007B9C
      FF007B94FF007B94FF00849CFF00849CFF0094ADFF00C6D6FF004A63E700FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE0094DEE700E7FF
      FF00CEFFFF00C6FFFF00CE630000CE630000FFBD6B00FFD6AD00C6630000FFFF
      FF00FFFFFF00FF9C0000FFFFFF00FF9C0000FFF7EF00FF9C0000FFEFDE00FF94
      0000FFEFDE00FF9C0000FFCE9C00FFE7C600FF940000EFFFFF00EFFFFF00C6FF
      FF00E7FFFF0094D6EF000094CE00FF00FF009C9CA500FFFFF700F7F7F700F7F7
      F700FF9C0000FFFFFF00FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFFF00FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FF94
      0000F7F7F700F7F7F700FFFFF7009C9CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C006BDEFF0018CEF700A59CA500FFFFFF00D6CED600A59C9C00A59C
      A500A59CA500A59CA5009C9C9C00A59CA500A59CA5009C9C9C00A79DA60072A5
      D1001DAEEF0000EAFC0001F2FE0006C5FD0000BBFD0003B4FC0083CDFF0099C2
      F400114EC200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BDBD
      BD00FF00FF00FF00FF006B84E7004A6BE700D6DEFF00C6CEFF00ADBDFF00A5B5
      FF009CB5FF009CB5FF009CB5FF009CADFF009CADFF0094ADFF0094ADFF008CAD
      FF008CA5FF008CA5FF008CA5FF009CADFF00BDCEFF009CADF700395ADE00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007BC6D6007BD6
      E700DEFFFF00B5FFFF00CE630000C6630000FFBD6B00FFD6AD00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF9C0000FFEFDE00FF9C
      0000FFEFDE00FF940000FFCE9C00FFE7C600FF940000EFFFFF00C6FFFF00FFFF
      FF0084D6EF007BC6D600FF00FF00FF00FF00A5A5A500F7FFFF00F7F7F700F7FF
      FF00FF9C0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFFF00FF9C
      0000F7FFFF00F7F7F700F7FFFF00A5A5A500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0073DEF70021CEFF009C9C9C00FFFFFF00B5ADB500D6D6D600EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF008CBCE5001AABEF0000E9FD0000F0FF0002C2FC008ADEFC00FFFFFF0091B0
      E300194DBE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF006B7BE7004263E700C6CEFF00D6DEFF00CEDE
      FF00C6CEFF00BDCEFF00B5C6FF00B5C6FF00ADC6FF00ADBDFF00A5BDFF00A5B5
      FF009CB5FF009CB5FF009CB5FF00A5BDFF00CED6FF00294ADE00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063C6
      D60073D6E700CEFFF700CE630000CE630000FFBD6B00FFD6AD00CE630000FFFF
      FF00FFFFFF00FF9C0000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF9C
      0000FFEFDE00FF940000FFCEA500FFE7C600FF940000FFFFFF00FFFFFF0084D6
      EF005AC6D600FF00FF00FF00FF00FF00FF009C9C9C00FFFFFF00F7FFFF00FFFF
      F700FFBD5A00FFD6A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFD6A500FFBD
      5A00FFFFF700F7FFFF00FFFFFF009C9C9C00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C007BE7FF0031CEFF00A5A5A500FFFFFF00A59CA500EFEFF700EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00F7F7F700F7F7F700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF0095BCE8001BAAED0000E8FA006DFAFE00FFFFFF00B5D1F000205A
      C4009BB66E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008494EF000829DE004263E7007B94
      EF00A5B5F700B5BDF700DEDEFF00DEE7FF00DEDEFF00D6DEFF00D6DEFF00BDC6
      FF00B5C6FF00B5BDFF00ADC6FF00CEDEFF008C9CEF005A73DE00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF004AADDE0063C6EF00CE630000CE630000FFBD6B00FFD6AD00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF9C0000FFEFDE00FF94
      0000FFEFDE00FF940000FFCEA500FFDEC600FF9C0000FFFFFF0084D6EF005AC6
      D600FF00FF00FF00FF00FF00FF00FF00FF009C9CA500FFFFFF00FFFFFF00FFFF
      FF00FFE7C600FFBD5A00FF940000FF940000FF940000FF940000FF940000FF94
      0000FF940000FF940000FF940000FF940000FF940000FF9C0000FF9C0000FF9C
      0000FF940000FF940000FF940000FF9C0000FF940000FF940000FFBD5A00FFE7
      C600FFFFFF00FFFFFF00FFFFFF009C9CA500FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0084E7FF0042D6FF007BB5BD00DEDEDE00A5A59C00F7F7F700F7F7
      F700A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500F7F7F700F7F7F6007DA7CA001FAFEE0089E1FA0090B3E8002562C4001260
      AC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006B84
      E700425ADE003152DE000029D6000029D6001842DE004A63E7007B8CEF00CED6
      FF00D6E7FF00BDCEFF00C6D6FF00CEDEFF001839DE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0042BDD600CE630000CE630000FFBD7300FFD6AD00CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF940000FFEFDE00FF9C
      0000FFEFDE00FF9C0000FFCE9C00FFE7C600FF9C000084D6EF005AC6D600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C00FF00FF00FF00FF00FF00FF00FF00
      FF0000639C0094E7FF005ADEFF0063CEE70084B5C6009C9C9C00F7F7F700F7F7
      F700A59CA500848484008484840084848400848484008484840084848400A59C
      A500F7F7F700F7F7F7009F9E9D0060AFE100118CE6001A6AD1006EB1E5000063
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00738CE7001039
      DE0094A5EF00DEE7FF00DEE7FF006B84EF007384E700FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00CE630000CE630000FFC67B00FFDEB500CE630000FFFF
      FF00FFFFFF00FF940000FFFFFF00FF940000FFF7EF00FF9C0000FFEFDE00FF94
      0000FFEFDE00FF9C0000FFCEA500FFE7CE00FF9400005AC6D600FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5A500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00A5A5A500FF00FF00FF00FF00FF00FF00FF00
      FF005A9CBD0063B5DE009CEFFF009CE7FF0094E7FF009CB5BD00DEDEDE00F7FF
      F700A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500F7FFF700DEDEDE009CB5BD0094E7FF009CE7FF009CEFFF0063B5DE005A9C
      BD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF005273DE008494EF00C6CEF7000029D600FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00CE630000DE9C5200FFDEAD00EFA54A00DE8C4200FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFEFDE00FFEF
      E700FFEFDE00FFD6AD00FFD6AD00FFE7CE00FF940000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00BDBDBD00DEDEDE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDEDE00BDBDBD00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005A9CBD0000639C00006394000063940029739C00ADADAD00F7F7
      F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F700ADADAD0029739C00006394000063940000639C005A9CBD00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF006B84E7002142DE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00CE630000CE630000DE8C4200F7BD8400FFCE
      8C00FFCE8C00FFCE8C00FFCE8C00FFCE8C00FFCE8C00FFCE8C00FFCE8C00FFCE
      8C00FFCE8C00FFCE8C00FFCE8C00FFCE8C00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600A59CA500A59C
      A500A59CA500A59CA500A5A5A500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59CA500A59C
      A500A59CA500A59CA500A59CA500A59CA500A59CA500A5A5A500A59CA500A59C
      A500A59CA500A59CA500C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00CECECE00A5A5
      AD00D6D6D600EFEFEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFEF00D6D6
      D600A5A5AD00CECECE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EFEF
      EF00C6C6C600ADADAD009C9C9C00A59CA500A59CA5009C9C9C00ADADAD00C6C6
      C600EFEFEF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00B5B5B500CECECE00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007BAD
      7B004A9C4A002994290010941000009400000094000010941000299429004A9C
      4A007BAD7B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007B8C
      BD004A5ABD002942C6001031CE000029D6000029D6001031CE002942C6004A5A
      BD007B8CBD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00ADADAD00BDBDBD00A5A5A500C6BDC600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063A56300089C080029AD
      39004ABD5A006BCE7B007BD6940084DEA50084DEA5007BD694006BCE7B004ABD
      5A0029AD3900089C080063A56300FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006373BD000829D600214A
      E7004263EF005273F700637BFF00738CFF006B8CFF00637BF7005273EF003963
      EF00214AE7000829D6006373BD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6BDC600B5B5B500EFEFEF00E7E7E700A5A5A500FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0084AD8C00089C080042B54A006BCE84008CE7
      AD0084DEA50063D68C004AD67B0039CE6B0039CE6B004AD67B0063D68C0084DE
      A5008CE7AD006BCE840042B54A00089C080084AD8C00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF008494C6000831DE003152E7005A73F700738C
      FF006B8CFF00526BFF00315AFF002952FF002152FF00315AFF004A6BFF006B8C
      FF00738CFF005A73F7003152E7000831D6008494C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00A5A5A500DEDEDE00EFEFEF00CEC6CE00CECECE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0063A5630029AD310063CE7B008CE7AD0063D68C0039CE
      6B0039CE6B0039CE6B0039CE6B0039CE6B0039CE6B0039CE6B0039CE6B0039CE
      6B0039CE6B0063D68C008CE7AD0063CE7B0029AD310063A56300FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF006373C600294AE7005273F7007394FF004A6BFF00214A
      FF002952FF003152FF00315AFF00395AFF00315AFF00315AFF002952FF00214A
      FF001842FF004A6BFF00738CFF005273F700214AE7006373C600FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5B5
      B500BDBDBD00EFEFEF00E7E7E700A5A5A500FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005AA55A0039B54A007BD6940073DE940039CE730039CE6B0039CE
      6B0039CE6B0039CE6B0031CE6B0039CE6B0039CE6B0039CE6B0039CE6B0039CE
      6B0039CE6B0039CE6B0039CE6B0073DE94007BD6940039B542005AA55A00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005A6BC6003152E7006B84FF005A7BFF00214AFF002952FF00395A
      FF004263FF004A6BFF005273FF005273FF005273FF004A6BFF004263FF00315A
      FF00214AFF001842FF001842FF005273FF006384F7003152E7005A6BBD00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E7D6B500DE9C4200FF94
      0000FF940000FF940000FF940000FF940000FF940000FF940000FF940000FF94
      0000FF940000FF940000FF940000FF940000FF940000FF940000FF940000FF94
      0000FF940000FF940000FF940000FF940000FF940000FF940000FF940000FF94
      0000FF940000DEA54A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5
      A500E7E7E700EFEFEF00BDBDBD00CECECE00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF006BAD6B0042B5420084DE9C0063DE8C0039D66B0039CE730039CE6B0039D6
      6B0039CE6B0039CE6B0039CE6B0039CE6B0042CE6B0039CE6B0039CE6B0039CE
      6B0039CE6B0039CE6B0039CE6B0042D6730063DE8C008CDE9C0039B54A006BAD
      6B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF006B7BCE003152E700738CFF004A6BFF002952FF00395AFF004A63FF005A7B
      FF008CA5FF00A5B5FF00ADB5FF00A5B5FF00A5B5FF009CADFF0094ADFF00738C
      FF00315AFF00214AFF001842FF001842FF004A6BFF00738CF7003952E7006B7B
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EFAD4A00FFEFD600FFEF
      D600FFE7D600FFE7CE00FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7
      C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7
      C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7C600FFE7CE00FFE7
      D600FFEFD600FFE7CE00EFAD5A00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADADAD00CEC6
      CE00F7EFEF00DEDEDE00B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0094BD
      940031AD310084DE940063DE8C0042D6730039D6730039D66B0039D66B0039D6
      6B0039D6730039CE6B0039D66B0039D6730039CE6B0039D66B0039D66B0039D6
      730039CE6B0039D6730039D66B0042D6730039D6730063DE8C0084DE9C0031AD
      310094BD9400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0094A5
      D600294ADE006B8CF7005273FF003152FF003963FF004A6BFF008494FF009CB5
      FF00738CF7004A63E700214ADE000831D6000831DE002142DE00395AE7005A7B
      EF007B94FF003152FF00214AFF00214AFF00214AFF004A6BFF006B8CF700294A
      DE0094A5D600FF00FF00FF00FF00FF00FF00FF00FF00FF940000FFCE8C00FFEF
      E700FFDEBD00FFD6B500FFD6AD00FFD6A500FFCEA500FFCEA500FFCEA500FFCE
      A500FFCEA500FFCEA500FFCEA500FFCEA500FFCEA500FFCEA500FFCEA500FFCE
      A500FFCEA500FFCEA500FFCEA500FFCEA500FFD6A500FFD6AD00FFD6B500FFDE
      BD00FFEFE700FFCE8C00FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600ADADAD00EFEF
      EF00EFEFEF00B5ADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00109C
      100073CE84007BE79C0042D6730039D6730039D6730039D66B0039D6730039D6
      730039D6730042D66B0039D66B0039D6730039D6730042D6730042D6730039D6
      730039D6730042D6730039D6730042D6730039D6730042D673007BE79C006BCE
      8400109C1000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF001031
      DE00637BF7006B84FF00315AFF003963FF004A6BFF0094ADFF007B94F700395A
      E7005A73E700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004A63C600294A
      DE00637BF7005A7BFF002952FF003152FF002952FF003152FF006384FF00637B
      F7001031DE00FF00FF00FF00FF00FF00FF00FF00FF00FF9C0000FFCE8C00FFC6
      7B00FFEFDE00FFDEBD00FFDEB500FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFD6
      A500FFD6A500FFD6A500FFD6A500FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFDEB500FFDEBD00FFEF
      DE00FFC67B00FFCE8C00FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADA5A500D6D6DE00EFF7
      EF00D6D6D600C6BDC600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007BB57B004ABD
      52009CE7B5004AD6730042D6730042D6730042D6730042D6730042D6730042D6
      730042D673008CE7AD008CE7AD0042D6730042D6730042D6730042D6730042D6
      730042D6730042D6730042D6730042D6730042D6730042D673004AD67B009CE7
      B5004ABD52007BB57B00FF00FF00FF00FF00FF00FF00FF00FF007B8CD600425A
      E7008CA5FF00395AFF003963FF004A6BFF0094ADFF006B8CEF002142DE00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004A63C600294ADE00849C
      FF006B84FF00395AFF003963FF003963FF003963FF00395AFF00395AFF008CA5
      FF00425AE7007B8CD600FF00FF00FF00FF00FF00FF00FF940000FFEFD600FFC6
      7300FFC67B00FFEFE700FFDEC600FFDEBD00FFD6B500FFD6AD00FFD6AD00FFD6
      AD00FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFD6AD00FFD6
      AD00FFD6AD00FFD6AD00FFD6AD00FFD6B500FFDEBD00FFDEC600FFEFE700FFC6
      7B00FFBD7300FFEFD600FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00BDBDBD00B5B5B500EFF7F700EFEF
      EF00A5A5A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00089C10007BD6
      940073E79C0042D67B0042D6730042D6730042D6730042D6730042D6730042D6
      730094E7AD00FFFFFF00FFFFFF008CE7AD0042DE730042D6730042D6730042D6
      730042D6730042D6730042D6730042DE730042D6730042D6730042DE7B007BE7
      9C007BD69400089C1000FF00FF00FF00FF00FF00FF00FF00FF000831D6006B8C
      F7006B84FF004263FF004A6BFF008494FF007B94F7002142DE00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF004A63C600294ADE008CA5FF006B8C
      FF004263FF004A6BFF005273FF00849CFF005A73FF00426BFF003963FF006B84
      FF006B8CF7000831D600FF00FF00FF00FF00FF00FF00FF940000FFEFDE00FFDE
      BD00FFBD7300FFC68400FFEFE700FFE7C600FFDEBD00FFDEBD00FFDEB500FFD6
      B500FFDEB500FFDEB500FFDEB500FFD6B500FFD6B500FFDEB500FFDEB500FFDE
      B500FFD6B500FFDEB500FFDEBD00FFDEBD00FFE7C600FFEFE700FFC68400FFBD
      7300FFDEBD00FFEFDE00FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00A5A5A500E7E7E700E7E7E7008CAD
      8C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009CCE9C0039B53900A5EF
      BD0052DE84004ADE7B0042DE7B0042D6730042DE7B0042DE7B0042DE7B008CEF
      AD00FFFFFF00FFFFFF00FFFFFF00FFFFFF008CE7AD0042DE7B0042DE730042DE
      7B0042DE7B0042DE730042DE7B004ADE7B0042DE7B004ADE73004ADE7B005ADE
      8400A5EFBD0039B539009CCE9C00FF00FF00FF00FF009CADE7003152E70094AD
      FF00426BFF004A6BFF005A7BFF00A5ADFF00395ADE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF005263C600294AE7008CA5FF00738CFF004A6B
      FF00527BFF006384FF00ADB5FF006B84EF009CADFF00527BFF004A6BFF00426B
      FF0094ADFF003152E7009CADE700FF00FF00FF00FF00FF9C0000FFEFDE00FFDE
      BD00FFDEB500FFBD7300FFC68400FFF7E700FFE7CE00FFE7C600FFDEBD00FFDE
      BD00FFDEBD00FFDEBD00F7D6B500EFCEAD00EFCEAD00F7D6B500FFDEBD00FFDE
      BD00FFDEBD00FFDEBD00FFE7C600FFE7CE00FFF7E700FFC68400FFBD7300FFDE
      B500FFDEBD00FFEFDE00FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00A5A5B500B5ADC600DEDEF70073A58C002194
      3900088C1800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006BB56B005AC66B009CEF
      B50052DE840042DE7B0042DE7B004ADE7B004ADE7B0042DE7B0094EFAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008CEFAD0042DE7B004ADE
      7B0042DE7B0042DE7B004ADE7B004ADE7B0042DE7B004ADE7B0042DE7B0052DE
      84009CEFB5005AC66B006BB56B00FF00FF00FF00FF006B7BD6005273EF0094AD
      FF004A73FF005273FF008CA5FF006B8CEF00526BDE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF004A63C6002952DE0094A5FF007B94FF005273FF005A7B
      FF006B84FF00ADBDFF008CA5F7000831DE006B8CEF008CA5FF005273FF004A73
      FF0094ADFF005273EF006B7BD600FF00FF00FF00FF00FF9C0000FFEFDE00FFDE
      C600FFDEBD00FFDEB500FFC67300FFCE8400FFF7E700FFE7D600FFE7CE00FFE7
      C600FFDEC600EFCEB500E7B57B00E7AD6300E7AD6300E7B57B00EFCEB500FFDE
      C600FFE7C600FFE7CE00FFE7D600FFF7E700FFCE8400FFC67300FFDEB500FFDE
      BD00FFDEC600FFEFDE00FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00D6D6D600A5A5A500EFEFEF00E7E7E700219418007BD6
      940063CE7300399C3900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0042AD42009CEFB5009CE7
      B50052DE840052DE840052DE840052DE840052DE84009CF7B500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009CF7B5004ADE
      73004ADE73004ADE7B004ADE73004ADE73004ADE7B004ADE73004ADE73005AE7
      84009CEFB500C6F7D60042AD4200FF00FF00FF00FF003952D6007384F700738C
      FF005A7BFF005A7BFF009CA5FF004A5AE700FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF004A5ACE00314AE7008C9CFF00738CFF005A7BFF005A7BFF006B7B
      FF00A5ADFF008C9CF7001031DE008494E700425AE70094A5FF005A7BFF005A7B
      FF00738CFF007384F7003952D600FF00FF00FF00FF00FF9C0000FFF7E700FFDE
      BD00FFDEBD00FFDEBD00FFDEBD00FFCE8400FFCE8400FFE7D600FFE7D600FFE7
      C600FFE7D600E7BD8C00E7B58C00FFEFDE00FFEFDE00E7B58C00D6A57B00FFDE
      BD00FFE7D600FFE7D600FFE7D600FFCE8400FFCE8400FFDEBD00FFDEBD00FFDE
      BD00FFDEBD00FFEFDE00FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF009C9CB500BDBDD600D6D6EF005A9C730039A552008CCE
      B5008CCEB5004AAD6B00398C5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00219C18009CE7B50073E7
      9C0052E784004AE784004ADE7B004ADE7B0094EFB500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0094EF
      B5004ADE7B004AE77B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B005AE7
      840073E79C009CE7B500219C1800FF00FF00FF00FF001839D60094ADF700738C
      FF005A7BFF005A7BFF00A5B5FF00214ADE00FF00FF00FF00FF00FF00FF00FF00
      FF005263C6003152E7009CADFF00849CFF00637BFF006B84FF007394FF00B5C6
      FF0094ADF7001031DE009CADEF00FF00FF00214ADE00A5BDFF00637BFF005A7B
      FF00738CFF0094ADF7001839D600FF00FF00FF00FF00FF940000FFF7E700FFE7
      CE00FFDEC600FFDEC600FFDEC600FFDEC600F7C67B00FFCE8C00FFF7EF00F7E7
      DE00EFB57300FFC67B00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFC67B00EFB5
      7300F7E7DE00FFF7EF00FFCE8C00F7C67B00FFDEC600FFDEC600FFDEC600FFDE
      C600FFE7CE00FFF7E700FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6C6C600ADADAD00F7F7F700D6DECE00009C000084DE9C006BDE
      940052D67B006BCE7B004ABD5A0029943100FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00089C08009CE7B5006BEF
      9C004AE784004AE784004AE784004AE784009CF7B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009CF7B5009CF7B500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009CF7B5004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE
      7B009CEFB500C6F7D600219C1800FF00FF00FF00FF000829DE009CADFF006B8C
      FF006B8CFF006B8CFF009CADFF001031DE00FF00FF00FF00FF00FF00FF004A5A
      CE00314AE70094A5FF007B94FF006373FF006384FF007384FF00A5B5FF008C9C
      F7001031DE008C9CEF00FF00FF00FF00FF001029DE009CADFF006B8CFF006B8C
      FF006B8CFF009CADFF000829DE00FF00FF00FF00FF00FF9C0000FFF7E700FFDE
      C600FFDEC600FFDEC600FFDEC600FFDEC600FFDEC600FFCE8400FFCE8400E7AD
      6300E7B58C00FFFFF700FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFFFF700E7B5
      8C00D6A58400FFCE8400FFCE8400FFDEC600FFDEC600FFDEC600FFDEC600FFDE
      C600FFDEC600FFF7EF00FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF009C94AD00CEC6DE00DED6EF004A8C5A004AAD6B009CEFB50052D6
      7B0052D67B0052D67B0042D673005ABD8400108C2900298442005A8C7300FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00089C0800ADEFBD0073EF
      9C005AE7940052E7840052E7840052E7840094EFB500FFFFFF00FFFFFF00FFFF
      FF0094EFB5004AE784004AE7840094EFB500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0094EFB50052E784004AE784004AE7840052E784004ADE7B004ADE
      7B0073EF9C00B5F7C600089C0800FF00FF00FF00FF000831D600ADBDFF007394
      FF006B8CFF006B8CFF00ADBDFF000831DE00FF00FF00FF00FF004A63C6003152
      DE009CB5FF008CA5FF006B8CFF007394FF00849CFF00BDC6FF009CADF7001031
      DE009CADEF00FF00FF00FF00FF00FF00FF000831D600ADBDFF006B8CFF006B8C
      FF007394FF00ADBDFF000831D600FF00FF00FF00FF00FF9C0000FFF7EF00FFE7
      D600FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00EFD6BD00E7AD6300FFCE
      8400FFEFE700FFE7C600FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFE7C600FFEF
      E700FFCE8400E7AD6300EFD6BD00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFF7EF00FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00B5BDB500BDB5BD00F7F7F700A5C6A500109C100094E7AD005ADE8C0042D6
      730042D6730042D6730042D673006BCE7B006BCE7B006BCE7B0042BD4A0021AD
      290021AD2100009400000094000010941000299431005AA55A00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00089C0800ADEFBD006BEF
      9C005AE794005AE794005AE794005AE794005AE794009CF7B500FFFFFF009CF7
      B5004AE7840052EF8C004AE7840052EF8C0094F7B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0094F7B50052E7840052E7840052E7840052E7840052E7
      84009CEFB5009CE7B50042AD4200FF00FF00FF00FF002139DE0094A5F7008494
      FF007B94FF007B94FF009CADFF002142DE00FF00FF00425ACE00314AE7009CA5
      FF008C9CFF007B94FF007B94FF007B94FF00ADBDFF00949CF7001031DE008C9C
      EF00FF00FF00FF00FF00FF00FF00FF00FF002142DE009CADFF007B94FF007B94
      FF008494FF0094A5F7002139DE00FF00FF00FF00FF00FF9C0000FFF7E700FFE7
      CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00E7DECE00E7BD8C00FFCE8C00FFFF
      F700FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00FFFFF700FFD69400E7BD8C00EFD6BD00FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFF7EF00FF9C0000FF00FF00FF00FF00FF00FF00FF00FF00CECE
      DE009494AD00D6D6EF00DED6EF003184420063BD84009CEFB50042D6730042D6
      730042D6730042D6730042D6730042D6730042D6730042D6730042D6730042D6
      730042D6730042D6730042D673007BCEA50063BD8C0042AD6300088C21005294
      6B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0042AD42008CDE9C009CF7
      BD006BEF94005AEF8C005AEF8C0052E7840052EF8C0052EF8C0052E7840052EF
      8C0052EF840052EF8C0052EF840052E7840052EF840094F7B500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0094F7B50052EF840052EF840063EF8C006BEF
      94009CF7BD008CDE9C0042AD4200FF00FF00FF00FF00395ADE008C9CF7009CB5
      FF007B94FF007394FF00ADBDFF004263E700425ABD003152E700A5BDFF0094AD
      FF007B94FF00849CFF008CA5FF00BDCEFF00A5B5F7000831DE009CA5EF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF004A6BE700ADBDFF007B94FF007B94
      FF009CB5FF008C9CF700395ADE00FF00FF00FF00FF00FF940000FFF7EF00FFEF
      DE00FFEFDE00FFEFDE00FFE7DE00E7DECE00E7BD7B00FFCE8C00FFF7EF00FFE7
      D600FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7D600FFF7EF00FFCE8C00E7BD7B00E7DECE00FFE7DE00FFEFDE00FFEF
      DE00FFEFDE00FFF7EF00FF940000FF00FF00FF00FF00FF00FF00FF00FF00B5AD
      B500CECECE00F7F7F70073B5730029AD31009CEFB5004ADE84004ADE7B0042DE
      7B004ADE730042DE7B004ADE7B004ADE7B004ADE7B004ADE7B0042DE7B004ADE
      7B0042DE7B004ADE7B004ADE7B0042D673007BE79C009CEFB5009CE7B50042B5
      4A0052AD5200FF00FF00FF00FF00FF00FF00FF00FF005AC663005AC66300C6FF
      D6006BEF9C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF
      8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C009CF7B500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009CF7B5005AEF8C005AEF8C005AEF8C005AEF
      8C00C6F7D60042B5420042B54200FF00FF00FF00FF006B7BE7006373EF00ADB5
      FF0084A5FF0084A5FF0094A5FF006B7BF700294ADE009CADFF00949CFF0084A5
      FF0084A5FF008494FF00B5BDFF0094A5F7001031DE008C9CEF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF003952CE007384F70094A5FF0084A5FF0084A5
      FF00ADB5FF006373EF006B7BE700FF00FF00FF00FF00FF9C0000FFF7F700FFEF
      DE00FFEFDE00FFEFDE00E7DECE00E7BD8C00FFCE8C00FFFFF700FFE7CE00FFE7
      CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFE7CE00FFFFF700FFD69400E7BD8C00EFD6BD00FFEFDE00FFEF
      DE00FFEFDE00FFF7F700FF9C0000FF00FF00FF00FF00FF00FF00C6BDD6009C9C
      B500E7DEF700D6D6E700188429009CEFB50084D6B5004ADE7B004ADE7B004ADE
      7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE
      7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B0084D6B5009CEF
      B50039A55A00FF00FF00FF00FF00FF00FF00FF00FF00ADDEAD0042B54200C6F7
      D60084F7AD006BEF9C0063EF94005AEF8C005AEF8C005AEF8C005AEF8C005AEF
      8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C005AEF8C009CF7
      B500FFFFFF00FFFFFF00FFFFFF009CF7B5005AEF8C0063EF94006BEF9C0084F7
      AD00C6F7D60042B54200ADDEAD00FF00FF00FF00FF00ADBDEF00395AE700C6D6
      FF0094ADFF008CA5FF0084A5FF00ADBDFF00849CF7009CB5FF00849CFF0084A5
      FF0094ADFF00C6CEFF00A5B5F7001031DE009CADEF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00425ADE00ADBDFF0084A5FF008CA5FF0094AD
      FF00C6D6FF00395AE700ADBDEF00FF00FF00FF00FF00FF9C0000FFF7F700FFF7
      E700FFEFE700EFDED600E7BD8400FFD69400FFF7EF00FFEFDE00FFEFD600FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEF
      DE00FFEFDE00FFEFD600FFEFDE00FFF7EF00FFD69400E7BD8400EFDED600FFEF
      E700FFF7E700FFF7F700FF9C0000FF00FF00FF00FF00FF00FF00ADADAD00D6D6
      D600FFFFFF006BAD6B004AB54A00ADEFC60063E78C0052E784004AE7840052E7
      84004AE784004AE784004AE784004AE77B004AE784004AE784004AE784004AE7
      7B0052E7840052E77B004AE77B0052E784005AE78C006BE794008CEFAD00C6F7
      CE0010A51800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00108C2900C6FF
      D6008CF7B5006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF
      9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF
      9C008CFFB500B5FFCE008CFFB5006BF79C006BF79C006BF79C006BF79C009CE7
      B500CEFFDE00108C2900FF00FF00FF00FF00FF00FF00FF00FF001029DE008C9C
      F700A5ADFF009CADFF009CADFF009CADFF0094A5FF009CADFF009CADFF00849C
      FF00B5BDFF00949CF7001031DE008C9CEF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF001839DE007B94F70094A5FF009CADFF009CADFF00A5AD
      FF008C9CF7001029DE00FF00FF00FF00FF00FF00FF00FF9C0000FFF7F700FFF7
      E700EFDED600E7BD8C00FFCE8C00FFFFF700FFF7E700FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFF7E700FFFFF700FFD69400E7BD8C00EFD6
      BD00FFF7E700FFF7F700FF9C0000FF00FF00FF00FF00CECECE00B5B5B500FFFF
      FF00FFFFF700089C0000B5EFBD00A5EFBD0073EF9C0063E794005AE78C005AE7
      8C0052E78C0052E7840052E7840052E7840052E784004AE7840052E7840052E7
      840052E7840052E7840052E784005AE78C006BE794007BEFA500BDF7D60073CE
      73006BBD6B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008CCE8C005AC6
      6300C6FFD6008CF7B5007BF7A5006BF79C0063F794005AF794005AF794005AF7
      8C005AF794005AF794005AF794005AF794005AF794005AF794005AF794005AF7
      94005AF794005AF794005AF7940063F794006BF79C006BF79C006BF79C00CEFF
      DE005AC663008CCE8C00FF00FF00FF00FF00FF00FF00FF00FF008C9CE7005A73
      E700CED6FF00A5B5FF009CADFF0094ADFF008CA5FF008CA5FF008CA5FF00BDCE
      FF009CADF7001031DE0094A5E700FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF002142DE007B94EF00B5C6FF0094ADFF009CADFF009CB5FF00CED6
      FF005A73E7008C9CE700FF00FF00FF00FF00FF00FF00FF9C0000FFF7F700EFE7
      E700E7BD8C00FFD69C00FFFFF700FFF7EF00FFF7EF00FFF7EF00FFF7E700FFF7
      E700FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7
      EF00FFF7E700FFF7E700FFF7EF00FFF7EF00FFF7EF00FFFFF700FFD69C00E7BD
      8C00EFE7DE00FFFFF700FF9C0000FF00FF00FF00FF00B5B5B500E7E7E700FFFF
      FF00CECED60039A5390094DE9C00BDF7D6008CEFAD0073EFA5006BEF940063EF
      940063EF940063EF8C005AEF94005AEF8C005AEF8C005AEF8C005AEF8C0052EF
      8C0052EF8C0052EF8C005AEF8C0063EF940073EF9C00A5F7BD00ADEFC600089C
      0800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00109C
      100094DE9C00B5FFCE008CF7B5007BF7AD0073F79C006BF79C0063F794005AF7
      940063F794005AF794005AF794005AF794005AF794005AF794005AF7940063F7
      94005AF7940063F7940063F794006BF79C007BF7A5008CFFB500BDF7CE0094DE
      9C00109C1000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF001039
      DE0094A5F700C6D6FF00ADB5FF009CB5FF0094ADFF0094ADFF00ADC6FF008494
      F7001031DE006373B500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00314A
      BD004263E7008CA5EF00BDCEFF009CADFF009CB5FF00A5B5FF00C6D6FF0094A5
      F7001039DE00FF00FF00FF00FF00FF00FF00FF00FF00FF9C0000EFE7E700E7C6
      8C00FFD69C00FFFFFF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7F700FFF7
      F700FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFF7
      EF00FFF7F700FFF7F700FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFFFFF00FFD6
      9C00E7C68C00EFE7E700FF940000FF00FF00FF00FF00DEDEDE00A59CA500DEDE
      DE00A5A5A500ADD6AD0021A52900CEF7DE00B5F7CE0094F7B50084F7AD007BEF
      A5007BEF9C0073EF9C006BEF9C0073EF9C006BF794006BF7940063EF940063EF
      94005AEF8C005AEF940063EF94006BEF9C0084F7AD00C6F7D6004ABD520084CE
      8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5DE
      B50039B54200ADEFBD00B5FFCE0094F7B5007BF7AD0073F79C006BF79C006BF7
      9C0063F7940063F7940063F7940063F7940063F7940063F7940063F7940063F7
      94006BF79C006BF79C0073F7A50084F7AD0094F7B500B5F7CE00ADEFBD0039B5
      4200B5DEB500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5BD
      EF00395AE700B5C6F700C6CEFF00ADBDFF00A5BDFF009CB5FF00A5B5FF00BDC6
      FF008494F7004A6BE700294ADE000831DE001031DE00294ADE00526BE700849C
      EF00BDCEFF00B5C6FF00A5B5FF00A5BDFF00ADBDFF00BDCEFF00B5C6FF00395A
      E700B5BDEF00FF00FF00FF00FF00FF00FF00FF00FF00FF940000EFC69400FFD6
      9C00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFFF00FFFFF700FFFFF700FFFF
      FF00FFD69C00EFC68C00FF9C0000FF00FF00FF00FF00FF00FF00E7E7E700BDBD
      BD00E7E7E700FF00FF006BBD6B0052BD5200D6FFDE00BDF7D600A5F7BD009CF7
      BD0094F7B5008CF7AD008CF7AD0084F7AD0084F7AD007BF7AD0073F79C0073F7
      9C006BF7940063F794006BF79C007BF7A500B5F7CE009CE7A50039AD3900FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0094CE940052BD5A00BDF7CE00B5F7CE009CFFBD008CFFB5007BFFAD0073FF
      A50073FF9C006BF79C006BF79C006BF79C006BF79C006BF79C006BF79C0073FF
      9C0073FFA5007BF7A5008CFFB5009CFFBD00B5FFCE00BDF7CE0052BD520094CE
      8C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00949CEF005273E700C6CEFF00C6D6FF00B5C6FF00ADBDFF00ADBDFF00A5BD
      FF00BDCEFF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00C6D6FF00BDCE
      FF00ADBDFF00A5BDFF00ADBDFF00B5C6FF00C6CEFF00C6CEFF00526BE700949C
      E700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF9C0000FFD6A500FFFF
      FF00FFFFFF00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFF
      FF00FFFFFF00FFD6A500FF940000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF006BBD6B004AB54A00C6F7D600D6FFDE00CEFF
      DE00BDFFD600B5F7D600ADF7C600A5FFC600A5F7BD009CF7BD0094F7B5008CF7
      AD007BF7AD007BF7A50084F7AD0094F7B500C6FFD60029A52900FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0084CE7B0052BD5A00B5EFC600C6FFD600A5FFC60094FFB5008CFF
      B50084FFAD0084F7AD007BFFAD007BF7A5007BF7A5007BFFAD0084F7AD0084FF
      AD008CFFAD0094FFBD00A5FFBD00C6FFD600B5EFC60052BD5A007BCE8400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF007B94EF00526BE700BDC6F700CEDEFF00B5CEFF00B5C6FF00B5C6
      FF00B5C6FF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADC6
      FF00B5C6FF00B5C6FF00BDCEFF00CEDEFF00BDC6FF005273E7008494E700FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FFB55A00FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFEFD600FFBD6300FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF007BC68400009C08004AB54A0084D6
      8400ADE7AD00B5E7BD00DEFFE700D6FFE700D6FFE700D6FFDE00CEFFDE00A5FF
      C6009CF7BD0094F7B50094F7B500C6FFD60084D6940052BD5A00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0094CE940042B5420094DE9C00D6FFDE00C6FFD600A5FF
      C6009CFFBD009CFFBD0094FFB50094FFB50094FFB50094FFB5009CFFBD009CFF
      BD00ADFFC600BDFFD600D6FFE70094DE9C0039B5420094CE9400FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF008C9CE700425AE7009CADF700DEE7FF00D6DEFF00BDCE
      FF00BDCEFF00BDCEFF00BDC6FF00B5C6FF00B5C6FF00B5CEFF00BDCEFF00BDCE
      FF00BDCEFF00CEDEFF00DEE7FF0094ADEF00425AE7008C9CEF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FFE7C600FFBD5A00FF94
      0000FF940000FF9C0000FF9C0000FF9C0000FF940000FF9C0000FF940000FF94
      0000FF940000FF940000FF940000FF940000FF940000FF940000FF940000FF94
      0000FF940000FF940000FF9C0000FF940000FF9C0000FF9C0000FF9C0000FF94
      0000FF940000FFC66B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006BC6
      6B0042AD390031AD3100009C00000094000021A518004AB54A007BCE8400CEFF
      D600D6FFDE00ADFFC600B5FFCE00C6F7D60018A51800FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00B5DEB500109C10005AC66300A5E7AD00DEFF
      E700D6FFDE00C6FFDE00B5FFD600B5FFCE00B5FFCE00B5FFD600C6FFDE00D6FF
      DE00D6FFE700A5E7AD0063C66300109C1000B5DEB500FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00B5BDEF001039DE00637BE700A5B5F700DEE7
      FF00DEE7FF00D6DEFF00CED6FF00C6D6FF00C6D6FF00CED6FF00D6DEFF00DEE7
      FF00DEE7FF00A5B5F700637BE7001039DE00B5BDF700FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0073C67300109C
      10008CD69400DEFFE700DEFFE7006BC66B0073C67300FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008CD69400109C100042B5
      4A007BCE7B009CE7A500BDEFCE00D6FFDE00D6FFDE00BDEFCE009CE7A5007BCE
      7B0042B54A00109C10008CD69400FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0094A5EF001031DE004263
      E7007B8CEF00A5B5F700C6CEFF00DEE7FF00DEE7FF00C6CEFF00A5B5F7007B8C
      EF004263E7001031DE0094A5EF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0052BD5A0084D68C00C6EFCE00009C0800FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5DE
      B5007BC673004AB54A0021A52100089C0800089C080021A521004AB54A007BC6
      7300B5DEB500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5BD
      F700738CE7004A63E7002142DE000831DE000831DE002142DE004A63E700738C
      E700B5BDF700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF006BC66B0021A521008CCE8C00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF006B7F9C006477930063769300FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00603D43005F3C44005E3D
      4300603D4400603D4400603D4400603D4400603D4400603D4500603D4500603D
      4500603D4500603D4500603D4400603D4400603D4400603D4400603D4400603D
      440059353C007F606800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00687DA00046699D003664A3003F70B2004579
      C4004A81CA004981C9004780C800477AC3003E69AE00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00399C3900009400000094000000940000009400000094000000940000399C
      3900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007B8C
      BD004A5ABD002942C6001031CE000829D6000829D6001031CE002942C6004A5A
      BD007B8CBD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00603C440098818600BFB3B600BFB1
      B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2
      B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2B500BFB2
      B500BFB2B40071535900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF003D659E003874BF00538ED00080AFE500BBDFFF00E1F6FF00E6FA
      FF00E0FBFF00EEFEFF00EEFEFF00EAFCFF00CEEBFF002C69C200FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004AAD
      4A006BCE7B009CE7B50094E7AD008CDEAD008CDEA50094E7AD009CE7B50073CE
      7B0052AD4A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006373BD000831DE00214A
      E700395AEF005273F700637BF7006B84FF006B84FF00637BF7005273F700395A
      EF00214AE7000831DE006373BD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00603C4300CABEC000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0081666B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002D61
      A500347BCE007FB9EC00D1F1FF00E7FFFF00DEFFFF00CEF7FF00C4F2FF008BDB
      FF006AD0FF00B4ECFF00C9F2FF00CBF2FF00AAEEFF00439EE8003F609700FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000A5E7BD0052D67B0042CE730031CE6B0039CE6B0042CE730052D68400ADE7
      BD0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF008494C6000831DE003152E7005A73F700738C
      FF006B84FF004263FF002152FF001042FF001042FF002152FF004263FF006B84
      FF00738CFF005A73F7003152E7000831DE008494C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF005F3D4300C0B5B700FFFFFF00FEFE
      FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFF
      FF00FFFFFF007D636900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004C70A2003277C70076B7
      F000D6FAFF00D6FCFF00B7EEFF0099E1FF008ADEFF0091E1FE0099E3FF0076D3
      FE005CC8FF0095DDFF00BBEDFF00C6F0FE00B3ECFF0058C4FD002D69B400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000ADE7BD0052D6840042CE730039CE6B0039CE6B0042CE730052D68400ADE7
      BD0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF006373C600214ADE005273EF00738CFF004263FF001039
      FF001039FF000839FF000839FF001039FF001039FF000839FF000839FF001039
      FF001039FF004263FF00738CFF005273EF00214ADE006373C600FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF005E3B4300BEB3B600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007C626800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF002C66B2004696E500BEEDFF00CEFB
      FF00A6E7FF006CCDFF0067CFFF007CDBFE0082DDFE008CDFFE0097E3FE0084DB
      FE005FC9FE0083D9FF00B6ECFE00BBEDFD00BBEBFF006ED5FF002A7BCA00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000ADE7C60052D6840042CE730039CE6B0039CE6B0042CE73005AD68400ADEF
      BD0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005A6BC6003152E7006B84FF005273FF001842FF001042FF001042
      FF001042FF001042FF001042FF001042FF001042FF001042FF001042FF001042
      FF001042FF001042FF001842FF005273FF006B84FF003152E7005A6BC600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF005E3B4300BFB3B500FFFFFF00FEFF
      FF00EEE7E800E0D8D500E3D7D800E3D8DA00E2D9D800E3D9D900E3D9D900E3D9
      D900E3D9D900E3D9D900E2D8D800E3D8D800E3D8D700E3D9D800F7F5F500FFFF
      FF00FFFFFF007C626800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00256FC20075BFF500C5FAFF00A4E9FF0072D7
      FF005DD3FE005FCEFE005ECAFF0075D8FF0080DBFF0085DDFF008FE0FF008ADD
      FF0063CBFF0077D3FF00ACE9FF00B3E9FF00B9EAFD0074D8FF002C84D3006C7D
      9F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000ADEFBD005AD6840042D6730039CE6B0039CE6B0042D673005AD68400A5EF
      BD00009C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF006B7BC6003152E700738CF700426BFF001842FF001842FF001042FF001842
      FF001042FF001842FF001042FF001842FF001842FF001842FF001842FF001842
      FF001842FF001842FF001842FF001842FF00426BFF00738CFF003152E7006B7B
      C600FF00FF00FF00FF00FF00FF00FF00FF005F3D4400C0B4B600FFFFFF00FFFF
      FF00D3C4C500B69E9C00BCA2A200BCA2A100BBA4A200BCA3A100BCA3A100BCA3
      A100BCA3A100BCA3A100BBA4A200BDA5A200BAA2A000BDA5A300EDE8E600FFFF
      FF00FFFFFF007D626800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF002278D50091D8FD00BCF4FF0089DDFF004BCDFE004CCD
      FF005BD1FF0064D6FF005ECCFE0066CDFF0079DAFE007FDCFD0087DEFF0086DD
      FE0069CCFF006ECFFF00A0E6FF00ACE7FF00B5EBFE007ADAFF003491DE00647F
      AC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000ADEFBD005AD6840042D6730039D66B0039D66B0042D673005AD68400ADEF
      BD0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0094A5
      D600214ADE006B84FF004A6BFF00214AFF00214AFF00184AFF00184AFF00184A
      FF00184AFF00214AFF00184AFF00214AFF00184AFF00214AFF00214AFF00184A
      FF00184AFF00184AFF00214AFF00184AFF00184AFF004A6BFF006B8CF700294A
      DE0094A5D600FF00FF00FF00FF00FF00FF005F3C4400BFB3B500FFFFFF00FFFE
      FF00E3D7D800CEBEBE00D1C1C100D2C1C000D1C2C100D1C1BF00D1C1BF00D1C1
      BF00D1C1BF00D1C1BF00D1C1C000D2C1C000D0C1BF00D0C2C000F2EEEE00FFFE
      FF00FFFFFF007D626800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF003888D9009CE5FF00A1EAFF005ACDFE004CC0FF0060CDFE0050CF
      FE0055CFFE005BD3FF0062D1FF0059C6FE006DD4FE007ADBFE0080DBFF0084DC
      FF006ACFFE0069CDFF0099E3FD00A4E6FF00AEEAFF0080DBFF0037A6F300446E
      A700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000A5EFBD0052D684004AD6730039D6730039D6730042D6730052D68400A5EF
      BD0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF001031
      D600637BF7006384FF002952FF002152FF002152FF007B94FF00B5BDFF007B94
      FF002152FF002152FF002152FF002152FF002152FF00214AFF002152FF00214A
      FF00214AFF00214AFF002152FF00214AFF002952FF002952FF006B84FF00637B
      F7001031D600FF00FF00FF00FF00FF00FF005F3D4400BFB3B600FFFFFF00FDFB
      FC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFEFD00FFFC
      FD00FFFFFF007D626800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF002685E10092E5FF0090E8FF003EC8FF0021C3FE0038C1FF0057C3FE0060CC
      FE004CCEFE0052CFFF005FD3FF0057CAFF005DC9FE0071D9FE0078D8FF0080DC
      FE0071D2FE0069CEFF0091DFFE009BE4FE00A9E8FF0085DDFF003DB4FF00396C
      AC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      00009CEFB50052DE7B0042D67B0042D6730042D6730042D6730052D67B00A5EF
      B50000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00738CD6004263
      EF008CA5FF00315AFF002952FF002952FF007B9CFF00FFFFFF00FFFFFF00FFFF
      FF008494FF002952FF002952FF002952FF002952FF002952FF002952FF002952
      FF002952FF002952FF002952FF002952FF002952FF002952FF00315AFF008CA5
      FF004263EF00738CD600FF00FF00FF00FF005F3D4400BFB3B600FFFFFF00FDF9
      FA00FFFDFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFEFD00FEFB
      FB00FFFDFD007C626700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF005AB9F8007FE6FF001ABBFF000DBBFF0029CAFF0030CBFF0036C3FE0053C4
      FF005ACCFE004BCDFF0054D0FF005BD0FF0052C4FF0066D1FF0070D8FE0079D9
      FE0076D5FF0068CCFF0087DBFE0095E2FE00A4E7FE0086DBFF0037ACFF00316C
      AF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      00009CE7B5004ADE7B0042D67B0042D6730042D6730042D673004ADE7B009CE7
      B50000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000831DE006B8C
      F7006384FF00315AFF00315AFF00315AFF00B5C6FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00849CFF00315AFF00315AFF00315AFF00315AFF00315AFF00315A
      FF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF00315AFF006384
      FF006B8CF7000831DE00FF00FF00FF00FF00603D4400C0B3B600FFFFFF00FEF9
      FA00ECE0E200DFD0D100DED4D300DFD4D300E0D2D300DFD3D200DED3D200DED4
      D200DDD3D200DED3D200DFD2D300DED3D400DFD4D400E5D5D600F7F3F300FFFF
      FF00FDFAFB007C606600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF001B81E7003B92DB003888D100318BD7000F96ED0020B8FB0030CCFF0036C6
      FF0053C5FE0051CCFF004ACCFF0058D1FF004EC7FF0056C8FE006AD8FF0071D8
      FE0073D5FF0069CCFE007FD9FF008FE1FE009AE6FE006EC6FF002EA1FF002D6E
      B400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00429C42000094
      0000009400000094000000940000009400000094000000940000009400000094
      000094EFB5004ADE7B0042DE7B0042DE7B0042DE7B0042DE7B004ADE7B0094EF
      B5000094000000940000009C00000094000000940000009C000000940000009C
      000000940000399C3900FF00FF00FF00FF00FF00FF009CADDE003152DE0094AD
      FF00426BFF003963FF003963FF00395AFF008CA5FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008CA5FF00395AFF003963FF003963FF003963FF00395A
      FF003963FF003963FF00395AFF003963FF00395AFF003963FF003963FF00426B
      FF0094ADFF003152DE009CADDE00FF00FF005F3D4400C0B5B600FFFFFF00FFFC
      FD00D1BFC000B2999800B79E9E00B49D9D00B69D9C00B79E9D00B79F9E00B69E
      9D00B79F9E00B69E9D00B99D9E00B7A09E00B79D9C00A8908E00D9CDCA00FFFA
      F800FFFFFF0080616900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00267BD10016A3F1002ECB
      FF0038C7FF0051C4FE004BCBFE004CCEFF0052CCFF0048C1FE0060D0FE006CD5
      FE006FD5FE006BD0FE007EDCFE008DE3FF0080D2FE0051B0FF00259AFF00346D
      B300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004A9C5A0042DE7B00A5EF
      B500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EF
      B500A5EFB50042DE7B0042DE7B0042DE7B0042DE7B0042DE7B0042DE7B00A5EF
      B500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EFB500A5EF
      B500A5EFB50042DE7B004A9C5A00FF00FF00FF00FF006B7BDE005A73EF0094AD
      FF004A6BFF004263FF004263FF004263FF004263FF008CA5FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008CA5FF004263FF004263FF004263FF004263
      FF00426BFF004263FF004263FF004263FF004263FF004263FF00426BFF004A6B
      FF0094ADFF005A73EF006B7BDE00FF00FF005F3D4400BFB4B600FFFFFF00FDF8
      F900DFD5D500CEBDBC00D0C0C000CEC0C000D0C0C000D2C1C200D3C0C200D3BF
      C200D3BFC200D3C0C300D6C4C300D8C8C600B5A4A3009F908A008C9AA3006190
      AC0096BBD1007A5B6200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00407CC700179A
      ED002DCBFE0039C6FF004BC3FE0047CCFF004ECFFD0047C3FF004BC6FE0067DB
      FE0071DAFF006DD2FE006FD1FF006DCBFF0056B0FD0040A5FF001D93FB004576
      B100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000940000ADEFC60063E7
      8C0052E784004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE
      7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE
      7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B004ADE7B0052E7
      840063E78C00C6F7D60000940000FF00FF00FF00FF003952DE006B84EF00738C
      FF004A63FF004263FF004263FF004263FF004263FF004263FF008494FF00E7E7
      FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF008494FF004263FF004263FF004263
      FF004263FF004263FF004263FF004263FF004A63FF004263FF004263FF004A6B
      FF00738CFF006B84EF003952DE00FF00FF005F3D4400BFB3B600FFFFFF00F8F3
      F500F7F3F500F7F4F400F9F2F300F8F5F600F7F5F400F8F3F400F9F3F400F9F3
      F500FAF4F500F7F1F100DDD8D900D8CDCC00CCC3BE009CA7AF000F699E000A6F
      AC0000659F00124B7500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004980
      C7001399ED002FCFFF003ECBFF004BCCFE0048D2FF0046C6FE0038B3FE004ABC
      FE0045AFFF00399EFE003497FE00399AFE004BA6FF0042B0FF00228BE8005C80
      B000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008C1800C6F7D60052E7
      840052E7840052E7840052E7840052E7840052E7840052E7840052E7840052E7
      840052E7840052E7840052E7840052E7840052E7840052E7840052E7840052E7
      840052E7840052E7840052E7840052E7840052E7840052E7840052E7840052E7
      840052E78400C6F7D600008C1800FF00FF00FF00FF001842D6009CADF700738C
      FF005A73FF005273FF005273FF004A73FF005273FF004A73FF005273FF0094AD
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0094ADFF005273FF005273
      FF005273FF004A73FF004A73FF004A73FF005273FF005273FF005273FF005273
      FF00738CFF009CADF7001842D600FF00FF005E3C4300BEB2B500FFFFFF00F8F0
      F200FBF5F600FCF8F800FDF7F900FCF7F800FBF7F700FAF5F600FBF7F700FBF6
      F700FDF9FA00DCD8D900BBB5B400889DA9004C7D9D0033739B00086CA60048A9
      E0003898CF001375AC002276A300FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF004181C8001192F2001E9DFF002292FF001A84FF001378FE001277FE00187E
      FF002088FF002F94FF003BA1FF004BB1FE0067C8FF003DB4FF001F7AD700FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000940000C6F7D6006BE7
      9C005AE78C0052E784005AE78C0052E78C0052E78C0052E7840052E7840052E7
      840052E7840052E77B0052E784004AE7840052E784004AE7840052E7840052E7
      840052E7840052E7840052E78C0052E7840052E78C0052E7840052E784005AE7
      8C0073E79C00C6F7D600009C0000FF00FF00FF00FF000829DE009CADFF006384
      FF005273FF005273FF00526BFF00526BFF00526BFF00526BFF00526BFF00526B
      FF008C9CFF00E7E7FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF008C9CFF00526B
      FF004A73FF00526BFF004A6BFF00526BFF00526BFF00526BFF00526BFF005273
      FF006384FF009CADFF000829DE00FF00FF005F3C4300BEB2B400FFFFFF00F7F2
      F300ECE4E400E3D5D600DED1D100E9D9D700EADDDB00E3D8D900E4D8DA00E5DA
      DB00E2D7D800BFB0AF008A95A10009699F001484C1001076AF00005B92003088
      BC0058B4E40044A5D9002384BC000D699D004681A300407EA3003D7C9E003D7E
      A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00276FCC000060F3000C7BFF001586FF00259CFE0030AFFF0032B0
      FD004EC9FE0060CDFE0064CEFF0065D1FF0051B6FF002596FD001D6FCE00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008C1800C6F7D6006BEF
      9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF
      9C0052E77B0052E77B0052E77B0052E77B0052E77B0052E77B0052E77B0052E7
      7B0052E77B006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF9C006BEF
      9C006BEF9C00C6F7D600008C1800FF00FF00FF00FF000831D600ADBDFF007B94
      FF006B84FF006384FF005A7BFF005A84FF005A7BFF005A7BFF00637BFF005A7B
      FF005A7BFF009CB5FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009CB5
      FF005A7BFF00637BFF006384FF00637BFF005A84FF005A7BFF00637BFF006B84
      FF007B94FF00ADBDFF000831D600FF00FF005F3C4400C0B3B600FFFFFF00FBF4
      F400CBBAB60097807E008B7E7D0084818400B59F9E00C5ACAA00BEA6A600B09A
      9A0097848000A0817A00406A84001286BA0050BFF30040ACE1001F7EBA001075
      A90047A7DC005FB6E3004FAEE000318FCB002382B9002181B8002886C000227F
      B8002077AC00317BA300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF002577D0000C81F8001B8FFF002090FE002094FE001C8C
      FE002592FE002691FF002B92FE00349BFE002C9AFF000874E900FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000940000C6F7D60094EF
      B5008CEFAD0084EFA50084EFA50084EFA50084EFA50084EFA5007BEF9C006BEF
      9C0063EF94005AE78C0052E78C0052E7840052E7840052E78C005AE78C0063E7
      94006BEF9C007BEFA50084EFA50084EFA50084EFA50084EFA50084EFA5008CEF
      AD0094EFB500C6F7D60000940000FF00FF00FF00FF002139DE0094A5F7008494
      FF006B84FF00738CFF00637BFF00738CFF00738CFF00738CFF00738CFF00637B
      FF00738CFF005A7BFF0094A5FF00E7E7FF00E7E7FF00E7E7FF00E7E7FF00E7E7
      FF0094A5FF006373FF00637BFF005A7BFF00637BFF00637BFF00637BFF006B7B
      FF008494FF0094A5F7002139DE00FF00FF005E3C4300C0B4B600FFFFFF00F9F2
      F300BDA9A8008178800023668C000C64990025638B009C969B00CEB3AE009E89
      8800868083003E6A89000D5D8B000A70A70063BAE40068C2EC004AB3E5003CA9
      DF0038A4DE0040A8DF0048A7DD003A9EDA00399EDA003F98D9003A99D6003C9F
      DA003C9CD800197AB200397DA400FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF001A68D0000679F8001795FF001889FF001D92
      FF001C91FE0033A4FF0044AEFF003EB4FF00118CF7003170BE00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008C1800C6F7D6007BEF
      A5007BEFA5007BEFA5007BEFA5007BEFA5007BEFA5007BEFA5007BEFA5007BEF
      A5006BEF94006BEF940052E7840052E7840052E7840052E7840052E784006BEF
      94006BEF94007BEFA5007BEFA5007BEFA5007BEFA5007BEFA5007BEFA5007BEF
      A5007BEFA500C6F7D600008C1800FF00FF00FF00FF00425ADE008CA5EF00A5B5
      FF007B94FF007394FF006B8CFF006B8CFF006B8CFF006B8CFF006B8CFF006B8C
      FF006B8CFF006B8CFF006B8CFF00ADB5FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00A5BDFF006B8CFF006B8CFF006B8CFF006B8CFF007394FF007B94
      FF00A5B5FF008CA5EF00425ADE00FF00FF005F3D4300C0B3B600FFFEFD00FBF0
      F000CDC1C0003F7498002A90C2006AC3EC00248BC00009639500859CAC00C6B6
      B1003C799C000E7DB8002892C9000A68A1003DA1D00073C9EF0075C6EF005ABA
      E90041AFE30034A9E10039AADF0048B0E1004BB4E50048B7E80048BAE50046BA
      E80043BEE9003AB3E1001372A700FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF002680CF0017AAF3002ABFFE0038CB
      FF002AC0FF0044CDFF0052C4FD00108BEC00296CBC00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF005ABD5A008CD69400DEFF
      E700D6F7E700D6FFE700D6FFE700D6F7DE00D6F7E700D6F7DE00CEF7DE00BDF7
      D600B5F7C6006BEF94005AEF8C0052EF8C0052EF8C005AEF8C006BEF9400ADF7
      C600BDF7D600CEF7DE00D6F7DE00D6F7E700D6F7DE00D6FFE700D6FFE700D6FF
      E700DEFFE7008CD694005AB55A00FF00FF00FF00FF006B7BE7006373EF00ADB5
      FF007B94FF00738CFF007B94FF007B94FF007B94FF007B94FF007B94FF007B94
      FF007B94FF007B94FF007B94FF006B84FF00A5ADFF00E7E7FF00E7E7FF00E7E7
      FF00E7E7FF00E7E7FF009CADFF006B84FF007B94FF007B94FF00738CFF007B94
      FF00ADB5FF006373EF006B7BE700FF00FF005E3B4200BFB2B500FFFDFC00F7ED
      EC00E3DBDC00246C980073B9E100ADE4FB008CD2F4004AA5D50004689F004A7E
      9D001D709F005ABAE60055B9E8003CA4DE0040A9DE004FB8E90068C1EB007CC8
      EC004EB6E9004FBBE80068C4EC0069C8ED0063C6ED005FC5EE0056C5EB0054C1
      EB0054C2EC0051C5EE001D81B600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF004C83C9001178DA00148C
      ED001185E4000D6FCB002877C8003F68800064654E00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF005AAD6B000094
      0000009400000094000000940000009400000094000000940000009400000094
      0000B5F7CE0073F79C005AEF8C005AEF8C005AEF8C005AEF8C0073F79C00B5F7
      CE00009400000094000000940000009400000094000000940000009400000094
      00000094000052AD6B00FF00FF00FF00FF00FF00FF00ADBDEF00425ADE00C6D6
      FF0094ADFF008CA5FF00849CFF007B9CFF007B9CFF007B9CFF007B9CFF007B94
      FF007B94FF007B9CFF007B94FF007B94FF007B9CFF00B5BDFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00ADBDFF00849CFF007B9CFF0084A5FF0094AD
      FF00C6D6FF00425ADE00ADBDEF00FF00FF00603E4500C2B6B800FFFFFF00F3EC
      E900FFF4F40081A9C3001674A80087C5E400B2E0F9009BD8F50068BCE3001579
      AF00005990004698C60087D0F4006BC5EE0042ACE10044AADD004AB5E30054BA
      E7005BBEE9007ACCED007DCBEF0074C9ED006ECAEB006FC7EC006BC7ED0065C4
      EC0060C4EC0063CCF2002D8EC1006497B500FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF005D624800ACAD7900B7B06F00605D2E00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000B5F7CE0073F79C0063EF94005AEF8C005AEF8C0063EF940073F79C00B5F7
      CE00009C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF001029DE008C9C
      F700A5B5FF008C9CFF008CA5FF008CA5FF008CA5FF007B94FF008CA5FF007B94
      FF007B94FF008CA5FF007B94FF008CA5FF007B94FF007B8CFF00A5B5FF00E7E7
      FF00E7E7FF00E7E7FF00E7E7FF00BDC6FF007B94FF008494FF008C9CFF00A5B5
      FF008C9CF7001029DE00FF00FF00FF00FF005E3B4100B8A9AD00F9F9FA00F1EB
      EB00F5ECEE00F6ECEC0075A1BC000B699D006EB2D700B8E3F900B1E2F70083CF
      EF003A93C100005991002D83B00086CBEB007DCDF1004DB2E3004CB3E50053B9
      E90077C7ED0090D3F00085D0EF0082CEEE007FCCEF007ECCEE0076CBEC0075CA
      EC006EC6EC0073D0F1003D9ECB002A7AA700FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF008D8B740097966100D6D6AA008C8C5B006C6B5800FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000BDFFCE0073F79C0063F794005AF794005AF78C0063F7940073F79C00B5F7
      CE0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00949CE7005A73
      EF00CED6FF00A5B5FF0094ADFF0094ADFF008CA5FF008CA5FF008CA5FF008CA5
      FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF008CA5FF00B5C6
      FF00FFFFFF00FFFFFF00FFFFFF00BDCEFF0094ADFF009CADFF00A5B5FF00CED6
      FF005A73EF00949CE700FF00FF00FF00FF00FF00FF007F6267008A707600896F
      7400866F73008D71770096747900585B6C001E648F004995C000B2DDF500C4EB
      FD00A6DEF9005DAED90003649C0009649B0073BBDA008CD1F60065C1E70064C0
      E90091D1F0009AD6EF0097D3F00091D2EF008ED0EE008CD0F00085D0F10084CE
      EF007ECBEF0080D2EF0055B1DB0008689F00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF007E805B00E7E6D200E0E0C20065673F00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009C
      0000B5F7CE0073F7A5006BF79C005AF794005AF7940063F7940073F7A500BDF7
      CE0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF001039
      DE0094A5F700C6CEFF00A5B5FF009CB5FF009CADFF0094ADFF0094ADFF0094AD
      FF0094ADFF0094ADFF0094A5FF008CA5FF0094ADFF0094ADFF0094A5FF0094AD
      FF00BDCEFF00D6DEFF00C6CEFF009CADFF009CB5FF00A5BDFF00C6D6FF0094A5
      EF001039DE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF003E81A9003586B1009ECE
      E500D6F1FE00BBE6FC007AC3E5002079AD00499FCA0079C9EE006DC5EA0096D3
      F200B1DFF400AADAF100A7D6F500A1D4F1009DD7EE009BD7F30096D2F20092D3
      EE008DD1F10092D4F20068BDE2000569A000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0082826600C1C1A600F1F1DC008484530070705C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000B5FFD60073F7A5006BF79C0063F7940063F794006BF79C0073F7A500BDFF
      CE00009C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5BD
      EF00395AE700B5C6F700C6CEFF00ADBDFF00A5B5FF009CB5FF009CB5FF009CAD
      FF009CADFF0094ADFF0094ADFF0094ADFF009CADFF009CADFF0094ADFF0094AD
      FF009CB5FF009CB5FF009CB5FF00A5B5FF00ADBDFF00BDCEFF00B5C6FF00395A
      DE00B5BDEF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00146D
      9F007CB4D100DAF3FD00D2EEFD009ED7F3007CCAED007CC8ED0085CEED00CAE8
      F400CAE5F700BEE4F800B9E0F800B5DEF300B1DEF300AFDCF300A8D9F200A1D8
      F100A1D7F400AADFF60065B5DD0005649C00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF007F7E5200B1B28000A0A06C006F6F4D00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000BDFFD6007BF7AD006BF79C006BF794006BF79C0073F7A5007BF7AD00BDFF
      D60000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00949CEF00526BE700C6CEFF00C6CEFF00B5C6FF00ADBDFF00A5BDFF00A5BD
      FF00A5B5FF009CB5FF009CB5FF009CB5FF00A5B5FF00A5B5FF00A5B5FF00A5B5
      FF00A5BDFF00A5BDFF00ADBDFF00B5C6FF00C6CEFF00C6CEFF00526BE700949C
      E700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF002C79A6005496BD00D6EBF700EBFBFF00A8DCF3007FCAEC008DCFEE00DDF0
      FA00DCEFFA00D4EBF900D0EAF700CAE8F500C6E5F800C1E3F600BCE2F200BDE3
      F700C3E9FC00A4DCF5002784B5004A87AA00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF008B8A6C00A2A16D00B7B78400818252008B8B
      8000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009C
      0000C6FFD6008CFFB50084FFAD007BFFA5007BF7A50084FFAD008CFFB500C6FF
      D600009C0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF007B94E7005273E700BDC6F700CEDEFF00BDC6FF00B5C6FF00B5C6
      FF00ADC6FF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADBDFF00ADC6
      FF00ADC6FF00B5C6FF00B5CEFF00D6DEFF00B5C6FF00526BE7008494E700FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF004A82A0003582AD00B1D3E400CDEFFD0090D1ED008CCEEF00EEFA
      FF00F0FBFE00E9F5F800EAF3FA00EAF7FF00E8F6FF00E1F2FE00D2ECFA00A2D4
      EA006FAFD6002D81B100417EA100FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0093946600BBBC88009C9C67007876
      5A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000CEFFDE009CFFBD0094FFB50094FFB50094FFB50094FFB5009CFFBD00CEFF
      DE0000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00949CEF00425AE7009CADEF00DEE7FF00CED6FF00BDCE
      FF00BDCEFF00BDCEFF00BDC6FF00BDCEFF00B5C6FF00B5C6FF00BDCEFF00BDCE
      FF00BDCEFF00CEDEFF00DEE7FF009CADF700395AE700949CEF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF002472A0005CA7CB009ED6F500CBE9FB00FFFF
      FE00FFFFFF00FFFFFF00FFFFFF00D8ECF400A2CBE00062A1C7003A89B400317F
      AA00367CA500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008B8C6600B4B48100BDBC88008080
      5400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094
      0000DEFFE700B5FFCE00B5FFCE00ADFFC600B5FFCE00ADFFCE00B5FFCE00D6FF
      E70000940000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00B5BDF7001039DE00637BE700ADB5F700DEE7
      FF00DEE7FF00D6DEFF00CED6FF00C6D6FF00C6D6FF00CED6FF00D6DEFF00DEE7
      FF00DEE7FF00ADBDF700637BE7001039DE00B5C6EF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF002A759F003B8DB900BEDDED00E6F3
      FA00C1D9E9008CB7D2005597BB001C77A7003481AC005B89A300FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A09F6D00CBCA97009898
      65008B897800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF005ABD
      5A0094D69400DEFFE700DEFFE700DEFFE700DEFFE700DEFFE700DEFFE70094D6
      94005ABD5A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0094A5EF001031DE004A63
      E7007B8CEF00A5B5EF00C6CEFF00DEDEFF00D6DEFF00C6CEFF00A5B5F7007B8C
      EF004263E7000831DE0094A5EF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF003A83AB000F679B002276
      A8000C689D002679A900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0095957600B3B37F009D9F
      6B00999A8B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF005ABD5A0000940000009C0000009C000000940000009C0000009400005ABD
      5A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5BD
      F700738CE7004A63DE002142DE000831DE000831DE002142DE004A63DE00738C
      E700B5BDF700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0096946A009E9F
      8100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000BFBF
      BF00C1C1C100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00B4A29A00AB968E00B89D8F00C4A49400C8AD9C00B09C9000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00CACA
      CA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CCCCCC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD946B00BD7B3900BD94
      6B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B87B
      6100AE5E3C009A3D1700912E010098300000972E0100962D00009A360400AC57
      2A00B5725800B08B8300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C9C9
      C900FFFFFF00FEFEFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C8C8C800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00BD7B4200DE8C2900EF9C4200DE8C
      2900BD844200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A5533500A4451D00A63E
      0600C8661F00E7893300F38E2D00F4892500F1832100EE832100EC801F00E470
      1700BE4B0000A84B14009C512D00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF003994B5000094
      CE00009CCE00009CCE000094CE00009CCE000094CE000094CE000094CE000094
      CE000094CE000094CE000094CE000094CE000094CE000094CE000094CE000094
      CE000094CE000094CE000094CE000094CE00009CCE000094CE00009CCE00009C
      CE000094CE003994B500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FFFFFF00C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00BD7B4A00DE8C2900FFBD6300FFB54A00FFC6
      6B00DE8C3100C6844A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00BD7455008F320E00BE612800E48E4700FFA9
      4C00FFA44100FF993B00FF9A3400FF973000FF942D00FF902A00FF8F1E00FF8C
      2200FF891C00FF870F00D6650B00AA410000A34B2C00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0042A5C60063CEEF0084E7
      FF0073E7FF0073DEFF006BDEFF006BDEFF006BE7FF006BE7FF006BE7FF006BE7
      FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7
      FF006BE7FF006BE7FF006BE7FF006BE7FF006BDEFF006BDEFF0073DEFF0073E7
      FF0084E7FF0063CEEF0042A5C600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00767676005C5C5C00C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C6C6C6005D5D5D0076767600FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00BD7B4200DE8C2900FFC66300FFAD3900FFA52100FFB5
      4A00FFCE8400DE943900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00A0452000A73D0F00E1955700FFB35B00FFAA4800FE9E
      4000FE9A3C00FD9B3C00FA973700FB943200FF923000FF8E2500FF902200FD87
      2200FF861F00FF871600FF861800FC7E1200C8510000A74A2800FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE0094E7FF0029D6
      FF0010CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CE
      FF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CE
      FF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0010CE
      FF0029D6FF0094E7FF000094CE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF006E6E6E00626262005252520058585800BDBD
      BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FFFFFF00BDBDBD005A5A5A0063636300616161006D6D6D00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00BD7B4A00DE8C2900FFC66B00FFAD4200FFA53100FFAD3900FFB5
      4A00FFC68400EFAD6300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF009E411700B1541F00F7B46A00FFB25900FFA24900FBA34200FBA2
      4400FE9D4100FC9A3300FB942C00FC912700FE8F2600FD902E00FB8B2B00FD8A
      1E00FA891D00FD861900FE7E0A00FE770100FF830500DE6105009A3A1300FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009CCE0094E7FF0031D6
      FF0018CEFF0010CEFF0010CEFF0010CEFF0008CEFF0008CEFF0008CEFF0008CE
      FF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CE
      FF0008CEFF0008CEFF0008CEFF0008CEFF0010CEFF0010CEFF0010CEFF0018CE
      FF0031D6FF0094E7FF00009CCE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF007C7C7C006B6B6B0075757500939393007777770061616100B2B3
      B300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00B2B2B200616060008988880090919100757574006C6B6B007B7D
      7C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00BD7B4A00DE8C3100FFC67300FFB54A00FFAD3100FFAD4200FFB55200FFC6
      7300FFD69C00DE944200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00A1471E00AC551F00F4BB7400FFB15A00FBA75500FFA64E00FCA54A00F79D
      3B00FD983100FFA04500FDA75500FCA55300FF9C4000FB8A2300FE861700FE8D
      1E00FB8B1900FC800E00F6943600F8A14E00FF801800FF800400E2630000A140
      1600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000094CE009CEFFF0031D6
      FF0021D6FF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CE
      FF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CE
      FF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0010CEFF0021D6
      FF0031D6FF009CEFFF000094CE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF006B6B6B00767676009292920096969600939393008A8A8A0065656500A3A3
      A300FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FDFDFD00A3A3A3006666660090908F009293950093969600929392007675
      76006A6A6A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD84
      4A00DE8C3100FFC67B00FFB55200FFAD4200FFAD4A00FFB55A00FFC67B00FFD6
      A500DE944A00DEA56300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BB71
      5300A03E1200F5C38200FFB96700FEAC5500FFAA5500FCA04600FEA34C00FEBE
      8700FEE4C900FFEFE100FDF6EE00FCF6EF00FFF0E200FDE4CA00FCBF8500FB90
      2F00FB7E0E00FAB46E00F6F2E600FDFFFF00FFE2BF00FA8B2700FF7B0300D052
      0000A8532F00FF00FF00FF00FF00FF00FF00FF00FF000094CE009CEFFF0039D6
      FF0021D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6
      FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6
      FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0021D6
      FF0039D6FF009CEFFF000094CE00FF00FF00FF00FF00FF00FF00FF00FF007979
      790087878700A4A4A4009F9F9F009F9F9F009E9E9E009F9F9F007F7F7F005858
      5800636363006363630063636300646464006464640064646400646464006262
      620063636300585858007F7F7F00A2A3A3009F9F9F009F9E9E00A09FA000A7A5
      A6008988880077787800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD844200DE8C
      3100FFCE8400FFBD6300FFB54A00FFB55200FFBD6300FFC68400FFD6A500DE94
      4A00DEA56300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009A31
      0300E0A97300FFC87200FEAC5C00FCAC5900FFA24C00FCBB7500FCEDD300FEFF
      FF00FBFFFF00FEFFFF00FFFFFF00FEFFFF00FDFFFF00FFFFFF00F6FFFF00FCEE
      D500F5C69C00FCFDFE00FAFFFF00FBFFFF00FCFFFF00FAB77800FE700000FF7C
      0400B3400000AD775900FF00FF00FF00FF00FF00FF00009CCE009CEFFF0042DE
      FF0029D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6
      FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6
      FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0021D6FF0029D6
      FF0042DEFF009CEFFF00009CCE00FF00FF00FF00FF00FF00FF007D7D7D009B9B
      9B00B0B0B000AAAAAA00AAAAAA00ABABAB00AAAAAA00ABABAB00ACACAC00A4A4
      A400A1A1A100A2A2A200A3A3A300A2A2A200A3A3A300A3A3A300A2A2A200A2A2
      A200A2A2A200A5A5A500ACACAC00ABABAB00ABACAB00AFACAE00AEACAD009DA9
      9C00B6B2B6009C9A9C007D7D7D00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C9C9C009C9C9C009C9C
      9C00A59C9C009C9C9C009C9C9C00A5A5A500FF00FF00AD734200DE8C3900FFCE
      8C00FFBD6B00FFB55A00FFBD6300FFBD6B00FFCE8C00FFDEAD00DE944A00DEA5
      6300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009F4C2600B46B
      3900FFD49600FCB46000FFB06000FBA94F00FDC99200FCFEF900FDFFFF00FBFC
      FE00FAFDFE00FCFEFC00FDFEFF00FEFDFF00FCFEFD00FBFCFE00FEFEFE00F8FF
      FF00FFFFFF00FFFDFD00FFFEFD00FCFFFE00FCFFFF00FECC9500FF770300FF7E
      0600EB660400A1431A00FF00FF00FF00FF00FF00FF000094CE009CEFFF004ADE
      FF0031D6FF0029D6FF0021D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6
      FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6
      FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0021D6FF0029D6FF0031D6
      FF004ADEFF009CEFFF000094CE00FF00FF00FF00FF0073737300A1A1A100C0BF
      BF00B8B8B800B9B9B900B7B7B700B5B5B500BABBBB00C3C4C300CDCACB00D2D4
      D400D6D7D700DBDADA00DADADA00D8D8D800D7D7D700D6D6D600D5D5D500D5D4
      D400CED0D000C9CBCB00C6C3C400BEBEBD00B9BBBC00B4B9B700499D4A000882
      08004C944C00C1BFBF00A3A1A30073737300FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00ADA5AD00BDBDBD00CECECE00DEDEDE00E7E7
      E700E7E7E700DEDEDE00CECECE00BDBDBD00ADA5AD009C7B5A00DEB57B00F7B5
      6B00FFB56300FFBD6B00FFC67300FFCE9400FFDEB500DE944A00DEA56B00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A1441500E6BA
      8400FFC68000FCB36400FEB05400FCC07F00FAFEFD00FBFFFF00FEFDFE00FCFE
      FD00FDFFFE00FCFFFF00FCFFFF00FDFFFF00FFFFFF00FCFCFC00F8FDFA00FDFE
      FF00FCFBFB00FAFDFC00FDFFF900FEFFFF00FFFFFF00FED9B400FF7C0C00FE7A
      0400FF7E0100BA430500AF796500FF00FF00FF00FF000094CE00A5EFFF0052DE
      FF0039DEFF0029DEFF0031DEFF0029DEFF0029DEFF0029DEFF0029DEFF0029DE
      FF0029DEFF0029DEFF0029DEFF0029DEFF0029DEFF0029DEFF0029DEFF0029DE
      FF0029DEFF0029DEFF0029DEFF0029DEFF0029DEFF0031DEFF0029DEFF0039DE
      FF0052DEFF00A5EFFF000094CE00FF00FF00FF00FF007D7D7D00C8C8C800C3C4
      C400C4C5C400C0C2C300CAC9CB00DDDADA00E6E8E700EDECEE00F1EFEF00EDEB
      EC00E7EAE700E9E5E500E4E5E500E3E3E300E2E2E200E0E0E000DEDFDF00DFDE
      DE00D9DDDA00DAD8DB00DBD9D800D4D6D600D7D1D700B8CAB80032B0380028A9
      2E00249C2700ACBBAB00CFCBCF007D7D7D00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00A59CA500BDBDBD00D6D6D600EFE7E700E7E7E700DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00E7E7E700E7E7E700D6D6DE00BDBDBD00ADADAD00CE9C
      6300F7BD6B00FFC68400FFD69C00FFDEB500DE944A00DEA56300FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B7775B00A7471B00FDDE
      A000FCBB6E00FDB16400FBB86900FBF0E100F6FFFF00FCFEFC00FFFEFC00FDFF
      FF00FBFCF800FEE0C000FEC28600FFBD7600FCD8B100FCFEFB00FCFEFE00FEFD
      FF00FCFDFF00FAFEFE00FCFFFC00FDFEFD00FFFFFF00FAE6D100FC7F1500FE7B
      0300FC7F0200DE5F0000A95B3600FF00FF00FF00FF000094CE00ADEFFF0052DE
      FF0042DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0042DE
      FF0052DEFF00ADEFFF000094CE00FF00FF00FF00FF0084848400D3D3D300D2D1
      D100CFD2D000DCE0E000F3EFF000F7F5F400F2F7F800F0F9FF00EAF8FE00ECF6
      FB00EAF4F900E7F2F700E6EFF600E4EDF400E3ECF300E1EAF100DFE8EF00DFE8
      ED00DBE6EB00D9E4E900D6E1E700D5DFE500D9DADB00CFD5CE0090D394007AD3
      81008BC88E00CBCFCC00D4D3D40084848400FF00FF00FF00FF00FF00FF00FF00
      FF00BDBDBD00E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00C6C6C600ADAD
      AD00C69C8400E7BDA500E7CEBD00CE8C6300CE947300FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00AD553000C0855300FFDC
      9900FCB36800FFB05E00FACFA000FEFFFF00F8FCFC00FDFDFE00FBFFFF00FDF2
      E400FFB76F00FA9A3900FE973000FA973400FDD3A600FBFFF900FDFFFF00FEFF
      FE00FBFFFC00F8FEFD00FEFEFE00FDFEFD00FFFEFF00FEF8EE00FE831B00FF7C
      0600FE7B0700F47201009A360500FF00FF00FF00FF000094CE00ADEFFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0063E7FF00ADEFFF000094CE00FF00FF00FF00FF008C8C8C00DFDFDF00DFDE
      DF00EEEDEF00F8F7F700F5F5F300F0FAFE00E8D0C700E3997800E3997500E299
      7900E5987600E0977500DE947400DE937300DD937300DD927200DC917200DB92
      7100DE926F00DA907000D88E6C00DB8E6F00D5BCAF00D7DBE100D5D7D400C8D1
      C600D4D2D400E2DFE300DFDFDF008C8C8C00FF00FF00FF00FF00FF00FF00A59C
      A500C6C6C600EFEFEF00DEDEDE00DEDEDE00DED6C600E7BD7B00F7AD3900FF9C
      1000FF9C1000F7AD3900EFBD7B00DED6C600DEDEDE00DEDEDE00E7EFEF00CEC6
      CE00ADADAD00E7CEAD00DE945200DEA56B00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00AB9A9A0097350800E6BB8C00FFD1
      8C00FEB76400FFB56D00F9EADA00FDFFFE00F8FFFE00FAFDFF00FDFCF500FBBD
      7500FE9C3F00FBA04D00FE9E3A00F9CDA200FCFFFF00F7FFFF00F8FFFE00FDFF
      FE00FEFFFD00FFFEFF00FEFCFF00FCFCFF00F8FEFE00FFFDF800FD8F3300FC7B
      0500FE7A0C00FF770600B4440100C2987F00FF00FF000094CE00ADF7FF0063E7
      FF004AE7FF0042E7FF004AE7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF004AE7FF0042E7FF004AE7
      FF0063E7FF00ADF7FF000094CE00FF00FF00FF00FF0093939300EBEBEB00F4F3
      F300FAFAF900F6F6F600F5FEFF00E8B29800DA5C1500DD641300DC6A1B00DC68
      1B00DC681700DC691900DB6A1900DC6B1900DC6B1A00DC6B1A00DD6C1B00DB69
      1A00DB6B1800DC681B00DD691C00DF651600DC5C1400D79F8500D4DDE200D9D3
      D800D2D0D300DDDDDD00EDEDED0093939300FF00FF00FF00FF00FF00FF00BDBD
      BD00EFEFEF00EFEFEF00DEDEDE00EFC68400E7942900E7A56B00EFBD9C00EFC6
      8400EFC68400EFC68400E7AD6B00E7942900EFC68400DEDEDE00EFEFEF00EFEF
      EF00ADADAD009C847300C68C7300FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00C6AB9F00922E0000F5D0A200FFCE
      8B00FFBB6A00FCB87500FFFBF400FEFFFF00FCFCFF00FEFFFF00FADDBF00FAA2
      5000FEA74D00FAA24A00F9A14500FCEDE000FBFFFF00FCFCFC00FFFDFE00FEFD
      FD00FCFFFE00FCFEFF00FEFEFB00FFFFFB00FEFDFF00FEFFFA00FDA75300FE79
      0300FD7B0A00FF7E0900BF500800B3755800FF00FF000094CE00B5F7FF006BE7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF006BE7FF00B5F7FF000094CE00FF00FF00FF00FF009A9A9A00F9F9F900FAFA
      FA00FAFAF900FAFDFD00F3DBD100E0661E00E4813600E48D4500E48B4600E48C
      4600E28B4600E38C4600E38D4600E48D4600E48D4700E48D4700E58E4700E18C
      4600E08B4500E38C4600E2894500E58E4700E4823B00DF641E00D9C0B600D5DA
      DC00D1CFD000DEDEE000FAFBFB009A9A9A00FF00FF00FF00FF00ADADAD00DEDE
      DE00E7E7E700E7DEDE00E7CEA500FFAD3100FFCE9400FFE7CE00FFDEBD00FFD6
      AD00FFD6AD00FFDEBD00FFE7CE00FFCE9400FFAD3100E7CEA500DEDEDE00E7E7
      E700DEDEDE00ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00C7A49800952F0000F0D3A400FFC9
      8800FDB96B00FCC58700FEFDFB00FDFFFF00FDFCFE00FFFFFF00FDCC9700FFA1
      4500FFA64C00FFA34C00FC9E4100FDDEB900FEFFFF00F8FFFE00FFFFFF00FDFD
      FF00FDFFFE00FFFFFC00FFFFF700FEFEFA00F7FFFF00FBFFFF00FBB57700FD77
      0100FF7E0800FE820900C0520800A5523300FF00FF00009CCE00B5F7FF006BE7
      FF005AE7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7
      FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7
      FF0052E7FF0052E7FF005AE7FF005AE7FF0052E7FF0052E7FF0052E7FF005AE7
      FF006BE7FF00B5F7FF00009CCE00FF00FF00FF00FF00A0A0A000FFFFFF00FDFD
      FE00F8FBFB00FCFFFF00EDAD8800E6762900EDA06200ED995E00E9985B00ED99
      5E00EB9A5D00EA9A5D00EA995B00EA995B00EA995C00EA995C00EB9A5C00EA99
      5D00ED995E00ED9A6000E99C5900EC985E00EE9E6500EA792E00D89A7300D2E2
      EA00D2D1D100E2E3E200FFFFFF00A0A0A000FF00FF00FF00FF00ADADAD00EFEF
      EF00DEDEDE00F7EFDE00E7943100EFC68400FFE7CE00FFD6AD00FFDEB500FFD6
      AD00FFD6AD00FFD6AD00FFD6AD00FFE7CE00EFC68400E7943100DEDEDE00EFEF
      EF00EFEFEF00ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00BE9F930097300000EFD4A500FFCC
      8C00FBB96A00FCCA8C00FEFEFD00FDFEFF00F8FFFF00FFFFFF00FCCA9500FFA6
      4600FBA65100FEA94A00FDA04100FBAB5900FCDEC300F9F7F400FFFEF900FEFF
      FF00FFFFFF00FDFFFF00F8FFFA00FAFDFF00FBFEFF00FBFFFF00FEC58A00FD75
      0300FE7F0B00FC810A00C1520800A0503100FF00FF00009CCE00B5F7FF0052E7
      FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7
      FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7
      FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7
      FF007BEFFF00B5F7FF000094CE00FF00FF00FF00FF00A5A5A500FFFFFF00FDFE
      FE00FEFDFD00FCFFFF00EFA67500EC803500F5AD7800F1A86E00F1A77100F2A5
      6F00EFA87100F3A66F00F2A76F00F2A76F00F2A76F00F2A76F00F2A76F00F2A7
      6D00F2A67000F2A66E00F5A97000EEA86D00F4AE7700F1853A00DE956500D4E5
      ED00D6D3D300E2E4E300FFFFFF00A5A5A500FF00FF00ADADAD00D6D6D600EFEF
      EF00E7E7E700F7C68400FFBD6300FFEFDE00FFDEBD00FFDEB500FFDEB500FFDE
      B500FFDEB500FFDEB500FFDEB500FFDEBD00FFEFDE00FFBD6300EFC68400E7E7
      E700EFEFEF00D6D6D600B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00A7989800962F0000F6DBAD00FFD1
      8F00FCBC6C00F9C07F00FDFDF900FCFFFF00FCFFFC00FDFFFF00F9D6AD00FCA5
      4700F9A95500FBA94F00FFA54C00FD9D4200FD9B3900FDA24B00FEB26C00FFCB
      9700FCDEC400FCF5F000FEFFFF00FCFFFF00FCFFFF00FAFFFF00FEA04800FE7D
      0A00FF7F1100FE810D00C2530A00A8674900FF00FF00009CCE00BDF7FF007BEF
      FF006BEFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF006BEF
      FF007BEFFF00BDF7FF000094CE00FF00FF00FF00FF00AAAAAA00FFFFFF00FDFE
      FE00FDFEFE00FFFFFF00F3AC7900F18E4400FBC29800FFE3CC00FFEAD500FFE9
      D100FFEBD200FFEAD100FFEAD200FFEAD100FFEAD100FFEAD100FFEAD100FFE9
      D200FFE8D200FFE8D100FFE9D300FFE9D300FFD0AC00F48E4300E19B6800D9E8
      EE00D7D5D400E7E4E400FFFFFF00AAAAAA00FF00FF00ADADAD00E7E7E700EFEF
      EF00EFEFEF00E79C5200FFCE9400FFEFD600FFDEB500FFDEB500FFDEB500FFDE
      B500FFDEB500FFDEB500FFDEB500FFDEB500FFEFD600EFC68400E79C5200EFEF
      EF00F7F7F700EFEFEF00ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00AD998D00922F0000E5CCA000FFD9
      A000FFBA6F00F9BC7900F7F1E400FEFFFF00FEFFFC00FAFEFF00FEEFDD00F9AE
      6200FEA84F00FDA85100FBA34D00FDA34A00FFA33E00FC9E3900F6973600FF8E
      2800FD902900F89F4800FBAF6700FACA9900FDD1A400FAA85E00FF800B00FA83
      1600FF811200FA7D0D00B3450700C88C7900FF00FF00009CCE00C6F7FF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF007BEFFF00C6F7FF000094CE00FF00FF00FF00FF00AEAEAE00FEFEFE00FFFF
      FF00FEFFFF00FEFFFF00F3AF7800F5985100FBC5A100C9968800AE8A8C00A887
      8B00A9888B00A9898C00AA898C00AA898C00AA898C00AA898C00AA898C00A889
      8D00AA8A8B00A5878900AF8C8D00C18F8800EBB79C00FEA05700E19F6800DCE8
      F200D6D8D900E7E5E600FFFFFF00AEAEAE00FF00FF00ADADAD00F7F7F700EFEF
      EF00E7EFE700F79C1000FFEFD600FFEFD600FFE7D600FFE7CE00FFE7CE00FFE7
      CE00FFE7CE00FFE7CE00FFE7CE00FFE7D600FFEFD600FFEFD600FF9C1000EFEF
      EF00F7F7F700EFEFEF00A5A5A500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A84C2000D29C6F00FFE9
      B400FDC07400FEBA7100FDDEB700FEFFFF00F8FEFB00FCFCFF00FEFFFF00FCE1
      C200FFA95000FDA14500FFA44900FFA24300FF9C3900FF9B3200FA973200FE94
      3A00FE953600FC902400FC881E00FD8B1B00FD871A00FD861400F9881800FE83
      1700FF831100F27E150095300000FF00FF00FF00FF000094CE00C6F7FF008CF7
      FF007BEFFF007BF7FF007BF7FF007BEFFF007BF7FF007BF7FF007BEFFF007BEF
      FF007BEFFF007BEFFF0073EFFF0073EFFF0073EFFF0073EFFF0073EFFF0073EF
      FF0073EFFF0073EFFF0073EFFF0073EFFF0073EFFF0073F7FF0073F7FF007BEF
      FF0084F7FF00C6F7FF000094CE00FF00FF00FF00FF00B3B3B300FFFFFF00FFFF
      FE00FEFFFF00FEFFFF00F6B57900F89E5300FCC7A200CA896B009C7E7D00CAAE
      AF00CCB0AE00CAAFAD00CCAFAE00CCAFAE00CCAFAE00CCAFAE00CCAFAE00CBB1
      AF00C8B1AD00C8ACAB009F7D7E00C1826400F0B89400FDA45900E6A46A00DCE9
      F400D8DBDC00E7E7E700FFFFFF00B3B3B300FF00FF00ADADAD00E7E7E700EFEF
      EF00EFEFEF00E78C2900FFF7E700FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFF7E700E78C2900EFEF
      EF00F7F7F700EFEFEF00ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B1674900B7633000FFF8
      D000F8C57A00FBBC6C00FEC68300FDFAF700FBFEFF00FBFEFE00FAFFFE00FEFF
      FF00FFEBD100FCC38900FBAE6000FEAF5E00F9B97C00FDCFA200FDB87700F998
      3900FF983500FA943600FC8E3300FB8D2E00FF8E2200FB892300FA842100FE87
      1600FF841400D7690E009B482000FF00FF00FF00FF000094CE00C6F7FF0073F7
      FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7
      FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7
      FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7FF007BF7
      FF007BF7FF00C6F7FF000094CE00FF00FF00FF00FF00B5B5B500FAFAFA00FFFF
      FF00FFFFFF00FFFFFF00FAB97900FAA25500FFD2AE00E5BC9B00ACB3B300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00AEB2B200E5BD9A00FFD5B000FBA45900EAA86A00E1EB
      F600DEDEDD00ECEEEC00FBFBFB00B5B5B500FF00FF00ADADAD00E7E7E700F7F7
      F700EFEFEF00F7B54200FFDEB500FFF7EF00FFF7E700FFEFE700FFEFE700FFEF
      E700F7EFE700FFEFE700FFF7E700FFF7E700FFF7EF00FFDEB500FFAD4200EFF7
      EF00F7F7F700E7E7E700ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00AF847300A5471A00F3DA
      B500FDDD9A00FBBB7100FAB96C00FBDBB400FFFFFF00FEFEFE00F9FDFE00F7FE
      FF00F9FFFF00F8FFFF00FEF9F400FFFCF600FDFFFF00FBFFFF00FBFFFF00FAC4
      9000FB952800FD963200FD972D00FE8F2800FF8E2400FE8C2200FD8A2100FF85
      1700FF8B2300AF470600A7695200FF00FF00FF00FF00009CCE00D6FFFF00B5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF009CF7FF009CF7FF0094F7FF0094F7FF008CF7FF0084F7FF0084F7FF007BF7
      FF007BF7FF0084F7FF0084F7FF0084F7FF007BF7FF007BF7FF0084F7FF0084F7
      FF0094F7FF00CEF7FF000094CE00FF00FF00FF00FF00B5B5B500C7C7C800D0D2
      D100D3D1D000CDD9E300E3A56400FE9D4000FEBD8600E1A87500A9AEAD00FFFF
      FF00FEFFFE00FDFEFE00FFFFFF00FFFFFF00FEFEFE00FEFEFE00FFFFFF00FDFF
      FE00FFFFFF00FFFFFF00ABACAB00E2A87400FFBF8500FF9D4000DE9E5E00C0C9
      D600C3C1C000CACAC800C8C8C800B5B5B500FF00FF00FF00FF00DEDED600F7F7
      F700EFF7F700F7CE8C00FFC66B00FFFFF700FFF7EF00FFF7EF00FFF7EF00FFF7
      EF00FFF7EF00FFF7EF00FFF7EF00FFF7EF00FFFFF700FFC66B00FFCE8C00EFF7
      F700F7F7F700DED6D600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009B401900CD91
      6100FFF8C900FDBF8300FFBC6F00FEBB6900F9E3C200FEFFFF00FDFFFE00FDFE
      FC00FBFCFD00FDFFFB00FEFFFF00FEFFFE00FFFEFB00FCFEFE00FBFFFF00FAEB
      D500FD9E4300FA933000F9933200FE922E00FF8E2800FD8E2700FC882100FF8C
      1C00E97E2200A0421700B5978D00FF00FF00FF00FF000094CE00DEFFFF00C6FF
      FF00BDFFFF00BDFFFF00BDFFFF00BDFFFF00BDFFFF00BDFFFF00BDFFFF00BDFF
      FF00BDFFFF00B5FFFF00ADFFFF00A5FFFF009CF7FF0094F7FF008CF7FF008CF7
      FF008CF7FF008CF7FF008CF7FF008CF7FF008CF7FF008CF7FF008CF7FF008CF7
      FF009CFFFF00CEFFFF00009CCE00FF00FF00FF00FF00FF00FF00FF00FF00ADAC
      AC00AEADAD00ADAEB100CCA47600F19F4100F6A34600DD984C00B6B7B600FCFF
      FF00FBFFFD00FDFFFD00FFFEFF00FFFFFF00FFFFFF00FEFEFE00FFFFFF00FDFF
      FE00FDFFFF00FEFEFF00B9B8B500DD984C00F6A24600F39F4100CFA67800AEAF
      B300B1AFAF00AFAEAD00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600F7F7
      F700F7F7F700F7EFDE00FFA52100FFDEAD00FFFFFF00FFFFF700FFF7F700FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFFF00FFDEB500FFA52100F7EFD600F7F7
      F700FFFFF700C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD896C009A3B
      0B00EEDAAF00FFE4A900FABC6F00FDBD6E00FBB76B00FDDCBA00FEFFFB00FDFF
      FF00FEFFFF00FBFEFF00F8FDFF00F9FEFF00FCFDFF00FDFFFF00F7FFFF00FBE0
      C700FE963600FE963200FC953500F9922E00F9942600FD8D2000FC881E00FF97
      2E00B14E0900A65D3B00FF00FF00FF00FF00FF00FF003994BD0094D6EF00E7FF
      FF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00DEFFFF00E7FFFF00E7FF
      FF00DEFFFF00DEFFFF00DEFFFF00C6FFFF00ADFFFF00A5FFFF009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF00A5F7
      FF00ADFFFF00D6FFFF000094CE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ABACAC00BEC1C000FDFF
      FF00FEFEFE00FDFEFE00FFFEFF00FEFEFE00FEFEFE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BFC2C200FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADADAD00E7E7
      E700FFFFFF00F7F7F700F7DEB500FFAD3900FFDEAD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFDEB500FFAD3900F7DEB500F7F7F700FFF7
      FF00E7E7E700ADADAD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009E43
      1E00B6693000FFF8CD00FED29E00FBBA6F00FDBB6D00FCB46600FEC78C00FAE9
      D500FEF7F800FEFFFF00FBFFFF00FCFFFF00FEFFFF00FFFBF800FBE8CE00F6AD
      6400FA9A3400FC9A3A00FD983400FD933100F9902E00FC8D2200FF9B3200D36E
      2300983B1000FF00FF00FF00FF00FF00FF00FF00FF007B9CAD005ABDDE000094
      CE000094CE00009CCE000094CE000094CE00009CCE000094CE000094CE00009C
      CE000094CE000094CE005ABDDE00DEFFFF00CEFFFF00BDFFFF00B5FFFF00B5FF
      FF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00ADFFFF00B5FF
      FF00BDFFFF00DEFFFF00009CC600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ACACAB00C0C1BF00FDFF
      FF00FDFFFF00FEFEFF00FFFFFF00FDFDFD00FEFEFE00F6F6F600F0EFEF00F1F1
      F000EEF0F100F4F7F700BEC0C000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C600FFFFFF00FFF7FF00F7F7F700F7DEB500FFA51800FFC67300FFE7B500FFF7
      E700FFF7E700FFDEB500FFC66B00FFA51800F7DEB500FFF7FF00F7FFF700FFFF
      FF00C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00AF9C
      960097300500CA8C5D00FFFED300FDD19400F8B46F00FFBA6A00FBB45D00F9AF
      6100FFBE7A00FFCA8C00FCC89400FDC79200FFC78800FEB57200FB9B4500FF9B
      3900FA9A4100FD983800FE983300FE943000FF8E3000FF9C3800E18238009838
      0800B3806A00FF00FF00FF00FF00FF00FF00FF00FF009C9CA500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF006BBDDE005ABDDE00DEFFFF00DEFFFF00CEFFFF00C6FF
      FF00C6FFFF00C6FFFF00C6FFFF00CEFFFF00C6FFFF00C6FFFF00C6FFFF00C6FF
      FF00CEFFFF00E7FFFF00009CCE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ACACAC00C2C2C300FCFF
      FF00FCFEFD00FEFFFE00FFFFFF00FFFFFF00F9F9F900C0C0C000C9C9C900CBCA
      CA00D0D1D100BABBBB009FA1A000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00D6CED600FFFFFF00F7FFFF00FFFFFF00FFEFDE00FFCE8C00FFB54200FF9C
      1800FF9C1800FFB54A00FFCE8C00FFEFDE00FFFFFF00FFFFF700F7FFFF00D6D6
      D6009C9CA500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00BC93820095330600CF955F00FFF5C600FFDA9900FCBA6900FFB46500FDB5
      6000FCB05800FEAB5100FFA74A00FFA44500FCA14300FAA04500FEA44400FDA2
      4000FDA03F00FF993B00FC963D00FD942F00FFA44500E58A3B00A53F0F00A568
      4F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009CA59C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006BBDDE0063C6DE0094DEEF00E7FFFF00E7FF
      FF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00E7FFFF00E7FF
      FF00E7FFFF0094D6EF005AB5DE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ACACAD00C4C4C400FEFF
      FF00FEFEFE00FEFFFF00FFFFFF00FFFFFF00F5F5F500D0D0D000FFFFFF00FFFF
      FF00E8E9E800A1A2A100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00A5A5A500CED6D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6D6D600A5A5
      A500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00BF9785009A380F00BB714400F8E3B300FFE9B300FBC58300FEB6
      6900FCAE5F00FDAF5D00FFAD5800FFAB5600FEA85400FDA65200FFA34800FC9E
      4400FB9A4000FD9A3C00FFA44A00FDB05900D57C38009D360900A4694F00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BDBDBD00DEDEDE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDEDE00C6C6C60073B5C6005ABDDE000094CE00009C
      CE000094CE000094CE00009CCE000094CE00009CCE000094CE00009CCE000094
      CE000094CE005ABDDE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ACACAC00C4C4C400FFFF
      FF00FFFFFF00FFFFFE00FFFFFF00FFFFFF00F8F8F800D4D4D400FFFFFF00E9E9
      E900A0A0A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF009C9C9C00C6C6C600E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFE7E700C6C6C600A59CA500FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0093360C00A5491800DBA67600FDEAB000FFE3
      A700FFCD8800FDBE7000FDB36600FFAC5900FEA94D00FEA84B00FFA84E00FFAC
      5300FFB55A00FFBC6500E79D5500B754210096321300B27F6600FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6009C9C
      A500A59CA500A59CA5009C9C9C009C9CA500A59CA500A5A59C00A59CA5009C9C
      A5009CA5A5009C9C9C00C6BDC600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADADAD00C7C7C700FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FFFFFF00F8F8F800DCDCDC00E8E8E800A1A1
      A100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00ADADAD00C6C6C600D6DEDE00EFEFEF00FFF7
      FF00F7F7F700EFEFEF00DEDEDE00C6C6C600ADADAD00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00AD694C009E320B00A44D1700C986
      5300F0C18F00F8D19700FED19200FDCC8A00FFCB8B00FEC38200FABF7A00F1B3
      6D00D78F4F00A74D15009D3B1300A4543800FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ADADAD00C7C7C700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00D1D1D100A4A4A400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5ADB500A5A5
      A500A5A5A500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00AE745D00AA51
      2C00912A0000A5441400B25D2800B96A3200BF6E3A00AE5D2C00A84C19009C31
      0100A23C1400B6705600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A8A8A800A5A5
      A500A8A8A800A8A8A800A8A8A800A8A8A800A8A8A8009D9D9D00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6978100A9614400A24A2600A84B2100AB5B3400BD856B00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00424D3E000000000000003E000000
      2800000080000000A00000000100010000000000000A00000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFF0000000000000000
      FF8001FFFFFFFFFF0000000000000000FF0000FFFFFFFFFF0000000000000000
      FE00007FFFFFFFFF0000000000000000FC00003FFFFFFFFF0000000000000000
      F800001FFFFFC0FF0000000000000000F000000FFFFF9E7F0000000000000000
      E00000070003BF3F0000000000000000C00000030003B09F0000000000000000
      800000011FFFA04F0000000000000000800000013FFFA2270000000000000000
      800000013FFF91130000000000000000800000013FFFC8890000000000000000
      800000013F00E4450000000000000000800000013F8072230000000000000000
      800000013FC039110000000000000000800000013FC41CB90000000000000000
      800000013FFE3E790000000000000000800000013FFE3F390000000000000000
      800000013E7F7F830000000000000000800000013E3FFFC70000000000000000
      800000013E3FFFFF0000000000000000800000013E7FFFFF0000000000000000
      C00000033FFFFF9F0000000000000000E00000071FFFFF9F0000000000000000
      F000000F0000001F0000000000000000F800001F0000001F0000000000000000
      FC00003FFFFFFFFF0000000000000000FE00007FFFFFFFFF0000000000000000
      FF0000FFFFFFFFFF0000000000000000FF8001FFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFF87FFFC0000003E0000007FFFFFFFFFFF07FFF80000001C0000003F800001F
      FFF07FFF80000001C0000003F000000FFFE0FFFF80000001C0000003F000000F
      FFE0FFFF8000000180000001F000000FFFC1FFFF8000000100000000F000000F
      FF83FFFF8000000100000000F000000FFF83FFFF8000000100000000F000000F
      FF07FFFF8000000100000000F000000FFF07FFFF8000000100000000F000000F
      FE07FFFF8000000100000000F000000FFC03FFFF8000000100000000F000000F
      FC01FFFF8000000100000000F000000FF800FFFF8000000100000000F000000F
      F8007FFF8000000100000000F000000FF000003F8000000100000000F000000F
      F000000F8000000100000000F000000FE00000078000000100000000F000000F
      E00000078000000100000000F000000FC00000078000000100000000F000000F
      C00000078000000100000000F000000F8000000F8000000100000000F0000007
      C000001F8000000100000000F0000007EC00001FC000000300000000F0000007
      FE00003FE000000700000000F0000007FF00003FF000000F00000000F000000F
      FFE0007FF800001F00000000F000000FFFFFC07FFC00003F00000000F000000F
      FFFFF0FFFC00007F00000000F800001FFFFFF9FFFE0000FF80000001FFC003FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFE007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFF
      FFE007FFFFE007FFFFFFFFFFFFF87FFFFF8001FFFF8001FFFFFFFFFFFFF07FFF
      FE00007FFE00007FFFFFFFFFFFF07FFFFC00003FFC00003FFFFFFFFFFFE0FFFF
      F800001FF800001F80000003FFE0FFFFF000000FF000000F80000001FFC1FFFF
      E0000007E000000780000001FF83FFFFE0000007E007C00780000001FF83FFFF
      C0000003C01F800380000001FF07FFFFC0000003C03F000380000001FF0FFFFF
      80000001807E000180000001FE07FFFF80000001807C000180000001FC03FFFF
      8000000180F8000180000001FC01FFFF8000000180F0010180000001F800FFFF
      8000000180E0030180000001F8001FFF8000000180C0070180000001F000003F
      8000000180800F0180000001E000000F8000000180001F0180000001E0000007
      8000000180003E0180000001C00000078000000180007E0180000001C0000007
      C0000003C000FC038000000180000007C0000003C001F803800000018000000F
      E0000007E003E007800000018000000FE0000007E000000780000001C400001F
      F000000FF000000F80000001FE00003FF800001FF800001F80000001FF00003F
      FC00003FFC00003F80000003FFE0007FFE00007FFE00007FFFFFFFFFFFFFC07F
      FF8001FFFF8001FFFFFFFFFFFFFFF0FFFFE007FFFFE007FFFFFFFFFFFFFFF8FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1FFFFFFFFFFFFFFFFFF
      800003FFFFFE007FFFF00FFFFFE007FF000003FFFFF8003FFFE007FFFF8001FF
      000003FFFFE0001FFFE007FFFE00007F000003FFFF80001FFFE007FFFC00003F
      000003FFFF00001FFFE007FFF800001F000003FFFE00000FFFE007FFF000000F
      000003FFFC00000FFFE007FFE0000007000003FFF800000FFFE007FFE0000007
      000003FFF000000FFFE007FFC0000003000003FFF000000FFFE007FFC0000003
      000003FFF000000FC000000380000001000003FFFF80000F8000000180000001
      000003FFFFC0000F8000000180000001000003FFFFE0000F8000000180000001
      000001FFFFF0001F80000001800000010000000FFFF8001F8000000180000001
      00000003FFFC003F800000018000000100000001FFFE003F8000000180000001
      00000001FFFF007F800000018000000100000001FFFF807FC000000380000001
      00000000FFFFF87FFFE007FFC000000300000000FFFFF83FFFE007FFC0000003
      80000000FFFFFC3FFFE007FFE0000007FF800000FFFFFC1FFFE007FFE0000007
      FFE00000FFFFFE1FFFE007FFF000000FFFF00000FFFFFE0FFFE007FFF800001F
      FFF80001FFFFFF0FFFE007FFFC00003FFFFE0007FFFFFF0FFFE007FFFE00007F
      FFFF003FFFFFFF87FFE007FFFF8001FFFFFF83FFFFFFFF87FFF00FFFFFE007FF
      FFFFFFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF007FFFFFFFFFFFFF81FFF
      FFFFFFFFFFE003FFFFFFFF8FFFE003FFFFFFFFFFFFE003FFFFFFFF07FF8001FF
      C0000003FFE003FFFFFFFE03FE00007F80000001FF8000FFFFFFFC03FC00003F
      80000001FE00003FFFFFF803F800001F80000001F800000FFFFFF003F000000F
      80000001F0000007FFFFE003E000000780000001E0000003FFFFC007E0000003
      80000001C0000001FF80800FC00000038000000180000000FE00001FC0000001
      8000000180000000F800003F800000018000000180000000F000007F80000001
      8000000180000000E00000FF000000008000000180000000E00001FF00000000
      8000000180000000C00003FF000000008000000180000000C00003FF00000000
      8000000180000000800001FF000000008000000180000000800001FF00000000
      8000000180000000800001FF800000018000000180000000800001FF80000001
      8000000180000000800001FF800000018000000180000000C00003FFC0000001
      80000001E0000003C00003FFC000000380000001FF8001FFC00003FFE0000007
      80000001FF8001FFE00007FFE000000780000001FF8001FFF00007FFF000000F
      80000001FF8003FFF0000FFFF800001F80000003FF8007FFF8001FFFFE00003F
      C001FFFFFF800FFFFE007FFFFF0000FFFFFFFFFFFF801FFFFFC3FFFFFFC003FF
      FFFFFFFFFFC03FFFFFFFFFFFFFF81FFF00000000000000000000000000000000
      000000000000}
  end
  object PMPastas: TPopupMenu
    OnPopup = PMPastasPopup
    Left = 582
    Top = 568
    object Novodiretrio1: TMenuItem
      Caption = '&Novo diret'#243'rio'
      OnClick = Novodiretrio1Click
    end
    object Novosubdiretrio1: TMenuItem
      Caption = 'Novo &subdiret'#243'rio'
      OnClick = Novosubdiretrio1Click
    end
    object Renomeardiretriosel1: TMenuItem
      Caption = '&Renomear diret'#243'rio sel.'
      OnClick = Renomeardiretriosel1Click
    end
    object Excluidiretrio1: TMenuItem
      Caption = '&Exclui diret'#243'rio'
      OnClick = Excluidiretrio1Click
    end
  end
  object QrEmailGerOp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'#11
      'FROM EmailGerOp')
    Left = 844
    Top = 244
  end
  object QrEmailGerMs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 364
    object QrEmailGerMsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailGerMsMailConta: TIntegerField
      FieldName = 'MailConta'
    end
    object QrEmailGerMsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEmailGerMsDe: TWideStringField
      FieldName = 'De'
      Size = 255
    end
    object QrEmailGerMsAssunto: TWideStringField
      FieldName = 'Assunto'
      Size = 255
    end
    object QrEmailGerMsIDMsg: TWideStringField
      FieldName = 'IDMsg'
      Size = 255
    end
    object QrEmailGerMsNomeDir: TWideStringField
      FieldName = 'NomeDir'
      Size = 255
    end
    object QrEmailGerMsDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
    object QrEmailGerMsCodDir: TIntegerField
      FieldName = 'CodDir'
    end
    object QrEmailGerMsUID: TWideStringField
      FieldName = 'UID'
      Size = 50
    end
    object QrEmailGerMsLido: TIntegerField
      FieldName = 'Lido'
    end
    object QrEmailGerMsSinalizado: TIntegerField
      FieldName = 'Sinalizado'
    end
    object QrEmailGerMsAnexos: TIntegerField
      FieldName = 'Anexos'
    end
  end
  object DsEmailGerMs: TDataSource
    DataSet = QrEmailGerMs
    Left = 512
    Top = 364
  end
  object IMAPDir: TIdIMAP4
    SASLMechanisms = <>
    MilliSecsToWaitToClearBuffer = 10
    Left = 467
    Top = 568
  end
  object IdThreadComponent1: TIdThreadComponent
    Active = False
    Loop = False
    Priority = tpHighest
    StopMode = smTerminate
    ThreadName = 'DmkMailVerify'
    OnException = IdThreadComponent1Exception
    OnRun = IdThreadComponent1Run
    Left = 520
    Top = 264
  end
  object QrEmailGerCP: TMySQLQuery
    Database = Dmod.MyDB
    Left = 156
    Top = 436
  end
end
