unit MailGerenOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkEdit,
  DmkDAC_PF;

type
  TFmMailGerenOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEmailGerOp: TmySQLQuery;
    DsEmailGerOp: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdDirMsgs: TdmkEdit;
    SpeedButton5: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmailGerOp();
    procedure MostraEdicao();
  public
    { Public declarations }
  end;

  var
  FmMailGerenOpc: TFmMailGerenOpc;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmMailGerenOpc.BtOKClick(Sender: TObject);
var
  DirMsgs: String;
begin
  DirMsgs := EdDirMsgs.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'EmailGerOp', False,
    ['DirMsgs'], ['Codigo'], [DirMsgs], [1], True) then
  begin
    Close;
  end else
    Geral.MB_Aviso('Falha ao atualizar!');
end;

procedure TFmMailGerenOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMailGerenOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMailGerenOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenEmailGerOp();
  //
  MostraEdicao();
end;

procedure TFmMailGerenOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMailGerenOpc.MostraEdicao;
begin
  EdDirMsgs.ValueVariant := QrEmailGerOp.FieldByName('DirMsgs').AsString;
end;

procedure TFmMailGerenOpc.ReopenEmailGerOp;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmailGerOp, Dmod.MyDB, [
    'SELECT * ',
    'FROM emailgerop ',
    '']);
end;

procedure TFmMailGerenOpc.SpeedButton5Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(FmMailGerenOpc, EdDirMsgs);
end;

end.
