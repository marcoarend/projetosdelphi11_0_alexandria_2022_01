unit MailGeren;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkCompoStore, CheckLst, dmkDBGrid,
  OleCtrls, SHDocVw, DBCGrids, dmkCheckBox, UnDmkEnums, Variants, mySQLDbTables,
  DmkDAC_PF, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdPOP3, IdIMAP4, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdMessage,
  IdAttachmentFile, IdText, UnMail, IdThreadComponent, Vcl.ImgList, IdMailBox,
  IdContext, IdCustomTCPServer, IdTCPServer, IdCmdTCPServer, IdIMAP4Server,
  Vcl.Menus, UnTreeView, Vcl.ToolWin, UnMailJan, dmkEdit, UnDmkImg,
  dmkEditCB, dmkDBLookupComboBox, System.ImageList;

type
  TTipoMsg = (istNenhum, istHTML, istPlano);
  TTipoMenu = (istLok, istOff, istOn);
  TFmMailGeren = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CSTabSheetChamou: TdmkCompoStore;
    Panel10: TPanel;
    QrEmailGer: TmySQLQuery;
    QrEmailGerCodigo: TIntegerField;
    QrEmailGerNome: TWideStringField;
    DsEmailGer: TDataSource;
    QrLoc: TmySQLQuery;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    StatusBar1: TStatusBar;
    Msg: TIdMessage;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrEmailGerDi: TmySQLQuery;
    ImageList1: TImageList;
    QrEmailGerDataHoraSinc: TDateTimeField;
    QrEmailGerDownDias: TIntegerField;
    PMPastas: TPopupMenu;
    TBEmails: TToolBar;
    TBVerificaArq: TToolButton;
    TBNovo: TToolButton;
    TBResponder: TToolButton;
    TBExcluiMail: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    TBLido: TToolButton;
    TBNLido: TToolButton;
    ToolButton14: TToolButton;
    TBPesqMail: TToolButton;
    TBMais: TToolButton;
    ToolButton19: TToolButton;
    Panel8: TPanel;
    BtOpcoes: TBitBtn;
    QrEmailGerOp: TmySQLQuery;
    QrEmailGerMs: TmySQLQuery;
    DsEmailGerMs: TDataSource;
    QrEmailGerMsCodigo: TIntegerField;
    QrEmailGerMsMailConta: TIntegerField;
    QrEmailGerMsDataHora: TDateTimeField;
    QrEmailGerMsDe: TWideStringField;
    QrEmailGerMsAssunto: TWideStringField;
    QrEmailGerMsIDMsg: TWideStringField;
    IMAPDir: TIdIMAP4;
    Novodiretrio1: TMenuItem;
    Renomeardiretriosel1: TMenuItem;
    Novosubdiretrio1: TMenuItem;
    Excluidiretrio1: TMenuItem;
    QrEmailGerMailBoxSeparator: TWideStringField;
    PB1: TProgressBar;
    IdThreadComponent1: TIdThreadComponent;
    STNomeDirSel: TStaticText;
    QrEmailGerMsNomeDir: TWideStringField;
    QrEmailGerMsDiretorio: TWideStringField;
    QrEmailGerMsCodDir: TIntegerField;
    LVEmails: TListView;
    Panel7: TPanel;
    QrEmailGerMsUID: TWideStringField;
    ToolButton3: TToolButton;
    TBNSinalizado: TToolButton;
    TBSinalizado: TToolButton;
    ToolButton11: TToolButton;
    QrEmailGerMsLido: TIntegerField;
    QrEmailGerMsSinalizado: TIntegerField;
    QrEmailGerMsAnexos: TIntegerField;
    Panel5: TPanel;
    Panel6: TPanel;
    ListBox1: TListBox;
    TreeView1: TTreeView;
    PnEmailGer: TPanel;
    SBEmailGer: TSpeedButton;
    CBEmailGer: TdmkDBLookupComboBox;
    EdEmailGer: TdmkEditCB;
    TBDiretorios: TToolBar;
    TBPastas: TToolButton;
    ToolButton20: TToolButton;
    TBPesqDir: TToolButton;
    ToolButton17: TToolButton;
    TBOn: TToolButton;
    TBOff: TToolButton;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    PcMsgs: TPageControl;
    TSHtml: TTabSheet;
    TsPlano: TTabSheet;
    MeTextoPlano: TMemo;
    TsAnexos: TTabSheet;
    Panel9: TPanel;
    BitBtn1: TBitBtn;
    WebBrowser1: TWebBrowser;
    QrEmailGerCP: TmySQLQuery;
    LVAnexos: TListView;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBEmailGerClick(Sender: TObject);
    procedure EdEmailGerChange(Sender: TObject);
    procedure Novodiretrio1Click(Sender: TObject);
    procedure Renomeardiretriosel1Click(Sender: TObject);
    procedure Novosubdiretrio1Click(Sender: TObject);
    procedure Excluidiretrio1Click(Sender: TObject);
    procedure TBVerificaArqClick(Sender: TObject);
    procedure IdThreadComponent1Run(Sender: TIdThreadComponent);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure LVEmailsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure TBNLidoClick(Sender: TObject);
    procedure TBLidoClick(Sender: TObject);
    procedure TBSinalizadoClick(Sender: TObject);
    procedure TBNSinalizadoClick(Sender: TObject);
    procedure TBExcluiMailClick(Sender: TObject);
    procedure IdThreadComponent1Exception(Sender: TIdThreadComponent;
      AException: Exception);
    procedure TBNovoClick(Sender: TObject);
    procedure TBResponderClick(Sender: TObject);
    procedure TBPesqMailClick(Sender: TObject);
    procedure TBMaisClick(Sender: TObject);
    procedure TBPesqDirClick(Sender: TObject);
    procedure TBOnClick(Sender: TObject);
    procedure TBOffClick(Sender: TObject);
    procedure PMPastasPopup(Sender: TObject);
    procedure LVEmailsCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure PcMsgsChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FMailCfg: Integer;
    FDiretorio: String;
    FOnLine, FReabreDir, FReabreMsg, FVerifi: Boolean;
    function  ConectaContaEmail(IMAP: TIdIMAP4): Boolean;
    function  AtualizaEmailDB2(MailCfg: Integer; Diretorio: String;
                Query, QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4; Msg: TIdMessage; Progress: TProgressBar;
                ListView: TListView; DataHoraSinc: TDateTime): Integer;
    procedure DesconectaContaEmail(IMAP: TIdIMAP4);
    procedure AtualizaDiretorios(MailCfg: Integer);
    procedure CarregaPastas(MailConta: Integer);
    procedure ConfiguraProgressBar();
    procedure AlteraFlagEmailMsg(Flag: TFlagEmailMsg);
    procedure RemoveEmailMsg();
    procedure ConfigurarPainelMsgs(TipoMsg: TTipoMsg; TemAnexo: Boolean);
    procedure ConfiguraTollBar(TipoMenu: TTipoMenu);
    procedure VerificaSeEstaOnLine(OffLine: Boolean);
    procedure CarregaMensagem(Codigo, Anexos: Integer);
  public
    { Public declarations }
  end;

  var
    FmMailGeren: TFmMailGeren;

implementation

uses UnMyObjects, Module, MyGlyfs, Principal, UMySQLModule, UnDmkWeb, UnMailEnv,
  ModuleGeral, MyDBCheck, MailGerenOpc;

{$R *.DFM}

procedure TFmMailGeren.CarregaPastas(MailConta: Integer);
var
  DirListExplode: TStringList;
  j, CodPasta: Integer;
  Linha, PastaAnt, Pasta: String;
  LocNode, LastNode: TTreeNode;
begin
  TreeView1.Items.Clear;
  ListBox1.Items.Clear;
  //
  //Sincroniza as pastas
  try
    Screen.Cursor := crHourGlass;
    //
    UMail.ReopenEmailGerDi(QrEmailGerDi, Dmod.MyDB, 0, MailConta);
    //
    while not QrEmailGerDi.Eof do
    begin
      Linha          := QrEmailGerDi.FieldByName('Nome').AsString;
      CodPasta       := QrEmailGerDi.FieldByName('Codigo').AsInteger;
      DirListExplode := Geral.Explode(Linha, QrEmailGerMailBoxSeparator.Value, 0);
      //
      ListBox1.Items.Add(Linha);
      try
        for j := 0 to DirListExplode.Count - 1 do
        begin
          Pasta    := UMail.TraduzNomeMailDir(DirListExplode[j]);
          PastaAnt := '';
          //
          if j = 0 then
          begin
            if UTreeView.LocalizaNodeInArrayNode(Pasta, 0, j, TreeView1) = nil then
              UTreeView.CriaNode(nil, Pasta, CodPasta, TreeView1);
          end else
          begin
            PastaAnt := UMail.TraduzNomeMailDir(DirListExplode[j - 1]);
            LastNode := UTreeView.LocalizaNodeInArrayNode(PastaAnt, 0, j -1, TreeView1);
            //
            if UTreeView.LocalizaNodeInArrayNode(Pasta, 0, j, TreeView1) = nil then
              UTreeView.CriaNode(LastNode, Pasta, CodPasta, TreeView1);
          end;
        end;
      finally
        DirListExplode.Free;
      end;
      QrEmailGerDi.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if TreeView1.Items.Count > 0 then
  begin
    TreeView1.Selected:= TreeView1.Items[0];
    TreeView1.FullExpand;
  end;
end;

procedure TFmMailGeren.RemoveEmailMsg();
var
  Item: TListItem;
  UID, Diretorio: String;
begin
  Item := LVEmails.Selected;
  //
  if Item <> nil then
  begin
    UID       := Item.SubItems[0];
    Diretorio := QrEmailGerDi.FieldByName('Nome').AsString;
    //
    if ConectaContaEmail(IMAPDir) then
    begin
      if IMAPDir.SelectMailBox(Diretorio) then
      begin
        if not IMAPDir.UIDDeleteMsg(UID) then
          Geral.MB_Aviso('Falha ao remover mensagem!')
        else
          Geral.MB_Aviso('Mensagem removida com sucesso!');
      end;
    end;
  end;
end;

procedure TFmMailGeren.AlteraFlagEmailMsg(Flag: TFlagEmailMsg);
var
  UID, Diretorio, De, Assunto: String;
  I, TotRec, Idx, Codigo, Lido, Sinalizado, Anexos: Integer;
begin
  if LVEmails.SelCount > 0 then
  begin
    Diretorio             := QrEmailGerDi.FieldByName('Nome').AsString;
    TBExcluiMail.Enabled  := False;
    TBLido.Enabled        := False;
    TBNLido.Enabled       := False;
    TBSinalizado.Enabled  := False;
    TBNSinalizado.Enabled := False;
    //
    if ConectaContaEmail(IMAPDir) then
    begin
      if IMAPDir.SelectMailBox(Diretorio) then
      begin
        TotRec       := LVEmails.Items.Count;
        PB1.Max      := TotRec;
        PB1.Position := 0;
        //
        for I := 0 to TotRec - 1 do
        begin
          if LVEmails.Items[I].Selected then
          begin
            Idx        := LVEmails.Items[I].Index;
            Codigo     := Geral.IMV(LVEmails.Items[I].Caption);
            UID        := LVEmails.Items[I].SubItems[0];
            De         := LVEmails.Items[I].SubItems[1];
            Assunto    := LVEmails.Items[I].SubItems[2];
            Lido       := Geral.IMV(LVEmails.Items[I].SubItems[6]);
            Sinalizado := Geral.IMV(LVEmails.Items[I].SubItems[7]);
            Anexos     := Geral.IMV(LVEmails.Items[I].SubItems[8]);
            //
            if UMail.AlteraFlagEmailMsg(Flag, UID, IMAPDir) then
            begin
              case flag of
                istLido:
                begin
                  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                    'UPDATE emailgerms SET Lido="1"',
                    'WHERE Codigo=' + Geral.FF0(Codigo),
                    '']) then
                  begin
                    LVEmails.Items[I].Delete;
                    UMail.InsereEmailsListView(LVEmails, Codigo, 1, Sinalizado,
                      Anexos, De, Assunto, UID, Idx);
                    LVEmails.Items[Idx].Selected := True;
                  end;
                end;
                istNaoLido:
                begin
                  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                    'UPDATE emailgerms SET Lido="0"',
                    'WHERE Codigo=' + Geral.FF0(Codigo),
                    '']) then
                  begin
                    LVEmails.Items[I].Delete;
                    UMail.InsereEmailsListView(LVEmails, Codigo, 0, Sinalizado,
                      Anexos, De, Assunto, UID, Idx);
                    LVEmails.Items[Idx].Selected := True;
                  end;
                end;
                istSinalizado:
                begin
                  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                    'UPDATE emailgerms SET Sinalizado="1"',
                    'WHERE Codigo=' + Geral.FF0(Codigo),
                    '']) then
                  begin
                    LVEmails.Items[I].Delete;
                    UMail.InsereEmailsListView(LVEmails, Codigo, Lido, 1,
                      Anexos, De, Assunto, UID, Idx);
                    LVEmails.Items[Idx].Selected := True;
                  end;
                end;
                istNaoSinalizado:
                begin
                  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                    'UPDATE emailgerms SET Sinalizado="0"',
                    'WHERE Codigo=' + Geral.FF0(Codigo),
                    '']) then
                  begin
                    LVEmails.Items[I].Delete;
                    UMail.InsereEmailsListView(LVEmails, Codigo, Lido, 0,
                      Anexos, De, Assunto, UID, Idx);
                    LVEmails.Items[Idx].Selected := True;
                  end;
                end;
              end;
              TreeView1.Update;
            end;
          end;
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
        end;
      end;
      DesconectaContaEmail(IMAPDir);
    end;
    TBExcluiMail.Enabled  := True;
    TBLido.Enabled        := True;
    TBNLido.Enabled       := True;
    TBSinalizado.Enabled  := True;
    TBNSinalizado.Enabled := True;
  end;
  PB1.Position := 0;
end;

procedure TFmMailGeren.DesconectaContaEmail(IMAP: TIdIMAP4);
var
  TipoConexao: TTipoConta;
begin
  UMail.DesconectaMailServer(IMAP, nil, TipoConexao);
  StatusBar1.Panels[0].Text := 'Status: Desconectado';
end;

function TFmMailGeren.ConectaContaEmail(IMAP: TIdIMAP4): Boolean;
var
  Codigo: Integer;
  TipoConexao: TTipoConta;
begin
  Result := False;
  //
  if not IMAP.Connected then
  begin
    if (QrEmailGer.State <> dsInactive) and (QrEmailGer.RecordCount > 0) then
    begin
      if DmkWeb.RemoteConnection then //Verifica internet
      begin
        try
          PnEmailGer.Enabled := False;
          //
          Codigo := FMailCfg;
          //
          if Codigo <> 0 then
          begin
            if UMail.ConectaContaEmail(Codigo, QrLoc, Dmod.MyDB, nil, IMAP, IdSSL1) = istIMAP4 then
              Result := True;
          end;
        finally
          PnEmailGer.Enabled := True;
        end;
      end;
    end;
  end else
  begin
    StatusBar1.Panels[0].Text := 'Status: Conectado';
    PnEmailGer.Enabled        := True;
    Result                    := True;
  end;
end;

procedure TFmMailGeren.ConfiguraProgressBar;
var
  ProgressBarStyle: Integer;
begin
  PB1.Align                  := alNone;
  StatusBar1.Panels[4].Style := psOwnerDraw;
  PB1.Parent                 := StatusBar1;
  //
  ProgressBarStyle := GetWindowLong(PB1.Handle, GWL_EXSTYLE);
  ProgressBarStyle := ProgressBarStyle - WS_EX_STATICEDGE;
  //
  SetWindowLong(PB1.Handle, GWL_EXSTYLE, ProgressBarStyle);
end;

procedure TFmMailGeren.ConfigurarPainelMsgs(TipoMsg: TTipoMsg;
  TemAnexo: Boolean);
begin
  PcMsgs.ActivePageIndex := 0;
  //
  case TipoMsg of
    istNenhum:
    begin
      PcMsgs.Visible      := False;
      Splitter2.Visible   := False;
      TSHtml.TabVisible   := False;
      TsPlano.TabVisible  := False;
      TsAnexos.TabVisible := False;
    end;
    istHTML:
    begin
      PcMsgs.Visible     := True;
      Splitter2.Visible  := True;
      TSHtml.TabVisible  := True;
      TsPlano.TabVisible := False;
      //
      if TemAnexo then
        TsAnexos.TabVisible := True
      else
        TsAnexos.TabVisible := False;
    end;
    istPlano:
    begin
      PcMsgs.Visible     := True;
      Splitter2.Visible  := True;
      TSHtml.TabVisible  := False;
      TsPlano.TabVisible := True;
      //
      //
      if TemAnexo then
        TsAnexos.TabVisible := True
      else
        TsAnexos.TabVisible := False;
    end;
  end;
end;

procedure TFmMailGeren.ConfiguraTollBar(TipoMenu: TTipoMenu);
begin
  case TipoMenu of
    istLok:
    begin
      //Diret�rios
      TBPastas.Enabled  := False;
      TBPesqDir.Enabled := False;
      TBOn.Enabled      := False;
      TBOff.Enabled     := False;
      //E-mails
      TBVerificaArq.Enabled := False;
      TBNovo.Enabled        := False;
      TBResponder.Enabled   := False;
      TBExcluiMail.Enabled  := False;
      TBLido.Enabled        := False;
      TBNLido.Enabled       := False;
      TBSinalizado.Enabled  := False;
      TBNSinalizado.Enabled := False;
      TBPesqMail.Enabled    := False;
      TBMais.Enabled        := False;
    end;
    istOff:
    begin
      //Diret�rios
      TBPastas.Enabled  := False;
      TBPesqDir.Enabled := True;
      TBOn.Enabled      := True;
      TBOff.Enabled     := False;
      //E-mails
      TBVerificaArq.Enabled := False;
      TBNovo.Enabled        := False;
      TBResponder.Enabled   := False;
      TBExcluiMail.Enabled  := False;
      TBLido.Enabled        := False;
      TBNLido.Enabled       := False;
      TBSinalizado.Enabled  := False;
      TBNSinalizado.Enabled := False;
      TBPesqMail.Enabled    := True;
      TBMais.Enabled        := True;
    end;
    istOn:
    begin
      //Diret�rios
      TBPastas.Enabled  := True;
      TBPesqDir.Enabled := True;
      TBOn.Enabled      := True;
      TBOff.Enabled     := True;
      //E-mails
      TBVerificaArq.Enabled := True;
      TBNovo.Enabled        := True;
      TBResponder.Enabled   := True;
      TBExcluiMail.Enabled  := True;
      TBLido.Enabled        := True;
      TBNLido.Enabled       := True;
      TBSinalizado.Enabled  := True;
      TBNSinalizado.Enabled := True;
      TBPesqMail.Enabled    := True;
      TBMais.Enabled        := True;
    end;
  end;
end;

procedure TFmMailGeren.EdEmailGerChange(Sender: TObject);
var
  MailConta: Integer;
  Diretorio: String;
begin
  EdEmailGer.Enabled := False;
  CBEmailGer.Enabled := False;
  SBEmailGer.Enabled := False;
  try
    FReabreDir := False;
    MailConta  := EdEmailGer.ValueVariant;
    //
    if MailConta <> 0 then
    begin
      StatusBar1.Panels[1].Text := '�ltimo sincronismo em: ' + Geral.FDT(QrEmailGerDataHoraSinc.Value, 0);
      //
      CarregaPastas(MailConta);
    end else
      StatusBar1.Panels[1].Text := '';
    //
    StatusBar1.Panels[2].Text := '';
    //
    QrEmailGerDi.First;
    //
    Diretorio            := QrEmailGerDi.FieldByName('Nome').AsString;
    STNomeDirSel.Caption := UMail.TraduzNomeMailDir(Diretorio);
    FMailCfg             := MailConta;
    FDiretorio           := Diretorio;
    //
    UMail.ReopenEmailGerMs(LVEmails, QrEmailGerMs, Dmod.MyDB, Diretorio, MailConta, 0);
    VerificaSeEstaOnLine(False);
    //
    FReabreDir := True;
  finally
    EdEmailGer.Enabled := True;
    CBEmailGer.Enabled := True;
    SBEmailGer.Enabled := True;
  end;
end;

procedure TFmMailGeren.Excluidiretrio1Click(Sender: TObject);
var
  Node: TTreeNode;
  SelCod, MailCfg: Integer;
begin
  Node    := TreeView1.Selected;
  MailCfg := FMailCfg;
  //
  if Node <> nil then
  begin
    if ConectaContaEmail(IMAPDir) then
    begin
      SelCod := Node.StateIndex;
      //
      if UMail.ExcluiPastaEmail_IMAP(SelCod, QrEmailGerDi, QrLoc, Dmod.MyDB, IMAPDir) then
        AtualizaDiretorios(MailCfg);
    end;
  end else
    Geral.MB_Aviso('Nenhum diret�rio foi selecionado!');
end;

procedure TFmMailGeren.AtualizaDiretorios(MailCfg: Integer);
var
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    if ConectaContaEmail(IMAPDir) then
    begin
      if IMAPDir.Connected then
        UMail.AtualizaDiretoriosIMAP(Dmod.QrUpd, QrEmailGerDi, Dmod.MyDB, IMAPDir,
          MailCfg, QrEmailGerMailBoxSeparator.Value);
    end;
    CarregaPastas(MailCfg);
    //
    DesconectaContaEmail(IMAPDir);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMailGeren.BitBtn1Click(Sender: TObject);
begin
(*
     if mbMsgP.MessageParts[liCount] is TIdText then
        begin
          Memo1.Lines.AddStrings((mbMsgP.MessageParts[liCount] as TIdText).Body);
        end
        else
        begin
          if mbMsgP.MessageParts[liCount] is TIdAttachment then
          begin
            if  mbMsgP.MessageParts[liCount] is TIdAttachmentFile then
            begin
              fName := TIdAttachmentFile(mbMsgP.MessageParts.Items[liCount]).Filename;
              if fName <> ''
                fName := selectedFolder + fName
              else
                fName := selectedFolder + defaultFilename;
              TIdAttachmentFile(mbMsgP.MessageParts.Items[liCount]).SaveToFile(fName);
            end
            else
            begin
               Memo1.Lines.Add('Attachment ignored - Not recognised as a file');
            end;
          end;
        end;
*)
end;

procedure TFmMailGeren.BtOpcoesClick(Sender: TObject);
begin
  UMailJan.MostraMailGerenOpc;
  //
  UMyMod.AbreQuery(QrEmailGerOp, Dmod.MyDB);
end;

procedure TFmMailGeren.BtSaidaClick(Sender: TObject);
begin
  if TFmMailGeren(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmMailGeren.FormCreate(Sender: TObject);
begin
  FReabreMsg := True;
  //
  //ConfigurarPainelMsgs(istNenhum, False);
  ConfiguraTollBar(istLok);
  ConfiguraProgressBar;
  //
  UMyMod.AbreQuery(QrEmailGerOp, Dmod.MyDB);
  UMail.ReopenEmailGer(QrEmailGer, Dmod.MyDB, 0);
  //
  EdEmailGer.ValueVariant := QrEmailGerCodigo.Value;
  CBEmailGer.KeyValue     := QrEmailGerCodigo.Value;
end;

procedure TFmMailGeren.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMailGeren.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmMailGeren.IdThreadComponent1Exception(Sender: TIdThreadComponent;
  AException: Exception);
begin
  IdThreadComponent1.Active := False;
end;

procedure TFmMailGeren.IdThreadComponent1Run(Sender: TIdThreadComponent);
var
  Controle, MailConta: Integer;
  Diretorio: String;
begin
  FVerifi   := True;
  MailConta := FMailCfg;
  Diretorio := FDiretorio;
  //
  if MailConta <> 0 then
  begin
    if Diretorio <> '' then
    begin
      try
        TBDiretorios.Enabled := False;
        FReabreMsg           := False;
        //
        StatusBar1.Panels[2].Text := 'Verificando mensagens. Aguarde!';
        StatusBar1.Refresh;
        //
        if ConectaContaEmail(IMAPDir) then
        begin
          AtualizaEmailDB2(MailConta, Diretorio, QrEmailGerMs,
            Dmod.QrUpd, Dmod.MyDB, IMAPDir, Msg, PB1, LVEmails,
            QrEmailGerDi.FieldByName('DataHoraSinc').AsDateTime);
          //
          DesconectaContaEmail(IMAPDir);
        end;
        FVerifi                   := False;
        StatusBar1.Panels[2].Text := '';
        StatusBar1.Refresh;
      finally
        IdThreadComponent1.Active := False;
        TBDiretorios.Enabled      := True;
        FReabreMsg                := True;
        TBVerificaArq.ImageIndex  := 3;
        TBVerificaArq.Caption     := 'Verificar';
      end;
    end else
    begin
      TBVerificaArq.Enabled := False;
      PnEmailGer.Enabled    := False;
      TBDiretorios.Enabled  := False;
      FReabreMsg            := False;
      try
        AtualizaDiretorios(MailConta);
      finally
        TBVerificaArq.Enabled := True;
        PnEmailGer.Enabled    := True;
        TBDiretorios.Enabled  := True;
        FReabreMsg            := True;
      end;
    end;
  end;
end;

function TFmMailGeren.AtualizaEmailDB2(MailCfg: Integer; Diretorio: String;
  Query, QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase; IMAP: TIdIMAP4;
  Msg: TIdMessage; Progress: TProgressBar; ListView: TListView;
  DataHoraSinc: TDateTime): Integer;
var
  I, Lido, Sinalizado, Anexos: Integer;
  UID, De, Assunto: String;
  Data: TDateTime;
  Pesq: TIdIMAP4SearchRec;
  Flag: TIdMessageFlagsSet;
begin
  Result := 0;
  //
  if IMAP.SelectMailBox(Diretorio) then
  begin
    if DataHoraSinc > 2 then
    begin
      Pesq.Date      := DataHoraSinc;
      Pesq.SearchKey := skSince;
      //
      //Pesquisa antes de trazer e-mails
      if IMAP.UIDSearchMailBox(Pesq) then
      begin
        TreeView1.Enabled := True;
        Progress.Position := 0;
        Progress.Max      := High(IMAP.MailBox.SearchResult);
        //
        try
          for I := 0 to High(IMAP.MailBox.SearchResult) do
          begin
            if FVerifi then
            begin
              try
                UID := Geral.FF0(IMAP.MailBox.SearchResult[I]);
                IMAP.KeepAlive;
                IMAP.UIDRetrieve(UID, Msg);
                IMAP.UIDRetrieveFlags(UID, Flag);
                //
                Result := UMail.InsereEmailDB(UID, Diretorio, MailCfg, Query,
                            QueryUpd, DataBase, Msg, Flag, ListView, De,
                            Assunto, Data, Lido, Sinalizado, Anexos);
                //
                if Result <> 0 then
                begin
                  if (De <> '') and (Assunto <> '') then
                  begin
                    if FDiretorio = Diretorio then
                    begin
                      UMail.InsereEmailsListView(ListView, Result, Lido,
                        Sinalizado, Anexos, De, Assunto, UID, 0);
                    end;
                  end;
                end;
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
              except
                Geral.MB_Aviso('Falha ao verificar e-mail!');
                Progress.Position := 0;
                Exit;
              end;
            end else
            begin
              Progress.Position := 0;
              Break;
            end;
          end;
        except
          Geral.MB_Aviso('Falha ao verificar e-mail!');
        end;
        //
        Progress.Position := 0;
      end else
        Geral.MB_Aviso('Falha ao verificar e-mail!');
    end else
    begin
      TreeView1.Enabled := True;
      Progress.Position := 0;
      Progress.Max      := IMAP.MailBox.TotalMsgs;
      //
      try
        for I := 0 to IMAP.MailBox.TotalMsgs - 1 do
        begin
          if FVerifi then
          begin
            try
              IMAP.KeepAlive;
              IMAP.GetUID(I + 1, UID);
              IMAP.UIDRetrieve(UID, Msg);
              IMAP.UIDRetrieveFlags(UID, Flag);
              //
              Result := UMail.InsereEmailDB(UID, Diretorio, MailCfg, Query,
                          QueryUpd, DataBase, Msg, Flag, ListView, De,
                          Assunto, Data, Lido, Sinalizado, Anexos);
              //
              if Result <> 0 then
              begin
                if (De <> '') and (Assunto <> '') then
                begin
                  if FDiretorio = Diretorio then
                    UMail.InsereEmailsListView(ListView, Result, Lido,
                      Sinalizado, Anexos, De, Assunto, UID, 0);
                end;
              end;
              //
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
            except
              Geral.MB_Aviso('Falha ao verificar e-mail!');
              Progress.Position := 0;
              Exit;
            end;
          end else
          begin
            Progress.Position := 0;
            Break;
          end;
        end;
      except
        Geral.MB_Aviso('Falha ao verificar e-mail!');
      end;
      Progress.Position := 0;
      FReabreDir        := False;
      //
      if FDiretorio = QrEmailGerDi.FieldByName('Nome').AsString then
      begin
        try
          AtualizaDiretorios(MailCfg);
        finally
          FReabreDir := True;
        end;
        UMail.ReopenEmailGerMs(LVEmails, QrEmailGerMs, Dmod.MyDB, Diretorio, MailCfg, Result);
        UTreeView.LocalizaNodeByStateIndex(TreeView1, QrEmailGerMs.FieldByName('CodDir').AsInteger);
      end;
    end;
  end;
  TreeView1.Enabled := True;
end;

procedure TFmMailGeren.LVEmailsCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Item.SubItems.Count > 6 then
  begin
    if Item.SubItems[5] = '1' then
      Sender.Canvas.Font.Style := []
    else
      Sender.Canvas.Font.Style := [fsBold];
    Sender.Update;
  end;
end;

procedure TFmMailGeren.LVEmailsSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  Codigo, Anexos: Integer;
begin
  if Selected then
  begin
    LVEmails.Enabled := False;
    //
    try
      Codigo := Geral.IMV(Item.Caption);
      Anexos := Geral.IMV(Item.SubItems[8]);
      //
      CarregaMensagem(Codigo, Anexos);
    finally
      LVEmails.Enabled := True;
    end;
  end;
end;

procedure TFmMailGeren.CarregaMensagem(Codigo, Anexos: Integer);
var
  Anexo: Boolean;
  TxtPlano, TxtHTML: String;
begin
  if Codigo <> 0 then
  begin
    UMail.ReopenEmailGerCP(QrEmailGerCP, Dmod.MyDB, Codigo);
    //
    if QrEmailGerCP.RecordCount > 0 then
    begin
      QrEmailGerCP.First;
      //
      while not QrEmailGerCP.Eof do
      begin
        if QrEmailGerCP.FieldByName('Tipo').AsInteger = 0 then //Mensagem
        begin
          if Pos('text/html', QrEmailGerCP.FieldByName('ContentType').AsString) > 0 then
            TxtHTML := TxtHTML + Geral.Substitui(QrEmailGerCP.FieldByName('Texto').AsString, 'charset=utf-8', 'charset=iso-8859-1')
          else if Pos('text/plain', QrEmailGerCP.FieldByName('ContentType').AsString) > 0 then
            TxtPlano := TxtPlano + Geral.Substitui(QrEmailGerCP.FieldByName('Texto').AsString, 'charset=utf-8', 'charset=iso-8859-1')
          else
            TxtPlano := 'N�o implementado!';
        end;
        //
        QrEmailGerCP.Next;
      end;
    end;
  end;
  if Anexos <> 0 then
    Anexo := True
  else
    Anexo := False;
  //
  DmkWeb.WBLoadHTML(WebBrowser1, TxtHTML);
  MeTextoPlano.Text := TxtPlano;
  //
  if TxtHTML <> '' then
    ConfigurarPainelMsgs(istHTML, Anexo)
  else
    ConfigurarPainelMsgs(istPlano, Anexo);
end;

procedure TFmMailGeren.Novodiretrio1Click(Sender: TObject);
var
  MailCfg: Integer;
begin
  MailCfg := FMailCfg;
  //
  if ConectaContaEmail(IMAPDir) then
  begin
    if UMail.CriaPastaEmail_IMAP(MailCfg, '', QrEmailGerDi, QrLoc, Dmod.MyDB, IMAPDir)
    then
      AtualizaDiretorios(MailCfg);
  end else
    Geral.MB_Aviso('N�o foi poiss�vel conectar no servidor!');
end;

procedure TFmMailGeren.Novosubdiretrio1Click(Sender: TObject);
var
  Node: TTreeNode;
  SelCod, MailCfg: Integer;
begin
  Node    := TreeView1.Selected;
  MailCfg := FMailCfg;
  //
  if Node <> nil then
  begin
    if ConectaContaEmail(IMAPDir) then
    begin
      SelCod := Node.StateIndex;
      //
      if UMail.CriaSubPastaEmail_IMAP(SelCod, QrEmailGerMailBoxSeparator.Value,
        QrEmailGerDi, QrLoc, Dmod.MyDB, IMAPDir)
      then
        AtualizaDiretorios(MailCfg);
    end;
  end else
    Geral.MB_Aviso('Nenhum diret�rio foi selecionado!');
end;

procedure TFmMailGeren.PcMsgsChange(Sender: TObject);
var
  Anexo: String;
  Item: TListItem;
begin
  LVAnexos.Items.Clear;
  //
  //Carrega anexos
  if PcMsgs.ActivePageIndex = 2 then
  begin
    if QrEmailGerCP.RecordCount > 0 then
    begin
      QrEmailGerCP.First;
      //
      while not QrEmailGerCP.Eof do
      begin
        if QrEmailGerCP.FieldByName('Tipo').AsInteger = 1 then //Anexo
        begin
          Anexo := QrEmailGerCP.FieldByName('Texto').AsString;
          //
          Item            := LVAnexos.Items.Add;
          Item.Caption    := Anexo;
          Item.ImageIndex := 17;
        end;
        //
        QrEmailGerCP.Next;
      end;
    end;
  end;
end;

procedure TFmMailGeren.PMPastasPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrEmailGerDi.State <> dsInactive) and (QrEmailGerDi.RecordCount > 0);
  //
  Novodiretrio1.Enabled        := Enab;
  Novosubdiretrio1.Enabled     := Enab;
  Renomeardiretriosel1.Enabled := Enab;
  Excluidiretrio1.Enabled      := Enab;
end;

procedure TFmMailGeren.Renomeardiretriosel1Click(Sender: TObject);
var
  Node: TTreeNode;
  SelCod, MailCfg: Integer;
  Nome: String;
begin
  Node    := TreeView1.Selected;
  MailCfg := FMailCfg;
  //
  if Node <> nil then
  begin
    if ConectaContaEmail(IMAPDir) then
    begin
      SelCod := Node.StateIndex;
      Nome   := Node.Text;
      //
      if UMail.RenomeiaPastaEmail_IMAP(SelCod, Nome, QrEmailGerDi, QrLoc, Dmod.MyDB, IMAPDir) then
        AtualizaDiretorios(MailCfg);
    end;
  end else
    Geral.MB_Aviso('Nenhum diret�rio foi selecionado!');
end;

procedure TFmMailGeren.SBEmailGerClick(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdEmailGer.ValueVariant;
  //
  UMailEnv.MostraMailCfg(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMail.ReopenEmailGer(QrEmailGer, Dmod.MyDB, 0);
    //
    EdEmailGer.ValueVariant := VAR_CADASTRO;
    CBEmailGer.KeyValue     := VAR_CADASTRO;
    EdEmailGer.SetFocus;
  end;
end;

procedure TFmMailGeren.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
  if Panel = StatusBar.Panels[4] then
  begin
    with PB1 do
    begin
      Top    := Rect.Top;
      Left   := Rect.Left;
      Width  := Rect.Right - Rect.Left - 15;
      Height := Rect.Bottom - Rect.Top;
    end;
  end;
end;

procedure TFmMailGeren.TBVerificaArqClick(Sender: TObject);
begin
  if IdThreadComponent1.Active then
  begin
    if Geral.MB_Pergunta('H� uma verifica��o em execu��o!' + sLineBreak +
      'Deseja realmente par�-la?') = ID_YES then
    begin
      TBVerificaArq.ImageIndex  := 3;
      TBVerificaArq.Caption     := 'Verificar';
      FVerifi                   := False;
    end;
  end else
  begin
    TBVerificaArq.ImageIndex  := 16;
    TBVerificaArq.Caption     := 'Parar';
    TreeView1.Enabled         := False;
    IdThreadComponent1.Active := True;
  end;
end;

procedure TFmMailGeren.TBSinalizadoClick(Sender: TObject);
begin
  AlteraFlagEmailMsg(istSinalizado);
end;

procedure TFmMailGeren.TBLidoClick(Sender: TObject);
begin
  AlteraFlagEmailMsg(istLido);
end;

procedure TFmMailGeren.TBNLidoClick(Sender: TObject);
begin
  AlteraFlagEmailMsg(istNaoLido);
end;

procedure TFmMailGeren.TBPesqDirClick(Sender: TObject);
begin
  if (QrEmailGerDi.State <> dsInactive) and (QrEmailGerDi.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Fazer!');
  end;
end;

procedure TFmMailGeren.TBPesqMailClick(Sender: TObject);
begin
  Geral.MB_Aviso('Fazer!');
end;

procedure TFmMailGeren.TBMaisClick(Sender: TObject);
begin
  Geral.MB_Aviso('Fazer!');
end;

procedure TFmMailGeren.TBNSinalizadoClick(Sender: TObject);
begin
  AlteraFlagEmailMsg(istNaoSinalizado);
end;

procedure TFmMailGeren.TBOffClick(Sender: TObject);
begin
  VerificaSeEstaOnLine(True);
end;

procedure TFmMailGeren.TBOnClick(Sender: TObject);
begin
  VerificaSeEstaOnLine(False);
end;

procedure TFmMailGeren.TBNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Fazer!');
end;

procedure TFmMailGeren.TBResponderClick(Sender: TObject);
begin
  Geral.MB_Aviso('Fazer!');
end;

procedure TFmMailGeren.TBExcluiMailClick(Sender: TObject);
begin
  Geral.MB_Aviso('Melhorar!');
  //RemoveEmailMsg();
end;

procedure TFmMailGeren.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  CodDir, MailCfg: Integer;
  Diretorio: String;
begin
  if FVerifi then
    FVerifi := False;
  //
  if FReabreDir then
  begin
    MailCfg := FMailCfg;
    CodDir  := Node.StateIndex;
    //
    if (CodDir <> 0) and (MailCfg <> 0) then
    begin
      QrEmailGerDi.Locate('Codigo', CodDir, []);
      //
      Diretorio := QrEmailGerDi.FieldByName('Nome').AsString;
      //
      UMail.ReopenEmailGerMs(LVEmails, QrEmailGerMs, Dmod.MyDB, Diretorio, MailCfg, 0);
      //
      if QrEmailGerMs.RecordCount > 0 then
        CarregaMensagem(QrEmailGerMsCodigo.Value, QrEmailGerMsAnexos.Value)
      else
        ConfigurarPainelMsgs(istNenhum, False);
      //
      STNomeDirSel.Caption := Node.Text;
      FMailCfg             := MailCfg;
      FDiretorio           := Diretorio;
    end;
  end;
end;

procedure TFmMailGeren.VerificaSeEstaOnLine(OffLine: Boolean);
begin
  TreeView1.Enabled := True;
  //
  if OffLine then
  begin
    FOnLine := False;
    //
    ConfiguraTollBar(istOff);
  end else
  begin
    FOnLine := DmkWeb.RemoteConnection;
    //
    if not FOnLine then
    begin
      Geral.MB_Aviso('N�o foi poss�vel conectar!');
      //
      ConfiguraTollBar(istOff);
    end else
      ConfiguraTollBar(istOn)
  end;
end;

end.
