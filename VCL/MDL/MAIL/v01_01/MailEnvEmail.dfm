object FmMailEnvEmail: TFmMailEnvEmail
  Left = 339
  Top = 185
  Anchors = []
  Caption = 'MAI-ENVIA-002 :: Envio de e-mails - Contatos'
  ClientHeight = 555
  ClientWidth = 866
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 866
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 807
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 748
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de e-mails - Contatos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de e-mails - Contatos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Envio de e-mails - Contatos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 866
    Height = 356
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 866
      Height = 356
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 866
        Height = 356
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 862
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesquisa: TdmkEdit
            Left = 0
            Top = 0
            Width = 862
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdPesquisaChange
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 44
          Width = 862
          Height = 310
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'EMail'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CONNOME'
              Title.Caption = 'Contato'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENTNOME'
              Title.Caption = 'Entidade'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTO'
              Title.Caption = 'Tipo de e-mail'
              Width = 100
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntiMail
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dmkDBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'EMail'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CONNOME'
              Title.Caption = 'Contato'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENTNOME'
              Title.Caption = 'Entidade'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTO'
              Title.Caption = 'Tipo de e-mail'
              Width = 100
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 415
    Width = 866
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 862
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 384
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#234' um duplo clique na grade para adicionar o e-mail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 384
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#234' um duplo clique na grade para adicionar o e-mail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 469
    Width = 866
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 687
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 685
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEntidades: TBitBtn
        Tag = 10062
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Entidades'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtEntidadesClick
      end
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 260
    Top = 224
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ema.EMail, con.Nome CONNOME,  etc.Nome NOMECTO,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTNOME, ent.Codigo'
      'FROM entimail ema'
      'LEFT JOIN enticontat con ON con.Controle = ema.Controle'
      'LEFT JOIN entidades ent ON ent.Codigo = ema.Codigo'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto '
      'WHERE IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%%" '
      'OR con.Nome LIKE "%%" OR ema.EMail LIKE "%%" '
      'ORDER BY ENTNOME, ema.Ordem, ema.Conta')
    Left = 232
    Top = 224
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
    object QrEntiMailENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
    object QrEntiMailCONNOME: TWideStringField
      FieldName = 'CONNOME'
      Size = 30
    end
    object QrEntiMailNOMECTO: TWideStringField
      FieldName = 'NOMECTO'
    end
    object QrEntiMailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
