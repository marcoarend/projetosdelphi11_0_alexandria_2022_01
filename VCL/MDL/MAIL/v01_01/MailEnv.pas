unit MailEnv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkCheckGroup,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, UnInternalConsts, dmkImage, dmkPopOutFntCBox, ToolWin,
  OleCtrls, SHDocVw, ImgList, dmkMemo, IdBaseComponent, IdMessage, IdIMAP4,
  IdAttachmentFile, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL, IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient,
  IdExplicitTLSClientServerBase, IdSMTPBase, IdSMTP, IdText, UnDmkProcFunc,
  CommCtrl, ShellAPI, MSHTML, UnDmkEnums;

type
  TFmMailEnv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtEnviar: TBitBtn;
    LaTitulo1C: TLabel;
    PnTopo: TPanel;
    EdPara: TdmkEdit;
    Label23: TLabel;
    Label1: TLabel;
    EdCC: TdmkEdit;
    Label2: TLabel;
    EdCCO: TdmkEdit;
    Label3: TLabel;
    EdAssunto: TdmkEdit;
    SBPara: TSpeedButton;
    SBCc: TSpeedButton;
    SBCco: TSpeedButton;
    CkConfLei: TCheckBox;
    CkTextoPlano: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    WebBrowser1: TWebBrowser;
    MeTextoPlano: TdmkMemo;
    Panel10: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ToolBar1: TToolBar;
    CBFontName: TdmkPopOutFntCBox;
    TBNegrito: TToolButton;
    TBItalico: TToolButton;
    TBSublinhado: TToolButton;
    TBCor: TToolButton;
    ToolButton5: TToolButton;
    TBListaNum: TToolButton;
    TBLista: TToolButton;
    TBDimRec: TToolButton;
    TBAumRec: TToolButton;
    ToolButton10: TToolButton;
    TTBAlinEsq: TToolButton;
    TTBAlinCen: TToolButton;
    TTBAlinDir: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    QrPreEmMsg: TmySQLQuery;
    QrPreEmMsgNOMETIPO: TWideStringField;
    QrPreEmMsgCodigo: TIntegerField;
    QrPreEmMsgControle: TIntegerField;
    QrPreEmMsgOrdem: TIntegerField;
    QrPreEmMsgTipo: TIntegerField;
    QrPreEmMsgTexto: TWideMemoField;
    QrPreEmMsgLk: TIntegerField;
    QrPreEmMsgDataCad: TDateField;
    QrPreEmMsgDataAlt: TDateField;
    QrPreEmMsgUserCad: TIntegerField;
    QrPreEmMsgUserAlt: TIntegerField;
    QrPreEmMsgAlterWeb: TSmallintField;
    QrPreEmMsgAtivo: TSmallintField;
    QrPreEmMsgDescricao: TWideStringField;
    QrPreEmMsgCidID_Img: TWideStringField;
    IdSMTP1: TIdSMTP;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    TabSheet4: TTabSheet;
    MeEncode: TMemo;
    Grade: TStringGrid;
    OpenDialog1: TOpenDialog;
    PB1: TProgressBar;
    QrPreEmail: TmySQLQuery;
    BitBtn1: TBitBtn;
    LaSolicitante: TLabel;
    EdEmailConta: TdmkEditCB;
    CBEmailConta: TdmkDBLookupComboBox;
    SBSolicitante: TSpeedButton;
    QrEmailConta: TmySQLQuery;
    DsEmailCfg: TDataSource;
    QrEmailCfg: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IMAP: TIdIMAP4;
    QrLoc: TmySQLQuery;
    TTBAlinJus: TToolButton;
    ToolButton1: TToolButton;
    ToolBar2: TToolBar;
    CBFontSize: TComboBox;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    TTBSub: TToolButton;
    TTBSob: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    CkDestinatarios: TCheckBox;
    SBInfoDest: TSpeedButton;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PFCpaLin3FonNomChange(Sender: TObject);
    procedure TBNegritoClick(Sender: TObject);
    procedure TBItalicoClick(Sender: TObject);
    procedure TBSublinhadoClick(Sender: TObject);
    procedure TBCorClick(Sender: TObject);
    procedure TBListaNumClick(Sender: TObject);
    procedure TBListaClick(Sender: TObject);
    procedure TBDimRecClick(Sender: TObject);
    procedure TBAumRecClick(Sender: TObject);
    procedure TTBAlinEsqClick(Sender: TObject);
    procedure TTBAlinCenClick(Sender: TObject);
    procedure TTBAlinDirClick(Sender: TObject);
    procedure ToolButton15Click(Sender: TObject);
    procedure ToolButton16Click(Sender: TObject);
    procedure CBFontSizeChange(Sender: TObject);
    procedure CkTextoPlanoClick(Sender: TObject);
    procedure SBParaClick(Sender: TObject);
    procedure SBCcClick(Sender: TObject);
    procedure SBCcoClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtEnviarClick(Sender: TObject);
    procedure IdSMTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure IdSMTP1Connected(Sender: TObject);
    procedure IdSMTP1Disconnected(Sender: TObject);
    procedure IdSMTP1FailedRecipient(Sender: TObject; const AAddress, ACode,
      AText: string; var VContinue: Boolean);
    procedure IdSMTP1TLSNotAvailable(Asender: TObject; var VContinue: Boolean);
    procedure IdSMTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdSMTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdSMTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure BitBtn1Click(Sender: TObject);
    procedure SBSolicitanteClick(Sender: TObject);
    procedure TTBAlinJusClick(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure TTBSubClick(Sender: TObject);
    procedure TTBSobClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure SBInfoDestClick(Sender: TObject);
  private
    { Private declarations }
    FIdMessage, FIdMessageCxaSai: TIdMessage;
    FImageMsg: Boolean;
    procedure ConfiguraAbas();
    procedure MostraEnvEmail(Tipo: Integer);
    procedure AdicionaAnexo(Caminho: String);
    procedure CarregaMensagem(PreEmail: Integer);
    function  SubstituiVariaveis(Texto: WideString): String;
    procedure ReopenEmailConta(Codigo: Integer);
    procedure ReopenPreEmail(Codigo: Integer);
    procedure ReopenEmailCfg(Codigo: Integer);
    //procedure SalvaNoDiretorioDeItensEnviados(); Movido para o UnMail
  public
    { Public declarations }
    FMailCfg, FPreEmail, FEmailConta, FExtraLen: Integer;
    FMaxSize: String;
    FEnviou, FIndiviDestin: Boolean;
    FTipoEmail: TMaiEnvType;
    FPara, FCCo: String;
    FAnexos: array of String;
    //FTextoTags: array[0..5] of String;
    FTextoTags: array of String;
    procedure ConfiguraEmail();
    function  EnviaEmail(var Msg: String; Destinatario: String = ''; 
              MostraMsg: Boolean = True): Boolean;
  end;

var
  FmMailEnv: TFmMailEnv;

const
  FIniPath = CO_DIR_RAIZ_DMK + '\Base64\';
  FBase64  = CO_DIR_RAIZ_DMK + '\Base64\Base64.exe';

implementation

uses {$IfNDef ServicoDoWindows}MyDBCheck, {$EndIf} DmkDAC_PF, UnMyObjects,
  Module, MailEnvEmail, UMySQLModule, ModuleGeral, UnMailEnv, UnMail,
  UnGrl_Consts, MyGlyfs, UnDmkHTML2, UnTextos_Jan, UnMailAllOS, UnitDmkTags;

{$R *.DFM}

procedure TFmMailEnv.CarregaMensagem(PreEmail: Integer);

  procedure AnexaHTMLs(PreEmail: Integer);
  var
    Doc: Variant;
    TextoHtml: WideString;
  begin
    TextoHtml := '';
    //
    QrPreEmMsg.Close;
    QrPreEmMsg.Params[0].AsInteger := PreEmail;
    QrPreEmMsg.Params[1].AsInteger := 1;//Html
    UMyMod.AbreQuery(QrPreEmMsg, DMod.MyDB);
    //
    if QrPreEmMsg.RecordCount > 0 then
    begin
      QrPreEmMsg.First;
      while not QrPreEmMsg.Eof do
      begin
        TextoHtml := TextoHtml + SubstituiVariaveis(QrPreEmMsgTexto.Value);
        //
        QrPreEmMsg.Next;
      end;
    end;
    if TextoHtml <> '' then
    begin
      Doc := WebBrowser1.Document;
      Doc.Clear;
      Doc.Write(TextoHtml);
      Doc.Close;
      //
      CkTextoPlano.Checked := False;
    end;
  end;

  procedure AnexaTextoPlano(PreEmail: Integer);
  var
    Texto: WideString;
  begin
    QrPreEmMsg.Close;
    QrPreEmMsg.Params[0].AsInteger := PreEmail;
    QrPreEmMsg.Params[1].AsInteger := 0;//Texto plano
    UMyMod.AbreQuery(QrPreEmMsg, DMod.MyDB);
    //
    if QrPreEmMsg.RecordCount > 0 then
    begin
      Texto := '';
      QrPreEmMsg.First;
      while not QrPreEmMsg.Eof do
      begin
        Texto := Texto + sLineBreak + SubstituiVariaveis(QrPreEmMsgTexto.Value);
        QrPreEmMsg.Next;
      end;
    end;
    if Texto <> '' then
    begin
      MeTextoPlano.Text := Texto;
      //
      CkTextoPlano.Checked := True;
    end;
  end;

  procedure AnexaImagensCorpo(PreEmail: Integer);
  begin
    QrPreEmMsg.Close;
    QrPreEmMsg.Params[0].AsInteger := PreEmail;
    QrPreEmMsg.Params[1].AsInteger := 2;//Verifica se tem imagens
    UMyMod.AbreQuery(QrPreEmMsg, DMod.MyDB);
    //
    if QrPreEmMsg.RecordCount > 0 then
      FImageMsg := True;
  end;

begin
  AnexaHTMLs(PreEmail);
  AnexaTextoPlano(PreEmail);
  AnexaImagensCorpo(PreEmail);
end;

procedure TFmMailEnv.CBFontSizeChange(Sender: TObject);
begin
  dmkHTML2.FonteMudaTamanho(WebBrowser1, CBFontSize);
end;

procedure TFmMailEnv.CkTextoPlanoClick(Sender: TObject);
begin
  ConfiguraAbas();
end;

procedure TFmMailEnv.ConfiguraAbas();
var
  Enab: Boolean;
begin
  TabSheet4.TabVisible := False;
  MeEncode.Visible     := False;
  //
  Enab := CkTextoPlano.Checked;
  //
  TabSheet1.TabVisible := not Enab;
  ToolBar1.Visible     := not Enab;
  TabSheet2.TabVisible := Enab;
  //
  if Enab then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
end;

procedure TFmMailEnv.IdSMTP1Connected(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1Connected(');
end;

procedure TFmMailEnv.IdSMTP1Disconnected(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1Disconnected!');
end;

procedure TFmMailEnv.IdSMTP1FailedRecipient(Sender: TObject; const AAddress,
  ACode, AText: string; var VContinue: Boolean);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1FailedRecipient: ' +
    'Codigo = ' + ACode + ' Texto: ' + AText);
  VContinue := True;
end;

procedure TFmMailEnv.IdSMTP1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1Status:' + AStatusText);
end;

procedure TFmMailEnv.IdSMTP1TLSNotAvailable(Asender: TObject;
  var VContinue: Boolean);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1TLSNotAvailable');
  VContinue := True;
end;

procedure TFmMailEnv.IdSMTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  PB1.Position := AWorkCount;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviado ' +
  IntToStr(AWorkCount) + FMaxSize);
end;

procedure TFmMailEnv.IdSMTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
var
  K: Integer;
begin
  PB1.Position := 0;
  if AWorkCountMax > 0 then
    K := AWorkCountMax
  else
  begin
    {
    for I := 1 to Grade.RowCount - 1 do
    begin
      K := K + Geral.FileSize(Grade.Cells[1, I]);
    end;
    }
    K := FExtraLen;
  end;
  PB1.Max := K;
  FMaxSize := ' de ' + IntToStr(K);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'IdSMTP1WorkBegin: ' + FMaxSize);
end;

procedure TFmMailEnv.IdSMTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio finalizado!!!');
  PB1.Position := 0;
end;

procedure TFmMailEnv.MostraEnvEmail(Tipo: Integer);
(*
var
  Topo: Integer;
*)
begin
{$IfNDef ServicoDoWindows}
  if DBCheck.CriaFm(TFmMailEnvEmail, FmMailEnvEmail, afmoLiberado) then
{$ELSE}
  Application.CreateForm(TFmMailEnvEmail, FmMailEnvEmail);
{$EndIf}
  begin
    FmMailEnvEmail.FTipoEmail := Tipo;
    FmMailEnvEmail.ShowModal;
    FmMailEnvEmail.Destroy;
  end;
end;

procedure TFmMailEnv.AdicionaAnexo(Caminho: String);
var
  Row: Integer;
begin
  if Length(Caminho) > 0 then
  begin
    if FileExists(Caminho) then
    begin
      if (Grade.RowCount = 2) and (Length(Grade.Cells[1, 1]) = 0) then
      begin
        Grade.Cells[1, 1] := Caminho;
      end else
      begin
        Row := Grade.RowCount + 1;
        //
        Grade.Cells[1, Row - 1] := Caminho;
        Grade.RowCount := Row;
      end;
    end;
  end;
end;

procedure TFmMailEnv.BitBtn1Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := Grade.Cells[Grade.Col, Grade.Row];
  //
  if not FileExists(Arquivo) then Exit;
  //
  if Geral.MensagemBox('Deseja abrir o arquivo?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION, 0) = ID_YES then
  begin
    Geral.AbreArquivo(Arquivo, True);
  end;
end;

procedure TFmMailEnv.BitBtn2Click(Sender: TObject);
var
  Arquivo: String;
begin
  if OpenDialog1.Execute then
  begin
    Arquivo := OpenDialog1.FileName;
    //
    AdicionaAnexo(Arquivo);
  end;
end;

procedure TFmMailEnv.BitBtn3Click(Sender: TObject);
begin
  MyObjects.ExcluiLinhaStringGrid(Grade);
end;

function TFmMailEnv.EnviaEmail(var Msg: String; Destinatario: String = ''; 
  MostraMsg: Boolean = True): Boolean;

  function AnexaTextoPlano(IdMessage: TIdMessage): Boolean;
  var
    Txt: String;
  begin
    Result := False;
    try
      Screen.Cursor := crHourGlass;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Indexando mensagem do tipo texto plano!');
      Application.ProcessMessages;
      with TIdText.Create(IdMessage.MessageParts) do
      begin
        Txt         := MeTextoPlano.Text;
        Body.Text   := Txt;
        ContentType := 'text/plain';
        //
        Result := True;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

  function AnexaHTMLs(IdMessage: TIdMessage): Boolean;
  var
    WebBrowser: TWebBrowser;
    Doc: Variant;
    TextoHTML: TIdText;
    Html: IHTMLElement;
    Txt: WideString;
    ImgLink: String;
    Ini, Fim: Integer;
  begin
    Result := False;
    try
      Screen.Cursor := crHourGlass;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Indexando mensagem do tipo HTML!');
      Application.ProcessMessages;
      TextoHTML := TIdText.Create(IdMessage.MessageParts);
      with TextoHTML do
      begin
        ContentType := 'text/html';
        Body.Text   := '';
        //
        if Assigned(WebBrowser1.Document) then
        begin
          Html := (WebBrowser1.Document AS IHTMLDocument2).body;
          while Html.parentElement <> nil do
            Html := Html.parentElement;

          Txt := Html.outerHTML;

          if (IdMessage = FIdMessageCxaSai) and (FTipoEmail in [meBloq, meBloqCnd, meNFSe]) then
          begin
            WebBrowser := TWebBrowser.Create(nil);
            try
              WebBrowser.Navigate('about:blank');
              Doc := WebBrowser.Document;
              Doc.Clear;
              //
              Doc.Close;
              //
              Html := (WebBrowser.Document AS IHTMLDocument2).body;
              //
              if Html <> nil then
              begin
                while Html.parentElement <> nil do
                  Html := Html.parentElement;

                ImgLink := Html.outerHTML;
                Ini     := Pos('<img', LowerCase(ImgLink));

                if Ini > 0 then
                begin
                  ImgLink := Copy(ImgLink, Ini);
                  Fim     := Pos('>', ImgLink);
                  ImgLink := Copy(ImgLink, 0, Fim);
                end;
                Txt := StringReplace(Txt, ImgLink, '', [rfReplaceAll, rfIgnoreCase]);
              end;
            finally
              WebBrowser.Free;
            end;
          end;
          Txt := dmkPF.TextoToHTMLUnicode(Txt);

          Body.Text := Txt;
          //
          Result := True;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

  function AnexaImagensCorpo(PreEmail: Integer; IdMessage: TIdMessage): Boolean;
  var
    ImgPath, TxtPath: String;
    ArqExiste: Boolean;
    Comando: PChar;
  begin
    Result := False;
    try
      Screen.Cursor := crHourGlass;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Anexando imagens no campo da mensagem!');
      Application.ProcessMessages;
      QrPreEmMsg.Close;
      QrPreEmMsg.Params[0].AsInteger := PreEmail;
      QrPreEmMsg.Params[1].AsInteger := 2;//Verifica se tem imagens
      UMyMod.AbreQuery(QrPreEmMsg, DMod.MyDB);
      //
      if QrPreEmMsg.RecordCount > 0 then
      begin
        QrPreEmMsg.First;
        while not QrPreEmMsg.Eof do
        begin
          ImgPath := FIniPath + FormatFloat('00000000', QrPreEmMsgControle.Value) +
          '\' + QrPreEmMsgDescricao.Value;
          TxtPath := dmkPF.MudaExtensaoDeArquivo(ImgPath, 'txt');
          //
          ArqExiste := FileExists(ImgPath);
          if not ArqExiste then
          begin
            if FileExists(FBase64) then
            begin
              Geral.SalvaTextoEmArquivo(TxtPath, SubstituiVariaveis(QrPreEmMsgTexto.Value), True);
              Comando := PChar(FBase64 + ' -d ' + TxtPath + ' ' + ImgPath);
              dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, MeEncode, True);
              ArqExiste := FileExists(ImgPath);
              //
              Result := True;
            end else
            begin
              if MostraMsg then
                Geral.MensagemBox('O aplicativo "' + FBase64 +
                  '" n�o existe para criar a imagem "' + ImgPath + '".', 'Aviso',
                  MB_OK+MB_ICONWARNING, 0)
              else
                Msg := 'O aplicativo "' + FBase64 +
                       '" n�o existe para criar a imagem "' + ImgPath + '".';
              Exit;
            end;
          end;
          if ArqExiste then
          begin
            with TIdAttachmentFile.Create(IdMessage.MessageParts, ImgPath) do
            begin
              ContentType        := 'image/jpg';
              ContentDisposition := 'inline';
              ContentTransfer    := 'base64';
              ContentID          := '<' + QrPreEmMsgCidID_Img.Value + '>';
            end;
          end;
          QrPreEmMsg.Next;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

  function AnexaArquivos(IdMessage: TIdMessage): Boolean;
  var
    Arquivo, Ext: String;
    I: Integer;
  begin
    Result := False;
    try
      Screen.Cursor := crHourGlass;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Anexando arquivos na mensagem!');
      Application.ProcessMessages;
      for I := 1 to Grade.RowCount - 1 do
      begin
        Arquivo := Grade.Cells[1, I];
        Ext     := Copy(LowerCase(ExtractFileExt(Arquivo)), 2);
        //
        if Arquivo <> '' then
        begin
          with TIdAttachmentFile.Create(IdMessage.MessageParts, Arquivo) do
          begin
            ContentType        := 'application/' + Ext;
            ContentDisposition := 'attachment';
            ContentTransfer    := 'base64';
          end;
          Result := True;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
const
  CO_Msg_AnexaTextoPlano = 'Falha ao anexar texto plano!';
  CO_Msg_AnexaHTMLs = 'Falha ao anexar texto HTML!';
  CO_Msg_AnexaImagensCorpo = 'Falha ao anexar imagens ao corpo do e-mail!';
  CO_Msg_AnexaArquivos = 'Falha ao anexar arquivos no e-mail!';
var
  AnexoArq: Boolean;
  Ss: TStringStream;
  S, Para, Msg2: String;
begin
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Reabrindo conta');
  //
  ReopenEmailConta(EdEmailConta.ValueVariant);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo destinat�rio');
  if Destinatario = '' then
    Para := EdPara.ValueVariant
  else
    Para := Destinatario;
  //
  if Para = '' then
  begin
    if MostraMsg then
      Geral.MB_Info('Nenhum destinat�rio foi definido!')
    else
      Msg := 'Nenhum destinat�rio foi definido!';
    Exit;
  end;
  if QrEmailConta.RecordCount = 0 then
  begin
    if MostraMsg then
      Geral.MB_Aviso('Envio abortado!' + sLineBreak + 'Motivo: Configura��o n�o definida.')
    else
      Msg := 'Envio abortado!' + sLineBreak + 'Motivo: Configura��o n�o definida.';
    Exit;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Desconectando SMTP');
  if IdSMTP1.Connected then
    IdSMTP1.Disconnect(True);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando SMTP');
  IdSMTP1.Host     := QrEmailConta.FieldByName('SMTPServer').AsString;
  IdSMTP1.Username := QrEmailConta.FieldByName('Usuario').AsString;
  IdSMTP1.Password := QrEmailConta.FieldByName('SENHA').AsString;
  //
  if QrEmailConta.FieldByName('Porta_Mail').AsInteger > 0 then
    IdSMTP1.Port := QrEmailConta.FieldByName('Porta_Mail').AsInteger
  else
    IdSMTP1.Port := 25;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando SSL');
  UMailAllOS.ConfiguraConexaoSSL(IdSMTP1, IdSSL1,
    QrEmailConta.FieldByName('Logi_SSL').AsInteger,
    QrEmailConta.FieldByName('EnvTLSUse').AsInteger,
    QrEmailConta.FieldByName('EnvTLSTipo').AsInteger);
  //
  if (IdSMTP1.Host = '') or (IdSMTP1.Username = '') or (IdSMTP1.Password = '') then
  begin
    if MostraMsg then
      Geral.MB_Info('Os dados de autentica��o est�o incompletos!' + sLineBreak +
        'Verifique se os campos: Host, Usu�rio e Senha est�o preenchidos no cadastro do pr� e-mail!')
    else
      Msg := 'Os dados de autentica��o est�o incompletos!' + sLineBreak +
             'Verifique se os campos: Host, Usu�rio e Senha est�o preenchidos no cadastro do pr� e-mail!';
    Exit;
  end;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'CONNECTANDO S M T P !!!!!!!');
    try
      MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Tentativa de conex�o 1');
      IdSMTP1.Connect;
    except
      on E: Exception do
      begin
        Msg2 := E.Message;
        if (E is EIdOSSLCouldNotLoadSSLLibrary) then
        try
          MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Tentativa de conex�o 2');
          try
            IdSMTP1.Connect;
          except
            MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Tentativa de conex�o 3');
            try
              IdSMTP1.Connect;
            except
              Geral.MB_Erro(IdSMTP1.LastCmdResult.Text.Text);
            end;
          end;
        except
          Geral.MB_Erro(Msg2);
        end
        else
          Geral.MB_Erro(Msg2);
      end;
    end;
  except
    raise;
      if MostraMsg then
        Geral.MB_Aviso('Erro na Conex�o...' + sLineBreak +
          'Verifique os dados de autentica��o na configura��o do pr� e-mail!')
      else
        Msg := 'Erro na Conex�o...' + sLineBreak +
               'Verifique os dados de autentica��o na configura��o do pr� e-mail!';
    Exit;
  end;
  if IdSMTP1.Connected then
  begin
    MyObjects.Informa2(LaAviso3, LaAviso4, False, '');
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando dados e-mail');
    FIdMessage                           := TIdMessage.Create(nil);
    FIdMessage.Subject                   := EdAssunto.ValueVariant;
    FIdMessage.From.Address              := QrEmailConta.FieldByName('Usuario').AsString;
    FIdMessage.From.Name                 := QrPreEmail.FieldByName('Send_Name').AsString;
    FIdMessage.Recipients.EMailAddresses := Para;
    FIdMessage.CCList.EMailAddresses     := EdCC.ValueVariant;
    FIdMessage.BccList.EMailAddresses    := EdCCO.ValueVariant;
    FIdMessage.Encoding                  := meMIME;
    FIdMessage.CharSet                   := 'iso-8859-1';
    //
    FIdMessageCxaSai                           := TIdMessage.Create(nil);
    FIdMessageCxaSai.Subject                   := EdAssunto.ValueVariant;
    FIdMessageCxaSai.From.Address              := QrEmailConta.FieldByName('Usuario').AsString;
    FIdMessageCxaSai.From.Name                 := QrPreEmail.FieldByName('Send_Name').AsString;
    FIdMessageCxaSai.Recipients.EMailAddresses := Para;
    FIdMessageCxaSai.CCList.EMailAddresses     := EdCC.ValueVariant;
    FIdMessageCxaSai.BccList.EMailAddresses    := EdCCO.ValueVariant;
    FIdMessageCxaSai.Encoding                  := meMIME;
    FIdMessageCxaSai.CharSet                   := 'iso-8859-1';
    //
    if CkConfLei.Checked then
    begin
      FIdMessage.ReceiptRecipient.Text       := QrEmailConta.FieldByName('Usuario').AsString;
      FIdMessageCxaSai.ReceiptRecipient.Text := QrEmailConta.FieldByName('Usuario').AsString;
    end else
    begin
      FIdMessage.ReceiptRecipient.Text       := '';
      FIdMessageCxaSai.ReceiptRecipient.Text := '';
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Anexando');
    AnexoArq := (Grade.RowCount >= 2) and (Grade.Cells[1, 1] <> '');
    //
    try
      BtEnviar.Enabled := False;
      //
      if not FImageMsg and not AnexoArq then
      begin
        if CkTextoPlano.Checked then
        begin
          if not AnexaTextoPlano(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
          if not AnexaTextoPlano(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
        end else
        begin
          if not AnexaHTMLs(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
          if not AnexaHTMLs(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
        end;
      end
      else if AnexoArq and FImageMsg then
      begin
        if CkTextoPlano.Checked then
        begin
          if not AnexaTextoPlano(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
          if not AnexaTextoPlano(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
        end else
        begin
          if not AnexaHTMLs(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
          if not AnexaHTMLs(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
        end;
        if not AnexaImagensCorpo(QrPreEmail.FieldByName('Codigo').AsInteger, FIdMessage) then
        begin
          if MostraMsg then
            Geral.MB_Erro(CO_Msg_AnexaImagensCorpo)
          else
            Msg := CO_Msg_AnexaImagensCorpo;
          Exit;
        end;
        if not AnexaImagensCorpo(QrPreEmail.FieldByName('Codigo').AsInteger, FIdMessageCxaSai) then
        begin
          if MostraMsg then
            Geral.MB_Erro(CO_Msg_AnexaImagensCorpo)
          else
            Msg := CO_Msg_AnexaImagensCorpo;
          Exit;
        end;
        //
        if AnexoArq then
        begin
          if not AnexaArquivos(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaArquivos)
            else
              Msg := CO_Msg_AnexaArquivos;
            Exit;
          end;
          if not AnexaArquivos(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaArquivos)
            else
              Msg := CO_Msg_AnexaArquivos;
            Exit;
          end;
        end;
      end
      else if not AnexoArq and FImageMsg then
      begin
        if CkTextoPlano.Checked then
        begin
          if not AnexaTextoPlano(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
          if not AnexaTextoPlano(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
        end else
        begin
          if not AnexaHTMLs(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
          if not AnexaHTMLs(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
        end;
        if not AnexaImagensCorpo(QrPreEmail.FieldByName('Codigo').AsInteger, FIdMessage) then
        begin
          if MostraMsg then
            Geral.MB_Erro(CO_Msg_AnexaImagensCorpo)
          else
            Msg := CO_Msg_AnexaImagensCorpo;
          Exit;
        end;
        if not AnexaImagensCorpo(QrPreEmail.FieldByName('Codigo').AsInteger, FIdMessageCxaSai) then
        begin
          if MostraMsg then
            Geral.MB_Erro(CO_Msg_AnexaImagensCorpo)
          else
            Msg := CO_Msg_AnexaImagensCorpo;
          Exit;
        end;
      end
      else if AnexoArq and not FImageMsg then
      begin
        // Texto puro, HTML e anexos
        if CkTextoPlano.Checked then
        begin
          if not AnexaTextoPlano(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
          if not AnexaTextoPlano(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaTextoPlano)
            else
              Msg := CO_Msg_AnexaTextoPlano;
            Exit;
          end;
        end else
        begin
          if not AnexaHTMLs(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
          if not AnexaHTMLs(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaHTMLs)
            else
              Msg := CO_Msg_AnexaHTMLs;
            Exit;
          end;
        end;
        if AnexoArq then
        begin
          if not AnexaArquivos(FIdMessage) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaArquivos)
            else
              Msg := CO_Msg_AnexaArquivos;
            Exit;
          end;
          if not AnexaArquivos(FIdMessageCxaSai) then
          begin
            if MostraMsg then
              Geral.MB_Erro(CO_Msg_AnexaArquivos)
            else
              Msg := CO_Msg_AnexaArquivos;
            Exit;
          end;
        end;
      end;
      try
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando mensagem em mem�ria!');
        Screen.Cursor := crHourGlass;
        Ss := TStringStream.Create(S);
        try
          FIdMessage.SaveToStream(Ss);
          FIdMessageCxaSai.SaveToStream(Ss);
          FExtraLen := Ss.Size;
        finally
          Ss.Free;
        end;

        // ini 2022-07-18
        //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Autenticando');
        //idSMTP1.Authenticate;
        // fim 2022-07-18

        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando mensagem');
        IdSMTP1.Send(FIdMessage);
        IdSMTP1.Disconnect;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando e-mail enviado');
        UMail.SalvaEmailNosItensEnviados(EdEmailConta.ValueVariant, DModG.QrAux,
          Dmod.MyDB, IMAP, IdSSL1, FIdMessageCxaSai, LaAviso1, LaAviso2);
        //
        FIdMessage.Free;
        FIdMessageCxaSai.Free;
        //
        FEnviou := True;
        Screen.Cursor := crDefault;
        //
        Result := True;
      except
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Falha ao enviar e-mail!');
        raise;
        Screen.Cursor := crDefault;
      end;
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      BtEnviar.Enabled := True;
    end;
  end;
end;

procedure TFmMailEnv.BtEnviarClick(Sender: TObject);
var
  Lista: TStringList;
  EmailConta, I: Integer;
  Para, Msg: String;
begin
  try
    BtEnviar.Enabled := False;
    //
    EmailConta := EdEmailConta.ValueVariant;
    FExtraLen  := 0;
    //
    if MyObjects.FIC(EmailConta = 0, nil, 'Configura��o de e-mail n�o definida!') then Exit;
    //
    if CkDestinatarios.Checked = True then
    begin
      Para := Trim(EdPara.ValueVariant);
      //
      if MyObjects.FIC(Para = '', EdPara, 'Nenhum destinat�rio foi definido!') then Exit;
      //
      Lista := TStringList.Create;
      try
        Lista := Geral.Explode(Para, ';', 1);
        //
        for I := 0 to Lista.Count - 1 do
        begin
//          if not EnviaEmail(Msg, Lista[I], False) then
          try
            EnviaEmail(Msg, Lista[I], False);
          except
            on E: EIdOSSLCouldNotLoadSSLLibrary do
            begin
              if not EnviaEmail(Msg, Lista[I], False) then
                Geral.MB_Erro(Msg);
            end;
            else
              Geral.MB_Erro(Msg);
          end;
        end;
      finally
        Lista.Free;
      end;
    end else
      EnviaEmail(Msg);
  finally
    BtEnviar.Enabled := True;
  end;
  Close;
end;

procedure TFmMailEnv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMailEnv.FormActivate(Sender: TObject);
begin
{$IfNDef ServicoDoWindows}
  MyObjects.CorIniComponente();
{$EndIf}
end;

procedure TFmMailEnv.FormCreate(Sender: TObject);
  procedure MontaStringGrid;
  begin
    MyObjects.LimpaGrade(Grade, 1, 1, True);
    //
    Grade.ColCount := 2;
    Grade.RowCount := 2;
    //
    Grade.Cells[1, 0] := 'Anexos';
  end;
begin
  ImgTipo.SQLType := stLok;
  FImageMsg       := False;
  FEnviou         := False;
  //
  MyObjects.Informa2(LaAviso3, LaAviso4, False, '');
  ToolBar1.Images := FmMyGlyfs.Lista_32X32_Textos;
  //
  dmkHTML2.DocumentoEmBranco(webbrowser1, True);
  //
  ConfiguraAbas();
  MontaStringGrid;
  //
  //Could not load SSL library.
end;

procedure TFmMailEnv.FormDestroy(Sender: TObject);
  procedure ExcluiAnexo(Caminho: String);
  begin
    if Caminho <> '' then
    begin
      if FileExists(Caminho) then
        DeleteFile(Caminho);
    end;
  end;
var
  I: Integer;
begin
  for I := Low(FAnexos) to High(FAnexos) do
    ExcluiAnexo(FAnexos[I]);
end;

procedure TFmMailEnv.FormResize(Sender: TObject);
begin
{$IfNDef ServicoDoWindows}
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
{$EndIf}
end;

procedure TFmMailEnv.PFCpaLin3FonNomChange(Sender: TObject);
begin
  dmkHTML2.FonteMuda(WebBrowser1, CBFontName.FonteNome);
end;

procedure TFmMailEnv.ReopenEmailCfg(Codigo: Integer);
(*
var
  SQL: String;
*)
begin
(*
  if Codigo <> 0 then
    SQL := 'AND con.Codigo=' + Geral.FF0(Codigo)
  else
    SQL := '';
*)
  //Mostrar Todos mas selecionar o correto
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmailCfg, Dmod.MyDB, [
    'SELECT con.Codigo, con.Nome ',
    'FROM emailconta con  ',
    'WHERE con.TipoConta = 1 ',
    'AND con.PermisNiv IN (0, ' + Geral.FF0(VAR_USUARIO) + ') ',
    //SQL,
    'ORDER BY con.Nome ',
    '']);
end;

procedure TFmMailEnv.ReopenEmailConta(Codigo: Integer);
var
  SQL: String;
begin
  if Codigo <> 0 then
    SQL := 'AND con.Codigo=' + Geral.FF0(Codigo)
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmailConta, Dmod.MyDB, [
    'SELECT con.Codigo, con.SMTPServer, con.Porta_Mail, con.Logi_SSL, con.Logi_Name, ',
    'con.Usuario, AES_DECRYPT(con.Logi_Pass, "' + CO_RandStrWeb01 + '") Logi_SENHA, ',
    'con.Logi_Auth, AES_DECRYPT(con.Password, "' + CO_RandStrWeb01 + '") SENHA,  ',
    'con.EnvTLSTipo, con.ReceTLSTipo, con.EnvTLSUse, con.ReceTLSUse ',
    'FROM emailconta con  ',
    'WHERE con.TipoConta = 1 ',
    'AND con.PermisNiv IN (0, ' + Geral.FF0(VAR_USUARIO) + ') ',
    SQL,  
    'ORDER BY con.Nome ',
    '']);
end;

procedure TFmMailEnv.ReopenPreEmail(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPreEmail, Dmod.MyDB, [
    'SELECT pre.Mail_Titu, pre.ReturnReciept, ',
    'pre.Saudacao, pre.Send_Name, ',
    'pre.NaoEnvBloq, pre.EmailConta ',
    'FROM preemail pre ',
    'WHERE pre.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmMailEnv.ConfiguraEmail();
var
  Continua: Boolean;
  I: Integer;
begin
  ReopenPreEmail(FPreEmail);
  //
  if QrPreEmail.RecordCount > 0 then
  begin
    ReopenEmailCfg(QrPreEmail.FieldByName('EmailConta').AsInteger);
    CarregaMensagem(FPreEmail);
    //
    EdEmailConta.ValueVariant := QrPreEmail.FieldByName('EmailConta').AsInteger;
    CBEmailConta.KeyValue     := QrPreEmail.FieldByName('EmailConta').AsInteger;
    EdAssunto.ValueVariant    := SubstituiVariaveis(QrPreEmail.FieldByName('Mail_Titu').AsString);
    CkConfLei.Checked         := Geral.IntToBool(QrPreEmail.FieldByName('ReturnReciept').AsInteger);
    BtEnviar.Enabled          := True;
    Continua                  := QrEmailCfg.RecordCount > 0;
  end else
  begin
    if FTipoEmail = meAvul then
    begin
      ReopenEmailCfg(0);
      //
      EdEmailConta.ValueVariant := FMailCfg;
      CBEmailConta.KeyValue     := FMailCfg;
      //
      Continua := QrEmailCfg.RecordCount > 0;
    end else
      Continua := FEmailConta <> 0;
  end;
  if not Continua then
  begin
    Geral.MensagemBox('N�o existe nenhuma configura��o de e-mail cadastrada para a filial!',
      'Aviso', MB_OK+MB_ICONWARNING, 0);
    BtEnviar.Enabled := False;
  end;
  //
  if FCCo <> '' then
    EdCCO.ValueVariant := FCCo;
  //
  if FPara <> '' then
    EdPara.ValueVariant := FPara;
  //
  CkDestinatarios.Checked := FIndiviDestin;
  //
  if QrPreEmail.FieldByName('NaoEnvBloq').AsInteger = 0 then
  begin
    for I := Low(FAnexos) to High(FAnexos) do
      AdicionaAnexo(FAnexos[I]);
  end;
end;

procedure TFmMailEnv.SBParaClick(Sender: TObject);
begin
  MostraEnvEmail(0);
end;

procedure TFmMailEnv.SBSolicitanteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  UMailEnv.MostraMailCfg(EdEmailConta.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    if (QrPreEmail.State <> dsInactive) and (QrPreEmail.RecordCount > 0) then
      Codigo := QrPreEmail.FieldByName('EmailConta').AsInteger
    else
      Codigo := 0;
    //
    ReopenEmailCfg(Codigo);
    //
    EdEmailConta.ValueVariant := VAR_CADASTRO;
    CBEmailConta.KeyValue     := VAR_CADASTRO;
    EdEmailConta.SetFocus;
  end;
end;

function TFmMailEnv.SubstituiVariaveis(Texto: WideString): String;
begin
  Result := Texto;
  //
  case FTipoEmail of
    meAvul://Avulso
    begin
      (*
      Result := Geral.Substitui(Result, '[Saudacao]', QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Cliente]' , FTextoTags[00]);
      Result := Geral.Substitui(Result, '[Data]'    , FTextoTags[01]);
      *)
      UnDmkTags.SubstituiVariaveis_Avulso(QrPreEmail.FieldByName('Saudacao').AsString,
        FTextoTags[00], FTextoTags[01], Result);
    end;
    meHisAlt://Aplicativos - Hist�rico de altera��es
    begin
      (*
      Result := Geral.Substitui(Result, '[APLICATIVO]',   FTextoTags[00]);
      Result := Geral.Substitui(Result, '[VERSAO]' ,      FTextoTags[01]);
      Result := Geral.Substitui(Result, '[APP_HISALTER]', FTextoTags[02]);
      Result := Geral.Substitui(Result, '[LINK]' ,        FTextoTags[03]);
      *)
      UnDmkTags.SubstituiVariaveis_HistoricoAlteracoes(FTextoTags[00],
        FTextoTags[01], FTextoTags[02], FTextoTags[03], Result);
    end;
    meNFSe://NFS-e
    begin
      (*
      Result := Geral.Substitui(Result, '[Empresa]' ,              DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[ChaveNFe]',              FTextoTags[01]);
      Result := Geral.Substitui(Result, '[Saudacao]',              QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Cliente]' ,              FTextoTags[00]);
      Result := Geral.Substitui(Result, '[NFSe_Serie]',            FTextoTags[02]);
      Result := Geral.Substitui(Result, '[NFSe_Numero]',           FTextoTags[03]);
      Result := Geral.Substitui(Result, '[Protocolo_e_Impressao]', FTextoTags[04]);
      Result := Geral.Substitui(Result, '[Protocolo]',             FTextoTags[05]);
      *)
      UnDmkTags.SubstituiVariaveis_NFSe(DModG.ObtemNomeFantasiaEmpresa(),
        FTextoTags[01], QrPreEmail.FieldByName('Saudacao').AsString,
        FTextoTags[00], FTextoTags[02], FTextoTags[03], FTextoTags[04],
        FTextoTags[05], Result);
    end;
    meNFe://NFe
    begin
      (*
      Result := Geral.Substitui(Result, '[Empresa]' ,        DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[ChaveNFe]',        FTextoTags[01]);
      Result := Geral.Substitui(Result, '[SerieNFe]',        FTextoTags[02]);
      Result := Geral.Substitui(Result, '[NumeroNFe]',       FTextoTags[03]);
      Result := Geral.Substitui(Result, '[Saudacao]',        QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Cliente]',         FTextoTags[00]);
      Result := Geral.Substitui(Result, '[ClienteFantasia]', FTextoTags[04]);
      *)
      UnDmkTags.SubstituiVariaveis_NFe(DModG.ObtemNomeFantasiaEmpresa(),
        FTextoTags[01], FTextoTags[02], FTextoTags[03],
        QrPreEmail.FieldByName('Saudacao').AsString, FTextoTags[00],
        FTextoTags[04], Result);
    end;
    meNFeEveCCE: //NFe Evento Carta De Corre��o Eletr�nica
    begin
      (*
      Result := Geral.Substitui(Result, '[Empresa]'          , DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[ChaveNFe]'         , FTextoTags[01]);
      Result := Geral.Substitui(Result, '[SerieNFe]'         , FTextoTags[14]);
      Result := Geral.Substitui(Result, '[NumeroNFe]'        , FTextoTags[15]);
      Result := Geral.Substitui(Result, '[Saudacao]'         , QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Cliente]'          , FTextoTags[00]);
      Result := Geral.Substitui(Result, '[versao]'           , FTextoTags[02]);
      Result := Geral.Substitui(Result, '[ORGAO]'            , FTextoTags[03]);
      Result := Geral.Substitui(Result, '[AMBIENTE]'         , FTextoTags[04]);
      Result := Geral.Substitui(Result, '[CNPJ_CPF]'         , FTextoTags[05]);
      Result := Geral.Substitui(Result, '[CHAVE_ACESSO]'     , FTextoTags[06]);
      Result := Geral.Substitui(Result, '[DATA]'             , FTextoTags[07]);
      Result := Geral.Substitui(Result, '[CODIGO_EVENTO]'    , FTextoTags[08]);
      Result := Geral.Substitui(Result, '[SEQUENCIAL_EVENTO]', FTextoTags[09]);
      Result := Geral.Substitui(Result, '[verEvento]'        , FTextoTags[10]);
      Result := Geral.Substitui(Result, '[descEvento]'       , FTextoTags[11]);
      Result := Geral.Substitui(Result, '[xCorrecao]'        , FTextoTags[12]);
      Result := Geral.Substitui(Result, '[xCondUso]'         , FTextoTags[13]);
      *)
      UnDmkTags.SubstituiVariaveis_CCe(DModG.ObtemNomeFantasiaEmpresa(),
        FTextoTags[01], FTextoTags[14], FTextoTags[15],
        QrPreEmail.FieldByName('Saudacao').AsString, FTextoTags[00],
        FTextoTags[02], FTextoTags[03], FTextoTags[04], FTextoTags[05],
        FTextoTags[06], FTextoTags[07], FTextoTags[08], FTextoTags[09],
        FTextoTags[10], FTextoTags[11], FTextoTags[12], FTextoTags[13], Result);
    end;
    meNFeEveCan: //NFe Evento de cancelamento
    begin
      (*
      Result := Geral.Substitui(Result, '[Empresa]' ,  DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[Saudacao]',  QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Cliente]' ,  FTextoTags[00]);
      Result := Geral.Substitui(Result, '[ChaveNFe]',  FTextoTags[01]);
      Result := Geral.Substitui(Result, '[SerieNFe]',  FTextoTags[02]);
      Result := Geral.Substitui(Result, '[NumeroNFe]', FTextoTags[03]);
      *)
      UnDmkTags.SubstituiVariaveis_NFe_Cancelamento(DModG.ObtemNomeFantasiaEmpresa(),
        QrPreEmail.FieldByName('Saudacao').AsString, FTextoTags[00], FTextoTags[01],
        FTextoTags[02], FTextoTags[03], Result);
    end;
    meBloq://Envio de bloquetos
    begin
      (*
      Result := Geral.Substitui(Result, '[Empresa]'              , DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[Nome]'                 , FTextoTags[00]);
      Result := Geral.Substitui(Result, '[Saudacao]'             , QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Valor]'                , FTextoTags[01]);
      Result := Geral.Substitui(Result, '[Vencimento]'           , FTextoTags[02]);
      Result := Geral.Substitui(Result, '[LinhaDigitavel]'       , FTextoTags[03]);
      Result := Geral.Substitui(Result, '[Protocolo_e_Impressao]', FTextoTags[04]);
      Result := Geral.Substitui(Result, '[Protocolo]'            , FTextoTags[05]);
      *)
      UnDmkTags.SubstituiVariaveis_Boletos(DModG.ObtemNomeFantasiaEmpresa(),
        FTextoTags[00], QrPreEmail.FieldByName('Saudacao').AsString, FTextoTags[01],
        FTextoTags[02], FTextoTags[03], FTextoTags[04], FTextoTags[05], Result);
    end;
    meBloqCnd: //Envio de bloquetos - Condom�nios
    begin
      (*
      Result := Geral.Substitui(Result, '[Administradora]', DModG.ObtemNomeFantasiaEmpresa());
      Result := Geral.Substitui(Result, '[Nome]'          , FTextoTags[00]);
      Result := Geral.Substitui(Result, '[Saudacao]'      , QrPreEmail.FieldByName('Saudacao').AsString);
      Result := Geral.Substitui(Result, '[Pronome]'       , '');
      Result := Geral.Substitui(Result, '[Valor]'         , FTextoTags[01]);
      Result := Geral.Substitui(Result, '[Vencimento]'    , FTextoTags[02]);
      Result := Geral.Substitui(Result, '[LinkConfirma]'  , FTextoTags[03]);
      Result := Geral.Substitui(Result, '[Protocolo]'     , FTextoTags[04]);
      Result := Geral.Substitui(Result, '[Condominio]'    , FTextoTags[05]);
      *)
      UnDmkTags.SubstituiVariaveis_Boletos_Condominio(DModG.ObtemNomeFantasiaEmpresa(),
        FTextoTags[00], QrPreEmail.FieldByName('Saudacao').AsString, '', FTextoTags[01],
        FTextoTags[02], FTextoTags[03], FTextoTags[04], FTextoTags[05], Result);
    end;
    meCobr:// Cobran�a
    begin
      (*
      Result := Geral.Substitui(Result, '[Time]'           , FTextoTags[00]); // Geral.FDT(Now(), 100);
      Result := Geral.Substitui(Result, '[Date]'           , FTextoTags[01]); // Geral.FDT(Date, 2);
      Result := Geral.Substitui(Result, '[Administradora]' , FTextoTags[02]); // DmodG.QrDonoNOMEDONO.Value;
      Result := Geral.Substitui(Result, '[Saudacao]'       , QrPreEmail.FieldByName('Saudacao').AsString); // '';
      Result := Geral.Substitui(Result, '[Pronome]'        , FTextoTags[04]); // QrEmailsPronome.Value;
      Result := Geral.Substitui(Result, '[Nome]'           , FTextoTags[05]); // QrEmailsNome.Value;
      Result := Geral.Substitui(Result, '[Condominio]'     , FTextoTags[06]); // QrEnvEmailNO_EMPRESA.Value;
      Result := Geral.Substitui(Result, '[Valor]'          , FTextoTags[07]); // '(solicite c�lculo do valor � administradora)';
      Result := Geral.Substitui(Result, '[Vencimento]'     , FTextoTags[08]); // Geral.FDT(QrEnvEmailVencimento.Value, 2);
      Result := Geral.Substitui(Result, '[LinkConfirma]'   , FTextoTags[09]); // '';
      Result := Geral.Substitui(Result, '[Protocolo]'      , FTextoTags[10]); // '';
      Result := Geral.Substitui(Result, '[UH]'             , FTextoTags[11]); // QrEnvEmailNO_DEPTO.Value;
      *)
      UnDmkTags.SubstituiVariaveis_Cobranca(FTextoTags[00], FTextoTags[01],
        FTextoTags[02], QrPreEmail.FieldByName('Saudacao').AsString,
        FTextoTags[04], FTextoTags[05], FTextoTags[06], FTextoTags[07],
        FTextoTags[08], FTextoTags[09], FTextoTags[10], FTextoTags[11], Result);
    end;
  end;
end;

(* Movido para o UnMail
procedure TFmMailEnv.SalvaNoDiretorioDeItensEnviados;
var
  Arq: String;
begin
  UMail.ConectaContaEmail(QrEmailConta.FieldByName('Codigo').AsInteger, QrLoc,
    Dmod.MyDB, nil, IMAP, IdSSL1);
  if IMAP.Connected then
  begin
    Arq := DateTimeToStr(Now) + '.eml';
    //
    FIdMessage.SaveToFile(Arq);
    //
    if FileExists(Arq) then
    begin
      IMAP.AppendMsgNoEncodeFromFile('INBOX.Sent', Arq, [mfSeen]);
      DeleteFile(Arq);
    end;
    IMAP.Disconnect();
  end;
end;
*)

procedure TFmMailEnv.SBCcClick(Sender: TObject);
begin
  MostraEnvEmail(1);
end;

procedure TFmMailEnv.SBCcoClick(Sender: TObject);
begin
  MostraEnvEmail(2);
end;

procedure TFmMailEnv.SBInfoDestClick(Sender: TObject);
begin
  Geral.MB_Aviso('Ao marcar esta op��o cada destinat�rio recebe uma mensagem endere�ada individualmente!' +
    sLineBreak + 'Com isso os destinat�rios n�o conhecer�o as outras pessoas que est�o recebendo esta mensagem!');
end;

procedure TFmMailEnv.TTBAlinCenClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(webbrowser1, tecAlinCen, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmMailEnv.TTBAlinEsqClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(webbrowser1, tecAlinEsq, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmMailEnv.TTBAlinDirClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(webbrowser1, tecAlinDir, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmMailEnv.TBAumRecClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecRecuoIn);
end;

procedure TFmMailEnv.TBCorClick(Sender: TObject);
begin
  dmkHTML2.FonteCor(WebBrowser1);
end;

procedure TFmMailEnv.TBDimRecClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecRecuoOut);
end;

procedure TFmMailEnv.TBItalicoClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taItalic);
end;

procedure TFmMailEnv.TBListaClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecListMarcador);
end;

procedure TFmMailEnv.TBListaNumClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecListNumeros);
end;

procedure TFmMailEnv.TBNegritoClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taBold);
end;

procedure TFmMailEnv.TBSublinhadoClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taUnderline);
end;

procedure TFmMailEnv.ToolButton15Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(webbrowser1, tecLinha);
end;

procedure TFmMailEnv.ToolButton16Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(webbrowser1, tecLink);
end;

procedure TFmMailEnv.ToolButton3Click(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taStrikeThrough);
end;

procedure TFmMailEnv.ToolButton6Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecImagem);
end;

procedure TFmMailEnv.ToolButton7Click(Sender: TObject);
begin
  Textos_Jan.ConfigarTabela(WebBrowser1);
end;

procedure TFmMailEnv.TTBSobClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taSuperscript, TTBSub, TTBSob);
end;

procedure TFmMailEnv.TTBSubClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WebBrowser1, taSubscript, TTBSub, TTBSob);
end;

procedure TFmMailEnv.TTBAlinJusClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WebBrowser1, tecAlinJus, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

end.

