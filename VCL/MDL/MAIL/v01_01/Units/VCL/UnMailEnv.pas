unit UnMailEnv;

interface

uses
  Windows, Forms, mySQLDbTables, SysUtils, DmkDAC_PF, CommCtrl, dmkGeral,
  UnDmkEnums, UnDmkProcFunc, Classes;

type
  TData = array of integer;
  PData = ^TData;
  TUnMailEnv = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ConfiguraVerifiMail(): TStringList;
    function  DiasToConfiguraVerifiMailValor(Valor: Integer): Integer;
    function  ConfiguraVerifiMailValorToDias(Valor: Integer): Integer;
    function  ConfiguraDownDias(): TStringList;
    function  ConfiguraDiasToDownDiasValor(Valor: Integer): Integer;
    function  ConfiguraDownDiasValorToDias(Valor: Integer): Integer;
    procedure MostraMailCfg(Codigo: Integer);
    function  Monta_e_Envia_Mail(Parametros: array of Variant;
                TipoEnvMail: TMaiEnvType; Anexos, TextoTags: array of String;
                MostraJanela: Boolean; IndiviDestin: Boolean = False): Boolean;
    {$IfNDef sWEB}
    function  ObtemListaDeWUsersMails(Entidades: array of Integer): String;
    function  ObtemListaDeVersoesAppMails(Aplicativo: Integer; Versao: String): String;
    {$EndIf}
    function  ObtemListaDeEntiMails(Entidade: Integer; TipEmail: array of Integer): String;
    function  ObtemListaDeCondMails(Depto: Integer): String;
  end;

var
  UMailEnv: TUnMailEnv;

implementation

uses {$IfNDef ServicoDoWindows}MyDBCheck, {$EndIf}
  MailEnv, Module, UnMyObjects, MailCfg, UnGrl_Geral;

{ TUnMailEnv }

procedure TUnMailEnv.MostraMailCfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmMailCfg, FmMailCfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmMailCfg.LocCod(Codigo, Codigo);
    FmMailCfg.ShowModal;
    FmMailCfg.Destroy;
  end;
end;

function TUnMailEnv.ConfiguraDownDias: TStringList;
var
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    Lista.Add('Os �ltimos 3 dias');
    Lista.Add('Os �ltimos 7 dias');
    Lista.Add('As 2 �ltimas semanas');
    Lista.Add('Os �ltimos 30 dias');
    Lista.Add('Qualquer per�odo (N�o recomend�vel demorar� muito mais tempo para verificar)');
  finally
    Result := Lista;
  end;
end;

function TUnMailEnv.ConfiguraDiasToDownDiasValor(Valor: Integer): Integer;
begin
  case Valor of
      3: Result := 0;
      7: Result := 1;
     14: Result := 2;
     30: Result := 3;
    else Result := 4;
  end;
end;

function TUnMailEnv.ConfiguraDownDiasValorToDias(Valor: Integer): Integer;
begin
  case Valor of
      0: Result := 3;
      1: Result := 7;
      2: Result := 14;
      3: Result := 30;
    else Result := 0;
  end;
end;

function TUnMailEnv.ConfiguraVerifiMail: TStringList;
var
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    Lista.Add('Manual');
    Lista.Add('A cada 15 minutos');
    Lista.Add('A cada 30 minutos');
    Lista.Add('A cada 1 hora');
  finally
    Result := Lista;
  end;
end;

function TUnMailEnv.DiasToConfiguraVerifiMailValor(Valor: Integer): Integer;
begin
  case Valor of
     15: Result := 1;
     30: Result := 2;
     60: Result := 3;
    else Result := 0;
  end;
end;

function TUnMailEnv.ConfiguraVerifiMailValorToDias(Valor: Integer): Integer;
begin
  case Valor of
      1: Result := 15;
      2: Result := 30;
      3: Result := 60;
    else Result := 0;
  end;
end;

function TUnMailEnv.Monta_e_Envia_Mail(Parametros: array of Variant;
  TipoEnvMail: TMaiEnvType; Anexos, TextoTags: array of String;
  MostraJanela: Boolean; IndiviDestin: Boolean = False): Boolean;
var
  QrLoc, QrLoc2: TmySQLQuery;
  EmailConta, MailCfg, PreEmail, Entidade, I: Integer;
  Para, Cco, Msg: String;
begin
  Result     := False;
  PreEmail   := 0;
  EmailConta := 0;
  //
  QrLoc := TmySQLQuery.Create(Dmod);
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDB;
  //
  QrLoc2 := TmySQLQuery.Create(Dmod);
  QrLoc2.Close;
  QrLoc2.Database := Dmod.MyDB;
  //
  case TipoEnvMail of
    meAvul: //Avulso
    begin
      //Parametros[00] = Configura��o do e-mail
      //Parametros[01] = Cco
      //Parametros[02] = Para
      //Parametros[03] = Pr� e-mail
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = Data
      PreEmail := Parametros[3];
      Cco      := Parametros[1];
      Para     := Parametros[2];
      MailCfg  := Parametros[0];
    end;
    meHisAlt: //Aplicativos - Hist�rico de altera��es
    begin
      {$IfNDef sWEB}
      //Parametros[00] = Aplicativo
      //Parametros[01] = Versao
      //Parametros[02] = Pr� e-mail
      //TextoTags[00]  = Nome do aplicativo
      //TextoTags[01]  = Vers�o do aplicativo
      //TextoTags[02]  = Lista com as novidades da vers�o
      //TextoTags[03]  = URL para acessar o hist�rico de altera��es na WEB
      PreEmail := Parametros[2];
      Cco      := '';
      Para     := ObtemListaDeVersoesAppMails(Parametros[0], Parametros[1]);
      MailCfg  := 0;
      {$Else}
      Grl_Geral.InfoSemModulo(mdlappWEB);
      {$EndIf}
    end;
    meNFSe: //NFS-e
    begin
      //Parametros[00] = N�mero da nota
      //Parametros[01] = C�digo da empresa
      //Parametros[02] = Ambiente
      //Parametros[03] = Status da nota
      //Parametros[04] = E-mail destinat�rio //Para protocolados
      //Anexos[00]     = DANFSE
      //Anexos[01]     = XML Nota
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = C�digo de verifica��o da NFSe
      //TextoTags[02]  = S�rie da nota fiscal
      //TextoTags[03]  = N�mero da nota fiscal
      //TextoTags[04]  = Protocolo para confirma�ao de recebimento de e-mails e impress�o
      //TextoTags[05]  = Protocolo para confirma�ao de recebimento de e-mails
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qrloc2, Dmod.MyDB, [
        'SELECT cab.Cliente ',
        'FROM nfsedpscab cab',
        'WHERE cab.Empresa=' + Geral.FF0(Parametros[1]),
        'AND cab.Ambiente=' + Geral.FF0(Parametros[2]),
        'AND cab.RpsIDNumero=' + Geral.FF0(Parametros[0]),
        '']);
      Entidade := QrLoc2.FieldByName('Cliente').AsInteger;
      //
      //Obtem pre-email
      if Parametros[3] = 100 then //Autorizada
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
          'SELECT NFSePreMailAut, MyEmailNFSe, NFSeTipCtoMail ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('NFSePreMailAut').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFSe').AsString;
        //
        if Length(Parametros) = 5 then
          Para := Parametros[04]
        else
          Para := ObtemListaDeEntiMails(Entidade, [QrLoc.FieldByName('NFSeTipCtoMail').AsInteger]);
        //
        MailCfg  := 0;
      end else
      begin  //Cancelada
        UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
          'SELECT NFSePreMailCan, MyEmailNFSe, NFSeTipCtoMail ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('NFSePreMailCan').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFSe').AsString;
        //
        if Length(Parametros) = 5 then
          Para := Parametros[04]
        else
          Para := ObtemListaDeEntiMails(Entidade, [QrLoc.FieldByName('NFSeTipCtoMail').AsInteger]);
        //
        MailCfg  := 0;
      end;
    end;
    meNFe: //NF-e
    begin
      //Parametros[00] = Entidade
      //Parametros[01] = C�digo da empresa
      //Parametros[02] = Status da nota
      //Anexos[00]     = DANFE
      //Anexos[01]     = XML Nota
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = ChaveNFe
      //TextoTags[02]  = S�rieNFe
      //TextoTags[03]  = N�meroNFe
      //TextoTags[04]  = Nome fantasia do cliente
      //
      Entidade := Parametros[0];
      //
      if Parametros[2] = 100 then //Autorizada
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
          'SELECT PreMailAut, MyEmailNFe, EntiTipCto ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('PreMailAut').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFe').AsString;
        Para     := ObtemListaDeEntiMails(Entidade, [QrLoc.FieldByName('EntiTipCto').AsInteger]);
        MailCfg  := 0;
      end else
      begin //Cancelada
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
          'SELECT PreMailCan, MyEmailNFe, EntiTipCto ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('PreMailCan').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFe').AsString;
        Para     := ObtemListaDeEntiMails(Entidade, [QrLoc.FieldByName('EntiTipCto').AsInteger]);
        MailCfg  := 0;
      end;
    end;
    meNFeEveCCE: //Envento Carta de Corre��o da NF-e
    begin
      //Parametros[00] = Entidade
      //Parametros[01] = C�digo da empresa
      //Parametros[02] = Status do evento
      //Anexos[00]     = XML do evento
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = ChaveNFe
      //TextoTags[02]  = versao
      //TextoTags[03]  = ORGAO
      //TextoTags[04]  = AMBIENTE
      //TextoTags[05]  = CNPJ_CPF
      //TextoTags[06]  = CHAVE_ACESSO
      //TextoTags[07]  = DATA
      //TextoTags[08]  = CODIGO_EVENTO
      //TextoTags[09]  = SEQUENCIAL_EVENTO
      //TextoTags[10]  = verEvento
      //TextoTags[11]  = descEvento
      //TextoTags[12]  = xCorrecao
      //TextoTags[13]  = xCondUso
      //TextoTags[14]  = SerieNFe
      //TextoTags[15]  = NumeroNFe
      //
      Entidade := Parametros[0];
      //
      if (Parametros[2] = '135') or (Parametros[2] = '136') then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
          'SELECT PreMailEveCCe, MyEmailNFe, EntiTipCto, EntiTipCt1 ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('PreMailEveCCe').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFe').AsString;
        Para     := ObtemListaDeEntiMails(Entidade,
                    [QrLoc.FieldByName('EntiTipCto').AsInteger,
                    QrLoc.FieldByName('EntiTipCt1').AsInteger]);
        MailCfg  := 0;
      end;
    end;
    meNFeEveCan: //Envento Cancelamento da NF-e
    begin
      //Parametros[00] = Entidade
      //Parametros[01] = C�digo da empresa
      //Parametros[02] = Status do evento
      //Anexos[00]     = XML do evento
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = ChaveNFe
      //TextoTags[02]  = SerieNFe
      //TextoTags[03]  = NumeroNFe
      Entidade := Parametros[0];
      //
      if (Parametros[2] = '135') or (Parametros[2] = '136') then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
          'SELECT PreMailCan, MyEmailNFe, EntiTipCto, EntiTipCt1 ',
          'FROM paramsemp ',
          'WHERE Codigo=' + Geral.FF0(Parametros[1]),
          '']);
        PreEmail := QrLoc.FieldByName('PreMailCan').AsInteger;
        Cco      := QrLoc.FieldByName('MyEmailNFe').AsString;
        Para     := ObtemListaDeEntiMails(Entidade,
                    [QrLoc.FieldByName('EntiTipCto').AsInteger,
                    QrLoc.FieldByName('EntiTipCt1').AsInteger]);
        MailCfg  := 0;
      end;
    end;
    meBloq, meBloqCnd: //Bloqueto
    begin
      //Parametros[00] = Pr� e-mail
      //Parametros[01] = E-mail destinat�rio
      //Anexos[00]     = Boleto em PDF
      //TextoTags[00]  = Nome do cliente
      //TextoTags[01]  = Valor do boleto
      //TextoTags[02]  = Vencimento do boleto
      //TextoTags[03]  = Linha digit�vel
      //TextoTags[04]  = Protocolo + impressao (confirma��o de recebimento + impress�o do boleto na WEB)
      //TextoTags[05]  = Protocolo (confirma��o de recebimento via bot�o de confirma��o)

      PreEmail := Parametros[0];
      Cco      := '';
      Para     := Parametros[1];
      MailCfg  := 0;
    end;
    meCobr: // Cobran�a
    begin
      //Parametros[00] = UH
      //Parametros[01] = Pr� e-mail
      //
      PreEmail := Parametros[0];
      Cco      := '';
      Para     := ObtemListaDeCondMails(Parametros[1]);
      MailCfg  := 0;
    end;
  end;
  QrLoc.Free;
  QrLoc2.Free;
  //
  {$IfNDef ServicoDoWindows}
    if DBCheck.CriaFm(TFmMailEnv, FmMailEnv, afmoNegarComAviso) then
  {$Else}
    Application.CreateForm(TFmMailEnv, FmMailEnv);
  {$EndIf}
  begin
    FmMailEnv.FCCo          := Cco;
    FmMailEnv.FPara         := Para;
    FmMailEnv.FIndiviDestin := IndiviDestin;
    FmMailEnv.FMailCfg      := MailCfg;
    //
    SetLength(FmMailEnv.FAnexos, Length(Anexos));
    //
    for I := Low(Anexos) to High(Anexos) do
      FmMailEnv.FAnexos[I] := Anexos[I];
    //
    SetLength(FmMailEnv.FTextoTags, Length(TextoTags));
    //
    for I := 0 to High(TextoTags) do
      FmMailEnv.FTextoTags[I] := TextoTags[I];
    //
    FmMailEnv.FTipoEmail  := TipoEnvMail;
    FmMailEnv.FPreEmail   := PreEmail;
    FmMailEnv.FEmailConta := EmailConta;
    //
    FmMailEnv.ConfiguraEmail();
    if MostraJanela then
      FmMailEnv.ShowModal
    else
      FmMailEnv.EnviaEmail(Msg);
    Result := FmMailEnv.FEnviou;
    //
    FmMailEnv.Destroy;
  end;
end;

function TUnMailEnv.ObtemListaDeCondMails(Depto: Integer): String;
var
  QrLoc: TmySQLQuery;
  Texto: String;
begin
  Texto := '';
  //
  QrLoc := TmySQLQuery.Create(Dmod);
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
  'SELECT Pronome, Nome, Emeio  ',
  'FROM condemeios ',
  'WHERE Conta=' + Geral.FF0(Depto),
  '']);
  if QrLoc.RecordCount > 0 then
  begin
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      Texto := Texto + QrLoc.FieldByName('Emeio').AsString + ';';
      //
      QrLoc.Next;
    end;
  end;
  QrLoc.Free;
  Result := Texto;
end;

{$IfNDef sWEB}
function TUnMailEnv.ObtemListaDeVersoesAppMails(Aplicativo: Integer; Versao: String): String;

  procedure ObtemListaEntidades(Query: TmySQLQuery; CampoEntidade: String;
    var ListaEnt: String);
  var
    Entidade: Integer;
  begin
    if Query.RecordCount > 0 then
    begin
      ListaEnt := '';
      //
      while not Query.Eof do
      begin
        Entidade := Query.FieldByName(CampoEntidade).AsInteger;
        //
        if Query.RecNo = Query.RecordCount then
          ListaEnt := ListaEnt + Geral.FF0(Entidade)
        else
          ListaEnt := ListaEnt + Geral.FF0(Entidade) + ', ';
        //
        Query.Next;
      end;
    end;
  end;

var
  ListaStr: TStringList;
  Lista: array of Integer;
  QrLoc: TmySQLQuery;
  Codigo, Tipo, Entidade: Integer;
  ListaEnt: String;
  I: Integer;
begin
  Result := '';
  //
  QrLoc := TmySQLQuery.Create(Dmod.MyDBn);
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDBn;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT wve.Codigo, wve.Tipo ',
    'FROM wversao wve',
    'WHERE wve.Aplicativo=' + Geral.FF0(Aplicativo),
    'AND wve.Versao = "' + Versao + '"',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    Codigo := QrLoc.FieldByName('Codigo').AsInteger;
    Tipo   := QrLoc.FieldByName('Tipo').AsInteger;
    //
    if Tipo = 0 then //BETA
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
        'SELECT Cliente ',
        'FROM ',
        '( ',
        'SELECT cli.Cliente ',
        'FROM wversaoits its ',
        'LEFT JOIN cliaplicli ali ON ali.Conta = its.Dispositivo ',
        'LEFT JOIN cliaplic cap ON cap.Controle = ali.Controle ',
        'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
        'WHERE its.Codigo=' + Geral.FF0(Codigo),
        'AND cli.Ativo = 1 ',
        'AND cli.Cliente > 0 ',
        ' ',
        'UNION ',
        ' ',
        'SELECT cli.Cliente ',
        'FROM cliaplicli ali ',
        'LEFT JOIN cliaplic cap ON cap.Controle = ali.Controle ',
        'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
        'WHERE (ali.Beta=1 OR cap.Beta=1) ',
        'AND cli.Cliente > 0 ',
        'AND cap.Aplicativo=' + Geral.FF0(Aplicativo),
        'AND cli.Ativo = 1 ',
        ') betaver ',
        'GROUP BY Cliente ',
        '']);
      ObtemListaEntidades(QrLoc, 'Cliente', ListaEnt);
    end else
    begin
      //Produ��o
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
        'SELECT cli.Cliente ',
        'FROM cliaplicli ali ',
        'LEFT JOIN cliaplic cap ON cap.Controle = ali.Controle ',
        'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
        'WHERE cap.Aplicativo=' + Geral.FF0(Aplicativo),
        'AND cli.Ativo = 1 ',
        'AND cli.Cliente > 0 ',
        'GROUP BY cli.Codigo ',
        '']);
      ObtemListaEntidades(QrLoc, 'Cliente', ListaEnt);
    end;
  end;
  if ListaEnt <> '' then
  begin
    ListaStr := TStringList.Create;
    try
      ListaStr := Geral.Explode(ListaEnt, ',', 1);
      //
      if ListaStr.Count > 0 then
      begin
        SetLength(Lista, ListaStr.Count);
        //
        for I := 0 to ListaStr.Count - 1 do
        begin
          Lista[I] := Geral.IMV(ListaStr[I]);
        end;
        //
        Result := ObtemListaDeWUsersMails(Lista);
      end;
    finally
      ListaStr.Free;
    end;
  end;
end;
{$EndIf}

{$IfNDef sWEB}
function TUnMailEnv.ObtemListaDeWUsersMails(Entidades: array of Integer): String;
var
  QrLoc: TmySQLQuery;
  i, j: Integer;
  ListaEntidades, Texto, Email: String;
begin
  Texto          := '';
  ListaEntidades := '';
  j              := High(Entidades);
  //
  for i := Low(Entidades) to j do
  begin
    if j = i then
      ListaEntidades := ListaEntidades + Geral.FF0(Entidades[i])
    else
      ListaEntidades := ListaEntidades + Geral.FF0(Entidades[i]) + ', ';
  end;
  QrLoc := TmySQLQuery.Create(Dmod.MyDBn);
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDBn;
  UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDBn, [
    'SELECT wus.Email ',
    'FROM wusers wus ',
    'LEFT JOIN entidades ent ON ent.Codigo = wus.Entidade ',
    'WHERE wus.Ativo = 1 ',
    'AND wus.Email <> "" ',
    'AND ent.Ativo = 1 ',
    'AND wus.Entidade IN (' + ListaEntidades + ') ',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      if QrLoc.RecNo = 0 then
      begin
        Texto := QrLoc.FieldByName('Email').AsString + ';';
      end else
      begin
        Email := QrLoc.FieldByName('Email').AsString;
        //
        if Pos(Trim(Email), Texto) = 0 then
          Texto := Texto + Email + ';';
      end;
      //
      QrLoc.Next;
    end;
  end;
  QrLoc.Free;
  Result := Texto;
end;
{$EndIf}

function TUnMailEnv.ObtemListaDeEntiMails(Entidade: Integer; TipEmail: array of Integer): String;
var
  QrLoc: TmySQLQuery;
  i, j: Integer;
  TipoEmail, Texto: String;
begin
  Texto     := '';
  TipoEmail := '';
  j         := High(TipEmail);
  //
  for i := Low(TipEmail) to j do
  begin    
    if j = i then
      TipoEmail := TipoEmail + Geral.FF0(TipEmail[i])
    else
      TipoEmail := TipoEmail + Geral.FF0(TipEmail[i]) + ', ';
  end;
  QrLoc := TmySQLQuery.Create(Dmod);
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
    'SELECT enm.Email ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'LEFT JOIN entimail enm ON enm.Controle = eco.Controle ',
    'WHERE ece.Codigo=' + Geral.FF0(Entidade),
    'AND enm.EntiTipCto IN (' + TipoEmail + ') ',
    'GROUP BY enm.Email ',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      if QrLoc.RecNo = 0 then
        Texto := QrLoc.FieldByName('EMail').AsString + ';'
      else
        Texto := Texto + QrLoc.FieldByName('EMail').AsString + ';';
      //
      QrLoc.Next;
    end;
  end;
  QrLoc.Free;
  Result := Texto;
end;

end.
